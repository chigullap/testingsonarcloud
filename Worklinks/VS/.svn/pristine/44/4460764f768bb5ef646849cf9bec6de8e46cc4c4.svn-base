﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class UnionCollectiveAgreement : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long UnionCollectiveAgreementId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String EnglishDescription { get; set; }

        [DataMember]
        public String FrenchDescription { get; set; }

        [DataMember]
        public long LabourUnionId { get; set; }

        [DataMember]
        public DateTime? CollectiveAgreementStartDate { get; set; }

        [DataMember]
        public DateTime? CollectiveAgreementEndDate { get; set; }

        [DataMember]
        public long? AttachmentId { get; set; }

        [DataMember]
        public AttachmentCollection AttachmentObjectCollection { get; set; }

        //public property
        public String Description
        {
            get
            {
                if (AttachmentObjectCollection != null && AttachmentObjectCollection.Count > 0)
                    return AttachmentObjectCollection[0].Description;
                else
                    return null;
            }
        }
        #endregion

        #region construct/destruct
        public UnionCollectiveAgreement()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(UnionCollectiveAgreement data)
        {
            base.CopyTo(data);

            data.UnionCollectiveAgreementId = UnionCollectiveAgreementId;
            data.EnglishDescription = EnglishDescription;
            data.FrenchDescription = FrenchDescription;
            data.LabourUnionId = LabourUnionId;
            data.CollectiveAgreementStartDate = CollectiveAgreementStartDate;
            data.CollectiveAgreementEndDate = CollectiveAgreementEndDate;
            data.AttachmentId = AttachmentId;

            //attachments
            if (AttachmentObjectCollection != null)
            {
                data.AttachmentObjectCollection = new AttachmentCollection();
                AttachmentObjectCollection.CopyTo(data.AttachmentObjectCollection);
            }
            else
                data.AttachmentObjectCollection = AttachmentObjectCollection;
        }
        public new void Clear()
        {
            base.Clear();

            UnionCollectiveAgreementId = -1;
            EnglishDescription = null;
            FrenchDescription = null;
            LabourUnionId = -1;
            CollectiveAgreementStartDate = null;
            CollectiveAgreementEndDate = null;
            AttachmentId = -1;
            AttachmentObjectCollection = null;
        }
        #endregion
    }
}