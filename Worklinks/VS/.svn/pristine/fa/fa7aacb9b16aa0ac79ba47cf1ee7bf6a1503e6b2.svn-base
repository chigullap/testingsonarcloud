﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WorkLinks.Common.Security;

namespace WorkLinks.Payroll.RoeCreation
{
    public partial class EmployeeRoeCreationPageControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private int FirstVisibleTab
        {
            get
            {
                if (RoleForm.EmployeeTerminationDetail.ViewFlag)
                    return 0;
                if (RoleForm.EmployeeTerminationROE.ViewFlag)
                    return 1;
                if (RoleForm.RoeCreationDetails.ViewFlag)
                    return 2;
                return -1;
            }
        }
        private String LastName
        {
            get
            {
                if (Page.RouteData.Values["lastName"] == null)
                    return null;
                else
                    return Page.RouteData.Values["lastName"].ToString();
            }
        }
        private string FirstName
        {
            get
            {
                if (Page.RouteData.Values["firstName"] == null)
                    return null;
                else
                    return Page.RouteData.Values["firstName"].ToString();
            }
        }
        private int EmployeePositionId
        {
            get
            {
                if (Page.RouteData.Values["employeePositionId"] == null)
                    return -1;
                else
                    return Convert.ToInt16(Page.RouteData.Values["employeePositionId"]);
            }
        }
        private String TabToSelect
        {
            get
            {
                if (Page.RouteData.Values["tabNameToSelect"] == null)
                    return null;
                else
                    return Page.RouteData.Values["tabNameToSelect"].ToString();
            }
        }
        private String PayrollProcessGroupCode
        {
            get
            {
                if (Page.RouteData.Values["payrollProcessGroupCode"] == null)
                    return null;
                else
                    return Page.RouteData.Values["payrollProcessGroupCode"].ToString();
            }
        }
       
        protected bool IsViewMode
        {
            get
            {
                return EmployeeTerminationControl1.IsViewMode
                    && EmployeeTerminationROEControl1.IsViewMode
                    && EmployeeRoeEarningsHoursControl1.IsViewMode;
            }
        }

        protected bool IsEditMode
        {
            get
            {
                return EmployeeTerminationControl1.IsEditMode;
            }
        }

        protected bool IsInsertMode
        {
            get
            {
                return EmployeeTerminationControl1.IsInsertMode;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.EmployeeTerminationControl1.EmployeeSelected
                    += new HumanResources.Terminations.EmployeeTerminationControl.EmployeeSelectedEventHandler(EmployeeTerminationControl1_EmployeeSelected);

                Page.Title = String.Format("ROE - {0}, {1}", LastName, FirstName);

                //select a tab if we are refreshing after an update
                SelectTab(TabToSelect);
            }

            //if this is an update, subscribe so we can refresh the data in the other tabs.
            if (IsEditMode)
            {
                this.EmployeeTerminationControl1.EmployeeSelected
                       += new HumanResources.Terminations.EmployeeTerminationControl.EmployeeSelectedEventHandler(EmployeeTerminationControl1_EmployeeSelected);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            InitializeControls();
            base.OnPreRender(e);
        }
        protected void InitializeControls()
        {
            RadTabStrip1.Enabled = IsViewMode;
        }

        private void SelectTab(string tabToSelect)
        {
            if (tabToSelect == EmployeeTerminationROEControl1.ID)
            {
                EmployeeTerminationROEControlPage.Selected = true;
                RadTabStrip1.SelectedIndex = 1;
            }
            else if (tabToSelect == EmployeeRoeEarningsHoursControl1.ID)
            {
                EmployeeRoeEarningsHoursControlPage.Selected = true;
                RadTabStrip1.SelectedIndex = 2;
            }
            else //default to the terminations tab
            {
                EmployeeTerminationControlPage.Selected = true;
                RadTabStrip1.SelectedIndex = 0;
            }
        }

        void EmployeeTerminationControl1_EmployeeSelected(object sender, HumanResources.Employee.Biographical.EmployeeSelectedEventArgs e)
        {
            //pass the employee_position_id to the other controls in this page
            this.EmployeeTerminationROEControl1.SetEmployeePositionId(e.EmployeePositionId);
            this.EmployeeRoeEarningsHoursControl1.SetEmployeePositionId(e.EmployeePositionId, PayrollProcessGroupCode);
        }

        protected void EmployeeTermination_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = RoleForm.EmployeeTerminationDetail.ViewFlag; }
        protected void EmployeeTerminationROE_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = RoleForm.EmployeeTerminationROE.ViewFlag; }
        protected void EmployeeRoeEarningsHours_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = RoleForm.RoeCreationDetails.ViewFlag; }

        protected void RadTabStrip1_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                RadTabStrip1.SelectedIndex = FirstVisibleTab;
        }

        protected void EmployeeMultiPage_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                EmployeeMultiPage.SelectedIndex = FirstVisibleTab;
        }
    }
}