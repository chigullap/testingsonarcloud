﻿<?xml version="1.0" encoding="utf-8"?>
<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="RemittanceImport" nillable="true" type="RemittanceImport" />
  <xs:complexType name="RemittanceImport">
    <xs:complexContent mixed="false">
      <xs:extension base="BusinessObject">
        <xs:sequence>
          <xs:element minOccurs="1" maxOccurs="1" name="RemittanceImportId" type="xs:long" />
          <xs:element minOccurs="0" maxOccurs="1" name="RemittanceImportStatusCode" type="xs:string" />
          <xs:element minOccurs="1" maxOccurs="1" name="ImportExportLogId" nillable="true" type="xs:long" />
          <xs:element minOccurs="1" maxOccurs="1" name="PayrollProcessId" nillable="true" type="xs:long" />
          <xs:element minOccurs="0" maxOccurs="1" name="RemittanceImportDetail" type="ArrayOfRemittanceImportDetail" />
          <xs:element minOccurs="1" maxOccurs="1" name="ImportDate" type="xs:dateTime" />
          <xs:element minOccurs="0" maxOccurs="1" name="Notes" type="xs:string" />
          <xs:element minOccurs="1" maxOccurs="1" name="WarningFlag" type="xs:boolean" />
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  <xs:complexType name="BusinessObject" abstract="true">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="1" name="RowVersion" type="xs:base64Binary" />
      <xs:element minOccurs="0" maxOccurs="1" name="CreateUser" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="CreateDatetime" nillable="true" type="xs:dateTime" />
      <xs:element minOccurs="0" maxOccurs="1" name="UpdateUser" type="xs:string" />
      <xs:element minOccurs="1" maxOccurs="1" name="UpdateDatetime" nillable="true" type="xs:dateTime" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="RemittanceImportDetail">
    <xs:complexContent mixed="false">
      <xs:extension base="BusinessObject">
        <xs:sequence>
          <xs:element minOccurs="1" maxOccurs="1" name="RemittanceImportDetailId" type="xs:long" />
          <xs:element minOccurs="1" maxOccurs="1" name="RemittanceImportId" type="xs:long" />
          <xs:element minOccurs="0" maxOccurs="1" name="EmployeeImportExternalIdentifier" type="xs:string" />
          <xs:element minOccurs="0" maxOccurs="1" name="LastName" type="xs:string" />
          <xs:element minOccurs="0" maxOccurs="1" name="FirstName" type="xs:string" />
          <xs:element minOccurs="0" maxOccurs="1" name="FederalBusinessNumber" type="xs:string" />
          <xs:element minOccurs="0" maxOccurs="1" name="QuebecBusinessNumber" type="xs:string" />
          <xs:element minOccurs="0" maxOccurs="1" name="WorkersCompensationImportExternalIdentifier" type="xs:string" />
          <xs:element minOccurs="1" maxOccurs="1" name="WorkersCompensationAssessable" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="1" maxOccurs="1" name="EmployerWorkersCompensationAmount" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="0" maxOccurs="1" name="EmployerHealthTaxProvinceStateCode" type="xs:string" />
          <xs:element minOccurs="1" maxOccurs="1" name="EmployerHealthTaxAssessable" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="1" maxOccurs="1" name="EmployerHealthTax" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="1" maxOccurs="1" name="GrossTaxableIncome" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="1" maxOccurs="1" name="FederalTax" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="1" maxOccurs="1" name="CanadaPensionPlan" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="1" maxOccurs="1" name="EmployerCanadaPensionPlan" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="1" maxOccurs="1" name="EmploymentInsurance" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="1" maxOccurs="1" name="EmployerEmploymentInsurance" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="1" maxOccurs="1" name="QuebecTax" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="1" maxOccurs="1" name="QuebecPensionPlan" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="1" maxOccurs="1" name="EmployerQuebecPensionPlan" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="1" maxOccurs="1" name="QuebecParentalInsurancePlan" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="1" maxOccurs="1" name="EmployerQuebecParentalInsurancePlan" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="1" maxOccurs="1" name="NunavutTax" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="1" maxOccurs="1" name="NorthwestTerritoriesTax" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="1" maxOccurs="1" name="CanadaRevenueAgencyGarnishment" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="0" maxOccurs="1" name="SocialInsuranceNumber" type="xs:string" />
          <xs:element minOccurs="1" maxOccurs="1" name="RevenueQuebecGarnishement" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="0" maxOccurs="1" name="ThirdPartyPaycode1" type="xs:string" />
          <xs:element minOccurs="1" maxOccurs="1" name="ThirdPartyAmount1" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="0" maxOccurs="1" name="ThirdPartyPaycode2" type="xs:string" />
          <xs:element minOccurs="1" maxOccurs="1" name="ThirdPartyAmount2" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="0" maxOccurs="1" name="ThirdPartyPaycode3" type="xs:string" />
          <xs:element minOccurs="1" maxOccurs="1" name="ThirdPartyAmount3" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="0" maxOccurs="1" name="ThirdPartyPaycode4" type="xs:string" />
          <xs:element minOccurs="1" maxOccurs="1" name="ThirdPartyAmount4" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="0" maxOccurs="1" name="ThirdPartyPaycode5" type="xs:string" />
          <xs:element minOccurs="1" maxOccurs="1" name="ThirdPartyAmount5" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="0" maxOccurs="1" name="ThirdPartyPaycode6" type="xs:string" />
          <xs:element minOccurs="1" maxOccurs="1" name="ThirdPartyAmount6" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="0" maxOccurs="1" name="ThirdPartyPaycode7" type="xs:string" />
          <xs:element minOccurs="1" maxOccurs="1" name="ThirdPartyAmount7" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="0" maxOccurs="1" name="ThirdPartyPaycode8" type="xs:string" />
          <xs:element minOccurs="1" maxOccurs="1" name="ThirdPartyAmount8" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="0" maxOccurs="1" name="ThirdPartyPaycode9" type="xs:string" />
          <xs:element minOccurs="1" maxOccurs="1" name="ThirdPartyAmount9" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="0" maxOccurs="1" name="ThirdPartyPaycode10" type="xs:string" />
          <xs:element minOccurs="1" maxOccurs="1" name="ThirdPartyAmount10" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="1" maxOccurs="1" name="PayrollChqAmount" nillable="true" type="xs:decimal" />
          <xs:element minOccurs="0" maxOccurs="1" name="AddressLine1" type="xs:string" />
          <xs:element minOccurs="0" maxOccurs="1" name="AddressLine2" type="xs:string" />
          <xs:element minOccurs="0" maxOccurs="1" name="City" type="xs:string" />
          <xs:element minOccurs="0" maxOccurs="1" name="PostalZipCode" type="xs:string" />
          <xs:element minOccurs="0" maxOccurs="1" name="ProvinceStateCode" type="xs:string" />
          <xs:element minOccurs="0" maxOccurs="1" name="CountryCode" type="xs:string" />
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  <xs:complexType name="ArrayOfRemittanceImportDetail">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="RemittanceImportDetail" nillable="true" type="RemittanceImportDetail" />
    </xs:sequence>
  </xs:complexType>
</xs:schema>