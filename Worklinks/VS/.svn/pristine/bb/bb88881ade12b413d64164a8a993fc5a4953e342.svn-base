<?xml version="1.0" encoding="utf-8"?>
<!--
     The notices below are provided with respect to licensed content incorporated herein:
     Copyright NorthgateArinso 2013. All Rights Reserved. http://www.ngahr.com
     Copyright HR-XML Consortium. All Rights Reserved. http://www.hrxmlconsortium.org and http://www.hr-xml.org. Terms of license can be found in license.txt.
     Copyright (c) 1997-2011 Open Applications Group, Inc. All Rights Reserved.  http://www.openapplications.org
     -->
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	xmlns:oa="http://www.openapplications.org/oagis/9" xmlns:hr="http://www.hr-xml.org/3"
	xmlns="http://www.ngahr.com/ngapexxml/1" targetNamespace="http://www.ngahr.com/ngapexxml/1"
	elementFormDefault="qualified" attributeFormDefault="unqualified">
	<xsd:annotation>
		<xsd:documentation> 
		Name: ConfirmPayServEmp.xsd 
		Status: 1.06 Release 
		Date this version: 2014-Jun-3 
		</xsd:documentation>
	</xsd:annotation>

	<xsd:import namespace="http://www.openapplications.org/oagis/9"
		schemaLocation="../Oagis/Nouns/BOD.xsd"/>
	<xsd:import namespace="http://www.hr-xml.org/3"
		schemaLocation="../HRXML/Developer/Nouns/IndicativeData.xsd"/>
	<xsd:include schemaLocation="../NgaHR/Components.xsd"/>

	<xsd:element name="ConfirmPayServEmp" type="ConfirmPayServEmpType"/>
	<xsd:complexType name="ConfirmPayServEmpType">
		<xsd:annotation>
			<xsd:documentation> ConfirmPayServEmp is used to communicate relative to the
				ProcessPayServEmp BOD. It is used to confirm back to the sender that the BOD was
				processed and report on any errors that were encountered. PayServEmp contains all
				data needed for a payroll service to run a correct payroll for that employee. It
				consists out of the standard HR-XML IndicativeData Noun and the NGA HR specific
				payroll related added elements. </xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="oa:BusinessObjectDocumentType">
				<xsd:sequence>
					<xsd:element name="DataArea" type="ConfirmPayServEmpDataAreaType" minOccurs="0"
						maxOccurs="1">
						<xsd:annotation>
							<xsd:documentation> The DataArea is the part of the BOD that contains
								business information. This information consists of a verb and one or
								more noun instances. </xsd:documentation>
						</xsd:annotation>
					</xsd:element>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:complexType name="ConfirmPayServEmpDataAreaType">
		<xsd:sequence>
			<xsd:element ref="oa:Confirm" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BOD" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="AlternateIdentifiers" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:element name="BOD" type="BODType"/>
	<xsd:complexType name="BODType">
		<xsd:sequence>
			<xsd:element ref="oa:OriginalApplicationArea" minOccurs="0"/>
			<xsd:group ref="oa:FreeFormTextGroup" minOccurs="0"/>
			<xsd:element ref="BODFailureMessage" minOccurs="0"/>
			<xsd:element ref="BODSuccessMessage" minOccurs="0"/>
			<xsd:element ref="oa:PartialBODFailureMessage" minOccurs="0"/>
			<xsd:element ref="oa:UserArea" minOccurs="0"/>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:element name="BODFailureMessage" type="BODFailureMessageType"/>
	<xsd:complexType name="BODFailureMessageType">
		<xsd:sequence>
			<xsd:element ref="ErrorProcessMessage" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="WarningProcessMessage" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="oa:NounFailureMessage" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="oa:NounSuccessMessage" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="oa:UserArea" minOccurs="0"/>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:element name="BODSuccessMessage" type="BODSuccessMessageType"/>
	<xsd:complexType name="BODSuccessMessageType">
		<xsd:sequence>
			<xsd:element ref="WarningProcessMessage" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="oa:NounSuccessMessage" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="oa:UserArea" minOccurs="0"/>
		</xsd:sequence>
	</xsd:complexType>
</xsd:schema>
