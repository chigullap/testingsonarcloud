﻿using System;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.PayrollBatch
{
    public partial class PayrollBatchSearchControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected PayrollBatchCriteria Criteria
        {
            get
            {
                PayrollBatchCriteria criteria = new PayrollBatchCriteria();
                criteria.BatchSource = (String)BatchSource.Value;
                criteria.Description = (String)Description.Value;
                criteria.PayrollProcessGroupCode = (String)PayGroupCode.Value;
                criteria.PayrollProcessRunTypeCode = (String)PayrollProcessRunTypeCode.Value;
                criteria.PayrollBatchStatusCode = (String)PayrollBatchStatusCode.Value;
                criteria.ProcessedFlag = (bool)ProcessedFlag.Value;

                return criteria;
            }
            set
            {
                BatchSource.Value = value.BatchSource;
                Description.Value = value.Description;
                PayGroupCode.Value = value.PayrollProcessGroupCode;
                PayrollProcessRunTypeCode.Value = value.PayrollProcessRunTypeCode;
                PayrollBatchStatusCode.Value = value.PayrollBatchStatusCode;
                ProcessedFlag.Value = value.ProcessedFlag;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.PayrollBatchSearch.ViewFlag);

            if (!IsPostBack)
            {
                Panel1.DataBind();
                PayrollBatchSearchResultsControl.SetPagerStyle(true);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search();
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Criteria = new PayrollBatchCriteria();
            SetPayrollBatchData(null);
        }
        public void Search()
        {
            PayrollBatchReportCollection collection = new PayrollBatchReportCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetPayrollBatchReport(Criteria));
            SetPayrollBatchData(collection);
        }
        private void SetPayrollBatchData(PayrollBatchReportCollection collection)
        {
            PayrollBatchSearchResultsControl.SetPayrollBatchData(collection);
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        #endregion
    }
}