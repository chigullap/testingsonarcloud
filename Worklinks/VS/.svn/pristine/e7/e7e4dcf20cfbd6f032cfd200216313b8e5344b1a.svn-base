﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerContactChannel : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ContactChannelId = "contact_channel_id";
            public static String ContactChannelTypeCode = "code_contact_channel_type_cd";
            public static String PrimaryContactValue = "primary_contact_value";
            public static String SecondaryContactValue = "secondary_contact_value";
        }
        #endregion

        #region main
        internal ContactChannelCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? contactChannelId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ContactChannel_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@contactChannelId", contactChannelId);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToContactChannelCollection(reader);
                }
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ContactChannel channel)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("ContactChannel_update", user.DatabaseName))
                {

                    command.BeginTransaction("ContactChannel_update");

                    command.AddParameterWithValue("@contactChannelId", channel.ContactChannelId);
                    command.AddParameterWithValue("@contactChannelTypeCode", channel.ContactChannelTypeCode);
                    command.AddParameterWithValue("@primaryContactValue", channel.PrimaryContactValue);
                    command.AddParameterWithValue("@secondaryContactValue", channel.SecondaryContactValue);
                    command.AddParameterWithValue("@overrideConcurrencyCheckFlag", channel.OverrideConcurrencyCheckFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", channel.RowVersion, ParameterDirection.InputOutput);

                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    channel.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
         }

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ContactChannel channel)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("ContactChannel_insert", user.DatabaseName))
                {
                    command.BeginTransaction("ContactChannel_insert");

                    SqlParameter ContactChannelIdParm = command.AddParameterWithValue("@contactChannelId", channel.ContactChannelId, ParameterDirection.Output);
                    command.AddParameterWithValue("@contactChannelTypeCode", channel.ContactChannelTypeCode);
                    command.AddParameterWithValue("@primaryContactValue", channel.PrimaryContactValue);
                    command.AddParameterWithValue("@secondaryContactValue", channel.SecondaryContactValue);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", channel.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    channel.ContactChannelId = Convert.ToInt64(ContactChannelIdParm.Value);
                    channel.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();

                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ContactChannel channel)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("ContactChannel_delete", user.DatabaseName))
                {
                    command.BeginTransaction("ContactChannel_delete");
                    command.AddParameterWithValue("@contactChannelId", channel.ContactChannelId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }


        public void ImportPhone(String database, String username, System.Collections.Generic.List<PhoneImport> phone)
        {
            DataBaseCommand command = GetStoredProcCommand("ContactChannel_batchInsert", database);

            using (command)
            {
                command.BeginTransaction("ContactChannel_batchInsert");

                //table parameter
                SqlParameter parmPhone = new SqlParameter();
                parmPhone.ParameterName = "@parmContactChannel";
                parmPhone.SqlDbType = SqlDbType.Structured;

                DataTable tablePhoneNos = LoadPhoneDataTableImport(phone);
                parmPhone.Value = tablePhoneNos;
                command.AddParameter(parmPhone);

                command.AddParameterWithValue("@updateUser", username);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }


        private DataTable LoadPhoneDataTableImport(System.Collections.Generic.List<PhoneImport> phone)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_identifier", typeof(String));
            table.Columns.Add("primary_contact_value", typeof(String));
            table.Columns.Add("secondary_contact_value", typeof(String));
            table.Columns.Add("code_contact_channel_type_cd", typeof(String));


            foreach (PhoneImport item in phone)
            {
                table.Rows.Add(new Object[]
                {
                    item.EmployeeIdentifier,
                    item.PrimaryContactValue,
                    item.SecondaryContactValue,
                    item.contactChanelType
                });
            }

            return table;
        }
        #endregion

        #region data mapping
        protected ContactChannelCollection MapToContactChannelCollection(IDataReader reader)
        {
            ContactChannelCollection collection = new ContactChannelCollection();
            while (reader.Read())
            {
                collection.Add(MapToContactChannel(reader));
            }

            return collection;
        }
        protected ContactChannel MapToContactChannel(IDataReader reader)
        {
            ContactChannel contact = new ContactChannel();
            base.MapToBase(contact, reader);

            contact.ContactChannelId=(long)CleanDataValue(reader[ColumnNames.ContactChannelId]);
            contact.ContactChannelTypeCode=(String)CleanDataValue(reader[ColumnNames.ContactChannelTypeCode]);
            contact.PrimaryContactValue=(String)CleanDataValue(reader[ColumnNames.PrimaryContactValue]);
            contact.SecondaryContactValue = (String)CleanDataValue(reader[ColumnNames.SecondaryContactValue]);

            return contact;
        }
        #endregion
    }
}
