﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading;
using System.Data.SqlClient;
using System.Data;

using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessLogic;
using WLP.BusinessLayer.BusinessObjects;

using Symmetry.TaxEngine;
using System.Diagnostics;

namespace ThreadEngine
{
    class Program
    {
        #region properties

        private static ConnectionStringSettings _connectionString = ConfigurationManager.ConnectionStrings["WorkLinks"];
        private static int _threadCount = Convert.ToInt16(ConfigurationManager.AppSettings["ThreadCount"]);
        private static String _databaseName = ConfigurationManager.AppSettings["DatabaseName"];
        private static Object lockObject = new Object();
        private static STEObjectPool pool;

        //will be filled when we get data from the db for the calc. (private static int _totalRowCount = 0;) //testing without db input, set to 5k
        private static int _totalRowCount = 0;
        private static int _rowIndex = -1;
        private static int _printRowIndex = -1;

        //keep track of open and closed threads
        private static int _threadOpenedCount = 0;
        private static int _threadClosedCount = 0;
        private static bool _controlThreadsActive = true;

        private static List<EngineTestInput> _controlCollection = new List<EngineTestInput>();
        private static List<PayrollEngineOutput> PayrollOutput = new List<PayrollEngineOutput>();

        //this runs behind firewall so dont need to go thru web services, go directly to mgmt layer
        private static ReportManagement _reportMgmt = new ReportManagement();

        //provinces (Federal Tax - "80")
        private static Dictionary<String, String> provinces = new Dictionary<String, String>()
        {
            { "AB", "81" },
            { "BC", "82" },
            { "MB", "83" },
            { "NB", "84" },
            { "NL", "85" },
            { "NT", "86" },
            { "NS", "87" },
            { "NU", "88" },
            { "ON", "89" },
            { "PE", "90" },
            { "QC", "91" },
            { "SK", "92" },
            { "YT", "93" },
        };
        #endregion

        static void Main(string[] args)
        {
            //testing STE pool
            pool = new STEObjectPool("C:/SVN/Worklinks/vs/Service/Windows/ThreadEngine/", _threadCount, 300, 600, _threadCount);

            //setup threads CANADA
            LoadDataSyncThread();

            //setup threads US
            //LoadDataSyncThreadUS();

            //stacy comment LoadDataSyncThread and use method below to do one record and paste in values manually.
            //StacyTestOneRec();
        }

        private static void StacyTestOneRec()
        {
            //stacy test
            PayrollEngineOutput payEngineResults = new PayrollEngineOutput();
            STEPayrollCalculator STE = pool.checkOut();

            String version = STE.getSTEVersion();
            DateTime payDate = DateTime.Now;
            Money zero = Money.zero();

            //set parms and clear
            STE.setPayrollRunParameters(payDate, 26, 13); //stacy test hardcoded payroll period number and current period
            STE.clearPayrollCalculations();

            //set calc method
            STE.setCalculationMethod("80", CalcMethod.ANNUALIZED, CalcMethod.NONE);
            STE.setCalculationMethod("89", CalcMethod.ANNUALIZED, CalcMethod.NONE);

            Money temp = new Money(4818.79000 + 681.41000 - 300.00000);

            STE.setWages("80", WageType.REGULAR, new Hours(0d), new Money(4818.79000 + 681.41000 - 300.00000), zero, zero, zero); //no ytd
            STE.setWages("80", WageType.CPP_WAGES, new Hours(0d), new Money(0.00), zero, zero, zero);//no ytd

            //stacy test
            STE.canadaSetCPPParameters(false, new Money(53600.00));
            STE.canadaSetEIParameters(false, new Money(49500.00));
            //end stacy test


            STE.setWages("80", WageType.EI_WAGES, new Hours(0d), new Money(0.00), zero, zero, zero);//no ytd

            STE.setWages("80", WageType.CAN_BONUS, new Hours(0d), new Money(0.00), zero, zero, zero);

            STE.setWages("89", WageType.REGULAR, new Hours(0d), new Money(4818.79000 + 681.41000 - 300.00000), zero, zero, zero);//no ytd

            //stacy test adding new provincial stuff
            STE.setWages("89", WageType.CAN_BONUS, new Hours(0d), new Money(0.00), zero, zero, zero);
            STE.setWages("89", WageType.CAN_LUMPSUM, new Hours(0d), new Money(0.00), zero, zero, zero);
            //end stacy test

            //set reductions - not needed for the input we have right now
            STE.canadaSetFederalReductions(zero, zero, zero, zero, zero);
            STE.canadaSetProvincialReductions("89", zero, zero);
            //set fed and prov parms
            STE.canadaSetFederalParameters(false, new Money(22276.00), zero, zero, zero);
            STE.canadaSetProvincialParameters(false, "89", new Money(17881.00), zero, zero, zero, zero, zero);

            //calc taxes and print to console
            STE.canadaCalculateTaxes();

            payEngineResults.EmployeeId = 217;
            payEngineResults.EmployeeCPP = STE.canadaGetCPPCalc().toDecimal();
            payEngineResults.EmployeeQPP = STE.canadaGetQPPCalc().toDecimal();
            payEngineResults.EmployeeEI = STE.canadaGetEICalc().toDecimal();
            payEngineResults.EmployerEI = STE.canadaGetEIEmployerCalc().toDecimal();
            payEngineResults.FederalTax = STE.canadaGetFederalCalc().toDecimal();
            payEngineResults.ProvincialTax = STE.canadaGetProvincialTax("89").toDecimal();
            payEngineResults.CombinedTaxes = STE.canadaGetFederalCalc().add(STE.canadaGetProvincialTax("89")).toDecimal();
            payEngineResults.BonusTaxFederal = STE.canadaGetCalculatedBonusTax("80").toDecimal();
            payEngineResults.BonusTaxProvincial = STE.canadaGetCalculatedBonusTax("89").toDecimal();
            payEngineResults.BonusEI = STE.canadaGetBonusEICalc().toDecimal();
            payEngineResults.BonusCPP = STE.canadaGetBonusCPPCalc().toDecimal();

            Decimal test = STE.canadaGetCalculatedLumpSumTax("80").toDecimal();
            Decimal test1 = STE.canadaGetCalculatedLumpSumTax("89").toDecimal();

            //end stacy test
        }

        private static void LoadDataSyncThread()
        {
            Console.WriteLine("Loading Main thread");
            Thread thread = new Thread(DataSync);
            thread.Name = String.Format("Main");
            thread.Start();
        }

        private static void LoadDataSyncThreadUS()
        {
            Console.WriteLine("Loading Main thread US");
            Thread thread = new Thread(DataSyncUS);
            thread.Name = String.Format("Main");
            thread.Start();
        }

        private static void DataSync()
        {
            try
            {
                //load data
                _controlCollection = GetData();

                //startup control threads
                _controlThreadsActive = true;

                LoadControlThreads();
            }
            catch (Exception exc)
            {
                Console.WriteLine("Error trying to read table data from memory", exc);
                //shutdown control threads
                ShutdownControlThreads();
            }
        }

        private static void DataSyncUS()
        {
            try
            {
                //load data
                _controlCollection = GetDataUS();

                //startup control threads
                _controlThreadsActive = true;

                LoadControlThreadsUS();
            }
            catch (Exception exc)
            {
                Console.WriteLine("Error trying to read table data from memory US", exc);
                //shutdown control threads
                ShutdownControlThreads();
            }
        }

        private static void LoadControlThreads()
        {
            Console.WriteLine("Creating threads, number specified in config is: " + _threadCount);

            for (int i = 0; i < _threadCount; i++)
            {
                Thread thread = new Thread(new Program().ProcessData);
                thread.Name = String.Format("Thread{0}", i + 1);
                thread.Start();
            }
        }

        private static void LoadControlThreadsUS()
        {
            Console.WriteLine("Creating threads, number specified in config is: " + _threadCount);

            for (int i = 0; i < _threadCount; i++)
            {
                Thread thread = new Thread(new Program().ProcessDataUS);
                thread.Name = String.Format("Thread{0}", i + 1);
                thread.Start();
            }
        }

        private void ProcessData()
        {
            lock (lockObject) { _threadOpenedCount++; }

            while (_controlThreadsActive && _rowIndex < _totalRowCount)
            {
                try
                {
                    lock (lockObject) { _rowIndex++; }
                    if (_rowIndex < _totalRowCount)
                        ProcessCanadianPayroll(_controlCollection[_rowIndex]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Problem executing ProcessData");
                    throw ex;
                }
            }

            lock (lockObject) { _threadClosedCount++; }

            lock (lockObject)
            {
                if (_threadOpenedCount == _threadClosedCount && _threadOpenedCount == _threadCount)
                    TriggerThreadsToStoreResultsInDatabase();
            }
        }

        private void ProcessDataUS()
        {
            lock (lockObject) { _threadOpenedCount++; }

            while (_controlThreadsActive && _rowIndex < _totalRowCount)
            {
                try
                {
                    lock (lockObject) { _rowIndex++; }
                    if (_rowIndex < _totalRowCount)
                        TestUS(_controlCollection[_rowIndex]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Problem executing ProcessDataUS");
                    throw ex;
                }
            }

            lock (lockObject) { _threadClosedCount++; }

            //lock (lockObject)
            //{
            //    if (_threadOpenedCount == _threadClosedCount && _threadOpenedCount == _threadCount)
            //        TriggerThreadsToStoreResultsInDatabase();
            //}
        }

        private static void TriggerThreadsToStoreResultsInDatabase()
        {
            Console.WriteLine("Number of items in the LIST are: " + PayrollOutput.Count);

            for (int i = 0; i < _threadCount; i++)
            {
                Thread thread = new Thread(new Program().StoreResultsInDatabase);
                thread.Name = String.Format("Thread{0}", i + 10);
                thread.Start();
            }
        }

        private void StoreResultsInDatabase()
        {
            while (_printRowIndex < _totalRowCount)
            {
                try
                {
                    //write to db, ensure no dupes are written
                    PayrollEngineOutput temp = null;
                    lock (lockObject)
                    {
                        _printRowIndex++;
                        if (_printRowIndex < _totalRowCount)
                        {
                            temp = PayrollOutput[_printRowIndex];
                            temp.BonusCPP = _printRowIndex + 1; //stacy test, this is my own test item, like a sequence number, so i can detect dupes and sort on db...
                        }
                    }
                    if (temp != null)
                    {
                        _reportMgmt.InsertPayrollEngineOutput(_databaseName, temp);
                        if ((temp.BonusCPP + 1) % 100 == 0)
                            Console.WriteLine("Stored items in db#: " + (temp.BonusCPP + 1));
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Problem executing StoreResultsInDatabase");
                    throw ex;
                }
            }
        }

        private static void ProcessCanadianPayroll(EngineTestInput inputs)
        {
            try
            {
                PayrollEngineOutput payEngineResults = new PayrollEngineOutput();
                Stopwatch processTimer = new Stopwatch();
                processTimer.Start();

                //using pool instead of instantiating
                STEPayrollCalculator STE = pool.checkOut();

                String version = STE.getSTEVersion();
                DateTime payDate = DateTime.Now;
                Money zero = Money.zero();

                //set parms and clear
                STE.setPayrollRunParameters(payDate, 26, 13); //stacy test hardcoded payroll period number and current period
                STE.clearPayrollCalculations();

                //set calc method
                STE.setCalculationMethod("80", CalcMethod.ANNUALIZED, CalcMethod.NONE);
                STE.setCalculationMethod(inputs.ProvinceNumber, CalcMethod.ANNUALIZED, CalcMethod.NONE);

                //set fed wages and cpp/qpp
                STE.setWages("80", WageType.REGULAR, new Hours(0d), new Money(inputs.Regular + inputs.TaxableBenefits - inputs.BeforeTaxDeductions), zero, zero, zero); //no ytd

                //set prov wages
                if (inputs.ProvinceNumber != "91")
                    STE.setWages(inputs.ProvinceNumber, WageType.REGULAR, new Hours(0d), new Money(inputs.Regular + inputs.TaxableBenefits - inputs.BeforeTaxDeductions), zero, zero, zero);//no ytd
                else
                    STE.setWages(inputs.ProvinceNumber, WageType.REGULAR, new Hours(0d), new Money(inputs.RegularQc + inputs.TaxableBenefits - inputs.BeforeTaxDeductions), zero, zero, zero);//no ytd

                if (inputs.ProvinceNumber != "91")
                {
                    //cpp
                    STE.setWages("80", WageType.CPP_WAGES, new Hours(0d), new Money(inputs.CppQppWagesCalculated), zero, zero, zero);//no ytd

                    //STACY STACY - STE has issues with passing in YTD value for this method...
                    STE.canadaSetCPPParameters(false, zero);  //workaround for now
                    //STE.canadaSetCPPParameters(false, new Money(inputs.YtdCppQppDeducted));
                }
                else
                {
                    //set qpp
                    STE.setWages("80", WageType.QPP_WAGES, new Hours(0d), new Money(inputs.CppQppWagesCalculated), zero, zero, zero);//no ytd
                    //STACY STACY - STE has issues with passing in YTD value for this method...
                    STE.canadaSetQPPParameters(false, new Money(zero));  //workaround for now

                    //set QPIP/PPIP
                    STE.setWages(inputs.ProvinceNumber, WageType.QPIP_WAGES, new Hours(0d), new Money(inputs.PpipWagesCalculated), zero, zero, new Money(inputs.YtdPpipDeducted));
                    STE.canadaQuebecSetQPIP(false, new Money(inputs.YtdPpipDeducted), new Money(inputs.YtdPpipDeducted)); //stacy, this uses YTD employEE and YTD employER values...
                }

                //set EI
                STE.setWages("80", WageType.EI_WAGES, new Hours(0d), new Money(inputs.EiWagesCalculated), zero, zero, zero);//no ytd

                //STACY STACY - STE has issues with passing in YTD value for this method...
                STE.canadaSetEIParameters(false, zero);                    //STE.canadaSetEIParameters(false, new Money(inputs.YtdEiDeducted));

                //set fed and prov BONUS
                STE.setWages("80", WageType.CAN_BONUS, new Hours(0d), new Money(inputs.CanBonus), zero, zero, zero);
                STE.setWages(inputs.ProvinceNumber, WageType.CAN_BONUS, new Hours(0d), new Money(inputs.CanBonus), zero, zero, zero);

                //set LUMP SUM
                STE.setWages(inputs.ProvinceNumber, WageType.CAN_LUMPSUM, new Hours(0d), new Money(inputs.CanLumpsum), zero, zero, zero);

                //set reductions
                STE.canadaSetFederalReductions(zero, zero, zero, zero, zero);
                STE.canadaSetProvincialReductions(inputs.ProvinceNumber, zero, zero);

                //set fed and prov claim amounts
                STE.canadaSetFederalParameters(false, new Money(inputs.TotalClaimAmountFederal), zero, zero, zero);
                STE.canadaSetProvincialParameters(false, inputs.ProvinceNumber, new Money(Convert.ToDecimal(inputs.TotalClaimAmountProvincial)), zero, zero, zero, zero, zero);

                //calc taxes
                STE.canadaCalculateTaxes();

                //store values in business object
                payEngineResults.EmployeeId = inputs.EmployeeId;
                payEngineResults.EmployeeCPP = STE.canadaGetCPPCalc().toDecimal();
                payEngineResults.EmployeeQPP = STE.canadaGetQPPCalc().toDecimal();

                if (inputs.ProvinceNumber == "91")
                    payEngineResults.EmployerCppQpp = STE.canadaGetQPPEmployerCalc().toDecimal();
                else
                    payEngineResults.EmployerCppQpp = payEngineResults.EmployeeCPP;

                payEngineResults.EmployeeEI = STE.canadaGetEICalc().toDecimal();
                payEngineResults.EmployerEI = STE.canadaGetEIEmployerCalc().toDecimal();
                payEngineResults.EmployeeQpip = STE.canadaQuebecGetQPIP().toDecimal();
                payEngineResults.EmployerQpip = STE.canadaQuebecGetEmployerQPIP().toDecimal();
                payEngineResults.FederalTax = STE.canadaGetFederalCalc().toDecimal();
                payEngineResults.QuebecFederalTax = STE.canadaQuebecGetCalculateFederal().toDecimal();
                payEngineResults.ProvincialTax = STE.canadaGetProvincialTax(inputs.ProvinceNumber).toDecimal();
                payEngineResults.LumpSumTax = STE.canadaGetCalculatedLumpSumTax(inputs.ProvinceNumber).toDecimal();
                payEngineResults.BonusTaxFederal = STE.canadaGetCalculatedBonusTax("80").toDecimal();
                payEngineResults.BonusTaxProvincial = STE.canadaGetCalculatedBonusTax(inputs.ProvinceNumber).toDecimal();
                payEngineResults.BonusEI = STE.canadaGetBonusEICalc().toDecimal();
                payEngineResults.BonusCPP = STE.canadaGetBonusCPPCalc().toDecimal();

                if (inputs.ProvinceNumber == "91")
                    payEngineResults.CombinedTaxes += payEngineResults.QuebecFederalTax;
                else
                    payEngineResults.CombinedTaxes = payEngineResults.FederalTax;

                payEngineResults.CombinedTaxes += payEngineResults.ProvincialTax
                                                        + payEngineResults.LumpSumTax
                                                        + payEngineResults.BonusTaxFederal
                                                        + payEngineResults.BonusTaxProvincial
                                                        + inputs.FederalAdditionalTax;

                if (inputs.ProvinceNumber == "91")
                    payEngineResults.CombinedTaxes += inputs.ProvincialAdditionalTax; //stacy test - make sure this is correct

                processTimer.Stop();
                payEngineResults.ElapsedTimeInMilliSeconds = Convert.ToDecimal(processTimer.Elapsed.TotalMilliseconds);
                payEngineResults.CreateUser = payEngineResults.UpdateUser = "steThreadingEngine";

                //store in list
                lock (PayrollOutput)
                {
                    PayrollOutput.Add(payEngineResults);
                }

                pool.checkIn(STE); //check back into pool
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void TestUS(EngineTestInput inputs)
        {
            String FEDERAL_LOCATION_CODE = "00-000-0000";

            try
            {
                //PayrollEngineOutput payEngineResults = new PayrollEngineOutput();
                Stopwatch processTimer = new Stopwatch();
                processTimer.Start();

                //using pool instead of instantiating
                STEPayrollCalculator STE = pool.checkOut();
                DateTime today = DateTime.Today;
                Money zero = Money.zero();

                //set parms and clear
                STE.setPayrollRunParameters(today, 52, 13);
                STE.clearPayrollCalculations();

                //set calc method
                STE.setCalculationMethod(FEDERAL_LOCATION_CODE, CalcMethod.ANNUALIZED, CalcMethod.NONE);

                STE.setWages(FEDERAL_LOCATION_CODE, WageType.REGULAR, new Hours(40.00), new Money(inputs.Regular), zero, zero, zero);
                STE.setFederalParameters(false, FederalFilingStatus.MARRIED, 2, true, zero, zero, zero);
                STE.setFICAParameters(false, false, zero, zero, true);
                STE.setMedicareParameters(false, zero, zero);
                STE.setEICParameters(EarnedIncomeCredit.SINGLE, zero, false);

                // The code for Indianapolis, Indiana (Marion County) is "18-097-452890"
                // The state GNIS code is 18
                // The county GNIS code is 097
                // The City GNIS code is 452890

                STE.setStateParameters("18-097-452890", true, false, StateRounding.DEFAULTROUNDING, zero, zero, zero, false);
                STE.setCalculationMethod("18-097-452890", CalcMethod.ANNUALIZED, CalcMethod.NONE);
                STE.setWages("18-097-452890", WageType.REGULAR, new Hours(40.00), new Money(inputs.Regular), zero, zero, zero);
                STE.setCountyParameters("18-097-452890", false, true);

                // The miscellaneous parameters vary by state
                // Indiana requires the following two parameters to withhold state tax
                STE.setStateMiscellaneousParameters("18-097-452890", "PERSONALEXEMPTIONS", "1");
                STE.setStateMiscellaneousParameters("18-097-452890", "DEPENDENTEXEMPTIONS", "2");

                //calc taxes
                STE.calculateTaxes();

                //write to console for now...
                Money.CURRENCY_SYMBOL_PREFIX = "";

                Console.WriteLine("FICA Withholding: " + STE.getFICACalc());
                Console.WriteLine("Medicare Withholding: " + STE.getMedicareCalc());
                Console.WriteLine("Federal Withholding: " + STE.getFederalCalc());
                Console.WriteLine("Earned Inc. Credit: " + STE.getEICCalc());
                Console.WriteLine("State Withholding: " + STE.getStateCalc("18-097-452890"));
                Console.WriteLine("County Withholding: " + STE.getCountyCalc("18-097-452890"));
                processTimer.Stop();
                Console.WriteLine("time is: " + Convert.ToDecimal(processTimer.Elapsed.TotalMilliseconds));

                //payEngineResults.ElapsedTimeInMilliSeconds = Convert.ToDecimal(processTimer.Elapsed.TotalMilliseconds);
                //payEngineResults.CreateUser = payEngineResults.UpdateUser = "steThreadingEngine";

                pool.checkIn(STE);

                //dispose doesnt work
                //STE.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static List<EngineTestInput> GetData()
        {
            List<EngineTestInput> tableData = new List<EngineTestInput>();

            //create sql details
            SqlConnection connection = new SqlConnection(_connectionString.ConnectionString);
            SqlDataReader reader;
            SqlCommand command = new SqlCommand();

            //create sql
            command.CommandText = "SELECT * FROM engine_test_input";
            command.CommandType = CommandType.Text;
            command.Connection = connection;

            try
            {
                connection.Open();
                reader = command.ExecuteReader();

                int count = reader.FieldCount;
                while (reader.Read())
                {
                    _totalRowCount++;

                    EngineTestInput temp = new EngineTestInput(); //STACY, 3 NEW COLUMNS HAVE BEEN ADDED, SO BIZ OBJECT WILL NEED NEW FIELDS

                    temp.EngineTestInputId = Convert.ToInt64(reader[0]);
                    temp.EmployeeId = Convert.ToInt64(reader[1]);
                    temp.TaxableCodeProvinceStateCode = Convert.ToString(reader[2]);
                    temp.ProvinceNumber = Convert.ToString(reader[3]);
                    temp.Regular = Convert.ToDecimal(reader[4]);
                    temp.CppQppWages = Convert.ToDecimal(reader[5]);
                    temp.CppQppDeducted = Convert.ToDecimal(reader[6]);
                    temp.EiWages = Convert.ToDecimal(reader[7]);
                    temp.EiDeducted = Convert.ToDecimal(reader[8]);
                    temp.PpipWages = Convert.ToDecimal(reader[9]);
                    temp.PpipDeducted = Convert.ToDecimal(reader[10]);
                    temp.RegularQc = Convert.ToDecimal(reader[11]);
                    temp.TaxableBenefits = Convert.ToDecimal(reader[12]);
                    temp.BeforeTaxDeductions = Convert.ToDecimal(reader[13]);
                    temp.CanBonus = Convert.ToDecimal(reader[14]);
                    temp.RetroactiveIncome = Convert.ToDecimal(reader[15]);
                    temp.CanLumpsum = Convert.ToDecimal(reader[16]);
                    temp.CanBonusQc = Convert.ToDecimal(reader[17]);
                    temp.CanLumpsumQc = Convert.ToDecimal(reader[18]);
                    temp.TaxDeducted = Convert.ToDecimal(reader[19]);
                    temp.TaxQcDeducted = Convert.ToDecimal(reader[20]);
                    temp.TotalClaimAmountFederal = Convert.ToDecimal(reader[21]);
                    temp.DesignatedAreaDeduction = Convert.ToDecimal(reader[22]);
                    temp.AuthorizedAnnualDeduction = Convert.ToDecimal(reader[23]);
                    temp.FederalAdditionalTax = Convert.ToDecimal(reader[24]);
                    temp.TotalClaimAmountProvincial = Convert.ToInt16(reader[25]);
                    temp.ProvincialAdditionalTax = Convert.ToDecimal(reader[26]);
                    temp.ProvincialAuthorizedAnnualDeduction = Convert.ToInt16(reader[27]);
                    temp.TaxCreditProvincial = Convert.ToInt16(reader[28]);
                    temp.ProvincialDesignatedAreaDeduction = Convert.ToInt16(reader[29]);
                    temp.Lcf = Convert.ToDecimal(reader[30]);
                    temp.Lcp = Convert.ToDecimal(reader[31]);
                    temp.NorthwestTerritoriesTaxCalculationBase = Convert.ToDecimal(reader[32]);
                    temp.NorthwestTerritoriesTaxAssessed = Convert.ToDecimal(reader[33]);
                    temp.NunavutTaxCalculationBase = Convert.ToDecimal(reader[34]);
                    temp.NunavutTaxAssessed = Convert.ToDecimal(reader[35]);
                    temp.EmployerEmploymentInsuranceAmount = Convert.ToDecimal(reader[36]);
                    temp.EmployerCanadaPensionPlanAmount = Convert.ToDecimal(reader[37]);
                    temp.EmployerProvincialParentalAmount = Convert.ToDecimal(reader[38]);
                    temp.YtdCppQppDeducted = Convert.ToDecimal(reader[39]);
                    temp.YtdEiDeducted = Convert.ToDecimal(reader[40]);
                    temp.YtdPpipDeducted = Convert.ToDecimal(reader[41]);
                    temp.CppQppWagesCalculated = Convert.ToDecimal(reader[42]); ;
                    temp.EiWagesCalculated = Convert.ToDecimal(reader[43]); ;
                    temp.PpipWagesCalculated = Convert.ToDecimal(reader[44]);

                    tableData.Add(temp);
                }

                connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("EXCEPTION");
                Console.WriteLine(ex.Message);
            }

            return tableData;
        }

        private static List<EngineTestInput> GetDataUS()
        {
            List<EngineTestInput> tableData = new List<EngineTestInput>();

            //create sql details
            //SqlConnection connection = new SqlConnection(_connectionString.ConnectionString);
            //SqlDataReader reader;
            //SqlCommand command = new SqlCommand();

            //create sql
            //command.CommandText = "SELECT * FROM engine_test_input";
            //command.CommandType = CommandType.Text;
            //command.Connection = connection;

            //try
            //{
            //connection.Open();
            //reader = command.ExecuteReader();

            //int count = reader.FieldCount;
            //while (reader.Read())
            for (int i = 0; i < 1000; i++)                    //set for US testing 100, 500 etc
            {
                _totalRowCount++;

                EngineTestInput temp = new EngineTestInput();

                temp.EngineTestInputId = Convert.ToInt64(-i);
                temp.Regular = Convert.ToDecimal(5000.00);

                tableData.Add(temp);
            }
            //connection.Close();
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine("EXCEPTION");
            //    Console.WriteLine(ex.Message);
            //}
            return tableData;
        }

        private static void ShutdownControlThreads()
        {
            _controlThreadsActive = false;

            while (_threadOpenedCount < _threadCount && _threadClosedCount < _threadCount)
            {
                Console.WriteLine(String.Format("Waiting for {0} Control threads to stop", _threadCount - _threadClosedCount));
                Thread.Sleep(5000);
            }
        }
    }
}
