﻿
using System;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;

namespace WorkLinks.DataLayer.DataAccess
{
    public class YearEndAccess
    {
        #region fields
        SqlServer.SqlServerYearEndAdjustmentsSearchReport _sqlServerYearEndAdjustmentsSearchReport = null;
        SqlServer.SqlServerYearEndAdjustment _sqlServerYearEndAdjustment = null;
        SqlServer.SqlServerYearEnd _sqlServerYearEnd = null;
        SqlServer.SqlServerYearEndAdjustmentTable _sqlServerYearEndAdjustmentTable = null;
        SqlServer.SqlServerYearEndT4XmlFileToWorklinks _sqlServerYearEndT4XmlFileToWorklinks = null;
        SqlServer.SqlServerYearEndT4aXmlFileToWorklinks _sqlServerYearEndT4aXmlFileToWorklinks = null;
        SqlServer.SqlServerYearEndR1XmlFileToWorklinks _sqlServerYearEndR1XmlFileToWorklinks = null;
        SqlServer.SqlServerYearEndR2XmlFileToWorklinks _sqlServerYearEndR2XmlFileToWorklinks = null;
        SqlServer.SqlServerYearEndT4rspXmlFileToWorklinks _sqlServerYearEndT4rspXmlFileToWorklinks = null;
        SqlServer.SqlServerYearEndNR4rspXmlFileToWorklinks _sqlServerYearEndNR4rspXmlFileToWorklinks = null;
        SqlServer.SqlServerYearEndT4arcaXmlFileToWorklinks _sqlServerYearEndT4arcaXmlFileToWorklinks = null;
        SqlServer.SqlServerCanadaRevenueAgencyT4Export _sqlServerCanadaRevenueAgencyT4Export = null;
        SqlServer.SqlServerCanadaRevenueAgencyT619 _sqlServerCanadaRevenueAgencyT619 = null;
        SqlServer.SqlServerRevenueQuebecR1Export _sqlServerRevenueQuebecR1Export = null;
        SqlServer.SqlServerRevenueQuebecTransmitterRecord _sqlServerRevenueQuebecTransmitterRecord = null;
        SqlServer.SqlServerYearEndAdjustmentAudit _sqlServerYearEndAdjustmentAudit = null;
        SqlServer.SqlServerYearEndNr4PayerAgent _sqlServerYearEndNr4PayerAgent = null;
        private SqlServer.SqlServerYearEndAdjustmentCode _sqlServerYearEndAdjustmentCode = null;
        #endregion

        #region properties
        private SqlServer.SqlServerYearEndAdjustmentsSearchReport SqlServerYearEndAdjustmentsSearchReport
        {
            get
            {
                if (_sqlServerYearEndAdjustmentsSearchReport == null)
                    _sqlServerYearEndAdjustmentsSearchReport = new SqlServer.SqlServerYearEndAdjustmentsSearchReport();

                return _sqlServerYearEndAdjustmentsSearchReport;
            }
        }
        private SqlServer.SqlServerYearEndAdjustment SqlServerYearEndAdjustment
        {
            get
            {
                if (_sqlServerYearEndAdjustment == null)
                    _sqlServerYearEndAdjustment = new SqlServer.SqlServerYearEndAdjustment();

                return _sqlServerYearEndAdjustment;
            }
        }
        private SqlServer.SqlServerYearEnd SqlServerYearEnd
        {
            get
            {
                if (_sqlServerYearEnd == null)
                    _sqlServerYearEnd = new SqlServer.SqlServerYearEnd();

                return _sqlServerYearEnd;
            }
        }
        private SqlServer.SqlServerYearEndAdjustmentTable SqlServerYearEndAdjustmentTable
        {
            get
            {
                if (_sqlServerYearEndAdjustmentTable == null)
                    _sqlServerYearEndAdjustmentTable = new SqlServer.SqlServerYearEndAdjustmentTable();

                return _sqlServerYearEndAdjustmentTable;
            }
        }
        private SqlServer.SqlServerYearEndT4XmlFileToWorklinks SqlServerYearEndT4XmlFileToWorklinks
        {
            get
            {
                if (_sqlServerYearEndT4XmlFileToWorklinks == null)
                    _sqlServerYearEndT4XmlFileToWorklinks = new SqlServer.SqlServerYearEndT4XmlFileToWorklinks();

                return _sqlServerYearEndT4XmlFileToWorklinks;
            }
        }
        private SqlServer.SqlServerYearEndT4aXmlFileToWorklinks SqlServerYearEndT4aXmlFileToWorklinks
        {
            get
            {
                if (_sqlServerYearEndT4aXmlFileToWorklinks == null)
                    _sqlServerYearEndT4aXmlFileToWorklinks = new SqlServer.SqlServerYearEndT4aXmlFileToWorklinks();

                return _sqlServerYearEndT4aXmlFileToWorklinks;
            }
        }
        private SqlServer.SqlServerYearEndR1XmlFileToWorklinks SqlServerYearEndR1XmlFileToWorklinks
        {
            get
            {
                if (_sqlServerYearEndR1XmlFileToWorklinks == null)
                    _sqlServerYearEndR1XmlFileToWorklinks = new SqlServer.SqlServerYearEndR1XmlFileToWorklinks();

                return _sqlServerYearEndR1XmlFileToWorklinks;
            }
        }
        private SqlServer.SqlServerYearEndR2XmlFileToWorklinks SqlServerYearEndR2XmlFileToWorklinks
        {
            get
            {
                if (_sqlServerYearEndR2XmlFileToWorklinks == null)
                    _sqlServerYearEndR2XmlFileToWorklinks = new SqlServer.SqlServerYearEndR2XmlFileToWorklinks();

                return _sqlServerYearEndR2XmlFileToWorklinks;
            }
        }
        private SqlServer.SqlServerYearEndT4rspXmlFileToWorklinks SqlServerYearEndT4rspXmlFileToWorklinks
        {
            get
            {
                if (_sqlServerYearEndT4rspXmlFileToWorklinks == null)
                    _sqlServerYearEndT4rspXmlFileToWorklinks = new SqlServer.SqlServerYearEndT4rspXmlFileToWorklinks();

                return _sqlServerYearEndT4rspXmlFileToWorklinks;
            }
        }
        private SqlServer.SqlServerYearEndNR4rspXmlFileToWorklinks SqlServerYearEndNR4rspXmlFileToWorklinks
        {
            get
            {
                if (_sqlServerYearEndNR4rspXmlFileToWorklinks == null)
                    _sqlServerYearEndNR4rspXmlFileToWorklinks = new SqlServer.SqlServerYearEndNR4rspXmlFileToWorklinks();

                return _sqlServerYearEndNR4rspXmlFileToWorklinks;
            }
        }
        private SqlServer.SqlServerYearEndNr4PayerAgent SqlServerYearEndNr4PayerAgent
        {
            get
            {
                if (_sqlServerYearEndNr4PayerAgent == null)
                    _sqlServerYearEndNr4PayerAgent = new SqlServer.SqlServerYearEndNr4PayerAgent();

                return _sqlServerYearEndNr4PayerAgent;
            }
        }
        private SqlServer.SqlServerYearEndT4arcaXmlFileToWorklinks SqlServerYearEndT4arcaXmlFileToWorklinks
        {
            get
            {
                if (_sqlServerYearEndT4arcaXmlFileToWorklinks == null)
                    _sqlServerYearEndT4arcaXmlFileToWorklinks = new SqlServer.SqlServerYearEndT4arcaXmlFileToWorklinks();

                return _sqlServerYearEndT4arcaXmlFileToWorklinks;
            }
        }
        private SqlServer.SqlServerCanadaRevenueAgencyT4Export SqlServerCanadaRevenueAgencyT4Export
        {
            get
            {
                if (_sqlServerCanadaRevenueAgencyT4Export == null)
                    _sqlServerCanadaRevenueAgencyT4Export = new SqlServer.SqlServerCanadaRevenueAgencyT4Export();

                return _sqlServerCanadaRevenueAgencyT4Export;
            }
        }
        private SqlServer.SqlServerCanadaRevenueAgencyT619 SqlServerCanadaRevenueAgencyT619
        {
            get
            {
                if (_sqlServerCanadaRevenueAgencyT619 == null)
                    _sqlServerCanadaRevenueAgencyT619 = new SqlServer.SqlServerCanadaRevenueAgencyT619();

                return _sqlServerCanadaRevenueAgencyT619;
            }
        }
        private SqlServer.SqlServerRevenueQuebecR1Export SqlServerRevenueQuebecR1Export
        {
            get
            {
                if (_sqlServerRevenueQuebecR1Export == null)
                    _sqlServerRevenueQuebecR1Export = new SqlServer.SqlServerRevenueQuebecR1Export();

                return _sqlServerRevenueQuebecR1Export;
            }
        }
        private SqlServer.SqlServerRevenueQuebecTransmitterRecord SqlServerRevenueQuebecTransmitterRecord
        {
            get
            {
                if (_sqlServerRevenueQuebecTransmitterRecord == null)
                    _sqlServerRevenueQuebecTransmitterRecord = new SqlServer.SqlServerRevenueQuebecTransmitterRecord();

                return _sqlServerRevenueQuebecTransmitterRecord;
            }
        }
        private SqlServer.SqlServerYearEndAdjustmentAudit SqlServerYearEndAdjustmentAudit
        {
            get
            {
                if (_sqlServerYearEndAdjustmentAudit == null)
                    _sqlServerYearEndAdjustmentAudit = new SqlServer.SqlServerYearEndAdjustmentAudit();

                return _sqlServerYearEndAdjustmentAudit;
            }
        }

        private SqlServer.SqlServerYearEndAdjustmentCode SqlServerYearEndAdjustmentCode => _sqlServerYearEndAdjustmentCode ?? (_sqlServerYearEndAdjustmentCode = new SqlServer.SqlServerYearEndAdjustmentCode());

        #endregion

        #region year end adjustment code

        public void InsertYearEndAdjustmentCodes(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, WorkLinksYearEndAdjustmentPaycodeCollection paycodes)
        {
            SqlServerYearEndAdjustmentCode.InsertBatch(user, paycodes);
        }

        #endregion

        #region year end adjustments search
        public YearEndAdjustmentsSearchReportCollection GetYearEndAdjustmentsSearchReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, YearEndAdjustmentsSearchCriteria criteria)
        {
            return SqlServerYearEndAdjustmentsSearchReport.GetYearEndAdjustmentsSearchReport(user, criteria);
        }
        #endregion

        #region year end adjustment
        public YearEndAdjustmentCollection GetYearEndAdjustment(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeeId, Decimal year, long yearEndAdjustmentTableId, String employerNumber, String provinceStateCode, bool? visibleFlag, long revision, String columnImportIdentifier)
        {
            return SqlServerYearEndAdjustment.GetYearEndAdjustment(user, employeeId, year, yearEndAdjustmentTableId, employerNumber, provinceStateCode, visibleFlag, revision, columnImportIdentifier);
        }
        public void UpdateYearEndAdjustment(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, YearEndAdjustment yearEndObj)
        {
            SqlServerYearEndAdjustment.Update(user, yearEndObj);
        }
        public YearEndAdjustmentTableCollection GetYearEndAdjustmentTables(WLP.BusinessLayer.BusinessObjects.DatabaseUser user)
        {
            return SqlServerYearEndAdjustmentTable.GetYearEndAdjustmentTables(user);
        }
        public void InsertIntoYearEndAdjustmentAudit(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, YearEndAdjustment yearEndAdjustmentObj, bool deletedFlag)
        {
            SqlServerYearEndAdjustmentAudit.Insert(user, yearEndAdjustmentObj, deletedFlag);
        }
        #endregion

        #region year end
        public YearEndCollection GetYearEnd(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Decimal? year)
        {
            return GetYearEnd(user, year, false);
        }
        public YearEndCollection GetYearEnd(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Decimal? year, bool getLatestFlag)
        {
            return SqlServerYearEnd.Select(user, year, getLatestFlag);
        }
        public YearEnd InsertYearEndRecord(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, YearEnd yearEndObj)
        {
            return SqlServerYearEnd.Insert(user, yearEndObj);
        }
        public void UpdateYearEndRecord(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, YearEnd yearEndObj)
        {
            SqlServerYearEnd.Update(user, yearEndObj);
        }
        public void PopulateYearEndTables(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Decimal year)
        {
            SqlServerYearEnd.PopulateYearEndTables(user, year);
        }
        #endregion

        #region T4
        public YearEndT4Collection SelectSingleT4ByKey(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long key)
        {
            return SqlServerYearEndT4XmlFileToWorklinks.SelectSingleT4ByKey(user, key);
        }
        public YearEndT4Collection GetBatchYearEndT4(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, string[] yearEndT4Ids)
        {
            return SqlServerYearEndT4XmlFileToWorklinks.SelectBatch(user, yearEndT4Ids);
        }
        public YearEndT4Collection GetYearEndT4(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, int year, long? businessNumberId, bool getOrginals = false, bool getAmended = false)
        {
            return SqlServerYearEndT4XmlFileToWorklinks.Select(user, year, businessNumberId, getOrginals, getAmended);
        }
        public YearEndT4 InsertYearEndT4(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, YearEndT4 yearEndT4Obj)
        {
            return SqlServerYearEndT4XmlFileToWorklinks.Insert(user, yearEndT4Obj);
        }
        public void UpdateYearEndT4(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long keyId, bool activeFlag)
        {
            SqlServerYearEndT4XmlFileToWorklinks.Update(user, keyId, activeFlag);
        }
        #endregion

        #region T4A
        public YearEndT4aCollection SelectSingleT4aByKey(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long key)
        {
            return SqlServerYearEndT4aXmlFileToWorklinks.SelectSingleT4aByKey(user, key);
        }
        public YearEndT4aCollection GetYearEndT4a(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, int year, long businessNumberId, bool getOrginals = false, bool getAmended = false)
        {
            return SqlServerYearEndT4aXmlFileToWorklinks.Select(user, year, businessNumberId, getOrginals, getAmended);
        }
        public YearEndT4a InsertYearEndT4a(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, YearEndT4a yearEndT4aObj)
        {
            return SqlServerYearEndT4aXmlFileToWorklinks.Insert(user, yearEndT4aObj);
        }
        public void UpdateYearEndT4a(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long keyId, bool activeFlag)
        {
            SqlServerYearEndT4aXmlFileToWorklinks.Update(user, keyId, activeFlag);
        }
        #endregion

        #region R1
        public YearEndR1Collection SelectSingleR1ByKey(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long key)
        {
            return SqlServerYearEndR1XmlFileToWorklinks.SelectSingleR1ByKey(user, key);
        }
        public YearEndR1Collection GetYearEndR1(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, int year, bool getOrginals = false, bool getAmended = false)
        {
            return SqlServerYearEndR1XmlFileToWorklinks.Select(user, year, getOrginals, getAmended);
        }
        public YearEndR1 InsertYearEndR1(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, YearEndR1 yearEndR1Obj)
        {
            return SqlServerYearEndR1XmlFileToWorklinks.Insert(user, yearEndR1Obj);
        }
        public void UpdateYearEndR1(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long keyId, bool activeFlag)
        {
            SqlServerYearEndR1XmlFileToWorklinks.Update(user, keyId, activeFlag);
        }
        #endregion

        #region R2
        public YearEndR2Collection SelectSingleR2ByKey(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long key)
        {
            return SqlServerYearEndR2XmlFileToWorklinks.SelectSingleR2ByKey(user, key);
        }
        public YearEndR2Collection GetYearEndR2(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, int year, bool getOrginals = false, bool getAmended = false)
        {
            return SqlServerYearEndR2XmlFileToWorklinks.Select(user, year, getOrginals, getAmended);
        }
        public YearEndR2 InsertYearEndR2(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, YearEndR2 yearEndR2Obj)
        {
            return SqlServerYearEndR2XmlFileToWorklinks.Insert(user, yearEndR2Obj);
        }
        public void UpdateYearEndR2(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long keyId, bool activeFlag)
        {
            SqlServerYearEndR2XmlFileToWorklinks.Update(user, keyId, activeFlag);
        }
        #endregion

        #region T4RSP
        public YearEndT4rspCollection GetYearEndT4rsp(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, int year, bool getOrginals = false, bool getAmended = false)
        {
            return SqlServerYearEndT4rspXmlFileToWorklinks.Select(user, year, getOrginals, getAmended);
        }
        public YearEndT4rspCollection SelectSingleT4rspByKey(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long key)
        {
            return SqlServerYearEndT4rspXmlFileToWorklinks.SelectSingleT4rspByKey(user, key);
        }
        public YearEndT4rsp InsertYearEndT4rsp(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, YearEndT4rsp yearEndT4rspObj)
        {
            return SqlServerYearEndT4rspXmlFileToWorklinks.Insert(user, yearEndT4rspObj);
        }
        public void UpdateYearEndT4rsp(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long keyId, bool activeFlag)
        {
            SqlServerYearEndT4rspXmlFileToWorklinks.Update(user, keyId, activeFlag);
        }
        public void DeleteYearEndT4rsp(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String year)
        {
            SqlServerYearEndT4rspXmlFileToWorklinks.Delete(user, year);
        }
        #endregion

        #region NR4 Payer Agent
        public YearEndNr4PayerAgentCollection SelectNr4PayerAgentByKey(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long key)
        {
            return SqlServerYearEndNr4PayerAgent.SelectNr4PayerAgentByKey(user, key);
        }
        #endregion

        #region NR4
        public YearEndNR4rspCollecion GetYearEndNR4rsp(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, int year, long businessNumberId, bool getOrginals = false, bool getAmended = false)
        {
            return SqlServerYearEndNR4rspXmlFileToWorklinks.Select(user, year, businessNumberId, getOrginals, getAmended);
        }
        public YearEndNR4rspCollecion SelectSingleNR4rspByKey(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long key)
        {
            return SqlServerYearEndNR4rspXmlFileToWorklinks.SelectSingleNR4rspByKey(user, key);
        }
        public YearEndNR4rsp InsertYearEndNR4rsp(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, YearEndNR4rsp yearEndNR4rspObj)
        {
            return SqlServerYearEndNR4rspXmlFileToWorklinks.Insert(user, yearEndNR4rspObj);
        }
        public void UpdateYearEndNR4rsp(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long keyId, bool activeFlag)
        {
            SqlServerYearEndNR4rspXmlFileToWorklinks.Update(user, keyId, activeFlag);
        }
        public void DeleteYearEndNR4rsp(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String year)
        {
            SqlServerYearEndNR4rspXmlFileToWorklinks.Delete(user, year);
        }
        #endregion

        #region T4ARCA
        public YearEndT4arcaCollection GetYearEndT4arca(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, int year, bool getOrginals = false, bool getAmended = false)
        {
            return SqlServerYearEndT4arcaXmlFileToWorklinks.Select(user, year, getOrginals, getAmended);
        }
        public YearEndT4arcaCollection SelectSingleT4arcaByKey(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long key)
        {
            return SqlServerYearEndT4arcaXmlFileToWorklinks.SelectSingleT4arcaByKey(user, key);
        }
        public YearEndT4arca InsertYearEndT4arca(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, YearEndT4arca yearEndT4arcaObj)
        {
            return SqlServerYearEndT4arcaXmlFileToWorklinks.Insert(user, yearEndT4arcaObj);
        }
        public void UpdateYearEndT4arca(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long keyId, bool activeFlag)
        {
            SqlServerYearEndT4arcaXmlFileToWorklinks.Update(user, keyId, activeFlag);
        }
        public void DeleteYearEndT4arca(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String year)
        {
            SqlServerYearEndT4arcaXmlFileToWorklinks.Delete(user, year);
        }
        #endregion

        #region T4 export
        public CanadaRevenueAgencyT4ExportCollection GetCanadaRevenueAgencyT4Export(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long yearEndId, String exportType)
        {
            return SqlServerCanadaRevenueAgencyT4Export.Select(user, yearEndId, exportType);
        }
        public void UpdateCanadaRevenueAgencyT4Export(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CanadaRevenueAgencyT4Export item)
        {
            SqlServerCanadaRevenueAgencyT4Export.Update(user, item);
        }
        public void InsertCanadaRevenueAgencyT4Export(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CanadaRevenueAgencyT4Export item)
        {
            SqlServerCanadaRevenueAgencyT4Export.Insert(user, item);
        }
        #endregion

        #region T619
        public CanadaRevenueAgencyT619Collection GetCanadaRevenueAgencyT619(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long canadaRevenueAgencyT619Id)
        {
            return SqlServerCanadaRevenueAgencyT619.Select(user, canadaRevenueAgencyT619Id);
        }
        public void UpdateCanadaRevenueAgencyT619(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CanadaRevenueAgencyT619 item)
        {
            SqlServerCanadaRevenueAgencyT619.Update(user, item);
        }
        public void InsertCanadaRevenueAgencyT619(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CanadaRevenueAgencyT619 item)
        {
            SqlServerCanadaRevenueAgencyT619.Insert(user, item);
        }
        #endregion

        #region RevenueQuebecR1Export
        public RevenueQuebecR1ExportCollection GetRevenueQuebecR1Export(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long yearEndId, String exportType)
        {
            return SqlServerRevenueQuebecR1Export.Select(user, yearEndId, exportType);
        }
        public void UpdateRevenueQuebecR1Export(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, RevenueQuebecR1Export item)
        {
            SqlServerRevenueQuebecR1Export.Update(user, item);
        }
        public void InsertRevenueQuebecR1Export(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, RevenueQuebecR1Export item)
        {
            SqlServerRevenueQuebecR1Export.Insert(user, item);
        }
        #endregion

        #region RevenueQuebecTransmitterRecord
        public RevenueQuebecTransmitterRecordCollection GetRevenueQuebecTransmitterRecord(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long revenueQuebecTransmitterRecordId)
        {
            return SqlServerRevenueQuebecTransmitterRecord.Select(user, revenueQuebecTransmitterRecordId);
        }
        public void UpdateRevenueQuebecTransmitterRecord(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, RevenueQuebecTransmitterRecord item)
        {
            SqlServerRevenueQuebecTransmitterRecord.Update(user, item);
        }
        public void InsertRevenueQuebecTransmitterRecord(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, RevenueQuebecTransmitterRecord item)
        {
            SqlServerRevenueQuebecTransmitterRecord.Insert(user, item);
        }
        #endregion
    }
}