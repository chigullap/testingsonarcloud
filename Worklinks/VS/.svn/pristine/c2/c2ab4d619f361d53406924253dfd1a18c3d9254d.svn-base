﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class YearEndT4rsp : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long YearEndT4rspId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public int Revision { get; set; }

        [DataMember]
        public long? PreviousRevisionYearEndT4rspId { get; set; }

        [DataMember]
        public String EmployerNumber { get; set; }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public Decimal Year { get; set; }

        [DataMember]
        public String Box14 { get; set; }

        [DataMember]
        public Decimal? Box16Amount { get; set; }

        [DataMember]
        public Decimal? Box18Amount { get; set; }

        [DataMember]
        public Decimal? Box20Amount { get; set; }

        [DataMember]
        public Decimal? Box22Amount { get; set; }

        [DataMember]
        public bool Box24Flag { get; set; }

        [DataMember]
        public Decimal? Box25Amount { get; set; }

        [DataMember]
        public Decimal? Box26Amount { get; set; }

        [DataMember]
        public Decimal? Box27Amount { get; set; }

        [DataMember]
        public Decimal? Box28Amount { get; set; }

        [DataMember]
        public Decimal? Box30Amount { get; set; }

        [DataMember]
        public Decimal? Box34Amount { get; set; }

        [DataMember]
        public Decimal? Box35Amount { get; set; }

        [DataMember]
        public String Box36 { get; set; }

        [DataMember]
        public String Box60 { get; set; }

        [DataMember]
        public String Box61 { get; set; }

        [DataMember]
        public Decimal? Box40Amount { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }
        #endregion

        #region construct/destruct
        public YearEndT4rsp()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(YearEndT4rsp data)
        {
            base.CopyTo(data);

            data.YearEndT4rspId = YearEndT4rspId;
            data.Revision = Revision;
            data.PreviousRevisionYearEndT4rspId = PreviousRevisionYearEndT4rspId;
            data.EmployerNumber = EmployerNumber;
            data.EmployeeId = EmployeeId;
            data.Year = Year;
            data.Box14 = Box14;
            data.Box16Amount = Box16Amount;
            data.Box18Amount = Box18Amount;
            data.Box20Amount = Box20Amount;
            data.Box22Amount = Box22Amount;
            data.Box24Flag = Box24Flag;
            data.Box25Amount = Box25Amount;
            data.Box26Amount = Box26Amount;
            data.Box27Amount = Box27Amount;
            data.Box28Amount = Box28Amount;
            data.Box30Amount = Box30Amount;
            data.Box34Amount = Box34Amount;
            data.Box35Amount = Box35Amount;
            data.Box36 = Box36;
            data.Box60 = Box60;
            data.Box61 = Box61;
            data.Box40Amount = Box40Amount;
            data.ActiveFlag = ActiveFlag;
        }
        public new void Clear()
        {
            base.Clear();

            YearEndT4rspId = -1;
            Revision = 0;
            PreviousRevisionYearEndT4rspId = null;
            EmployerNumber = null;
            EmployeeId = -1;
            Year = 0;
            Box14 = null;
            Box16Amount = null;
            Box18Amount = null;
            Box20Amount = null;
            Box22Amount = null;
            Box24Flag = false;
            Box25Amount = null;
            Box26Amount = null;
            Box27Amount = null;
            Box28Amount = null;
            Box30Amount = null;
            Box34Amount = null;
            Box35Amount = null;
            Box36 = null;
            Box60 = null;
            Box61 = null;
            Box40Amount = null;
            ActiveFlag = true;
        }
        #endregion
    }
}