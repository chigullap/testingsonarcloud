﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.ExportCheque
{
    public partial class ExportChequeControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public bool IsViewMode { get { return ExportChequeGrid.IsViewMode; } }
        public bool IsUpdate { get { return ExportChequeGrid.IsEditMode; } }
        public bool IsInsert { get { return ExportChequeGrid.IsInsertMode; } }
        protected long PayrollProccessId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["payrollProcessId"]); }
        }
        protected String EmployeeNumber
        {
            get { return Convert.ToString(Page.RouteData.Values["employeeNumber"]); }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.ExportCheque.ViewFlag);

            WireEvents();

            if (!IsPostBack)
                LoadExportChequeInformation();

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.StartsWith("groupOfPayrollMasterPaymentIds="))
            {
                List<String> payrollMasterPaymentIds = new List<String>();
                String payrollMasterPaymentIdsString = args.Substring(31);

                foreach (String payrollMasterPaymentId in payrollMasterPaymentIdsString.ToString().Split(','))
                {
                    payrollMasterPaymentIds.Add(payrollMasterPaymentId);
                }

                try
                {
                    byte[] file = Common.ServiceWrapper.HumanResourcesClient.CreateCitiChequeEftFromPayrollMasterPaymentRecords(payrollMasterPaymentIds.ToArray(), Common.ApplicationParameter.CitiChequeAccountNumber, Common.ApplicationParameter.CompanyShortName, EmployeeNumber, PayrollProccessId, Common.ApplicationParameter.EftOrginatorId, ((PayrollMasterPayment)Data[0]).EmployeeId);
                    String chequeFileName = Common.ServiceWrapper.HumanResourcesClient.GetCitiNamingFormat(Common.ApplicationParameter.CitiChequeFileName, PayrollProccessId);

                    //this will prompt the user to open or download the file
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + chequeFileName);
                    Response.AddHeader("Content-Length", file.Length.ToString());

                    Response.OutputStream.Write(file, 0, file.Length);

                    Response.Flush();
                    Response.End();
                }
                catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
                {
                    if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        protected void WireEvents()
        {
            ExportChequeGrid.NeedDataSource += new Telerik.Web.UI.GridNeedDataSourceEventHandler(ExportChequeGrid_NeedDataSource);
        }
        protected void LoadExportChequeInformation()
        {
            Data = PayrollMasterPaymentCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetPayrollMasterPaymentDirectDeposit(PayrollProccessId, EmployeeNumber));

            foreach (PayrollMasterPayment dynamicsEFT in Data)
            {
                dynamicsEFT.AccountNumber = "XXXXXXXX" + dynamicsEFT.AccountNumber.Substring(dynamicsEFT.AccountNumber.Length - 3);
            }
        }
        #endregion

        #region event handlers
        void ExportChequeGrid_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            ExportChequeGrid.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        #endregion
    }
}