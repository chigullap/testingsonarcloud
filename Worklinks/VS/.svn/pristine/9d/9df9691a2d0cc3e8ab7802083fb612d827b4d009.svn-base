<?xml version="1.0" encoding="utf-8"?>
<!--
     The notices below are provided with respect to licensed content incorporated herein:
     Copyright NorthgateArinso 2013. All Rights Reserved. http://www.ngahr.com
-->
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	xmlns:ccts="urn:un:unece:uncefact:documentation:1.1" xmlns:hr="http://www.hr-xml.org/3"
	xmlns="http://www.ngahr.com/ngapexxml/1" targetNamespace="http://www.ngahr.com/ngapexxml/1"
	elementFormDefault="qualified" attributeFormDefault="unqualified">
	<xsd:annotation>
		<xsd:documentation> 
		Name: BE.xsd 
		Status: 1.06 Release 
		Date this version: 2014-Jun-3
		</xsd:documentation>
	</xsd:annotation>

	<xsd:include schemaLocation="../Components.xsd"/>

	<xsd:element name="BE" type="BEType"/>
	<xsd:complexType name="BEType">
		<xsd:annotation>
			<xsd:documentation source="http://www.ngahr.com/ngapexxml" xml:lang="en">
				<ccts:DictionaryEntryName> NGA Belgium </ccts:DictionaryEntryName>
				<ccts:DefinitionText> Local data for Belgium. </ccts:DefinitionText>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="BESI" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="BEFiscal" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="BEWT" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEContract" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEID" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIP" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="EffectiveDateAttributeGroup"/>
	</xsd:complexType>
	<xsd:element name="BESI" type="BESIType"/>
	<xsd:complexType name="BESIType">
		<xsd:annotation>
			<xsd:documentation source="http://www.ngahr.com/ngapexxml" xml:lang="en">
				<ccts:DictionaryEntryName> NGA Belgium SI </ccts:DictionaryEntryName>
				<ccts:DefinitionText> Social Insurance data for Belgium. </ccts:DefinitionText>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="BESINumberINSS" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BESIEmployeeSpecification" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BESIEmployerClass" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BESIWorkerCode" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BESIFactoryShutdownFund" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BESISICountry" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="EffectiveDateAttributeGroup"/>
	</xsd:complexType>

	<xsd:element name="BEFiscal" type="BEFiscalType"/>
	<xsd:complexType name="BEFiscalType">
		<xsd:annotation>
			<xsd:documentation source="http://www.ngahr.com/ngapexxml" xml:lang="en">
				<ccts:DictionaryEntryName> NGA Belgium Fiscal </ccts:DictionaryEntryName>
				<ccts:DefinitionText> Fiscal data for Belgium </ccts:DefinitionText>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="BEFiscalNumberChildren" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalNrDisabledChildren" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalChildFundNumber" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalPartnerIncome" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalNumberMinus65" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalNumberDisabledMinus65" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalNumberAbove65" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalNumberDisabledAbove65" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalPartnerDisabled" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalDisabilityPercentage" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalPartnersProfession" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalMaritalStatus" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalSingleParent" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalEmployeeDisabled" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalLeaveRegulationTax" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalTaxCalculationMethod" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalResident" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalTotalTaxPercentage" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalTaxAdvance" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalReasonTaxExemption" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalBorderWorker" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalAddPercForeigners" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalFische218Code" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEFiscalTaxDeclarationMethod" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="EffectiveDateAttributeGroup"/>
	</xsd:complexType>

	<xsd:element name="BEWT" type="BEWTType"/>
	<xsd:complexType name="BEWTType">
		<xsd:annotation>
			<xsd:documentation source="http://www.ngahr.com/ngapexxml" xml:lang="en">
				<ccts:DictionaryEntryName> Working Time </ccts:DictionaryEntryName>
				<ccts:DefinitionText> Additional Social Insurance and Fiscal data related to working
					time </ccts:DefinitionText>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="BEWTReferenceWorkingSchedule" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEWTReorganizationMeasure" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEWTReorganizationMeasureType" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEWTTaxDeductionShift" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="EffectiveDateAttributeGroup"/>
	</xsd:complexType>

	<xsd:element name="BEContract" type="BEContractType"/>
	<xsd:complexType name="BEContractType">
		<xsd:annotation>
			<xsd:documentation source="http://www.ngahr.com/ngapexxml" xml:lang="en">
				<ccts:DictionaryEntryName> NGA Belgium Contract </ccts:DictionaryEntryName>
				<ccts:DefinitionText> Contains additional contract information required for Belgian
					payroll. </ccts:DefinitionText>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="BEContractJointCommission" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEContractCollectiveAgreement" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEContractSeniority" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEContractMonthsPerYear" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEContractMealVoucher" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEContractDismissalPeriod" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEContractNotificationDate" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="EffectiveDateAttributeGroup"/>
	</xsd:complexType>

	<xsd:element name="BEID" type="BEIDType"/>
	<xsd:complexType name="BEIDType">
		<xsd:annotation>
			<xsd:documentation source="http://www.ngahr.com/ngapexxml" xml:lang="en">
				<ccts:DictionaryEntryName> NGA Belgium Indemnity Definition </ccts:DictionaryEntryName>
				<ccts:DefinitionText> NGA Belgium Indemnity Definition </ccts:DefinitionText>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="BEIDIndemnityType" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIDHalftime" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIDDependentFamMembers" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIDEmployerClass" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIDWorkerCode" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIDDebtor" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIDEmployerID" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIDSocWelfareBenefit" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIDFirstPaymentDate" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIDNoticeDate" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIDJointCommission" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIDNACEcode" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIDCIamtAdjustment" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIDWagetype" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="EffectiveDateAttributeGroup"/>
	</xsd:complexType>

	<xsd:element name="BEIP" type="BEIPType"/>
	<xsd:complexType name="BEIPType">
		<xsd:annotation>
			<xsd:documentation source="http://www.ngahr.com/ngapexxml" xml:lang="en">
				<ccts:DictionaryEntryName> NGA Belgium Indemnity Payment </ccts:DictionaryEntryName>
				<ccts:DefinitionText> NGA Belgium Indemnity Payment </ccts:DefinitionText>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="BEIPIndemnityID" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPCapitalization" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPReferencetext" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPAgreementtype" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPcheckbox2005" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPResumptionMeasure" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPResumptionWorkType" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPMultipleEmployers" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPCIamtAdjustment" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPIncompleteMonth" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPWagetype1" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPCiAmount1" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPWagetype2" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPCiAmount2" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPWagetype3" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPCiAmount3" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPWagetype4" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPCiAmount4" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPWagetype5" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="BEIPCiAmount5" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="EffectiveDateAttributeGroup"/>
	</xsd:complexType>
</xsd:schema>
