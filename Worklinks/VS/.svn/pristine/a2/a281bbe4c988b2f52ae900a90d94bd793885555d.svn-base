﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RoleSearchControl.ascx.cs" Inherits="WorkLinks.Admin.RoleEditor.RoleSearchControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RoleSummaryGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnClear">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RoleSummaryGrid" />
                <telerik:AjaxUpdatedControl ControlID="RoleDescription" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<table width="100%">
    <tr valign="top">
        <td>
            <asp:Panel ID="Panel3" runat="server" DefaultButton="btnSearch">
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="RoleDescription" runat="server" ResourceName="RoleDescription" TabIndex="010" />
                        </td>
                    </tr>
                </table>
                <div class="SearchCriteriaButtons">
                    <wasp:WLPButton ID="btnClear" Icon-PrimaryIconUrl="~/App_Themes/Default/clear.gif" ResourceName="btnClear" runat="server" Text="Clear" OnClientClicked="clear" OnClick="btnClear_Click" CssClass="button" />
                    <wasp:WLPButton ID="btnSearch" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" ResourceName="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                </div>
            </asp:Panel>
        </td>
    </tr>
    <tr valign="top">
        <td>
            <wasp:WLPToolBar ID="RoleSummaryToolBar" runat="server" Width="100%" OnClientLoad="RoleSummaryToolBar_init">
                <Items>
                    <wasp:WLPToolBarButton OnPreRender="AddToolBar_PreRender" Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" onclick="Add()" CommandName="add" ResourceName="Add" />
                    <wasp:WLPToolBarButton OnPreRender="DetailsToolBar_PreRender" Text="Details" onclick="OpenDetails()" CommandName="details" ResourceName="Details" />
                    <wasp:WLPToolBarButton OnPreRender="DataSecurityToolBar_PreRender" Text="Data Security" onclick="OpenDataSecurity()" CommandName="DataSecurity" ResourceName="DataSecurity" />
                    <wasp:WLPToolBarButton OnPreRender="FieldSecurityToolBar_PreRender" Text="Field Security" onclick="OpenFieldSecurity()" CommandName="fieldSecurity" ResourceName="FieldSecurity" />
                    <wasp:WLPToolBarButton OnPreRender="FormSecurityToolBar_PreRender" Text="Form Security" onclick="OpenFormSecurity()" CommandName="formSecurity" ResourceName="FormSecurity" />
                </Items>
            </wasp:WLPToolBar>

            <wasp:WLPGrid
                ID="RoleSummaryGrid"
                runat="server"
                AllowPaging="true"
                PagerStyle-AlwaysVisible="true"
                PageSize="100"
                AllowSorting="true"
                GridLines="None"
                Height="400px"
                AutoAssignModifyProperties="true">

                <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                    <ClientEvents OnRowDblClick="OnRowDblClick" OnRowClick="OnRowClick" />
                </ClientSettings>

                <MasterTableView ClientDataKeyNames="RoleId" AutoGenerateColumns="false" DataKeyNames="RoleId" CommandItemDisplay="Top">
                    <CommandItemTemplate></CommandItemTemplate>
                    <Columns>
                        <wasp:GridBoundControl DataField="EnglishDesc" LabelText="English Description**" SortExpression="EnglishDesc" UniqueName="EnglishDesc" ResourceName="EnglishDesc">
                            <HeaderStyle Width="16%" />
                        </wasp:GridBoundControl>
                        <wasp:GridBoundControl DataField="FrenchDesc" LabelText="French Description**" SortExpression="FrenchDesc" UniqueName="FrenchDesc" ResourceName="FrenchDesc">
                            <HeaderStyle Width="16%" />
                        </wasp:GridBoundControl>
                        <wasp:GridKeyValueControl OnNeedDataSource="PopulateCombo" LabelText="Role Type**" DataField="RoleTypeSecurityRoleId" SortExpression="RoleTypeSecurityRoleId" Type="RoleTypeSecurityRoleId" ResourceName="RoleTypeSecurityRoleId">
                            <HeaderStyle Width="16%" />
                        </wasp:GridKeyValueControl>
                    </Columns>
                </MasterTableView>

                <HeaderContextMenu EnableAutoScroll="true" />
            </wasp:WLPGrid>
        </td>
    </tr>
</table>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="RoleWindows"
    Width="1000"
    Height="300"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    OnClientClose="onClientClose"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="FieldWindows"
    Width="1000"
    Height="600"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var roleId;
        var rowSelected;
        var addButton;
        var detailsButton;
        var dataButton;
        var fieldButton;
        var formButton;
        var toolbar;

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function clear() {
            roleId = null;
        }

        function RoleSummaryToolBar_init(sender) {
            initializeControls();
        }

        function initializeControls() {
            toolbar = $find('<%= RoleSummaryToolBar.ClientID %>');

            addButton = toolbar.findButtonByCommandName('add');
            detailsButton = toolbar.findButtonByCommandName('details');
            dataButton = toolbar.findButtonByCommandName('DataSecurity');
            fieldButton = toolbar.findButtonByCommandName('fieldSecurity');
            formButton = toolbar.findButtonByCommandName('formSecurity');

            if (detailsButton != null)
                detailsButton.disable();

            if (dataButton != null)
                dataButton.disable();

            if (fieldButton != null)
                fieldButton.disable();

            if (formButton != null)
                formButton.disable();

            roleId = null;
        }

        function OnRowDblClick(sender, eventArgs) {
            if (toolbar.findButtonByCommandName('details') != null)
                OpenDetails(sender);
        }

        function OnRowClick(sender, eventArgs) {
            roleId = eventArgs.getDataKeyValue('RoleId');
            rowSelected = true;

            if (detailsButton != null)
                detailsButton.enable();

            if (dataButton != null)
                dataButton.enable();

            if (fieldButton != null)
                fieldButton.enable();

            if (formButton != null)
                formButton.enable();
        }

        function OpenDetails() {
            if (roleId != null)
                openWindowWithManager('RoleWindows', String.format('/Admin/Role/View/{0}', roleId), false);
        }

        function OpenDataSecurity() {
            if (roleId != null)
                openWindowWithManager('FieldWindows', String.format('/Admin/Role/DataSecurity/{0}', roleId), false);
        }

        function OpenFieldSecurity() {
            if (roleId != null)
                openWindowWithManager('FieldWindows', String.format('/Admin/FieldSecurity/FieldSecurity/{0}', roleId), false);
        }

        function OpenFormSecurity() {
            if (roleId != null)
                openWindowWithManager('FieldWindows', String.format('/Admin/FormSecurity/FormSecurity/{0}', roleId), false);
        }

        function Add() {
            openWindowWithManager('RoleWindows', String.format('/Admin/Role/Add/{0}', -1), false);
        }

        function RowSelected() {
            return rowSelected;
        }

        function onClientClose(sender, eventArgs) {
            __doPostBack('<%= RoleSummaryGrid.ClientID %>', '<%= String.Format("refresh") %>');
        }
    </script>
</telerik:RadScriptBlock>