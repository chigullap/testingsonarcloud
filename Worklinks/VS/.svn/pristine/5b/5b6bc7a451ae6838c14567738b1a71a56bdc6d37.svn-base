<?xml version="1.0" encoding="utf-8"?>
<!--
     The notices below are provided with respect to licensed content incorporated herein:
     Copyright NorthgateArinso 2013. All Rights Reserved. http://www.ngahr.com
     Copyright HR-XML Consortium. All Rights Reserved. http://www.hrxmlconsortium.org and http://www.hr-xml.org. Terms of license can be found in license.txt.
     Copyright (c) 1997-2011 Open Applications Group, Inc. All Rights Reserved.  http://www.openapplications.org
     -->
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	xmlns:oa="http://www.openapplications.org/oagis/9" xmlns:hr="http://www.hr-xml.org/3"
	xmlns="http://www.ngahr.com/ngapexxml/1" targetNamespace="http://www.ngahr.com/ngapexxml/1"
	elementFormDefault="qualified" attributeFormDefault="unqualified">
	<xsd:annotation>
		<xsd:documentation> 
		Name: AcknowledgeBOD.xsd 
		Status: 1.06 Release 
		Date this version: 2014-Jun-3 
		</xsd:documentation>
	</xsd:annotation>

	<xsd:import namespace="http://www.openapplications.org/oagis/9"
		schemaLocation="../Oagis/Nouns/BOD.xsd"/>
	<xsd:import namespace="http://www.hr-xml.org/3"
		schemaLocation="../HRXML/Developer/Nouns/IndicativeData.xsd"/>

	<xsd:element name="AcknowledgePayServEmp" type="AcknowledgePayServEmpType"/>
	<xsd:complexType name="AcknowledgePayServEmpType">
		<xsd:annotation>
			<xsd:documentation> AcknowledgeBOD conveys the result of a sent BOD. Within Payroll
				Exchange one and the same BOD type (AcknowledgeBOD) is used for any of the other BOD
				types sent. This means there is no specific AcknowledgeBOD for each BOD type.
				Acknowledge is a "Response Verb". Response Expressions represent and communicate
				actions taken by the receiver. XPath is the default expression language. The
				actionCode attribute (Accepted, Modified, Rejected) specifies the action taken by
				the receiver. ChangeStatus allows detailed change and state information to be
				returned. </xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="oa:BusinessObjectDocumentType">
				<xsd:sequence>
					<xsd:element name="DataArea" type="AcknowledgePayServEmpDataAreaType"
						minOccurs="0" maxOccurs="1">
						<xsd:annotation>
							<xsd:documentation> The DataArea is the part of the BOD that contains
								business information. This information consists of a verb and one or
								more noun instances. </xsd:documentation>
						</xsd:annotation>
					</xsd:element>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:complexType name="AcknowledgePayServEmpDataAreaType">
		<xsd:sequence>
			<xsd:element ref="oa:Acknowledge" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
	</xsd:complexType>
</xsd:schema>
