﻿using System;
using System.Data;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerAccrualPolicySearch : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String AccrualPolicyDescriptionId = "accrual_policy_description_id";
            public static String AccrualPolicyId = "accrual_policy_id";
            public static String AccrualPolicyDescriptionField = "description";
        }
        #endregion

        #region main
        internal AccrualPolicySearchCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String criteria)
        {
            DataBaseCommand command = GetStoredProcCommand("AccrualPolicy_report", user.DatabaseName);

            command.AddParameterWithValue("@codeLanguage", user.LanguageCode);
            command.AddParameterWithValue("@accrualPolicyDescription", criteria = criteria == "" ? null : criteria); //if criteria is "", then make it null for the proc, otherwise leave it alone

            using (IDataReader reader = command.ExecuteReader())
                return MapToAccrualPolicyDescriptionCollection(reader);
        }
        #endregion

        #region data mapping
        protected AccrualPolicySearchCollection MapToAccrualPolicyDescriptionCollection(IDataReader reader)
        {
            AccrualPolicySearchCollection collection = new AccrualPolicySearchCollection();

            while (reader.Read())
                collection.Add(MapToAccrualPolicyDescription(reader));

            return collection;
        }
        protected AccrualPolicySearch MapToAccrualPolicyDescription(IDataReader reader)
        {
            AccrualPolicySearch item = new AccrualPolicySearch();
            base.MapToBase(item, reader);

            item.AccrualPolicyDescriptionId = (long)CleanDataValue(reader[ColumnNames.AccrualPolicyDescriptionId]);
            item.AccrualPolicyId = (long)CleanDataValue(reader[ColumnNames.AccrualPolicyId]);
            item.AccrualPolicyDescriptionField = (String)CleanDataValue(reader[ColumnNames.AccrualPolicyDescriptionField]);

            return item;
        }
        #endregion
    }
}