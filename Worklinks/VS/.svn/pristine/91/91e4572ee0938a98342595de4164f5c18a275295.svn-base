﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class Person : GovernmentIdentificationNumberTypeTemplate
    {
        #region fields
        private long _personId = -1;
        #endregion

        #region properties
        public override string Key
        {
            get { return PersonId.ToString(); }
        }

        [DataMember]
        public long PersonId
        {
            get { return _personId; }
            set
            {
                KeyChangedEventArgs args = new KeyChangedEventArgs();
                args.OldKey = _personId.ToString();
                _personId = value;
                args.NewKey = Key;
                if (!(args.OldKey ?? String.Empty).Equals(args.NewKey ?? String.Empty) && this.GetType().Equals(typeof(Person)))
                    OnKeyChanged(args);
            }
        }

        [DataMember]
        public String TitleCode { get; set; }

        [DataMember]
        public String GenderCode { get; set; }

        [DataMember]
        [Required]
        public String LastName { get; set; }

        [DataMember]
        public String MiddleName { get; set; }

        [DataMember]
        [Required]
        public String FirstName { get; set; }

        [DataMember]
        public String GovernmentIdentificationNumberTypeCode { get; set; }

        [DataMember]
        [Required]
        public String GovernmentIdentificationNumber1 { get; set; }

        [DataMember]
        public String GovernmentIdentificationNumber2 { get; set; }

        [DataMember]
        public String GovernmentIdentificationNumber3 { get; set; }

        [DataMember]
        [Required]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public String KnownAsName { get; set; }

        [DataMember]
        public new String LanguageCode { get; set; }

        [DataMember]
        public bool DisabledFlag { get; set; }

        [DataMember]
        public bool StudentFlag { get; set; }

        [DataMember]
        public String SmokerCode { get; set; }

        [DataMember]
        public bool OverrideConcurrencyCheckFlag { get; set; }

        public String ChequeName
        {
            get { return String.Format("{0}, {1}", LastName, FirstName); }
        }

        public int Age
        {
            get
            {
                if (BirthDate == null || DateTime.Now < BirthDate)
                    return 0;
                else
                {
                    DateTime birthDate = (DateTime)BirthDate;
                    DateTime now = DateTime.Now;
                    return now.Year - birthDate.Year + ((birthDate.Month > now.Month || (birthDate.Month == now.Month && birthDate.Day > now.Day)) ? -1 : 0);
                }
            }
        }
        #endregion

        #region construct/destruct
        public Person()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            Person item = new Person();
            this.CopyTo(item);
            return item;
        }
        public new void Clear()
        {
            base.Clear();
            PersonId = -1;
            TitleCode = null;
            GenderCode = null;
            LastName = null;
            MiddleName = null;
            FirstName = null;
            GovernmentIdentificationNumberTypeCode = null;
            GovernmentIdentificationNumber1 = null;
            GovernmentIdentificationNumber2 = null;
            GovernmentIdentificationNumber3 = null;
            BirthDate = null;
            KnownAsName = null;
            LanguageCode = null;
            DisabledFlag = false;
            StudentFlag = false;
            SmokerCode = null;
            OverrideConcurrencyCheckFlag = false;
        }
        public virtual void CopyTo(Person data)
        {
            base.CopyTo(data);
            data.RowVersion = RowVersion;

            data.PersonId = PersonId;
            data.TitleCode = TitleCode;
            data.GenderCode = GenderCode;
            data.LastName = LastName;
            data.MiddleName = MiddleName;
            data.FirstName = FirstName;
            data.GovernmentIdentificationNumberTypeCode = GovernmentIdentificationNumberTypeCode;
            data.GovernmentIdentificationNumber1 = GovernmentIdentificationNumber1;
            data.GovernmentIdentificationNumber2 = GovernmentIdentificationNumber2;
            data.GovernmentIdentificationNumber3 = GovernmentIdentificationNumber3;
            data.BirthDate = BirthDate;
            data.KnownAsName = KnownAsName;
            data.LanguageCode = LanguageCode;
            data.DisabledFlag = DisabledFlag;
            data.StudentFlag = StudentFlag;
            data.SmokerCode = SmokerCode;
            data.OverrideConcurrencyCheckFlag = OverrideConcurrencyCheckFlag;
        }
        #endregion
    }
}