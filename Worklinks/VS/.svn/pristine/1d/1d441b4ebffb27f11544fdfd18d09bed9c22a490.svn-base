﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerRevenueQuebecR1Export : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String RevenueQuebecR1ExportId = "revenue_quebec_r1_export_id";
            public static String RevenueQuebecTransmitterRecordId = "revenue_quebec_transmitter_record_id";
            public static String YearEndId = "year_end_id";
            public static String YearEndFormCode = "code_year_end_form_cd";
        }
        #endregion

        #region main
        internal RevenueQuebecR1ExportCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long yearEndId, String exportType)
        {
            DataBaseCommand command = GetStoredProcCommand("RevenueQuebecR1Export_select", user.DatabaseName);
            command.AddParameterWithValue("@yearEndId", yearEndId);
            command.AddParameterWithValue("@exportType", exportType);

            using (IDataReader reader = command.ExecuteReader())
                return MapToRevenueQuebecR1ExportCollection(reader);
        }
        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, RevenueQuebecR1Export item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("RevenueQuebecR1Export_update", user.DatabaseName))
                {
                    command.BeginTransaction("RevenueQuebecR1Export_update");

                    command.AddParameterWithValue("@revenueQuebecR1ExportId", item.RevenueQuebecR1ExportId);
                    command.AddParameterWithValue("@revenueQuebecTransmitterRecordId", item.RevenueQuebecTransmitterRecordId);
                    command.AddParameterWithValue("@yearEndId", item.YearEndId);
                    command.AddParameterWithValue("@exportType", item.YearEndFormCode);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    item.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, RevenueQuebecR1Export item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("RevenueQuebecR1Export_insert", user.DatabaseName))
            {
                command.BeginTransaction("RevenueQuebecR1Export_insert");

                SqlParameter idParm = command.AddParameterWithValue("@revenueQuebecR1ExportId", item.RevenueQuebecR1ExportId, ParameterDirection.Output);
                command.AddParameterWithValue("@revenueQuebecTransmitterRecordId", item.RevenueQuebecTransmitterRecordId);
                command.AddParameterWithValue("@yearEndId", item.YearEndId);
                command.AddParameterWithValue("@exportType", item.YearEndFormCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.RevenueQuebecR1ExportId = Convert.ToInt64(idParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }
        #endregion

        #region data mapping
        protected RevenueQuebecR1ExportCollection MapToRevenueQuebecR1ExportCollection(IDataReader reader)
        {
            RevenueQuebecR1ExportCollection collection = new RevenueQuebecR1ExportCollection();

            while (reader.Read())
                collection.Add(MapToRevenueQuebecR1Export(reader));

            return collection;
        }
        protected RevenueQuebecR1Export MapToRevenueQuebecR1Export(IDataReader reader)
        {
            RevenueQuebecR1Export item = new RevenueQuebecR1Export();
            base.MapToBase(item, reader);

            item.RevenueQuebecR1ExportId = (long)CleanDataValue(reader[ColumnNames.RevenueQuebecR1ExportId]);
            item.RevenueQuebecTransmitterRecordId = (long)CleanDataValue(reader[ColumnNames.RevenueQuebecTransmitterRecordId]);
            item.YearEndId = (long)CleanDataValue(reader[ColumnNames.YearEndId]);
            item.YearEndFormCode = (String)CleanDataValue(reader[ColumnNames.YearEndFormCode]);

            return item;
        }
        #endregion
    }
}