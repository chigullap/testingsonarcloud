﻿using System;
using System.Data;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerAccrualEntitlementSearch : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String AccrualEntitlementDescriptionId = "accrual_entitlement_description_id";
            public static String AccrualEntitlementId = "accrual_entitlement_id";
            public static String AccrualEntitlementDescriptionField = "description";
        }
        #endregion

        #region main
        internal AccrualEntitlementSearchCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String criteria)
        {
            DataBaseCommand command = GetStoredProcCommand("AccrualEntitlement_report", user.DatabaseName);

            command.AddParameterWithValue("@codeLanguage", user.LanguageCode);
            command.AddParameterWithValue("@accrualEntitlementDescription", criteria = criteria == "" ? null : criteria); //if criteria is "", then make it null for the proc, otherwise leave it alone

            using (IDataReader reader = command.ExecuteReader())
                return MapToAccrualEntitlementDescriptionCollection(reader);
        }
        #endregion

        #region data mapping
        protected AccrualEntitlementSearchCollection MapToAccrualEntitlementDescriptionCollection(IDataReader reader)
        {
            AccrualEntitlementSearchCollection collection = new AccrualEntitlementSearchCollection();

            while (reader.Read())
                collection.Add(MapToAccrualEntitlementDescription(reader));

            return collection;
        }
        protected AccrualEntitlementSearch MapToAccrualEntitlementDescription(IDataReader reader)
        {
            AccrualEntitlementSearch item = new AccrualEntitlementSearch();
            base.MapToBase(item, reader);

            item.AccrualEntitlementDescriptionId = (long)CleanDataValue(reader[ColumnNames.AccrualEntitlementDescriptionId]);
            item.AccrualEntitlementId = (long)CleanDataValue(reader[ColumnNames.AccrualEntitlementId]);
            item.AccrualEntitlementDescriptionField = (String)CleanDataValue(reader[ColumnNames.AccrualEntitlementDescriptionField]);

            return item;
        }
        #endregion
    }
}