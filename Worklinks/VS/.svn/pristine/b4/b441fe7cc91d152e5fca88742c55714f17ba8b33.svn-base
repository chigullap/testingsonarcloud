﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerYearEndT4aXmlFileToWorklinks : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string YearEndT4aId = "year_end_t4a_id";
            public static string Year = "year";
            public static string EmployeeId = "employee_id";
            public static string EmployeeNumber = "employee_number";
            public static string EmployerNumber = "employer_number";
            public static string PensionSuperannuation = "pension_superannuation";
            public static string LumpSumPay18 = "lump_sum_pay_18";
            public static string SelfEmployedCommission = "self_employed_commission";
            public static string IncomeTaxDeduction22 = "income_tax_deduction_22";
            public static string Annuities = "annuities";
            public static string EligibleRetiringAllow = "eligible_retiring_allow";
            public static string NonEligibleRetiringAllow = "non_eligible_retiring_allow";
            public static string OtherIncome = "other_income";
            public static string PatronageAllocation = "patronage_allocation";
            public static string RegisteredPensionPlanContributions = "registered_pension_plan_contributions";
            public static string PensionAdjustment = "pension_adjustment";
            public static string RegistrationNumber = "registration_number";
            public static string FootNotesCode = "foot_notes_code";
            public static string RespIncome = "resp_income";
            public static string RespAssistance = "resp_assistance";
            public static string CharitableDonations46 = "charitable_donations_46";
            public static string Footnotes = "footnotes";
            public static string ProvincialTax = "provincial_tax";
            public static string RecordStatus = "record_status";
            public static string GenerateXml = "generate_xml";
            public static string XmlCreated = "xml_created";
            public static string FeesForServices = "fees_for_services";
            public static string UnregisteredPlanAmount109 = "unregistered_plan_amount_109";
            public static string MedicalPremiumBenefitsAmount118 = "medical_premium_benefits_amount_118";
            public static string GroupInsurancePlanAmount119 = "group_insurance_plan_amount_119";
            public static string LumpSumPaymentsFromUnregisteredPlan102 = "lump_sum_payments_from_unregistered_plan_102";
            public static string LumpSumPaymentsFromUnregisteredPlan108 = "lump_sum_payments_from_unregistered_plan_108";
            public static string LumpSumPaymentsFromUnregisteredPlan110 = "lump_sum_payments_from_unregistered_plan_110";
            public static string LumpSumPaymentsFromUnregisteredPlan158 = "lump_sum_payments_from_unregistered_plan_158";
            public static string LumpSumPaymentsFromUnregisteredPlan180 = "lump_sum_payments_from_unregistered_plan_180";
            public static string LumpSumPaymentsFromUnregisteredPlan190 = "lump_sum_payments_from_unregistered_plan_190";
            public static string TaxDeferredCooperativeShare129 = "tax_deferred_cooperative_share_129";
            public static string VariablePensionBenefits133 = "variable_pension_benefits_133";
            public static string RecipientPaidPreminumsPrivateHealthServices135 = "recipient_paid_preminums_private_health_services_135";
            public static string TuitionAssistanceAdultBasicEducation196 = "tuition_assistance_adult_basic_education_196";
            public static string ResearchGrants104 = "research_grants_104";
            public static string PaymentsWageLossReplacementPlan107 = "payments_wage_loss_replacement_plan_107";
            public static string VeteransBenefits127 = "veterans_benefits_127";
            public static string WageEarnerProtectionProgram132 = "wage_earner_protection_program_132";
            public static string SubpQualitifed152 = "subp_qualitifed_152";
            public static string BankruptcySettlement156 = "bankruptcy_settlement_156";
            public static string RprrPayments194 = "rprr_payments_194";
            public static string RegisteredDisabilitySavingsPlan131 = "registered_disability_savings_plan_131";
            public static string ScholarshipsPrizes105 = "scholarships_prizes_105";
            public static string DeathBenefits106 = "death_benefits_106";
            public static string LoanBenefits117 = "loan_benefits_117";
            public static string RevokedDpspPayments123 = "revoked_dpsp_payments_123";
            public static string DisabilityBenefitsPaid125 = "disability_benefits_paid_125";
            public static string ApprenticeshipIncentiveGrant130 = "apprenticeship_incentive_grant_130";
            public static string TfsaTaxableAmount134 = "tfsa_taxable_amount_134";
            public static string CanadianBenefitPyvc136 = "canadian_benefit_pyvc_136";
            public static string LabourAdjustmentBenefitsAct150 = "labour_adjustment_benefits_act_150";
            public static string CashAwardPrize154 = "cash_award_prize_154";
            public static string VeteransBenefitsEligibleForPensionSplitting128 = "veterans_benefits_eligible_for_pension_splitting_128";
            public static string ActiveFlag = "active_flag";
            public static string Revision = "revision";
            public static string PreviousRevisionYearEndT4aId = "previous_revision_year_end_t4a_id";
        }
        #endregion

        #region main
        public YearEndT4aCollection Select(DatabaseUser user, int year, long businessNumberId, bool getOrginals = false, bool getAmended = false)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndT4a_select", user.DatabaseName);

            command.AddParameterWithValue("@year", year);
            command.AddParameterWithValue("@businessNumberId", businessNumberId);
            command.AddParameterWithValue("@getOriginals", getOrginals);
            command.AddParameterWithValue("@getAmended", getAmended);

            using (IDataReader reader = command.ExecuteReader())
                return MapToYearEndT4aCollection(reader);
        }
        public YearEndT4aCollection SelectSingleT4aByKey(DatabaseUser user, long key)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndT4aByKeyId_select", user.DatabaseName);

            command.AddParameterWithValue("@yearEndT4aId", key);

            using (IDataReader reader = command.ExecuteReader())
                return MapToYearEndT4aCollection(reader);
        }
        public YearEndT4a Insert(DatabaseUser user, YearEndT4a yearEndT4a)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndT4a_insert", user.DatabaseName);

            using (command)
            {
                command.BeginTransaction("YearEndT4a_insert");

                SqlParameter yearEndT4aIdParm = command.AddParameterWithValue("@yearEndT4aId", yearEndT4a.YearEndT4aId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", yearEndT4a.EmployeeId);
                command.AddParameterWithValue("@employerNumber", yearEndT4a.EmployerNumber);
                command.AddParameterWithValue("@year", yearEndT4a.Year);
                command.AddParameterWithValue("@pensionSuperannuation", yearEndT4a.PensionSuperannuation);
                command.AddParameterWithValue("@lumpSumPay18", yearEndT4a.LumpSumPay18);
                command.AddParameterWithValue("@selfEmployedCommission", yearEndT4a.SelfEmployedCommission);
                command.AddParameterWithValue("@incomeTaxDeduction22", yearEndT4a.IncomeTaxDeduction22);
                command.AddParameterWithValue("@annuities", yearEndT4a.Annuities);
                command.AddParameterWithValue("@eligibleRetiringAllow", yearEndT4a.EligibleRetiringAllow);
                command.AddParameterWithValue("@nonEligibleRetiringAllow", yearEndT4a.NonEligibleRetiringAllow);
                command.AddParameterWithValue("@otherIncome", yearEndT4a.OtherIncome);
                command.AddParameterWithValue("@patronageAllocation", yearEndT4a.PatronageAllocation);
                command.AddParameterWithValue("@registeredPensionPlanContributions", yearEndT4a.RegisteredPensionPlanContributions);
                command.AddParameterWithValue("@pensionAdjustment", yearEndT4a.PensionAdjustment);
                command.AddParameterWithValue("@registrationNumber", yearEndT4a.RegistrationNumber);
                command.AddParameterWithValue("@footNotesCode", yearEndT4a.FootNotesCode);
                command.AddParameterWithValue("@respIncome", yearEndT4a.RespIncome);
                command.AddParameterWithValue("@respAssistance", yearEndT4a.RespAssistance);
                command.AddParameterWithValue("@charitableDonations46", yearEndT4a.CharitableDonations46);
                command.AddParameterWithValue("@footnotes", yearEndT4a.Footnotes);
                command.AddParameterWithValue("@provincialTax", yearEndT4a.ProvincialTax);
                command.AddParameterWithValue("@RecordStatus", yearEndT4a.RecordStatus);
                command.AddParameterWithValue("@GenerateXml", yearEndT4a.GenerateXml);
                command.AddParameterWithValue("@XmlCreated", yearEndT4a.XmlCreated);
                command.AddParameterWithValue("@feesForServices", yearEndT4a.FeesForServices);
                command.AddParameterWithValue("@unregisteredPlanAmount109", yearEndT4a.UnregisteredPlanAmount109);
                command.AddParameterWithValue("@medicalPremiumBenefitsAmount118", yearEndT4a.MedicalPremiumBenefitsAmount118);
                command.AddParameterWithValue("@groupInsurancePlanAmount119", yearEndT4a.GroupInsurancePlanAmount119);
                command.AddParameterWithValue("@lumpSumPaymentsFromUnregisteredPlan102", yearEndT4a.LumpSumPaymentsFromUnregisteredPlan102);
                command.AddParameterWithValue("@lumpSumPaymentsFromUnregisteredPlan108", yearEndT4a.LumpSumPaymentsFromUnregisteredPlan108);
                command.AddParameterWithValue("@lumpSumPaymentsFromUnregisteredPlan110", yearEndT4a.LumpSumPaymentsFromUnregisteredPlan110);
                command.AddParameterWithValue("@lumpSumPaymentsFromUnregisteredPlan158", yearEndT4a.LumpSumPaymentsFromUnregisteredPlan158);
                command.AddParameterWithValue("@lumpSumPaymentsFromUnregisteredPlan180", yearEndT4a.LumpSumPaymentsFromUnregisteredPlan180);
                command.AddParameterWithValue("@lumpSumPaymentsFromUnregisteredPlan190", yearEndT4a.LumpSumPaymentsFromUnregisteredPlan190);
                command.AddParameterWithValue("@taxDeferredCooperativeShare129", yearEndT4a.TaxDeferredCooperativeShare129);
                command.AddParameterWithValue("@variablePensionBenefits133", yearEndT4a.VariablePensionBenefits133);
                command.AddParameterWithValue("@recipientPaidPreminumsPrivateHealthServices135", yearEndT4a.RecipientPaidPreminumsPrivateHealthServices135);
                command.AddParameterWithValue("@tuitionAssistanceAdultBasicEducation196", yearEndT4a.TuitionAssistanceAdultBasicEducation196);
                command.AddParameterWithValue("@researchGrants104", yearEndT4a.ResearchGrants104);
                command.AddParameterWithValue("@paymentsWageLossReplacementPlan107", yearEndT4a.PaymentsWageLossReplacementPlan107);
                command.AddParameterWithValue("@veteransBenefits127", yearEndT4a.VeteransBenefits127);
                command.AddParameterWithValue("@wageEarnerProtectionProgram132", yearEndT4a.WageEarnerProtectionProgram132);
                command.AddParameterWithValue("@subpQualitifed152", yearEndT4a.SubpQualitifed152);
                command.AddParameterWithValue("@bankruptcySettlement156", yearEndT4a.BankruptcySettlement156);
                command.AddParameterWithValue("@rprrPayments194", yearEndT4a.RprrPayments194);
                command.AddParameterWithValue("@registeredDisabilitySavingsPlan131", yearEndT4a.RegisteredDisabilitySavingsPlan131);
                command.AddParameterWithValue("@scholarshipsPrizes105", yearEndT4a.ScholarshipsPrizes105);
                command.AddParameterWithValue("@deathBenefits106", yearEndT4a.DeathBenefits106);
                command.AddParameterWithValue("@loanBenefits117", yearEndT4a.LoanBenefits117);
                command.AddParameterWithValue("@revokedDpspPayments123", yearEndT4a.RevokedDpspPayments123);
                command.AddParameterWithValue("@disabilityBenefitsPaid125", yearEndT4a.DisabilityBenefitsPaid125);
                command.AddParameterWithValue("@apprenticeshipIncentiveGrant130", yearEndT4a.ApprenticeshipIncentiveGrant130);
                command.AddParameterWithValue("@tfsaTaxableAmount134", yearEndT4a.TfsaTaxableAmount134);
                command.AddParameterWithValue("@canadianBenefitPyvc136", yearEndT4a.CanadianBenefitPyvc136);
                command.AddParameterWithValue("@labourAdjustmentBenefitsAct150", yearEndT4a.LabourAdjustmentBenefitsAct150);
                command.AddParameterWithValue("@cashAwardPrize154", yearEndT4a.CashAwardPrize154);
                command.AddParameterWithValue("@veteransBenefitsEligibleForPensionSplitting128", yearEndT4a.VeteransBenefitsEligibleForPensionSplitting128);
                command.AddParameterWithValue("@activeFlag", yearEndT4a.ActiveFlag);
                command.AddParameterWithValue("@revision", yearEndT4a.Revision);
                command.AddParameterWithValue("@previousRevisionYearEndT4aId", yearEndT4a.PreviousRevisionYearEndT4aId);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", yearEndT4a.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                yearEndT4a.YearEndT4aId = Convert.ToInt64(yearEndT4aIdParm.Value);
                yearEndT4a.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return yearEndT4a;
            }
        }
        public void Update(DatabaseUser user, long keyId, bool activeFlag)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("YearEndT4a_update", user.DatabaseName))
                {
                    command.BeginTransaction("YearEndT4a_update");

                    command.AddParameterWithValue("@yearEndT4aId", keyId);
                    command.AddParameterWithValue("@activeFlag", activeFlag);

                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected YearEndT4aCollection MapToYearEndT4aCollection(IDataReader reader)
        {
            YearEndT4aCollection collection = new YearEndT4aCollection();

            while (reader.Read())
                collection.Add(MapToYearEndT4a(reader));

            return collection;
        }
        protected YearEndT4a MapToYearEndT4a(IDataReader reader)
        {
            YearEndT4a item = new YearEndT4a();
            MapToBase(item, reader);

            item.YearEndT4aId = (long)CleanDataValue(reader[ColumnNames.YearEndT4aId]);
            item.Year = (decimal)CleanDataValue(reader[ColumnNames.Year]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EmployeeNumber = (string)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.EmployerNumber = (string)CleanDataValue(reader[ColumnNames.EmployerNumber]);
            item.PensionSuperannuation = (decimal)CleanDataValue(reader[ColumnNames.PensionSuperannuation]);
            item.LumpSumPay18 = (decimal)CleanDataValue(reader[ColumnNames.LumpSumPay18]);
            item.SelfEmployedCommission = (decimal)CleanDataValue(reader[ColumnNames.SelfEmployedCommission]);
            item.IncomeTaxDeduction22 = (decimal)CleanDataValue(reader[ColumnNames.IncomeTaxDeduction22]);
            item.Annuities = (decimal)CleanDataValue(reader[ColumnNames.Annuities]);
            item.EligibleRetiringAllow = (decimal)CleanDataValue(reader[ColumnNames.EligibleRetiringAllow]);
            item.NonEligibleRetiringAllow = (decimal)CleanDataValue(reader[ColumnNames.NonEligibleRetiringAllow]);
            item.OtherIncome = (decimal)CleanDataValue(reader[ColumnNames.OtherIncome]);
            item.PatronageAllocation = (decimal)CleanDataValue(reader[ColumnNames.PatronageAllocation]);
            item.RegisteredPensionPlanContributions = (decimal)CleanDataValue(reader[ColumnNames.RegisteredPensionPlanContributions]);
            item.PensionAdjustment = (decimal)CleanDataValue(reader[ColumnNames.PensionAdjustment]);
            item.RegistrationNumber = (string)CleanDataValue(reader[ColumnNames.RegistrationNumber]);
            item.FootNotesCode = (short)CleanDataValue(reader[ColumnNames.FootNotesCode]);
            item.RespIncome = (decimal)CleanDataValue(reader[ColumnNames.RespIncome]);
            item.RespAssistance = (decimal)CleanDataValue(reader[ColumnNames.RespAssistance]);
            item.CharitableDonations46 = (decimal)CleanDataValue(reader[ColumnNames.CharitableDonations46]);
            item.Footnotes = (string)CleanDataValue(reader[ColumnNames.Footnotes]);
            item.ProvincialTax = (decimal)CleanDataValue(reader[ColumnNames.ProvincialTax]);
            item.RecordStatus = (short?)CleanDataValue(reader[ColumnNames.RecordStatus]);
            item.GenerateXml = (byte?)CleanDataValue(reader[ColumnNames.GenerateXml]);
            item.XmlCreated = (byte?)CleanDataValue(reader[ColumnNames.XmlCreated]);
            item.FeesForServices = (decimal)CleanDataValue(reader[ColumnNames.FeesForServices]);
            item.UnregisteredPlanAmount109 = (decimal)CleanDataValue(reader[ColumnNames.UnregisteredPlanAmount109]);
            item.MedicalPremiumBenefitsAmount118 = (decimal)CleanDataValue(reader[ColumnNames.MedicalPremiumBenefitsAmount118]);
            item.GroupInsurancePlanAmount119 = (decimal)CleanDataValue(reader[ColumnNames.GroupInsurancePlanAmount119]);
            item.LumpSumPaymentsFromUnregisteredPlan102 = (decimal)CleanDataValue(reader[ColumnNames.LumpSumPaymentsFromUnregisteredPlan102]);
            item.LumpSumPaymentsFromUnregisteredPlan108 = (decimal)CleanDataValue(reader[ColumnNames.LumpSumPaymentsFromUnregisteredPlan108]);
            item.LumpSumPaymentsFromUnregisteredPlan110 = (decimal)CleanDataValue(reader[ColumnNames.LumpSumPaymentsFromUnregisteredPlan110]);
            item.LumpSumPaymentsFromUnregisteredPlan158 = (decimal)CleanDataValue(reader[ColumnNames.LumpSumPaymentsFromUnregisteredPlan158]);
            item.LumpSumPaymentsFromUnregisteredPlan180 = (decimal)CleanDataValue(reader[ColumnNames.LumpSumPaymentsFromUnregisteredPlan180]);
            item.LumpSumPaymentsFromUnregisteredPlan190 = (decimal)CleanDataValue(reader[ColumnNames.LumpSumPaymentsFromUnregisteredPlan190]);
            item.TaxDeferredCooperativeShare129 = (decimal)CleanDataValue(reader[ColumnNames.TaxDeferredCooperativeShare129]);
            item.VariablePensionBenefits133 = (decimal)CleanDataValue(reader[ColumnNames.VariablePensionBenefits133]);
            item.RecipientPaidPreminumsPrivateHealthServices135 = (decimal)CleanDataValue(reader[ColumnNames.RecipientPaidPreminumsPrivateHealthServices135]);
            item.TuitionAssistanceAdultBasicEducation196 = (decimal)CleanDataValue(reader[ColumnNames.TuitionAssistanceAdultBasicEducation196]);
            item.ResearchGrants104 = (decimal)CleanDataValue(reader[ColumnNames.ResearchGrants104]);
            item.PaymentsWageLossReplacementPlan107 = (decimal)CleanDataValue(reader[ColumnNames.PaymentsWageLossReplacementPlan107]);
            item.VeteransBenefits127 = (decimal)CleanDataValue(reader[ColumnNames.VeteransBenefits127]);
            item.WageEarnerProtectionProgram132 = (decimal)CleanDataValue(reader[ColumnNames.WageEarnerProtectionProgram132]);
            item.SubpQualitifed152 = (decimal)CleanDataValue(reader[ColumnNames.SubpQualitifed152]);
            item.BankruptcySettlement156 = (decimal)CleanDataValue(reader[ColumnNames.BankruptcySettlement156]);
            item.RprrPayments194 = (decimal)CleanDataValue(reader[ColumnNames.RprrPayments194]);
            item.RegisteredDisabilitySavingsPlan131 = (decimal)CleanDataValue(reader[ColumnNames.RegisteredDisabilitySavingsPlan131]);
            item.ScholarshipsPrizes105 = (decimal)CleanDataValue(reader[ColumnNames.ScholarshipsPrizes105]);
            item.DeathBenefits106 = (decimal)CleanDataValue(reader[ColumnNames.DeathBenefits106]);
            item.LoanBenefits117 = (decimal)CleanDataValue(reader[ColumnNames.LoanBenefits117]);
            item.RevokedDpspPayments123 = (decimal)CleanDataValue(reader[ColumnNames.RevokedDpspPayments123]);
            item.DisabilityBenefitsPaid125 = (decimal)CleanDataValue(reader[ColumnNames.DisabilityBenefitsPaid125]);
            item.ApprenticeshipIncentiveGrant130 = (decimal)CleanDataValue(reader[ColumnNames.ApprenticeshipIncentiveGrant130]);
            item.TfsaTaxableAmount134 = (decimal)CleanDataValue(reader[ColumnNames.TfsaTaxableAmount134]);
            item.CanadianBenefitPyvc136 = (decimal)CleanDataValue(reader[ColumnNames.CanadianBenefitPyvc136]);
            item.LabourAdjustmentBenefitsAct150 = (decimal)CleanDataValue(reader[ColumnNames.LabourAdjustmentBenefitsAct150]);
            item.CashAwardPrize154 = (decimal)CleanDataValue(reader[ColumnNames.CashAwardPrize154]);
            item.VeteransBenefitsEligibleForPensionSplitting128 = (decimal)CleanDataValue(reader[ColumnNames.VeteransBenefitsEligibleForPensionSplitting128]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.Revision = (int)CleanDataValue(reader[ColumnNames.Revision]);
            item.PreviousRevisionYearEndT4aId = (long?)CleanDataValue(reader[ColumnNames.PreviousRevisionYearEndT4aId]);

            return item;
        }
        #endregion
    }
}