﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Text;
using System.Transactions;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;
using WorkLinks.DataLayer.DataAccess;
using WorkLinks.DataLayer.DataAccess.SqlServer;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class PersonManagement
    {
        #region fields
        private PersonAccess _personAccess = null;
        private AddressAccess _addressAccess = null;
        private CodeManagement _codeManagement = null;
        private EmployeeManagement _employeeManagement = null;
        private ApplicationResourceManagement _guiManagement = null;
        private WizardManagement _wizardManagement = null;
        #endregion

        #region properties
        private PersonAccess PersonAccess
        {
            get
            {
                if (_personAccess == null)
                    _personAccess = new PersonAccess();

                return _personAccess;
            }
        }
        private AddressAccess AddressAccess
        {
            get
            {
                if (_addressAccess == null)
                    _addressAccess = new AddressAccess();

                return _addressAccess;
            }
        }
        private CodeManagement CodeManagement
        {
            get
            {
                if (_codeManagement == null)
                    _codeManagement = new CodeManagement();

                return _codeManagement;
            }
        }
        private EmployeeManagement EmployeeManagement
        {
            get
            {
                if (_employeeManagement == null)
                    _employeeManagement = new EmployeeManagement();

                return _employeeManagement;
            }
        }
        private ApplicationResourceManagement GuiManagement
        {
            get
            {
                if (_guiManagement == null)
                    _guiManagement = new ApplicationResourceManagement();

                return _guiManagement;
            }
        }
        private WizardManagement WizardManagement
        {
            get
            {
                if (_wizardManagement == null)
                    _wizardManagement = new WizardManagement();

                return _wizardManagement;
            }
        }
        #endregion

        #region person
        public PersonCollection GetPerson(DatabaseUser user, long personId)
        {
            try
            {
                return PersonAccess.GetPerson(user, personId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - GetPerson", ex);
                throw;
            }
        }

        public void UpdateAuthPerson(DatabaseUser user, string adminDatabaseName, string userName, Person person)
        {
            try
            {
                PersonAccess.UpdateAuthPerson(user, adminDatabaseName, userName, person);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - UpdateAuthPerson", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }

        public void UpdatePerson(DatabaseUser user, Person person)
        {
            try
            {
                PersonAccess.UpdatePerson(user, person);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - UpdatePerson", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void UpdateUnionContactPerson(DatabaseUser user, Person person)
        {
            try
            {
                PersonAccess.UpdateUnionContactPerson(user, person);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - UpdateUnionContactPerson", ex);
                throw;
            }
        }
        public void DeletePerson(DatabaseUser user, Person person)
        {
            try
            {
                PersonAccess.DeletePerson(user, person);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - DeletePerson", ex);
                throw;
            }
        }
        public void InsertPerson(DatabaseUser user, string overrideDatabaseName,  Person person)
        {
            try
            {
                PersonAccess.InsertPerson(user, overrideDatabaseName, person);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - InsertPerson", ex);
                throw;
            }
        }
        #endregion

        #region person address
        public void ProcessAddressImportViaService(String database, String username, AddressImport[] array)
        {
            List<AddressImport> partial = new List<AddressImport>();

            for (int i = 0; i < array.Length; i++)
            {
                //add to list
                partial.Add(array[i]);

                if ((i + 1) % 5000 == 0 || i == array.Length - 1)
                {
                    //store in db 
                    PersonAccess.ImportAddressUsingService(database, username, partial);

                    //clear the list
                    partial.Clear();
                }
            }
        }
        public PersonAddressCollection GetPersonAddress(DatabaseUser user, long personId)
        {
            try
            {
                PersonAddressCollection temp = PersonAccess.GetPersonAddress(user, personId);

                //format for GUI
                foreach (PersonAddress address in temp)
                    FormatPostalZipCode(address);

                return temp;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - GetPersonAddress", ex);
                throw;
            }
        }
        public AddressCollection GetAddress(DatabaseUser user, long addressId)
        {
            try
            {
                AddressCollection temp = AddressAccess.GetAddress(user, addressId);

                //format for GUI
                foreach (Address address in temp)
                    FormatPostalZipCode(address);

                return temp;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - GetAddress", ex);
                throw;
            }
        }
        /// <summary>
        /// used in scenarios where addresses are shared
        /// </summary>
        /// <param name="address"></param>
        /// <param name="handlePreviousAddress"></param>
        public void UpdatePersonAddress(DatabaseUser user, PersonAddress address, bool handlePreviousAddress, List<PersonAddress> theData)
        {
            try
            {
                if (handlePreviousAddress)
                {
                    //check data before allowing updates
                    if (CheckForUniqueAddressType(address, theData))
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                            //get existing person address
                            PersonAddressCollection existingPersonAddressCollection = PersonAccess.GetPersonAddress(user, null, address.PersonAddressId, null);
                            PersonAddress existingPersonAddress = existingPersonAddressCollection.Count > 0 ? existingPersonAddressCollection[0] : null;

                            //format the postal/zip code
                            if (address.PostalZipCode != null && address.PostalZipCode.Length > 0)
                                CleanUpPostalZipCode(address);

                            //insert/update address
                            if (AddressAccess.GetAddress(user, address.AddressId).Count > 0)
                                AddressAccess.UpdateAddress(user, address);
                            else
                                AddressAccess.InsertAddress(user, address);

                            //insert/update personaddress
                            if (existingPersonAddress == null)
                                PersonAccess.InsertPersonAddress(user, address);
                            else
                                PersonAccess.UpdatePersonAddress(user, address);

                            //we need to cleanup orphans
                            CleanupOrphanAddress(user, existingPersonAddress);

                            scope.Complete();
                        }
                    }
                    else
                        throw new Exception("Address type is already in use");
                }
                else
                {
                    //check data before allowing updates
                    if (CheckForUniqueAddressType(address, theData))
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                            //format the postal/zip code
                            if (address.PostalZipCode != null && address.PostalZipCode.Length > 0)
                                CleanUpPostalZipCode(address);

                            AddressAccess.UpdateAddress(user, address);
                            PersonAccess.UpdatePersonAddress(user, address);

                            scope.Complete();
                        }
                    }
                    else
                        throw new Exception("Address type is already in use");
                }

                //format postal/zip code for GUI viewing
                FormatPostalZipCode(address);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - UpdatePersonAddress", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        //format postal/zip code for GUI viewing
        public void FormatPostalZipCode(Address address)
        {
            if (address.PostalZipCode != null && address.PostalZipCode != "")
            {
                if (address.CountryCode == "CA" && address.PostalZipCode.Length == 6)
                    address.PostalZipCode = String.Format("{0} {1}", address.PostalZipCode.Substring(0, 3), address.PostalZipCode.Substring(3, 3));
                else if (address.CountryCode == "US" && address.PostalZipCode.Length > 8)
                    address.PostalZipCode = String.Format("{0}-{1}", address.PostalZipCode.Substring(0, 5), address.PostalZipCode.Substring(5, 4));
                else
                    address.PostalZipCode = address.PostalZipCode.ToUpper();
            }
        }
        public void CleanUpPostalZipCode(Address address)
        {
            if (address.CountryCode == "CA")
            {
                address.PostalZipCode = address.PostalZipCode.Replace(" ", "");
                address.PostalZipCode = address.PostalZipCode.ToUpper();
            }
            else if (address.CountryCode == "US" && address.PostalZipCode.Length > 5)
            {
                address.PostalZipCode = address.PostalZipCode.Replace(" ", "");
                address.PostalZipCode = address.PostalZipCode.Replace("-", "");
            }
            else
                address.PostalZipCode = address.PostalZipCode.ToUpper();
        }
        private void CleanupOrphanAddress(DatabaseUser user, Address address)
        {
            if (address != null)
            {
                if (PersonAccess.GetPersonAddress(user, null, null, address.AddressId).Count == 0)
                    AddressAccess.DeleteAddress(user, address);
            }
        }
        public void DeletePersonAddress(DatabaseUser user, PersonAddress address)
        {
            try
            {
                PersonAccess.DeletePersonAddress(user, address);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - DeletePersonAddress", ex);
                throw;
            }
        }
        public void DeletePersonAddressCleanupOrphans(DatabaseUser user, PersonAddress address)
        {
            try
            {
                PersonAccess.DeletePersonAddress(user, address);

                //we need to cleanup orphans
                CleanupOrphanAddress(user, address);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - DeletePersonAddressCleanupOrphans", ex);
                throw;
            }
        }
        public void InsertPersonAddress(DatabaseUser user, PersonAddress address)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //clean the postal/zip code for db insert
                    if (address.PostalZipCode != null && address.PostalZipCode.Length > 0)
                        CleanUpPostalZipCode(address);

                    AddressAccess.InsertAddress(user, address);
                    PersonAccess.InsertPersonAddress(user, address);

                    scope.Complete();

                    //format postal/zip code for GUI viewing
                    FormatPostalZipCode(address);
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - InsertPersonAddressOneArg", ex);
                throw;
            }
        }
        public void InsertPersonAddress(DatabaseUser user, PersonAddress address, List<PersonAddress> theData)
        {
            try
            {
                //check data before allowing updates
                if (CheckForUniqueAddressType(address, theData))
                    InsertPersonAddress(user, address);
                else
                    throw new Exception("Address type is already in use");
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - InsertPersonAddress2Args", ex);
                throw;
            }
        }
        #endregion

        #region address import
        private Dictionary<String, WizardCache> AddressGetWizardCache(DatabaseUser user, WorkLinksPersonAddress[] xsdObject)
        {
            List<String> importExternalIdentifiers = new List<String>();

            foreach (WorkLinksPersonAddress address in xsdObject)
                importExternalIdentifiers.Add(address.ImportExternalIdentifier);

            return WizardManagement.GetWizardCache(user, importExternalIdentifiers);
        }
        private String AddressDecodeCode(DatabaseUser user, WorkLinksPersonAddress address)
        {
            String validationMessage = null;
            StringBuilder output = new StringBuilder();

            try
            {
                address.CountryCode = CodeManagement.DecodeCode(user, CodeTableType.CountryCode, address.CountryCodeExternalIdentifier);
            }
            catch (FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.CodeMismatch)
                    output.AppendLine(ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            try
            {
                address.ProvinceStateCode = CodeManagement.DecodeCode(user, CodeTableType.ProvinceStateCode, address.ProvinceStateCodeExternalIdentifier);
            }
            catch (FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.CodeMismatch)
                    output.AppendLine(ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            try
            {
                address.PersonAddressTypeCode = CodeManagement.DecodeCode(user, CodeTableType.PersonAddressTypeCode, address.PersonAddressTypeCodeExternalIdentifier);
            }
            catch (FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.CodeMismatch)
                    output.AppendLine(ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (output.Length > 0)
                validationMessage += output.ToString();

            return validationMessage;
        }
        public void ProcessAddress(DatabaseUser user, WorkLinksPersonAddress[] xsdObject, ImportExportLog log, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions)
        {
            Dictionary<String, Employee> employeeCache = EmployeeManagement.GetEmployeeCache(user, xsdObject);
            ProcessAddress(user, employeeCache, xsdObject, log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);
        }
        public void ProcessAddress(DatabaseUser user, Dictionary<String, Employee> employeeCache, WorkLinksPersonAddress[] xsdObject, ImportExportLog log, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions)
        {
            try
            {
                StringBuilder output = new StringBuilder();
                int failedRows = 0;
                int successfulRows = 0;

                Dictionary<String, WizardCache> wizardCache = AddressGetWizardCache(user, xsdObject);

                using (TransactionScope scope = new TransactionScope())
                {
                    int i = 0;

                    WorkLinksPersonAddressCollection existingEmployeeAddresses = new WorkLinksPersonAddressCollection();
                    WorkLinksPersonAddressCollection pendingEmployeeAddresses = new WorkLinksPersonAddressCollection();

                    foreach (WorkLinksPersonAddress address in xsdObject)
                    {
                        String decodeCodeValidationMessage = AddressDecodeCode(user, address);

                        if (address.PersonAddressTypeCode == null) //if the type is missing, default it to "HOME"
                            address.PersonAddressTypeCode = "HOME";

                        if (address.MeetsAddressRequirements(decodeCodeValidationMessage))
                        {
                            if (employeeCache.ContainsKey(address.ImportExternalIdentifier)) //employee was found in the database
                            {
                                successfulRows++;

                                address.AddressId = i--;
                                address.PersonId = employeeCache[address.ImportExternalIdentifier].PersonId;
                                existingEmployeeAddresses.Add(address);
                            }
                            else
                            {
                                if (wizardCache.ContainsKey(address.ImportExternalIdentifier)) //employee is pending
                                {
                                    successfulRows++;

                                    address.AddressId = i--;
                                    pendingEmployeeAddresses.Add(address);
                                }
                                else //employee is missing
                                {
                                    failedRows++;
                                    output.Append(String.Format(GuiManagement.GetResourceByLanguageAndKey(user, "ErrorMessages", "MissingEmployee"), address.ImportExternalIdentifier) + "\r\n");
                                }
                            }
                        }
                        else
                        {
                            failedRows++;
                            output.Append(address.GetValidationMessage(decodeCodeValidationMessage));
                        }
                    }

                    ImportAddress(user, existingEmployeeAddresses, pendingEmployeeAddresses, autoGenerateEmployeeNumber, employeeNumberFormat, autoAddSecondaryPositions);

                    if (importDirtySave || failedRows == 0)
                        scope.Complete();
                }

                output.AppendLine("----------");
                output.AppendLine("# rows total: " + (successfulRows + failedRows));
                output.AppendLine("# rows succeeded: " + successfulRows);
                output.AppendLine("# rows failed: " + failedRows);

                if (failedRows > 0)
                {
                    if (importDirtySave)
                        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n" + "----------\r\n");
                    else
                        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportCompleteProcessFailed") + "\r\n" + "----------\r\n");
                }
                else
                    output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n");

                log.SuccessFlag = true;
                log.ProcessingOutput += output.ToString();
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - ProcessAddress", ex);
                throw;
            }
        }
        private void ImportAddress(DatabaseUser user, WorkLinksPersonAddressCollection existingEmployeeAddresses, WorkLinksPersonAddressCollection pendingEmployeeAddresses, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool autoAddSecondaryPositions)
        {
            //insert, update or delete existing addresses
            if (existingEmployeeAddresses.Count > 0)
                PersonAccess.ImportAddress(user, existingEmployeeAddresses);

            //update pending employee bank accounts or insert new bank accounts
            if (pendingEmployeeAddresses.Count > 0)
            {
                WizardCacheImportCollection collection = new WizardCacheImportCollection();
                int i = 0;

                foreach (WorkLinksPersonAddress address in pendingEmployeeAddresses)
                {
                    collection.Add(new WizardCacheImport
                    {
                        WizardCacheId = i--,
                        WizardId = 1, //1 = Add Employee Wizard
                        ImportExternalIdentifier = address.ImportExternalIdentifier,
                        WizardItemId = 2, //2 = AddressDetailControl
                        Data = address.PersonAddressCollection
                    });
                }

                WizardManagement.ImportWizardCache(user, collection);
                WizardManagement.ValidateAndHireEmployee(user, collection, autoGenerateEmployeeNumber, employeeNumberFormat, autoAddSecondaryPositions);
            }
        }
        #endregion

        #region person ContactChannel
        public void ProcessPhoneImportViaService(String database, String username, PhoneImport[] array)
        {
            List<PhoneImport> phonelist = new List<PhoneImport>();

            for (int i = 0; i < array.Length; i++)
            {
                //add to list
                phonelist.Add(array[i]);

                if ((i + 1) % 5000 == 0 || i == array.Length - 1)
                {
                    //store in db 
                    PersonAccess.ImportPhone(database, username, phonelist);

                    //clear the list
                    phonelist.Clear();
                }
            }
        }
        public PersonContactChannelCollection GetPersonContactChannel(DatabaseUser user, long id, String contactChannelCategoryCode)
        {
            try
            {
                PersonContactChannelCollection collection = PersonAccess.GetPersonContactChannel(user, id, contactChannelCategoryCode, null);

                foreach (PersonContactChannel channel in collection)
                    PersonAccess.GetContactChannel(user, channel.ContactChannelId)[0].CopyTo(channel);

                return collection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - GetPersonContactChannel", ex);
                throw;
            }
        }
        private bool CheckForUniqueAddressType(PersonAddress pa, List<PersonAddress> listPa)
        {
            //person must have unique ADDRESS type
            bool bContinueUpdate = true;

            foreach (PersonAddress idi in listPa)
            {
                if (idi.Key != pa.Key)
                {
                    if (idi.PersonAddressTypeCode == pa.PersonAddressTypeCode)
                    {
                        bContinueUpdate = false;
                        break;
                    }
                }
            }

            return bContinueUpdate;
        }
        private bool CheckForUniqueEmailPhoneType(PersonContactChannel pcc, List<PersonContactChannel> listPcc)
        {
            //person must have unique EMAIL and PHONE type
            bool bContinueUpdate = true;

            foreach (PersonContactChannel idi in listPcc)
            {
                if (idi.Key != pcc.Key)
                {
                    if (idi.ContactChannelTypeCode == pcc.ContactChannelTypeCode)
                    {
                        bContinueUpdate = false;
                        break;
                    }
                }
            }

            return bContinueUpdate;
        }
        public void UpdateContactChannel(DatabaseUser user, ContactChannel channel)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    PersonAccess.UpdateContactChannel(user, channel);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - UpdateContactChannel", ex);
                throw;
            }
        }
        public void UpdatePersonContactChannel(DatabaseUser user, PersonContactChannel channel, List<PersonContactChannel> theData)
        {
            try
            {
                if (CheckForUniqueEmailPhoneType(channel, theData))
                {
                    using (TransactionScope scope = new TransactionScope())
                    {
                        //perform updates
                        PersonAccess.UpdateContactChannel(user, channel);
                        PersonAccess.UpdatePersonContactChannel(user, channel);

                        scope.Complete();
                    }
                }
                else
                    throw new Exception("Email/Phone type is already in use");
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - UpdatePersonContactChannel", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void InsertPersonContactChannel(DatabaseUser user, PersonContactChannel channel)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    /*if contactchannel does not exist, add it*/
                    if (channel.ContactChannelId < 0)
                        PersonAccess.InsertContactChannel(user, channel);

                    PersonAccess.InsertPersonContactChannel(user, channel);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - InsertPersonContactChannel", ex);
                throw;
            }
        }
        public void InsertPersonContactChannel(DatabaseUser user, PersonContactChannel channel, List<PersonContactChannel> theData)
        {
            try
            {
                if (CheckForUniqueEmailPhoneType(channel, theData))
                    InsertPersonContactChannel(user, channel);
                else
                    throw new Exception("Email/Phone type is already in use");
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - InsertPersonContactChannel", ex);
                throw;
            }
        }
        public void DeletePersonContactChannel(DatabaseUser user, PersonContactChannel channel)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    PersonAccess.DeletePersonContactChannel(user, channel);
                    CleanupOrphanContactChannel(user, channel);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - DeletePersonContactChannel", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        private void CleanupOrphanContactChannel(DatabaseUser user, ContactChannel channel)
        {
            if (channel != null)
            {
                if (PersonAccess.GetPersonContactChannel(user, null, null, channel.ContactChannelId).Count == 0)
                    PersonAccess.DeleteContactChannel(user, channel);
            }
        }
        #endregion

        #region Contact Channel
        public ContactChannelCollection GetContactChannel(DatabaseUser user, long? contactChannelId)
        {
            try
            {
                return PersonAccess.GetContactChannel(user, contactChannelId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - GetContactChannel", ex);
                throw;
            }
        }
        #endregion

        #region government identification number type template
        public GovernmentIdentificationNumberTypeTemplateCollection GetGovernmentIdentificationNumberTypeTemplate(DatabaseUser user, String numberTypeCode)
        {
            try
            {
                return PersonAccess.GetGovernmentIdentificationNumberTypeTemplate(user, numberTypeCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PersonManagement - GetGovernmentIdentificationNumberTypeTemplate", ex);
                throw;
            }
        }
        #endregion
    }
}