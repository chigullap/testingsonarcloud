﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CollectiveAgreementControl.ascx.cs" Inherits="WorkLinks.Admin.Union.CollectiveAgreementControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPGrid
    ID="CollectiveAgreementGrid"
    runat="server"
    AutoGenerateColumns="false"
    GridLines="None"
    AutoAssignModifyProperties="true"
    AutoInsertOnAdd="false"
    OnNeedDataSource="CollectiveAgreementGrid_NeedDataSource"
    OnDeleteCommand="CollectiveAgreementGrid_DeleteCommand"
    OnUpdateCommand="CollectiveAgreementGrid_UpdateCommand"
    OnInsertCommand="CollectiveAgreementGrid_InsertCommand"
    OnItemCommand="CollectiveAgreementGrid_ItemCommand"
    OnItemCreated="CollectiveAgreementGrid_ItemCreated"
    OnItemDataBound="CollectiveAgreementGrid_ItemDataBound">

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">

        <CommandItemTemplate>
            <wasp:WLPToolBar ID="UnionCollectiveAgreemntToolBar" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add"></wasp:WLPToolBarButton>
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <RowIndicatorColumn>
            <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn>
            <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>

        <Columns>
            <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif"></wasp:GridEditCommandControl>
            <wasp:GridBoundControl DataField="EnglishDescription" LabelText="**EnglishDescription" SortExpression="EnglishDescription" UniqueName="EnglishDescription" ResourceName="EnglishDescription">
                <HeaderStyle Width="300px" />
            </wasp:GridBoundControl>
            <wasp:GridBoundControl DataField="FrenchDescription" LabelText="**FrenchDescription" SortExpression="FrenchDescription" UniqueName="FrenchDescription" ResourceName="FrenchDescription">
                <HeaderStyle Width="300px" />
            </wasp:GridBoundControl>
            <wasp:GridDateTimeControl DataField="CollectiveAgreementStartDate" LabelText="**CollectiveAgreementStartDate" SortExpression="CollectiveAgreementStartDate" UniqueName="CollectiveAgreementStartDate" ResourceName="CollectiveAgreementStartDate"></wasp:GridDateTimeControl>
            <wasp:GridDateTimeControl DataField="CollectiveAgreementEndDate" LabelText="**CollectiveAgreementEndDate" SortExpression="CollectiveAgreementEndDate" UniqueName="CollectiveAgreementEndDate" ResourceName="CollectiveAgreementEndDate"></wasp:GridDateTimeControl>
            <telerik:GridTemplateColumn UniqueName="TemplateAttachmentColumn" HeaderText="Attachment">
                <ItemTemplate>
                    <asp:HyperLink ID="AttachmentLink" runat="server" Text="Link" Visible="false"></asp:HyperLink>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>"></wasp:GridButtonControl>
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table width="55%" align="left">
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="EnglishDescription" runat="server" Value='<%# Bind("EnglishDescription") %>' ResourceName="EnglishDescription" Mandatory="true" TabIndex="010"></wasp:TextBoxControl>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="FrenchDescription" runat="server" Value='<%# Bind("FrenchDescription") %>' ResourceName="FrenchDescription" Mandatory="true" TabIndex="020"></wasp:TextBoxControl>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="CollectiveAgreementStartDate" runat="server" Value='<%# Bind("CollectiveAgreementStartDate") %>' ResourceName="CollectiveAgreementStartDate" Mandatory="true" TabIndex="030" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="CollectiveAgreementEndDate" runat="server" Value='<%# Bind("CollectiveAgreementEndDate") %>' ResourceName="CollectiveAgreementEndDate" Mandatory="true" TabIndex="040" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# !IsUpdate %>' ResourceName="Insert" />
                                    </td>
                                    <td>
                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table align="right">
                    <tr>
                        <td>
                            <telerik:RadAsyncUpload runat="server" ID="UploadAttachment" MaxFileInputsCount="1" HideFileInput="true" Localization-Select="Attach" OnClientValidationFailed="OnClientValidationFailed" AllowedFileExtensions="<%# AllowedFileExtensions %>" />
                            <wasp:WLPButton runat="server" ID="RemoveAttachment" ResourceName="RemoveAttachment" OnClick="RemoveAttachment_Click" Visible="false"></wasp:WLPButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl Visible="false" runat="server" ID="Description" ResourceName="Description" Value='<%# Bind("Description") %>' ReadOnly="false" TabIndex="080" />
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>
    </MasterTableView>
</wasp:WLPGrid>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="CollectiveAttachmentWindows"
    Width="800"
    Height="500"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function ShowAttachment(attachmentId) {
            openWindowWithManager('CollectiveAttachmentWindows', String.format('/Attachment/{0}', attachmentId), false);
            return false;
        }

        function OnClientValidationFailed(sender, args) {
            var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
            if (args.get_fileName().lastIndexOf('.') != -1) {
                if (sender.get_allowedFileExtensions().indexOf(fileExtention) == -1) {
                    alert("Invalid file selected.");
                    sender.deleteFileInputAt(0);
                }
            }
        }
    </script>
</telerik:RadScriptBlock>