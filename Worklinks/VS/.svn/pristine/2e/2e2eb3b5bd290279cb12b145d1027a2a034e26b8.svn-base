﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]

    public class LanguageEntity : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties

        //English details

        public override string Key
        {
            get
            {
                return String.Format("{0},{1}", ControlFieldValueId, FrenchControlFieldValueId);
            }
        }

        [DataMember]
        public long ControlFieldValueId {get;set;}
        [DataMember]
        public long ControlFieldId { get; set; }
        [DataMember]
        public String FieldIdentifier { get; set; }
        [DataMember]
        public String CodeLanguageCd { get; set; }
        [DataMember]
        public long SecurityRoleId { get; set; }    //"role_id" from "form_description" table, which is passed into the select procedure that populates this class
        [DataMember]
        public String AttributeIdentifier { get; set; }
        [DataMember]
        public String EnglishFieldValue { get; set; }
        
        //French details
        [DataMember]
        public long FrenchControlFieldValueId { get; set; }
        [DataMember]
        public String FrenchFieldValue { get; set; }
        [DataMember]
        public byte[] FrenchRowVersion { get; set; }


        #endregion

        #region construct/destruct

        public LanguageEntity()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone

        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(LanguageEntity data)
        {
            base.CopyTo(data);

            data.ControlFieldValueId = ControlFieldValueId;
            data.ControlFieldId = ControlFieldId;
            data.FieldIdentifier = FieldIdentifier;
            data.CodeLanguageCd = CodeLanguageCd;
            data.SecurityRoleId = SecurityRoleId;
            data.AttributeIdentifier = AttributeIdentifier;
            data.EnglishFieldValue = EnglishFieldValue;
            data.FrenchControlFieldValueId = FrenchControlFieldValueId;
            data.FrenchFieldValue = FrenchFieldValue;
            data.FrenchRowVersion = FrenchRowVersion;
        }
        public new void Clear()
        {
            base.Clear();

            ControlFieldValueId = -1;
            ControlFieldId = -1;
            FieldIdentifier = "";
            CodeLanguageCd = "";
            SecurityRoleId = -1;
            AttributeIdentifier = "";
            EnglishFieldValue = "";
            FrenchControlFieldValueId = -1;
            FrenchFieldValue = "";
            FrenchRowVersion = new byte[8];
        }

        #endregion
    }
}
