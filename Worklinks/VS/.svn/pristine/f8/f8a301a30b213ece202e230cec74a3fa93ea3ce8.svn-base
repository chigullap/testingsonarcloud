﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PaycodeYearEndReportMap : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long YearEndReportMapId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public decimal EffectiveYear { get; set; }
        [DataMember]
        public string YearEndFormCode { get; set; }
        [DataMember]
        public string PaycodeCode { get; set; }
        [DataMember]
        public string YearEndFormBoxCode { get; set; }
        [DataMember]
        public string CountryCode { get; set; }
        [DataMember]
        public string ProvinceStateCode { get; set; }
        #endregion

        #region construct/destruct
        public PaycodeYearEndReportMap()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            PaycodeYearEndReportMap item = new PaycodeYearEndReportMap();
            CopyTo(item);

            return item;
        }
        public virtual void CopyTo(PaycodeYearEndReportMap data)
        {
            base.CopyTo(data);

            data.YearEndReportMapId = YearEndReportMapId;
            data.EffectiveYear = EffectiveYear;
            data.YearEndFormCode = YearEndFormCode;
            data.PaycodeCode = PaycodeCode;
            data.YearEndFormBoxCode = YearEndFormBoxCode;
            data.CountryCode = CountryCode;
            data.ProvinceStateCode = ProvinceStateCode;
        }
        public new void Clear()
        {
            base.Clear();

            YearEndReportMapId = -1;
            EffectiveYear = 0;
            YearEndFormCode = null;
            PaycodeCode = null;
            YearEndFormBoxCode = null;
            CountryCode = null;
            ProvinceStateCode = null;
        }
        #endregion
    }
}