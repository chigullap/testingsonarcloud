﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerYearEndNR4rspXmlFileToWorklinks : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String YearEndNR4rspId = "year_end_nr4_id";
            public static String Revision = "revision";
            public static String PreviousRevisionYearEndNR4rspId = "previous_revision_year_end_nr4_id";
            public static String EmployerNumber = "employer_number";
            public static String EmployeeId = "employee_id";
            public static String Year = "year";
            public static String Box11 = "box_11";
            public static String Box12 = "box_12";
            public static String PayerAgentIdentificationNumber = "payer_agent_identification_number";
            public static String YearEndNr4PayerAgentId = "year_end_nr4_payer_agent_id";
            public static String Box13 = "box_13";
            public static String Box14 = "box_14";
            public static String Box15 = "box_15";
            public static String Box16Amount = "box_16_amount";
            public static String Box17Amount = "box_17_amount";
            public static String Box18 = "box_18";
            public static String Box24 = "box_24";
            public static String Box25 = "box_25";
            public static String Box26Amount = "box_26_amount";
            public static String Box27Amount = "box_27_amount";
            public static String Box28 = "box_28";
            public static String ActiveFlag = "active_flag";
        }
        #endregion

        #region main
        public YearEndNR4rspCollecion Select(DatabaseUser user, int year, long businessNumberId, bool getOrginals = false, bool getAmended = false)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndNR4rsp_select", user.DatabaseName);

            command.AddParameterWithValue("@year", year);
            command.AddParameterWithValue("@businessNumberId", businessNumberId);
            command.AddParameterWithValue("@getOriginals", getOrginals);
            command.AddParameterWithValue("@getAmended", getAmended);

            using (IDataReader reader = command.ExecuteReader())
                return MapToYearEndNR4RspCollection(reader);
        }
        public YearEndNR4rspCollecion SelectSingleNR4rspByKey(DatabaseUser user, long key)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndNR4rspByKeyId_select", user.DatabaseName);

            command.AddParameterWithValue("@YearEndNR4rspId", key);

            using (IDataReader reader = command.ExecuteReader())
                return MapToYearEndNR4RspCollection(reader);
        }
        public YearEndNR4rsp Insert(DatabaseUser user, YearEndNR4rsp yearEndNR4rspObj)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndNR4rsp_insert", user.DatabaseName);

            using (command)
            {
                command.BeginTransaction("YearEndNR4rsp_insert");

                SqlParameter yearEndNR4rspIdParm = command.AddParameterWithValue("@yearEndNR4rspId", yearEndNR4rspObj.YearEndNR4rspId, ParameterDirection.Output);
                command.AddParameterWithValue("@revision", yearEndNR4rspObj.Revision);
                command.AddParameterWithValue("@previousRevisionYearEndNR4rspId", yearEndNR4rspObj.PreviousRevisionYearEndNR4rspId);
                command.AddParameterWithValue("@employerNumber", yearEndNR4rspObj.EmployerNumber);
                command.AddParameterWithValue("@employeeId", yearEndNR4rspObj.EmployeeId);
                command.AddParameterWithValue("@year", yearEndNR4rspObj.Year);
                command.AddParameterWithValue("@box11", yearEndNR4rspObj.Box11);
                command.AddParameterWithValue("@box12", yearEndNR4rspObj.Box12);
                command.AddParameterWithValue("@payerAgentIdentificationNumber", yearEndNR4rspObj.PayerAgentIdentificationNumber);
                command.AddParameterWithValue("@yearEndNr4PayerAgentId", yearEndNR4rspObj.YearEndNr4PayerAgentId);
                command.AddParameterWithValue("@box13", yearEndNR4rspObj.Box13);
                command.AddParameterWithValue("@box14", yearEndNR4rspObj.Box14);
                command.AddParameterWithValue("@box15", yearEndNR4rspObj.Box15);
                command.AddParameterWithValue("@box16Amount", yearEndNR4rspObj.Box16Amount);
                command.AddParameterWithValue("@box17Amount", yearEndNR4rspObj.Box17Amount);
                command.AddParameterWithValue("@box18", yearEndNR4rspObj.Box18);
                command.AddParameterWithValue("@box24", yearEndNR4rspObj.Box24);
                command.AddParameterWithValue("@box25", yearEndNR4rspObj.Box25);
                command.AddParameterWithValue("@box26Amount", yearEndNR4rspObj.Box26Amount);
                command.AddParameterWithValue("@box27Amount", yearEndNR4rspObj.Box27Amount);
                command.AddParameterWithValue("@box28", yearEndNR4rspObj.Box28);
                command.AddParameterWithValue("@activeFlag", yearEndNR4rspObj.ActiveFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", yearEndNR4rspObj.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                yearEndNR4rspObj.YearEndNR4rspId = Convert.ToInt64(yearEndNR4rspIdParm.Value);
                yearEndNR4rspObj.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return yearEndNR4rspObj;
            }
        }
        public void Update(DatabaseUser user, long keyId, bool activeFlag)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("YearEndNR4rsp_update", user.DatabaseName))
                {
                    command.BeginTransaction("YearEndNR4rsp_update");

                    command.AddParameterWithValue("@keyId", keyId);
                    command.AddParameterWithValue("@activeFlag", activeFlag);

                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, String year)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("YearEndNR4rsp_delete", user.DatabaseName))
                {
                    command.BeginTransaction("YearEndNR4rsp_delete");

                    command.AddParameterWithValue("@year", Convert.ToDecimal(year));

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected YearEndNR4rspCollecion MapToYearEndNR4RspCollection(IDataReader reader)
        {
            YearEndNR4rspCollecion collection = new YearEndNR4rspCollecion();

            while (reader.Read())
                collection.Add(MapToYearEndNR4Rsp(reader));

            return collection;
        }
        protected YearEndNR4rsp MapToYearEndNR4Rsp(IDataReader reader)
        {
            YearEndNR4rsp item = new YearEndNR4rsp();
            base.MapToBase(item, reader);

            item.YearEndNR4rspId = (long)CleanDataValue(reader[ColumnNames.YearEndNR4rspId]);
            item.Revision = (int)CleanDataValue(reader[ColumnNames.Revision]);
            item.PreviousRevisionYearEndNR4rspId = (long?)CleanDataValue(reader[ColumnNames.PreviousRevisionYearEndNR4rspId]);
            item.EmployerNumber = (String)CleanDataValue(reader[ColumnNames.EmployerNumber]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.Year = (Decimal)CleanDataValue(reader[ColumnNames.Year]);
            item.Box11 = (String)CleanDataValue(reader[ColumnNames.Box11]);
            item.Box12 = (String)CleanDataValue(reader[ColumnNames.Box12]);
            item.PayerAgentIdentificationNumber = (String)CleanDataValue(reader[ColumnNames.PayerAgentIdentificationNumber]);
            item.YearEndNr4PayerAgentId = (long?)CleanDataValue(reader[ColumnNames.YearEndNr4PayerAgentId]);
            item.Box13 = (String)CleanDataValue(reader[ColumnNames.Box13]);
            item.Box14 = (String)CleanDataValue(reader[ColumnNames.Box14]);
            item.Box15 = (String)CleanDataValue(reader[ColumnNames.Box15]);
            item.Box16Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box16Amount]);
            item.Box17Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box17Amount]);
            item.Box18 = (String)CleanDataValue(reader[ColumnNames.Box18]);
            item.Box24 = (String)CleanDataValue(reader[ColumnNames.Box24]);
            item.Box25 = (String)CleanDataValue(reader[ColumnNames.Box25]);
            item.Box26Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box26Amount]);
            item.Box27Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box27Amount]);
            item.Box28 = (String)CleanDataValue(reader[ColumnNames.Box28]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);

            return item;
        }
        #endregion
    }
}