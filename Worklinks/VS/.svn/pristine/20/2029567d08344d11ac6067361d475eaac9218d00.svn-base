﻿using System;
using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeProfile : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeProfileId = "employee_profile_id";
            public static String EmployeeId = "employee_id";
            public static String EmployeeNumber = "employee_number";
            public static String LastName = "last_name";
            public static String FirstName = "first_name";
            public static String BirthDate = "birth_date";
            public static String HireDate = "hire_date";
            public static String Status = "status";
            public static String PayrollProcessGroup = "payroll_process_group";
            public static String Job = "job";
            public static String LastLoginDate = "last_login_datetime";
            public static String PhoneNumber = "phone_number";
            public static String EmailAddress = "email_address";
            public static String Address = "address";
        }
        #endregion

        #region select
        internal EmployeeProfileCollection SelectEmployeeProfile(DatabaseUser user)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeProfile_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@userName", user.UserName);
                command.AddParameterWithValue("@languageCode", user.LanguageCode);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToEmployeeProfileCollection(reader);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeProfileCollection MapToEmployeeProfileCollection(IDataReader reader)
        {
            EmployeeProfileCollection collection = new EmployeeProfileCollection();

            while (reader.Read())
                collection.Add(MapToEmployeeProfile(reader));

            return collection;
        }
        protected EmployeeProfile MapToEmployeeProfile(IDataReader reader)
        {
            EmployeeProfile item = new EmployeeProfile();

            item.EmployeeProfileId = (long)CleanDataValue(reader[ColumnNames.EmployeeProfileId]);
            item.EmployeeId = (long?)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EmployeeNumber = (String)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.LastName = (String)CleanDataValue(reader[ColumnNames.LastName]);
            item.FirstName = (String)CleanDataValue(reader[ColumnNames.FirstName]);
            item.BirthDate = (DateTime?)CleanDataValue(reader[ColumnNames.BirthDate]);
            item.HireDate = (DateTime?)CleanDataValue(reader[ColumnNames.HireDate]);
            item.Status = (String)CleanDataValue(reader[ColumnNames.Status]);
            item.PayrollProcessGroup = (String)CleanDataValue(reader[ColumnNames.PayrollProcessGroup]);
            item.Job = (String)CleanDataValue(reader[ColumnNames.Job]);
            item.LastLoginDate = (DateTime?)CleanDataValue(reader[ColumnNames.LastLoginDate]);
            item.PhoneNumber = (String)CleanDataValue(reader[ColumnNames.PhoneNumber]);
            item.EmailAddress = (String)CleanDataValue(reader[ColumnNames.EmailAddress]);
            item.Address = (String)CleanDataValue(reader[ColumnNames.Address]);

            return item;
        }
        #endregion
    }
}