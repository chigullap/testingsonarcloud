﻿using System;
using System.Security.Claims;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.RoleEditor
{
    public partial class RoleSearchControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        protected bool? worklinksAdministrationFlag = false;
        #endregion

        #region properties
        protected String Criteria
        {
            get
            {
                String searchEntry = RoleDescription.TextValue;
                return searchEntry;
            }
            set { RoleDescription.TextValue = value; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.RoleSearch.ViewFlag);

            RoleSummaryGrid.NeedDataSource += new Telerik.Web.UI.GridNeedDataSourceEventHandler(RoleSummaryGrid_NeedDataSource);

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.Equals(String.Format("refresh")))
            {
                long keyId = -1;

                //if a row was selected
                if (RoleSummaryGrid.SelectedItems.Count != 0)
                    keyId = Convert.ToInt64(((Telerik.Web.UI.GridDataItem)RoleSummaryGrid.SelectedItems[0]).GetDataKeyValue(RoleSummaryGrid.MasterTableView.DataKeyNames[0]));

                Search(Criteria);

                if (keyId != -1)
                    RoleSummaryGrid.SelectRowByFirstDataKey(keyId); // Re-select the selected row
            }
        }
        public void Search(String criteria)
        {
            //String key = null;
            if (RoleSummaryGrid.SelectedItems.Count > 0)
            {
                //throw new Exception("not done with this yet");
                Telerik.Web.UI.GridDataItem item = (Telerik.Web.UI.GridDataItem)RoleSummaryGrid.SelectedItems[0];
            }

            //String loggedInUser = ((ClaimsIdentity)System.Web.HttpContext.Current.User.Identity).FindFirst("uname").Value;//yuc
            String loggedInUser = System.Web.HttpContext.Current.User.Identity.Name.ToString();
            SecurityUser user = Common.ServiceWrapper.SecurityClient.GetSecurityUser(loggedInUser)[0];
            worklinksAdministrationFlag = !user.WorklinksAdministrationFlag;

            Data = RoleDescriptionCollection.ConvertCollection(Common.ServiceWrapper.SecurityClient.GetRoleDescriptions(criteria, null, worklinksAdministrationFlag));
            RoleSummaryGrid.Rebind();
        }
        #endregion

        #region event handlers
        void RoleSummaryGrid_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            RoleSummaryGrid.DataSource = Data;
        }
        protected void PopulateCombo(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateComboBoxWithRoleDescription((WLP.Web.UI.Controls.ICodeControl)sender);
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search(Criteria);
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            RoleDescription.TextValue = "";
            Data = null;
            RoleSummaryGrid.DataBind();
        }
        #endregion

        #region security handlers
        protected void AddToolBar_PreRender(object sender, EventArgs e) { ((WLP.Web.UI.Controls.WLPToolBarButton)sender).Visible = ((WLP.Web.UI.Controls.WLPToolBarButton)sender).Visible && Common.Security.RoleForm.RoleEdit.AddFlag; }
        protected void DetailsToolBar_PreRender(object sender, EventArgs e) { ((WLP.Web.UI.Controls.WLPToolBarButton)sender).Visible = ((WLP.Web.UI.Controls.WLPToolBarButton)sender).Visible && Common.Security.RoleForm.RoleEdit.ViewFlag; }
        protected void DataSecurityToolBar_PreRender(object sender, EventArgs e) { ((WLP.Web.UI.Controls.WLPToolBarButton)sender).Visible = ((WLP.Web.UI.Controls.WLPToolBarButton)sender).Visible && Common.Security.RoleForm.RoleSecurityData.ViewFlag; }
        protected void FieldSecurityToolBar_PreRender(object sender, EventArgs e) { ((WLP.Web.UI.Controls.WLPToolBarButton)sender).Visible = ((WLP.Web.UI.Controls.WLPToolBarButton)sender).Visible && Common.Security.RoleForm.RoleSecurityField.ViewFlag; }
        protected void FormSecurityToolBar_PreRender(object sender, EventArgs e) { ((WLP.Web.UI.Controls.WLPToolBarButton)sender).Visible = ((WLP.Web.UI.Controls.WLPToolBarButton)sender).Visible && Common.Security.RoleForm.RoleSecurityForm.ViewFlag; }
        #endregion
    }
}