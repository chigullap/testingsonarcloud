﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeePaycodeIncome.ascx.cs" Inherits="WorkLinks.Payroll.Paycode.EmployeePaycodeIncome" ResourceName="EmployeePaycodeIncome" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<wasp:WLPFormView
    ID="EmployeePaycodeIncomeView"
    runat="server"
    OnDataBound="EmployeePaycodeIncomeView_DataBound"
    OnNeedDataSource="EmployeePaycodeIncomeView_NeedDataSource"
    OnInserting="EmployeePaycodeIncomeView_Inserting"
    OnUpdating="EmployeePaycodeIncomeView_Updating"
    DataKeyNames="Key"
    Width="100%">

    <ItemTemplate>
        <wasp:WLPToolBar ID="EmployeeDetailToolBarItem" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="EmployeeDetailToolBar_ButtonClick">
            <Items>
                <wasp:WLPToolBarButton Visible='<%# UpdateFlag %>' Enabled='<%# !IsGlobalEmployeePaycode %>' Text="Edit" ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="edit" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PaycodeCodeItem" runat="server" Type="PaycodeCode" OnDataBinding="PaycodeCode_NeedDataSource" ResourceName="PaycodeCode" Value='<%# Eval("PaycodeCode") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="CodePaycodeTypeCdItem" runat="server" Type="PaycodeTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="CodePaycodeTypeCd" Value='<%# Eval("PaycodeTypeCode") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="AmountRateItem" runat="server" ResourceName="AmountRate" Value='<%# Eval("AmountRate") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="AmountUnitsItem" runat="server" MaxLength="12" DecimalDigits="2" ResourceName="AmountUnits" Value='<%# Eval("AmountUnits") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="AmountPercentageItem" runat="server" MaxLength="12" DecimalDigits="4" ResourceName="AmountPercentage" Value='<%# Eval("AmountPercentage") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="PayPeriodMinimumItem" runat="server" ResourceName="PayPeriodMinimum" Value='<%# Eval("PayPeriodMinimum") %>' ReadOnly="true" Visible="false" />
                    </td> 
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="ExcludeAmountItem" runat="server" MaxLength="13" DecimalDigits="2" ResourceName="ExcludeAmount" Value='<%# Eval("ExcludeAmount") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="PayPeriodMaximumItem" runat="server" ResourceName="PayPeriodMaximum" Value='<%# Eval("PayPeriodMaximum") %>' ReadOnly="true" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="ExcludePercentageItem" runat="server" MaxLength="12" DecimalDigits="4" ResourceName="ExcludePercentage" Value='<%# Eval("ExcludePercentage") %>' ReadOnly="true" Visible="false" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="MonthlyMaximumItem" runat="server" ResourceName="MonthlyMaximum" Value='<%# Eval("MonthlyMaximum") %>' ReadOnly="true" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="GroupIncomeFactorItem" runat="server" MaxLength="13" DecimalDigits="4" ResourceName="GroupIncomeFactor" Value='<%# Eval("GroupIncomeFactor") %>' ReadOnly="true" Visible="false" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="YearlyMaximumItem" runat="server" ResourceName="YearlyMaximum" Value='<%# Eval("YearlyMaximum") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="RoundUpToItem" runat="server" MaxLength="8" DecimalDigits="0" ResourceName="RoundUpTo" Value='<%# Eval("RoundUpTo") %>' ReadOnly="true" Visible="false" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="RatePerRoundUpToItem" runat="server" MaxLength="17" DecimalDigits="4" ResourceName="RatePerRoundUpTo" Value='<%# Eval("RatePerRoundUpTo") %>' ReadOnly="true" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="OrganizationUnitItem" runat="server" ResourceName="OrganizationUnit" Value='<%# Eval("OrganizationUnitDescription") %>' ReadOnly="true" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="StartDateItem" runat="server" ResourceName="StartDate" Value='<%# Eval("StartDate") %>' ReadOnly="true" Visible="false" />
                    </td>
                    <td>
                        <wasp:DateControl ID="CutoffDateItem" runat="server" ResourceName="CutoffDate" Value='<%# Eval("CutoffDate") %>' ReadOnly="true" Visible="false" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar ID="EmployeeDetailToolBar" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="EmployeeDetailToolBar_ButtonClick">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert" />
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update" />
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CausesValidation="false" CommandName="cancel" ResourceName="Cancel" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:WLPLabel ID="Message" runat="server" CssClass="failureNotification" Text='<%# PaycodeDataValidateMsg %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PaycodeCode" runat="server" Type="PaycodeCode" OnDataBinding="PaycodeCode_DataBinding" ResourceName="PaycodeCode" Value='<%# Bind("PaycodeCode") %>' ReadOnly='<%# IsEditMode || IsViewMode %>' TabIndex="010" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="CodePaycodeTypeCd" runat="server" Type="PaycodeTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="CodePaycodeTypeCd" Value='<%# Bind("PaycodeTypeCode") %>' ReadOnly="true" TabIndex="020" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="AmountRate" runat="server" ResourceName="AmountRate" Value='<%# Bind("AmountRate") %>' TabIndex="030" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="AmountUnits" runat="server" ResourceName="AmountUnits" Value='<%# Bind("AmountUnits") %>' TabIndex="040" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="AmountPercentage" runat="server" MaxLength="12" DecimalDigits="4" ResourceName="AmountPercentage" Value='<%# Bind("AmountPercentage") %>' TabIndex="050" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="PayPeriodMinimum" runat="server" ResourceName="PayPeriodMinimum" Value='<%# Bind("PayPeriodMinimum") %>' TabIndex="060" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="ExcludeAmount" runat="server" MaxLength="13" DecimalDigits="2" ResourceName="ExcludeAmount" Value='<%# Bind("ExcludeAmount") %>' TabIndex="070" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="PayPeriodMaximum" runat="server" ResourceName="PayPeriodMaximum" Value='<%# Bind("PayPeriodMaximum") %>' TabIndex="080" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="ExcludePercentage" runat="server" MaxLength="12" DecimalDigits="4" ResourceName="ExcludePercentage" Value='<%# Bind("ExcludePercentage") %>' TabIndex="090" Visible="false" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="MonthlyMaximum" runat="server" ResourceName="MonthlyMaximum" Value='<%# Bind("MonthlyMaximum") %>' TabIndex="100" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="GroupIncomeFactor" runat="server" MaxLength="13" DecimalDigits="4" ResourceName="GroupIncomeFactor" Value='<%# Bind("GroupIncomeFactor") %>' TabIndex="110" Visible="false" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="YearlyMaximum" runat="server" ResourceName="YearlyMaximum" Value='<%# Bind("YearlyMaximum") %>' TabIndex="120" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="RoundUpTo" runat="server" MaxLength="8" DecimalDigits="0" ResourceName="RoundUpTo" Value='<%# Bind("RoundUpTo") %>' TabIndex="130" Visible="false" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="RatePerRoundUpTo" runat="server" MaxLength="17" DecimalDigits="4" ResourceName="RatePerRoundUpTo" Value='<%# Bind("RatePerRoundUpTo") %>' TabIndex="140" Visible="false" />
                    </td>
                </tr>
               <%-- <tr>
                    <td>
                        <div class="form_control">
                            <span class="label">
                                <wasp:WLPLabel ID="OrganizationUnitLabel" runat="server" Text="**OrganizationUnit**" ResourceName="OrganizationUnit" />
                            </span>
                            <span class="field">
                                <telerik:RadDropDownTree ID="OrganizationUnitDropDownTree" ClientIDMode="Static" runat="server" DataTextField="Description" DataFieldID="OrganizationUnitId" DataFieldParentID="ParentOrganizationUnitId" TextMode="FullPath" DataValueField="OrganizationUnitId" OnClientEntryAdding="OnClientEntryAdding" OnClientLoad="onClientLoad" OnClientClearButtonClicking="onClientClearButtonClicking">         
                                    <DropDownSettings Height="200px" Width="340px" CloseDropDownOnSelection="true" />
                                    <ButtonSettings ShowClear="true" />
                                </telerik:RadDropDownTree>
                                <asp:HiddenField ID="OrganizationUnitDropDownValues" ClientIDMode="Static" runat="server" Value="" />
                            </span>
                        </div>
                    </td>
                </tr>--%>
                <tr>
                    <td>
                        <wasp:DateControl ID="StartDate" runat="server" ResourceName="StartDate" Value='<%# Bind("StartDate") %>' Visible="false" TabIndex="050" />
                    </td>
                    <td>
                        <wasp:DateControl ID="CutoffDate" runat="server" ResourceName="CutoffDate" Value='<%# Bind("CutoffDate") %>' Visible="false" TabIndex="060" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>

</wasp:WLPFormView>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function OnClientEntryAdding(sender, args) {
            var node = args.get_node();

            //get count of child nodes of the selected node, if it has children, don't allow it to be selected
            if (node.get_allNodes().length > 0) {
                args.set_cancel(true);
            }
            else {
                var parentNode = node;
                //get id of the node selected
                var ids = [node.get_value()];

                while (parentNode.get_level() > 0) {
                    parentNode = parentNode.get_parent();
                    //get parent node id 
                    ids.push(parentNode.get_value());
                }

                var hiddenOrgControl = document.getElementById('OrganizationUnitDropDownValues');
                //reverse the array so it's parent->child order
                hiddenOrgControl.value = ids.reverse();
                hiddenOrgControl.value = '/' + hiddenOrgControl.value.replace(/,/g, '/');
            }
        }

        function onClientLoad(sender, args) {
            var hiddenOrgControl = document.getElementById('OrganizationUnitDropDownValues');
            var orgUnits = hiddenOrgControl.value.split("/");
            var currentNode = sender.get_embeddedTree();
            var orgUnitPathTextDescription = "";

            currentNode = currentNode.findNodeByValue(orgUnits[0]);

            if (currentNode != null) {
                currentNode.expand();
                currentNode.select();
                //get text of parent node
                orgUnitPathTextDescription = currentNode.get_text();

                for (var i = 1; i < orgUnits.length; i++) {
                    for (var j = 0; j < currentNode._children.get_count(); j++) {
                        if (currentNode._children._array[j].get_value() == orgUnits[i]) {
                            currentNode = currentNode._children._array[j];
                            currentNode.expand();
                            currentNode.select();
                            //get text of child node
                            orgUnitPathTextDescription += '/' + currentNode.get_text();

                            break;
                        }
                    }
                }
            }

            //set the path as the default message for the RadDropDownTree
            if (orgUnitPathTextDescription != null)
                sender._defaultMessage = orgUnitPathTextDescription;
        }

        function onClientClearButtonClicking(sender, args) {
            //clear the default message
            sender._defaultMessage = '<%= DropDownTreeDefaultMessage %>';

            var hiddenOrgControl = document.getElementById('OrganizationUnitDropDownValues');
            var orgUnits = hiddenOrgControl.value.split("/");
            var currentNode = sender.get_embeddedTree();
            currentNode = currentNode.findNodeByValue(orgUnits[0]);

            //collapse all nodes in the path that were previously expanded
            if (currentNode != null) {
                currentNode.collapse();

                for (var i = 1; i < orgUnits.length; i++) {
                    for (var j = 0; j < currentNode._children.get_count(); j++) {
                        if (currentNode._children._array[j].get_value() == orgUnits[i]) {
                            currentNode = currentNode._children._array[j];
                            currentNode.collapse();
                            break;
                        }
                    }
                }
            }

            //set org units to nothing
            hiddenOrgControl.value = null;
        }
    </script>
</telerik:RadScriptBlock>