﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace WorkLinks.BusinessLayer.BusinessObjects.Xsd
{
    [Serializable]
    [DataContract]
    public class WorkLinksYearEndAdjustmentPaycode : YearEndAdjustmentPaycode, IEmployeeIdentifier
    {
        #region properties
        [DataMember]
        [Required]
        public String ImportExternalIdentifier { get; set; }

        public String EmployeeImportExternalIdentifier
        {
            get { return ImportExternalIdentifier; }
            set { ImportExternalIdentifier = value; }
        }

        [DataMember]
        public String EmployerExternalIdentifier { get; set; }

        [DataMember]
        public String ProvinceStateCodeExternalIdentifier { get; set; }

        [DataMember]
        public String PaycodeCodeExternalIdentifier { get; set; }

        #region have to have these if using IEmployerIdentifier
        public long EmployeeId { get; set; }

        public String PayrollProcessGroupCode { get; set; }

        public bool HasErrors { get; set; }
        #endregion

        #endregion

        public String ErrorMessage(Decimal year)
        {
            StringBuilder rtn = new StringBuilder();

            if (!this.IsValid)
                rtn.AppendLine(FormatValidationMessage(false, this.GetValidationInformation()));
            
            if (this.Year != year)
                rtn.AppendLine(FormatValidationMessage(false, "Imported year doesn't match current year end."));
            
            if (this.ProvinceStateCode.Trim().Equals(String.Empty))
                rtn.AppendLine(FormatValidationMessage(false, "Must have a province."));

            if(string.IsNullOrWhiteSpace(this.PaycodeCode))
                rtn.AppendLine(FormatValidationMessage(false, "Must have a paycode."));

            return rtn.Length > 0 ? rtn.ToString() : null;
        }

        private String FormatValidationMessage(bool warningFlag, String message)
        {
            return String.Format("{0}:::Year:{1},BusinessNumber:{2},Province:{3},Employee:{4},Amount:{5},Paycode:{6},Message:{7}",
                warningFlag ? "Warning" : "Error - import failure", this.Year, this.EmployerExternalIdentifier, this.ProvinceStateCodeExternalIdentifier, this.EmployeeImportExternalIdentifier, this.Amount, this.PaycodeCodeExternalIdentifier, message);
        }


    }
}