﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerAccrualEntitlementDetailPaycode : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String AccrualEntitlementDetailPaycodeId = "accrual_entitlement_detail_paycode_id";
            public static String AccrualEntitlementDetailId = "accrual_entitlement_detail_id";
            public static String PaycodeCode = "code_paycode_cd";
        }
        #endregion

        #region main
        internal AccrualEntitlementDetailPaycodeCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long entitlementDetailId)
        {
            DataBaseCommand command = GetStoredProcCommand("AccrualEntitlementDetailPaycode_select", user.DatabaseName);

            command.AddParameterWithValue("@accrualEntitlementDetailId", entitlementDetailId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToAccrualEntitlementDetailPaycodeCollection(reader);
        }
        public AccrualEntitlementDetailPaycode Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetailPaycode detailPaycode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("AccrualEntitlementDetailPaycode_insert", user.DatabaseName))
            {
                command.BeginTransaction("AccrualEntitlementDetailPaycode_insert");

                SqlParameter detailPaycodeIdParam = command.AddParameterWithValue("@accrualEntitlementDetailPaycodeId", detailPaycode.AccrualEntitlementDetailPaycodeId, ParameterDirection.Output);
                command.AddParameterWithValue("@accrualEntitlementDetailId", detailPaycode.AccrualEntitlementDetailId);
                command.AddParameterWithValue("@paycodeCode", detailPaycode.PaycodeCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", detailPaycode.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                detailPaycode.AccrualEntitlementDetailPaycodeId = Convert.ToInt64(detailPaycodeIdParam.Value);
                detailPaycode.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return detailPaycode;
            }
        }
        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetailPaycode detailPaycode)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("AccrualEntitlementDetailPaycode_update", user.DatabaseName))
                {
                    command.BeginTransaction("AccrualEntitlementDetailPaycode_update");

                    command.AddParameterWithValue("@accrualEntitlementDetailPaycodeId", detailPaycode.AccrualEntitlementDetailPaycodeId);
                    command.AddParameterWithValue("@accrualEntitlementDetailId", detailPaycode.AccrualEntitlementDetailId);
                    command.AddParameterWithValue("@paycodeCode", detailPaycode.PaycodeCode);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", detailPaycode.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    detailPaycode.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long entitlementDetailId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("AccrualEntitlementDetailPaycode_delete", user.DatabaseName))
                {
                    command.BeginTransaction("AccrualEntitlementDetailPaycode_delete");

                    command.AddParameterWithValue("@accrualEntitlementDetailId", entitlementDetailId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected AccrualEntitlementDetailPaycodeCollection MapToAccrualEntitlementDetailPaycodeCollection(IDataReader reader)
        {
            AccrualEntitlementDetailPaycodeCollection collection = new AccrualEntitlementDetailPaycodeCollection();

            while (reader.Read())
                collection.Add(MapToAccrualEntitlementDetailPaycode(reader));

            return collection;
        }
        protected AccrualEntitlementDetailPaycode MapToAccrualEntitlementDetailPaycode(IDataReader reader)
        {
            AccrualEntitlementDetailPaycode item = new AccrualEntitlementDetailPaycode();
            base.MapToBase(item, reader);

            item.AccrualEntitlementDetailPaycodeId = (long)CleanDataValue(reader[ColumnNames.AccrualEntitlementDetailPaycodeId]);
            item.AccrualEntitlementDetailId = (long)CleanDataValue(reader[ColumnNames.AccrualEntitlementDetailId]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);

            return item;
        }
        #endregion
    }
}