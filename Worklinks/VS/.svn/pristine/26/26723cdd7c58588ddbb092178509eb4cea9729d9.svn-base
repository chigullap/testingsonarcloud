﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerSecurityLabelRole : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String SecurityLabelRoleId = "security_label_role_id";
            public static String SecurityRoleId = "security_role_id";
            public static String LabelPath = "label_path";
        }
        #endregion

        #region main
        internal SecurityLabelRoleCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long securityRoleId)
        {
            DataBaseCommand command = GetStoredProcCommand("SecurityLabelRole_select", user.DatabaseName);
            command.AddParameterWithValue("@securityRoleId", securityRoleId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToSecurityLabelRoleCollection(reader);
            }
        }

        public SecurityLabelRole Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, SecurityLabelRole item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SecurityLabelRole_insert", user.DatabaseName))
            {
                command.BeginTransaction("SecurityLabelRole_insert");

                SqlParameter SecurityLabelRoleIdParm = command.AddParameterWithValue("securityLabelRoleId", item.SecurityLabelRoleId, ParameterDirection.Output);
                command.AddParameterWithValue("@securityRoleId", item.SecurityRoleId);
                command.AddParameterWithValue("@labelPath", item.LabelPath);


                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                item.SecurityLabelRoleId = Convert.ToInt64(SecurityLabelRoleIdParm.Value);
                command.CommitTransaction();

                return item;
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long roleId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SecurityLabelRole_delete", user.DatabaseName))
                {
                    command.BeginTransaction("SecurityLabelRole_delete");
                    command.AddParameterWithValue("@securityRoleId", roleId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected SecurityLabelRoleCollection MapToSecurityLabelRoleCollection(IDataReader reader)
        {
            SecurityLabelRoleCollection collection = new SecurityLabelRoleCollection();
            while (reader.Read())
            {
                collection.Add(MapToSecurityLabelRole(reader));
            }

            return collection;
        }
        protected SecurityLabelRole MapToSecurityLabelRole(IDataReader reader)
        {
            SecurityLabelRole item = new SecurityLabelRole();
            base.MapToBase(item, reader);

            item.SecurityLabelRoleId = (long)CleanDataValue(reader[ColumnNames.SecurityLabelRoleId]);
            item.SecurityRoleId = (long)CleanDataValue(reader[ColumnNames.SecurityRoleId]);
            item.LabelPath = (String)CleanDataValue(reader[ColumnNames.LabelPath]);

            return item;
        }
        #endregion
    }
}