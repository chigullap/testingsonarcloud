﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddressListControl.ascx.cs" Inherits="WorkLinks.HumanResources.Employee.Address.AddressListControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPGrid
    ID="PersonAddressGrid"
    runat="server"
    GridLines="None"
    AutoGenerateColumns="false"
    AutoInsertOnAdd="false"
    OnDeleteCommand="PersonAddressGrid_DeleteCommand"
    OnInsertCommand="PersonAddressGrid_InsertCommand"
    OnUpdateCommand="PersonAddressGrid_UpdateCommand"
    OnItemCommand="PersonAddressGrid_ItemCommand">

    <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
        <Selecting AllowRowSelect="false" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
        <SortExpressions>
            <telerik:GridSortExpression FieldName="PrimaryFlag" SortOrder="Descending" />
        </SortExpressions>

        <CommandItemTemplate>
            <wasp:WLPToolBar ID="EmployeeSummaryToolBar" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add" CausesValidation="false" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <Columns>
            <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="PersonAddressTypeCode" Type="PersonAddressTypeCode" ResourceName="PersonAddressTypeCode" />
            <wasp:GridCheckBoxControl DataField="PrimaryFlag" LabelText="PrimaryFlag" SortExpression="PrimaryFlag" UniqueName="PrimaryFlag" ResourceName="PrimaryFlag" />
            <wasp:GridBoundControl DataField="MailingLabel" LabelText="MailingLabel" SortExpression="MailingLabel" UniqueName="MailingLabel" ResourceName="MailingLabel" />
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="CountryCode" Type="CountryCode" ResourceName="CountryCode" />
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:CheckBoxControl ID="PrimaryFlag" runat="server" ResourceName="PrimaryFlag" Value='<%# Bind("PrimaryFlag") %>' ReadOnly="false" TabIndex="010" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="PersonAddressTypeCode" runat="server" Type="PersonAddressTypeCode" OnDataBinding="CodeEdit_NeedDataSource" ResourceName="PersonAddressTypeCode" Value='<%# Bind("PersonAddressTypeCode") %>' TabIndex="020" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="City" runat="server" ResourceName="City" Value='<%# Bind("City") %>' MaxLength="30" ReadOnly="false" TabIndex="060" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="AddressLine1" runat="server" ResourceName="AddressLine1" Value='<%# Bind("AddressLine1") %>' MaxLength="30" ReadOnly="false" TabIndex="030" />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="CountryCode" runat="server" Type="CountryCode" AutoPostback="true" OnDataBinding="CountryCode_NeedDataSource" OnSelectedIndexChanged="CountryCode_SelectedIndexChanged" ResourceName="CountryCode" Value='<%# Bind("CountryCode") %>' TabIndex="070" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="AddressLine2" runat="server" ResourceName="AddressLine2" Value='<%# Bind("AddressLine2") %>' MaxLength="30" ReadOnly="false" TabIndex="040" />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="ProvinceStateCode" runat="server" Type="ProvinceStateCode" ResourceName="ProvinceStateCode" OnSelectedIndexChanged="ProvinceStateCode_SelectedIndexChanged" TabIndex="080" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="PostalZipCode" runat="server" ResourceName="PostalZipCode" Value='<%# Bind("PostalZipCode") %>' ReadOnly="false" TabIndex="090" />
                            <asp:CustomValidator ID="PostalZipCodeValidator" runat="server" ErrorMessage="**Invalid Postal/Zip Code**" ForeColor="Red" OnServerValidate="PostalZipCodeValidator_ServerValidate">* Invalid Postal/Zip Code</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# !IsUpdate %>' ResourceName="Insert" />
                                    </td>
                                    <td>
                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>
    </MasterTableView>

    <HeaderContextMenu EnableAutoScroll="true" />

</wasp:WLPGrid>