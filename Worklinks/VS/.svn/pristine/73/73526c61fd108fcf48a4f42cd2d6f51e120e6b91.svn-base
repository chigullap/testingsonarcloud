<?xml version="1.0" encoding="utf-8"?>
<!--
     The notices below are provided with respect to licensed content incorporated herein:
     Copyright NorthgateArinso 2013. All Rights Reserved. http://www.ngahr.com
-->
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	xmlns:ccts="urn:un:unece:uncefact:documentation:1.1" xmlns:hr="http://www.hr-xml.org/3"
	xmlns="http://www.ngahr.com/ngapexxml/1" targetNamespace="http://www.ngahr.com/ngapexxml/1"
	elementFormDefault="qualified" attributeFormDefault="unqualified">
	<xsd:annotation>
		<xsd:documentation> 
		Name: US.xsd 
		Status: 1.06 Release 
		Date this version: 2014-Jun-3
		</xsd:documentation>
	</xsd:annotation>

	<xsd:include schemaLocation="../Components.xsd"/>

	<xsd:element name="US" type="USType"/>
	<xsd:complexType name="USType">
		<xsd:annotation>
			<xsd:documentation source="http://www.ngahr.com/ngapexxml" xml:lang="en">
				<ccts:DictionaryEntryName> NGA United States </ccts:DictionaryEntryName>
				<ccts:DefinitionText> Local data for United States </ccts:DefinitionText>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="USRTA" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWTA" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USUEState" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHFed" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHState" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="EffectiveDateAttributeGroup"/>
	</xsd:complexType>
	<xsd:element name="USRTA" type="USRTAType"/>
	<xsd:complexType name="USRTAType">
		<xsd:annotation>
			<xsd:documentation source="http://www.ngahr.com/ngapexxml" xml:lang="en">
				<ccts:DictionaryEntryName> NGA United States Resident Tax Area </ccts:DictionaryEntryName>
				<ccts:DefinitionText> Resident Tax Area </ccts:DefinitionText>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="USRTATaxArea" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="EffectiveDateAttributeGroup"/>
	</xsd:complexType>

	<xsd:element name="USWTA" type="USWTAType"/>
	<xsd:complexType name="USWTAType">
		<xsd:annotation>
			<xsd:documentation source="http://www.ngahr.com/ngapexxml" xml:lang="en">
				<ccts:DictionaryEntryName> NGA United States Work Tax Area </ccts:DictionaryEntryName>
				<ccts:DefinitionText> Work Tax Area </ccts:DefinitionText>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="USWTATaxArea" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWTAAllocation" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWTAPredominantTaxArea" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="EffectiveDateAttributeGroup"/>
	</xsd:complexType>

	<xsd:element name="USUEState" type="USUEStateType"/>
	<xsd:complexType name="USUEStateType">
		<xsd:annotation>
			<xsd:documentation source="http://www.ngahr.com/ngapexxml" xml:lang="en">
				<ccts:DictionaryEntryName> NGA United States Unemployment State </ccts:DictionaryEntryName>
				<ccts:DefinitionText> Unemployment State </ccts:DefinitionText>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="USUEStateTaxAuthority" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USUEStateWorksite" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="EffectiveDateAttributeGroup"/>
	</xsd:complexType>

	<xsd:element name="USWHFed" type="USWHFedType"/>
	<xsd:complexType name="USWHFedType">
		<xsd:annotation>
			<xsd:documentation source="http://www.ngahr.com/ngapexxml" xml:lang="en">
				<ccts:DictionaryEntryName> NGA United States Witholding Information </ccts:DictionaryEntryName>
				<ccts:DefinitionText> Witholding Information </ccts:DefinitionText>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="USWHFedTaxAuthority" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHFedFilingStatus" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHFedAllowances" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHFedTaxExemptIndicator" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHFedAdditionalWithholding" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHFedNonResidentTaxCalc" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHFedAlternativeFormula" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHFedLastNameDiffers" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="EffectiveDateAttributeGroup"/>
	</xsd:complexType>

	<xsd:element name="USWHState" type="USWHStateType"/>
	<xsd:complexType name="USWHStateType">
		<xsd:annotation>
			<xsd:documentation source="http://www.ngahr.com/ngapexxml" xml:lang="en">
				<ccts:DictionaryEntryName> NGA United States Witholding Information </ccts:DictionaryEntryName>
				<ccts:DefinitionText> Witholding Information </ccts:DefinitionText>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="USWHStateTaxAuthority" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHStateFilingStatus" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHStateAllowances" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHStateAdditionalAllowance" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHStatePersonalAllowance" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHStateDependentAllowance" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHStateExemptionAmount" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHStateAddExemptionAmount" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHStateTaxExemptIndicator" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHStateAddWithholding" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHStateAdditionalPercent" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHStateNonResidentTaxCalc" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHStateAlternativeFormula" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="USWHStateLastNameDiffers" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="EffectiveDateAttributeGroup"/>
	</xsd:complexType>

</xsd:schema>
