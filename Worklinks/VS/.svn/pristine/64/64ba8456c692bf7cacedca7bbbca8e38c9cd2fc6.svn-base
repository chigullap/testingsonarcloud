﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class GroupDescription : WLP.BusinessLayer.BusinessObjects.SecurityGroup, ISecurityRoleDescription
    {
        #region fields
        [DataMember]
        public long? _securityUserSecurityRoleId = null;
        #endregion

        #region properties
        [DataMember]
        public String EnglishDesc { get; set; }

        [DataMember]
        public String FrenchDesc { get; set; }

        //only used in the User Admin section to determine if a user is assigned to a group or not (when in edit mode and all groups are shown).
        [DataMember]
        public long? SecurityUserSecurityRoleId { get; set; }

        //regular property
        [DataMember]
        public bool IsGroupSelected { get; set; }

        [DataMember]
        public bool WorklinksAdministrationFlag { get; set; }
        #endregion

        public long RoleId { get { return GroupId; } }


        #region construct/destruct
        public GroupDescription()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            GroupDescription group = new GroupDescription();
            this.CopyTo(group);

            return group;
        }

        public void CopyTo(GroupDescription data)
        {
            base.CopyTo(data);

            data.EnglishDesc = EnglishDesc;
            data.FrenchDesc = FrenchDesc;
            data.SecurityUserSecurityRoleId = SecurityUserSecurityRoleId;
            data.WorklinksAdministrationFlag = WorklinksAdministrationFlag;
        }

        public new void Clear()
        {
            base.Clear();

            EnglishDesc = "";
            FrenchDesc = "";
            SecurityUserSecurityRoleId = null;
            WorklinksAdministrationFlag = false;
        }
        #endregion
    }
}