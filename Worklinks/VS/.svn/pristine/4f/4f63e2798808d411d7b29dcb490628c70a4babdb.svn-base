﻿using System;
using System.Security.Claims;
using System.Web.UI;
using Telerik.Web.UI;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Common;

namespace WorkLinks.Admin.FormSecurityEditor
{
    public partial class FormSecurityControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private int RoleId
        {
            get { return Convert.ToInt16(Page.RouteData.Values["roleId"]); }
        }
        public bool IsViewMode
        {
            get { return FormGrid.IsViewMode; }
        }
        public bool UpdateFlag
        {
            get { return Common.Security.RoleForm.RoleSecurityForm.UpdateFlag; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.RoleSecurityForm.ViewFlag);

            WireEvents();

            this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "FormSecurity"));

            if (!IsPostBack)
            {
                LoadFormGridInfo();
                RemoveParentScrollBar();
            }
        }
        protected void WireEvents()
        {
            FormGrid.NeedDataSource += new GridNeedDataSourceEventHandler(FormGrid_NeedDataSource);
            FormGrid.PreRender += new EventHandler(FormGrid_PreRender);
            FormGrid.UpdateAllCommand += new WLP.Web.UI.Controls.WLPGrid.UpdateAllCommandEventHandler(FormGrid_UpdateAllCommand);
        }
        protected void LoadFormGridInfo()
        {
            Data = FormSecurityCollection.ConvertCollection(Common.ServiceWrapper.SecurityClient.GetFormSecurityInfo(RoleId, !GetSecurityUser().WorklinksAdministrationFlag));
        }
        protected SecurityUser GetSecurityUser()
        {
            //String loggedInUser = ((ClaimsIdentity)System.Web.HttpContext.Current.User.Identity).FindFirst("uname").Value;//yuc
            String loggedInUser = System.Web.HttpContext.Current.User.Identity.Name.ToString();
            SecurityUser user = Common.ServiceWrapper.SecurityClient.GetSecurityUser(loggedInUser)[0];

            return user;
        }
        private void RemoveParentScrollBar()
        {
            // Remove the scroll bar for this Control by overriding the RadPane settings in Site.Master
            Control parentPane = FormGrid.Parent.Parent.Parent;

            if (parentPane is RadPane)
                ((RadPane)parentPane).Scrolling = SplitterPaneScrolling.None;
        }
        #endregion

        #region event handlers
        void FormGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            FormGrid.DataSource = Data;
        }
        void FormGrid_PreRender(object sender, EventArgs e)
        {
            foreach (GridDataItem item in FormGrid.MasterTableView.Items)
            {
                System.Web.UI.WebControls.CheckBox cbView = ((System.Web.UI.WebControls.CheckBox)item["ViewFlag"].Controls[0]);
                cbView.Enabled = !IsViewMode;

                System.Web.UI.WebControls.CheckBox cbAdd = ((System.Web.UI.WebControls.CheckBox)item["AddFlag"].Controls[0]);
                cbAdd.Enabled = !IsViewMode;

                System.Web.UI.WebControls.CheckBox cbUpdate = ((System.Web.UI.WebControls.CheckBox)item["UpdateFlag"].Controls[0]);
                cbUpdate.Enabled = !IsViewMode;

                System.Web.UI.WebControls.CheckBox cbDelete = ((System.Web.UI.WebControls.CheckBox)item["DeleteFlag"].Controls[0]);
                cbDelete.Enabled = !IsViewMode;
            }
        }
        void FormGrid_UpdateAllCommand(object sender, WLP.Web.UI.Controls.UpdateAllCommandEventArgs e)
        {
            try
            {
                // Pass the collection to WS, determine if deletes or inserts are needed, then return collection
                FormSecurityCollection collection = new FormSecurityCollection();

                foreach (FormSecurity entity in e.Items)
                    collection.Add(entity);

                Common.ServiceWrapper.SecurityClient.UpdateFormSecurity(ApplicationParameter.AuthDatabaseName, ApplicationParameter.SecurityClientUsedByAngular, collection.ToArray(), RoleId);

                Common.Security.RoleForm.Clear();
                LoadFormGridInfo();
                FormGrid.Rebind();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}