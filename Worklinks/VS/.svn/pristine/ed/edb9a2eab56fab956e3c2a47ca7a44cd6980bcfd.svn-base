﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UnionContactControl.ascx.cs" Inherits="WorkLinks.Admin.Union.UnionContactControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="UnionDetailControl.ascx" TagName="UnionDetailControl" TagPrefix="uc1" %>
<%@ Register Src="../../HumanResources/Employee/Address/AddressDetailControl.ascx" TagName="AddressDetailControl" TagPrefix="uc2" %>
<%@ Register Src="../../HumanResources/Employee/ContactChannel/PersonPhoneControl.ascx" TagName="PersonPhoneControl" TagPrefix="uc3" %>
<%@ Register Src="CollectiveAgreementControl.ascx" TagName="CollectiveAgreementControl" TagPrefix="uc4" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="UnionPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="UnionPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="UnionPanel" runat="server">
    <wasp:WLPToolBar ID="UnionContactToolBar" runat="server" Width="100%" OnButtonClick="UnionContactToolBar_ButtonClick" OnClientButtonClicking="onClientButtonClicking" OnClientLoad="UnionContactToolBar_init">
        <Items>
            <wasp:WLPToolBarButton Text="Add" Visible='<%# AddFlag %>' ImageUrl="~/App_Themes/Default/Add.gif" CommandName="Add" ResourceName="Add"></wasp:WLPToolBarButton>
            <wasp:WLPToolBarButton Text="Delete" Visible='<%# DeleteFlag %>' ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ResourceName="Delete"></wasp:WLPToolBarButton>
        </Items>
    </wasp:WLPToolBar>

    <wasp:WLPGrid
        ID="UnionContactInfoGrid"
        runat="server"
        GridLines="None"
        OnDataBound="UnionContactInfoGrid_DataBound"
        AutoGenerateColumns="false"
        OnSelectedIndexChanged="UnionContactInfoGrid_SelectedIndexChanged"
        Height="193px">

        <ClientSettings EnablePostBackOnRowClick="true" AllowKeyboardNavigation="true">
            <Selecting AllowRowSelect="true" />
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <ClientEvents OnRowClick="OnRowClick" OnGridCreated="onGridCreated" OnCommand="onCommand" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">

            <CommandItemTemplate></CommandItemTemplate>

            <Columns>
                <wasp:GridBoundControl DataField="EnglishDescription" LabelText="**EnglishDescription" SortExpression="EnglishDescription" UniqueName="EnglishDescription" ResourceName="EnglishDescription">
                    <HeaderStyle Width="300px" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="FrenchDescription" LabelText="**FrenchDescription" SortExpression="FrenchDescription" UniqueName="FrenchDescription" ResourceName="FrenchDescription">
                    <HeaderStyle Width="300px" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="LastName" LabelText="**LastName" SortExpression="LastName" UniqueName="LastName" ResourceName="LastName" />
                <wasp:GridBoundControl DataField="FirstName" LabelText="**FirstName" SortExpression="FirstName" UniqueName="FirstName" ResourceName="FirstName" />
                <wasp:GridBoundControl DataField="PhoneNumber" LabelText="**PhoneNumber" SortExpression="PhoneNumber" UniqueName="PhoneNumber" ResourceName="PhoneNumber" />
            </Columns>

        </MasterTableView>

    </wasp:WLPGrid>

    <wasp:WLPTabStrip ID="ContactDetailTabStrip" runat="server" SelectedIndex="0" MultiPageID="ContactDetailMultiPage" Style="margin-bottom: 0">
        <Tabs>
            <wasp:WLPTab Text="Union" PageViewID="UnionPageView" Selected="true" ResourceName="Union" />
            <wasp:WLPTab Text="Address" ResourceName="Address" />
            <wasp:WLPTab Text="Phone" ResourceName="Phone" />
            <wasp:WLPTab Text="Collective Agreement" ResourceName="CollectiveAgreement" OnInit="CollectiveAgreementView_OnInit" />
        </Tabs>
    </wasp:WLPTabStrip>

    <telerik:RadMultiPage ID="ContactDetailMultiPage" runat="server" SelectedIndex="0">
        <telerik:RadPageView ID="UnionPageView" runat="server" Selected="true">
            <uc1:UnionDetailControl ID="UnionDetailControl" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="AddressPageView" runat="server">
            <uc2:AddressDetailControl ID="AddressDetailControl" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="PersonPhoneView" runat="server">
            <uc3:PersonPhoneControl ID="PersonPhoneControl" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="CollectiveAgreementView" runat="server" OnInit="CollectiveAgreementView_OnInit">
            <uc4:CollectiveAgreementControl ID="CollectiveAgreementControl" runat="server" />
        </telerik:RadPageView>
    </telerik:RadMultiPage>
</asp:Panel>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function UnionContactToolBar_init(sender) {
            initializeControls();
        }

        function initializeControls() {
            if ($find('<%= UnionContactToolBar.ClientID %>') != null)
                toolbar = $find('<%= UnionContactToolBar.ClientID %>');

            enableButtons(getSelectedRow() != null);
        }

        function getSelectedRow() {
            var unionContactInfoGrid = $find('<%= UnionContactInfoGrid.ClientID %>');
            if (unionContactInfoGrid == null)
                return null;

            var masterTable = unionContactInfoGrid.get_masterTableView();
            if (masterTable == null)
                return null;

            var selectedRow = masterTable.get_selectedItems();
            if (selectedRow.length == 1)
                return selectedRow[0];
            else
                return null;
        }

        function onGridCreated(sender, eventArgs) {
            var selectedRow = getSelectedRow();
            if (selectedRow != null)
                enableButtons(true);
        }

        function onCommand(sender, args) {
            if (args.get_commandName() == 'Page')
                initializeControls();
        }

        function OnRowClick(sender, eventArgs) {
            enableButtons(true);
        }

        function enableButtons(enable) {
            var deleteButton = toolbar.findButtonByCommandName('Delete');

            if (deleteButton != null)
                deleteButton.set_enabled(enable);
        }
    
        function onClientButtonClicking(sender, args) {
            var comandName = args.get_item().get_commandName();
            if (comandName == "Delete") {
                var message = "<asp:Literal runat="server" Text="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />";
                args.set_cancel(!confirm(message));
            }
        }
    </script>
</telerik:RadScriptBlock>