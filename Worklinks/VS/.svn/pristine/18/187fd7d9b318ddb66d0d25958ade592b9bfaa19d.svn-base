﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.SalaryPlan
{
    public partial class SalaryPlanAddRuleControl : WLP.Web.UI.WLPUserControl
    {
        public bool IsViewMode { get { return AddRuleGrid.IsViewMode; } }
        public bool IsUpdate { get { return AddRuleGrid.IsEditMode; } }
        public bool IsInsert { get { return AddRuleGrid.IsInsertMode; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();

            Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "SalaryPlanAddRuleControl"));

            if (!IsPostBack)
            {
                LoadSalaryPlanGradeRule();
            }
        }

        private void LoadSalaryPlanGradeRule()
        {
            Data = Common.ServiceWrapper.HumanResourcesClient.GetSalaryPlanGradeRules();
        }

        private void WireEvents()
        {
            AddRuleGrid.NeedDataSource += new GridNeedDataSourceEventHandler(AddRuleGrid_NeedDataSource);
            AddRuleGrid.PreRender += new EventHandler(AddRuleGrid_PreRender);
        }

        #region events

        protected void AddRuleGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            AddRuleGrid.DataSource = Data;
        }

        void AddRuleGrid_PreRender(object sender, EventArgs e)
        {
            AddRuleGrid.ClientSettings.EnablePostBackOnRowClick = false;
            AddRuleGrid.ClientSettings.Selecting.AllowRowSelect = true;
        }

        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        #endregion

        #region updates
        protected void AddRuleGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                SalaryPlanGradeRule item = (SalaryPlanGradeRule)e.Item.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.InsertPlanGradeRule(item).CopyTo(item);
                ((DataItemCollection<SalaryPlanGradeRule>)Data).Add(item);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.UniqueIndexFault)
                    ScriptManager.RegisterStartupScript(Page, GetType(), "PopupScript", string.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "UniqueIndex")), true);

                LoadSalaryPlanGradeRule();
                e.Canceled = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected void AddRuleGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                SalaryPlanGradeRule item = (SalaryPlanGradeRule)e.Item.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.UpdatePlanGradeRule(item).CopyTo((SalaryPlanGradeRule)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                else if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.UniqueIndexFault)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "UniqueIndex")), true);

                LoadSalaryPlanGradeRule();
                e.Canceled = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}