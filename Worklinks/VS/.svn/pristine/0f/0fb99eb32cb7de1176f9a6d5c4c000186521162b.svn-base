﻿using System;
using System.ServiceModel;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class AccrualManagement
    {
        #region fields
        DataLayer.DataAccess.AccrualAccess _accrualAccess = null;
        #endregion

        #region properties
        private DataLayer.DataAccess.AccrualAccess AccrualAccess
        {
            get
            {
                if (_accrualAccess == null)
                    _accrualAccess = new DataLayer.DataAccess.AccrualAccess();

                return _accrualAccess;
            }
        }
        #endregion

        #region accrual entitlement search
        public AccrualEntitlementSearchCollection GetAccrualEntitlementReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String criteria)
        {
            try
            {
                return AccrualAccess.GetAccrualEntitlementReport(user, criteria);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - GetAccrualEntitlementReport", ex);
                throw;
            }
        }
        public void DeleteAccrualEntitlementReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long entitlementId)
        {
            try
            {
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    //delete from the accrual entitlement detail table
                    AccrualEntitlementDetailCollection entitlementDetailCollection = GetAccrualEntitlementDetail(user, entitlementId, -1); //-1 gets everything
                    foreach (AccrualEntitlementDetail detail in entitlementDetailCollection)
                        DeleteAccrualEntitlementDetail(user, detail);

                    //delete from the accrual entitlement table
                    AccrualEntitlementCollection entitlementCollection = GetAccrualEntitlement(user, entitlementId);
                    if (entitlementCollection.Count > 0)
                        DeleteAccrualEntitlement(user, entitlementCollection[0]);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - DeleteAccrualEntitlementReport", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region accrual entitlement
        public AccrualEntitlementCollection GetAccrualEntitlement(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? entitlementId)
        {
            try
            {
                return AccrualAccess.GetAccrualEntitlement(user, entitlementId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - GetAccrualEntitlement", ex);
                throw;
            }
        }
        public AccrualEntitlement InsertAccrualEntitlement(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlement entitlement)
        {
            try
            {
                return AccrualAccess.InsertAccrualEntitlement(user, entitlement);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - InsertAccrualEntitlement", ex);
                throw;
            }
        }
        public void UpdateAccrualEntitlement(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlement entitlement)
        {
            try
            {
                AccrualAccess.UpdateAccrualEntitlement(user, entitlement);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - UpdateAccrualEntitlement", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteAccrualEntitlement(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlement entitlement)
        {
            try
            {
                AccrualAccess.DeleteAccrualEntitlement(user, entitlement);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - DeleteAccrualEntitlement", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region accrual entitlement detail
        public AccrualEntitlementDetailCollection GetAccrualEntitlementDetail(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long entitlementId, long entitlementDetailId)
        {
            try
            {
                return AccrualAccess.GetAccrualEntitlementDetail(user, entitlementId, entitlementDetailId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - GetAccrualEntitlementDetail", ex);
                throw;
            }
        }
        public AccrualEntitlementDetail InsertAccrualEntitlementDetail(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetail entitlementDetail)
        {
            try
            {
                return AccrualAccess.InsertAccrualEntitlementDetail(user, entitlementDetail);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - InsertAccrualEntitlementDetail", ex);
                throw;
            }
        }
        public void UpdateAccrualEntitlementDetail(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetail entitlementDetail, AccrualEntitlementDetailValueCollection detailValueCollection, AccrualEntitlementDetailPaycodeCollection detailPaycodeCollection)
        {
            try
            {
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    //insert into or update the accrual entitlement detail table
                    if (entitlementDetail.AccrualEntitlementDetailId > 0)
                        AccrualAccess.UpdateAccrualEntitlementDetail(user, entitlementDetail);
                    else
                        InsertAccrualEntitlementDetail(user, entitlementDetail);

                    //set the AccrualEntitlementDetailId
                    foreach (AccrualEntitlementDetailValue valueCollection in detailValueCollection)
                        valueCollection.AccrualEntitlementDetailId = entitlementDetail.AccrualEntitlementDetailId;

                    foreach (AccrualEntitlementDetailPaycode detailPaycode in detailPaycodeCollection)
                        detailPaycode.AccrualEntitlementDetailId = entitlementDetail.AccrualEntitlementDetailId;

                    //update the accrual entitlement detail value table
                    UpdateAccrualEntitlementDetailValue(user, detailValueCollection);

                    //insert into the accrual entitlement detail paycode table
                    InsertAccrualEntitlementDetailPaycode(user, entitlementDetail.AccrualEntitlementDetailId, detailPaycodeCollection);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - UpdateAccrualEntitlementDetail", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteAccrualEntitlementDetail(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetail entitlementDetail)
        {
            try
            {
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    //delete from the accrual entitlement detail paycode table
                    DeleteAccrualEntitlementDetailPaycode(user, entitlementDetail.AccrualEntitlementDetailId);

                    //delete from the accrual entitlement detail value table
                    AccrualEntitlementDetailValueCollection detailValueCollection = GetAccrualEntitlementDetailValue(user, entitlementDetail.AccrualEntitlementDetailId);
                    foreach (AccrualEntitlementDetailValue detailValue in detailValueCollection)
                        DeleteAccrualEntitlementDetailValue(user, detailValue);

                    //delete from the accrual entitlement table
                    AccrualAccess.DeleteAccrualEntitlementDetail(user, entitlementDetail);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - DeleteAccrualEntitlementDetail", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region accrual entitlement detail paycode
        public AccrualEntitlementDetailPaycodeCollection GetAccrualEntitlementDetailPaycode(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long entitlementDetailId)
        {
            try
            {
                return AccrualAccess.GetAccrualEntitlementDetailPaycode(user, entitlementDetailId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - GetAccrualEntitlementDetailPaycode", ex);
                throw;
            }
        }
        public AccrualEntitlementDetailPaycodeCollection InsertAccrualEntitlementDetailPaycode(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long entitlementDetailId, AccrualEntitlementDetailPaycodeCollection detailPaycodeCollection)
        {
            try
            {
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    //delete from the accrual entitlement detail paycode table
                    DeleteAccrualEntitlementDetailPaycode(user, entitlementDetailId);

                    //insert into the accrual entitlement detail paycode table
                    foreach (AccrualEntitlementDetailPaycode detailPaycode in detailPaycodeCollection)
                        AccrualAccess.InsertAccrualEntitlementDetailPaycode(user, detailPaycode);

                    scope.Complete();
                }

                return detailPaycodeCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - InsertAccrualEntitlementDetailPaycode", ex);
                throw;
            }
        }
        public void DeleteAccrualEntitlementDetailPaycode(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long entitlementDetailId)
        {
            try
            {
                AccrualAccess.DeleteAccrualEntitlementDetailPaycode(user, entitlementDetailId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - DeleteAccrualEntitlementDetailPaycode", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region accrual entitlement detail value
        public AccrualEntitlementDetailValueCollection GetAccrualEntitlementDetailValue(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long entitlementDetailId)
        {
            try
            {
                return AccrualAccess.GetAccrualEntitlementDetailValue(user, entitlementDetailId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - GetAccrualEntitlementDetailValue", ex);
                throw;
            }
        }
        public AccrualEntitlementDetailValue InsertAccrualEntitlementDetailValue(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetailValue detailValue)
        {
            try
            {
                return AccrualAccess.InsertAccrualEntitlementDetailValue(user, detailValue);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - InsertAccrualEntitlementDetailValue", ex);
                throw;
            }
        }
        public AccrualEntitlementDetailValueCollection UpdateAccrualEntitlementDetailValue(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetailValueCollection detailValueCollection)
        {
            try
            {
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    foreach (AccrualEntitlementDetailValue detailValue in detailValueCollection)
                    {
                        if (detailValue.AccrualEntitlementDetailValueId > 0)
                            AccrualAccess.UpdateAccrualEntitlementDetailValue(user, detailValue);
                        else if (detailValue.AccrualEntitlementDetailValueId < 0 && detailValue.EffectiveMonth != null && detailValue.AmountPercentage != null)
                            InsertAccrualEntitlementDetailValue(user, detailValue);
                    }

                    scope.Complete();
                }

                return detailValueCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - UpdateAccrualEntitlementDetailValue", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteAccrualEntitlementDetailValue(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetailValue detailValue)
        {
            try
            {
                AccrualAccess.DeleteAccrualEntitlementDetailValue(user, detailValue);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - DeleteAccrualEntitlementDetailValue", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region accrual policy search
        public AccrualPolicySearchCollection GetAccrualPolicyReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String criteria)
        {
            try
            {
                return AccrualAccess.GetAccrualPolicyReport(user, criteria);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - GetAccrualPolicyReport", ex);
                throw;
            }
        }
        public void DeleteAccrualPolicyReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long policyId)
        {
            try
            {
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    //delete from the accrual policy entitlement table
                    DeleteAccrualPolicyEntitlement(user, policyId);

                    //delete from the accrual policy table
                    AccrualPolicyCollection policyCollection = GetAccrualPolicy(user, policyId);
                    if (policyCollection.Count > 0)
                        DeleteAccrualPolicy(user, policyCollection[0]);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - DeleteAccrualPolicyReport", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region accrual policy
        public AccrualPolicyCollection GetAccrualPolicy(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? policyId)
        {
            try
            {
                return AccrualAccess.GetAccrualPolicy(user, policyId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - GetAccrualPolicy", ex);
                throw;
            }
        }
        public AccrualPolicy InsertAccrualPolicy(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualPolicy policy, AccrualPolicyEntitlementCollection policyEntitlementCollection)
        {
            try
            {
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    //insert into the accrual policy table
                    AccrualAccess.InsertAccrualPolicy(user, policy);

                    //insert into the accrual policy entitlement table
                    InsertAccrualPolicyEntitlement(user, policy.AccrualPolicyId, policyEntitlementCollection);

                    scope.Complete();
                }

                return policy;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - InsertAccrualPolicy", ex);
                throw;
            }
        }
        public void UpdateAccrualPolicy(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualPolicy policy, AccrualPolicyEntitlementCollection policyEntitlementCollection)
        {
            try
            {
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    //update the accrual policy table
                    AccrualAccess.UpdateAccrualPolicy(user, policy);

                    //insert into the accrual policy entitlement table
                    InsertAccrualPolicyEntitlement(user, policy.AccrualPolicyId, policyEntitlementCollection);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - UpdateAccrualPolicy", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteAccrualPolicy(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualPolicy policy)
        {
            try
            {
                AccrualAccess.DeleteAccrualPolicy(user, policy);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - DeleteAccrualPolicy", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region accrual policy entitlement
        public AccrualPolicyEntitlementCollection GetAccrualPolicyEntitlement(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long policyId)
        {
            try
            {
                return AccrualAccess.GetAccrualPolicyEntitlement(user, policyId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - GetAccrualPolicyEntitlement", ex);
                throw;
            }
        }
        public AccrualPolicyEntitlementCollection InsertAccrualPolicyEntitlement(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long policyId, AccrualPolicyEntitlementCollection policyEntitlementCollection)
        {
            try
            {
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    //delete from the accrual policy entitlement table
                    DeleteAccrualPolicyEntitlement(user, policyId);

                    //insert into the accrual policy entitlement table
                    foreach (AccrualPolicyEntitlement policyEntitlement in policyEntitlementCollection)
                    {
                        //set the policy id
                        if (policyEntitlement.AccrualPolicyId == -1)
                            policyEntitlement.AccrualPolicyId = policyId;

                        AccrualAccess.InsertAccrualPolicyEntitlement(user, policyEntitlement);
                    }

                    scope.Complete();
                }

                return policyEntitlementCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - InsertAccrualPolicyEntitlement", ex);
                throw;
            }
        }
        public void DeleteAccrualPolicyEntitlement(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long policyId)
        {
            try
            {
                AccrualAccess.DeleteAccrualPolicyEntitlement(user, policyId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "AccrualManagement - DeleteAccrualPolicyEntitlement", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion
    }
}