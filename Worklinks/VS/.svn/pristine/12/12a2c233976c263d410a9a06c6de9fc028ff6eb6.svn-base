﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Xsd
{
    [Serializable]
    public class WorkLinksEmployee : Employee , IImportEmployeeIdentifier
    {
        public enum ChangeType
        {
            None,
            Insert,
            Modify,
        }

        #region data properties
        [DataMember]
        public String BODID { get; set; }

        [DataMember]
        public DateTime XmlCreationDateTime { get; set; }

        [DataMember]
        public String ImportExternalDatabaseReferenceId { get; set; }

        [DataMember]
        public String ImportExternalDatabaseLogicalId { get; set; }

        [DataMember]
        public bool BiographicalEnabledFlag { get; set; }

        [DataMember]
        public ChangeType BiographicalChangeType { get; set; }

        [DataMember]
        public bool EmploymentInformationEnabledFlag { get; set; }

        [DataMember]
        public ChangeType EmploymentInformationChangeType { get; set; }

        [DataMember]
        public bool PaycodeEnabledFlag { get; set; }

        [DataMember]
        public ChangeType PaycodeChangeType { get; set; }

        [DataMember]
        public bool AddressEnabledFlag { get; set; }

        [DataMember]
        public ChangeType AddressChangeType { get; set; }

        [DataMember]
        public bool PhoneEnabledFlag { get; set; }

        [DataMember]
        public ChangeType PhoneChangeType { get; set; }

        [DataMember]
        public bool EmailEnabledFlag { get; set; }

        [DataMember]
        public ChangeType EmailChangeType { get; set; }

        [DataMember]
        public bool PositionEnabledFlag { get; set; }

        [DataMember]
        public ChangeType PositionChangeType { get; set; }

        [DataMember]
        public bool TerminationEnabledFlag { get; set; }

        [DataMember]
        public ChangeType TerminationChangeType { get; set; }

        [DataMember]
        public bool RehireEnabledFlag { get; set; }

        [DataMember]
        public ChangeType RehireChangeType { get; set; }

        [DataMember]
        public bool LeaveEnabledFlag { get; set; }

        [DataMember]
        public ChangeType LeaveChangeType { get; set; }

        [DataMember]
        public bool StatutoryDeductionEnabledFlag { get; set; }

        [DataMember]
        public ChangeType StatutoryDeductionChangeType { get; set; }

        [DataMember]
        public bool BankingEnabledFlag { get; set; }

        [DataMember]
        public ChangeType BankingChangeType { get; set; }

        [DataMember]
        public bool PayrollBatchEnabledFlag { get; set; }

        [DataMember]
        public ChangeType PayrollBatchChangeType { get; set; }

        [DataMember]
        public String MaritalStatusExternalIdentifier { get; set; }

        [DataMember]
        public String BilingualismExternalIdentifier { get; set; }

        [DataMember]
        public String HealthCareProvinceStateExternalIdentifier { get; set; }

        [DataMember]
        public String CitizenshipExternalIdentifier { get; set; }

        [DataMember]
        public String EmploymentEquityExternalIdentifier { get; set; }

        [DataMember]
        public String LanguageExternalIdentifier { get; set; }

        [DataMember]
        public String TitleExternalIdentifier { get; set; }

        [DataMember]
        public String GenderExternalIdentifier { get; set; }

        [DataMember]
        public String LeaveTerminationTypeCodeExternalIdentifier { get; set; }

        [DataMember]
        public DateTime LeaveTerminationDate { get; set; }

        [DataMember]
        public DateTime? WorkReturnDate { get; set; }

        [DataMember]
        public WorkLinksPersonAddressCollection WorkLinksAddressCollection { get; set; }

        [DataMember]
        public WorkLinksPayrollBatch PayrollBatch { get; set; }

        [DataMember]
        public WorkLinksContactChannelCollection Phones { get; set; }

        [DataMember]
        public WorkLinksContactChannelCollection Emails { get; set; }

        [DataMember]
        public WorkLinksEmployeeBankingCollection WorkLinksBankingCollection { get; set; }

        [DataMember]
        public WorkLinksEmployeePosition WorkLinksEmployeePosition { get; set; }

        public String DecodeCodeValidationMessage { get; set; }

        public EmployeeCollection EmployeeCollection
        {
            get
            {
                EmployeeCollection collection = new EmployeeCollection();
                Employee employee = new Employee();
                base.CopyTo(employee);
                collection.Add(employee);

                return collection;
            }
        }

        public PersonAddressCollection AddressCollection
        {
            get
            {
                PersonAddressCollection collection = new PersonAddressCollection();

                if (PrimaryAddress != null)
                {
                    PersonAddress address = new PersonAddress();
                    PrimaryAddress.CopyTo(address);
                    collection.Add(address);
                }

                return collection;
            }
        }

        public EmployeePositionCollection EmployeePositionCollection
        {
            get
            {
                EmployeePositionCollection collection = new EmployeePositionCollection();

                if (WorkLinksEmployeePosition != null)
                {
                    EmployeePosition position = new EmployeePosition();
                    WorkLinksEmployeePosition.CopyTo(position);
                    collection.Add(position);
                }

                return collection;
            }
        }
        #endregion

        public void SetDecodeCodeValidationMessage(String message)
        {
            DecodeCodeValidationMessage = ((DecodeCodeValidationMessage) ?? "")  + message + Environment.NewLine;
        }

        public bool MeetsEmployeeRequirements()
        {
            return GetValidationMessage() == null;
        }
        public String GetValidationMessage()
        {
            String validationMessage = null;
            System.Text.StringBuilder output = new System.Text.StringBuilder();

            if (!IsValid)
                output.Append(GetValidationInformation());

            if (DecodeCodeValidationMessage != null)
                output.AppendLine(DecodeCodeValidationMessage);

            if (output.Length > 0)
            {
                //add employee number to message
                validationMessage = String.Format("Employee {0}: ", ImportExternalIdentifier) + Environment.NewLine;
                validationMessage += output.ToString();
            }

            return validationMessage;
        }
    }
}