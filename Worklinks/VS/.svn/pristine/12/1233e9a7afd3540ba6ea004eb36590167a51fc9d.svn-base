﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerYearEndAdjustmentCode : SqlServerBase
    {
        #region InsertBatch

        public void InsertBatch(DatabaseUser user, WorkLinksYearEndAdjustmentPaycodeCollection paycodes)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("YearEndAdjustmentPaycode_import", user.DatabaseName))
                {
                    DataTable tablePaycodes = LoadYearEndAdjustmentPaycodeTable(paycodes);

                    command.AddParameter(new SqlParameter("@parmPaycodes", SqlDbType.Structured) { Value = tablePaycodes });
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.BeginTransaction("YearEndAdjustmentPaycode_import");
                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        private static DataTable LoadYearEndAdjustmentPaycodeTable(WorkLinksYearEndAdjustmentPaycodeCollection items)
        {
            DataTable table = new DataTable();

            table.Columns.Add("year_end_adjustment_paycode_id", typeof(long));
            table.Columns.Add("import_export_log_id", typeof(long));
            table.Columns.Add("year", typeof(decimal));
            table.Columns.Add("business_number_id", typeof(long));
            table.Columns.Add("employee_id", typeof(long));
            table.Columns.Add("code_province_state_cd", typeof(string));
            table.Columns.Add("code_paycode_cd", typeof(string));
            table.Columns.Add("amount", typeof(decimal));

            foreach (WorkLinksYearEndAdjustmentPaycode paycode in items)
            {
                table.Rows.Add(
                    paycode.YearEndAdjustmentPaycodeId,
                    paycode.ImportExportLogId,
                    paycode.Year,
                    paycode.BusinessNumberId,
                    paycode.EmployeeId,
                    paycode.ProvinceStateCode,
                    paycode.PaycodeCode,
                    paycode.Amount);
            }

            return table;
        }
    }

    #endregion
}
