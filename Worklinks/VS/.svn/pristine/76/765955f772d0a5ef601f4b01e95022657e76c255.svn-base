﻿<?xml version="1.0" encoding="UTF-8"?>
<!--
     The notices below are provided with respect to licensed content incorporated herein:
     Copyright NorthgateArinso 2013. All Rights Reserved. http://www.ngahr.com
     Copyright HR-XML Consortium. All Rights Reserved. http://www.hrxmlconsortium.org and http://www.hr-xml.org. Terms of license can be found in license.txt.
     Copyright (c) 1997-2011 Open Applications Group, Inc. All Rights Reserved.  http://www.openapplications.org
     .../NgaHR/PayServEmpExtension.
     -->
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.ngahr.com/ngapexxml/1"
	xmlns:hr="http://www.hr-xml.org/3" xmlns:ccts="urn:un:unece:uncefact:documentation:1.1"
	xmlns:oa="http://www.openapplications.org/oagis/9"
	targetNamespace="http://www.ngahr.com/ngapexxml/1" elementFormDefault="qualified"
	attributeFormDefault="unqualified">
	<xsd:annotation>
		<xsd:documentation>
 		 Name: PayServEmpExtension.xsd   
		 Status: 1.06 Release
		 Date this version: 2014-Jun-3 
		 </xsd:documentation>
	</xsd:annotation>

	<xsd:include schemaLocation="Components.xsd"/>
	<xsd:include schemaLocation="LocalPayrollData.xsd"/>
	<xsd:import namespace="http://www.openapplications.org/oagis/9"
		schemaLocation="../Oagis/Common/OAGi/Fields.xsd"/>
	<xsd:import namespace="http://www.hr-xml.org/3" schemaLocation="../HRXML/Common/Components.xsd"/>

	<xsd:complexType name="PayServEmpExtensionType">
		<xsd:annotation>
			<xsd:documentation source="http://www.ngahr.com/ngapexxml" xml:lang="en">
				<ccts:DictionaryEntryName> NGA Indicative Data BOD Extension </ccts:DictionaryEntryName>
				<ccts:DefinitionText> Grouping of all extra fields which are needed by a Payroll
					Service to correctly process the payroll but not supported in the standard
					HR-XML structure of Indicative Data. </ccts:DefinitionText>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="PaymentInstructions" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="CostAssignment" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="PayScales" minOccurs="0"/>
			<xsd:element ref="DateSpecifications" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="AlternateIdentifiers" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="LocalPayrollData" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="EffectiveDateAttributeGroup"/>
	</xsd:complexType>
	<xsd:element name="PaymentInstructions" type="PaymentInstructionsType"/>
	<xsd:complexType name="PaymentInstructionsType">
		<xsd:annotation>
			<xsd:documentation source="http://www.ngahr.com/ngapexxml" xml:lang="en">
				<ccts:DictionaryEntryName> NGA Payment Instructions </ccts:DictionaryEntryName>
				<ccts:DefinitionText> Grouping of data related to the payment of an employee.
				</ccts:DefinitionText>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="hr:ID" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="PaymentType" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="PaymentMethod" minOccurs="1" maxOccurs="1"/>
			<xsd:element ref="LocalPaymentMethod" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="hr:PaymentTypeCode" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="Amount" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="PaymentPercentage" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="DirectDepositAccount" minOccurs="0" maxOccurs="1"/>
			<xsd:element ref="hr:CurrencyCode" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="EffectiveDateAttributeGroup"/>
	</xsd:complexType>
</xsd:schema>
