﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class YearEnd : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long YearEndId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public Decimal Year { get; set; }

        [DataMember]
        public String CodeYearEndStatusCd { get; set; }

        [DataMember]
        public DateTime? T4PrintDatetime { get; set; }

        [DataMember]
        public DateTime? T4aPrintDatetime { get; set; }

        [DataMember]
        public DateTime? R1PrintDatetime { get; set; }

        [DataMember]
        public DateTime? R2PrintDatetime { get; set; }

        [DataMember]
        public DateTime? NR4PrintDatetime { get; set; }

        [DataMember]
        public DateTime? T4MassFileDatetime { get; set; }

        [DataMember]
        public DateTime? T4aMassFileDatetime { get; set; }

        [DataMember]
        public DateTime? R1MassFileDatetime { get; set; }

        [DataMember]
        public DateTime? R2MassFileDatetime { get; set; }

        [DataMember]
        public DateTime? NR4MassFileDatetime { get; set; }

        [DataMember]
        public DateTime? T4ExportDatetime { get; set; }

        [DataMember]
        public DateTime? T4aExportDatetime { get; set; }

        [DataMember]
        public DateTime? R1ExportDatetime { get; set; }

        [DataMember]
        public DateTime? R2ExportDatetime { get; set; }

        [DataMember]
        public DateTime? NR4ExportDatetime { get; set; }

        [DataMember]
        public int CurrentRevision { get; set; }
        #endregion

        #region construct/destruct
        public YearEnd()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(YearEnd data)
        {
            base.CopyTo(data);

            data.YearEndId = YearEndId;
            data.Year = Year;
            data.CodeYearEndStatusCd = CodeYearEndStatusCd;
            data.T4PrintDatetime = T4PrintDatetime;
            data.T4aPrintDatetime = T4aPrintDatetime;
            data.R1PrintDatetime = R1PrintDatetime;
            data.R2PrintDatetime = R2PrintDatetime;
            data.NR4PrintDatetime = NR4PrintDatetime;
            data.T4MassFileDatetime = T4MassFileDatetime;
            data.T4aMassFileDatetime = T4aMassFileDatetime;
            data.R1MassFileDatetime = R1MassFileDatetime;
            data.R2MassFileDatetime = R2MassFileDatetime;
            data.NR4MassFileDatetime = NR4MassFileDatetime;
            data.T4ExportDatetime = T4ExportDatetime;
            data.T4aExportDatetime = T4aExportDatetime;
            data.R1ExportDatetime = R1ExportDatetime;
            data.R2ExportDatetime = R2ExportDatetime;
            data.NR4ExportDatetime = NR4ExportDatetime;
            data.CurrentRevision = CurrentRevision;
        }
        public new void Clear()
        {
            base.Clear();

            YearEndId = -1;
            Year = -1000;
            CodeYearEndStatusCd = null;
            T4PrintDatetime = null;
            T4aPrintDatetime = null;
            R1PrintDatetime = null;
            R2PrintDatetime = null;
            NR4PrintDatetime = null;
            T4MassFileDatetime = null;
            T4aMassFileDatetime = null;
            R1MassFileDatetime = null;
            R2MassFileDatetime = null;
            T4ExportDatetime = null;
            T4aExportDatetime = null;
            R1ExportDatetime = null;
            R2ExportDatetime = null;
            NR4ExportDatetime = null;
            CurrentRevision = 0;
        }
        #endregion
    }
}