﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SalaryEmployee : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {

        #region properties
        [DataMember]
        public long EmployeePositionId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long EmployeeId { get; set; }
        [DataMember]
        public Decimal CompensationAmount { get; set; }
        [DataMember]
        public String CodePayrollProcessGroupCd { get; set; }
        [DataMember]
        public String CodeEmployeePositionStatusCd { get; set; }
        [DataMember]
        public Decimal StandardHours { get; set; }
        [DataMember]
        public String CodePaymentMethodCd { get; set; }
        [DataMember]
        public Decimal? LockedStandardHours { get; set; }
        [DataMember]
        public Decimal? LockedCompensationAmount { get; set; }
        [DataMember]
        public String PaycodeCode { get; set; }
        [DataMember]
        public Decimal AmountRateFactor { get; set; }
        [DataMember]
        public bool AutoPopulateRateFlag { get; set; }
        [DataMember]
        public bool IncludeEmploymentInsuranceHoursFlag { get; set; }
        [DataMember]
        public bool UseSalaryStandardHourFlag { get; set; }
        [DataMember]
        public bool PrimaryRecordFlag { get; set; }
        [DataMember]
        public int? WorkdayCount { get; set; }
        [DataMember]
        public int WorkDaysPerWeek { get; set; }
        [DataMember]
        public Decimal HoursPerWeek { get; set; }
        [DataMember]
        public DateTime StartDateRange { get; set; }
        [DataMember]
        public DateTime EndDateRange { get; set; }
        [DataMember]
        public Decimal OffsetHour { get; set; }

        [DataMember]
        public EmployeePosition Position { get; set; }
        #endregion

        #region construct/destruct
        public SalaryEmployee()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(SalaryEmployee data)
        {
            base.CopyTo(data);
            data.EmployeePositionId = EmployeePositionId;
            data.EmployeeId = EmployeeId;
            data.CompensationAmount = CompensationAmount;
            data.CodePayrollProcessGroupCd = CodePayrollProcessGroupCd;
            data.CodeEmployeePositionStatusCd = CodeEmployeePositionStatusCd;
            data.StandardHours = StandardHours;
            data.LockedCompensationAmount = LockedCompensationAmount;
            data.LockedStandardHours = LockedStandardHours;
            data.PaycodeCode = PaycodeCode;
            data.AmountRateFactor = AmountRateFactor;
            data.AutoPopulateRateFlag = AutoPopulateRateFlag;
            data.IncludeEmploymentInsuranceHoursFlag = IncludeEmploymentInsuranceHoursFlag;
            data.UseSalaryStandardHourFlag = UseSalaryStandardHourFlag;
            data.PrimaryRecordFlag = PrimaryRecordFlag;
            data.WorkdayCount = WorkdayCount;
            data.WorkDaysPerWeek = WorkDaysPerWeek;
            data.HoursPerWeek = HoursPerWeek;
            data.StartDateRange = StartDateRange;
            data.EndDateRange = EndDateRange;
            data.OffsetHour = OffsetHour;
            if (Position == null)
            {
                data.Position = null;
            }
            else
            {
                data.Position = new EmployeePosition();
                Position.CopyTo(data.Position);
            }
        }
        public new void Clear()
        {
            base.Clear();
            EmployeePositionId = -1;
            EmployeeId = -1;
            CompensationAmount = 0;
            CodePayrollProcessGroupCd = "";
            CodeEmployeePositionStatusCd = "";
            StandardHours = -1;
            LockedCompensationAmount = null;
            LockedStandardHours = null;
            PaycodeCode = null;
            AmountRateFactor = -1;
            AutoPopulateRateFlag = false;
            IncludeEmploymentInsuranceHoursFlag = true;
            UseSalaryStandardHourFlag = false;
            PrimaryRecordFlag = true;
            WorkdayCount = null;
            WorkDaysPerWeek = 0;
            HoursPerWeek = 0;
            Position = null;
            StartDateRange = DateTime.Now;
            EndDateRange = DateTime.Now;
            OffsetHour = 0;
        }

        #endregion

    }
}
