﻿using System;
using System.Web;
using System.Web.Routing;

namespace WorkLinks.Web.WorkLinks
{
    public class Global : System.Web.HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
//            System.Web.Http.GlobalConfiguration.Configure(WebApiConfig.Register);
            RegisterRoutes(RouteTable.Routes);
        }
        /// <summary>
        /// setup core route structure for WorkLinks website
        /// </summary>
        /// <param name="routes"></param>
        public static void RegisterRoutes(RouteCollection routes)
        {



            #region human resources
            //HR employee search 
            routes.MapPageRoute("HREmployeeSearch",
                "HumanResources/Employee/Search/",
                "~/HumanResources/Employee/EmployeeSearchPage.aspx"
                );
            //HR wizard search 
            routes.MapPageRoute("HRWizardSearch",
                "HumanResources/Employee/Wizard/",
                "~/HumanResources/Wizards/WizardSearchPage.aspx"
                );
            //disciplinaryAction
            routes.MapPageRoute("EmployeeDiscipline",
                "HumanResources/EmployeeDiscipline/{employeeId}/{lastName}/{firstName}",
                "~/HumanResources/DisciplinaryAction/DisciplinaryActionModulePage.aspx"
                );
            //types
            routes.MapPageRoute("ContactType",
                "HumanResources/ContactType/{type}/{employeeId}",
                "~/HumanResources/Contacts/ContactTypePage.aspx"
                );
            //employee
            routes.MapPageRoute("Contact",
                "HumanResources/Contact/{employeeId}/{lastName}/{firstName}",
                "~/HumanResources/Contacts/ContactModulePage.aspx"
                );
            //education
            routes.MapPageRoute("Education",
                "HumanResources/Education/{employeeId}/{lastName}/{firstName}",
                "~/HumanResources/Employee/Education/EmployeeEducationModulePage.aspx"
                );
            //grievance
            routes.MapPageRoute("Grievance",
                "HumanResources/Grievance/{action}/{grievanceId}",
                "~/HumanResources/Grievances/GrievancePage.aspx"
                );
            //employee
            routes.MapPageRoute("Employee",
                "HumanResources/Employee/{action}/{employeeId}/{lastName}/{firstName}",
                "~/HumanResources/Employee/EmployeePage.aspx"
                );
            //add employee
            routes.MapPageRoute("EmployeeAdd",
                "HumanResources/EmployeeWizard/{action}/{wizardCacheId}/{wizard_index}/{parentPage}/{isTemplate}/{countryCode}",
                "~/HumanResources/Wizards/WizardAddEmployeePage.aspx"
                );
            //employee history
            routes.MapPageRoute("EmployeePosition",
                "HumanResources/EmployeePosition/{employeeId}/{lastName}/{firstName}/{payrollProcessGroupCode}",
                "~/HumanResources/EmployeePosition/EmployeePositionPage.aspx"
                );
            //employee employment
            routes.MapPageRoute("EmployeeEmploymentInformation",
                "HumanResources/Employment/{employeeId}/{lastName}/{firstName}",
                "~/HumanResources/Employee/EmploymentInformation/EmployeeEmploymentInformationPage.aspx"
                );
            //company property
            routes.MapPageRoute("CompanyProperty",
                "HumanResources/CompanyProperty/{employeeId}/{lastName}/{firstName}",
                "~/HumanResources/Employee/CompanyProperty/CompanyProperty.aspx"
                );
            //terminations
            routes.MapPageRoute("EmployeeTermination",
                "HumanResources/Terminations/{employeeId}/{lastName}/{firstName}/{tabNameToSelect}",
                "~/HumanResources/Terminations/EmployeeTerminationPage.aspx"
                );
            //wsib health and safety report
            routes.MapPageRoute("WsibHealthAndSafetyReport",
                "HumanResources/WsibHealthAndSafetyReport/{employeeId}/{lastName}/{firstName}",
                "~/HumanResources/HealthAndSafetyReport/HealthAndSafetyReportPage.aspx"
                );
            //entitlement
            routes.MapPageRoute("Entitlement",
                "HumanResources/Entitlement/{action}/{entitlementId}",
                "~/HumanResources/Entitlement/EntitlementPage.aspx"
                );
            //policy
            routes.MapPageRoute("Policy",
                "HumanResources/Policy/{action}/{policyId}",
                "~/HumanResources/Policy/PolicyPage.aspx"
                );
            //employee benefit
            routes.MapPageRoute("EmployeeBenefit",
                "HumanResources/Benefit/{employeeId}/{lastName}/{firstName}",
                "~/HumanResources/Employee/Benefit/EmployeeBenefitModulePage.aspx"
                );
            #endregion

            #region payroll
            //TEMP batch payroll
            routes.MapPageRoute("BulkPayroll",
                "Payroll/PayrollProcessBulk",
                "~/Payroll/PayrollProcess/PayrollProcessBulkPage.aspx"
                );
            //payroll processing
            routes.MapPageRoute("PayrollProcessing",
                "HumanResources/PayrollProcessing/View/{payrollProcessId}",
                "~/Payroll/PayrollProcess/PayrollProcessingPage.aspx"
                );
            //statutory deductions
            routes.MapPageRoute("EmployeeStatutoryDeductions",
                "Payroll/StatutoryDeduction/{employeeId}/{lastName}/{firstName}",
                "~/Payroll/StatDeduction/StatutoryDeductionPage.aspx"
                );
            //payroll batch transactions
            routes.MapPageRoute("PayrollBatch",
                  "PayrollBatch/PayrollBatchTransaction/{action}/{PayrollBatchId}/{PayProcessGrpCd}/{PayProcessRunTypeCd}",
                  "~/Payroll/PayrollBatch/PayrollBatchTransactionPage.aspx"
                  );
            //employee banking
            routes.MapPageRoute("EmployeeBanking",
                "Payroll/Banking/EmployeeBankingPage/{employeeId}/{lastName}/{firstName}",
                "~/Payroll/Banking/EmployeeBankingPage.aspx"
                );
            //employee paycode
            routes.MapPageRoute("EmployeePaycode",
                "Payroll/Paycode/EmployeePaycodePage/{employeeId}/{lastName}/{firstName}",
                "~/Payroll/Paycode/EmployeePaycodePage.aspx"
                );
            //employee payslip
            routes.MapPageRoute("EmployeePaySlip",
                "Payroll/PaySlip/EmployeePaySlipPage/{employeeId}/{employeeNumber}/{lastName}/{firstName}",
                "~/Payroll/PaySlip/EmployeePaySlipPage.aspx"
                );
            //ROE Creation Details
            routes.MapPageRoute("ROECreationDetails",
                "Payroll/RoeCreation/{action}/{employeeId}/{lastName}/{firstName}/{employeePositionId}/{tabNameToSelect}/{payrollProcessGroupCode}",
                "~/Payroll/RoeCreation/RoeCreationDetails.aspx"
                );
            //ROE Export
            routes.MapPageRoute("CreationROE",
                "Payroll/CreateROE/{employeePositionIds}",
                "~/Payroll/RoeCreation/CreateROEPage.aspx"
                );
            //Payslip Export Cheque
            routes.MapPageRoute("PayslipExportCheque",
                "Payroll/ExportCheque/ExportCheque/{payrollProcessId}/{employeeNumber}",
                "~/Payroll/ExportCheque/ExportChequePage.aspx"
                );
            //eft payment
            routes.MapPageRoute("EftPayment",
                "Payroll/PayrollProcess/EftPayment/{payrollProcessId}",
                "~/Payroll/PayrollProcess/EftPaymentPage.aspx"
                );
            //remittance import
            routes.MapPageRoute("RemittanceImport",
               "Admin/ImportExport/{importType}",
               "~/Payroll/Remittance/RemittanceImportExportPage.aspx"
               );
            //remittance import approval
            routes.MapPageRoute("RemittanceImportApproval",
                "Payroll/Remittance/ImportApproval/{remittanceImportId}",
                "~/Payroll/Remittance/RemittanceImportApprovalPage.aspx"
                );
            #endregion

            #region admin
            //user administration
            routes.MapPageRoute("UserAdmin",
                "Admin/UserAdmin/{action}/{personId}",
                "~/Admin/UserAdmin/UserAdminPersonPage.aspx"
                );
            //year end processing
            routes.MapPageRoute("YearEndProcessing",
               "Admin/YearEndProcessing/View",
               "~/Admin/YearEnd/YearEndProcessingPage.aspx"
               );
            //language editor
            routes.MapPageRoute("Form",
                "Admin/Form/{formId}",
                "~/Admin/LanguageEditor/LanguageEditorPage.aspx"
                );
            //group editor
            routes.MapPageRoute("Group",
                "Admin/Group/{action}/{groupId}",
                "~/Admin/GroupEditor/GroupEditorPage.aspx"
                );
            //role data security
            routes.MapPageRoute("Role",
                "Admin/Role/DataSecurity/{roleId}",
                "~/Admin/RoleEditor/RoleDatabasePage.aspx"
                );
            //role editor
            routes.MapPageRoute("RoleDataSecurity",
                "Admin/Role/{action}/{roleId}",
                "~/Admin/RoleEditor/RoleEditorPage.aspx"
                );
            //field security
            routes.MapPageRoute("FieldSecurity",
                "Admin/FieldSecurity/FieldSecurity/{roleId}",
                "~/Admin/FieldSecurity/FieldSecurityPage.aspx"
                );
            //form security
            routes.MapPageRoute("FormSecurity",
                "Admin/FormSecurity/FormSecurity/{roleId}",
                "~/Admin/FormSecurityEditor/FormSecurityPage.aspx"
                );
            //security categories edit screen
            routes.MapPageRoute("SecurityCategoriesEdit",
                "Admin/SecurityCategories/EditPage/",
                "~/Admin/SecurityCategories/SecurityCategoriesEditPage.aspx"
                );
            //year end form adjustment
            routes.MapPageRoute("YearEndFormAdjustment",
                "Admin/YearEndAdjustments/YearEndAdjustment/{action}/{keyId}/{year}/{employeeId}/{province}/{businessNumber}/{yearEndTypeForm}/{yearEndAdjustmentTableId}/{revision}/{currentRevision}",
                "~/Admin/YearEndAdjustments/YearEndAdjustmentPage.aspx"
                );
            //t4 export
            routes.MapPageRoute("Export",
               "Admin/Export/{exportType}/{yearEndId}/{year}",
                "~/Admin/Export/ExportPage.aspx"
               );
            //XML Export
            routes.MapPageRoute("CreateXml",
                "YearEnd/DownLoadXml/{exportType}",
                "~/Admin/Export/XmlCreatePage.aspx"
                );
            //r1 export
            routes.MapPageRoute("R1Export",
               "Admin/R1Export/{exportType}/{yearEndId}/{year}",
                "~/Admin/R1Export/R1ExportPage.aspx"
               );
            //csb export
            routes.MapPageRoute("CSBExport",
               "CSB/CSBExport/{payrollProcessId}",
                "~/Admin/CSBExport/CSBExportPage.aspx"
               );
            //CSB Export Download
            routes.MapPageRoute("CreateCSB",
                "CSB/Download/{processId}",
                "~/Admin/CSBExport/CSBDownloadPage.aspx"
                );
            //wsib
            routes.MapPageRoute("Wsib",
                "Admin/Wsib/{action}/{wsibCode}",
                "~/Admin/WsibAdmin/WsibEditPage.aspx"
                );
            //third party
            routes.MapPageRoute("ThirdParty",
                "Admin/ThirdParty/{action}/{codeThirdPartyCode}",
                "~/Admin/ThirdParty/ThirdPartyEditPage.aspx"
                );
            //wcb cheque remittance frequency
            routes.MapPageRoute("WcbChequeRemittanceFrequency",
                "Admin/WcbChequeRemittanceFrequency/{action}/{wcbChequeRemittanceFrequencyCode}",
                "~/Admin/WcbChequeRemittanceFrequency/WcbChequeRemittanceFrequencyEditPage.aspx"
                );
            //paycode maintenance income
            routes.MapPageRoute("PaycodeMaintenanceIncome",
                "Admin/PaycodeMaintenance/Income/{action}",
                "~/Admin/PaycodeMaintenance/Income/PaycodeIncomePage.aspx"
                );
            //paycode maintenance deduction
            routes.MapPageRoute("PaycodeMaintenanceDeduction",
                "Admin/PaycodeMaintenance/Deduction/{action}",
                "~/Admin/PaycodeMaintenance/Deduction/PaycodeDeductionPage.aspx"
                );
            //paycode maintenance benefit
            routes.MapPageRoute("PaycodeMaintenanceBenefit",
                "Admin/PaycodeMaintenance/Benefit/{action}",
                "~/Admin/PaycodeMaintenance/Benefit/PaycodeBenefitPage.aspx"
                );
            //paycode maintenance employer
            routes.MapPageRoute("PaycodeMaintenanceEmployer",
                "Admin/PaycodeMaintenance/Employer/{action}",
                "~/Admin/PaycodeMaintenance/Employer/PaycodeEmployerPage.aspx"
                );
            //paycode year end report map
            routes.MapPageRoute("PaycodeYearEndReportMap",
                "Admin/PaycodeMaintenance/YearEndReportMap/{paycodeCode}",
                "~/Admin/PaycodeMaintenance/PaycodeYearEndReportMapPage.aspx"
                );
            //paycode payroll transaction offset association
            routes.MapPageRoute("PaycodePayrollTransactionOffsetAssociation",
                "Admin/PaycodeMaintenance/PayrollTransactionOffsetAssociation/{paycodeCode}",
                "~/Admin/PaycodeMaintenance/PaycodePayrollTransactionOffsetAssociationPage.aspx"
                );
            //paycode import
            routes.MapPageRoute("PaycodeImport",
               "Admin/PaycodeMaintenance/Import",
               "~/Admin/PaycodeMaintenance/PaycodeImportPage.aspx"
               );
            //override periods
            routes.MapPageRoute("PayrollProcessGroupOverridePeriod",
               "Admin/PayGroup/OverridePeriods/{payrollProcessGroupCode}",
               "~/Admin/PayGroup/PayrollProcessGroupOverridePeriodPage.aspx"
               );
            //email
            routes.MapPageRoute("Email",
               "Admin/Email/{action}/{emailId}/{name}",
               "~/Admin/Email/EmailPage.aspx"
               );
            //benefit policy
            routes.MapPageRoute("BenefitPolicy",
                "Admin/Benefit/Policy/{action}/{policyId}",
                "~/Admin/Benefit/BenefitPolicyPage.aspx"
                );
            //benefit plan
            routes.MapPageRoute("BenefitPlan",
                "Admin/Benefit/Plan/{action}/{planId}",
                "~/Admin/Benefit/BenefitPlanPage.aspx"
                );
            //salary plan
            routes.MapPageRoute("SalaryPlan",
                "Admin/SalaryPlan/Edit",
                "~/Admin/SalaryPlan/GradePage.aspx"
                );

            //salary plan add page
            routes.MapPageRoute("SalaryPlanAdd",
               "Admin/SalaryPlan/Add/{salaryPlanId}",
               "~/Admin/SalaryPlan/SalaryPlanAddPage.aspx"
               );

            //salary plan rule add page
            routes.MapPageRoute("SalaryPlanAddRule",
               "Admin/SalaryPlan/AddRule/",
               "~/Admin/SalaryPlan/SalaryPlanAddRulePage.aspx"
               );

            routes.MapPageRoute("RulePayCodeAccumulation",
                "Admin/SalaryPlan/RulePayCodeAccum/",
                "~/Admin/SalaryPlan/RulePayCodeAccumulationPage.aspx");

            #endregion

            //wizards
            routes.MapPageRoute("Wizards",
                "Wizard/{type}/{action}/{employeeId}",
                "~/Wizard/WizardPage.aspx"
                );
            //change password
            routes.MapPageRoute("ChangePassword",
                "Account/ChangePassword/{lockMode}",
                "~/Account/ChangePassword.aspx"
                );
            //reports
            routes.MapPageRoute("Report",
                "Report/{reportName}/{payrollProcessId}/{employeeNumber}/{reportType}/{year}/{employerNumber}/{provinceCode}/{revision}",
                "~/Report/ReportPage.aspx"
                );
            //reports which also needs employeeId like Employee Pay Slip Report
            routes.MapPageRoute("Report2",
                "Report/{reportName}/{payrollProcessId}/{employeeNumber}/{reportType}/{year}/{employerNumber}/{provinceCode}/{revision}/{employeeId}",
                "~/Report/ReportPage.aspx"
                );
            //HR menu reports
            routes.MapPageRoute("HRReport",
                "Report/{reportName}/{reportType}/{employeeNumber}/{startDate}/{endDate}/{organizationUnit}/{paymentMethodCode}",
                "~/Report/ReportPage.aspx"
                );
            //remittance reports
            routes.MapPageRoute("RemitReport",
                "Report/{reportName}/{detailType}/{remitCode}/{exportId}/{remitDate}/{reportType}",
                "~/Report/ReportPage.aspx"
                );
            //attachments
            routes.MapPageRoute("Attachment",
                "Attachment/{attachmentId}",
                "~/Attachments/AttachmentPage.aspx"
                );
            //health tax
            routes.MapPageRoute("HealthTax",
                "Admin/HealthTax/{action}/{healthTaxId}",
                "~/Admin/HealthTax/HealthTaxEditPage.aspx"
                );
            //remittance detail cra source deductions
            routes.MapPageRoute("RemitDetailCraSourceDed",
                "Payroll/Remittance/RemittanceDetailCraSourceDeductions/{remitCode}/{exportId}/{remitDate}",
                "~/Payroll/Remittance/RemittanceDetail/RemittanceDetailCraSourceDeductionsPage.aspx"
                );
            //remittance detail wcb deductions
            routes.MapPageRoute("RemitDetailWcbDed",
                "Payroll/Remittance/RemittanceDetailWcbDeductions/{detailType}/{remitCode}/{exportId}/{remitDate}",
                "~/Payroll/Remittance/RemittanceDetail/RemittanceDetailWcbDeductionsPage.aspx"
                );
            //remittance detail health tax deductions
            routes.MapPageRoute("RemitDetailHealthDed",
                "Payroll/Remittance/RemittanceDetailHealthDeductions/{remitCode}/{exportId}",
                "~/Payroll/Remittance/RemittanceDetail/RemittanceDetailHealthDeductionsPage.aspx"
                );
            //remittance detail garnishment deductions
            routes.MapPageRoute("RemitDetailGarnDed",
                "Payroll/Remittance/RemittanceDetailGarnishmentDeductions/{detailType}/{exportId}",
                "~/Payroll/Remittance/RemittanceDetail/RemittanceDetailGarnishmentDeductionsPage.aspx"
                );
            //remittance detail rq source deductions
            routes.MapPageRoute("RemitDetailRqSourceDed",
                "Payroll/Remittance/RemittanceDetailRqSourceDeductions/{exportId}",
                "~/Payroll/Remittance/RemittanceDetail/RemittanceDetailRqSourceDeductionsPage.aspx"
                );
            routes.MapPageRoute("OrganizaitionalUnitEditor",
               "Admin/OrganizationalUnitEdit/{action}/{OrganizationUnitId}/{OrganizationUnitLevelId}",
               "~/Admin/OrganizationalUnit/OrganizationalUnitEditPage.aspx"
               );

            //exports for REAL on the payroll menu
            routes.MapPageRoute("RealExport",
                "Export/{exportName}",
                "~/Export/ExportPage.aspx"
                );
        }
        void Application_End(object sender, EventArgs e)
        {
            // Code that runs on application shutdown
        }
        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs
            Exception exc = Server.GetLastError();
            Session["Exception"] = exc;
        }
        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

            //            AreaRegistration.RegisterAllAreas();
            base.Session["init"] = 0;
        }
        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode is set to InProc in the Web.config file.
            // If session mode is set to StateServer or SQLServer, the event is not raised.
            if (Session["UserName"] != null)
            {
//                Common.ServiceWrapper.SecurityClient.UpdateLogoutUser(Common.ApplicationParameter.DatabaseName, Session["UserName"].ToString());
//                Session["UserName"] = null;
            }
            
        }
    }
}