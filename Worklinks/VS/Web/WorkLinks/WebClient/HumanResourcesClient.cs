﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Web.BusinessService.Contract;

namespace WorkLinks.WebClient
{
    public class HumanResourcesClient : BaseClient, IHumanResources
    {
        #region fields
        private ChannelFactory<IHumanResources> _factory = null;
        private String _wcfServiceUrl = null;
        private long _maxReceivedMessageSize;
        private long _receiveTimeoutTicks;
        private long _sendTimeoutTicks;
        #endregion

        #region properties
        private ChannelFactory<IHumanResources> Factory
        {
            get
            {
                if (_factory == null)
                {
                    BasicHttpBinding binding = new BasicHttpBinding();
                    binding.MaxReceivedMessageSize = _maxReceivedMessageSize;
                    binding.ReceiveTimeout = new TimeSpan(_receiveTimeoutTicks);
                    binding.SendTimeout = new TimeSpan(_sendTimeoutTicks);

                    _factory = new ChannelFactory<IHumanResources>(binding, new EndpointAddress(_wcfServiceUrl));
                }

                return _factory;
            }
        }
        #endregion

        public HumanResourcesChannelWrapper CreateChannel()
        {
            return new HumanResourcesChannelWrapper(Factory.CreateChannel());
        }

        #region constructors/destructors
        public HumanResourcesClient(String wcfServiceUrl, String wcfServiceName, long maxReceivedMessageSize, long receiveTimeoutTicks, long sendTimeoutTicks)
        {
            _wcfServiceUrl = new Uri(new Uri(wcfServiceUrl), wcfServiceName).ToString();
            _maxReceivedMessageSize = maxReceivedMessageSize;
            _receiveTimeoutTicks = receiveTimeoutTicks;
            _sendTimeoutTicks = sendTimeoutTicks;
        }
        #endregion

        #region wcf functions

        #region employee biographical
        public String GetEmployeeNumber(long employeeId)
        {
            return GetEmployeeNumber(DatabaseUser, employeeId);
        }
        public String GetEmployeeNumber(DatabaseUser user, long employeeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeNumber(user, employeeId);
        }
        public EmployeeCollection GetEmployee(EmployeeCriteria criteria)
        {
            return GetEmployee(DatabaseUser, criteria);
        }
        public EmployeeCollection GetEmployee(DatabaseUser user, EmployeeCriteria criteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployee(user, criteria);
        }
        public Employee UpdateEmployee(Employee employee, System.Collections.Generic.List<ContactType> deletedItems)
        {
            return UpdateEmployee(DatabaseUser, employee, deletedItems);
        }
        public Employee UpdateEmployee(DatabaseUser user, Employee employee, System.Collections.Generic.List<ContactType> deletedItems)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployee(user, employee, deletedItems);
        }
        public Employee InsertEmployee(Employee employee)
        {
            return InsertEmployee(DatabaseUser, employee);
        }
        public Employee InsertEmployee(DatabaseUser user, Employee employee)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployee(user, employee);
        }
        public void DeleteEmployee(Employee employee)
        {
            DeleteEmployee(DatabaseUser, employee);
        }
        public void DeleteEmployee(DatabaseUser user, Employee employee)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployee(user, employee);
        }
        public bool CheckDatabaseForDuplicateSin(string sin, long employeeId, string employeeNumber)
        {
            return CheckDatabaseForDuplicateSin(DatabaseUser, sin, employeeId, employeeNumber);
        }
        public bool CheckDatabaseForDuplicateSin(DatabaseUser user, string sin, long employeeId, string employeeNumber)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CheckDatabaseForDuplicateSin(user, sin, employeeId, employeeNumber);
        }
        public bool ValidateSIN(String SIN, String governmentIdentificationNumberTypeCode)
        {
            return ValidateSIN(DatabaseUser, SIN, governmentIdentificationNumberTypeCode);
        }
        public bool ValidateSIN(DatabaseUser user, String SIN, String governmentIdentificationNumberTypeCode)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ValidateSIN(user, SIN, governmentIdentificationNumberTypeCode);
        }

        #endregion

        #region employee education
        public EmployeeEducationCollection GetEmployeeEducation(long employeeId)
        {
            return GetEmployeeEducation(DatabaseUser, employeeId);
        }
        public EmployeeEducationCollection GetEmployeeEducation(DatabaseUser user, long employeeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeEducation(user, employeeId);
        }
        public EmployeeEducation UpdateEmployeeEducation(EmployeeEducation education)
        {
            return UpdateEmployeeEducation(DatabaseUser, education);
        }
        public EmployeeEducation UpdateEmployeeEducation(DatabaseUser user, EmployeeEducation education)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeeEducation(user, education);
        }
        public EmployeeEducation InsertEmployeeEducation(EmployeeEducation education)
        {
            return InsertEmployeeEducation(DatabaseUser, education);
        }
        public EmployeeEducation InsertEmployeeEducation(DatabaseUser user, EmployeeEducation education)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeeEducation(user, education);
        }
        public EmployeeEducation DeleteEmployeeEducation(EmployeeEducation education)
        {
            return DeleteEmployeeEducation(DatabaseUser, education);
        }
        public EmployeeEducation DeleteEmployeeEducation(DatabaseUser user, EmployeeEducation education)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.DeleteEmployeeEducation(user, education);
        }
        #endregion

        #region employee company property
        public EmployeeCompanyPropertyCollection GetEmployeeCompanyProperty(long employeeId)
        {
            return GetEmployeeCompanyProperty(DatabaseUser, employeeId);
        }
        public EmployeeCompanyPropertyCollection GetEmployeeCompanyProperty(DatabaseUser user, long employeeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeCompanyProperty(user, employeeId);
        }
        public EmployeeCompanyProperty UpdateEmployeeCompanyProperty(EmployeeCompanyProperty property)
        {
            return UpdateEmployeeCompanyProperty(DatabaseUser, property);
        }
        public EmployeeCompanyProperty UpdateEmployeeCompanyProperty(DatabaseUser user, EmployeeCompanyProperty property)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeeCompanyProperty(user, property);
        }
        public EmployeeCompanyProperty InsertEmployeeCompanyProperty(EmployeeCompanyProperty property)
        {
            return InsertEmployeeCompanyProperty(DatabaseUser, property);
        }
        public EmployeeCompanyProperty InsertEmployeeCompanyProperty(DatabaseUser user, EmployeeCompanyProperty property)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeeCompanyProperty(user, property);
        }
        public EmployeeCompanyProperty DeleteEmployeeCompanyProperty(EmployeeCompanyProperty property)
        {
            return DeleteEmployeeCompanyProperty(DatabaseUser, property);
        }
        public EmployeeCompanyProperty DeleteEmployeeCompanyProperty(DatabaseUser user, EmployeeCompanyProperty property)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.DeleteEmployeeCompanyProperty(user, property);
        }
        #endregion

        #region employee contact
        public ContactCollection GetContact(long employeeId, long? contactId)
        {
            return GetContact(DatabaseUser, employeeId, contactId);
        }
        public ContactCollection GetContact(DatabaseUser user, long employeeId, long? contactId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetContact(user, employeeId, contactId);
        }
        public Contact UpdateContact(Contact contact)
        {
            return UpdateContact(DatabaseUser, contact);
        }
        public Contact UpdateContact(DatabaseUser user, Contact contact)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateContact(user, contact);
        }
        public Contact InsertContact(Contact contact)
        {
            return InsertContact(DatabaseUser, contact);
        }
        public Contact InsertContact(DatabaseUser user, Contact contact)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertContact(user, contact);
        }
        public void DeleteContact(Contact contact)
        {
            DeleteContact(DatabaseUser, contact);
        }
        public void DeleteContact(DatabaseUser user, Contact contact)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteContact(user, contact);
        }
        #endregion

        #region employee contact type
        public ContactTypeCollection GetContactType(long employeeId, String contactTypeCode)
        {
            return GetContactType(DatabaseUser, employeeId, contactTypeCode);
        }
        public ContactTypeCollection GetContactType(DatabaseUser user, long employeeId, String contactTypeCode)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetContactType(user, employeeId, contactTypeCode);
        }
        public void UpdateContactType(ContactTypeCollection contactTypes)
        {
            UpdateContactType(DatabaseUser, contactTypes);
        }
        public void UpdateContactType(DatabaseUser user, ContactTypeCollection contactTypes)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.UpdateContactType(user, contactTypes);
        }
        public ContactType InsertContactType(ContactType contactType)
        {
            return InsertContactType(DatabaseUser, contactType);
        }
        public ContactType InsertContactType(DatabaseUser user, ContactType contactType)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertContactType(user, contactType);
        }
        public void DeleteContactType(ContactType contactType)
        {
            DeleteContactType(DatabaseUser, contactType);
        }
        public void DeleteContactType(DatabaseUser user, ContactType contactType)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteContactType(user, contactType);
        }
        #endregion

        #region employee skill
        public EmployeeSkillCollection GetEmployeeSkill(long employeeId)
        {
            return GetEmployeeSkill(DatabaseUser, employeeId);
        }
        public EmployeeSkillCollection GetEmployeeSkill(DatabaseUser user, long employeeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeSkill(user, employeeId);
        }
        public EmployeeSkill UpdateEmployeeSkill(EmployeeSkill skill)
        {
            return UpdateEmployeeSkill(DatabaseUser, skill);
        }
        public EmployeeSkill UpdateEmployeeSkill(DatabaseUser user, EmployeeSkill skill)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeeSkill(user, skill);
        }
        public EmployeeSkill InsertEmployeeSkill(EmployeeSkill skill)
        {
            return InsertEmployeeSkill(DatabaseUser, skill);
        }
        public EmployeeSkill InsertEmployeeSkill(DatabaseUser user, EmployeeSkill skill)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeeSkill(user, skill);
        }
        public void DeleteEmployeeSkill(EmployeeSkill skill)
        {
            DeleteEmployeeSkill(DatabaseUser, skill);
        }
        public void DeleteEmployeeSkill(DatabaseUser user, EmployeeSkill skill)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeeSkill(user, skill);
        }
        #endregion

        #region employee course
        public EmployeeCourseCollection GetEmployeeCourse(long employeeId)
        {
            return GetEmployeeCourse(DatabaseUser, employeeId);
        }
        public EmployeeCourseCollection GetEmployeeCourse(DatabaseUser user, long employeeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeCourse(user, employeeId);
        }
        public EmployeeCourse UpdateEmployeeCourse(EmployeeCourse course)
        {
            return UpdateEmployeeCourse(DatabaseUser, course);
        }
        public EmployeeCourse UpdateEmployeeCourse(DatabaseUser user, EmployeeCourse course)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeeCourse(user, course);
        }
        public EmployeeCourse InsertEmployeeCourse(EmployeeCourse course)
        {
            return InsertEmployeeCourse(DatabaseUser, course);
        }
        public EmployeeCourse InsertEmployeeCourse(DatabaseUser user, EmployeeCourse course)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeeCourse(user, course);
        }
        public void DeleteEmployeeCourse(EmployeeCourse course)
        {
            DeleteEmployeeCourse(DatabaseUser, course);
        }
        public void DeleteEmployeeCourse(DatabaseUser user, EmployeeCourse course)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeeCourse(user, course);
        }
        #endregion

        #region employee membership
        public EmployeeMembershipCollection GetEmployeeMembership(long employeeId)
        {
            return GetEmployeeMembership(DatabaseUser, employeeId);
        }
        public EmployeeMembershipCollection GetEmployeeMembership(DatabaseUser user, long employeeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeMembership(user, employeeId);
        }
        public EmployeeMembership InsertEmployeeMembership(EmployeeMembership membership)
        {
            return InsertEmployeeMembership(DatabaseUser, membership);
        }
        public EmployeeMembership InsertEmployeeMembership(DatabaseUser user, EmployeeMembership membership)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeeMembership(user, membership);
        }
        public EmployeeMembership UpdateEmployeeMembership(EmployeeMembership membership)
        {
            return UpdateEmployeeMembership(DatabaseUser, membership);
        }
        public EmployeeMembership UpdateEmployeeMembership(DatabaseUser user, EmployeeMembership membership)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeeMembership(user, membership);
        }
        public void DeleteEmployeeMembership(EmployeeMembership course)
        {
            DeleteEmployeeMembership(DatabaseUser, course);
        }
        public void DeleteEmployeeMembership(DatabaseUser user, EmployeeMembership membership)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeeMembership(user, membership);
        }
        #endregion

        #region pension export
        public byte[] CreatePensionExportFile(long payrollProcessId)
        {
            return CreatePensionExportFile(DatabaseUser, payrollProcessId);
        }
        public byte[] CreatePensionExportFile(DatabaseUser user, long payrollProcessId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreatePensionExportFile(user, payrollProcessId);
        }
        #endregion

        #region HSBC EFT
        public byte[] CreateHSBCEft(long payrollProcessId, string employeeNumber, string eftType)
        {
            return CreateHSBCEft(DatabaseUser, payrollProcessId, employeeNumber, eftType);
        }
        public byte[] CreateHSBCEft(DatabaseUser user, long payrollProcessId, string employeeNumber, string eftType)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreateHSBCEft(user, payrollProcessId, employeeNumber, eftType);
        }
        #endregion

        #region CITI EFT
        public byte[] CreateCITIeft(long payrollProcessId, String eftClientNumber, String employeeNumber, string eftType)
        {
            return CreateCITIeft(DatabaseUser, payrollProcessId, eftClientNumber, employeeNumber, eftType);
        }
        public byte[] CreateCITIeft(DatabaseUser user, long payrollProcessId, String eftClientNumber, String employeeNumber, string eftType)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreateCITIeft(user, payrollProcessId, eftClientNumber, employeeNumber, eftType);
        }
        public byte[] CreateCitiChequeFile(long payrollProcessId, String eftClientNumber, String companyName, String employeeNumber, String originatorId, string eftType)
        {
            return CreateCitiChequeFile(DatabaseUser, payrollProcessId, eftClientNumber, companyName, employeeNumber, originatorId, eftType);
        }
        public byte[] CreateCitiChequeFile(DatabaseUser user, long payrollProcessId, String eftClientNumber, String companyName, String employeeNumber, String originatorId, string eftType)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreateCitiChequeFile(user, payrollProcessId, eftClientNumber, companyName, employeeNumber, originatorId, eftType);
        }
        public byte[] CreateCitiAchFile(long payrollProcessId, String eftClientNumber, String companyName, String companyShortName, String employeeNumber, string eftType)
        {
            return CreateCitiAchFile(DatabaseUser, payrollProcessId, eftClientNumber, companyName, companyShortName, employeeNumber, eftType);
        }
        public byte[] CreateCitiAchFile(DatabaseUser user, long payrollProcessId, String eftClientNumber, String companyName, String companyShortName, String employeeNumber, string eftType)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreateCitiAchFile(user, payrollProcessId, eftClientNumber, companyName, companyShortName, employeeNumber, eftType);
        }
        public byte[] CreateCitiEftAndChequeFile(long payrollProcessId, String citiEftFileName, String citiChequeFileName, String eftClientNumber, String citiChequeAccountNumber, String companyName, String companyShortName, String employeeNumber, String originatorId, string eftType)
        {
            return CreateCitiEftAndChequeFile(DatabaseUser, payrollProcessId, citiEftFileName, citiChequeFileName, eftClientNumber, citiChequeAccountNumber, companyName, companyShortName, employeeNumber, originatorId, eftType);
        }
        public byte[] CreateCitiEftAndChequeFile(DatabaseUser user, long payrollProcessId, String citiEftFileName, String citiChequeFileName, String eftClientNumber, String citiChequeAccountNumber, String companyName, String companyShortName, String employeeNumber, String originatorId, string eftType)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreateCitiEftAndChequeFile(user, payrollProcessId, citiEftFileName, citiChequeFileName, eftClientNumber, citiChequeAccountNumber, companyName, companyShortName, employeeNumber, originatorId, eftType);
        }
        public String GetCitiNamingFormat(String eftFileNameFormat, long payrollprocessid)
        {
            return GetCitiNamingFormat(DatabaseUser, eftFileNameFormat, payrollprocessid);
        }
        public String GetCitiNamingFormat(DatabaseUser user, String eftFileNameFormat, long payrollprocessid)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetCitiNamingFormat(user, eftFileNameFormat, payrollprocessid);
        }
        #endregion

        #region RRSP export
        public byte[] CreateRRSPExport(long payrollProcessId, String rrspHeader, String rrspGroupNumber)
        {
            return CreateRRSPExport(DatabaseUser, payrollProcessId, rrspHeader, rrspGroupNumber);
        }
        public byte[] CreateRRSPExport(DatabaseUser user, long payrollProcessId, String rrspHeader, String rrspGroupNumber)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreateRRSPExport(user, payrollProcessId, rrspHeader, rrspGroupNumber);
        }
        #endregion

        #region CIC Plus Weekly Export
        public byte[] CreateCicPlusWeeklyExportFile(decimal periodYear)
        {
            return CreateCicPlusWeeklyExportFile(DatabaseUser, periodYear);
        }
        public byte[] CreateCicPlusWeeklyExportFile(DatabaseUser user, decimal periodYear)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreateCicPlusWeeklyExportFile(user, periodYear);
        }
        #endregion

        #region empower remittance export and garnishment export
        public byte[] CreateEmpowerRemittanceExport(long payrollProcessId, String revenueQuebecTaxId, bool useQuebecTaxId)
        {
            return CreateEmpowerRemittanceExport(DatabaseUser, payrollProcessId, revenueQuebecTaxId, useQuebecTaxId);
        }
        public byte[] CreateEmpowerRemittanceExport(DatabaseUser user, long payrollProcessId, String revenueQuebecTaxId, bool useQuebecTaxId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreateEmpowerRemittanceExport(user, payrollProcessId, revenueQuebecTaxId, useQuebecTaxId);
        }
        public byte[] CreateGarnishmentExportFile(long payrollProcessId, String empowerFein)
        {
            return CreateGarnishmentExportFile(DatabaseUser, payrollProcessId, empowerFein);
        }
        public byte[] CreateGarnishmentExportFile(DatabaseUser user, long payrollProcessId, String empowerFein)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreateGarnishmentExportFile(user, payrollProcessId, empowerFein);
        }
        #endregion

        #region RBC EFT EDI
        public byte[] GenerateRBCEftEdiFile(long payrollProcessId, RbcEftEdiParameters ediParms, string employeeNumber)
        {
            return GenerateRBCEftEdiFile(DatabaseUser, payrollProcessId, ediParms, employeeNumber);
        }
        public byte[] GenerateRBCEftEdiFile(DatabaseUser user, long payrollProcessId, RbcEftEdiParameters ediParms, string employeeNumber)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GenerateRBCEftEdiFile(user, payrollProcessId, ediParms, employeeNumber);
        }
        #endregion


        #region SCOTIA BANK EDI 
        public byte[] GenerateScotiaBankEdiFile(long payrollProcessId, ScotiaBankEdiParameters ediParms, string employeeNumber)
        {
            return GenerateScotiaBankEdiFile(DatabaseUser, payrollProcessId, ediParms, employeeNumber);
        }
        public byte[] GenerateScotiaBankEdiFile(DatabaseUser user, long payrollProcessId, ScotiaBankEdiParameters ediParms, string employeeNumber)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GenerateScotiaBankEdiFile(user, payrollProcessId, ediParms, employeeNumber);
        }
        #endregion

        #region real custom export
        public byte[] CreateRealCustomExport(long payrollProcessId)
        {
            return CreateRealCustomExport(DatabaseUser, payrollProcessId);
        }
        public byte[] CreateRealCustomExport(DatabaseUser user, long payrollProcessId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreateRealCustomExport(user, payrollProcessId);
        }
        #endregion

        #region social costs export
        public byte[] CreateSocialCostsExport(long payrollProcessId)
        {
            return CreateSocialCostsExport(DatabaseUser, payrollProcessId);
        }
        public byte[] CreateSocialCostsExport(DatabaseUser user, long payrollProcessId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreateSocialCostsExport(user, payrollProcessId);
        }

        #endregion

        #region CRA GARNISHMENT EDI
        public byte[] CreateCraEftGarnishmentFile(long payrollProcessId, RbcEftSourceDeductionParameters garnishmentParms, string employeeNumber)
        {
            return CreateCraEftGarnishmentFile(DatabaseUser, payrollProcessId, garnishmentParms, employeeNumber);
        }
        public byte[] CreateCraEftGarnishmentFile(DatabaseUser user, long payrollProcessId, RbcEftSourceDeductionParameters garnishmentParms, string employeeNumber)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreateCraEftGarnishmentFile(user, payrollProcessId, garnishmentParms, employeeNumber);
        }
        #endregion

        #region DynamicsXmlToWorklinks
        public byte[] GenerateEftFile(long payrollProcessId, String eftClientNumber, bool eftTransmissionFlag, String eftFileIndicator, String companyName, String companyShortName, String employeeNumber, string eftType)
        {
            return GenerateEftFile(DatabaseUser, payrollProcessId, eftClientNumber, eftTransmissionFlag, eftFileIndicator, companyName, companyShortName, employeeNumber, eftType);
        }
        public byte[] GenerateEftFile(DatabaseUser user, long payrollProcessId, String eftClientNumber, bool eftTransmissionFlag, String eftFileIndicator, String companyName, String companyShortName, String employeeNumber, string eftType)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GenerateEftFile(user, payrollProcessId, eftClientNumber, eftTransmissionFlag, eftFileIndicator, companyName, companyShortName, employeeNumber, eftType);
        }
        #endregion

        #region employee banking
        public EmployeeBankingCollection GetEmployeeBanking(long employeeId)
        {
            return GetEmployeeBanking(DatabaseUser, employeeId);
        }
        public EmployeeBankingCollection GetEmployeeBanking(DatabaseUser user, long employeeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeBanking(user, employeeId);
        }
        public EmployeeBanking UpdateEmployeeBanking(EmployeeBanking bank)
        {
            return UpdateEmployeeBanking(DatabaseUser, bank);
        }
        public EmployeeBanking UpdateEmployeeBanking(DatabaseUser user, EmployeeBanking bank)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeeBanking(user, bank);
        }
        public EmployeeBanking InsertEmployeeBanking(EmployeeBanking bank)
        {
            return InsertEmployeeBanking(DatabaseUser, bank);
        }
        public EmployeeBanking InsertEmployeeBanking(DatabaseUser user, EmployeeBanking bank)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeeBanking(user, bank);
        }
        public void DeleteEmployeeBanking(EmployeeBanking bank)
        {
            DeleteEmployeeBanking(DatabaseUser, bank);
        }
        public void DeleteEmployeeBanking(DatabaseUser user, EmployeeBanking bank)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeeBanking(user, bank);
        }
        #endregion

        #region person
        public PersonCollection GetPerson(long personId)
        {
            return GetPerson(DatabaseUser, personId);
        }
        public PersonCollection GetPerson(DatabaseUser user, long personId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPerson(user, personId);
        }
        public void UpdatePerson(Person person)
        {
            UpdatePerson(DatabaseUser, person);
        }
        public void UpdatePerson(DatabaseUser user, Person person)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.UpdatePerson(user, person);
        }
        public Person UpdateUnionPerson(Person person)
        {
            return UpdateUnionPerson(DatabaseUser, person);
        }
        public Person UpdateUnionPerson(DatabaseUser user, Person person)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateUnionPerson(user, person);
        }
        public Person InsertPerson(Person person)
        {
            return InsertPerson(DatabaseUser, person);
        }
        public Person InsertPerson(DatabaseUser user, Person person)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertPerson(user, person);
        }
        public void DeletePerson(Person person)
        {
            DeletePerson(DatabaseUser, person);
        }
        public void DeletePerson(DatabaseUser user, Person person)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeletePerson(user, person);
        }
        #endregion

        #region person address
        public PersonAddressCollection GetPersonAddress(long personId)
        {
            return GetPersonAddress(DatabaseUser, personId);
        }
        public PersonAddressCollection GetPersonAddress(DatabaseUser user, long personId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPersonAddress(user, personId);
        }
        public AddressCollection GetAddress(long addressId)
        {
            return GetAddress(DatabaseUser, addressId);
        }
        public AddressCollection GetAddress(DatabaseUser user, long addressId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetAddress(user, addressId);
        }
        public PersonAddress UpdatePersonAddress(PersonAddress address, bool handlePreviousAddress, System.Collections.Generic.List<PersonAddress> theData)
        {
            return UpdatePersonAddress(DatabaseUser, address, handlePreviousAddress, theData);
        }
        public PersonAddress UpdatePersonAddress(DatabaseUser user, PersonAddress address, bool handlePreviousAddress, System.Collections.Generic.List<PersonAddress> theData)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdatePersonAddress(user, address, handlePreviousAddress, theData);
        }
        public PersonAddress InsertPersonAddress(PersonAddress address, System.Collections.Generic.List<PersonAddress> theData)
        {
            return InsertPersonAddress(DatabaseUser, address, theData);
        }
        public PersonAddress InsertPersonAddress(DatabaseUser user, PersonAddress address, System.Collections.Generic.List<PersonAddress> theData)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertPersonAddress(user, address, theData);
        }
        public void DeletePersonAddress(PersonAddress address)
        {
            DeletePersonAddress(DatabaseUser, address);
        }
        public void DeletePersonAddress(DatabaseUser user, PersonAddress address)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeletePersonAddress(user, address);
        }
        #endregion

        #region person ContactChannel
        public PersonContactChannelCollection GetPersonContactChannel(long personId, String contactChannelCategoryCode)
        {
            return GetPersonContactChannel(DatabaseUser, personId, contactChannelCategoryCode);
        }
        public PersonContactChannelCollection GetPersonContactChannel(DatabaseUser user, long personId, String contactChannelCategoryCode)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPersonContactChannel(user, personId, contactChannelCategoryCode);
        }
        public PersonContactChannel UpdatePersonContactChannel(PersonContactChannel contact, System.Collections.Generic.List<PersonContactChannel> theData)
        {
            return UpdatePersonContactChannel(DatabaseUser, contact, theData);
        }
        public PersonContactChannel UpdatePersonContactChannel(DatabaseUser user, PersonContactChannel contact, System.Collections.Generic.List<PersonContactChannel> theData)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdatePersonContactChannel(user, contact, theData);
        }
        public PersonContactChannel InsertPersonContactChannel(PersonContactChannel contact, System.Collections.Generic.List<PersonContactChannel> theData)
        {
            return InsertPersonContactChannel(DatabaseUser, contact, theData);
        }
        public PersonContactChannel InsertPersonContactChannel(DatabaseUser user, PersonContactChannel contact, System.Collections.Generic.List<PersonContactChannel> theData)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertPersonContactChannel(user, contact, theData);
        }
        public void DeletePersonContactChannel(PersonContactChannel contact)
        {
            DeletePersonContactChannel(DatabaseUser, contact);
        }
        public void DeletePersonContactChannel(DatabaseUser user, PersonContactChannel contact)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeletePersonContactChannel(user, contact);
        }
        #endregion

        #region ContactChannel
        public ContactChannelCollection GetContactChannel(long contactChannelId)
        {
            return GetContactChannel(DatabaseUser, contactChannelId);
        }
        public ContactChannelCollection GetContactChannel(DatabaseUser user, long contactChannelId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetContactChannel(user, contactChannelId);
        }
        #endregion

        #region government identification number type template
        public GovernmentIdentificationNumberTypeTemplateCollection GetGovernmentIdentificationNumberTypeTemplate(String numberTypeCode)
        {
            return GetGovernmentIdentificationNumberTypeTemplate(DatabaseUser, numberTypeCode);
        }
        public GovernmentIdentificationNumberTypeTemplateCollection GetGovernmentIdentificationNumberTypeTemplate(DatabaseUser user, String numberTypeCode)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetGovernmentIdentificationNumberTypeTemplate(user, numberTypeCode);
        }
        #endregion

        #region grievance
        public GrievanceCollection GetGrievance(long id)
        {
            return GetGrievance(DatabaseUser, id);
        }
        public GrievanceCollection GetGrievance(DatabaseUser user, long id)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetGrievance(user, id);
        }
        public Grievance UpdateGrievance(Grievance data)
        {
            return UpdateGrievance(DatabaseUser, data);
        }
        public Grievance UpdateGrievance(DatabaseUser user, Grievance data)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateGrievance(user, data);
        }
        public Grievance InsertGrievance(Grievance data)
        {
            return InsertGrievance(DatabaseUser, data);
        }
        public Grievance InsertGrievance(DatabaseUser user, Grievance data)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertGrievance(user, data);
        }
        public void DeleteGrievance(Grievance data)
        {
            DeleteGrievance(DatabaseUser, data);
        }
        public void DeleteGrievance(DatabaseUser user, Grievance data)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteGrievance(user, data);
        }
        #endregion

        #region grievance action
        public GrievanceActionCollection GetGrievanceAction(long id)
        {
            return GetGrievanceAction(DatabaseUser, id);
        }
        public GrievanceActionCollection GetGrievanceAction(DatabaseUser user, long id)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetGrievanceAction(user, id);
        }
        public GrievanceAction UpdateGrievanceAction(GrievanceAction data)
        {
            return UpdateGrievanceAction(DatabaseUser, data);
        }
        public GrievanceAction UpdateGrievanceAction(DatabaseUser user, GrievanceAction data)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateGrievanceAction(user, data);
        }
        public GrievanceAction InsertGrievanceAction(GrievanceAction data)
        {
            return InsertGrievanceAction(DatabaseUser, data);
        }
        public GrievanceAction InsertGrievanceAction(DatabaseUser user, GrievanceAction data)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertGrievanceAction(user, data);
        }
        public void DeleteGrievanceAction(GrievanceAction data)
        {
            DeleteGrievanceAction(DatabaseUser, data);
        }
        public void DeleteGrievanceAction(DatabaseUser user, GrievanceAction data)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteGrievanceAction(user, data);
        }
        #endregion

        #region grievance report
        public GrievanceCollection GetGrievanceSummary(GrievanceCriteria criteria)
        {
            return GetGrievanceSummary(DatabaseUser, criteria);
        }
        public GrievanceCollection GetGrievanceSummary(DatabaseUser user, GrievanceCriteria criteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetGrievanceSummary(user, criteria);
        }
        public EmployeeSummaryCollection GetEmployeeSummary(EmployeeCriteria criteria)
        {
            return GetEmployeeSummary(DatabaseUser, criteria);
        }
        public EmployeeSummaryCollection GetEmployeeSummary(DatabaseUser user, EmployeeCriteria criteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeSummary(user, criteria);
        }
        #endregion

        #region labour union
        public LabourUnionSummaryCollection GetLabourUnion(long? labourUnionId)
        {
            return GetLabourUnion(DatabaseUser, labourUnionId);
        }
        public LabourUnionSummaryCollection GetLabourUnion(DatabaseUser user, long? labourUnionId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetLabourUnion(user, labourUnionId);
        }
        public LabourUnionSummary UpdateUnionContactData(LabourUnionSummary union)
        {
            return UpdateUnionContactData(DatabaseUser, union);
        }
        public LabourUnionSummary UpdateUnionContactData(DatabaseUser user, LabourUnionSummary union)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateUnionContactData(user, union);
        }
        public LabourUnion UpdateUnionContact(LabourUnion union)
        {
            return UpdateUnionContact(DatabaseUser, union);
        }
        public LabourUnion UpdateUnionContact(DatabaseUser user, LabourUnion union)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateUnionContact(user, union);
        }
        public LabourUnionSummary InsertUnionContactInformationSummaryData(LabourUnionSummary union)
        {
            return InsertUnionContactInformationSummaryData(DatabaseUser, union);
        }
        public LabourUnionSummary InsertUnionContactInformationSummaryData(DatabaseUser user, LabourUnionSummary union)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertUnionContactInformationSummaryData(user, union);
        }
        public LabourUnion InsertUnionContact(LabourUnion union)
        {
            return InsertUnionContact(DatabaseUser, union);
        }
        public LabourUnion InsertUnionContact(DatabaseUser user, LabourUnion union)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertUnionContact(user, union);
        }
        public void DeleteUnionContact(LabourUnion union)
        {
            DeleteUnionContact(DatabaseUser, union);
        }
        public void DeleteUnionContact(DatabaseUser user, LabourUnion union)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteUnionContact(user, union);
        }
        #endregion

        #region Union Collective Agreement
        public UnionCollectiveAgreementCollection GetUnionCollectiveAgreement(long? labourUnionId)
        {
            return GetUnionCollectiveAgreement(DatabaseUser, labourUnionId);
        }
        public UnionCollectiveAgreementCollection GetUnionCollectiveAgreement(DatabaseUser user, long? labourUnionId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetUnionCollectiveAgreement(user, labourUnionId);
        }
        public UnionCollectiveAgreement UpdateUnionCollectiveAgreement(UnionCollectiveAgreement collectiveAgreement)
        {
            return UpdateUnionCollectiveAgreement(DatabaseUser, collectiveAgreement);
        }
        public UnionCollectiveAgreement UpdateUnionCollectiveAgreement(DatabaseUser user, UnionCollectiveAgreement collectiveAgreement)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateUnionCollectiveAgreement(user, collectiveAgreement);
        }
        public UnionCollectiveAgreement InsertUnionCollectiveAgreement(UnionCollectiveAgreement collectiveAgreement)
        {
            return InsertUnionCollectiveAgreement(DatabaseUser, collectiveAgreement);
        }
        public UnionCollectiveAgreement InsertUnionCollectiveAgreement(DatabaseUser user, UnionCollectiveAgreement collectiveAgreement)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertUnionCollectiveAgreement(user, collectiveAgreement);
        }
        public void DeleteUnionCollectiveAgreement(UnionCollectiveAgreement collectiveAgreement)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteUnionCollectiveAgreement(DatabaseUser, collectiveAgreement);
        }
        public void DeleteUnionCollectiveAgreement(DatabaseUser user, UnionCollectiveAgreement collectiveAgreement)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteUnionCollectiveAgreement(user, collectiveAgreement);
        }
        #endregion

        #region employee license certificate
        public EmployeeLicenseCertificateCollection GetEmployeeLicenseCertificate(long employeeId)
        {
            return GetEmployeeLicenseCertificate(DatabaseUser, employeeId);
        }
        public EmployeeLicenseCertificateCollection GetEmployeeLicenseCertificate(DatabaseUser user, long employeeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeLicenseCertificate(user, employeeId);
        }
        public EmployeeLicenseCertificate UpdateEmployeeLicenseCertificate(EmployeeLicenseCertificate licenseCertificate)
        {
            return UpdateEmployeeLicenseCertificate(DatabaseUser, licenseCertificate);
        }
        public EmployeeLicenseCertificate UpdateEmployeeLicenseCertificate(DatabaseUser user, EmployeeLicenseCertificate licenseCertificate)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeeLicenseCertificate(user, licenseCertificate);
        }
        public EmployeeLicenseCertificate InsertEmployeeLicenseCertificate(EmployeeLicenseCertificate licenseCertificate)
        {
            return InsertEmployeeLicenseCertificate(DatabaseUser, licenseCertificate);
        }
        public EmployeeLicenseCertificate InsertEmployeeLicenseCertificate(DatabaseUser user, EmployeeLicenseCertificate licenseCertificate)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeeLicenseCertificate(user, licenseCertificate);
        }
        public void DeleteEmployeeLicenseCertificate(EmployeeLicenseCertificate licenseCertificate)
        {
            DeleteEmployeeLicenseCertificate(DatabaseUser, licenseCertificate);
        }
        public void DeleteEmployeeLicenseCertificate(DatabaseUser user, EmployeeLicenseCertificate licenseCertificate)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeeLicenseCertificate(user, licenseCertificate);
        }
        #endregion

        #region employee paycode
        public bool CheckRecurringIncomeCode()
        {
            return CheckRecurringIncomeCode(DatabaseUser);
        }
        public bool CheckRecurringIncomeCode(DatabaseUser user)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CheckRecurringIncomeCode(user);
        }
        public EmployeePaycodeCollection GetEmployeePaycode(long employeeId)
        {
            return GetEmployeePaycode(DatabaseUser, employeeId);
        }
        public EmployeePaycodeCollection GetEmployeePaycode(DatabaseUser user, long employeeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeePaycode(user, employeeId);
        }
        public EmployeePaycode UpdateEmployeePaycode(EmployeePaycode employeePaycode)
        {
            return UpdateEmployeePaycode(DatabaseUser, employeePaycode);
        }
        public EmployeePaycode UpdateEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeePaycode(user, employeePaycode);
        }
        public EmployeePaycode InsertEmployeePaycode(EmployeePaycode employeePaycode)
        {
            return InsertEmployeePaycode(DatabaseUser, employeePaycode);
        }
        public EmployeePaycode InsertEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeePaycode(user, employeePaycode);
        }
        public void DeleteEmployeePaycode(EmployeePaycode employeePaycode)
        {
            DeleteEmployeePaycode(DatabaseUser, employeePaycode);
        }
        public void DeleteEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeePaycode(user, employeePaycode);
        }
        public decimal CalculateRate(long employeeId, decimal baseRate, decimal rateFactor, bool vacationCalculationOverrideFlag)
        {
            return CalculateRate(DatabaseUser, employeeId, baseRate, rateFactor, vacationCalculationOverrideFlag);
        }
        public decimal CalculateRate(DatabaseUser user, long employeeId, decimal baseRate, decimal rateFactor, bool vacationCalculationOverrideFlag)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CalculateRate(user, employeeId, baseRate, rateFactor, vacationCalculationOverrideFlag);
        }
        public bool ValidatePaycodeEntryData(EmployeePaycode employeePaycode)
        {
            return ValidatePaycodeEntryData(DatabaseUser, employeePaycode);
        }
        public bool ValidatePaycodeEntryData(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ValidatePaycodeEntryData(user, employeePaycode);
        }
        #endregion

        #region employee award honour
        public EmployeeAwardHonourCollection GetEmployeeAwardHonour(long employeeId)
        {
            return GetEmployeeAwardHonour(DatabaseUser, employeeId);
        }
        public EmployeeAwardHonourCollection GetEmployeeAwardHonour(DatabaseUser user, long employeeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeAwardHonour(user, employeeId);
        }
        public EmployeeAwardHonour UpdateEmployeeAwardHonour(EmployeeAwardHonour awardHonour)
        {
            return UpdateEmployeeAwardHonour(DatabaseUser, awardHonour);
        }
        public EmployeeAwardHonour UpdateEmployeeAwardHonour(DatabaseUser user, EmployeeAwardHonour awardHonour)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeeAwardHonour(user, awardHonour);
        }
        public EmployeeAwardHonour InsertEmployeeAwardHonour(EmployeeAwardHonour awardHonour)
        {
            return InsertEmployeeAwardHonour(DatabaseUser, awardHonour);
        }
        public EmployeeAwardHonour InsertEmployeeAwardHonour(DatabaseUser user, EmployeeAwardHonour awardHonour)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeeAwardHonour(user, awardHonour);
        }
        public void DeleteEmployeeAwardHonour(EmployeeAwardHonour awardHonour)
        {
            DeleteEmployeeAwardHonour(DatabaseUser, awardHonour);
        }
        public void DeleteEmployeeAwardHonour(DatabaseUser user, EmployeeAwardHonour awardHonour)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeeAwardHonour(user, awardHonour);
        }
        #endregion

        #region employee discipline
        public EmployeeDisciplineCollection GetEmployeeDiscipline(long employeeId)
        {
            return GetEmployeeDiscipline(DatabaseUser, employeeId);
        }
        public EmployeeDisciplineCollection GetEmployeeDiscipline(DatabaseUser user, long employeeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeDiscipline(user, employeeId);
        }
        public EmployeeDiscipline UpdateEmployeeDiscipline(EmployeeDiscipline discipline)
        {
            return UpdateEmployeeDiscipline(DatabaseUser, discipline);
        }
        public EmployeeDiscipline UpdateEmployeeDiscipline(DatabaseUser user, EmployeeDiscipline discipline)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeeDiscipline(user, discipline);
        }
        public EmployeeDiscipline InsertEmployeeDiscipline(EmployeeDiscipline discipline)
        {
            return InsertEmployeeDiscipline(DatabaseUser, discipline);
        }
        public EmployeeDiscipline InsertEmployeeDiscipline(DatabaseUser user, EmployeeDiscipline discipline)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeeDiscipline(user, discipline);
        }
        public void DeleteEmployeeDiscipline(EmployeeDiscipline discipline)
        {
            DeleteEmployeeDiscipline(DatabaseUser, discipline);
        }
        public void DeleteEmployeeDiscipline(DatabaseUser user, EmployeeDiscipline discipline)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeeDiscipline(user, discipline);
        }
        #endregion

        #region employee discipline action
        public EmployeeDisciplineActionCollection GetEmployeeDisciplineAction(long employeeDisciplineId)
        {
            return GetEmployeeDisciplineAction(DatabaseUser, employeeDisciplineId);
        }
        public EmployeeDisciplineActionCollection GetEmployeeDisciplineAction(DatabaseUser user, long employeeDisciplineId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeDisciplineAction(user, employeeDisciplineId);
        }
        public EmployeeDisciplineAction UpdateEmployeeDisciplineAction(EmployeeDisciplineAction disciplineAction)
        {
            return UpdateEmployeeDisciplineAction(DatabaseUser, disciplineAction);
        }
        public EmployeeDisciplineAction UpdateEmployeeDisciplineAction(DatabaseUser user, EmployeeDisciplineAction disciplineAction)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeeDisciplineAction(user, disciplineAction);
        }
        public EmployeeDisciplineAction InsertEmployeeDisciplineAction(EmployeeDisciplineAction disciplineAction)
        {
            return InsertEmployeeDisciplineAction(DatabaseUser, disciplineAction);
        }
        public EmployeeDisciplineAction InsertEmployeeDisciplineAction(DatabaseUser user, EmployeeDisciplineAction disciplineAction)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeeDisciplineAction(user, disciplineAction);
        }
        public void DeleteEmployeeDisciplineAction(EmployeeDisciplineAction disciplineAction)
        {
            DeleteEmployeeDisciplineAction(DatabaseUser, disciplineAction);
        }
        public void DeleteEmployeeDisciplineAction(DatabaseUser user, EmployeeDisciplineAction disciplineAction)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeeDisciplineAction(user, disciplineAction);
        }
        #endregion

        #region organization unit management
        public OrganizationUnitCollection GetOrganizationUnitByOrganizationUnitLevelId(OrganizationUnitCriteria criteria)
        {
            return GetOrganizationUnitByOrganizationUnitLevelId(DatabaseUser, criteria);
        }
        public OrganizationUnitCollection GetOrganizationUnitByOrganizationUnitLevelId(DatabaseUser user, OrganizationUnitCriteria criteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetOrganizationUnitByOrganizationUnitLevelId(user, criteria);
        }
        public OrganizationUnit UpdateOrganizationUnit(OrganizationUnit item)
        {
            return UpdateOrganizationUnit(DatabaseUser, item);
        }
        public OrganizationUnitAssociationCollection GetOrganizationUnitAssociation(OrganizationUnitCriteria criteria)
        {
            return GetOrganizationUnitAssociation(DatabaseUser, criteria);
        }
        public OrganizationUnitAssociationCollection GetOrganizationUnitAssociation(DatabaseUser user, OrganizationUnitCriteria criteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetOrganizationUnitAssociation(user, criteria);
        }
        public OrganizationUnitAssociationCollection InsertOrganizationUnitAssociation(OrganizationUnitAssociation item)
        {
            return InsertOrganizationUnitAssociation(DatabaseUser, item);
        }
        public OrganizationUnitAssociationCollection InsertOrganizationUnitAssociation(DatabaseUser user, OrganizationUnitAssociation item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertOrganizationUnitAssociation(user, item);
        }
        public void DeleteOrganizationUnitAssociation(OrganizationUnitAssociation item)
        {
            DeleteOrganizationUnitAssociation(DatabaseUser, item);
        }
        public void DeleteOrganizationUnitAssociation(DatabaseUser user, OrganizationUnitAssociation item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteOrganizationUnitAssociation(user, item);
        }
        public OrganizationUnit UpdateOrganizationUnit(DatabaseUser user, OrganizationUnit item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateOrganizationUnit(user, item);
        }
        public void DeleteOrganizationUnit(OrganizationUnit item)
        {
            DeleteOrganizationUnit(DatabaseUser, item);
        }
        public void DeleteOrganizationUnit(DatabaseUser user, OrganizationUnit item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteOrganizationUnit(user, item);
        }
        public OrganizationUnit InsertOrganizationUnit(OrganizationUnit item)
        {
            return InsertOrganizationUnit(DatabaseUser, item);
        }
        public OrganizationUnit InsertOrganizationUnit(DatabaseUser user, OrganizationUnit item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertOrganizationUnit(user, item);
        }
        public OrganizationUnitCollection GetOrganizationUnit(long? organizationUnitId)
        {
            return GetOrganizationUnit(DatabaseUser, organizationUnitId);
        }
        public OrganizationUnitCollection GetOrganizationUnit(DatabaseUser user, long? organizationUnitId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetOrganizationUnit(user, organizationUnitId);
        }
        public OrganizationUnitCollection GetOrganizationUnitByParentOrganizationUnitId(long? parentOrganizationUnitId)
        {
            return GetOrganizationUnitByParentOrganizationUnitId(DatabaseUser, parentOrganizationUnitId);
        }
        public OrganizationUnitCollection GetOrganizationUnitByParentOrganizationUnitId(DatabaseUser user, long? parentOrganizationUnitId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetOrganizationUnitByParentOrganizationUnitId(user, parentOrganizationUnitId);
        }
        public OrganizationUnitCollection GetOrganizationUnitWithLevelDescription(long? organizationUnitId)
        {
            return GetOrganizationUnitWithLevelDescription(DatabaseUser, organizationUnitId);
        }
        public OrganizationUnitCollection GetOrganizationUnitWithLevelDescription(DatabaseUser user, long? organizationUnitId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetOrganizationUnitWithLevelDescription(user, organizationUnitId);
        }
        public OrganizationUnitLevelCollection GetOrganizationUnitLevel()
        {
            return GetOrganizationUnitLevel(DatabaseUser);
        }
        public OrganizationUnitLevelCollection GetOrganizationUnitLevel(DatabaseUser user)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetOrganizationUnitLevel(user);
        }

        public OrganizationUnitLevelDescriptionCollection GetOrganizationUnitLevelDescription(long organizationUnitLevelId)
        {
            return GetOrganizationUnitLevelDescription(DatabaseUser, organizationUnitLevelId);
        }
        public OrganizationUnitLevelDescriptionCollection GetOrganizationUnitLevelDescription(DatabaseUser user, long organizationUnitLevelId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetOrganizationUnitLevelDescription(user, organizationUnitLevelId);
        }

        public OrganizationUnitLevel InsertOrganizationUnitLevel(OrganizationUnitLevel item)
        {
            return InsertOrganizationUnitLevel(DatabaseUser, item);
        }
        public OrganizationUnitLevel InsertOrganizationUnitLevel(DatabaseUser user, OrganizationUnitLevel item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertOrganizationUnitLevel(user, item);
        }
        public OrganizationUnitLevel UpdateOrganizationUnitLevel(OrganizationUnitLevel item)
        {
            return UpdateOrganizationUnitLevel(DatabaseUser, item);
        }
        public OrganizationUnitLevel UpdateOrganizationUnitLevel(DatabaseUser user, OrganizationUnitLevel item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateOrganizationUnitLevel(user, item);
        }
        public void DeleteOrganizationUnitLevel(OrganizationUnitLevel item)
        {
            DeleteOrganizationUnitLevel(DatabaseUser, item);
        }
        public void DeleteOrganizationUnitLevel(DatabaseUser user, OrganizationUnitLevel item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteOrganizationUnitLevel(user, item);
        }
        public bool CheckEmployeeExist()
        {
            return CheckEmployeeExist(DatabaseUser);
        }
        public bool CheckEmployeeExist(DatabaseUser user)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CheckEmployeeExist(user);
        }
        #endregion

        #region employee termination
        public EmployeeTerminationOtherCollection GetEmployeeTerminationOther(long employeePositionId)
        {
            return GetEmployeeTerminationOther(DatabaseUser, employeePositionId);
        }
        public EmployeeTerminationOtherCollection GetEmployeeTerminationOther(DatabaseUser user, long employeePositionId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeTerminationOther(user, employeePositionId);
        }
        public EmployeeTerminationOther UpdateEmployeeTerminationOther(EmployeeTerminationOther item)
        {
            return UpdateEmployeeTerminationOther(DatabaseUser, item);
        }
        public EmployeeTerminationOther UpdateEmployeeTerminationOther(DatabaseUser user, EmployeeTerminationOther item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeeTerminationOther(user, item);
        }
        public void DeleteEmployeeTerminationOther(EmployeeTerminationOther item)
        {
            DeleteEmployeeTerminationOther(DatabaseUser, item);
        }
        public void DeleteEmployeeTerminationOther(DatabaseUser user, EmployeeTerminationOther item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeeTerminationOther(user, item);
        }
        public EmployeeTerminationOther InsertEmployeeTerminationOther(EmployeeTerminationOther item)
        {
            return InsertEmployeeTerminationOther(DatabaseUser, item);
        }
        public EmployeeTerminationOther InsertEmployeeTerminationOther(DatabaseUser user, EmployeeTerminationOther item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeeTerminationOther(user, item);
        }
        public EmployeeTerminationRoeCollection SelectRoe(long employeePositionId)
        {
            return SelectRoe(DatabaseUser, employeePositionId);
        }
        public EmployeeTerminationRoeCollection SelectRoe(DatabaseUser user, long employeePositionId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.SelectRoe(user, employeePositionId);
        }
        public EmployeeTerminationRoe UpdateRoe(EmployeeTerminationRoe item)
        {
            return UpdateRoe(DatabaseUser, item);
        }
        public EmployeeTerminationRoe UpdateRoe(DatabaseUser user, EmployeeTerminationRoe item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateRoe(user, item);
        }
        public void DeleteRoe(EmployeeTerminationRoe item)
        {
            DeleteRoe(DatabaseUser, item);
        }
        public void DeleteRoe(DatabaseUser user, EmployeeTerminationRoe item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteRoe(user, item);
        }
        public EmployeeTerminationRoe InsertRoe(EmployeeTerminationRoe item)
        {
            return InsertRoe(DatabaseUser, item);
        }
        public EmployeeTerminationRoe InsertRoe(DatabaseUser user, EmployeeTerminationRoe item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertRoe(user, item);
        }
        #endregion

        #region employee position
        public EmployeePositionSummaryCollection GetEmployeePositionSummary(EmployeePositionCriteria criteria)
        {
            return GetEmployeePositionSummary(DatabaseUser, criteria);
        }
        public EmployeePositionSummaryCollection GetEmployeePositionSummary(DatabaseUser user, EmployeePositionCriteria criteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeePositionSummary(user, criteria);
        }
        public EmployeePositionCollection GetEmployeePosition(EmployeePositionCriteria criteria)
        {
            return GetEmployeePosition(DatabaseUser, criteria);
        }
        public EmployeePositionCollection GetEmployeePosition(DatabaseUser user, EmployeePositionCriteria criteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeePosition(user, criteria);
        }
        public EmployeePosition UpdateEmployeePosition(EmployeePosition item, bool autoAddSecondaryPositions)
        {
            return UpdateEmployeePosition(DatabaseUser, item, autoAddSecondaryPositions);
        }
        public EmployeePosition UpdateEmployeePosition(DatabaseUser user, EmployeePosition item, bool autoAddSecondaryPositions)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeePosition(user, item, autoAddSecondaryPositions);
        }
        public bool IsProcessDateOpen(long? payrollProcessId)
        {
            return IsProcessDateOpen(DatabaseUser, payrollProcessId);
        }
        public bool IsProcessDateOpen(DatabaseUser user, long? payrollProcessId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.IsProcessDateOpen(user, payrollProcessId);
        }
        public void DeleteEmployeePosition(EmployeePosition position, EmployeePosition formerPosition)
        {
            DeleteEmployeePosition(DatabaseUser, position, formerPosition);
        }
        public void DeleteEmployeePosition(DatabaseUser user, EmployeePosition position, EmployeePosition formerPosition)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeePosition(user, position, formerPosition);
        }
        public void DeleteEmployeeTermination(EmployeePosition employeePosition)
        {
            DeleteEmployeeTermination(DatabaseUser, employeePosition);
        }
        public void DeleteEmployeeTermination(DatabaseUser user, EmployeePosition employeePosition)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeeTermination(user, employeePosition);
        }
        public EmployeePosition InsertEmployeePosition(EmployeePosition position, EmployeePosition formerPosition, DateTime? payrollPeriodCutoffDate, String payrollProcessGroupCode, String roeDefaultContactLastName, String roeDefaultContactFirstName, String roeDefaultContactTelephone, String roeDefaultContactTelephoneExt, bool autoAddSecondaryPositions)
        {
            return InsertEmployeePosition(DatabaseUser, position, formerPosition, payrollPeriodCutoffDate, payrollProcessGroupCode, roeDefaultContactLastName, roeDefaultContactFirstName, roeDefaultContactTelephone, roeDefaultContactTelephoneExt, autoAddSecondaryPositions);
        }
        public EmployeePosition InsertEmployeePosition(DatabaseUser user, EmployeePosition position, EmployeePosition formerPosition, DateTime? payrollPeriodCutoffDate, String payrollProcessGroupCode, String roeDefaultContactLastName, String roeDefaultContactFirstName, String roeDefaultContactTelephone, String roeDefaultContactTelephoneExt, bool autoAddSecondaryPositions)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeePosition(user, position, formerPosition, payrollPeriodCutoffDate, payrollProcessGroupCode, roeDefaultContactLastName, roeDefaultContactFirstName, roeDefaultContactTelephone, roeDefaultContactTelephoneExt, autoAddSecondaryPositions);
        }
        public EmployeePosition CreateEmployeePosition()
        {
            return CreateEmployeePosition(DatabaseUser);
        }
        public EmployeePosition CreateEmployeePosition(DatabaseUser user)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreateEmployeePosition(user);
        }
        #endregion

        #region employee position workday
        public EmployeePositionWorkdayCollection GetEmployeePositionWorkday(long employeePositionId)
        {
            return GetEmployeePositionWorkday(DatabaseUser, employeePositionId);
        }
        public EmployeePositionWorkdayCollection GetEmployeePositionWorkday(DatabaseUser user, long employeePositionId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeePositionWorkday(user, employeePositionId);
        }
        public void InsertEmployeePositionWorkday(EmployeePositionWorkday workday)
        {
            InsertEmployeePositionWorkday(DatabaseUser, workday);
        }
        public void InsertEmployeePositionWorkday(DatabaseUser user, EmployeePositionWorkday workday)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.InsertEmployeePositionWorkday(user, workday);
        }
        public void UpdateEmployeePositionWorkday(EmployeePositionWorkday workday)
        {
            UpdateEmployeePositionWorkday(DatabaseUser, workday);
        }
        public void UpdateEmployeePositionWorkday(DatabaseUser user, EmployeePositionWorkday workday)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.UpdateEmployeePositionWorkday(user, workday);
        }
        public void DeleteEmployeePositionWorkday(EmployeePositionWorkday workday)
        {
            DeleteEmployeePositionWorkday(DatabaseUser, workday);
        }
        public void DeleteEmployeePositionWorkday(DatabaseUser user, EmployeePositionWorkday workday)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeePositionWorkday(user, workday);
        }
        #endregion

        #region employee position organization unit level report
        public EmployeePositionOrganizationUnitCollection GetEmployeePositionOrganizationUnitLevelSummary(long employeePositionId)
        {
            return GetEmployeePositionOrganizationUnitLevelSummary(DatabaseUser, employeePositionId);
        }
        public EmployeePositionOrganizationUnitCollection GetEmployeePositionOrganizationUnitLevelSummary(DatabaseUser user, long employeePositionId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeePositionOrganizationUnitLevelSummary(user, employeePositionId);
        }
        #endregion

        #region employee position secondary
        public EmployeePositionSecondaryCollection GetEmployeePositionSecondary(long? employeePositionSecondaryId, long? employeePositionId, String languageCode, string organizationUnit)
        {
            return GetEmployeePositionSecondary(DatabaseUser, employeePositionSecondaryId, employeePositionId, languageCode, organizationUnit);
        }
        public EmployeePositionSecondaryCollection GetEmployeePositionSecondary(DatabaseUser user, long? employeePositionSecondaryId, long? employeePositionId, String languageCode, string organizationUnit)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeePositionSecondary(user, employeePositionSecondaryId, employeePositionId, languageCode, organizationUnit);
        }
        public EmployeePositionSecondary InsertEmployeePositionSecondary(EmployeePositionSecondary item)
        {
            return InsertEmployeePositionSecondary(DatabaseUser, item);
        }
        public EmployeePositionSecondary InsertEmployeePositionSecondary(DatabaseUser user, EmployeePositionSecondary item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeePositionSecondary(user, item);
        }
        public EmployeePositionSecondary UpdateEmployeePositionSecondary(EmployeePositionSecondary item)
        {
            return UpdateEmployeePositionSecondary(DatabaseUser, item);
        }
        public EmployeePositionSecondary UpdateEmployeePositionSecondary(DatabaseUser user, EmployeePositionSecondary item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeePositionSecondary(user, item);
        }
        public void DeleteEmployeePositionSecondary(EmployeePositionSecondary item)
        {
            DeleteEmployeePositionSecondary(DatabaseUser, item);
        }
        public void DeleteEmployeePositionSecondary(DatabaseUser user, EmployeePositionSecondary item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeePositionSecondary(user, item);
        }
        #endregion

        #region employee position secondary organization unit
        public EmployeePositionSecondaryOrganizationUnitCollection GetEmployeePositionSecondaryOrganizationUnit(long? employeePositionSecondaryId)
        {
            return GetEmployeePositionSecondaryOrganizationUnit(DatabaseUser, employeePositionSecondaryId);
        }
        public EmployeePositionSecondaryOrganizationUnitCollection GetEmployeePositionSecondaryOrganizationUnit(DatabaseUser user, long? employeePositionSecondaryId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeePositionSecondaryOrganizationUnit(user, employeePositionSecondaryId);
        }
        public void DeleteEmployeePositionSecondaryOrganizationUnit(EmployeePositionSecondaryOrganizationUnit item)
        {
            DeleteEmployeePositionSecondaryOrganizationUnit(DatabaseUser, item);
        }
        public void DeleteEmployeePositionSecondaryOrganizationUnit(DatabaseUser user, EmployeePositionSecondaryOrganizationUnit item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeePositionSecondaryOrganizationUnit(user, item);
        }
        #endregion

        #region employee wsib health and safety report
        public EmployeeWsibHealthAndSafetyReportCollection GetEmployeeWsibHealthAndSafetyReport(long employeeId)
        {
            return GetEmployeeWsibHealthAndSafetyReport(DatabaseUser, employeeId);
        }
        public EmployeeWsibHealthAndSafetyReportCollection GetEmployeeWsibHealthAndSafetyReport(DatabaseUser user, long employeeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeWsibHealthAndSafetyReport(user, employeeId);
        }
        public EmployeeWsibHealthAndSafetyReportPersonCollection GetEmployeeWsibHealthAndSafetyReportPerson(long employeeId)
        {
            return GetEmployeeWsibHealthAndSafetyReportPerson(DatabaseUser, employeeId);
        }
        public EmployeeWsibHealthAndSafetyReportPersonCollection GetEmployeeWsibHealthAndSafetyReportPerson(DatabaseUser user, long employeeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeWsibHealthAndSafetyReportPerson(user, employeeId);
        }
        public EmployeeWsibHealthAndSafetyReport InsertEmployeeWsibHealthAndSafetyReport(EmployeeWsibHealthAndSafetyReport item)
        {
            return InsertEmployeeWsibHealthAndSafetyReport(DatabaseUser, item);
        }
        public EmployeeWsibHealthAndSafetyReport InsertEmployeeWsibHealthAndSafetyReport(DatabaseUser user, EmployeeWsibHealthAndSafetyReport item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeeWsibHealthAndSafetyReport(user, item);
        }
        public EmployeeWsibHealthAndSafetyReport UpdateEmployeeWsibHealthAndSafetyReport(EmployeeWsibHealthAndSafetyReport item)
        {
            return UpdateEmployeeWsibHealthAndSafetyReport(DatabaseUser, item);
        }
        public EmployeeWsibHealthAndSafetyReport UpdateEmployeeWsibHealthAndSafetyReport(DatabaseUser user, EmployeeWsibHealthAndSafetyReport item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeeWsibHealthAndSafetyReport(user, item);
        }
        public void DeleteEmployeeWsibHealthAndSafetyReport(EmployeeWsibHealthAndSafetyReport item)
        {
            DeleteEmployeeWsibHealthAndSafetyReport(DatabaseUser, item);
        }
        public void DeleteEmployeeWsibHealthAndSafetyReport(DatabaseUser user, EmployeeWsibHealthAndSafetyReport item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeeWsibHealthAndSafetyReport(user, item);
        }
        #endregion

        #region statutory deduction
        public StatutoryDeductionCollection GetStatutoryDeduction(long id)
        {
            return GetStatutoryDeduction(DatabaseUser, id);
        }
        public StatutoryDeductionCollection GetStatutoryDeduction(DatabaseUser user, long id)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetStatutoryDeduction(user, id);
        }
        public StatutoryDeduction UpdateStatutoryDeduction(StatutoryDeduction item)
        {
            return UpdateStatutoryDeduction(DatabaseUser, item);
        }
        public StatutoryDeduction UpdateStatutoryDeduction(DatabaseUser user, StatutoryDeduction item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateStatutoryDeduction(user, item);
        }
        public void DeleteStatutoryDeduction(StatutoryDeduction item)
        {
            DeleteStatutoryDeduction(DatabaseUser, item);
        }
        public void DeleteStatutoryDeduction(DatabaseUser user, StatutoryDeduction item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteStatutoryDeduction(user, item);
        }
        public StatutoryDeduction InsertStatutoryDeduction(StatutoryDeduction item)
        {
            return InsertStatutoryDeduction(DatabaseUser, item);
        }
        public StatutoryDeduction InsertStatutoryDeduction(DatabaseUser user, StatutoryDeduction item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertStatutoryDeduction(user, item);
        }
        #endregion

        #region statutory deduction barbados
        public StatutoryDeduction InsertStatutoryDeductionBarbados(StatutoryDeduction item)
        {
            return InsertStatutoryDeductionBarbados(DatabaseUser, item);
        }
        public StatutoryDeduction InsertStatutoryDeductionBarbados(DatabaseUser user, StatutoryDeduction item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertStatutoryDeductionBarbados(user, item);
        }
        public StatutoryDeduction UpdateStatutoryDeductionBarbados(StatutoryDeduction item)
        {
            return UpdateStatutoryDeductionBarbados(DatabaseUser, item);
        }
        public StatutoryDeduction UpdateStatutoryDeductionBarbados(DatabaseUser user, StatutoryDeduction item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateStatutoryDeductionBarbados(user, item);
        }
        public void DeleteStatutoryDeductionBarbados(StatutoryDeduction item)
        {
            DeleteStatutoryDeductionBarbados(DatabaseUser, item);
        }
        public void DeleteStatutoryDeductionBarbados(DatabaseUser user, StatutoryDeduction item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteStatutoryDeductionBarbados(user, item);
        }
        #endregion

        #region statutory deduction saint lucia
        public StatutoryDeduction InsertStatutoryDeductionSaintLucia(StatutoryDeduction item)
        {
            return InsertStatutoryDeductionSaintLucia(DatabaseUser, item);
        }
        public StatutoryDeduction InsertStatutoryDeductionSaintLucia(DatabaseUser user, StatutoryDeduction item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertStatutoryDeductionSaintLucia(user, item);
        }
        public StatutoryDeduction UpdateStatutoryDeductionSaintLucia(StatutoryDeduction item)
        {
            return UpdateStatutoryDeductionSaintLucia(DatabaseUser, item);
        }
        public StatutoryDeduction UpdateStatutoryDeductionSaintLucia(DatabaseUser user, StatutoryDeduction item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateStatutoryDeductionSaintLucia(user, item);
        }
        public void DeleteStatutoryDeductionSaintLucia(StatutoryDeduction item)
        {
            DeleteStatutoryDeductionSaintLucia(DatabaseUser, item);
        }
        public void DeleteStatutoryDeductionSaintLucia(DatabaseUser user, StatutoryDeduction item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteStatutoryDeductionSaintLucia(user, item);
        }
        #endregion

        #region statutory deduction trinidad
        public StatutoryDeduction InsertStatutoryDeductionTrinidad(StatutoryDeduction item)
        {
            return InsertStatutoryDeductionTrinidad(DatabaseUser, item);
        }
        public StatutoryDeduction InsertStatutoryDeductionTrinidad(DatabaseUser user, StatutoryDeduction item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertStatutoryDeductionTrinidad(user, item);
        }
        public StatutoryDeduction UpdateStatutoryDeductionTrinidad(StatutoryDeduction item)
        {
            return UpdateStatutoryDeductionTrinidad(DatabaseUser, item);
        }
        public StatutoryDeduction UpdateStatutoryDeductionTrinidad(DatabaseUser user, StatutoryDeduction item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateStatutoryDeductionTrinidad(user, item);
        }
        public void DeleteStatutoryDeductionTrinidad(StatutoryDeduction item)
        {
            DeleteStatutoryDeductionTrinidad(DatabaseUser, item);
        }
        public void DeleteStatutoryDeductionTrinidad(DatabaseUser user, StatutoryDeduction item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteStatutoryDeductionTrinidad(user, item);
        }
        #endregion

        #region statutory deduction jamaica
        public StatutoryDeduction InsertStatutoryDeductionJamaica(StatutoryDeduction item)
        {
            return InsertStatutoryDeductionJamaica(DatabaseUser, item);
        }
        public StatutoryDeduction InsertStatutoryDeductionJamaica(DatabaseUser user, StatutoryDeduction item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertStatutoryDeductionJamaica(user, item);
        }
        public StatutoryDeduction UpdateStatutoryDeductionJamaica(StatutoryDeduction item)
        {
            return UpdateStatutoryDeductionJamaica(DatabaseUser, item);
        }
        public StatutoryDeduction UpdateStatutoryDeductionJamaica(DatabaseUser user, StatutoryDeduction item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateStatutoryDeductionJamaica(user, item);
        }
        public void DeleteStatutoryDeductionJamaica(StatutoryDeduction item)
        {
            DeleteStatutoryDeductionJamaica(DatabaseUser, item);
        }
        public void DeleteStatutoryDeductionJamaica(DatabaseUser user, StatutoryDeduction item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteStatutoryDeductionJamaica(user, item);
        }
        #endregion

        #region statutory deduction default
        public StatutoryDeductionDefaultCollection GetStatutoryDeductionDefaults(String codeProvinceStateCd)
        {
            return GetStatutoryDeductionDefaults(DatabaseUser, codeProvinceStateCd);
        }
        public StatutoryDeductionDefaultCollection GetStatutoryDeductionDefaults(DatabaseUser user, String codeProvinceStateCd)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetStatutoryDeductionDefaults(user, codeProvinceStateCd);
        }
        #endregion

        #region employee employment information
        public EmployeeEmploymentInformationCollection GetEmployeeEmploymentInformation(long id)
        {
            return GetEmployeeEmploymentInformation(DatabaseUser, id);
        }
        public EmployeeEmploymentInformationCollection GetEmployeeEmploymentInformation(DatabaseUser user, long id)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeEmploymentInformation(user, id);
        }
        public EmployeeEmploymentInformation UpdateEmployeeEmploymentInformation(EmployeeEmploymentInformation item)
        {
            return UpdateEmployeeEmploymentInformation(DatabaseUser, item);
        }
        public EmployeeEmploymentInformation UpdateEmployeeEmploymentInformation(DatabaseUser user, EmployeeEmploymentInformation item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeeEmploymentInformation(user, item);
        }
        public void DeleteEmployeeEmploymentInformation(EmployeeEmploymentInformation item)
        {
            DeleteEmployeeEmploymentInformation(DatabaseUser, item);
        }
        public void DeleteEmployeeEmploymentInformation(DatabaseUser user, EmployeeEmploymentInformation item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeeEmploymentInformation(user, item);
        }
        public EmployeeEmploymentInformation InsertEmployeeEmploymentInformation(EmployeeEmploymentInformation item)
        {
            return InsertEmployeeEmploymentInformation(DatabaseUser, item);
        }
        public EmployeeEmploymentInformation InsertEmployeeEmploymentInformation(DatabaseUser user, EmployeeEmploymentInformation item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeeEmploymentInformation(user, item);
        }
        #endregion

        #region payroll period
        public PayrollPeriodCollection GetPayrollPeriodById(long payrollPeriodId)
        {
            return GetPayrollPeriodById(DatabaseUser, payrollPeriodId);
        }
        public PayrollPeriodCollection GetPayrollPeriodById(DatabaseUser user, long payrollPeriodId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPayrollPeriodById(user, payrollPeriodId);
        }
        public PayrollPeriodCollection GetPayrollPeriod(PayrollPeriodCriteria criteria)
        {
            return GetPayrollPeriod(DatabaseUser, criteria);
        }
        public PayrollPeriodCollection GetPayrollPeriod(DatabaseUser user, PayrollPeriodCriteria criteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPayrollPeriod(user, criteria);
        }
        public PayrollPeriod UpdatePayrollPeriod(PayrollPeriod item)
        {
            return UpdatePayrollPeriod(DatabaseUser, item);
        }
        public PayrollPeriod UpdatePayrollPeriod(DatabaseUser user, PayrollPeriod item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdatePayrollPeriod(user, item);
        }
        public void DeletePayrollPeriod(PayrollPeriod item)
        {
            DeletePayrollPeriod(DatabaseUser, item);
        }
        public void DeletePayrollPeriod(DatabaseUser user, PayrollPeriod item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeletePayrollPeriod(user, item);
        }
        public PayrollPeriod InsertPayrollPeriod(PayrollPeriod item)
        {
            return InsertPayrollPeriod(DatabaseUser, item);
        }
        public PayrollPeriod InsertPayrollPeriod(DatabaseUser user, PayrollPeriod item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertPayrollPeriod(user, item);
        }
        #endregion

        #region payroll process
        public PayrollProcessSummaryCollection GetPayrollProcessSummary(PayrollProcessCriteria criteria)
        {
            return GetPayrollProcessSummary(DatabaseUser, criteria);
        }
        public PayrollProcessSummaryCollection GetPayrollProcessSummary(DatabaseUser user, PayrollProcessCriteria criteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPayrollProcessSummary(user, criteria);
        }
        public PayrollProcessCollection GetPayrollProcess(PayrollProcessCriteria criteria)
        {
            return GetPayrollProcess(DatabaseUser, criteria);
        }
        public PayrollProcessCollection GetPayrollProcess(DatabaseUser user, PayrollProcessCriteria criteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPayrollProcess(user, criteria);
        }
        public PayrollProcess UpdatePayrollProcess(PayrollProcess item, PayrollProcess.CalculationProcess processType, String version, bool autoGenSalaryEmployee, bool calculateBmsPension, bool lockUnlockButtonPressed, bool prorationEnabledFlag, byte[] uploadedFile, String uploadedFileName, bool autoAddSecondaryPositions, bool newArrearsProcessingFlag)
        {
            return UpdatePayrollProcess(DatabaseUser, item, processType, version, autoGenSalaryEmployee, calculateBmsPension, lockUnlockButtonPressed, prorationEnabledFlag, uploadedFile, uploadedFileName, autoAddSecondaryPositions, newArrearsProcessingFlag);
        }
        public PayrollProcess UpdatePayrollProcess(DatabaseUser user, PayrollProcess item, PayrollProcess.CalculationProcess processType, String version, bool autoGenSalaryEmployee, bool calculateBmsPension, bool lockUnlockButtonPressed, bool prorationEnabledFlag, byte[] uploadedFile, String uploadedFileName, bool autoAddSecondaryPositions, bool newArrearsProcessingFlag)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdatePayrollProcess(user, item, processType, version, autoGenSalaryEmployee, calculateBmsPension, lockUnlockButtonPressed, prorationEnabledFlag, uploadedFile, uploadedFileName, autoAddSecondaryPositions, newArrearsProcessingFlag);
        }
        #endregion

        #region pending ROE
        public RoeCreationSearchResultsCollection GetPendingRoeEmployeeSummary(String criteria)
        {
            return GetPendingRoeEmployeeSummary(DatabaseUser, criteria);
        }
        public RoeCreationSearchResultsCollection GetPendingRoeEmployeeSummary(DatabaseUser user, String criteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPendingRoeEmployeeSummary(user, criteria);
        }
        public void SetPendingRoeStatusForEmployeeId(long selectedEmployeeId)
        {
            SetPendingRoeStatusForEmployeeId(DatabaseUser, selectedEmployeeId);
        }
        public void SetPendingRoeStatusForEmployeeId(DatabaseUser user, long selectedEmployeeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.SetPendingRoeStatusForEmployeeId(user, selectedEmployeeId);
        }
        public EmployeeRoeAmountCollection GetRoeData(long employeePositionId, long employeeId)
        {
            return GetRoeData(DatabaseUser, employeePositionId, employeeId);
        }
        public EmployeeRoeAmountCollection GetRoeData(DatabaseUser user, long employeePositionId, long employeeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetRoeData(user, employeePositionId, employeeId);
        }
        public EmployeeRoeAmount UpdateEmployeeRoeAmount(EmployeeRoeAmount roeObj)
        {
            return UpdateEmployeeRoeAmount(DatabaseUser, roeObj);
        }
        public EmployeeRoeAmount UpdateEmployeeRoeAmount(DatabaseUser user, EmployeeRoeAmount roeObj)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeeRoeAmount(user, roeObj);
        }
        public void UpdateEmployeeRoeDetail(EmployeeRoeAmountDetail[] empRoeArray)
        {
            UpdateEmployeeRoeDetail(DatabaseUser, empRoeArray);
        }
        public void UpdateEmployeeRoeDetail(DatabaseUser user, EmployeeRoeAmountDetail[] empRoeArray)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.UpdateEmployeeRoeDetail(user, empRoeArray);
        }
        #endregion

        #region post transaction
        public PayrollProcess PostTransaction(PayrollProcess process, String status, RbcEftEdiParameters ediParms, ScotiaBankEdiParameters scotiaEdiParms, SendEmailParameters emailParms, RbcEftSourceDeductionParameters garnishmentParms, bool craRemitFlag, bool rqRemitFlag, bool sendInvoice, bool garnishmentRemitFlag, bool wcbChequeRemitFlag, bool healthTaxChequeRemitFlag, bool autoAddSecondaryPositions)
        {
            return PostTransaction(DatabaseUser, process, status, ediParms, scotiaEdiParms, emailParms, garnishmentParms, craRemitFlag, rqRemitFlag, sendInvoice, garnishmentRemitFlag, wcbChequeRemitFlag, healthTaxChequeRemitFlag, autoAddSecondaryPositions);
        }
        public PayrollProcess PostTransaction(DatabaseUser user, PayrollProcess process, String status, RbcEftEdiParameters ediParms, ScotiaBankEdiParameters scotiaEdiParms, SendEmailParameters emailParms, RbcEftSourceDeductionParameters garnishmentParms, bool craRemitFlag, bool rqRemitFlag, bool sendInvoice, bool garnishmentRemitFlag, bool wcbChequeRemitFlag, bool healthTaxChequeRemitFlag, bool autoAddSecondaryPositions)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.PostTransaction(user, process, status, ediParms, scotiaEdiParms, emailParms, garnishmentParms, craRemitFlag, rqRemitFlag, sendInvoice, garnishmentRemitFlag, wcbChequeRemitFlag, healthTaxChequeRemitFlag, autoAddSecondaryPositions);
        }
        #endregion

        #region payroll batch report and payroll batch transaction
        public PayrollBatchReportCollection GetPayrollBatchReport(PayrollBatchCriteria criteria)
        {
            return GetPayrollBatchReport(DatabaseUser, criteria);
        }
        public PayrollBatchReportCollection GetPayrollBatchReport(DatabaseUser user, PayrollBatchCriteria criteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPayrollBatchReport(user, criteria);
        }
        public PayrollBatchCollection GetPayrollBatch(long payrollBatchId)
        {
            return GetPayrollBatch(DatabaseUser, payrollBatchId);
        }
        public PayrollBatchCollection GetPayrollBatch(DatabaseUser user, long payrollBatchId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPayrollBatch(user, payrollBatchId);
        }
        public PayrollBatch InsertPayrollBatch(PayrollBatch PayrollBatch)
        {
            return InsertPayrollBatch(DatabaseUser, PayrollBatch);
        }
        public PayrollBatch InsertPayrollBatch(DatabaseUser user, PayrollBatch PayrollBatch)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertPayrollBatch(user, PayrollBatch);
        }
        public void UpdatePayrollBatch(PayrollBatch item)
        {
            UpdatePayrollBatch(DatabaseUser, item);
        }
        public void UpdatePayrollBatch(DatabaseUser user, PayrollBatch item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.UpdatePayrollBatch(user, item);
        }
        public void DeletePayrollBatch(PayrollBatchReport payrollBatch)
        {
            DeletePayrollBatch(DatabaseUser, payrollBatch);
        }
        public void DeletePayrollBatch(DatabaseUser user, PayrollBatchReport payrollBatch)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeletePayrollBatch(user, payrollBatch);
        }
        public PayrollPeriodCollection GetPayrollPeriodIdFromPayrollProcessGroupCode(PayrollBatchCriteria payrollBatchCriteria)
        {
            return GetPayrollPeriodIdFromPayrollProcessGroupCode(DatabaseUser, payrollBatchCriteria);
        }
        public PayrollPeriodCollection GetPayrollPeriodIdFromPayrollProcessGroupCode(DatabaseUser user, PayrollBatchCriteria payrollBatchCriteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPayrollPeriodIdFromPayrollProcessGroupCode(user, payrollBatchCriteria);
        }
        public PayrollTransactionSummaryCollection GetPayrollTransactionSummary(long payrollBatchAdjustmentOtherAmountId)
        {
            return GetPayrollTransactionSummary(DatabaseUser, payrollBatchAdjustmentOtherAmountId);
        }
        public PayrollTransactionSummaryCollection GetPayrollTransactionSummary(DatabaseUser user, long payrollBatchAdjustmentOtherAmountId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPayrollTransactionSummary(user, payrollBatchAdjustmentOtherAmountId);
        }
        public PayrollTransaction UpdatePayrollTransaction(PayrollTransaction payrollTransaction)
        {
            return UpdatePayrollTransaction(DatabaseUser, payrollTransaction);
        }
        public PayrollTransaction UpdatePayrollTransaction(DatabaseUser user, PayrollTransaction payrollTransaction)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdatePayrollTransaction(user, payrollTransaction);
        }
        public PayrollTransaction InsertPayrollTransaction(PayrollTransaction payrollTransaction)
        {
            return InsertPayrollTransaction(DatabaseUser, payrollTransaction);
        }
        public PayrollTransaction InsertPayrollTransaction(DatabaseUser user, PayrollTransaction payrollTransaction)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertPayrollTransaction(user, payrollTransaction);
        }
        public void DeletePayrollTransaction(PayrollTransaction payrollTransaction)
        {
            DeletePayrollTransaction(DatabaseUser, payrollTransaction);
        }
        public void DeletePayrollTransaction(DatabaseUser user, PayrollTransaction payrollTransaction)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeletePayrollTransaction(user, payrollTransaction);
        }
        public void DeletePayrollBatchCascadeTransactions(PayrollBatch payrollBatch)
        {
            DeletePayrollBatchCascadeTransactions(DatabaseUser, payrollBatch);
        }
        public void DeletePayrollBatchCascadeTransactions(DatabaseUser user, PayrollBatch payrollBatch)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeletePayrollBatchCascadeTransactions(user, payrollBatch);
        }
        #endregion

        #region wizards
        public WizardCacheCollection GetWizardCache(long wizardId, long? wizardCacheId, String itemName)
        {
            return GetWizardCache(DatabaseUser, wizardId, wizardCacheId, itemName);
        }
        public WizardCacheCollection GetWizardCache(DatabaseUser user, long wizardId, long? wizardCacheId, String itemName)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetWizardCache(user, wizardId, wizardCacheId, itemName);
        }
        public WizardCache UpdateWizardCache(WizardCache wizard)
        {
            return UpdateWizardCache(DatabaseUser, wizard);
        }
        public WizardCache UpdateWizardCache(DatabaseUser user, WizardCache wizard)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateWizardCache(user, wizard);
        }
        public WizardCacheItem UpdateWizardCacheItem(WizardCacheItem item)
        {
            return UpdateWizardCacheItem(DatabaseUser, item);
        }
        public WizardCacheItem UpdateWizardCacheItem(DatabaseUser user, WizardCacheItem item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateWizardCacheItem(user, item);
        }
        public Employee AddEmployee(long wizardCacheId, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool autoAddSecondaryPositions)
        {
            return AddEmployee(DatabaseUser, wizardCacheId, autoGenerateEmployeeNumber, employeeNumberFormat, autoAddSecondaryPositions);
        }
        public Employee AddEmployee(DatabaseUser user, long wizardCacheId, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool autoAddSecondaryPositions)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.AddEmployee(user, wizardCacheId, autoGenerateEmployeeNumber, employeeNumberFormat, autoAddSecondaryPositions);
        }
        public void DeleteWizardEmployee(long wizardCacheId)
        {
            DeleteWizardEmployee(DatabaseUser, wizardCacheId);
        }
        public void DeleteWizardEmployee(DatabaseUser user, long wizardCacheId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteWizardEmployee(user, wizardCacheId);
        }
        public bool ValidateWizardEmployee(WizardCacheCollection wizardCacheCollection)
        {
            return ValidateWizardEmployee(DatabaseUser, wizardCacheCollection);
        }
        public bool ValidateWizardEmployee(DatabaseUser user, WizardCacheCollection wizardCacheCollection)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ValidateWizardEmployee(user, wizardCacheCollection);
        }
        #endregion

        #region screen control
        public void MergeScreenControl(ScreenControl[] screenControlArray)
        {
            MergeScreenControl(DatabaseUser, screenControlArray);
        }
        public void MergeScreenControl(DatabaseUser user, ScreenControl[] screenControlArray)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.MergeScreenControl(user, screenControlArray);
        }
        #endregion

        #region import/export
        public ImportExportSummaryCollection GetImportExportSummary()
        {
            return GetImportExportSummary(DatabaseUser);
        }
        public ImportExportSummaryCollection GetImportExportSummary(DatabaseUser user)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetImportExportSummary(user);
        }
        #endregion

        #region vendor
        public VendorCollection GetVendor(long vendorId)
        {
            return GetVendor(DatabaseUser, vendorId);
        }
        public VendorCollection GetVendor(DatabaseUser user, long vendorId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetVendor(user, vendorId);
        }
        public Vendor UpdateVendor(Vendor vendor)
        {
            return UpdateVendor(DatabaseUser, vendor);
        }
        public Vendor UpdateVendor(DatabaseUser user, Vendor vendor)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateVendor(user, vendor);
        }
        public Vendor InsertVendor(Vendor vendor)
        {
            return InsertVendor(DatabaseUser, vendor);
        }
        public Vendor InsertVendor(DatabaseUser user, Vendor vendor)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertVendor(user, vendor);
        }
        public void DeleteVendor(Vendor vendor)
        {
            DeleteVendor(DatabaseUser, vendor);
        }
        public void DeleteVendor(DatabaseUser user, Vendor vendor)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteVendor(user, vendor);
        }
        #endregion

        #region temporary
        public System.Collections.Generic.KeyValuePair<String, String>[] GetDynamicsOutputFileList(String dynamicsGPOutputFolder, long payrollProcessId)
        {
            return GetDynamicsOutputFileList(DatabaseUser, dynamicsGPOutputFolder, payrollProcessId);
        }
        public System.Collections.Generic.KeyValuePair<String, String>[] GetDynamicsOutputFileList(DatabaseUser user, String dynamicsGPOutputFolder, long payrollProcessId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetDynamicsOutputFileList(user, dynamicsGPOutputFolder, payrollProcessId);
        }
        public String getFileText(String filePath)
        {
            return getFileText(DatabaseUser, filePath);
        }
        public String getFileText(DatabaseUser user, String filePath)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.getFileText(user, filePath);
        }
        #endregion

        #region year end adjustments search
        public YearEndAdjustmentsSearchReportCollection GetYearEndAdjustmentsSearchReport(YearEndAdjustmentsSearchCriteria criteria)
        {
            return GetYearEndAdjustmentsSearchReport(DatabaseUser, criteria);
        }
        public YearEndAdjustmentsSearchReportCollection GetYearEndAdjustmentsSearchReport(DatabaseUser user, YearEndAdjustmentsSearchCriteria criteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetYearEndAdjustmentsSearchReport(user, criteria);
        }
        #endregion

        #region year end adjustment
        public YearEndAdjustmentCollection GetYearEndAdjustment(long employeeId, decimal year, long yearEndAdjustmentTableId, String employerNumber, String provinceStateCode, long revision, String columnImportIdentifier)
        {
            return GetYearEndAdjustment(DatabaseUser, employeeId, year, yearEndAdjustmentTableId, employerNumber, provinceStateCode, revision, columnImportIdentifier);
        }
        public YearEndAdjustmentCollection GetYearEndAdjustment(DatabaseUser user, long employeeId, decimal year, long yearEndAdjustmentTableId, String employerNumber, String provinceStateCode, long revision, String columnImportIdentifier)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetYearEndAdjustment(user, employeeId, year, yearEndAdjustmentTableId, employerNumber, provinceStateCode, revision, columnImportIdentifier);
        }
        public YearEndAdjustmentCollection UpdateYearEndAdjustment(YearEndAdjustment[] yearEndAdjustmentCollection)
        {
            return UpdateYearEndAdjustment(DatabaseUser, yearEndAdjustmentCollection);
        }
        public YearEndAdjustmentCollection UpdateYearEndAdjustment(DatabaseUser user, YearEndAdjustment[] yearEndAdjustmentCollection)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateYearEndAdjustment(user, yearEndAdjustmentCollection);
        }
        public YearEndAdjustmentsSearchReport InsertYearEndAdjustment(YearEndAdjustmentsSearchReport report, long? previousKeyId = null)
        {
            return InsertYearEndAdjustment(DatabaseUser, report, previousKeyId);
        }
        public YearEndAdjustmentsSearchReport InsertYearEndAdjustment(DatabaseUser user, YearEndAdjustmentsSearchReport report, long? previousKeyId = null)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertYearEndAdjustment(user, report, previousKeyId);
        }
        public YearEndAdjustmentTableCollection GetYearEndAdjustmentTables()
        {
            return GetYearEndAdjustmentTables(DatabaseUser);
        }
        public YearEndAdjustmentTableCollection GetYearEndAdjustmentTables(DatabaseUser user)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetYearEndAdjustmentTables(user);
        }
        public void DeleteYearEndReport(YearEndAdjustmentsSearchReport reportObj)
        {
            DeleteYearEndReport(DatabaseUser, reportObj);
        }
        public void DeleteYearEndReport(DatabaseUser user, YearEndAdjustmentsSearchReport reportObj)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteYearEndReport(user, reportObj);
        }
        #endregion

        public YearEndT4Collection GetYearEndT4(int year, long? businessNumberId)
        {
            return GetYearEndT4(DatabaseUser, year, businessNumberId);
        }
        public YearEndT4Collection GetYearEndT4(DatabaseUser user, int year, long? businessNumberId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetYearEndT4(user, year, businessNumberId);
        }
        public YearEndT4Collection GetBatchYearEndT4(string[] yearEndT4Ids)
        {
            return GetBatchYearEndT4(DatabaseUser, yearEndT4Ids);
        }
        public YearEndT4Collection GetBatchYearEndT4(DatabaseUser user, string[] yearEndT4Ids)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetBatchYearEndT4(user, yearEndT4Ids);
        }

        #region year end clone
        public YearEndT4 SelectSingleT4ByKey(long key)
        {
            return SelectSingleT4ByKey(DatabaseUser, key);
        }
        public YearEndT4 SelectSingleT4ByKey(DatabaseUser user, long key)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.SelectSingleT4ByKey(user, key);
        }
        public YearEndT4a SelectSingleT4aByKey(long key)
        {
            return SelectSingleT4aByKey(DatabaseUser, key);
        }
        public YearEndT4a SelectSingleT4aByKey(DatabaseUser user, long key)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.SelectSingleT4aByKey(user, key);
        }
        public YearEndR1 SelectSingleR1ByKey(long key)
        {
            return SelectSingleR1ByKey(DatabaseUser, key);
        }
        public YearEndR1 SelectSingleR1ByKey(DatabaseUser user, long key)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.SelectSingleR1ByKey(user, key);
        }
        public YearEndR2 SelectSingleR2ByKey(long key)
        {
            return SelectSingleR2ByKey(DatabaseUser, key);
        }
        public YearEndR2 SelectSingleR2ByKey(DatabaseUser user, long key)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.SelectSingleR2ByKey(user, key);
        }
        public YearEndT4rsp SelectSingleT4rspByKey(long key)
        {
            return SelectSingleT4rspByKey(DatabaseUser, key);
        }
        public YearEndT4rsp SelectSingleT4rspByKey(DatabaseUser user, long key)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.SelectSingleT4rspByKey(user, key);
        }
        public YearEndNR4rsp SelectSingleNR4rspByKey(long key)
        {
            return SelectSingleNR4rspByKey(DatabaseUser, key);
        }
        public YearEndNR4rsp SelectSingleNR4rspByKey(DatabaseUser user, long key)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.SelectSingleNR4rspByKey(user, key);
        }
        public YearEndT4arca SelectSingleT4arcaByKey(long key)
        {
            return SelectSingleT4arcaByKey(DatabaseUser, key);
        }
        public YearEndT4arca SelectSingleT4arcaByKey(DatabaseUser user, long key)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.SelectSingleT4arcaByKey(user, key);
        }
        #endregion

        #region year end
        public YearEndCollection GetYearEnd(long? year)
        {
            return GetYearEnd(DatabaseUser, year);
        }
        public YearEndCollection GetYearEnd(DatabaseUser user, long? year)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetYearEnd(user, year);
        }
        public void CreateCurrentYE()
        {
            CreateCurrentYE(DatabaseUser);
        }
        public void CreateCurrentYE(DatabaseUser user)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.CreateCurrentYE(user);
        }
        public void CloseVersionYE(YearEnd yearEnd)
        {
            CloseVersionYE(DatabaseUser, yearEnd);
        }
        public void CloseVersionYE(DatabaseUser user, YearEnd yearEnd)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.CloseVersionYE(user, yearEnd);
        }
        public void CloseCurrentYE(YearEnd yearEnd)
        {
            CloseCurrentYE(DatabaseUser, yearEnd);
        }
        public void CloseCurrentYE(DatabaseUser user, YearEnd yearEnd)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.CloseCurrentYE(user, yearEnd);
        }
        #endregion

        #region business number
        public BusinessNumberCollection GetBusinessNumber(long? businessNumberId)
        {
            return GetBusinessNumber(DatabaseUser, businessNumberId);
        }
        public BusinessNumberCollection GetBusinessNumber(DatabaseUser user, long? businessNumberId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetBusinessNumber(user, businessNumberId);
        }
        public BusinessNumber InsertBusinessNumber(BusinessNumber businessNumber)
        {
            return InsertBusinessNumber(DatabaseUser, businessNumber);
        }
        public BusinessNumber InsertBusinessNumber(DatabaseUser user, BusinessNumber businessNumber)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertBusinessNumber(user, businessNumber);
        }
        public void UpdateBusinessNumber(BusinessNumber businessNumber)
        {
            UpdateBusinessNumber(DatabaseUser, businessNumber);
        }
        public void UpdateBusinessNumber(DatabaseUser user, BusinessNumber businessNumber)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.UpdateBusinessNumber(user, businessNumber);
        }
        public void DeleteBusinessNumber(BusinessNumber businessNumber)
        {
            DeleteBusinessNumber(DatabaseUser, businessNumber);
        }
        public void DeleteBusinessNumber(DatabaseUser user, BusinessNumber businessNumber)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteBusinessNumber(user, businessNumber);
        }
        #endregion

        #region year end zip file naming format
        public String GetYearEndZipFileNamingFormat(String zipFileNameFormat)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetYearEndZipFileNamingFormat(zipFileNameFormat);
        }
        #endregion

        #region sap gl naming format
        public String GenerateSapFileName(String zipFileNameFormat, long payrollProcessId)
        {
            return GenerateSapFileName(DatabaseUser, zipFileNameFormat, payrollProcessId);
        }
        public String GenerateSapFileName(DatabaseUser user, String zipFileNameFormat, long payrollProcessId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GenerateSapFileName(user, zipFileNameFormat, payrollProcessId);
        }
        #endregion

        #region mass payslip file zip naming format
        public string GenerateFileName(String zipFileNameFormat, long payrollProcessId)
        {
            return GenerateFileName(DatabaseUser, zipFileNameFormat, payrollProcessId);
        }
        public String GenerateFileName(DatabaseUser user, String zipFileNameFormat, long payrollProcessId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GenerateFileName(user, zipFileNameFormat, payrollProcessId);
        }
        #endregion

        #region payroll master payment
        public PayrollMasterPaymentCollection GetPayrollMasterPaymentDirectDeposit(long payrollProcessId, String employeeNumber)
        {
            return GetPayrollMasterPaymentDirectDeposit(DatabaseUser, payrollProcessId, employeeNumber);
        }
        public PayrollMasterPaymentCollection GetPayrollMasterPaymentDirectDeposit(DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPayrollMasterPaymentDirectDeposit(user, payrollProcessId, employeeNumber);
        }
        #endregion

        #region export ftp
        public ExportFtpCollection GetExportFtp(long? exportFtpId, string codeExportEftTypeCd, string ftpName)
        {
            return GetExportFtp(DatabaseUser, exportFtpId, codeExportEftTypeCd, ftpName);
        }
        public ExportFtpCollection GetExportFtp(DatabaseUser user, long? exportFtpId, string codeExportEftTypeCd, string ftpName)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetExportFtp(user, exportFtpId, codeExportEftTypeCd, ftpName);
        }
        public ExportFtp UpdateExportFtp(ExportFtp export)
        {
            return UpdateExportFtp(DatabaseUser, export);
        }
        public ExportFtp UpdateExportFtp(DatabaseUser user, ExportFtp export)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateExportFtp(user, export);
        }
        public ExportFtp InsertExportFtp(ExportFtp export)
        {
            return InsertExportFtp(DatabaseUser, export);
        }
        public ExportFtp InsertExportFtp(DatabaseUser user, ExportFtp export)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertExportFtp(user, export);
        }
        public void DeleteExportFtp(ExportFtp export)
        {
            DeleteExportFtp(DatabaseUser, export);
        }
        public void DeleteExportFtp(DatabaseUser user, ExportFtp export)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteExportFtp(user, export);
        }
        #endregion

        #region pay register export
        public PayRegisterExportCollection GetPayRegisterExport(long? exportQueueId, string codeExportFtpTypeCd)
        {
            return GetPayRegisterExport(DatabaseUser, exportQueueId, codeExportFtpTypeCd);
        }
        public PayRegisterExportCollection GetPayRegisterExport(DatabaseUser user, long? exportQueueId, string codeExportFtpTypeCd)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPayRegisterExport(user, exportQueueId, codeExportFtpTypeCd);
        }
        public PayRegisterExport UpdatePayRegisterExport(PayRegisterExport export)
        {
            return UpdatePayRegisterExport(DatabaseUser, export);
        }
        public PayRegisterExport UpdatePayRegisterExport(DatabaseUser user, PayRegisterExport export)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdatePayRegisterExport(user, export);
        }
        public PayRegisterExport InsertPayRegisterExport(PayRegisterExport export)
        {
            return InsertPayRegisterExport(DatabaseUser, export);
        }
        public PayRegisterExport InsertPayRegisterExport(DatabaseUser user, PayRegisterExport export)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertPayRegisterExport(user, export);
        }
        public void DeletePayRegisterExport(PayRegisterExport export)
        {
            DeletePayRegisterExport(DatabaseUser, export);
        }
        public void DeletePayRegisterExport(DatabaseUser user, PayRegisterExport export)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeletePayRegisterExport(user, export);
        }
        #endregion

        #region get year end negative amount employee
        public Employee GetYearEndNegativeAmountEmployee(Decimal year, int revision)
        {
            return GetYearEndNegativeAmountEmployee(DatabaseUser, year, revision);
        }
        public Employee GetYearEndNegativeAmountEmployee(DatabaseUser user, Decimal year, int revision)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetYearEndNegativeAmountEmployee(user, year, revision);
        }
        #endregion

        #region org unit hierarchy tree
        public OrganizationUnitAssociationTreeCollection GetOrganizationUnitAssociationTree(String codeLanguageCd, bool? checkActiveFlag)
        {
            return GetOrganizationUnitAssociationTree(DatabaseUser, codeLanguageCd, checkActiveFlag);
        }
        public OrganizationUnitAssociationTreeCollection GetOrganizationUnitAssociationTree(DatabaseUser user, String codeLanguageCd, bool? checkActiveFlag)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetOrganizationUnitAssociationTree(user, codeLanguageCd, checkActiveFlag);
        }
        #endregion

        #region employee position labour distribution
        public EmployeePositionLabourDistributionSummaryCollection GetEmployeePositionLabourDistribution(long employeePositionId, String codeLanguageCd)
        {
            return GetEmployeePositionLabourDistribution(DatabaseUser, employeePositionId, codeLanguageCd);
        }
        public EmployeePositionLabourDistributionSummaryCollection GetEmployeePositionLabourDistribution(DatabaseUser user, long employeePositionId, String codeLanguageCd)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeePositionLabourDistribution(user, employeePositionId, codeLanguageCd);
        }
        public EmployeePositionLabourDistributionSummary UpdateEmployeePositionLabourDistribution(EmployeePositionLabourDistributionSummary summary)
        {
            return UpdateEmployeePositionLabourDistribution(DatabaseUser, summary);
        }
        public EmployeePositionLabourDistributionSummary UpdateEmployeePositionLabourDistribution(DatabaseUser user, EmployeePositionLabourDistributionSummary summary)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeePositionLabourDistribution(user, summary);
        }
        public EmployeePositionLabourDistributionSummary InsertEmployeePositionLabourDistribution(EmployeePositionLabourDistributionSummary summary)
        {
            return InsertEmployeePositionLabourDistribution(DatabaseUser, summary);
        }
        public EmployeePositionLabourDistributionSummary InsertEmployeePositionLabourDistribution(DatabaseUser user, EmployeePositionLabourDistributionSummary summary)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeePositionLabourDistribution(user, summary);
        }
        public void DeleteEmployeePositionLabourDistribution(EmployeePositionLabourDistributionSummary summary)
        {
            DeleteEmployeePositionLabourDistribution(DatabaseUser, summary);
        }
        public void DeleteEmployeePositionLabourDistribution(DatabaseUser user, EmployeePositionLabourDistributionSummary summary)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeePositionLabourDistribution(user, summary);
        }
        #endregion

        #region create citi cheque eft from payroll master payment records
        public byte[] CreateCitiChequeEftFromPayrollMasterPaymentRecords(String[] payrollMasterPaymentIds, String eftClientNumber, String companyShortName, String employeeNumber, long payrollProccessId, String originatorId, long employeeId)
        {
            return CreateCitiChequeEftFromPayrollMasterPaymentRecords(DatabaseUser, payrollMasterPaymentIds, eftClientNumber, companyShortName, employeeNumber, payrollProccessId, originatorId, employeeId);
        }
        public byte[] CreateCitiChequeEftFromPayrollMasterPaymentRecords(DatabaseUser user, String[] payrollMasterPaymentIds, String eftClientNumber, String companyShortName, String employeeNumber, long payrollProccessId, String originatorId, long employeeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreateCitiChequeEftFromPayrollMasterPaymentRecords(user, payrollMasterPaymentIds, eftClientNumber, companyShortName, employeeNumber, payrollProccessId, originatorId, employeeId);
        }
        #endregion

        #region employee photo
        public EmployeePhotoCollection GetEmployeePhoto(long employeeId)
        {
            return GetEmployeePhoto(DatabaseUser, employeeId);
        }
        public EmployeePhotoCollection GetEmployeePhoto(DatabaseUser user, long employeeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeePhoto(user, employeeId);
        }
        public void InsertEmployeePhoto(EmployeePhoto photo)
        {
            InsertEmployeePhoto(DatabaseUser, photo);
        }
        public void InsertEmployeePhoto(DatabaseUser user, EmployeePhoto photo)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.InsertEmployeePhoto(user, photo);
        }
        public void UpdateEmployeePhoto(EmployeePhoto photo)
        {
            UpdateEmployeePhoto(DatabaseUser, photo);
        }
        public void UpdateEmployeePhoto(DatabaseUser user, EmployeePhoto photo)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.UpdateEmployeePhoto(user, photo);
        }
        public void DeleteEmployeePhoto(EmployeePhoto photo)
        {
            DeleteEmployeePhoto(DatabaseUser, photo);
        }
        public void DeleteEmployeePhoto(DatabaseUser user, EmployeePhoto photo)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeePhoto(user, photo);
        }
        #endregion

        #region attachment
        public AttachmentCollection GetAttachment(long attachmentId)
        {
            return GetAttachment(DatabaseUser, attachmentId);
        }
        public AttachmentCollection GetAttachment(DatabaseUser user, long attachmentId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetAttachment(user, attachmentId);
        }
        public void InsertAttachment(Attachment attachment)
        {
            InsertAttachment(DatabaseUser, attachment);
        }
        public void InsertAttachment(DatabaseUser user, Attachment attachment)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.InsertAttachment(user, attachment);
        }
        public void UpdateAttachment(Attachment attachment)
        {
            UpdateAttachment(DatabaseUser, attachment);
        }
        public void UpdateAttachment(DatabaseUser user, Attachment attachment)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.UpdateAttachment(user, attachment);
        }
        public void DeleteAttachment(Attachment attachment)
        {
            DeleteAttachment(DatabaseUser, attachment);
        }
        public void DeleteAttachment(DatabaseUser user, Attachment attachment)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteAttachment(user, attachment);
        }
        #endregion

        #region employee remittance stub
        public EmployeeRemittanceStubCollection GetEmployeeRemittanceStub(long employeePaycodeId)
        {
            return GetEmployeeRemittanceStub(DatabaseUser, employeePaycodeId);
        }
        public EmployeeRemittanceStubCollection GetEmployeeRemittanceStub(DatabaseUser user, long employeePaycodeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeRemittanceStub(user, employeePaycodeId);
        }
        public void InsertEmployeeRemittanceStub(EmployeeRemittanceStub stub)
        {
            InsertEmployeeRemittanceStub(DatabaseUser, stub);
        }
        public void InsertEmployeeRemittanceStub(DatabaseUser user, EmployeeRemittanceStub stub)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.InsertEmployeeRemittanceStub(user, stub);
        }
        public void UpdateEmployeeRemittanceStub(EmployeeRemittanceStub stub)
        {
            UpdateEmployeeRemittanceStub(DatabaseUser, stub);
        }
        public void UpdateEmployeeRemittanceStub(DatabaseUser user, EmployeeRemittanceStub stub)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.UpdateEmployeeRemittanceStub(user, stub);
        }
        public void DeleteEmployeeRemittanceStub(EmployeeRemittanceStub stub)
        {
            DeleteEmployeeRemittanceStub(DatabaseUser, stub);
        }
        public void DeleteEmployeeRemittanceStub(DatabaseUser user, EmployeeRemittanceStub stub)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeeRemittanceStub(user, stub);
        }
        #endregion

        #region paycode year end report map
        public PaycodeYearEndReportMapCollection GetPaycodeYearEndReportMap(String paycodeCode)
        {
            return GetPaycodeYearEndReportMap(DatabaseUser, paycodeCode);
        }
        public PaycodeYearEndReportMapCollection GetPaycodeYearEndReportMap(DatabaseUser user, String paycodeCode)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPaycodeYearEndReportMap(user, paycodeCode);
        }
        public PaycodeYearEndReportMap UpdatePaycodeYearEndReportMap(PaycodeYearEndReportMap paycodeYearEndReportMap)
        {
            return UpdatePaycodeYearEndReportMap(DatabaseUser, paycodeYearEndReportMap);
        }
        public PaycodeYearEndReportMap UpdatePaycodeYearEndReportMap(DatabaseUser user, PaycodeYearEndReportMap paycodeYearEndReportMap)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdatePaycodeYearEndReportMap(user, paycodeYearEndReportMap);
        }
        public PaycodeYearEndReportMap InsertPaycodeYearEndReportMap(PaycodeYearEndReportMap paycodeYearEndReportMap)
        {
            return InsertPaycodeYearEndReportMap(DatabaseUser, paycodeYearEndReportMap);
        }
        public PaycodeYearEndReportMap InsertPaycodeYearEndReportMap(DatabaseUser user, PaycodeYearEndReportMap paycodeYearEndReportMap)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertPaycodeYearEndReportMap(user, paycodeYearEndReportMap);
        }
        public void DeletePaycodeYearEndReportMap(PaycodeYearEndReportMap paycodeYearEndReportMap)
        {
            DeletePaycodeYearEndReportMap(DatabaseUser, paycodeYearEndReportMap);
        }
        public void DeletePaycodeYearEndReportMap(DatabaseUser user, PaycodeYearEndReportMap paycodeYearEndReportMap)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeletePaycodeYearEndReportMap(user, paycodeYearEndReportMap);
        }
        #endregion

        #region paycode payroll transaction offset association
        public PaycodePayrollTransactionOffsetAssociationCollection GetPaycodePayrollTransactionOffsetAssociation(String paycodeCode)
        {
            return GetPaycodePayrollTransactionOffsetAssociation(DatabaseUser, paycodeCode);
        }
        public PaycodePayrollTransactionOffsetAssociationCollection GetPaycodePayrollTransactionOffsetAssociation(DatabaseUser user, String paycodeCode)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPaycodePayrollTransactionOffsetAssociation(user, paycodeCode);
        }
        public PaycodePayrollTransactionOffsetAssociation UpdatePaycodePayrollTransactionOffsetAssociation(PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation)
        {
            return UpdatePaycodePayrollTransactionOffsetAssociation(DatabaseUser, paycodePayrollTransactionOffsetAssociation);
        }
        public PaycodePayrollTransactionOffsetAssociation UpdatePaycodePayrollTransactionOffsetAssociation(DatabaseUser user, PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdatePaycodePayrollTransactionOffsetAssociation(user, paycodePayrollTransactionOffsetAssociation);
        }
        public PaycodePayrollTransactionOffsetAssociation InsertPaycodePayrollTransactionOffsetAssociation(PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation)
        {
            return InsertPaycodePayrollTransactionOffsetAssociation(DatabaseUser, paycodePayrollTransactionOffsetAssociation);
        }
        public PaycodePayrollTransactionOffsetAssociation InsertPaycodePayrollTransactionOffsetAssociation(DatabaseUser user, PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertPaycodePayrollTransactionOffsetAssociation(user, paycodePayrollTransactionOffsetAssociation);
        }
        public void DeletePaycodePayrollTransactionOffsetAssociation(PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation)
        {
            DeletePaycodePayrollTransactionOffsetAssociation(DatabaseUser, paycodePayrollTransactionOffsetAssociation);
        }
        public void DeletePaycodePayrollTransactionOffsetAssociation(DatabaseUser user, PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeletePaycodePayrollTransactionOffsetAssociation(user, paycodePayrollTransactionOffsetAssociation);
        }
        #endregion

        #region accrual entitlement search
        public AccrualEntitlementSearchCollection GetAccrualEntitlementReport(String criteria)
        {
            return GetAccrualEntitlementReport(DatabaseUser, criteria);
        }
        public AccrualEntitlementSearchCollection GetAccrualEntitlementReport(DatabaseUser user, String criteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetAccrualEntitlementReport(user, criteria);
        }
        public void DeleteAccrualEntitlementReport(long entitlementId)
        {
            DeleteAccrualEntitlementReport(DatabaseUser, entitlementId);
        }
        public void DeleteAccrualEntitlementReport(DatabaseUser user, long entitlementId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteAccrualEntitlementReport(user, entitlementId);
        }
        #endregion

        #region accrual entitlement
        public AccrualEntitlementCollection GetAccrualEntitlement(long? entitlementId)
        {
            return GetAccrualEntitlement(DatabaseUser, entitlementId);
        }
        public AccrualEntitlementCollection GetAccrualEntitlement(DatabaseUser user, long? entitlementId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetAccrualEntitlement(user, entitlementId);
        }
        public AccrualEntitlement InsertAccrualEntitlement(AccrualEntitlement entitlement)
        {
            return InsertAccrualEntitlement(DatabaseUser, entitlement);
        }
        public AccrualEntitlement InsertAccrualEntitlement(DatabaseUser user, AccrualEntitlement entitlement)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertAccrualEntitlement(user, entitlement);
        }
        public AccrualEntitlement UpdateAccrualEntitlement(AccrualEntitlement entitlement)
        {
            return UpdateAccrualEntitlement(DatabaseUser, entitlement);
        }
        public AccrualEntitlement UpdateAccrualEntitlement(DatabaseUser user, AccrualEntitlement entitlement)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateAccrualEntitlement(user, entitlement);
        }
        #endregion

        #region accrual entitlement detail
        public AccrualEntitlementDetailCollection GetAccrualEntitlementDetail(long entitlementId, long entitlementDetailId)
        {
            return GetAccrualEntitlementDetail(DatabaseUser, entitlementId, entitlementDetailId);
        }
        public AccrualEntitlementDetailCollection GetAccrualEntitlementDetail(DatabaseUser user, long entitlementId, long entitlementDetailId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetAccrualEntitlementDetail(user, entitlementId, entitlementDetailId);
        }
        public AccrualEntitlementDetail InsertAccrualEntitlementDetail(AccrualEntitlementDetail entitlementDetail)
        {
            return InsertAccrualEntitlementDetail(DatabaseUser, entitlementDetail);
        }
        public AccrualEntitlementDetail InsertAccrualEntitlementDetail(DatabaseUser user, AccrualEntitlementDetail entitlementDetail)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertAccrualEntitlementDetail(user, entitlementDetail);
        }
        public AccrualEntitlementDetail UpdateAccrualEntitlementDetail(AccrualEntitlementDetail entitlementDetail, AccrualEntitlementDetailValueCollection entitlementDetailValueCollection, AccrualEntitlementDetailPaycodeCollection entitlementDetailPaycodeCollection)
        {
            return UpdateAccrualEntitlementDetail(DatabaseUser, entitlementDetail, entitlementDetailValueCollection, entitlementDetailPaycodeCollection);
        }
        public AccrualEntitlementDetail UpdateAccrualEntitlementDetail(DatabaseUser user, AccrualEntitlementDetail entitlementDetail, AccrualEntitlementDetailValueCollection entitlementDetailValueCollection, AccrualEntitlementDetailPaycodeCollection entitlementDetailPaycodeCollection)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateAccrualEntitlementDetail(user, entitlementDetail, entitlementDetailValueCollection, entitlementDetailPaycodeCollection);
        }
        public void DeleteAccrualEntitlementDetail(AccrualEntitlementDetail entitlementDetail)
        {
            DeleteAccrualEntitlementDetail(DatabaseUser, entitlementDetail);
        }
        public void DeleteAccrualEntitlementDetail(DatabaseUser user, AccrualEntitlementDetail entitlementDetail)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteAccrualEntitlementDetail(user, entitlementDetail);
        }
        #endregion

        #region accrual entitlement detail paycode
        public AccrualEntitlementDetailPaycodeCollection GetAccrualEntitlementDetailPaycode(long entitlementDetailId)
        {
            return GetAccrualEntitlementDetailPaycode(DatabaseUser, entitlementDetailId);
        }
        public AccrualEntitlementDetailPaycodeCollection GetAccrualEntitlementDetailPaycode(DatabaseUser user, long entitlementDetailId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetAccrualEntitlementDetailPaycode(user, entitlementDetailId);
        }
        #endregion

        #region accrual entitlement detail value
        public AccrualEntitlementDetailValueCollection GetAccrualEntitlementDetailValue(long entitlementDetailId)
        {
            return GetAccrualEntitlementDetailValue(DatabaseUser, entitlementDetailId);
        }
        public AccrualEntitlementDetailValueCollection GetAccrualEntitlementDetailValue(DatabaseUser user, long entitlementDetailId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetAccrualEntitlementDetailValue(user, entitlementDetailId);
        }
        #endregion

        #region accrual policy search
        public AccrualPolicySearchCollection GetAccrualPolicyReport(String criteria)
        {
            return GetAccrualPolicyReport(DatabaseUser, criteria);
        }
        public AccrualPolicySearchCollection GetAccrualPolicyReport(DatabaseUser user, String criteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetAccrualPolicyReport(user, criteria);
        }
        public void DeleteAccrualPolicyReport(long policyId)
        {
            DeleteAccrualPolicyReport(DatabaseUser, policyId);
        }
        public void DeleteAccrualPolicyReport(DatabaseUser user, long policyId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteAccrualPolicyReport(user, policyId);
        }
        #endregion

        #region accrual policy
        public AccrualPolicyCollection GetAccrualPolicy(long? policyId)
        {
            return GetAccrualPolicy(DatabaseUser, policyId);
        }
        public AccrualPolicyCollection GetAccrualPolicy(DatabaseUser user, long? policyId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetAccrualPolicy(user, policyId);
        }
        public AccrualPolicy InsertAccrualPolicy(AccrualPolicy policy, AccrualPolicyEntitlementCollection policyEntitlementCollection)
        {
            return InsertAccrualPolicy(DatabaseUser, policy, policyEntitlementCollection);
        }
        public AccrualPolicy InsertAccrualPolicy(DatabaseUser user, AccrualPolicy policy, AccrualPolicyEntitlementCollection policyEntitlementCollection)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertAccrualPolicy(user, policy, policyEntitlementCollection);
        }
        public AccrualPolicy UpdateAccrualPolicy(AccrualPolicy policy, AccrualPolicyEntitlementCollection policyEntitlementCollection)
        {
            return UpdateAccrualPolicy(DatabaseUser, policy, policyEntitlementCollection);
        }
        public AccrualPolicy UpdateAccrualPolicy(DatabaseUser user, AccrualPolicy policy, AccrualPolicyEntitlementCollection policyEntitlementCollection)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateAccrualPolicy(user, policy, policyEntitlementCollection);
        }
        #endregion

        #region accrual policy entitlement
        public AccrualPolicyEntitlementCollection GetAccrualPolicyEntitlement(long policyId)
        {
            return GetAccrualPolicyEntitlement(DatabaseUser, policyId);
        }
        public AccrualPolicyEntitlementCollection GetAccrualPolicyEntitlement(DatabaseUser user, long policyId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetAccrualPolicyEntitlement(user, policyId);
        }
        #endregion

        #region global employee paycode
        public EmployeePaycodeCollection GetGlobalEmployeePaycode(long? globalEmployeePaycodeId)
        {
            return GetGlobalEmployeePaycode(DatabaseUser, globalEmployeePaycodeId);
        }
        public EmployeePaycodeCollection GetGlobalEmployeePaycode(DatabaseUser user, long? globalEmployeePaycodeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetGlobalEmployeePaycode(user, globalEmployeePaycodeId);
        }
        public EmployeePaycode InsertGlobalEmployeePaycode(EmployeePaycode employeePaycode)
        {
            return InsertGlobalEmployeePaycode(DatabaseUser, employeePaycode);
        }
        public EmployeePaycode InsertGlobalEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertGlobalEmployeePaycode(user, employeePaycode);
        }
        public EmployeePaycode UpdateGlobalEmployeePaycode(EmployeePaycode employeePaycode)
        {
            return UpdateGlobalEmployeePaycode(DatabaseUser, employeePaycode);
        }
        public EmployeePaycode UpdateGlobalEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateGlobalEmployeePaycode(user, employeePaycode);
        }
        public void DeleteGlobalEmployeePaycode(EmployeePaycode employeePaycode)
        {
            DeleteGlobalEmployeePaycode(DatabaseUser, employeePaycode);
        }
        public void DeleteGlobalEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteGlobalEmployeePaycode(user, employeePaycode);
        }
        #endregion

        #region statutory holiday date
        public StatutoryHolidayDateCollection GetStatutoryHolidayDateReport(StatutoryDeductionCriteria criteria)
        {
            return GetStatutoryHolidayDateReport(DatabaseUser, criteria);
        }
        public StatutoryHolidayDateCollection GetStatutoryHolidayDateReport(DatabaseUser user, StatutoryDeductionCriteria criteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetStatutoryHolidayDateReport(user, criteria);
        }
        public StatutoryHolidayDateCollection GetStatutoryHolidayDate(long? statutoryHolidayDateId)
        {
            return GetStatutoryHolidayDate(DatabaseUser, statutoryHolidayDateId);
        }
        public StatutoryHolidayDateCollection GetStatutoryHolidayDate(DatabaseUser user, long? statutoryHolidayDateId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetStatutoryHolidayDate(user, statutoryHolidayDateId);
        }
        public StatutoryHolidayDate InsertStatutoryHolidayDate(StatutoryHolidayDate statutoryHolidayDate)
        {
            return InsertStatutoryHolidayDate(DatabaseUser, statutoryHolidayDate);
        }
        public StatutoryHolidayDate InsertStatutoryHolidayDate(DatabaseUser user, StatutoryHolidayDate statutoryHolidayDate)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertStatutoryHolidayDate(user, statutoryHolidayDate);
        }
        public StatutoryHolidayDate UpdateStatutoryHolidayDate(StatutoryHolidayDate statutoryHolidayDate)
        {
            return UpdateStatutoryHolidayDate(DatabaseUser, statutoryHolidayDate);
        }
        public StatutoryHolidayDate UpdateStatutoryHolidayDate(DatabaseUser user, StatutoryHolidayDate statutoryHolidayDate)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateStatutoryHolidayDate(user, statutoryHolidayDate);
        }
        public void DeleteStatutoryHolidayDate(StatutoryHolidayDate statutoryHolidayDate)
        {
            DeleteStatutoryHolidayDate(DatabaseUser, statutoryHolidayDate);
        }
        public void DeleteStatutoryHolidayDate(DatabaseUser user, StatutoryHolidayDate statutoryHolidayDate)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteStatutoryHolidayDate(user, statutoryHolidayDate);
        }
        #endregion

        #region remittance
        public RemittanceCollection GetRemittance()
        {
            return GetRemittance(DatabaseUser);
        }
        public RemittanceCollection GetRemittance(DatabaseUser user)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetRemittance(user);
        }
        #endregion

        #region remittance detail
        public RemittanceDetailCraSourceDeductionsCollection GetRemittanceDetailCraSourceDeductions(long? craExportId, string remitCode, DateTime remitDate)
        {
            return GetRemittanceDetailCraSourceDeductions(DatabaseUser, craExportId, remitCode, remitDate);
        }
        public RemittanceDetailCraSourceDeductionsCollection GetRemittanceDetailCraSourceDeductions(DatabaseUser user, long? craExportId, string remitCode, DateTime remitDate)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetRemittanceDetailCraSourceDeductions(user, craExportId, remitCode, remitDate);
        }

        public RemittanceDetailWcbDeductionsCollection GetRemittanceDetailWcbDeductions(string detailType, long? wcbExportId, string remitCode, DateTime remitDate)
        {
            return GetRemittanceDetailWcbDeductions(DatabaseUser, detailType, wcbExportId, remitCode, remitDate);
        }
        public RemittanceDetailWcbDeductionsCollection GetRemittanceDetailWcbDeductions(DatabaseUser user, string detailType, long? wcbExportId, string remitCode, DateTime remitDate)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetRemittanceDetailWcbDeductions(user, detailType, wcbExportId, remitCode, remitDate);
        }

        public RemittanceDetailChequeHealthTaxDeductionsCollection GetRemittanceDetailChequeHealthTaxDeductions(long? chequeHealthTaxExportId, string codeHealthTaxCode)
        {
            return GetRemittanceDetailChequeHealthTaxDeductions(DatabaseUser, chequeHealthTaxExportId, codeHealthTaxCode);
        }
        public RemittanceDetailChequeHealthTaxDeductionsCollection GetRemittanceDetailChequeHealthTaxDeductions(DatabaseUser user, long? chequeHealthTaxExportId, string codeHealthTaxCode)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetRemittanceDetailChequeHealthTaxDeductions(user, chequeHealthTaxExportId, codeHealthTaxCode);
        }

        public RemittanceDetailGarnishmentDeductionsCollection GetRemittanceDetailGarnishmentDeductions(string detailType, long? exportId)
        {
            return GetRemittanceDetailGarnishmentDeductions(DatabaseUser, detailType, exportId);
        }
        public RemittanceDetailGarnishmentDeductionsCollection GetRemittanceDetailGarnishmentDeductions(DatabaseUser user, string detailType, long? exportId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetRemittanceDetailGarnishmentDeductions(user, detailType, exportId);
        }

        public RemittanceDetailRqSourceDeductionsCollection GetRemittanceDetailRqSourceDeductions(long? rqExportId)
        {
            return GetRemittanceDetailRqSourceDeductions(DatabaseUser, rqExportId);
        }
        public RemittanceDetailRqSourceDeductionsCollection GetRemittanceDetailRqSourceDeductions(DatabaseUser user, long? rqExportId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetRemittanceDetailRqSourceDeductions(user, rqExportId);
        }

        #endregion

        #region remittance import
        public RemittanceImportCollection GetRemittanceImport(long? remittanceImportId)
        {
            return GetRemittanceImport(DatabaseUser, remittanceImportId);
        }
        public RemittanceImportCollection GetRemittanceImport(DatabaseUser user, long? remittanceImportId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetRemittanceImport(user, remittanceImportId);
        }
        //public RemittanceImportDetailCollection GetRemittanceImportDetail(long remittanceImportId)
        //{
        //    return GetRemittanceImportDetail(DatabaseUser, remittanceImportId);
        //}
        //public RemittanceImportDetailCollection GetRemittanceImportDetail(DatabaseUser user, long remittanceImportId)
        //{
        //    using (HumanResourcesChannelWrapper wrapper = CreateChannel())
        //        return wrapper.Channel.GetRemittanceImportDetail(user, remittanceImportId);
        //}
        //public void ProcessRemittanceImport(long remittanceImportId, String remittanceImportStatusCode)
        //{
        //    ProcessRemittanceImport(DatabaseUser, remittanceImportId, remittanceImportStatusCode);
        //}
        //public void ProcessRemittanceImport(DatabaseUser user, long remittanceImportId, String remittanceImportStatusCode)
        //{
        //    using (HumanResourcesChannelWrapper wrapper = CreateChannel())
        //        wrapper.Channel.ProcessRemittanceImport(user, remittanceImportId, remittanceImportStatusCode);
        //}
        #endregion

        #region email
        public EmailCollection GetEmail(long? emailId)
        {
            return GetEmail(DatabaseUser, emailId);
        }
        public EmailCollection GetEmail(DatabaseUser user, long? emailId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmail(user, emailId);
        }
        public Email InsertEmail(Email item)
        {
            return InsertEmail(DatabaseUser, item);
        }
        public Email InsertEmail(DatabaseUser user, Email item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmail(user, item);
        }
        public Email UpdateEmail(Email item)
        {
            return UpdateEmail(DatabaseUser, item);
        }
        public Email UpdateEmail(DatabaseUser user, Email item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmail(user, item);
        }
        public void DeleteEmail(Email item)
        {
            DeleteEmail(DatabaseUser, item);
        }
        public void DeleteEmail(DatabaseUser user, Email item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmail(user, item);
        }
        #endregion

        #region email template
        public EmailTemplateCollection GetEmailTemplate(long emailId)
        {
            return GetEmailTemplate(DatabaseUser, emailId);
        }
        public EmailTemplateCollection GetEmailTemplate(DatabaseUser user, long emailId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmailTemplate(user, emailId);
        }
        public EmailTemplate InsertEmailTemplate(EmailTemplate item)
        {
            return InsertEmailTemplate(DatabaseUser, item);
        }
        public EmailTemplate InsertEmailTemplate(DatabaseUser user, EmailTemplate item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmailTemplate(user, item);
        }
        public EmailTemplate UpdateEmailTemplate(EmailTemplate item)
        {
            return UpdateEmailTemplate(DatabaseUser, item);
        }
        public EmailTemplate UpdateEmailTemplate(DatabaseUser user, EmailTemplate item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmailTemplate(user, item);
        }
        public void DeleteEmailTemplate(EmailTemplate item)
        {
            DeleteEmailTemplate(DatabaseUser, item);
        }
        public void DeleteEmailTemplate(DatabaseUser user, EmailTemplate item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmailTemplate(user, item);
        }
        #endregion

        #region email rules
        public EmailRuleCollection GetEmailRule(long emailId)
        {
            return GetEmailRule(DatabaseUser, emailId);
        }
        public EmailRuleCollection GetEmailRule(DatabaseUser user, long emailId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmailRule(user, emailId);
        }
        public EmailRule InsertEmailRule(EmailRule item)
        {
            return InsertEmailRule(DatabaseUser, item);
        }
        public EmailRule InsertEmailRule(DatabaseUser user, EmailRule item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmailRule(user, item);
        }
        public EmailRule UpdateEmailRule(EmailRule item)
        {
            return UpdateEmailRule(DatabaseUser, item);
        }
        public EmailRule UpdateEmailRule(DatabaseUser user, EmailRule item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmailRule(user, item);
        }
        public void DeleteEmailRule(EmailRule item)
        {
            DeleteEmailRule(DatabaseUser, item);
        }
        public void DeleteEmailRule(DatabaseUser user, EmailRule item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmailRule(user, item);
        }
        #endregion

        #region health tax
        public HealthTaxCollection GetHealthTax(long? healthTaxId)
        {
            return GetHealthTax(DatabaseUser, healthTaxId);
        }
        public HealthTaxCollection GetHealthTax(DatabaseUser user, long? healthTaxId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetHealthTax(user, healthTaxId);
        }
        public HealthTax InsertHealthTax(HealthTax healthTax)
        {
            return InsertHealthTax(DatabaseUser, healthTax);
        }
        public HealthTax InsertHealthTax(DatabaseUser user, HealthTax healthTax)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertHealthTax(user, healthTax);
        }
        public HealthTax UpdateHealthTax(HealthTax healthTax)
        {
            return UpdateHealthTax(DatabaseUser, healthTax);
        }
        public HealthTax UpdateHealthTax(DatabaseUser user, HealthTax healthTax)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateHealthTax(user, healthTax);
        }
        public void DeleteHealthTax(HealthTax healthTax)
        {
            DeleteHealthTax(DatabaseUser, healthTax);
        }
        public void DeleteHealthTax(DatabaseUser user, HealthTax healthTax)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteHealthTax(user, healthTax);
        }
        #endregion

        #region health tax detail
        public HealthTaxDetailCollection GetHealthTaxDetail(long healthTaxId)
        {
            return GetHealthTaxDetail(DatabaseUser, healthTaxId);
        }
        public HealthTaxDetailCollection GetHealthTaxDetail(DatabaseUser user, long healthTaxId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetHealthTaxDetail(user, healthTaxId);
        }
        public HealthTaxDetail InsertHealthTaxDetail(HealthTaxDetail healthTaxDetail)
        {
            return InsertHealthTaxDetail(DatabaseUser, healthTaxDetail);
        }
        public HealthTaxDetail InsertHealthTaxDetail(DatabaseUser user, HealthTaxDetail healthTaxDetail)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertHealthTaxDetail(user, healthTaxDetail);
        }
        public HealthTaxDetail UpdateHealthTaxDetail(HealthTaxDetail healthTaxDetail)
        {
            return UpdateHealthTaxDetail(DatabaseUser, healthTaxDetail);
        }
        public HealthTaxDetail UpdateHealthTaxDetail(DatabaseUser user, HealthTaxDetail healthTaxDetail)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateHealthTaxDetail(user, healthTaxDetail);
        }
        public void DeleteHealthTaxDetail(HealthTaxDetail healthTaxDetail)
        {
            DeleteHealthTaxDetail(DatabaseUser, healthTaxDetail);
        }
        public void DeleteHealthTaxDetail(DatabaseUser user, HealthTaxDetail healthTaxDetail)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteHealthTaxDetail(user, healthTaxDetail);
        }
        #endregion

        #region employee payslip
        public EmployeePayslipCollection GetEmployeePayslip()
        {
            return GetEmployeePayslip(DatabaseUser);
        }
        public EmployeePayslipCollection GetEmployeePayslip(DatabaseUser user)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeePayslip(user);
        }
        #endregion

        #region labour cost
        public LabourCostCollection GetLabourCost(Decimal year)
        {
            return GetLabourCost(DatabaseUser, year);
        }
        public LabourCostCollection GetLabourCost(DatabaseUser user, Decimal year)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetLabourCost(user, year);
        }
        #endregion

        #region headcount
        public HeadcountCollection GetHeadcount()
        {
            return GetHeadcount(DatabaseUser);
        }
        public HeadcountCollection GetHeadcount(DatabaseUser user)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetHeadcount(user);
        }
        public HeadcountCollection GetHeadcountByOrganizationUnitLevelId(long organizationUnitLevelId)
        {
            return GetHeadcountByOrganizationUnitLevelId(DatabaseUser, organizationUnitLevelId);
        }
        public HeadcountCollection GetHeadcountByOrganizationUnitLevelId(DatabaseUser user, long organizationUnitLevelId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetHeadcountByOrganizationUnitLevelId(user, organizationUnitLevelId);
        }
        #endregion

        #region employee profile
        public EmployeeProfileCollection GetEmployeeProfile()
        {
            return GetEmployeeProfile(DatabaseUser);
        }
        public EmployeeProfileCollection GetEmployeeProfile(DatabaseUser user)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeProfile(user);
        }
        #endregion

        #region payroll breakdown
        public PayrollBreakdownCollection GetPayrollBreakdown(String userName, String breakdownType, String filteredBy)
        {
            return GetPayrollBreakdown(DatabaseUser, userName, breakdownType, filteredBy);
        }
        public PayrollBreakdownCollection GetPayrollBreakdown(DatabaseUser user, String userName, String breakdownType, String filteredBy)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPayrollBreakdown(user, userName, breakdownType, filteredBy);
        }
        #endregion

        #region employee custom field config
        public EmployeeCustomFieldConfigCollection GetEmployeeCustomFieldConfig(long? employeeCustomFieldConfigId)
        {
            return GetEmployeeCustomFieldConfig(DatabaseUser, employeeCustomFieldConfigId);
        }
        public EmployeeCustomFieldConfigCollection GetEmployeeCustomFieldConfig(DatabaseUser user, long? employeeCustomFieldConfigId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeCustomFieldConfig(user, employeeCustomFieldConfigId);
        }
        public EmployeeCustomFieldConfig InsertEmployeeCustomFieldConfig(EmployeeCustomFieldConfig item)
        {
            return InsertEmployeeCustomFieldConfig(DatabaseUser, item);
        }
        public EmployeeCustomFieldConfig InsertEmployeeCustomFieldConfig(DatabaseUser user, EmployeeCustomFieldConfig item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeeCustomFieldConfig(user, item);
        }
        public EmployeeCustomFieldConfig UpdateEmployeeCustomFieldConfig(EmployeeCustomFieldConfig item)
        {
            return UpdateEmployeeCustomFieldConfig(DatabaseUser, item);
        }
        public EmployeeCustomFieldConfig UpdateEmployeeCustomFieldConfig(DatabaseUser user, EmployeeCustomFieldConfig item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeeCustomFieldConfig(user, item);
        }
        public void DeleteEmployeeCustomFieldConfig(EmployeeCustomFieldConfig item)
        {
            DeleteEmployeeCustomFieldConfig(DatabaseUser, item);
        }
        public void DeleteEmployeeCustomFieldConfig(DatabaseUser user, EmployeeCustomFieldConfig item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeeCustomFieldConfig(user, item);
        }
        #endregion

        #region employee custom field
        public EmployeeCustomFieldCollection GetEmployeeCustomField(long? employeeCustomFieldId)
        {
            return GetEmployeeCustomField(DatabaseUser, employeeCustomFieldId);
        }
        public EmployeeCustomFieldCollection GetEmployeeCustomField(DatabaseUser user, long? employeeCustomFieldId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeCustomField(user, employeeCustomFieldId);
        }
        public EmployeeCustomField InsertEmployeeCustomField(EmployeeCustomField item)
        {
            return InsertEmployeeCustomField(DatabaseUser, item);
        }
        public EmployeeCustomField InsertEmployeeCustomField(DatabaseUser user, EmployeeCustomField item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeeCustomField(user, item);
        }
        public EmployeeCustomField UpdateEmployeeCustomField(EmployeeCustomField item)
        {
            return UpdateEmployeeCustomField(DatabaseUser, item);
        }
        public EmployeeCustomField UpdateEmployeeCustomField(DatabaseUser user, EmployeeCustomField item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeeCustomField(user, item);
        }
        public void DeleteEmployeeCustomField(EmployeeCustomField item)
        {
            DeleteEmployeeCustomField(DatabaseUser, item);
        }
        public void DeleteEmployeeCustomField(DatabaseUser user, EmployeeCustomField item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeeCustomField(user, item);
        }
        #endregion

        #region salary plan grade and effective detail
        public SalaryPlanGradeAndEffectiveDetailCollection GetSalaryPlanGradeAndEffectiveDetail(long salaryPlanId)
        {
            return GetSalaryPlanGradeAndEffectiveDetail(DatabaseUser, salaryPlanId);
        }
        public SalaryPlanGradeAndEffectiveDetailCollection GetSalaryPlanGradeAndEffectiveDetail(DatabaseUser user, long salaryPlanId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetSalaryPlanGradeAndEffectiveDetail(user, salaryPlanId);
        }

        public SalaryPlanGradeMinEffectiveDateCollection SelectMinEffectiveDate(long organizationUnitId)
        {
            return SelectMinEffectiveDate(DatabaseUser, organizationUnitId);
        }
        public SalaryPlanGradeMinEffectiveDateCollection SelectMinEffectiveDate(DatabaseUser user, long organizationUnitId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.SelectMinEffectiveDate(user, organizationUnitId);
        }

        public void AddNewSalaryPlanGradeAndSteps(SalaryPlanGradeAndEffectiveDetail item)
        {
            AddNewSalaryPlanGradeAndSteps(DatabaseUser, item);
        }
        public void AddNewSalaryPlanGradeAndSteps(DatabaseUser user, SalaryPlanGradeAndEffectiveDetail item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.AddNewSalaryPlanGradeAndSteps(user, item);
        }

        #endregion

        #region salary plan
        public SalaryPlanCollection GetSalaryPlan(long? salaryPlanId)
        {
            return GetSalaryPlan(DatabaseUser, salaryPlanId);
        }
        public SalaryPlanCollection GetSalaryPlan(DatabaseUser user, long? salaryPlanId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetSalaryPlan(user, salaryPlanId);
        }
        public SalaryPlanOrganizationUnitCollection GetSalaryPlanOrganizationUnit(string organizationUnit)
        {
            return GetSalaryPlanOrganizationUnit(DatabaseUser, organizationUnit);
        }
        public SalaryPlanOrganizationUnitCollection GetSalaryPlanOrganizationUnit(DatabaseUser user, string organizationUnit)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetSalaryPlanOrganizationUnit(user, organizationUnit);
        }
        #endregion

        #region salary plan grade
        public SalaryPlanGradeCollection GetSalaryPlanGrade(long? salaryPlanGradeId, long? salaryPlanId, string languageCode)
        {
            return GetSalaryPlanGrade(DatabaseUser, salaryPlanGradeId, salaryPlanId, languageCode);
        }
        public SalaryPlanGradeCollection GetSalaryPlanGrade(DatabaseUser user, long? salaryPlanGradeId, long? salaryPlanId, string languageCode)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetSalaryPlanGrade(user, salaryPlanGradeId, salaryPlanId, languageCode);
        }

        public SalaryPlanGradeOrgUnitDescriptionComboCollection PopulateComboBoxWithSalaryPlanGrades(long salaryPlanId, long organizationUnitLevelId)
        {
            return PopulateComboBoxWithSalaryPlanGrades(DatabaseUser, salaryPlanId, organizationUnitLevelId);
        }
        public SalaryPlanGradeOrgUnitDescriptionComboCollection PopulateComboBoxWithSalaryPlanGrades(DatabaseUser user, long salaryPlanId, long organizationUnitLevelId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.PopulateComboBoxWithSalaryPlanGrades(user, salaryPlanId, organizationUnitLevelId);
        }       
               
        #endregion

        #region salary plan grade step
        public SalaryPlanGradeStepCollection GetSalaryPlanGradeStep(long? salaryPlanGradeStepId, long? salaryPlanGradeId)
        {
            return GetSalaryPlanGradeStep(DatabaseUser, salaryPlanGradeStepId, salaryPlanGradeId);
        }
        public SalaryPlanGradeStepCollection GetSalaryPlanGradeStep(DatabaseUser user, long? salaryPlanGradeStepId, long? salaryPlanGradeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetSalaryPlanGradeStep(user, salaryPlanGradeStepId, salaryPlanGradeId);
        }
        public SalaryPlanGradeStepDetailCollection GetSalaryPlanGradeStepDetail(long? salaryPlanGradeStepDetailId, long? salaryPlanGradeStepId)
        {
            return GetSalaryPlanGradeStepDetail(DatabaseUser, salaryPlanGradeStepDetailId, salaryPlanGradeStepId);
        }
        public SalaryPlanGradeStepDetailCollection GetSalaryPlanGradeStepDetail(DatabaseUser user, long? salaryPlanGradeStepDetailId, long? salaryPlanGradeStepId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetSalaryPlanGradeStepDetail(user, salaryPlanGradeStepDetailId, salaryPlanGradeStepId);
        }
        #endregion

        #region real export to ftp
        public void CreateAndExportRealEmployeeFiles(string realFtpName, string laborFileName, string employeeFileName, string adjustmentFileName)
        {
            CreateAndExportRealEmployeeFiles(DatabaseUser, realFtpName, laborFileName, employeeFileName, adjustmentFileName);
        }
        public void CreateAndExportRealEmployeeFiles(DatabaseUser user, string realFtpName, string laborFileName, string employeeFileName, string adjustmentFileName)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.CreateAndExportRealEmployeeFiles(user, realFtpName, laborFileName, employeeFileName, adjustmentFileName);
        }
        #endregion

        #region labor level export
        public byte[] GenerateREALLaborLevelExportFile()
        {
            return GenerateREALLaborLevelExportFile(DatabaseUser);
        }
        public byte[] GenerateREALLaborLevelExportFile(DatabaseUser user)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GenerateREALLaborLevelExportFile(user);
        }
        #endregion

        #region adjustment rule export
        public byte[] GenerateREALAdjustmentRuleExportFile()
        {
            return GenerateREALAdjustmentRuleExportFile(DatabaseUser);
        }
        public byte[] GenerateREALAdjustmentRuleExportFile(DatabaseUser user)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GenerateREALAdjustmentRuleExportFile(user);
        }
        #endregion

        #region employee export
        public byte[] GenerateREALEmployeeExportFile()
        {
            return GenerateREALEmployeeExportFile(DatabaseUser);
        }
        public byte[] GenerateREALEmployeeExportFile(DatabaseUser user)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GenerateREALEmployeeExportFile(user);
        }

        public byte[] GenerateEmployeeBiographicsFile(IEnumerable<long> employeeIds)
        {
            return GenerateEmployeeBiographicsFile(DatabaseUser, employeeIds);
        }

        public byte[] GenerateEmployeeBiographicsFile(DatabaseUser user, IEnumerable<long> employeeIds)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GenerateEmployeeBiographicsFile(user, employeeIds);
        }

        #endregion        

        public TempBulkPayrollProcessCollection GetTempBulkPayrollProcess()
        {
            return GetTempBulkPayrollProcess(DatabaseUser);
        }
        public TempBulkPayrollProcessCollection GetTempBulkPayrollProcess(DatabaseUser user)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetTempBulkPayrollProcess(user);
        }

        #region benefit policy search
        public BenefitPolicySearchCollection GetBenefitPolicyReport(string criteria)
        {
            return GetBenefitPolicyReport(DatabaseUser, criteria);
        }
        public BenefitPolicySearchCollection GetBenefitPolicyReport(DatabaseUser user, string criteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetBenefitPolicyReport(user, criteria);
        }

        public void DeleteBenefitPolicyReport(long benefitPolicyId)
        {
            DeleteBenefitPolicyReport(DatabaseUser, benefitPolicyId);
        }
        public void DeleteBenefitPolicyReport(DatabaseUser user, long benefitPolicyId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteBenefitPolicyReport(user, benefitPolicyId);
        }
        #endregion

        #region benefit plan search
        public BenefitPlanSearchCollection GetBenefitPlanReport(String criteria)
        {
            return GetBenefitPlanReport(DatabaseUser, criteria);
        }
        public BenefitPlanSearchCollection GetBenefitPlanReport(DatabaseUser user, String criteria)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetBenefitPlanReport(user, criteria);
        }

        public void DeleteBenefitPlanReport(long entitlementId)
        {
            DeleteBenefitPlanReport(DatabaseUser, entitlementId);
        }
        public void DeleteBenefitPlanReport(DatabaseUser user, long benefitPlanId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteBenefitPlanReport(user, benefitPlanId);
        }
        #endregion

        #region benefit policy
        public BenefitPolicyCollection GetBenefitPolicy(long? benefitPolicyId)
        {
            return GetBenefitPolicy(DatabaseUser, benefitPolicyId);
        }
        public BenefitPolicyCollection GetBenefitPolicy(DatabaseUser user, long? benefitPolicyId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetBenefitPolicy(user, benefitPolicyId);
        }

        public BenefitPolicy InsertBenefitPolicy(BenefitPolicy benefitPolicy, BenefitPolicyPlanCollection benefitPolicyPlanCollection)
        {
            return InsertBenefitPolicy(DatabaseUser, benefitPolicy, benefitPolicyPlanCollection);
        }
        public BenefitPolicy InsertBenefitPolicy(DatabaseUser user, BenefitPolicy benefitPolicy, BenefitPolicyPlanCollection benefitPolicyPlanCollection)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertBenefitPolicy(user, benefitPolicy, benefitPolicyPlanCollection);
        }

        public BenefitPolicy UpdateBenefitPolicy(BenefitPolicy benefitPolicy, BenefitPolicyPlanCollection benefitPolicyPlanCollection)
        {
            return UpdateBenefitPolicy(DatabaseUser, benefitPolicy, benefitPolicyPlanCollection);
        }
        public BenefitPolicy UpdateBenefitPolicy(DatabaseUser user, BenefitPolicy benefitPolicy, BenefitPolicyPlanCollection benefitPolicyPlanCollection)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateBenefitPolicy(user, benefitPolicy, benefitPolicyPlanCollection);
        }
        #endregion

        #region benefit plan
        public BenefitPlanCollection GetBenefitPlan(long? benefitPlanId)
        {
            return GetBenefitPlan(DatabaseUser, benefitPlanId);
        }
        public BenefitPlanCollection GetBenefitPlan(DatabaseUser user, long? benefitPlanId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetBenefitPlan(user, benefitPlanId);
        }

        public BenefitPlan InsertBenefitPlan(BenefitPlan benefitPlan)
        {
            return InsertBenefitPlan(DatabaseUser, benefitPlan);
        }
        public BenefitPlan InsertBenefitPlan(DatabaseUser user, BenefitPlan benefitPlan)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertBenefitPlan(user, benefitPlan);
        }

        public BenefitPlan UpdateBenefitPlan(BenefitPlan benefitPlan)
        {
            return UpdateBenefitPlan(DatabaseUser, benefitPlan);
        }
        public BenefitPlan UpdateBenefitPlan(DatabaseUser user, BenefitPlan benefitPlan)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateBenefitPlan(user, benefitPlan);
        }
        #endregion

        #region benefit plan detail
        public BenefitPlanDetailCollection GetBenefitPlanDetail(long benefitPlanId, long benefitPlanDetailId)
        {
            return GetBenefitPlanDetail(DatabaseUser, benefitPlanId, benefitPlanDetailId);
        }
        public BenefitPlanDetailCollection GetBenefitPlanDetail(DatabaseUser user, long benefitPlanId, long benefitPlanDetailId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetBenefitPlanDetail(user, benefitPlanId, benefitPlanDetailId);
        }

        public BenefitPlanDetail InsertBenefitPlanDetail(BenefitPlanDetail benefitPlanDetail)
        {
            return InsertBenefitPlanDetail(DatabaseUser, benefitPlanDetail);
        }
        public BenefitPlanDetail InsertBenefitPlanDetail(DatabaseUser user, BenefitPlanDetail benefitPlanDetail)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertBenefitPlanDetail(user, benefitPlanDetail);
        }

        public BenefitPlanDetail UpdateBenefitPlanDetail(BenefitPlanDetail benefitPlanDetail)
        {
            return UpdateBenefitPlanDetail(DatabaseUser, benefitPlanDetail);
        }
        public BenefitPlanDetail UpdateBenefitPlanDetail(DatabaseUser user, BenefitPlanDetail benefitPlanDetail)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateBenefitPlanDetail(user, benefitPlanDetail);
        }

        public void DeleteBenefitPlanDetail(BenefitPlanDetail benefitPlanDetail)
        {
            DeleteBenefitPlanDetail(DatabaseUser, benefitPlanDetail);
        }
        public void DeleteBenefitPlanDetail(DatabaseUser user, BenefitPlanDetail benefitPlanDetail)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteBenefitPlanDetail(user, benefitPlanDetail);
        }
        #endregion

        #region benefit policy plan
        public BenefitPolicyPlanCollection GetBenefitPolicyPlan(long benefitPolicyId)
        {
            return GetBenefitPolicyPlan(DatabaseUser, benefitPolicyId);
        }
        public BenefitPolicyPlanCollection GetBenefitPolicyPlan(DatabaseUser user, long benefitPolicyId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetBenefitPolicyPlan(user, benefitPolicyId);
        }
        #endregion

        #region employee benefit
        public EmployeeBenefitCollection GetEmployeeBenefit(long employeeId)
        {
            return GetEmployeeBenefit(DatabaseUser, employeeId);
        }
        public EmployeeBenefitCollection GetEmployeeBenefit(DatabaseUser user, long employeeId)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeBenefit(user, employeeId);
        }

        public EmployeeBenefit UpdateEmployeeBenefit(EmployeeBenefit employeeBenefit)
        {
            return UpdateEmployeeBenefit(DatabaseUser, employeeBenefit);
        }
        public EmployeeBenefit UpdateEmployeeBenefit(DatabaseUser user, EmployeeBenefit employeeBenefit)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateEmployeeBenefit(user, employeeBenefit);
        }

        public EmployeeBenefit InsertEmployeeBenefit(EmployeeBenefit employeeBenefit)
        {
            return InsertEmployeeBenefit(DatabaseUser, employeeBenefit);
        }
        public EmployeeBenefit InsertEmployeeBenefit(DatabaseUser user, EmployeeBenefit employeeBenefit)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertEmployeeBenefit(user, employeeBenefit);
        }

        public void DeleteEmployeeBenefit(EmployeeBenefit employeeBenefit)
        {
            DeleteEmployeeBenefit(DatabaseUser, employeeBenefit);
        }
        public void DeleteEmployeeBenefit(DatabaseUser user, EmployeeBenefit employeeBenefit)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteEmployeeBenefit(user, employeeBenefit);
        }
        #endregion

        #endregion

        public SalaryPlanGradeRuleCollection GetSalaryPlanGradeRules()
        {
            return GetSalaryPlanGradeRules(DatabaseUser);
        }

        public SalaryPlanGradeRuleCollection GetSalaryPlanGradeRules(DatabaseUser user)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetSalaryPlanGradeRules(user);
        }

        public SalaryPlanGradeRule InsertPlanGradeRule(SalaryPlanGradeRule item)
        {
            return InsertPlanGradeRule(DatabaseUser, item);
        }
        public SalaryPlanGradeRule InsertPlanGradeRule(DatabaseUser user, SalaryPlanGradeRule item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertPlanGradeRule(user, item);
        }

        public SalaryPlanGradeRule UpdatePlanGradeRule(SalaryPlanGradeRule item)
        {
            return UpdatePlanGradeRule(DatabaseUser, item);
        }
        public SalaryPlanGradeRule UpdatePlanGradeRule(DatabaseUser user, SalaryPlanGradeRule item)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdatePlanGradeRule(user, item);
        }

        public SalaryPlanGradeRuleCodeCollection GetSalaryPlanGradeRuleCodes()
        {
            return GetSalaryPlanGradeRuleCodes(DatabaseUser);
        }

        public SalaryPlanGradeRuleCodeCollection GetSalaryPlanGradeRuleCodes(DatabaseUser user)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetSalaryPlanGradeRuleCodes(user);
        }

        public SalaryPlanGradeRuleCode InsertGradeRuleCode(SalaryPlanGradeRuleCode code)
        {
            return InsertSalaryPlanGradeRuleCode(DatabaseUser, code);
        }

        public SalaryPlanGradeRuleCode InsertSalaryPlanGradeRuleCode(DatabaseUser user, SalaryPlanGradeRuleCode code)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertSalaryPlanGradeRuleCode(user, code);
        }

        public SalaryPlanGradeRuleCode UpdateGradeRuleCode(SalaryPlanGradeRuleCode code)
        {
            return UpdateSalaryPlanGradeRuleCode(DatabaseUser, code);
        }

        public SalaryPlanGradeRuleCode UpdateSalaryPlanGradeRuleCode(DatabaseUser user, SalaryPlanGradeRuleCode code)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateSalaryPlanGradeRuleCode(user, code);
        }

        public void DeleteGradeRuleCode(SalaryPlanGradeRuleCode code)
        {
            DeleteSalaryPlanGradeRuleCode(DatabaseUser, code);
        }

        public void DeleteSalaryPlanGradeRuleCode(DatabaseUser user, SalaryPlanGradeRuleCode code)
        {
            using (HumanResourcesChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteSalaryPlanGradeRuleCode(user, code);
        }
    }

    public class HumanResourcesChannelWrapper : IDisposable
    {
        IHumanResources _channel = null;

        public IHumanResources Channel
        {
            get { return _channel; }
        }

        public HumanResourcesChannelWrapper(IHumanResources channel)
        {
            _channel = channel;
        }

        public void Dispose()
        {
            try
            {
                ((IClientChannel)_channel).Close();
            }
            catch (FaultException)
            {
                ((IClientChannel)_channel).Abort();
            }
            catch (CommunicationException)
            {
                ((IClientChannel)_channel).Abort();
            }
        }
    }
}