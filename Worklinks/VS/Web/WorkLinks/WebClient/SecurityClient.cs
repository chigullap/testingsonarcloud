﻿using System;
using System.ServiceModel;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Web.BusinessService.Contract;

namespace WorkLinks.WebClient
{
    public class SecurityClient : BaseClient, ISecurity
    {
        #region fields
        private ChannelFactory<ISecurity> _factory = null;
        private String _wcfServiceUrl = null;
        private long _maxReceivedMessageSize;
        private long _receiveTimeoutTicks;
        private long _sendTimeoutTicks;
        #endregion

        #region properties
        private ChannelFactory<ISecurity> Factory
        {
            get
            {
                if (_factory == null)
                {
                    BasicHttpBinding binding = new BasicHttpBinding();
                    binding.MaxReceivedMessageSize = _maxReceivedMessageSize;
                    binding.ReceiveTimeout = new TimeSpan(_receiveTimeoutTicks);
                    binding.SendTimeout = new TimeSpan(_sendTimeoutTicks);

                    _factory = new ChannelFactory<ISecurity>(binding, new EndpointAddress(_wcfServiceUrl));
                }

                return _factory;
            }
        }
        #endregion

        public SecurityChannelWrapper CreateChannel()
        {
            return new SecurityChannelWrapper(Factory.CreateChannel());
        }

        #region constructors/destructors
        public SecurityClient(String wcfServiceUrl, String wcfServiceName, long maxReceivedMessageSize, long receiveTimeoutTicks, long sendTimeoutTicks)
        {
            _wcfServiceUrl = new Uri(new Uri(wcfServiceUrl), wcfServiceName).ToString();
            _maxReceivedMessageSize = maxReceivedMessageSize;
            _receiveTimeoutTicks = receiveTimeoutTicks;
            _sendTimeoutTicks = sendTimeoutTicks;
        }
        #endregion

        #region wcf functions

        #region security user
        public bool ValidateUserWithHistory(SecurityUserLoginHistory history, string password, int passwordAttemptWindow, int maxInvalidPasswordAttempts, String loginExpiryDays)
        {
            return ValidateUserWithHistory(Common.ApplicationParameter.DatabaseName, history, password, passwordAttemptWindow, maxInvalidPasswordAttempts, loginExpiryDays);
        }
        public bool ValidateUserWithHistory(String databaseName, SecurityUserLoginHistory history, string password, int passwordAttemptWindow, int maxInvalidPasswordAttempts, String loginExpiryDays)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ValidateUserWithHistory(databaseName, history, password, passwordAttemptWindow, maxInvalidPasswordAttempts, loginExpiryDays);
        }
        public bool ValidateUser(string userName, string password)
        {
            return ValidateUser(Common.ApplicationParameter.DatabaseName, userName, password);
        }
        public bool ValidateUser(String databaseName, string userName, string password)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ValidateUser(databaseName, userName, password);
        }
        public bool DoesSecurityUserAlreadyExist(long employeeId)
        {
            return DoesSecurityUserAlreadyExist(DatabaseUser, employeeId);
        }
        public bool DoesSecurityUserAlreadyExist(DatabaseUser user, long employeeId)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.DoesSecurityUserAlreadyExist(user, employeeId);
        }
        public bool ValidateUserName(string username)
        {
            return ValidateUserName(DatabaseUser, username);
        }
        public bool ValidateUserName(DatabaseUser user, string username)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ValidateUserName(user, username);
        }
        public bool ValidatePasswordHistory(string username, string password)
        {
            return ValidatePasswordHistory(DatabaseUser, username, password);
        }
        public bool ValidatePasswordHistory(DatabaseUser user, string username, string password)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ValidatePasswordHistory(user, username, password);
        }
        //public void UpdateLogoutUser(string userName)
        //{
        //    UpdateLogoutUser(DatabaseUser, userName);
        //}
        //public void UpdateLogoutUser(DatabaseUser user, string userName)
        //{
        //    using (SecurityChannelWrapper wrapper = CreateChannel())
        //        wrapper.Channel.UpdateLogoutUser(user, userName);
        //}
        //public void UpdateLogoutUser(String databaseName, String userName)
        //{
        //    DatabaseUser dbUser = new DatabaseUser() { DatabaseName = databaseName, UserName = userName };
        //    UpdateLogoutUser(dbUser, userName);
        //}
        public SecurityUserCollection GetSecurityUser(string userName)
        {
            return GetSecurityUser(Common.ApplicationParameter.DatabaseName, userName);
        }
        public SecurityUserCollection GetSecurityUser(String databaseName, string userName)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetSecurityUser(databaseName, userName);
        }
        public SecurityUser InsertUser(SecurityUser securityUser, String authDatabaseName, String ngSecurityClientId, String loginExpiryHours, String ngSecurity, String _ngDbSecurity, String _ngApiSecurity)
        {
            return InsertUser(DatabaseUser, securityUser, authDatabaseName, ngSecurityClientId, loginExpiryHours, ngSecurity, _ngDbSecurity, _ngApiSecurity);
        }
        public SecurityUser InsertUser(DatabaseUser user, SecurityUser securityUser, String authDatabaseName, String ngSecurityClientId, String loginExpiryHours, String ngSecurity, String _ngDbSecurity, String _ngApiSecurity)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertUser(user, securityUser, authDatabaseName, ngSecurityClientId, loginExpiryHours, ngSecurity, _ngDbSecurity, _ngApiSecurity);
        }
        public void UpdateSecurityUser(string username, string newPassword, SecurityUser securityUser, String passwordExpiryDays, String loginExpiryHours, bool onlyUpdateLocalDbForLockedOutFlag)
        {
            UpdateSecurityUser(DatabaseUser, username, newPassword, securityUser, passwordExpiryDays, loginExpiryHours, Common.ApplicationParameter.AuthDatabaseName, onlyUpdateLocalDbForLockedOutFlag);
        }
        public void UpdateSecurityUser(DatabaseUser user, string username, string newPassword, SecurityUser securityUser, String passwordExpiryDays, String loginExpiryHours, string adminDatabaseName, bool onlyUpdateLocalDbForLockedOutFlag)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.UpdateSecurityUser(user, username, newPassword, securityUser, passwordExpiryDays, loginExpiryHours, adminDatabaseName, onlyUpdateLocalDbForLockedOutFlag);
        }
        public void UpdateSecurityUserPassword(string username, string newPassword, SecurityUser securityUser, String passwordExpiryDays, String loginExpiryHours)
        {
            UpdateSecurityUserPassword(DatabaseUser, username, newPassword, securityUser, passwordExpiryDays, loginExpiryHours, Common.ApplicationParameter.AuthDatabaseName);
        }
        public void UpdateSecurityUserPassword(DatabaseUser user, string username, string newPassword, SecurityUser securityUser, String passwordExpiryDays, String loginExpiryHours, string adminDatabaseName)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.UpdateSecurityUserPassword(user, username, newPassword, securityUser, passwordExpiryDays, loginExpiryHours,adminDatabaseName);
        }
        #endregion

        #region Security Categories
        public SecurityCategoryCollection GetSecurityCategories(bool isViewMode)
        {
            return GetSecurityCategories(DatabaseUser, isViewMode);
        }
        public SecurityCategoryCollection GetSecurityCategories(DatabaseUser user, bool isViewMode)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetSecurityCategories(user, isViewMode);
        }
        public void UpdateCategories(SecurityCategoryCollection securityCategoryColl)
        {
            UpdateCategories(DatabaseUser, securityCategoryColl);
        }
        public void UpdateCategories(DatabaseUser user, SecurityCategoryCollection securityCategoryColl)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.UpdateCategories(user, securityCategoryColl);
        }
        public void RebuildSecurity()
        {
            RebuildSecurity(DatabaseUser);
        }
        public void RebuildSecurity(DatabaseUser user)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.RebuildSecurity(user);
        }
        #endregion

        #region user admin
        public UserSummaryCollection GetUserSummary(UserAdminSearchCriteria criteria)
        {
            return GetUserSummary(DatabaseUser, criteria);
        }
        public UserSummaryCollection GetUserSummary(DatabaseUser user, UserAdminSearchCriteria criteria)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetUserSummary(user, criteria);
        }
        public SecurityUserCollection GetPersonUserDetails(long personId)
        {
            return GetPersonUserDetails(DatabaseUser, personId);
        }
        public SecurityUserCollection GetPersonUserDetails(DatabaseUser user, long personId)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPersonUserDetails(user, personId);
        }
        public void DeleteUser(long personId)
        {
            DeleteUser(DatabaseUser, personId);
        }
        public void DeleteUser(DatabaseUser user, long personId)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteUser(user, personId);
        }

        public int SelectSecurityClientUser(string authDatabaseName, long securityUserId)
        {
            return SelectSecurityClientUser(DatabaseUser, authDatabaseName, securityUserId);
        }
        public int SelectSecurityClientUser(DatabaseUser user, string authDatabaseName, long securityUserId)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.SelectSecurityClientUser(user, authDatabaseName, securityUserId);
        }

        #endregion

        #region security marking
        public SecurityMarkingCollection GetSecurityMarking(int hierarchicalSortOrder)
        {
            return GetSecurityMarking(DatabaseUser, hierarchicalSortOrder);
        }
        public SecurityMarkingCollection GetSecurityMarking(DatabaseUser user, int hierarchicalSortOrder)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetSecurityMarking(user, hierarchicalSortOrder);
        }
        #endregion

        #region security label role
        public SecurityLabelRoleCollection GetSecurityLabelRole(long securityRoleId)
        {
            return GetSecurityLabelRole(DatabaseUser, securityRoleId);
        }
        public SecurityLabelRoleCollection GetSecurityLabelRole(DatabaseUser user, long securityRoleId)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetSecurityLabelRole(user, securityRoleId);
        }
        public SecurityLabelRoleCollection UpdateSecurityLabelRole(long roleId, SecurityLabelRoleCollection labels)
        {
            return UpdateSecurityLabelRole(DatabaseUser, roleId, labels);
        }
        public SecurityLabelRoleCollection UpdateSecurityLabelRole(DatabaseUser user, long roleId, SecurityLabelRoleCollection labels)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateSecurityLabelRole(user, roleId, labels);
        }
        #endregion

        #region Role Description
        public RoleDescriptionCollection GetRoleDescriptions(String criteria, long? roleId, bool? worklinksAdministrationFlag)
        {
            return GetRoleDescriptions(DatabaseUser, criteria, roleId, worklinksAdministrationFlag);
        }
        public RoleDescriptionCollection GetRoleDescriptions(DatabaseUser user, String criteria, long? roleId, bool? worklinksAdministrationFlag)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetRoleDescriptions(user, criteria, roleId, worklinksAdministrationFlag);
        }
        public RoleDescription UpdateRoleDesc(String authDatabaseName, String ngSecurityClientId, RoleDescription roleDesc)
        {
            return UpdateRoleDesc(DatabaseUser, authDatabaseName, ngSecurityClientId, roleDesc);
        }
        public RoleDescription UpdateRoleDesc(DatabaseUser user, String authDatabaseName, String ngSecurityClientId, RoleDescription roleDesc)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateRoleDesc(user, authDatabaseName, ngSecurityClientId, roleDesc);
        }
        public RoleDescription InsertRoleDescription(String authDatabaseName, String ngSecurityClientId, RoleDescription roleDesc)
        {
            return InsertRoleDescription(DatabaseUser, authDatabaseName, ngSecurityClientId, roleDesc);
        }
        public RoleDescription InsertRoleDescription(DatabaseUser user, String authDatabaseName, String ngSecurityClientId, RoleDescription roleDesc)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertRoleDescription(user, authDatabaseName, ngSecurityClientId, roleDesc);
        }
        #endregion

        #region Group Description
        public GroupDescriptionCollection GetUserGroups(long securityUserId, bool isViewMode, bool? worklinksAdministrationFlag)
        {
            return GetUserGroups(DatabaseUser, securityUserId, isViewMode, worklinksAdministrationFlag);
        }
        public GroupDescriptionCollection GetUserGroups(DatabaseUser user, long securityUserId, bool isViewMode, bool? worklinksAdministrationFlag)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetUserGroups(user, securityUserId, isViewMode, worklinksAdministrationFlag);
        }
        public GroupDescriptionCollection GetGroupDescriptions(string criteria, long? groupId, bool? worklinksAdministrationFlag)
        {
            return GetGroupDescriptions(DatabaseUser, criteria, groupId, worklinksAdministrationFlag);
        }
        public GroupDescriptionCollection GetGroupDescriptions(DatabaseUser user, string criteria, long? groupId, bool? worklinksAdministrationFlag)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetGroupDescriptions(user, criteria, groupId, worklinksAdministrationFlag);
        }
        public GroupDescription UpdateGroupDesc(string authDatabaseName, string ngSecurityClientId, GroupDescription grpDesc)
        {
            return UpdateGroupDesc(DatabaseUser, authDatabaseName, ngSecurityClientId, grpDesc);
        }
        public GroupDescription UpdateGroupDesc(DatabaseUser user, string authDatabaseName, string ngSecurityClientId, GroupDescription grpDesc)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateGroupDesc(user, authDatabaseName, ngSecurityClientId, grpDesc);
        }
        public GroupDescription InsertGroupDescription(String authDatabaseName, String ngSecurityClientId, GroupDescription grpDesc)
        {
            return InsertGroupDescription(DatabaseUser, authDatabaseName, ngSecurityClientId, grpDesc);
        }
        public GroupDescription InsertGroupDescription(DatabaseUser user, String authDatabaseName, String ngSecurityClientId, GroupDescription grpDesc)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertGroupDescription(user, authDatabaseName, ngSecurityClientId, grpDesc);
        }
        public void UpdateUserGroups(string userName, long securityUserId, GroupDescription[] groupArray)
        {
            UpdateUserGroups(DatabaseUser, Common.ApplicationParameter.AuthDatabaseName, Common.ApplicationParameter.SecurityClientUsedByAngular, userName, securityUserId, groupArray);
        }
        public void UpdateUserGroups(DatabaseUser user, string authDatabaseName, string ngSecurityClientId, string userName, long securityUserId, GroupDescription[] groupArray)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.UpdateUserGroups(user, authDatabaseName, ngSecurityClientId, userName, securityUserId, groupArray);
        }
        #endregion

        #region Form Security
        public FormSecurityCollection GetFormSecurityInfo(long roleId, bool? worklinksAdministrationFlag)
        {
            return GetFormSecurityInfo(DatabaseUser, roleId, worklinksAdministrationFlag);
        }
        public FormSecurityCollection GetFormSecurityInfo(DatabaseUser user, long roleId, bool? worklinksAdministrationFlag)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetFormSecurityInfo(user, roleId, worklinksAdministrationFlag);
        }
        public void UpdateFormSecurity(String authDatabaseName, String ngSecurityClientId, FormSecurity[] formsArray, long roleId)
        {
            UpdateFormSecurity(DatabaseUser, authDatabaseName, ngSecurityClientId, formsArray, roleId);
        }
        public void UpdateFormSecurity(DatabaseUser user, String authDatabaseName, String ngSecurityClientId, FormSecurity[] formsArray, long roleId)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.UpdateFormSecurity(user, authDatabaseName, ngSecurityClientId, formsArray, roleId);
        }
        #endregion

        public string GetEmailByEmployeeId(long employeeId)
        {
            return GetEmailByEmployeeId(DatabaseUser, employeeId);
        }
        public string GetEmailByEmployeeId(DatabaseUser user, long employeeId)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmailByEmployeeId(user, employeeId);
        }

        #region SecurityUserProfile
        public SecurityUserProfileCollection GetSecurityUserProfile(String databaseName, String userName)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetSecurityUserProfile(databaseName, userName);
        }
        public SecurityUserProfile UpdateSecurityUserProfile(SecurityUserProfile item)
        {
            return UpdateSecurityUserProfile(DatabaseUser, item);
        }
        public SecurityUserProfile UpdateSecurityUserProfile(DatabaseUser user, SecurityUserProfile item)
        {
            using (SecurityChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateSecurityUserProfile(user, item);
        }
        #endregion

        #endregion
    }

    public class SecurityChannelWrapper : IDisposable
    {
        ISecurity _channel = null;
        public ISecurity Channel
        {
            get
            {
                return _channel;
            }
        }
        public SecurityChannelWrapper(ISecurity channel)
        {
            _channel = channel;
        }
        public void Dispose()
        {
            try
            {
                ((IClientChannel)_channel).Close();
            }
            catch (FaultException)
            {
                ((IClientChannel)_channel).Abort();
            }
            catch (CommunicationException)
            {
                ((IClientChannel)_channel).Abort();
            }
        }
    }
}