﻿using System;
using System.ServiceModel;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Web.BusinessService.Contract;

namespace WorkLinks.WebClient
{
    public class CodeClient : BaseClient, ICode
    {
        #region fields
        private ChannelFactory<ICode> _factory = null;
        private String _wcfServiceUrl = null;
        private long _maxReceivedMessageSize;
        private long _receiveTimeoutTicks;
        private long _sendTimeoutTicks;
        #endregion

        #region properties
        private ChannelFactory<ICode> Factory
        {
            get
            {
                if (_factory == null)
                {
                    BasicHttpBinding binding = new BasicHttpBinding();
                    binding.MaxReceivedMessageSize = _maxReceivedMessageSize;
                    binding.ReceiveTimeout = new TimeSpan(_receiveTimeoutTicks);
                    binding.SendTimeout = new TimeSpan(_sendTimeoutTicks);

                    _factory = new ChannelFactory<ICode>(binding, new EndpointAddress(_wcfServiceUrl));
                }

                return _factory;
            }
        }
        #endregion

        public CodeChannelWrapper CreateChannel()
        {
            return new CodeChannelWrapper(Factory.CreateChannel());
        }

        #region constructors/destructors
        public CodeClient(String wcfServiceUrl, String wcfServiceName, long maxReceivedMessageSize, long receiveTimeoutTicks, long sendTimeoutTicks)
        {
            _wcfServiceUrl = new Uri(new Uri(wcfServiceUrl), wcfServiceName).ToString();
            _maxReceivedMessageSize = maxReceivedMessageSize;
            _receiveTimeoutTicks = receiveTimeoutTicks;
            _sendTimeoutTicks = sendTimeoutTicks;
        }
        #endregion

        #region wcf functions

        #region field language editor
        public LanguageEntityCollection GetFieldLanguageInfo(Decimal formId, String attributeIdentifier, String englishLanguageCode)
        {
            return GetFieldLanguageInfo(DatabaseUser, formId, attributeIdentifier, englishLanguageCode);
        }
        public LanguageEntityCollection GetFieldLanguageInfo(DatabaseUser user, Decimal formId, String attributeIdentifier, String englishLanguageCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetFieldLanguageInfo(user, formId, attributeIdentifier, englishLanguageCode);
        }
        public LanguageEntityCollection UpdateFieldLanguage(LanguageEntity[] entityCollection, String englishLanguageCode, String frenchLanguageCode)
        {
            return UpdateFieldLanguage(DatabaseUser, entityCollection, englishLanguageCode, frenchLanguageCode);
        }
        public LanguageEntityCollection UpdateFieldLanguage(DatabaseUser user, LanguageEntity[] entityCollection, String englishLanguageCode, String frenchLanguageCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateFieldLanguage(user, entityCollection, englishLanguageCode, frenchLanguageCode);
        }
        #endregion

        #region field editor
        public FormDescriptionCollection GetFormInfo(String criteria)
        {
            return GetFormInfo(DatabaseUser, criteria);
        }
        public FormDescriptionCollection GetFormInfo(DatabaseUser user, String criteria)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetFormInfo(user, criteria);
        }
        public FieldEntityCollection GetFieldInfo(Decimal formId, String attributeIdentifier, int roleId)
        {
            return GetFieldInfo(DatabaseUser, formId, attributeIdentifier, roleId);
        }
        public FieldEntityCollection GetFieldInfo(DatabaseUser user, Decimal formId, String attributeIdentifier, int roleId)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetFieldInfo(user, formId, attributeIdentifier, roleId);
        }
        public FieldEntityCollection UpdateFieldEntity(FieldEntity[] entityCollection)
        {
            return UpdateFieldEntity(DatabaseUser, entityCollection);
        }
        public FieldEntityCollection UpdateFieldEntity(DatabaseUser user, FieldEntity[] entityCollection)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateFieldEntity(user, entityCollection);
        }
        #endregion

        #region code table description
        public CodeTableDescriptionCollection GetCodeTableDescriptionRows(String tableName, String code)
        {
            return GetCodeTableDescriptionRows(DatabaseUser, tableName, code);
        }
        public CodeTableDescriptionCollection GetCodeTableDescriptionRows(DatabaseUser user, String tableName, String code)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetCodeTableDescriptionRows(user, tableName, code);
        }
        public CodeTable UpdateCodeTableDescRow(CodeTable codeTableDesc, String codeTableName)
        {
            return UpdateCodeTableDescRow(DatabaseUser, codeTableDesc, codeTableName);
        }
        public CodeTable UpdateCodeTableDescRow(DatabaseUser user, CodeTable codeTableDesc, String codeTableName)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateCodeTableDescRow(user, codeTableDesc, codeTableName);
        }
        public void DeleteCodeTableDescRow(CodeTable codeTableDesc, String codeTableName)
        {
            DeleteCodeTableDescRow(DatabaseUser, codeTableDesc, codeTableName);
        }
        public void DeleteCodeTableDescRow(DatabaseUser user, CodeTable codeTableDesc, String codeTableName)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteCodeTableDescRow(user, codeTableDesc, codeTableName);
        }
        public CodeTable InsertCodeTableDescRow(CodeTable codeTableDesc, String codeTableName)
        {
            return InsertCodeTableDescRow(DatabaseUser, codeTableDesc, codeTableName);
        }
        public CodeTable InsertCodeTableDescRow(DatabaseUser user, CodeTable codeTableDesc, String codeTableName)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertCodeTableDescRow(user, codeTableDesc, codeTableName);
        }
        #endregion

        #region code table
        public CodeTableCollection GetCodeTableRows(String tableName, String parentTableName)
        {
            return GetCodeTableRows(DatabaseUser, tableName, parentTableName);
        }
        public CodeTableCollection GetCodeTableRows(DatabaseUser user, String tableName, String parentTableName)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetCodeTableRows(user, tableName, parentTableName);
        }
        public CodeTable UpdateCodeTableData(CodeTable codeTable, String codeTableName, String parentTableName)
        {
            return UpdateCodeTableData(DatabaseUser, codeTable, codeTableName, parentTableName);
        }
        public CodeTable UpdateCodeTableData(DatabaseUser user, CodeTable codeTable, String codeTableName, String parentTableName)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateCodeTableData(user, codeTable, codeTableName, parentTableName);
        }
        public CodeTable UpdateCodeTableRow(CodeTable codeTable, String codeTableName, String parentTableName)
        {
            return UpdateCodeTableRow(DatabaseUser, codeTable, codeTableName, parentTableName);
        }
        public CodeTable UpdateCodeTableRow(DatabaseUser user, CodeTable codeTable, String codeTableName, String parentTableName)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateCodeTableRow(user, codeTable, codeTableName, parentTableName);
        }
        public CodeTable InsertCodeTableData(CodeTable codeTable, String codeTableName, String parentTableName)
        {
            return InsertCodeTableData(DatabaseUser, codeTable, codeTableName, parentTableName);
        }
        public CodeTable InsertCodeTableData(DatabaseUser user, CodeTable codeTable, String codeTableName, String parentTableName)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertCodeTableData(user, codeTable, codeTableName, parentTableName);
        }
        public void DeleteCodeTableData(CodeTable codeTable, String codeTableName)
        {
            DeleteCodeTableData(DatabaseUser, codeTable, codeTableName);
        }
        public void DeleteCodeTableData(DatabaseUser user, CodeTable codeTable, String codeTableName)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteCodeTableData(user, codeTable, codeTableName);
        }
        public void DeleteCodeTableRow(CodeTable codeTable, String codeTableName)
        {
            DeleteCodeTableRow(DatabaseUser, codeTable, codeTableName);
        }
        public void DeleteCodeTableRow(DatabaseUser user, CodeTable codeTable, String codeTableName)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                DeleteCodeTableRow(user, codeTable, codeTableName);
        }
        public CodeTable InsertCodeTableRow(CodeTable codeTable, String codeTableName, String parentTableName)
        {
            return InsertCodeTableRow(DatabaseUser, codeTable, codeTableName, parentTableName);
        }
        public CodeTable InsertCodeTableRow(DatabaseUser user, CodeTable codeTable, String codeTableName, String parentTableName)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertCodeTableRow(user, codeTable, codeTableName, parentTableName);
        }
        public CodeTableIndexCollection GetCodeTableIndex(String tableName)
        {
            return GetCodeTableIndex(DatabaseUser, tableName);
        }
        public CodeTableIndexCollection GetCodeTableIndex(DatabaseUser user, String tableName)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetCodeTableIndex(user, tableName);
        }
        public CodeCollection GetCodeTable(String language, CodeTableType type, String code)
        {
            return GetCodeTable(Common.ApplicationParameter.DatabaseName, language, type, code);
        }
        /// <summary>
        /// Use this method when need to retrieve data by parent column name and value.
        /// </summary>
        /// <param name="language"></param>
        /// <param name="type"></param>
        /// <param name="code"></param>
        /// <param name="parentColumnName"></param>
        /// <param name="parentColumnValue"></param>
        /// <returns></returns>
        public CodeCollection GetCodeTable(String language, CodeTableType type, String code, String parentColumnName, String parentColumnValue)
        {
            return GetCodeTableByParentColumn(Common.ApplicationParameter.DatabaseName, language, type, code, parentColumnName, parentColumnValue);
        }
        public CodeCollection GetCodeTable(String databaseName, String language, CodeTableType type, String code)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetCodeTable(databaseName, language, type, code);
        }
        /// <summary>
        /// Implementation of method of ICode.  
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="language"></param>
        /// <param name="type"></param>
        /// <param name="code"></param>
        /// <param name="parentColumnName"></param>
        /// <param name="parentColumnValue"></param>
        /// <returns></returns>
        public CodeCollection GetCodeTableByParentColumn(String databaseName, String language, CodeTableType type, String code, String parentColumnName, String parentColumnValue)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetCodeTableByParentColumn(databaseName, language, type, code, parentColumnName, parentColumnValue);
        }
        #endregion

        #region salary plan code table
        public SalaryPlanCodeTableCollection GetSalaryPlanCodeTableRows(String tableName, String parentSalaryPlanCode)
        {
            return GetSalaryPlanCodeTableRows(DatabaseUser, tableName, parentSalaryPlanCode);
        }
        public SalaryPlanCodeTableCollection GetSalaryPlanCodeTableRows(DatabaseUser user, String tableName, String parentSalaryPlanCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetSalaryPlanCodeTableRows(user, tableName, parentSalaryPlanCode);
        }
        public SalaryPlanCodeTable InsertSalaryPlanCodeTableData(SalaryPlanCodeTable salaryPlanCodeTable)
        {
            return InsertSalaryPlanCodeTableData(DatabaseUser, salaryPlanCodeTable);
        }
        public SalaryPlanCodeTable InsertSalaryPlanCodeTableData(DatabaseUser user, SalaryPlanCodeTable salaryPlanCodeTable)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertSalaryPlanCodeTableData(user, salaryPlanCodeTable);
        }
        public SalaryPlanCodeTable UpdateSalaryPlanCodeTableData(SalaryPlanCodeTable salaryPlanCodeTable)
        {
            return UpdateSalaryPlanCodeTableData(DatabaseUser, salaryPlanCodeTable);
        }
        public SalaryPlanCodeTable UpdateSalaryPlanCodeTableData(DatabaseUser user, SalaryPlanCodeTable salaryPlanCodeTable)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateSalaryPlanCodeTableData(user, salaryPlanCodeTable);
        }
        public void DeleteSalaryPlanCodeTableData(SalaryPlanCodeTable salaryPlanCodeTable)
        {
            DeleteSalaryPlanCodeTableData(DatabaseUser, salaryPlanCodeTable);
        }
        public void DeleteSalaryPlanCodeTableData(DatabaseUser user, SalaryPlanCodeTable salaryPlanCodeTable)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteSalaryPlanCodeTableData(user, salaryPlanCodeTable);
        }
        #endregion

        #region personAddressType code table
        public PersonAddressTypeCodeTableCollection GetPersonAddressTypeCodeTableRows()
        {
            return GetPersonAddressTypeCodeTableRows(DatabaseUser);
        }
        public PersonAddressTypeCodeTableCollection GetPersonAddressTypeCodeTableRows(DatabaseUser user)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPersonAddressTypeCodeTableRows(user);
        }
        public PersonAddressTypeCodeTable GetPersonAddressTypeCodeTableRow(short priority)
        {
            return GetPersonAddressTypeCodeTableRow(DatabaseUser, priority);
        }
        public PersonAddressTypeCodeTable GetPersonAddressTypeCodeTableRow(DatabaseUser user, short priority)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPersonAddressTypeCodeTableRow(user, priority);
        }
        public PersonAddressTypeCodeTable UpdatePersonAddressTypeCodeTableData(PersonAddressTypeCodeTable personAddressTypeCodeTable)
        {
            return UpdatePersonAddressTypeCodeTableData(DatabaseUser, personAddressTypeCodeTable);
        }
        public PersonAddressTypeCodeTable UpdatePersonAddressTypeCodeTableData(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTable)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdatePersonAddressTypeCodeTableData(user, personAddressTypeCodeTable);
        }
        public void DeletePersonAddressTypeCodeTableData(PersonAddressTypeCodeTable personAddressTypeCodeTable)
        {
            DeletePersonAddressTypeCodeTableData(DatabaseUser, personAddressTypeCodeTable);
        }
        public void DeletePersonAddressTypeCodeTableData(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTable)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeletePersonAddressTypeCodeTableData(user, personAddressTypeCodeTable);
        }
        public PersonAddressTypeCodeTable InsertPersonAddressTypeCodeTableData(PersonAddressTypeCodeTable personAddressTypeCodeTable)
        {
            return InsertPersonAddressTypeCodeTableData(DatabaseUser, personAddressTypeCodeTable);
        }
        public PersonAddressTypeCodeTable InsertPersonAddressTypeCodeTableData(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTable)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertPersonAddressTypeCodeTableData(user, personAddressTypeCodeTable);
        }
        #endregion

        #region contact channel
        public CodeCollection GetContactChannelTypeCode(String contactChannelCategoryCode)
        {
            return GetContactChannelTypeCode(DatabaseUser, contactChannelCategoryCode);
        }
        public CodeCollection GetContactChannelTypeCode(DatabaseUser user, String contactChannelCategoryCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetContactChannelTypeCode(user, contactChannelCategoryCode);
        }
        public CodeCollection GetContactChannelTypeCodeRemovingTypesInUse(String contactChannelCategoryCode, long personId)
        {
            return GetContactChannelTypeCodeRemovingTypesInUse(DatabaseUser, contactChannelCategoryCode, personId);
        }
        public CodeCollection GetContactChannelTypeCodeRemovingTypesInUse(DatabaseUser user, String contactChannelCategoryCode, long personId)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetContactChannelTypeCodeRemovingTypesInUse(user, contactChannelCategoryCode, personId);
        }
        public CodeCollection GetContactChannelTypeCodeRemovingTypesInUseLeavingSelected(String contactChannelCategoryCode, long personId, String selectedCode)
        {
            return GetContactChannelTypeCodeRemovingTypesInUseLeavingSelected(DatabaseUser, contactChannelCategoryCode, personId, selectedCode);
        }
        public CodeCollection GetContactChannelTypeCodeRemovingTypesInUseLeavingSelected(DatabaseUser user, String contactChannelCategoryCode, long personId, String selectedCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetContactChannelTypeCodeRemovingTypesInUseLeavingSelected(user, contactChannelCategoryCode, personId, selectedCode);
        }
        #endregion

        #region paycodes
        public CodeCollection GetPaycodeTypeCode(String paycode)
        {
            return GetPaycodeTypeCode(DatabaseUser, paycode);
        }
        public CodeCollection GetPaycodeTypeCode(DatabaseUser user, String paycode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPaycodeTypeCode(user, paycode);
        }
        public CodeCollection GetPaycodesByType(String paycodeTypeCode)
        {
            return GetPaycodesByType(DatabaseUser, paycodeTypeCode);
        }
        public CodeCollection GetPaycodesByType(DatabaseUser user, String paycodeTypeCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPaycodesByType(user, paycodeTypeCode);
        }
        public CodeCollection GetPaycodeByCode(String paycodeCode)
        {
            return GetPaycodeByCode(DatabaseUser, paycodeCode);
        }
        public CodeCollection GetPaycodeByCode(DatabaseUser user, String paycodeCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPaycodeByCode(user, paycodeCode);
        }
        public CodeCollection GetPaycodesByTypeRemovingPaycodesInUse(String paycodeTypeCode, long? employeeId)
        {
            return GetPaycodesByTypeRemovingPaycodesInUse(DatabaseUser, paycodeTypeCode, employeeId);
        }
        public CodeCollection GetPaycodesByTypeRemovingPaycodesInUse(DatabaseUser user, String paycodeTypeCode, long? employeeId)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPaycodesByTypeRemovingPaycodesInUse(user, paycodeTypeCode, employeeId);
        }
        public CodeCollection GetPaycodesByTypeRemovingGlobalPaycodesInUse(String paycodeTypeCode)
        {
            return GetPaycodesByTypeRemovingGlobalPaycodesInUse(DatabaseUser, paycodeTypeCode);
        }
        public CodeCollection GetPaycodesByTypeRemovingGlobalPaycodesInUse(DatabaseUser user, String paycodeTypeCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPaycodesByTypeRemovingGlobalPaycodesInUse(user, paycodeTypeCode);
        }
        public CodeCollection RemoveNonRecurringIncomeCodes(CodeCollection collection)
        {
            return RemoveNonRecurringIncomeCodes(DatabaseUser, collection);
        }
        public CodeCollection RemoveNonRecurringIncomeCodes(DatabaseUser user, CodeCollection collection)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.RemoveNonRecurringIncomeCodes(DatabaseUser, collection);
        }
        public CodeCollection GetRecurringIncomePaycodesRemovingPaycodesInUse(String paycodeTypeCode, long? employeeId)
        {
            return GetRecurringIncomePaycodesRemovingPaycodesInUse(DatabaseUser, paycodeTypeCode, employeeId);
        }
        public CodeCollection GetRecurringIncomePaycodesRemovingPaycodesInUse(DatabaseUser user, String paycodeTypeCode, long? employeeId)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetRecurringIncomePaycodesRemovingPaycodesInUse(user, paycodeTypeCode, employeeId);
        }
        public CodeCollection GetPaycodesRemovingAssociationsInUse(String paycodeAssociationTypeCode)
        {
            return GetPaycodesRemovingAssociationsInUse(DatabaseUser, paycodeAssociationTypeCode);
        }
        public CodeCollection GetPaycodesRemovingAssociationsInUse(DatabaseUser user, String paycodeAssociationTypeCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPaycodesRemovingAssociationsInUse(user, paycodeAssociationTypeCode);
        }
        public CodeCollection GetRecurringIncomePaycodesRemovingGlobalPaycodesInUse(String paycodeTypeCode)
        {
            return GetRecurringIncomePaycodesRemovingGlobalPaycodesInUse(DatabaseUser, paycodeTypeCode);
        }
        public CodeCollection GetRecurringIncomePaycodesRemovingGlobalPaycodesInUse(DatabaseUser user, String paycodeTypeCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetRecurringIncomePaycodesRemovingGlobalPaycodesInUse(user, paycodeTypeCode);
        }
        public CodeCollection GetEmployeeCustomFieldNameCodesRemovingInUse()
        {
            return GetEmployeeCustomFieldNameCodesRemovingInUse(DatabaseUser);
        }
        public CodeCollection GetEmployeeCustomFieldNameCodesRemovingInUse(DatabaseUser user)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeCustomFieldNameCodesRemovingInUse(user);
        }
        public bool IsPaycodeGarnishment(String paycode, String paycodeType)
        {
            return IsPaycodeGarnishment(DatabaseUser, paycode, paycodeType);
        }
        public bool IsPaycodeGarnishment(DatabaseUser user, String paycode, String paycodeType)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.IsPaycodeGarnishment(user, paycode, paycodeType);
        }
        #endregion

        #region code paycode
        public CodePaycodeCollection GetCodePaycode(String paycode, bool useExternalFlag)
        {
            return GetCodePaycode(DatabaseUser, paycode, useExternalFlag);
        }
        public CodePaycodeCollection GetCodePaycode(DatabaseUser user, String paycode, bool useExternalFlag)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetCodePaycode(user, paycode, useExternalFlag);
        }
        public CodePaycode UpdateCodePaycode(CodePaycode paycode, bool overrideConcurrencyCheckFlag)
        {
            return UpdateCodePaycode(DatabaseUser, paycode, overrideConcurrencyCheckFlag);
        }
        public CodePaycode UpdateCodePaycode(DatabaseUser user, CodePaycode paycode, bool overrideConcurrencyCheckFlag)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateCodePaycode(user, paycode, overrideConcurrencyCheckFlag);
        }
        public CodePaycode InsertCodePaycode(CodePaycode paycode)
        {
            return InsertCodePaycode(DatabaseUser, paycode);
        }
        public CodePaycode InsertCodePaycode(DatabaseUser user, CodePaycode paycode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertCodePaycode(user, paycode);
        }
        public void DeleteCodePaycode(CodePaycode paycode)
        {
            DeleteCodePaycode(DatabaseUser, paycode);
        }
        public void DeleteCodePaycode(DatabaseUser user, CodePaycode paycode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteCodePaycode(user, paycode);
        }
        #endregion

        #region code paycode benefit
        public CodePaycodeBenefitCollection GetCodePaycodeBenefit(String paycodeCode)
        {
            return GetCodePaycodeBenefit(DatabaseUser, paycodeCode);
        }
        public CodePaycodeBenefitCollection GetCodePaycodeBenefit(DatabaseUser user, String paycodeCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetCodePaycodeBenefit(user, paycodeCode);
        }
        public CodePaycodeBenefit UpdateCodePaycodeBenefit(CodePaycodeBenefit paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false)
        {
            return UpdateCodePaycodeBenefit(DatabaseUser, paycode, overrideConcurrencyCheckFlag, paycodeImport);
        }
        public CodePaycodeBenefit UpdateCodePaycodeBenefit(DatabaseUser user, CodePaycodeBenefit paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateCodePaycodeBenefit(user, paycode, overrideConcurrencyCheckFlag, paycodeImport);
        }
        public CodePaycodeBenefit InsertCodePaycodeBenefit(CodePaycodeBenefit paycode)
        {
            return InsertCodePaycodeBenefit(DatabaseUser, paycode);
        }
        public CodePaycodeBenefit InsertCodePaycodeBenefit(DatabaseUser user, CodePaycodeBenefit paycode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertCodePaycodeBenefit(user, paycode);
        }
        #endregion

        #region code paycode deduction
        public CodePaycodeDeductionCollection GetCodePaycodeDeduction(String paycodeCode)
        {
            return GetCodePaycodeDeduction(DatabaseUser, paycodeCode);
        }
        public CodePaycodeDeductionCollection GetCodePaycodeDeduction(DatabaseUser user, String paycodeCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetCodePaycodeDeduction(user, paycodeCode);
        }
        public CodePaycodeDeduction UpdateCodePaycodeDeduction(CodePaycodeDeduction paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false)
        {
            return UpdateCodePaycodeDeduction(DatabaseUser, paycode, overrideConcurrencyCheckFlag, paycodeImport);
        }
        public CodePaycodeDeduction UpdateCodePaycodeDeduction(DatabaseUser user, CodePaycodeDeduction paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateCodePaycodeDeduction(user, paycode, overrideConcurrencyCheckFlag, paycodeImport);
        }
        public CodePaycodeDeduction InsertCodePaycodeDeduction(CodePaycodeDeduction paycode)
        {
            return InsertCodePaycodeDeduction(DatabaseUser, paycode);
        }
        public CodePaycodeDeduction InsertCodePaycodeDeduction(DatabaseUser user, CodePaycodeDeduction paycode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertCodePaycodeDeduction(user, paycode);
        }
        #endregion

        #region code paycode income
        public CodePaycodeIncomeCollection GetCodePaycodeIncome(String paycodeCode)
        {
            return GetCodePaycodeIncome(DatabaseUser, paycodeCode);
        }
        public CodePaycodeIncomeCollection GetCodePaycodeIncome(DatabaseUser user, String paycodeCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetCodePaycodeIncome(user, paycodeCode);
        }
        public CodePaycodeIncome UpdateCodePaycodeIncome(CodePaycodeIncome paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false)
        {
            return UpdateCodePaycodeIncome(DatabaseUser, paycode, overrideConcurrencyCheckFlag, paycodeImport);
        }
        public CodePaycodeIncome UpdateCodePaycodeIncome(DatabaseUser user, CodePaycodeIncome paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateCodePaycodeIncome(user, paycode, overrideConcurrencyCheckFlag, paycodeImport);
        }
        public CodePaycodeIncome InsertCodePaycodeIncome(CodePaycodeIncome paycode)
        {
            return InsertCodePaycodeIncome(DatabaseUser, paycode);
        }
        public CodePaycodeIncome InsertCodePaycodeIncome(DatabaseUser user, CodePaycodeIncome paycode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertCodePaycodeIncome(user, paycode);
        }
        #endregion

        #region code paycode employer
        public CodePaycodeEmployerCollection GetCodePaycodeEmployer(String paycodeCode)
        {
            return GetCodePaycodeEmployer(DatabaseUser, paycodeCode);
        }
        public CodePaycodeEmployerCollection GetCodePaycodeEmployer(DatabaseUser user, String paycodeCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetCodePaycodeEmployer(user, paycodeCode);
        }
        public CodePaycodeEmployer UpdateCodePaycodeEmployer(CodePaycodeEmployer paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false)
        {
            return UpdateCodePaycodeEmployer(DatabaseUser, paycode, overrideConcurrencyCheckFlag, paycodeImport);
        }
        public CodePaycodeEmployer UpdateCodePaycodeEmployer(DatabaseUser user, CodePaycodeEmployer paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateCodePaycodeEmployer(user, paycode, overrideConcurrencyCheckFlag, paycodeImport);
        }
        public CodePaycodeEmployer InsertCodePaycodeEmployer(CodePaycodeEmployer paycode)
        {
            return InsertCodePaycodeEmployer(DatabaseUser, paycode);
        }
        public CodePaycodeEmployer InsertCodePaycodeEmployer(DatabaseUser user, CodePaycodeEmployer paycode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertCodePaycodeEmployer(user, paycode);
        }
        #endregion

        #region paycode attached paycode provision
        public PaycodeAttachedPaycodeProvisionCollection GetPaycodeAttachedPaycodeProvision(String paycodeCode)
        {
            return GetPaycodeAttachedPaycodeProvision(DatabaseUser, paycodeCode);
        }
        public PaycodeAttachedPaycodeProvisionCollection GetPaycodeAttachedPaycodeProvision(DatabaseUser user, String paycodeCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPaycodeAttachedPaycodeProvision(user, paycodeCode);
        }
        #endregion

        #region paycode export
        public byte[] ExportPaycodeFile(String paycodeCode, String paycodeType)
        {
            return ExportPaycodeFile(DatabaseUser, paycodeCode, paycodeType);
        }
        public byte[] ExportPaycodeFile(DatabaseUser user, String paycodeCode, String paycodeType)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ExportPaycodeFile(user, paycodeCode, paycodeType);
        }
        #endregion

        #region code payroll processing group
        public Decimal GetCurrentPayrollYear()
        {
            return GetCurrentPayrollYear(DatabaseUser);
        }
        public Decimal GetCurrentPayrollYear(DatabaseUser user)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetCurrentPayrollYear(user);
        }
        public PayrollProcessingGroupCollection GetPayrollProcessingGroup()
        {
            return GetPayrollProcessingGroup(DatabaseUser);
        }
        public PayrollProcessingGroupCollection GetPayrollProcessingGroup(DatabaseUser user)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPayrollProcessingGroup(user);
        }
        public PayrollProcessingGroup UpdatePayrollProcessingGroup(PayrollProcessingGroup ppG)
        {
            return UpdatePayrollProcessingGroup(DatabaseUser, ppG);
        }
        public PayrollProcessingGroup UpdatePayrollProcessingGroup(DatabaseUser user, PayrollProcessingGroup ppG)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdatePayrollProcessingGroup(user, ppG);
        }
        public void DeletePayrollProcessingGroup(PayrollProcessingGroup ppG)
        {
            DeletePayrollProcessingGroup(DatabaseUser, ppG);
        }
        public void DeletePayrollProcessingGroup(DatabaseUser user, PayrollProcessingGroup ppG)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeletePayrollProcessingGroup(user, ppG);
        }
        public PayrollProcessingGroup InsertPayrollProcessingGroupCode(PayrollProcessingGroup ppG)
        {
            return InsertPayrollProcessingGroupCode(DatabaseUser, ppG);
        }
        public PayrollProcessingGroup InsertPayrollProcessingGroupCode(DatabaseUser user, PayrollProcessingGroup ppG)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertPayrollProcessingGroupCode(user, ppG);
        }
        #endregion

        #region payroll process group override period
        public PayrollProcessGroupOverridePeriodCollection GetPayrollProcessGroupOverridePeriod(String payrollProcessGroupCode)
        {
            return GetPayrollProcessGroupOverridePeriod(DatabaseUser, payrollProcessGroupCode);
        }
        public PayrollProcessGroupOverridePeriodCollection GetPayrollProcessGroupOverridePeriod(DatabaseUser user, String payrollProcessGroupCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPayrollProcessGroupOverridePeriod(user, payrollProcessGroupCode);
        }
        public PayrollProcessGroupOverridePeriod InsertPayrollProcessGroupOverridePeriod(PayrollProcessGroupOverridePeriod overridePeriod)
        {
            return InsertPayrollProcessGroupOverridePeriod(DatabaseUser, overridePeriod);
        }
        public PayrollProcessGroupOverridePeriod InsertPayrollProcessGroupOverridePeriod(DatabaseUser user, PayrollProcessGroupOverridePeriod overridePeriod)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertPayrollProcessGroupOverridePeriod(user, overridePeriod);
        }
        public PayrollProcessGroupOverridePeriod UpdatePayrollProcessGroupOverridePeriod(PayrollProcessGroupOverridePeriod overridePeriod)
        {
            return UpdatePayrollProcessGroupOverridePeriod(DatabaseUser, overridePeriod);
        }
        public PayrollProcessGroupOverridePeriod UpdatePayrollProcessGroupOverridePeriod(DatabaseUser user, PayrollProcessGroupOverridePeriod overridePeriod)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdatePayrollProcessGroupOverridePeriod(user, overridePeriod);
        }
        public void DeletePayrollProcessGroupOverridePeriod(PayrollProcessGroupOverridePeriod overridePeriod)
        {
            DeletePayrollProcessGroupOverridePeriod(DatabaseUser, overridePeriod);
        }
        public void DeletePayrollProcessGroupOverridePeriod(DatabaseUser user, PayrollProcessGroupOverridePeriod overridePeriod)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeletePayrollProcessGroupOverridePeriod(user, overridePeriod);
        }
        #endregion

        #region code system
        public CodeSystemCollection GetCodeSystem()
        {
            return GetCodeSystem(DatabaseUser);
        }
        public CodeSystemCollection GetCodeSystem(DatabaseUser user)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetCodeSystem(user);
        }
        public CodeSystem UpdateCodeSystem(CodeSystem code)
        {
            return UpdateCodeSystem(DatabaseUser, code);
        }
        public CodeSystem UpdateCodeSystem(DatabaseUser user, CodeSystem code)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateCodeSystem(user, code);
        }
        public CodeSystem InsertCodeSystem(CodeSystem code)
        {
            return InsertCodeSystem(DatabaseUser, code);
        }
        public CodeSystem InsertCodeSystem(DatabaseUser user, CodeSystem code)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertCodeSystem(user, code);
        }
        public void DeleteCodeSystem(CodeSystem code)
        {
            DeleteCodeSystem(DatabaseUser, code);
        }
        public void DeleteCodeSystem(DatabaseUser user, CodeSystem code)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteCodeSystem(user, code);
        }
        #endregion

        #region paycode association
        public PaycodeAssociationCollection GetPaycodeAssociation(String paycodeAssociationTypeCode)
        {
            return GetPaycodeAssociation(DatabaseUser, paycodeAssociationTypeCode);
        }
        public PaycodeAssociationCollection GetPaycodeAssociation(DatabaseUser user, String paycodeAssociationTypeCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPaycodeAssociation(user, paycodeAssociationTypeCode);
        }
        public PaycodeAssociationTypeCollection GetPaycodeAssociationType(String paycodeAssociationTypeCode)
        {
            return GetPaycodeAssociationType(DatabaseUser, paycodeAssociationTypeCode);
        }
        public PaycodeAssociationTypeCollection GetPaycodeAssociationType(DatabaseUser user, String paycodeAssociationTypeCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetPaycodeAssociationType(user, paycodeAssociationTypeCode);
        }
        public PaycodeAssociationType InsertPaycodeAssociationType(PaycodeAssociationType paycode)
        {
            return InsertPaycodeAssociationType(DatabaseUser, paycode);
        }
        public PaycodeAssociationType InsertPaycodeAssociationType(DatabaseUser user, PaycodeAssociationType paycode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertPaycodeAssociationType(user, paycode);
        }
        public PaycodeAssociation InsertPaycodeAssociation(PaycodeAssociation paycode)
        {
            return InsertPaycodeAssociation(DatabaseUser, paycode);
        }
        public PaycodeAssociation InsertPaycodeAssociation(DatabaseUser user, PaycodeAssociation paycode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertPaycodeAssociation(user, paycode);
        }
        public PaycodeAssociationType UpdatePaycodeAssociationType(PaycodeAssociationType paycode)
        {
            return UpdatePaycodeAssociationType(DatabaseUser, paycode);
        }
        public PaycodeAssociationType UpdatePaycodeAssociationType(DatabaseUser user, PaycodeAssociationType paycode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdatePaycodeAssociationType(user, paycode);
        }
        public PaycodeAssociation UpdatePaycodeAssociation(PaycodeAssociation paycode)
        {
            return UpdatePaycodeAssociation(DatabaseUser, paycode);
        }
        public PaycodeAssociation UpdatePaycodeAssociation(DatabaseUser user, PaycodeAssociation paycode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdatePaycodeAssociation(user, paycode);
        }
        public void DeletePaycodeAssociationType(PaycodeAssociationType paycode)
        {
            DeletePaycodeAssociationType(DatabaseUser, paycode);
        }
        public void DeletePaycodeAssociationType(DatabaseUser user, PaycodeAssociationType paycode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeletePaycodeAssociationType(user, paycode);
        }
        public void DeletePaycodeAssociation(PaycodeAssociation paycode)
        {
            DeletePaycodeAssociation(DatabaseUser, paycode);
        }
        public void DeletePaycodeAssociation(DatabaseUser user, PaycodeAssociation paycode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeletePaycodeAssociation(user, paycode);
        }
        #endregion

        #region code third party

        public CodeThirdParty InsertThirdPartyCode(CodeThirdParty thirdParty)
        {
            return InsertThirdPartyCode(DatabaseUser, thirdParty);
        }
        public CodeThirdParty InsertThirdPartyCode(DatabaseUser user, CodeThirdParty thirdParty)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertThirdPartyCode(user, thirdParty);
        }
        public CodeThirdParty UpdateThirdPartyCode(CodeThirdParty thirdParty)
        {
            return UpdateThirdPartyCode(DatabaseUser, thirdParty);
        }
        public CodeThirdParty UpdateThirdPartyCode(DatabaseUser user, CodeThirdParty thirdParty)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateThirdPartyCode(user, thirdParty);
        }
        public CodeThirdPartyCollection GetCodeThirdParty(string thirdPartyCode)
        {
            return GetCodeThirdParty(DatabaseUser, thirdPartyCode);
        }
        public CodeThirdPartyCollection GetCodeThirdParty(DatabaseUser user, string thirdPartyCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetCodeThirdParty(user, thirdPartyCode);
        }
        public void DeleteThirdPartyCode(CodeThirdParty thirdPartyCode)
        {
            DeleteThirdPartyCode(DatabaseUser, thirdPartyCode);
        }
        public void DeleteThirdPartyCode(DatabaseUser user, CodeThirdParty thirdPartyCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteThirdPartyCode(user, thirdPartyCode);
        }

        #endregion

        #region code wsib
        public CodeWsibCollection GetCodeWsib(String wsibCode)
        {
            return GetCodeWsib(DatabaseUser, wsibCode);
        }
        public CodeWsibCollection GetCodeWsib(DatabaseUser user, String wsibCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetCodeWsib(user, wsibCode);
        }
        public CodeWsib UpdateCodeWsib(CodeWsib codeWsib)
        {
            return UpdateCodeWsib(DatabaseUser, codeWsib);
        }
        public CodeWsib UpdateCodeWsib(DatabaseUser user, CodeWsib codeWsib)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateCodeWsib(user, codeWsib);
        }
        public CodeWsib InsertCodeWsib(CodeWsib codeWsib)
        {
            return InsertCodeWsib(DatabaseUser, codeWsib);
        }
        public CodeWsib InsertCodeWsib(DatabaseUser user, CodeWsib codeWsib)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertCodeWsib(user, codeWsib);
        }
        public void DeleteCodeWsib(CodeWsib codeWsib)
        {
            DeleteCodeWsib(DatabaseUser, codeWsib);
        }
        public void DeleteCodeWsib(DatabaseUser user, CodeWsib codeWsib)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteCodeWsib(user, codeWsib);
        }
        public CodeWsibEffectiveCollection GetCodeWsibEffective(String wsibCode)
        {
            return GetCodeWsibEffective(DatabaseUser, wsibCode);
        }
        public CodeWsibEffectiveCollection GetCodeWsibEffective(DatabaseUser user, String wsibCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetCodeWsibEffective(user, wsibCode);
        }
        public CodeWsibEffective InsertCodeWsibEffective(CodeWsibEffective codeWsibEffective)
        {
            return InsertCodeWsibEffective(DatabaseUser, codeWsibEffective);
        }
        public CodeWsibEffective InsertCodeWsibEffective(DatabaseUser user, CodeWsibEffective codeWsibEffective)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertCodeWsibEffective(user, codeWsibEffective);
        }
        public CodeWsibEffective UpdateCodeWsibEffective(CodeWsibEffective codeWsibEffective)
        {
            return UpdateCodeWsibEffective(DatabaseUser, codeWsibEffective);
        }
        public CodeWsibEffective UpdateCodeWsibEffective(DatabaseUser user, CodeWsibEffective codeWsibEffective)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateCodeWsibEffective(user, codeWsibEffective);
        }
        public void DeleteCodeWsibEffective(CodeWsibEffective codeWsibEffective)
        {
            DeleteCodeWsibEffective(DatabaseUser, codeWsibEffective);
        }
        public void DeleteCodeWsibEffective(DatabaseUser user, CodeWsibEffective codeWsibEffective)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteCodeWsibEffective(user, codeWsibEffective);
        }
        #endregion

        #region code wcb cheque remittance frequency
        public CodeWcbChequeRemittanceFrequencyCollection GetCodeWcbChequeRemittanceFrequency(String wcbChequeRemittanceFrequencyCode)
        {
            return GetCodeWcbChequeRemittanceFrequency(DatabaseUser, wcbChequeRemittanceFrequencyCode);
        }
        public CodeWcbChequeRemittanceFrequencyCollection GetCodeWcbChequeRemittanceFrequency(DatabaseUser user, String wcbChequeRemittanceFrequencyCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetCodeWcbChequeRemittanceFrequency(user, wcbChequeRemittanceFrequencyCode);
        }
        public CodeWcbChequeRemittanceFrequency InsertCodeWcbChequeRemittanceFrequency(CodeWcbChequeRemittanceFrequency codeWcbChequeRemittanceFrequency)
        {
            return InsertCodeWcbChequeRemittanceFrequency(DatabaseUser, codeWcbChequeRemittanceFrequency);
        }
        public CodeWcbChequeRemittanceFrequency InsertCodeWcbChequeRemittanceFrequency(DatabaseUser user, CodeWcbChequeRemittanceFrequency codeWcbChequeRemittanceFrequency)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertCodeWcbChequeRemittanceFrequency(user, codeWcbChequeRemittanceFrequency);
        }
        public CodeWcbChequeRemittanceFrequency UpdateCodeWcbChequeRemittanceFrequency(CodeWcbChequeRemittanceFrequency codeWcbChequeRemittanceFrequency)
        {
            return UpdateCodeWcbChequeRemittanceFrequency(DatabaseUser, codeWcbChequeRemittanceFrequency);
        }
        public CodeWcbChequeRemittanceFrequency UpdateCodeWcbChequeRemittanceFrequency(DatabaseUser user, CodeWcbChequeRemittanceFrequency codeWcbChequeRemittanceFrequency)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateCodeWcbChequeRemittanceFrequency(user, codeWcbChequeRemittanceFrequency);
        }
        public void DeleteCodeWcbChequeRemittanceFrequency(CodeWcbChequeRemittanceFrequency codeWcbChequeRemittanceFrequency)
        {
            DeleteCodeWcbChequeRemittanceFrequency(DatabaseUser, codeWcbChequeRemittanceFrequency);
        }
        public void DeleteCodeWcbChequeRemittanceFrequency(DatabaseUser user, CodeWcbChequeRemittanceFrequency codeWcbChequeRemittanceFrequency)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteCodeWcbChequeRemittanceFrequency(user, codeWcbChequeRemittanceFrequency);
        }
        public WcbChequeRemittanceFrequencyDetailCollection GetWcbChequeRemittanceFrequencyDetail(String wcbChequeRemittanceFrequencyCode)
        {
            return GetWcbChequeRemittanceFrequencyDetail(DatabaseUser, wcbChequeRemittanceFrequencyCode);
        }
        public WcbChequeRemittanceFrequencyDetailCollection GetWcbChequeRemittanceFrequencyDetail(DatabaseUser user, String wcbChequeRemittanceFrequencyCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetWcbChequeRemittanceFrequencyDetail(user, wcbChequeRemittanceFrequencyCode);
        }
        public WcbChequeRemittanceFrequencyDetail InsertWcbChequeRemittanceFrequencyDetail(WcbChequeRemittanceFrequencyDetail wcbChequeRemittanceFrequencyDetail)
        {
            return InsertWcbChequeRemittanceFrequencyDetail(DatabaseUser, wcbChequeRemittanceFrequencyDetail);
        }
        public WcbChequeRemittanceFrequencyDetail InsertWcbChequeRemittanceFrequencyDetail(DatabaseUser user, WcbChequeRemittanceFrequencyDetail wcbChequeRemittanceFrequencyDetail)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertWcbChequeRemittanceFrequencyDetail(user, wcbChequeRemittanceFrequencyDetail);
        }
        public WcbChequeRemittanceFrequencyDetail UpdateWcbChequeRemittanceFrequencyDetail(WcbChequeRemittanceFrequencyDetail wcbChequeRemittanceFrequencyDetail)
        {
            return UpdateWcbChequeRemittanceFrequencyDetail(DatabaseUser, wcbChequeRemittanceFrequencyDetail);
        }
        public WcbChequeRemittanceFrequencyDetail UpdateWcbChequeRemittanceFrequencyDetail(DatabaseUser user, WcbChequeRemittanceFrequencyDetail wcbChequeRemittanceFrequencyDetail)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateWcbChequeRemittanceFrequencyDetail(user, wcbChequeRemittanceFrequencyDetail);
        }
        public void DeleteWcbChequeRemittanceFrequencyDetail(WcbChequeRemittanceFrequencyDetail wcbChequeRemittanceFrequencyDetail)
        {
            DeleteWcbChequeRemittanceFrequencyDetail(DatabaseUser, wcbChequeRemittanceFrequencyDetail);
        }
        public void DeleteWcbChequeRemittanceFrequencyDetail(DatabaseUser user, WcbChequeRemittanceFrequencyDetail wcbChequeRemittanceFrequencyDetail)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteWcbChequeRemittanceFrequencyDetail(user, wcbChequeRemittanceFrequencyDetail);
        }
        #endregion

        #region security
        public CodeCollection GetRoleDescription(String securityRoleType, bool useSystemRoleId, bool? worklinksAdministrationFlag)
        {
            return GetRoleDescription(DatabaseUser, securityRoleType, useSystemRoleId, worklinksAdministrationFlag);
        }
        public CodeCollection GetRoleDescription(DatabaseUser user, String securityRoleType, bool useSystemRoleId, bool? worklinksAdministrationFlag)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetRoleDescription(user, securityRoleType, useSystemRoleId, worklinksAdministrationFlag);
        }
        public CodeCollection GetRoleDescriptionCombo(String securityRoleType, String selectedValue, bool useSystemRoleId, bool? worklinksAdministrationFlag)
        {
            return GetRoleDescriptionCombo(DatabaseUser, securityRoleType, selectedValue, useSystemRoleId, worklinksAdministrationFlag);
        }
        public CodeCollection GetRoleDescriptionCombo(DatabaseUser user, String securityRoleType, String selectedValue, bool useSystemRoleId, bool? worklinksAdministrationFlag)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetRoleDescriptionCombo(user, securityRoleType, selectedValue, useSystemRoleId, worklinksAdministrationFlag);
        }
        #endregion

        public CodeCollection GetBusinessNumber(long? id)
        {
            return GetBusinessNumber(DatabaseUser, id);
        }
        public CodeCollection GetBusinessNumber(DatabaseUser user, long? id)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetBusinessNumber(user, id);
        }
        public CodeCollection GetDisciplineTypeCode(string disciplineCode)
        {
            return GetDisciplineTypeCode(DatabaseUser, disciplineCode);
        }
        public CodeCollection GetDisciplineTypeCode(DatabaseUser user, string disciplineCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetDisciplineTypeCode(user, disciplineCode);
        }
        public CodeCollection GetProvinceStateCode(String country)
        {
            return GetProvinceStateCode(DatabaseUser, country);
        }
        public CodeCollection GetProvinceStateCode(DatabaseUser user, String country)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetProvinceStateCode(user, country);
        }
        public CodeCollection GetDayCode(String monthCode)
        {
            return GetDayCode(DatabaseUser, monthCode);
        }
        public CodeCollection GetDayCode(DatabaseUser user, String monthCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetDayCode(user, monthCode);
        }
        public CodeCollection GetYearEndFormBoxCode(String yearEndFormCode)
        {
            return GetYearEndFormBoxCode(DatabaseUser, yearEndFormCode);
        }
        public CodeCollection GetYearEndFormBoxCode(DatabaseUser user, String yearEndFormCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetYearEndFormBoxCode(user, yearEndFormCode);
        }
        public CodeCollection GetUnionCollectiveAgreementCode(String unionCode)
        {
            return GetUnionCollectiveAgreementCode(DatabaseUser, unionCode);
        }
        public CodeCollection GetUnionCollectiveAgreementCode(DatabaseUser user, String unionCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetUnionCollectiveAgreementCode(user, unionCode);
        }
        public CodeCollection GetEmployeeBankCode(String bankingCountryCode)
        {
            return GetEmployeeBankCode(DatabaseUser, bankingCountryCode);
        }
        public CodeCollection GetEmployeeBankCode(DatabaseUser user, String bankingCountryCode)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetEmployeeBankCode(user, bankingCountryCode);
        }

        #region revenue quebec business 
        public RevenueQuebecBusiness GetRevenueQuebecBusiness()
        {
            return GetRevenueQuebecBusiness(DatabaseUser);
        }
        public RevenueQuebecBusiness GetRevenueQuebecBusiness(DatabaseUser user)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetRevenueQuebecBusiness(user);
        }
        public bool ValidateModulus11CheckDigit(string businessId)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ValidateModulus11CheckDigit(businessId);
        }
        public RevenueQuebecBusiness UpdateRevenueQuebecBusiness(RevenueQuebecBusiness revenueQuebecBusiness)
        {
            return UpdateRevenueQuebecBusiness(DatabaseUser, revenueQuebecBusiness);
        }
        public RevenueQuebecBusiness UpdateRevenueQuebecBusiness(DatabaseUser user, RevenueQuebecBusiness revenueQuebecBusiness)
        {
            using (CodeChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateRevenueQuebecBusiness(user, revenueQuebecBusiness);
        }
        #endregion

        #endregion
    }

    public class CodeChannelWrapper : IDisposable
    {
        ICode _channel = null;

        public ICode Channel { get { return _channel; } }

        public CodeChannelWrapper(ICode channel)
        {
            _channel = channel;
        }
        public void Dispose()
        {
            try
            {
                ((IClientChannel)_channel).Close();
            }
            catch (FaultException)
            {
                ((IClientChannel)_channel).Abort();
            }
            catch (CommunicationException)
            {
                ((IClientChannel)_channel).Abort();
            }
        }
    }
}