﻿using System;
using System.ServiceModel;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Web.BusinessService.Contract;

namespace WorkLinks.WebClient
{
    public class XmlClient : BaseClient, IXml
    {
        #region fields
        private ChannelFactory<IXml> _factory = null;
        private String _wcfServiceUrl = null;
        private long _maxReceivedMessageSize;
        private long _receiveTimeoutTicks;
        private long _sendTimeoutTicks;
        #endregion

        #region properties
        private ChannelFactory<IXml> Factory
        {
            get
            {
                if (_factory == null)
                {
                    BasicHttpBinding binding = new BasicHttpBinding();
                    binding.MaxReceivedMessageSize = _maxReceivedMessageSize;
                    binding.ReceiveTimeout = new TimeSpan(_receiveTimeoutTicks);
                    binding.SendTimeout = new TimeSpan(_sendTimeoutTicks);

                    _factory = new ChannelFactory<IXml>(binding, new EndpointAddress(_wcfServiceUrl));
                }

                return _factory;
            }
        }
        #endregion

        public XmlChannelWrapper CreateChannel()
        {
            return new XmlChannelWrapper(Factory.CreateChannel());
        }

        #region constructors/destructors
        public XmlClient(String wcfServiceUrl, String wcfServiceName, long maxReceivedMessageSize, long receiveTimeoutTicks, long sendTimeoutTicks)
        {
            _wcfServiceUrl = new Uri(new Uri(wcfServiceUrl), wcfServiceName).ToString();
            _maxReceivedMessageSize = maxReceivedMessageSize;
            _receiveTimeoutTicks = receiveTimeoutTicks;
            _sendTimeoutTicks = sendTimeoutTicks;
        }
        #endregion

        #region wcf functions

        public void ImportBatchesViaFtp(string importName, string importExportProcessingDirectory, bool importDirtySave, bool autoAddSecondaryPositions, string realFtpName, string filePatternToMatch, string payrollProcessRunTypeCode, string payrollProcessGroupCode)
        {
            ImportBatchesViaFtp(DatabaseUser, importName, importExportProcessingDirectory, importDirtySave, autoAddSecondaryPositions, realFtpName, filePatternToMatch, payrollProcessRunTypeCode, payrollProcessGroupCode);
        }
        public void ImportBatchesViaFtp(DatabaseUser user, string importName, string importExportProcessingDirectory, bool importDirtySave, bool autoAddSecondaryPositions, string realFtpName, string filePatternToMatch, string payrollProcessRunTypeCode, string payrollProcessGroupCode)
        {
            using (XmlChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.ImportBatchesViaFtp(user, importName, importExportProcessingDirectory, importDirtySave, autoAddSecondaryPositions, realFtpName, filePatternToMatch, payrollProcessRunTypeCode, payrollProcessGroupCode);
        }
        public String ImportFile(long importExportId, byte[] data, String importFileName, String importExportProcessingDirectory, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions)
        {
            return ImportFile(DatabaseUser, importExportId, data, importFileName, importExportProcessingDirectory, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);
        }
        public String ImportFile(DatabaseUser user, long importExportId, byte[] data, String importFileName, String importExportProcessingDirectory, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions)
        {
            using (XmlChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ImportFile(user, importExportId, data, importFileName, importExportProcessingDirectory, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);
        }
        public byte[] ExportRoe(long[] employeePositionIds, String importExportProcessingDirectory, String roeIssueCode, out bool missingRoeReason)
        {
            return ExportRoe(DatabaseUser, employeePositionIds, importExportProcessingDirectory, roeIssueCode, out missingRoeReason);
        }
        public byte[] ExportRoe(DatabaseUser user, long[] employeePositionIds, String importExportProcessingDirectory, String roeIssueCode, out bool missingRoeReason)
        {
            using (XmlChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ExportRoe(user, employeePositionIds, importExportProcessingDirectory, roeIssueCode, out missingRoeReason);
        }
        public CanadaRevenueAgencyT4ExportCollection ExportT4Details(String year, String exportType)
        {
            return ExportT4Details(DatabaseUser, year, exportType);
        }
        public CanadaRevenueAgencyT4ExportCollection ExportT4Details(DatabaseUser user, String year, String exportType)
        {
            using (XmlChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ExportT4Details(user, year, exportType);
        }
        public CanadaRevenueAgencyT4Export UpdateExportT4Details(CanadaRevenueAgencyT4Export export)
        {
            return UpdateExportT4Details(DatabaseUser, export);
        }
        public CanadaRevenueAgencyT4Export UpdateExportT4Details(DatabaseUser user, CanadaRevenueAgencyT4Export export)
        {
            using (XmlChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateExportT4Details(user, export);
        }
        public CanadaRevenueAgencyT4Export InsertExportT4Details(CanadaRevenueAgencyT4Export export, String year)
        {
            return InsertExportT4Details(DatabaseUser, export, year);
        }
        public CanadaRevenueAgencyT4Export InsertExportT4Details(DatabaseUser user, CanadaRevenueAgencyT4Export export, String year)
        {
            using (XmlChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertExportT4Details(user, export, year);
        }

        #region T4/T4A/R1 export
        public byte[] ExportT4CIC(int year, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2)
        {
            return ExportT4CIC(DatabaseUser, year, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2);
        }
        public byte[] ExportT4CIC(DatabaseUser user, int year, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2)
        {
            using (XmlChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ExportT4CIC(user, year, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2);
        }
        public byte[] ExportT4aCIC(int year, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2)
        {
            return ExportT4aCIC(DatabaseUser, year, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2);
        }
        public byte[] ExportT4aCIC(DatabaseUser user, int year, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2)
        {
            using (XmlChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ExportT4aCIC(user, year, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2);
        }
        public byte[] ExportR1CIC(int year, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string revenueQuebecTaxId)
        {
            return ExportR1CIC(DatabaseUser, year, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, revenueQuebecTaxId);
        }
        public byte[] ExportR1CIC(DatabaseUser user, int year, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string revenueQuebecTaxId)
        {
            using (XmlChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ExportR1CIC(user, year, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, revenueQuebecTaxId);
        }
        #endregion

        public byte[] ExportT4XmlFile(int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string exportType)
        {
            return ExportT4XmlFile(DatabaseUser, year, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, exportType);
        }
        public byte[] ExportT4XmlFile(DatabaseUser user, int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, String exportType)
        {
            using (XmlChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ExportT4XmlFile(user, year, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, exportType);
        }
        public byte[] ExportT4aXmlFile(int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string exportType)
        {
            return ExportT4aXmlFile(DatabaseUser, year, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, exportType);
        }
        public byte[] ExportT4aXmlFile(DatabaseUser user, int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string exportType)
        {
            using (XmlChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ExportT4aXmlFile(user, year, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, exportType);
        }
        public byte[] ExportNR4XmlFile(int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string exportType)
        {
            return ExportNR4XmlFile(DatabaseUser, year, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, exportType);
        }
        public byte[] ExportNR4XmlFile(DatabaseUser user, int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string exportType)
        {
            using (XmlChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ExportNR4XmlFile(user, year, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, exportType);
        }
        public byte[] ExportR1XmlFile(int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string revenueQuebecTaxId)
        {
            return ExportR1XmlFile(DatabaseUser, year, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, revenueQuebecTaxId);
        }
        public byte[] ExportR1XmlFile(DatabaseUser user, int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string revenueQuebecTaxId)
        {
            using (XmlChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ExportR1XmlFile(user, year, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, revenueQuebecTaxId);
        }
        public byte[] ExportR2XmlFile(int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string revenueQuebecTaxId)
        {
            return ExportR2XmlFile(DatabaseUser, year, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, revenueQuebecTaxId);
        }
        public byte[] ExportR2XmlFile(DatabaseUser user, int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string revenueQuebecTaxId)
        {
            using (XmlChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ExportR2XmlFile(user, year, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, revenueQuebecTaxId);
        }
        public RevenueQuebecR1ExportCollection ExportR1Details(String year, String exportType)
        {
            return ExportR1Details(DatabaseUser, year, exportType);
        }
        public RevenueQuebecR1ExportCollection ExportR1Details(DatabaseUser user, String year, String exportType)
        {
            using (XmlChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ExportR1Details(user, year, exportType);
        }
        public RevenueQuebecR1Export UpdateExportR1Details(RevenueQuebecR1Export export)
        {
            return UpdateExportR1Details(DatabaseUser, export);
        }
        public RevenueQuebecR1Export UpdateExportR1Details(DatabaseUser user, RevenueQuebecR1Export export)
        {
            using (XmlChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateExportR1Details(user, export);
        }
        public RevenueQuebecR1Export InsertExportR1Details(RevenueQuebecR1Export export)
        {
            return InsertExportR1Details(DatabaseUser, export);
        }
        public RevenueQuebecR1Export InsertExportR1Details(DatabaseUser user, RevenueQuebecR1Export export)
        {
            using (XmlChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertExportR1Details(user, export);
        }
        #endregion
    }

    public class XmlChannelWrapper : IDisposable
    {
        IXml _channel = null;
        public IXml Channel { get { return _channel; } }

        public XmlChannelWrapper(IXml channel)
        {
            _channel = channel;
        }
        public void Dispose()
        {
            try
            {
                ((IClientChannel)_channel).Close();
            }
            catch (FaultException)
            {
                ((IClientChannel)_channel).Abort();
            }
            catch (CommunicationException)
            {
                ((IClientChannel)_channel).Abort();
            }
        }
    }
}