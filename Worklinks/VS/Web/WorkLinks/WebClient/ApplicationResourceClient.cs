﻿using System;
using System.ServiceModel;
using WorkLinks.Web.BusinessService.Contract;

namespace WorkLinks.WebClient
{
    public class ApplicationResourceClient : IApplicationResource
    {
        #region fields
        private ChannelFactory<IApplicationResource> _factory = null;
        private String _wcfServiceUrl = null;
        private long _maxReceivedMessageSize;
        private long _receiveTimeoutTicks;
        private long _sendTimeoutTicks;
        #endregion

        #region properties
        private ChannelFactory<IApplicationResource> Factory
        {
            get
            {
                if (_factory == null)
                {
                    BasicHttpBinding binding = new BasicHttpBinding();
                    binding.MaxReceivedMessageSize = _maxReceivedMessageSize;
                    binding.ReceiveTimeout = new TimeSpan(_receiveTimeoutTicks);
                    binding.SendTimeout = new TimeSpan(_sendTimeoutTicks);

                    _factory = new ChannelFactory<IApplicationResource>(binding, new EndpointAddress(_wcfServiceUrl));
                }

                return _factory;
            }
        }
        #endregion

        public ApplicationResourceChannelWrapper CreateChannel()
        {
            return new ApplicationResourceChannelWrapper(Factory.CreateChannel());
        }

        #region constructors/destructors
        public ApplicationResourceClient(String wcfServiceUrl, String wcfServiceName, long maxReceivedMessageSize, long receiveTimeoutTicks, long sendTimeoutTicks)
        {
            _wcfServiceUrl = new Uri(new Uri(wcfServiceUrl), wcfServiceName).ToString();
            _maxReceivedMessageSize = maxReceivedMessageSize;
            _receiveTimeoutTicks = receiveTimeoutTicks;
            _sendTimeoutTicks = sendTimeoutTicks;
        }
        #endregion

        #region wcf functions
        public string GetResourceByLanguageAndKey(String databaseName, long securityRoleId, string languageCode, string virtualPath, string resourceKey)
        {
            using (ApplicationResourceChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetResourceByLanguageAndKey(databaseName, securityRoleId, languageCode, virtualPath, resourceKey);
        }
        #endregion
    }

    public class ApplicationResourceChannelWrapper : IDisposable
    {
        IApplicationResource _channel = null;

        public IApplicationResource Channel { get { return _channel; } }

        public ApplicationResourceChannelWrapper(IApplicationResource channel)
        {
            _channel = channel;
        }

        public void Dispose()
        {
            try
            {
                ((IClientChannel)_channel).Close();
            }
            catch (FaultException)
            {
                ((IClientChannel)_channel).Abort();
            }
            catch (CommunicationException)
            {
                ((IClientChannel)_channel).Abort();
            }
        }
    }
}