﻿using System;
using System.Web;
using System.Web.Security;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Common.Security;

namespace WorkLinks.WebClient
{
    public class BaseClient
    {
        private const String _profileKey = "Profile";

        protected static SecurityUserProfile Profile
        {
            get
            {
                if (HttpContext.Current.Profile != null)
                    return (SecurityUserProfile)HttpContext.Current.Profile[_profileKey];
                else
                    return null;
            }
        }

        protected DatabaseUser DatabaseUser
        {
            get
            {
                WorkLinksMembershipUser user = (WorkLinksMembershipUser)Profile.User;

                DatabaseUser databaseUser = new DatabaseUser();

                databaseUser.DatabaseName = Common.ApplicationParameter.DatabaseName;
                databaseUser.LanguageCode = Profile.LanguageCode;
                databaseUser.SecurityRoleId = (long)user.GetPrimarySecurityGroup().DatabaseSecurityRoleId;

                if (databaseUser.SecurityRoleId > 0)
                    databaseUser.SecurityUserId = user.SecurityUserId;
                else
                    databaseUser.SecurityUserId = -1;

                databaseUser.UserName = user.UserName;

                return databaseUser;
            }
        }
    }
}