﻿using System;
using System.ServiceModel;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Web.BusinessService.Contract;

namespace WorkLinks.WebClient
{
    public class ReportClient : BaseClient, IReport
    {
        #region fields
        private ChannelFactory<IReport> _factory = null;
        private String _wcfServiceUrl = null;
        private long _maxReceivedMessageSize;
        private long _receiveTimeoutTicks;
        private long _sendTimeoutTicks;
        #endregion

        #region properties
        private ChannelFactory<IReport> Factory
        {
            get
            {
                if (_factory == null)
                {
                    BasicHttpBinding binding = new BasicHttpBinding();
                    binding.MaxReceivedMessageSize = _maxReceivedMessageSize;
                    binding.ReceiveTimeout = new TimeSpan(_receiveTimeoutTicks);
                    binding.SendTimeout = new TimeSpan(_sendTimeoutTicks);

                    _factory = new ChannelFactory<IReport>(binding, new EndpointAddress(_wcfServiceUrl));
                }

                return _factory;
            }
        }
        #endregion

        public ReportChannelWrapper CreateChannel()
        {
            return new ReportChannelWrapper(Factory.CreateChannel());
        }

        #region constructors/destructors
        public ReportClient(String wcfServiceUrl, String wcfServiceName, long maxReceivedMessageSize, long receiveTimeoutTicks, long sendTimeoutTicks)
        {
            _wcfServiceUrl = new Uri(new Uri(wcfServiceUrl), wcfServiceName).ToString();
            _maxReceivedMessageSize = maxReceivedMessageSize;
            _receiveTimeoutTicks = receiveTimeoutTicks;
            _sendTimeoutTicks = sendTimeoutTicks;
        }
        #endregion

        #region wcf functions
        //regular reports
        public byte[] GetReport(String reportName, long payrollProcessId, String employeeNumber, String reportType, String year, String employerNumber, String provinceCode, String reportShowDescriptionField, String revisionNumber, String reportPayRegisterSortOrder, long employeeId)
        {
            return GetReport(DatabaseUser, reportName, payrollProcessId, employeeNumber, reportType, year, employerNumber, provinceCode, reportShowDescriptionField, revisionNumber, reportPayRegisterSortOrder, employeeId);
        }
        public byte[] GetReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String reportName, long payrollProcessId, String employeeNumber, String reportType, String year, String employerNumber, String provinceCode, String reportShowDescriptionField, String revisionNumber, String reportPayRegisterSortOrder, long employeeId)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetReport(user, reportName, payrollProcessId, employeeNumber, reportType, year, employerNumber, provinceCode, reportShowDescriptionField, revisionNumber, reportPayRegisterSortOrder, employeeId);
        }
        //remittance reports (from remittance screen)
        public byte[] GetRemittanceReport(string reportName, string detailType, string remitCode, string exportId, string reportType, string remitDate)
        {
            return GetRemittanceReport(DatabaseUser, reportName, detailType, remitCode, exportId, reportType, remitDate);
        }
        public byte[] GetRemittanceReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, string reportName, string detailType, string remitCode, string exportId, string reportType, string remitDate)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetRemittanceReport(user, reportName, detailType, remitCode, exportId, reportType, remitDate);
        }
        public byte[] GetYearEndReport(YearEndReportsCriteria criteria)
        {
            return GetYearEndReport(DatabaseUser, criteria);
        }
        public byte[] GetYearEndReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, YearEndReportsCriteria criteria)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetYearEndReport(user, criteria);
        }
        //reports from HR->Reports menu
        public byte[] GetHRMenuReport(HrReportsCriteria criteria)
        {
            return GetHRMenuReport(DatabaseUser, criteria);
        }
        public byte[] GetHRMenuReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, HrReportsCriteria criteria)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetHRMenuReport(user, criteria);
        }
        public byte[] GetReportForAllEmployeesOfThisPayProcess(string massPayslipInternalFileFormat, long payrollProcessId, string reportShowDescriptionField, string reportPayRegisterSortOrder, int maxDegreeOfParallelism)
        {
            return GetReportForAllEmployeesOfThisPayProcess(DatabaseUser, massPayslipInternalFileFormat, payrollProcessId, reportShowDescriptionField, reportPayRegisterSortOrder, maxDegreeOfParallelism);
        }
        public byte[] GetReportForAllEmployeesOfThisPayProcess(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, string massPayslipInternalFileFormat, long payrollProcessId, string reportShowDescriptionField, string reportPayRegisterSortOrder, int maxDegreeOfParallelism)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetReportForAllEmployeesOfThisPayProcess(user, massPayslipInternalFileFormat, payrollProcessId, reportShowDescriptionField, reportPayRegisterSortOrder, maxDegreeOfParallelism);
        }
        public byte[] CreateGLExportFile(long payrollProcessId, string glType, string reportShowDescriptionField, string reportPayRegisterSortOrder)
        {
            return CreateGLExportFile(DatabaseUser, payrollProcessId, glType, reportShowDescriptionField, reportPayRegisterSortOrder);
        }
        public byte[] CreateGLExportFile(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, string glType, string reportShowDescriptionField, string reportPayRegisterSortOrder)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreateGLExportFile(user, payrollProcessId, glType, reportShowDescriptionField, reportPayRegisterSortOrder);
        }
        public byte[] CreateMercerPensionExportFile(long payrollProcessId, string reportShowDescriptionField, string reportPayRegisterSortOrder)
        {
            return CreateMercerPensionExportFile(DatabaseUser, payrollProcessId, reportShowDescriptionField, reportPayRegisterSortOrder);
        }
        public byte[] CreateMercerPensionExportFile(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, string reportShowDescriptionField, string reportPayRegisterSortOrder)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreateMercerPensionExportFile(user, payrollProcessId, reportShowDescriptionField, reportPayRegisterSortOrder);
        }
        public byte[] CreateStockExportFile(long payrollProcessId)
        {
            return CreateStockExportFile(DatabaseUser, payrollProcessId);
        }
        public byte[] CreateStockExportFile(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreateStockExportFile(user, payrollProcessId);
        }
        public byte[] ExportCeridianPayProcess(long payrollProcessId, long onEFTAmount, String companyNumber, string companyName, BusinessLayer.BusinessObjects.Export.CeridianBilling.ProcessType ceridianBillingProcessType, DateTime wcbStartDate)
        {
            return ExportCeridianPayProcess(DatabaseUser, payrollProcessId, onEFTAmount, companyNumber, companyName, ceridianBillingProcessType, wcbStartDate);
        }
        public byte[] ExportCeridianPayProcess(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, long onEFTAmount, String companyNumber, string companyName, BusinessLayer.BusinessObjects.Export.CeridianBilling.ProcessType ceridianBillingProcessType, DateTime wcbStartDate)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.ExportCeridianPayProcess(user, payrollProcessId, onEFTAmount, companyNumber, companyName, ceridianBillingProcessType, wcbStartDate);
        }
        public byte[] GetReportForAllEmployeesOfThisYear(string massPayslipInternalFileFormat, long year, string reportType, string reportShowDescriptionField, string reportPayRegisterSortOrder)
        {
            return GetReportForAllEmployeesOfThisYear(DatabaseUser, massPayslipInternalFileFormat, year, reportType, reportShowDescriptionField, reportPayRegisterSortOrder);
        }
        public byte[] GetReportForAllEmployeesOfThisYear(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, string massPayslipInternalFileFormat, long year, string reportType, string reportShowDescriptionField, string reportPayRegisterSortOrder)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetReportForAllEmployeesOfThisYear(user, massPayslipInternalFileFormat, year, reportType, reportShowDescriptionField, reportPayRegisterSortOrder);
        }
        public byte[] GetMassFileForAllReportsForYear(String[] massPayslipInternalFileFormats, long year, String[] reportTypes, String reportShowDescriptionField, String reportPayRegisterSortOrder, String revision)
        {
            return GetMassFileForAllReportsForYear(DatabaseUser, massPayslipInternalFileFormats, year, reportTypes, reportShowDescriptionField, reportPayRegisterSortOrder, revision);
        }
        public byte[] GetMassFileForAllReportsForYear(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String[] massPayslipInternalFileFormats, long year, String[] reportTypes, String reportShowDescriptionField, String reportPayRegisterSortOrder, String revision)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetMassFileForAllReportsForYear(user, massPayslipInternalFileFormats, year, reportTypes, reportShowDescriptionField, reportPayRegisterSortOrder, revision);
        }

        //#region report testing
        //public BusinessLayer.BusinessObjects.Test.TestReportCollection GetTestReports(WLP.BusinessLayer.BusinessObjects.DatabaseUser user)
        //{
        //    using (ReportChannelWrapper wrapper = CreateChannel())
        //        return wrapper.Channel.GetTestReports(user);
        //}
        //public BusinessLayer.BusinessObjects.Test.TestReportParameterCollection GetTestReportParameters(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long testReportId)
        //{
        //    using (ReportChannelWrapper wrapper = CreateChannel())
        //        return wrapper.Channel.GetTestReportParameters(user, testReportId);
        //}
        //public BusinessLayer.BusinessObjects.Test.TestReportExpectedValueCollection GetTestReportExpectedValues(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long testReportId)
        //{
        //    using (ReportChannelWrapper wrapper = CreateChannel())
        //        return wrapper.Channel.GetTestReportExpectedValues(user, testReportId);
        //}
        //#endregion

        #region CSB Export
        public CanadaRevenueAgencyBondExportCollection GetCanadaRevenueAgencyBondExport(long payrollProcessId)
        {
            return GetCanadaRevenueAgencyBondExport(DatabaseUser, payrollProcessId);
        }
        public CanadaRevenueAgencyBondExportCollection GetCanadaRevenueAgencyBondExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetCanadaRevenueAgencyBondExport(user, payrollProcessId);
        }
        public CanadaRevenueAgencyBondExport InsertCanadaRevenueAgencySavingsBondExportData(CanadaRevenueAgencyBondExport export)
        {
            return InsertCanadaRevenueAgencySavingsBondExportData(DatabaseUser, export);
        }
        public CanadaRevenueAgencyBondExport InsertCanadaRevenueAgencySavingsBondExportData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CanadaRevenueAgencyBondExport export)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertCanadaRevenueAgencySavingsBondExportData(user, export);
        }
        public CanadaRevenueAgencyBondExport UpdateCanadaRevenueAgencySavingsBondExportData(CanadaRevenueAgencyBondExport export)
        {
            return UpdateCanadaRevenueAgencySavingsBondExportData(DatabaseUser, export);
        }
        public CanadaRevenueAgencyBondExport UpdateCanadaRevenueAgencySavingsBondExportData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CanadaRevenueAgencyBondExport export)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateCanadaRevenueAgencySavingsBondExportData(user, export);
        }
        public byte[] CreateCSBExportFile(long payrollProcessId)
        {
            return CreateCSBExportFile(DatabaseUser, payrollProcessId);
        }
        public byte[] CreateCSBExportFile(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.CreateCSBExportFile(user, payrollProcessId);
        }
        #endregion

        public ReportCollection GetReportRecord(String reportName)
        {
            return GetReportRecord(DatabaseUser, reportName);
        }
        public ReportCollection GetReportRecord(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String reportName)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetReportRecord(user, reportName);
        }

        #region report export
        public ReportExportCollection GetReportExport(long? exportId)
        {
            return GetReportExport(DatabaseUser, exportId);
        }
        public ReportExportCollection GetReportExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? exportId)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.GetReportExport(user, exportId);
        }
        public ReportExport UpdateReportExport(ReportExport export)
        {
            return UpdateReportExport(DatabaseUser, export);
        }
        public ReportExport UpdateReportExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ReportExport export)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.UpdateReportExport(user, export);
        }
        public ReportExport InsertReportExport(ReportExport export)
        {
            return InsertReportExport(DatabaseUser, export);
        }
        public ReportExport InsertReportExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ReportExport export)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                return wrapper.Channel.InsertReportExport(user, export);
        }
        public void DeleteReportExport(ReportExport export)
        {
            DeleteReportExport(DatabaseUser, export);
        }
        public void DeleteReportExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ReportExport export)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.DeleteReportExport(user, export);
        }
        #endregion

        #region sftp report and export section
        public void CreateReportsAndExports(long payrollProcessId, String periodYear, PayRegisterExportCollection collection, string pexFtp, String reportShowDescriptionField, String reportPayRegisterSortOrder, String revenueQuebecTaxId, bool useQuebecTaxId, String citiEftFileName, String citiChequeFileName, String eftClientNumber, String citiChequeAccountNumber, String companyName, String companyShortName, String massPayslipInternalFileFormat, string originatorId, string eftType, int maxDegreeOfParallelism)
        {
            CreateReportsAndExports(DatabaseUser, payrollProcessId, periodYear, collection, pexFtp, reportShowDescriptionField, reportPayRegisterSortOrder, revenueQuebecTaxId, useQuebecTaxId, citiEftFileName, citiChequeFileName, eftClientNumber, citiChequeAccountNumber, companyName, companyShortName, massPayslipInternalFileFormat, originatorId, eftType, maxDegreeOfParallelism);
        }
        public void CreateReportsAndExports(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, String periodYear, PayRegisterExportCollection collection, string pexFtp, String reportShowDescriptionField, String reportPayRegisterSortOrder, String revenueQuebecTaxId, bool useQuebecTaxId, String citiEftFileName, String citiChequeFileName, String eftClientNumber, String citiChequeAccountNumber, String companyName, String companyShortName, String massPayslipInternalFileFormat, string originatorId, string eftType, int maxDegreeOfParallelism)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.CreateReportsAndExports(user, payrollProcessId, periodYear, collection, pexFtp, reportShowDescriptionField, reportPayRegisterSortOrder, revenueQuebecTaxId, useQuebecTaxId, citiEftFileName, citiChequeFileName, eftClientNumber, citiChequeAccountNumber, companyName, companyShortName, massPayslipInternalFileFormat, originatorId, eftType, maxDegreeOfParallelism);
        }
        #endregion

        #region rbc edi eft export section
        public void FtpRbcEdiExport(long payrollProcessId, RbcEftEdiParameters ediParms, RbcEftSourceDeductionParameters garnishmentParms, string rbcFtpName)
        {
            FtpRbcEdiExport(DatabaseUser, payrollProcessId, ediParms, garnishmentParms, rbcFtpName);
        }
        public void FtpRbcEdiExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, RbcEftEdiParameters ediParms, RbcEftSourceDeductionParameters garnishmentParms, string rbcFtpName)
        {
            using (ReportChannelWrapper wrapper = CreateChannel())
                wrapper.Channel.FtpRbcEdiExport(user, payrollProcessId, ediParms, garnishmentParms, rbcFtpName);
        }
        #endregion

        #endregion //end main region
    }

    public class ReportChannelWrapper : IDisposable
    {
        IReport _channel = null;

        public IReport Channel { get { return _channel; } }

        public ReportChannelWrapper(IReport channel)
        {
            _channel = channel;
        }

        public void Dispose()
        {
            try
            {
                ((IClientChannel)_channel).Close();
            }
            catch (FaultException)
            {
                ((IClientChannel)_channel).Abort();
            }
            catch (CommunicationException)
            {
                ((IClientChannel)_channel).Abort();
            }
        }
    }
}