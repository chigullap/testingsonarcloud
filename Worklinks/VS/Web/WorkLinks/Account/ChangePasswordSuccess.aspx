﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="Account.Master" AutoEventWireup="true" CodeBehind="ChangePasswordSuccess.aspx.cs" Inherits="WorkLinks.Web.WorkLinks.Account.ChangePasswordSuccess" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent"></asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>Change Password</h2>
    <p>Your password has been changed successfully.</p>

    <wasp:WLPButton ID="CloseButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Close" OnClientClicked="Close" />

    <script type="text/javascript">
        function getRadWindow() {
            var popup = null;
            if (window.radWindow)
                popup = window.radWindow;
            else if (window.frameElement.radWindow)
                popup = window.frameElement.radWindow;
            return popup;
        }

        function Close(button, args) {
            button.set_autoPostBack(false);
            var popup = getRadWindow();
            setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
        }
    </script>
</asp:Content>