﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WorkLinks.Web.WorkLinks.Account.Login" ResourceName="Login" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script type="text/javascript">
    if (window != window.top) {
        window.top.location.href = window.location.href;
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Login Page</title>
    <style type="text/css">
        /* login CSS */
        .styleLogin {
            color: #FF6600;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 16px;
        }

        #login_main_box {
            width: 1000px;
            margin: auto;
        }

        .login_logo {
            height: 280px;
        }

        .middle_login_content {
            width: 496px;
            height: 668px;
            margin: auto;
        }

        .user_password {
            /*background-image:url(images/user_password.gif);
				background-repeat:no-repeat;*/
            margin-top: 55px;
            margin-left: auto;
            margin-right: auto;
            width: 374px;
            height: 72px;
        }

        .login_input {
            border: #cccccc solid 1px;
            height: 22px;
            width: 280px;
        }

        .user_name_login {
            font-family:  Arial, Helvetica, sans-serif;
            font-size: 16px;
            /*color:#000000;*/
            color: #a2a2a2;
            font-weight: normal;
        }

        .login_caption {
			background-repeat: no-repeat; 
			color: #00b0ef; 
			font-family: Arial, Helvetica, sans-serif; 
			font-weight: bold; font-size: 22px;
            text-align: center !important;
        }
    </style>
</head>
<body topmargin="0" background="../App_Themes/Default/pagebg.gif">
    <div align="center">
        <table border="0" cellspacing="0" cellpadding="0" width="663" height="432">
            <tr>
                <td width="663" height="432" valign="top" background="../App_Themes/Default/bg.gif" style="background-repeat: no-repeat">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <img src="../App_Themes/Default/spacer.gif" width="5" height="282">
                                <div style =" margin-left:310px; margin-top:-140px;">
                                    <wasp:WLPLabel Width="320px" CssClass="login_caption" ID="LoginMainCaptionLine" ResourceName="LoginMainCaptionLine" runat="server" AutoPostBack="False"  Rows="4" TextMode="MultiLine"></wasp:WLPLabel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div align="center">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="484" height="246" valign="middle" background="">
                                                <div align="center">
                                                    <form id="form1" runat="server" name="form1" method="post" action="">
                                                        <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>

                                                        <asp:Login
                                                            ID="Login1"
                                                            runat="server"
                                                            Width="400px"
                                                            DisplayRememberMe="False"
                                                            TitleText=""
                                                            Style="color: #E5E5E5; font-weight: bold; font-size: larger; margin-top: 0px; margin-left: 2px"
                                                            DestinationPageUrl="~/HumanResources/LandingPage.aspx" OnLoggedIn="Login1_LoggedIn"
                                                            OnLoginError="Login1_LoginError">

                                                            <LayoutTemplate>
                                                                <table border="0" cellpadding="0" style="width: 500px;">
                                                                    <tr>
                                                                        <td align="left" valign="middle">
                                                                            <wasp:WLPLabel ID="UserNameLabel" ResourceName="UserNameLabel" runat="server" Text="User Name:" AssociatedControlID="UserName" CssClass="styleLogin"></wasp:WLPLabel>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="UserName" CssClass="login_input" runat="server"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" colspan="2" style="height: 5px;"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="middle">
                                                                            <wasp:WLPLabel ID="PasswordLabel" runat="server" Text="Password:" ResourceName="PasswordLabel" AssociatedControlID="Password" CssClass="styleLogin"></wasp:WLPLabel>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Password" CssClass="login_input" runat="server" TextMode="Password"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" colspan="2" style="color: Red;">
                                                                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" colspan="2" style="height: 5px;"></td>
                                                                    </tr>
                                                                    <tr valign="middle">
                                                                        <td align="center" colspan="2">
                                                                            <wasp:WLPButton Style="margin: 2px;" ID="LoginImageButton" Icon-PrimaryIconUrl="../App_Themes/Default/update.gif" Text="Log In" runat="server" CommandName="Login" ResourceName="LoginImageButton" OnPreRender="LoginImageButton_PreRender" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" colspan="2" style="height: 5px;"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" colspan="2">
                                                                            <%--<asp:CheckBox ID="RememberMe" Font-Size="Small" Style="margin:2px;" ForeColor="Black"  Height="20px" runat="server" Text="Remeber Me" ResourceName="RememberMe"/>--%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" colspan="2" style="height: 20px;"></td>
                                                                    </tr>
                                                                    <tr valign="bottom">
                                                                        <td align="center" colspan="2">
                                                                            <wasp:WLPLabel CssClass="styleLogin" ID="LowerHeader" runat="server" ForeColor="White" Font-Size="Large" Font-Bold="false" ResourceName="LowerHeader" Text="Cloud Payroll Solution"></wasp:WLPLabel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" colspan="2" style="height: 20px;"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" colspan="2">
                                                                            <wasp:WLPLabel CssClass="styleLogin" ID="CompanyName" runat="server" Font-Size="Large" Font-Bold="False" AutoPostBack="False" Columns="0" MaxLength="0" OnPreRender="CompanyName_PreRender" ReadOnly="True" Rows="0" TextMode="SingleLine"></wasp:WLPLabel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </LayoutTemplate>
                                                        </asp:Login>
                                                        <asp:HiddenField ID="Resolution" runat="server" Value="" />
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div style="position: fixed; bottom: 0; left: 0;">
        <wasp:WLPLabel ID="VersionNumber" ForeColor="White" runat="server" AutoPostBack="False" Font-Size="Small" Font-Bold="false" Text="" OnPreRender="Version_PreRender" ReadOnly="true"></wasp:WLPLabel>
    </div>
</body>
</html>

<script type="text/javascript">
    document.getElementById("<%=Resolution.ClientID%>").value = screen.width + 'x' + screen.height;
</script>