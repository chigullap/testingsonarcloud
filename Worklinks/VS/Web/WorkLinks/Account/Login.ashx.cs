﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkLinks.Account
{
    /// <summary>
    /// Summary description for Login
    /// </summary>
    public class Login : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                context.Response.Redirect("~/UnauthorizedPage.aspx");
            }
            else
            {
                string loginUrl = "~/Account/Login.aspx";
                if (!string.IsNullOrEmpty(context.Request.Params["returnUrl"]))
                {
                    loginUrl = loginUrl + "?returnUrl=" + HttpUtility.UrlEncode(context.Request.Params["returnUrl"]);
                }
                context.Response.Redirect(loginUrl);
            }
        }

        public bool IsReusable
        {
            get { return true; }
        }
    }
}