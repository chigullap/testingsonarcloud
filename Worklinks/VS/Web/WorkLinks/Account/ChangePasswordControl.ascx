﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangePasswordControl.ascx.cs" Inherits="WorkLinks.Account.ChangePasswordControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<h2><wasp:WLPLabel ID="ChangePasswordLabel" runat="server" Text="**ChangePassword**" ResourceName="ChangePassword" /></h2>
<p><wasp:WLPLabel ID="MinRequiredPasswordLengthLabel" runat="server" Text="**MinRequiredPasswordLength**" ResourceName="MinRequiredPasswordLength" /></p>

<asp:ChangePassword
    ID="ChangeUserPassword"
    runat="server"
    CancelDestinationPageUrl="~/HumanResources/LandingPage.aspx"
    EnableViewState="false"
    RenderOuterTable="false"
    SuccessPageUrl="ChangePasswordSuccess.aspx">

    <ChangePasswordTemplate>
        <span class="failureNotification">
            <asp:Literal ID="FailureText" runat="server"></asp:Literal>
        </span>

        <asp:ValidationSummary ID="ChangeUserPasswordValidationSummary" runat="server" CssClass="failureNotification" ValidationGroup="ChangeUserPasswordValidationGroup" />

        <div class="accountInfoPasswordChange">
            <fieldset class="changePassword">
                <legend><wasp:WLPLabel ID="AccountInformationLabel" runat="server" Text="**AccountInformation**" ResourceName="AccountInformation" /></legend>
                <table>
                    <tr>
                        <td align="right">
                            <wasp:WLPLabel ID="CurrentPasswordLabel" runat="server" Text="**OldPassword**" AssociatedControlID="CurrentPassword" ResourceName="OldPassword"></wasp:WLPLabel>
                        </td>
                        <td>
                            <asp:TextBox ID="CurrentPassword" runat="server" CssClass="passwordEntryPasswordChange" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword" CssClass="failureNotification" ErrorMessage="<%$ Resources:ErrorMessages, PasswordIsRequired %>" ToolTip="<%$ Resources:ErrorMessages, PasswordIsRequired %>" ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="OldPasswordValidator" runat="server" ControlToValidate="CurrentPassword" ErrorMessage="<%$ Resources:ErrorMessages, OldPasswordDoesNotMatch %>" ValidationGroup="ChangeUserPasswordValidationGroup" OnServerValidate="ValidateOldPassword">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <wasp:WLPLabel ID="NewPasswordLabel" runat="server" Text="**NewPassword**" AssociatedControlID="NewPassword" ResourceName="NewPassword"></wasp:WLPLabel>
                        </td>
                        <td>
                            <asp:TextBox ID="NewPassword" runat="server" CssClass="passwordEntryPasswordChange" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword" CssClass="failureNotification" ErrorMessage="<%$ Resources:ErrorMessages, NewPasswordIsRequired %>" ToolTip="<%$ Resources:ErrorMessages, NewPasswordIsRequired %>" ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="OldNewPasswordCompare" runat="server" ControlToCompare="CurrentPassword" ControlToValidate="NewPassword" CssClass="failureNotification" Display="Dynamic" ErrorMessage="<%$ Resources:ErrorMessages, OldNewPasswordCompare %>" Operator="NotEqual" ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:CompareValidator>
                            <asp:CustomValidator ID="PasswordRulesvalidator" runat="server" ControlToValidate="NewPassword" ForeColor="Red" ErrorMessage="" ValidationGroup="ChangeUserPasswordValidationGroup" OnServerValidate="ValidatePasswordRules">*</asp:CustomValidator>
                            <asp:CustomValidator ID="PasswordHistoryValidator" runat="server" ControlToValidate="NewPassword" ErrorMessage="<%$ Resources:ErrorMessages, PasswordPreviouslyUsed %>" ValidationGroup="ChangeUserPasswordValidationGroup" OnServerValidate="ValidatePasswordHistory">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <wasp:WLPLabel ID="ConfirmNewPasswordLabel" runat="server" Text="**ConfirmNewPassword**" AssociatedControlID="ConfirmNewPassword" ResourceName="ConfirmNewPassword"></wasp:WLPLabel>
                        </td>
                        <td>
                            <asp:TextBox ID="ConfirmNewPassword" runat="server" CssClass="passwordEntryPasswordChange" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword" CssClass="failureNotification" Display="Dynamic" ErrorMessage="<%$ Resources:ErrorMessages, ConfirmNewPasswordIsRequired %>" ToolTip="<%$ Resources:ErrorMessages, ConfirmNewPasswordIsRequired %>" ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword" CssClass="failureNotification" Display="Dynamic" ErrorMessage="<%$ Resources:ErrorMessages, NewPasswordCompare %>" ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:CompareValidator>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <p class="submitButton">
                <wasp:WLPButton ID="CancelPushButton" runat="server" CausesValidation="false" Text="**Cancel**" OnClientClicked="Close" CommandName="Cancel" ResourceName="CancelPushButton"></wasp:WLPButton>
                <wasp:WLPButton ID="ChangePasswordPushButton" runat="server" Text="**ChangePassword**" ValidationGroup="ChangeUserPasswordValidationGroup" CommandName="ChangePassword" ResourceName="ChangePasswordPushButton"></wasp:WLPButton>
            </p>
        </div>
    </ChangePasswordTemplate>
</asp:ChangePassword>

<script type="text/javascript">
    function getRadWindow() {
        var popup = null;

        if (window.radWindow)
            popup = window.radWindow;
        else if (window.frameElement.radWindow)
            popup = window.frameElement.radWindow;

        return popup;
    }

    function Close(button, args) {
        button.set_autoPostBack(false);
        var popup = getRadWindow();
        setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
    }
</script>