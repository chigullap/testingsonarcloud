﻿<%@ Page Title="" Language="C#" MasterPageFile="Account.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="WorkLinks.Account.ChangePassword" ResourceName="ChangePassword" %>
<%@ Register Src="ChangePasswordControl.ascx" TagName="ChangePasswordControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
     <% if(Convert.ToBoolean(ConfigurationManager.AppSettings.Get("ANGULAR"))) { %>
        <iframe style="height:450px; width:550px; border: none;" src="<%=ConfigurationManager.AppSettings.Get("ANGULARURL")%>/admin/changepassword?language=<%=LanguageCode.ToLower()%>"></iframe>
    <% } %>
    <% else {%>
        <uc1:ChangePasswordControl ID="ChangePasswordControl1" runat="server" />
        <% } %>
    </ContentTemplate>
</asp:Content>