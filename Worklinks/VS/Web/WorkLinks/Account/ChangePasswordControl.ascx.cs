﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

namespace WorkLinks.Account
{
    public partial class ChangePasswordControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private bool IsLocked
        {
            get { return ((String)Page.RouteData.Values["lockMode"]).ToLower().Equals("true"); }
        }
        #endregion

        #region event handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.ChangePassword.ViewFlag);

            ((WLP.Web.UI.Controls.WLPButton)ChangeUserPassword.ChangePasswordTemplateContainer.FindControl("CancelPushButton")).Visible = !IsLocked;

            MinRequiredPasswordLengthLabel.Text = String.Format(GetGlobalResourceObject("PageContent", "MinRequiredPasswordLength").ToString(), System.Web.Security.Membership.MinRequiredPasswordLength);
        }

        //stacy test -- maybe remove this
        protected void SetValidationExpression(object sender, EventArgs e)
        {
            RegularExpressionValidator temp = (RegularExpressionValidator)sender;
            temp.ValidationExpression = "[^\\s]{" + Convert.ToInt16(System.Web.Security.Membership.Provider.MinRequiredPasswordLength) + ",20}";
        }

        //check that the old password matches what's in the db
        public void ValidateOldPassword(object sender, ServerValidateEventArgs args)
        {
            args.IsValid = Common.ServiceWrapper.SecurityClient.ValidateUser(Page.User.Identity.Name, args.Value.ToString());
        }

        //check that the new password has not been used previously
        public void ValidatePasswordHistory(object sender, ServerValidateEventArgs args)
        {
            args.IsValid = Common.ServiceWrapper.SecurityClient.ValidatePasswordHistory(Page.User.Identity.Name, args.Value.ToString());
        }

        //validate password rules
        public void ValidatePasswordRules(object sender, ServerValidateEventArgs args)
        {
            String password = args.Value.ToString();
            String errorMessage = "";
            bool exitMethod = false;

            //check if a number is required
            if (Convert.ToBoolean(Common.ApplicationParameter.PasswordOneNumberRequired))
            {
                if (!Regex.IsMatch(password, @"\d"))
                {
                    exitMethod = true;
                    errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "PasswordRequiresNumber");
                }
            }

            //check if a non-alpha char is required
            if (!exitMethod && Convert.ToBoolean(Common.ApplicationParameter.PasswordNonAlphaRequired))
            {
                if (password.All(char.IsLetterOrDigit))
                {
                    exitMethod = true;
                    errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "PasswordRequiresNonAlphaNumeric");
                }
            }

            //check if a Capital letter is required
            if (!exitMethod && Convert.ToBoolean(Common.ApplicationParameter.PasswordOneCapRequired))
            {
                if (!password.Any(char.IsUpper))
                {
                    exitMethod = true;
                    errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "PasswordRequiresCapitalLetter");
                }
            }

            //check if a lowercase letter is required
            if (!exitMethod && Convert.ToBoolean(Common.ApplicationParameter.PasswordOneLowerCaseRequired))
            {
                if (!password.Any(char.IsLower))
                {
                    exitMethod = true;
                    errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "PasswordRequiresLowerCaseLetter");
                }
            }

            //check if username is permitted in the password or not
            if (!exitMethod && Convert.ToBoolean(Common.ApplicationParameter.PasswordCantContainUserName))
            {
                if (password.Contains(System.Web.Security.Membership.GetUser().UserName.ToString()))
                {
                    exitMethod = true;
                    errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "PasswordCantContainUserName");
                }
            }

            //check if password contains spaces and quotes
            if (!exitMethod && Convert.ToBoolean(Common.ApplicationParameter.PasswordExcludeSpacesAndQuotes))
            {
                if (password.Contains(" ") || password.Contains("'") || password.Contains("\""))
                {
                    exitMethod = true;
                    errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "PasswordExcludeSpacesAndQuotes");
                }
            }

            //check that the password does not exceed the max character repeat pattern
            if (!exitMethod)
            {
                Int32 repeatCount = Convert.ToInt32(Common.ApplicationParameter.PasswordMaxCharacterRepeat);

                if (Regex.IsMatch(password, @"(.)\1{" + repeatCount + ",}", RegexOptions.IgnoreCase))
                {
                    exitMethod = true;
                    errorMessage = String.Format((String)GetGlobalResourceObject("ErrorMessages", "PasswordMaxCharacterRepeat"), repeatCount);
                }
            }

            //check that the password meets the minimum length requirement
            if (!exitMethod)
            {
                if (password.Length < Convert.ToInt32(Common.ApplicationParameter.PasswordMinLength))
                {
                    exitMethod = true;
                    errorMessage = String.Format((String)GetGlobalResourceObject("ErrorMessages", "PasswordMinLength"), Convert.ToInt32(Common.ApplicationParameter.PasswordMinLength));
                }
            }

            args.IsValid = !exitMethod;

            if (!args.IsValid)
                ((CustomValidator)sender).ErrorMessage = "* " + errorMessage;
        }
        #endregion
    }
}