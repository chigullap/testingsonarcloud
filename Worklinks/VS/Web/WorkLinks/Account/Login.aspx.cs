﻿using System;

namespace WorkLinks.Web.WorkLinks.Account
{
    public partial class Login : WLP.Web.UI.WLPPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Login1_LoggedIn(object sender, EventArgs e)
        {
        }

        protected void CompanyName_PreRender(object sender, EventArgs e)
        {
            WLP.Web.UI.Controls.WLPLabel label = ((WLP.Web.UI.Controls.WLPLabel)sender);
            String version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            if (version.Equals(Common.ApplicationParameter.DatabaseVersion))
                label.Text = Common.ApplicationParameter.CompanyName;
            else
            {
                label.ForeColor = System.Drawing.Color.Red;
                label.Text = String.Format("Database version {0} does not match application version {1}.", Common.ApplicationParameter.DatabaseVersion, version);
            }
        }

        protected void Version_PreRender(object sender, EventArgs e)
        {
            //show version on login screen
            ((WLP.Web.UI.Controls.WLPLabel)sender).Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        protected void LoginImageButton_PreRender(object sender, EventArgs e)
        {
            String version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            ((System.Web.UI.WebControls.WebControl)sender).Enabled = (Common.ApplicationParameter.DatabaseVersion.Equals(version));
        }

        protected void Login1_LoginError(object sender, EventArgs e)
        {
            Login1.FailureText = (String)GetGlobalResourceObject("ErrorMessages", "LoginFailure");
        }
    }
}