﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;

namespace WorkLinks.Report
{
    public partial class SkillReportControl : WLP.Web.UI.WLPUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.SkillReport.ViewFlag);

            if (!IsPostBack)
            {
                Panel1.DataBind();
            }
        }


        #region event handlers
        protected void EmployeeSkillCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateEmployeeSkillCodeControl((ICodeControl)sender, LanguageCode);
        }

        protected void OrganizationUnitId_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateOrganizationUnitWithLevelDescription((ICodeControl)sender);
        }

        protected void EmployeeSkillCode_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            // remember the real value of the "Employee Position Status" dropdown 
            EmployeeSkillCodeValue.Value = e.Value;
        }

        protected void OrganizationUnitId_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            // remember the real value of the "Organization Unit Id" dropdown 
            OrganizationUnitIdValue.Value = e.Value;
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            EmployeeSkillCode.SelectedIndex = 0;
            OrganizationUnitId.SelectedIndex = 0;

            EmployeeSkillCodeValue.Value = "";
            OrganizationUnitIdValue.Value = "";
        }
        #endregion
    }
}