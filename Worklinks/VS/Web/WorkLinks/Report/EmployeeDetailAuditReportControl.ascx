﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeDetailAuditReportControl.ascx.cs" Inherits="WorkLinks.Report.EmployeeDetailAuditReportControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnViewReport">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pdfSplitter" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnClear">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SearchPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="SearchPanel" runat="server">
    <table width="100%">
        <tr valign="top">
            <td style="float: left;">
                <div>
                    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnViewReport">
                        <table width="100%">
                            <tr>
                                <td>
                                    <wasp:DateControl ID="StartDate" runat="server" ResourceName="dateStart" OnSelectedDateChanged="StartDate_SelectedDateChanged" AutoPostback="true" Mandatory="true" Overlay="true" TabIndex="010" />
                                </td>
                                <td>
                                   <wasp:DateControl ID="EndDate" runat="server" ResourceName="dateEnd" Mandatory="true" Overlay="true" TabIndex="020" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:TextBoxControl ID="OrganizationUnitDescription" Text="**OrganizationUnitDescription**" runat="server" ResourceName="OrganizationUnitDescription" ReadOnly="false" TabIndex="030" />
                                </td>
                                <td>
                                    <wasp:TextBoxControl ID="EmployeeNumber" Text="**EmployeeNumber**" runat="server" ResourceName="EmployeeNumber" ReadOnly="false" TabIndex="040"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:ComboBoxControl ID="PaymentMethodCode" Text="**PaymentMethodCode**" runat="server" ResourceName="PaymentMethodCode" OnDataBinding="Code_NeedDataSource" Type="PaymentMethodCode" ReadOnly="false" TabIndex="050" />
                                </td>
                            </tr>
                        </table>
                        <div class="SearchCriteriaButtons">
                            <wasp:WLPButton ID="btnClear" runat="server" Text="**Clear**" Icon-PrimaryIconUrl="~/App_Themes/Default/clear.gif" OnClick="btnClear_Click" OnClientClicked="btnClear_Clicked" ResourceName="Clear" />
                            <wasp:WLPButton ID="btnViewReport" runat="server" Text="**View**" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" AutoPostBack="false" OnClientClicked="btnViewReport_Clicked" ResourceName="ViewReport" />
                        </div>
                    </asp:Panel>
                </div>
            </td>
            <td style="float: right;">
                <div>
                    <wasp:WLPLabel ID="lblExport" runat="server" Font-Size="Small" Text="**Export**" ResourceName="Export" />
                    <wasp:WLPButton ID="btnExportExcel" runat="server" Text="**Excel**" AutoPostBack="false" OnClientClicked="btnExportExcel_Clicked" Enabled="false" ResourceName="ExportExcel" />
                </div>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
                <div>
                    <telerik:RadSplitter ID="pdfSplitter" runat="server" Orientation="Horizontal" Height="600" Width="100%" VisibleDuringInit="false" OnClientLoad="showContentForIE">
                        <telerik:RadPane ID="pdfPane" runat="server" ShowContentDuringLoad="false" />
                    </telerik:RadSplitter>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var startDate = null;
        var endDate = null;
        var employeeNumber = null;
        var organizationUnit = null;
        var paymentMethod = null;
        var isValid = false;

        function showContentForIE(splt) {
            if ($telerik.isIE)
                splt._panes[0]._showContentDuringLoad = true;
        }

        function getReportPath(reportName, reportType, employeeNumber, startDate, endDate, organizationUnit, paymentMethod) {
            return getUrl(String.format('/Report/{0}/{1}/{2}/{3}/{4}/{5}/{6}', reportName, reportType, employeeNumber, startDate, endDate, organizationUnit, paymentMethod));
        }

        function downloadReport(reportName, reportType, employeeNumber, dateStart, dateEnd, organizationUnit, paymentMethod) {
            var frame = document.createElement("frame");
            frame.src = getReportPath(reportName, reportType, employeeNumber, dateStart, dateEnd, organizationUnit, paymentMethod);
            frame.style.display = "none";
            document.body.appendChild(frame);
        }

        function enableButtons(enable) {
            var exportExcelButton = $find("<%= btnExportExcel.ClientID %>");
            exportExcelButton.set_enabled(enable);
        }

        function btnClear_Clicked(sender, eventArgs) {
            enableButtons(false);

            var pdfPane = $find("<%= pdfPane.ClientID %>");
            if (pdfPane._contentUrl != "")
                pdfPane.set_contentUrl("");
        }

        function getParameters() {
            var startDateControl = document.getElementById("<%= StartDate.ClientID %>");
            if (startDateControl != null)
                startDate = (document.getElementById(startDateControl.attributes['fieldClientId'].value)).value;
            
            var endDateControl = document.getElementById("<%= EndDate.ClientID %>");
            if (endDateControl != null)
                endDate = (document.getElementById(endDateControl.attributes['fieldClientId'].value)).value;
            
            var employeeNumberControl = document.getElementById("<%= EmployeeNumber.ClientID %>");
            if (employeeNumberControl != null)
                employeeNumber = (document.getElementById(employeeNumberControl.attributes['fieldClientId'].value)).value;
            if (employeeNumber == "")
                employeeNumber = "x";
            
            var orgUnitControl = document.getElementById("<%= OrganizationUnitDescription.ClientID %>");
            if (orgUnitControl != null)
                organizationUnit = (document.getElementById(orgUnitControl.attributes['fieldClientId'].value)).value;
            if (organizationUnit == "")
                organizationUnit = "x";
            
            var paymentMethodControl = document.getElementById("<%= PaymentMethodCode.ClientID %>");
            if (paymentMethodControl != null)
                paymentMethod = (document.getElementById(paymentMethodControl.attributes['fieldClientId'].value)).value;
            if (paymentMethod == "")
                paymentMethod = "x";

            isValid = !(startDate == '' || startDate == null || endDate == '' || endDate == null);
        }

        function btnViewReport_Clicked(sender, eventArgs) {
            getParameters();

            if (isValid) {
                var pdfPane = $find("<%= pdfPane.ClientID %>");
                pdfPane.set_contentUrl(getReportPath("EmployeeDetailAuditReport", "PDF", employeeNumber, startDate, endDate, organizationUnit, paymentMethod));

                enableButtons(true);
            }
        }

        function btnExportExcel_Clicked(sender, eventArgs) {
            getParameters();

            if (isValid)
                downloadReport("EmployeeDetailAuditReport", "EXCEL", employeeNumber, startDate, endDate, organizationUnit, paymentMethod);
        }
    </script>
</telerik:RadScriptBlock>