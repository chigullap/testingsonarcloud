﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HrChangesAuditReportControl.ascx.cs" Inherits="WorkLinks.Report.HrChangesAuditReportControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnViewReport">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pdfSplitter" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
 
<table width="100%">
    <tr valign="top">
        <td style="float: left;">
            <div>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnViewReport">
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:DateControl ID="StartDate" LabelText="**StartDate**" runat="server" ResourceName="StartDate" Mandatory="true" TabIndex="010" />
                            </td>
                            <td>
                                <wasp:DateControl ID="EndDate" LabelText="**EndDate**" runat="server" ResourceName="EndDate" Mandatory="true" TabIndex="020" />
                            </td>
                            <td>
                                <wasp:ComboBoxControl ID="OrganizationUnitId" LabelText="**OrganizationUnitId**" runat="server" ResourceName="OrganizationUnitId" Type="OrganizationUnitId" 
                                                                                                                  AutoPostback="true" OnDataBinding="OrganizationUnitId_NeedDataSource" OnSelectedIndexChanged="OrganizationUnitId_SelectedIndexChanged" 
                                                                                                                  OnClientSelectedIndexChanged="btnClear_Clicked" TabIndex="030" />
                                <asp:HiddenField ID="OrganizationUnitIdValue" ClientIDMode="Static" runat="server" Value="" />
                            </td>
                        </tr>
                    </table>
                    <div class="SearchCriteriaButtons">
                        <wasp:WLPButton ID="btnClear" runat="server" Text="**Clear**" Icon-PrimaryIconUrl="~/App_Themes/Default/clear.gif" OnClick="btnClear_Click" OnClientClicked="btnClear_Clicked" ResourceName="Clear" />
                        <wasp:WLPButton ID="btnViewReport" runat="server" Text="**View**" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" AutoPostBack="false" OnClientClicked="btnViewReport_Clicked" ResourceName="ViewReport" />
                    </div>
                </asp:Panel>
            </div>
        </td>
        <td style="float: right;">
            <div>
                <wasp:WLPLabel ID="lblExport" runat="server" Font-Size="Small" Text="**Export**" ResourceName="Export"></wasp:WLPLabel>
                <wasp:WLPButton ID="btnExportPdf" runat="server" Text="**PDF**" AutoPostBack="false" OnClientClicked="btnExportPdf_Clicked" Enabled="false" ResourceName="ExportPdf" />
                <wasp:WLPButton ID="btnExportExcel" runat="server" Text="**Excel**" AutoPostBack="false" OnClientClicked="btnExportExcel_Clicked" Enabled="false" ResourceName="ExportExcel" />
            </div>
        </td>
    </tr>
    <tr valign="top">
        <td colspan="2">
            <div>
                <telerik:RadSplitter ID="pdfSplitter" runat="server" Orientation="Horizontal" Height="600" Width="100%" VisibleDuringInit="false" OnClientLoad="showContentForIE">
                    <telerik:RadPane ID="pdfPane" runat="server" ShowContentDuringLoad="false"></telerik:RadPane>
                </telerik:RadSplitter>
            </div>
        </td>
    </tr>
</table>
 
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var startDateValue = null;
        var endDateValue = null;
        var organizationUnitIdValue = null;
 
        function showContentForIE(splt) {
            if ($telerik.isIE)
                splt._panes[0]._showContentDuringLoad = true;
        }
 
        function getReportPath(reportName, payrollProcessId, employeeNumber, reportType, startDate, endDate, organizationUnitId, revision) {
            //alert("getReportPath(): startDate=" + startDate);
            //alert("getReportPath(): endDate=" + endDate);
            //alert("getReportPath(): organizationUnitId=" + organizationUnitId);
            return getUrl(String.format('/Report/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}', reportName, payrollProcessId, employeeNumber, reportType, startDate, endDate, organizationUnitId, revision));
        }
 
        function getParameters() {
            // Parameter 1
            var startDateControl = document.getElementById("<%= StartDate.ClientID %>");
            if (startDateControl != null) {
                startDateValue = (document.getElementById(startDateControl.attributes['fieldClientId'].value)).value;
            }
            //alert("getParameters(): Parameter 1: startDateValue=" + startDateValue);
 
            // Parameter 2
            var endDateControl = document.getElementById("<%= EndDate.ClientID %>");
            if (endDateControl != null) {
                endDateValue = (document.getElementById(endDateControl.attributes['fieldClientId'].value)).value;
            }
            //alert("getParameters(): Parameter 2: endDateValue=" + endDateValue);

            // Parameter 3
            var organizationUnitIdValueHiddenControl = document.getElementById('OrganizationUnitIdValue');
            if (organizationUnitIdValueHiddenControl != null) {
                organizationUnitIdValue = organizationUnitIdValueHiddenControl.value;
            }
            if (organizationUnitIdValue == "") {
                organizationUnitIdValue = "x";
            }
            //alert("getParameters(): Parameter 3: organizationUnitIdValue=" + organizationUnitIdValue);
        }
 
        function downloadReport(reportName, payrollProcessId, employeeNumber, reportType, startDate, endDate, organizationUnitId, revision) {
            var frame = document.createElement("frame");
            frame.src = getReportPath(reportName, payrollProcessId, employeeNumber, reportType, startDate, endDate, organizationUnitId, revision);
            frame.style.display = "none";
            document.body.appendChild(frame);
        }
 
        function enableButtons(enable) {
            var exportPdfButton = $find("<%= btnExportPdf.ClientID %>");
            exportPdfButton.set_enabled(enable);
 
            var exportExcelButton = $find("<%= btnExportExcel.ClientID %>");
            exportExcelButton.set_enabled(enable);
        }
 
        function btnClear_Clicked(sender, eventArgs) {
            enableButtons(false);
 
            var pdfPane = $find("<%= pdfPane.ClientID %>");
            pdfPane.set_contentUrl("");
        }
 
        function btnViewReport_Clicked(sender, eventArgs) {
            getParameters();
 
            if (startDateValue == null || startDateValue == "" || endDateValue == null || endDateValue == "") {
                alert("There was an error in processing your request. Please try again.");
            } else {
                var pdfPane = $find("<%= pdfPane.ClientID %>");
                pdfPane.set_contentUrl(getReportPath("HrChangesAuditReport", -1, "x", "PDF", startDateValue, endDateValue, organizationUnitIdValue, "x"));
 
                enableButtons(true);
            }
        }
 
        function btnExportPdf_Clicked(sender, eventArgs) {
            getParameters();
 
            if (startDateValue == null || startDateValue == "" || endDateValue == null || endDateValue == "") {
                alert("There was an error in processing your request. Please try again.");
            } else {
                downloadReport("HrChangesAuditReport", -1, "x", "PDFDownload", startDateValue, endDateValue, organizationUnitIdValue, "x");
            }
        }
 
        function btnExportExcel_Clicked(sender, eventArgs) {
            getParameters();
 
            if (startDateValue == null || startDateValue == "" || endDateValue == null || endDateValue == "") {
                alert("There was an error in processing your request. Please try again.");
            } else {
                downloadReport("HrChangesAuditReport", -1, "x", "EXCEL", startDateValue, endDateValue, organizationUnitIdValue, "x");
            }
        }
    </script>
</telerik:RadScriptBlock>
