﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaycodeReportControl.ascx.cs" Inherits="WorkLinks.Report.PaycodeReportControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnViewReport">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pdfSplitter" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnClear">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pdfSplitter" />
                <telerik:AjaxUpdatedControl ControlID="Year" />
                <telerik:AjaxUpdatedControl ControlID="PaycodeTypeCode" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<table width="100%">
    <tr valign="top">
        <td style="float: left;">
            <div>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnExportExcel">
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="Year" LabelText="**Year**" runat="server" ResourceName="Year" Type="Year" OnDataBinding="Year_NeedDataSource" TabIndex="010" />
                            </td>
                            <td>
                                <wasp:ComboBoxControl ID="PaycodeTypeCode" LabelText="**PaycodeType**" runat="server" ResourceName="PaycodeTypeCode" Type="PaycodeTypeCode" OnDataBinding="Code_NeedDataSource" TabIndex="020" />
                            </td>
                            <td>
                                <wasp:WLPLabel ID="lblExport" runat="server" Font-Size="Small" Text="**Export**" ResourceName="Export" />
                                <wasp:WLPButton ID="btnExportExcel" runat="server" Text="**Excel**" AutoPostBack="false" OnClientClicked="btnExportExcel_Clicked" Enabled="true" ResourceName="ExportExcel" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center">
                                <wasp:WLPLabel ID="lblNoPreview" runat="server" Font-Size="Small" Text="No preview available, please export" ResourceName="NoPreviewMessage" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </td>
    </tr>
</table>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var year = null;
        var paycodeType = null;

        function showContentForIE(splt) {
            if ($telerik.isIE) splt._panes[0]._showContentDuringLoad = true;
        }

        function getReportPath(reportName, payrollProcessId, employeeNumber, reportType, year, employerNumber, provinceCode, revision) {
            return getUrl(String.format('/Report/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}', reportName, payrollProcessId, employeeNumber, reportType, year, employerNumber, provinceCode, revision));
        }

        function getParameters() {
            var yearControl = document.getElementById("<%= Year.ClientID %>");
            if (yearControl != null) year = (document.getElementById(yearControl.attributes['fieldClientId'].value)).value;
            if (year == "" || year == null) year = "x";

            var paycodeTypeControl = document.getElementById("<%= PaycodeTypeCode.ClientID %>");
            if (paycodeTypeControl != null) paycodeType = (document.getElementById(paycodeTypeControl.attributes['fieldClientId'].value)).value;
            if (paycodeType == "" || paycodeType == null) paycodeType = "x";
        }

        function downloadReport(reportName, payrollProcessId, employeeNumber, reportType, year, employerNumber, provinceCode, revision) {
            var frame = document.createElement("frame");
            frame.src = getReportPath(reportName, payrollProcessId, employeeNumber, reportType, year, employerNumber, provinceCode, revision);
            frame.style.display = "none";
            document.body.appendChild(frame);
        }

        function btnExportExcel_Clicked(sender, eventArgs) {
            getParameters();
            downloadReport("PaycodeReport", -1, "x", "EXCEL", year, paycodeType, "x", "x");
        }
    </script>
</telerik:RadScriptBlock>