﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SalaryHourlyReportPage.aspx.cs" Inherits="WorkLinks.Report.SalaryHourlyReportPage" %>
<%@ Register Src="SalaryHourlyReportControl.ascx" TagName="SalaryHourlyReportControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:SalaryHourlyReportControl ID="SalaryHourlyReportControl1" SearchLocation="Report" runat="server" />        
    </ContentTemplate>
</asp:Content>