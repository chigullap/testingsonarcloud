﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StatHolidayPage.aspx.cs" Inherits="WorkLinks.Report.StatHolidayPage" %>
<%@ Register Src="StatHolidayControl.ascx" TagName="StatHolidayControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:StatHolidayControl ID="StatHolidayControl1" SearchLocation="Report" runat="server" />        
    </ContentTemplate>
</asp:Content>