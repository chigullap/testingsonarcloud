﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;

namespace WorkLinks.Report
{
    public partial class AttritionReportControl : WLP.Web.UI.WLPUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.AttritionReport.ViewFlag);

            if (!IsPostBack)
            {
                Panel1.DataBind();
                Year.Value = DateTime.Now.Year.ToString();

                Common.CodeHelper.PopulateControl(MonthCode, LanguageCode);
                MonthCode.DataBind();
            }
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            MonthCode.SelectedIndex = 0;
            Year.Value = DateTime.Now.Year.ToString();
        }
        protected void Year_NeedDataSource(object sender, EventArgs e)
        {
            int currentYear = DateTime.Now.Year;
            CodeCollection codeCollection = new CodeCollection();

            for (int i = 2013; i <= currentYear; i++)
            {
                codeCollection.Add(new CodeObject()
                {
                    Code = i.ToString(),
                    Description = i.ToString(),
                    LanguageCode = LanguageCode
                });
            }

            ((ICodeControl)sender).DataSource = codeCollection;
            Year.Value = (currentYear).ToString();
        }
    }
}