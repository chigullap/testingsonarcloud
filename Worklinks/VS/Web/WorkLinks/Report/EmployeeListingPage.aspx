﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeeListingPage.aspx.cs" Inherits="WorkLinks.Report.EmployeeListingPage" %>
<%@ Register Src="EmployeeListingControl.ascx" TagName="EmployeeListingControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:EmployeeListingControl ID="EmployeeListingControl1" SearchLocation="Report" runat="server" />        
    </ContentTemplate>
</asp:Content>