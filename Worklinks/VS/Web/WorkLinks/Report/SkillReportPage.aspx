﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SkillReportPage.aspx.cs" Inherits="WorkLinks.Report.SkillReportPage" %>
<%@ Register Src="SkillReportControl.ascx" TagName="SkillReportControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:SkillReportControl ID="SkillReportControl1" SearchLocation="Report" runat="server" />        
    </ContentTemplate>
</asp:Content>
