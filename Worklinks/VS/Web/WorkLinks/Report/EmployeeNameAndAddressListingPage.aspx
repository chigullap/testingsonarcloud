﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeeNameAndAddressListingPage.aspx.cs" Inherits="WorkLinks.Report.EmployeeNameAndAddressListingPage" %>
<%@ Register Src="EmployeeNameAndAddressListingControl.ascx" TagName="EmployeeNameAndAddressListingControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:EmployeeNameAndAddressListingControl ID="EmployeeNameAndAddressListingControl1" SearchLocation="Report" runat="server" />        
    </ContentTemplate>
</asp:Content>