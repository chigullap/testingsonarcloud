﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;

using System.Collections.Generic;

namespace WorkLinks.Report
{
    public partial class PaycodeYearEndMappingReportControl : WLP.Web.UI.WLPUserControl
    {
        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Panel1.DataBind();
            }
        }
        #endregion

        #region event handlers
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Year.Value = Convert.ToString(DateTime.Now.Year);
        }
        protected void Year_NeedDataSource(object sender, EventArgs e)
        {
            int currentYear = DateTime.Now.Year;
            CodeCollection codeCollection = new CodeCollection();
            for (int i = 2013; i <= currentYear; i++)
            {
                codeCollection.Add(new CodeObject()
                {
                    Code = i.ToString(),
                    Description = i.ToString(),
                    LanguageCode = LanguageCode
                });
            }
            ////////////////////////////////////////////////////////////////////////////////
            ((ICodeControl)sender).DataSource = codeCollection;
            Year.Value = (currentYear).ToString();
        }
        #endregion
    }
}