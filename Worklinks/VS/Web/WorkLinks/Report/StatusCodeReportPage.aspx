﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StatusCodeReportPage.aspx.cs" Inherits="WorkLinks.Report.StatusCodeReportPage" %>
<%@ Register Src="StatusCodeReportControl.ascx" TagName="StatusCodeReportControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:StatusCodeReportControl ID="StatusCodeReportControl1" SearchLocation="Report" runat="server" />        
    </ContentTemplate>
</asp:Content>