﻿using System;

namespace WorkLinks.Report
{
    public partial class TerminatedEmployeesControl : WLP.Web.UI.WLPUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.TerminatedEmployees.ViewFlag);
        }
    }
}