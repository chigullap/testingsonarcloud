﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeeEmergencyContactsPage.aspx.cs" Inherits="WorkLinks.Report.EmployeeEmergencyContactsPage" %>
<%@ Register Src="EmployeeEmergencyContactsControl.ascx" TagName="EmployeeEmergencyContactsControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:EmployeeEmergencyContactsControl ID="EmployeeEmergencyContactsControl1" SearchLocation="Report" runat="server" />        
    </ContentTemplate>
</asp:Content>