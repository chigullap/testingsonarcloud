﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TerminatedEmployeesPage.aspx.cs" Inherits="WorkLinks.Report.TerminatedEmployeesPage" %>
<%@ Register Src="TerminatedEmployeesControl.ascx" TagName="TerminatedEmployeesControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:TerminatedEmployeesControl ID="TerminatedEmployeesControl1" SearchLocation="Report" runat="server" />        
    </ContentTemplate>
</asp:Content>