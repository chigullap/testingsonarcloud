﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PaycodeReportPage.aspx.cs" Inherits="WorkLinks.Report.PaycodeReportPage" %>
<%@ Register Src="PaycodeReportControl.ascx" TagName="PaycodeReportControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:PaycodeReportControl ID="PaycodeReportControl1" SearchLocation="Report" runat="server" />        
    </ContentTemplate>
</asp:Content>