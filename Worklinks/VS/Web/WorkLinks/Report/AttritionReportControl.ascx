﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttritionReportControl.ascx.cs" Inherits="WorkLinks.Report.AttritionReportControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnViewReport">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pdfSplitter" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnClear">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pdfSplitter" />
                <telerik:AjaxUpdatedControl ControlID="MonthCode" />
                <telerik:AjaxUpdatedControl ControlID="Year" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<table width="100%">
    <tr valign="top">
        <td style="float: left;">
            <div>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnViewReport">
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="MonthCode" runat="server" Type="MonthCode" Text="**MonthCode**" ResourceName="MonthCode" Mandatory="true" TabIndex="010" />
                            </td>
                            <td>
                                <wasp:ComboBoxControl ID="Year" LabelText="**Year**" runat="server" ResourceName="Year" IncludeEmptyItem="false" Type="Year" OnDataBinding="Year_NeedDataSource" Mandatory="true" TabIndex="020" />
                            </td>
                        </tr>
                    </table>
                    <div class="SearchCriteriaButtons">
                        <wasp:WLPButton ID="btnClear" runat="server" Text="**Clear**" Icon-PrimaryIconUrl="~/App_Themes/Default/clear.gif" OnClick="btnClear_Click" OnClientClicked="btnClear_Clicked" ResourceName="Clear" />
                        <wasp:WLPButton ID="btnViewReport" runat="server" Text="**View**" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" AutoPostBack="false" OnClientClicked="btnViewReport_Clicked" ResourceName="ViewReport" />
                    </div>
                </asp:Panel>
            </div>
        </td>
        <td style="float: right;">
            <div>
                <wasp:WLPLabel ID="lblExport" runat="server" Font-Size="Small" Text="**Export**" ResourceName="Export" />
                <wasp:WLPButton ID="btnExportPdf" runat="server" Text="**PDF**" AutoPostBack="false" OnClientClicked="btnExportPdf_Clicked" Enabled="false" ResourceName="ExportPdf" />
                <wasp:WLPButton ID="btnExportExcel" runat="server" Text="**Excel**" AutoPostBack="false" OnClientClicked="btnExportExcel_Clicked" Enabled="false" ResourceName="ExportExcel" />
            </div>
        </td>
    </tr>
    <tr valign="top">
        <td colspan="2">
            <div>
                <telerik:RadSplitter ID="pdfSplitter" runat="server" Orientation="Horizontal" Height="600" Width="100%" VisibleDuringInit="false" OnClientLoad="showContentForIE">
                    <telerik:RadPane ID="pdfPane" runat="server" ShowContentDuringLoad="false" />
                </telerik:RadSplitter>
            </div>
        </td>
    </tr>
</table>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var monthCode = null;
        var year = null;
        var isValid = false;

        function showContentForIE(splt) {
            if ($telerik.isIE)
                splt._panes[0]._showContentDuringLoad = true;
        }

        function getReportPath(reportName, payrollProcessId, employeeNumber, reportType, year, employerNumber, provinceCode, revision) {
            return getUrl(String.format('/Report/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}', reportName, payrollProcessId, employeeNumber, reportType, year, employerNumber, provinceCode, revision));
        }

        function getParameters() {
            var yearControl = document.getElementById("<%= Year.ClientID %>");
            if (yearControl != null)
                year = (document.getElementById(yearControl.attributes['fieldClientId'].value)).value;

            var monthCodeControl = document.getElementById("<%= MonthCode.ClientID %>");
            if (monthCodeControl != null)
                monthCode = (document.getElementById(monthCodeControl.attributes['fieldClientId'].value)).value.substring(0, 2);

            isValid = !(year == null || monthCode == '' || monthCode == null);
        }

        function downloadReport(reportName, payrollProcessId, employeeNumber, reportType, year, employerNumber, provinceCode, revision) {
            var frame = document.createElement("frame");
            frame.src = getReportPath(reportName, payrollProcessId, employeeNumber, reportType, year, employerNumber, provinceCode, revision);
            frame.style.display = "none";
            document.body.appendChild(frame);
        }

        function enableButtons(enable) {
            var exportPdfButton = $find("<%= btnExportPdf.ClientID %>");
            exportPdfButton.set_enabled(enable);

            var exportExcelButton = $find("<%= btnExportExcel.ClientID %>");
            exportExcelButton.set_enabled(enable);
        }

        function btnViewReport_Clicked(sender, eventArgs) {
            getParameters();

            if (isValid) {
                var pdfPane = $find("<%= pdfPane.ClientID %>");
                pdfPane.set_contentUrl(getReportPath("AttritionReport", -1, monthCode, "PDF", year, "x", "x", "x"));

                enableButtons(true);
            }
        }

        function btnClear_Clicked(sender, eventArgs) {
            enableButtons(false);

            var pdfPane = $find("<%= pdfPane.ClientID %>");
            if (pdfPane._contentUrl != "")
                pdfPane.set_contentUrl("");
        }

        function btnExportPdf_Clicked(sender, eventArgs) {
            getParameters();

            if (isValid)
                downloadReport("AttritionReport", -1, monthCode, "PDFDownload", year, "x", "x", "x");
        }

        function btnExportExcel_Clicked(sender, eventArgs) {
            getParameters();

            if (isValid)
                downloadReport("AttritionReport", -1, monthCode, "EXCEL", year, "x", "x", "x");
        }
    </script>
</telerik:RadScriptBlock>