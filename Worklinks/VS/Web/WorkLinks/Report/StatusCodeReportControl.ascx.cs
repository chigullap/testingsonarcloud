﻿using System;

namespace WorkLinks.Report
{
    public partial class StatusCodeReportControl : WLP.Web.UI.WLPUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.StatusCodeReport.ViewFlag);
        }
    }
}