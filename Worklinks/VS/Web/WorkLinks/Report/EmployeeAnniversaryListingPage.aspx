﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeeAnniversaryListingPage.aspx.cs" Inherits="WorkLinks.Report.EmployeeAnniversaryListingPage" %>
<%@ Register Src="EmployeeAnniversaryListingControl.ascx" TagName="EmployeeAnniversaryListingControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:EmployeeAnniversaryListingControl ID="EmployeeAnniversaryListingControl1" SearchLocation="Report" runat="server" />        
    </ContentTemplate>
</asp:Content>