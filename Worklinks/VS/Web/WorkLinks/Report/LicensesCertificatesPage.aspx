﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LicensesCertificatesPage.aspx.cs" Inherits="WorkLinks.Report.LicensesCertificatesPage" %>
<%@ Register Src="LicensesCertificatesControl.ascx" TagName="LicensesCertificatesControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:LicensesCertificatesControl ID="LicensesCertificatesControl1" SearchLocation="Report" runat="server" />        
    </ContentTemplate>
</asp:Content>