﻿using System;

namespace WorkLinks.Report
{
    public partial class NonStatDeductionsControl : WLP.Web.UI.WLPUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.NonStatDeductions.ViewFlag);

            if (!IsPostBack)
            {
                Panel1.DataBind();
                DefaultDate();

                Common.CodeHelper.PopulateVendorComboControlFilterByType(Vendor, "OTHER");
                Vendor.DataBind();
            }
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            DefaultDate();
            Vendor.SelectedIndex = 0;
        }
        private void DefaultDate()
        {
            //default to the first day of the month prior to current date
            DateTime today = DateTime.Today;
            Date.Value = new DateTime(today.Year, today.Month, 1).AddMonths(-1);
        }
    }
}