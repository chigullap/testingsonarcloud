﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NonStatDeductionsPage.aspx.cs" Inherits="WorkLinks.Report.NonStatDeductionsPage" %>
<%@ Register Src="NonStatDeductionsControl.ascx" TagName="NonStatDeductionsControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:NonStatDeductionsControl ID="NonStatDeductionsControl1" SearchLocation="Report" runat="server" />        
    </ContentTemplate>
</asp:Content>