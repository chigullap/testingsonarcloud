﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProbationReportPage.aspx.cs" Inherits="WorkLinks.Report.ProbationReportPage" %>
<%@ Register Src="ProbationReportControl.ascx" TagName="ProbationReportControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:ProbationReportControl ID="ProbationReportControl1" SearchLocation="Report" runat="server" />        
    </ContentTemplate>
</asp:Content>

