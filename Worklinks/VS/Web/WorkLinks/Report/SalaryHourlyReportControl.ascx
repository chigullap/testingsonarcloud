﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SalaryHourlyReportControl.ascx.cs" Inherits="WorkLinks.Report.SalaryHourlyReportControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnViewReport">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pdfSplitter" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<table width="100%">
    <tr valign="top">
        <td style="float: left;">
            <div>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnViewReport">
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="EmployeePositionStatusCode" LabelText="**EmployeePositionStatusCode**" runat="server" ResourceName="EmployeePositionStatusCode" Type="EmployeePositionStatusCode" AutoPostback="true" OnDataBinding="EmployeePositionStatusCode_NeedDataSource" OnSelectedIndexChanged="EmployeePositionStatusCode_SelectedIndexChanged" OnClientSelectedIndexChanged="btnClear_Clicked" TabIndex="010" />
                                <asp:HiddenField ID="EmployeePositionStatusCodeValue" ClientIDMode="Static" runat="server" Value="" />
                            </td>
                            <td>
                                <wasp:ComboBoxControl ID="OrganizationUnitId" LabelText="**OrganizationUnitId**" runat="server" ResourceName="OrganizationUnitId" Type="OrganizationUnitId" AutoPostback="true" OnDataBinding="OrganizationUnitId_NeedDataSource" OnSelectedIndexChanged="OrganizationUnitId_SelectedIndexChanged" OnClientSelectedIndexChanged="btnClear_Clicked" TabIndex="020" />
                                <asp:HiddenField ID="OrganizationUnitIdValue" ClientIDMode="Static" runat="server" Value="" />
                            </td>
                        </tr>
                    </table>
                    <div class="SearchCriteriaButtons">
                        <wasp:WLPButton ID="btnClear" runat="server" Text="**Clear**" Icon-PrimaryIconUrl="~/App_Themes/Default/clear.gif" OnClick="btnClear_Click" OnClientClicked="btnClear_Clicked" ResourceName="Clear" />
                        <wasp:WLPButton ID="btnViewReport" runat="server" Text="**View**" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" AutoPostBack="false" OnClientClicked="btnViewReport_Clicked" ResourceName="ViewReport" />
                    </div>
                </asp:Panel>
            </div>
        </td>
        <td style="float: right;">
            <div>
                <wasp:WLPLabel ID="lblExport" runat="server" Font-Size="Small" Text="**Export**" ResourceName="Export"></wasp:WLPLabel>
                <wasp:WLPButton ID="btnExportPdf" runat="server" Text="**PDF**" AutoPostBack="false" OnClientClicked="btnExportPdf_Clicked" Enabled="false" ResourceName="ExportPdf" />
                <wasp:WLPButton ID="btnExportExcel" runat="server" Text="**Excel**" AutoPostBack="false" OnClientClicked="btnExportExcel_Clicked" Enabled="false" ResourceName="ExportExcel" />
            </div>
        </td>
    </tr>
    <tr valign="top">
        <td colspan="2">
            <div>
                <telerik:RadSplitter ID="pdfSplitter" runat="server" Orientation="Horizontal" Height="600" Width="100%" VisibleDuringInit="false" OnClientLoad="showContentForIE">
                    <telerik:RadPane ID="pdfPane" runat="server" ShowContentDuringLoad="false"></telerik:RadPane>
                </telerik:RadSplitter>
            </div>
        </td>
    </tr>
</table>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var employeePositionStatusCodeValue = null;
        var organizationUnitIdValue = null;

        function showContentForIE(splt) {
            if ($telerik.isIE)
                splt._panes[0]._showContentDuringLoad = true;
        }

        function getReportPath(reportName, payrollProcessId, employeeNumber, reportType, employeePositionStatusCode, organizationUnitId, provinceCode, revision) {
            return getUrl(String.format('/Report/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}', reportName, payrollProcessId, employeeNumber, reportType, employeePositionStatusCode, organizationUnitId, provinceCode, revision));
        }

        function getParameters() {
            // Parameter 1
            var employeePositionStatusCodeValueHiddenControl = document.getElementById('EmployeePositionStatusCodeValue');
            if (employeePositionStatusCodeValueHiddenControl != null) {
                employeePositionStatusCodeValue = employeePositionStatusCodeValueHiddenControl.value;
            }
            if (employeePositionStatusCodeValue == "") {
                employeePositionStatusCodeValue = "x";
            }
            //alert("getParameters(): Parameter 1: employeePositionStatusCodeValue=" + employeePositionStatusCodeValue);

            // Parameter 2
            var organizationUnitIdValueHiddenControl = document.getElementById('OrganizationUnitIdValue');
            if (organizationUnitIdValueHiddenControl != null) {
                organizationUnitIdValue = organizationUnitIdValueHiddenControl.value;
            }
            if (organizationUnitIdValue == "") {
                organizationUnitIdValue = "x";
            }
            //alert("getParameters(): Parameter 2: organizationUnitIdValue=" + organizationUnitIdValue);
        }

        function downloadReport(reportName, payrollProcessId, employeeNumber, reportType, employeePositionStatusCode, organizationUnitId, provinceCode, revision) {
            var frame = document.createElement("frame");
            frame.src = getReportPath(reportName, payrollProcessId, employeeNumber, reportType, employeePositionStatusCode, organizationUnitId, provinceCode, revision);
            frame.style.display = "none";
            document.body.appendChild(frame);
        }

        function enableButtons(enable) {
            var exportPdfButton = $find("<%= btnExportPdf.ClientID %>");
            exportPdfButton.set_enabled(enable);

            var exportExcelButton = $find("<%= btnExportExcel.ClientID %>");
            exportExcelButton.set_enabled(enable);
        }

        function btnClear_Clicked(sender, eventArgs) {
            enableButtons(false);

            var pdfPane = $find("<%= pdfPane.ClientID %>");
            if (pdfPane._contentUrl != "")
                pdfPane.set_contentUrl("");
        }

        function btnViewReport_Clicked(sender, eventArgs) {
            getParameters();

            var pdfPane = $find("<%= pdfPane.ClientID %>");
            pdfPane.set_contentUrl(getReportPath("SalaryHourlyReport", -1, "x", "PDF", employeePositionStatusCodeValue, organizationUnitIdValue, "x", "x"));

            enableButtons(true);
        }

        function btnExportPdf_Clicked(sender, eventArgs) {
            getParameters();

            downloadReport("SalaryHourlyReport", -1, "x", "PDFDownload", employeePositionStatusCodeValue, organizationUnitIdValue, "x", "x");
        }

        function btnExportExcel_Clicked(sender, eventArgs) {
            getParameters();

            downloadReport("SalaryHourlyReport", -1, "x", "EXCEL", employeePositionStatusCodeValue, organizationUnitIdValue, "x", "x");
        }
    </script>
</telerik:RadScriptBlock>