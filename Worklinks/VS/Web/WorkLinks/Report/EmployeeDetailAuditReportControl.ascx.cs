﻿using System;
using WLP.Web.UI.Controls;

namespace WorkLinks.Report
{
    public partial class EmployeeDetailAuditReportControl : WLP.Web.UI.WLPUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EmployeeDetailAuditReport.ViewFlag);

            if (!IsPostBack)
                Panel1.DataBind();
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            StartDate.Value = null;
            EndDate.Value = null;
            OrganizationUnitDescription.Value = null;
            EmployeeNumber.Value = null;
            PaymentMethodCode.SelectedIndex = 0;
        }
        protected void StartDate_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            EndDate.MinDate = e.NewDate;
            EndDate.MaxDate = AddDays(e.NewDate, 365);
        }
        private DateTime? AddDays(DateTime? startDate, int numOfDays)
        {
            if (startDate != null && numOfDays > 0)
                return ((DateTime)startDate).AddDays(numOfDays);

            return null;
        }
    }
}