﻿using System;
using System.Linq;
using System.Web.Script.Serialization;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Report
{
    public partial class ReportControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected string ReportName { get { return (string)Page.RouteData.Values["reportName"]; } }
        private long PayrollProcessId { get { return Convert.ToInt64(Page.RouteData.Values["payrollProcessId"]); } }
        private string EmployeeNumber { get { return Convert.ToString(Page.RouteData.Values["employeeNumber"]); } }
        private string ReportType { get { return Convert.ToString(Page.RouteData.Values["reportType"]); } }
        private string Year { get { return Convert.ToString(Page.RouteData.Values["year"]); } }
        private string EmployerNumber { get { return Convert.ToString(Page.RouteData.Values["employerNumber"]); } }
        private string Province { get { return Convert.ToString(Page.RouteData.Values["provinceCode"]); } }
        private string RevisionNumber { get { return Convert.ToString(Page.RouteData.Values["revision"]); } }
        private string PageLanguage { get { return this.LanguageCode; } }
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }

        //remittance reports
        private string DetailType { get { return Convert.ToString(Page.RouteData.Values["detailType"]); } }
        private string RemitCode { get { return Convert.ToString(Page.RouteData.Values["remitCode"]); } }
        private string ExportId { get { return Convert.ToString(Page.RouteData.Values["exportId"]); } }
        private string RemitDate { get { return Convert.ToString(Page.RouteData.Values["remitDate"]); } }

        //HR reports
        private string StartDate { get { return Convert.ToString(Page.RouteData.Values["startDate"]); } }
        private string EndDate { get { return Convert.ToString(Page.RouteData.Values["endDate"]); } }
        private string OrganizationUnit { get { return Convert.ToString(Page.RouteData.Values["organizationUnit"]); } }
        private string PaymentMethodCode { get { return Convert.ToString(Page.RouteData.Values["paymentMethodCode"]); } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    //are users permitted to see the report being loaded?
                    ReportPermissions();

                    //load the report
                    byte[] pdfReport = GetPdfReport();

                    if (ReportType.Equals("PDF"))
                        Response.ContentType = "application/pdf";
                    else if (ReportType.Equals("PDFDownload"))
                    {
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-Disposition", string.Format("attachment;filename=\"{0}.pdf\"", ReportName));
                    }
                    else
                    {
                        Response.ContentType = "application/vnd.ms-excel";

                        if (PayrollProcessId > 0)
                            Response.AddHeader("Content-Disposition", string.Format("attachment;filename=\"{0}_{1}.xls\"", ReportName, PayrollProcessId));
                        else
                            Response.AddHeader("Content-Disposition", string.Format("attachment;filename=\"{0}.xls\"", ReportName));

                        Response.AddHeader("Content-Length", pdfReport.Length.ToString());
                    }

                    Response.OutputStream.Write(pdfReport, 0, pdfReport.Length);
                    Response.Flush();
                    Response.End();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        private byte[] GetPdfReport()
        {
            if (ReportName == "T4") //check if report is from the YE adjustments screen
            {
                System.Collections.Generic.List<YearEndT4> t4s = new System.Collections.Generic.List<YearEndT4> { new YearEndT4() { YearEndT4Id = Convert.ToInt64(PayrollProcessId.ToString().Remove(PayrollProcessId.ToString().Length - 1)) } };
                YearEndReportsCriteria criteria = new YearEndReportsCriteria()
                {
                    ReportName = ReportName,
                    ReportType = ReportType,
                    YearEndT4Ids = new JavaScriptSerializer().Serialize(t4s.Select(x => new { year_end_t4_id = x.YearEndT4Id }))
                };

                return Common.ServiceWrapper.ReportClient.GetYearEndReport(criteria);
            }
            else if (ReportName == "T4MassPrint")
            {
                YearEndT4Collection collection = Common.ServiceWrapper.HumanResourcesClient.GetYearEndT4(Convert.ToInt32(Year), null);
                YearEndReportsCriteria criteria = new YearEndReportsCriteria()
                {
                    ReportName = ReportName,
                    ReportType = ReportType,
                    YearEndT4Ids = new JavaScriptSerializer().Serialize(collection.Select(x => new { year_end_t4_id = x.YearEndT4Id })),
                    Year = Year
                };

                return Common.ServiceWrapper.ReportClient.GetYearEndReport(criteria);
            }
            else if (ReportName == "EmployeeDetailAuditReport") //check if report is from the HR->Reports Menu
            {
                HrReportsCriteria criteria = new HrReportsCriteria()
                {
                    ReportName = ReportName,
                    EmployeeNumber = EmployeeNumber,
                    ReportType = ReportType,
                    StartDate = StartDate,
                    EndDate = EndDate,
                    OrganizationUnit = OrganizationUnit,
                    PaymentMethodCode = PaymentMethodCode
                };

                return Common.ServiceWrapper.ReportClient.GetHRMenuReport(criteria);
            }
            else if (DetailType == string.Empty) //if not year end report, HR->Reports menu report item and not a remittance report, call the regular method for reports
                return Common.ServiceWrapper.ReportClient.GetReport(ReportName, PayrollProcessId, EmployeeNumber, ReportType.Equals("PDFDownload") ? ReportType.Remove(ReportType.IndexOf("Download")) : ReportType, Year, EmployerNumber, Province, Common.ApplicationParameter.ReportShowDescriptionField, RevisionNumber, Common.ApplicationParameter.ReportPayRegisterSortOrder, EmployeeId);
            else //if a remittance report, call a method with less args
                return Common.ServiceWrapper.ReportClient.GetRemittanceReport(ReportName, DetailType, RemitCode, ExportId, ReportType, RemitDate);
        }
        private void ReportPermissions()
        {
            if (ReportName.Equals("Changes"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterChangesReport.ViewFlag);
            else if (ReportName.Equals("EmployeePaySlip"))
                SetAuthorized(Common.Security.RoleForm.PayrollEmployeeSearchPaySlip.ViewFlag);
            else if (ReportName.Equals("CompensationList"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterCompensationList.ViewFlag);
            else if (ReportName.Equals("CurrentVsPrior"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterCurrentVsPrior.ViewFlag);
            else if (ReportName.Equals("CurrentVsPriorNet"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterCurrentVsPriorNet.ViewFlag);
            else if (ReportName.Equals("CurrentVsPriorPeriodVariances"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterCurrentVsPriorPeriodVariances.ViewFlag);
            else if (ReportName.Equals("CurrentVsPriorTwoPeriodVariances"))
                SetAuthorized(Common.Security.RoleForm.CurrentVsPriorTwoPeriodVariances.ViewFlag);
            else if (ReportName.Equals("Garnishment"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterGarnishment.ViewFlag);
            else if (ReportName.Equals("PayException"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterPayException.ViewFlag);
            else if (ReportName.Equals("GrossVsSalary"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterGrossVsSalary.ViewFlag);
            else if (ReportName.Equals("PD7AReport"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterPD7A.ViewFlag);
            else if (ReportName.Equals("YTDBonusCommish"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterYtdBonusCommission.ViewFlag);
            else if (ReportName.Equals("Pension"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterPensionReport.ViewFlag);
            else if (ReportName.Equals("PayrollPayRegisterMassPaySlipPrint"))
                SetAuthorized(Common.Security.RoleForm.PayrollPayRegisterMassPaySlipPrint.ViewFlag);
            else if (ReportName.Equals("T4"))
                SetAuthorized(Common.Security.RoleForm.T4Report.ViewFlag);
            else if (ReportName.Equals("T4SummaryMassPrint"))
                SetAuthorized(Common.Security.RoleForm.T4SummaryMassReportPrint.ViewFlag);
            else if (ReportName.Equals("NR4SummaryMassPrint"))
                SetAuthorized(Common.Security.RoleForm.NR4SummaryMassReportPrint.ViewFlag);
            else if (ReportName.Equals("T4A"))
                SetAuthorized(Common.Security.RoleForm.T4AReport.ViewFlag);
            else if (ReportName.Equals("R1"))
                SetAuthorized(Common.Security.RoleForm.R1Report.ViewFlag);
            else if (ReportName.Equals("R2"))
                SetAuthorized(Common.Security.RoleForm.R2Report.ViewFlag);
            else if (ReportName.Equals("NR4"))
                SetAuthorized(Common.Security.RoleForm.NR4Report.ViewFlag);
            else if (ReportName.Equals("T4RSP"))
                SetAuthorized(Common.Security.RoleForm.T4RSPReport.ViewFlag);
            else if (ReportName.Equals("T4ARCA"))
                SetAuthorized(Common.Security.RoleForm.T4ARCAReport.ViewFlag);
            else if (ReportName.Equals("T4MassPrint"))
                SetAuthorized(Common.Security.RoleForm.T4MassReportPrint.ViewFlag);
            else if (ReportName.Equals("T4RSPMassPrint"))
                SetAuthorized(Common.Security.RoleForm.T4RSPMassReportPrint.ViewFlag);
            else if (ReportName.Equals("T4AMassPrint"))
                SetAuthorized(Common.Security.RoleForm.T4AMassReportPrint.ViewFlag);
            else if (ReportName.Equals("T4ARCAMassPrint"))
                SetAuthorized(Common.Security.RoleForm.T4ARCAMassReportPrint.ViewFlag);
            else if (ReportName.Equals("NR4MassPrint"))
                SetAuthorized(Common.Security.RoleForm.NR4MassReportPrint.ViewFlag);
            else if (ReportName.Equals("R1MassPrint"))
                SetAuthorized(Common.Security.RoleForm.R1MassReportPrint.ViewFlag);
            else if (ReportName.Equals("R2MassPrint"))
                SetAuthorized(Common.Security.RoleForm.R2MassReportPrint.ViewFlag);
            else if (ReportName.Equals("NameAddressException"))
                SetAuthorized(Common.Security.RoleForm.NameAddressException.ViewFlag);
            else if (ReportName.Equals("YearEndWCBReport"))
                SetAuthorized(Common.Security.RoleForm.YearEndWCBReport.ViewFlag);
            else if (ReportName.Equals("YearEndPaycodeSummaryReport"))
                SetAuthorized(Common.Security.RoleForm.YearEndPaycodeSummaryReport.ViewFlag);
            else if (ReportName.Equals("R1SummaryOfSourceDeductions"))
                SetAuthorized(Common.Security.RoleForm.R1SummaryOfSourceDeductions.ViewFlag);
            else if (ReportName.Equals("R2Summary"))
                SetAuthorized(Common.Security.RoleForm.R2Summary.ViewFlag);
            else if (ReportName.Equals("T4ASummaryMassPrint"))
                SetAuthorized(Common.Security.RoleForm.T4ASummaryMassReportPrint.ViewFlag);
            else if (ReportName.Equals("YeAdjustAudit"))
                SetAuthorized(Common.Security.RoleForm.YearEndAdjustmentsAudit.ViewFlag);
            else if (ReportName.Equals("MassYeAdjustAudit"))
                SetAuthorized(Common.Security.RoleForm.YearEndAdjustmentsAudit.ViewFlag);
            else if (ReportName.Equals("StatDeductionExceptionReport"))
                SetAuthorized(Common.Security.RoleForm.StatDeductionException.ViewFlag);
            else if (ReportName.Equals("YearEndHealthTaxSummary"))
                SetAuthorized(Common.Security.RoleForm.YearEndHealthTaxSummary.ViewFlag);
            else if (ReportName.Equals("WCB"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterWCB.ViewFlag);
            else if (ReportName.Equals("GL"))
                SetAuthorized(Common.Security.RoleForm.GLButton.ViewFlag);
            else if (ReportName.Equals("EmployeeSalaryReport"))
                SetAuthorized(Common.Security.RoleForm.EmployeeSalaryReport.ViewFlag);
            else if (ReportName.Equals("UnitedWayReport"))
                SetAuthorized(Common.Security.RoleForm.UnitedWayReport.ViewFlag);
            else if (ReportName.Equals("SerpYtdReport"))
                SetAuthorized(Common.Security.RoleForm.SerpYtdReport.ViewFlag);
            else if (ReportName.Equals("PayrollDetailsReport"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterPayDetails.ViewFlag);
            else if (ReportName.Equals("PayrollDetailsOrgUnit"))
                SetAuthorized(Common.Security.RoleForm.PayrollDetailsOrgUnit.ViewFlag);
            else if (ReportName.Equals("GLEmployeeDetailsReport"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterGLDetails.ViewFlag);
            else if (ReportName.Equals("PaySlipMailingLabel"))
                SetAuthorized(Common.Security.RoleForm.PayrollEmployeeSearchPaySlipMailingFlag.ViewFlag);
            else if (ReportName.Equals("CSB"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterCSB.ViewFlag);
            else if (ReportName.Equals("FedProvRemitSummaryReport"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterFedProvRemitSummary.ViewFlag);
            else if (ReportName.Equals("ProvincialHealthSummary"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterProvHealthSummary.ViewFlag);
            else if (ReportName.Equals("QuebecRemitSummary"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterQuebecRemitSummary.ViewFlag);
            else if (ReportName.Equals("PreYET4ADetail"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterPreYET4ADetail.ViewFlag);
            else if (ReportName.Equals("PreYET4Detail"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterPreYET4Detail.ViewFlag);
            else if (ReportName.Equals("PreYER1Detail"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterPreYER1Detail.ViewFlag);
            else if (ReportName.Equals("PreYearEndPIER"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterPIERPreYE.ViewFlag);
            else if (ReportName.Equals("BenefitArrearsReport"))
                SetAuthorized(Common.Security.RoleForm.PayrollRegisterBenefitArrears.ViewFlag);
            else if (ReportName.Equals("YearEndT4Detail"))
                SetAuthorized(Common.Security.RoleForm.YearEndT4Detail.ViewFlag);
            else if (ReportName.Equals("YearEndT4ADetail"))
                SetAuthorized(Common.Security.RoleForm.YearEndT4ADetail.ViewFlag);
            else if (ReportName.Equals("YearEndR1Detail"))
                SetAuthorized(Common.Security.RoleForm.YearEndR1Detail.ViewFlag);
            else if (ReportName.Equals("YearEndR2Detail"))
                SetAuthorized(Common.Security.RoleForm.YearEndR2Detail.ViewFlag);
            else if (ReportName.Equals("ResearchAndDevelopment"))
                SetAuthorized(Common.Security.RoleForm.ResearchAndDevelopment.ViewFlag);
            else if (ReportName.Equals("YearEndAdjustmentReport"))
                SetAuthorized(Common.Security.RoleForm.YearEndAdjustmentReport.ViewFlag);
            else if (ReportName.Equals("UserAccessInformation"))
                SetAuthorized(Common.Security.RoleForm.UserAccessInformationReport.ViewFlag);
            else if (ReportName.Equals("WageTypeCatalog"))
                SetAuthorized(Common.Security.RoleForm.WageTypeCatalogReport.ViewFlag);
            else if (ReportName.Equals("PensionDemographicsReport"))
                SetAuthorized(Common.Security.RoleForm.PensionDemographicsReport.ViewFlag);
            else if (ReportName.Equals("SrpReport"))
                SetAuthorized(Common.Security.RoleForm.SrpReport.ViewFlag);
            else if (ReportName.Equals("GrossUpReport"))
                SetAuthorized(Common.Security.RoleForm.GrossUpReport.ViewFlag);
            else if (ReportName.Equals("PreYearEndDetail"))
                SetAuthorized(Common.Security.RoleForm.PreYearEndDetail.ViewFlag);
            else if (ReportName.Equals("EmployeeAnniversaryListing"))
                SetAuthorized(Common.Security.RoleForm.EmployeeAnniversaryListing.ViewFlag);
            else if (ReportName.Equals("EmployeeEmergencyContacts"))
                SetAuthorized(Common.Security.RoleForm.EmployeeEmergencyContacts.ViewFlag);
            else if (ReportName.Equals("EmployeeListingReport"))
                SetAuthorized(Common.Security.RoleForm.EmployeeListingReport.ViewFlag);
            else if (ReportName.Equals("SkillsCertificateLicenseSample"))
                SetAuthorized(Common.Security.RoleForm.SkillsCertificateLicenseSample.ViewFlag);
            else if (ReportName.Equals("StatHoliday"))
                SetAuthorized(Common.Security.RoleForm.StatHoliday.ViewFlag);
            else if (ReportName.Equals("PaycodeReport"))
                SetAuthorized(Common.Security.RoleForm.PaycodeReport.ViewFlag);
            else if (ReportName.Equals("PaymentList"))
                SetAuthorized(Common.Security.RoleForm.PaymentListReport.ViewFlag);
            else if (ReportName.Equals("EmployeePhoneList"))
                SetAuthorized(Common.Security.RoleForm.EmployeePhoneList.ViewFlag);
            else if (ReportName.Equals("EmployeeBirthdayList"))
                SetAuthorized(Common.Security.RoleForm.EmployeeBirthdayList.ViewFlag);
            else if (ReportName.Equals("EmployeeNameAndAddressListing"))
                SetAuthorized(Common.Security.RoleForm.EmployeeNameAndAddressListing.ViewFlag);
            else if (ReportName.Equals("BarbadosDeductionSummary"))
                SetAuthorized(Common.Security.RoleForm.BarbadosDeductionSummary.ViewFlag);
            else if (ReportName.Equals("StLuciaDeductionSummary"))
                SetAuthorized(Common.Security.RoleForm.StLuciaDeductionSummary.ViewFlag);
            else if (ReportName.Equals("TrinidadDeductionSummary"))
                SetAuthorized(Common.Security.RoleForm.TrinidadDeductionSummary.ViewFlag);
            else if (ReportName.Equals("JamaicaDeductionSummary"))
                SetAuthorized(Common.Security.RoleForm.JamaicaDeductionSummary.ViewFlag);
            else if (ReportName.Equals("JamaicaGovMonthlyRemittance"))
                SetAuthorized(Common.Security.RoleForm.JamaicaGovernmentMonthlyRemittance.ViewFlag);
            else if (ReportName.Equals("AccidentDangerousOccurrence"))
                SetAuthorized(Common.Security.RoleForm.EmployeeWsibHealthAndSafetyReport.ViewFlag);
            else if (ReportName.Equals("TerminatedEmployees"))
                SetAuthorized(Common.Security.RoleForm.TerminatedEmployees.ViewFlag);
            else if (ReportName.Equals("SalaryHourlyReport"))
                SetAuthorized(Common.Security.RoleForm.SalaryHourlyReport.ViewFlag);
            else if (ReportName.Equals("StatusCodeReport"))
                SetAuthorized(Common.Security.RoleForm.StatusCodeReport.ViewFlag);
            else if (ReportName.Equals("VacationReport"))
                SetAuthorized(Common.Security.RoleForm.VacationReport.ViewFlag);
            else if (ReportName.Equals("AttritionReport"))
                SetAuthorized(Common.Security.RoleForm.AttritionReport.ViewFlag);
            else if (ReportName.Equals("CraGarnishmentDetailsReport") || ReportName.Equals("RqSourceDeductions") || ReportName.Equals("ChequeHealthTaxRemit") || ReportName.Equals("CraWcbRemitDetailsReport") || ReportName.Equals("ChequeWcbRemitDetailsReport") || ReportName.Equals("CraSourceDeductions"))
                SetAuthorized(Common.Security.RoleForm.Remittance.ViewFlag);
            else if (ReportName.Equals("CustomerInvoice"))
                SetAuthorized(Common.Security.RoleForm.InvoiceCanada.ViewFlag);
            else if (ReportName.Equals("EftCustomerInvoiceJamaica"))
                SetAuthorized(Common.Security.RoleForm.InvoiceJamaica.ViewFlag);
            else if (ReportName.Equals("PaycodeYearEndMapping"))
                SetAuthorized(Common.Security.RoleForm.PaycodeYearEndMappingReport.ViewFlag);
            else if (ReportName.Equals("EmployeeDetailAuditReport"))
                SetAuthorized(Common.Security.RoleForm.EmployeeDetailAuditReport.ViewFlag);
            else if (ReportName.Equals("ProbationReport"))
                SetAuthorized(Common.Security.RoleForm.ProbationReport.ViewFlag);
            else if (ReportName.Equals("HrChangesAuditReport"))
                SetAuthorized(Common.Security.RoleForm.HrChangesAuditReport.ViewFlag);
            else if (ReportName.Equals("SkillReport"))
                SetAuthorized(Common.Security.RoleForm.SkillReport.ViewFlag);
            else if (ReportName.Equals("NationalHousingTrust"))
                SetAuthorized(Common.Security.RoleForm.PayrollEmployeeSearchNHT.ViewFlag);
            else if (ReportName.Equals("NonStatDeductions"))
                SetAuthorized(Common.Security.RoleForm.NonStatDeductions.ViewFlag);
            else if (ReportName.Equals("MonthlyStatutoryRemittance"))
                SetAuthorized(Common.Security.RoleForm.MonthlyStatutoryRemittance.ViewFlag);
            else if (ReportName.Equals("P45"))
                SetAuthorized(Common.Security.RoleForm.P45Report.ViewFlag);
            else if (ReportName.Equals("AccumHoursSummaryRateProgression"))
                SetAuthorized(Common.Security.RoleForm.AccumulatedHoursReportMenuItem.ViewFlag);
        }
        #endregion
    }
}