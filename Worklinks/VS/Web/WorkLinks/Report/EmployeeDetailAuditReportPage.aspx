﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeeDetailAuditReportPage.aspx.cs" Inherits="WorkLinks.Report.EmployeeDetailAuditReportPage" %>
<%@ Register Src="EmployeeDetailAuditReportControl.ascx" TagName="EmployeeDetailAuditReportControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:EmployeeDetailAuditReportControl ID="EmployeeDetailAuditReportControl1" SearchLocation="Report" runat="server" />        
    </ContentTemplate>
</asp:Content>