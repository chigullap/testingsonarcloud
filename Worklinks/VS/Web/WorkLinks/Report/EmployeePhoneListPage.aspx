﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeePhoneListPage.aspx.cs" Inherits="WorkLinks.Report.EmployeePhoneListPage" %>
<%@ Register Src="EmployeePhoneListControl.ascx" TagName="EmployeePhoneListControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:EmployeePhoneListControl ID="EmployeePhoneListControl1" SearchLocation="Report" runat="server" />        
    </ContentTemplate>
</asp:Content>