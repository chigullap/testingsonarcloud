﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeeBirthdayListPage.aspx.cs" Inherits="WorkLinks.Report.EmployeeBirthdayListPage" %>
<%@ Register Src="EmployeeBirthdayListControl.ascx" TagName="EmployeeBirthdayListControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:EmployeeBirthdayListControl ID="EmployeeBirthdayListControl1" SearchLocation="Report" runat="server" />        
    </ContentTemplate>
</asp:Content>