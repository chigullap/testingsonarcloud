﻿using System;

namespace WorkLinks.Report
{
    public partial class StatHoliday : WLP.Web.UI.WLPUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.StatHoliday.ViewFlag);
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Date.Value = null;
        }
    }
}