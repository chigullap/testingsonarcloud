﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HrChangesAuditReportPage.aspx.cs" Inherits="WorkLinks.Report.HrChangesAuditReportPage" %>
<%@ Register Src="HrChangesAuditReportControl.ascx" TagName="HrChangesAuditReportControl" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:HrChangesAuditReportControl ID="HrChangesAuditReportControl1" SearchLocation="Report" runat="server" />        
    </ContentTemplate>
</asp:Content>
