﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Common.Security;

namespace WorkLinks
{
    public partial class SecurityUserGroupControl : WLPUserControl
    {
        #region fields
        #endregion

        #region properties
        private WorkLinksMembershipUser User
        {
            get
            {
                return (WorkLinksMembershipUser)((SecurityUserProfile)Profile).User;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ProfileGroup.DataBind();
            }
        }
        public void DestroyGroupCache()
        {
        }
        protected void ProfileGroup_DataBinding(object sender, EventArgs e)
        {
            CodeCollection codes = new CodeCollection();

            if (User==null || User.Groups==null)
                this.Response.Redirect("~/UnauthorizedPage.aspx");

            foreach (GroupDescription group in User.Groups)
            {
                codes.Add(new CodeObject() { Code = group.Key, Description = LanguageCode.ToLower().Equals("en") ? group.EnglishDesc : group.FrenchDesc });
            }

            ((WLP.Web.UI.Controls.WLPComboBox)sender).DataSource = codes;

            ((WLP.Web.UI.Controls.WLPComboBox)sender).SelectedValue = User.GetPrimarySecurityGroup().Key;
        }
        protected void ProfileGroup_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            Profile.SecurityGroupId = Convert.ToInt64(e.Value);
            ((WorkLinksMembershipUser)((SecurityUserProfile)Profile).User).SetPrimarySecurityGroup(Profile.SecurityGroupId);
            Profile.ModifiedFlag = true;
            Response.Redirect("~/");
        }
        #endregion
    }
}