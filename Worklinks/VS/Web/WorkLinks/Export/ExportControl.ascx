﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExportControl.ascx.cs" Inherits="WorkLinks.Export.Export" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<style>
    ul.override {  
        display: block;
        list-style-type: disc;
        margin: 1em 1em 0 1em;
        padding-left: 40px;
    }
</style>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPToolBar ID="RealExportViewToolBar" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="RealExportViewToolBar_ButtonClick">
        <Items>
            <wasp:WLPToolBarButton Text="Submit" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="submit" ResourceName="submit"></wasp:WLPToolBarButton>
            <wasp:WLPToolBarButton Text="Cancel" onclick="closeAndReturn()" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" ResourceName="Cancel" CausesValidation="false"></wasp:WLPToolBarButton>
        </Items>
    </wasp:WLPToolBar>

    <table width="100%">
        <tr>
            <td><asp:Literal runat="server" Text="<%$ Resources:PageContent, ExportMsg %>" /><br />
                <ul class="override">
                    <li id="exportItem1" runat="server">
                        <asp:Literal runat="server" Text="<%$ Resources:PageContent, ExportControlLine1 %>"/>
                    </li>
                    <li id="exportItem2" runat="server">
                        <asp:Literal runat="server" Text="<%$ Resources:PageContent, ExportControlLine2 %>"/>
                    </li>
                    <li id="exportItem3" runat="server">
                        <asp:Literal runat="server" Text="<%$ Resources:PageContent, ExportControlLine3 %>"/>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</asp:Panel>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function getRadWindow() {
            var popup = null;

            if (window.radWindow)
                popup = window.radWindow;
            else if (window.frameElement.radWindow)
                popup = window.frameElement.radWindow;

            return popup;
        }

        function closeAndReturn() {
            var popup = getRadWindow();
            setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
        }
    </script>
</telerik:RadScriptBlock>