﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Export
{
    public partial class Export : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected String ExportName { get { return (String)Page.RouteData.Values["exportName"]; } }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    //load the export
                    byte[] export = null;
                    String fileName = null;

                    if (ExportName != "ALL")
                    {
                        if (ExportName == "LaborLevel")
                        {
                            //load the labor level export
                            export = Common.ServiceWrapper.HumanResourcesClient.GenerateREALLaborLevelExportFile() ?? new byte[0];

                            //filename stored in code system
                            fileName = String.Format(Common.ApplicationParameter.LaborLevelExportFileName, DateTime.Now);
                        }
                        else if (ExportName == "Employee")
                        {
                            //load the labor level export
                            export = Common.ServiceWrapper.HumanResourcesClient.GenerateREALEmployeeExportFile() ?? new byte[0];

                            //filename stored in code system
                            fileName = String.Format(Common.ApplicationParameter.EmployeeExportFileName, DateTime.Now);
                        }
                        else if (ExportName == "AdjustmentRule")
                        {
                            //load the adjustment rule export
                            export = Common.ServiceWrapper.HumanResourcesClient.GenerateREALAdjustmentRuleExportFile() ?? new byte[0];

                            //filename stored in code system
                            fileName = String.Format(Common.ApplicationParameter.AdjustmentRuleExportFileName, DateTime.Now);
                        }

                        //if an export was created, send it to the screen
                        if (export != null && fileName != null)
                        {
                            Response.Clear();

                            //this will prompt the user to open or download the file
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", fileName));
                            Response.AddHeader("Content-Length", export.Length.ToString());

                            Response.OutputStream.Write(export, 0, export.Length);
                            Response.Flush();
                            Response.End();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        protected void RealExportViewToolBar_ButtonClick(object sender, Telerik.Web.UI.RadToolBarEventArgs e)
        {
            if (((Telerik.Web.UI.RadToolBarButton)(e.Item)).CommandName.Equals("submit"))
            {
                try
                {
                    //go to business logic layer
                    Common.ServiceWrapper.HumanResourcesClient.CreateAndExportRealEmployeeFiles(
                            Common.ApplicationParameter.RealFtp, 
                            Common.ApplicationParameter.LaborLevelExportFileName,
                            Common.ApplicationParameter.EmployeeExportFileName,
                            Common.ApplicationParameter.AdjustmentRuleExportFileName);

                    //close the RadWindow
                    ScriptManager.RegisterStartupScript(Page, GetType(), "close and return", "closeAndReturn();", true);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error creating or sending the export files");
                }
            }
        }
    }
}
