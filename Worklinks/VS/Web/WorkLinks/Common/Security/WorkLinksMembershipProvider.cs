﻿using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Common.Security
{
    public class WorkLinksMembershipProvider : MembershipProvider
    {
        #region private vars
        private int _minRequiredPasswordLength = 0;
        private bool _enablePasswordRetrieval = false;
        private bool _enablePasswordReset = true;
        private bool _requiresQuestionAndAnswer = false;
        private bool _requiresUniqueEmail = false;
        private int _maxInvalidPasswordAttempts = 10;
        private int _minRequiredNonalphanumericCharacters = 0;
        private int _passwordAttemptWindow = 10;
        private const String _profileKey = "Profile";

        #endregion

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        protected static SecurityUserProfile Profile
        {
            get
            {
                if (HttpContext.Current.Profile != null)
                    return (SecurityUserProfile)HttpContext.Current.Profile[_profileKey];
                else
                    return null;
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword) //NOTE: oldPassword is not used, it is validated on the control before we get to this point.
        {
            SecurityUserCollection users = new SecurityUserCollection();
            users.Load(ServiceWrapper.SecurityClient.GetSecurityUser(username));
            if (users.Count == 1)
            {
                ServiceWrapper.SecurityClient.UpdateSecurityUserPassword(username, newPassword, users[0], ApplicationParameter.PasswordExpiryDays, ApplicationParameter.LoginExpiryPasswordChangeHours);
                Profile.User = null;
                return true;
            }
            else
                return false;
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get
            {
                return _enablePasswordReset;
            }
        }

        public override bool EnablePasswordRetrieval
        {
            get
            {
                return _enablePasswordRetrieval;
            }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {

            WorkLinksMembershipUser user = (WorkLinksMembershipUser)Profile.User;

            if (user == null)
            {
                SecurityUserCollection users = ServiceWrapper.SecurityClient.GetSecurityUser(username);
                if (users.Count == 1)
                {
                    user = GetMembershipUser(users[0]);
                    user.Groups = ServiceWrapper.SecurityClient.GetUserGroups(new DatabaseUser() { DatabaseName = ApplicationParameter.DatabaseName }, user.SecurityUserId, true, false);
                    user.SetPrimarySecurityGroup(Profile.SecurityGroupId);

                    Profile.User = user;
                    //Profile.UserIdentifier = Convert.ToString(user.SecurityUserId);
                }
            }

            return user;
        }

        private WorkLinksMembershipUser GetMembershipUser(SecurityUser user)
        {
            return new WorkLinksMembershipUser(user);
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get
            {
                return _maxInvalidPasswordAttempts;
            }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get
            {
                return _minRequiredNonalphanumericCharacters;
            }
        }

        public override int MinRequiredPasswordLength
        {
            get
            {
                return _minRequiredPasswordLength;
            }
        }

        /// <summary>
        /// The number of minutes in which a maximum number of invalid password or password answer attempts are allowed before the membership user is locked out. 
        /// </summary>
        public override int PasswordAttemptWindow
        {
            get
            {
                return _passwordAttemptWindow;
            }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get
            {
                return MembershipPasswordFormat.Hashed;
            }
        }

        public override string PasswordStrengthRegularExpression
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get
            {
                return _requiresQuestionAndAnswer;
            }
        }

        public override bool RequiresUniqueEmail
        {
            get
            {
                return _requiresUniqueEmail;
            }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string username, string password)
        {
            SecurityUserLoginHistory history = new SecurityUserLoginHistory();
            history.UserName = username;
            history.UpdateUser = username;
            history.LoginAttemptDatetime = DateTime.Now;
            history.IpAddress = HttpContext.Current.Request.UserHostAddress;
            history.Browser = ((HttpBrowserCapabilities)HttpContext.Current.Request.Browser).Browser;
            history.BrowserVersion = ((HttpBrowserCapabilities)HttpContext.Current.Request.Browser).Version;
            history.Platform = ((HttpBrowserCapabilities)HttpContext.Current.Request.Browser).Platform;

            if (HttpContext.Current.Request.UserAgent.IndexOf("Windows NT 5.1") > 0)
                history.Version = "XP";
            else if (HttpContext.Current.Request.UserAgent.IndexOf("Windows NT 6.0") > 0)
                history.Version = "VISTA";
            else if (HttpContext.Current.Request.UserAgent.IndexOf("Windows NT 6.1") > 0)
                history.Version = "7";
            else if (HttpContext.Current.Request.UserAgent.IndexOf("Windows NT 6.2") > 0)
                history.Version = "8";
            else if (HttpContext.Current.Request.UserAgent.IndexOf("Windows NT 6.3") > 0)
                history.Version = "8.1";
            else if (HttpContext.Current.Request.UserAgent.IndexOf("Windows NT 10") > 0)
                history.Version = "10";

            history.Resolution = ((System.Web.UI.WebControls.HiddenField)((System.Web.UI.Page)HttpContext.Current.Handler).Controls[2].Controls[5]).Value;

            return ServiceWrapper.SecurityClient.ValidateUserWithHistory(history, password, ApplicationParameter.PasswordAttemptWindow, ApplicationParameter.MaxInvalidPasswordAttempts, ApplicationParameter.LoginExpiryDays);
        }

        public override void Initialize(string name, NameValueCollection config)
        {
            //initialize abstract class then grab values
            base.Initialize(name, config);

            //get value from value in code_system table
            _minRequiredPasswordLength = Convert.ToInt32(ApplicationParameter.PasswordMinLength);

            //get values from web.config
            _enablePasswordRetrieval = Convert.ToBoolean(config["enablePasswordRetrieval"]);
            _enablePasswordReset = Convert.ToBoolean(config["enablePasswordReset"]);
            _requiresQuestionAndAnswer = Convert.ToBoolean(config["requiresQuestionAndAnswer"]);
            _requiresUniqueEmail = Convert.ToBoolean(config["requiresUniqueEmail"]);
            _maxInvalidPasswordAttempts = ApplicationParameter.MaxInvalidPasswordAttempts;
            _minRequiredNonalphanumericCharacters = Convert.ToInt32(config["minRequiredNonalphanumericCharacters"]);
            _passwordAttemptWindow = ApplicationParameter.PasswordAttemptWindow;
        }
    }
}