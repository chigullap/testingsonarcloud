﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;


namespace WorkLinks.Common.Security
{
    public class WorkLinksMembershipUser : System.Web.Security.MembershipUser
    {

        #region fields
        private SecurityGroup _primaryGroup = null;

        #endregion

        public long SecurityUserId
        {
            get { return Convert.ToInt64(ProviderUserKey); }
        }

        public DateTime? PasswordExpiryDatetime {get;set;}
        public bool IsPasswordExpired
        {
            get 
            {
                if (PasswordExpiryDatetime == null)
                    return false;
                else
                    return (DateTime.Now - ((DateTime)PasswordExpiryDatetime)).Ticks > 0;
            }
        }

        public GroupDescriptionCollection Groups { get; set; }

        public WorkLinksMembershipUser(SecurityUser user) :
            base("WorkLinksMembershipProvider",
                    user.UserName,
                    user.Key,
                    user.Email,
                    null,
                    null,
                    true,
                    user.IsLockedOutFlag,
                    user.CreateDatetime ?? DateTime.MinValue,
                    user.LastLoginDate ?? DateTime.MinValue,
                    user.LastActivityDatetime ?? DateTime.MinValue,
                    user.LastPasswordChangedDatetime ?? DateTime.MinValue,
                    user.LastLockoutDatetime ?? DateTime.MinValue)
        {
            PasswordExpiryDatetime=user.PasswordExpiryDatetime;
        }

        #region main
        public SecurityGroup GetPrimarySecurityGroup()
        {
            if (_primaryGroup == null && Groups != null && Groups.Count > 0)
                _primaryGroup = Groups[0];
            return _primaryGroup;
        }
        public void SetPrimarySecurityGroup(long groupId)
        {
            if (Groups != null && Groups[groupId.ToString()] != null)
            {
                _primaryGroup = Groups[groupId.ToString()];
            }
            

        }


        #endregion
    }
}