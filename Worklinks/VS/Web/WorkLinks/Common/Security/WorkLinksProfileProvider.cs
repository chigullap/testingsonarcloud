﻿using System;
using System.Configuration;
using System.Security.Claims;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Common.Security
{
    public class WorkLinksProfileProvider : ProfileProvider
    {
        private static String _profileKey = "WorkLinksProfileProvider._profileKey";
        private DateTime _datetime = DateTime.Now;

        private System.Web.SessionState.HttpSessionState Session { get { return HttpContext.Current.Session; } }

        private SecurityUserProfile Profile
        {
            get
            {
                //is profile in session
                if (Session[_profileKey] == null)
                {

                    //String userName = ((ClaimsIdentity)System.Web.HttpContext.Current.User.Identity).FindFirst("uname").Value; //yuc
                    String userName = System.Web.HttpContext.Current.User.Identity.Name.ToString();
                    //get the old cached profile
                    SecurityUserProfileCollection collection = ServiceWrapper.SecurityClient.GetSecurityUserProfile(ApplicationParameter.DatabaseName, userName);
                    SecurityUserProfile profile = null;
                    if (collection.Count > 0)
                        profile = collection[0];

                    //not a known profile, or cached profile missing
                    if (profile == null)
                    {
                                                profile = new SecurityUserProfile();
                                                profile.Initialize();
                                                profile.UpdateUser = userName;
                                                profile.UpdateDatetime = DateTime.Now;
                    }
                    else
                    {
                        //user user
                        SecurityUserCollection users = ServiceWrapper.SecurityClient.GetSecurityUser(userName);
                        if (users.Count == 1)
                        {
                            WorkLinksMembershipUser user = new WorkLinksMembershipUser(users[0]);
                            user.Groups = ServiceWrapper.SecurityClient.GetUserGroups(new DatabaseUser() { DatabaseName = ApplicationParameter.DatabaseName }, user.SecurityUserId, true, false);
                            user.SetPrimarySecurityGroup(profile.SecurityGroupId);

                            profile.User = user;
                            //profile.UserIdentifier = Convert.ToString(user.SecurityUserId);
                        }
                    }

                    //set profile date format from code system
//                    profile.ShortDateFormat = ApplicationParameter.ShortDateFormat;
//                    profile.LongDateFormat = ApplicationParameter.LongDateFormat;

                    Session.Add(_profileKey, profile);

                }

                return (SecurityUserProfile)Session[_profileKey];
            }
        }

        public override int DeleteInactiveProfiles(ProfileAuthenticationOption authenticationOption, DateTime userInactiveSinceDate)
        {
            throw new NotImplementedException();
        }

        public override int DeleteProfiles(string[] usernames)
        {
            throw new NotImplementedException();
        }

        public override int DeleteProfiles(ProfileInfoCollection profiles)
        {
            throw new NotImplementedException();
        }

        public override ProfileInfoCollection FindInactiveProfilesByUserName(ProfileAuthenticationOption authenticationOption, string usernameToMatch, DateTime userInactiveSinceDate, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override ProfileInfoCollection FindProfilesByUserName(ProfileAuthenticationOption authenticationOption, string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override ProfileInfoCollection GetAllInactiveProfiles(ProfileAuthenticationOption authenticationOption, DateTime userInactiveSinceDate, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override ProfileInfoCollection GetAllProfiles(ProfileAuthenticationOption authenticationOption, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfInactiveProfiles(ProfileAuthenticationOption authenticationOption, DateTime userInactiveSinceDate)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection collection)
        {
            bool isAuthenticated = (bool)context["IsAuthenticated"];
            //String userName = ((ClaimsIdentity)System.Web.HttpContext.Current.User.Identity).FindFirst("uname").Value;//yuc
            String userName = System.Web.HttpContext.Current.User.Identity.Name.ToString();

            SettingsPropertyValueCollection values = new SettingsPropertyValueCollection();
            foreach (SettingsProperty property in collection)
            {
                SettingsPropertyValue value = new SettingsPropertyValue(property);

                if (isAuthenticated)
                    value.PropertyValue = Profile;
                else
                {
                    SecurityUserProfile profile = new SecurityUserProfile();
                    profile.Initialize();
                    value.PropertyValue = profile;
                }

                values.Add(value);
            }

            return values;
        }

        public override void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection collection)
        {
            bool isAuthenticated = (bool)context["IsAuthenticated"];

            if (isAuthenticated)
            {
                SecurityUserProfile profile = (SecurityUserProfile)collection["Profile"].PropertyValue;

                if (profile.ModifiedFlag)
                {
                    ServiceWrapper.SecurityClient.UpdateSecurityUserProfile(profile);
                    profile.ModifiedFlag = false;
                    if (Session != null)
                        Session[_profileKey] = null;
                }
            }
        }
    }
}