﻿using System;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Common.Security
{
    public static class RoleForm
    {
        #region fields
        private static System.Collections.Generic.Dictionary<long, FormSecurityCollection> _roleForms = new System.Collections.Generic.Dictionary<long, FormSecurityCollection>();
        #endregion

        #region properties
        private static System.Web.Profile.ProfileBase Profile { get { return System.Web.HttpContext.Current.Profile; } }
        private static long CurrentGuiRoleId { get { return RoleResourceCollection.CurrentGuiSecurityRoleId; } }

        public static FormSecurity P45Report { get { return GetFormsInRole(CurrentGuiRoleId, "P45_REPORT"); } } //where do I put this?

        #region home
        //widgets
        public static FormSecurity WelcomeWidget { get { return GetFormsInRole(CurrentGuiRoleId, "WIDGET_WELCOME"); } }
        public static FormSecurity WelcomeWidgetEditProfile { get { return GetFormsInRole(CurrentGuiRoleId, "WLC_EDITPF"); } }
        public static FormSecurity WelcomeWidgetEditContacts { get { return GetFormsInRole(CurrentGuiRoleId, "WLC_EDITCON"); } }
        public static FormSecurity VacationSickTimeWidget { get { return GetFormsInRole(CurrentGuiRoleId, "WIDGET_VSICKTIME"); } }
        public static FormSecurity EmployeeTimeReviewWidget { get { return GetFormsInRole(CurrentGuiRoleId, "WIDGET_TIMEREV"); } }
        public static FormSecurity EmployeePayslipWidget { get { return GetFormsInRole(CurrentGuiRoleId, "WIDGET_EMP_SLIP"); } }
        public static FormSecurity EmployeeListWidget { get { return GetFormsInRole(CurrentGuiRoleId, "WIDGET_EMP_LIST"); } }
        public static FormSecurity HeadcountWidget { get { return GetFormsInRole(CurrentGuiRoleId, "WIDGET_HEADCOUNT"); } }
        public static FormSecurity PayrollBreakdownWidget { get { return GetFormsInRole(CurrentGuiRoleId, "WID_PAYBREAKDOWN"); } }
        public static FormSecurity UserManagementWidget { get { return GetFormsInRole(CurrentGuiRoleId, "WID_USER_MANAGE"); } }
        public static FormSecurity LabourCostWidget { get { return GetFormsInRole(CurrentGuiRoleId, "WIDGET_LAB_COST"); } }
        #endregion

        #region human resources
        public static bool HumanResources { get { return EmployeeSearch.ViewFlag || GrievanceSearch.ViewFlag || WizardSearch.ViewFlag || Reports; } }

        #region employee
        public static FormSecurity EmployeeSearch { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_SEARCH"); } }
        public static FormSecurity EmployeeDeleteButton { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_DELETE"); } }
        public static FormSecurity EmployeeSearchExport { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_EXPORTSEARCH"); } }

        //biographical
        public static bool Employee { get { return EmployeeBiographical.ViewFlag || EmployeeAddress.ViewFlag || EmployeePhone.ViewFlag || EmployeeEmail.ViewFlag; } }
        public static FormSecurity EmployeeBiographical { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_BIOGRAPHICAL"); } }
        public static FormSecurity EmployeeAddress { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_ADDRESS"); } }
        public static FormSecurity EmployeePhone { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_PHONE"); } }
        public static FormSecurity EmployeeEmail { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_EMAIL"); } }

        //employment information
        public static FormSecurity EmployeeEmployment { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_EMPLOYMENT"); } }

        //position
        public static FormSecurity EmployeePosition { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_POSITION"); } }
        public static FormSecurity EmployeePositionOverrideEditButtonSecurity { get { return GetFormsInRole(CurrentGuiRoleId, "POS_EDIT"); } }
        public static FormSecurity EmployeePositionLabourDistrubution { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_POS_LAB_DIST"); } }
        public static FormSecurity EmployeePositionAlternateLabour { get { return GetFormsInRole(CurrentGuiRoleId, "ALT_LABOUR"); } }

        //contacts
        public static FormSecurity EmployeeContacts { get { return GetFormsInRole(CurrentGuiRoleId, "CONTACT"); } }

        //competencies
        public static bool EmployeeEducation
        {
            get
            {
                return EmployeeEducationDetail.ViewFlag || EmployeeSkill.ViewFlag || EmployeeLicense.ViewFlag || EmployeeAwards.ViewFlag || EmployeeCourse.ViewFlag ||
                    EmployeeMembership.ViewFlag;
            }
        }
        public static FormSecurity EmployeeEducationDetail { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_EDUCATION"); } }
        public static FormSecurity EmployeeSkill { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_SKILL"); } }
        public static FormSecurity EmployeeLicense { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_LICENSE"); } }
        public static FormSecurity EmployeeAwards { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_AWARDS"); } }
        public static FormSecurity EmployeeCourse { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_COURSE"); } }
        public static FormSecurity EmployeeMembership { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_MEMBER"); } }

        //company property
        public static FormSecurity EmployeeProperty { get { return GetFormsInRole(CurrentGuiRoleId, "CMP_PROPERTY"); } }

        //disciplinary
        public static FormSecurity EmployeeDisciplinary { get { return GetFormsInRole(CurrentGuiRoleId, "DISCIPLINARY"); } }

        //termination
        public static bool EmployeeTermination { get { return EmployeeTerminationDetail.ViewFlag || EmployeeTerminationOther.ViewFlag || EmployeeTerminationROE.ViewFlag; } }
        public static FormSecurity EmployeeTerminationDetail { get { return GetFormsInRole(CurrentGuiRoleId, "TERM"); } }
        public static FormSecurity EmployeeTerminationOther { get { return GetFormsInRole(CurrentGuiRoleId, "TERM_OTHER"); } }
        public static FormSecurity EmployeeTerminationROE { get { return GetFormsInRole(CurrentGuiRoleId, "TERM_ROE"); } }

        //health and safety report
        public static FormSecurity EmployeeWsibHealthAndSafetyReport { get { return GetFormsInRole(CurrentGuiRoleId, "WSIB_HS_REPORT"); } }

        //benefit
        public static bool EmployeeBenefit { get { return EmployeeBenefitSummary.ViewFlag; } }
        public static FormSecurity EmployeeBenefitSummary { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_BEN_SUM"); } }
        #endregion

        //greivances
        public static bool Grievance { get { return GrievanceSearch.ViewFlag || GrievanceDetail.ViewFlag; } }
        public static FormSecurity GrievanceSearch { get { return GetFormsInRole(CurrentGuiRoleId, "GRIEV_SEARCH"); } }
        public static FormSecurity GrievanceDetail { get { return GetFormsInRole(CurrentGuiRoleId, "GRIEV_DETAIL"); } }

        //wizard
        public static FormSecurity WizardSearch { get { return GetFormsInRole(CurrentGuiRoleId, "WIZ_SEARCH"); } }

        //reports
        public static bool Reports
        {
            get
            {
                return EmployeePhoneList.ViewFlag || EmployeeBirthdayList.ViewFlag || EmployeeNameAndAddressListing.ViewFlag || EmployeeAnniversaryListing.ViewFlag ||
                    EmployeeEmergencyContacts.ViewFlag || EmployeeListingReport.ViewFlag || SkillsCertificateLicenseSample.ViewFlag || TerminatedEmployees.ViewFlag ||
                    SalaryHourlyReport.ViewFlag || StatusCodeReport.ViewFlag || PaycodeReport.ViewFlag || StatHoliday.ViewFlag || AttritionReport.ViewFlag ||
                    EmployeeDetailAuditReport.ViewFlag || ProbationReport.ViewFlag || HrChangesAuditReport.ViewFlag || SkillReport.ViewFlag;
            }
        }
        public static FormSecurity EmployeePhoneList { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_PHONE_LIST"); } }
        public static FormSecurity EmployeeBirthdayList { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_BDAY_LIST"); } }
        public static FormSecurity EmployeeNameAndAddressListing { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_NAME_ADDRESS"); } }
        public static FormSecurity EmployeeAnniversaryListing { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_ANNV_LISTING"); } }
        public static FormSecurity EmployeeEmergencyContacts { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_EMERGENCY"); } }
        public static FormSecurity EmployeeListingReport { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_LISTING"); } }
        public static FormSecurity SkillsCertificateLicenseSample { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_SKILLS"); } }
        public static FormSecurity TerminatedEmployees { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_TERM"); } }
        public static FormSecurity SalaryHourlyReport { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_RATE_LISTING"); } }
        public static FormSecurity StatusCodeReport { get { return GetFormsInRole(CurrentGuiRoleId, "STATUS_CODE"); } }
        public static FormSecurity PaycodeReport { get { return GetFormsInRole(CurrentGuiRoleId, "PAYCODE_REPORT"); } }
        public static FormSecurity StatHoliday { get { return GetFormsInRole(CurrentGuiRoleId, "STAT_HOLIDAY"); } }
        public static FormSecurity AttritionReport { get { return GetFormsInRole(CurrentGuiRoleId, "ATTRITION_REPORT"); } }
        public static FormSecurity EmployeeDetailAuditReport { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_DETAIL_AUDIT"); } }
        public static FormSecurity ProbationReport { get { return GetFormsInRole(CurrentGuiRoleId, "PROBATION_REPORT"); } }
        public static FormSecurity HrChangesAuditReport { get { return GetFormsInRole(CurrentGuiRoleId, "HR_CHG_AD_REPORT"); } }
        public static FormSecurity SkillReport { get { return GetFormsInRole(CurrentGuiRoleId, "SKILL_REPORT"); } }
        #endregion

        #region future functionality
        //time and attendance
        public static bool TA { get { return false; } }

        //SH
        public static bool SH { get { return false; } }

        //scheduler
        public static bool Scheduler { get { return false; } }

        //budget
        public static bool Budget { get { return false; } }
        #endregion

        #region payroll
        public static bool Payroll
        {
            get
            {
                return PayrollEmployee || PayrollBatchSearch.ViewFlag || PayrollException.ViewFlag || PayrollProcess.ViewFlag || RoeCreationSearch.ViewFlag ||
                    PayrollPayments.ViewFlag || Remittance.ViewFlag || RemittanceImport.ViewFlag || Import.ViewFlag || PayrollReportMenu;
            }
        }

        #region employee
        public static bool PayrollEmployee
        {
            get
            {
                return EmployeeBanking.ViewFlag || EmployeeStatutoryDeduction || EmployeePaycode.ViewFlag || PayrollEmployeeSearchNHT.ViewFlag ||
                    PayrollEmployeeSearchPaySlip.ViewFlag || PayrollEmployeeSearchPaySlipMailingFlag.ViewFlag || EmployeePosition.ViewFlag;
            }
        }

        //banking
        public static FormSecurity EmployeeBanking { get { return GetFormsInRole(CurrentGuiRoleId, "DIRECT_DEPOSIT"); } }

        //stat deductions
        public static bool EmployeeStatutoryDeduction
        {
            get
            {
                return EmployeeStatutoryDeductionCanadian.ViewFlag || EmployeeStatutoryDeductionTaxOverride.ViewFlag || EmployeeStatutoryDeductionStLucia.ViewFlag ||
                    EmployeeStatutoryDeductionBarbados.ViewFlag || EmployeeStatutoryDeductionTrinidad.ViewFlag || EmployeeStatutoryDeductionJamaica.ViewFlag;
            }
        }
        public static FormSecurity EmployeeStatutoryDeductionCanadian { get { return GetFormsInRole(CurrentGuiRoleId, "STAT_DEDUCTION"); } }
        public static FormSecurity EmployeeStatutoryDeductionTaxOverride { get { return GetFormsInRole(CurrentGuiRoleId, "STAT_OVERRIDE"); } }
        public static FormSecurity EmployeeStatutoryDeductionStLucia { get { return GetFormsInRole(CurrentGuiRoleId, "STAT_STLUCIA"); } }
        public static FormSecurity EmployeeStatutoryDeductionBarbados { get { return GetFormsInRole(CurrentGuiRoleId, "STAT_BARBADOS"); } }
        public static FormSecurity EmployeeStatutoryDeductionTrinidad { get { return GetFormsInRole(CurrentGuiRoleId, "STAT_TRINIDAD"); } }
        public static FormSecurity EmployeeStatutoryDeductionJamaica { get { return GetFormsInRole(CurrentGuiRoleId, "STAT_JAMAICA"); } }

        //paycode
        public static FormSecurity EmployeePaycode { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_PAYCODE"); } }

        //NHT
        public static FormSecurity PayrollEmployeeSearchNHT { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_SRCH_NHT"); } }

        //payslip
        public static FormSecurity PayrollEmployeeSearchPaySlip { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_SRCH_PAYSLIP"); } }
        public static FormSecurity ExportCheque { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_EXP_CHEQUE"); } }

        //mailing label
        public static FormSecurity PayrollEmployeeSearchPaySlipMailingFlag { get { return GetFormsInRole(CurrentGuiRoleId, "PAYSLIP_MAIL_LBL"); } }
        #endregion

        public static FormSecurity PayrollException { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_EXCEPTION"); } }

        //payroll batch
        public static FormSecurity PayrollBatchSearch { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_BATCH_SEARCH"); } }
        public static FormSecurity PayrollBatch { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_BATCH_TRAN"); } }

        //payroll process
        public static FormSecurity PayrollProcess { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_PROCESS"); } }

        public static FormSecurity PayrollProcessLockUnlockButtons { get { return GetFormsInRole(CurrentGuiRoleId, "LOCK_BTN"); } }

        //roe creation search
        public static FormSecurity RoeCreationSearch { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_ROE_CREATE"); } }

        public static bool RoeCreationRoeDetails
        {
            get
            {
                return EmployeeTerminationDetail.ViewFlag || EmployeeTerminationOther.ViewFlag || RoeCreationDetails.ViewFlag;
            }
        }
        public static FormSecurity RoeCreationDetails { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_ROE_DETAIL"); } }

        #region payments
        public static FormSecurity PayrollPayments { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_PAYMENTS"); } }

        //NHT button
        public static FormSecurity PayrollPayRegisterNHT { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_NHT"); } }

        //payslip button
        public static FormSecurity PayrollPayRegisterPaySlip { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_PAYSLIP"); } }

        //mass payslip print button
        public static FormSecurity PayrollPayRegisterMassPaySlipPrint { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_MSS_PRNT"); } }

        //export menu
        public static bool ExportMenu
        {
            get
            {
                return CSBExport.ViewFlag || PayrollCeridianExportButton.ViewFlag || EFTButton.ViewFlag || PensionExportButton.ViewFlag || GLButton.ViewFlag ||
                    RRSPButton.ViewFlag || StockButton.ViewFlag || EmpowerRemittanceButton.ViewFlag || EnableEmpowerGarnishmentButton.ViewFlag ||
                    CICPlusWeeklyButton.ViewFlag || CraGarnishmentButton.ViewFlag || SocialCostsExport.ViewFlag || RealCustomExport.ViewFlag;
            }
        }
        public static FormSecurity CSBExport { get { return GetFormsInRole(CurrentGuiRoleId, "CSB_EXPORT"); } }
        public static FormSecurity PayrollCeridianExportButton { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_CERD_EXPORT"); } }
        public static FormSecurity EFTButton { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_EFT"); } }
        public static FormSecurity PensionExportButton { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_PENS_EXP"); } }
        public static FormSecurity GLButton { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_GL"); } }
        public static FormSecurity RRSPButton { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_RRSP"); } }
        public static FormSecurity StockButton { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_STOCK"); } }
        public static FormSecurity EmpowerRemittanceButton { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_EMPOWER"); } }
        public static FormSecurity EnableEmpowerGarnishmentButton { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_EMPGARN"); } }
        public static FormSecurity CICPlusWeeklyButton { get { return GetFormsInRole(CurrentGuiRoleId, "CIC_PLUS_WEEKLY"); } }
        public static FormSecurity CraGarnishmentButton { get { return GetFormsInRole(CurrentGuiRoleId, "CRA_GARN_EXPORT"); } }
        public static FormSecurity SocialCostsExport { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_SOCIAL"); } }
        public static FormSecurity RealCustomExport { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REAL_CUSTOM"); } }

        //eft payment button and payment list report
        public static FormSecurity EFTPayment { get { return GetFormsInRole(CurrentGuiRoleId, "EFTPYMNT"); } }

        //real export to ftp
        public static FormSecurity RealExport { get { return GetFormsInRole(CurrentGuiRoleId, "REALEXPT"); } }

        //real ftp import on process screen
        public static FormSecurity RealFtpImport { get { return GetFormsInRole(CurrentGuiRoleId, "RLFTPIMP"); } }

        //transmit files button
        public static FormSecurity PayrollPayRegisterTransmitFiles { get { return GetFormsInRole(CurrentGuiRoleId, "TRANSMIT_FILES"); } }

        //parent report menu
        public static bool PayRegisterReportMenu { get { return PayRegisterReportStandardMenu || PayRegisterReportCustomMenu; } }

        //standard reports
        public static bool PayRegisterReportStandardMenu
        {
            get
            {
                return PayrollPayRegisterDetails.ViewFlag || PayrollPayRegisterSummary.ViewFlag || PayrollRegisterChangesReport.ViewFlag || PayrollRegisterCompensationList.ViewFlag ||
                       PayrollRegisterCurrentVsPrior.ViewFlag || PayrollRegisterCurrentVsPriorNet.ViewFlag || PayrollRegisterGarnishment.ViewFlag || PayrollRegisterPayException.ViewFlag ||
                       PayrollRegisterGrossVsSalary.ViewFlag || PayrollRegisterPD7A.ViewFlag || BarbadosDeductionSummary.ViewFlag || StLuciaDeductionSummary.ViewFlag ||
                       TrinidadDeductionSummary.ViewFlag || JamaicaDeductionSummary.ViewFlag || PayrollRegisterYtdBonusCommission.ViewFlag || PayrollRegisterWCB.ViewFlag ||
                       PayrollRegisterCSB.ViewFlag || PayrollRegisterPayDetails.ViewFlag || PayrollRegisterGLDetails.ViewFlag || PayrollRegisterFedProvRemitSummary.ViewFlag ||
                       PayrollRegisterProvHealthSummary.ViewFlag || PayrollRegisterQuebecRemitSummary.ViewFlag || PayrollRegisterPreYET4Detail.ViewFlag ||
                       PayrollRegisterPreYET4ADetail.ViewFlag || PayrollRegisterPreYER1Detail.ViewFlag || PayrollRegisterPreYER2Detail.ViewFlag || PayrollRegisterPIERPreYE.ViewFlag ||
                       PayrollRegisterBenefitArrears.ViewFlag || PaymentListReport.ViewFlag || VacationReport.ViewFlag || InvoiceJamaica.ViewFlag || InvoiceCanada.ViewFlag || 
                       AccumulatedHoursReportMenuItem.ViewFlag;
            }
        }
        //funding statements for Canada and Jamaica
        public static FormSecurity InvoiceCanada { get { return GetFormsInRole(CurrentGuiRoleId, "INVOICE_CANADA"); } }
        public static FormSecurity InvoiceJamaica { get { return GetFormsInRole(CurrentGuiRoleId, "INVOICE_JAMAICA"); } }

        public static FormSecurity PayrollPayRegisterDetails { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_DETAILS"); } }
        public static FormSecurity PayrollPayRegisterSummary { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_SUMMARY"); } }
        public static FormSecurity PayrollRegisterChangesReport { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_CHANGES"); } }
        public static FormSecurity PayrollRegisterCompensationList { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_COMPENSA"); } }
        public static FormSecurity PayrollRegisterCurrentVsPrior { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_CUR_PRI"); } }
        public static FormSecurity PayrollRegisterCurrentVsPriorNet { get { return GetFormsInRole(CurrentGuiRoleId, "PYRG_CURPRI_NET"); } }
        public static FormSecurity PayrollRegisterCurrentVsPriorPeriodVariances { get { return GetFormsInRole(CurrentGuiRoleId, "CUR_PRI_PER_VAR"); } }
        public static FormSecurity PayrollRegisterGarnishment { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_GARN"); } }
        public static FormSecurity PayrollRegisterPayException { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_GROSSNET"); } }
        public static FormSecurity PayrollRegisterGrossVsSalary { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_GROSSSAL"); } }
        public static FormSecurity PayrollRegisterPD7A { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_PD7A"); } }
        public static FormSecurity BarbadosDeductionSummary { get { return GetFormsInRole(CurrentGuiRoleId, "BB_DEDUCTION"); } }
        public static FormSecurity StLuciaDeductionSummary { get { return GetFormsInRole(CurrentGuiRoleId, "LC_DEDUCTION"); } }
        public static FormSecurity TrinidadDeductionSummary { get { return GetFormsInRole(CurrentGuiRoleId, "TT_DEDUCTION"); } }
        public static FormSecurity JamaicaDeductionSummary { get { return GetFormsInRole(CurrentGuiRoleId, "JM_DEDUCTION"); } }
        public static FormSecurity JamaicaGovernmentMonthlyRemittance { get { return GetFormsInRole(CurrentGuiRoleId, "JM_MTH_REMIT"); } }
        public static FormSecurity PayrollRegisterYtdBonusCommission { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_YTDBC"); } }
        public static FormSecurity PayrollRegisterWCB { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_WCB"); } }
        public static FormSecurity PayrollRegisterPayDetails { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_DETAILS"); } }
        public static FormSecurity PayrollRegisterGLDetails { get { return GetFormsInRole(CurrentGuiRoleId, "GL_EMP_DETAILS"); } }
        public static FormSecurity PayrollRegisterFedProvRemitSummary { get { return GetFormsInRole(CurrentGuiRoleId, "FED_REMIT_SUM"); } }
        public static FormSecurity PayrollRegisterProvHealthSummary { get { return GetFormsInRole(CurrentGuiRoleId, "PROV_HEALTH_SUM"); } }
        public static FormSecurity ResearchAndDevelopment { get { return GetFormsInRole(CurrentGuiRoleId, "RESEARCH-DEV"); } }
        public static FormSecurity PayrollRegisterQuebecRemitSummary { get { return GetFormsInRole(CurrentGuiRoleId, "QUE_REMIT_SUM"); } }
        public static FormSecurity PayrollRegisterPreYET4ADetail { get { return GetFormsInRole(CurrentGuiRoleId, "PRE_YE_T4A"); } }
        public static FormSecurity PayrollRegisterPreYET4Detail { get { return GetFormsInRole(CurrentGuiRoleId, "PRE_YE_T4"); } }
        public static FormSecurity PayrollRegisterPreYER1Detail { get { return GetFormsInRole(CurrentGuiRoleId, "PRE_YE_R1"); } }
        public static FormSecurity PayrollRegisterPreYER2Detail { get { return GetFormsInRole(CurrentGuiRoleId, "PRE_YE_R2_DET"); } }
        public static FormSecurity PayrollRegisterPIERPreYE { get { return GetFormsInRole(CurrentGuiRoleId, "PRE_YE_PIER"); } }
        public static FormSecurity PayrollRegisterBenefitArrears { get { return GetFormsInRole(CurrentGuiRoleId, "PRE_YE_BEN_ARR"); } }
        public static FormSecurity PayrollRegisterCSB { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_CSB"); } }
        public static FormSecurity VacationReport { get { return GetFormsInRole(CurrentGuiRoleId, "VAC_REPORT"); } }
        public static FormSecurity UserAccessInformationReport { get { return GetFormsInRole(CurrentGuiRoleId, "USER_ACS_INFO"); } }
        public static FormSecurity PaymentListReport { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_LIST"); } }
        public static FormSecurity AccumulatedHoursReportMenuItem => GetFormsInRole(CurrentGuiRoleId, "ACC_HOUR_RPT");

        //custom reports
        public static bool PayRegisterReportCustomMenu
        {
            get
            {
                return PayrollRegisterPensionReport.ViewFlag || EmployeeSalaryReport.ViewFlag || UnitedWayReport.ViewFlag || SerpYtdReport.ViewFlag || PensionDemographicsReport.ViewFlag ||
                    SrpReport.ViewFlag || GrossUpReport.ViewFlag || PreYearEndDetail.ViewFlag || A1235GLDetailsReport.ViewFlag || CurrentVsPriorTwoPeriodVariances.ViewFlag ||
                    MonthlyStatutoryRemittance.ViewFlag || PayrollDetailsOrgUnit.ViewFlag || ResearchAndDevelopment.ViewFlag;
            }
        }
        public static FormSecurity PayrollRegisterPensionReport { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_PENSION"); } }
        public static FormSecurity EmployeeSalaryReport { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_EMP_SAL"); } }
        public static FormSecurity UnitedWayReport { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_ABB_UW"); } }
        public static FormSecurity SerpYtdReport { get { return GetFormsInRole(CurrentGuiRoleId, "SERP_YTD"); } }
        public static FormSecurity PensionDemographicsReport { get { return GetFormsInRole(CurrentGuiRoleId, "PENS_DEMOGRAPHIC"); } }
        public static FormSecurity SrpReport { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_SRP"); } }
        public static FormSecurity GrossUpReport { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_REG_GROSS"); } }
        public static FormSecurity PreYearEndDetail { get { return GetFormsInRole(CurrentGuiRoleId, "PRE_YE_DETAIL"); } }
        public static FormSecurity A1235GLDetailsReport { get { return GetFormsInRole(CurrentGuiRoleId, "A1235_GL_DET"); } }
        public static FormSecurity CurrentVsPriorTwoPeriodVariances { get { return GetFormsInRole(CurrentGuiRoleId, "CUR_PRI_2PER_VAR"); } }
        public static FormSecurity MonthlyStatutoryRemittance { get { return GetFormsInRole(CurrentGuiRoleId, "MTH_STAT_REMIT"); } }
        public static FormSecurity PayrollDetailsOrgUnit { get { return GetFormsInRole(CurrentGuiRoleId, "PAY_DETAILS_ORG"); } }
        #endregion

        //remittance
        public static FormSecurity Remittance { get { return GetFormsInRole(CurrentGuiRoleId, "REMITTANCE"); } }

        //remittance import
        public static FormSecurity RemittanceImport { get { return GetFormsInRole(CurrentGuiRoleId, "REMITTANCE_IMP"); } }
        public static FormSecurity RemittanceImportApproval { get { return GetFormsInRole(CurrentGuiRoleId, "REMIT_IMP_APV"); } }

        //import
        public static FormSecurity Import { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_IMPORT"); } }

        //reports
        public static bool PayrollReportMenu
        {
            get
            {
                return PaycodeYearEndMappingReport.ViewFlag || NonStatDeductions.ViewFlag || WageTypeCatalogReport.ViewFlag;
            }
        }
        public static FormSecurity PaycodeYearEndMappingReport { get { return GetFormsInRole(CurrentGuiRoleId, "PCYEARENDMAPPING"); } }
        public static FormSecurity NonStatDeductions { get { return GetFormsInRole(CurrentGuiRoleId, "NONSTATDED"); } }
        public static FormSecurity WageTypeCatalogReport { get { return GetFormsInRole(CurrentGuiRoleId, "WAGE_TYPE_CAT"); } }
        #endregion

        #region setup
        public static bool HRSetup
        {
            get
            {
                return UnionContact.ViewFlag || PolicySearch.ViewFlag || EntitlementSearch.ViewFlag || Vendor.ViewFlag || SalaryPlan.ViewFlag || PayrollProcessingGroup.ViewFlag ||
                    OrganizationUnitLevel.ViewFlag || Email.ViewFlag || WizardTemplate.ViewFlag || AdminPaycode || Benefit;
            }
        }

        //union contact
        public static FormSecurity UnionContact { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_UNION"); } }
        public static FormSecurity UnionCollectiveAgreemnt { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_COLLAGRMNT"); } }

        //policy
        public static FormSecurity PolicySearch { get { return GetFormsInRole(CurrentGuiRoleId, "POLICY_SEARCH"); } }
        public static FormSecurity PolicyEdit { get { return GetFormsInRole(CurrentGuiRoleId, "POLICY_EDIT"); } }

        //entitlements
        public static FormSecurity EntitlementSearch { get { return GetFormsInRole(CurrentGuiRoleId, "ENTIT_SEARCH"); } }
        public static FormSecurity EntitlementEdit { get { return GetFormsInRole(CurrentGuiRoleId, "ENTIT_EDIT"); } }

        //vendor
        public static FormSecurity Vendor { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_VENDOR"); } }

        //salary plan
        public static FormSecurity SalaryPlan { get { return GetFormsInRole(CurrentGuiRoleId, "SALARY_PLAN"); } }

        //payroll processing group
        public static FormSecurity PayrollProcessingGroup { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_PAY_GROUP"); } }
        public static FormSecurity PayrollProcessGroupOverridePeriod { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_PAY_OVRD"); } }

        //organization unit
        public static FormSecurity OrganizationUnitLevel { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_ORG"); } }

        //email
        public static FormSecurity Email { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_EMAIL"); } }

        //wizard template
        public static FormSecurity WizardTemplate { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_WIZ_TEMP"); } }
        public static FormSecurity WizardSelectTemplate { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_SEL_TEMP"); } }

        #region paycode
        public static bool AdminPaycode
        {
            get
            {
                return GlobalEmployeePaycode.ViewFlag || PaycodeAssociation.ViewFlag || PaycodeMaintenance.ViewFlag;
            }
        }

        //global employee paycode
        public static FormSecurity GlobalEmployeePaycode { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_GLOBALPAY"); } }

        //paycode association
        public static FormSecurity PaycodeAssociation { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_PAYCODEAS"); } }

        //paycode maintenance
        public static FormSecurity PaycodeMaintenance { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_PAY_MAIN"); } }
        public static bool PaycodeMaintenanceReportMenu { get { return WageTypeCatalogReport.ViewFlag; } }
        public static FormSecurity PaycodeYearEndReportMap { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_PAY_YEMAP"); } }
        public static FormSecurity PaycodePayrollTransactionOffsetAssociation { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_PAY_PTOA"); } }
        #endregion

        #endregion


        #region benefit
        public static bool Benefit
        {
            get
            {
                return BenefitPolicySearch.ViewFlag || BenefitPlanSearch.ViewFlag;
            }
        }

        //benefit policy
        public static FormSecurity BenefitPolicySearch { get { return GetFormsInRole(CurrentGuiRoleId, "BEN_POLICY_SRCH"); } }
        public static FormSecurity BenefitPolicyEdit { get { return GetFormsInRole(CurrentGuiRoleId, "BEN_POLICY_EDIT"); } }

        //benefit plan
        public static FormSecurity BenefitPlanSearch { get { return GetFormsInRole(CurrentGuiRoleId, "BEN_PLAN_SRCH"); } }
        public static FormSecurity BenefitPlanEdit { get { return GetFormsInRole(CurrentGuiRoleId, "BEN_PLAN_EDIT"); } }
        #endregion

        #endregion

        #region admin
        public static bool Admin
        {
            get
            {
                return CodeMaintenance.ViewFlag || PersonAddressTypeCodeMaintenance.ViewFlag || SalaryPlanCodeMaintenance.ViewFlag || CodeSystem.ViewFlag ||
                    MandatoryField.ViewFlag || LanguageSearch.ViewFlag || ExportFtp.ViewFlag || ReportExport.ViewFlag || PayRegisterExport.ViewFlag ||
                    Wsib.ViewFlag || WcbChequeRemittanceFrequency.ViewFlag || HealthTax.ViewFlag || StatutoryHolidayDate.ViewFlag || RevenueQuebecBusinessForm.ViewFlag ||
                    EmployeeCustomFieldConfig.ViewFlag || AdminYearEnd || AdminSecurity || ThirdParty.ViewFlag;
            }
        }

        public static FormSecurity CodeMaintenance { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_CODE"); } }
        public static FormSecurity PersonAddressTypeCodeMaintenance { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_P_ADD_TYPE"); } }
        public static FormSecurity SalaryPlanCodeMaintenance { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_SAL_PLAN"); } }
        public static FormSecurity CodeSystem { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_CODE_SYS"); } }
        public static FormSecurity MandatoryField { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_MANDATORY"); } }

        //language editor
        public static FormSecurity LanguageSearch { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_LANGSEARCH"); } }
        public static FormSecurity LanguageEdit { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_LANGUAGE"); } }

        public static FormSecurity ExportFtp { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_EXPT_FTP"); } }
        public static FormSecurity ReportExport { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_REPORT_EXP"); } }
        public static FormSecurity PayRegisterExport { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_PAYREGEXP"); } }
        public static FormSecurity Wsib { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_WSIB"); } }
        public static FormSecurity WcbChequeRemittanceFrequency { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_WCB_CRF"); } }
        public static FormSecurity HealthTax { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_HEALTHTAX"); } }
        public static FormSecurity StatutoryHolidayDate { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_STAT_HOL"); } }
        public static FormSecurity RevenueQuebecBusinessForm { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_REV_QC_BUS"); } }
        public static FormSecurity EmployeeCustomFieldConfig { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_CUSTCONFIG"); } }
        public static FormSecurity ThirdParty { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_THIRDPARTY"); } }        

        #region year end
        public static bool AdminYearEnd { get { return YearEndAdjustmentsSearch.ViewFlag || YearEnd.ViewFlag; } }

        //year end adjustments
        public static FormSecurity YearEndAdjustmentsSearch { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_YESEARCH"); } }
        public static FormSecurity YearEndAdjustmentsPageControl { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_YEADJPAGE"); } }
        public static FormSecurity YearEndAdjustmentsDetailsControl { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_YEADJDET"); } }
        public static FormSecurity T4Report { get { return GetFormsInRole(CurrentGuiRoleId, "T4_REPORT"); } }
        public static FormSecurity T4AReport { get { return GetFormsInRole(CurrentGuiRoleId, "T4A_REPORT"); } }
        public static FormSecurity R1Report { get { return GetFormsInRole(CurrentGuiRoleId, "R1_REPORT"); } }
        public static FormSecurity R2Report { get { return GetFormsInRole(CurrentGuiRoleId, "R2_REPORT"); } }
        public static FormSecurity T4RSPReport { get { return GetFormsInRole(CurrentGuiRoleId, "T4RSP_REPORT"); } }
        public static FormSecurity T4ARCAReport { get { return GetFormsInRole(CurrentGuiRoleId, "T4ARCA_REPORT"); } }
        public static FormSecurity NR4Report { get { return GetFormsInRole(CurrentGuiRoleId, "NR4_REPORT"); } }

        //year end
        public static FormSecurity YearEnd { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_YEAR_END"); } }

        //year end mass print
        public static bool YearEndMassPrintMenu
        {
            get
            {
                return T4MassReportPrint.ViewFlag || R1MassReportPrint.ViewFlag || T4AMassReportPrint.ViewFlag || R2MassReportPrint.ViewFlag || T4RSPMassReportPrint.ViewFlag ||
                    T4ARCAMassReportPrint.ViewFlag || NR4MassReportPrint.ViewFlag;
            }
        }
        public static FormSecurity T4MassReportPrint { get { return GetFormsInRole(CurrentGuiRoleId, "T4MASS_REPORT"); } }
        public static FormSecurity R1MassReportPrint { get { return GetFormsInRole(CurrentGuiRoleId, "R1MASS_REPORT"); } }
        public static FormSecurity T4AMassReportPrint { get { return GetFormsInRole(CurrentGuiRoleId, "T4AMASS_REPORT"); } }
        public static FormSecurity R2MassReportPrint { get { return GetFormsInRole(CurrentGuiRoleId, "R2MASS_REPORT"); } }
        public static FormSecurity T4RSPMassReportPrint { get { return GetFormsInRole(CurrentGuiRoleId, "T4RSPMASS_REPORT"); } }
        public static FormSecurity T4ARCAMassReportPrint { get { return GetFormsInRole(CurrentGuiRoleId, "T4ARCAMASSREPORT"); } }
        public static FormSecurity NR4MassReportPrint { get { return GetFormsInRole(CurrentGuiRoleId, "NR4MASS_REPORT"); } }

        //year end mass file
        public static bool YearEndMassFileMenu { get { return MassFileAll.ViewFlag || T4MassReportFile.ViewFlag || R1MassReportFile.ViewFlag || T4AMassReportFile.ViewFlag; } }
        public static FormSecurity MassFileAll { get { return GetFormsInRole(CurrentGuiRoleId, "MASS_FILE_ALL"); } }
        public static FormSecurity T4MassReportFile { get { return GetFormsInRole(CurrentGuiRoleId, "T4MASS_FILE"); } }
        public static FormSecurity R1MassReportFile { get { return GetFormsInRole(CurrentGuiRoleId, "R1MASS_FILE"); } }
        public static FormSecurity T4AMassReportFile { get { return GetFormsInRole(CurrentGuiRoleId, "T4AMASS_FILE"); } }

        //year end export
        public static bool YearEndExportMenu { get { return T4Export.ViewFlag || R1Export.ViewFlag || T4AExport.ViewFlag || R2Export.ViewFlag || T4RSPExport || T4ARCAExport || NR4Export.ViewFlag; } }
        public static FormSecurity T4Export { get { return GetFormsInRole(CurrentGuiRoleId, "T4_EXPORT"); } }
        public static FormSecurity R1Export { get { return GetFormsInRole(CurrentGuiRoleId, "R1_EXPORT"); } }
        public static FormSecurity T4AExport { get { return GetFormsInRole(CurrentGuiRoleId, "T4A_EXPORT"); } }
        public static FormSecurity R2Export { get { return GetFormsInRole(CurrentGuiRoleId, "R2_EXPORT"); } }
        public static bool T4RSPExport { get { return T4Export.ViewFlag || R1Export.ViewFlag || T4AExport.ViewFlag || R2Export.ViewFlag || NR4Export.ViewFlag; } } //placeholder
        public static bool T4ARCAExport { get { return T4Export.ViewFlag || R1Export.ViewFlag || T4AExport.ViewFlag || R2Export.ViewFlag || NR4Export.ViewFlag; } } //placeholder
        public static FormSecurity NR4Export { get { return GetFormsInRole(CurrentGuiRoleId, "NR4_EXPORT"); } }

        //year end summary
        public static bool YearEndSummaryMenu
        {
            get
            {
                return T4SummaryMassReportPrint.ViewFlag || R1SummaryOfSourceDeductions.ViewFlag || T4ASummaryMassReportPrint.ViewFlag || R2Summary.ViewFlag ||
                    T4RSPSummary || T4ARCASummary || NR4SummaryMassReportPrint.ViewFlag;
            }
        }
        public static FormSecurity T4SummaryMassReportPrint { get { return GetFormsInRole(CurrentGuiRoleId, "T4_SUMMARY"); } }
        public static FormSecurity R1SummaryOfSourceDeductions { get { return GetFormsInRole(CurrentGuiRoleId, "R1SUM_DED"); } }
        public static FormSecurity T4ASummaryMassReportPrint { get { return GetFormsInRole(CurrentGuiRoleId, "T4A_SUMMARY"); } }
        public static FormSecurity R2Summary { get { return GetFormsInRole(CurrentGuiRoleId, "R2_SUMMARY"); } }
        public static bool T4RSPSummary //placeholder
        {
            get
            {
                return T4SummaryMassReportPrint.ViewFlag || R1SummaryOfSourceDeductions.ViewFlag || T4ASummaryMassReportPrint.ViewFlag || R2Summary.ViewFlag ||
                    NR4SummaryMassReportPrint.ViewFlag;
            }
        }
        public static bool T4ARCASummary //placeholder
        {
            get
            {
                return T4SummaryMassReportPrint.ViewFlag || R1SummaryOfSourceDeductions.ViewFlag || T4ASummaryMassReportPrint.ViewFlag || R2Summary.ViewFlag ||
                    NR4SummaryMassReportPrint.ViewFlag;
            }
        }
        public static FormSecurity NR4SummaryMassReportPrint { get { return GetFormsInRole(CurrentGuiRoleId, "NR4_SUMMARY"); } }

        //year end reports
        public static bool YearEndReportMenu
        {
            get
            {
                return StatDeductionException.ViewFlag || NameAddressException.ViewFlag || YearEndWCBReport.ViewFlag || YearEndPaycodeSummaryReport.ViewFlag ||
                    YearEndAdjustmentsAudit.ViewFlag || YearEndHealthTaxSummary.ViewFlag || YearEndR1Detail.ViewFlag || YearEndR2Detail.ViewFlag ||
                    YearEndT4Detail.ViewFlag || YearEndT4ADetail.ViewFlag || YearEndAdjustmentReport.ViewFlag;
            }
        }
        public static FormSecurity StatDeductionException { get { return GetFormsInRole(CurrentGuiRoleId, "STATDED_EXCPTN"); } }
        public static FormSecurity NameAddressException { get { return GetFormsInRole(CurrentGuiRoleId, "NA_EXCEPTION"); } }
        public static FormSecurity YearEndWCBReport { get { return GetFormsInRole(CurrentGuiRoleId, "YR_END_WCB"); } }
        public static FormSecurity YearEndPaycodeSummaryReport { get { return GetFormsInRole(CurrentGuiRoleId, "YR_END_PAY_SUMM"); } }
        public static FormSecurity YearEndAdjustmentsAudit { get { return GetFormsInRole(CurrentGuiRoleId, "YR_END_ADJ_AUDIT"); } }
        public static FormSecurity YearEndHealthTaxSummary { get { return GetFormsInRole(CurrentGuiRoleId, "YE_HLTH_TAX_SUM"); } }
        public static FormSecurity YearEndR1Detail { get { return GetFormsInRole(CurrentGuiRoleId, "YE_R1_DETAIL"); } }
        public static FormSecurity YearEndR2Detail { get { return GetFormsInRole(CurrentGuiRoleId, "YE_R2_DETAIL"); } }
        public static FormSecurity YearEndT4Detail { get { return GetFormsInRole(CurrentGuiRoleId, "YE_T4_DETAIL"); } }
        public static FormSecurity YearEndT4ADetail { get { return GetFormsInRole(CurrentGuiRoleId, "YE_T4A_DETAIL"); } }
        public static FormSecurity YearEndAdjustmentReport { get { return GetFormsInRole(CurrentGuiRoleId, "YE_ADJUSTMENT"); } }
        // Salary Plan Accumulation
        public static FormSecurity RulePaycodeAccumulation => GetFormsInRole(CurrentGuiRoleId, "RULE_PAYCODE_ACC");

        #endregion

        #region security
        public static bool AdminSecurity { get { return UserSearch.ViewFlag || RoleSearch.ViewFlag || GroupSearch.ViewFlag || Category.ViewFlag; } }

        //user admin
        public static FormSecurity UserSearch { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_USR_SEARCH"); } }
        public static FormSecurity UserEdit { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_USR_EDIT"); } }

        //role editor
        public static FormSecurity RoleSearch { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_ROL_SEARCH"); } }
        public static FormSecurity RoleEdit { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_ROL_EDIT"); } }
        public static FormSecurity RoleSecurityData { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_SEC_DATA"); } }
        public static FormSecurity RoleSecurityField { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_SEC_FIELD"); } }
        public static FormSecurity RoleSecurityForm { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_SEC_FORM"); } }

        //group editor
        public static FormSecurity GroupSearch { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_GRP_SEARCH"); } }
        public static FormSecurity GroupEdit { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_GRP_EDIT"); } }

        //security categories
        public static FormSecurity Category { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_CAT"); } }
        public static FormSecurity CategoryEdit { get { return GetFormsInRole(CurrentGuiRoleId, "ADMIN_CAT_EDIT"); } }
        #endregion

        //CIC export
        public static FormSecurity T4CICExport { get { return GetFormsInRole(CurrentGuiRoleId, "T4_CIC_EXPORT"); } }
        public static FormSecurity T4ACICExport { get { return GetFormsInRole(CurrentGuiRoleId, "T4A_CIC_EXPORT"); } }
        public static FormSecurity R1CICExport { get { return GetFormsInRole(CurrentGuiRoleId, "R1_CIC_EXPORT"); } }
        #endregion

        #region export menu item on payroll menu
        public static bool PayrollMenuExport
        {
            get
            {
                return LaborLevel.ViewFlag || EmployeeExport.ViewFlag || AdjustmentRuleExport.ViewFlag;
            }
        }
        public static FormSecurity LaborLevel { get { return GetFormsInRole(CurrentGuiRoleId, "LABOR_LEVEL"); } }
        public static FormSecurity EmployeeExport { get { return GetFormsInRole(CurrentGuiRoleId, "EMP_EXPORT"); } }
        public static FormSecurity AdjustmentRuleExport { get { return GetFormsInRole(CurrentGuiRoleId, "ADJUSTMENT_RULE"); } }
        #endregion

        #region help
        public static FormSecurity ChangePassword { get { return GetFormsInRole(CurrentGuiRoleId, "HELP_CNG_PASS"); } }
        #endregion

        #region main
        private static FormSecurityCollection GetForms(long roleId)
        {
            if (!_roleForms.ContainsKey(roleId))
            {
                lock (_roleForms)
                {
                    FormSecurityCollection coll = new FormSecurityCollection();
                    coll.Load(ServiceWrapper.SecurityClient.GetFormSecurityInfo(roleId, null));
                    _roleForms[roleId] = coll;
                }
            }

            return _roleForms[roleId];
        }
        private static FormSecurity GetFormsInRole(long roleId, String label)
        {
            FormSecurityCollection rtn = GetForms(roleId);
            FormSecurity securityObj = null;

            foreach (FormSecurity obj in rtn)
                if (obj.Label == label)
                    securityObj = obj;

            if (securityObj == null)
                return new FormSecurity();
            else
                return securityObj;
        }
        public static void Clear()
        {
            lock (_roleForms)
                _roleForms.Clear();
        }
        #endregion
    }
}