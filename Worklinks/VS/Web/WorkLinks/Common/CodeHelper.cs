﻿using System;
using System.Collections.Generic;
using System.Linq;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Common
{
    public class CodeHelper
    {
        #region fields
        private static Dictionary<String, Dictionary<CodeTableType, CodeCollection>> _cache = null;
        private static Object _addLock = new Object(); //threadsafety
        private const String _profileKey = "Profile";
        #endregion

        #region properties

        public static Dictionary<String, Dictionary<CodeTableType, CodeCollection>> Cache
        {
            get
            {
                if (_cache == null)
                    _cache = new Dictionary<String, Dictionary<CodeTableType, CodeCollection>>();

                return _cache;
            }
        }
        protected String LanguageCode { get { return WLP.Web.UI.WLPPage.LanguageCode; } }
        #endregion

        public static CodeCollection GetCodeTable(String languageCode, CodeTableType type)
        {
            CodeCollection collection = null;

            //not caching code system values anymore
            if (type == CodeTableType.System)
                collection = ServiceWrapper.CodeClient.GetCodeTable(languageCode, type, null);
            else
            {
                Dictionary<CodeTableType, CodeCollection> codeTableCache = GetCodeTableCache(languageCode);

                if (!codeTableCache.TryGetValue(type, out collection))
                {
                    lock (_addLock) //don't thread table loads
                    {
                        collection = ServiceWrapper.CodeClient.GetCodeTable(languageCode, type, null);
                        codeTableCache.Add(type, collection);
                    }
                }
            }

            return collection;
        }
        /// <summary>
        /// Use this method when need to retrieve data by parent column name and value.
        /// </summary>
        /// <param name="languageCode"></param>
        /// <param name="type"></param>
        /// <param name="parentColumnName"></param>
        /// <param name="parentColumnValue"></param>
        /// <returns></returns>
        public static CodeCollection GetCodeTable(String languageCode, CodeTableType type, String parentColumnName, String parentColumnValue)
        {
            CodeCollection collection = null;

            //not caching code system values anymore
            if (type == CodeTableType.System)
                collection = ServiceWrapper.CodeClient.GetCodeTable(languageCode, type, null, parentColumnName, parentColumnValue);
            else
            {
                Dictionary<CodeTableType, CodeCollection> codeTableCache = GetCodeTableCache(languageCode);

                if (!codeTableCache.TryGetValue(type, out collection))
                {
                    lock (_addLock) //don't thread table loads
                    {
                        collection = ServiceWrapper.CodeClient.GetCodeTable(languageCode, type, null, parentColumnName, parentColumnValue);
                        codeTableCache.Add(type, collection);
                    }
                }
            }

            return collection;
        }
        public static CodeCollection GetCodeTable(String languageCode, CodeTableType type, String code)
        {
            CodeCollection collection = new CodeCollection();

            if (code == null)
                collection = GetCodeTable(languageCode, type);
            else
            {
                CodeCollection temp = GetCodeTable(languageCode, type);

                if (temp != null && temp[code] != null)
                    collection.Add(temp[code]);
            }

            return collection;
        }
        /// <summary>
        /// Use this method when need to retrieve data by parent column name and value.
        /// </summary>
        /// <param name="languageCode"></param>
        /// <param name="type"></param>
        /// <param name="code"></param>
        /// <param name="parentColumnName"></param>
        /// <param name="parentColumnValue"></param>
        /// <returns></returns>
        public static CodeCollection GetCodeTable(String languageCode, CodeTableType type, String code, String parentColumnName, String parentColumnValue)
        {
            CodeCollection collection = new CodeCollection();

            if (code == null)
                collection = GetCodeTable(languageCode, type, parentColumnName, parentColumnValue);
            else
            {
                CodeCollection temp = GetCodeTable(languageCode, type, parentColumnName, parentColumnValue);

                if (temp != null && temp[code] != null)
                    collection.Add(temp[code]);
            }

            return collection;
        }
        public static Dictionary<CodeTableType, CodeCollection> GetCodeTableCache(String languageCode)
        {
            Dictionary<CodeTableType, CodeCollection> codeTableCache = null;

            if (!Cache.TryGetValue(languageCode, out codeTableCache))
            {
                lock (Cache)
                {
                    codeTableCache = new Dictionary<CodeTableType, CodeCollection>();
                    _cache.Add(languageCode, codeTableCache);
                }
            }

            return codeTableCache;
        }
        public static void ResetCodeTableCache()
        {
            _cache = null;
        }
        public static String GetCodeDescription(String languageCode, CodeTableType type, String code)
        {
            CodeCollection codeObj = GetCodeTable(languageCode, type, code);
            return codeObj[0].Description.ToString();
        }
        public static void PopulateWithDefaultCountryCanada(ICodeControl combo, String languageCode)
        {
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type);
            PopulateCountryControlWithCanada(combo, languageCode, type);
        }
        public static void PopulateCountryControlWithCanada(ICodeControl combo, String languageCode, CodeTableType type)
        {
            if (combo.ReadOnly || combo is GridKeyValueControl)
                combo.DataSource = GetCodeTable(languageCode, type, "CA");
        }
        public static void PopulateControl(ICodeControl combo, String languageCode)
        {
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type);
            PopulateControl(combo, languageCode, type);
        }
        public static CodeCollection GetCode(String codeTableName, String languageCode, String readOnlyValue)
        {
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), codeTableName);
            if (readOnlyValue != null)
                return GetCodeTable(languageCode, type, readOnlyValue);
            else
            {
                CodeCollection collection = new CodeCollection();

                foreach (CodeObject code in GetCodeTable(languageCode, type, null))
                {
                    if (code.ActiveFlag)
                        collection.Add(code);
                }

                return collection;
            }
        }
        /// <summary>
        /// Use this method when need data by parent column name and value.
        /// </summary>
        /// <param name="combo"></param>
        /// <param name="languageCode"></param>
        /// <param name="parentColumnName"></param>
        /// <param name="parentColumnValue"></param>
        public static void PopulateControl(ICodeControl combo, String languageCode, String parentColumnName, String parentColumnValue)
        {
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type);
            PopulateControl(combo, languageCode, type, parentColumnName, parentColumnValue);
        }
        public static void PopulateControl(ICodeControl combo, String languageCode, CodeTableType type)
        {
            if (combo.ReadOnly || combo is GridKeyValueControl)
                combo.DataSource = GetCodeTable(languageCode, type, combo.SelectedValue);
            else
            {
                CodeCollection collection = new CodeCollection();

                foreach (CodeObject code in GetCodeTable(languageCode, type, null))
                {
                    if (code.ActiveFlag)
                        collection.Add(code);
                }

                combo.DataSource = collection;
            }
        }
        /// <summary>
        /// Use this method when need data by parent column name and value.
        /// </summary>
        /// <param name="combo"></param>
        /// <param name="languageCode"></param>
        /// <param name="type"></param>
        /// <param name="parentColumnName"></param>
        /// <param name="parentColumnValue"></param>
        public static void PopulateControl(ICodeControl combo, String languageCode, CodeTableType type, String parentColumnName, String parentColumnValue)
        {
            if (combo.ReadOnly || combo is GridKeyValueControl)
                combo.DataSource = GetCodeTable(languageCode, type, combo.SelectedValue, parentColumnName, parentColumnValue);
            else
            {
                CodeCollection collection = new CodeCollection();

                foreach (CodeObject code in GetCodeTable(languageCode, type, null, parentColumnName, parentColumnValue))
                {
                    if (code.ActiveFlag)
                        collection.Add(code);
                }

                combo.DataSource = collection;
            }
        }
        public static void PopulateControlWithoutFiltering(ICodeControl combo, String languageCode)
        {
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type);
            PopulateControlWithoutFiltering(combo, languageCode, type);
        }
        public static void PopulateControlWithoutFiltering(ICodeControl combo, String languageCode, CodeTableType type)
        {
            CodeCollection collection = new CodeCollection();

            foreach (CodeObject code in GetCodeTable(languageCode, type, null))
                collection.Add(code);

            combo.DataSource = collection;
        }
        public static void PopulateEmployeePositionStatusCodeControl(ICodeControl combo, String languageCode)
        {
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type);
            PopulateEmployeePositionStatusCodeControl(combo, languageCode, type);
        }
        public static void PopulateEmployeePositionStatusCodeControl(ICodeControl combo, String languageCode, CodeTableType type)
        {
            CodeCollection collection = new CodeCollection();

            foreach (CodeObject code in GetCodeTable(languageCode, type, null))
            {
                //these codes arent in a combo, so their active flag is 0.  But "default" status is set to active to filter it out form employee search status checkbox
                if (!code.ActiveFlag)
                    collection.Add(code);
            }

            combo.DataSource = collection;
        }

        public static void PopulateEmployeeSkillCodeControl(ICodeControl combo, String languageCode)
        {
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type);
            PopulateEmployeeSkillCodeControl(combo, languageCode, type);
        }
        public static void PopulateEmployeeSkillCodeControl(ICodeControl combo, String languageCode, CodeTableType type)
        {
            CodeCollection collection = new CodeCollection();

            foreach (CodeObject code in GetCodeTable(languageCode, type, null))
            {
                //only pick the active codes
                if (code.ActiveFlag)
                    collection.Add(code);
            }

            combo.DataSource = collection;
        }

        public static void PopulateComboWithReason(ICodeControl combo, String languageCode, String eventCode)
        {
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type);
            PopulateReasonControl(combo, languageCode, type, eventCode);
        }
        public static void PopulateReasonControl(ICodeControl combo, String languageCode, CodeTableType type, String eventCode)
        {
            if (combo.ReadOnly || combo is GridKeyValueControl)
                combo.DataSource = ServiceWrapper.CodeClient.GetCodeTable(languageCode, type, combo.SelectedValue);
            else
            {
                CodeCollection collection = new CodeCollection();

                foreach (CodeObject code in ServiceWrapper.CodeClient.GetCodeTable(languageCode, type, null))
                {
                    if (code.ActiveFlag && eventCode == code.ParentCode)
                        collection.Add(code);
                }

                combo.DataSource = collection;
            }
        }
        public static void PopulateComboWithStatus(ICodeControl combo, String languageCode)
        {
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type);
            PopulateStatusControl(combo, languageCode, type);
        }
        public static void PopulateStatusControl(ICodeControl combo, String languageCode, CodeTableType type)
        {
            CodeCollection collection = new CodeCollection();

            foreach (CodeObject code in ServiceWrapper.CodeClient.GetCodeTable(languageCode, type, "AC")) //should return just the 1 object
            {
                if (code.Code == "AC")
                {
                    collection.Add(code);
                    break;
                }
            }

            combo.DataSource = collection;
        }
        public static void PopulateControlWithStatusBaseOnEvent(ICodeControl combo, String languageCode, ICodeControl eventActionControl, String prevStatus, out String statusToDisplay)
        {
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type);
            PopulateStatusControlWithStatusBasedOnEvent(combo, languageCode, type, eventActionControl, prevStatus, out statusToDisplay);
        }
        public static void PopulateStatusControlWithStatusBasedOnEvent(ICodeControl combo, String languageCode, CodeTableType type, ICodeControl eventActionControl, String prevStatus, out String statusToDisplay)
        {
            //get the code table type for the action/event combo
            CodeTableType actionEventType = (CodeTableType)Enum.Parse(typeof(CodeTableType), eventActionControl.Type);
            String statusToUse = "";
            statusToDisplay = "";

            //find the status that this event should use
            foreach (CodeObject code in ServiceWrapper.CodeClient.GetCodeTable(languageCode, actionEventType, eventActionControl.SelectedValue.ToString()))
                statusToUse = code.ParentCode;

            CodeCollection collection = new CodeCollection();

            foreach (CodeObject code in ServiceWrapper.CodeClient.GetCodeTable(languageCode, type, null))
            {
                //we use the prev status
                if (statusToUse == "X")
                {
                    if (code.Code == prevStatus)
                    {
                        collection.Add(code);
                        statusToDisplay = code.Code;
                        break;
                    }
                }
                //we use the status based on action/event
                else
                {
                    if (code.Code == statusToUse)
                    {
                        collection.Add(code);
                        statusToDisplay = code.Code;
                        break;
                    }
                }
            }

            combo.DataSource = collection;
        }
        public static void PopulateComboWithEvent(ICodeControl combo, String languageCode, String status, String actionCode, bool IsEditMode)
        {
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type);
            PopulateEventControl(combo, languageCode, type, status, actionCode, IsEditMode);
        }
        public static void PopulateEventControl(ICodeControl combo, String languageCode, CodeTableType type, String status, String actionCode, bool IsEditMode)
        {
            if (combo.ReadOnly || combo is GridKeyValueControl)
                combo.DataSource = ServiceWrapper.CodeClient.GetCodeTable(languageCode, type, combo.SelectedValue);
            else
            {
                CodeCollection collection = new CodeCollection();

                foreach (CodeObject code in ServiceWrapper.CodeClient.GetCodeTable(languageCode, type, null))
                {
                    //if the employee Status is Active then the only Events available are non-system events 
                    //the Employee status in the position will remain as ACTIVE for the new position entered
                    if (status == "AC")
                    {
                        if (code.ActiveFlag && !code.SystemFlag)
                            collection.Add(code);
                        else if (IsEditMode && code.Code == actionCode) //if we are in edit mode, allow the prev action to be used (in the case we are just updating other information, we need this in the combo)
                            collection.Add(code);
                    }
                    //if the employee Status is SL70/SL100/LD0 then the only Events available are non-system events and RFL (return from leave).
                    else if (status == "SL100" || status == "SL70" || status == "LD0")
                    {
                        if ((code.ActiveFlag && !code.SystemFlag) || code.Code == "RFL")
                            collection.Add(code);
                        else if (IsEditMode && code.Code == actionCode) //if we are in edit mode, allow the prev action to be used (in the case we are just updating other information, we need this in the combo)
                            collection.Add(code);
                    }
                    //if the employee Status is Leave, Long term disability, or Benefited Leave then the available Events are the non-system events plus Rehire.
                    else if (status == "L" || status == "BL")
                    {
                        if ((code.ActiveFlag && !code.SystemFlag) || (code.Code == "RH" || code.Code == "RHRESET"))
                            collection.Add(code);
                        else if (IsEditMode && code.Code == actionCode) //if we are in edit mode, allow the prev action to be used (in the case we are just updating other information, we need this in the combo)
                            collection.Add(code);
                    }
                    else if (status == "TER")
                    {
                        if (code.ActiveFlag && (code.Code == "RH" || code.Code == "RHRESET"))
                            collection.Add(code);
                        else if (IsEditMode && code.Code == actionCode) //this may not be needed, depends if we can edit a termination record or not
                            collection.Add(code);
                    }
                    else if (status == null) //add Wizard will have null status
                    {
                        if (code.Code == "NEW")
                        {
                            collection.Add(code);
                            break;
                        }
                    }
                }

                combo.DataSource = collection;
            }
        }
        public static void PopulateYears(ICodeControl combo, string languageCode)
        {
            int currentYear = DateTime.Now.Year;
            CodeCollection codeCollection = new CodeCollection();

            for (int i = 2013; i <= currentYear; i++)
            {
                codeCollection.Add(new CodeObject()
                {
                    Code = i.ToString(),
                    Description = i.ToString(),
                    LanguageCode = languageCode
                });
            }

            combo.DataSource = codeCollection;
        }
        public static String[] PopulateAllowedFileExtensions(String languageCode)
        {
            CodeCollection collection = new CodeCollection();
            String[] allowedFileExtensions = null;

            foreach (CodeObject code in GetCodeTable(languageCode, CodeTableType.FileTypeCode, null))
                collection.Add(code);

            allowedFileExtensions = new String[collection.Count];

            for (int i = 0; i < collection.Count; i++)
                allowedFileExtensions[i] = collection[i].Description;

            return allowedFileExtensions;
        }
        public static String CheckSINValidation(string SIN, string governmentIdentificationNumberTypeCode, long employeeId, string employeeNumber)
        {
            bool isValid = false;
            string returnVal = "true";

            if (!String.IsNullOrEmpty(SIN.Replace("_", "").Replace("-", "")))
            {
                isValid = ServiceWrapper.HumanResourcesClient.ValidateSIN(SIN, governmentIdentificationNumberTypeCode);
                //if a valid SIN was entered...
                if (isValid)
                {
                    if (ApplicationParameter.ShowSinWarning == "on")
                    {
                        //first check if we have a duplicate SIN in the system
                        bool duplicateFound = ServiceWrapper.HumanResourcesClient.CheckDatabaseForDuplicateSin(SIN, employeeId, employeeNumber);
                        if (duplicateFound)
                        {
                            //show a warning or give an error?
                            if (ApplicationParameter.SinBusinessRule == "error")
                                returnVal = "false"; //do not allow duplilcate SIN so set the validator "IsValid" to false
                            else if (ApplicationParameter.SinBusinessRule == "warning")
                                returnVal = "warning"; //client side will prompt user to continue or not
                        }
                    }
                }
                else
                    returnVal = "false";
            }
            else
                returnVal = "false";

            return returnVal;
        }
        public static void PopulatePositionActionControl(ICodeControl combo, String languageCode)
        {
            //called form the Terminations page, different rules apply than in the employee position
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type);
            PopulatePositionActionControl(combo, languageCode, type);
        }
        public static void PopulatePositionActionControl(ICodeControl combo, String languageCode, CodeTableType type)
        {
            if (combo.ReadOnly || combo is GridKeyValueControl)
                combo.DataSource = GetCodeTable(languageCode, type, combo.SelectedValue);
            else
            {
                CodeCollection collection = new CodeCollection();

                foreach (CodeObject code in GetCodeTable(languageCode, type, null))
                {
                    if ((code.ActiveFlag && code.SystemFlag) && code.ParentCode != "AC")
                        collection.Add(code);
                }

                combo.DataSource = collection;
            }
        }
        public static void PopulateComboBoxWithPhoneType(ICodeControl control, String languageCode)
        {
            PopulateComboBoxWithContactType(control, "PHONE"); //**HACK**FIX HARD CODE*/
        }
        public static void PopulateComboBoxWithEmailType(ICodeControl control)
        {
            PopulateComboBoxWithContactType(control, "EMAIL"); //**HACK**FIX HARD CODE*/
        }
        public static void PopulateComboBoxWithTypeForWizardRemovingTypesInUse(ICodeControl combo, IDataItemCollection<IDataItem> DataItemCollection, String languageCode, String WizardItemName, bool leaveSelectedItem, String paycodeTypeCode = "")
        {
            String type = null;

            switch (WizardItemName)
            {
                case "PersonPhoneControl":
                    type = "PHONE";
                    break;
                case "PersonEmailControl":
                    type = "EMAIL";
                    break;
                case "AddressDetailControl":
                    type = "ADDRESS";
                    break;
                case "EmployeePaycodeModuleControl":
                    type = paycodeTypeCode;
                    break;
                default:
                    type = null;
                    break;
            }

            if (type == null)
                throw new NotImplementedException();

            CodeCollection codeCollection = null;

            if (type == "PHONE" || type == "EMAIL")
            {
                codeCollection = ServiceWrapper.CodeClient.GetContactChannelTypeCode(type);

                if (DataItemCollection.Count > 0)
                {
                    foreach (PersonContactChannel channel in (PersonContactChannelCollection)DataItemCollection) //Active Cached Collection of items
                    {
                        foreach (CodeObject codeObject in codeCollection)
                        {
                            if (channel.ContactChannelTypeCode == codeObject.Code)
                            {
                                if (combo.SelectedValue != null && leaveSelectedItem)
                                {
                                    if (codeObject.Code != combo.SelectedValue)
                                        codeCollection.Remove(codeObject);
                                    break;
                                }
                                else
                                {
                                    codeCollection.Remove(codeObject);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else if (type == "ADDRESS")
            {
                codeCollection = new CodeCollection();

                foreach (CodeObject code in GetCodeTable(languageCode, (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type), null))
                {
                    if (code.ActiveFlag)
                        codeCollection.Add(code);
                }

                if (DataItemCollection.Count > 0)
                {
                    foreach (PersonAddress personAddress in (PersonAddressCollection)DataItemCollection) //Active Cached Collection of items
                    {
                        foreach (CodeObject codeObject in codeCollection)
                        {
                            if (personAddress.PersonAddressTypeCode == codeObject.Code)
                            {
                                if (combo.SelectedValue != null && leaveSelectedItem)
                                {
                                    if (codeObject.Code != combo.SelectedValue)
                                        codeCollection.Remove(codeObject);
                                    break;
                                }
                                else
                                {
                                    codeCollection.Remove(codeObject);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                codeCollection = ServiceWrapper.CodeClient.GetPaycodesByType(type);
                //remove non-active codes
                FilterByActiveFlag(codeCollection, true);

                if (type == "1")
                    codeCollection = ServiceWrapper.CodeClient.RemoveNonRecurringIncomeCodes(codeCollection);

                if (DataItemCollection.Count > 0)
                {
                    foreach (EmployeePaycode paycode in (EmployeePaycodeCollection)DataItemCollection) //Active Cached Collection of items
                    {
                        if (paycode.PaycodeTypeCode == type) //ignore paycodes not associated with the type that has sent us to this method
                        {
                            foreach (CodeObject codeObject in codeCollection)
                            {
                                if (paycode.PaycodeCode == codeObject.Code)
                                {
                                    if (combo.SelectedValue != null && leaveSelectedItem)
                                    {
                                        if (codeObject.Code != combo.SelectedValue)
                                            codeCollection.Remove(codeObject);
                                        break;
                                    }
                                    else
                                    {
                                        codeCollection.Remove(codeObject);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            combo.DataSource = codeCollection;
        }

        public static void PopulateComboBoxWithSeqCodesRemovingTypesInUseWizard(ICodeControl combo, String languageCode, EmployeeBankingCollection bankcollection)
        {
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type);
            CodeCollection collection = new CodeCollection();

            foreach (CodeObject code in GetCodeTable(languageCode, type, null))
            {
                if (code.ActiveFlag)
                    collection.Add(code);
            }

            //get the sequence codes associated to this person
            EmployeeBankingCollection temp = bankcollection;

            //filter out the combo box to show types not in use
            for (int i = collection.Count; i > 0; i--)
            {
                foreach (EmployeeBanking item in temp)
                {
                    if (item.CodeEmployeeBankingSequenceCode == collection[i - 1].Code) //employee is already assigned this sequence code
                    {
                        collection.RemoveAt(i - 1); //remove this option
                        break;
                    }
                }
            }

            combo.DataSource = collection;
        }
        public static void PopulateComboBoxWithSeqCodesRemovingTypesInUse(ICodeControl combo, String languageCode, long employeeId)
        {
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type);
            CodeCollection collection = new CodeCollection();

            foreach (CodeObject code in GetCodeTable(languageCode, type, null))
            {
                if (code.ActiveFlag)
                    collection.Add(code);
            }

            //get the sequence codes associated to this person
            EmployeeBankingCollection temp = ServiceWrapper.HumanResourcesClient.GetEmployeeBanking(employeeId);

            //filter out the combo box to show types not in use
            for (int i = collection.Count; i > 0; i--)
            {
                foreach (EmployeeBanking item in temp)
                {
                    if (item.CodeEmployeeBankingSequenceCode == collection[i - 1].Code) //employee is already assigned this sequence code
                    {
                        collection.RemoveAt(i - 1); //remove this option
                        break;
                    }
                }
            }

            combo.DataSource = collection;
        }
        public static void PopulateComboBoxWithSeqCodesRemovingTypesInUseLeavingSelected(ICodeControl combo, String languageCode, long employeeId, String selectedCode)
        {
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type);
            CodeCollection collection = new CodeCollection();

            foreach (CodeObject code in GetCodeTable(languageCode, type, null))
            {
                if (code.ActiveFlag)
                    collection.Add(code);
            }

            //get the sequence codes associated to this person
            EmployeeBankingCollection temp = ServiceWrapper.HumanResourcesClient.GetEmployeeBanking(employeeId);

            //filter out the combo box to show types not in use
            for (int i = collection.Count; i > 0; i--)
            {
                foreach (EmployeeBanking item in temp)
                {
                    if (item.CodeEmployeeBankingSequenceCode == collection[i - 1].Code && item.CodeEmployeeBankingSequenceCode != selectedCode) //employee is already assigned this sequence code
                    {
                        collection.RemoveAt(i - 1); //remove this option
                        break;
                    }
                }
            }

            combo.DataSource = collection;
        }
        public static void PopulateComboBoxWithPhoneTypeRemovingTypesInUse(ICodeControl control, String languageCode, long personId)
        {
            PopulateComboBoxWithContactTypeRemovingTypesInUse(control, languageCode, "PHONE", personId); //**HACK**FIX HARD CODE*/
        }
        public static void PopulateComboBoxWithEmailTypeRemovingTypesInUse(ICodeControl control, String languageCode, long personId)
        {
            PopulateComboBoxWithContactTypeRemovingTypesInUse(control, languageCode, "EMAIL", personId); //**HACK**FIX HARD CODE*/
        }
        public static void PopulateComboBoxWithContactTypeRemovingTypesInUse(ICodeControl combo, String languageCode, String type, long personId)
        {
            combo.DataSource = ServiceWrapper.CodeClient.GetContactChannelTypeCodeRemovingTypesInUse(type, personId);
        }
        public static void PopulateComboBoxWithEmailTypeRemovingTypesInUseLeavingSelected(ICodeControl control, String languageCode, long personId, String selectedCode)
        {
            PopulateComboBoxWithContactTypeRemovingTypesInUseLeavingSelected(control, languageCode, "EMAIL", personId, selectedCode); //**HACK**FIX HARD CODE*/
        }
        public static void PopulateComboBoxWithContactTypeRemovingTypesInUseLeavingSelected(ICodeControl combo, String languageCode, String type, long personId, String selectedCode)
        {
            combo.DataSource = ServiceWrapper.CodeClient.GetContactChannelTypeCodeRemovingTypesInUseLeavingSelected(type, personId, selectedCode);
        }
        //phone - calls method above
        public static void PopulateComboBoxWithPhoneTypeRemovingTypesInUseLeavingSelected(ICodeControl control, String languageCode, long personId, String selectedCode)
        {
            PopulateComboBoxWithContactTypeRemovingTypesInUseLeavingSelected(control, languageCode, "PHONE", personId, selectedCode); //**HACK**FIX HARD CODE*/
        }
        public static void PopulateComboBoxWithContactType(ICodeControl combo, String type)
        {
            String value = null;

            if (combo.ReadOnly || combo is GridKeyValueControl)
                value = combo.SelectedValue;

            Object obj = ServiceWrapper.CodeClient.GetContactChannelTypeCode(type);

            combo.DataSource = obj;
        }
        //Load the code table index data in a dropdown
        public void PopulateComboBoxWithCodeTableIndex(ComboBoxControl control)
        {
            CodeCollection collection = new CodeCollection();

            foreach (CodeTableIndex table in ServiceWrapper.CodeClient.GetCodeTableIndex(null))
                collection.Add(new CodeObject() { Code = table.TableName, Description = table.TableDescription });

            PopulateComboBox(control, collection);
        }
        public void PopulateComboBoxWithImportExport(ICodeControl control)
        {
            control.DataSource = GetImportExportCodeCollection();
        }
        public static CodeCollection GetImportExportCodeCollection()
        {
            CodeCollection collection = new CodeCollection();

            foreach (ImportExportSummary importExport in ServiceWrapper.HumanResourcesClient.GetImportExportSummary())
            {
                if (importExport.ActiveFlag)
                    collection.Add(new CodeObject() { Code = importExport.Key, Description = importExport.Description });
            }

            return collection;
        }
        public void PopulateComboBoxWithRemittanceImportExport(ICodeControl control)
        {
            CodeCollection collection = new CodeCollection();

            foreach (ImportExportSummary importExport in ServiceWrapper.HumanResourcesClient.GetImportExportSummary())
            {
                if (importExport.ActiveFlag && importExport.Label == "RemittanceImp")
                {
                    collection.Add(new CodeObject() { Code = importExport.Key, Description = importExport.Description });
                    break;
                }
            }

            control.DataSource = collection;
        }
        public static void PopulateVendorComboControl(ICodeControl control)
        {
            CodeCollection collection = new CodeCollection();

            foreach (Vendor vendor in ServiceWrapper.HumanResourcesClient.GetVendor(-1)) //-1 gets all vendors
                collection.Add(new CodeObject() { Code = vendor.VendorId.ToString(), Description = vendor.Name });

            control.DataSource = collection;
        }
        public static void PopulateVendorComboControlFilterByType(ICodeControl control, string garnishmentTypeCode)
        {
            CodeCollection collection = new CodeCollection();

            foreach (Vendor vendor in ServiceWrapper.HumanResourcesClient.GetVendor(-1)) //-1 gets all vendors
            {
                if (vendor.GarnishmentTypeCode.ToUpper().Equals(garnishmentTypeCode.ToUpper()))
                    collection.Add(new CodeObject() { Code = vendor.VendorId.ToString(), Description = vendor.Name });
            }

            control.DataSource = collection;
        }
        public static void PopulateComboBoxWithReport(ICodeControl control)
        {
            CodeCollection codeCollection = new CodeCollection();
            ReportCollection reportCollection = ServiceWrapper.ReportClient.GetReportRecord(null);

            foreach (BusinessLayer.BusinessObjects.Report report in reportCollection)
                codeCollection.Add(new CodeObject() { Code = report.Key, Description = report.Name });

            control.DataSource = codeCollection;
        }
        public void PopulateComboBoxLabelWithOrganizationUnitLevel(ComboBoxControl control)
        {
            CodeCollection collection = new CodeCollection();

            foreach (OrganizationUnitLevel level in ServiceWrapper.HumanResourcesClient.GetOrganizationUnitLevel())
                collection.Add(new CodeObject() { Code = level.Key, Description = level.Description });

            control.LabelText = collection[control.LabelValue].Description;
        }

        public static string GetOrganizationLevelDescription(long organizationUnitLevel, string languageCode)
        {
            OrganizationUnitLevelDescriptionCollection collection = ServiceWrapper.HumanResourcesClient.GetOrganizationUnitLevelDescription(organizationUnitLevel);

            IEnumerable<OrganizationUnitLevelDescription> row = from item in collection
                                                                where item.LanguageCode == languageCode
                                                                select item;
            //this query should return just two rows, return the one corresponding to the langauge passed in
            return row.ElementAt(0).LevelDescription;
        }

        public static void PopulateComboBoxWithSalaryPlanGrades(ICodeControl control, string languageCode, long salaryPlanId, string organizationUnitLevelId)
        {
            CodeCollection coll = new CodeCollection();

            SalaryPlanGradeOrgUnitDescriptionComboCollection orgs = ServiceWrapper.HumanResourcesClient.PopulateComboBoxWithSalaryPlanGrades(salaryPlanId, Convert.ToInt64(organizationUnitLevelId));
            foreach (SalaryPlanGradeOrgUnitDescriptionCombo item in orgs)
            {
                CodeObject obj = new CodeObject() { Code = item.OrganizationUnitId.ToString(), Description = item.ImportExternalIdentifier.ToString() + "-" + ((languageCode == "EN") ? item.EnglishDescription : item.FrenchDescription) };
                coll.Add(obj);
            }

            control.DataSource = coll;
        }

        public void PopulateComboBoxWithOrganizationUnit(System.Web.UI.WebControls.WebControl control, long? parentOrganizationUnitHierarchyId)
        {
            ComboBoxControl combo = (ComboBoxControl)control;
            CodeCollection convertedCollection = new CodeCollection();
            OrganizationUnitCollection collection = null;

            if (combo.ReadOnly)
                collection = ServiceWrapper.HumanResourcesClient.GetOrganizationUnit(Convert.ToInt64(combo.SelectedValue));
            else
                collection = ServiceWrapper.HumanResourcesClient.GetOrganizationUnitByParentOrganizationUnitId(parentOrganizationUnitHierarchyId);

            foreach (OrganizationUnit item in collection)
            {
                if (!item.ActiveFlag && !(combo.ReadOnly))
                {
                    //convertedCollection.Remove(obj);
                }
                else
                {
                    CodeObject obj = new CodeObject() { Code = item.Key, Description = item.Description };
                    convertedCollection.Add(obj);
                }
            }

            if (control is ComboBoxControl)
                PopulateComboBox(combo, convertedCollection);
        }
        public void PopulateComboBoxWithOrganizationUnitByLevel(System.Web.UI.WebControls.WebControl control, long? levelId)
        {
            ComboBoxControl combo = (ComboBoxControl)control;
            CodeCollection convertedCollection = new CodeCollection();
            OrganizationUnitCollection collection = null;

            collection = ServiceWrapper.HumanResourcesClient.GetOrganizationUnitByOrganizationUnitLevelId(new OrganizationUnitCriteria() { OrganizationUnitLevelId = levelId });
            foreach (OrganizationUnit item in collection)
            {
                if (!item.ActiveFlag && !(combo.ReadOnly))
                {
                    //convertedCollection.Remove(obj);
                }
                else
                {
                    CodeObject obj = new CodeObject() { Code = item.Key, Description = item.Description };
                    convertedCollection.Add(obj);
                }
            }

            if (control is ComboBoxControl)
                PopulateComboBox(combo, convertedCollection);
        }
        public static void PopulateComboBoxWithUnion(ComboBoxControl control, String languageCode)
        {
            LabourUnionSummaryCollection collection = ServiceWrapper.HumanResourcesClient.GetLabourUnion(null);
            CodeCollection convertedCollection = new CodeCollection();

            foreach (LabourUnionSummary union in collection)
            {
                convertedCollection.Add(new CodeObject()
                {
                    Code = union.LabourUnionId.ToString(),
                    Description = (languageCode == "EN") ? union.EnglishDescription : union.FrenchDescription,
                    LanguageCode = (languageCode == "EN") ? "EN" : "FR"
                });
            }

            control.DataSource = convertedCollection;
        }
        public static void PopulateComboBoxWithPolicy(ComboBoxControl control, String languageCode)
        {
            AccrualPolicyCollection collection = ServiceWrapper.HumanResourcesClient.GetAccrualPolicy(null);
            CodeCollection convertedCollection = new CodeCollection();

            foreach (AccrualPolicy policy in collection)
            {
                convertedCollection.Add(new CodeObject()
                {
                    Code = policy.AccrualPolicyId.ToString(),
                    Description = (languageCode == "EN") ? policy.EnglishDescription : policy.FrenchDescription,
                    LanguageCode = (languageCode == "EN") ? "EN" : "FR"
                });
            }

            control.DataSource = convertedCollection;
        }
        public static void PopulateComboBoxWithBenefitPolicy(ComboBoxControl control, String languageCode)
        {
            BenefitPolicyCollection collection = ServiceWrapper.HumanResourcesClient.GetBenefitPolicy(null);
            CodeCollection convertedCollection = new CodeCollection();

            foreach (BenefitPolicy policy in collection)
            {
                convertedCollection.Add(new CodeObject()
                {
                    Code = policy.BenefitPolicyId.ToString(),
                    Description = (languageCode == "EN") ? policy.EnglishDescription : policy.FrenchDescription,
                    LanguageCode = (languageCode == "EN") ? "EN" : "FR"
                });
            }

            control.DataSource = convertedCollection;
        }
        public static void PopulateComboBoxWithBenefitPolicy(ICodeControl control, String languageCode)
        {
            BenefitPolicyCollection collection = ServiceWrapper.HumanResourcesClient.GetBenefitPolicy(null);
            CodeCollection convertedCollection = new CodeCollection();

            foreach (BenefitPolicy policy in collection)
            {
                convertedCollection.Add(new CodeObject()
                {
                    Code = policy.BenefitPolicyId.ToString(),
                    Description = (languageCode == "EN") ? policy.EnglishDescription : policy.FrenchDescription,
                    LanguageCode = (languageCode == "EN") ? "EN" : "FR"
                });
            }

            control.DataSource = convertedCollection;
        }
        public static void PopulateProvince(ICodeControl combo)
        {
            combo.DataSource = ServiceWrapper.CodeClient.GetProvinceStateCode("CA"); //**HACK**FIX HARD CODE*/
        }
        public static void PopulateComboBoxWithAddressTypeRemovingTypesInUse(ICodeControl combo, String languageCode, long personId)
        {
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type);
            CodeCollection collection = new CodeCollection();

            foreach (CodeObject code in GetCodeTable(languageCode, type, null))
            {
                if (code.ActiveFlag)
                    collection.Add(code);
            }

            //get the address types associated to this person
            PersonAddressCollection temp = ServiceWrapper.HumanResourcesClient.GetPersonAddress(personId);

            //filter out the combo box to show types not in use
            for (int i = collection.Count; i > 0; i--)
            {
                foreach (PersonAddress item in temp)
                {
                    if (item.PersonAddressTypeCode == collection[i - 1].Code) //employee is already assigned this contact type
                    {
                        collection.RemoveAt(i - 1); //remove this option
                        break;
                    }
                }
            }

            combo.DataSource = collection;
        }
        public static void PopulateComboBoxWithAddressTypeRemovingTypesInUseLeavingSelected(ICodeControl combo, String languageCode, long personId, String selectedCode)
        {
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type);
            CodeCollection collection = new CodeCollection();

            foreach (CodeObject code in GetCodeTable(languageCode, type, null))
            {
                if (code.ActiveFlag)
                    collection.Add(code);
            }

            //get the address types associated to this person
            PersonAddressCollection temp = ServiceWrapper.HumanResourcesClient.GetPersonAddress(personId);

            //filter out the combo box to show types not in use
            for (int i = collection.Count; i > 0; i--)
            {
                foreach (PersonAddress item in temp)
                {
                    if (item.PersonAddressTypeCode == collection[i - 1].Code && item.PersonAddressTypeCode != selectedCode) //employee is already assigned this contact type
                    {
                        collection.RemoveAt(i - 1); //remove this option
                        break;
                    }
                }
            }

            combo.DataSource = collection;
        }
        public static void PopulateComboBoxWithCanadianProvinces(ICodeControl combo, String languageCode)
        {
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type);
            CodeCollection tempCC = new CodeCollection();

            //This will populate a dropdown with Canadian Provinces
            if (combo.ReadOnly || combo is GridKeyValueControl)
            {
                tempCC = GetCodeTable(languageCode, type, combo.SelectedValue);

                CodeCollection provinces = new CodeCollection();

                //filter out by code_country_cd = 'CA'
                foreach (CodeObject tmp in tempCC)
                {
                    if (tmp.ParentCode == "CA")
                        provinces.Add(tmp);
                }

                combo.DataSource = provinces;
            }
            else
            {
                CodeCollection collection = new CodeCollection();

                foreach (CodeObject code in GetCodeTable(languageCode, type, null))
                {
                    if (code.ActiveFlag && code.ParentCode == "CA")
                        collection.Add(code);
                }

                combo.DataSource = collection;
            }
        }
        public static bool CheckRecurringIncomeCode()
        {
            return ServiceWrapper.HumanResourcesClient.CheckRecurringIncomeCode();
        }
        public static CodeCollection GetPaycodesByType(String employeePaycodeTypeCode, String languageCode)
        {
            CodeCollection collection = new CodeCollection();
            collection.Load(ServiceWrapper.CodeClient.GetPaycodesByType(employeePaycodeTypeCode));

            return collection;
        }
        public static void PopulateComboBoxWithPaycodesByType(ICodeControl combo, String employeePaycodeTypeCode)
        {
            //This will populate a dropdown with Paycodes by their type
            combo.DataSource = ServiceWrapper.CodeClient.GetPaycodesByType(employeePaycodeTypeCode);
        }
        public static void PopulateComboBoxWithPaycodesByType(ICodeControl combo, String employeePaycodeTypeCode, bool showActive)
        {
            combo.DataSource = FilterByActiveFlag(ServiceWrapper.CodeClient.GetPaycodesByType(employeePaycodeTypeCode), showActive);
        }
        public static void PopulateComboBoxWithPaycodesByTypeRemovingPaycodesInUse(ComboBoxControl combo, String employeePaycodeTypeCode, String languageCode, long? employeeId)
        {
            //This will populate a dropdown with Paycodes by their type, removing those already assigned to an employee
            combo.DataSource = ServiceWrapper.CodeClient.GetPaycodesByTypeRemovingPaycodesInUse(employeePaycodeTypeCode, employeeId);
        }
        public static void PopulateComboBoxWithPaycodesByTypeRemovingPaycodesInUse(ComboBoxControl combo, String employeePaycodeTypeCode, String languageCode, long? employeeId, bool showActive)
        {
            combo.DataSource = FilterByActiveFlag(ServiceWrapper.CodeClient.GetPaycodesByTypeRemovingPaycodesInUse(employeePaycodeTypeCode, employeeId), showActive);
        }
        public static void PopulateComboBoxWithPaycodesByTypeRemovingGlobalPaycodesInUse(ComboBoxControl combo, String employeePaycodeTypeCode, bool showActive)
        {
            combo.DataSource = FilterByActiveFlag(ServiceWrapper.CodeClient.GetPaycodesByTypeRemovingGlobalPaycodesInUse(employeePaycodeTypeCode), showActive);
        }
        public static void PopulateComboBoxWithRecurringIncomePaycodesRemovingPaycodesInUse(ComboBoxControl combo, String employeePaycodeTypeCode, String languageCode, long? employeeId)
        {
            combo.DataSource = ServiceWrapper.CodeClient.GetRecurringIncomePaycodesRemovingPaycodesInUse(employeePaycodeTypeCode, employeeId);
        }
        public static void PopulateComboBoxWithRecurringIncomePaycodesRemovingPaycodesInUse(ComboBoxControl combo, String employeePaycodeTypeCode, String languageCode, long? employeeId, bool showActive)
        {
            combo.DataSource = FilterByActiveFlag(ServiceWrapper.CodeClient.GetRecurringIncomePaycodesRemovingPaycodesInUse(employeePaycodeTypeCode, employeeId), showActive);
        }
        public static void GetPaycodesRemovingAssociationsInUse(ComboBoxControl combo, String paycodeAssociationTypeCode, bool showActive)
        {
            combo.DataSource = FilterByActiveFlag(ServiceWrapper.CodeClient.GetPaycodesRemovingAssociationsInUse(paycodeAssociationTypeCode), showActive);
        }
        public static void PopulateComboBoxWithRecurringIncomePaycodesRemovingGlobalPaycodesInUse(ComboBoxControl combo, String employeePaycodeTypeCode, bool showActive)
        {
            combo.DataSource = FilterByActiveFlag(ServiceWrapper.CodeClient.GetRecurringIncomePaycodesRemovingGlobalPaycodesInUse(employeePaycodeTypeCode), showActive);
        }
        public static void PopulateControlWithEmployeeCustomFieldNameCodes(ICodeControl combo)
        {
            combo.DataSource = FilterByActiveFlag(ServiceWrapper.CodeClient.GetEmployeeCustomFieldNameCodesRemovingInUse(), true);
        }
        private static CodeCollection FilterByActiveFlag(CodeCollection codeCollection, bool showActive)
        {
            for (int i = codeCollection.Count; i > 0; i--)
            {
                if (codeCollection[i - 1].ActiveFlag != showActive)
                    codeCollection.RemoveAt(i - 1);
            }

            return codeCollection;
        }
        private void PopulateComboBox(ComboBoxControl combo, CodeCollection collection)
        {
            combo.DataSource = collection;
        }
        public static void PopulateEmployeeControl(ICodeControl control, bool hideEmpNumber)
        {
            long employeeId;
            CodeObject code = new CodeObject();

            if (control.SelectedValue == null)
                employeeId = Convert.ToInt64(control.SelectedValue);
            else
                employeeId = Convert.ToInt64(control.SelectedValue.Equals(String.Empty) ? null : control.SelectedValue); //check for empty string and replace with null

            EmployeeCollection employees = ServiceWrapper.HumanResourcesClient.GetEmployee(new EmployeeCriteria() { EmployeeId = employeeId });

            if (employees.Count > 0)
            {
                code.Code = employees[0].EmployeeId.ToString();

                if (hideEmpNumber)
                    code.Description = employees[0].ChequeName;
                else
                    code.Description = employees[0].ChequeName + " (" + employees[0].EmployeeNumber + ")";
            }

            control.DataSource = new List<CodeObject>() { code };
        }
        public static void PopulateStartDatePeriodYearControl(KeyValueControl control)
        {
            if ((control.Value ?? String.Empty).Equals(String.Empty))
            {
                control.Value = null;
                control.Description = null;
            }
            else
            {
                long payrollPeriodId = Convert.ToInt64(control.Value);
                PayrollPeriodCollection pp = ServiceWrapper.HumanResourcesClient.GetPayrollPeriodById(payrollPeriodId); //returns one row

                control.Description = pp[0].StartDatePeriodYear;
            }
        }
        public static void PopulateBusinessNumber(ICodeControl combo)
        {
            long? value = null;

            if (combo.ReadOnly || combo is GridKeyValueControl)
                value = Convert.ToInt64(combo.SelectedValue);

            combo.DataSource = ServiceWrapper.CodeClient.GetBusinessNumber(value);
        }
        public static void PopulateCompanyFromOrganizationUnit(ICodeControl combo)
        {
            long? value = null;

            if (combo.ReadOnly || combo is GridKeyValueControl)
                value = Convert.ToInt64(combo.SelectedValue);

            OrganizationUnitCollection organizationUnits = ServiceWrapper.HumanResourcesClient.GetOrganizationUnitByOrganizationUnitLevelId(new OrganizationUnitCriteria() { OrganizationUnitLevelId = 1 });
            CodeCollection codes = new CodeCollection();

            foreach (OrganizationUnit unit in organizationUnits)
            {
                if (value == null)
                    codes.Add(new CodeObject() { Code = unit.OrganizationUnitId.ToString(), Description = unit.Description });
                else
                {
                    if (unit.OrganizationUnitId.Equals(value))
                        codes.Add(new CodeObject() { Code = unit.OrganizationUnitId.ToString(), Description = unit.Description });
                }
            }

            combo.DataSource = codes;
        }
        public static void PopulateOrganizationUnitWithLevelDescription(ICodeControl combo)
        {
            OrganizationUnitCollection organizationUnits = null;
            CodeCollection codes = null;


            organizationUnits = ServiceWrapper.HumanResourcesClient.GetOrganizationUnitWithLevelDescription(null);
            codes = new CodeCollection();

            foreach (OrganizationUnit organizationUnit in organizationUnits)
            {
                codes.Add(new CodeObject() { Code = organizationUnit.OrganizationUnitId.ToString(), Description = organizationUnit.Description });
            }

            combo.DataSource = codes;
        }
        public static void PopulateEmployerNumber(ICodeControl combo)
        {
            long? value = null;

            if (combo.ReadOnly || combo is GridKeyValueControl)
                value = Convert.ToInt64(combo.SelectedValue);

            BusinessNumberCollection numbers = ServiceWrapper.HumanResourcesClient.GetBusinessNumber(null);
            CodeCollection codes = new CodeCollection();

            foreach (BusinessNumber number in numbers)
            {
                if (value == null)
                    codes.Add(new CodeObject() { Code = number.EmployerNumber, Description = number.EmployerNumber });
                else
                {
                    if (number.BusinessNumberId.Equals(value))
                        codes.Add(new CodeObject() { Code = number.EmployerNumber, Description = number.EmployerNumber });
                }
            }

            combo.DataSource = codes;
        }
        public static void PopulatePaycodes(ICodeControl combo, String languageCode)
        {
            CodeCollection collection = new CodeCollection();

            if (combo.ReadOnly || combo is GridKeyValueControl)
                collection = ServiceWrapper.CodeClient.GetPaycodeByCode(combo.SelectedValue);
            else
            {
                foreach (CodeObject code in GetPaycodesByType(null, languageCode))
                {
                    if (code.ActiveFlag)
                        collection.Add(code);
                }
            }

            combo.DataSource = collection;
        }

        #region salary plan
        public static void PopulateComboBoxWithSalaryPlan(ComboBoxControl control, String languageCode)
        {
            long? salaryPlanId = null;

            if (control.ReadOnly)
                salaryPlanId = control.SelectedValue != null ? Convert.ToInt64(control.SelectedValue) : -1;

            control.DataSource = GetSalaryPlan(salaryPlanId, languageCode, control.ReadOnly);
        }
        public static void PopulateSalaryPlan(ICodeControl combo, String languageCode)
        {
            long? salaryPlanId = null;

            if (combo.ReadOnly || combo is GridKeyValueControl)
                salaryPlanId = combo.SelectedValue != "" ? Convert.ToInt64(combo.SelectedValue) : -1;

            combo.DataSource = GetSalaryPlan(salaryPlanId, languageCode, combo.ReadOnly);
        }
        private static CodeCollection GetSalaryPlan(long? salaryPlanId, String languageCode, bool isReadOnly)
        {
            CodeCollection collection = new CodeCollection();

            foreach (SalaryPlan item in ServiceWrapper.HumanResourcesClient.GetSalaryPlan(salaryPlanId))
            {
                if (isReadOnly || item.ActiveFlag)
                    collection.Add(new CodeObject() { Code = item.Key, Description = languageCode.Equals("EN") ? item.EnglishDescription : item.FrenchDescription });
            }

            return collection;
        }
        public static void PopulateComboBoxWithSalaryPlanGrade(ComboBoxControl control, long? salaryPlanId, string languageCode)
        {
            long? salaryPlanGradeId = null;

            if (control.ReadOnly)
            {
                salaryPlanGradeId = control.SelectedValue != null ? Convert.ToInt64(control.SelectedValue) : -1;
                salaryPlanId = null;
            }

            control.DataSource = GetSalaryPlanGrade(salaryPlanGradeId, salaryPlanId, languageCode, control.ReadOnly);
        }
        public static void PopulateSalaryPlanGrade(ICodeControl combo, String languageCode)
        {
            long? salaryPlanGradeId = null;

            if (combo.ReadOnly || combo is GridKeyValueControl)
                salaryPlanGradeId = combo.SelectedValue != "" ? Convert.ToInt64(combo.SelectedValue) : -1;

            combo.DataSource = GetSalaryPlanGrade(salaryPlanGradeId, null, languageCode, combo.ReadOnly);
        }
        private static CodeCollection GetSalaryPlanGrade(long? salaryPlanGradeId, long? salaryPlanId, String languageCode, bool isReadOnly)
        {
            CodeCollection collection = new CodeCollection();

            foreach (SalaryPlanGrade item in ServiceWrapper.HumanResourcesClient.GetSalaryPlanGrade(salaryPlanGradeId, salaryPlanId, languageCode))
            {
                if (isReadOnly || item.ActiveFlag)
                    collection.Add(new CodeObject() { Code = item.Key, Description = languageCode.Equals("EN") ? item.EnglishDescription : item.FrenchDescription });
            }

            return collection;
        }
        public static void PopulateSalaryPlanGradeStep(ICodeControl combo, String languageCode)
        {
            long? salaryPlanGradeStepId = null;

            if (combo.ReadOnly || combo is GridKeyValueControl)
                salaryPlanGradeStepId = combo.SelectedValue != "" ? Convert.ToInt64(combo.SelectedValue) : -1;

            combo.DataSource = GetSalaryPlanGradeStep(salaryPlanGradeStepId, null, languageCode, combo.ReadOnly);
        }

        

        public static void PopulateComboBoxWithSalaryPlanGradeStep(ComboBoxControl control, long? salaryCodeGradeId, String languageCode)
        {
            long? salaryPlanGradeStepId = null;

            if (control.ReadOnly)
            {
                salaryPlanGradeStepId = control.SelectedValue != null ? Convert.ToInt64(control.SelectedValue) : -1;
                salaryCodeGradeId = null;
            }

            control.DataSource = GetSalaryPlanGradeStep(salaryPlanGradeStepId, salaryCodeGradeId, languageCode, control.ReadOnly);
        }
        private static CodeCollection GetSalaryPlanGradeStep(long? salaryPlanGradeStepId, long? salaryPlanGradeId, String languageCode, bool isReadOnly)
        {
            CodeCollection collection = new CodeCollection();

            foreach (SalaryPlanGradeStep item in ServiceWrapper.HumanResourcesClient.GetSalaryPlanGradeStep(salaryPlanGradeStepId, salaryPlanGradeId))
            {
                if (isReadOnly || item.ActiveFlag)
                    collection.Add(new CodeObject() { Code = item.Key, Description = languageCode.Equals("EN") ? item.EnglishDescription : item.FrenchDescription });
            }

            return collection;
        }
        #endregion

        #region security
        public static void PopulateComboBoxWithRoleDescription(ICodeControl combo)
        {
            if (combo.ReadOnly || combo is GridKeyValueControl)
            {
                String code = combo.SelectedValue;
                combo.DataSource = ServiceWrapper.CodeClient.GetRoleDescriptionCombo("SS", combo.SelectedValue, true, null);
            }
            else
                combo.DataSource = ServiceWrapper.CodeClient.GetRoleDescription("SS", true, null);
        }
        public static void PopulateComboBoxWithRoleDescriptionForGroupScreen(ICodeControl combo, String languageCode, bool? worklinksAdministrationFlag)
        {
            if (combo.ReadOnly || combo is GridKeyValueControl)
            {
                String code = combo.SelectedValue;
                combo.DataSource = ServiceWrapper.CodeClient.GetRoleDescriptionCombo("ROL", combo.SelectedValue, false, worklinksAdministrationFlag);
            }
            else
                combo.DataSource = ServiceWrapper.CodeClient.GetRoleDescription("ROL", false, worklinksAdministrationFlag);
        }
        #endregion

        #region pay register export
        //Load the export data in a dropdown
        public static void PopulateComboBoxWithExport(ICodeControl combo)
        {
            CodeCollection collection = new CodeCollection();

            if (combo.ReadOnly || combo is GridKeyValueControl)
            {
                ReportExportCollection coll = ServiceWrapper.ReportClient.GetReportExport(Convert.ToInt64(combo.SelectedValue));
                collection.Add(new CodeObject() { Code = coll[0].ExportId.ToString(), Description = coll[0].Name });
                combo.DataSource = collection;
            }
            else
            {
                foreach (ReportExport export in ServiceWrapper.ReportClient.GetReportExport(null))
                    collection.Add(new CodeObject() { Code = export.ExportId.ToString(), Description = export.Name, ActiveFlag = export.ActiveFlag });

                combo.DataSource = collection;
            }
        }
        //Load the export ftp data in a dropdown
        public static void PopulateComboBoxWithExportFtp(ICodeControl combo)
        {
            CodeCollection collection = new CodeCollection();

            if (combo.ReadOnly || combo is GridKeyValueControl)
            {
                if (combo.SelectedValue.Length != 0) //this is a nullable field, so skip db call
                {
                    ExportFtpCollection coll = ServiceWrapper.HumanResourcesClient.GetExportFtp(Convert.ToInt64(combo.SelectedValue), null, null);
                    collection.Add(new CodeObject() { Code = coll[0].ExportFtpId.ToString(), Description = coll[0].Description });
                }

                combo.DataSource = collection;
            }
            else
            {
                foreach (ExportFtp export in ServiceWrapper.HumanResourcesClient.GetExportFtp(null, null, null))
                    collection.Add(new CodeObject() { Code = export.ExportFtpId.ToString(), Description = export.Description });

                combo.DataSource = collection;
            }
        }
        #endregion

        public static void PopulateWsibControlRemovingEstimates(ICodeControl combo, string languageCode)
        {
            CodeTableType type = (CodeTableType)Enum.Parse(typeof(CodeTableType), combo.Type);
            CodeCollection collection = new CodeCollection();

            foreach (CodeWsib item in ServiceWrapper.CodeClient.GetCodeWsib(null))
            {
                //wcb codes with HideInEmploymentScreenFlag = true are codes not meant for employees so hide them
                if (item.ActiveFlag && !item.HideInEmploymentScreenFlag)
                    collection.Add(new CodeObject()
                    {
                        Code = item.WsibCode,
                        Description = (languageCode == "EN" ? item.EnglishDescription : item.FrenchDescription)
                    });
            }

            combo.DataSource = collection;
        }
    }
}