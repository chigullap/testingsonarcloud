﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public class Resource
    {
        private bool _initialized = false;
        private Object _value = null;

        public bool Initialized
        {
            get { return _initialized; }
        }

        public Object Value 
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                _initialized = true;
            }
        }
    }
}
