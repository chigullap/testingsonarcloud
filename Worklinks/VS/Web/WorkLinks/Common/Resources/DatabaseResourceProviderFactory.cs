﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Compilation;

namespace WorkLinks.Common.Resources
{
    public class DatabaseResourceProviderFactory : ResourceProviderFactory
    {


        public override IResourceProvider CreateGlobalResourceProvider(string classKey)
        {
            return new DatabaseResourceProvider(classKey);
        }

        public override IResourceProvider CreateLocalResourceProvider(string virtualPath)
        {
            return new DatabaseResourceProvider(virtualPath);
        }
    }
}
