﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkLinks.Common.Resources
{
    public class DisposableBaseType : IDisposable
    {
        private bool _disposed;
        protected bool Disposed
        {
            get
            {
                lock (this)
                {
                    return _disposed;
                }
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            lock (this)
            {
                if (_disposed == false)
                {
                    Cleanup();
                    _disposed = true;

                    GC.SuppressFinalize(this);
                }
            }
        }

        #endregion

        protected virtual void Cleanup()
        {
            
        }

        ~DisposableBaseType()
        {
            Cleanup();
        } 
    }
}
