﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public class ResourceCollection 
    {

        private Dictionary<String, Resource> _collection = new Dictionary<string, Resource>();


        public Resource this[String key]
        {
            get
            {
                if (!_collection.ContainsKey(key))
                    _collection.Add(key, new Resource());
                return _collection[key];
            }
        }

    }
}
