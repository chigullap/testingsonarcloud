﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public class LanguageResourceCollection 
    {
        private Dictionary<String, ResourceCollection> _collection = new Dictionary<String, ResourceCollection>();

        public ResourceCollection this[String languageCode]
        {
            get
            {
                if (!_collection.ContainsKey(languageCode))
                    _collection.Add(languageCode, new ResourceCollection());
                return _collection[languageCode];
            }
        }
    }
}
