﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;

using System.Resources;

namespace WorkLinks.Common.Resources
{
    public class DatabaseResourceReader : DisposableBaseType,  IResourceReader,  IEnumerable<KeyValuePair<string, object>>
    {
        #region fields
        private ListDictionary _resourceDictionary;
        #endregion

        #region constructor/destructor
        public DatabaseResourceReader(ListDictionary resourceDictionary)
        {
           _resourceDictionary = resourceDictionary;
        }

        #endregion
        #region IResourceReader Members

        public void Close()
        {
            this.Dispose();
        }

        public System.Collections.IDictionaryEnumerator GetEnumerator()
        {
            if (Disposed)
            {
                throw new ObjectDisposedException("DatabaseResourceReader object is already disposed.");
            }

            return _resourceDictionary.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            if (Disposed)
            {
                throw new ObjectDisposedException("DatabaseResourceReader object is already disposed.");
            }

            return _resourceDictionary.GetEnumerator();
        }

        #endregion

        #region IDisposable Members

        public new void Dispose()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IEnumerable<KeyValuePair<string,object>> Members

        IEnumerator<KeyValuePair<string, object>> IEnumerable<KeyValuePair<string, object>>.GetEnumerator()
        {
            if (Disposed)
            {
                throw new ObjectDisposedException("DatabaseResourceReader object is already disposed.");
            }

            return _resourceDictionary.GetEnumerator() as IEnumerator<KeyValuePair<string, object>>;
        }

        #endregion

        protected override void Cleanup()
        {
            try
            {
                _resourceDictionary = null;
            }
            finally
            {
                base.Cleanup();
            }
        }
    }
}
