﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.Common;
using WorkLinks.WebClient;
using WorkLinks.Common.Security;
using System.Web;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public class RoleResourceCollection 
    {
        #region fields
        private Dictionary<long,LanguageResourceCollection> _collection = new Dictionary<long,LanguageResourceCollection>();
        private const long _everybodyGuiRoleId = 1;
        private const String _profileKey = "Profile";
        #endregion

        #region properties

        protected static SecurityUserProfile Profile
        {
            get
            {
                if (HttpContext.Current.Profile != null)
                    return (SecurityUserProfile)HttpContext.Current.Profile[_profileKey];
                else
                    return null;
            }
        }

        private static WorkLinksMembershipUser User
        {
            get
            {
                return (WorkLinksMembershipUser)Profile.User;
            }
        }

        public static long CurrentGuiSecurityRoleId
        {
            get
            {
                if (System.Web.HttpContext.Current.Profile == null
                    || User.GetPrimarySecurityGroup() == null
                    || User.GetPrimarySecurityGroup().GuiSecurityRoleId == null
                    )
                {
                    return _everybodyGuiRoleId;
                }
                else
                {
                    return (long)(User.GetPrimarySecurityGroup().GuiSecurityRoleId);
                }
            }
        }
        #endregion


        public Object GetValue(String path, String key, CultureInfo culture)
        {
            long roleId=GetGuidSecurityRoleId(key);
            String languageCode=GetLanguage(culture,key);

            Resource resource = this[roleId][languageCode][key];
            if (!resource.Initialized)
            {
                lock (this)
                {
                    resource.Value = ServiceWrapper.ApplicationResourceClient.GetResourceByLanguageAndKey(ApplicationParameter.DatabaseName, roleId, languageCode, path, key);
                }
            }
            return resource.Value;
        }

        public void Clear()
        {
            _collection.Clear();
        }


        public LanguageResourceCollection this[long securityRoleId]
        {
            get
            {
                if (!_collection.ContainsKey(securityRoleId))
                    _collection.Add(securityRoleId, new LanguageResourceCollection());
                return _collection[securityRoleId];
            }
        }

        private String GetLanguage(System.Globalization.CultureInfo culture, string key)
        {
            if (culture == null || culture.TwoLetterISOLanguageName.ToLower().Equals("iv"))
                culture = CultureInfo.CurrentUICulture;

            return key.ToLower().EndsWith("mandatory") || key.ToLower().EndsWith("security") ? "EN" : culture.TwoLetterISOLanguageName;
        }

        private long GetGuidSecurityRoleId(String key)
        {
            return key.ToLower().EndsWith(".security") ? GuiSecurityRoleId : _everybodyGuiRoleId;
        }
        private long GuiSecurityRoleId
        {
            get
            {
                if (System.Web.HttpContext.Current.Profile == null
                    || User.GetPrimarySecurityGroup() == null
                    || User.GetPrimarySecurityGroup().GuiSecurityRoleId == null
                    )
                {
                    return _everybodyGuiRoleId;
                }
                else
                {
                    return (long)(User.GetPrimarySecurityGroup().GuiSecurityRoleId);
                }
            }
        }

    }
}
