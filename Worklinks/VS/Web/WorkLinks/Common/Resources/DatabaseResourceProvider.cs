﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Compilation;
using System.Globalization;
using System.Collections.Specialized;

using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;



namespace WorkLinks.Common.Resources
{

    /// <summary>
    /// Storage of resources in the database.
    /// Used in place of resx.  
    /// </summary>
    public class DatabaseResourceProvider : DisposableBaseType,  IResourceProvider
    {
        #region fields
        //cache
        private RoleResourceCollection _resourceCache = new RoleResourceCollection();
        private String _virtualPath;
        #endregion

        #region constructors/destructors
        public DatabaseResourceProvider(String virtualPath)
        {
            lock (this)
            {
                _virtualPath = virtualPath;
                DatabaseResourceCache.Add(_virtualPath, this);
            }
        }
        #endregion

        #region IResourceProvider Members


        public void ClearCache()
        {
            this._resourceCache.Clear();
        }


        public object GetObject(string resourceKey, System.Globalization.CultureInfo culture)
        {
            if (string.IsNullOrEmpty(resourceKey))
                throw new ArgumentNullException("resourceKey");

            return _resourceCache.GetValue(_virtualPath, resourceKey, culture);
        }

        public System.Resources.IResourceReader ResourceReader
        {
            get 
            {
                if (Disposed)
                {
                    throw new ObjectDisposedException("DBResourceProvider object is already disposed.");
                }

                // this is required for implicit resources 
                // this is also used for the expression editor sheet 

                //ListDictionary resourceDictionary = new ListDictionary();
                //foreach (ApplicationResource resource in _client.GetResourceByLanguage(_virtualPath, GetLanguage(CultureInfo.CurrentUICulture.TwoLetterISOLanguageName)))
                //{
                //    resourceDictionary.Add(resource.Key, resource.Value);
                //}

                return null;
            }
        }


        #endregion

        protected override void Cleanup()
        {
            try
            {
                this._resourceCache.Clear();
                DatabaseResourceCache.Remove(_virtualPath);
            }
            finally
            {
                base.Cleanup();
            }
        }
    }
}
