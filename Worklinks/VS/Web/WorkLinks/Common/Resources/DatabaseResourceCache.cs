﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkLinks.Common.Resources
{

    public static class DatabaseResourceCache
    {
        private static Dictionary<String, DatabaseResourceProvider> _providerCache = new Dictionary<string, DatabaseResourceProvider>();


        public static void Add(String key,DatabaseResourceProvider provider)
        {
            if(!_providerCache.ContainsKey(key))
                _providerCache.Add(key, provider);
        }

        public static void Remove(String key)
        {
            _providerCache.Remove(key);
        }

        /// <summary>
        /// resets entire resource cache
        /// </summary>
        public static void ClearCache()
        {
            foreach (DatabaseResourceProvider provider in _providerCache.Values)
            {
                provider.ClearCache();
            }
        }
    }
}