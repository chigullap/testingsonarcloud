﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.WebClient;

namespace WorkLinks.Common
{
    public class ServiceWrapper
    {
        #region fields
        private static CodeClient _codeClient = null;
        private static ApplicationResourceClient _applicationResourceClient = null;
        private static XmlClient _xmlClient = null;
        private static SecurityClient _securityClient = null;
        private static ReportClient _reportClient = null;
        private static HumanResourcesClient _humanResourcesClient = null;
        #endregion

        #region properties
        public static HumanResourcesClient HumanResourcesClient
        {
            get
            {
                if (_humanResourcesClient == null)
                    _humanResourcesClient = new HumanResourcesClient
                        (
                            ApplicationParameter.WcfServiceUrl,
                            ApplicationParameter.WcfHumanResourcesServiceName,
                            ApplicationParameter.WcfMaxReceivedMessageSize,
                            DecodeTimeFrameToTicks(ApplicationParameter.WcfReceiveTimeout),
                            DecodeTimeFrameToTicks(ApplicationParameter.WcfSendTimeout)
                        );

                return _humanResourcesClient;
            }
        }
        public static CodeClient CodeClient
        {
            get
            {
                if (_codeClient == null)
                    _codeClient = new CodeClient
                        (
                            ApplicationParameter.WcfServiceUrl,
                            ApplicationParameter.WcfCodeServiceName,
                            ApplicationParameter.WcfMaxReceivedMessageSize,
                            DecodeTimeFrameToTicks(ApplicationParameter.WcfReceiveTimeout),
                            DecodeTimeFrameToTicks(ApplicationParameter.WcfSendTimeout)
                        );

                return _codeClient;
            }
        }
        public static ApplicationResourceClient ApplicationResourceClient
        {
            get
            {
                if (_applicationResourceClient == null)
                    _applicationResourceClient = new ApplicationResourceClient
                        (
                            ApplicationParameter.WcfServiceUrl,
                            ApplicationParameter.WcfApplicationResourceServiceName,
                            ApplicationParameter.WcfMaxReceivedMessageSize,
                            DecodeTimeFrameToTicks(ApplicationParameter.WcfReceiveTimeout),
                            DecodeTimeFrameToTicks(ApplicationParameter.WcfSendTimeout)
                        );

                return _applicationResourceClient;
            }
        }
        public static XmlClient XmlClient
        {
            get
            {
                if (_xmlClient == null)
                    _xmlClient = new XmlClient
                        (
                            ApplicationParameter.WcfServiceUrl,
                            ApplicationParameter.WcfXmlServiceName,
                            ApplicationParameter.WcfMaxReceivedMessageSize,
                            DecodeTimeFrameToTicks(ApplicationParameter.WcfReceiveTimeout),
                            DecodeTimeFrameToTicks(ApplicationParameter.WcfSendTimeout)
                        );

                return _xmlClient;
            }
        }
        public static SecurityClient SecurityClient
        {
            get
            {
                if (_securityClient == null)
                    _securityClient = new SecurityClient
                        (
                            ApplicationParameter.WcfServiceUrl,
                            ApplicationParameter.WcfSecurityServiceName,
                            ApplicationParameter.WcfMaxReceivedMessageSize,
                            DecodeTimeFrameToTicks(ApplicationParameter.WcfReceiveTimeout),
                            DecodeTimeFrameToTicks(ApplicationParameter.WcfSendTimeout)
                        );

                return _securityClient;
            }
        }
        public static ReportClient ReportClient
        {
            get
            {
                if (_reportClient == null)
                    _reportClient = new ReportClient
                        (
                            ApplicationParameter.WcfServiceUrl,
                            ApplicationParameter.WcfReportServiceName,
                            ApplicationParameter.WcfMaxReceivedMessageSize,
                            DecodeTimeFrameToTicks(ApplicationParameter.WcfReceiveTimeout),
                            DecodeTimeFrameToTicks(ApplicationParameter.WcfSendTimeout)
                        );

                return _reportClient;
            }
        }
        #endregion

        private static long DecodeTimeFrameToTicks(String timeframe)
        {
            String[] time = timeframe.Split(':');
            return (new TimeSpan(Convert.ToInt32(time[0]), Convert.ToInt32(time[1]), Convert.ToInt32(time[2]))).Ticks;
        }

        public static class EmployeeTerminations
        {
            //roe
            public static EmployeeTerminationRoeCollection SelectRoe(long employeePositionId)
            {
                EmployeeTerminationRoeCollection collection = new EmployeeTerminationRoeCollection();
                collection.Load(HumanResourcesClient.SelectRoe(employeePositionId));
                return collection;
            }
            public static EmployeeTerminationRoe UpdateRoe(EmployeeTerminationRoe item)
            {
                return HumanResourcesClient.UpdateRoe(item);
            }
            public static void DeleteRoe(EmployeeTerminationRoe item)
            {
                HumanResourcesClient.DeleteRoe(item);
            }
            public static EmployeeTerminationRoe InsertRoe(EmployeeTerminationRoe item)
            {
                return HumanResourcesClient.InsertRoe(item);
            }

            //other
            public static EmployeeTerminationOtherCollection Select(long employeePositionId)
            {
                EmployeeTerminationOtherCollection collection = new EmployeeTerminationOtherCollection();
                collection.Load(HumanResourcesClient.GetEmployeeTerminationOther(employeePositionId));
                return collection;
            }
            public static EmployeeTerminationOther Update(EmployeeTerminationOther item)
            {
                return HumanResourcesClient.UpdateEmployeeTerminationOther(item);
            }
            public static void Delete(EmployeeTerminationOther item)
            {
                HumanResourcesClient.DeleteEmployeeTerminationOther(item);
            }
            public static EmployeeTerminationOther Insert(EmployeeTerminationOther item)
            {
                return HumanResourcesClient.InsertEmployeeTerminationOther(item);
            }
        }

        public static class EmployeePosition
        {
            public static EmployeePositionOrganizationUnitCollection SelectNewEmployeePositionOrganizationUnitLevelSummary()
            {
                EmployeePositionOrganizationUnitCollection collection = new EmployeePositionOrganizationUnitCollection();
                collection.Load(HumanResourcesClient.GetEmployeePositionOrganizationUnitLevelSummary(-1));
                return collection;
            }
            public static EmployeePositionSummaryCollection SelectSummary(EmployeePositionCriteria criteria)
            {
                EmployeePositionSummaryCollection collection = new EmployeePositionSummaryCollection();
                collection.Load(HumanResourcesClient.GetEmployeePositionSummary(criteria));
                return collection;
            }
            public static EmployeePositionCollection Select(DatabaseUser user, long employeeId)
            {
                EmployeePositionCollection collection = new EmployeePositionCollection();
                collection.Load(HumanResourcesClient.GetEmployeePosition(user, new EmployeePositionCriteria() { EmployeeId = employeeId }));
                return collection;
            }
            public static EmployeePositionCollection SelectLatestRecord(EmployeePositionCriteria criteria)
            {
                EmployeePositionCollection collection = new EmployeePositionCollection();
                collection.Load(HumanResourcesClient.GetEmployeePosition(criteria));
                return collection;
            }
            public static BusinessLayer.BusinessObjects.EmployeePosition Insert(BusinessLayer.BusinessObjects.EmployeePosition position, BusinessLayer.BusinessObjects.EmployeePosition formerPosition, DateTime? payrollPeriodCutoffDate, String payrollProcessGroupCode)
            {
                return HumanResourcesClient.InsertEmployeePosition(position, formerPosition, payrollPeriodCutoffDate, payrollProcessGroupCode, ApplicationParameter.RoeDefaultContactLastName, ApplicationParameter.RoeDefaultContactFirstName, ApplicationParameter.RoeDefaultContactTelephone, ApplicationParameter.RoeDefaultContactTelephoneExt, ApplicationParameter.AutoAddSecondaryPositions);
            }
            public static bool IsProcessDateOpen(long? payrollProcessId)
            {
                return HumanResourcesClient.IsProcessDateOpen(payrollProcessId);
            }
            public static BusinessLayer.BusinessObjects.EmployeePosition Update(BusinessLayer.BusinessObjects.EmployeePosition position)
            {
                return HumanResourcesClient.UpdateEmployeePosition(position, ApplicationParameter.AutoAddSecondaryPositions);
            }
            public static void Delete(BusinessLayer.BusinessObjects.EmployeePosition position, BusinessLayer.BusinessObjects.EmployeePosition formerPosition)
            {
                HumanResourcesClient.DeleteEmployeePosition(position, formerPosition);
            }
            public static void DeleteTermination(BusinessLayer.BusinessObjects.EmployeePosition employeePosition)
            {
                HumanResourcesClient.DeleteEmployeeTermination(employeePosition);
            }
            public static BusinessLayer.BusinessObjects.EmployeePosition Create()
            {
                return HumanResourcesClient.CreateEmployeePosition();
            }
        }

        public static class StatutoryDeduction
        {
            public static StatutoryDeductionCollection Select(long employeeId)
            {
                StatutoryDeductionCollection collection = new StatutoryDeductionCollection();
                collection.Load(HumanResourcesClient.GetStatutoryDeduction(employeeId));
                return collection;
            }
            public static BusinessLayer.BusinessObjects.StatutoryDeduction Insert(BusinessLayer.BusinessObjects.StatutoryDeduction item)
            {
                return HumanResourcesClient.InsertStatutoryDeduction(item);
            }
            public static BusinessLayer.BusinessObjects.StatutoryDeduction Update(BusinessLayer.BusinessObjects.StatutoryDeduction item)
            {
                return HumanResourcesClient.UpdateStatutoryDeduction(item);
            }
            public static void Delete(BusinessLayer.BusinessObjects.StatutoryDeduction item)
            {
                HumanResourcesClient.DeleteStatutoryDeduction(item);
            }

            //Barbados
            public static BusinessLayer.BusinessObjects.StatutoryDeduction InsertBarbados(BusinessLayer.BusinessObjects.StatutoryDeduction item)
            {
                return HumanResourcesClient.InsertStatutoryDeductionBarbados(item);
            }
            public static BusinessLayer.BusinessObjects.StatutoryDeduction UpdateBarbados(BusinessLayer.BusinessObjects.StatutoryDeduction item)
            {
                return HumanResourcesClient.UpdateStatutoryDeductionBarbados(item);
            }
            public static void DeleteBarbados(BusinessLayer.BusinessObjects.StatutoryDeduction item)
            {
                HumanResourcesClient.DeleteStatutoryDeductionBarbados(item);
            }

            //Saint Lucia
            public static BusinessLayer.BusinessObjects.StatutoryDeduction InsertSaintLucia(BusinessLayer.BusinessObjects.StatutoryDeduction item)
            {
                return HumanResourcesClient.InsertStatutoryDeductionSaintLucia(item);
            }
            public static BusinessLayer.BusinessObjects.StatutoryDeduction UpdateSaintLucia(BusinessLayer.BusinessObjects.StatutoryDeduction item)
            {
                return HumanResourcesClient.UpdateStatutoryDeductionSaintLucia(item);
            }
            public static void DeleteSaintLucia(BusinessLayer.BusinessObjects.StatutoryDeduction item)
            {
                HumanResourcesClient.DeleteStatutoryDeductionSaintLucia(item);
            }

            //Trinidad
            public static BusinessLayer.BusinessObjects.StatutoryDeduction InsertTrinidad(BusinessLayer.BusinessObjects.StatutoryDeduction item)
            {
                return HumanResourcesClient.InsertStatutoryDeductionTrinidad(item);
            }
            public static BusinessLayer.BusinessObjects.StatutoryDeduction UpdateTrinidad(BusinessLayer.BusinessObjects.StatutoryDeduction item)
            {
                return HumanResourcesClient.UpdateStatutoryDeductionTrinidad(item);
            }
            public static void DeleteTrinidad(BusinessLayer.BusinessObjects.StatutoryDeduction item)
            {
                HumanResourcesClient.DeleteStatutoryDeductionTrinidad(item);
            }

            //Jamaica
            public static BusinessLayer.BusinessObjects.StatutoryDeduction InsertJamaica(BusinessLayer.BusinessObjects.StatutoryDeduction item)
            {
                return HumanResourcesClient.InsertStatutoryDeductionJamaica(item);
            }
            public static BusinessLayer.BusinessObjects.StatutoryDeduction UpdateJamaica(BusinessLayer.BusinessObjects.StatutoryDeduction item)
            {
                return HumanResourcesClient.UpdateStatutoryDeductionJamaica(item);
            }
            public static void DeleteJamaica(BusinessLayer.BusinessObjects.StatutoryDeduction item)
            {
                HumanResourcesClient.DeleteStatutoryDeductionJamaica(item);
            }
        }

        public static class EmployeeEmploymentInformation
        {
            public static EmployeeEmploymentInformationCollection Select(long employeeId)
            {
                EmployeeEmploymentInformationCollection collection = new EmployeeEmploymentInformationCollection();
                collection.Load(HumanResourcesClient.GetEmployeeEmploymentInformation(employeeId));
                return collection;
            }
            public static BusinessLayer.BusinessObjects.EmployeeEmploymentInformation Insert(BusinessLayer.BusinessObjects.EmployeeEmploymentInformation item)
            {
                return HumanResourcesClient.InsertEmployeeEmploymentInformation(item);
            }
            public static BusinessLayer.BusinessObjects.EmployeeEmploymentInformation Update(BusinessLayer.BusinessObjects.EmployeeEmploymentInformation item)
            {
                return HumanResourcesClient.UpdateEmployeeEmploymentInformation(item);
            }
            public static void Delete(BusinessLayer.BusinessObjects.EmployeeEmploymentInformation item)
            {
                HumanResourcesClient.DeleteEmployeeEmploymentInformation(item);
            }
        }

        public static class PayrollPeriod
        {
            public static PayrollPeriodCollection Select(PayrollPeriodCriteria criteria)
            {
                PayrollPeriodCollection collection = new PayrollPeriodCollection();
                collection.Load(HumanResourcesClient.GetPayrollPeriod(criteria));
                return collection;
            }
        }

        public static class PayrollProcess
        {
            public static PayrollProcessCollection Select(PayrollProcessCriteria criteria)
            {
                PayrollProcessCollection collection = new PayrollProcessCollection();
                collection.Load(HumanResourcesClient.GetPayrollProcess(criteria));
                return collection;
            }
            public static BusinessLayer.BusinessObjects.PayrollProcess Update(BusinessLayer.BusinessObjects.PayrollProcess item, BusinessLayer.BusinessObjects.PayrollProcess.CalculationProcess processType)
            {
                return Update(item, processType, ApplicationParameter.DatabaseVersion, false, null, null);
            }
            public static BusinessLayer.BusinessObjects.PayrollProcess Update(BusinessLayer.BusinessObjects.PayrollProcess item, BusinessLayer.BusinessObjects.PayrollProcess.CalculationProcess processType, String version, bool lockUnlockButtonPressed, byte[] uploadedFile, String uploadedFileName)
            {
                return HumanResourcesClient.UpdatePayrollProcess(item, processType, version, ApplicationParameter.AutoGenerateSalaryEmployeeBatch, ApplicationParameter.CalculateBmsPension, lockUnlockButtonPressed, ApplicationParameter.ProrationEnabledFlag, uploadedFile, uploadedFileName, ApplicationParameter.AutoAddSecondaryPositions, ApplicationParameter.NewArrearsProcessingFlag);
            }
            public static BusinessLayer.BusinessObjects.PayrollProcess PostTransaction(BusinessLayer.BusinessObjects.PayrollProcess process, String status, RbcEftEdiParameters ediParms, ScotiaBankEdiParameters scotiaEdiParms, SendEmailParameters emailParms, RbcEftSourceDeductionParameters garnishmentParms, bool craRemitFlag, bool rqRemitFlag, bool sendInvoice, bool garnishmentRemitFlag, bool wcbChequeRemitFlag, bool healthTaxChequeRemitFlag, bool autoAddSecondaryPositions)
            {
                return HumanResourcesClient.PostTransaction(process, status, ediParms, scotiaEdiParms, emailParms, garnishmentParms, craRemitFlag, rqRemitFlag, sendInvoice, garnishmentRemitFlag, wcbChequeRemitFlag, healthTaxChequeRemitFlag, autoAddSecondaryPositions);
            }
        }

        public static class PayrollBatchReport
        {
            public static PayrollBatchReportCollection Select(PayrollBatchCriteria criteria)
            {
                PayrollBatchReportCollection collection = new PayrollBatchReportCollection();
                collection.Load(HumanResourcesClient.GetPayrollBatchReport(criteria));
                return collection;
            }
        }
    }
}