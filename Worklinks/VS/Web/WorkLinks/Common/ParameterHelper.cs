﻿using System;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Common
{
    public partial class GlobalResourceEmailFields : WLP.Web.UI.WLPUserControl
    {
        public string GetInvoiceEmailSubject()
        {
            return GetGlobalResourceObject("Messages", "InvoiceEmailSubject").ToString();
        }
        public string GetInvoiceEmailBody()
        {
            return GetGlobalResourceObject("Messages", "InvoiceEmailBody").ToString();
        }
    }

    public static class ParameterHelper
    {

        public enum EftType
        {
            CACH,
            CCHQ,
            CEFT,
            CWCHQ,
            R820DD,
            RACH,
            R820GARN,
            HSBC,
            SCOT_EDI,
            NONE
        }

        public static string DetermineEftType(string code)
        {
            String eftType = "";

            switch (code)
            {
                case "CITI":
                    eftType = EftType.CEFT.ToString();
                    break;
                case "HSBC":
                    eftType = EftType.HSBC.ToString();
                    break;
                case "CITI_EFT":
                    eftType = EftType.CACH.ToString();
                    break;
                case "CITI_EFT_CHEQ":
                    eftType = EftType.CWCHQ.ToString();
                    break;
                case "CITI_CHEQ":
                    eftType = EftType.CCHQ.ToString();
                    break;
                case "RBC_EDI":
                    eftType = EftType.R820DD.ToString();
                    break;
                case "RBC_ACH":
                    eftType = EftType.RACH.ToString();
                    break;
                case "R820GARN":
                    eftType = EftType.R820GARN.ToString();
                    break;
                case "SCOT_EDI":
                    eftType = EftType.SCOT_EDI.ToString();
                    break;
                default:
                    eftType = EftType.NONE.ToString(); //remit only clients wont be using our payroll
                    break;
            }

            return eftType;
        }
        public static SendEmailParameters CreateAndPopulateEmailParameters()
        {
            SendEmailParameters parms = new SendEmailParameters();

            parms.ToAddress = ApplicationParameter.EmailToAddress;
            parms.FromAddress = ApplicationParameter.EmailFromAddress;
            parms.CcAddress = ApplicationParameter.EmailCcAddress;
            parms.BccAddress = ApplicationParameter.EmailBccAddress;
            parms.Subject = String.Format(new GlobalResourceEmailFields().GetInvoiceEmailSubject(), ApplicationParameter.CompanyName);
            parms.Body = new GlobalResourceEmailFields().GetInvoiceEmailBody();

            parms.SmtpHostName = ApplicationParameter.EmailSMTPServerAddress;
            parms.SmtpPortNumber = ApplicationParameter.EmailPort;
            parms.SmtpLogin = ApplicationParameter.EmailLoginUser;
            parms.SmtpPassword = ApplicationParameter.EmailPassword;
            parms.EnableSSL = ApplicationParameter.EmailSSL;
            parms.FileName = ApplicationParameter.EmailInvoiceFileName;

            return parms; 
        }

        public static ScotiaBankEdiParameters CreateAndPopulateScotiaBankDirectDepositParameters(string eftType)
        {
            ScotiaBankEdiParameters parms = new ScotiaBankEdiParameters();

            parms.ScotiaBankIsa06 = ApplicationParameter.ScotiaBankIsa06;
            parms.ScotiaBankIsa08 = ApplicationParameter.ScotiaBankIsa08;
            parms.ScotiaBankIsa15 = ApplicationParameter.ScotiaBankIsa15;
            parms.ScotiaBankBpr07 = ApplicationParameter.ScotiaBankBpr07;
            parms.ScotiaBankBpr09 = ApplicationParameter.ScotiaBankBpr09;
            parms.CompanyShortName = ApplicationParameter.CompanyShortName;
            parms.PreAuthorizedDebitFlag = ApplicationParameter.PreAuthorizedDebitFlag; //only used in pre-authorization file creation
            parms.PreAuthorizedDebitFileType = ApplicationParameter.PreAuthorizedDebitFileType;
            parms.ScotiaFtpName = ApplicationParameter.ScotiaFtp;

            parms.EftType = DetermineEftType(eftType);

            return parms;
        }

        public static RbcEftEdiParameters CreateAndPopulateRbcDirectDepositChequeParameters(string eftType)
        {
            RbcEftEdiParameters parms = new RbcEftEdiParameters();

            parms.IsaSenderInterchangeID = ApplicationParameter.IsaSenderInterchangeID;
            parms.IsaReceiverInterchangeID = ApplicationParameter.IsaReceiverInterchangeID;
            parms.IsaTestIndicator = ApplicationParameter.IsaTestIndicator;
            parms.DirectDepositBankCodeTransitNumber = ApplicationParameter.DirectDepositBankCodeTransitNumber;
            parms.DirectDepositAccountNumber = ApplicationParameter.DirectDepositAccountNumber;
            parms.DirectDepositGsan = ApplicationParameter.DirectDepositGsan;
            parms.CompanyName = ApplicationParameter.CompanyName;
            parms.CompanyShortName = ApplicationParameter.CompanyShortName;
            parms.ChequesChequeIssuance = ApplicationParameter.ChequesChequeIssuance;
            parms.CustomLine1 = ApplicationParameter.CustomLine1;
            parms.CustomLine2 = ApplicationParameter.CustomLine2;
            parms.CustomLine3 = ApplicationParameter.CustomLine3;
            parms.DescriptionPayeeMatch = ApplicationParameter.DescriptionPayeeMatch;
            parms.PreAuthorizedDebitClientNumber = ApplicationParameter.PreAuthorizedDebitClientNumber; //only used in pre-authorization file creation
            parms.PreAuthorizedDebitFlag = ApplicationParameter.PreAuthorizedDebitFlag; //only used in pre-authorization file creation
            parms.PreAuthorizedDebitFileType = ApplicationParameter.PreAuthorizedDebitFileType;
            parms.UsTrn = ApplicationParameter.UsTrn; //only used with US bank accounts
            parms.RbcFtpName = ApplicationParameter.RbcFtp;

            parms.EftType = DetermineEftType(eftType);

            return parms;
        }

        public static RbcEftSourceDeductionParameters CreateAndPopulateRbcSourceDeductionParameters(string garnishmentEftType)
        {
            RbcEftSourceDeductionParameters parms = new RbcEftSourceDeductionParameters();

            parms.IsaSenderInterchangeIDSourceDed = ApplicationParameter.IsaSenderInterchangeIDSourceDed;
            parms.IsaReceiverInterchangeIDSourceDed = ApplicationParameter.IsaReceiverInterchangeIDSourceDed;
            parms.IsaTestIndicatorSourceDed = ApplicationParameter.IsaTestIndicatorSourceDed;
            parms.DirectDepositBankCodeTransitNumber = ApplicationParameter.DirectDepositBankCodeTransitNumber;
            parms.DirectDepositAccountNumber = ApplicationParameter.DirectDepositAccountNumber;
            parms.CraBankCodeTransitNumberSourceDed = ApplicationParameter.CraBankCodeTransitNumberSourceDed;
            parms.CraAccountNumberSourceDed = ApplicationParameter.CraAccountNumberSourceDed;
            parms.GsSenderInterchangeIdSourceDed = ApplicationParameter.GsSenderInterchangeIdSourceDed;
            parms.GsApplicationReceiversCodeSourceDed = ApplicationParameter.GsApplicationReceiversCodeSourceDed;
            parms.RefItReferenceNumberSourceDed = ApplicationParameter.RefItReferenceNumberSourceDed;
            parms.N1PrNameSourceDed = ApplicationParameter.N1PrNameSourceDed;

            parms.GarnishmentEftType = DetermineEftType(garnishmentEftType);

            return parms;
        }
    }
}