﻿using System;
using System.Configuration;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Common
{
    public static class ApplicationParameter
    {
        #region fields

        #region from web.config
        private const string _wcfServiceURL = "WcfServiceURL";
        private const string _databaseName = "DatabaseName";
        private const string _authDatabaseName = "AuthDatabaseName";
        private const string _wcfCodeServiceName = "WcfCodeServiceName";
        private const string _wcfApplicationResourceServiceName = "WcfApplicationResourceServiceName";
        private const string _wcfXmlServiceName = "WcfXmlServiceName";
        private const string _wcfSecurityServiceName = "WcfSecurityServiceName";
        private const string _wcfHumanResourcesServiceName = "WcfHumanResourcesServiceName";
        private const string _wcfReportServiceName = "WcfReportServiceName";
        private const string _wcfMaxReceivedMessageSize = "WcfMaxReceivedMessageSize";
        private const string _wcfReceiveTimeout = "WcfReceiveTimeout";
        private const string _wcfSendTimeout = "WcfSendTimeout";
        private const string _securityAuthority = "SecurityAuthority";
        private const string _securityRedirectUri = "SecurityRedirectUri";
        private const string _securityPostLogoutRedirectUri = "SecurityPostLogoutRedirectUri";
        private const string _scope = "Scope";
        private const string _securityClientId = "SecurityClientId";
        private const string _securityClientUsedByAngular = "SecurityClientUsedByAngular";
        private const string _ngSecurity = "NgSecurity";
        private const string _ngDbSecurity = "NgDbSecurity";
        private const string _ngApiSecurity = "NgApiSecurity";
        #endregion

        #region code_system table

        private const string _dynamicsGPCompanyName = "GPCPNYNM";
        private const string _dynamicsDatabaseName = "GPCPNYCD";

        private const string _eftClientNumber = "EFTACCT";
        private const string _eftTransmissionFlag = "EFTTRANS";
        private const string _eftFileIndicator = "EFTIND";
        private const string _eftType = "EFTTYPE";
        private const string _citiEftZipFileName = "EFTZIP";
        private const string _citiEftFileName = "EFTNAME";
        private const string _citiChequeFileName = "CHEQNAME";
        private const string _citiChequeAccountNumber = "EFTCHQAC";
        private const string _calculateBmsPension = "CALCPENS";

        private const string _companyName = "CMPNAME";
        private const string _dbVersion = "VERSION";
        private const string _maxInvalidPasswordAttempts = "MVPWATT";
        private const string _passwordAttemptWindow = "PWATTWIN";
        private const string _ceridianCompanyNumber = "CMPNBR";
        private const string _autoGenerateEmployeeNumber = "AUTOENUM";              //auto generate employee number is either "on" or "off"
        private const string _autoGenerateEmployeeNumberFormat = "ENUMFRMT";
        private const string _importDirtySave = "IMPDS";
        private const string _ontarioEHTAmount = "ON_EHT";
        private const string _organizationUnitOverride = "ORGORIDE";
        private const string _showSinWarning = "SINWARN";                           //show SIN warning is either "on" or "off"
        private const string _sinBusinessRule = "SIN-RULE";                         //business rule to show a "warning" or an "error"
        private const string _autoGenerateSalaryEmployeeBatch = "GENSAL";           //business rule to generate a batch containing salary employee transactions
        private const string _importExportProcessingDirectory = "IMPEXDIR";         //temp directory for processing import/export operations
        private const string _companyShortName = "CMPSHORT";
        private const string _wcbStartDate = "WCBSTDT";
        private const string _ceridianBillingProcessType = "CERPRTYP";
        private const string _roeIssueCode = "ROEISSUE";
        private const string _salaryPlanGradeOrganizationUnitLevel = "OULSALGR";

        //password rules
        private const string _passwordOneNumberRequired = "PW-1NUM";
        private const string _passwordNonAlphaRequired = "PW-NON-A";
        private const string _passwordOneCapRequired = "PW-1CAP";
        private const string _passwordOneLowerCaseRequired = "PW-1LOWR";
        private const string _passwordCantContainUserName = "PW-NOUSR";
        private const string _passwordExcludeSpacesAndQuotes = "PW-NOSPC";
        private const string _passwordMaxCharacterRepeat = "PW-X-REP";
        private const string _passwordMinLength = "PW-MNLEN";

        private const string _passwordExpiryDays = "PWEXPDAY";
        private const string _loginExpiryDays = "LOGEXPDY";
        private const string _loginExpiryPasswordChangeHours = "LOGEXPHR";

        //ftps to use
        private const string _rbcFtp = "RBC_FTP";
        private const string _scotiaFtp = "SCOT_FTP";
        private const string _pexFtp = "PEX_FTP";
        private const string _realFtp = "REAL_FTP";
        private const string _advFtp = "ADV_FTP";
        private const string _realFtpFilePatternToMatch = "SFTP_PAT";


        //reports and exports
        private const string _eftOriginatorId = "EFTOID";
        private const string _reportPensionExportFileFormat = "PENXNAME";
        private const string _reportEmpowerExportFileFormat = "EPWXNAME";
        private const string _reportGLExportFileFormat = "GLXNAME";

        private const string _massPayslipFileZipFormat = "MASSFZIP";
        private const string _massPayslipInternalFileFormat = "MASSPSFM";
        private const string _massT4InternalFileFormat = "T4NAME";
        private const string _massT4RSPInternalFileFormat = "T4RSPNAM";
        private const string _massT4aInternalFileFormat = "T4ANAME";
        private const string _massT4ARCAInternalFileFormat = "T4ARCANM";
        private const string _massNR4InternalFileFormat = "NR4NAME";
        private const string _massR1InternalFileFormat = "R1NAME";
        private const string _massR2InternalFileFormat = "R2NAME";
        private const string _yearEndZipFileName = "YRENDZIP";

        private const string _stockExportFileFormat = "ABSTEXFN";
        private const string _empowerGarnishmentExportFileFormat = "EGFORMAT";
        private const string _empowerGarnishmentFein = "EMP_FEIN";
        private const string _reportPayRegisterSortOrder = "RPTREGSO";
        private const string _reportShowDescriptionField = "RPTSHOWD";
        private const string _proprietorSocialInsuranceNumber1 = "CMPSIN1";
        private const string _proprietorSocialInsuranceNumber2 = "CMPSIN2";
        private const string _employeeBiographicsExportFilename = "EB_FNAME";


        //stacy test --to remove eventually but not yet
        private const string _revenueQuebecTaxId = "RQUETXID";
        //end stacy test


        private const string _empowerUseQuebecBusinessNumber = "EMQCFLAG";

        private const string _maxDegreeOfParallelism = "MAX_PARA";


        //SCOTIABANK EDI direct deposit for JAMAICA
        private const string _isa06 = "SCTISA06";
        private const string _isa08 = "SCTISA08";
        private const string _isa15 = "SCTISA15";
        private const string _bpr07 = "SCTBPR07";
        private const string _bpr09 = "SCTBPR09";

        //PRE AUTHORIZED DEBIT
        private const string _preAuthorizedDebitFlag = "PA_DEBIT";
        private const string _preAuthorizedDebitFileType = "PAD_TYPE";        
        private const string _preAuthorizedDebitClientNumber = "PACLIENT";

        //RBC EFT EDI direct deposit and cheques
        private const string _isaSenderInterchangeID = "RBC_ID1";
        private const string _isaReceiverInterchangeID = "RBC_ID2";
        private const string _isaTestIndicator = "RBC_IND";
        private const string _directDepositBankCodeTransitNumber = "RBC_FI_T";
        private const string _directDepositAccountNumber = "RBC_FI_A";
        private const string _directDepositGsan = "RBC_GSAN";
        private const string _chequesChequeIssuance = "RBC_ROUT";
        private const string _customLine1 = "RBCCSTL1";
        private const string _customLine2 = "RBCCSTL2";
        private const string _customLine3 = "RBCCSTL3";
        private const string _sendInvoice = "RBC_INVC";
        private const string _descriptionPayeeMatch = "RBC_REF4";
        private const string _usTrn = "US_TRN";        //used for US bank accounts 

        //RBC EFT Source Deductions
        private const string _isaSenderInterchangeIDSourceDed = "RBC_ID1S";
        private const string _isaReceiverInterchangeIDSourceDed = "RBC_ID2S";
        private const string _isaTestIndicatorSourceDed = "RBC_INDS";
        private const string _craBankCodeTransitNumberSourceDed = "RBC_CRAT";
        private const string _craAccountNumberSourceDed = "RBC_CRAA";
        private const string _gsSenderInterchangeIdSourceDed = "RBC_SNID";
        private const string _gsApplicationReceiversCodeSourceDed = "RBC_ARCD";
        private const string _refItReferenceNumberSourceDed = "RBC_REF";
        private const string _n1PrNameSourceDed = "RBC_NM";

        private const string _garnishmentEftType = "GARNEFT";

        //determines if we are using cra/rq remittances
        private const string _craRemitFlag = "CRASUBMT";
        private const string _craGarnishmentRemitFlag = "GARSUBMT";

        //health tax cheque remittances
        private const string _healthTaxChequeRemitFlag = "HTAX_CHQ";
        private const string _healthTaxChequeSendInvoiceNumBizDaysBeforeRemitDate = "HTX_CHQI";
        private const string _healthTaxChequeSendFileNumBizDaysBeforeRemitDate = "HTX_CHQS";
        private const string _emailChequeHealthTaxInvoiceFileName = "CHTXFILE";
        private const string _emailChequeHealthTaxSubject = "CHTXSUBJ";
        private const string _emailChequeHealthTaxBody = "CHTXBODY";

        //wcb cheque remittances
        private const string _wcbChequeRemitFlag = "WCB_CHQ";
        private const string _wcbChequeSendInvoiceNumBizDaysBeforeRemitDate = "WCB_CHQI";
        private const string _wcbChequeSendFileNumBizDaysBeforeRemitDate = "WCB_CHQS";
        private const string _emailChequeWcbInvoiceFileName = "CWCBFILE";
        private const string _emailChequeWcbSubject = "CWCBSUBJ";
        private const string _emailChequeWcbBody = "CWCBBODY";

        //rq remittances
        private const string _rqRemitFlag = "RQSUBMIT";

        //using Nunavut Tax remittance
        private const string _nunavutTaxRemitFlag = "NUSUBMIT"; 

        //using NTW Tax remittance
        private const string _nwtTaxRemitFlag = "NWTSUBMT";

        //using auto calc/post of payroll (clients using hr but not payroll)
        private const string _autoPayrollFlag = "AUTOCPAY";

        //proration
        private const string _prorationEnabledFlag = "CALCPRO";

        //RRSP export default information
        private const string _rrspGroupNumber = "RBCGRPNO";
        private const string _rrspHeader = "CNTRHEAD";

        //ROE default contact information
        private const string _roeDefaultContactFirstName = "ROEFNAME";
        private const string _roeDefaultContactLastName = "ROELNAME";
        private const string _roeDefaultContactTelephone = "ROEPHONE";
        private const string _roeDefaultContactTelephoneExt = "ROEPHEXT";

        //common control formats
        private const string _shortDateFormat = "DATESHRT";
        private const string _longDateFormat = "DATELONG";

        //default workday
        private const string _defaultWorkday = "DEWRKDAY";

        //r1/r2 export
        private const string _transmitterNumber = "R1TRANUM";
        private const string _transmitterName = "R1TRANME";
        private const string _revenuQuebecR1CertificationNumber = "R1CERTNM";
        private const string _revenuQuebecR2CertificationNumber = "R2CERTNM";

        //employee employment information
        private const string _probationDateAmount = "EIPROAMT";
        private const string _probationDateAmountType = "EIPROTYP";
        private const string _seniorityDateAmount = "EISENAMT";
        private const string _seniorityDateAmountType = "EISENTYP";
        private const string _nextReviewDateAmount = "EINXTAMT";
        private const string _nextReviewDateAmountType = "EINXTTYP";
        private const string _lastReviewDateAmount = "EILSTAMT";
        private const string _lastReviewDateAmountType = "EILSTTYP";
        private const string _increaseDateAmount = "EIINCAMT";
        private const string _increaseDateAmountType = "EIINCTYP";
        private const string _anniversaryDateAmount = "EIANVAMT";
        private const string _anniversaryDateAmountType = "EIANVTYP";

        //paycode description
        private const string _paycodeDescription = "PCODDESC";

        //RBC SEND INVOICE VIA EMAIL
        private const string _emailToAddress = "MAILTO";
        private const string _emailFromAddress = "MAILFROM";
        private const string _emailCcAddress = "MAILCC";
        private const string _emailBccAddress = "MAILBCC";

        private const string _emailSMTPServerAddress = "MAILSMTP";
        private const string _emailPort = "MAILPORT";
        private const string _emailLoginUser = "MAILLOGN";
        private const string _emailPassword = "MAILPASS";
        private const string _emailSSL = "MAILSSL";
        private const string _emailInvoiceFileName = "MAILFILE";

        //CRA default address
        private const string _defaultCRAAddress = "CRA_ADDR";

        //Runs WorkLinks payroll or not
        private const string _clientDoesNotCalcPayroll = "NOCALC";

        //labor level export filename
        private const string _laborLevelExportFileName = "LL_FNAME";

        //employee export filename
        private const string _employeeExportFileName = "EI_FNAME";

        //social costs export filename
        private const string _socialCostsExportFileName = "SOCFNAME";

        //real custom export filename
        private const string _realCustomExportFileName = "RL_FNAME";
        
        //adjustment rule export filename
        private const string _adjustmentRuleExportFileName = "AR_FNAME";

        private const string _autoAddSecondaryPositions = "AUTOSP";

        //new arrears processing
        private const string _newArrearsProcessingFlag = "NWARREAR";
        #endregion

        #endregion

        #region global properties

        #region from web.config
        public static string WcfServiceUrl { get { return ConfigurationManager.AppSettings.Get(_wcfServiceURL); } }
        public static string DatabaseName { get { return ConfigurationManager.AppSettings.Get(_databaseName); } }
        public static string AuthDatabaseName { get { return ConfigurationManager.AppSettings.Get(_authDatabaseName); } }
        public static string WcfCodeServiceName { get { return ConfigurationManager.AppSettings.Get(_wcfCodeServiceName); } }
        public static string WcfApplicationResourceServiceName { get { return ConfigurationManager.AppSettings.Get(_wcfApplicationResourceServiceName); } }
        public static string WcfXmlServiceName { get { return ConfigurationManager.AppSettings.Get(_wcfXmlServiceName); } }
        public static string WcfSecurityServiceName { get { return ConfigurationManager.AppSettings.Get(_wcfSecurityServiceName); } }
        public static string WcfHumanResourcesServiceName { get { return ConfigurationManager.AppSettings.Get(_wcfHumanResourcesServiceName); } }
        public static string WcfReportServiceName { get { return ConfigurationManager.AppSettings.Get(_wcfReportServiceName); } }
        public static long WcfMaxReceivedMessageSize { get { return Convert.ToInt64(ConfigurationManager.AppSettings.Get(_wcfMaxReceivedMessageSize)); } }
        public static string WcfReceiveTimeout { get { return ConfigurationManager.AppSettings.Get(_wcfReceiveTimeout); } }
        public static string WcfSendTimeout { get { return ConfigurationManager.AppSettings.Get(_wcfSendTimeout); } }
        public static string SecurityClientId { get { return ConfigurationManager.AppSettings.Get(_securityClientId); } }
        public static string SecurityClientUsedByAngular { get { return ConfigurationManager.AppSettings.Get(_securityClientUsedByAngular); } }
        public static string SecurityAuthority { get { return ConfigurationManager.AppSettings.Get(_securityAuthority); } }
        public static string SecurityRedirectUri { get { return ConfigurationManager.AppSettings.Get(_securityRedirectUri); } }
        public static string SecurityPostLogoutRedirectUri { get { return ConfigurationManager.AppSettings.Get(_securityPostLogoutRedirectUri); } }
        public static string Scope { get { return ConfigurationManager.AppSettings.Get(_scope); } }
        public static string NgSecurity { get { return ConfigurationManager.AppSettings.Get(_ngSecurity); } }
        public static string NgDbSecurity { get { return ConfigurationManager.AppSettings.Get(_ngDbSecurity); } }
        public static string NgApiSecurity { get { return ConfigurationManager.AppSettings.Get(_ngApiSecurity); } }
        #endregion

        #region email
        //RBC SEND INVOICE VIA EMAIL
        public static string EmailToAddress { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _emailToAddress)[0].Description; } }
        public static string EmailFromAddress { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _emailFromAddress)[0].Description; } }
        public static string EmailCcAddress { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _emailCcAddress)[0].Description; } }
        public static string EmailBccAddress { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _emailBccAddress)[0].Description; } }
        public static string EmailSMTPServerAddress { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _emailSMTPServerAddress)[0].Description; } }
        public static int EmailPort { get { return Convert.ToInt32(CodeHelper.GetCodeTable("EN", CodeTableType.System, _emailPort)[0].Description); } }
        public static string EmailLoginUser { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _emailLoginUser)[0].Description; } }
        public static string EmailPassword { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _emailPassword)[0].Description; } }
        public static bool EmailSSL { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _emailSSL)[0].Description); } }
        public static string EmailInvoiceFileName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _emailInvoiceFileName)[0].Description; } }
        #endregion

        #region code_system table
        public static string DynamicsCompanyName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _dynamicsGPCompanyName)[0].Description; } }
        public static string DynamicsDatabaseName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _dynamicsDatabaseName)[0].Description; } }
        public static string EFTClientNumber { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _eftClientNumber)[0].Description; } }
        public static bool EFTTransmissionFlag { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _eftTransmissionFlag)[0].Description); } }
        public static string EFTFileIndicator { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _eftFileIndicator)[0].Description; } }
        public static string EFTType { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _eftType)[0].Description; } }
        public static string CitiEftZipFileName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _citiEftZipFileName)[0].Description; } }
        public static string CitiEftFileName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _citiEftFileName)[0].Description; } }
        public static string CitiChequeFileName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _citiChequeFileName)[0].Description; } }
        public static string CitiChequeAccountNumber { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _citiChequeAccountNumber)[0].Description; } }
        public static bool CalculateBmsPension { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _calculateBmsPension)[0].Description); } }
        public static string CompanyName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _companyName)[0].Description; } }
        public static string DatabaseVersion { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _dbVersion)[0].Description; } }
        public static int MaxInvalidPasswordAttempts { get { return Convert.ToInt32(CodeHelper.GetCodeTable("EN", CodeTableType.System, _maxInvalidPasswordAttempts)[0].Description); } }
        public static int PasswordAttemptWindow { get { return Convert.ToInt32(CodeHelper.GetCodeTable("EN", CodeTableType.System, _passwordAttemptWindow)[0].Description); } }
        public static string CeridianCompanyNumber { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _ceridianCompanyNumber)[0].Description; } }
        public static string AutoGenerateEmployeeNumber { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _autoGenerateEmployeeNumber)[0].Description; } }
        public static string AutoGenerateEmployeeNumberFormat { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _autoGenerateEmployeeNumberFormat)[0].Description; } }
        public static string ImportDirtySave { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _importDirtySave)[0].Description; } }
        public static long OntarioEHTAmount { get { return Convert.ToInt64(CodeHelper.GetCodeTable("EN", CodeTableType.System, _ontarioEHTAmount)[0].Description); } }
        public static bool OrganizationUnitOverride { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _organizationUnitOverride)[0].Description.Equals("1"); } }
        public static string ShowSinWarning { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _showSinWarning)[0].Description; } }
        public static string SinBusinessRule { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _sinBusinessRule)[0].Description; } }
        public static bool AutoGenerateSalaryEmployeeBatch { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _autoGenerateSalaryEmployeeBatch)[0].Description); } }
        public static string ImportExportProcessingDirectory { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _importExportProcessingDirectory)[0].Description; } }
        public static string RoeIssueCode { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _roeIssueCode)[0].Description; } }
        public static string SalaryPlanGradeOrganizationUnitLevel { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _salaryPlanGradeOrganizationUnitLevel)[0].Description; } }

        public static BusinessLayer.BusinessObjects.Export.CeridianBilling.ProcessType CeridianBillingProcessType
        {
            get
            {
                string rtn = CodeHelper.GetCodeTable("EN", CodeTableType.System, _ceridianBillingProcessType)[0].Description;
                int val = Convert.ToInt32(rtn == null ? "1" : rtn);
                return (BusinessLayer.BusinessObjects.Export.CeridianBilling.ProcessType)val;
            }
        }
        public static string CompanyShortName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _companyShortName)[0].Description; } }
        public static DateTime WCBStartDate
        {
            get
            {
                string rtn = CodeHelper.GetCodeTable("EN", CodeTableType.System, _wcbStartDate)[0].Description;
                return Convert.ToDateTime(rtn == null ? "1900-01-01" : rtn);
            }
        }

        //password rules
        public static string PasswordOneNumberRequired { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _passwordOneNumberRequired)[0].Description; } }
        public static string PasswordNonAlphaRequired { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _passwordNonAlphaRequired)[0].Description; } }
        public static string PasswordOneCapRequired { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _passwordOneCapRequired)[0].Description; } }
        public static string PasswordOneLowerCaseRequired { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _passwordOneLowerCaseRequired)[0].Description; } }
        public static string PasswordCantContainUserName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _passwordCantContainUserName)[0].Description; } }
        public static string PasswordExcludeSpacesAndQuotes { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _passwordExcludeSpacesAndQuotes)[0].Description; } }
        public static string PasswordMaxCharacterRepeat { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _passwordMaxCharacterRepeat)[0].Description; } }
        public static string PasswordMinLength { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _passwordMinLength)[0].Description; } }
        public static string PasswordExpiryDays { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _passwordExpiryDays)[0].Description; } }
        public static string LoginExpiryDays { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _loginExpiryDays)[0].Description; } }
        public static string LoginExpiryPasswordChangeHours { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _loginExpiryPasswordChangeHours)[0].Description; } }

        //proration
        public static bool ProrationEnabledFlag { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _prorationEnabledFlag)[0].Description); } }

        //ftps to use
        public static string RbcFtp { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _rbcFtp)[0].Description; } }
        public static string ScotiaFtp { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _scotiaFtp)[0].Description; } }        
        public static string PexFtp { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _pexFtp)[0].Description; } }
        public static string RealFtp { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _realFtp)[0].Description; } }
        public static string AdvFtp { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _advFtp)[0].Description; } }        
        public static string RealFtpFilePatternToMatch { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _realFtpFilePatternToMatch)[0].Description; } }

        //reports and exports
        public static string EftOrginatorId { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _eftOriginatorId)[0].Description; } }
        public static string ReportPensionExportFileFormat { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _reportPensionExportFileFormat)[0].Description; } }
        public static string ReportEmpowerExportFileFormat { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _reportEmpowerExportFileFormat)[0].Description; } }
        public static string ReportGLExportFileFormat { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _reportGLExportFileFormat)[0].Description; } }
        public static string MassPayslipFileZipFormat { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _massPayslipFileZipFormat)[0].Description; } }
        public static string MassPayslipInternalFileFormat { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _massPayslipInternalFileFormat)[0].Description; } }
        public static string MassT4InternalFileFormat { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _massT4InternalFileFormat)[0].Description; } }
        public static string MassT4RSPInternalFileFormat { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _massT4RSPInternalFileFormat)[0].Description; } }
        public static string MassT4aInternalFileFormat { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _massT4aInternalFileFormat)[0].Description; } }
        public static string MassT4ARCAInternalFileFormat { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _massT4ARCAInternalFileFormat)[0].Description; } }
        public static string MassNR4InternalFileFormat { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _massNR4InternalFileFormat)[0].Description; } }
        public static string MassR1InternalFileFormat { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _massR1InternalFileFormat)[0].Description; } }
        public static string MassR2InternalFileFormat { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _massR2InternalFileFormat)[0].Description; } }
        public static string YearEndZipFileName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _yearEndZipFileName)[0].Description; } }
        public static string StockExportFileFormat { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _stockExportFileFormat)[0].Description; } }
        public static string EmpowerGarnishmentExportFileFormat { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _empowerGarnishmentExportFileFormat)[0].Description; } }
        public static string EmpowerGarnishmentFein { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _empowerGarnishmentFein)[0].Description; } }
        public static string RRSPGroupNumber { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _rrspGroupNumber)[0].Description; } }
        public static string RRSPHeader { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _rrspHeader)[0].Description; } }
        public static string ReportPayRegisterSortOrder { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _reportPayRegisterSortOrder)[0].Description; } }
        public static string ReportShowDescriptionField { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _reportShowDescriptionField)[0].Description; } }
        public static string ProprietorSocialInsuranceNumber1 { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _proprietorSocialInsuranceNumber1)[0].Description; } }
        public static string ProprietorSocialInsuranceNumber2 { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _proprietorSocialInsuranceNumber2)[0].Description; } }
        public static bool EmpowerUseQuebecBusinessNumber { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _empowerUseQuebecBusinessNumber)[0].Description); } }
        public static string EmployeeBiographicFilename { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _employeeBiographicsExportFilename)[0].Description; } }
    

        //stacy test --to remove eventually but not yet
        public static string RevenueQuebecTaxId { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _revenueQuebecTaxId)[0].Description; } }
        //end stacy test

        

        public static int MaxDegreeOfParallelism
        {
            get
            {
                if (string.IsNullOrWhiteSpace(CodeHelper.GetCodeTable("EN", CodeTableType.System, _maxDegreeOfParallelism)[0].Description))
                    return 1;
                else
                    return Convert.ToInt32(CodeHelper.GetCodeTable("EN", CodeTableType.System, _maxDegreeOfParallelism)[0].Description);
            }
        }

        //SCOTIABANK EDI direct deposit for JAMAICA
        public static string ScotiaBankIsa06 { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _isa06)[0].Description; } }
        public static string ScotiaBankIsa08 { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _isa08)[0].Description; } }
        public static string ScotiaBankIsa15 { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _isa15)[0].Description; } }
        public static string ScotiaBankBpr07 { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _bpr07)[0].Description; } }
        public static string ScotiaBankBpr09 { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _bpr09)[0].Description; } }


        //PRE AUTHORIZED DEBIT 
        public static bool PreAuthorizedDebitFlag { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _preAuthorizedDebitFlag)[0].Description); } }
        public static string PreAuthorizedDebitFileType { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _preAuthorizedDebitFileType)[0].Description; } }
        public static string PreAuthorizedDebitClientNumber { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _preAuthorizedDebitClientNumber)[0].Description; } }

        //RBC EFT EDI
        public static string IsaSenderInterchangeID { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _isaSenderInterchangeID)[0].Description; } }
        public static string IsaReceiverInterchangeID { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _isaReceiverInterchangeID)[0].Description; } }
        public static string IsaTestIndicator { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _isaTestIndicator)[0].Description; } }
        public static string DirectDepositBankCodeTransitNumber { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _directDepositBankCodeTransitNumber)[0].Description; } }
        public static string DirectDepositAccountNumber { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _directDepositAccountNumber)[0].Description; } }
        public static string DirectDepositGsan { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _directDepositGsan)[0].Description; } }
        public static string ChequesChequeIssuance { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _chequesChequeIssuance)[0].Description; } }
        public static string CustomLine1 { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _customLine1)[0].Description; } }
        public static string CustomLine2 { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _customLine2)[0].Description; } }
        public static string CustomLine3 { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _customLine3)[0].Description; } }
        public static bool SendInvoice { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _sendInvoice)[0].Description); } }
        public static string DescriptionPayeeMatch { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _descriptionPayeeMatch)[0].Description; } }
        public static string UsTrn { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _usTrn)[0].Description; } }

        //RBC EFT SOURCE DEDUCTIONS
        public static string IsaSenderInterchangeIDSourceDed { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _isaSenderInterchangeIDSourceDed)[0].Description; } }
        public static string IsaReceiverInterchangeIDSourceDed { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _isaReceiverInterchangeIDSourceDed)[0].Description; } }
        public static string IsaTestIndicatorSourceDed { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _isaTestIndicatorSourceDed)[0].Description; } }
        public static string CraBankCodeTransitNumberSourceDed { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _craBankCodeTransitNumberSourceDed)[0].Description; } }
        public static string CraAccountNumberSourceDed { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _craAccountNumberSourceDed)[0].Description; } }
        public static string GsSenderInterchangeIdSourceDed { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _gsSenderInterchangeIdSourceDed)[0].Description; } }
        public static string GsApplicationReceiversCodeSourceDed { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _gsApplicationReceiversCodeSourceDed)[0].Description; } }
        public static string RefItReferenceNumberSourceDed { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _refItReferenceNumberSourceDed)[0].Description; } }
        public static string N1PrNameSourceDed { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _n1PrNameSourceDed)[0].Description; } }
        public static string GarnishmentEftType { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _garnishmentEftType)[0].Description; } }

        //determines if we are using cra/rq remittances
        public static bool CraRemitFlag { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _craRemitFlag)[0].Description); } }
        public static bool CraGarnishmentRemitFlag { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _craGarnishmentRemitFlag)[0].Description); } }

        //health tax cheque remittances
        public static bool HealthTaxChequeRemitFlag { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _healthTaxChequeRemitFlag)[0].Description); } }
        public static bool HealthTaxChequeSendInvoiceNumBizDaysBeforeRemitDate { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _healthTaxChequeSendInvoiceNumBizDaysBeforeRemitDate)[0].Description); } }
        public static bool HealthTaxChequeSendFileNumBizDaysBeforeRemitDate { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _healthTaxChequeSendFileNumBizDaysBeforeRemitDate)[0].Description); } }
        public static string EmailChequeHealthTaxInvoiceFileName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _emailChequeHealthTaxInvoiceFileName)[0].Description; } }
        public static string EmailChequeHealthTaxSubject { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _emailChequeHealthTaxSubject)[0].Description; } }
        public static string EmailChequeHealthTaxBody { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _emailChequeHealthTaxBody)[0].Description; } }

        //wcb cheque remittances
        public static bool WcbChequeRemitFlag { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _wcbChequeRemitFlag)[0].Description); } }
        public static bool WcbChequeSendInvoiceNumBizDaysBeforeRemitDate { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _wcbChequeSendInvoiceNumBizDaysBeforeRemitDate)[0].Description); } }
        public static bool WcbChequeSendFileNumBizDaysBeforeRemitDate { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _wcbChequeSendFileNumBizDaysBeforeRemitDate)[0].Description); } }
        public static string EmailChequeWcbInvoiceFileName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _emailChequeWcbInvoiceFileName)[0].Description; } }
        public static string EmailChequeWcbSubject { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _emailChequeWcbSubject)[0].Description; } }
        public static string EmailChequeWcbBody { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _emailChequeWcbBody)[0].Description; } }

        //rq remittances
        public static bool RqRemitFlag { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _rqRemitFlag)[0].Description); } }

        //Nunavut Tax remittance
        public static bool NunavutTaxRemitFlag { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _nunavutTaxRemitFlag)[0].Description); } }

        //NTW Tax remittance
        public static bool NWTTaxRemitFlag { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _nwtTaxRemitFlag)[0].Description); } }

        //using auto calc/post of payroll (clients using hr but not payroll)
        public static bool AutoPayrollFlag { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _autoPayrollFlag)[0].Description); } }


        //ROE default contact information
        public static string RoeDefaultContactLastName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _roeDefaultContactLastName)[0].Description; } }
        public static string RoeDefaultContactFirstName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _roeDefaultContactFirstName)[0].Description; } }
        public static string RoeDefaultContactTelephone { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _roeDefaultContactTelephone)[0].Description; } }
        public static string RoeDefaultContactTelephoneExt { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _roeDefaultContactTelephoneExt)[0].Description; } }

        //common control formats
        public static string ShortDateFormat { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _shortDateFormat)[0].Description; } }
        public static string LongDateFormat { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _longDateFormat)[0].Description; } }

        //default workday
        public static string DefaultWorkday { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _defaultWorkday)[0].Description; } }

        //r1 export
        public static string TransmitterNumber { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _transmitterNumber)[0].Description; } }
        public static string TransmitterName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _transmitterName)[0].Description; } }
        public static string RevenuQuebecR1CertificationNumber { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _revenuQuebecR1CertificationNumber)[0].Description; } }
        public static string RevenuQuebecR2CertificationNumber { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _revenuQuebecR2CertificationNumber)[0].Description; } }

        //employee employment information
        public static string ProbationDateAmount { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _probationDateAmount)[0].Description; } }
        public static string ProbationDateAmountType { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _probationDateAmountType)[0].Description; } }
        public static string SeniorityDateAmount { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _seniorityDateAmount)[0].Description; } }
        public static string SeniorityDateAmountType { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _seniorityDateAmountType)[0].Description; } }
        public static string NextReviewDateAmount { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _nextReviewDateAmount)[0].Description; } }
        public static string NextReviewDateAmountType { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _nextReviewDateAmountType)[0].Description; } }
        public static string LastReviewDateAmount { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _lastReviewDateAmount)[0].Description; } }
        public static string LastReviewDateAmountType { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _lastReviewDateAmountType)[0].Description; } }
        public static string IncreaseDateAmount { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _increaseDateAmount)[0].Description; } }
        public static string IncreaseDateAmountType { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _increaseDateAmountType)[0].Description; } }
        public static string AnniversaryDateAmount { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _anniversaryDateAmount)[0].Description; } }
        public static string AnniversaryDateAmountType { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _anniversaryDateAmountType)[0].Description; } }

        //paycode description
        public static string PaycodeDescription { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _paycodeDescription)[0].Description; } }

        //CRA default address
        public static string DefaultCRAAddress { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _defaultCRAAddress)[0].Description; } }

        //run worklinks payroll or not
        public static bool ClientDoesNotCalcPayroll { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _clientDoesNotCalcPayroll)[0].Description); } }

        //labor level export filename
        public static string LaborLevelExportFileName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _laborLevelExportFileName)[0].Description; } }
        
        //employee export filename
        public static string EmployeeExportFileName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _employeeExportFileName)[0].Description; } }

        //employee export filename
        public static string SocialCostsExportFileName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _socialCostsExportFileName)[0].Description; } }

        public static string REALCustomExportFileName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _realCustomExportFileName)[0].Description; } }

        //adjustment rule export filename
        public static string AdjustmentRuleExportFileName { get { return CodeHelper.GetCodeTable("EN", CodeTableType.System, _adjustmentRuleExportFileName)[0].Description; } }

        public static bool AutoAddSecondaryPositions { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _autoAddSecondaryPositions)[0].Description); } }

        //process new arrears offset calcs vs old BMS version 
        public static bool NewArrearsProcessingFlag { get { return Convert.ToBoolean(CodeHelper.GetCodeTable("EN", CodeTableType.System, _newArrearsProcessingFlag)[0].Description); } }


        
        #endregion

        #endregion

        #region codes
        public static class Code
        {
            public static class PayrollBatchStatusCode
            {
                public static string Approved = "AP";
                public static string UnApproved = "UA";
            }
            public static class PayrollProcessStatusCode
            {
                public static string New = "NW";
                public static string CalculationPending = "PN";
                public static string CalculationProcessed = "CP";
                public static string PostPending = "PP";
                public static string Processed = "PT";
                public static string Arrears = "ARR";
                public static string ImportError = "IMP_ERR";
            }
            public static class YearEndStatusCode
            {
                public static string Pending = "PEN";
            }
        }
        #endregion
    }
}