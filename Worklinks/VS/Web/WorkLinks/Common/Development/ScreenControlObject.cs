﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WLP.BusinessLayer.BusinessObjects;
using System.Web.UI;
using System.Reflection;
using System.IO;
using System.Diagnostics;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.Web.UI;

namespace WorkLinks.Development
{

    public class ScreenControlObject
    {
        #region fields
        #endregion

        public void FindControls(Object control, ScreenControlCollection screenControlCollection)
        {
            if (control is IWLPControl)
            {
                ScreenControl screenControl = GetScreenControl((IWLPControl)control);
                if (screenControl.ResourceName != null && screenControl.ResourceName != String.Empty && screenControlCollection[String.Format("{0}|{1}|{2}", screenControl.VirtualPath, screenControl.ResourceName, screenControl.ControlType)] == null)
                    screenControlCollection.Add(screenControl);
            }

            if (control is Control && ((Control)control).Controls.Count > 0)
            {
                foreach (Control childControl in ((Control)control).Controls)
                {
                    FindControls(childControl, screenControlCollection);
                }
            }
            if (control is Telerik.Web.UI.GridHeaderItem)
            {
                foreach (Telerik.Web.UI.GridColumn gridControl in ((Telerik.Web.UI.GridHeaderItem)control).OwnerTableView.Columns)
                {
                    if (gridControl is IWLPControl)
                        FindControls(gridControl, screenControlCollection);
                }
            }
            if (control is WLP.Web.UI.Controls.GridTemplateControl && ((WLP.Web.UI.Controls.GridTemplateControl)control).ColumnEditor != null)
            {
                FindControls(((WLP.Web.UI.Controls.GridTemplateControl)control).ColumnEditor.ContainerControl, screenControlCollection);
            }
        }


        private ScreenControl GetScreenControl(IWLPControl control)
        {
            ScreenControl screenControl = new ScreenControl();
            screenControl.VirtualPath = control.AppRelativeVirtualPath.Trim('~');
            screenControl.ControlType = (control.GetType().GetProperties()[0]).ReflectedType.Name;
            screenControl.ResourceName = ((IWLPControl)control).ResourceName;
            screenControl.SortOrder = ((IWLPControl)control).TabIndex;

            return screenControl;
        }

        public void UpdateScreenControls(ScreenControlCollection screenControlCollection)
        {
            Common.ServiceWrapper.HumanResourcesClient.MergeScreenControl(screenControlCollection.ToArray());
        }
    }

}
