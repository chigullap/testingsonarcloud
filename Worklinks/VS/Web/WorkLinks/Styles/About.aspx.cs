﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using WLP.Web.UI;

namespace WorkLinks.Web.WorkLinks
{
    public partial class About : WLPPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected String Version
        {
            get 
            { 
                return String.Format("Version: {0} ", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString()); 
            }
        }

    }
}
