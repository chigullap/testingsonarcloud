﻿<%@ Page Title="About Us" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="About.aspx.cs" Inherits="WorkLinks.Web.WorkLinks.About" ResourceName="About" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        <asp:Literal ID="h2" runat="server" ></asp:Literal>
    </h2>
    <p>
        <asp:Literal ID="p" runat="server" ></asp:Literal>
        <p style="font-size:large; font-weight:bold;"><%= Version %></p>
    </p>

<!--    <iframe frameBorder="0" src="http://localhost:4200/admin/import"></iframe>-->
</asp:Content>
