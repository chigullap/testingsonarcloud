using System;

using WLP.Web.UI;
using WorkLinks.Development;
using WorkLinks.BusinessLayer.BusinessObjects;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web;

namespace WorkLinks
{
    public class WorkLinksPage : WLPPage
    {

        bool DevelopmentModeEnabled
        {
            get
            {
                return (System.Configuration.ConfigurationManager.AppSettings.Get("DevelopmentModeEnabled") == "true" ? true : false);
            }
        }

        protected override void OnLoad(EventArgs e)
        {


            base.OnLoad(e);


            if (Session["UserName"] == null)
            {
                //force logout, no session user
                //Request.GetOwinContext().Authentication.SignOut();
            }


        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        protected override void OnPreInit(EventArgs e)
        {
            this._shortDateFormat = Common.ApplicationParameter.ShortDateFormat;
            this._longDateFormat = Common.ApplicationParameter.LongDateFormat;

            if (DevelopmentModeEnabled)
            {
                Button btnMapObjectFields = new Button();
                btnMapObjectFields.ID = "MapObjectFields";
                btnMapObjectFields.Text = "Map Object Fields";
                btnMapObjectFields.Visible = DevelopmentModeEnabled;
                btnMapObjectFields.CausesValidation = false;
                btnMapObjectFields.Click += new EventHandler(btnMapObjectFields_Click);

                Control placeHolderControl = Page.FindControl("MainContent");
                if (placeHolderControl == null)
                    placeHolderControl = Page.FindControl("ContentPlaceHolder1");

                if (placeHolderControl != null)
                {
                    placeHolderControl.Controls.AddAt(0, btnMapObjectFields);
                }
                else
                {
                    MasterPage theMaster = Page.Master;
                    while (theMaster != null)
                    {
                        placeHolderControl = theMaster.FindControl("MainContent");
                        if (placeHolderControl == null)
                            placeHolderControl = theMaster.FindControl("ContentPlaceHolder1");

                        if (placeHolderControl != null)
                        {
                            placeHolderControl.Controls.AddAt(0, btnMapObjectFields);
                            break;
                        }
                        theMaster = theMaster.Master;
                    }
                }
            }
        }

        public void btnMapObjectFields_Click(object sender, EventArgs e)
        {
            ScreenControlObject screenControlObject = new ScreenControlObject();
            ScreenControlCollection screenControlObjectCollection = new ScreenControlCollection();
            screenControlObject.FindControls(this.Page, screenControlObjectCollection);
            screenControlObject.UpdateScreenControls(screenControlObjectCollection);
        }
    }

}