﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmailTemplatePage.aspx.cs" Inherits="WorkLinks.Admin.Email.EmailTemplatePage" %>
<%@ Register Src="~/Admin/Email/EmailTemplateControl.ascx" TagPrefix="uc1" TagName="EmailTemplateControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:EmailTemplateControl runat="server" id="EmailTemplateControl" />
</asp:Content>