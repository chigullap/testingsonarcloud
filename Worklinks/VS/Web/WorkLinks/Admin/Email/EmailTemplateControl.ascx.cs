﻿using System;
using System.Web.UI.WebControls;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.Email
{
    public partial class EmailTemplateControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private const String _emailIdKey = "EmailIdKey";
        private long _emailId = -1;
        private const String _actionKey = "ActionKey";
        private String _action;
        #endregion

        #region properties
        public long EmailId
        {
            get
            {
                if (_emailId == -1)
                {
                    Object obj = ViewState[_emailIdKey];
                    if (obj != null)
                        _emailId = Convert.ToInt64(obj);
                }

                return _emailId;
            }
            set
            {
                _emailId = value;
                ViewState[_emailIdKey] = _emailId;
            }
        }
        public String Action
        {
            get
            {
                if (_action == null)
                {
                    Object obj = ViewState[_actionKey];
                    if (obj != null)
                        _action = (String)(obj);
                }

                return _action;
            }
            set
            {
                _action = value;
                ViewState[_actionKey] = _action;
            }
        }
        public bool IsEditMode { get { return EmailTemplateView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool IsInsertMode { get { return EmailTemplateView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsViewMode { get { return EmailTemplateView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool AddFlag { get { return Common.Security.RoleForm.Email.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.Email.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.Email.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Action == "view")
            {
                if (!IsPostBack)
                    LoadEmailTemplateInformation();
            }
            else if (Action == "add")
            {
                if (!IsPostBack)
                {
                    EmailTemplateCollection newCollection = new EmailTemplateCollection();
                    newCollection.AddNew();
                    Data = newCollection;

                    //change formview's mode to insert mode
                    EmailTemplateView.ChangeMode(FormViewMode.Insert);

                    //databind
                    EmailTemplateView.DataBind();
                }
            }
        }
        public void SetActionAndEmailId(String action, long emailId)
        {
            Action = action;
            EmailId = emailId;
        }
        protected void LoadEmailTemplateInformation()
        {
            //create business object and fill it with the data passed to the page then bind the grid
            EmailTemplateCollection collection = new EmailTemplateCollection();
            collection.Add(LoadDataIntoBusinessClass());

            Data = collection;
            EmailTemplateView.DataBind();
        }
        protected EmailTemplate LoadDataIntoBusinessClass()
        {
            EmailTemplateCollection temp = new EmailTemplateCollection();
            temp.Load(Common.ServiceWrapper.HumanResourcesClient.GetEmailTemplate(EmailId));
            return temp[0]; //will only ever have one item in this collection
        }
        #endregion

        #region event handlers
        protected void EmailTemplateView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            EmailTemplateView.DataSource = Data;
        }
        protected void EmailTemplateView_DataBound(object sender, EventArgs e)
        {
            /*
            if (IsInsertMode)
            {
                ComboBoxControl primaryLanguageCode = (ComboBoxControl)EmailTemplateView.FindControl("PrimaryLanguageCode");
                if (primaryLanguageCode != null)
                {
                    primaryLanguageCode.Value = "EN";
                    primaryLanguageCode.ReadOnly = true;
                }

                ComboBoxControl secondaryLanguageCode = (ComboBoxControl)EmailTemplateView.FindControl("SecondaryLanguageCode");
                if (secondaryLanguageCode != null)
                {
                    secondaryLanguageCode.Value = "FR";
                    secondaryLanguageCode.ReadOnly = true;
                }
            }
            */
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        #endregion

        #region handle updates
        protected void EmailTemplateView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            EmailTemplate item = (EmailTemplate)e.DataItem;
            Common.ServiceWrapper.HumanResourcesClient.InsertEmailTemplate(item).CopyTo((EmailTemplate)Data[item.Key]);
            EmailId = item.EmailId;
        }
        protected void EmailTemplateView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                EmailTemplate item = (EmailTemplate)e.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.UpdateEmailTemplate(item).CopyTo((EmailTemplate)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}