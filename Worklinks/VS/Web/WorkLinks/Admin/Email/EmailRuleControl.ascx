﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailRuleControl.ascx.cs" Inherits="WorkLinks.Admin.Email.EmailRule" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPGrid
    ID="EmailRuleGrid"
    runat="server"
    GridLines="None"
    AutoGenerateColumns="false"
    OnDeleteCommand="EmailRuleGrid_DeleteCommand"
    OnInsertCommand="EmailRuleGrid_InsertCommand"
    OnUpdateCommand="EmailRuleGrid_UpdateCommand"
    AutoAssignModifyProperties="true">

    <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="true">
        <Selecting AllowRowSelect="false" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
        <CommandItemTemplate>
            <wasp:WLPToolBar ID="EmailRuleToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
                <Items>
                    <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <Columns>
            <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
            <wasp:GridBoundControl DataField="OrganizationUnitDescription" LabelText="**OrganizationUnitDescription**" SortExpression="OrganizationUnitDescription" UniqueName="OrganizationUnitDescription" ResourceName="OrganizationUnitDescription" />
            <wasp:GridBoundControl DataField="EmailTo" LabelText="**EmailTo**" SortExpression="EmailTo" UniqueName="EmailTo" ResourceName="EmailTo" />
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="**EmailTriggerFieldCode**" DataField="EmailTriggerFieldCode" Type="EmailTriggerFieldCode" ResourceName="EmailTriggerFieldCode" />
            <wasp:GridNumericControl DataField="TriggerFieldOffsetDays" LabelText="**TriggerFieldOffsetDays**" SortExpression="TriggerFieldOffsetDays" UniqueName="TriggerFieldOffsetDays" DataType="System.Int64" ResourceName="TriggerFieldOffsetDays" />
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            <div class="form_control">
                                <span class="label">
                                    <wasp:WLPLabel ID="OrganizationUnitLabel" runat="server" Text="**OrganizationUnit**" ResourceName="OrganizationUnit" />
                                </span>
                                <span class="field">
                                    <telerik:RadDropDownTree ID="OrganizationUnitDropDownTree" ClientIDMode="Static" runat="server" DataTextField="Description" DataFieldID="OrganizationUnitId" DataFieldParentID="ParentOrganizationUnitId" TextMode="FullPath" DataValueField="OrganizationUnitId" OnClientEntryAdding="OnClientEntryAdding" OnClientLoad="onClientLoad" OnClientClearButtonClicking="onClientClearButtonClicking">
                                        <DropDownSettings Height="200px" Width="340px" CloseDropDownOnSelection="true" />
                                        <ButtonSettings ShowClear="true" />
                                    </telerik:RadDropDownTree>
                                    <asp:HiddenField ID="OrganizationUnitDropDownValues" ClientIDMode="Static" runat="server" Value="" />
                                </span>  
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="EmailTo" runat="server" TextMode="multiline" Width="100%" Rows="2" ResourceName="EmailTo" Value='<%# Bind("EmailTo") %>' Mandatory="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="EmailTriggerFieldCode" runat="server" Type="EmailTriggerFieldCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmailTriggerFieldCode" Value='<%# Bind("EmailTriggerFieldCode") %>' Mandatory="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:NumericControl ID="TriggerFieldOffsetDays" runat="server" DecimalDigits="0" ResourceName="TriggerFieldOffsetDays" Value='<%# Bind("TriggerFieldOffsetDays") %>' Mandatory="true" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsInsert %>' ResourceName="Insert" />
                                    </td>
                                    <td>
                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="Cancel" CausesValidation="false" ResourceName="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>
    </MasterTableView>

    <HeaderContextMenu EnableAutoScroll="true" />

</wasp:WLPGrid>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function OnClientEntryAdding(sender, args) {
            var node = args.get_node();

            //get count of child nodes of the selected node, if it has children, don't allow it to be selected
            if (node.get_allNodes().length > 0) {
                args.set_cancel(true);
            }
            else {
                var parentNode = node;
                //get id of the node selected
                var ids = [node.get_value()];

                while (parentNode.get_level() > 0) {
                    parentNode = parentNode.get_parent();
                    //get parent node id 
                    ids.push(parentNode.get_value());
                }
                var hiddenOrgControl = document.getElementById('OrganizationUnitDropDownValues');
                //reverse the array so it's parent->child order
                hiddenOrgControl.value = ids.reverse();
            }
        }

        function onClientLoad(sender, args) {
            var hiddenOrgControl = document.getElementById('OrganizationUnitDropDownValues');
            var orgUnits = hiddenOrgControl.value.split(",");
            var currentNode = sender.get_embeddedTree();
            var orgUnitPathTextDescription = "";

            currentNode = currentNode.findNodeByValue(orgUnits[0]);

            if (currentNode != null) {
                currentNode.expand();
                currentNode.select();
                //get text of parent node
                orgUnitPathTextDescription = currentNode.get_text();

                for (var i = 1; i < orgUnits.length; i++) {
                    for (var j = 0; j < currentNode._children.get_count(); j++) {
                        if (currentNode._children._array[j].get_value() == orgUnits[i]) {
                            currentNode = currentNode._children._array[j];
                            currentNode.expand();
                            currentNode.select();
                            //get text of child node
                            orgUnitPathTextDescription += '/' + currentNode.get_text();

                            break;
                        }
                    }
                }
            }

            //set the path as the default message for the RadDropDownTree
            if (orgUnitPathTextDescription != null)
                sender._defaultMessage = orgUnitPathTextDescription;
        }

        function onClientClearButtonClicking(sender, args) {
            //clear the default message
            sender._defaultMessage = '<%= DropDownTreeDefaultMessage %>';

            var hiddenOrgControl = document.getElementById('OrganizationUnitDropDownValues');
            var orgUnits = hiddenOrgControl.value.split(",");

            var currentNode = sender.get_embeddedTree();
            currentNode = currentNode.findNodeByValue(orgUnits[0]);

            //collapse all nodes in the path that were previously expanded
            if (currentNode != null) {
                currentNode.collapse();

                for (var i = 1; i < orgUnits.length; i++) {
                    for (var j = 0; j < currentNode._children.get_count() ; j++) {
                        if (currentNode._children._array[j].get_value() == orgUnits[i]) {
                            currentNode = currentNode._children._array[j];
                            currentNode.collapse();
                            break;
                        }
                    }
                }
            }

            //set org units to nothing
            hiddenOrgControl.value = null;
        }
    </script>
</telerik:RadScriptBlock>