﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.Email
{
    public partial class EmailSearchControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public bool IsViewMode { get { return EmailGrid.IsViewMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.Email.AddFlag; } }
        public bool ViewFlag { get { return Common.Security.RoleForm.Email.ViewFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.Email.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.Email.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.Email.ViewFlag);

            WireEvents();

            if (!IsPostBack)
                LoadEmailInformation();

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.StartsWith("refresh"))
            {
                LoadEmailInformation();
                EmailGrid.Rebind();
            }
        }
        protected void WireEvents()
        {
            EmailGrid.NeedDataSource += new GridNeedDataSourceEventHandler(EmailGrid_NeedDataSource);
        }
        protected void LoadEmailInformation()
        {
            Data = EmailCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetEmail(null));
        }
        #endregion

        #region event handlers
        void EmailGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EmailGrid.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        #endregion

        #region handle updates
        protected void EmailToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("delete") && EmailGrid.SelectedValue != null)
                DeleteEmail((BusinessLayer.BusinessObjects.Email)Data[EmailGrid.SelectedValue.ToString()]);
        }
        protected void DeleteEmail(BusinessLayer.BusinessObjects.Email email)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteEmail(email);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    EmailGrid.SelectedIndexes.Clear();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            LoadEmailInformation();
            EmailGrid.Rebind();
        }
        #endregion

        #region security handlers
        protected void AddButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && AddFlag; }
        protected void DetailsButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && ViewFlag; }
        protected void DeleteButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && DeleteFlag; }
        #endregion
    }
}