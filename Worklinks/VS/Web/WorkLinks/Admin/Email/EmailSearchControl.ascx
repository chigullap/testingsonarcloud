﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailSearchControl.ascx.cs" Inherits="WorkLinks.Admin.Email.EmailSearchControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPToolBar ID="EmailToolBar" runat="server" Width="100%" OnButtonClick="EmailToolBar_ButtonClick" OnClientLoad="EmailToolBar_init">
        <Items>
            <wasp:WLPToolBarButton Text="**Add**" ImageUrl="~/App_Themes/Default/Add.gif" onclick="Add()" CommandName="add" ResourceName="Add" OnPreRender="AddButton_PreRender"></wasp:WLPToolBarButton>
            <wasp:WLPToolBarButton Text="**Details**" onclick="Open();" CommandName="details" ResourceName="Details" OnPreRender="DetailsButton_PreRender"></wasp:WLPToolBarButton>
            <wasp:WLPToolBarButton Text="**Delete**" ImageUrl="~\App_Themes\Default\Delete.gif" CommandName="delete" ResourceName="Delete" OnPreRender="DeleteButton_PreRender"></wasp:WLPToolBarButton>
        </Items>
    </wasp:WLPToolBar>

    <wasp:WLPGrid
        ID="EmailGrid"
        runat="server"
        AllowPaging="true"
        PagerStyle-AlwaysVisible="true"
        PageSize="100"
        AllowSorting="true"
        GridLines="None"
        AutoAssignModifyProperties="true">

        <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="false">
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
            <ClientEvents OnRowDblClick="OnRowDblClick" OnRowClick="OnRowClick" OnGridCreated="onGridCreated" OnCommand="onCommand" />
        </ClientSettings>

        <MasterTableView
            ClientDataKeyNames="EmailId, Name, Description, EmailTemplateTypeCode"
            AutoGenerateColumns="false"
            DataKeyNames="EmailId"
            CommandItemDisplay="Top"
            AllowSorting="false">

            <SortExpressions>
                <telerik:GridSortExpression FieldName="Name" SortOrder="Ascending" />
            </SortExpressions>

            <CommandItemTemplate></CommandItemTemplate>

            <Columns>
                <wasp:GridBoundControl DataField="Name" LabelText="**Name**" SortExpression="Name" UniqueName="Name" ResourceName="Name"></wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="Description" LabelText="**Description**" SortExpression="Description" UniqueName="Description" ResourceName="Description"></wasp:GridBoundControl>
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="EmailTemplateTypeCode" LabelText="**EmailTemplateTypeCode**" SortExpression="EmailTemplateTypeCode" Type="EmailTemplateTypeCode" ResourceName="EmailTemplateTypeCode"></wasp:GridKeyValueControl>
            </Columns>

        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true"></HeaderContextMenu>

    </wasp:WLPGrid>
</asp:Panel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="EmailWindows"
    Width="1000"
    Height="700"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientClose="onClientClose"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var emailId;
        var name;
        var toolbar = null;

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function EmailToolBar_init(sender) {
            initializeControls();
        }

        function initializeControls() {
            if ($find('<%= EmailToolBar.ClientID %>') != null)
                toolbar = $find('<%= EmailToolBar.ClientID %>');

            enableButtons(getSelectedRow() != null);
        }

        function OnRowDblClick(sender, eventArgs) {
            var toolbar = $find('<%= EmailToolBar.ClientID %>');
            if (toolbar.findButtonByCommandName('details') != null)
                Open(sender);
        }

        function getSelectedRow() {
            var grid = $find('<%= EmailGrid.ClientID %>');
            if (grid == null)
                return null;

            var masterTable = grid.get_masterTableView();
            if (masterTable == null)
                return null;

            var selectedRow = masterTable.get_selectedItems();
            if (selectedRow.length == 1)
                return selectedRow[0];
            else
                return null;
        }

        function onGridCreated(sender, eventArgs) {
            var selectedRow = getSelectedRow();
            if (selectedRow != null) {
                emailId = selectedRow.getDataKeyValue('EmailId');
                name = selectedRow.getDataKeyValue('Name');
                enableButtons(true);
            }
        }

        function onCommand(sender, args) {
            if (args.get_commandName() == 'Page') {
                emailId = null;
                name = null;
                initializeControls();
            }
        }

        function OnRowClick(sender, eventArgs) {
            emailId = eventArgs.getDataKeyValue('EmailId');
            name = eventArgs.getDataKeyValue('Name');
            enableButtons(true);
        }

        function enableButtons(enable) {
            var detailsButton = toolbar.findButtonByCommandName('details');
            var deleteButton = toolbar.findButtonByCommandName('delete');

            if (detailsButton != null)
                detailsButton.set_enabled(enable);

            if (deleteButton != null)
                deleteButton.set_enabled(enable);
        }

        function Open() {
            if (emailId != null) {
                openWindowWithManager('EmailWindows', String.format('/Admin/Email/{0}/{1}/{2}', 'view', emailId, name), false);
                return false;
            }
        }

        function Add() {
            openWindowWithManager('EmailWindows', String.format('/Admin/Email/{0}/{1}/{2}', 'add', -1, 'x'), false);
            return false;
        }

        function onClientClose(sender, eventArgs) {
            var arg = sender.argument;
            if (arg != null && arg.closeWithChanges)
                __doPostBack('<%= MainPanel.ClientID %>', 'refresh');
        }
    </script>
</telerik:RadScriptBlock>