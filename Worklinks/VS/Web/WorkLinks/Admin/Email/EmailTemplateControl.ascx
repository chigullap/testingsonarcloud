﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailTemplateControl.ascx.cs" Inherits="WorkLinks.Admin.Email.EmailTemplateControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPFormView
    ID="EmailTemplateView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key"
    OnNeedDataSource="EmailTemplateView_NeedDataSource"
    OnUpdating="EmailTemplateView_Updating"
    OnInserting="EmailTemplateView_Inserting"
    OnDataBound="EmailTemplateView_DataBound">

    <ItemTemplate>
        <wasp:WLPToolBar ID="EmailTemplateToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Text="**Edit**" ImageUrl="~/App_Themes/Default/Edit.gif" Visible='<%# UpdateFlag %>' CommandName="edit" ResourceName="Edit" CausesValidation="false"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="Name" MaxLength="16"  runat="server" ResourceName="Name" Value='<%# Bind("Name") %>' ReadOnly="true" Mandatory="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="EmailTemplateTypeCode" runat="server" Type="EmailTemplateTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmailTemplateTypeCode" Value='<%# Bind("EmailTemplateTypeCode") %>' ReadOnly="true" Mandatory="true"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="Description" runat="server" TextMode="multiline" Width="100%" Rows="3" ResourceName="Description" Value='<%# Bind("Description") %>' ReadOnly="true" Mandatory="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PrimaryLanguageCode" runat="server" TextValue="English" ReadOnly="true" runat="server" Type="LanguageCode" ResourceName="PrimaryLanguageCode" ></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PrimarySubject" FieldStyle="width: 600px !important;"  Width="100%" runat="server" ResourceName="PrimarySubject" Value='<%# Bind("PrimarySubject") %>' ReadOnly="true" Mandatory="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PrimaryBodyDetail" runat="server" FieldStyle="width: 600px !important;" TextMode="multiline" Width="100%" Rows="7" ResourceName="PrimaryBodyDetail" Value='<%# Bind("PrimaryBodyDetail") %>' ReadOnly="true" Mandatory="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="SecondaryLanguageCode" TextValue="French"  runat="server" ReadOnly="true" Type="LanguageCode" ResourceName="SecondaryLanguageCode"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="SecondarySubject" FieldStyle="width: 600px !important;"  Width="100%" runat="server" ResourceName="SecondarySubject" Value='<%# Bind("SecondarySubject") %>' ReadOnly="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="SecondaryBodyDetail" runat="server" FieldStyle="width: 600px !important;" TextMode="multiline" Width="100%" Rows="7" ResourceName="SecondaryBodyDetail" Value='<%# Bind("SecondaryBodyDetail") %>' ReadOnly="true"></wasp:TextBoxControl>
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar ID="EmailTemplateToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
            <Items>
                <wasp:WLPToolBarButton Text="**Insert**" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="**Update**" ImageUrl="~/App_Themes/Default/Update.gif" Visible='<%# IsEditMode %>' CommandName="update" ResourceName="Update"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="**Cancel**" ImageUrl="~/App_Themes/Default/Cancel.gif" Visible='<%# !IsInsertMode %>' CommandName="cancel" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="**Cancel**" ImageUrl="~/App_Themes/Default/Cancel.gif" Visible='<%# IsInsertMode %>' onClick="Close()" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="Name" MaxLength="16" runat="server" ResourceName="Name" Value='<%# Bind("Name") %>' Mandatory="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="EmailTemplateTypeCode" runat="server" Type="EmailTemplateTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmailTemplateTypeCode" Value='<%# Bind("EmailTemplateTypeCode") %>' Mandatory="true"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="Description" runat="server" TextMode="multiline" Width="100%" Rows="3" ResourceName="Description" Value='<%# Bind("Description") %>' Mandatory="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PrimaryLanguageCode" runat="server" TextValue="English" ReadOnly="true" runat="server" Type="LanguageCode" ResourceName="PrimaryLanguageCode" ></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PrimarySubject" runat="server" FieldStyle="width: 600px !important;" Width="100%" ResourceName="PrimarySubject" Value='<%# Bind("PrimarySubject") %>' Mandatory="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PrimaryBodyDetail" runat="server" FieldStyle="width: 600px !important;" TextMode="multiline" Width="100%" Rows="7" ResourceName="PrimaryBodyDetail" Value='<%# Bind("PrimaryBodyDetail") %>' Mandatory="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="SecondaryLanguageCode" TextValue="French"  runat="server" ReadOnly="true" Type="LanguageCode" ResourceName="SecondaryLanguageCode"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="SecondarySubject" runat="server" FieldStyle="width: 600px !important;"  Width="100%" ResourceName="SecondarySubject" Value='<%# Bind("SecondarySubject") %>'></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="SecondaryBodyDetail" runat="server" FieldStyle="width: 600px !important;" TextMode="multiline" Width="100%" Rows="7" ResourceName="SecondaryBodyDetail" Value='<%# Bind("SecondaryBodyDetail") %>'></wasp:TextBoxControl>
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>
</wasp:WLPFormView>

<telerik:RadScriptBlock runat="server">
    <script type="text/javascript">
        function processClick(commandName) {
            var arg = new Object;
            arg.closeWithChanges = true;
            getRadWindow().argument = arg;
        }

        function getRadWindow() {
            var popup = null;

            if (window.radWindow)
                popup = window.radWindow;
            else if (window.frameElement.radWindow)
                popup = window.frameElement.radWindow;

            return popup;
        }

        function Close(button, args) {
            var popup = getRadWindow();
            setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
        }

        function ToolBarClick(sender, args) {
            var button = args.get_item();
            var commandName = button.get_commandName();
            processClick(commandName);

            if (<%= IsInsertMode.ToString().ToLower() %>) {
                if (commandName == "cancel")
                    window.close();
            }
        }
    </script>
</telerik:RadScriptBlock>