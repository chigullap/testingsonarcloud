﻿using System;
using System.Web.UI.WebControls;

namespace WorkLinks.Admin.Email
{
    public partial class EmailControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private String Action { get { return Page.RouteData.Values["action"].ToString().ToLower(); } }
        private long EmailId { get { return Convert.ToInt64(Page.RouteData.Values["emailId"]); } }
        private String Name { get { return Convert.ToString(Page.RouteData.Values["name"]); } }
        protected bool IsViewMode { get { return EmailTemplateControl.IsViewMode && EmailRuleControl.IsViewMode; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.Email.ViewFlag);

            if (!IsPostBack)
                WireEvents();
        }
        protected void WireEvents()
        {
            this.EmailTemplateControl.SetActionAndEmailId(Action, EmailId);
            this.EmailRuleControl.SetEmailId(EmailId);
        }
        protected override void OnPreRender(EventArgs e)
        {
            InitializeControls();
            base.OnPreRender(e);
        }
        protected void InitializeControls()
        {
            EmailTabStrip.Enabled = IsViewMode;
        }
        protected void EmailMultiPage_PreRender(object sender, EventArgs e)
        {
            this.EmailRuleControl.SetEmailId(EmailTemplateControl.EmailId);
        }
        #endregion

        #region security handlers
        protected void EmailTemplate_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = Common.Security.RoleForm.Email.ViewFlag; }
        protected void EmailRule_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = Common.Security.RoleForm.Email.ViewFlag; }
        #endregion
    }
}