﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.Email
{
    public partial class EmailRule : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private const String _emailIdKey = "EmailIdKey";
        private long _emailId = -1;
        #endregion

        #region properties
        public long EmailId
        {
            get
            {
                if (_emailId == -1)
                {
                    Object obj = ViewState[_emailIdKey];
                    if (obj != null)
                        _emailId = Convert.ToInt64(obj);
                }

                return _emailId;
            }
            set
            {
                _emailId = value;
                ViewState[_emailIdKey] = _emailId;
            }
        }
        public bool IsViewMode { get { return EmailRuleGrid.IsViewMode; } }
        public bool IsUpdate { get { return EmailRuleGrid.IsEditMode; } }
        public bool IsInsert { get { return EmailRuleGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.Email.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.Email.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.Email.DeleteFlag; } }
        public String DropDownTreeDefaultMessage { get { return String.Format(GetGlobalResourceObject("PageContent", "ChooseADepartment").ToString()); } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();

            if (!IsPostBack)
            {
                LoadEmailRule();
                Initialize();
            }
        }
        protected void WireEvents()
        {
            EmailRuleGrid.NeedDataSource += new GridNeedDataSourceEventHandler(EmailRuleGrid_NeedDataSource);
            EmailRuleGrid.ItemDataBound += EmailRuleGrid_ItemDataBound;
        }
        public void SetEmailId(long emailId)
        {
            EmailId = emailId;
        }
        protected void Initialize()
        {
            //find the EmailRuleGrid
            WLPGrid grid = (WLPGrid)this.FindControl("EmailRuleGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        protected void LoadEmailRule()
        {
            Data = EmailRuleCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetEmailRule(EmailId));
        }
        #endregion

        #region event handlers
        void EmailRuleGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //if in edit mode, show the RadDropDownTree and select the associated department
            if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
            {
                GridEditFormItem item = (GridEditFormItem)e.Item;
                RadDropDownTree organizationUnitDropDownTree = (RadDropDownTree)item.FindControl("OrganizationUnitDropDownTree");

                if (organizationUnitDropDownTree != null)
                {
                    organizationUnitDropDownTree.DataSource = Common.ServiceWrapper.HumanResourcesClient.GetOrganizationUnitAssociationTree(LanguageCode, null);
                    organizationUnitDropDownTree.DefaultMessage = DropDownTreeDefaultMessage;
                    organizationUnitDropDownTree.DataBind();

                    if (!IsInsert && !IsViewMode)
                    {
                        BusinessLayer.BusinessObjects.EmailRule emailRule = (BusinessLayer.BusinessObjects.EmailRule)item.DataItem;

                        //asp hidden field to hold org hierarchy
                        HiddenField orgHierarchy = (HiddenField)item.FindControl("OrganizationUnitDropDownValues");
                        if (orgHierarchy != null)
                            orgHierarchy.Value = emailRule.OrganizationUnit;
                    }
                }
            }
        }
        void EmailRuleGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EmailRuleGrid.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        #endregion

        #region handle updates
        protected void RebindGrid()
        {
            LoadEmailRule();
            EmailRuleGrid.Rebind();
            EmailRuleGrid_NeedDataSource(null, null);
        }
        protected void EmailRuleGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            BusinessLayer.BusinessObjects.EmailRule item = (BusinessLayer.BusinessObjects.EmailRule)e.Item.DataItem;
            item.EmailId = EmailId;

            HiddenField hiddenOrg = (HiddenField)e.Item.FindControl("OrganizationUnitDropDownValues");
            if (hiddenOrg != null)
                item.OrganizationUnit = hiddenOrg.Value;

            Common.ServiceWrapper.HumanResourcesClient.InsertEmailRule(item).CopyTo((BusinessLayer.BusinessObjects.EmailRule)Data[item.Key]);
            RebindGrid();
        }
        protected void EmailRuleGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                BusinessLayer.BusinessObjects.EmailRule item = (BusinessLayer.BusinessObjects.EmailRule)e.Item.DataItem;

                HiddenField hiddenOrg = (HiddenField)e.Item.FindControl("OrganizationUnitDropDownValues");
                if (hiddenOrg != null)
                    item.OrganizationUnit = hiddenOrg.Value;

                Common.ServiceWrapper.HumanResourcesClient.UpdateEmailRule(item).CopyTo((BusinessLayer.BusinessObjects.EmailRule)Data[item.Key]);
                RebindGrid();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void EmailRuleGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteEmailRule((BusinessLayer.BusinessObjects.EmailRule)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    LoadEmailRule();
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}