﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmailSearchPage.aspx.cs" Inherits="WorkLinks.Admin.Email.EmailSearchPage" %>
<%@ Register Src="EmailSearchControl.ascx" TagName="EmailSearchControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:EmailSearchControl ID="EmailSearchControl1" runat="server" />
</asp:Content>