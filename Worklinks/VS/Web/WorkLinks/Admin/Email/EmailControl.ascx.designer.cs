﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace WorkLinks.Admin.Email {
    
    
    public partial class EmailControl {
        
        /// <summary>
        /// RadAjaxManagerProxy1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadAjaxManagerProxy RadAjaxManagerProxy1;
        
        /// <summary>
        /// MainPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel MainPanel;
        
        /// <summary>
        /// EmailTabStrip control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPTabStrip EmailTabStrip;
        
        /// <summary>
        /// EmailMultiPage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadMultiPage EmailMultiPage;
        
        /// <summary>
        /// EmailTemplatePageView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadPageView EmailTemplatePageView;
        
        /// <summary>
        /// EmailTemplateControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WorkLinks.Admin.Email.EmailTemplateControl EmailTemplateControl;
        
        /// <summary>
        /// EmailRulePageView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadPageView EmailRulePageView;
        
        /// <summary>
        /// EmailRuleControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WorkLinks.Admin.Email.EmailRule EmailRuleControl;
    }
}
