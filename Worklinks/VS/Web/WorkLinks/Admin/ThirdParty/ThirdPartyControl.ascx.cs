﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.ThirdParty
{
    public partial class ThirdPartyControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public bool IsViewMode { get { return ThirdPartyGrid.IsViewMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.ThirdParty.AddFlag; } }
        public bool ViewFlag { get { return Common.Security.RoleForm.ThirdParty.ViewFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.ThirdParty.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.ThirdParty.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.Wsib.ViewFlag);

            WireEvents();

            if (!IsPostBack)
                LoadThirdPartyInformation();

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.StartsWith("refresh"))
            {
                LoadThirdPartyInformation();
                ThirdPartyGrid.Rebind();
            }
        }
        protected void WireEvents()
        {
            ThirdPartyGrid.NeedDataSource += new GridNeedDataSourceEventHandler(ThirdPartyGrid_NeedDataSource);
        }
        protected void LoadThirdPartyInformation()
        {
            Data = CodeThirdPartyCollection.ConvertCollection(Common.ServiceWrapper.CodeClient.GetCodeThirdParty(null));
        }
        protected void CodePaycodeCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateComboBoxWithPaycodesByType((ICodeControl)sender, null);
        }
        #endregion

        #region event handlers
        void ThirdPartyGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            ThirdPartyGrid.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void ThirdPartyToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("delete") && ThirdPartyGrid.SelectedValue != null)
                DeleteThirdParty((CodeThirdParty)Data[ThirdPartyGrid.SelectedValue.ToString()]);
        }
        protected void DeleteThirdParty(CodeThirdParty thirdPartyCode)
        {
            try
            {
                Common.ServiceWrapper.CodeClient.DeleteThirdPartyCode(thirdPartyCode);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    ThirdPartyGrid.SelectedIndexes.Clear();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            LoadThirdPartyInformation();
            ThirdPartyGrid.Rebind();
        }
        #endregion

        #region security handlers
        protected void AddButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && AddFlag; }
        protected void DetailsButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && ViewFlag; }
        protected void DeleteButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && DeleteFlag; }
        #endregion
    }
}