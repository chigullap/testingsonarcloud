﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ThirdPartyControl.aspx.cs" Inherits="WorkLinks.Admin.ThirdParty.ThirdPartyControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPToolBar ID="ThirdPartyToolBar" runat="server" Width="100%" OnButtonClick="ThirdPartyToolBar_ButtonClick" OnClientLoad="ThirdPartyToolBar_init">
        <Items>
            <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" onclick="Add()" CommandName="add" ResourceName="Add" OnPreRender="AddButton_PreRender" />
            <wasp:WLPToolBarButton Text="Details" onclick="Open();" CommandName="details" ResourceName="Details" OnPreRender="DetailsButton_PreRender" />
            <wasp:WLPToolBarButton Text="Delete" ImageUrl="~\App_Themes\Default\Delete.gif" CommandName="delete" ResourceName="Delete" OnPreRender="DeleteButton_PreRender" />
        </Items>
    </wasp:WLPToolBar>

    <wasp:WLPGrid
        ID="ThirdPartyGrid"
        runat="server"
        AllowPaging="true"
        PagerStyle-AlwaysVisible="true"
        PageSize="100"
        AllowSorting="true"
        GridLines="None"
        AutoAssignModifyProperties="true">

        <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="false">
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
            <ClientEvents OnRowDblClick="OnRowDblClick" OnRowClick="OnRowClick" OnGridCreated="onGridCreated" OnCommand="onCommand" />
        </ClientSettings>

        <MasterTableView
            ClientDataKeyNames="CodeThirdPartyCd, EnglishDescription, FrenchDescription, AccountNumber, ImportExternalIdentifier"
            AutoGenerateColumns="false"
            DataKeyNames="CodeThirdPartyCd"
            CommandItemDisplay="Top"
            AllowSorting="false">

            <SortExpressions>
                <telerik:GridSortExpression FieldName="CodeThirdPartyCd" SortOrder="Ascending" />
            </SortExpressions>

            <CommandItemTemplate></CommandItemTemplate>

            <Columns>
                <wasp:GridBoundControl DataField="CodeThirdPartyCd" LabelText="**CodeThirdPartyCd**" SortExpression="CodeThirdPartyCd" UniqueName="CodeThirdPartyCd" ResourceName="CodeThirdPartyCd" />
                <wasp:GridBoundControl DataField="EnglishDescription" LabelText="**EnglishDescription**" SortExpression="EnglishDescription" UniqueName="EnglishDescription" ResourceName="EnglishDescription" />
                <wasp:GridBoundControl DataField="FrenchDescription" LabelText="**FrenchDescription**" SortExpression="FrenchDescription" UniqueName="FrenchDescription" ResourceName="FrenchDescription" />
                <wasp:GridBoundControl DataField="AccountNumber" LabelText="**AccountNumber**" SortExpression="AccountNumber" UniqueName="AccountNumber" ResourceName="AccountNumber" />
                <wasp:GridKeyValueControl OnNeedDataSource="CodePaycodeCode_NeedDataSource" DataField="CodePaycodeCode" LabelText="**CodePaycodeCode**" Type="CodePaycodeCode" ResourceName="CodePaycodeCode" />
                <wasp:GridBoundControl DataField="ImportExternalIdentifier" LabelText="**ImportExternalIdentifier**" SortExpression="ImportExternalIdentifier" UniqueName="ImportExternalIdentifier" ResourceName="ImportExternalIdentifier" />
            </Columns>

        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true" />

    </wasp:WLPGrid>
</asp:Panel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="ThirdPartyWindows"
    Width="1000"
    Height="600"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientClose="onClientClose"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var codeThirdPartyCode;
        var toolbar = null;

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function ThirdPartyToolBar_init(sender) {
            initializeControls();
        }

        function initializeControls() {
            if ($find('<%= ThirdPartyToolBar.ClientID %>') != null)
                toolbar = $find('<%= ThirdPartyToolBar.ClientID %>');
            enableButtons(getSelectedRow() != null);
        }

        function OnRowDblClick(sender, eventArgs) {
            var toolbar = $find('<%= ThirdPartyToolBar.ClientID %>');

            if (toolbar.findButtonByCommandName('details') != null)
                Open(sender);
        }

        function getSelectedRow() {
            var thirdPartyGrid = $find('<%= ThirdPartyGrid.ClientID %>');
            if (thirdPartyGrid == null)
                return null;

            var MasterTable = thirdPartyGrid.get_masterTableView();
            if (MasterTable == null)
                return null;

            var selectedRow = MasterTable.get_selectedItems();
            if (selectedRow.length == 1)
                return selectedRow[0];
            else
                return null;
        }

        function onGridCreated(sender, eventArgs) {
            var selectedRow = getSelectedRow();
            if (selectedRow != null) {
                codeThirdPartyCode = selectedRow.getDataKeyValue('CodeThirdPartyCd');
                enableButtons(true);
            }
            else
                codeThirdPartyCode = null;
        }

        function onCommand(sender, args) {
            if (args.get_commandName() == 'Page') {
                codeThirdPartyCode = null;
                initializeControls();
            }
        }

        function OnRowClick(sender, eventArgs) {
            codeThirdPartyCode = eventArgs.getDataKeyValue('CodeThirdPartyCd');
            enableButtons(true);
        }

        function enableButtons(enable) {
            var detailsButton = toolbar.findButtonByCommandName('details');
            var deleteButton = toolbar.findButtonByCommandName('delete');
            
            if (detailsButton != null)
                detailsButton.set_enabled(enable);

            if (deleteButton != null)
                deleteButton.set_enabled(enable);
        }

        function Open() {
            if (codeThirdPartyCode != null) {
                openWindowWithManager('ThirdPartyWindows', String.format('/Admin/ThirdParty/{0}/{1}', 'view', codeThirdPartyCode), false);
                return false;
            }
        }

        function Add() {
            openWindowWithManager('ThirdPartyWindows', String.format('/Admin/ThirdParty/{0}/{1}', 'add', 'x'), false);
            return false;
        }

        function onClientClose(sender, eventArgs) {
            var arg = sender.argument;
            if (arg != null && arg.closeWithChanges)
                __doPostBack('<%= MainPanel.ClientID %>', 'refresh');
        }
    </script>
</telerik:RadScriptBlock>