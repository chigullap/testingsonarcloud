﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ThirdPartyEditControl.ascx.cs" Inherits="WorkLinks.Admin.ThirdParty.ThirdPartyEditControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<wasp:WLPFormView
    ID="ThirdPartyDetailsView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key">

     <ItemTemplate>
        <wasp:WLPToolBar ID="ThirdPartyDetailsToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
            <Items>
                <wasp:WLPToolBarButton Text="Edit" Visible='<%# UpdateFlag %>' ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="Edit" ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>

         <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="CodeThirdPartyCd" ReadOnly="true" runat="server" ResourceName="CodeThirdPartyCd" Value='<%# Eval("CodeThirdPartyCd") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="CodeProvinceStateCode" ReadOnly="true" runat="server" Type="ProvinceStateCode" OnDataBinding="CodeProvinceStateCode_NeedDataSource" ResourceName="CodeProvinceStateCode" Value='<%# Eval("CodeProvinceStateCode") %>' Mandatory="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="EnglishDescription" ReadOnly="true" runat="server" ResourceName="EnglishDescription" Value='<%# Eval("EnglishDescription") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="FrenchDescription" ReadOnly="true" runat="server" ResourceName="FrenchDescription" Value='<%# Eval("FrenchDescription") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="AccountNumber" ReadOnly="true" runat="server" ResourceName="AccountNumber" Value='<%# Eval("AccountNumber") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="SortOrder" ReadOnly="true" runat="server" DecimalDigits="0" ResourceName="SortOrder" Value='<%# Eval("SortOrder") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="ImportExternalIdentifier" ReadOnly="true" runat="server" ResourceName="ImportExternalIdentifier" Value='<%# Eval("ImportExternalIdentifier") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="ActiveFlag" ReadOnly="true" runat="server" ResourceName="ActiveFlag" Value='<%# Eval("ActiveFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="CodePaycodeCode" ReadOnly="true" runat="server" Type="PaycodeCode" OnDataBinding="Code_NeedDataSource" ResourceName="CodePaycodeCode" Value='<%# Eval("CodePaycodeCode") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="ExcludeFromRemittanceExportFlag" ReadOnly="true" runat="server" ResourceName="ExcludeFromRemittanceExportFlag" Value='<%# Eval("ExcludeFromRemittanceExportFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="VendorId" runat="server" OnDataBinding="VendorCombo_NeedDataSource" ResourceName="VendorId" Value='<%# Eval("VendorId") %>' ReadOnly="true" Mandatory="true" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="SystemFlag" ReadOnly="true" runat="server" ResourceName="SystemFlag" Value='<%# Eval("SystemFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="CodeWcbChequeRemittanceFrequencyCd" ReadOnly="true" runat="server" Type="WcbChequeRemittanceFrequencyCode" OnDataBinding="Code_NeedDataSource" ResourceName="CodeWcbChequeRemittanceFrequency" Value='<%# Eval("CodeWcbChequeRemittanceFrequencyCd") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="BusinessNumberId" runat="server" OnDataBinding="BusinessNumberId_NeedDataSource" ResourceName="BusinessNumberId" Value='<%# Eval("BusinessNumberId") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="CodeRemittanceMethodCd" runat="server" Type="RemittanceMethodCode" OnDataBinding="Code_NeedDataSource" ResourceName="CodeRemittanceMethodCd" Value='<%# Bind("CodeRemittanceMethodCd") %>' Mandatory="true" ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="BankingCountryCode" runat="server" Type="BankingCountryCode" OnDataBinding="Code_NeedDataSource" ResourceName="BankingCountryCode" Value='<%# Bind("CodeBankingCountryCd") %>' ReadOnly="true" Mandatory="true"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="EmployeeBankingCode" runat="server" Type="EmployeeBankingTypeCode" OnDataBinding="EmployeeBankingCode_NeedDataSource" ResourceName="EmployeeBankingCode" Value='<%# Bind("CodeThirdPartyBankCd") %>' Mandatory="true" ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="Transit" runat="server" ResourceName="ThirdPartyTransitNumber" Value='<%# Bind("ThirdPartyTransitNumber") %>' ReadOnly="true" Mandatory="true" ></wasp:TextBoxControl>
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="BankAccountNumber" runat="server" ResourceName="ThirdPartyAccountNumber" Value='<%# Bind("ThirdPartyAccountNumber") %>' ReadOnly="true" Mandatory="true" ></wasp:TextBoxControl>
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>
    <EditItemTemplate>
        <wasp:WLPToolBar ID="ThirdPartyToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
            <Items>
                <wasp:WLPToolBarButton Text="*Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode  %>' ResourceName="Insert" CommandName="insert"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="*Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" ResourceName="Update" Visible='<%# IsEditMode %>'></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" Visible='<%# !IsInsertMode  %>' CommandName="cancel" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" Visible='<%# IsInsertMode  %>' onClick="Close()" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>

        <fieldset>
            <table width="100%">
                <tr> 
                    <td>
                        <wasp:TextBoxControl ID="CodeThirdPartyCd" ReadOnly='<%# IsEditMode || IsViewMode %>' runat="server" ResourceName="CodeThirdPartyCd" Value='<%# Bind("CodeThirdPartyCd") %>' Mandatory="true" TabIndex="010" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="CodeProvinceStateCode" runat="server" Type="ProvinceStateCode" OnDataBinding="CodeProvinceStateCode_NeedDataSource" ResourceName="ProvinceStateCode" Value='<%# Bind("CodeProvinceStateCode") %>' TabIndex="020" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="EnglishDescription" runat="server" ResourceName="EnglishDescription" Value='<%# Bind("EnglishDescription") %>' Mandatory="true" TabIndex="030" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="FrenchDescription" runat="server" ResourceName="FrenchDescription" Value='<%# Bind("FrenchDescription") %>' Mandatory="true" TabIndex="040" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="AccountNumber" runat="server" ResourceName="AccountNumber" Value='<%# Bind("AccountNumber") %>' Mandatory="true" TabIndex="050" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="SortOrder" runat="server" DecimalDigits="0" ResourceName="SortOrder" Value='<%# Bind("SortOrder") %>' Mandatory="true" TabIndex="060" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="ImportExternalIdentifier" runat="server" ResourceName="ImportExternalIdentifier" Value='<%# Bind("ImportExternalIdentifier") %>' Mandatory="true" TabIndex="070" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="ActiveFlag" runat="server" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>' TabIndex="080" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="CodePaycodeCode" runat="server" Type="PaycodeCode" OnDataBinding="Code_NeedDataSource" ResourceName="CodePaycodeCode" Value='<%# Bind("CodePaycodeCode") %>' Mandatory="true" TabIndex="080" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="ExcludeFromRemittanceExportFlag" runat="server" ResourceName="ExcludeFromRemittanceExportFlag" Value='<%# Bind("ExcludeFromRemittanceExportFlag") %>' TabIndex="90" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="VendorId" runat="server" OnDataBinding="VendorCombo_NeedDataSource" ResourceName="VendorId" Value='<%# Bind("VendorId") %>' Mandatory="true" TabIndex="100" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="SystemFlag" runat="server" ResourceName="SystemFlag" Value='<%# Bind("SystemFlag") %>' TabIndex="110" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="CodeWcbChequeRemittanceFrequencyCd" runat="server" Type="WcbChequeRemittanceFrequencyCode" OnDataBinding="Code_NeedDataSource" ResourceName="CodeWcbChequeRemittanceFrequency" Value='<%# Bind("CodeWcbChequeRemittanceFrequencyCd") %>' Mandatory="true" TabIndex="120" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="BusinessNumberId" runat="server" OnDataBinding="BusinessNumberId_NeedDataSource" ResourceName="BusinessNumberId" Value='<%# Bind("BusinessNumberId") %>' TabIndex="130" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="CodeRemittanceMethodCd" runat="server" IncludeEmptyItem="false" AutoPostback="true" OnDataBinding="Code_NeedDataSource" OnSelectedIndexChanged="CodeRemittanceMethodCd_SelectedIndexChanged" Type="RemittanceMethodCode" ResourceName="CodeRemittanceMethodCd" Value='<%# Bind("CodeRemittanceMethodCd") %>' TabIndex="140" />
                    </td>
                    <td>
                    </td>
                </tr>
               <tr>
                    <td>
                        <wasp:ComboBoxControl ID="BankingCountryCode" runat="server" Type="BankingCountryCode" AutoSelect="true" AutoPostback="true" OnDataBinding="BankingCountryCode_NeedDataSource" OnSelectedIndexChanged="BankingCountryCode_SelectedIndexChanged"  ResourceName="BankingCountryCode" Value='<%# Bind("CodeBankingCountryCd") %>' Mandatory="true" TabIndex="150"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="EmployeeBankingCode" runat="server" Type="EmployeeBankingTypeCode" ResourceName="EmployeeBankingCode" Value='<%# Bind("CodeThirdPartyBankCd") %>' Mandatory="true" TabIndex="160"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:WLPRegularExpressionValidator ID="val1Transit" ResourceName="val1Transit" Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="Transit" ValidationExpression="^\d{5}$"></wasp:WLPRegularExpressionValidator>
                        <wasp:TextBoxControl ID="Transit" MaxLength="5" runat="server" ResourceName="ThirdPartyTransitNumber" Value='<%# Bind("ThirdPartyTransitNumber") %>' ReadOnly="false" Mandatory="true" TabIndex="170"></wasp:TextBoxControl>
                    </td>
                    <td>
                        <wasp:WLPRegularExpressionValidator ID="val1AccountNumber" ResourceName="val1AccountNumber" Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="BankAccountNumber" ValidationExpression="^[0-9]{5,17}$"></wasp:WLPRegularExpressionValidator>
                        <wasp:TextBoxControl ID="BankAccountNumber" MaxLength="17" runat="server" ResourceName="ThirdPartyAccountNumber" Value='<%# Bind("ThirdPartyAccountNumber") %>' ReadOnly="false" Mandatory="true" TabIndex="180"></wasp:TextBoxControl>
                    </td>
                </tr>
            </table>
        </fieldset>

    </EditItemTemplate>

</wasp:WLPFormView>

<telerik:RadScriptBlock runat="server">
    <script type="text/javascript">
        function processClick(commandName) {
            var arg = new Object;
            arg.closeWithChanges = true;
            getRadWindow().argument = arg;
        }

        function getRadWindow() {
            var popup = null;

            if (window.radWindow)
                popup = window.radWindow;
            else if (window.frameElement.radWindow)
                popup = window.frameElement.radWindow;

            return popup;
        }

        function Close(button, args) {
            var popup = getRadWindow();
            setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
        }

        function ToolBarClick(sender, args) {
            var button = args.get_item();
            var commandName = button.get_commandName();
            processClick(commandName);

            if (<%= IsInsertMode.ToString().ToLower() %>) {
                if (commandName=="cancel") {
                    window.close();
                }
            }

        }
    </script>
</telerik:RadScriptBlock>
