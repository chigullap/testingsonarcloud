﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.ThirdParty
{
    public partial class ThirdPartyEditControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private string Action { get { return Page.RouteData.Values["action"].ToString().ToLower(); } }
        private string CodeThirdPartyCd { get { return Convert.ToString(Page.RouteData.Values["codeThirdPartyCode"]); } }
        //public bool PreAuthorizedDebitFlag { get { return Common.ApplicationParameter.PreAuthorizedDebitFlag; } }

        public bool IsEditMode { get { return ThirdPartyDetailsView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool IsInsertMode { get { return ThirdPartyDetailsView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsViewMode { get { return ThirdPartyDetailsView.CurrentMode.Equals(FormViewMode.ReadOnly); } }

        public bool AddFlag { get { return Common.Security.RoleForm.ThirdParty.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.ThirdParty.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.ThirdParty.DeleteFlag; } }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.ThirdParty.ViewFlag);

            WireEvents();

            if (Action == "view")
            {
                Page.Title = "Edit a Third Party Code";

                if (!IsPostBack)
                {
                    LoadThirdPartyInformation();
                }
            }
            else if (Action == "add")
            {
                if (!IsPostBack)
                {
                    Page.Title = "Add a Third Party Code";

                    CodeThirdPartyCollection newCollection = new CodeThirdPartyCollection();
                    newCollection.AddNew();
                    Data = newCollection;

                    //change formview's mode to Insert mode
                    ThirdPartyDetailsView.ChangeMode(FormViewMode.Insert);

                    //databind
                    ThirdPartyDetailsView.DataBind();
                }
                else if (IsInsertMode)
                {
                    //do nothing
                }
                else
                {
                    //we are loading the edit page after an add
                    Page.Title = "Edit a Third Party Code";
                }
            }
        }

        protected void LoadThirdPartyInformation()
        {
            //create business object and fill it with the data passed to the page, then bind the grid
            CodeThirdPartyCollection collection = new CodeThirdPartyCollection();
            collection.Add(LoadDataIntoBusinessClass());

            Data = collection;
            ThirdPartyDetailsView.DataBind();
        }

        protected CodeThirdParty LoadDataIntoBusinessClass()
        {
            CodeThirdPartyCollection temp = new CodeThirdPartyCollection();
            temp.Load(Common.ServiceWrapper.CodeClient.GetCodeThirdParty(CodeThirdPartyCd));

            return temp[0]; //will only ever have 1 item in this collection
        }

        protected void WireEvents()
        {
            ThirdPartyDetailsView.NeedDataSource += new WLPFormView.NeedDataSourceEventHandler(ThirdPartyDetailsView_NeedDataSource);
            ThirdPartyDetailsView.Updating += ThirdPartyDetailsView_Updating;
            ThirdPartyDetailsView.Inserting += ThirdPartyDetailsView_Inserting;
        }

        #region event handlers
        void ThirdPartyDetailsView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            ThirdPartyDetailsView.DataSource = Data;
        }

        protected void CodeProvinceStateCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateProvince((ICodeControl)sender);
        }

        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);

            ComboBoxControl currentCombo = (ComboBoxControl)sender;

            //show hide the banking info based on remittance method value
            if (currentCombo != null && currentCombo.ID == "BankingCountryCode")
                ShowHideBanking();
        }

        protected void VendorCombo_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateVendorComboControl((ICodeControl)sender);
        }

        protected void BusinessNumberId_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateBusinessNumber((ICodeControl)sender);
        }

        protected void BankingCountryCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
            BindEmployeeBankingCodeCombo(sender);

            //determine if we show the banking information on the details screen
            ShowHideBanking();
        }

        protected void BankingCountryCode_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindEmployeeBankingCodeCombo(o);
        }

        private void BindEmployeeBankingCodeCombo(object sender)
        {
            if (sender is ComboBoxControl)
            {
                ComboBoxControl bankingCountryCombo = (ComboBoxControl)sender;
                WLPFormView formItem = (WLPFormView)bankingCountryCombo.BindingContainer;
                ComboBoxControl employeeBankingCombo = (ComboBoxControl)formItem.FindControl("EmployeeBankingCode");
                string bankingCountryCode = bankingCountryCombo.Items.Count == 2 ? bankingCountryCombo.Items[1].Value : bankingCountryCombo.SelectedValue;

                employeeBankingCombo.DataSource = Common.ServiceWrapper.CodeClient.GetEmployeeBankCode(bankingCountryCode);
                employeeBankingCombo.DataBind();
            }
        }

        protected void EmployeeBankingCode_NeedDataSource(object sender, EventArgs e)
        {
            if (sender is ComboBoxControl employeeBankingCode)
            {
                WLPFormView formItem = (WLPFormView)employeeBankingCode.BindingContainer;
                ComboBoxControl bankingCountryCode = (ComboBoxControl)formItem.FindControl("bankingCountryCode");

                employeeBankingCode.DataSource = Common.ServiceWrapper.CodeClient.GetEmployeeBankCode(bankingCountryCode.SelectedValue);
            }
        }

        protected void CodeRemittanceMethodCd_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //show hide the banking info based on remittance method value
            ShowHideBanking();
        }

        private void ShowHideBanking()
        {
            //find the remit method
            ComboBoxControl remitMethodCombo = (ComboBoxControl)ThirdPartyDetailsView.FindControl("CodeRemittanceMethodCd");

            if (remitMethodCombo != null)
            {
                ComboBoxControl bankingCountryCode = (ComboBoxControl)ThirdPartyDetailsView.FindControl("BankingCountryCode");
                ComboBoxControl employeeBankingCodeCombo = (ComboBoxControl)ThirdPartyDetailsView.FindControl("EmployeeBankingCode");
                TextBoxControl transitTextBox = (TextBoxControl)ThirdPartyDetailsView.FindControl("Transit");
                TextBoxControl bankAccountTextBox = (TextBoxControl)ThirdPartyDetailsView.FindControl("BankAccountNumber");

                //hide banking info
                bool showBanking = (remitMethodCombo.Value.ToString() == "BANKING");

                if (bankingCountryCode != null)
                {
                    bankingCountryCode.Visible = showBanking;
                    if (!showBanking) bankingCountryCode.Value = null;
                }
                if (employeeBankingCodeCombo != null)
                {
                    employeeBankingCodeCombo.Visible = showBanking;
                    if (!showBanking) employeeBankingCodeCombo.Value = null;
                }
                if (transitTextBox != null)
                {
                    transitTextBox.Visible = showBanking;
                    if (!showBanking) transitTextBox.Value = null;
                }
                if (bankAccountTextBox != null)
                {
                    bankAccountTextBox.Visible = showBanking;
                    if (!showBanking) bankAccountTextBox.Value = null;
                }

                ToggleValidators(showBanking);
            }
        }

        protected void ToggleValidators(bool toggle)
        {
            WLPRegularExpressionValidator transitValidator = (WLPRegularExpressionValidator)ThirdPartyDetailsView.FindControl("val1Transit");
            if (transitValidator != null)
                transitValidator.Enabled = toggle;

            WLPRegularExpressionValidator accountValidator = (WLPRegularExpressionValidator)ThirdPartyDetailsView.FindControl("val1AccountNumber");
            if (accountValidator != null)
                accountValidator.Enabled = toggle;
        }

        #endregion

        #region handle updates
        void ThirdPartyDetailsView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            CodeThirdParty item = (CodeThirdParty)e.DataItem;
            Common.ServiceWrapper.CodeClient.InsertThirdPartyCode(item).CopyTo((CodeThirdParty)Data[item.Key]);
        }
        void ThirdPartyDetailsView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                CodeThirdParty item = (CodeThirdParty)e.DataItem;
                Common.ServiceWrapper.CodeClient.UpdateThirdPartyCode(item).CopyTo((CodeThirdParty)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, GetType(), "PopupScript", string.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
