﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormSecurityControl.ascx.cs" Inherits="WorkLinks.Admin.FormSecurityEditor.FormSecurityControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPGrid
        ID="FormGrid"
        EditMode="InPlace"
        runat="server"
        GridLines="None"
        Width="100%"
        Height="550px"
        AutoGenerateColumns="false"
        AutoInsertOnAdd="false"
        AllowMultiRowEdit="true"
        Skin="">

        <ClientSettings>
            <Selecting AllowRowSelect="false" />
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key" TableLayout="Fixed">
            <CommandItemTemplate>
                <wasp:WLPToolBar ID="FormGridToolBar" runat="server" Width="100%" AutoPostBack="true">
                    <Items>
                        <wasp:WLPToolBarButton Text="Edit" ImageUrl="~/App_Themes/Default/edit.gif" CommandName="EditAll" Visible='<%# IsViewMode && UpdateFlag %>' CausesValidation="true" ResourceName="Edit" />
                        <wasp:WLPToolBarButton Text="Update All" ImageUrl="~/App_Themes/Default/update.gif" CommandName="UpdateAll" Visible='<%# !IsViewMode %>' CausesValidation="true" ResourceName="Update" />
                        <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/cancel.gif" CommandName="CancelAll" Visible='<%# !IsViewMode %>' ResourceName="Cancel" />
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridBoundControl DataField="FormDescription" UniqueName="FormDescription" ResourceName="FormDescriptionField">
                    <HeaderStyle Width="300px" />
                </wasp:GridBoundControl>
                <wasp:GridCheckBoxControl DataField="ViewFlag" LabelText="ViewFlag" UniqueName="ViewFlag" ResourceName="ViewFlag" />
                <wasp:GridCheckBoxControl DataField="AddFlag" LabelText="AddFlag" UniqueName="AddFlag" ResourceName="AddFlag" />
                <wasp:GridCheckBoxControl DataField="UpdateFlag" LabelText="UpdateFlag" UniqueName="UpdateFlag" ResourceName="UpdateFlag" />
                <wasp:GridCheckBoxControl DataField="DeleteFlag" LabelText="DeleteFlag" UniqueName="DeleteFlag" ResourceName="DeleteFlag" />
            </Columns>
        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true" />

    </wasp:WLPGrid>
</asp:Panel>