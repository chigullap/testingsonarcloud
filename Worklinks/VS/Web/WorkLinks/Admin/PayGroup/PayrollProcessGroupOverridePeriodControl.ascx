﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayrollProcessGroupOverridePeriodControl.ascx.cs" Inherits="WorkLinks.Admin.PayGroup.PayrollProcessGroupOverridePeriodControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPGrid
        ID="OverridePeriodGrid"
        runat="server"
        GridLines="None"
        AutoGenerateColumns="false"
        AutoAssignModifyProperties="true"
        OnInsertCommand="OverridePeriodGrid_InsertCommand"
        OnUpdateCommand="OverridePeriodGrid_UpdateCommand"
        OnDeleteCommand="OverridePeriodGrid_DeleteCommand">

        <ClientSettings EnablePostBackOnRowClick="false" AllowColumnsReorder="false" ReorderColumnsOnClient="true">
            <Selecting AllowRowSelect="false" />
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="Year" SortOrder="Descending" />
            </SortExpressions>

            <CommandItemTemplate>
                <wasp:WLPToolBar ID="OverridePeriodToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
                    <Items>
                        <wasp:WLPToolBarButton Text="*Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add"></wasp:WLPToolBarButton>
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif"></wasp:GridEditCommandControl>
                <wasp:GridBoundControl DataField="Year" LabelText="*Year" UniqueName="Year" ResourceName="Year"></wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="Period" LabelText="*Period" UniqueName="Period" ResourceName="Period"></wasp:GridBoundControl>
                <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>"></wasp:GridButtonControl>
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="Year" runat="server" ResourceName="Year" Value='<%# Bind("Year") %>' Mandatory="true" TabIndex="010" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="Period" runat="server" ResourceName="Period" Value='<%# Bind("Period") %>' Mandatory="true" TabIndex="020" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update"></wasp:WLPButton>
                                            <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsInsert %>' ResourceName="Insert"></wasp:WLPButton>
                                        </td>
                                        <td>
                                            <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="Cancel" CausesValidation="false" ResourceName="Cancel"></wasp:WLPButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true"></HeaderContextMenu>

    </wasp:WLPGrid>
</asp:Panel>