﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayrollProcessingGroupControl.ascx.cs" Inherits="WorkLinks.Admin.PayGroup.PayrollProcessingGroupControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPToolBar ID="PayrollProcessGroupToolBar" runat="server" Width="100%" OnButtonClick="PayrollProcessGroupToolBar_ButtonClick" OnClientLoad="PayrollProcessGroupToolBar_init">
        <Items>
            <wasp:WLPToolBarButton runat="server" Text="*Add" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsViewMode && AddFlag %>' CommandName="Add" ResourceName="Add" OnPreRender="AddButton_PreRender" />
            <wasp:WLPToolBarButton runat="server" Text="*OverridePeriod" onclick="OpenOverridePeriod();" CommandName="OverridePeriod" ResourceName="OverridePeriod" OnPreRender="OverridePeriodButton_PreRender" />
        </Items>
    </wasp:WLPToolBar>

    <wasp:WLPGrid
        ID="PayrollProcessGroupGrid"
        runat="server"
        GridLines="None"
        AllowPaging="true"
        PagerStyle-AlwaysVisible="true"
        PageSize="100"
        AutoGenerateColumns="false"
        OnDeleteCommand="PayrollProcessGroupGrid_DeleteCommand"
        OnInsertCommand="PayrollProcessGroupGrid_InsertCommand"
        OnUpdateCommand="PayrollProcessGroupGrid_UpdateCommand"
        OnItemDataBound="PayrollProcessGroupGrid_ItemDataBound"
        AutoAssignModifyProperties="true">

        <ClientSettings EnablePostBackOnRowClick="false" AllowColumnsReorder="true" ReorderColumnsOnClient="true">
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
            <ClientEvents OnRowClick="onRowClick" OnGridCreated="onGridCreated" OnCommand="onCommand" />
        </ClientSettings>

        <MasterTableView ClientDataKeyNames="PayrollProcessGroupCode" CommandItemDisplay="Top" DataKeyNames="PayrollProcessGroupCode">

            <SortExpressions>
                <telerik:GridSortExpression FieldName="PayrollProcessGroupCountryCode, PayrollProcessGroupCode" SortOrder="Ascending" />
            </SortExpressions>

            <CommandItemTemplate></CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="**PayrollProcessGroupCountryCode**" DataField="PayrollProcessGroupCountryCode" Type="PayrollProcessGroupCountryCode" ResourceName="PayrollProcessGroupCountryCode" />
                <wasp:GridBoundControl DataField="PayrollProcessGroupCode" LabelText="**PayrollProcessGroupCode**" SortExpression="PayrollProcessGroupCode" UniqueName="PayrollProcessGroupCode" ResourceName="PayrollProcessGroupCode" />
                <wasp:GridBoundControl DataField="PayrollProcessGroupEnglishDesc" LabelText="**PayrollProcessGroupEnglishDesc**" SortExpression="PayrollProcessGroupEnglishDesc" UniqueName="PayrollProcessGroupEnglishDesc" ResourceName="PayrollProcessGroupEnglishDesc" />
                <wasp:GridBoundControl DataField="PayrollProcessGroupFrenchDesc" LabelText="**PayrollProcessGroupFrenchDesc**" SortExpression="PayrollProcessGroupFrenchDesc" UniqueName="PayrollProcessGroupFrenchDesc" ResourceName="PayrollProcessGroupFrenchDesc" />
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="**PaymentFrequencyCode**" DataField="PaymentFrequencyCode" Type="PaymentFrequencyTypeCode" ResourceName="PaymentFrequencyCode" />
                <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="PayrollProcessGroupCountryCode" runat="server" Type="PayrollProcessGroupCountryCode" AutoSelect="true" OnDataBinding="Code_NeedDataSource" ResourceName="PayrollProcessGroupCountryCode" Value='<%# Bind("PayrollProcessGroupCountryCode") %>' Mandatory="true" ReadOnly='<%# !IsInsert %>' TabIndex="020" />
                            </td>
                            <td>
                                <wasp:CheckBoxControl ID="ManualChequeFlag" runat="server" ResourceName="ManualChequeFlag" Value='<%# Bind("ManualChequeFlag") %>' ReadOnly="false"></wasp:CheckBoxControl>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="PaymentFrequencyCode" runat="server" Type="PaymentFrequencyTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="PaymentFrequencyCode" Value='<%# Bind("PaymentFrequencyCode") %>' AutoPostback="true" OnSelectedIndexChanged="PaymentFrequencyCode_SelectedIndexChanged" Mandatory="true" ReadOnly='<%# !IsInsert %>' TabIndex="010" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="PayrollProcessGroupCode" runat="server" ResourceName="PayrollProcessGroupCode" Value='<%# Bind("PayrollProcessGroupCode") %>' Mandatory="true" ReadOnly='<%# !IsInsert %>' TabIndex="030" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="PayrollProcessGroupEnglishDesc" runat="server" ResourceName="PayrollProcessGroupEnglishDesc" Value='<%# Bind("PayrollProcessGroupEnglishDesc") %>' Mandatory="true" TabIndex="040" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="PayrollProcessGroupFrenchDesc" runat="server" ResourceName="PayrollProcessGroupFrenchDesc" Value='<%# Bind("PayrollProcessGroupFrenchDesc") %>' Mandatory="true" TabIndex="050" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="PeriodYear" runat="server" ResourceName="PeriodYear" Value='<%# Bind("PeriodYear") %>' Mandatory="true" ReadOnly="true" TabIndex="060" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="Period" runat="server" ResourceName="Period" Value='<%# Bind("Period") %>' Mandatory="true" ReadOnly='<%# !IsInsert %>' TabIndex="070" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:DateControl ID="PeriodStartDate" runat="server" ResourceName="PeriodStartDate" Value='<%# Bind("PeriodStartDate") %>' AutoPostback="true" OnSelectedDateChanged="PeriodStartDate_SelectedDateChanged" Mandatory="true" ReadOnly='<%# !IsInsert %>' TabIndex="080" />
                                <asp:CustomValidator ForeColor="Red" ID="PeriodStartDateValidator" runat="server" ControlToValidate="PeriodStartDate" OnServerValidate="ValidatePeriodStartDate" />
                            </td>
                            <td>
                                <wasp:DateControl ID="PeriodCutoffDate" runat="server" ResourceName="PeriodCutoffDate" Value='<%# Bind("PeriodCutoffDate") %>' Mandatory="true" ReadOnly='<%# DontAllowEdit %>' TabIndex="090" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                            <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsInsert %>' ResourceName="Insert" />
                                        </td>
                                        <td>
                                            <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="Cancel" CausesValidation="false" ResourceName="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true" />

    </wasp:WLPGrid>
</asp:Panel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="PayrollProcessGroupWindows"
    Width="500"
    Height="500"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var toolbar;
        var payrollProcessGroupCode;

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function PayrollProcessGroupToolBar_init(sender) {
            initializeControls();
        }

        function initializeControls() {
            if ($find('<%= PayrollProcessGroupToolBar.ClientID %>') != null)
                toolbar = $find('<%= PayrollProcessGroupToolBar.ClientID %>');

            var overridePeriodButton = toolbar.findButtonByCommandName('OverridePeriod');
            if (overridePeriodButton != null) {
                if (getSelectedRow() != null)
                    overridePeriodButton.set_enabled(true);
                else
                    overridePeriodButton.set_enabled(false);
            }
        }

        function getSelectedRow() {
            var grid = $find('<%= PayrollProcessGroupGrid.ClientID %>');
            if (grid == null)
                return null;

            var masterTable = grid.get_masterTableView();
            if (masterTable == null)
                return null;

            var selectedRow = masterTable.get_selectedItems();
            if (selectedRow.length == 1)
                return selectedRow[0];
            else
                return null;
        }

        function onGridCreated(sender, eventArgs) {
            var selectedRow = getSelectedRow();
            if (selectedRow != null)
                payrollProcessGroupCode = selectedRow.getDataKeyValue('PayrollProcessGroupCode');
        }

        function onCommand(sender, args) {
            if (args.get_commandName() == 'Page') {
                payrollProcessGroupCode = null;
                initializeControls();
            }
        }

        function onRowClick(sender, eventArgs) {
            payrollProcessGroupCode = eventArgs.getDataKeyValue('PayrollProcessGroupCode');
            
            var overridePeriodButton = toolbar.findButtonByCommandName('OverridePeriod');
            if (overridePeriodButton != null)
                overridePeriodButton.set_enabled(true);
        }

        function ButtonGetEnabled(button) {
            return toolbar.findButtonByCommandName('' + button + '').get_enabled();
        }

        function OpenOverridePeriod() {
            if (payrollProcessGroupCode != null && ButtonGetEnabled('OverridePeriod')) {
                openWindowWithManager('PayrollProcessGroupWindows', String.format('/Admin/PayGroup/OverridePeriods/{0}', payrollProcessGroupCode), false);
                return false;
            }
        }
    </script>
</telerik:RadScriptBlock>