﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.PayGroup
{
    public partial class PayrollProcessingGroupControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public bool IsViewMode { get { return PayrollProcessGroupGrid.IsViewMode; } }
        public bool IsUpdate { get { return PayrollProcessGroupGrid.IsEditMode; } }
        public bool IsInsert { get { return PayrollProcessGroupGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.PayrollProcessingGroup.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.PayrollProcessingGroup.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.PayrollProcessingGroup.DeleteFlag; } }
        protected bool OverridePeriodMenuItem { get { return Common.Security.RoleForm.PayrollProcessGroupOverridePeriod.ViewFlag; } }
        public bool DontAllowEdit { get { return true; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.PayrollProcessingGroup.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                LoadProcessingGroupInformation();
                Initialize();
            }
        }
        protected void WireEvents()
        {
            PayrollProcessGroupGrid.NeedDataSource += new GridNeedDataSourceEventHandler(PayrollProcessGroupGrid_NeedDataSource);
        }
        protected void Initialize()
        {
            //find the PayrollProcessGroupGrid
            WLPGrid grid = (WLPGrid)this.FindControl("PayrollProcessGroupGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        protected void LoadProcessingGroupInformation()
        {
            Data = PayrollProcessingGroupCollection.ConvertCollection(Common.ServiceWrapper.CodeClient.GetPayrollProcessingGroup());
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            InitializeControls();
        }
        protected void InitializeControls()
        {
            //this code makes sure the RadToolbar is set to the proper status before/during/after update/insert operations
            PayrollProcessGroupToolBar.Enabled = IsViewMode;
        }
        #endregion

        #region event handlers
        protected void PayrollProcessGroupToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            //force the grid to be in insert mode
            if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("add"))
                ((GridCommandItem)PayrollProcessGroupGrid.MasterTableView.GetItems(GridItemType.CommandItem)[0]).FireCommandEvent(RadGrid.InitInsertCommandName, String.Empty);
        }
        protected void PayrollProcessGroupGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //when in insert mode, populate the period year from the database function udf_getCurrentPayrollYear
            if (e.Item is GridEditFormInsertItem)
            {
                GridEditFormItem item = (GridEditFormItem)e.Item;
                TextBoxControl year = (TextBoxControl)item.FindControl("PeriodYear");
                year.Value = Common.ServiceWrapper.CodeClient.GetCurrentPayrollYear();
            }
        }
        void PayrollProcessGroupGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            PayrollProcessGroupGrid.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void PaymentFrequencyCode_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ComboBoxControl combo = (ComboBoxControl)sender;
            GridEditFormItem formItem = (GridEditFormItem)combo.BindingContainer;
            DateControl periodStartDate = (DateControl)formItem.FindControl("PeriodStartDate");
            DateControl periodCutoffDate = (DateControl)formItem.FindControl("PeriodCutoffDate");

            //wipe the start and cutoff dates
            if (periodStartDate != null) periodStartDate.Value = null;
            if (periodCutoffDate != null)
            {
                periodCutoffDate.Value = null;
                //default this to readonly.  It can only be overridden in the SM case wheres start day is not 1 or 16.
                periodCutoffDate.ReadOnly = true;
            }
        }
        protected void PeriodStartDate_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            DateControl periodStartDate = (DateControl)sender;
            GridEditFormItem formItem = (GridEditFormItem)periodStartDate.BindingContainer;
            DateControl periodCutoffDate = (DateControl)formItem.FindControl("PeriodCutoffDate");
            String paymentFrequency = null;

            if (((ComboBoxControl)formItem.FindControl("PaymentFrequencyCode")).Value != null)
                paymentFrequency = ((ComboBoxControl)formItem.FindControl("PaymentFrequencyCode")).Value.ToString();
            
            if (paymentFrequency != null && periodCutoffDate != null)
            {
                DateTime startDate = (DateTime)periodStartDate.Value;
                int day = startDate.Day;
                int month = startDate.Month;
                int year = startDate.Year;
                int daysInMonth = DateTime.DaysInMonth(year, month);
                bool isLeapYear = DateTime.IsLeapYear(year);

                //default this to readonly.  It can only be overridden in the SM case wheres start day is not 1 or 16.
                periodCutoffDate.ReadOnly = true;

                //for the yearly pay frequency
                if (paymentFrequency == "1")
                {
                    //if it is a leap year, the cutoff date is the start date + 365 days
                    if (isLeapYear)
                        periodCutoffDate.Value = startDate.AddDays(365);

                    //if it is not a leap year, the cutoff date is the start date + 364 days
                    else
                        periodCutoffDate.Value = startDate.AddDays(364);
                }

                //for the half-year pay frequency, the cutoff date is the start date + 6 months less a day
                else if (paymentFrequency == "2")
                    periodCutoffDate.Value = startDate.AddMonths(6).AddDays(-1);

                //for the quarterly pay frequency, the cutoff date is the start date + 3 months less a day
                else if (paymentFrequency == "4")
                    periodCutoffDate.Value = startDate.AddMonths(3).AddDays(-1);

                //for the bi-monthly pay frequency, the cutoff date is the start date + 2 months less a day
                else if (paymentFrequency == "6")
                    periodCutoffDate.Value = startDate.AddMonths(2).AddDays(-1);

                //for the monthly pay frequency, the cutoff date is the last day of the month
                else if (paymentFrequency == "12")
                    periodCutoffDate.Value = startDate.AddDays(daysInMonth - day);

                //for the semi-monthly pay frequency
                else if (paymentFrequency == "24")
                {
                    //if the start date is the first of the month, the cutoff date is the 15th day of the month
                    if (day == 1)
                        periodCutoffDate.Value = startDate.AddDays(14);

                    //if the start date is on the 16th day of the month, the cutoff date is the last day of the month
                    else if (day == 16)
                        periodCutoffDate.Value = startDate.AddDays(daysInMonth - day);

                    //this does not follow the 1-15, 16-EOM pattern so make the cutoff date editable
                    else
                    {
                        periodCutoffDate.ReadOnly = false;
                        periodCutoffDate.Value = null;
                    }
                }

                //for the bi-weekly pay frequency, the cutoff date is the start date + 13 days
                else if (paymentFrequency == "26/27")
                    periodCutoffDate.Value = startDate.AddDays(13);

                //for the weekly pay frequency, the cutoff date is the start date + 6 days
                else if (paymentFrequency == "52/53")
                    periodCutoffDate.Value = startDate.AddDays(6);
            }
        }
        public void ValidatePeriodStartDate(object sender, ServerValidateEventArgs args)
        {
            CustomValidator validator = (CustomValidator)sender;
            GridEditFormItem formItem = (GridEditFormItem)validator.BindingContainer;
            String paymentFrequency = ((ComboBoxControl)formItem.FindControl("PaymentFrequencyCode")).Value.ToString();
            DateControl periodStartDate = (DateControl)formItem.FindControl("PeriodStartDate");
            args.IsValid = true;

            if (paymentFrequency != null && periodStartDate != null)
            {
                int day = ((DateTime)periodStartDate.Value).Day;
                String errorMessage = null;

                //for the monthly pay frequency, the start date can only be the first day of the month
                if (paymentFrequency == "12" && day != 1)
                {
                    args.IsValid = false;
                    errorMessage = String.Format((String)GetGlobalResourceObject("ErrorMessages", "PaymentFrequencyMonthly"));
                }

                if (!args.IsValid)
                    validator.ErrorMessage = errorMessage;
            }
        }
        #endregion

        #region handle updates
        protected void PayrollProcessGroupGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            PayrollProcessingGroup ppGroup = (PayrollProcessingGroup)e.Item.DataItem;
            Common.ServiceWrapper.CodeClient.InsertPayrollProcessingGroupCode(ppGroup).CopyTo((PayrollProcessingGroup)Data[String.Empty]);

            //clear all codes from assembly
            Common.CodeHelper.ResetCodeTableCache();
        }
        protected void PayrollProcessGroupGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                PayrollProcessingGroup ppGroup = (PayrollProcessingGroup)e.Item.DataItem;
                Common.ServiceWrapper.CodeClient.UpdatePayrollProcessingGroup(ppGroup).CopyTo((PayrollProcessingGroup)Data[ppGroup.Key]);

                //clear all codes from assembly
                Common.CodeHelper.ResetCodeTableCache();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void PayrollProcessGroupGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.CodeClient.DeletePayrollProcessingGroup((PayrollProcessingGroup)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region security handlers
        protected void AddButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && AddFlag; }
        protected void OverridePeriodButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && OverridePeriodMenuItem; }
        #endregion
    }
}