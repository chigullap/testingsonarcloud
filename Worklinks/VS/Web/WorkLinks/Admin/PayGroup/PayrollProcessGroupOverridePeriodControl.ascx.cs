﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.PayGroup
{
    public partial class PayrollProcessGroupOverridePeriodControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected String PayrollProcessGroupCode
        {
            get
            {
                if (Page.RouteData.Values["payrollProcessGroupCode"] == null)
                    return null;
                else
                    return Page.RouteData.Values["payrollProcessGroupCode"].ToString();
            }
        }
        public bool IsViewMode
        {
            get { return OverridePeriodGrid.IsViewMode; }
        }
        public bool IsUpdate
        {
            get { return OverridePeriodGrid.IsEditMode; }
        }
        public bool IsInsert
        {
            get { return OverridePeriodGrid.IsInsertMode; }
        }
        public bool AddFlag
        {
            get { return Common.Security.RoleForm.PayrollProcessGroupOverridePeriod.AddFlag; }
        }
        public bool UpdateFlag
        {
            get { return Common.Security.RoleForm.PayrollProcessGroupOverridePeriod.UpdateFlag; }
        }
        public bool DeleteFlag
        {
            get { return Common.Security.RoleForm.PayrollProcessGroupOverridePeriod.DeleteFlag; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.PayrollProcessGroupOverridePeriod.ViewFlag);

            this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "OverridePeriods"));

            WireEvents();

            if (!IsPostBack)
            {
                LoadOverridePeriod();
                Initialize();
            }
        }
        protected void WireEvents()
        {
            OverridePeriodGrid.NeedDataSource += new GridNeedDataSourceEventHandler(OverridePeriodGrid_NeedDataSource);
        }
        protected void Initialize()
        {
            //find the OverridePeriodGrid
            WLPGrid grid = (WLPGrid)this.FindControl("OverridePeriodGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        protected void LoadOverridePeriod()
        {
            Data = PayrollProcessGroupOverridePeriodCollection.ConvertCollection(Common.ServiceWrapper.CodeClient.GetPayrollProcessGroupOverridePeriod(PayrollProcessGroupCode));
        }
        #endregion

        #region event handlers
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        void OverridePeriodGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            OverridePeriodGrid.DataSource = Data;
        }
        #endregion

        #region handle updates
        protected void OverridePeriodGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                PayrollProcessGroupOverridePeriod item = (PayrollProcessGroupOverridePeriod)e.Item.DataItem;
                item.PayrollProcessGroupCode = PayrollProcessGroupCode;

                Common.ServiceWrapper.CodeClient.InsertPayrollProcessGroupOverridePeriod(item).CopyTo((PayrollProcessGroupOverridePeriod)Data[item.Key]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void OverridePeriodGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                PayrollProcessGroupOverridePeriod item = (PayrollProcessGroupOverridePeriod)e.Item.DataItem;
                Common.ServiceWrapper.CodeClient.UpdatePayrollProcessGroupOverridePeriod(item).CopyTo((PayrollProcessGroupOverridePeriod)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);

                e.Canceled = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void OverridePeriodGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.CodeClient.DeletePayrollProcessGroupOverridePeriod((PayrollProcessGroupOverridePeriod)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);

                LoadOverridePeriod();
                e.Canceled = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}