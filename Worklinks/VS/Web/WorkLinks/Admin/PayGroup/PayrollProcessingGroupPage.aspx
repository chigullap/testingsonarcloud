﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PayrollProcessingGroupPage.aspx.cs" Inherits="WorkLinks.Admin.PayGroup.PayrollProcessingGroupPage" ResourceName="PayrollProcessingGroupPage" %>
<%@ Register Src="PayrollProcessingGroupControl.ascx" TagName="PayrollProcessingGroupControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:PayrollProcessingGroupControl ID="PayrollProcessingGroupControl1" runat="server" />
</asp:Content>