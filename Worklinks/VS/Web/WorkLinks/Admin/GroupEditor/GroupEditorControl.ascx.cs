﻿using System;
using System.Security.Claims;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Common;

namespace WorkLinks.Admin.GroupEditor
{
    public partial class GroupEditorControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private long GroupId { get { return Convert.ToInt64(Page.RouteData.Values["groupId"]); } }
        public bool IsEditMode { get { return GroupInfoView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.Edit); } }
        public bool IsInsertMode { get { return GroupInfoView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.Insert); } }
        public bool IsViewMode { get { return GroupInfoView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.ReadOnly); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.GroupEdit.UpdateFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.GroupEdit.ViewFlag);

            WireEvents();

            if ((Page.RouteData.Values["action"].ToString().ToLower() == "view"))
            {
                this.Page.Title = "Edit a Group";

                if (!IsPostBack) LoadGroupInformation();
            }
            else if ((Page.RouteData.Values["action"].ToString().ToLower() == "add"))
            {
                if (!IsPostBack)
                {
                    this.Page.Title = "Add a Group";

                    GroupDescriptionCollection newGroupCollection = new GroupDescriptionCollection();
                    newGroupCollection.AddNew();
                    Data = newGroupCollection;

                    // Change formview's mode to Insert mode
                    GroupInfoView.ChangeMode(System.Web.UI.WebControls.FormViewMode.Insert);

                    // Databind
                    GroupInfoView.DataBind();
                }
                else
                    this.Page.Title = "Edit a Group"; // We are loading the edit page after an add
            }

            if (!checkForWorkLinksAdministrationFlagIsValid()) SetAuthorized(false);
        }
        protected bool checkForWorkLinksAdministrationFlagIsValid()
        {
            bool checkForWorkLinksAdministrationFlag = false;

            if (GetSecurityUser().WorklinksAdministrationFlag)
                checkForWorkLinksAdministrationFlag = true;
            else
                checkForWorkLinksAdministrationFlag = !((GroupDescription)Data[0]).WorklinksAdministrationFlag;

            return checkForWorkLinksAdministrationFlag;
        }
        protected void WireEvents()
        {
            GroupInfoView.PreRender += new EventHandler(GroupInfoView_PreRender);
            GroupInfoView.NeedDataSource += new WLP.Web.UI.Controls.WLPFormView.NeedDataSourceEventHandler(GroupInfoView_NeedDataSource);
            GroupInfoView.Updating += new WLP.Web.UI.Controls.WLPFormView.ItemUpdatingEventHandler(GroupInfoView_Updating);
            GroupInfoView.Inserting += new WLP.Web.UI.Controls.WLPFormView.ItemInsertingEventHandler(GroupInfoView_Inserting);
        }
        protected SecurityUser GetSecurityUser()
        {
            //String loggedInUser = ((ClaimsIdentity)System.Web.HttpContext.Current.User.Identity).FindFirst("uname").Value;//yuc
            String loggedInUser = System.Web.HttpContext.Current.User.Identity.Name.ToString();
            SecurityUser user = Common.ServiceWrapper.SecurityClient.GetSecurityUser(loggedInUser)[0];

            return user;
        }
        protected void LoadGroupInformation()
        {
            // Create business object and fill it with the data passed to the page, then bind the grid.
            GroupDescriptionCollection grp = new GroupDescriptionCollection();
            grp.Add(LoadDataIntoBusinessClass());

            Data = grp;
            GroupInfoView.DataBind();
        }
        GroupDescription LoadDataIntoBusinessClass()
        {
            GroupDescriptionCollection temp = new GroupDescriptionCollection();
            temp.Load(Common.ServiceWrapper.SecurityClient.GetGroupDescriptions(null, GroupId, null));

            return temp[0]; // Will only ever have 1 item in this collection
        }
        #endregion

        #region event handlers
        void GroupInfoView_PreRender(object sender, EventArgs e)
        {
            WLP.Web.UI.Controls.CheckBoxControl worklinksAdmin = (WLP.Web.UI.Controls.CheckBoxControl)GroupInfoView.FindControl("WorklinksAdministrationFlag");

            if (worklinksAdmin != null)
                if (!GetSecurityUser().WorklinksAdministrationFlag) worklinksAdmin.Style.Add("display", "none");
        }
        void GroupInfoView_NeedDataSource(object sender, WLP.Web.UI.Controls.WLPNeedDataSourceEventArgs e)
        {
            GroupInfoView.DataSource = Data;
        }
        protected void PopulateCombo(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateComboBoxWithRoleDescriptionForGroupScreen((WLP.Web.UI.Controls.ICodeControl)sender, LanguageCode, !GetSecurityUser().WorklinksAdministrationFlag);
        }
        #endregion

        #region handle updates
        void GroupInfoView_Updating(object sender, WLP.Web.UI.Controls.WLPItemUpdatingEventArgs e)
        {
            try
            {
                GroupDescription grpDesc = (GroupDescription)e.DataItem; // One description being updated
                Common.ServiceWrapper.SecurityClient.UpdateGroupDesc(ApplicationParameter.AuthDatabaseName, ApplicationParameter.SecurityClientUsedByAngular, grpDesc).CopyTo((GroupDescription)Data[grpDesc.Key]);

                if (e.CommandArgument.ToString() == "close")
                {
                    System.Web.UI.ClientScriptManager manager = Page.ClientScript;
                    manager.RegisterStartupScript(this.GetType(), "close and return", "closeAndReturn();", true);
                }
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void GroupInfoView_Inserting(object sender, WLP.Web.UI.Controls.WLPItemInsertingEventArgs e)
        {
            GroupDescription grpDesc = (GroupDescription)e.DataItem;
            Common.ServiceWrapper.SecurityClient.InsertGroupDescription(ApplicationParameter.AuthDatabaseName, ApplicationParameter.SecurityClientUsedByAngular, grpDesc).CopyTo((GroupDescription)Data[grpDesc.Key]);

            if (e.CommandArgument.ToString() == "close")
            {
                System.Web.UI.ClientScriptManager manager = Page.ClientScript;
                manager.RegisterStartupScript(this.GetType(), "close and return", "closeAndReturn();", true);
            }
        }
        #endregion
    }
}