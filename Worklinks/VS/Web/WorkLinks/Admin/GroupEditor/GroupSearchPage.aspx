﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GroupSearchPage.aspx.cs" Inherits="WorkLinks.Admin.GroupEditor.GroupSearchPage" %>
<%@ Register Src="GroupSearchControl.ascx" TagName="GroupSearchControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:GroupSearchControl ID="GroupSearchControl1" SearchLocation="Admin" runat="server" />        
    </ContentTemplate>
</asp:Content>