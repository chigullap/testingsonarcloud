﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupSearchControl.ascx.cs" Inherits="WorkLinks.Admin.GroupEditor.GroupSearchControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="GroupSummaryGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnClear">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="GroupSummaryGrid" />
                <telerik:AjaxUpdatedControl ControlID="GroupDescriptionTextBox" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<table width="100%">
    <tr valign="top">
        <td>
            <asp:Panel ID="Panel3" runat="server" DefaultButton="btnSearch">
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="GroupDescriptionTextBox" runat="server" ResourceName="GroupDescription" TabIndex="010" />
                        </td>
                    </tr>
                </table>
                <div class="SearchCriteriaButtons">
                    <wasp:WLPButton ID="btnClear" Icon-PrimaryIconUrl="~/App_Themes/Default/clear.gif" ResourceName="btnClear" runat="server" Text="Clear" OnClientClicked="clear" OnClick="btnClear_Click" CssClass="button" />
                    <wasp:WLPButton ID="btnSearch" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" ResourceName="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                </div>
            </asp:Panel>
        </td>
    </tr>
    <tr valign="top">
        <td>
            <wasp:WLPToolBar ID="GroupSummaryToolBar" runat="server" Width="100%" OnClientLoad="GroupSummaryToolBar_init">
                <Items>
                    <wasp:WLPToolBarButton OnPreRender="AddToolBar_PreRender" Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" onclick="Add()" CommandName="add" ResourceName="Add" />
                    <wasp:WLPToolBarButton OnPreRender="DetailsToolBar_PreRender" Text="Details" onclick="Open()" CommandName="details" ResourceName="Details" />
                </Items>
            </wasp:WLPToolBar>
            <wasp:WLPGrid
                ID="GroupSummaryGrid"
                runat="server"
                AllowPaging="true"
                PagerStyle-AlwaysVisible="true"
                PageSize="100"
                AllowSorting="true"
                GridLines="None"
                Height="400px"
                AutoAssignModifyProperties="true">

                <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                    <ClientEvents OnRowDblClick="OnRowDblClick" OnRowClick="OnRowClick" />
                </ClientSettings>

                <MasterTableView ClientDataKeyNames="GroupId" AutoGenerateColumns="false" DataKeyNames="GroupId" CommandItemDisplay="Top">
                    <CommandItemTemplate></CommandItemTemplate>
                    <Columns>
                        <wasp:GridBoundControl DataField="EnglishDesc" LabelText="English Description**" SortExpression="EnglishDesc" UniqueName="EnglishDesc" ResourceName="EnglishDesc">
                            <HeaderStyle Width="16%" />
                        </wasp:GridBoundControl>
                        <wasp:GridBoundControl DataField="FrenchDesc" LabelText="French Description**" SortExpression="FrenchDesc" UniqueName="FrenchDesc" ResourceName="FrenchDesc">
                            <HeaderStyle Width="16%" />
                        </wasp:GridBoundControl>
                        <wasp:GridKeyValueControl OnNeedDataSource="PopulateCombo" LabelText="Database Role**" DataField="DatabaseSecurityRoleId" SortExpression="DatabaseSecurityRoleId" Type="DatabaseSecurityRoleId" ResourceName="DatabaseSecurityRoleId">
                            <HeaderStyle Width="16%" />
                        </wasp:GridKeyValueControl>
                        <wasp:GridKeyValueControl OnNeedDataSource="PopulateCombo" LabelText="GUI Role**" DataField="GuiSecurityRoleId" SortExpression="GuiSecurityRoleId" Type="GuiSecurityRoleId" ResourceName="GuiSecurityRoleId">
                            <HeaderStyle Width="16%" />
                        </wasp:GridKeyValueControl>
                    </Columns>
                </MasterTableView>

                <HeaderContextMenu EnableAutoScroll="true" />

            </wasp:WLPGrid>
        </td>
    </tr>
</table>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="GroupWindows"
    Width="1000"
    Height="300"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    OnClientClose="onClientClose"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var groupId;
        var rowSelected;
        var addButton;
        var detailsButton;
        var toolbar;

        function clear() {
            groupId = null;
            securityRoleDescId = null;
            securityRoleId = null;
            GroupDescriptionTextBox = "";
        }

        function GroupSummaryToolBar_init(sender) {
            initializeControls();
        }

        function initializeControls() {
            toolbar = $find('<%= GroupSummaryToolBar.ClientID %>');
            addButton = toolbar.findButtonByCommandName('add');
            detailsButton = toolbar.findButtonByCommandName('details');
            groupId = null;

            if (detailsButton != null)
                detailsButton.disable();
        }

        function OnRowDblClick(sender, eventArgs) {
            if (toolbar.findButtonByCommandName('details') != null)
                Open(sender);
        }

        function OnRowClick(sender, eventArgs) {
            groupId = eventArgs.getDataKeyValue('GroupId');
            rowSelected = true;

            if (detailsButton != null)
                detailsButton.enable();
        }

        function Open() {
            if (groupId != null)
                openWindowWithManager('GroupWindows', String.format('/Admin/Group/View/{0}', groupId), false);
        }

        function Add() {
            openWindowWithManager('GroupWindows', String.format('/Admin/Group/Add/{0}', -1), false);
        }

        function RowSelected() {
            return rowSelected;
        }

        function onClientClose(sender, eventArgs) {
            __doPostBack('<%= GroupSummaryGrid.ClientID %>', '<%= String.Format("refresh") %>');
        }
    </script>
</telerik:RadScriptBlock>