﻿using System;
using System.Security.Claims;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.GroupEditor
{
    public partial class GroupSearchControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        protected bool? worklinksAdministrationFlag = false;
        #endregion

        #region properties
        protected String Criteria
        {
            get
            {
                String searchEntry = GroupDescriptionTextBox.TextValue;
                return searchEntry;
            }
            set
            {
                GroupDescriptionTextBox.TextValue = value;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.GroupSearch.ViewFlag);

            WireEvents();

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.Equals(String.Format("refresh")))
            {
                long keyId = -1;

                //if a row was selected
                if (GroupSummaryGrid.SelectedItems.Count != 0)
                    keyId = Convert.ToInt64(((Telerik.Web.UI.GridDataItem)GroupSummaryGrid.SelectedItems[0]).GetDataKeyValue(GroupSummaryGrid.MasterTableView.DataKeyNames[0]));

                Search(Criteria);

                if (keyId != -1)
                    GroupSummaryGrid.SelectRowByFirstDataKey(keyId); //re-select the selected row
            }
        }
        protected void WireEvents()
        {
            GroupSummaryGrid.NeedDataSource += new Telerik.Web.UI.GridNeedDataSourceEventHandler(GroupSummaryGrid_NeedDataSource);
        }
        public void Search(String criteria)
        {
            //String loggedInUser = ((ClaimsIdentity)System.Web.HttpContext.Current.User.Identity).FindFirst("uname").Value;//yuc
            String loggedInUser = System.Web.HttpContext.Current.User.Identity.Name.ToString();
            SecurityUser user = Common.ServiceWrapper.SecurityClient.GetSecurityUser(loggedInUser)[0];

            worklinksAdministrationFlag = !user.WorklinksAdministrationFlag;
            Data = GroupDescriptionCollection.ConvertCollection(Common.ServiceWrapper.SecurityClient.GetGroupDescriptions(criteria, null, worklinksAdministrationFlag));

            GroupSummaryGrid.Rebind();
        }
        #endregion

        #region event handlers
        void GroupSummaryGrid_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            GroupSummaryGrid.DataSource = Data;
        }
        protected void PopulateCombo(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateComboBoxWithRoleDescriptionForGroupScreen((ICodeControl)sender, LanguageCode, null);
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search(Criteria);
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            GroupDescriptionTextBox.TextValue = "";
            Data = null;
            GroupSummaryGrid.DataBind();
        }
        #endregion

        #region security handlers
        protected void AddToolBar_PreRender(object sender, EventArgs e)
        {
            ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.GroupSearch.AddFlag;
        }
        protected void DetailsToolBar_PreRender(object sender, EventArgs e)
        {
            ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.GroupEdit.ViewFlag;
        }
        #endregion
    }
}