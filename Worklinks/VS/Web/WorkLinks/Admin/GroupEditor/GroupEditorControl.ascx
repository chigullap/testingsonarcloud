﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupEditorControl.ascx.cs" Inherits="WorkLinks.Admin.GroupEditor.GroupEditorControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<wasp:WLPFormView ID="GroupInfoView" runat="server" RenderOuterTable="false" DataKeyNames="Key">
    <ItemTemplate>
        <wasp:WLPToolBar ID="GroupDescSummaryToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
            <Items>
                <wasp:WLPToolBarButton Text="Edit" Visible='<%# UpdateFlag %>' ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="Edit" ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="EnglishDesc" runat="server" ResourceName="EnglishDesc" Value='<%# Eval("EnglishDesc") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="FrenchDesc" runat="server" ResourceName="FrenchDesc" Value='<%# Eval("FrenchDesc") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="DatabaseSecurityRoleId" OnDataBinding="PopulateCombo" LabelText="DB Role Type**" runat="server" Type="DatabaseSecurityRoleId" ResourceName="DatabaseSecurityRoleId" Value='<%# Eval("DatabaseSecurityRoleId") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="GuiSecurityRoleId" OnDataBinding="PopulateCombo" LabelText="GUI Role Type**" runat="server" Type="GuiSecurityRoleId" ResourceName="GuiSecurityRoleId" Value='<%# Eval("GuiSecurityRoleId") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="WorklinksAdministrationFlag" LabelText="WorkLinks Admin**" runat="server" ResourceName="WorklinksAdministrationFlag" Value='<%# Eval("WorklinksAdministrationFlag") %>' ReadOnly="true" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>
    <EditItemTemplate>
        <wasp:WLPToolBar ID="GroupDetailToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" CommandArgument="close" Visible='<%# IsEditMode %>' ResourceName="UpdateAndClose"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" CommandArgument="close" ResourceName="InsertAndClose"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton ImageUrl="~/App_Themes/Default/Cancel.gif" onclick="closeAndReturn()" CommandName="cancel" CausesValidation="False" ResourceName="Cancel"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="EnglishDesc" runat="server" ResourceName="EnglishDesc" Value='<%# Bind("EnglishDesc") %>' TabIndex="010" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="FrenchDesc" runat="server" ResourceName="FrenchDesc" Value='<%# Bind("FrenchDesc") %>' TabIndex="020" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="DatabaseSecurityRoleId" runat="server" LabelText="DB Role Type**" IncludeEmptyItem="false" Type="DatabaseSecurityRoleId" OnDataBinding="PopulateCombo" ResourceName="DatabaseSecurityRoleId" Value='<%# Bind("DatabaseSecurityRoleId") %>' TabIndex="030" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="GuiSecurityRoleId" runat="server" LabelText="GUI Role Type**" IncludeEmptyItem="false" Type="GuiSecurityRoleId" OnDataBinding="PopulateCombo" ResourceName="GuiSecurityRoleId" Value='<%# Bind("GuiSecurityRoleId") %>' TabIndex="040" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="WorklinksAdministrationFlag" runat="server" ResourceName="WorklinksAdministrationFlag" Value='<%# Bind("WorklinksAdministrationFlag") %>' TabIndex="050" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>
</wasp:WLPFormView>

<script type="text/javascript">
    function closeAndReturn() {
        var closeWithChanges = true;
        var popup = getRadWindow();
        setTimeout(function () { popup.close(closeWithChanges); }, 0); // Have to use "setTimeout" to get around an IE9 bug
    }

    function getRadWindow() {
        var popup = null;
        if (window.radWindow)
            popup = window.radWindow;
        else if (window.frameElement.radWindow)
            popup = window.frameElement.radWindow;
        return popup;
    }
</script>