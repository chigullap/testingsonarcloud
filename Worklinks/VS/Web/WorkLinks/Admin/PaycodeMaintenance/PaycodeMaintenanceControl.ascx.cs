﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.PaycodeMaintenance
{
    public partial class PaycodeMaintenanceControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public bool IsViewMode { get { return PaycodeMaintenanceGrid.IsViewMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.PaycodeMaintenance.AddFlag; } }
        public bool ViewFlag { get { return Common.Security.RoleForm.PaycodeMaintenance.ViewFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.PaycodeMaintenance.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.PaycodeMaintenance.DeleteFlag; } }
        protected bool EnableYearEndReportMapMenuItem { get { return Common.Security.RoleForm.PaycodeYearEndReportMap.ViewFlag; } }
        protected bool EnablePayrollTransactionOffsetAssociation { get { return Common.Security.RoleForm.PaycodePayrollTransactionOffsetAssociation.ViewFlag; } }
        protected bool EnableReportsMenu { get { return Common.Security.RoleForm.PaycodeMaintenanceReportMenu; } }
        protected bool EnableWageTypeCatalogMenuItem { get { return Common.Security.RoleForm.WageTypeCatalogReport.ViewFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.PaycodeMaintenance.ViewFlag);

            WireEvents();

            if (!IsPostBack)
                LoadPaycodeMaintenanceInformation();

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null)
            {
                if (args.StartsWith("refresh"))
                {
                    LoadPaycodeMaintenanceInformation();
                    PaycodeMaintenanceGrid.Rebind();
                }
                else if (args.StartsWith("export"))
                {
                    String paycodeCode = PaycodeMaintenanceGrid.SelectedValue.ToString();
                    String paycodeType = ((CodePaycode)Data[paycodeCode]).PaycodeTypeCode;
                    String fileName = Common.ApplicationParameter.DynamicsDatabaseName + "_" + paycodeCode + ".xml";
                    byte[] file = Common.ServiceWrapper.CodeClient.ExportPaycodeFile(paycodeCode, paycodeType) ?? new byte[0];

                    Response.Clear();

                    //this will prompt the user to open or download the file
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName);
                    Response.AddHeader("Content-Length", file.Length.ToString());

                    Response.OutputStream.Write(file, 0, file.Length);
                    Response.Flush();
                    Response.End();
                }
            }
        }
        protected void WireEvents()
        {
            PaycodeMaintenanceGrid.NeedDataSource += new GridNeedDataSourceEventHandler(PaycodeMaintenanceGrid_NeedDataSource);
        }
        protected void LoadPaycodeMaintenanceInformation()
        {
            Data = CodePaycodeCollection.ConvertCollection(Common.ServiceWrapper.CodeClient.GetCodePaycode(null, false));
        }
        #endregion

        #region event handlers
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadPaycodeMaintenanceInformation();
            PaycodeMaintenanceGrid.Rebind();
        }
        void PaycodeMaintenanceGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            PaycodeMaintenanceGrid.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void PaycodeMaintenanceToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("delete") && PaycodeMaintenanceGrid.SelectedValue != null)
                DeletePaycode((CodePaycode)Data[PaycodeMaintenanceGrid.SelectedValue.ToString()]);
        }
        protected void DeletePaycode(CodePaycode paycode)
        {
            try
            {
                Common.ServiceWrapper.CodeClient.DeleteCodePaycode(paycode);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);

                PaycodeMaintenanceGrid.SelectedIndexes.Clear();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            LoadPaycodeMaintenanceInformation();
            PaycodeMaintenanceGrid.Rebind();
        }
        protected void PaycodeMaintenanceGrid_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            int i = 0;
        }
        protected void PaycodeMaintenanceGrid_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            int i = 0;
        }
        protected void PaycodeMaintenanceGrid_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            int i = 0;
        }
        #endregion

        #region security handlers
        protected void AddDropDown_PreRender(object sender, EventArgs e) { ((WLPToolBarDropDown)sender).Visible = ((WLPToolBarDropDown)sender).Visible && AddFlag; }
        protected void DetailsButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && ViewFlag; }
        protected void DeleteButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && DeleteFlag; }
        protected void YearEndReportMapButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableYearEndReportMapMenuItem; }
        protected void PayrollTransactionOffsetAssociationButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnablePayrollTransactionOffsetAssociation; }
        protected void ExportButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && ViewFlag; }
        protected void ImportButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && AddFlag && UpdateFlag; }
        protected void ReportMenuButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableReportsMenu; }
        protected void ReportsToolBar_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableReportsMenu; }
        protected void WageTypeCatalogMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableWageTypeCatalogMenuItem; }
        #endregion
    }
}