﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaycodeMaintenanceControl.ascx.cs" Inherits="WorkLinks.Admin.PaycodeMaintenance.PaycodeMaintenanceControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <table width="100%">
        <tr valign="top">
            <td>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnRefresh">
                    <div class="SearchCriteriaButtons">
                        <wasp:WLPButton ID="btnRefresh" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" runat="server" Text="Refresh" ResourceName="btnRefresh" OnClientClicked="clear" OnClick="btnRefresh_Click" />
                    </div>
                </asp:Panel>
            </td>
        </tr>
        <tr valign="top">
            <td>
                <wasp:WLPToolBar ID="PaycodeMaintenanceToolBar" runat="server" Width="100%" OnButtonClick="PaycodeMaintenanceToolBar_ButtonClick" OnClientLoad="PaycodeMaintenanceToolBar_init">
                    <Items>
                        <wasp:WLPToolBarDropDown Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" ResourceName="Add" OnPreRender="AddDropDown_PreRender">
                            <Buttons>
                                <wasp:WLPToolBarButton runat="server" Text="*Income" onclick="AddIncome()" ResourceName="AddIncome" CommandName="addIncome" />
                                <wasp:WLPToolBarButton runat="server" Text="*Deduction" onclick="AddDeduction()" ResourceName="AddDeduction" CommandName="addDeduction" />
                                <wasp:WLPToolBarButton runat="server" Text="*Benefit" onclick="AddBenefit()" ResourceName="AddBenefit" CommandName="addBenefit" />
                                <wasp:WLPToolBarButton runat="server" Text="*Employer" onclick="AddEmployer()" ResourceName="AddEmployer" CommandName="addEmployer" />
                            </Buttons>
                        </wasp:WLPToolBarDropDown>
                        <wasp:WLPToolBarButton runat="server" Text="*Details" onclick="OpenDetails();" ResourceName="Details" CommandName="details" OnPreRender="DetailsButton_PreRender" />
                        <wasp:WLPToolBarButton Text="*Delete" ImageUrl="~\App_Themes\Default\Delete.gif" ResourceName="Delete" CommandName="delete" OnPreRender="DeleteButton_PreRender" />
                        <wasp:WLPToolBarButton runat="server" Text="*YearEndReportMap" onclick="OpenYearEndReportMap();" ResourceName="YearEndReportMap" CommandName="yearEndReportMap" OnPreRender="YearEndReportMapButton_PreRender" />
                        <wasp:WLPToolBarButton runat="server" Text="*PayrollTransactionOffsetAssociation" onclick="OpenPayrollTransactionOffsetAssociation();" ResourceName="PayrollTransactionOffsetAssociation" CommandName="payrollTransactionOffsetAssociation" OnPreRender="PayrollTransactionOffsetAssociationButton_PreRender" />
                        <wasp:WLPToolBarButton runat="server" Text="*Export" onclick="OpenExport();" ResourceName="Export" CommandName="export" OnPreRender="ExportButton_PreRender" />
                        <wasp:WLPToolBarButton runat="server" Text="*Import" onclick="OpenImport();" ResourceName="Import" CommandName="import" OnPreRender="ImportButton_PreRender" />
                        <wasp:WLPToolBarButton CommandName="ReportMenuButton" OnPreRender="ReportMenuButton_PreRender">
                            <ItemTemplate>
                                <telerik:RadMenu ID="ReportRadMenu" ClickToOpen="true" runat="server" EnableRoundedCorners="true" Style="z-index: 1900" OnClientItemClicked="reportItemClicked" OnClientLoad="reportOnInit">
                                    <Items>
                                        <wasp:WLPMenuItem runat="server" Text="*Reports" OnPreRender="ReportsToolBar_PreRender" ResourceName="ReportsMenu">
                                            <Items>
                                                <wasp:WLPMenuItem runat="server" Text="*WageTypeCatalog" ResourceName="WageTypeCatalog" OnPreRender="WageTypeCatalogMenuItem_PreRender">
                                                    <Items>
                                                        <wasp:WLPMenuItem Value="WageTypeCatalog_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableWageTypeCatalogMenuItem %>' />
                                                    </Items>
                                                </wasp:WLPMenuItem>
                                            </Items>
                                        </wasp:WLPMenuItem>
                                    </Items>
                                </telerik:RadMenu>
                            </ItemTemplate>
                        </wasp:WLPToolBarButton>
                    </Items>
                </wasp:WLPToolBar>

                <wasp:WLPGrid
                    ID="PaycodeMaintenanceGrid"
                    runat="server"
                    AllowPaging="true"
                    PagerStyle-AlwaysVisible="true"
                    PageSize="100"
                    AllowSorting="true"
                    GridLines="None"
                    AutoAssignModifyProperties="true"
                    OnPageIndexChanged="PaycodeMaintenanceGrid_PageIndexChanged"
                    OnPageSizeChanged="PaycodeMaintenanceGrid_PageSizeChanged"
                    OnSortCommand="PaycodeMaintenanceGrid_SortCommand"
                    AllowFilteringByColumn="true" 
                    FilterType="CheckList">

                    <GroupingSettings CaseSensitive="false" />

                    <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="false">
                        <Scrolling AllowScroll="false" UseStaticHeaders="true" />
                        <Selecting AllowRowSelect="true" />
                        <ClientEvents OnRowDblClick="OnRowDblClick" OnRowClick="OnRowClick" OnGridCreated="onGridCreated" OnCommand="onCommand" />
                    </ClientSettings>

                    <MasterTableView
                        ClientDataKeyNames="PaycodeCode, PaycodeTypeCode, ReportGroupCodePaycodeTypeCd, EnglishDescription, FrenchDescription, SystemFlag"
                        AutoGenerateColumns="false"
                        DataKeyNames="PaycodeCode"
                        CommandItemDisplay="Top"
                        AllowSorting="true"
                        AllowFilteringByColumn="true">

                        <SortExpressions>
                            <telerik:GridSortExpression FieldName="PaycodeCode" SortOrder="Ascending" />
                        </SortExpressions>

                        <CommandItemTemplate></CommandItemTemplate>

                        <Columns>
                            <wasp:GridBoundControl DataField="PaycodeCode" SortExpression="PaycodeCode" UniqueName="PaycodeCode" ResourceName="PaycodeCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                <HeaderStyle Width="15%" />
                            </wasp:GridBoundControl>
                            <wasp:GridBoundControl DataField="EnglishDescription" SortExpression="EnglishDescription" UniqueName="EnglishDescription" ResourceName="EnglishDescription" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                <HeaderStyle Width="30%" />
                            </wasp:GridBoundControl>
                            <wasp:GridBoundControl DataField="FrenchDescription" SortExpression="FrenchDescription" UniqueName="FrenchDescription" ResourceName="FrenchDescription" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                <HeaderStyle Width="30%" />
                            </wasp:GridBoundControl>
                            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="PaycodeTypeCode" SortExpression="PaycodeTypeCode" Type="PaycodeTypeCode" ResourceName="PaycodeTypeCode">
                                <HeaderStyle Width="10%" />
                            </wasp:GridKeyValueControl>
                            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="ReportGroupCodePaycodeTypeCd" SortExpression="ReportGroupCodePaycodeTypeCd" Type="PaycodeTypeCode" ResourceName="ReportGroupPaycodeTypeCode">
                                <HeaderStyle Width="15%" />
                            </wasp:GridKeyValueControl>
                        </Columns>

                    </MasterTableView>

                    <HeaderContextMenu EnableAutoScroll="true" />

                </wasp:WLPGrid>
            </td>
        </tr>
    </table>
</asp:Panel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="PaycodeMaintenanceWindows"
    Width="1000"
    Height="700"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientClose="onClientClose"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var paycodeCode;
        var paycodeTypeCode;
        var systemFlag;
        var toolbar = null;
        var reportMenu;

        function clear() {
            paycodeCode = null;
            paycodeTypeCode = null;
            systemFlag = null;
        }

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function PaycodeMaintenanceToolBar_init(sender) {
            initializeControls();
        }

        function initializeControls() {
            if ($find('<%= PaycodeMaintenanceToolBar.ClientID %>') != null)
                toolbar = $find('<%= PaycodeMaintenanceToolBar.ClientID %>');

            enableButtons(getSelectedRow() != null);

            var detailsButton = toolbar.findButtonByCommandName('details');
            if (detailsButton != null) {
                if (getSelectedRow() != null)
                    detailsButton.set_enabled(true);
                else
                    detailsButton.set_enabled(false);
            }

            var yearEndReportMapButton = toolbar.findButtonByCommandName('yearEndReportMap');
            if (yearEndReportMapButton != null) {
                if (getSelectedRow() != null)
                    yearEndReportMapButton.set_enabled(true);
                else
                    yearEndReportMapButton.set_enabled(false);
            }

            var payrollTransactionOffsetAssociationButton = toolbar.findButtonByCommandName('payrollTransactionOffsetAssociation');
            if (payrollTransactionOffsetAssociationButton != null) {
                if (getSelectedRow() != null)
                    payrollTransactionOffsetAssociationButton.set_enabled(true);
                else
                    payrollTransactionOffsetAssociationButton.set_enabled(false);
            }

            var exportButton = toolbar.findButtonByCommandName('export');
            if (exportButton != null) {
                if (getSelectedRow() != null)
                    exportButton.set_enabled(true);
                else
                    exportButton.set_enabled(false);
            }
        }

        function OnRowDblClick(sender, eventArgs) {
            var toolbar = $find('<%= PaycodeMaintenanceToolBar.ClientID %>');
            if (toolbar.findButtonByCommandName('details') != null && ButtonGetEnabled('details'))
                OpenDetails(sender);
        }

        function getSelectedRow() {
            var paycodeMaintenanceGrid = $find('<%= PaycodeMaintenanceGrid.ClientID %>');
            if (paycodeMaintenanceGrid == null)
                return null;

            var MasterTable = paycodeMaintenanceGrid.get_masterTableView();
            if (MasterTable == null)
                return null;

            var selectedRow = MasterTable.get_selectedItems();
            if (selectedRow.length == 1)
                return selectedRow[0];
            else
                return null;
        }

        function onGridCreated(sender, eventArgs) {
            var selectedRow = getSelectedRow();
            if (selectedRow != null) {
                paycodeCode = selectedRow.getDataKeyValue('PaycodeCode');
                paycodeTypeCode = selectedRow.getDataKeyValue('PaycodeTypeCode');
                systemFlag = selectedRow.getDataKeyValue('SystemFlag');
                enableButtons(true);
            }
        }

        function onCommand(sender, args) {
            if (args.get_commandName() == 'Page') {
                paycodeCode = null;
                paycodeTypeCode = null;
                systemFlag = null;

                initializeControls();
            }
        }

        function OnRowClick(sender, eventArgs) {
            paycodeCode = eventArgs.getDataKeyValue('PaycodeCode');
            paycodeTypeCode = eventArgs.getDataKeyValue('PaycodeTypeCode');
            systemFlag = eventArgs.getDataKeyValue('SystemFlag');

            enableButtons(true);

            var detailsButton = toolbar.findButtonByCommandName('details');
            if (detailsButton != null)
                detailsButton.set_enabled(true);
            
            var yearEndReportMapButton = toolbar.findButtonByCommandName('yearEndReportMap');
            if (yearEndReportMapButton != null)
                yearEndReportMapButton.set_enabled(true);

            var payrollTransactionOffsetAssociationButton = toolbar.findButtonByCommandName('payrollTransactionOffsetAssociation');
            if (payrollTransactionOffsetAssociationButton != null)
                payrollTransactionOffsetAssociationButton.set_enabled(true);

            var exportButton = toolbar.findButtonByCommandName('export');
            if (exportButton != null)
                exportButton.set_enabled(true);
        }

        function enableButtons(enable) {
            var deleteButton = toolbar.findButtonByCommandName('delete');

            if (deleteButton != null)
                deleteButton.set_enabled(enable && systemFlag != "True");
        }

        function ButtonGetEnabled(button) {
            return toolbar.findButtonByCommandName('' + button + '').get_enabled();
        }

        function AddIncome() {
            if (ButtonGetEnabled('addIncome')) {
                openWindowWithManager('PaycodeMaintenanceWindows', String.format('/Admin/PaycodeMaintenance/Income/{0}/{1}', 'add', 'x'), false);
                return false;
            }
        }

        function AddDeduction() {
            if (ButtonGetEnabled('addDeduction')) {
                openWindowWithManager('PaycodeMaintenanceWindows', String.format('/Admin/PaycodeMaintenance/Deduction/{0}/{1}', 'add', 'x'), false);
                return false;
            }
        }

        function AddBenefit() {
            if (ButtonGetEnabled('addBenefit')) {
                openWindowWithManager('PaycodeMaintenanceWindows', String.format('/Admin/PaycodeMaintenance/Benefit/{0}/{1}', 'add', 'x'), false);
                return false;
            }
        }

        function AddEmployer() {
            if (ButtonGetEnabled('addEmployer')) {
                openWindowWithManager('PaycodeMaintenanceWindows', String.format('/Admin/PaycodeMaintenance/Employer/{0}/{1}', 'add', 'x'), false);
                return false;
            }
        }

        function OpenDetails() {
            if (paycodeCode != null && ButtonGetEnabled('details')) {
                var encodedPaycode = encodeURIComponent(paycodeCode);
                if (paycodeTypeCode == '<%= ((short)WorkLinks.BusinessLayer.BusinessObjects.CodePaycode.PaycodeType.Income).ToString() %>') {
                    openWindowWithManager('PaycodeMaintenanceWindows', String.format('/Admin/PaycodeMaintenance/Income/{0}?paycodeCode={1}', 'view', encodedPaycode), false);
                    return false;
                }
                else if (paycodeTypeCode == '<%= ((short)WorkLinks.BusinessLayer.BusinessObjects.CodePaycode.PaycodeType.Deduction).ToString() %>') {
                    openWindowWithManager('PaycodeMaintenanceWindows', String.format('/Admin/PaycodeMaintenance/Deduction/{0}?paycodeCode={1}', 'view', encodedPaycode), false);
                    return false;
                }
                else if (paycodeTypeCode == '<%= ((short)WorkLinks.BusinessLayer.BusinessObjects.CodePaycode.PaycodeType.Benefit).ToString() %>') {
                    openWindowWithManager('PaycodeMaintenanceWindows', String.format('/Admin/PaycodeMaintenance/Benefit/{0}?paycodeCode={1}', 'view', encodedPaycode), false);
                    return false;
                }
                else if (paycodeTypeCode == '<%= ((short)WorkLinks.BusinessLayer.BusinessObjects.CodePaycode.PaycodeType.Employer).ToString() %>') {
                    openWindowWithManager('PaycodeMaintenanceWindows', String.format('/Admin/PaycodeMaintenance/Employer/{0}?paycodeCode={1}', 'view', encodedPaycode), false);
                    return false;
                }
            }
        }

        function OpenYearEndReportMap() {
            if (paycodeCode != null && ButtonGetEnabled('yearEndReportMap')) {
                openWindowWithManager('PaycodeMaintenanceWindows', String.format('/Admin/PaycodeMaintenance/YearEndReportMap/{0}', paycodeCode), false);
                return false;
            }
        }

        function OpenPayrollTransactionOffsetAssociation() {
            if (paycodeCode != null && ButtonGetEnabled('payrollTransactionOffsetAssociation')) {
                openWindowWithManager('PaycodeMaintenanceWindows', String.format('/Admin/PaycodeMaintenance/PayrollTransactionOffsetAssociation/{0}', paycodeCode), false);
                return false;
            }
        }

        function OpenExport() {
            if (paycodeCode != null)
                __doPostBack('<%= this.ClientID %>', 'export');
        }

        function OpenImport() {
            if (ButtonGetEnabled('import')) {
                openWindowWithManager('PaycodeMaintenanceWindows', String.format('/Admin/PaycodeMaintenance/Import'), false);
                return false;
            }
        }

        //sets the handler for report menu
        function reportOnInit(sender, eventArgs) {
            reportMenu = sender;
        }

        //opens selected report
        function reportItemClicked(sender, eventArgs) {
            menuValue = eventArgs.get_item().get_value();
            if (menuValue != null) {
                var splitterIndex = menuValue.lastIndexOf("_");
                var reportName = menuValue.substr(0, splitterIndex);
                var outputType = menuValue.substr(splitterIndex + 1, 99);

                //if excel format, do not open a child window.
                if (outputType == 'EXCEL')
                    invokeDownload(reportName, outputType);
                else
                    openWindowWithManager('PaycodeMaintenanceWindows', String.format('/Report/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}', reportName, -1, 'null', outputType, 'x', 'x', 'x', 'x'), true);
            }
        }

        function invokeDownload(reportName, outputType) {
            var frame = document.createElement("frame");
            frame.src = getUrl(String.format('/Report/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}', reportName, -1, 'null', outputType, 'x', 'x', 'x', 'x')), null;
            frame.style.display = "none";
            document.body.appendChild(frame);
        }

        function onClientClose(sender, eventArgs) {
            var arg = sender.argument;
            if (arg != null && arg.closeWithChanges)
                __doPostBack('<%= MainPanel.ClientID %>', 'refresh');
        }
    </script>
</telerik:RadScriptBlock>