﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaycodeEmployerControl.ascx.cs" Inherits="WorkLinks.Admin.PaycodeMaintenance.Employer.PaycodeEmployerControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<wasp:WLPFormView ID="PaycodeEmployerView" runat="server" RenderOuterTable="false" DataKeyNames="Key">
    <ItemTemplate>
        <wasp:WLPToolBar ID="PaycodeEmployerToolBarItem" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
            <Items>
                <wasp:WLPToolBarButton Text="Edit" Visible='<%# UpdateFlag && !IsSystemFlag %>' ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="Edit" ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PaycodeCodeCdItem" runat="server" ReadOnly="true" ResourceName="PaycodeCode" Value='<%# Eval("PaycodeCode") %>' Mandatory="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="EnglishDescriptionItem" runat="server" ReadOnly="true" ResourceName="EnglishDescription" Value='<%# Eval("EnglishDescription") %>' Mandatory="true"></wasp:TextBoxControl>
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="FrenchDescriptionItem" runat="server" ReadOnly="true" ResourceName="FrenchDescription" Value='<%# Eval("FrenchDescription") %>' Mandatory="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="ImportExternalIdentifierItem" runat="server" ReadOnly="true" ResourceName="ImportExternalIdentifier" Value='<%# Eval("PaycodeCodeImportExternalIdentifier") %>' Mandatory="true"></wasp:TextBoxControl>
                    </td>
                    <td>
                        <wasp:NumericControl ID="SortOrderItem" runat="server" ReadOnly="true" DecimalDigits="0" ResourceName="SortOrder" Value='<%# Eval("SortOrder") %>' Mandatory="true"></wasp:NumericControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ActiveFlagItem" runat="server" ReadOnly="true" ResourceName="ActiveFlag" Value='<%# Eval("ActiveFlag") %>'></wasp:CheckBoxControl>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="SystemFlagItem" runat="server" ReadOnly="true" ResourceName="SystemFlag" Value='<%# Eval("SystemFlag") %>'></wasp:CheckBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeInPayslipFlagItem" runat="server" ReadOnly="true" ResourceName="IncludeInPayslipFlag" Value='<%# Eval("IncludeInPayslipFlag") %>'></wasp:CheckBoxControl>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeInPayRegisterFlagItem" runat="server" ReadOnly="true" ResourceName="IncludeInPayRegisterFlag" Value='<%# Eval("IncludeInPayRegisterFlag") %>'></wasp:CheckBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="GeneralLedgerMaskItem" runat="server" ReadOnly="true" ResourceName="GeneralLedgerMask" Value='<%# Eval("GeneralLedgerMask") %>'></wasp:TextBoxControl>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="AllowGeneralLedgerOverrideFlagItem" runat="server" ReadOnly="true" ResourceName="AllowGeneralLedgerOverrideFlag" Value='<%# Eval("AllowGeneralLedgerOverrideFlag") %>'></wasp:CheckBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="OffsetGeneralLedgerMaskItem" runat="server" ReadOnly="true" ResourceName="OffsetGeneralLedgerMask" Value='<%# Eval("OffsetGeneralLedgerMask") %>'></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="ReportGroupPaycodeTypeCodeItem" runat="server" Type="PaycodeTypeCode" OnDataBinding="ReportGroupPaycodeTypeCode_NeedDataSource" ReadOnly="true" ResourceName="ReportGroupPaycodeTypeCode" Value='<%# Eval("ReportGroupCodePaycodeTypeCd") %>' Mandatory="true"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="ReportDisplayUnitFlagItem" runat="server" ReadOnly="true" ResourceName="ReportDisplayUnitFlag" Value='<%# Eval("ReportDisplayUnitFlag") %>'></wasp:CheckBoxControl>
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar ID="PaycodeBenefitToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' ResourceName="Insert" CommandName="insert"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" ResourceName="Update" Visible='<%# IsEditMode %>'></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Cancel" onclick="Close()" ImageUrl="~/App_Themes/Default/Cancel.gif" CausesValidation="false" CommandName="cancel" ResourceName="Cancel"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PaycodeCodeCd" runat="server" MaxLength="8" ReadOnly='<%# IsEditMode || IsViewMode %>' ResourceName="PaycodeCode" Value='<%# Bind("PaycodeCode") %>' Mandatory="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="EnglishDescription" runat="server" ResourceName="EnglishDescription" Value='<%# Bind("EnglishDescription") %>' Mandatory="true"></wasp:TextBoxControl>
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="FrenchDescription" runat="server" ResourceName="FrenchDescription" Value='<%# Bind("FrenchDescription") %>' Mandatory="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="ImportExternalIdentifier" runat="server" ResourceName="ImportExternalIdentifier" Value='<%# Bind("PaycodeCodeImportExternalIdentifier") %>' Mandatory="true"></wasp:TextBoxControl>
                    </td>
                    <td>
                        <wasp:NumericControl ID="SortOrder" runat="server" DecimalDigits="0" ResourceName="SortOrder" Value='<%# Bind("SortOrder") %>' Mandatory="true"></wasp:NumericControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ActiveFlag" runat="server" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>'></wasp:CheckBoxControl>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="SystemFlag" runat="server" ResourceName="SystemFlag" Value='<%# Bind("SystemFlag") %>'></wasp:CheckBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeInPayslipFlag" runat="server" ResourceName="IncludeInPayslipFlag" Value='<%# Bind("IncludeInPayslipFlag") %>'></wasp:CheckBoxControl>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeInPayRegisterFlag" runat="server" ResourceName="IncludeInPayRegisterFlag" Value='<%# Bind("IncludeInPayRegisterFlag") %>'></wasp:CheckBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="GeneralLedgerMask" runat="server" ResourceName="GeneralLedgerMask" Value='<%# Bind("GeneralLedgerMask") %>'></wasp:TextBoxControl>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="AllowGeneralLedgerOverrideFlag" runat="server" ResourceName="AllowGeneralLedgerOverrideFlag" Value='<%# Bind("AllowGeneralLedgerOverrideFlag") %>'></wasp:CheckBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="OffsetGeneralLedgerMask" runat="server" ResourceName="OffsetGeneralLedgerMask" Value='<%# Bind("OffsetGeneralLedgerMask") %>'></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="ReportGroupPaycodeTypeCode" runat="server" Type="PaycodeTypeCode" OnDataBinding="ReportGroupPaycodeTypeCode_NeedDataSource" ResourceName="ReportGroupPaycodeTypeCode" Value='<%# Bind("ReportGroupCodePaycodeTypeCd") %>' Mandatory="true"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="ReportDisplayUnitFlag" runat="server" ResourceName="ReportDisplayUnitFlag" Value='<%# Bind("ReportDisplayUnitFlag") %>'></wasp:CheckBoxControl>
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>

</wasp:WLPFormView>

<script type="text/javascript">
    function Close(button, args) {
        var popup = getRadWindow();
        setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
    }

    function processClick(commandName) {
        var arg = new Object;
        arg.closeWithChanges = true;
        getRadWindow().argument = arg;
    }

    function getRadWindow() {
        var popup = null;

        if (window.radWindow)
            popup = window.radWindow;
        else if (window.frameElement.radWindow)
            popup = window.frameElement.radWindow;

        return popup;
    }

    function ToolBarClick(sender, args) {
        var button = args.get_item();
        var commandName = button.get_commandName();

        if (commandName != "cancel") {
            processClick(commandName);
        }

        if (<%= IsInsertMode.ToString().ToLower() %> || <%= IsImportMode.ToString().ToLower() %>) {
            if (commandName == "cancel") {
                window.close();
            }
        }
    }
</script>