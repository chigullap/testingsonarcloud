﻿using System;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.PaycodeMaintenance.Employer
{
    public partial class PaycodeEmployerControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private String Action { get { return Page.RouteData.Values["action"].ToString().ToLower(); } }
        private String PaycodeCode { get { return Page.Request.QueryString["paycodeCode"]; } }
        public bool IsImportMode { get { return Action == "import"; } }
        public bool IsSystemFlag { get { return ((CodePaycodeEmployerCollection)Data)[0].SystemFlag; } }
        public bool IsEditMode { get { return PaycodeEmployerView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.Edit); } }
        public bool IsInsertMode { get { return PaycodeEmployerView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.Insert); } }
        public bool IsViewMode { get { return PaycodeEmployerView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.ReadOnly); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.PaycodeMaintenance.UpdateFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.PaycodeMaintenance.ViewFlag);

            WireEvents();

            if (Action == "view")
            {
                this.Page.Title = "Edit an Employer Paycode";

                if (!IsPostBack)
                    LoadPaycodeEmployer();
            }
            else if (Action == "add")
            {
                if (!IsPostBack)
                {
                    this.Page.Title = "Add an Employer Paycode";

                    LoadNewPaycodeEmployer();
                }
                else
                    this.Page.Title = "Edit an Employer Paycode";
            }
            else if (IsImportMode)
            {
                this.Page.Title = "Edit an Employer Paycode";

                if (!IsPostBack)
                    LoadImportedPaycodeEmployer();
            }
        }
        protected void WireEvents()
        {
            PaycodeEmployerView.NeedDataSource += new WLPFormView.NeedDataSourceEventHandler(PaycodeEmployerView_NeedDataSource);
            PaycodeEmployerView.Updating += PaycodeEmployerView_Updating;
            PaycodeEmployerView.Inserting += PaycodeEmployerView_Inserting;
            PaycodeEmployerView.ItemCreated += new EventHandler(PaycodeEmployerView_ItemCreated);
        }
        protected void LoadPaycodeEmployer()
        {
            CodePaycodeEmployerCollection collection = Common.ServiceWrapper.CodeClient.GetCodePaycodeEmployer(PaycodeCode);
            Data = collection;

            PaycodeEmployerView.DataBind();
        }
        protected void LoadNewPaycodeEmployer()
        {
            CodePaycodeEmployerCollection collection = new CodePaycodeEmployerCollection();
            collection.Add(new CodePaycodeEmployer() { PaycodeCode = "" });
            Data = collection;

            PaycodeEmployerView.ChangeMode(System.Web.UI.WebControls.FormViewMode.Insert);
            PaycodeEmployerView.DataBind();
        }
        protected void LoadImportedPaycodeEmployer()
        {
            CodePaycodeEmployerCollection collection = new CodePaycodeEmployerCollection();
            collection.Add((CodePaycodeEmployer)Session["HandOff"]);
            Session.Remove("HandOff");
            Data = collection;

            PaycodeEmployerView.ChangeMode(System.Web.UI.WebControls.FormViewMode.Edit);
            PaycodeEmployerView.DataBind();
        }
        #endregion

        #region event handlers
        protected void PaycodeEmployerView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            PaycodeEmployerView.DataSource = Data;
        }
        protected void PaycodeEmployerView_ItemCreated(object sender, EventArgs e)
        {
            if (IsInsertMode)
            {
                CheckBoxControl activeFlag = (CheckBoxControl)PaycodeEmployerView.FindControl("ActiveFlag");
                activeFlag.Value = true;

                CheckBoxControl includeInPayslipFlag = (CheckBoxControl)PaycodeEmployerView.FindControl("IncludeInPayslipFlag");
                includeInPayslipFlag.Value = false;

                CheckBoxControl includeInPayRegisterFlag = (CheckBoxControl)PaycodeEmployerView.FindControl("IncludeInPayRegisterFlag");
                includeInPayRegisterFlag.Value = false;

                CheckBoxControl allowGeneralLedgerOverrideFlag = (CheckBoxControl)PaycodeEmployerView.FindControl("AllowGeneralLedgerOverrideFlag");
                allowGeneralLedgerOverrideFlag.Value = false;

                CheckBoxControl reportDisplayUnitFlag = (CheckBoxControl)PaycodeEmployerView.FindControl("ReportDisplayUnitFlag");
                reportDisplayUnitFlag.Value = false;

                ComboBoxControl reportGroupPaycodeTypeCode = (ComboBoxControl)PaycodeEmployerView.FindControl("ReportGroupPaycodeTypeCode");
                reportGroupPaycodeTypeCode.Value = "6";
            }
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void ReportGroupPaycodeTypeCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControlWithoutFiltering((ICodeControl)sender, LanguageCode);
        }
        #endregion

        #region handle updates
        void PaycodeEmployerView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            try
            {
                CodePaycodeEmployer item = (CodePaycodeEmployer)e.DataItem;
                item.PaycodeTypeCode = ((short)CodePaycode.PaycodeType.Employer).ToString();
                Common.ServiceWrapper.CodeClient.InsertCodePaycodeEmployer(item).CopyTo((CodePaycodeEmployer)Data[item.Key]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void PaycodeEmployerView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                CodePaycodeEmployer item = (CodePaycodeEmployer)e.DataItem;

                if (IsImportMode)
                {
                    CodePaycodeEmployerCollection collection = Common.ServiceWrapper.CodeClient.GetCodePaycodeEmployer(item.PaycodeCode);

                    if (collection != null && collection.Count > 0)
                        Common.ServiceWrapper.CodeClient.UpdateCodePaycodeEmployer(item, true).CopyTo((CodePaycodeEmployer)Data[item.Key]);
                    else
                        Common.ServiceWrapper.CodeClient.InsertCodePaycodeEmployer(item).CopyTo((CodePaycodeEmployer)Data[item.Key]);
                }
                else
                    Common.ServiceWrapper.CodeClient.UpdateCodePaycodeEmployer(item, false).CopyTo((CodePaycodeEmployer)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);

                e.Cancel = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}