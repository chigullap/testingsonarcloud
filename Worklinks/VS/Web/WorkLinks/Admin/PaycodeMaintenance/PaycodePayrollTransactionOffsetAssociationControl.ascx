﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaycodePayrollTransactionOffsetAssociationControl.ascx.cs" Inherits="WorkLinks.Admin.PaycodeMaintenance.PaycodePayrollTransactionOffsetAssociationPage" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPGrid
        ID="PaycodePayrollTransactionOffsetAssociationGrid"
        runat="server"
        GridLines="none"
        AutoGenerateColumns="false"
        AutoAssignModifyProperties="true"
        OnInsertCommand="PaycodePayrollTransactionOffsetAssociationGrid_InsertCommand"
        OnUpdateCommand="PaycodePayrollTransactionOffsetAssociationGrid_UpdateCommand"
        OnDeleteCommand="PaycodePayrollTransactionOffsetAssociationGrid_DeleteCommand">

        <ClientSettings EnablePostBackOnRowClick="false" AllowColumnsReorder="true" ReorderColumnsOnClient="true">
            <Selecting AllowRowSelect="false" />
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
            <CommandItemTemplate>
                <wasp:WLPToolBar ID="PaycodePayrollTransactionOffsetAssociationToolBar" runat="server" Width="100%" AutoPostBack="true" OnPreRender="PaycodePayrollTransactionOffsetAssociationToolBar_PreRender" Style="margin-bottom: 0">
                    <Items>
                        <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add"></wasp:WLPToolBarButton>
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif"></wasp:GridEditCommandControl>
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="OffsetCodePaycodeCode" Type="PaycodeCode" ResourceName="OffsetCodePaycodeCode"></wasp:GridKeyValueControl>
                <wasp:GridNumericControl DataField="OffsetMultiplier" SortExpression="OffsetMultiplier" UniqueName="OffsetMultiplier" DataType="System.Decimal" ResourceName="OffsetMultiplier"></wasp:GridNumericControl>
                <wasp:GridCheckBoxControl DataField="UseBaseSalaryRateFlag" SortExpression="UseBaseSalaryRateFlag" UniqueName="UseBaseSalaryRateFlag" ResourceName="UseBaseSalaryRateFlag"></wasp:GridCheckBoxControl>
                <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>"></wasp:GridButtonControl>
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:ComboBoxControl ID="OffsetCodePaycodeCode" runat="server" Type="PaycodeCode" OnDataBinding="Code_NeedDataSource" ResourceName="OffsetCodePaycodeCode" Value='<%# Bind("OffsetCodePaycodeCode") %>' Mandatory="true" TabIndex="010" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <wasp:NumericControl ID="OffsetMultiplier" runat="server" MaxLength="9" MinValue="0000.0000" MaxValue="9999.9999" DecimalDigits="4" ResourceName="OffsetMultiplier" Value='<%# Bind("OffsetMultiplier") %>' Mandatory="true" TabIndex="020" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <wasp:CheckBoxControl ID="UseBaseSalaryRateFlag" runat="server" ResourceName="UseBaseSalaryRateFlag" Value='<%# Bind("UseBaseSalaryRateFlag") %>' TabIndex="030" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                            <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsInsert %>' ResourceName="Insert" />
                                        </td>
                                        <td>
                                            <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="Cancel" CausesValidation="False" ResourceName="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true"></HeaderContextMenu>

    </wasp:WLPGrid>
</asp:Panel>