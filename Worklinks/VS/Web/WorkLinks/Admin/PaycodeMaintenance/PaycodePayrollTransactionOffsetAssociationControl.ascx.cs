﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.PaycodeMaintenance
{
    public partial class PaycodePayrollTransactionOffsetAssociationPage : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected String PaycodeCode
        {
            get
            {
                if (Page.RouteData.Values["paycodeCode"] == null)
                    return null;
                else
                    return Page.RouteData.Values["paycodeCode"].ToString();
            }
        }
        public bool IsViewMode { get { return PaycodePayrollTransactionOffsetAssociationGrid.IsViewMode; } }
        public bool IsUpdate { get { return PaycodePayrollTransactionOffsetAssociationGrid.IsEditMode; } }
        public bool IsInsert { get { return PaycodePayrollTransactionOffsetAssociationGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.PaycodePayrollTransactionOffsetAssociation.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.PaycodePayrollTransactionOffsetAssociation.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.PaycodePayrollTransactionOffsetAssociation.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.PaycodePayrollTransactionOffsetAssociation.ViewFlag);

            this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "PaycodePayrollTransactionOffsetAssociation"));

            WireEvents();

            if (!IsPostBack)
            {
                LoadPaycodePayrollTransactionOffsetAssociation();
                Initialize();
            }
        }
        protected void WireEvents()
        {
            PaycodePayrollTransactionOffsetAssociationGrid.NeedDataSource += new GridNeedDataSourceEventHandler(PaycodePayrollTransactionOffsetAssociationGrid_NeedDataSource);
        }
        protected void Initialize()
        {
            //find the PaycodePayrollTransactionOffsetAssociationGrid
            WLPGrid grid = (WLPGrid)this.FindControl("PaycodePayrollTransactionOffsetAssociationGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        protected void LoadPaycodePayrollTransactionOffsetAssociation()
        {
            Data = PaycodePayrollTransactionOffsetAssociationCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetPaycodePayrollTransactionOffsetAssociation(PaycodeCode));
        }
        #endregion

        #region event handlers
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        void PaycodePayrollTransactionOffsetAssociationGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            PaycodePayrollTransactionOffsetAssociationGrid.DataSource = Data;
        }
        protected void PaycodePayrollTransactionOffsetAssociationToolBar_PreRender(object sender, EventArgs e)
        {
            GridCommandItem cmdItem = (GridCommandItem)PaycodePayrollTransactionOffsetAssociationGrid.MasterTableView.GetItems(GridItemType.CommandItem)[0];
            WLPToolBar toolbar = (WLPToolBar)cmdItem.FindControl("PaycodePayrollTransactionOffsetAssociationToolBar");
        }
        #endregion

        #region handle updates
        protected void PaycodePayrollTransactionOffsetAssociationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                PaycodePayrollTransactionOffsetAssociation item = (PaycodePayrollTransactionOffsetAssociation)e.Item.DataItem;
                item.PaycodeCode = PaycodeCode;
                Common.ServiceWrapper.HumanResourcesClient.InsertPaycodePayrollTransactionOffsetAssociation(item).CopyTo((PaycodePayrollTransactionOffsetAssociation)Data[item.Key]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void PaycodePayrollTransactionOffsetAssociationGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                PaycodePayrollTransactionOffsetAssociation item = (PaycodePayrollTransactionOffsetAssociation)e.Item.DataItem;
                item.PaycodeCode = PaycodeCode;
                Common.ServiceWrapper.HumanResourcesClient.UpdatePaycodePayrollTransactionOffsetAssociation(item).CopyTo((PaycodePayrollTransactionOffsetAssociation)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);

                e.Canceled = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void PaycodePayrollTransactionOffsetAssociationGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeletePaycodePayrollTransactionOffsetAssociation((PaycodePayrollTransactionOffsetAssociation)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);

                LoadPaycodePayrollTransactionOffsetAssociation();
                e.Canceled = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}