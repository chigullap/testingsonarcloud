﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaycodeImportControl.ascx.cs" Inherits="WorkLinks.Admin.PaycodeMaintenance.PaycodeImportControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<table>
    <tr>
        <td>
            <wasp:WLPUpload ID="UploadControl" MaxFileSize="0" ResourceName="UploadControl" runat="server" ControlObjectsVisibility="None" Width="300px"></wasp:WLPUpload>
            <asp:CustomValidator ID="CustomValidatorForUpload" runat="server" ForeColor="Red" ClientValidationFunction="validateRadUpload" ErrorMessage='<%$ Resources:ErrorMessages, SelectAFile %>'></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            <wasp:WLPButton ID="Submit" runat="server" Text="Submit" ResourceName="Submit" OnClick="Submit_Click" OnClientClicked="disableControl"></wasp:WLPButton>
        </td>
    </tr>
</table>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function validateRadUpload(source, e) {
            e.IsValid = false;

            var upload = $find("<%= UploadControl.ClientID %>");
            var inputs = upload.getFileInputs();

            for (var i = 0; i < inputs.length; i++) {
                // Check for empty string or invalid extension.
                if (inputs[i].value != "") {
                    e.IsValid = true;
                    break;
                }
            }
        }

        function disableControl(source, e) {
            if (Page_IsValid)
                source.set_enabled(false);
        }
    </script>
</telerik:RadScriptBlock>