﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.PaycodeMaintenance
{
    public partial class PaycodeBenefitControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private String Action { get { return Page.RouteData.Values["action"].ToString().ToLower(); } }
        private String PaycodeCode { get { return Page.Request.QueryString["paycodeCode"]; } }
        public bool IsImportMode { get { return Action == "import"; } }
        public bool IsSystemFlag { get { return ((CodePaycodeBenefitCollection)Data)[0].SystemFlag; } }
        public bool IsEditMode { get { return PaycodeBenefitView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool IsInsertMode { get { return PaycodeBenefitView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsViewMode { get { return PaycodeBenefitView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.PaycodeMaintenance.UpdateFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.PaycodeMaintenance.ViewFlag);

            WireEvents();

            if (Action == "view")
            {
                this.Page.Title = "Edit a Benefit Paycode";

                if (!IsPostBack)
                {
                    CodePaycodeBenefitCollection collection = new CodePaycodeBenefitCollection();
                    collection = Common.ServiceWrapper.CodeClient.GetCodePaycodeBenefit(PaycodeCode);
                    Data = collection;

                    PaycodeBenefitView.DataBind();
                }
            }
            else if (Action == "add")
            {
                if (!IsPostBack)
                {
                    this.Page.Title = "Add a Benefit Paycode";

                    CodePaycodeBenefitCollection collection = new CodePaycodeBenefitCollection();
                    collection.Add(new CodePaycodeBenefit() { PaycodeCode = "" });
                    Data = collection;

                    PaycodeBenefitView.ChangeMode(FormViewMode.Insert);
                    PaycodeBenefitView.DataBind();
                }
                else
                    this.Page.Title = "Edit a Benefit Paycode";
            }
            else if (IsImportMode)
            {
                this.Page.Title = "Edit a Benefit Paycode";

                if (!IsPostBack)
                {
                    CodePaycodeBenefitCollection collection = new CodePaycodeBenefitCollection();
                    collection.Add((CodePaycodeBenefit)Session["HandOff"]);
                    Session.Remove("HandOff");
                    Data = collection;

                    PaycodeBenefitView.ChangeMode(FormViewMode.Edit);
                    PaycodeBenefitView.DataBind();
                }
            }

            CallClientSideMethods();
        }
        protected void WireEvents()
        {
            PaycodeBenefitView.NeedDataSource += new WLPFormView.NeedDataSourceEventHandler(PaycodeBenefitView_NeedDataSource);
            PaycodeBenefitView.Updating += PaycodeBenefitView_Updating;
            PaycodeBenefitView.Inserting += PaycodeBenefitView_Inserting;
            PaycodeBenefitView.ItemCreated += new EventHandler(PaycodeBenefitView_ItemCreated);
        }
        protected void CallClientSideMethods()
        {
            //get the SelectedValue property from the PaycodeRateBasedOnCode control and send it as a parameter to the PaycodeRateBasedOnCodeRules client-side method
            ComboBoxControl paycodeRateBasedOnCode = (ComboBoxControl)PaycodeBenefitView.FindControl("PaycodeRateBasedOnCode");
            if (paycodeRateBasedOnCode == null)
                paycodeRateBasedOnCode = (ComboBoxControl)PaycodeBenefitView.FindControl("PaycodeRateBasedOnCodeItem");

            System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PaycodeRateBasedOnCodeRules", String.Format("PaycodeRateBasedOnCodeRules('{0}');", paycodeRateBasedOnCode.SelectedValue), true);
            SetAmountRateFactorMandatory();
        }
        protected void SetAmountRateFactorMandatory()
        {
            CheckBoxControl autoPopulateRateFlag = (CheckBoxControl)PaycodeBenefitView.FindControl("AutoPopulateRateFlag");
            if (autoPopulateRateFlag == null)
                autoPopulateRateFlag = (CheckBoxControl)PaycodeBenefitView.FindControl("AutoPopulateRateFlagItem");

            NumericControl amountRateFactor = (NumericControl)PaycodeBenefitView.FindControl("AmountRateFactor");
            if (amountRateFactor == null)
                amountRateFactor = (NumericControl)PaycodeBenefitView.FindControl("AmountRateFactorItem");

            amountRateFactor.Mandatory = !autoPopulateRateFlag.Checked;

            if (Action == "add" && amountRateFactor.Mandatory)
                CallClientSideDefaultMethods();
        }
        protected void CallClientSideDefaultMethods()
        {
            //call the DefaultAmountRateFactor client-side method
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "DefaultAmountRateFactor", "DefaultAmountRateFactor();", true);
        }
        #endregion

        #region event handlers
        protected void PaycodeBenefitView_ItemCreated(object sender, EventArgs e)
        {
            if (IsInsertMode)
            {
                CheckBoxControl activeFlag = (CheckBoxControl)PaycodeBenefitView.FindControl("ActiveFlag");
                activeFlag.Value = true;
            }
        }
        void PaycodeBenefitView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            PaycodeBenefitView.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void ReportGroupPaycodeTypeCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControlWithoutFiltering((ICodeControl)sender, LanguageCode);
        }
        protected void FederalTaxPaycodeCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode, "code_paycode_type_cd", Convert.ToInt32(CodePaycode.PaycodeType.Deduction).ToString());
        }
        protected void QuebecTaxPaycodeCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode, "code_paycode_type_cd", Convert.ToInt32(CodePaycode.PaycodeType.Deduction).ToString());
        }
        protected void AttachedPaycodes_NeedDataSource(object sender, EventArgs e)
        {
            CodeCollection paycodes = Common.CodeHelper.GetPaycodesByType("1", LanguageCode);
            CodeCollection attachedPaycodes = new CodeCollection();

            foreach (CodeObject code in paycodes)
            {
                foreach (PaycodeAttachedPaycodeProvision paycode in ((CodePaycodeBenefit)Data[0]).AttachedPaycodes)
                {
                    if (paycode.ProvisionPaycodeCode == code.Code)
                    {
                        attachedPaycodes.Add(new CodeObject() { Code = code.Code, Description = code.Description });
                        break;
                    }
                }
            }

            //in view mode, we only want to display the attached paycodes
            ((ICodeControl)sender).DataSource = IsViewMode ? attachedPaycodes : paycodes;
        }
        protected void PaycodeAttachedPaycodeProvision_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            if ((ComboBoxControl)sender != null)
            {
                foreach (PaycodeAttachedPaycodeProvision paycode in ((CodePaycodeBenefit)Data[0]).AttachedPaycodes)
                {
                    if (paycode.ProvisionPaycodeCode == e.Item.Value)
                    {
                        e.Item.Checked = true;
                        break;
                    }
                }
            }
        }
        protected void AutoPopulateRateFlag_CheckedChanged(object sender, EventArgs e)
        {
            SetAmountRateFactorMandatory();
        }
        #endregion

        #region handle updates
        void PaycodeBenefitView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            try
            {
                CodePaycodeBenefit item = (CodePaycodeBenefit)e.DataItem;
                item.PaycodeTypeCode = ((short)CodePaycode.PaycodeType.Benefit).ToString();
                item.AttachedPaycodes = GetAttachedPaycodes(item.PaycodeCode);

                Common.ServiceWrapper.CodeClient.InsertCodePaycodeBenefit(item).CopyTo((CodePaycodeBenefit)Data[item.Key]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void PaycodeBenefitView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                CodePaycodeBenefit item = (CodePaycodeBenefit)e.DataItem;
                item.AttachedPaycodes = GetAttachedPaycodes(item.PaycodeCode);

                if (IsImportMode)
                {
                    CodePaycodeCollection collection = Common.ServiceWrapper.CodeClient.GetCodePaycode(item.PaycodeCode, false);

                    if (collection != null && collection.Count > 0)
                        Common.ServiceWrapper.CodeClient.UpdateCodePaycodeBenefit(item, true, true).CopyTo((CodePaycodeBenefit)Data[item.Key]);
                    else
                        Common.ServiceWrapper.CodeClient.InsertCodePaycodeBenefit(item).CopyTo((CodePaycodeBenefit)Data[item.Key]);
                }
                else
                    Common.ServiceWrapper.CodeClient.UpdateCodePaycodeBenefit(item, false).CopyTo((CodePaycodeBenefit)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);

                e.Cancel = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected PaycodeAttachedPaycodeProvisionCollection GetAttachedPaycodes(String paycodeCode)
        {
            PaycodeAttachedPaycodeProvisionCollection attachedPaycodes = new PaycodeAttachedPaycodeProvisionCollection();
            ComboBoxControl paycodeAttachedPaycodeProvision = (ComboBoxControl)PaycodeBenefitView.FindControl("PaycodeAttachedPaycodeProvision");

            if (paycodeAttachedPaycodeProvision != null)
            {
                RadComboBoxItemCollection items = paycodeAttachedPaycodeProvision.Items;
                int x = 0;

                for (int i = 0; i < items.Count; i++)
                {
                    if (items[i].Checked)
                    {
                        x++;
                        attachedPaycodes.Add(new PaycodeAttachedPaycodeProvision { PaycodeAttachedPaycodeProvisionId = -x, PaycodeCode = paycodeCode, ProvisionPaycodeCode = items[i].Value });
                    }
                }
            }

            return attachedPaycodes;
        }
        #endregion
    }
}