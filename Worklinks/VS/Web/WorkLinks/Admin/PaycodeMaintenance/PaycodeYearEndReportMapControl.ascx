﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaycodeYearEndReportMapControl.ascx.cs" Inherits="WorkLinks.Admin.PaycodeMaintenance.PaycodeYearEndReportMapControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPGrid
        ID="YearEndReportMapGrid"
        runat="server"
        GridLines="None"
        AutoGenerateColumns="false"
        AutoAssignModifyProperties="true"
        OnInsertCommand="YearEndReportMapGrid_InsertCommand"
        OnUpdateCommand="YearEndReportMapGrid_UpdateCommand"
        OnDeleteCommand="YearEndReportMapGrid_DeleteCommand">

        <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="true">
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="false" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="EffectiveYear" SortOrder="Descending" />
            </SortExpressions>

            <CommandItemTemplate>
                <wasp:WLPToolBar ID="YearEndReportMapToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
                    <Items>
                        <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add" />
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif">
                    <HeaderStyle Width="5%" />
                </wasp:GridEditCommandControl>
                <wasp:GridBoundControl DataField="EffectiveYear" LabelText="**EffectiveYear**" SortExpression="EffectiveYear" UniqueName="EffectiveYear" ResourceName="EffectiveYear">
                    <HeaderStyle Width="18%" />
                </wasp:GridBoundControl>
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="YearEndFormCode" LabelText="**YearEndFormCode**" Type="YearEndFormCode" ResourceName="YearEndFormCode">
                    <HeaderStyle Width="18%" />
                </wasp:GridKeyValueControl>
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="YearEndFormBoxCode" LabelText="**YearEndFormBoxCode**" Type="YearEndFormBoxCode" ResourceName="BoxNumber">
                    <HeaderStyle Width="18%" />
                </wasp:GridKeyValueControl>
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="CountryCode" LabelText="**CountryCode**" Type="CountryCode" ResourceName="CountryCode">
                    <HeaderStyle Width="18%" />
                </wasp:GridKeyValueControl>
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="ProvinceStateCode" LabelText="**ProvinceStateCode**" Type="ProvinceStateCode" ResourceName="ProvinceStateCode">
                    <HeaderStyle Width="18%" />
                </wasp:GridKeyValueControl>
                <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>">
                    <HeaderStyle Width="5%" />
                </wasp:GridButtonControl>
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:NumericControl ID="EffectiveYear" Text="**EffectiveYear**" runat="server" MaxLength="4" DecimalDigits="0" GroupSeparator="" ResourceName="EffectiveYear" Value='<%# Bind("EffectiveYear") %>' Mandatory="true" TabIndex="010" />
                                        </td>
                                        <td>
                                            <wasp:ComboBoxControl ID="YearEndFormCode" Text="**YearEndFormCode**" runat="server" Type="YearEndFormCode" OnDataBinding="YearEndFormCode_NeedDataSource" AutoPostback="true" OnSelectedIndexChanged="YearEndFormCode_SelectedIndexChanged" ResourceName="YearEndFormCode" Value='<%# Bind("YearEndFormCode") %>' Mandatory="true" TabIndex="020" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <wasp:ComboBoxControl ID="CountryCode" Text="**CountryCode**" runat="server" Type="CountryCode" OnDataBinding="CountryCode_NeedDataSource" AutoPostback="true" OnSelectedIndexChanged="CountryCode_SelectedIndexChanged" ResourceName="CountryCode" Value='<%# Bind("CountryCode") %>' TabIndex="030" />
                                        </td>
                                        <td>
                                            <wasp:ComboBoxControl ID="YearEndFormBoxCode" Text="**YearEndFormBoxCode**" runat="server" Type="YearEndFormBoxCode" ResourceName="BoxNumber" Mandatory="true" Value='<%# Bind("YearEndFormBoxCode") %>' TabIndex="040" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <wasp:ComboBoxControl ID="ProvinceStateCode" Text="**ProvinceStateCode**" runat="server" Type="ProvinceStateCode" ResourceName="ProvinceStateCode" Value='<%# Bind("ProvinceStateCode") %>' TabIndex="050" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsInsert %>' ResourceName="Insert" />
                                                    </td>
                                                    <td>
                                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="Cancel" CausesValidation="false" ResourceName="Cancel" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true" />

    </wasp:WLPGrid>
</asp:Panel>