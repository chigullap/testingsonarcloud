﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaycodeIncomeControl.ascx.cs" Inherits="WorkLinks.Admin.PaycodeMaintenance.PaycodeIncomeControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<wasp:WLPFormView ID="PaycodeIncomeView" runat="server" RenderOuterTable="false" DataKeyNames="Key">
    <ItemTemplate>
        <wasp:WLPToolBar ID="PaycodeIncomeToolBarItem" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
            <Items>
                <wasp:WLPToolBarButton Text="Edit" Visible='<%# UpdateFlag && !IsSystemFlag %>' ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="Edit" ResourceName="Edit" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="CodePaycodeCdItem" runat="server" ReadOnly="true" ResourceName="PaycodeCode" Value='<%# Eval("PaycodeCode") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="EnglishDescriptionItem" runat="server" ReadOnly="true" ResourceName="EnglishDescription" Value='<%# Eval("EnglishDescription") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="FrenchDescriptionItem" runat="server" ReadOnly="true" ResourceName="FrenchDescription" Value='<%# Eval("FrenchDescription") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="ImportExternalIdentifierItem" runat="server" ReadOnly="true" ResourceName="ImportExternalIdentifier" Value='<%# Eval("PaycodeCodeImportExternalIdentifier") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="SortOrderItem" runat="server" ReadOnly="true" DecimalDigits="0" ResourceName="SortOrder" Value='<%# Eval("SortOrder") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ActiveFlagItem" runat="server" ReadOnly="true" ResourceName="ActiveFlag" Value='<%# Eval("ActiveFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="SystemFlagItem" runat="server" ReadOnly="true" ResourceName="SystemFlag" Value='<%# Eval("SystemFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeInPayslipFlagItem" runat="server" ReadOnly="true" ResourceName="IncludeInPayslipFlag" Value='<%# Eval("IncludeInPayslipFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeInPayRegisterFlagItem" runat="server" ReadOnly="true" ResourceName="IncludeInPayRegisterFlag" Value='<%# Eval("IncludeInPayRegisterFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="GeneralLedgerMaskItem" runat="server" ReadOnly="true" ResourceName="GeneralLedgerMask" Value='<%# Eval("GeneralLedgerMask") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="AllowGeneralLedgerOverrideFlagItem" runat="server" ReadOnly="true" ResourceName="AllowGeneralLedgerOverrideFlag" Value='<%# Eval("AllowGeneralLedgerOverrideFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="AutoPopulateRateFlagItem" runat="server" ReadOnly="true" ResourceName="AutoPopulateRateFlag" Value='<%# Eval("AutoPopulateRateFlag") %>' />
                    </td>
                    <td>
                        <wasp:NumericControl ID="AmountRateFactorItem" runat="server" ReadOnly="true" DecimalDigits="4" ResourceName="AmountRateFactor" Value='<%# Eval("AmountRateFactor") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeEmploymentInsuranceHoursFlagItem" runat="server" ReadOnly="true" ResourceName="IncludeEmploymentInsuranceHoursFlag" Value='<%# Eval("IncludeEmploymentInsuranceHoursFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="ReportGroupPaycodeTypeCodeItem" runat="server" ReadOnly="true" Type="PaycodeTypeCode" OnDataBinding="ReportGroupPaycodeTypeCode_NeedDataSource" ResourceName="ReportGroupPaycodeTypeCode" Value='<%# Eval("ReportGroupCodePaycodeTypeCd") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="ReportDisplayUnitFlagItem" runat="server" ReadOnly="true" ResourceName="ReportDisplayUnitFlag" Value='<%# Eval("ReportDisplayUnitFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="GarnishmentFlagItem" runat="server" ReadOnly="true" ResourceName="GarnishmentFlag" Value='<%# Eval("GarnishmentFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeInArrearsFlagItem" runat="server" ReadOnly="true" ResourceName="IncludeInArrearsFlag" Value='<%# Eval("IncludeInArrearsFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="RecurringIncomeCodeFlagItem" runat="server" ReadOnly="true" ResourceName="RecurringIncomeCodeFlag" Value='<%# Eval("RecurringIncomeCodeFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="VacationCalculationOverrideFlagItem" runat="server" ReadOnly="true" ResourceName="VacationCalculationOverrideFlag" Value='<%# Eval("VacationCalculationOverrideFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="SubtractHourFromSalaryFlagItem" runat="server" ReadOnly="true" ResourceName="SubtractHourFromSalaryFlag" Value='<%# Eval("SubtractHourFromSalaryFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="UseSalaryStandardHourFlagItem" runat="server" ReadOnly="true" ResourceName="UseSalaryStandardHourFlag" Value='<%# Eval("UseSalaryStandardHourFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PaycodeActivationFrequencyCodeItem" runat="server" Type="PaycodeActivationFrequencyCode" OnDataBinding="Code_NeedDataSource" ReadOnly="true" ResourceName="PaycodeActivationFrequencyCode" Value='<%# Eval("PaycodeActivationFrequencyCode") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="PaycodeIncomePayTypeCodeItem" runat="server" ReadOnly="true" Type="PaycodeIncomePayTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="PaycodeIncomePayTypeCode" Value='<%# Eval("PaycodeIncomePayTypeCode") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="TaxOverrideFlagItem" runat="server" ReadOnly="true" ResourceName="TaxOverrideFlag" Value='<%# Eval("TaxOverrideFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="IncomeTaxFlagItem" runat="server" ReadOnly="true" ResourceName="IncomeTaxFlag" Value='<%# Eval("IncomeTaxFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="FederalTaxPaycodeCodeItem" runat="server" ReadOnly="true" ResourceName="FederalTaxPaycodeCode" Value='<%# Eval("FederalTaxPaycodeCode") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="CanadaQuebecPensionPlanFlagItem" runat="server" ReadOnly="true" ResourceName="CanadaQuebecPensionPlanFlag" Value='<%# Eval("CanadaQuebecPensionPlanFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="EmploymentInsuranceFlagItem" runat="server" ReadOnly="true" ResourceName="EmploymentInsuranceFlag" Value='<%# Eval("EmploymentInsuranceFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="ProvincialParentalInsurancePlanFlagItem" runat="server" ReadOnly="true" ResourceName="ProvincialParentalInsurancePlanFlag" Value='<%# Eval("ProvincialParentalInsurancePlanFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="QuebecTaxFlagItem" runat="server" ReadOnly="true" ResourceName="QuebecTaxFlag" Value='<%# Eval("QuebecTaxFlag") %>' />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="QuebecTaxPaycodeCodeItem" runat="server" ReadOnly="true" ResourceName="QuebecTaxPaycodeCode" Value='<%# Eval("QuebecTaxPaycodeCode") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ProvincialHealthTaxFlagItem" runat="server" ReadOnly="true" ResourceName="ProvincialHealthTaxFlag" Value='<%# Eval("ProvincialHealthTaxFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="WorkersCompensationBoardFlagItem" runat="server" ReadOnly="true" ResourceName="WorkersCompensationBoardFlag" Value='<%# Eval("WorkersCompensationBoardFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="BarbadosNationalInsuranceSchemeFlagItem" runat="server" ReadOnly="true" ResourceName="BarbadosNationalInsuranceSchemeFlag" Value='<%# Eval("BarbadosNationalInsuranceSchemeFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="BarbadosIncomeTaxFlagItem" runat="server" ReadOnly="true" ResourceName="BarbadosIncomeTaxFlag" Value='<%# Eval("BarbadosIncomeTaxFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="SaintLuciaNationalInsuranceCorporationFlagItem" runat="server" ReadOnly="true" ResourceName="SaintLuciaNationalInsuranceCorporationFlag" Value='<%# Eval("SaintLuciaNationalInsuranceCorporationFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="SaintLuciaIncomeTaxFlagItem" runat="server" ReadOnly="true" ResourceName="SaintLuciaIncomeTaxFlag" Value='<%# Eval("SaintLuciaIncomeTaxFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="TrinidadNationalInsuranceBoardFlagItem" runat="server" ReadOnly="true" ResourceName="TrinidadNationalInsuranceBoardFlag" Value='<%# Eval("TrinidadNationalInsuranceBoardFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="TrinidadIncomeTaxFlagItem" runat="server" ReadOnly="true" ResourceName="TrinidadIncomeTaxFlag" Value='<%# Eval("TrinidadIncomeTaxFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="JamaicaNationalInsuranceSchemeFlagItem" runat="server" ReadOnly="true" ResourceName="JamaicaNationalInsuranceSchemeFlag" Value='<%# Eval("JamaicaNationalInsuranceSchemeFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="JamaicaIncomeTaxFlagItem" runat="server" ReadOnly="true" ResourceName="JamaicaIncomeTaxFlag" Value='<%# Eval("JamaicaIncomeTaxFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="JamaicaNationalHealthTaxFlagItem" runat="server" ReadOnly="true" ResourceName="JamaicaNationalHealthTaxFlag" Value='<%# Eval("JamaicaNationalHealthTaxFlag") %>' />
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>
    <EditItemTemplate>
        <wasp:WLPToolBar ID="PaycodeIncomeToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' ResourceName="Insert" CommandName="insert" />
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" ResourceName="Update" Visible='<%# IsEditMode %>' />
                <wasp:WLPToolBarButton Text="Cancel" onclick="Close()" ImageUrl="~/App_Themes/Default/Cancel.gif" CausesValidation="false" CommandName="cancel" ResourceName="Cancel" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="CodePaycodeCd" runat="server" MaxLength="8" ReadOnly='<%# IsEditMode || IsViewMode %>' ResourceName="PaycodeCode" Value='<%# Bind("PaycodeCode") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="EnglishDescription" runat="server" ResourceName="EnglishDescription" Value='<%# Bind("EnglishDescription") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="FrenchDescription" runat="server" ResourceName="FrenchDescription" Value='<%# Bind("FrenchDescription") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="ImportExternalIdentifier" runat="server" ResourceName="ImportExternalIdentifier" Value='<%# Bind("PaycodeCodeImportExternalIdentifier") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="SortOrder" runat="server" DecimalDigits="0" ResourceName="SortOrder" Value='<%# Bind("SortOrder") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ActiveFlag" runat="server" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="SystemFlag" runat="server" ResourceName="SystemFlag" Value='<%# Bind("SystemFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeInPayslipFlag" runat="server" ResourceName="IncludeInPayslipFlag" Value='<%# Bind("IncludeInPayslipFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeInPayRegisterFlag" runat="server" ResourceName="IncludeInPayRegisterFlag" Value='<%# Bind("IncludeInPayRegisterFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="GeneralLedgerMask" runat="server" ResourceName="GeneralLedgerMask" Value='<%# Bind("GeneralLedgerMask") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="AllowGeneralLedgerOverrideFlag" runat="server" ResourceName="AllowGeneralLedgerOverrideFlag" Value='<%# Bind("AllowGeneralLedgerOverrideFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="AutoPopulateRateFlag" runat="server" OnCheckedChanged="AutoPopulateRateFlag_CheckedChanged" AutoPostBack="true" ResourceName="AutoPopulateRateFlag" Value='<%# Bind("AutoPopulateRateFlag") %>' />
                    </td>
                    <td>
                        <wasp:NumericControl ID="AmountRateFactor" runat="server" MinValue="0" MaxValue="9.9999" DecimalDigits="4" ResourceName="AmountRateFactor" Value='<%# Bind("AmountRateFactor") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeEmploymentInsuranceHoursFlag" runat="server" ResourceName="IncludeEmploymentInsuranceHoursFlag" Value='<%# Bind("IncludeEmploymentInsuranceHoursFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="ReportGroupPaycodeTypeCode" runat="server" Type="PaycodeTypeCode" OnDataBinding="ReportGroupPaycodeTypeCode_NeedDataSource" ResourceName="ReportGroupPaycodeTypeCode" Value='<%# Bind("ReportGroupCodePaycodeTypeCd") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="ReportDisplayUnitFlag" runat="server" ResourceName="ReportDisplayUnitFlag" Value='<%# Bind("ReportDisplayUnitFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="GarnishmentFlag" runat="server" ResourceName="GarnishmentFlag" Value='<%# Bind("GarnishmentFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeInArrearsFlag" runat="server" ResourceName="IncludeInArrearsFlag" Value='<%# Bind("IncludeInArrearsFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="RecurringIncomeCodeFlag" runat="server" ResourceName="RecurringIncomeCodeFlag" Value='<%# Bind("RecurringIncomeCodeFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="VacationCalculationOverrideFlag" runat="server" ResourceName="VacationCalculationOverrideFlag" Value='<%# Bind("VacationCalculationOverrideFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="SubtractHourFromSalaryFlag" runat="server" ResourceName="SubtractHourFromSalaryFlag" Value='<%# Bind("SubtractHourFromSalaryFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="UseSalaryStandardHourFlag" runat="server" ResourceName="UseSalaryStandardHourFlag" Value='<%# Bind("UseSalaryStandardHourFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PaycodeActivationFrequencyCode" runat="server" Type="PaycodeActivationFrequencyCode" OnDataBinding="Code_NeedDataSource" ResourceName="PaycodeActivationFrequencyCode" Value='<%# Bind("PaycodeActivationFrequencyCode")%>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="PaycodeIncomePayTypeCode" runat="server" Type="PaycodeIncomePayTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="PaycodeIncomePayTypeCode" Value='<%# Bind("PaycodeIncomePayTypeCode") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="TaxOverrideFlag" runat="server" ResourceName="TaxOverrideFlag" Value='<%# Bind("TaxOverrideFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="IncomeTaxFlag" runat="server" ResourceName="IncomeTaxFlag" Value='<%# Bind("IncomeTaxFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="FederalTaxPaycodeCode" runat="server" Type="PaycodeCode" OnDataBinding="FederalTaxPaycodeCode_NeedDataSource" ResourceName="FederalTaxPaycodeCode" Value='<%# Bind("FederalTaxPaycodeCode") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="CanadaQuebecPensionPlanFlag" runat="server" ResourceName="CanadaQuebecPensionPlanFlag" Value='<%# Bind("CanadaQuebecPensionPlanFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="EmploymentInsuranceFlag" runat="server" ResourceName="EmploymentInsuranceFlag" Value='<%# Bind("EmploymentInsuranceFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="ProvincialParentalInsurancePlanFlag" runat="server" ResourceName="ProvincialParentalInsurancePlanFlag" Value='<%# Bind("ProvincialParentalInsurancePlanFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="QuebecTaxFlag" runat="server" ResourceName="QuebecTaxFlag" Value='<%# Bind("QuebecTaxFlag") %>' />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="QuebecTaxPaycodeCode" runat="server" Type="PaycodeCode" OnDataBinding="QuebecTaxPaycodeCode_NeedDataSource" ResourceName="QuebecTaxPaycodeCode" Value='<%# Bind("QuebecTaxPaycodeCode") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ProvincialHealthTaxFlag" runat="server" ResourceName="ProvincialHealthTaxFlag" Value='<%# Bind("ProvincialHealthTaxFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="WorkersCompensationBoardFlag" runat="server" ResourceName="WorkersCompensationBoardFlag" Value='<%# Bind("WorkersCompensationBoardFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="BarbadosNationalInsuranceSchemeFlag" runat="server" ResourceName="BarbadosNationalInsuranceSchemeFlag" Value='<%# Bind("BarbadosNationalInsuranceSchemeFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="BarbadosIncomeTaxFlag" runat="server" ResourceName="BarbadosIncomeTaxFlag" Value='<%# Bind("BarbadosIncomeTaxFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="SaintLuciaNationalInsuranceCorporationFlag" runat="server" ResourceName="SaintLuciaNationalInsuranceCorporationFlag" Value='<%# Bind("SaintLuciaNationalInsuranceCorporationFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="SaintLuciaIncomeTaxFlag" runat="server" ResourceName="SaintLuciaIncomeTaxFlag" Value='<%# Bind("SaintLuciaIncomeTaxFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="TrinidadNationalInsuranceBoardFlag" runat="server" ResourceName="TrinidadNationalInsuranceBoardFlag" Value='<%# Bind("TrinidadNationalInsuranceBoardFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="TrinidadIncomeTaxFlag" runat="server" ResourceName="TrinidadIncomeTaxFlag" Value='<%# Bind("TrinidadIncomeTaxFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="JamaicaNationalInsuranceSchemeFlag" runat="server" ResourceName="JamaicaNationalInsuranceSchemeFlag" Value='<%# Bind("JamaicaNationalInsuranceSchemeFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="JamaicaIncomeTaxFlag" runat="server" ResourceName="JamaicaIncomeTaxFlag" Value='<%# Bind("JamaicaIncomeTaxFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="JamaicaNationalHealthTaxFlag" runat="server" ResourceName="JamaicaNationalHealthTaxFlag" Value='<%# Bind("JamaicaNationalHealthTaxFlag") %>' />
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>
</wasp:WLPFormView>

<script type="text/javascript">
    function Close(button, args) {
        var popup = getRadWindow();
        setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
    }

    function processClick(commandName) {
        var arg = new Object;
        arg.closeWithChanges = true;
        getRadWindow().argument = arg;
    }

    function getRadWindow() {
        var popup = null;

        if (window.radWindow)
            popup = window.radWindow;
        else if (window.frameElement.radWindow)
            popup = window.frameElement.radWindow;

        return popup;
    }

    function ToolBarClick(sender, args) {
        var button = args.get_item();
        var commandName = button.get_commandName();

        if (commandName != "cancel") {
            processClick(commandName);
        }

        if (<%= IsInsertMode.ToString().ToLower() %> || <%= IsImportMode.ToString().ToLower() %>) {
            if (commandName == "cancel") {
                window.close();
            }
        }
    }
    
    function DefaultAmountRateFactor() {
        var amountRateFactorControl = document.getElementById('<%= (PaycodeIncomeView.FindControl("AmountRateFactor") ?? PaycodeIncomeView.FindControl("AmountRateFactorItem")).ClientID %>');
        if (amountRateFactorControl != null) var amountRateFactor = document.getElementById(amountRateFactorControl.attributes['fieldClientId'].value);

        amountRateFactor.value = 1;
    }
</script>