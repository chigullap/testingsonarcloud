﻿using System;
using System.Web.UI.WebControls;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.PaycodeMaintenance
{
    public partial class PaycodeIncomeControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private String Action { get { return Page.RouteData.Values["action"].ToString().ToLower(); } }
        private String PaycodeCode { get { return Page.Request.QueryString["paycodeCode"]; } }
        public bool IsImportMode { get { return Action == "import"; } }
        public bool IsSystemFlag { get { return ((CodePaycodeIncomeCollection)Data)[0].SystemFlag; } }
        public bool IsEditMode { get { return PaycodeIncomeView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool IsInsertMode { get { return PaycodeIncomeView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsViewMode { get { return PaycodeIncomeView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.PaycodeMaintenance.UpdateFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.PaycodeMaintenance.ViewFlag);

            WireEvents();

            if (Action == "view")
            {
                this.Page.Title = "Edit an Income Paycode";

                if (!IsPostBack)
                {
                    CodePaycodeIncomeCollection collection = new CodePaycodeIncomeCollection();
                    collection = Common.ServiceWrapper.CodeClient.GetCodePaycodeIncome(PaycodeCode);
                    Data = collection;

                    PaycodeIncomeView.DataBind();
                }
            }
            else if (Action == "add")
            {
                if (!IsPostBack)
                {
                    this.Page.Title = "Add an Income Paycode";

                    CodePaycodeIncomeCollection collection = new CodePaycodeIncomeCollection();
                    collection.Add(new CodePaycodeIncome() { PaycodeCode = "" });
                    Data = collection;

                    PaycodeIncomeView.ChangeMode(FormViewMode.Insert);
                    PaycodeIncomeView.DataBind();
                }
                else
                    this.Page.Title = "Edit an Income Paycode";
            }
            else if (IsImportMode)
            {
                this.Page.Title = "Edit an Income Paycode";

                if (!IsPostBack)
                {
                    CodePaycodeIncomeCollection collection = new CodePaycodeIncomeCollection();
                    collection.Add((CodePaycodeIncome)Session["HandOff"]);
                    Session.Remove("HandOff");
                    Data = collection;

                    PaycodeIncomeView.ChangeMode(FormViewMode.Edit);
                    PaycodeIncomeView.DataBind();
                }
            }

            SetAmountRateFactorMandatory();
        }
        protected void WireEvents()
        {
            PaycodeIncomeView.NeedDataSource += new WLPFormView.NeedDataSourceEventHandler(PaycodeIncomeView_NeedDataSource);
            PaycodeIncomeView.Updating += PaycodeIncomeView_Updating;
            PaycodeIncomeView.Inserting += PaycodeIncomeView_Inserting;
            PaycodeIncomeView.ItemCreated += new EventHandler(PaycodeIncomeView_ItemCreated);
        }
        protected void SetAmountRateFactorMandatory()
        {
            CheckBoxControl autoPopulateRateFlag = (CheckBoxControl)PaycodeIncomeView.FindControl("AutoPopulateRateFlag");
            if (autoPopulateRateFlag == null)
                autoPopulateRateFlag = (CheckBoxControl)PaycodeIncomeView.FindControl("AutoPopulateRateFlagItem");

            NumericControl amountRateFactor = (NumericControl)PaycodeIncomeView.FindControl("AmountRateFactor");
            if (amountRateFactor == null)
                amountRateFactor = (NumericControl)PaycodeIncomeView.FindControl("AmountRateFactorItem");

            amountRateFactor.Mandatory = !autoPopulateRateFlag.Checked;

            if (Action == "add" && amountRateFactor.Mandatory)
                CallClientSideDefaultMethods();
        }
        protected void CallClientSideDefaultMethods()
        {
            //call the DefaultAmountRateFactor client-side method
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "DefaultAmountRateFactor", "DefaultAmountRateFactor();", true);
        }
        #endregion

        #region event handlers
        protected void PaycodeIncomeView_ItemCreated(object sender, EventArgs e)
        {
            if (IsInsertMode)
            {
                CheckBoxControl activeFlag = (CheckBoxControl)PaycodeIncomeView.FindControl("ActiveFlag");
                activeFlag.Value = true;
            }
        }
        void PaycodeIncomeView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            PaycodeIncomeView.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void ReportGroupPaycodeTypeCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControlWithoutFiltering((ICodeControl)sender, LanguageCode);
        }
        protected void FederalTaxPaycodeCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode, "code_paycode_type_cd", Convert.ToInt32(CodePaycode.PaycodeType.Deduction).ToString());
        }
        protected void QuebecTaxPaycodeCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode, "code_paycode_type_cd", Convert.ToInt32(CodePaycode.PaycodeType.Deduction).ToString());
        }
        protected void AutoPopulateRateFlag_CheckedChanged(object sender, EventArgs e)
        {
            SetAmountRateFactorMandatory();
        }
        #endregion

        #region handle updates
        void PaycodeIncomeView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            try
            {
                CodePaycodeIncome item = (CodePaycodeIncome)e.DataItem;
                item.PaycodeTypeCode = ((short)CodePaycode.PaycodeType.Income).ToString();

                Common.ServiceWrapper.CodeClient.InsertCodePaycodeIncome(item).CopyTo((CodePaycodeIncome)Data[item.Key]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void PaycodeIncomeView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                CodePaycodeIncome item = (CodePaycodeIncome)e.DataItem;

                if (IsImportMode)
                {
                    CodePaycodeCollection collection = Common.ServiceWrapper.CodeClient.GetCodePaycode(item.PaycodeCode, false);

                    if (collection != null && collection.Count > 0)
                        Common.ServiceWrapper.CodeClient.UpdateCodePaycodeIncome(item, true, true).CopyTo((CodePaycodeIncome)Data[item.Key]);
                    else
                        Common.ServiceWrapper.CodeClient.InsertCodePaycodeIncome(item).CopyTo((CodePaycodeIncome)Data[item.Key]);
                }
                else
                    Common.ServiceWrapper.CodeClient.UpdateCodePaycodeIncome(item, false).CopyTo((CodePaycodeIncome)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);

                e.Cancel = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}