﻿using System;
using System.IO;
using System.Xml.Serialization;
using Telerik.Web.UI;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.PaycodeMaintenance
{
    public partial class PaycodeImportControl : WLP.Web.UI.WLPUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "PaycodeImport"));
        }
        protected void Submit_Click(object sender, EventArgs e)
        {
            try
            {
                UploadedFile file = UploadControl.UploadedFiles[0];
                byte[] fileContent = new byte[file.ContentLength];
                file.InputStream.Read(fileContent, 0, (int)file.ContentLength);

                String paycodeType = GetPaycodeType(fileContent);
                XmlSerializer xml = null;
                MemoryStream stream = new MemoryStream(fileContent);

                if (paycodeType == ((short)CodePaycode.PaycodeType.Benefit).ToString())
                {
                    xml = new XmlSerializer(typeof(CodePaycodeBenefit));
                    CodePaycodeBenefit paycodeBenefit = (CodePaycodeBenefit)xml.Deserialize(stream);
                    Session["HandOff"] = paycodeBenefit;

                    Response.Redirect(String.Format("~/Admin/PaycodeMaintenance/Benefit/{0}/{1}", "import", paycodeBenefit.PaycodeCode));
                }
                else if (paycodeType == ((short)CodePaycode.PaycodeType.Deduction).ToString())
                {
                    xml = new XmlSerializer(typeof(CodePaycodeDeduction));
                    CodePaycodeDeduction paycodeDeduction = (CodePaycodeDeduction)xml.Deserialize(stream);
                    Session["HandOff"] = paycodeDeduction;

                    Response.Redirect(String.Format("~/Admin/PaycodeMaintenance/Deduction/{0}/{1}", "import", paycodeDeduction.PaycodeCode));
                }
                else if (paycodeType == ((short)CodePaycode.PaycodeType.Income).ToString())
                {
                    xml = new XmlSerializer(typeof(CodePaycodeIncome));
                    CodePaycodeIncome paycodeIncome = (CodePaycodeIncome)xml.Deserialize(stream);
                    Session["HandOff"] = paycodeIncome;

                    Response.Redirect(String.Format("~/Admin/PaycodeMaintenance/Income/{0}/{1}", "import", paycodeIncome.PaycodeCode));
                }
                else if (paycodeType == ((short)CodePaycode.PaycodeType.Employer).ToString())
                {
                    xml = new XmlSerializer(typeof(CodePaycode));
                    CodePaycode paycode = (CodePaycode)xml.Deserialize(stream);
                    Session["HandOff"] = paycode;

                    Response.Redirect(String.Format("~/Admin/PaycodeMaintenance/Employer/{0}/{1}", "import", paycode.PaycodeCode));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public String GetPaycodeType(byte[] fileContent)
        {
            MemoryStream stream = new MemoryStream(fileContent);
            XmlSerializer xml = new XmlSerializer(typeof(CodePaycode));
            CodePaycode paycode = (CodePaycode)xml.Deserialize(stream);

            return paycode.PaycodeTypeCode;
        }
    }
}