﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaycodeDeductionControl.ascx.cs" Inherits="WorkLinks.Admin.PaycodeMaintenance.PaycodeDeductionControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<wasp:WLPFormView ID="PaycodeDeductionView" runat="server" RenderOuterTable="false" DataKeyNames="Key">
    <ItemTemplate>
        <wasp:WLPToolBar ID="PaycodeDeductionToolBarItem" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
            <Items>
                <wasp:WLPToolBarButton Text="Edit" Visible='<%# UpdateFlag && !IsSystemFlag %>' ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="Edit" ResourceName="Edit" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="CodePaycodeCdItem" runat="server" ReadOnly="true" ResourceName="PaycodeCode" Value='<%# Eval("PaycodeCode") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="PaycodeAttachedPaycodeProvisionItem" runat="server" CheckBoxes="true" IncludeEmptyItem="false" Type="CodePaycode" OnDataBinding="AttachedPaycodes_NeedDataSource" OnItemDataBound="PaycodeAttachedPaycodeProvision_ItemDataBound" OnClientDropDownOpening="PaycodeAttachedPaycodeProvision_ClientDropDownOpening" ResourceName="PaycodeAttachedPaycodeProvision" Value='<%# Eval("AttachedPaycodes")%>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="EnglishDescriptionItem" runat="server" ReadOnly="true" ResourceName="EnglishDescription" Value='<%# Eval("EnglishDescription") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="FrenchDescriptionItem" runat="server" ReadOnly="true" ResourceName="FrenchDescription" Value='<%# Eval("FrenchDescription") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="ImportExternalIdentifierItem" runat="server" ReadOnly="true" ResourceName="ImportExternalIdentifier" Value='<%# Eval("PaycodeCodeImportExternalIdentifier") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="SortOrderItem" runat="server" ReadOnly="true" DecimalDigits="0" ResourceName="SortOrder" Value='<%# Eval("SortOrder") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ActiveFlagItem" runat="server" ReadOnly="true" ResourceName="ActiveFlag" Value='<%# Eval("ActiveFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="SystemFlagItem" runat="server" ReadOnly="true" ResourceName="SystemFlag" Value='<%# Eval("SystemFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeInPayslipFlagItem" runat="server" ReadOnly="true" ResourceName="IncludeInPayslipFlag" Value='<%# Eval("IncludeInPayslipFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeInPayRegisterFlagItem" runat="server" ReadOnly="true" ResourceName="IncludeInPayRegisterFlag" Value='<%# Eval("IncludeInPayRegisterFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="GeneralLedgerMaskItem" runat="server" ReadOnly="true" ResourceName="GeneralLedgerMask" Value='<%# Eval("GeneralLedgerMask") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="AllowGeneralLedgerOverrideFlagItem" runat="server" ReadOnly="true" ResourceName="AllowGeneralLedgerOverrideFlag" Value='<%# Eval("AllowGeneralLedgerOverrideFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="AutoPopulateRateFlagItem" runat="server" ReadOnly="true" ResourceName="AutoPopulateRateFlag" Value='<%# Eval("AutoPopulateRateFlag") %>' />
                    </td>
                    <td>
                        <wasp:NumericControl ID="AmountRateFactorItem" runat="server" ReadOnly="true" DecimalDigits="4" ResourceName="AmountRateFactor" Value='<%# Eval("AmountRateFactor") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeEmploymentInsuranceHoursFlagItem" runat="server" ReadOnly="true" ResourceName="IncludeEmploymentInsuranceHoursFlag" Value='<%# Eval("IncludeEmploymentInsuranceHoursFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="ReportGroupPaycodeTypeCodeItem" runat="server" ReadOnly="true" Type="PaycodeTypeCode" OnDataBinding="ReportGroupPaycodeTypeCode_NeedDataSource" ResourceName="ReportGroupPaycodeTypeCode" Value='<%# Eval("ReportGroupCodePaycodeTypeCd") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="ReportDisplayUnitFlagItem" runat="server" ReadOnly="true" ResourceName="ReportDisplayUnitFlag" Value='<%# Eval("ReportDisplayUnitFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="GarnishmentFlagItem" runat="server" ReadOnly="true" ResourceName="GarnishmentFlag" Value='<%# Eval("GarnishmentFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeInArrearsFlagItem" runat="server" ReadOnly="true" ResourceName="IncludeInArrearsFlag" Value='<%# Eval("IncludeInArrearsFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="RecurringIncomeCodeFlagItem" runat="server" ReadOnly="true" ResourceName="RecurringIncomeCodeFlag" Value='<%# Eval("RecurringIncomeCodeFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="VacationCalculationOverrideFlagItem" runat="server" ReadOnly="true" ResourceName="VacationCalculationOverrideFlag" Value='<%# Eval("VacationCalculationOverrideFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="SubtractHourFromSalaryFlagItem" runat="server" ReadOnly="true" ResourceName="SubtractHourFromSalaryFlag" Value='<%# Eval("SubtractHourFromSalaryFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="UseSalaryStandardHourFlagItem" runat="server" ReadOnly="true" ResourceName="UseSalaryStandardHourFlag" Value='<%# Eval("UseSalaryStandardHourFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PaycodeActivationFrequencyCodeItem" runat="server" Type="PaycodeActivationFrequencyCode" OnDataBinding="Code_NeedDataSource" ReadOnly="true" ResourceName="PaycodeActivationFrequencyCode" Value='<%# Eval("PaycodeActivationFrequencyCode") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="RequiredMinimumIncomeAmountItem" runat="server" ReadOnly="true" DecimalDigits="4" ResourceName="RequiredMinimumIncomeAmount" Value='<%# Eval("RequiredMinimumIncomeAmount") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="PayPeriodMaximumAmountItem" runat="server" ReadOnly="true" DecimalDigits="4" ResourceName="PayPeriodMaximumAmount" Value='<%# Eval("PayPeriodMaximumAmount") %>' />
                    </td>
                    <td>
                        <wasp:NumericControl ID="PayPeriodMinimumAmountItem" runat="server" ReadOnly="true" DecimalDigits="4" ResourceName="PayPeriodMinimumAmount" Value='<%# Eval("PayPeriodMinimumAmount") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="YearlyMaximumAmountItem" runat="server" ReadOnly="true" DecimalDigits="4" ResourceName="YearlyMaximumAmount" Value='<%# Eval("YearlyMaximumAmount") %>' />
                    </td>
                    <td>
                        <wasp:NumericControl ID="YearlyMinimumAmountItem" runat="server" ReadOnly="true" DecimalDigits="4" ResourceName="YearlyMinimumAmount" Value='<%# Eval("YearlyMinimumAmount") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PaycodeRateBasedOnCodeItem" runat="server" ReadOnly="true" Type="PaycodeRateBasedOnCode" OnDataBinding="Code_NeedDataSource" ResourceName="PaycodeRateBasedOnCode" Value='<%# Eval("PaycodeRateBasedOnCode") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="BeforeIncomeTaxDeductionFlagItem" runat="server" ReadOnly="true" ResourceName="BeforeIncomeTaxDeductionFlag" Value='<%# Eval("BeforeIncomeTaxDeductionFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="BeforeBonusTaxDeductionFlagItem" runat="server" ReadOnly="true" ResourceName="BeforeBonusTaxDeductionFlag" Value='<%# Eval("BeforeBonusTaxDeductionFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="BeforeRetroactiveTaxDeductionFlagItem" runat="server" ReadOnly="true" ResourceName="BeforeRetroactiveTaxDeductionFlag" Value='<%# Eval("BeforeRetroactiveTaxDeductionFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="BeforeJamaicaTaxDeductionFlagItem" runat="server" ReadOnly="true" ResourceName="BeforeJamaicaTaxDeductionFlag" Value='<%# Eval("BeforeJamaicaTaxDeductionFlag") %>' />
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar ID="PaycodeDeductionToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' ResourceName="Insert" CommandName="insert" />
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" ResourceName="Update" Visible='<%# IsEditMode %>' />
                <wasp:WLPToolBarButton Text="Cancel" onclick="Close()" ImageUrl="~/App_Themes/Default/Cancel.gif" CausesValidation="false" CommandName="cancel" ResourceName="Cancel" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="CodePaycodeCd" runat="server" MaxLength="8" ReadOnly='<%# IsEditMode || IsViewMode %>' ResourceName="PaycodeCode" Value='<%# Bind("PaycodeCode") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="PaycodeAttachedPaycodeProvision" runat="server" CheckBoxes="true" IncludeEmptyItem="false" Type="CodePaycode" OnDataBinding="AttachedPaycodes_NeedDataSource" OnItemDataBound="PaycodeAttachedPaycodeProvision_ItemDataBound" ResourceName="PaycodeAttachedPaycodeProvision" Value='<%# Bind("AttachedPaycodes")%>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="EnglishDescription" runat="server" ResourceName="EnglishDescription" Value='<%# Bind("EnglishDescription") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="FrenchDescription" runat="server" ResourceName="FrenchDescription" Value='<%# Bind("FrenchDescription") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="ImportExternalIdentifier" runat="server" ResourceName="ImportExternalIdentifier" Value='<%# Bind("PaycodeCodeImportExternalIdentifier") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="SortOrder" runat="server" DecimalDigits="0" ResourceName="SortOrder" Value='<%# Bind("SortOrder") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ActiveFlag" runat="server" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="SystemFlag" runat="server" ResourceName="SystemFlag" Value='<%# Bind("SystemFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeInPayslipFlag" runat="server" ResourceName="IncludeInPayslipFlag" Value='<%# Bind("IncludeInPayslipFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeInPayRegisterFlag" runat="server" ResourceName="IncludeInPayRegisterFlag" Value='<%# Bind("IncludeInPayRegisterFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="GeneralLedgerMask" runat="server" ResourceName="GeneralLedgerMask" Value='<%# Bind("GeneralLedgerMask") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="AllowGeneralLedgerOverrideFlag" runat="server" ResourceName="AllowGeneralLedgerOverrideFlag" Value='<%# Bind("AllowGeneralLedgerOverrideFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="AutoPopulateRateFlag" runat="server" OnCheckedChanged="AutoPopulateRateFlag_CheckedChanged" AutoPostBack="true" ResourceName="AutoPopulateRateFlag" Value='<%# Bind("AutoPopulateRateFlag") %>' />
                    </td>
                    <td>
                        <wasp:NumericControl ID="AmountRateFactor" runat="server" MinValue="0" MaxValue="9.9999" DecimalDigits="4" ResourceName="AmountRateFactor" Value='<%# Bind("AmountRateFactor") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeEmploymentInsuranceHoursFlag" runat="server" ResourceName="IncludeEmploymentInsuranceHoursFlag" Value='<%# Bind("IncludeEmploymentInsuranceHoursFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="ReportGroupPaycodeTypeCode" runat="server" Type="PaycodeTypeCode" OnDataBinding="ReportGroupPaycodeTypeCode_NeedDataSource" ResourceName="ReportGroupPaycodeTypeCode" Value='<%# Bind("ReportGroupCodePaycodeTypeCd") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="ReportDisplayUnitFlag" runat="server" ResourceName="ReportDisplayUnitFlag" Value='<%# Bind("ReportDisplayUnitFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="GarnishmentFlag" runat="server" ResourceName="GarnishmentFlag" Value='<%# Bind("GarnishmentFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="IncludeInArrearsFlag" runat="server" ResourceName="IncludeInArrearsFlag" Value='<%# Bind("IncludeInArrearsFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="RecurringIncomeCodeFlag" runat="server" ResourceName="RecurringIncomeCodeFlag" Value='<%# Bind("RecurringIncomeCodeFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="VacationCalculationOverrideFlag" runat="server" ResourceName="VacationCalculationOverrideFlag" Value='<%# Bind("VacationCalculationOverrideFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="SubtractHourFromSalaryFlag" runat="server" ResourceName="SubtractHourFromSalaryFlag" Value='<%# Bind("SubtractHourFromSalaryFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="UseSalaryStandardHourFlag" runat="server" ResourceName="UseSalaryStandardHourFlag" Value='<%# Bind("UseSalaryStandardHourFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PaycodeActivationFrequencyCode" runat="server" Type="PaycodeActivationFrequencyCode" OnDataBinding="Code_NeedDataSource" ResourceName="PaycodeActivationFrequencyCode" Value='<%# Bind("PaycodeActivationFrequencyCode")%>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="RequiredMinimumIncomeAmount" runat="server" DecimalDigits="4" ResourceName="RequiredMinimumIncomeAmount" Value='<%# Bind("RequiredMinimumIncomeAmount") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="PayPeriodMaximumAmount" runat="server" DecimalDigits="4" ResourceName="PayPeriodMaximumAmount" Value='<%# Bind("PayPeriodMaximumAmount") %>' />
                    </td>
                    <td>
                        <wasp:NumericControl ID="PayPeriodMinimumAmount" runat="server" DecimalDigits="4" ResourceName="PayPeriodMinimumAmount" Value='<%# Bind("PayPeriodMinimumAmount") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="YearlyMaximumAmount" runat="server" DecimalDigits="4" ResourceName="YearlyMaximumAmount" Value='<%# Bind("YearlyMaximumAmount") %>' />
                    </td>
                    <td>
                        <wasp:NumericControl ID="YearlyMinimumAmount" runat="server" DecimalDigits="4" ResourceName="YearlyMinimumAmount" Value='<%# Bind("YearlyMinimumAmount") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PaycodeRateBasedOnCode" runat="server" Type="PaycodeRateBasedOnCode" OnDataBinding="Code_NeedDataSource" ResourceName="PaycodeRateBasedOnCode" Value='<%# Bind("PaycodeRateBasedOnCode") %>' OnClientSelectedIndexChanged="PaycodeRateBasedOnCode_ClientSelectedIndexChanged" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="BeforeIncomeTaxDeductionFlag" runat="server" ResourceName="BeforeIncomeTaxDeductionFlag" Value='<%# Bind("BeforeIncomeTaxDeductionFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="BeforeBonusTaxDeductionFlag" runat="server" ResourceName="BeforeBonusTaxDeductionFlag" Value='<%# Bind("BeforeBonusTaxDeductionFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="BeforeRetroactiveTaxDeductionFlag" runat="server" ResourceName="BeforeRetroactiveTaxDeductionFlag" Value='<%# Bind("BeforeRetroactiveTaxDeductionFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="BeforeJamaicaTaxDeductionFlag" runat="server" ResourceName="BeforeJamaicaTaxDeductionFlag" Value='<%# Bind("BeforeJamaicaTaxDeductionFlag") %>' />
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>
</wasp:WLPFormView>

<script type="text/javascript">
    function Close(button, args) {
        var popup = getRadWindow();
        setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
    }

    function processClick(commandName) {
        var arg = new Object;
        arg.closeWithChanges = true;
        getRadWindow().argument = arg;
    }

    function getRadWindow() {
        var popup = null;

        if (window.radWindow)
            popup = window.radWindow;
        else if (window.frameElement.radWindow)
            popup = window.frameElement.radWindow;

        return popup;
    }

    function ToolBarClick(sender, args) {
        var button = args.get_item();
        var commandName = button.get_commandName();

        if (commandName != "cancel") {
            processClick(commandName);
        }

        if (<%= IsInsertMode.ToString().ToLower() %> || <%= IsImportMode.ToString().ToLower() %>) {
            if (commandName == "cancel") {
                window.close();
            }
        }
    }

    function PaycodeRateBasedOnCode_ClientSelectedIndexChanged(sender, eventArgs) {
        var selectedValue = eventArgs.get_item()._control._value;
        PaycodeRateBasedOnCodeRules(selectedValue);
    }

    function PaycodeRateBasedOnCodeRules(selectedValue) {
        var paycodeAttachedPaycodeProvision = document.getElementById('<%= (PaycodeDeductionView.FindControl("PaycodeAttachedPaycodeProvision") ?? PaycodeDeductionView.FindControl("PaycodeAttachedPaycodeProvisionItem")).ClientID %>');
        if (paycodeAttachedPaycodeProvision != null) {
            if (selectedValue == "PAYDOLL" || selectedValue == "EIHOURS") // Paycode Dollars
                paycodeAttachedPaycodeProvision.style.display = "inline-block";
            else // No selection, Est. Annual Salary, Gross Pay or Net Pay
                paycodeAttachedPaycodeProvision.style.display = "none";
        }
    }

    // This method allows the user to open the PaycodeAttachedPaycodeProvision drop down in view mode, but not change any of the checked items.
    function PaycodeAttachedPaycodeProvision_ClientDropDownOpening(sender, eventArgs) {
        var items = sender.get_items(); 
        for (i = 0; i < items.get_count(); i++) {
            var item = items.getItem(i);
            item.set_enabled(false);
        }
    }

    function DefaultAmountRateFactor() {
        var amountRateFactorControl = document.getElementById('<%= (PaycodeDeductionView.FindControl("AmountRateFactor") ?? PaycodeDeductionView.FindControl("AmountRateFactorItem")).ClientID %>');
        if (amountRateFactorControl != null) var amountRateFactor = document.getElementById(amountRateFactorControl.attributes['fieldClientId'].value);

        amountRateFactor.value = 1;
    }
</script>