﻿using System;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.PaycodeMaintenance
{
    public partial class PaycodeYearEndReportMapControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public bool IsViewMode { get { return YearEndReportMapGrid.IsViewMode; } }
        public bool IsUpdate { get { return YearEndReportMapGrid.IsEditMode; } }
        public bool IsInsert { get { return YearEndReportMapGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.PaycodeYearEndReportMap.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.PaycodeYearEndReportMap.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.PaycodeYearEndReportMap.DeleteFlag; } }
        protected string PaycodeCode { get { return Page.RouteData.Values["paycodeCode"]?.ToString(); } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.PaycodeYearEndReportMap.ViewFlag);

            Page.Title = $"{GetGlobalResourceObject("PageTitle", "PaycodeYearEndReportMap")}";

            WireEvents();

            if (!IsPostBack)
            {
                LoadYearEndReportMap();
                Initialize();
            }
        }
        protected void WireEvents()
        {
            YearEndReportMapGrid.NeedDataSource += new Telerik.Web.UI.GridNeedDataSourceEventHandler(YearEndReportMapGrid_NeedDataSource);
        }
        protected void Initialize()
        {
            //find the YearEndReportMapGrid
            WLPGrid grid = (WLPGrid)FindControl("YearEndReportMapGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        protected void LoadYearEndReportMap()
        {
            Data = PaycodeYearEndReportMapCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetPaycodeYearEndReportMap(PaycodeCode));
        }
        #endregion

        #region event handlers
        void YearEndReportMapGrid_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            YearEndReportMapGrid.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void YearEndFormCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
            BindYearEndFormBoxCombo(sender);
        }
        private void BindYearEndFormBoxCombo(object sender)
        {
            if (sender is ComboBoxControl yearEndFormCombo)
            {
                ComboBoxControl yearEndFormBoxCombo = (ComboBoxControl)((Telerik.Web.UI.GridEditFormItem)yearEndFormCombo.BindingContainer).FindControl("YearEndFormBoxCode");
                yearEndFormBoxCombo.DataSource = Common.ServiceWrapper.CodeClient.GetYearEndFormBoxCode(yearEndFormCombo.SelectedValue);
                yearEndFormBoxCombo.DataBind();
            }
        }
        protected void CountryCode_NeedDataSource(object sender, EventArgs e)
        {
            Code_NeedDataSource(sender, e);
            BindProvinceStateCombo(sender);
        }
        private void BindProvinceStateCombo(object sender)
        {
            if (sender is ComboBoxControl countryCombo)
            {
                ComboBoxControl provinceStateCombo = (ComboBoxControl)((Telerik.Web.UI.GridEditFormItem)countryCombo.BindingContainer).FindControl("ProvinceStateCode");
                provinceStateCombo.DataSource = Common.ServiceWrapper.CodeClient.GetProvinceStateCode(countryCombo.SelectedValue);
                provinceStateCombo.DataBind();
            }
        }
        protected void CountryCode_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindProvinceStateCombo(sender);
        }
        protected void YearEndFormCode_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindYearEndFormBoxCombo(sender);
        }
        #endregion

        #region handle updates
        protected void YearEndReportMapGrid_InsertCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                PaycodeYearEndReportMap item = (PaycodeYearEndReportMap)e.Item.DataItem;
                item.PaycodeCode = PaycodeCode;
                Common.ServiceWrapper.HumanResourcesClient.InsertPaycodeYearEndReportMap(item).CopyTo((PaycodeYearEndReportMap)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.UniqueIndexFault)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, GetType(), "PopupScript", $"alert('{GetGlobalResourceObject("ErrorMessages", "UniqueIndex")}');", true);

                LoadYearEndReportMap();
                e.Canceled = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void YearEndReportMapGrid_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                PaycodeYearEndReportMap item = (PaycodeYearEndReportMap)e.Item.DataItem;
                item.PaycodeCode = PaycodeCode;
                Common.ServiceWrapper.HumanResourcesClient.UpdatePaycodeYearEndReportMap(item).CopyTo((PaycodeYearEndReportMap)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, GetType(), "PopupScript", $"alert('{GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")}');", true);

                e.Canceled = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void YearEndReportMapGrid_DeleteCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeletePaycodeYearEndReportMap((PaycodeYearEndReportMap)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, GetType(), "PopupScript", $"alert('{GetGlobalResourceObject("ErrorMessages", "FKConstraint")}');", true);

                LoadYearEndReportMap();
                e.Canceled = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}