﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CSBExportControl.ascx.cs" Inherits="WorkLinks.Admin.CSBExport.CSBExportControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<wasp:WLPFormView
    ID="CSBExportDetailView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key"
    OnNeedDataSource="CSBExportDetailView_NeedDataSource"
    OnUpdating="CSBExportDetailView_Updating">

    <ItemTemplate>
        <wasp:WLPToolBar ID="CSBExportToolBar" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="CSBExportToolBar_ButtonClick">
            <Items>
                <wasp:WLPToolBarButton Text="Edit" Visible='<%# UpdateFlag %>' ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="edit" ResourceName="Edit"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Export" Visible='<%# IsViewMode %>' ImageUrl="" CommandName="export" ResourceName="export"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <div>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="TransmitterOrgId" runat="server" LabelText="TransmitterOrgId" ResourceName="TransmitterOrgId" Value='<%# Eval("TransmitterOrganizationNumber") %>' ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="OrganizationId" runat="server" LabelText="OrganizationId" ResourceName="OrganizationId" Value='<%# Eval("OrganizationNumber") %>' ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="CanadaRevenueAgencyBondPaymentTypeCode" Text="PaymentTypeCode" runat="server" Type="CanadaRevenueAgencyBondPaymentTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="CanadaRevenueAgencyBondPaymentTypeCode" Value='<%# Eval("CodeCanadaRevenueAgencyBondPaymentTypeCd") %>' ReadOnly="true" TabIndex="030" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:RadioButtonControl ID="ContactRadioButton" Value='<%# RadioCodeValue %>' runat="server" LabelText="ContactRadioButton" OnDataBinding="RadioButton_NeedDataSource" OnPreRender="ContactRadioButton_PreRender" ReadOnly="true" ResourceName="ContactRadioButton"></wasp:RadioButtonControl>
                        </td>
                        <td>
                            <span id="EmailSpan">
                                <wasp:TextBoxControl ID="Email" runat="server" ResourceName="Email" Value='<%# Bind("ContactEmail") %>' ReadOnly="true" />
                            </span>
                            <span id="FaxSpan">
                                <wasp:MaskedControl ID="Fax" Mask="###-###-####" runat="server" ResourceName="Fax" Value='<%# Bind("ContactFax") %>' ReadOnly="true" />
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar ID="CSBExportToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" ValidationGroup="ContactValidationGroup" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" ValidationGroup="ContactValidationGroup" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Cancel" onclick="Close()" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" ResourceName="Cancel" CausesValidation="false"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <div>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="TransmitterOrgId" runat="server" MaxLength="5" LabelText="TransmitterOrgId" ResourceName="TransmitterOrgId" Value='<%# Bind("TransmitterOrganizationNumber") %>' ReadOnly="false" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="OrganizationId" runat="server" MaxLength="5" LabelText="OrganizationId" ResourceName="OrganizationId" Value='<%# Bind("OrganizationNumber") %>' ReadOnly="false" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="CanadaRevenueAgencyBondPaymentTypeCode" Text="PaymentTypeCode" runat="server" Type="CanadaRevenueAgencyBondPaymentTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="CanadaRevenueAgencyBondPaymentTypeCode" Value='<%# Bind("CodeCanadaRevenueAgencyBondPaymentTypeCd") %>' ReadOnly="false" TabIndex="030" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:RadioButtonControl ID="ContactRadioButton" Value='<%# RadioCodeValue %>' runat="server" LabelText="ContactRadioButton" OnDataBinding="RadioButton_NeedDataSource" OnPreRender="ContactRadioButton_PreRender" ReadOnly="false" ResourceName="ContactRadioButton"></wasp:RadioButtonControl>
                        </td>
                        <td>
                            <span id="EmailSpan">
                                <wasp:TextBoxControl ID="Email" runat="server" ResourceName="Email" MaxLength="40" Value='<%# Bind("ContactEmail") %>' />
                                <asp:CustomValidator ID="EmailValidator" runat="server" ForeColor="Red" ValidationGroup="ContactValidationGroup" ErrorMessage="**Invalid Email Format**" OnServerValidate="EmailValidator_ServerValidate">* Missing or Invalid Email Format</asp:CustomValidator>
                            </span>
                            <span id="FaxSpan">
                                <wasp:MaskedControl ID="Fax" Mask="###-###-####" runat="server" ResourceName="Fax" Value='<%# Bind("ContactFax") %>' ReadOnly="false" />
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </EditItemTemplate>

</wasp:WLPFormView>

<script type="text/javascript">
    function checkedChanged(item) {
        changeVisibility(item.value);
    }

    function changeVisibility(type) {
        var showEmail = (type == 'EMAIL') ? 'block' : 'none';
        var showFax = (type == 'FAX') ? 'block' : 'none';

        //get controls to hide/show
        var email = $get('EmailSpan');
        var fax = $get('FaxSpan');

        email.style.display = showEmail;
        fax.style.display = showFax;

        var emailValidator = document.getElementById('<%= CSBExportDetailView.FindControl("Email").FindControl("Validator").ClientID %>');
        var faxValidator = document.getElementById('<%= CSBExportDetailView.FindControl("Fax").FindControl("Validator").ClientID %>');
                           
        if (emailValidator != null) 
            ValidatorEnable(emailValidator, (showEmail == 'block'));

        if (faxValidator != null) 
            ValidatorEnable(faxValidator, (showFax == 'block'));
    }

    function getRadWindow() {
        var popup = null;

        if (window.radWindow)
            popup = window.radWindow;
        else if (window.frameElement.radWindow)
            popup = window.frameElement.radWindow;

        return popup;
    }

    function Close(button, args) {
        var popup = getRadWindow();
        setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
    }

    function ToolBarClick(sender, args) {
        if (<%= IsInsertMode.ToString().ToLower() %>) {
            var button = args.get_item();
            var commandName = button.get_commandName();

            if (commandName == "cancel") {
                window.close();
            }
        }
    }

    function invokeDownload(processId) {
        var frame = document.createElement("frame");
        frame.src = getUrl('/CSB/Download/' + processId), null;
        frame.style.display = "none";
        document.body.appendChild(frame);
    }
</script>