﻿using System;

namespace WorkLinks.Admin.CSBExport
{
    public partial class CSBDownloadControl : WLP.Web.UI.WLPUserControl
    {
        protected String PayrollProcessId { get { return (String)Page.RouteData.Values["processId"]; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            byte[] output = (byte[])Session["HandOff"];
            Session.Remove("HandOff");

            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment;filename=CSB_" + String.Format("{0}.txt", PayrollProcessId));
            Response.OutputStream.Write(output, 0, output.Length);
            Response.Flush();
            Response.End();
        }
    }
}