﻿using System;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.CSBExport
{
    public partial class CSBExportControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private long PayrollProcessId { get { return Convert.ToInt64(Page.RouteData.Values["payrollProcessId"]); } }
        public bool IsViewMode { get { return CSBExportDetailView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool IsEditMode { get { return CSBExportDetailView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool IsInsertMode { get { return CSBExportDetailView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.CSBExport.UpdateFlag; } }
        public String RadioCodeValue
        {
            get
            {
                if (Data != null && Data.Count != 0)
                {
                    if (((CanadaRevenueAgencyBondExport)Data[0]).ContactFax != null && ((CanadaRevenueAgencyBondExport)Data[0]).ContactFax.ToString().Length > 0)
                        return "FAX";
                    else if (((CanadaRevenueAgencyBondExport)Data[0]).ContactEmail != null && ((CanadaRevenueAgencyBondExport)Data[0]).ContactEmail.ToString().Length > 0)
                        return "EMAIL";
                    else
                        return "EMAIL";
                }
                else
                    return "EMAIL";
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.CSBExport.ViewFlag);

            this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "CSBExport"));

            if (!IsPostBack)
            {
                CanadaRevenueAgencyBondExportCollection coll = new CanadaRevenueAgencyBondExportCollection();

                coll.Load(Common.ServiceWrapper.ReportClient.GetCanadaRevenueAgencyBondExport(PayrollProcessId));
                Data = coll;

                if (coll.Count > 0 && coll[0].CanadaRevenueAgencyBondExportId == -1)
                {
                    CSBExportDetailView.ChangeMode(FormViewMode.Edit);
                    CSBExportDetailView.DataBind();
                }
                else if (coll.Count == 0)
                {
                    coll.Add(new CanadaRevenueAgencyBondExport());
                    Data = coll;
                    CSBExportDetailView.ChangeMode(FormViewMode.Edit);
                    CSBExportDetailView.DataBind();
                }
                else
                    LoadUser();
            }
        }
        protected void LoadUser()
        {
            CSBExportDetailView.DataBind();
        }
        #endregion

        #region event handlers
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void CSBExportDetailView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            CSBExportDetailView.DataSource = Data;
        }
        protected void RadioButton_NeedDataSource(object sender, EventArgs e)
        {
            WLP.BusinessLayer.BusinessObjects.CodeCollection collection = new WLP.BusinessLayer.BusinessObjects.CodeCollection();
            collection.Add(new WLP.BusinessLayer.BusinessObjects.CodeObject() { Code = "EMAIL", Description = "Email" });
            collection.Add(new WLP.BusinessLayer.BusinessObjects.CodeObject() { Code = "FAX", Description = "Fax" });

            ((ICodeControl)sender).DataSource = collection;
        }
        protected void CSBExportToolBar_ButtonClick(object sender, Telerik.Web.UI.RadToolBarEventArgs e)
        {
            if (((Telerik.Web.UI.RadToolBarButton)(e.Item)).CommandName.ToLower().Equals("export"))
            {
                Session["HandOff"] = Common.ServiceWrapper.ReportClient.CreateCSBExportFile(PayrollProcessId) ?? new byte[0];
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "invokeDownload", "invokeDownload(" + PayrollProcessId + ");", true);

                RefreshPage();
            }
        }
        protected void ContactRadioButton_PreRender(object sender, EventArgs e)
        {
            RadioButtonControl control = (RadioButtonControl)sender;

            foreach (ListItem item in control.Items)
                item.Attributes.Add("onclick", "checkedChanged(this)");

            ChangeVisibility((RadioButtonControl)sender);
        }
        protected void EmailValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (CheckContactRadioButton())
                args.IsValid = true;
            else
            {
                args.IsValid = false;

                TextBoxControl EmailText = (TextBoxControl)CSBExportDetailView.FindControl("Email");
                if (EmailText != null)
                {
                    String email = EmailText.Value.ToString();

                    if (!String.IsNullOrEmpty(email))
                        args.IsValid = Regex.IsMatch(email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
                    else
                        args.IsValid = false;
                }
            }
        }
        #endregion

        #region handles updates
        protected void CSBExportDetailView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            if (Page.IsValid)
            {
                CanadaRevenueAgencyBondExport info = (CanadaRevenueAgencyBondExport)e.DataItem;
                info.PayrollProcessId = PayrollProcessId;

                SetContactInfo(info);

                try
                {
                    if (info.CanadaRevenueAgencyBondExportId == -1)
                        //do insert
                        Common.ServiceWrapper.ReportClient.InsertCanadaRevenueAgencySavingsBondExportData(info).CopyTo((CanadaRevenueAgencyBondExport)Data[info.Key]);
                    else
                        //do update
                        Common.ServiceWrapper.ReportClient.UpdateCanadaRevenueAgencySavingsBondExportData(info).CopyTo((CanadaRevenueAgencyBondExport)Data[info.Key]);
                }
                catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
                {
                    if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                        e.Cancel = true;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
                e.Cancel = true;
        }
        #endregion

        private bool CheckContactRadioButton()
        {
            bool disableEmailValidator = false;

            RadioButtonControl radio = (RadioButtonControl)CSBExportDetailView.FindControl("ContactRadioButton");

            if (radio.SelectedValue == "FAX")
                disableEmailValidator = true;

            return disableEmailValidator;
        }
        private void SetContactInfo(CanadaRevenueAgencyBondExport export)
        {
            RadioButtonControl radio = (RadioButtonControl)CSBExportDetailView.FindControl("ContactRadioButton");
            if (radio != null)
            {
                if (radio.SelectedValue == "EMAIL")
                    export.ContactFax = null;
                else if (radio.SelectedValue == "FAX")
                    export.ContactEmail = null;
            }
        }
        protected void ChangeVisibility(RadioButtonControl list)
        {
            ChangeVisibility(list.SelectedValue);
        }
        protected void ChangeVisibility(String selectedValue)
        {
            System.Web.UI.ClientScriptManager manager = Page.ClientScript;
            manager.RegisterStartupScript(this.GetType(), "initialize", String.Format("changeVisibility('{0}');", selectedValue), true);
        }
        private void RefreshPage()
        {
            CanadaRevenueAgencyBondExportCollection coll = new CanadaRevenueAgencyBondExportCollection();
            coll.Load(Common.ServiceWrapper.ReportClient.GetCanadaRevenueAgencyBondExport(PayrollProcessId));
            Data = coll;

            CSBExportDetailView.DataBind();
        }
    }
}