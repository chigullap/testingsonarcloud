﻿using System;
using System.Web.UI;
using Telerik.Web.UI;
using WLP.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Common;

namespace WorkLinks.Admin.PayRegisterExportAdmin
{
    public partial class PayRegisterExportControl : WLPUserControl
    {
        #region properties
        public bool IsViewMode { get { return PayRegisterExportGrid.IsViewMode; } }
        public bool IsUpdate { get { return PayRegisterExportGrid.IsEditMode; } }
        public bool IsInsert { get { return PayRegisterExportGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.PayRegisterExport.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.PayRegisterExport.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.PayRegisterExport.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.PayRegisterExport.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                LoadPayRegisterExportInformation();
                Initialize();
            }
        }

        protected void WireEvents()
        {
            PayRegisterExportGrid.NeedDataSource += new GridNeedDataSourceEventHandler(PayRegisterExportGrid_NeedDataSource);
            PayRegisterExportGrid.ItemDataBound += PayRegisterExportGrid_ItemDataBound;
        }

        void PayRegisterExportGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                if (IsInsert)
                {
                    CheckBoxControl activeFlagBox = ((CheckBoxControl)e.Item.FindControl("ActiveFlag"));
                    activeFlagBox.Checked = true;
                }
            }
        }

        protected void Initialize()
        {
            //Find the PayRegisterExportGrid
            WLPGrid grid = (WLPGrid)this.FindControl("PayRegisterExportGrid");

            //hide the edit/delete images in the rows.
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }

        protected void LoadPayRegisterExportInformation()
        {
            Data = PayRegisterExportCollection.ConvertCollection(ServiceWrapper.HumanResourcesClient.GetPayRegisterExport(null, null));
        }
        #endregion

        #region event handlers
        void PayRegisterExportGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            PayRegisterExportGrid.DataSource = Data;
        }

        protected void Code_PopulateComboBoxWithExport(object sender, EventArgs e)
        {
            CodeHelper.PopulateComboBoxWithExport((ICodeControl)sender);
        }

        protected void Code_PopulateComboBoxWithExportFtp(object sender, EventArgs e)
        {
            CodeHelper.PopulateComboBoxWithExportFtp((ICodeControl)sender);
        }
        #endregion

        #region handle updates
        protected void PayRegisterExportGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                ServiceWrapper.HumanResourcesClient.DeletePayRegisterExport((PayRegisterExport)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void PayRegisterExportGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            PayRegisterExport export = (PayRegisterExport)e.Item.DataItem;
            ServiceWrapper.HumanResourcesClient.InsertPayRegisterExport(export).CopyTo((PayRegisterExport)Data[export.Key]);
        }

        protected void PayRegisterExportGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                PayRegisterExport export = (PayRegisterExport)e.Item.DataItem;
                ServiceWrapper.HumanResourcesClient.UpdatePayRegisterExport(export).CopyTo((PayRegisterExport)Data[export.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}