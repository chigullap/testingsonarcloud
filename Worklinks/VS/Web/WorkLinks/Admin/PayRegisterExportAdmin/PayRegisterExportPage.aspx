﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PayRegisterExportPage.aspx.cs" Inherits="WorkLinks.Admin.PayRegisterExportAdmin.PayRegisterExportPage" ResourceName="PayRegisterExport" %>
<%@ Register Src="PayRegisterExportControl.ascx" TagName="PayRegisterExportControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:PayRegisterExportControl ID="PayRegisterExportControl1" runat="server" />
</asp:Content>