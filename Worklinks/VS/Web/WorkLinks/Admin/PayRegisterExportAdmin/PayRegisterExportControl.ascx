﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayRegisterExportControl.ascx.cs" Inherits="WorkLinks.Admin.PayRegisterExportAdmin.PayRegisterExportControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPGrid
        ID="PayRegisterExportGrid"
        runat="server"
        GridLines="None"
        AutoGenerateColumns="false"
        AllowPaging="true"
        PagerStyle-AlwaysVisible="true"
        PageSize="100"
        OnDeleteCommand="PayRegisterExportGrid_DeleteCommand"
        OnInsertCommand="PayRegisterExportGrid_InsertCommand"
        OnUpdateCommand="PayRegisterExportGrid_UpdateCommand"
        AutoAssignModifyProperties="true">

        <ClientSettings EnablePostBackOnRowClick="false" AllowColumnsReorder="true" ReorderColumnsOnClient="true">
            <Selecting AllowRowSelect="false" />
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="ExportId" SortOrder="Ascending" />
            </SortExpressions>

            <CommandItemTemplate>
                <wasp:WLPToolBar ID="PayRegisterExportToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
                    <Items>
                        <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag%>' ResourceName="Add" />
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
                <wasp:GridKeyValueControl OnNeedDataSource="Code_PopulateComboBoxWithExport" LabelText="Export" DataField="ExportId" Type="ExportId" ResourceName="ExportId" />
                <wasp:GridKeyValueControl OnNeedDataSource="Code_PopulateComboBoxWithExportFtp" LabelText="Export FTP" DataField="ExportFtpId" Type="ExportFtpId" ResourceName="ExportFtpId" />
                <wasp:GridCheckBoxControl DataField="ActiveFlag" LabelText="Active" SortExpression="ActiveFlag" UniqueName="ActiveFlag" ResourceName="ActiveFlag" />
                <wasp:GridCheckBoxControl DataField="UnzipFlag" LabelText="UnzipFlag" SortExpression="UnzipFlag" UniqueName="UnzipFlag" ResourceName="UnzipFlag" />
                <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning%>" />
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="ExportId" runat="server" OnDataBinding="Code_PopulateComboBoxWithExport" ResourceName="ExportIdCombo" Value='<%# Bind("ExportId") %>' Mandatory="true" ReadOnly="false" TabIndex="010" />
                            </td>
                            <td>
                                <wasp:CheckBoxControl ID="ActiveFlag" runat="server" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>' ReadOnly="false" TabIndex="030" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="ExportFtpId" runat="server" OnDataBinding="Code_PopulateComboBoxWithExportFtp" ResourceName="ExportFtpIdCombo" Value='<%# Bind("ExportFtpId") %>' Mandatory="false" ReadOnly="false" TabIndex="020" />
                            </td>
                            <td>
                                <wasp:CheckBoxControl ID="UnzipFlag" LabelText="UnzipFlag" runat="server" ResourceName="UnzipFlag" Value='<%# Bind("UnzipFlag") %>' ReadOnly="false" TabIndex="040" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                            <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsInsert %>' ResourceName="Insert" />
                                        </td>
                                        <td>
                                            <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="Cancel" CausesValidation="false" ResourceName="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>

        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true" />

    </wasp:WLPGrid>
</asp:Panel>