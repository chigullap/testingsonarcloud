﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalEmployeePaycodeControl.ascx.cs" Inherits="WorkLinks.Admin.GlobalEmployeePaycode.GlobalEmployeePaycodeControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="GlobalEmployeePaycodeDeduction.ascx" TagName="GlobalEmployeePaycodeDeduction" TagPrefix="uc1" %>
<%@ Register Src="GlobalEmployeePaycodeBenefit.ascx" TagName="GlobalEmployeePaycodeBenefit" TagPrefix="uc2" %>
<%@ Register Src="GlobalEmployeePaycodeIncome.ascx" TagName="GlobalEmployeePaycodeIncome" TagPrefix="uc3" %>

<wasp:WLPAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="PaycodePanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="PaycodePanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</wasp:WLPAjaxManagerProxy>

<asp:Panel runat="server" ID="PaycodePanel">
    <wasp:WLPGrid
        ID="GlobalEmployeePaycodeGrid"
        runat="server"
        GridLines="None"
        AutoAssignModifyProperties="true"
        AutoGenerateColumns="false"
        OnInsertCommand="GlobalEmployeePaycodeGrid_InsertCommand"
        OnUpdateCommand="GlobalEmployeePaycodeGrid_UpdateCommand"
        OnDeleteCommand="GlobalEmployeePaycodeGrid_DeleteCommand"
        OnDataBound="GlobalEmployeePaycodeGrid_DataBound"
        OnNeedDataSource="GlobalEmployeePaycodeGrid_NeedDataSource"
        OnSelectedIndexChanged="GlobalEmployeePaycodeGrid_SelectedIndexChanged">

        <ClientSettings EnablePostBackOnRowClick="true">
            <Selecting AllowRowSelect="true" />
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
            <RowIndicatorColumn>
                <HeaderStyle Width="20px" />
            </RowIndicatorColumn>

            <ExpandCollapseColumn>
                <HeaderStyle Width="20px" />
            </ExpandCollapseColumn>

            <SortExpressions>
                <telerik:GridSortExpression FieldName="PaycodeTypeCode, PaycodeCode" SortOrder="Ascending" />
            </SortExpressions>

            <CommandItemTemplate>
                <wasp:WLPToolBar ID="GlobalEmployeePaycodeToolBar" runat="server" AutoPostBack="true" Width="100%" OnButtonClick="GlobalEmployeePaycodeToolBar_ButtonClick" Enabled="<%# IsViewMode && AddFlag %>">
                    <Items>
                        <wasp:WLPToolBarDropDown runat="server" ImageUrl="~/App_Themes/Default/Add.gif" Text="**Add**" ResourceName="Add" OnPreRender="AddDropDown_PreRender">
                            <Buttons>
                                <wasp:WLPToolBarButton runat="server" Text="**Income**" ResourceName="AddIncome" CommandName="AddIncome" Visible="<%# IsRecurringIncome %>" />
                                <wasp:WLPToolBarButton runat="server" Text="**Deduction**" ResourceName="AddDeduction" CommandName="AddDeduction" />
                                <wasp:WLPToolBarButton runat="server" Text="**Benefit**" ResourceName="AddBenefit" CommandName="AddBenefit" />
                            </Buttons>
                        </wasp:WLPToolBarDropDown>
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridKeyValueControl DataField="PaycodeCode" LabelText="**PaycodeCode**" OnNeedDataSource="PaycodeCode_NeedDataSource" Type="PaycodeCode" ResourceName="PaycodeCode">
                    <HeaderStyle Width="350px" />
                </wasp:GridKeyValueControl>
                <wasp:GridKeyValueControl DataField="PaycodeTypeCode" LabelText="**PaycodeTypeCode**" OnNeedDataSource="Code_NeedDataSource" Type="PaycodeTypeCode" ResourceName="PaycodeTypeCode">
                    <HeaderStyle Width="100px" />
                </wasp:GridKeyValueControl>
                <wasp:GridNumericControl DataField="AmountRate" LabelText="**AmountRate**" DataFormatString="{0:$###,##0.00}" SortExpression="AmountRate" UniqueName="AmountRate" DataType="System.Int64" ResourceName="AmountRate" />
                <wasp:GridNumericControl DataField="AmountPercentage" LabelText="**AmountPercentage**" DataFormatString="{0:#####0.0000}" SortExpression="AmountPercentage" UniqueName="AmountPercentage" DataType="System.Int64" ResourceName="AmountPercentage" />
                <wasp:GridNumericControl DataField="YearlyMaximum" LabelText="**YearlyMaximum**" DataFormatString="{0:$###,##0.00}" SortExpression="YearlyMaximum" UniqueName="YearlyMaximum" DataType="System.Int64" ResourceName="YearlyMaximum" />
                <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>">
                     <HeaderStyle Width="50px" />
                </wasp:GridButtonControl>
            </Columns>
        </MasterTableView>

    </wasp:WLPGrid>

    <telerik:RadMultiPage ID="RadMultiPage1" runat="server" Height="350px">
        <telerik:RadPageView ID="DeductionPageView" runat="server">
            <uc1:GlobalEmployeePaycodeDeduction ID="GlobalEmployeePaycodeDeduction" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="BenefitPageView" runat="server">
            <uc2:GlobalEmployeePaycodeBenefit ID="GlobalEmployeePaycodeBenefit" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="IncomePageView" runat="server">
            <uc3:GlobalEmployeePaycodeIncome ID="GlobalEmployeePaycodeIncome" runat="server" />
        </telerik:RadPageView>
    </telerik:RadMultiPage>
</asp:Panel>