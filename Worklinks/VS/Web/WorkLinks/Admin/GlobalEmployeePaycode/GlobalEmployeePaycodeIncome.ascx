﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalEmployeePaycodeIncome.ascx.cs" Inherits="WorkLinks.Admin.GlobalEmployeePaycode.GlobalEmployeePaycodeIncome" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPFormView
    ID="GlobalEmployeePaycodeIncomeView"
    runat="server"
    OnNeedDataSource="GlobalEmployeePaycodeIncomeView_NeedDataSource"
    OnInserting="GlobalEmployeePaycodeIncomeView_Inserting"
    OnUpdating="GlobalEmployeePaycodeIncomeView_Updating"
    DataKeyNames="Key"
    Width="100%">

    <ItemTemplate>
        <wasp:WLPToolBar ID="EmployeDetailToolBarItem" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="EmployeeDetailToolBar_ButtonClick">
            <Items>
                <wasp:WLPToolBarButton Visible='<%# UpdateFlag %>' Text="Edit" ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="edit" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PaycodeCodeItem" runat="server" Type="PaycodeCode" OnDataBinding="PaycodeCode_NeedDataSource" ResourceName="PaycodeCode" Value='<%# Eval("PaycodeCode") %>' ReadOnly="true" Mandatory="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="CodePaycodeTypeCdItem" runat="server" Type="PaycodeTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="CodePaycodeTypeCd" Value='<%# Eval("PaycodeTypeCode") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="AmountRateItem" runat="server" ResourceName="AmountRate" Value='<%# Eval("AmountRate") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="AmountUnitsItem" runat="server" MaxLength="12" DecimalDigits="2" ResourceName="AmountUnits" Value='<%# Eval("AmountUnits") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="AmountPercentageItem" runat="server" MaxLength="12" DecimalDigits="4" ResourceName="AmountPercentage" Value='<%# Eval("AmountPercentage") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="PayPeriodMinimumItem" runat="server" ResourceName="PayPeriodMinimum" Value='<%# Eval("PayPeriodMinimum") %>' ReadOnly="true" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="ExcludeAmountItem" runat="server" MaxLength="13" DecimalDigits="2" ResourceName="ExcludeAmount" Value='<%# Eval("ExcludeAmount") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="PayPeriodMaximumItem" runat="server" ResourceName="PayPeriodMaximum" Value='<%# Eval("PayPeriodMaximum") %>' ReadOnly="true" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="ExcludePercentageItem" runat="server" MaxLength="12" DecimalDigits="4" ResourceName="ExcludePercentage" Value='<%# Eval("ExcludePercentage") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="MonthlyMaximumItem" runat="server" ResourceName="MonthlyMaximum" Value='<%# Eval("MonthlyMaximum") %>' ReadOnly="true" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="GroupIncomeFactorItem" runat="server" MaxLength="13" DecimalDigits="4" ResourceName="GroupIncomeFactor" Value='<%# Eval("GroupIncomeFactor") %>' ReadOnly="true" Visible="false" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="YearlyMaximumItem" runat="server" ResourceName="YearlyMaximum" Value='<%# Eval("YearlyMaximum") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="RoundUpToItem" runat="server" MaxLength="8" DecimalDigits="0" ResourceName="RoundUpTo" Value='<%# Eval("RoundUpTo") %>' ReadOnly="true" Visible="false" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="RatePerRoundUpToItem" runat="server" MaxLength="17" DecimalDigits="4" ResourceName="RatePerRoundUpTo" Value='<%# Eval("RatePerRoundUpTo") %>' ReadOnly="true" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="StartDateItem" runat="server" ResourceName="StartDate" Value='<%# Eval("StartDate") %>' ReadOnly="true" Visible="false" />
                    </td>
                    <td>
                        <wasp:DateControl ID="CutoffDateItem" runat="server" ResourceName="CutoffDate" Value='<%# Eval("CutoffDate") %>' ReadOnly="true" Visible="false" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar ID="EmployeeDetailToolBar" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="EmployeeDetailToolBar_ButtonClick">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert" />
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update" />
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CausesValidation="false" CommandName="cancel" ResourceName="Cancel" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:WLPLabel ID="Message" runat="server" CssClass="failureNotification" Text='<%# PaycodeDataValidateMsg %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PaycodeCode" runat="server" Type="PaycodeCode" OnDataBinding="PaycodeCode_DataBinding" ResourceName="PaycodeCode" Value='<%# Bind("PaycodeCode") %>' ReadOnly='<%# IsEditMode || IsViewMode %>' Mandatory="true" TabIndex="010" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="CodePaycodeTypeCd" runat="server" Type="PaycodeTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="CodePaycodeTypeCd" Value='<%# Bind("PaycodeTypeCode") %>' ReadOnly="true" TabIndex="020" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="AmountRate" runat="server" ResourceName="AmountRate" Value='<%# Bind("AmountRate") %>' TabIndex="030" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="AmountUnits" runat="server" ResourceName="AmountUnits" Value='<%# Bind("AmountUnits") %>' TabIndex="040" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="AmountPercentage" runat="server" MaxLength="12" DecimalDigits="4" ResourceName="AmountPercentage" Value='<%# Bind("AmountPercentage") %>' TabIndex="050" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="PayPeriodMinimum" runat="server" ResourceName="PayPeriodMinimum" Value='<%# Bind("PayPeriodMinimum") %>' TabIndex="060" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="ExcludeAmount" runat="server" MaxLength="13" DecimalDigits="2" ResourceName="ExcludeAmount" Value='<%# Bind("ExcludeAmount") %>' TabIndex="070" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="PayPeriodMaximum" runat="server" ResourceName="PayPeriodMaximum" Value='<%# Bind("PayPeriodMaximum") %>' TabIndex="080" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="ExcludePercentage" runat="server" MaxLength="12" DecimalDigits="4" ResourceName="ExcludePercentage" Value='<%# Bind("ExcludePercentage") %>' TabIndex="090" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="MonthlyMaximum" runat="server" ResourceName="MonthlyMaximum" Value='<%# Bind("MonthlyMaximum") %>' TabIndex="100" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="GroupIncomeFactor" runat="server" MaxLength="13" DecimalDigits="4" ResourceName="GroupIncomeFactor" Value='<%# Bind("GroupIncomeFactor") %>' TabIndex="110" Visible="false" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="YearlyMaximum" runat="server" ResourceName="YearlyMaximum" Value='<%# Bind("YearlyMaximum") %>' TabIndex="120" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="RoundUpTo" runat="server" MaxLength="8" DecimalDigits="0" ResourceName="RoundUpTo" Value='<%# Bind("RoundUpTo") %>' TabIndex="130" Visible="false" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="RatePerRoundUpTo" runat="server" MaxLength="17" DecimalDigits="4" ResourceName="RatePerRoundUpTo" Value='<%# Bind("RatePerRoundUpTo") %>' TabIndex="140" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="StartDate" runat="server" ResourceName="StartDate" Value='<%# Bind("StartDate") %>' Visible="false" TabIndex="050" />
                    </td>
                    <td>
                        <wasp:DateControl ID="CutoffDate" runat="server" ResourceName="CutoffDate" Value='<%# Bind("CutoffDate") %>' Visible="false" TabIndex="060" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>

</wasp:WLPFormView>