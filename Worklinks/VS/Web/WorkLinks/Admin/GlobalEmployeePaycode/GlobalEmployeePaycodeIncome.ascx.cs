﻿using System;
using System.Web.UI.WebControls;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.GlobalEmployeePaycode
{
    public partial class GlobalEmployeePaycodeIncome : WLP.Web.UI.WLPUserControl
    {
        public event EmployeeDetailToolbarButtonClickEventHandler EmployeeDetailToolbarButtonClick;

        #region fields
        private const String _employeePaycodeTypeCode = "1";
        private String _paycodeDataValidateMsg = "";
        #endregion

        #region properties
        public EmployeePaycode EmployeePaycode
        {
            get
            {
                EmployeePaycode paycode = null;

                if (Data != null && Data.Count > 0)
                    paycode = (EmployeePaycode)Data[0];

                return paycode;
            }
        }
        public new IDataItemCollection<IDataItem> Data
        {
            get
            {
                return base.Data;
            }
            set
            {
                if (IsInsertMode)
                {
                    foreach (EmployeePaycode paycode in value)
                        paycode.PaycodeTypeCode = _employeePaycodeTypeCode;
                }

                base.Data = value;
            }
        }
        public String PaycodeDataValidateMsg
        {
            get { return _paycodeDataValidateMsg; }
            set { _paycodeDataValidateMsg = value; }
        }
        public long EmployeePaycodeId { get { return EmployeePaycode.EmployeePaycodeId; } }
        public bool IsGlobalEmployeePaycode { get { return EmployeePaycode.GlobalEmployeePaycodeFlag; } }
        public bool IsViewMode { get { return GlobalEmployeePaycodeIncomeView.CurrentMode == FormViewMode.ReadOnly; } }
        public bool IsInsertMode { get { return GlobalEmployeePaycodeIncomeView.CurrentMode == FormViewMode.Insert; } }
        public bool IsEditMode { get { return GlobalEmployeePaycodeIncomeView.CurrentMode == FormViewMode.Edit; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.GlobalEmployeePaycode.UpdateFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        public void ChangeModeInsert()
        {
            GlobalEmployeePaycodeIncomeView.ChangeMode(FormViewMode.Insert);
            GlobalEmployeePaycodeIncomeView.DataBind();
        }
        #endregion

        #region event handlers
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void PaycodeCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulatePaycodes((ICodeControl)sender, LanguageCode);
        }
        protected void PaycodeCode_DataBinding(object sender, EventArgs e)
        {
            if (sender is ComboBoxControl)
            {
                if (IsInsertMode)
                    Common.CodeHelper.PopulateComboBoxWithRecurringIncomePaycodesRemovingGlobalPaycodesInUse((ComboBoxControl)sender, _employeePaycodeTypeCode, true);
                else
                    Common.CodeHelper.PopulateComboBoxWithPaycodesByType((ComboBoxControl)sender, _employeePaycodeTypeCode);

                BindCodePayCodeTypeCd(sender);
            }
        }
        private void BindCodePayCodeTypeCd(object sender)
        {
            if (sender is ComboBoxControl)
            {
                ComboBoxControl paycodeCombo = (ComboBoxControl)sender;
                WLPFormView formItem = paycodeCombo.BindingContainer as WLPFormView;
                ComboBoxControl paycodeTypeCombo = (ComboBoxControl)formItem.FindControl("CodePaycodeTypeCd");

                paycodeTypeCombo.DataSource = Common.ServiceWrapper.CodeClient.GetPaycodeTypeCode(paycodeCombo.SelectedValue);
                paycodeTypeCombo.DataBind();

                if (formItem.DataItem != null && formItem.DataItem is CodePaycode)
                    paycodeTypeCombo.Value = 1;
            }
        }
        protected void GlobalEmployeePaycodeIncomeView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            OnNeedDataSource(e);
            GlobalEmployeePaycodeIncomeView.DataSource = Data;
        }
        protected virtual void OnEmployeeDetailToolbarButtonClick(object sender, Telerik.Web.UI.RadToolBarEventArgs e)
        {
            if (EmployeeDetailToolbarButtonClick != null)
                EmployeeDetailToolbarButtonClick(sender, e);
        }
        protected void EmployeeDetailToolBar_ButtonClick(object sender, Telerik.Web.UI.RadToolBarEventArgs e)
        {
            OnEmployeeDetailToolbarButtonClick(sender, e);
        }
        #endregion

        #region handles updates
        protected void GlobalEmployeePaycodeIncomeView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            if (Common.ServiceWrapper.HumanResourcesClient.ValidatePaycodeEntryData((EmployeePaycode)e.DataItem))
            {
                PaycodeDataValidateMsg = "";
                OnInserting(e);
            }
            else
            {
                PaycodeDataValidateMsg = String.Format("{0}", GetGlobalResourceObject("ErrorMessages", "PaycodeDataValidate"));
                e.Cancel = true;
            }
        }
        protected void GlobalEmployeePaycodeIncomeView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            if (Common.ServiceWrapper.HumanResourcesClient.ValidatePaycodeEntryData((EmployeePaycode)e.DataItem))
            {
                PaycodeDataValidateMsg = "";
                OnUpdating(e);
            }
            else
            {
                PaycodeDataValidateMsg = String.Format("{0}", GetGlobalResourceObject("ErrorMessages", "PaycodeDataValidate"));
                e.Cancel = true;
            }
        }
        #endregion
    }
}