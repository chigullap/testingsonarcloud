﻿using System;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.GlobalEmployeePaycode
{
    public delegate void EmployeeDetailToolbarButtonClickEventHandler(object sender, RadToolBarEventArgs e);

    public partial class GlobalEmployeePaycodeControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private int _rowIndex = -1;
        private string _rowIndexKey = "RowIndexKey";
        private bool _updatePerformed = false;
        #endregion

        #region properties
        protected EmployeePaycode CurrentEmployeePaycode
        {
            get
            {
                EmployeePaycode paycode = null;

                if (IsInsertMode && !InsertComplete)
                    paycode = new EmployeePaycode() { PaycodeCode = String.Empty };
                else
                {
                    if (GlobalEmployeePaycodeGrid.SelectedValue != null)
                        paycode = (EmployeePaycode)Data[GlobalEmployeePaycodeGrid.SelectedValue.ToString()];
                }

                return paycode;
            }
        }
        protected EmployeePaycodeCollection CurrentEmployeePaycodeCollection
        {
            get
            {
                EmployeePaycodeCollection collection = new EmployeePaycodeCollection();

                if (CurrentEmployeePaycode != null)
                    collection.Load(CurrentEmployeePaycode);

                return collection;
            }
        }
        public int RowIndex
        {
            get
            {
                if (_rowIndex == -1)
                {
                    Object obj = ViewState[_rowIndexKey];
                    if (obj != null)
                        _rowIndex = (int)obj;
                }

                return _rowIndex;
            }
            set
            {
                _rowIndex = value;
                ViewState[_rowIndexKey] = _rowIndex;
            }
        }
        public bool IsInsertMode { get { return GlobalEmployeePaycodeBenefit.IsInsertMode || GlobalEmployeePaycodeDeduction.IsInsertMode || GlobalEmployeePaycodeIncome.IsInsertMode; } }
        public bool IsViewMode { get { return GlobalEmployeePaycodeBenefit.IsViewMode && GlobalEmployeePaycodeIncome.IsViewMode && GlobalEmployeePaycodeDeduction.IsViewMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.GlobalEmployeePaycode.AddFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.GlobalEmployeePaycode.DeleteFlag; } }
        public bool IsRecurringIncome { get { return Common.CodeHelper.CheckRecurringIncomeCode(); } }
        public bool InsertComplete { get; set; }
        public bool CancelClicked { get; set; }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.GlobalEmployeePaycode.ViewFlag);

            if (!IsPostBack)
            {
                LoadGlobalEmployeePaycode();
                Initialize();
            }

            WireEvents();
        }
        private void LoadGlobalEmployeePaycode()
        {
            Data = EmployeePaycodeCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetGlobalEmployeePaycode(null));
        }
        protected void Initialize()
        {
            //find the GlobalEmployeePaycodeGrid
            WLPGrid grid = (WLPGrid)this.FindControl("GlobalEmployeePaycodeGrid");

            //hide the delete image in the rows
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        private void WireEvents()
        {
            GlobalEmployeePaycodeIncome.NeedDataSource += new NeedDataSourceEventHandler(LoadableControl_NeedDataSource);
            GlobalEmployeePaycodeIncome.Inserting += new InsertingEventHandler(LoadableControl_Inserting);
            GlobalEmployeePaycodeIncome.Updating += new UpdatingEventHandler(ILoadableControl_Updating);
            GlobalEmployeePaycodeIncome.EmployeeDetailToolbarButtonClick += new EmployeeDetailToolbarButtonClickEventHandler(LoadableControl_EmployeeDetailToolbarButtonClick);

            GlobalEmployeePaycodeBenefit.NeedDataSource += new NeedDataSourceEventHandler(LoadableControl_NeedDataSource);
            GlobalEmployeePaycodeBenefit.Inserting += new InsertingEventHandler(LoadableControl_Inserting);
            GlobalEmployeePaycodeBenefit.Updating += new UpdatingEventHandler(ILoadableControl_Updating);
            GlobalEmployeePaycodeBenefit.EmployeeDetailToolbarButtonClick += new EmployeeDetailToolbarButtonClickEventHandler(LoadableControl_EmployeeDetailToolbarButtonClick);

            GlobalEmployeePaycodeDeduction.NeedDataSource += new NeedDataSourceEventHandler(LoadableControl_NeedDataSource);
            GlobalEmployeePaycodeDeduction.Inserting += new InsertingEventHandler(LoadableControl_Inserting);
            GlobalEmployeePaycodeDeduction.Updating += new UpdatingEventHandler(ILoadableControl_Updating);
            GlobalEmployeePaycodeDeduction.EmployeeDetailToolbarButtonClick += new EmployeeDetailToolbarButtonClickEventHandler(LoadableControl_EmployeeDetailToolbarButtonClick);
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            InitializeControls();
        }
        protected void InitializeControls()
        {
            GlobalEmployeePaycodeGrid.Enabled = IsViewMode;

            //this code makes sure the RadToolbar is set to the proper status before/during/after update/insert operations
            GridCommandItem cmdItem = (GridCommandItem)GlobalEmployeePaycodeGrid.MasterTableView.GetItems(GridItemType.CommandItem)[0];
            RadToolBar toolBar = (RadToolBar)cmdItem.FindControl("GlobalEmployeePaycodeToolBar");
            toolBar.Enabled = IsViewMode;
        }
        private void GetSelectedEmployeePaycode()
        {
            if (GlobalEmployeePaycodeGrid.SelectedItems.Count > 0)
            {
                switch (CurrentEmployeePaycode.PaycodeTypeCode)
                {
                    case "1":
                        GlobalEmployeePaycodeIncome.Data = CurrentEmployeePaycodeCollection;
                        GlobalEmployeePaycodeIncome.DataBind();
                        RadMultiPage1.FindPageViewByID("IncomePageView").Selected = true;
                        break;
                    case "2":
                        GlobalEmployeePaycodeBenefit.Data = CurrentEmployeePaycodeCollection;
                        GlobalEmployeePaycodeBenefit.DataBind();
                        RadMultiPage1.FindPageViewByID("BenefitPageView").Selected = true;
                        break;
                    case "3":
                        GlobalEmployeePaycodeDeduction.Data = CurrentEmployeePaycodeCollection;
                        GlobalEmployeePaycodeDeduction.DataBind();
                        RadMultiPage1.FindPageViewByID("DeductionPageView").Selected = true;
                        break;
                }
            }
        }
        #endregion

        #region handles updates
        protected void GlobalEmployeePaycodeGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            EmployeePaycode data = (EmployeePaycode)e.Item.DataItem;
            Common.ServiceWrapper.HumanResourcesClient.InsertGlobalEmployeePaycode(data).CopyTo((EmployeePaycode)Data[data.Key]);
        }
        protected void GlobalEmployeePaycodeGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                EmployeePaycode data = (EmployeePaycode)e.Item.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.UpdateGlobalEmployeePaycode(data).CopyTo((EmployeePaycode)Data[data.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void GlobalEmployeePaycodeGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                EmployeePaycode data = (EmployeePaycode)e.Item.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.DeleteGlobalEmployeePaycode(data);

                GlobalEmployeePaycodeGrid.Rebind();

                //check type and rebind associated control
                switch (data.PaycodeTypeCode)
                {
                    case "1":
                        GlobalEmployeePaycodeIncome.DataBind();
                        break;
                    case "2":
                        GlobalEmployeePaycodeBenefit.DataBind();
                        break;
                    case "3":
                        GlobalEmployeePaycodeDeduction.DataBind();
                        break;
                    default:
                        break;
                }
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void LoadableControl_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            EmployeePaycode data = (EmployeePaycode)e.DataItem;
            ((DataItemCollection<EmployeePaycode>)Data).Add(Common.ServiceWrapper.HumanResourcesClient.InsertGlobalEmployeePaycode(data));

            InsertComplete = true;
            GlobalEmployeePaycodeGrid.Rebind();
        }
        void ILoadableControl_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                _updatePerformed = true;
                EmployeePaycode data = (EmployeePaycode)e.DataItem;

                Common.ServiceWrapper.HumanResourcesClient.UpdateGlobalEmployeePaycode(data).CopyTo(data);
                GlobalEmployeePaycodeGrid.Rebind();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region event handlers
        protected void GlobalEmployeePaycodeGrid_DataBound(object sender, EventArgs e)
        {
            //if we arrived here because the CancelClicked variable was set to true then set it back to false
            if (CancelClicked)
                CancelClicked = false;

            if (GlobalEmployeePaycodeGrid.Items.Count > 0)
            {
                if (!_updatePerformed)
                {
                    GlobalEmployeePaycodeGrid.Items[0].Selected = true;
                    RowIndex = 0;
                }
                else
                {
                    GlobalEmployeePaycodeGrid.Items[RowIndex].Selected = true;
                    _updatePerformed = false;
                }

                GetSelectedEmployeePaycode();
            }
        }
        protected void GlobalEmployeePaycodeGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            GlobalEmployeePaycodeGrid.DataSource = Data;
        }
        protected void GlobalEmployeePaycodeGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            //get the index of the item being edited; will only be one item
            GridIndexCollection rows = GlobalEmployeePaycodeGrid.ItemIndexes;
            RowIndex = Convert.ToInt16(rows[0]);

            GetSelectedEmployeePaycode();
        }
        protected void GlobalEmployeePaycodeToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            RadToolBarButton button = ((RadToolBarButton)e.Item);

            if (button.CommandName.Equals("AddIncome"))
            {
                RadMultiPage1.FindPageViewByID("IncomePageView").Selected = true;
                GlobalEmployeePaycodeIncome.ChangeModeInsert();
            }
            else if (button.CommandName.Equals("AddDeduction"))
            {
                RadMultiPage1.FindPageViewByID("DeductionPageView").Selected = true;
                GlobalEmployeePaycodeDeduction.ChangeModeInsert();
            }
            else if (button.CommandName.Equals("AddBenefit"))
            {
                RadMultiPage1.FindPageViewByID("BenefitPageView").Selected = true;
                GlobalEmployeePaycodeBenefit.ChangeModeInsert();
            }
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControlWithoutFiltering((ICodeControl)sender, LanguageCode);
        }
        protected void PaycodeCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulatePaycodes((ICodeControl)sender, LanguageCode);
        }
        protected void AddDropDown_PreRender(object sender, EventArgs e)
        {
            ((WLPToolBarDropDown)sender).Visible = ((WLPToolBarDropDown)sender).Visible && AddFlag;
        }
        void LoadableControl_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            if (CancelClicked && GlobalEmployeePaycodeGrid.Items.Count > 0)
                GlobalEmployeePaycodeGrid_DataBound(null, null);
            else
            {
                if (((WLP.Web.UI.WLPUserControl)sender).ID == "GlobalEmployeePaycodeIncome")
                    GlobalEmployeePaycodeIncome.Data = CurrentEmployeePaycodeCollection;
                else if (((WLP.Web.UI.WLPUserControl)sender).ID == "GlobalEmployeePaycodeBenefit")
                    GlobalEmployeePaycodeBenefit.Data = CurrentEmployeePaycodeCollection;
                else if (((WLP.Web.UI.WLPUserControl)sender).ID == "GlobalEmployeePaycodeDeduction")
                    GlobalEmployeePaycodeDeduction.Data = CurrentEmployeePaycodeCollection;
            }
        }
        void LoadableControl_EmployeeDetailToolbarButtonClick(object sender, RadToolBarEventArgs e)
        {
            RadToolBarButton button = ((RadToolBarButton)e.Item);

            switch (button.CommandName.ToLower())
            {
                case "cancel":
                    CancelClicked = true;
                    break;
                case "insert":
                case "performinsert":
                case "update":
                case "edit":
                case "initinsert":
                    break;
            }
        }
        #endregion
    }
}