﻿using System;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.FieldSecurity
{
    public partial class FieldSecurityControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private FieldEntityCollection _collection = new FieldEntityCollection();  //used to populate child grid
        private const String _attributeIdentifier = "Security";
        #endregion

        #region properties
        private long? SelectedFormId { get { return FormGrid.SelectedValue == null ? (long?)null : Convert.ToInt64(FormGrid.SelectedValue); } }
        private int RoleId { get { return Convert.ToInt16(Page.RouteData.Values["roleId"]); } }
        private string CommandName { get; set; }
        public bool IsViewMode { get { return FieldGrid.IsViewMode; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.RoleSecurityField.UpdateFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.RoleSecurityField.ViewFlag);

            this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "FieldSecurity"));

            WireEvents();

            if (!IsPostBack) LoadFormGridInfo();
        }
        protected void WireEvents()
        {
            FormGrid.NeedDataSource += new Telerik.Web.UI.GridNeedDataSourceEventHandler(FormGrid_NeedDataSource);
            FormGrid.SelectedIndexChanged += new EventHandler(FormGrid_SelectedIndexChanged);
            FieldGrid.NeedDataSource += new Telerik.Web.UI.GridNeedDataSourceEventHandler(FieldGrid_NeedDataSource);
            FieldGrid.UpdateAllCommand += new WLP.Web.UI.Controls.WLPGrid.UpdateAllCommandEventHandler(FieldGrid_UpdateAllCommand);
            FieldGrid.PreRender += new EventHandler(FieldGrid_PreRender);
        }
        protected void LoadFormGridInfo()
        {
            Data = FormDescriptionCollection.ConvertCollection(Common.ServiceWrapper.CodeClient.GetFormInfo(null));
        }
        #endregion

        #region event handlers
        void FieldGrid_PreRender(object sender, EventArgs e)
        {
            foreach (Telerik.Web.UI.GridItem item in FieldGrid.MasterTableView.Items)
            {
                WLP.Web.UI.Controls.ComboBoxControl control = (WLP.Web.UI.Controls.ComboBoxControl)item.FindControl("FieldValue");
                control.ReadOnly = IsViewMode;
            }
        }
        void FormGrid_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            FormGrid.DataSource = Data;
        }
        protected void FormGrid_DataBound(object sender, EventArgs e)
        {
            if (FormGrid.Items.Count > 0) FormGrid.Items[0].Selected = true;
        }
        protected void FormGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            FieldGrid.Rebind();
        }
        void FieldGrid_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (SelectedFormId != null)
            {
                _collection.Load(Common.ServiceWrapper.CodeClient.GetFieldInfo(Convert.ToDecimal(SelectedFormId), _attributeIdentifier, RoleId));
                FieldGrid.DataSource = _collection;
            }
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((WLP.Web.UI.Controls.ICodeControl)sender, LanguageCode);
        }
        void FieldGrid_UpdateAllCommand(object sender, WLP.Web.UI.Controls.UpdateAllCommandEventArgs e)
        {
            // Pass the collection to WS, determine if updates or inserts are needed, then return collection.
            FieldEntityCollection collection = new FieldEntityCollection();

            foreach (FieldEntity entity in e.Items)
                collection.Add(entity);

            Common.ServiceWrapper.CodeClient.UpdateFieldEntity(collection.ToArray());
            Common.Resources.DatabaseResourceCache.ClearCache();
        }
        #endregion
    }
}