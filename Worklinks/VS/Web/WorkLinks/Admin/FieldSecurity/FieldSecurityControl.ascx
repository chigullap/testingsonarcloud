﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FieldSecurityControl.ascx.cs" Inherits="WorkLinks.Admin.FieldSecurity.FieldSecurityControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<style>
    div.form_control span.label { width: 0 !important; }
</style>

<!-- Ajax proxy -->
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="FormGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="FieldGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy2" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="FieldGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="FormGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<!-- Parent grid -->
<wasp:WLPGrid
    ID="FormGrid"
    runat="server"
    GridLines="None"
    Height="210px"
    AutoGenerateColumns="false"
    AutoInsertOnAdd="false"
    OnDataBound="FormGrid_DataBound"
    AllowMultiRowEdit="true">

    <ClientSettings EnablePostBackOnRowClick="true" AllowKeyboardNavigation="true">
        <Selecting AllowRowSelect="true" />
        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="FormId">
        <CommandItemTemplate></CommandItemTemplate>
        <Columns>
            <wasp:GridBoundControl DataField="FormDescriptionField" LabelText="**FormName" SortExpression="FormDescriptionField" UniqueName="FormDescriptionField" ResourceName="FormDescriptionField" />
        </Columns>
    </MasterTableView>

    <HeaderContextMenu EnableAutoScroll="true" />

</wasp:WLPGrid>

<!-- Child grid -->
<wasp:WLPGrid
    ID="FieldGrid"
    EditMode="InPlace"
    runat="server"
    GridLines="None"
    AutoGenerateColumns="false"
    AutoInsertOnAdd="false"
    AllowMultiRowEdit="true"
    Skin=""
    Height="340px">

    <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
        <Selecting AllowRowSelect="false" />
        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
        <CommandItemTemplate>
            <wasp:WLPToolBar ID="FieldGridToolBar" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="**Edit" ImageUrl="~/App_Themes/Default/edit.gif" CommandName="EditAll" Visible='<%# IsViewMode && UpdateFlag %>' CausesValidation="true" ResourceName="Edit" />
                    <wasp:WLPToolBarButton Text="**UpdateAll" ImageUrl="~/App_Themes/Default/update.gif" CommandName="UpdateAll" Visible='<%# !IsViewMode %>' CausesValidation="true" ResourceName="Update" />
                    <wasp:WLPToolBarButton Text="**Cancel" ImageUrl="~/App_Themes/Default/cancel.gif" CommandName="CancelAll" Visible='<%# !IsViewMode %>' ResourceName="Cancel" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>
        <Columns>
            <wasp:GridTemplateControl UniqueName="HeaderColumns" Reorderable="false">
                <HeaderTemplate>
                    <table width="100%">
                        <tr>
                            <td align="left" width="30%">Option</td>
                            <td align="left" width="70%">Name</td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table width="100%">
                        <tr>
                            <td width="30%">
                                <wasp:ComboBoxControl ID="FieldValue" HideLabel="true" IncludeEmptyItem="false" Text="Field Value**" runat="server" Type="FieldSecurityTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="FieldValue" Value='<%# Bind("FieldValue") %>' />
                            </td>
                            <td width="70%">
                                <wasp:WLPLabel ID="FieldIdentifier" runat="server" Text='<%# Bind("FieldIdentifier") %>' />
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <EditItemTemplate>
                    <table width="100%">
                        <tr>
                            <td width="30%">
                                <wasp:ComboBoxControl ID="FieldValue" HideLabel="true" IncludeEmptyItem="false" Text="Field Value**" runat="server" Type="FieldSecurityTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="FieldValue" Value='<%# Bind("FieldValue") %>' />
                            </td>
                            <td width="70%">
                                <wasp:WLPLabel ID="FieldIdentifier" runat="server" Text='<%# Bind("FieldIdentifier") %>' />
                            </td>
                        </tr>
                    </table>
                </EditItemTemplate>
            </wasp:GridTemplateControl>
        </Columns>
    </MasterTableView>

    <HeaderContextMenu EnableAutoScroll="true" />

</wasp:WLPGrid>