﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportExportControl.ascx.cs" Inherits="WorkLinks.Admin.ReportExportAdmin.ReportExportControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPGrid
        ID="ReportExportGrid"
        runat="server"
        GridLines="None"
        AutoGenerateColumns="false"
        AllowPaging="true"
        PagerStyle-AlwaysVisible="true"
        PageSize="100"
        OnDeleteCommand="ReportExportGrid_DeleteCommand"
        OnInsertCommand="ReportExportGrid_InsertCommand"
        OnUpdateCommand="ReportExportGrid_UpdateCommand"
        AutoAssignModifyProperties="true">

        <ClientSettings EnablePostBackOnRowClick="false" AllowColumnsReorder="true" ReorderColumnsOnClient="true">
            <Selecting AllowRowSelect="false" />
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="Name" SortOrder="Ascending" />
            </SortExpressions>

            <CommandItemTemplate>
                <wasp:WLPToolBar ID="ReportExportToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
                    <Items>
                        <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag%>' ResourceName="Add" />
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
                <wasp:GridBoundControl DataField="Name" LabelText="Name" SortExpression="Name" UniqueName="Name" ResourceName="Name" />
                <wasp:GridKeyValueControl OnNeedDataSource="Report_NeedDataSource" LabelText="Report" DataField="ReportId" Type="ReportId" ResourceName="ReportId" />
                <wasp:GridBoundControl DataField="FileFormat" LabelText="FileFormat" SortExpression="FileFormat" UniqueName="FileFormat" ResourceName="FileFormat" />
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="Export Type" DataField="ExportTypeCode" Type="ExportTypeCode" ResourceName="ExportTypeCode" />
                <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning%>" />
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="Name" Text="Name:" runat="server" ResourceName="Name" Value='<%# Bind("Name") %>' Mandatory="true" ReadOnly="false" TabIndex="010" />
                            </td>
                            <td>
                                <wasp:ComboBoxControl ID="ReportId" runat="server" LabelText="Report:" Type="ReportId" OnDataBinding="Report_NeedDataSource" ResourceName="ReportId" Value='<%# Bind("ReportId") %>' Mandatory="false" ReadOnly="false" TabIndex="020" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="FileFormat" FieldWidth="350px" Width="700px" Text="File Format:" runat="server" ResourceName="FileFormat" Value='<%# Bind("FileFormat") %>' Mandatory="true" ReadOnly="false" TabIndex="030" />
                            </td>
                            <td>
                                <wasp:ComboBoxControl ID="ExportTypeCode" runat="server" LabelText="Export Type:" Type="ExportTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="ExportTypeCode" Value='<%# Bind("ExportTypeCode") %>' Mandatory="true" ReadOnly="false" TabIndex="040" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                            <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsInsert %>' ResourceName="Insert" />
                                        </td>
                                        <td>
                                            <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="Cancel" CausesValidation="false" ResourceName="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true" />

    </wasp:WLPGrid>
</asp:Panel>