﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportExportPage.aspx.cs" Inherits="WorkLinks.Admin.ReportExportAdmin.ReportExportPage" ResourceName="ReportExport" %>
<%@ Register Src="ReportExportControl.ascx" TagName="ReportExportControl" TagPrefix="uc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:ReportExportControl ID="ReportExportControl1" runat="server" />
</asp:Content>