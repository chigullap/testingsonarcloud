﻿using System;
using System.Web.UI;
using Telerik.Web.UI;
using WLP.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Common;

namespace WorkLinks.Admin.ReportExportAdmin
{
    public partial class ReportExportControl : WLPUserControl
    {
        #region properties
        public bool IsViewMode { get { return ReportExportGrid.IsViewMode; } }
        public bool IsUpdate { get { return ReportExportGrid.IsEditMode; } }
        public bool IsInsert { get { return ReportExportGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.ReportExport.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.ReportExport.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.ReportExport.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.ReportExport.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                LoadReportExportInformation();
                Initialize();
            }
        }

        protected void WireEvents()
        {
            ReportExportGrid.NeedDataSource += new GridNeedDataSourceEventHandler(ReportExportGrid_NeedDataSource);
            ReportExportGrid.ItemDataBound += ReportExportGrid_ItemDataBound;
        }

        protected void Initialize()
        {
            //Find the ReportExportGrid
            WLPGrid grid = (WLPGrid)this.FindControl("ReportExportGrid");

            //hide the edit image in the rows.
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
        }

        void ReportExportGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                //hide the delete image if there is a dependency.
                GridDataItem dataItem = e.Item as GridDataItem;
                long? keyId = Convert.ToInt64(dataItem.GetDataKeyValue("Key").ToString());
                ReportExport export = ServiceWrapper.ReportClient.GetReportExport(keyId)[0];
                dataItem["deleteButton"].Controls[0].Visible = !export.HasChildrenFlag;
            }
        }

        protected void LoadReportExportInformation()
        {
            Data = ReportExportCollection.ConvertCollection(ServiceWrapper.ReportClient.GetReportExport(null));
        }
        #endregion

        #region event handlers
        void ReportExportGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            ReportExportGrid.DataSource = Data;
        }

        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }

        protected void Report_NeedDataSource(object sender, EventArgs e)
        {
            CodeHelper.PopulateComboBoxWithReport((ICodeControl)sender);
        }
        #endregion

        #region handle updates
        protected void ReportExportGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                ServiceWrapper.ReportClient.DeleteReportExport((ReportExport)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ReportExportGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            ReportExport export = (ReportExport)e.Item.DataItem;
            ServiceWrapper.ReportClient.InsertReportExport(export).CopyTo((ReportExport)Data[export.Key]);
        }

        protected void ReportExportGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                ReportExport export = (ReportExport)e.Item.DataItem;
                ServiceWrapper.ReportClient.UpdateReportExport(export).CopyTo((ReportExport)Data[export.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}