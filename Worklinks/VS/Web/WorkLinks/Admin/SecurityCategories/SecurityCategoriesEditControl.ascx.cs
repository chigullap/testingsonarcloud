﻿using System;
using System.Web.UI;
using Telerik.Web.UI;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.SecurityCategories
{
    public partial class SecurityCategoriesEditControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private const String _categoryKey = "categoryKey";
        private SecurityCategoryCollection _categoryCollection = null;
        #endregion

        #region properties
        protected SecurityCategoryCollection SecurityCategoryColl
        {
            get
            {
                if (_categoryCollection == null)
                    _categoryCollection = (SecurityCategoryCollection)ViewState[_categoryKey];

                return _categoryCollection;
            }
            set
            {
                _categoryCollection = value;
                ViewState[_categoryKey] = _categoryCollection;
            }
        }
        protected SecurityCategoryCollection DeltaCategory
        {
            get
            {
                SecurityCategoryCollection temp = new SecurityCategoryCollection();

                foreach (SecurityCategory cat in SecurityCategoryColl)
                {
                    if (cat.HierarchicalSortOrder == 0)
                        temp.Add(cat);
                }

                return temp;
            }
        }
        protected SecurityCategoryCollection Categories
        {
            get
            {
                SecurityCategoryCollection temp = new SecurityCategoryCollection();

                foreach (SecurityCategory cat in SecurityCategoryColl)
                {
                    if (cat.HierarchicalSortOrder > 0)
                        temp.Add(cat);
                }

                return temp;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.CategoryEdit.ViewFlag);

            if (!IsPostBack)
            {
                Page.Title = "Security Categories";
                SecurityCategoryCollection test = new SecurityCategoryCollection();
                test.Load(Common.ServiceWrapper.SecurityClient.GetSecurityCategories(false));
                SecurityCategoryColl = test;
            }
        }
        protected void AvailableGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            AvailableGrid.DataSource = DeltaCategory;
        }
        protected void AvailableGrid_RowDrop(object sender, GridDragDropEventArgs e)
        {
            RowDrop((RadGrid)sender, e);
        }
        protected void AssignedGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            AssignedGrid.DataSource = Categories;
        }
        protected void SecurityCategoriesToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            try
            {
                if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("update"))
                {
                    //send the whole collection of categories (after converting to webservice object) and update the database table.
                    SecurityCategoryCollection wsObject = new SecurityCategoryCollection();

                    foreach (SecurityCategory obj in SecurityCategoryColl)
                        wsObject.Add(obj);

                    Common.ServiceWrapper.SecurityClient.UpdateCategories(wsObject);

                    ClientScriptManager manager = Page.ClientScript;
                    manager.RegisterStartupScript(this.GetType(), "close and return", "closeAndReturn();", true);
                }
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void RebindAll()
        {
            AvailableGrid.Rebind();
            AssignedGrid.Rebind();
        }
        protected SecurityCategory GetCategoryToMove(RadGrid sourceGrid, String key) //gets the category being dragged
        {
            SecurityCategory categoryToMove = null;

            if (sourceGrid.Equals(AssignedGrid))
                categoryToMove = Categories.Get(key);
            else
                categoryToMove = DeltaCategory.Get(key);

            return categoryToMove;
        }
        protected void RowDrop(RadGrid sourceGrid, GridDragDropEventArgs e)
        {
            if (e.DestinationGrid != null)
            {
                String sourceDataKey = (String)e.DraggedItems[0].GetDataKeyValue("Key");
                bool addition = false;
                bool directionUp = true;
                int oldIndex = -1;

                //get the category to move
                SecurityCategory categoryToMove = GetCategoryToMove(sourceGrid, sourceDataKey);

                //inserting into position from 
                if (e.DestinationGrid.Equals(AssignedGrid))
                {
                    //get the row index of where the category was dropped in the grid
                    int index = GetDestinationRowIndex(e.DestDataItem, e.DropPosition);
                    SecurityCategory type = new SecurityCategory();

                    oldIndex = categoryToMove.HierarchicalSortOrder;
                    categoryToMove.HierarchicalSortOrder = index + 1;
                    categoryToMove.SelectedFlag = true;
                    addition = true;

                    if (oldIndex != 0)
                        directionUp = oldIndex > index;
                }
                else
                {
                    oldIndex = categoryToMove.HierarchicalSortOrder;
                    categoryToMove.HierarchicalSortOrder = 0;
                    categoryToMove.SelectedFlag = false;
                    addition = false;
                }

                ReOrderGrid(sourceDataKey, addition, oldIndex, directionUp);
                RebindAll();
            }
        }
        protected void ReOrderGrid(String key, bool addition, int oldIndex, bool directionUp)
        {
            if (addition && directionUp) //adding to the assigned grid, or moving records within the same grid in upwards direction
            {
                for (short i = 0; i < Categories.Count; i++)
                {
                    if (Categories[i].Key != key && Categories[i].HierarchicalSortOrder >= Categories[key].HierarchicalSortOrder)
                    {
                        if (oldIndex == 0 || Categories[i].HierarchicalSortOrder < oldIndex)
                            Categories[i].HierarchicalSortOrder++;
                    }
                }
            }
            else if (addition && !directionUp) //moving records within the same grid in downward direction
            {
                for (short i = 0; i < Categories.Count; i++)
                {
                    if (Categories[i].Key != key && Categories[i].HierarchicalSortOrder <= Categories[key].HierarchicalSortOrder)
                    {
                        if (Categories[i].HierarchicalSortOrder > oldIndex)
                            Categories[i].HierarchicalSortOrder--;
                    }
                }

                //before we exit, if row was dragged to the bottom of the grid, decrement the sort order
                if (Categories[key].HierarchicalSortOrder > Categories.Count)
                    Categories[key].HierarchicalSortOrder--;
            }
            else //removing from assigned grid
            {
                //oldIndex will be 0 if we are moving a record from the available grid TO the available grid (ie.  row swapping in Available grid), so do nothing.
                if (oldIndex != 0)
                {
                    for (short i = 0; i < Categories.Count; i++)
                    {
                        if (Categories[i].HierarchicalSortOrder > oldIndex)
                            Categories[i].HierarchicalSortOrder--;
                    }
                }
            }
        }
        // finds the index for inserting the dropped row
        protected int GetDestinationRowIndex(GridDataItem item, GridItemDropPosition position)
        {
            int index = (item != null) ? item.DataSetIndex + (position.Equals(GridItemDropPosition.Above) ? 0 : 1) : 0;
            return index;
        }
    }
}