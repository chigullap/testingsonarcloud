﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecurityCategoriesControl.ascx.cs" Inherits="WorkLinks.Admin.SecurityCategories.SecurityCategoriesControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPGrid
    ID="SecurityCategoriesGrid"
    runat="server"
    GridLines="None"
    Width="100%"
    Height="100%"
    OnNeedDataSource="SecurityCategoriesGrid_NeedDataSource">

    <ClientSettings AllowKeyboardNavigation="true">
        <Selecting AllowRowSelect="false" />
    </ClientSettings>

    <MasterTableView AutoGenerateColumns="False" DataKeyNames="Key" CommandItemDisplay="Top">
        <CommandItemTemplate>
            <wasp:WLPToolBar ID="SecurityCategoriesGridToolBar" runat="server" Width="100%" OnLoad="SecurityCategoriesGridToolBar_Load" OnClientButtonClicking="onClientButtonClicking" OnButtonClick="SecurityCategoriesGridToolBar_ButtonClick">
                <Items>
                    <wasp:WLPToolBarButton OnPreRender="EditToolBar_PreRender" Text="Edit" PostBack="false" ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="TypeEdit" ResourceName="Edit"></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton OnPreRender="RebuildSecurityToolBar_PreRender" Text="Rebuild Security" ImageUrl="" CommandName="RebuildSecurity" ResourceName="RebuildSecurity"></wasp:WLPToolBarButton>
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <RowIndicatorColumn>
            <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn>
            <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>

        <Columns>
            <wasp:GridBoundControl DataField="HierarchicalSortOrder" LabelText="Sort Order" SortExpression="HierarchicalSortOrder" UniqueName="HierarchicalSortOrder" ResourceName="HierarchicalSortOrder"></wasp:GridBoundControl>
            <wasp:GridBoundControl DataField="SecurityCategoryDescription" LabelText="Description" SortExpression="SecurityCategoryDescription" UniqueName="SecurityCategoryDescription" ResourceName="SecurityCategoryDescription"></wasp:GridBoundControl>
        </Columns>
    </MasterTableView>

</wasp:WLPGrid>

<wasp:WLPWindowManager ClientIDMode="Static" ID="SecurityCategoryWindows" Width="640" Height="500" VisibleStatusbar="false" Behaviors="Close,Move,Resize,Minimize" runat="server" Modal="true" DestroyOnClose="true" ShowContentDuringLoad="false">
    <Windows>
        <telerik:RadWindow ClientIDMode="Static" ID="SecurityCategoryWindow" runat="server" OnClientClose="onClientClose">
        </telerik:RadWindow>
    </Windows>
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function OpenSecurityCategoryEdit() {
            openWindowWithManager('SecurityCategoryWindows', '/Admin/SecurityCategories/EditPage', false);
        }

        function onClientClose(sender, eventArgs) {
            __doPostBack('<%= SecurityCategoriesGrid.ClientID %>', '<%= String.Format("refresh") %>');
        }

        function onClientButtonClicking(sender, args) {
            var comandName = args.get_item().get_commandName();
            if (comandName == "RebuildSecurity") {
                var message = "<asp:Literal runat="server" Text="<%$ Resources:WarningMessages, SecurityRebuildDeleteNotificationWarning %>" />";
                args.set_cancel(!confirm(message));
            }
        }  
    </script>
</telerik:RadScriptBlock>