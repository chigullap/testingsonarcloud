﻿using System;
using System.Web.UI;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.SecurityCategories
{
    public partial class SecurityCategoriesControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private bool IsViewMode { get { return SecurityCategoriesGrid.IsViewMode; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.Category.ViewFlag);

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.Equals(String.Format("refresh")))
                RefreshGrid();

            if (!IsPostBack)
                LoadGrid();

            RemoveParentScrollBar();
        }
        public void RefreshGrid()
        {
            LoadGrid();
            SecurityCategoriesGrid.Rebind();
        }
        //load employee and contact types
        private void LoadGrid()
        {
            Data = SecurityCategoryCollection.ConvertCollection(Common.ServiceWrapper.SecurityClient.GetSecurityCategories(IsViewMode));
        }
        protected void SecurityCategoriesGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            SecurityCategoriesGrid.DataSource = Data;
        }
        protected void SecurityCategoriesGridToolBar_Load(object sender, EventArgs e)
        {
            RadToolBar toolbar = (RadToolBar)sender;
            RadToolBarButton button = (RadToolBarButton)toolbar.FindButtonByCommandName("TypeEdit");
            button.Attributes["onclick"] = "OpenSecurityCategoryEdit()";
        }
        protected void SecurityCategoriesGridToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("rebuildsecurity"))
                Common.ServiceWrapper.SecurityClient.RebuildSecurity(); //rebuild security

            RefreshGrid();
        }
        private void RemoveParentScrollBar()
        {
            //Remove the scroll bar for this Control by overriding the RadPane settings in Site.Master.
            Control parentPane = SecurityCategoriesGrid.Parent.Parent.Parent;
            if (parentPane is RadPane)
                ((RadPane)parentPane).Scrolling = SplitterPaneScrolling.None;
        }
        #endregion

        #region security handlers
        protected void EditToolBar_PreRender(object sender, EventArgs e)
        {
            ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.CategoryEdit.UpdateFlag;
        }
        protected void RebuildSecurityToolBar_PreRender(object sender, EventArgs e)
        {
            ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.CategoryEdit.ViewFlag;
        }
        #endregion
    }
}