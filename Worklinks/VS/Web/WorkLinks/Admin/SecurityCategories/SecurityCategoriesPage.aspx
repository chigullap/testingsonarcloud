﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SecurityCategoriesPage.aspx.cs" Inherits="WorkLinks.Admin.SecurityCategories.SecurityCategoriesPage" ResourceName="Title" %>
<%@ Register Src="SecurityCategoriesControl.ascx" TagName="SecurityCategoriesControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:SecurityCategoriesControl ID="SecurityCategoriesControl1" runat="server" />
</asp:Content>