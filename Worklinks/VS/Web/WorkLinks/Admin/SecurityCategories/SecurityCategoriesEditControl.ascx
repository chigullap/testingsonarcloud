﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecurityCategoriesEditControl.ascx.cs" Inherits="WorkLinks.Admin.SecurityCategories.SecurityCategoriesEditControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="AvailableGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="AvailableGrid" />
                <telerik:AjaxUpdatedControl ControlID="AssignedGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="AssignedGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="AvailableGrid" />
                <telerik:AjaxUpdatedControl ControlID="AssignedGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<table width="100%">
    <tr>
        <td colspan="2">
            <wasp:WLPToolBar ID="SecurityCategoriesToolBar" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="SecurityCategoriesToolBar_ButtonClick">
                <Items>
                    <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" ResourceName="Update"></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" CausesValidation="False" onclick="closeAndReturn()" ResourceName="Cancel"></wasp:WLPToolBarButton>
                </Items>
            </wasp:WLPToolBar>
        </td>
    </tr>
    <tr>
        <td>
            <wasp:WLPLabel ID="lblAvailable" runat="server" Font-Bold="true" Text="**Available**" ResourceName="Available"></wasp:WLPLabel>

            <wasp:WLPGrid
                ID="AvailableGrid"
                runat="server"
                GridLines="None"
                Width="300px"
                Height="350px"
                OnNeedDataSource="AvailableGrid_NeedDataSource"
                OnRowDrop="AvailableGrid_RowDrop">

                <ClientSettings AllowKeyboardNavigation="true" AllowRowsDragDrop="true">
                    <Selecting AllowRowSelect="true" EnableDragToSelectRows="false" />
                    <ClientEvents OnRowDropping="onRowDropping" />
                </ClientSettings>

                <MasterTableView AutoGenerateColumns="false" DataKeyNames="Key" CommandItemDisplay="None">
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="SecurityCategoryDescription" SortOrder="Ascending" />
                    </SortExpressions>

                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>

                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>

                    <Columns>
                        <wasp:GridBoundControl DataField="SecurityCategoryDescription" LabelText="Description" SortExpression="SecurityCategoryDescription" UniqueName="SecurityCategoryDescription" ResourceName="SecurityCategoryDescription"></wasp:GridBoundControl>
                    </Columns>
                </MasterTableView>

            </wasp:WLPGrid>
        </td>
        <td>
            <wasp:WLPLabel ID="lblAssigned" runat="server" Font-Bold="true" Text="**Assigned**" ResourceName="Assigned"></wasp:WLPLabel>

            <wasp:WLPGrid
                ID="AssignedGrid"
                runat="server"
                GridLines="None"
                Width="300px"
                Height="350px"
                OnNeedDataSource="AssignedGrid_NeedDataSource"
                OnRowDrop="AvailableGrid_RowDrop">

                <ClientSettings AllowKeyboardNavigation="true" AllowRowsDragDrop="true">
                    <Selecting AllowRowSelect="true" EnableDragToSelectRows="false" />
                    <ClientEvents OnRowDropping="onRowDropping" />
                </ClientSettings>

                <MasterTableView AutoGenerateColumns="false" DataKeyNames="Key" CommandItemDisplay="None">
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="HierarchicalSortOrder" SortOrder="Ascending" />
                    </SortExpressions>

                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>

                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>

                    <Columns>
                        <wasp:GridBoundControl DataField="HierarchicalSortOrder" LabelText="Sort Order" SortExpression="HierarchicalSortOrder" UniqueName="HierarchicalSortOrder" ResourceName="HierarchicalSortOrder"></wasp:GridBoundControl>
                        <wasp:GridBoundControl DataField="SecurityCategoryDescription" LabelText="Description" SortExpression="SecurityCategoryDescription" UniqueName="SecurityCategoryDescription" ResourceName="SecurityCategoryDescription"></wasp:GridBoundControl>
                    </Columns>
                </MasterTableView>

            </wasp:WLPGrid>
        </td>
    </tr>
</table>

<script type="text/javascript">
    function onRowDropping(sender, args) {

    }

    function getRadWindow() {
        var popup = null;

        if (window.radWindow)
            popup = window.radWindow;
        else if (window.frameElement.radWindow)
            popup = window.frameElement.radWindow;

        return popup;
    }

    function closeAndReturn() {
        var closeWithChanges = true;
        var popup = getRadWindow(); //get a reference to the current RadWindow

        //Close the window with parms
        setTimeout(function () { popup.close(closeWithChanges); }, 0); //have to use "setTimeout" to get around an IE9 bug
    }
</script>