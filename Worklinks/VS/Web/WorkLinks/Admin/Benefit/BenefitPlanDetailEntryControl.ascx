﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BenefitPlanDetailEntryControl.ascx.cs" Inherits="WorkLinks.Admin.Benefit.BenefitPlanDetailEntryControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPFormView
    ID="PlanDetailView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key">

    <ItemTemplate>
        <wasp:WLPToolBar ID="PlanDetailToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
            <Items>
                <wasp:WLPToolBarButton Visible="<%# UpdateFlag %>" ImageUrl="~/App_Themes/Default/Edit.gif" Text="**Edit" CommandName="Edit" ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>

        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:DateControl ID="EffectiveDate" runat="server" Value='<%# Bind("EffectiveDate") %>' ResourceName="EffectiveDate" Mandatory="true" ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="BenefitPlanTypeCode" runat="server" Type="BenefitPlanTypeCode" Value='<%# Bind("BenefitPlanTypeCode") %>' ResourceName="BenefitPlanTypeCode" OnDataBinding="Code_NeedDataSource" Mandatory="true" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="BenefitPlanStatusCode" runat="server" Type="BenefitPlanStatusCode" Value='<%# Bind("BenefitPlanStatusCode") %>' ResourceName="BenefitPlanStatusCode" OnDataBinding="Code_NeedDataSource" Mandatory="true" ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="VendorId" runat="server" Value='<%# Bind("VendorId") %>' ResourceName="Vendor" OnDataBinding="Vendor_NeedDataSource" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="EmployeePaycodeCode" runat="server" Type="PaycodeCode" Value='<%# Bind("EmployeePaycodeCode")%>' ResourceName="EmployeePaycodeCode" OnDataBinding="Code_NeedDataSource" ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="EmployerPaycodeCode" runat="server" Type="PaycodeCode" Value='<%# Bind("EmployerPaycodeCode")%>' ResourceName="EmployerPaycodeCode" OnDataBinding="Code_NeedDataSource" ReadOnly="true" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar ID="PlanDetailToolBarEdit" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
            <Items>
                <wasp:WLPToolBarButton Visible="<%# IsEditMode %>" ImageUrl="~/App_Themes/Default/Update.gif" Text="**Update" CommandName="Update" ResourceName="Update"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton ImageUrl="~/App_Themes/Default/Cancel.gif" Text="**Cancel" CausesValidation="false" CommandName="Cancel" ResourceName="Cancel"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>

        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:DateControl ID="EffectiveDateEdit" runat="server" Value='<%# Bind("EffectiveDate") %>' ResourceName="EffectiveDate" Mandatory="true" ReadOnly="<%# !IsInsertMode %>" TabIndex="010" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="BenefitPlanTypeCodeEdit" runat="server" Type="BenefitPlanTypeCode" Value='<%# Bind("BenefitPlanTypeCode") %>' ResourceName="BenefitPlanTypeCode" OnDataBinding="Code_NeedDataSource" Mandatory="true" ReadOnly="<%# !IsInsertMode %>" TabIndex="020" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="BenefitPlanStatusCodeEdit" runat="server" Type="BenefitPlanStatusCode" Value='<%# Bind("BenefitPlanStatusCode") %>' ResourceName="BenefitPlanStatusCode" OnDataBinding="Code_NeedDataSource" Mandatory="true" ReadOnly="<%# !IsInsertMode %>" TabIndex="030" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="VendorIdEdit" runat="server" Value='<%# Bind("VendorId") %>' ResourceName="Vendor" OnDataBinding="Vendor_NeedDataSource" ReadOnly="<%# !IsInsertMode %>" TabIndex="040" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="EmployeePaycodeCodeEdit" runat="server" Type="PaycodeCode" Value='<%# Bind("EmployeePaycodeCode")%>' ResourceName="EmployeePaycodeCode" OnDataBinding="Code_NeedDataSource" ReadOnly="<%# !IsInsertMode %>" TabIndex="050" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="EmployerPaycodeCodeEdit" runat="server" Type="PaycodeCode" Value='<%# Bind("EmployerPaycodeCode")%>' ResourceName="EmployerPaycodeCode" OnDataBinding="Code_NeedDataSource" ReadOnly="<%# !IsInsertMode %>" TabIndex="060" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>

</wasp:WLPFormView>
