﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BenefitPlanSearchControl.ascx.cs" Inherits="WorkLinks.Admin.Benefit.BenefitPlanSearchControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="PlanSummaryGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnClear">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="PlanSummaryGrid" />
                <telerik:AjaxUpdatedControl ControlID="Description" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<table width="100%">
    <tr valign="top">
        <td>
            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="Description" runat="server" ResourceName="Description" TabIndex="010" />
                        </td>
                    </tr>
                </table>
                <div class="SearchCriteriaButtons">
                    <wasp:WLPButton ID="btnClear" runat="server" Text="**Clear**" Icon-PrimaryIconUrl="~/App_Themes/Default/clear.gif" CssClass="button" OnClientClicked="clear" OnClick="btnClear_Click" ResourceName="Clear" />
                    <wasp:WLPButton ID="btnSearch" runat="server" Text="**Search**" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" OnClick="btnSearch_Click" ResourceName="Search" />
                </div>
            </asp:Panel>
        </td>
    </tr>
    <tr valign="top">
        <td>
            <wasp:WLPToolBar ID="PlanSummaryToolBar" runat="server" Width="100%" OnButtonClick="PlanSummaryToolBar_ButtonClick" OnClientLoad="PlanSummaryToolBar_init" OnClientButtonClicking="OnClientButtonClicking">
                <Items>
                    <wasp:WLPToolBarButton OnPreRender="AddToolBar_PreRender" Text="**Add**" ImageUrl="~/App_Themes/Default/Add.gif" onclick="Add();" CommandName="Add" ResourceName="Add" />
                    <wasp:WLPToolBarButton OnPreRender="DetailsToolBar_PreRender" Text="**Details**" onclick="Open();" CommandName="Details" ResourceName="Details" />
                    <wasp:WLPToolBarButton OnPreRender="DeleteToolBar_PreRender" Text="**Delete**" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ResourceName="Delete" />
                </Items>
            </wasp:WLPToolBar>

            <wasp:WLPGrid
                ID="PlanSummaryGrid"
                runat="server"
                AllowPaging="true"
                PagerStyle-AlwaysVisible="true"
                PageSize="100"
                AllowSorting="true"
                GridLines="None"
                Height="400px"
                AutoAssignModifyProperties="true">

                <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                    <ClientEvents OnRowDblClick="OnRowDblClick" OnRowClick="OnRowClick" OnCommand="OnCommand" />
                </ClientSettings>

                <MasterTableView ClientDataKeyNames="BenefitPlanId" AutoGenerateColumns="false" DataKeyNames="BenefitPlanId" CommandItemDisplay="Top">
                    <CommandItemTemplate></CommandItemTemplate>

                    <Columns>
                        <wasp:GridBoundControl DataField="BenefitPlanDescriptionField" LabelText="**Description**" SortExpression="BenefitPlanDescriptionField" UniqueName="BenefitPlanDescriptionField" ResourceName="Description" />
                    </Columns>
                </MasterTableView>

                <HeaderContextMenu EnableAutoScroll="true" />

            </wasp:WLPGrid>
        </td>
    </tr>
</table>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="PlanWindows"
    Width="1100"
    Height="700"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    OnClientClose="OnClientClose"
    EnableViewState="false"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="ShowContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var benefitPlanId;
        var rowSelected;
        var addButton;
        var detailsButton;
        var deleteButton;
        var toolbar;

        function ShowContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function clear() {
            benefitPlanId = null;
        }

        function PlanSummaryToolBar_init(sender) {
            InitializeControls();
        }

        function InitializeControls() {
            toolbar = $find('<%= PlanSummaryToolBar.ClientID %>');
            addButton = toolbar.findButtonByCommandName('Add');
            detailsButton = toolbar.findButtonByCommandName('Details');
            deleteButton = toolbar.findButtonByCommandName('Delete');
            benefitPlanId = null;

            if (detailsButton != null)
                detailsButton.disable();

            if (deleteButton != null)
                deleteButton.disable();
        }

        function OnRowDblClick(sender, eventArgs) {
            if (toolbar.findButtonByCommandName('Details') != null)
                Open(sender);
        }

        function OnRowClick(sender, eventArgs) {
            benefitPlanId = eventArgs.getDataKeyValue('BenefitPlanId');
            rowSelected = true;

            if (detailsButton != null)
                detailsButton.enable();

            if (deleteButton != null)
                deleteButton.enable();
        }

        function OnClientButtonClicking(sender, args) {
            if (benefitPlanId != null) {
                var comandName = args.get_item().get_commandName();
                if (comandName == "Delete") {
                    var message = "<asp:Literal runat="server" Text="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />";
                    args.set_cancel(!confirm(message));
                }
                else
                    args.set_cancel(true);
            }
            else
                args.set_cancel(true);
        }

        function OnClientClose(sender, eventArgs) {
            var arg = sender.argument;
            if (arg != null && arg.closeWithChanges)
                __doPostBack('<%= PlanSummaryGrid.ClientID %>', 'refresh');
        }

        function Add() {
            openWindowWithManager('PlanWindows', String.format('/Admin/Benefit/Plan/Add/{0}', -1), false);
        }

        function Open() {
            if (benefitPlanId != null)
                openWindowWithManager('PlanWindows', String.format('/Admin/Benefit/Plan/View/{0}', benefitPlanId), false);
        }

        function RowSelected() {
            return rowSelected;
        }

        function OnCommand(sender, args) {
            if (args.get_commandName() == 'Page') {
                benefitPlanId = null;
                InitializeControls();
            }
        }
    </script>
</telerik:RadScriptBlock>
