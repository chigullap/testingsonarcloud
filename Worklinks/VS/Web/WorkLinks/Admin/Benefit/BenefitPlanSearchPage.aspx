﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BenefitPlanSearchPage.aspx.cs" Inherits="WorkLinks.Admin.Benefit.BenefitPlanSearchPage" %>
<%@ Register Src="BenefitPlanSearchControl.ascx" TagName="BenefitPlanSearchControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:BenefitPlanSearchControl ID="BenefitPlanSearchControl1" SearchLocation="Plan" runat="server" />        
    </ContentTemplate>
</asp:Content>

