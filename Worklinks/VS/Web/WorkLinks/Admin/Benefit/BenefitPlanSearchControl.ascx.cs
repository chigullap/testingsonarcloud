﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.Benefit
{
    public partial class BenefitPlanSearchControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected String Criteria
        {
            get
            {
                String searchEntry = Description.TextValue;
                return searchEntry;
            }
            set
            {
                Description.TextValue = value;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.BenefitPlanSearch.ViewFlag);

            WireEvents();

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.Equals(String.Format("refresh")))
                RefreshGrid();
        }

        protected void WireEvents()
        {
            PlanSummaryGrid.NeedDataSource += new GridNeedDataSourceEventHandler(PlanSummaryGrid_NeedDataSource);

            //work around to remove empty rows caused by the OnCommand Telerik bug
            PlanSummaryGrid.ItemDataBound += new GridItemEventHandler(PlanSummaryGrid_ItemDataBound);
            PlanSummaryGrid.PreRender += new EventHandler(PlanSummaryGrid_PreRender);
            PlanSummaryGrid.AllowPaging = true;
        }

        protected void RefreshGrid()
        {
            long keyId = -1;

            //if a row was selected
            if (PlanSummaryGrid.SelectedItems.Count != 0)
                keyId = Convert.ToInt64(((GridDataItem)PlanSummaryGrid.SelectedItems[0]).GetDataKeyValue(PlanSummaryGrid.MasterTableView.DataKeyNames[0]));

            Search(Criteria);

            if (keyId != -1)
                PlanSummaryGrid.SelectRowByFirstDataKey(keyId); //re-select the selected row
        }

        public void Search(String criteria)
        {
            Data = BenefitPlanSearchCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetBenefitPlanReport(criteria));
            PlanSummaryGrid.Rebind();
        }

        protected void Delete(BenefitPlanSearch benefitPlanSearch)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteBenefitPlanReport(Convert.ToInt64(benefitPlanSearch.Key));
                Data.Remove(benefitPlanSearch.Key);

                PlanSummaryGrid.SelectedIndexes.Clear();
                PlanSummaryGrid.Rebind();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    PlanSummaryGrid.SelectedIndexes.Clear();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region event handlers
        void PlanSummaryGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            PlanSummaryGrid.DataSource = Data;
        }

        protected void PlanSummaryToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("delete") && PlanSummaryGrid.SelectedValue != null)
                Delete((BenefitPlanSearch)Data[PlanSummaryGrid.SelectedValue.ToString()]);
        }

        protected void PlanSummaryGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                //work around to remove empty rows caused by the OnCommand Telerik bug 
                if (String.Equals((dataItem["BenefitPlanDescriptionField"].Text).ToUpper(), "&NBSP;"))
                {
                    //hide the row
                    e.Item.Display = false;
                }
            }
        }

        void PlanSummaryGrid_PreRender(object sender, EventArgs e)
        {
            //work around to remove empty rows caused by the OnCommand Telerik bug
            if (Data == null)
                PlanSummaryGrid.AllowPaging = false;
            else
                PlanSummaryGrid.AllowPaging = true;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search(Criteria);
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Description.TextValue = "";
            Data = null;
            PlanSummaryGrid.DataBind();
        }
        #endregion

        #region security handlers
        protected void AddToolBar_PreRender(object sender, EventArgs e)
        {
            ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.BenefitPlanSearch.AddFlag;
        }

        protected void DetailsToolBar_PreRender(object sender, EventArgs e)
        {
            ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.BenefitPlanSearch.ViewFlag;
        }

        protected void DeleteToolBar_PreRender(object sender, EventArgs e)
        {
            ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.BenefitPlanSearch.DeleteFlag;
        }
        #endregion
    }
}