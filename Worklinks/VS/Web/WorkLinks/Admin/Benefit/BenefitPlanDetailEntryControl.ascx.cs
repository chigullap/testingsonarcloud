﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.Benefit
{
    public partial class BenefitPlanDetailEntryControl : WLP.Web.UI.WLPUserControl
    {
        #region delegates
        public delegate void PlanUpdatedEventHandler(object sender, EventArgs e);
        public delegate void PlanCancelledEventHandler(object sender, EventArgs e);
        public event PlanUpdatedEventHandler PlanUpdated;
        public event PlanCancelledEventHandler PlanCancelled;
        #endregion

        #region fields
        private long _benefitPlanId = -1;
        private long _benefitPlanDetailId = -1;
        private const String _benefitPlanIdKey = "_benefitPlanIdKey";
        private const String _benefitPlanDetailIdKey = "_benefitPlanDetailIdKey";
        #endregion

        #region properties     
        public bool IsEditMode
        {
            get { return PlanDetailView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.Edit); }
        }

        public bool IsViewMode
        {
            get { return PlanDetailView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.ReadOnly); }
        }

        public bool IsPlanViewMode
        {
            get { return ((BenefitPlanControl)this.Parent.Parent.Parent).IsViewMode; }
        }

        public bool IsInsertMode
        {
            get { return BenefitPlanDetailId == -1; }
        }

        public bool UpdateFlag
        {
            get { return Common.Security.RoleForm.BenefitPlanEdit.UpdateFlag; }
        }

        public long BenefitPlanId
        {
            get
            {
                if (_benefitPlanId == -1)
                    _benefitPlanId = Convert.ToInt64(ViewState[_benefitPlanIdKey]);

                return _benefitPlanId;
            }
            set
            {
                _benefitPlanId = value;
                ViewState[_benefitPlanIdKey] = _benefitPlanId;
            }
        }

        public long BenefitPlanDetailId
        {
            get
            {
                if (_benefitPlanDetailId == -1)
                    _benefitPlanDetailId = Convert.ToInt64(ViewState[_benefitPlanDetailIdKey]);

                return _benefitPlanDetailId;
            }
            set
            {
                _benefitPlanDetailId = value;
                ViewState[_benefitPlanDetailIdKey] = _benefitPlanDetailId;
            }
        }

        public BenefitPlanDetail LastPlanDetail
        {
            get
            {
                BenefitPlanDetail lastBenefitPlanDetail = null;
                BenefitPlanDetailCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetBenefitPlanDetail(BenefitPlanId, -1); //-1 gets everything

                if (collection.Count > 0)
                    lastBenefitPlanDetail = collection[0];

                return lastBenefitPlanDetail;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();
        }

        protected void WireEvents()
        {
            PlanDetailView.NeedDataSource += new WLPFormView.NeedDataSourceEventHandler(PlanDetailView_NeedDataSource);
            PlanDetailView.Updating += new WLPFormView.ItemUpdatingEventHandler(PlanDetailView_Updating);
            PlanDetailView.DataBound += PlanDetailView_DataBound;
            PlanDetailView.ItemCommand += PlanDetailView_ItemCommand;
            PlanDetailView.PreRender += PlanDetailView_PreRender;
        }

        private void InitializeControls()
        {
            //this code makes sure the RadToolbar is set to the proper status before/during/after update/insert operations
            WLPToolBar toolbar = (WLPToolBar)PlanDetailView.FindControl("PlanDetailToolBar");
            if (toolbar == null)
            {
                toolbar = (WLPToolBar)PlanDetailView.FindControl("PlanDetailToolBarEdit");
            }
            toolbar.Enabled = IsPlanViewMode;
        }

        public void LoadPlanDetailEntry()
        {
            BenefitPlanDetailCollection collection = new BenefitPlanDetailCollection();

            if (IsInsertMode) //if we are doing an insert
            {
                if (LastPlanDetail != null) //if there is a previous entitlement detail record
                {
                    collection.Add(LastPlanDetail);

                    //reset the BenefitPlanDetailId and EffectiveDate
                    collection[0].BenefitPlanDetailId = BenefitPlanDetailId;
                    collection[0].EffectiveDate = null;
                }
                else
                {
                    collection.AddNew();
                    collection[0].BenefitPlanId = BenefitPlanId; //set the BenefitPlanId
                }
            }
            else
                collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetBenefitPlanDetail(BenefitPlanId, BenefitPlanDetailId));

            Data = collection;

            if (IsInsertMode) //if we are doing an insert
                PlanDetailView.ChangeMode(System.Web.UI.WebControls.FormViewMode.Edit);

            PlanDetailView.DataBind();
        }
        #endregion


        #region event handlers
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }

        protected void Vendor_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateVendorComboControl((ICodeControl)sender);
        }

        void PlanDetailView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            PlanDetailView.DataSource = Data;
        }

        private void PlanDetailView_DataBound(object sender, EventArgs e)
        {
            if (IsEditMode && IsInsertMode)
            {
                //get the latest entitlement detail record and set the MinDate to a day after the EffectiveDate
                DateControl dateControl = (DateControl)PlanDetailView.FindControl("EffectiveDate");
                if (dateControl != null && LastPlanDetail != null)
                    dateControl.MinDate = ((DateTime)LastPlanDetail.EffectiveDate).AddDays(1);
            }
        }

        private void PlanDetailView_ItemCommand(object sender, System.Web.UI.WebControls.FormViewCommandEventArgs e)
        {
            if (e.CommandName == "Cancel")
            {
                if (IsInsertMode) //if we are doing an insert
                    OnPlanCancelled(null);
            }
        }

        private void PlanDetailView_PreRender(object sender, EventArgs e)
        {
        }

        void PlanDetailView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                BenefitPlanDetail entitlementDetail = (BenefitPlanDetail)e.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.UpdateBenefitPlanDetail(entitlementDetail).CopyTo((BenefitPlanDetail)Data[entitlementDetail.Key]);

                //refresh the grid on the PlanDetailControl
                OnPlanUpdated(null);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region event triggers
        protected virtual void OnPlanUpdated(EventArgs args)
        {
            if (PlanUpdated != null)
                PlanUpdated(this, args);
        }
        protected virtual void OnPlanCancelled(EventArgs args)
        {
            if (PlanCancelled != null)
                PlanCancelled(this, args);
        }
        #endregion

        #region override
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            InitializeControls();
        }
        #endregion
    }
}