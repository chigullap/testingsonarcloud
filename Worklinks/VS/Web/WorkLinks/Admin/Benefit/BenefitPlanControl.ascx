﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BenefitPlanControl.ascx.cs" Inherits="WorkLinks.Admin.Benefit.BenefitPlanControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Src="BenefitPlanDetailControl.ascx" TagName="BenefitPlanDetailControl" TagPrefix="uc1" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="Panel1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="Panel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="Panel1" runat="server">
    <wasp:WLPFormView
        ID="PlanView"
        runat="server"
        RenderOuterTable="false"
        DataKeyNames="Key">

        <ItemTemplate>
            <wasp:WLPToolBar ID="BenefitPlanToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
                <Items>
                    <wasp:WLPToolBarButton Visible="<%# UpdateFlag %>" ImageUrl="~/App_Themes/Default/Edit.gif" Text="**Edit" CommandName="Edit" ResourceName="Edit"></wasp:WLPToolBarButton>
                </Items>
            </wasp:WLPToolBar>

            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="EnglishDescription" runat="server" LabelText="**EnglishDescription" ResourceName="EnglishDescription" Value='<%# Eval("EnglishDescription") %>' Mandatory="true" ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="FrenchDescription" runat="server" LabelText="**FrenchDescription" ResourceName="FrenchDescription" Value='<%# Eval("FrenchDescription") %>' Mandatory="true" ReadOnly="true" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </ItemTemplate>

        <EditItemTemplate>
            <wasp:WLPToolBar ID="BenefitPlanToolBarEdit" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
                <Items>
                    <wasp:WLPToolBarButton Visible="<%# IsEditMode %>" ImageUrl="~/App_Themes/Default/Update.gif" Text="**Update" CommandName="Update" ResourceName="Update"></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton Visible="<%# IsInsertMode %>" ImageUrl="~/App_Themes/Default/Add.gif" Text="**Add" CommandName="Insert" ResourceName="Insert"></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton Visible='<%# !IsInsertMode %>' ImageUrl="~/App_Themes/Default/Cancel.gif" Text="**Cancel" CommandName="cancel" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton Visible='<%# IsInsertMode %>' ImageUrl="~/App_Themes/Default/Cancel.gif" Text="**Cancel" onClick="Close()" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
                </Items>
            </wasp:WLPToolBar>

            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="EnglishDescriptionEdit" runat="server" LabelText="**EnglishDescription" ResourceName="EnglishDescription" Value='<%# Bind("EnglishDescription") %>' Mandatory="true" TabIndex="010" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="FrenchDescriptionEdit" runat="server" LabelText="**FrenchDescription" ResourceName="FrenchDescription" Value='<%# Bind("FrenchDescription") %>' Mandatory="true" TabIndex="020" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </EditItemTemplate>

    </wasp:WLPFormView>

    <uc1:BenefitPlanDetailControl ID="PlanDetailControl" runat="server" Visible="false" />
</asp:Panel>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function Close(button, args) {
            var popup = getRadWindow();
            setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
        }

        function processClick(commandName) {
            var arg = new Object;
            arg.closeWithChanges = true;
            getRadWindow().argument = arg;
        }

        function getRadWindow() {
            var popup = null;

            if (window.radWindow)
                popup = window.radWindow;
            else if (window.frameElement.radWindow)
                popup = window.frameElement.radWindow;

            return popup;
        }

        function ToolBarClick(sender, args) {
            var button = args.get_item();
            var commandName = button.get_commandName();

            processClick(commandName);

            if (<%= IsInsertMode.ToString().ToLower() %>) {
                if (commandName == "Cancel") {
                    window.close();
                }
            }
        }
    </script>
</telerik:RadScriptBlock>
