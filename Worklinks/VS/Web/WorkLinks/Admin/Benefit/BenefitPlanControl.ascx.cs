﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.Benefit
{
    public partial class BenefitPlanControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private String Action
        {
            get { return Page.RouteData.Values["action"].ToString().ToLower(); }
        }

        private long PlanId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["planId"]); }
        }

        public bool IsEditMode
        {
            get { return PlanView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.Edit); }
        }

        public bool IsInsertMode
        {
            get { return PlanView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.Insert); }
        }

        public bool IsViewMode
        {
            get { return PlanView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.ReadOnly); }
        }

        public bool UpdateFlag
        {
            get { return Common.Security.RoleForm.BenefitPlanEdit.UpdateFlag; }
        }

        public bool IsPlanDetailViewMode
        {
            get { return PlanDetailControl.IsViewMode && PlanDetailControl.IsDetailEntryViewMode; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.BenefitPlanEdit.ViewFlag);

            WireEvents();

            if (Action == "view")
            {
                this.Page.Title = "Edit a Plan";

                if (!IsPostBack)
                {
                    LoadPlan();
                    LoadPlanDetailControl();
                }
            }
            else if (Action == "add")
            {
                if (!IsPostBack)
                {
                    this.Page.Title = "Add a Plan";
                    LoadNewPlan();
                }
                else
                {
                    this.Page.Title = "Edit a Plan";
                    LoadPlanDetailControl();
                }
            }
        }

        protected void WireEvents()
        {
            PlanView.NeedDataSource += new WLPFormView.NeedDataSourceEventHandler(PlanView_NeedDataSource);
            PlanView.Inserting += new WLPFormView.ItemInsertingEventHandler(PlanView_Inserting);
            PlanView.Updating += new WLPFormView.ItemUpdatingEventHandler(PlanView_Updating);
        }

        protected void LoadPlan()
        {
            BenefitPlanCollection collection = new BenefitPlanCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetBenefitPlan(PlanId));
            Data = collection;

            PlanView.DataBind();
        }

        protected void LoadNewPlan()
        {
            BenefitPlanCollection collection = new BenefitPlanCollection();
            collection.AddNew();
            Data = collection;

            PlanView.ChangeMode(System.Web.UI.WebControls.FormViewMode.Insert);
            PlanView.DataBind();
        }

        protected void LoadPlanDetailControl()
        {
            PlanDetailControl.Visible = true;
            PlanDetailControl.BenefitPlanId = ((BenefitPlanCollection)Data)[0].BenefitPlanId;
            PlanDetailControl.LoadPlanDetail();
            PlanDetailControl.Initialize();
        }
        #endregion

        #region handles updates
        void PlanView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            BenefitPlan benefitPlan = (BenefitPlan)e.DataItem;
            Common.ServiceWrapper.HumanResourcesClient.InsertBenefitPlan(benefitPlan).CopyTo((BenefitPlan)Data[benefitPlan.Key]);
        }

        void PlanView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                BenefitPlan benefitPlan = (BenefitPlan)e.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.UpdateBenefitPlan(benefitPlan).CopyTo((BenefitPlan)Data[benefitPlan.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region event handlers
        void PlanView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            PlanView.DataSource = Data;
        }

        protected void InitializeControls()
        {
            //this code makes sure the RadToolbar is set to the proper status before/during/after update/insert operations
            WLPToolBar toolbar = (WLPToolBar)PlanView.FindControl("BenefitPlanToolBar");
            if (toolbar == null)
            {
                toolbar = (WLPToolBar)PlanView.FindControl("BenefitPlanToolBarEdit");
            }
            toolbar.Enabled = IsPlanDetailViewMode;
        }
        #endregion

        #region override
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            InitializeControls();
        }
        #endregion
    }
}