﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BenefitPolicySearchPage.aspx.cs" Inherits="WorkLinks.Admin.Benefit.BenefitPolicySearchPage" %>
<%@ Register Src="BenefitPolicySearchControl.ascx" TagName="PolicySearchControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:PolicySearchControl ID="PolicySearchControl1" SearchLocation="Policy" runat="server" />        
    </ContentTemplate>
</asp:Content>

