﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BenefitPlanDetailControl.ascx.cs" Inherits="WorkLinks.Admin.Benefit.BenefitPlanDetailControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Src="BenefitPlanDetailEntryControl.ascx" TagName="BenefitPlanDetailEntryControl" TagPrefix="uc1" %>

<table width="100%">
    <tr>
        <td colspan="2">
            <table>
                <tr>
                    <td style="width: 215px; height: 480px;">
                        <wasp:WLPGrid
                            ID="PlanDetailGrid"
                            runat="server"
                            GridLines="None"
                            AutoAssignModifyProperties="true"
                            AutoGenerateColumns="false"
                            Width="100%"
                            Height="100%"
                            OnDeleteCommand="PlanDetailGrid_DeleteCommand"
                            OnItemCommand="PlanDetailGrid_ItemCommand"
                            OnNeedDataSource="PlanDetailGrid_NeedDataSource"
                            OnPreRender="PlanDetailGrid_PreRender">

                            <ClientSettings EnablePostBackOnRowClick="true" AllowColumnsReorder="false" ReorderColumnsOnClient="false">
                                <Selecting AllowRowSelect="true" />
                                <Scrolling AllowScroll="false" UseStaticHeaders="true" />
                            </ClientSettings>

                            <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
                                <CommandItemTemplate>
                                    <wasp:WLPToolBar ID="PlanDetailToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
                                        <Items>
                                            <wasp:WLPToolBarButton Text="**Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="Add" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add"></wasp:WLPToolBarButton>
                                        </Items>
                                    </wasp:WLPToolBar>
                                </CommandItemTemplate>

                                <Columns>
                                    <wasp:GridDateTimeControl DataField="EffectiveDate" LabelText="**EffectiveDate" SortExpression="EffectiveDate" UniqueName="EffectiveDate" ResourceName="EffectiveDate"></wasp:GridDateTimeControl>
                                    <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>"></wasp:GridButtonControl>
                                </Columns>
                            </MasterTableView>

                            <HeaderContextMenu EnableAutoScroll="true"></HeaderContextMenu>

                        </wasp:WLPGrid>
                    </td>
                    <td style="width: 800px; height: 480px;">
                        <div style="height:100%;width:100%">
                        <uc1:BenefitPlanDetailEntryControl ID="PlanDetailEntryControl" runat="server" Visible="false" />
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
