﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.Benefit
{
    public partial class BenefitPolicyControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private String Action
        {
            get { return Page.RouteData.Values["action"].ToString().ToLower(); }
        }

        private long PolicyId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["policyId"]); }
        }

        public bool IsEditMode
        {
            get { return PolicyView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.Edit); }
        }

        public bool IsInsertMode
        {
            get { return PolicyView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.Insert); }
        }

        public bool IsViewMode
        {
            get { return PolicyView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.ReadOnly); }
        }

        public bool UpdateFlag
        {
            get { return Common.Security.RoleForm.BenefitPolicyEdit.UpdateFlag; }
        }

        public BenefitPolicyPlanCollection PolicyPlanCollection { get; set; }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.BenefitPolicyEdit.ViewFlag);

            WireEvents();

            if (Action == "view")
            {
                this.Page.Title = "Edit a Policy";

                if (!IsPostBack)
                    LoadPolicy();
            }
            else if (Action == "add")
            {
                if (!IsPostBack)
                {
                    this.Page.Title = "Add a Policy";

                    LoadNewPolicy();
                }
                else
                    this.Page.Title = "Edit a Policy";
            }
        }

        protected void LoadPolicy()
        {
            BenefitPolicyCollection collection = new BenefitPolicyCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetBenefitPolicy(PolicyId));
            Data = collection;

            PolicyView.DataBind();
        }

        protected void LoadNewPolicy()
        {
            BenefitPolicyCollection collection = new BenefitPolicyCollection();
            collection.AddNew();
            Data = collection;

            PolicyView.ChangeMode(System.Web.UI.WebControls.FormViewMode.Insert);
            PolicyView.DataBind();
        }

        protected void GetPolicyPlanValues()
        {
            //get the values to insert from the PolicyPlanControl
            PolicyPlanControl.InsertPolicyPlan();
            PolicyPlanCollection = PolicyPlanControl.PolicyPlanCollection;
        }

        protected void WireEvents()
        {
            PolicyView.NeedDataSource += new WLP.Web.UI.Controls.WLPFormView.NeedDataSourceEventHandler(PolicyView_NeedDataSource);
            PolicyView.Inserting += new WLP.Web.UI.Controls.WLPFormView.ItemInsertingEventHandler(PolicyView_Inserting);
            PolicyView.Updating += new WLP.Web.UI.Controls.WLPFormView.ItemUpdatingEventHandler(PolicyView_Updating);
            PolicyView.PreRender += PolicyView_PreRender;
        }
        #endregion

        #region handles updates
        void PolicyView_Inserting(object sender, WLP.Web.UI.Controls.WLPItemInsertingEventArgs e)
        {
            try
            {
                GetPolicyPlanValues();

                BenefitPolicy policy = (BenefitPolicy)e.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.InsertBenefitPolicy(policy, PolicyPlanCollection).CopyTo((BenefitPolicy)Data[policy.Key]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void PolicyView_Updating(object sender, WLP.Web.UI.Controls.WLPItemUpdatingEventArgs e)
        {
            try
            {
                GetPolicyPlanValues();

                BenefitPolicy policy = (BenefitPolicy)e.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.UpdateBenefitPolicy(policy, PolicyPlanCollection).CopyTo((BenefitPolicy)Data[policy.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region event handlers
        void PolicyView_NeedDataSource(object sender, WLP.Web.UI.Controls.WLPNeedDataSourceEventArgs e)
        {
            PolicyView.DataSource = Data;
        }

        private void PolicyView_PreRender(object sender, EventArgs e)
        {
            PolicyPlanControl.PolicyId = ((BenefitPolicyCollection)Data)[0].BenefitPolicyId;
            PolicyPlanControl.IsViewMode = IsViewMode;
            PolicyPlanControl.LoadPolicyPlan();
        }
        #endregion
    }
}