﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HumanResources/Employee.Master" AutoEventWireup="true" CodeBehind="BenefitPolicyPage.aspx.cs" Inherits="WorkLinks.Admin.Benefit.BenefitPolicyPage" %>
<%@ Register Src="BenefitPolicyControl.ascx" TagName="PolicyControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <uc1:PolicyControl ID="PolicyControl" runat="server" />
</asp:Content>

