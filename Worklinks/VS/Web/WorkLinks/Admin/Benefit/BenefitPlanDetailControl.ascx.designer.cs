﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace WorkLinks.Admin.Benefit {
    
    
    public partial class BenefitPlanDetailControl {
        
        /// <summary>
        /// PlanDetailGrid control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPGrid PlanDetailGrid;
        
        /// <summary>
        /// PlanDetailToolBar control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPToolBar PlanDetailToolBar;
        
        /// <summary>
        /// PlanDetailEntryControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WorkLinks.Admin.Benefit.BenefitPlanDetailEntryControl PlanDetailEntryControl;
    }
}
