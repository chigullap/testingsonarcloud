﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.Benefit
{
    public partial class BenefitPlanDetailControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private long _benefitPlanId = -1;
        private long _benefitPlanDetailId = -1;
        private const String _benefitPlanIdKey = "_benefitPlanIdKey";
        private bool _isRowClickFired = false;
        #endregion

        #region properties
        public long BenefitPlanId
        {
            get
            {
                if (_benefitPlanId == -1)
                    _benefitPlanId = Convert.ToInt64(ViewState[_benefitPlanIdKey]);

                return _benefitPlanId;
            }
            set
            {
                _benefitPlanId = value;
                ViewState[_benefitPlanIdKey] = _benefitPlanId;
            }
        }

        public long BenefitPlanDetailId
        {
            get { return _benefitPlanDetailId; }
            set { _benefitPlanDetailId = value; }
        }

        public bool AddFlag
        {
            get { return Common.Security.RoleForm.BenefitPlanEdit.AddFlag; }
        }

        public bool DeleteFlag
        {
            get { return Common.Security.RoleForm.BenefitPlanEdit.DeleteFlag; }
        }

        public bool IsInsertMode
        {
            get { return PlanDetailGrid.IsInsertMode; }
        }

        public bool IsViewMode
        {
            get { return PlanDetailGrid.IsViewMode; }
        }

        public bool IsPlanViewMode
        {
            get { return ((BenefitPlanControl)this.Parent.Parent).IsViewMode; }
        }

        public bool IsDetailEntryViewMode
        {
            get { return PlanDetailEntryControl.IsViewMode; }
        }

        public bool IsRowClickFired
        {
            get { return _isRowClickFired; }
            set { _isRowClickFired = value; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();
        }

        public void Initialize()
        {
            //find the PlanDetailGrid
            WLP.Web.UI.Controls.WLPGrid grid = (WLP.Web.UI.Controls.WLPGrid)this.FindControl("PlanDetailGrid");

            //hide the delete image in the rows based on permissions
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }

        public void LoadPlanDetail()
        {
            Data = Common.ServiceWrapper.HumanResourcesClient.GetBenefitPlanDetail(BenefitPlanId, -1); //-1 gets everything
        }

        protected void LoadPlanDetailEntryControl()
        {
            //load the PlanDetailEntryControl
            PlanDetailEntryControl.Visible = true;
            PlanDetailEntryControl.BenefitPlanId = BenefitPlanId;
            PlanDetailEntryControl.BenefitPlanDetailId = BenefitPlanDetailId;
            PlanDetailEntryControl.LoadPlanDetailEntry();
        }

        protected void WireEvents()
        {
            this.PlanDetailEntryControl.PlanUpdated += BenefitPlanDetailEntryControl_PlanUpdated;
            this.PlanDetailEntryControl.PlanCancelled += BenefitPlanDetailEntryControl_PlanCancelled;
        }

        private void InitializeControls()
        {
            //this code makes sure the RadToolbar is set to the proper status before/during/after update/insert operations
            GridCommandItem cmdItem = (GridCommandItem)PlanDetailGrid.MasterTableView.GetItems(GridItemType.CommandItem)[0];
            RadToolBar toolBar = (RadToolBar)cmdItem.FindControl("PlanDetailToolBar");
            if (toolBar == null)
            {
                toolBar = (RadToolBar)cmdItem.FindControl("PlanDetailToolBarEdit");
            }
            toolBar.Enabled = IsPlanViewMode && IsDetailEntryViewMode;
        }
        #endregion

        #region event handlers
        private void BenefitPlanDetailEntryControl_PlanUpdated(object sender, EventArgs e)
        {
            //refresh the PlanDetailGrid
            LoadPlanDetail();
            PlanDetailGrid.Rebind();
        }

        private void BenefitPlanDetailEntryControl_PlanCancelled(object sender, EventArgs e)
        {
            PlanDetailEntryControl.Visible = false;
        }

        protected void PlanDetailGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteBenefitPlanDetail((BenefitPlanDetail)e.Item.DataItem);

                //refresh the PlanDetailGrid
                LoadPlanDetail();
                PlanDetailGrid_NeedDataSource(null, null);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void PlanDetailGrid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Add")
            {
                PlanDetailGrid.SelectedIndexes.Clear();
                LoadPlanDetailEntryControl();
            }
            else if (e.CommandName == "RowClick")
            {
                IsRowClickFired = true;
                BenefitPlanDetailId = ((BenefitPlanDetailCollection)Data)[e.Item.ItemIndex].BenefitPlanDetailId;
                LoadPlanDetailEntryControl();
            }
            else
                PlanDetailEntryControl.Visible = false;
        }

        protected void PlanDetailGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            PlanDetailGrid.DataSource = Data;
        }

        protected void PlanDetailGrid_PreRender(object sender, EventArgs e)
        {
            if (!IsRowClickFired)
            {
                if (IsViewMode && PlanDetailGrid.MasterTableView.Items.Count > 0)
                {
                    BenefitPlanDetail data = (BenefitPlanDetail)PlanDetailGrid.MasterTableView.Items[0].DataItem;
                    if (data != null)
                    {
                        PlanDetailGrid.MasterTableView.Items[0].Selected = true; //select the first row
                        BenefitPlanDetailId = data.BenefitPlanDetailId;
                        LoadPlanDetailEntryControl();
                    }
                }
            }
        }
        #endregion

        #region override
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            InitializeControls();
        }
        #endregion
    }
}
