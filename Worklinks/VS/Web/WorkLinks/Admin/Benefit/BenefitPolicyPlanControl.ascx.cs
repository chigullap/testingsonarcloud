﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.Benefit
{
    public partial class BenefitPolicyPlanControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private long _policyId = -1;
        private const String _policyIdKey = "_policyIdKey";
        private BenefitPolicyPlanCollection _policyPlanCollection = null;
        private CodeCollection _checkedPlans = null;
        #endregion

        #region properties
        public long PolicyId
        {
            get
            {
                if (_policyId == -1)
                    _policyId = Convert.ToInt64(ViewState[_policyIdKey]);

                return _policyId;
            }
            set
            {
                _policyId = value;
                ViewState[_policyIdKey] = _policyId;
            }
        }

        public BenefitPolicyPlanCollection PolicyPlanCollection
        {
            get
            {
                if (_policyPlanCollection == null)
                    _policyPlanCollection = new BenefitPolicyPlanCollection();

                return _policyPlanCollection;
            }
        }

        public CodeCollection CheckedPlans
        {
            get
            {
                if (_checkedPlans == null)
                    _checkedPlans = new CodeCollection();

                return _checkedPlans;
            }
        }

        public bool IsViewMode { get; set; }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void LoadPolicyPlan()
        {
            BenefitPolicyPlanCollection collection = new BenefitPolicyPlanCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetBenefitPolicyPlan(PolicyId));
            Data = collection;

            PolicyPlan.DataBind();
        }

        public void InsertPolicyPlan()
        {
            CodeCollection policyPlan = (CodeCollection)PolicyPlan.Value;

            for (int i = 0; i < policyPlan.Count; i++)
            {
                PolicyPlanCollection.Add(new BenefitPolicyPlan
                {
                    BenefitPolicyPlanId = -i - 1,
                    BenefitPlanId = Convert.ToInt64(policyPlan[i].Code)
                });
            }
        }
        #endregion

        #region event handlers
        protected void PolicyPlan_NeedDataSource(object sender, EventArgs e)
        {
            BenefitPlanSearchCollection planSearchCollection = Common.ServiceWrapper.HumanResourcesClient.GetBenefitPlanReport(null);
            CodeCollection plans = new CodeCollection();

            foreach (BenefitPlanSearch planSearch in planSearchCollection)
            {
                plans.Add(new CodeObject()
                {
                    Code = planSearch.BenefitPlanId.ToString(),
                    Description = planSearch.BenefitPlanDescriptionField,
                    LanguageCode = LanguageCode
                });
            }

            foreach (CodeObject code in plans)
            {
                foreach (BenefitPolicyPlan policyPlan in (BenefitPolicyPlanCollection)Data)
                {
                    if (policyPlan.BenefitPlanId.ToString() == code.Code)
                        CheckedPlans.Add(new CodeObject() { Code = code.Code, Description = code.Description });
                }
            }

            if (IsViewMode)
                plans = CheckedPlans;

            ((ICodeControl)sender).DataSource = plans;
            ((CheckedListBoxControl)sender).Value = CheckedPlans;
        }
        #endregion
    }
}