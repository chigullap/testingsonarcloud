﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BenefitPolicyPlanControl.ascx.cs" Inherits="WorkLinks.Admin.Benefit.BenefitPolicyPlanControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<style>
    .RadListBox .rlbGroup {
        border-width: 0 !important;
    }
</style>

<table width="100%">
    <tr>
        <td style="padding: 10px 34px 20px 34px;">
            <wasp:WLPLabel ID="BenefitPlansLabel" runat="server" Text="**Benefit Plans" Font-Bold="true" ResourceName="BenefitPlans" />

            <wasp:CheckedListBoxControl
                ID="PolicyPlan"
                runat="server"
                LabelText=""
                ListBoxHeight="215px"
                ListBoxWidth="640px"
                ResourceName="BenefitPolicyPlan"
                Type="PolicyPlan"
                OnDataBinding="PolicyPlan_NeedDataSource"
                Value='<%# CheckedPlans %>'
                ReadOnly='<%# IsViewMode %>' />
        </td>
    </tr>
</table>
