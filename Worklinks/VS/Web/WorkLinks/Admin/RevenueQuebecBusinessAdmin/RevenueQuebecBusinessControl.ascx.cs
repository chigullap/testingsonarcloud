﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WLP.Web.UI;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.RevenueQuebecBusinessAdmin
{
    public partial class RevenueQuebecBusinessControl : WLPUserControl
    {
        #region Properties

        private string CurrentBusinessId { get; set; }
        private string CurrentFileSequenceNumber { get; set; }
        private string CurrentFormCode { get; set; }
        private string CurrentPeriodSubdivision { get; set; }
        private RevenueQuebecBusiness OriginalRevenueQuebecBusiness
        {
            get
            {
                return (RevenueQuebecBusiness)ViewState["OriginalRevenueQuebecBusiness"];
            }
            set
            {
                ViewState["OriginalRevenueQuebecBusiness"] = value;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetAuthorized(Common.Security.RoleForm.RevenueQuebecBusinessForm.ViewFlag);
                LoadData();
            }
        }

        protected void LoadData()
        {
            //RevenueQuebecBusiness revenueQuebecBusiness = Common.ServiceWrapper.CodeClient.GetRevenueQuebecBusiness();
            OriginalRevenueQuebecBusiness = Common.ServiceWrapper.CodeClient.GetRevenueQuebecBusiness();
            BusinessId.Value = OriginalRevenueQuebecBusiness.BusinessId;
            FileSequenceNumber.Value = OriginalRevenueQuebecBusiness.FileSequenceNumber;
            FormCode.Value = "";
            PeriodSubdivision.Value = "";
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            // Dont proceed if validation failed
            if (!Page.IsValid)
            {
                return;
            }

            bool proceedUpdate = true;

            if (OriginalRevenueQuebecBusiness != null)
            {
                if (String.Equals(OriginalRevenueQuebecBusiness.BusinessId, CurrentBusinessId) &&
                    String.Equals(OriginalRevenueQuebecBusiness.FileSequenceNumber, CurrentFileSequenceNumber))
                {
                    proceedUpdate = false;
                }
            }

            if (proceedUpdate)
            {
                update();
            }
        }


        protected void update()
        {
            try
            {
                OriginalRevenueQuebecBusiness.BusinessId = CurrentBusinessId;
                OriginalRevenueQuebecBusiness.FileSequenceNumber = CurrentFileSequenceNumber;

                OriginalRevenueQuebecBusiness = Common.ServiceWrapper.CodeClient.UpdateRevenueQuebecBusiness(OriginalRevenueQuebecBusiness);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            LoadData();
        }


        #region Validation

        protected void ValidateBusinessId(object source, ServerValidateEventArgs args)
        {
            CustomValidator customValidator = (CustomValidator)source;
            String errorMessage = null;

            int businessIdInteger = -1;
            string firstTwoCharacters = null;           // First 2 characters validation
            int firstTwoCharactersInteger = -1;         // First 2 characters validation
            bool firstTwoCharactersValidation = false;  // First 2 characters validation


            CurrentBusinessId = args.Value;

            // "Business Id" has no value so raise error
            if (String.IsNullOrWhiteSpace(CurrentBusinessId))
            {
                errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "RevenueQuebecBusinessIdLengthError");
                //errorMessage = "The Business Id must be 10 digits in length";
                args.IsValid = false;
                customValidator.ErrorMessage = errorMessage;
                return;
            }

            CurrentBusinessId = CurrentBusinessId.Trim();

            // "Business Id" has no value so raise error
            if (String.IsNullOrWhiteSpace(CurrentBusinessId))
            {
                errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "RevenueQuebecBusinessIdLengthError");
                //errorMessage = "The Business Id must be 10 digits in length";
                args.IsValid = false;
                customValidator.ErrorMessage = errorMessage;
                return;
            }

            // "Business Id" is not 10 digits in length so raise error
            if (CurrentBusinessId.Length != 10)
            {
                errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "RevenueQuebecBusinessIdLengthError");
                //errorMessage = "The Business Id must be 10 digits in length";
                args.IsValid = false;
                customValidator.ErrorMessage = errorMessage;
                return;
            }

            // "Business Id" is not a number so raise error
            if (!Int32.TryParse(CurrentBusinessId, out businessIdInteger))
            {
                errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "RevenueQuebecBusinessIdLengthError");
                //errorMessage = "The Business Id must be 10 digits in length";
                args.IsValid = false;
                customValidator.ErrorMessage = errorMessage;
                return;
            }

            firstTwoCharacters = CurrentBusinessId.Substring(0, 2);
            firstTwoCharactersValidation = false;

            // The first 2 characters must be a number and it must be the following: 10, 12, 40 – 59
            if (Int32.TryParse(firstTwoCharacters, out firstTwoCharactersInteger))
            {
                if (firstTwoCharactersInteger == 10)
                {
                    firstTwoCharactersValidation = true;
                }
                else if (firstTwoCharactersInteger == 12)
                {
                    firstTwoCharactersValidation = true;
                }
                else if (firstTwoCharactersInteger >= 40 && firstTwoCharactersInteger <= 59)
                {
                    firstTwoCharactersValidation = true;
                }
            }

            // The first 2 characters validation failed
            if (!firstTwoCharactersValidation)
            {
                errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "RevenueQuebecBusinessIdFirstTwoDigitError");
                //errorMessage = "The first two digits of the Business Id must be the following: 10, 12, 40 – 59";
                args.IsValid = false;
                customValidator.ErrorMessage = errorMessage;
                return;
            }

            // Validation of Modulus 11 Check Digit failed so raise error
            if (!Common.ServiceWrapper.CodeClient.ValidateModulus11CheckDigit(CurrentBusinessId))
            {
                errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "RevenueQuebecBusinessIdModulus11CheckDigitError");
                //errorMessage = "The first two digits of the Business Id must be the following: 10, 12, 40 – 59";
                args.IsValid = false;
                customValidator.ErrorMessage = errorMessage;
                return;
            }
        }

        protected void ValidateFileSequenceNumber(object source, ServerValidateEventArgs args)
        {
            CustomValidator customValidator = (CustomValidator)source;
            String errorMessage = null;

            int fileSequenceNumberLength = -1;
            string firstTwoCharacters = null;
            string lastFourCharacters = null;
            int lastFourCharactersInteger = -1;


            CurrentFileSequenceNumber = args.Value;

            if (String.IsNullOrWhiteSpace(CurrentFileSequenceNumber))
            {
                errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "RevenueQuebecFileSequenceNumberFirstTwoCharacterError");
                //errorMessage = "Position 11 and 12 must be 'RS'";
                args.IsValid = false;
                customValidator.ErrorMessage = errorMessage;
                return;
            }

            CurrentFileSequenceNumber = CurrentFileSequenceNumber.Trim();

            fileSequenceNumberLength = CurrentFileSequenceNumber.Length;

            if (fileSequenceNumberLength < 2)
            {
                errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "RevenueQuebecFileSequenceNumberFirstTwoCharacterError");
                //errorMessage = "Position 11 and 12 must be 'RS'";
                args.IsValid = false;
                customValidator.ErrorMessage = errorMessage;
                return;
            }

            firstTwoCharacters = CurrentFileSequenceNumber.Substring(0, 2);

            if (!String.Equals(firstTwoCharacters, "RS"))
            {
                errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "RevenueQuebecFileSequenceNumberFirstTwoCharacterError");
                //errorMessage = "Position 11 and 12 must be 'RS'";
                args.IsValid = false;
                customValidator.ErrorMessage = errorMessage;
                return;
            }

            if (fileSequenceNumberLength != 6)
            {
                errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "RevenueQuebecFileSequenceNumberLengthError");
                //errorMessage = "The file sequence number must be 4 digits";
                args.IsValid = false;
                customValidator.ErrorMessage = errorMessage;
                return;
            }

            lastFourCharacters = CurrentFileSequenceNumber.Substring(2);
            lastFourCharactersInteger = -1;

            if (!Int32.TryParse(lastFourCharacters, out lastFourCharactersInteger))
            {
                errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "RevenueQuebecFileSequenceNumberNotNumericError");
                //errorMessage = "The file sequence number must be numeric";
                args.IsValid = false;
                customValidator.ErrorMessage = errorMessage;
                return;
            }

            if (lastFourCharactersInteger < 1 || lastFourCharactersInteger > 9999)
            {
                errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "RevenueQuebecFileSequenceNumberRangeError");
                //errorMessage = "The file sequence number must be between 0001 and 9999";
                args.IsValid = false;
                customValidator.ErrorMessage = errorMessage;
                return;
            }
        }

        protected void ValidateFormCode(object source, ServerValidateEventArgs args)
        {
            CustomValidator customValidator = (CustomValidator)source;
            String errorMessage = null;


            CurrentFormCode = args.Value;

            // "Form Code" has no value so dont proceed
            if (String.IsNullOrWhiteSpace(CurrentFormCode))
            {
                return;
            }

            CurrentFormCode = CurrentFormCode.Trim();

            // "Form Code" has no value so dont proceed
            if (String.IsNullOrWhiteSpace(CurrentFormCode))
            {
                return;
            }

            // "Form Code" has value but its not 232 and not 212 so raise error
            if (!String.Equals(CurrentFormCode, "232") && !String.Equals(CurrentFormCode, "212"))
            {
                errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "RevenueQuebecFormCodeInvalidError");
                //errorMessage = "Invalid form code";
                args.IsValid = false;
                customValidator.ErrorMessage = errorMessage;
                return;
            }
        }

        protected void ValidatePeriodSubdivision(object source, ServerValidateEventArgs args)
        {
            CustomValidator customValidator = (CustomValidator)source;
            String errorMessage = null;


            CurrentPeriodSubdivision = args.Value;

            // "Period Subdivision" has no value so dont proceed
            if (String.IsNullOrWhiteSpace(CurrentPeriodSubdivision))
            {
                return;
            }

            CurrentPeriodSubdivision = CurrentPeriodSubdivision.Trim();

            // "Period Subdivision" has no value so dont proceed
            if (String.IsNullOrWhiteSpace(CurrentPeriodSubdivision))
            {
                return;
            }

            // "Form Code" has no value so dont proceed
            if (String.IsNullOrWhiteSpace(CurrentFormCode))
            {
                return;
            }

            if (String.Equals(CurrentFormCode, "232"))
            {
                if (String.Equals(CurrentPeriodSubdivision, "00"))
                {
                    errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "RevenueQuebecPeriodSubdivisionInvalidError");
                    //errorMessage = "Invalid period subdivision";
                    args.IsValid = false;
                    customValidator.ErrorMessage = errorMessage;
                    return;
                }
            }
            else if (String.Equals(CurrentFormCode, "212"))
            {
                if (String.Equals(CurrentPeriodSubdivision, "01"))
                {
                    errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "RevenueQuebecPeriodSubdivisionInvalidError");
                    //errorMessage = "Invalid period subdivision";
                    args.IsValid = false;
                    customValidator.ErrorMessage = errorMessage;
                    return;
                }
            }
        }

        #endregion
    }
}