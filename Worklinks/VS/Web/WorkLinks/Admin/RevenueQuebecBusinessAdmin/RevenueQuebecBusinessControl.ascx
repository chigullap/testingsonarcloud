﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RevenueQuebecBusinessControl.ascx.cs" Inherits="WorkLinks.Admin.RevenueQuebecBusinessAdmin.RevenueQuebecBusinessControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Panel ID="Panel1" runat="server">

    <table width="100%" border="0">
        <tr>
            <td width="25%">
                <wasp:TextBoxControl ID="BusinessId" ResourceName="BusinessId" ReadOnly="false" TabIndex="010" runat="server"></wasp:TextBoxControl>
            </td>
            <td width="75%">
                <asp:CustomValidator ID="BusinessIdValidator" runat="server" ForeColor="Red" ControlToValidate="BusinessId" OnServerValidate="ValidateBusinessId"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td width="25%">
                <wasp:TextBoxControl ID="FileSequenceNumber" ResourceName="FileSequenceNumber" ReadOnly="false" TabIndex="020" runat="server"></wasp:TextBoxControl>
            </td>
            <td width="75%">
                <asp:CustomValidator ID="FileSequenceNumberValidator" runat="server" ForeColor="Red" ControlToValidate="FileSequenceNumber" OnServerValidate="ValidateFileSequenceNumber"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td width="25%">
                <wasp:TextBoxControl ID="FormCode" ResourceName="FormCode" ReadOnly="false" TabIndex="030" runat="server"></wasp:TextBoxControl>
            </td>
            <td width="75%">
                <asp:CustomValidator ID="FormCodeValidator" runat="server" ForeColor="Red" ControlToValidate="FormCode" OnServerValidate="ValidateFormCode"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td width="25%">
                <wasp:TextBoxControl ID="PeriodSubdivision" ResourceName="PeriodSubdivision" ReadOnly="false" TabIndex="040" runat="server"></wasp:TextBoxControl>
            </td>
            <td width="75%">
                <asp:CustomValidator ID="PeriodSubdivisionValidator" runat="server" ForeColor="Red" ControlToValidate="PeriodSubdivision" OnServerValidate="ValidatePeriodSubdivision"></asp:CustomValidator>
            </td>
        </tr>
    </table>

    <div>
        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/Update.gif" runat="server" Text="Update" ResourceName="btnUpdate" OnClick="btnUpdate_Click" />
        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/Cancel.gif" runat="server" Text="Cancel" ResourceName="btnCancel" CausesValidation="false" OnClick="btnCancel_Click" />
    </div>

</asp:Panel>
