﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="YearEndProcessingPage.aspx.cs" Inherits="WorkLinks.Admin.YearEnd.YearEndProcessingPage" %>
<%@ Register Src="YearEndProcessingControl.ascx" TagName="YearEndProcessingControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:YearEndProcessingControl ID="YearEndProcessingControl1" runat="server" />
</asp:Content>