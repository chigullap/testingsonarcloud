﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="YearEndControl.ascx.cs" Inherits="WorkLinks.Admin.YearEnd.YearEndControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="SearchPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SearchPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="SearchPanel" runat="server">
    <table width="100%">
        <tr valign="top">
            <td>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnRefresh">
                    <div class="SearchCriteriaButtons">
                        <wasp:WLPButton ID="btnRefresh" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" runat="server" Text="Refresh" ResourceName="btnRefresh" OnClientClicked="clear" OnClick="btnRefresh_Click" />
                    </div>
                </asp:Panel>
            </td>
        </tr>
        <tr valign="top">
            <td>
                <wasp:WLPToolBar ID="YearEndToolBar" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="YearEndToolBar_ButtonClick" OnClientLoad="YearEndToolBar_init" OnClientButtonClicking="checkGeneratePayrollConfirmation" Style="z-index: 1900">
                    <Items>
                        <wasp:WLPToolBarButton OnPreRender="CreateCurrentYEToolBar_PreRender" Text="*CreateCurrentYE" CommandName="CreateCurrentYE" ResourceName="CreateCurrentYE" />
                        <wasp:WLPToolBarButton OnPreRender="CloseVersionYEToolBar_PreRender" Text="*CloseVersionYE" CommandName="CloseVersionYE" ResourceName="CloseVersionYE" />
                        <wasp:WLPToolBarButton OnPreRender="CloseCurrentYEToolBar_PreRender" Text="*CloseCurrentYE" CommandName="CloseCurrentYE" ResourceName="CloseCurrentYE" />
                        <wasp:WLPToolBarDropDown runat="server" OnPreRender="MassPrint_PreRender" Text="*MassPrint" CommandName="MassPrint" ResourceName="MassPrint" Enabled="false">
	                        <Buttons>
		                        <wasp:WLPToolBarButton OnPreRender="MassT4PrintToolBar_PreRender" Text="*MassT4Print" onclick="openMassT4Print();" runat="server" CommandName="MassT4Print" ResourceName="MassT4Print" />
                                <wasp:WLPToolBarButton OnPreRender="MassR1PrintToolBar_PreRender" Text="*MassR1Print" onclick="openMassR1Print();" runat="server" CommandName="MassR1Print" ResourceName="MassR1Print" />
                                <wasp:WLPToolBarButton OnPreRender="MassT4APrintToolBar_PreRender" Text="*MassT4APrint" onclick="openMassT4APrint();" runat="server" CommandName="MassT4APrint" ResourceName="MassT4APrint" />
                                <wasp:WLPToolBarButton OnPreRender="MassR2PrintToolBar_PreRender" Text="*MassR2Print" onclick="openMassR2Print();" runat="server" CommandName="MassR2Print" ResourceName="MassR2Print" />
                                <wasp:WLPToolBarButton OnPreRender="MassT4RSPPrintToolBar_PreRender" Text="*MassT4RSPPrint" onclick="openMassT4RSPPrint();" runat="server" CommandName="MassT4RSPPrint" ResourceName="MassT4RSPPrint" />
                                <wasp:WLPToolBarButton OnPreRender="MassT4ARCAPrintToolBar_PreRender" Text="*MassT4ARCAPrint" onclick="openMassT4ARCAPrint();" runat="server" CommandName="MassT4ARCAPrint" ResourceName="MassT4ARCAPrint" />
                                <wasp:WLPToolBarButton OnPreRender="MassNR4PrintToolBar_PreRender" Text="*MassNR4Print" onclick="openMassNR4Print();" runat="server" CommandName="MassNR4Print" ResourceName="MassNR4Print" />
	                        </Buttons>
                        </wasp:WLPToolBarDropDown>
                        <wasp:WLPToolBarDropDown runat="server" OnPreRender="MassFile_PreRender" Text="*MassFile" CommandName="MassFile" ResourceName="MassFile" Enabled="false">
	                        <Buttons>
                                <wasp:WLPToolBarButton OnPreRender="MassFileAll_PreRender" Text="*MassFileAll" onclick="openMassFileAll();" runat="server" CommandName="MassFileAll" ResourceName="MassFileAll" />
                                <%--<wasp:WLPToolBarButton OnPreRender="MassT4FileToolBar_PreRender" Text="*MassT4File" onclick="openMassT4File();" runat="server" CommandName="MassT4File" ResourceName="MassT4File" />
                                <wasp:WLPToolBarButton OnPreRender="MassR1FileToolBar_PreRender" Text="*MassR1File" onclick="openMassR1File();" runat="server" CommandName="MassR1File" ResourceName="MassR1File" />
                                <wasp:WLPToolBarButton OnPreRender="MassT4AFileToolBar_PreRender" Text="*MassT4AFile" onclick="openMassT4AFile();" runat="server" CommandName="MassT4AFile" ResourceName="MassT4AFile" />--%>
	                        </Buttons>
                        </wasp:WLPToolBarDropDown>
                        <wasp:WLPToolBarDropDown runat="server" OnPreRender="Export_PreRender" Text="*Export" CommandName="Export" ResourceName="Export" Enabled="false">
	                        <Buttons>
                                <wasp:WLPToolBarButton OnPreRender="T4Export_PreRender" Text="*T4Export" onclick="openT4Export();" runat="server" CommandName="T4Export" ResourceName="T4Export" />
                                <wasp:WLPToolBarButton OnPreRender="R1Export_PreRender" Text="*R1Export" onclick="openR1Export();" runat="server" CommandName="R1Export" ResourceName="R1Export" />
                                <wasp:WLPToolBarButton OnPreRender="T4AExport_PreRender" Text="*T4AExport" onclick="openT4AExport();" runat="server" CommandName="T4AExport" ResourceName="T4AExport" />
                                <wasp:WLPToolBarButton OnPreRender="R2Export_PreRender" Text="*R2Export" onclick="openR2Export();" runat="server" CommandName="R2Export" ResourceName="R2Export" />
                                <wasp:WLPToolBarButton OnPreRender="T4RSPExport_PreRender" Text="*T4RSPExport" onclick="openT4RSPExport();" runat="server" CommandName="T4RSPExport" ResourceName="T4RSPExport" />
                                <wasp:WLPToolBarButton OnPreRender="T4ARCAExport_PreRender" Text="*T4ARCAExport" onclick="openT4ARCAExport();" runat="server" CommandName="T4ARCAExport" ResourceName="T4ARCAExport" />
                                <wasp:WLPToolBarButton OnPreRender="NR4Export_PreRender" Text="*NR4Export" onclick="openNR4Export();" runat="server" CommandName="NR4Export" ResourceName="NR4Export" />
	                        </Buttons>
                        </wasp:WLPToolBarDropDown>
                        <wasp:WLPToolBarDropDown runat="server" OnPreRender="Summary_PreRender" Text="*Summary" CommandName="Summary" ResourceName="Summary" Enabled="false">
	                        <Buttons>
                                <wasp:WLPToolBarButton OnPreRender="T4Summary_PreRender" Text="*T4Summary" onclick="openMassT4SummaryPrint();" runat="server" CommandName="T4Summary" ResourceName="T4Summary" />
                                <wasp:WLPToolBarButton OnPreRender="R1Summary_PreRender" Text="*R1Summary" onclick="openR1SummaryOfSourceDeductions();" runat="server" CommandName="R1SummaryOfSourceDeductions" ResourceName="R1SummaryOfSourceDeductions" />
                                <wasp:WLPToolBarButton OnPreRender="T4ASummary_PreRender" Text="*T4ASummary" onclick="openMassT4ASummaryPrint();" runat="server" CommandName="T4ASummary" ResourceName="T4ASummary" />
		                        <wasp:WLPToolBarButton OnPreRender="R2Summary_PreRender" Text="*R2Summary" onclick="openR2Summary();" runat="server" CommandName="R2Summary" ResourceName="R2Summary" />
                                <wasp:WLPToolBarButton OnPreRender="T4RSPSummary_PreRender" Text="*T4RSPSummary" onclick="openT4RSPSummary();" runat="server" CommandName="T4RSPSummary" ResourceName="T4RSPSummary" />
                                <wasp:WLPToolBarButton OnPreRender="T4ARCASummary_PreRender" Text="*T4ARCASummary" onclick="openT4ARCASummary();" runat="server" CommandName="T4ARCASummary" ResourceName="T4ARCASummary" />
		                        <wasp:WLPToolBarButton OnPreRender="NR4Summary_PreRender" Text="*NR4Summary" onclick="openNR4Summary();" runat="server" CommandName="NR4Summary" ResourceName="NR4Summary" />
                            </Buttons>
                        </wasp:WLPToolBarDropDown>
                        <wasp:WLPToolBarButton CommandName="ReportMenuButton" OnPreRender="ReportMenuButton_PreRender">
                            <ItemTemplate>
                                <telerik:RadMenu ID="ReportRadMenu" ClickToOpen="true" runat="server" EnableRoundedCorners="true" OnClientItemClicked="reportItemClicked" OnClientLoad="reportOnInit" Style="z-index: 1900">
                                    <Items>
                                        <wasp:WLPMenuItem runat="server" Text="*Reports" OnPreRender="ReportsToolBar_PreRender" ResourceName="ReportsMenu">
                                            <Items>
                                                <wasp:WLPMenuItem runat="server" Text="*StatDeductionExceptionReport" ResourceName="StatDeductionExceptionReport" OnPreRender="StatDeductionExceptionReport_PreRender">
                                                    <Items>
                                                        <wasp:WLPMenuItem Value="StatDeductionExceptionReport_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableStatDeductionExceptionReportButton %>' />
                                                        <wasp:WLPMenuItem Value="StatDeductionExceptionReport_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableExceptionReportMenuItem %>' />
                                                    </Items>
                                                </wasp:WLPMenuItem>
                                                <wasp:WLPMenuItem runat="server" Text="*NameAddressExceptionReport" ResourceName="NameAddressExceptionReport" OnPreRender="NameAddressExceptionReport_PreRender">
                                                    <Items>
                                                        <wasp:WLPMenuItem Value="NameAddressException_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableExceptionReportMenuItem %>' />
                                                        <wasp:WLPMenuItem Value="NameAddressException_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableExceptionReportMenuItem %>' />
                                                    </Items>
                                                </wasp:WLPMenuItem>
                                                <wasp:WLPMenuItem runat="server" Text="*YearEndWCBReport" ResourceName="YearEndWCBReportMenu" OnPreRender="YearEndWCBReportMenuItem_PreRender">
                                                    <Items>
                                                        <wasp:WLPMenuItem Value="YearEndWCBReport_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableYearEndWCBReportMenuItem %>' />
                                                    </Items>
                                                </wasp:WLPMenuItem>
                                                <wasp:WLPMenuItem runat="server" Text="*YearEndPaycodeSummaryReport" ResourceName="YearEndPaycodeSummaryReportMenu" OnPreRender="YearEndPaycodeSummaryReportMenuItem_PreRender">
                                                    <Items>
                                                        <wasp:WLPMenuItem Value="YearEndPaycodeSummaryReport_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableYearEndPaycodeSummaryMenuItem %>' />
                                                    </Items>
                                                </wasp:WLPMenuItem>
                                                <wasp:WLPMenuItem runat="server" Text="*YearEndAdjustmentsAudit" ResourceName="YearEndAdjustmentsAuditMenu" OnPreRender="YearEndAdjustmentsAuditMenuItem_PreRender">
                                                    <Items>
                                                        <wasp:WLPMenuItem Value="MassYeAdjustAudit_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableYearEndAdjustmentsAuditMenuItem %>' />
                                                    </Items>
                                                </wasp:WLPMenuItem>
                                                <wasp:WLPMenuItem runat="server" Text="*YearEndAdjustmentReport" ResourceName="YearEndAdjustmentReportMenu" OnPreRender="YearEndAdjustmentReportMenuItem_PreRender">
                                                    <Items>
                                                        <wasp:WLPMenuItem Value="YearEndAdjustmentReport_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableYearEndAdjustmentReportMenuItem %>' />
                                                    </Items>
                                                </wasp:WLPMenuItem>
                                                <wasp:WLPMenuItem runat="server" Text="*YearEndHealthTaxSummary" ResourceName="YearEndHealthTaxSummaryMenu" OnPreRender="YearEndHealthTaxSummaryMenuItem_PreRender">
                                                    <Items>
                                                        <wasp:WLPMenuItem Value="YearEndHealthTaxSummary_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableYearEndHealthTaxSummaryMenuItem %>' />
                                                    </Items>
                                                </wasp:WLPMenuItem>
                                                <wasp:WLPMenuItem runat="server" Text="*YearEndR1Detail" ResourceName="YearEndR1DetailMenu" OnPreRender="YearEndR1DetailMenuItem_PreRender">
                                                    <Items>
                                                        <wasp:WLPMenuItem Value="YearEndR1Detail_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableYearEndR1DetailMenuItem %>' />
                                                    </Items>
                                                </wasp:WLPMenuItem>
                                                <wasp:WLPMenuItem runat="server" Text="*YearEndR2Detail" ResourceName="YearEndR2DetailMenu" OnPreRender="YearEndR2DetailMenuItem_PreRender">
                                                    <Items>
                                                        <wasp:WLPMenuItem Value="YearEndR2Detail_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableYearEndR2DetailMenuItem %>' />
                                                    </Items>
                                                </wasp:WLPMenuItem>
                                                <wasp:WLPMenuItem runat="server" Text="*YearEndT4Detail" ResourceName="YearEndT4DetailMenu" OnPreRender="YearEndT4DetailMenuItem_PreRender">
                                                    <Items>
                                                        <wasp:WLPMenuItem Value="YearEndT4Detail_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableYearEndT4DetailMenuItem %>' />
                                                    </Items>
                                                </wasp:WLPMenuItem>
                                                <wasp:WLPMenuItem runat="server" Text="*YearEndT4ADetail" ResourceName="YearEndT4ADetailMenu" OnPreRender="YearEndT4ADetailMenuItem_PreRender">
                                                    <Items>
                                                        <wasp:WLPMenuItem Value="YearEndT4ADetail_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableYearEndT4ADetailMenuItem %>' />
                                                    </Items>
                                                </wasp:WLPMenuItem>
                                            </Items>
                                        </wasp:WLPMenuItem>
                                    </Items>
                                </telerik:RadMenu>
                            </ItemTemplate>
                        </wasp:WLPToolBarButton>
                    </Items>
                </wasp:WLPToolBar>

                <wasp:WLPGrid
                    ID="YearEndGrid"
                    runat="server"
                    AllowPaging="true"
                    PagerStyle-AlwaysVisible="true"
                    PageSize="100"
                    GridLines="None"
                    AutoAssignModifyProperties="true">

                    <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
                        <Scrolling AllowScroll="false" UseStaticHeaders="true" />
                        <Selecting AllowRowSelect="true" />
                        <ClientEvents OnRowClick="OnRowClick" />
                    </ClientSettings>

                    <MasterTableView ClientDataKeyNames="YearEndId, Year, CurrentRevision" AutoGenerateColumns="false" DataKeyNames="YearEndId" CommandItemDisplay="Top">
                        <CommandItemTemplate></CommandItemTemplate>
                        <Columns>
                            <wasp:GridBoundControl DataField="Year" LabelText="Year" UniqueName="Year" ResourceName="Year">
                                <HeaderStyle Width="100px" />
                            </wasp:GridBoundControl>
                            <wasp:GridBoundControl DataField="CurrentRevision" LabelText="CurrentRevision" UniqueName="CurrentRevision" ResourceName="CurrentRevision" />
                            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="RRCodeYearEndStatusCd" DataField="CodeYearEndStatusCd" Type="CodeYearEndStatusTypeCode" ResourceName="CodeYearEndStatusCd">
                                <HeaderStyle Width="100px" />
                            </wasp:GridKeyValueControl>
                            <wasp:GridDateTimeControl DataField="T4PrintDatetime" DataType="System.DateTime" LabelText="T4PrintDatetime" UniqueName="T4PrintDatetime" ResourceName="T4PrintDatetime" />
                            <wasp:GridDateTimeControl DataField="R1PrintDatetime" DataType="System.DateTime" LabelText="R1PrintDatetime" UniqueName="R1PrintDatetime" ResourceName="R1PrintDatetime" />
                            <wasp:GridDateTimeControl DataField="T4aPrintDatetime" DataType="System.DateTime" LabelText="T4aPrintDatetime" UniqueName="T4aPrintDatetime" ResourceName="T4aPrintDatetime" />
                            <wasp:GridDateTimeControl DataField="R2PrintDatetime" DataType="System.DateTime" LabelText="R2PrintDatetime" UniqueName="R2PrintDatetime" ResourceName="R2PrintDatetime" />
                            <wasp:GridDateTimeControl DataField="T4MassFileDatetime" DataType="System.DateTime" LabelText="T4MassFileDatetime" UniqueName="T4MassFileDatetime" ResourceName="T4MassFileDatetime" />
                            <wasp:GridDateTimeControl DataField="R1MassFileDatetime" DataType="System.DateTime" LabelText="R1MassFileDatetime" UniqueName="R1MassFileDatetime" ResourceName="R1MassFileDatetime" />
                            <wasp:GridDateTimeControl DataField="T4aMassFileDatetime" DataType="System.DateTime" LabelText="T4aMassFileDatetime" UniqueName="T4aMassFileDatetime" ResourceName="T4aMassFileDatetime" />
                            <wasp:GridDateTimeControl DataField="T4ExportDatetime" DataType="System.DateTime" LabelText="T4ExportDatetime" UniqueName="T4ExportDatetime" ResourceName="T4ExportDatetime" />
                            <wasp:GridDateTimeControl DataField="R1ExportDatetime" DataType="System.DateTime" LabelText="R1ExportDatetime" UniqueName="R1ExportDatetime" ResourceName="R1ExportDatetime" />
                            <wasp:GridDateTimeControl DataField="T4aExportDatetime" DataType="System.DateTime" LabelText="T4aExportDatetime" UniqueName="T4aExportDatetime" ResourceName="T4aExportDatetime" />
                            <wasp:GridDateTimeControl DataField="R2ExportDatetime" DataType="System.DateTime" LabelText="R2ExportDatetime" UniqueName="R2ExportDatetime" ResourceName="R2ExportDatetime" />
                        </Columns>
                    </MasterTableView>

                    <HeaderContextMenu EnableAutoScroll="true" />
                </wasp:WLPGrid>
            </td>
        </tr>
    </table>
</asp:Panel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="YearEndWindows"
    Width="1000"
    Height="600"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    OnClientClose="onClientClose"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="YearEndExportWindows"
    Width="1000"
    Height="600"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    OnClientClose="onClientClose"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js" type="text/javascript"></script>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var toolbar;
        var year;
        var yearEndId;
        var revision;
        var reportMenu;

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function rowIsSelected() {
            var getSeletectedRow = getSelectedRow();
            return (getSeletectedRow != null);
        }

        function clear() {
            year = null;
            yearEndId = null;
            revision = null;
        }

        function YearEndToolBar_init(sender) {
            initializeControls();
        }

        function initializeControls() {
            if ($find('<%= YearEndToolBar.ClientID %>') != null)
                toolbar = $find('<%= YearEndToolBar.ClientID %>');

            enableButtons(rowIsSelected());
        }

        function getSelectedRow() {
            var yearEndGrid = $find('<%= YearEndGrid.ClientID %>');
            if (yearEndGrid != null) {
                var MasterTable = yearEndGrid.get_masterTableView();
                if (MasterTable != null)
                    return MasterTable.get_selectedItems()[0];
                else
                    return null;
            }
            else
                return null;
        }

        function enableButtons(enable) {
            //mass print
            var massPrintButton = null;
            var massT4ReportButton = toolbar.findButtonByCommandName('MassT4Print');
            var massR1ReportButton = toolbar.findButtonByCommandName('MassR1Print');
            var massT4aReportButton = toolbar.findButtonByCommandName('MassT4APrint');
            var massR2ReportButton = toolbar.findButtonByCommandName('MassR2Print');
            var massT4rspReportButton = toolbar.findButtonByCommandName('MassT4RSPPrint');
            var massT4arcaReportButton = toolbar.findButtonByCommandName('MassT4ARCAPrint');
            var massNr4ReportButton = toolbar.findButtonByCommandName('MassNR4Print');

            if (massT4ReportButton != null) {
                massT4ReportButton.set_enabled(enable);
                massPrintButton = massT4ReportButton.get_parent();
            }

            if (massR1ReportButton != null) {
                massR1ReportButton.set_enabled(enable);
                massPrintButton = massR1ReportButton.get_parent();
            }

            if (massT4aReportButton != null) {
                massT4aReportButton.set_enabled(enable);
                massPrintButton = massT4aReportButton.get_parent();
            }

            if (massR2ReportButton != null) {
                massR2ReportButton.set_enabled(enable);
                massPrintButton = massR2ReportButton.get_parent();
            }

            if (massT4rspReportButton != null) {
                massT4rspReportButton.set_enabled(enable);
                massPrintButton = massT4rspReportButton.get_parent();
            }

            if (massT4arcaReportButton != null) {
                massT4arcaReportButton.set_enabled(enable);
                massPrintButton = massT4arcaReportButton.get_parent();
            }

            if (massNr4ReportButton != null) {
                massNr4ReportButton.set_enabled(enable);
                massPrintButton = massNr4ReportButton.get_parent();
            }

            if (massPrintButton != null)
                massPrintButton.set_enabled(enable);

            //mass file
            var massFileButton = null;
            var massFileAllButton = toolbar.findButtonByCommandName('MassFileAll');
            var massT4ReportFileButton = toolbar.findButtonByCommandName('MassT4File');
            var massR1ReportFileButton = toolbar.findButtonByCommandName('MassR1File');
            var massT4aReportFileButton = toolbar.findButtonByCommandName('MassT4AFile');

            if (massFileAllButton != null) {
                massFileAllButton.set_enabled(enable);
                massFileButton = massFileAllButton.get_parent();
            }

            if (massT4ReportFileButton != null) {
                massT4ReportFileButton.set_enabled(enable);
                massFileButton = massT4ReportFileButton.get_parent();
            }

            if (massR1ReportFileButton != null) {
                massR1ReportFileButton.set_enabled(enable);
                massFileButton = massR1ReportFileButton.get_parent();
            }

            if (massT4aReportFileButton != null) {
                massT4aReportFileButton.set_enabled(enable);
                massFileButton = massT4aReportFileButton.get_parent();
            }

            if (massFileButton != null)
                massFileButton.set_enabled(enable);

            //export
            var exportButton = null;
            var t4ExportButton = toolbar.findButtonByCommandName('T4Export');
            var r1ExportButton = toolbar.findButtonByCommandName('R1Export');
            var t4AExportButton = toolbar.findButtonByCommandName('T4AExport');
            var r2ExportButton = toolbar.findButtonByCommandName('R2Export');
            var t4RSPExportButton = toolbar.findButtonByCommandName('T4RSPExport');
            var t4ARCAExportButton = toolbar.findButtonByCommandName('T4ARCAExport');
            var nR4ExportButton = toolbar.findButtonByCommandName('NR4Export');

            if (t4ExportButton != null) {
                t4ExportButton.set_enabled(enable);
                exportButton = t4ExportButton.get_parent();
            }

            if (r1ExportButton != null) {
                r1ExportButton.set_enabled(enable);
                exportButton = r1ExportButton.get_parent();
            }

            if (t4AExportButton != null) {
                t4AExportButton.set_enabled(enable);
                exportButton = t4AExportButton.get_parent();
            }

            if (r2ExportButton != null) {
                r2ExportButton.set_enabled(enable);
                exportButton = r2ExportButton.get_parent();
            }

            if (t4RSPExportButton != null) {
                t4RSPExportButton.set_enabled(enable);
                exportButton = t4RSPExportButton.get_parent();
            }

            if (t4ARCAExportButton != null) {
                t4ARCAExportButton.set_enabled(enable);
                exportButton = t4ARCAExportButton.get_parent();
            }

            if (nR4ExportButton != null) {
                nR4ExportButton.set_enabled(enable);
                exportButton = nR4ExportButton.get_parent();
            }

            if (exportButton != null)
                exportButton.set_enabled(enable);

            //summary
            var summaryButton = null;
            var t4SummaryButton = toolbar.findButtonByCommandName('T4Summary');
            var r1SummaryButton = toolbar.findButtonByCommandName('R1SummaryOfSourceDeductions');
            var t4ASummaryButton = toolbar.findButtonByCommandName('T4ASummary');
            var r2SummaryButton = toolbar.findButtonByCommandName('R2Summary');
            var t4RSPSummaryButton = toolbar.findButtonByCommandName('T4RSPSummary');
            var t4ARCASummaryButton = toolbar.findButtonByCommandName('T4ARCASummary');
            var nR4SummaryButton = toolbar.findButtonByCommandName('NR4Summary');

            if (t4SummaryButton != null) {
                t4SummaryButton.set_enabled(enable);
                summaryButton = t4SummaryButton.get_parent();
            }

            if (r1SummaryButton != null) {
                r1SummaryButton.set_enabled(enable);
                summaryButton = r1SummaryButton.get_parent();
            }

            if (t4ASummaryButton != null) {
                t4ASummaryButton.set_enabled(enable);
                summaryButton = t4ASummaryButton.get_parent();
            }

            if (r2SummaryButton != null) {
                r2SummaryButton.set_enabled(enable);
                summaryButton = r2SummaryButton.get_parent();
            }

            if (t4RSPSummaryButton != null) {
                t4RSPSummaryButton.set_enabled(enable);
                summaryButton = t4RSPSummaryButton.get_parent();
            }

            if (t4ARCASummaryButton != null) {
                t4ARCASummaryButton.set_enabled(enable);
                summaryButton = t4ARCASummaryButton.get_parent();
            }

            if (nR4SummaryButton != null) {
                nR4SummaryButton.set_enabled(enable);
                summaryButton = nR4SummaryButton.get_parent();
            }

            if (summaryButton != null)
                summaryButton.set_enabled(enable);

            //reports
            var reportsMenuButton = toolbar.findButtonByCommandName('ReportMenuButton');

            if (reportsMenuButton != null) {
                reportsMenuButton.set_enabled(enable);
                reportMenu.set_enabled(enable);
            }
        }

        function OnRowClick(sender, eventArgs) {
            year = eventArgs.getDataKeyValue('Year');
            yearEndId = eventArgs.getDataKeyValue('YearEndId');
            revision = eventArgs.getDataKeyValue('CurrentRevision');
            rowSelected = true;

            enableButtons(true);
        }

        //mass print
        function openMassT4Print() {
            if (year != null) {
                var message = YearEndNegativeAmountEmployeeMessage();
                if (message != null)
                    alert(message);
                else {
                    openWindowWithManager('YearEndWindows', String.format('/Report/T4MassPrint/{0}/{1}/{2}/{3}/{4}/{5}/{6}', -1, -1, 'PDF', year, 'x', 'x', 'x'), true);
                    return false;
                }
            }
        }

        function openMassR1Print() {
            if (year != null) {
                var message = YearEndNegativeAmountEmployeeMessage();
                if (message != null)
                    alert(message);
                else {
                    openWindowWithManager('YearEndWindows', String.format('/Report/R1MassPrint/{0}/{1}/{2}/{3}/{4}/{5}/{6}', -1, -1, 'PDF', year, 'x', 'x', 'x'), true);
                    return false;
                }
            }
        }

        function openMassT4APrint() {
            if (year != null) {
                var message = YearEndNegativeAmountEmployeeMessage();
                if (message != null)
                    alert(message);
                else {
                    openWindowWithManager('YearEndWindows', String.format('/Report/T4AMassPrint/{0}/{1}/{2}/{3}/{4}/{5}/{6}', -1, -1, 'PDF', year, 'x', 'x', 'x'), true);
                    return false;
                }
            }
        }

        function openMassR2Print() {
            if (year != null) {
                var message = YearEndNegativeAmountEmployeeMessage();
                if (message != null)
                    alert(message);
                else {
                    openWindowWithManager('YearEndWindows', String.format('/Report/R2MassPrint/{0}/{1}/{2}/{3}/{4}/{5}/{6}', -1, -1, 'PDF', year, 'x', 'x', 'x'), true);
                    return false;
                }
            }
        }

        function openMassT4RSPPrint() {
            if (year != null) {
                var message = YearEndNegativeAmountEmployeeMessage();
                if (message != null)
                    alert(message);
                else {
                    openWindowWithManager('YearEndWindows', String.format('/Report/T4RSPMassPrint/{0}/{1}/{2}/{3}/{4}/{5}/{6}', -1, -1, 'PDF', year, 'x', 'x', 'x'), true);
                    return false;
                }
            }
        }

        function openMassT4ARCAPrint() {
            if (year != null) {
                var message = YearEndNegativeAmountEmployeeMessage();
                if (message != null)
                    alert(message);
                else {
                    openWindowWithManager('YearEndWindows', String.format('/Report/T4ARCAMassPrint/{0}/{1}/{2}/{3}/{4}/{5}/{6}', -1, -1, 'PDF', year, 'x', 'x', 'x'), true);
                    return false;
                }
            }
        }

        function openMassNR4Print() {
            if (year != null) {
                var message = YearEndNegativeAmountEmployeeMessage();
                if (message != null)
                    alert(message);
                else {
                    openWindowWithManager('YearEndWindows', String.format('/Report/NR4MassPrint/{0}/{1}/{2}/{3}/{4}/{5}/{6}', -1, -1, 'PDF', year, 'x', 'x', 'x'), true);
                    return false;
                }
            }
        }

        //mass file
        function openMassFileAll() {
            if (year != null) {
                var message = YearEndNegativeAmountEmployeeMessage();
                if (message != null)
                    alert(message);
                else
                    __doPostBack('<%= this.ClientID %>', String.format('massfileallyear={0},revision={1}', year, revision));
            }
        }

      <%--  function openMassT4File() {
            if (year != null) {
                var message = YearEndNegativeAmountEmployeeMessage();
                if (message != null)
                    alert(message);
                else
                    __doPostBack('<%= this.ClientID %>', String.format('t4year={0}', year));
            }
        }--%>

        <%--function openMassR1File() {
            if (year != null) {
                var message = YearEndNegativeAmountEmployeeMessage();
                if (message != null)
                    alert(message);
                else
                    __doPostBack('<%= this.ClientID %>', String.format('r1year={0}', year));
            }
        }--%>

        <%--function openMassT4AFile() {
            if (year != null) {
                var message = YearEndNegativeAmountEmployeeMessage();
                if (message != null)
                    alert(message);
                else
                    __doPostBack('<%= this.ClientID %>', String.format('t4ayear={0}', year));
            }
        }--%>

        //export
        function openT4Export() {
            if (year != null) {
                var message = YearEndNegativeAmountEmployeeMessage();
                if (message != null)
                    alert(message);
                else {
                    openWindowWithManager('YearEndExportWindows', String.format('/Admin/Export/T4/{0}/{1}', yearEndId, year), false);
                    return false;
                }
            }
        }

        function openR1Export() {
            if (year != null) {
                var message = YearEndNegativeAmountEmployeeMessage();
                if (message != null)
                    alert(message);
                else {
                    openWindowWithManager('YearEndExportWindows', String.format('/Admin/R1Export/R1/{0}/{1}', yearEndId, year), false);
                    return false;
                }
            }
        }

        function openT4AExport() {
            if (year != null) {
                var message = YearEndNegativeAmountEmployeeMessage();
                if (message != null)
                    alert(message);
                else {
                    openWindowWithManager('YearEndExportWindows', String.format('/Admin/Export/T4A/{0}/{1}', yearEndId, year), false);
                    return false;
                }
            }
        }

        function openR2Export() {
            if (year != null) {
                var message = YearEndNegativeAmountEmployeeMessage();
                if (message != null)
                    alert(message);
                else {
                    openWindowWithManager('YearEndExportWindows', String.format('/Admin/R1Export/R2/{0}/{1}', yearEndId, year), false);
                    return false;
                }
            }
        }

        function openT4RSPExport() {
            //placeholder
        }

        function openT4ARCAExport() {
            //placeholder
        }

        function openNR4Export() {
            if (year != null) {
                var message = YearEndNegativeAmountEmployeeMessage();
                if (message != null)
                    alert(message);
                else {
                    openWindowWithManager('YearEndExportWindows', String.format('/Admin/Export/NR4/{0}/{1}', yearEndId, year), false);
                    return false;
                }
            }
        }

        //summary
        function openMassT4SummaryPrint() {
            if (year != null) {
                openWindowWithManager('YearEndWindows', String.format('/Report/T4SummaryMassPrint/{0}/{1}/{2}/{3}/{4}/{5}/{6}', -1, -1, 'PDF', year, 'x', 'x', revision), true);
                return false;
            }
        }

        function openR1SummaryOfSourceDeductions() {
            if (year != null) {
                openWindowWithManager('YearEndWindows', String.format('/Report/R1SummaryOfSourceDeductions/{0}/{1}/{2}/{3}/{4}/{5}/{6}', -1, -1, 'PDF', year, 'x', 'x', revision), true);
                return false;
            }
        }

        function openMassT4ASummaryPrint() {
            if (year != null) {
                openWindowWithManager('YearEndWindows', String.format('/Report/T4ASummaryMassPrint/{0}/{1}/{2}/{3}/{4}/{5}/{6}', -1, -1, 'PDF', year, 'x', 'x', revision), true);
                return false;
            }
        }

        function openR2Summary() {
            if (year != null) {
                openWindowWithManager('YearEndWindows', String.format('/Report/R2Summary/{0}/{1}/{2}/{3}/{4}/{5}/{6}', -1, -1, 'PDF', year, 'x', 'x', revision), true);
                return false;
            }
        }

        function openT4RSPSummary() {
            //placeholder
        }

        function openT4ARCASummary() {
            //placeholder
        }

        function openNR4Summary() {
            if (year != null) {
                openWindowWithManager('YearEndWindows', String.format('/Report/NR4SummaryMassPrint/{0}/{1}/{2}/{3}/{4}/{5}/{6}', -1, -1, 'PDF', year, 'x', 'x', revision), true);
                return false;
            }
        }

        //json javascript function to call the GetYearEndNegativeAmountEmployeeMessage webmethod
        function YearEndNegativeAmountEmployeeMessage(source, args) {
            var rtn = null;
            $.ajax
            ({
                type: "POST",
                url: getUrl("/Admin/YearEnd/YearEndPage.aspx/GetYearEndNegativeAmountEmployeeMessage"),
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                cache: false,
                data: "{'year': '" + year + "' ,'revision': '" + revision + "'}",
                success: function (msg) {
                    rtn = msg.d;
                }
            });
            return rtn;
        }

        //sets the handler for report menu
        function reportOnInit(sender, eventArgs) {
            reportMenu = sender;
        }

        //opens selected report
        function reportItemClicked(sender, eventArgs) {
           
            menuValue = eventArgs.get_item().get_value();
            if (menuValue != null && year != null) {
                var splitterIndex = menuValue.lastIndexOf("_");
                var reportName = menuValue.substr(0, splitterIndex);
                var outputType = menuValue.substr(splitterIndex + 1, 99);

                //if excel format, do not open a child window.
                if (outputType == 'EXCEL')
                    invokeDownload(reportName, outputType);
                else
                    openWindowWithManager('YearEndWindows', String.format('/Report/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}', reportName, year, 'null', outputType, year, 'x', 'x', revision), true);
            }
        }

        //force a page refresh after the window closes so the correct rowversion is used when closing the year
        function onClientClose(sender, eventArgs) {
            __doPostBack('<%= SearchPanel.ClientID %>', 'refreshScreen');
        }

        function invokeDownload(reportName, outputType) {
            var frame = document.createElement("frame");
            frame.src = getUrl(String.format('/Report/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}', reportName, year, 'null', outputType, year, 'x', 'x', revision)), null;
            frame.style.display = "none";
            document.body.appendChild(frame);
        }

        //json javascript function to call the GetGenerateNewPayrollYearMessage webmethod
        function GenerateNewPayrollYearMessage(source, args) {
            var rtn = null;
            $.ajax
            ({
                type: "POST",
                url: getUrl("/Admin/YearEnd/YearEndPage.aspx/GetGenerateNewPayrollYearMessage"),
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                cache: false,
                success: function (msg) {
                    rtn = msg.d;
                }
            });
            return rtn;
        }

        function checkGeneratePayrollConfirmation(sender, args) {
            var button = args.get_item();
            if (button.get_commandName() == "CreateCurrentYE") {
                var message = GenerateNewPayrollYearMessage();
                args.set_cancel(!confirm(message));
            }
        }
    </script>
</telerik:RadScriptBlock>