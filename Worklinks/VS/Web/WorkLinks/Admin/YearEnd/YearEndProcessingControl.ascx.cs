﻿using System;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.YearEnd
{
    public partial class YearEndProcessingControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private static String _yearEndStatusPending = Common.ApplicationParameter.Code.YearEndStatusCode.Pending;
        #endregion

        #region properties
        private BusinessLayer.BusinessObjects.YearEnd YearEndProcess
        {
            get
            {
                if (Data.Count > 0)
                    return (BusinessLayer.BusinessObjects.YearEnd)Data[0];
                else
                    return null;
            }
        }
        private bool YearEndStatusPending { get { return YearEndStatusCode.Equals(_yearEndStatusPending); } }
        private String YearEndStatusCode
        {
            get
            {
                if (YearEndProcess != null && YearEndProcess.CodeYearEndStatusCd != null)
                    return YearEndProcess.CodeYearEndStatusCd;
                else
                    return String.Empty;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                LoadYearEndProcess();
        }
        protected void LoadYearEndProcess()
        {
            YearEndCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetYearEnd(null);
            Data = collection;
        }
        protected void LandingPageButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/HumanResources/LandingPage.aspx");
        }
        protected void Timer1_Tick(object sender, EventArgs e)
        {
            LoadYearEndProcess();
            RedirectBack();
        }
        private void RedirectBack()
        {
            if (!YearEndStatusPending) { Response.Redirect("~/Admin/YearEnd/YearEndPage.aspx"); }
        }
    }
}