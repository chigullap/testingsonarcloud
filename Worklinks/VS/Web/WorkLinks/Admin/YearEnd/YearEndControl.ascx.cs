﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.YearEnd
{
    public partial class YearEndControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        //mass print
        protected bool EnableMassPrint { get { return Common.Security.RoleForm.YearEndMassPrintMenu; } }
        protected bool EnableMassT4PrintButton { get { return Common.Security.RoleForm.T4MassReportPrint.ViewFlag; } }
        protected bool EnableMassR1PrintButton { get { return Common.Security.RoleForm.R1MassReportPrint.ViewFlag; } }
        protected bool EnableMassT4APrintButton { get { return Common.Security.RoleForm.T4AMassReportPrint.ViewFlag; } }
        protected bool EnableMassR2PrintButton { get { return Common.Security.RoleForm.R2MassReportPrint.ViewFlag; } }
        protected bool EnableMassT4RSPPrintButton { get { return Common.Security.RoleForm.T4RSPMassReportPrint.ViewFlag; } }
        protected bool EnableMassT4ARCAPrintButton { get { return Common.Security.RoleForm.T4ARCAMassReportPrint.ViewFlag; } }
        protected bool EnableMassNR4PrintButton { get { return Common.Security.RoleForm.NR4MassReportPrint.ViewFlag; } }

        //mass file
        protected bool EnableMassFile { get { return Common.Security.RoleForm.YearEndMassFileMenu; } }
        protected bool EnableMassFileAllReportButton { get { return Common.Security.RoleForm.MassFileAll.ViewFlag; } }
        protected bool EnableMassT4FileButton { get { return Common.Security.RoleForm.T4MassReportFile.ViewFlag; } }
        protected bool EnableMassR1FileButton { get { return Common.Security.RoleForm.R1MassReportFile.ViewFlag; } }
        protected bool EnableMassT4AFileButton { get { return Common.Security.RoleForm.T4AMassReportFile.ViewFlag; } }

        //export
        protected bool EnableExport { get { return Common.Security.RoleForm.YearEndExportMenu; } }
        protected bool EnableT4ExportButton { get { return Common.Security.RoleForm.T4Export.ViewFlag; } }
        protected bool EnableR1ExportButton { get { return Common.Security.RoleForm.R1Export.ViewFlag; } }
        protected bool EnableT4AExportButton { get { return Common.Security.RoleForm.T4AExport.ViewFlag; } }
        protected bool EnableR2ExportButton { get { return Common.Security.RoleForm.R2Export.ViewFlag; } }
        protected bool EnableT4RSPExportButton { get { return Common.Security.RoleForm.T4RSPExport; } }
        protected bool EnableT4ARCAExportButton { get { return Common.Security.RoleForm.T4ARCAExport; } }
        protected bool EnableNR4ExportButton { get { return Common.Security.RoleForm.NR4Export.ViewFlag; } }

        //summary
        protected bool EnableSummary { get { return Common.Security.RoleForm.YearEndSummaryMenu; } }
        protected bool EnableT4SummaryButton { get { return Common.Security.RoleForm.T4SummaryMassReportPrint.ViewFlag; } }
        protected bool EnableR1SummaryButton { get { return Common.Security.RoleForm.R1SummaryOfSourceDeductions.ViewFlag; } }
        protected bool EnableT4ASummaryButton { get { return Common.Security.RoleForm.T4ASummaryMassReportPrint.ViewFlag; } }
        protected bool EnableR2SummaryButton { get { return Common.Security.RoleForm.R2Summary.ViewFlag; } }
        protected bool EnableT4RSPSummaryButton { get { return Common.Security.RoleForm.T4RSPSummary; } }
        protected bool EnableT4ARCASummaryButton { get { return Common.Security.RoleForm.T4ARCASummary; } }
        protected bool EnableNR4SummaryButton { get { return Common.Security.RoleForm.NR4SummaryMassReportPrint.ViewFlag; } }

        //reports
        protected bool EnableReportsMenu { get { return Common.Security.RoleForm.YearEndReportMenu; } }
        protected bool EnableStatDeductionExceptionReportButton { get { return Common.Security.RoleForm.StatDeductionException.ViewFlag; } }
        protected bool EnableExceptionReportMenuItem { get { return Common.Security.RoleForm.NameAddressException.ViewFlag; } }
        protected bool EnableYearEndWCBReportMenuItem { get { return Common.Security.RoleForm.YearEndWCBReport.ViewFlag; } }
        protected bool EnableYearEndPaycodeSummaryMenuItem { get { return Common.Security.RoleForm.YearEndPaycodeSummaryReport.ViewFlag; } }
        protected bool EnableYearEndAdjustmentsAuditMenuItem { get { return Common.Security.RoleForm.YearEndAdjustmentsAudit.ViewFlag; } }
        protected bool EnableYearEndHealthTaxSummaryMenuItem { get { return Common.Security.RoleForm.YearEndHealthTaxSummary.ViewFlag; } }
        protected bool EnableYearEndR1DetailMenuItem { get { return Common.Security.RoleForm.YearEndR1Detail.ViewFlag; } }
        protected bool EnableYearEndR2DetailMenuItem { get { return Common.Security.RoleForm.YearEndR2Detail.ViewFlag; } }
        protected bool EnableYearEndT4DetailMenuItem { get { return Common.Security.RoleForm.YearEndT4Detail.ViewFlag; } }
        protected bool EnableYearEndT4ADetailMenuItem { get { return Common.Security.RoleForm.YearEndT4ADetail.ViewFlag; } }
        protected bool EnableYearEndAdjustmentReportMenuItem { get { return Common.Security.RoleForm.YearEndAdjustmentReport.ViewFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.YearEnd.ViewFlag);

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            //we know we are processing a mass T4/T4A/R1
            if (args != null && args.Length > 7 && args.ToString().ToLower() != "refreshscreen" && !(args.ToString().ToLower().StartsWith("massfileallyear=")) && (args.Contains("t4year=") || args.Contains("t4ayear=") || args.Contains("r1year=")))
            {
                /*
                 * 
                 * 
                 *      THIS SECTION HAS BEEN UNAVAILABLE TO CLIENTS SINCE 2014 and is probably out of date.  Uncomment and correct if it comes back into use.
                 *      
                 *      
                 */

                //long year = -1;
                //String reportType = "";
                //String reportFileName = "";

                //if (args != null && args.Length > 7 && args.Substring(0, 7).ToLower().Equals("t4year="))
                //{
                //    year = Convert.ToInt64(args.Substring(7));
                //    reportType = "T4MassFile";
                //    reportFileName = Common.ApplicationParameter.MassT4InternalFileFormat;
                //}
                //else if (args != null && args.Length > 8 && args.Substring(0, 8).ToLower().Equals("t4ayear="))
                //{
                //    year = Convert.ToInt64(args.Substring(8));
                //    reportType = "T4AMassFile";
                //    reportFileName = Common.ApplicationParameter.MassT4aInternalFileFormat;
                //}
                //else if (args != null && args.Length > 7 && args.Substring(0, 7).ToLower().Equals("r1year="))
                //{
                //    year = Convert.ToInt64(args.Substring(7));
                //    reportType = "R1MassFile";
                //    reportFileName = Common.ApplicationParameter.MassR1InternalFileFormat;
                //}

                //byte[] file = Common.ServiceWrapper.ReportClient.GetReportForAllEmployeesOfThisYear(reportFileName, year, reportType, Common.ApplicationParameter.ReportShowDescriptionField, Common.ApplicationParameter.ReportPayRegisterSortOrder) ?? new byte[0];
                //String zipFileName = Common.ServiceWrapper.HumanResourcesClient.GetYearEndZipFileNamingFormat(Common.ApplicationParameter.YearEndZipFileName); //datetime will be auto populated

                //Response.Clear();
                //Response.ContentType = "application/zip";
                //Response.AppendHeader("content-disposition", "attachment; filename=" + zipFileName);

                //Response.OutputStream.Write(file, 0, file.Length);
                //Response.Flush();
                //Response.End();
            }
            else if (args != null && args.Length > 16 && args.Substring(0, 16).ToLower().Equals("massfileallyear="))
            {
                long year = -1;
                String revision = SeperateArgs(args, out year);
                String[] reportTypes = new String[7] { "T4MassFile", "T4RSPMassFile", "T4AMassFile", "T4ARCAMassFile", "NR4MassFile", "R1MassFile", "R2MassFile" };
                String[] reportFileNames = new String[7] { Common.ApplicationParameter.MassT4InternalFileFormat, Common.ApplicationParameter.MassT4RSPInternalFileFormat, Common.ApplicationParameter.MassT4aInternalFileFormat, Common.ApplicationParameter.MassT4ARCAInternalFileFormat, Common.ApplicationParameter.MassNR4InternalFileFormat, Common.ApplicationParameter.MassR1InternalFileFormat, Common.ApplicationParameter.MassR2InternalFileFormat };
                byte[] file = Common.ServiceWrapper.ReportClient.GetMassFileForAllReportsForYear(reportFileNames, year, reportTypes, Common.ApplicationParameter.ReportShowDescriptionField, Common.ApplicationParameter.ReportPayRegisterSortOrder, revision) ?? new byte[0];
                String zipFileName = Common.ServiceWrapper.HumanResourcesClient.GetYearEndZipFileNamingFormat(Common.ApplicationParameter.YearEndZipFileName); //datetime will be auto populated

                Response.Clear();
                Response.ContentType = "application/zip";
                Response.AppendHeader("content-disposition", "attachment; filename=" + zipFileName);

                Response.OutputStream.Write(file, 0, file.Length);
                Response.Flush();
                Response.End();
            }
            else
            {
                WireEvents();

                if (!IsPostBack || (args != null && args.ToString() == "refreshScreen"))
                {
                    if (args != null && args.ToString() == "refreshScreen")
                        RefreshGrid();
                    else
                        btnRefresh_Click(null, null);

                    for (int i = 0; i < Data.Count; i++) //if any records are in a PEN (pending) status, they are still processing
                    {
                        if (((BusinessLayer.BusinessObjects.YearEnd)Data[i]).CodeYearEndStatusCd == "PEN")
                            Response.Redirect("~/Admin/YearEndProcessing/View");
                    }

                    Panel1.DataBind();
                }
            }
        }
        protected void WireEvents()
        {
            YearEndGrid.NeedDataSource += new GridNeedDataSourceEventHandler(YearEndGrid_NeedDataSource);
            YearEndGrid.ItemDataBound += new GridItemEventHandler(YearEndGrid_ItemDataBound);
            YearEndGrid.PreRender += new EventHandler(YearEndGrid_PreRender);
            YearEndGrid.AllowPaging = true;
        }
        protected void RefreshGrid()
        {
            long keyId = -1;

            //if a row was selected
            if (YearEndGrid.SelectedItems.Count != 0)
                keyId = Convert.ToInt64(((GridDataItem)YearEndGrid.SelectedItems[0]).GetDataKeyValue(YearEndGrid.MasterTableView.DataKeyNames[0]));

            YearEndGrid.Rebind();

            if (keyId != -1)
                YearEndGrid.SelectRowByFirstDataKey(keyId); //re-select the selected row
        }
        //seperate the args when the massfileallyear is selected
        private String SeperateArgs(String args, out long year)
        {
            int prevIndex = 0;
            int nextIndex = args.IndexOf(",", 0);

            year = Convert.ToInt64(args.Substring(16, nextIndex - 16));
            prevIndex = nextIndex + 1;

            return args.Substring(prevIndex + 9);
        }
        #endregion

        #region event handlers
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            YearEndCollection collection = new YearEndCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetYearEnd(null));
            Data = collection;

            YearEndGrid.Rebind();
            EnableDisableButtons();
        }
        private void EnableDisableButtons()
        {
            if (Data.Count == 0 || ((BusinessLayer.BusinessObjects.YearEnd)Data[0]).CodeYearEndStatusCd == "CMP")
            {
                ((WLPToolBarButton)YearEndToolBar.FindButtonByCommandName("CloseCurrentYE")).Enabled = false;
                ((WLPToolBarButton)YearEndToolBar.FindButtonByCommandName("CloseVersionYE")).Enabled = false;
            }
            else
            {
                ((WLPToolBarButton)YearEndToolBar.FindButtonByCommandName("CloseCurrentYE")).Enabled = true;
                ((WLPToolBarButton)YearEndToolBar.FindButtonByCommandName("CloseVersionYE")).Enabled = true;
            }

            if (Data.Count == 0 || ((BusinessLayer.BusinessObjects.YearEnd)Data[0]).CodeYearEndStatusCd == "CMP")
                ((WLPToolBarButton)YearEndToolBar.FindButtonByCommandName("CreateCurrentYE")).Enabled = true;
            else
                ((WLPToolBarButton)YearEndToolBar.FindButtonByCommandName("CreateCurrentYE")).Enabled = false;
        }
        protected void YearEndToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            WLPToolBarButton button = ((WLPToolBarButton)e.Item);

            if (button.CommandName.ToLower().Equals("createcurrentye"))
                CreateCurrentYE();
            else if (button.CommandName.ToLower().Equals("closecurrentye"))
                CloseCurrentYE();
            else if (button.CommandName.ToLower().Equals("closeversionye"))
                CloseVersionYE();
        }
        protected void CreateCurrentYE()
        {
            //this process will be replaced with a new process to replace the GP one
            Common.ServiceWrapper.HumanResourcesClient.CreateCurrentYE();
            Response.Redirect("~/Admin/YearEndProcessing/View");
        }
        protected void CloseVersionYE()
        {
            try
            {
                ((BusinessLayer.BusinessObjects.YearEnd)Data[0]).CurrentRevision++;
                Common.ServiceWrapper.HumanResourcesClient.CloseVersionYE((BusinessLayer.BusinessObjects.YearEnd)Data[0]);
                btnRefresh_Click(null, null);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void CloseCurrentYE()
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.CloseCurrentYE((BusinessLayer.BusinessObjects.YearEnd)Data[0]);
                Response.Redirect("~/Admin/YearEndProcessing/View");
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void YearEndGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            YearEndGrid.DataSource = Data;
        }
        protected void YearEndGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                //work around to remove empty rows caused by the OnCommand Telerik bug 
                if (String.Equals((dataItem["Year"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["CodeYearEndStatusCd"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["T4aPrintDatetime"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["R1PrintDatetime"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["T4PrintDatetime"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["R2PrintDatetime"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["T4ExportDatetime"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["R1ExportDatetime"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["T4aExportDatetime"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["R2ExportDatetime"].Text).ToUpper(), "&NBSP;"))
                {
                    //hide the row                
                    e.Item.Display = false;
                }
            }
        }
        void YearEndGrid_PreRender(object sender, EventArgs e)
        {
            //work around to remove empty rows caused by the OnCommand Telerik bug
            if (Data == null)
                YearEndGrid.AllowPaging = false;
            else
                YearEndGrid.AllowPaging = true;
        }
        #endregion

        #region security handlers
        protected void CreateCurrentYEToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.YearEnd.ViewFlag; }
        protected void CloseVersionYEToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.YearEnd.ViewFlag; }
        protected void CloseCurrentYEToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.YearEnd.ViewFlag; }

        //mass print
        protected void MassPrint_PreRender(object sender, EventArgs e) { ((WLPToolBarDropDown)sender).Enabled = ((WLPToolBarDropDown)sender).Visible && EnableMassPrint; }
        protected void MassT4PrintToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableMassT4PrintButton; }
        protected void MassR1PrintToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableMassR1PrintButton; }
        protected void MassT4APrintToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableMassT4APrintButton; }
        protected void MassR2PrintToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableMassR2PrintButton; }
        protected void MassT4RSPPrintToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableMassT4RSPPrintButton; }
        protected void MassT4ARCAPrintToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableMassT4ARCAPrintButton; }
        protected void MassNR4PrintToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableMassNR4PrintButton; }

        //mass file
        protected void MassFile_PreRender(object sender, EventArgs e) { ((WLPToolBarDropDown)sender).Enabled = ((WLPToolBarDropDown)sender).Visible && EnableMassFile; }
        protected void MassFileAll_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableMassFileAllReportButton; }
        protected void MassT4FileToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableMassT4FileButton; }
        protected void MassR1FileToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableMassR1FileButton; }
        protected void MassT4AFileToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableMassT4AFileButton; }

        //export
        protected void Export_PreRender(object sender, EventArgs e) { ((WLPToolBarDropDown)sender).Enabled = ((WLPToolBarDropDown)sender).Visible && EnableExport; }
        protected void T4Export_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableT4ExportButton; }
        protected void R1Export_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableR1ExportButton; }
        protected void T4AExport_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableT4AExportButton; }
        protected void R2Export_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableR2ExportButton; }
        protected void T4RSPExport_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableT4RSPExportButton; }
        protected void T4ARCAExport_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableT4ARCAExportButton; }
        protected void NR4Export_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableNR4ExportButton; }

        //summary
        protected void Summary_PreRender(object sender, EventArgs e) { ((WLPToolBarDropDown)sender).Enabled = ((WLPToolBarDropDown)sender).Visible && EnableSummary; }
        protected void T4Summary_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableT4SummaryButton; }
        protected void R1Summary_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableR1SummaryButton; }
        protected void T4ASummary_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableT4ASummaryButton; }
        protected void R2Summary_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableR2SummaryButton; }
        protected void T4RSPSummary_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableT4RSPSummaryButton; }
        protected void T4ARCASummary_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableT4ARCASummaryButton; }
        protected void NR4Summary_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableNR4SummaryButton; }

        //reports
        protected void ReportMenuButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableReportsMenu; }
        protected void ReportsToolBar_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableReportsMenu; }
        protected void StatDeductionExceptionReport_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableStatDeductionExceptionReportButton; }
        protected void NameAddressExceptionReport_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableExceptionReportMenuItem; }
        protected void YearEndWCBReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableYearEndWCBReportMenuItem; }
        protected void YearEndPaycodeSummaryReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableYearEndPaycodeSummaryMenuItem; }
        protected void YearEndAdjustmentsAuditMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableYearEndAdjustmentsAuditMenuItem; }
        protected void YearEndHealthTaxSummaryMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableYearEndHealthTaxSummaryMenuItem; }
        protected void YearEndR1DetailMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableYearEndR1DetailMenuItem; }
        protected void YearEndR2DetailMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableYearEndR2DetailMenuItem; }
        protected void YearEndT4DetailMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableYearEndT4DetailMenuItem; }
        protected void YearEndT4ADetailMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableYearEndT4ADetailMenuItem; }
        protected void YearEndAdjustmentReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableYearEndAdjustmentReportMenuItem; }
        #endregion
    }
}