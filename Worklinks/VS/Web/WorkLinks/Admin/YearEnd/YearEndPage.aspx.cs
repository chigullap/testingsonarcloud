﻿using System;
using System.Web.Services;

namespace WorkLinks.Admin.YearEnd
{
    public partial class YearEndPage : WorkLinksPage
    {
        private String GetErrorMessage(BusinessLayer.BusinessObjects.Employee emp)
        {
            String errorMsg = (String)GetGlobalResourceObject("ErrorMessages", "YearEndNegativeAmountEmployee");
            return String.Format(errorMsg, emp.FirstName, emp.LastName, emp.EmployeeNumber);
        }
        private String GetGenerateNewPayrollErrorMessage(Decimal year)
        {
            String errorMsg = (String)GetGlobalResourceObject("ErrorMessages", "GenerateNewPayrollYearWarning");
            return String.Format(errorMsg, year, year - 1);
        }
        [WebMethod]
        public static String GetYearEndNegativeAmountEmployeeMessage(Decimal year, int revision)
        {
            BusinessLayer.BusinessObjects.Employee employee = Common.ServiceWrapper.HumanResourcesClient.GetYearEndNegativeAmountEmployee(year, revision);
            if (employee != null)
            {
                YearEndPage ye = new YearEndPage();
                return ye.GetErrorMessage(employee);
            }
            else
                return null;
        }
        [WebMethod]
        public static String GetGenerateNewPayrollYearMessage()
        {
            Decimal year = Common.ServiceWrapper.CodeClient.GetCurrentPayrollYear();
            YearEndPage yearEnd = new YearEndPage();
            return yearEnd.GetGenerateNewPayrollErrorMessage(year);
        }
    }
}