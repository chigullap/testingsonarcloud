﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.YearEndAdjustments
{
    public partial class YearEndAdjustmentDetailsControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public bool IsAddRevision
        {
            get { return ((YearEndAdjustment)Data[0]).Id < 0; }
        }
        public bool IsViewMode
        {
            get { return YearEndAdjustmentDetailsGrid.IsViewMode; }
        }
        public bool UpdateFlag
        {
            get { return Common.Security.RoleForm.YearEndAdjustmentsDetailsControl.UpdateFlag; }
        }
        public bool Enabled
        {
            get { return YearEndAdjustmentDetailsGrid.Enabled; }
            set { YearEndAdjustmentDetailsGrid.Enabled = value; }
        }
        public bool AllowUpdate { get; set; }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();
        }
        protected void WireEvents()
        {
            YearEndAdjustmentDetailsGrid.NeedDataSource += new GridNeedDataSourceEventHandler(YearEndAdjustmentDetailsGrid_NeedDataSource);
            YearEndAdjustmentDetailsGrid.UpdateAllCommand += new WLPGrid.UpdateAllCommandEventHandler(YearEndAdjustmentDetailsGrid_UpdateAllCommand);
            YearEndAdjustmentDetailsGrid.PreRender += new EventHandler(YearEndAdjustmentDetailsGrid_PreRender);
        }
        void ReassignAllowUpdate(YearEndAdjustment yearEndAdjustment)
        {
            //AllowUpdate will lose its value after edit/update so query the db to reassign its value
            switch (yearEndAdjustment.TableName)
            {
                case "year_end_t4":
                    {
                        YearEndT4 yearEndT4 = Common.ServiceWrapper.HumanResourcesClient.SelectSingleT4ByKey(Convert.ToInt64(yearEndAdjustment.Id));
                        YearEndCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetYearEnd(Convert.ToInt64(yearEndT4.Year));
                        AllowUpdate = (yearEndT4.Revision == collection[0].CurrentRevision);
                        break;
                    }
                case "year_end_t4a":
                    {
                        YearEndT4a yearEndT4a = Common.ServiceWrapper.HumanResourcesClient.SelectSingleT4aByKey(Convert.ToInt64(yearEndAdjustment.Id));
                        YearEndCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetYearEnd(Convert.ToInt64(yearEndT4a.Year));
                        AllowUpdate = (yearEndT4a.Revision == collection[0].CurrentRevision);
                        break;
                    }
                case "year_end_r1":
                    {
                        YearEndR1 yearEndR1 = Common.ServiceWrapper.HumanResourcesClient.SelectSingleR1ByKey(Convert.ToInt64(yearEndAdjustment.Id));
                        YearEndCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetYearEnd(Convert.ToInt64(yearEndR1.Year));
                        AllowUpdate = (yearEndR1.Revision == collection[0].CurrentRevision);
                        break;
                    }
                case "year_end_r2":
                    {
                        YearEndR2 yearEndR2 = Common.ServiceWrapper.HumanResourcesClient.SelectSingleR2ByKey(Convert.ToInt64(yearEndAdjustment.Id));
                        YearEndCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetYearEnd(Convert.ToInt64(yearEndR2.Year));
                        AllowUpdate = (yearEndR2.Revision == collection[0].CurrentRevision);
                        break;
                    }
                case "year_end_t4rsp":
                    {
                        YearEndT4rsp yearEndT4rsp = Common.ServiceWrapper.HumanResourcesClient.SelectSingleT4rspByKey(Convert.ToInt64(yearEndAdjustment.Id));
                        YearEndCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetYearEnd(Convert.ToInt64(yearEndT4rsp.Year));
                        AllowUpdate = (yearEndT4rsp.Revision == collection[0].CurrentRevision);
                        break;
                    }
                case "year_end_nr4":
                    {
                        YearEndNR4rsp yearEndNR4rsp = Common.ServiceWrapper.HumanResourcesClient.SelectSingleNR4rspByKey(Convert.ToInt64(yearEndAdjustment.Id));
                        YearEndCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetYearEnd(Convert.ToInt64(yearEndNR4rsp.Year));
                        AllowUpdate = (yearEndNR4rsp.Revision == collection[0].CurrentRevision);
                        break;
                    }
                case "year_end_t4arca":
                    {
                        YearEndT4arca yearEndT4arca = Common.ServiceWrapper.HumanResourcesClient.SelectSingleT4arcaByKey(Convert.ToInt64(yearEndAdjustment.Id));
                        YearEndCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetYearEnd(Convert.ToInt64(yearEndT4arca.Year));
                        AllowUpdate = (yearEndT4arca.Revision == collection[0].CurrentRevision);
                        break;
                    }
                default:
                    break;
            }
        }
        #endregion

        #region event handlers
        void YearEndAdjustmentDetailsGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            YearEndAdjustmentDetailsGrid.DataSource = Data;

            if (Data != null)
            {
                if (IsAddRevision)
                    YearEndAdjustmentDetailsGrid.SetEditAllMode(true);
                else
                    ReassignAllowUpdate(((YearEndAdjustment)Data[0]));
            }
        }
        void YearEndAdjustmentDetailsGrid_PreRender(object sender, EventArgs e)
        {
            SetGridEnabledMode(YearEndAdjustmentDetailsGrid, !IsViewMode);

            foreach (GridItem item in YearEndAdjustmentDetailsGrid.MasterTableView.Items)
            {
                TextBoxControl descriptionControl = (TextBoxControl)item.FindControl("Description");
                descriptionControl.ReadOnly = IsViewMode;

                NumericControl columnValueControl = (NumericControl)item.FindControl("ColumnValue");
                columnValueControl.ReadOnly = IsViewMode;

                TextBoxControl stringValueControl = (TextBoxControl)item.FindControl("StringValue");
                stringValueControl.ReadOnly = IsViewMode;

                NumericControl adjustmentValueControl = (NumericControl)item.FindControl("AdjustmentValue");
                adjustmentValueControl.ReadOnly = IsViewMode;

                NumericControl totalValueControl = (NumericControl)item.FindControl("TotalValue");
                totalValueControl.ReadOnly = true;
            }
        }
        private void SetGridEnabledMode(WLPGrid grid, bool isEnabled)
        {
            grid.ClientSettings.EnablePostBackOnRowClick = false;
            grid.ClientSettings.Selecting.AllowRowSelect = false;
        }
        public void LoadYearEndAdjustmentDetailsGrid(long employeeId, Decimal year, long yearEndAdjustmentTableId, String language, String employerNumber, String provinceStateCode, int revision, int currentRevision, bool addRevision)
        {
            AllowUpdate = ((addRevision ? revision + 1 : revision) == currentRevision);

            //if province was null in search return, it was passed here with an "x" to avoid an exception with page routing; replace with "" for the proc
            if (provinceStateCode == "x")
                provinceStateCode = "";

            Data = null;
            Data = YearEndAdjustmentCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetYearEndAdjustment(employeeId, year, yearEndAdjustmentTableId, employerNumber, provinceStateCode, revision, null));

            if (addRevision)
            {
                foreach (YearEndAdjustment item in Data)
                    item.Id = -1;
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //if an add or add revision is occuring, avoid this code as this is for existing records
            if (Data != null)
            {
                if (!IsAddRevision)
                {
                    //determine if current period is open; if not, do not allow edits
                    YearEndCollection yearEndcollection = new YearEndCollection();
                    yearEndcollection.Load(Common.ServiceWrapper.HumanResourcesClient.GetYearEnd(null));

                    foreach (BusinessLayer.BusinessObjects.YearEnd yearEnd in yearEndcollection)
                    {
                        if (yearEnd.CodeYearEndStatusCd == "OPEN")
                        {
                            GridCommandItem cmdItem = (GridCommandItem)YearEndAdjustmentDetailsGrid.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                            RadToolBar toolBar = (RadToolBar)cmdItem.FindControl("YearEndAdjustmentDetailsToolBar");
                            RadToolBarButton editAllButton = (RadToolBarButton)toolBar.FindButtonByCommandName("EditAll");
                            editAllButton.Enabled = true;
                            break;
                        }
                    }
                }
            }
        }
        protected void YearEndAdjustmentDetailsGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                WebControl amountControl = ((NumericControl)e.Item.FindControl("ColumnValue")).FieldControl;
                if (amountControl != null)
                {
                    WebControl adjustmentControl = ((NumericControl)e.Item.FindControl("AdjustmentValue")).FieldControl;
                    WebControl totalControl = ((NumericControl)e.Item.FindControl("TotalValue")).FieldControl;

                    amountControl.Attributes.Add("amountControlName", amountControl.ClientID);
                    amountControl.Attributes.Add("adjustmentControlName", adjustmentControl.ClientID);
                    amountControl.Attributes.Add("totalControlName", totalControl.ClientID);

                    adjustmentControl.Attributes.Add("amountControlName", amountControl.ClientID);
                    adjustmentControl.Attributes.Add("adjustmentControlName", adjustmentControl.ClientID);
                    adjustmentControl.Attributes.Add("totalControlName", totalControl.ClientID);
                }
            }
        }
        void YearEndAdjustmentDetailsGrid_UpdateAllCommand(object sender, UpdateAllCommandEventArgs e)
        {
            //pass the collection to WS, determine if updates or inserts are needed, then return collection
            YearEndAdjustmentCollection collection = new YearEndAdjustmentCollection();

            if (IsAddRevision)
            {
                YearEndAdjustmentsSearchReport report = ((YearEndAdjustmentPageControl)Parent.Parent).AddRevisionInsert();
                String key = report.KeyId.ToString().Substring(0, report.KeyId.ToString().Length - 1); //remove the last digit as it is corresponding to the form type

                foreach (YearEndAdjustment item in e.Items)
                    item.Id = Convert.ToInt64(key);
            }

            foreach (YearEndAdjustment item in e.Items)
                collection.Add(item);

            Common.ServiceWrapper.HumanResourcesClient.UpdateYearEndAdjustment(collection.ToArray());

            ReassignAllowUpdate(collection[0]);

            //need to set the ColumnValue to TotalValue and the AdjustmentValue to null when returning to ViewMode after an update
            foreach (YearEndAdjustment item in collection)
            {
                if (item.IsNumericFlag)
                {
                    ///hack
                    if (item.Key == "pension_adjustment_52")
                        item.ColumnValue = Math.Round(Convert.ToDecimal(item.TotalValue), 0, MidpointRounding.AwayFromZero).ToString();
                    else
                        item.ColumnValue = item.TotalValue;
                    item.AdjustmentValue = null;
                }
            }
        }
        #endregion
    }
}