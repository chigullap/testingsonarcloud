﻿using System;

namespace WorkLinks.Admin.YearEndAdjustments
{
    public partial class YearEndAdjustmentPage : WorkLinksPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "YearEndAdjustment")); ;
        }
    }
}