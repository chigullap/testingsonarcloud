﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Payroll.PayrollBatch;

namespace WorkLinks.Admin.YearEndAdjustments
{
    public partial class YearEndAdjustmentPageControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private String _dupeEmpMsg = "";
        #endregion

        #region properties

        protected long YearEndAdjustmentTableId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["yearEndAdjustmentTableId"]); }
        }
        protected String YearEndTypeForm
        {
            get { return Convert.ToString(Page.RouteData.Values["yearEndTypeForm"]); }
        }
        protected String BusinessNumber
        {
            get { return Convert.ToString(Page.RouteData.Values["businessNumber"]) == "x" ? "" : Convert.ToString(Page.RouteData.Values["businessNumber"]); }

        }
        protected String Province
        {
            get { return Convert.ToString(Page.RouteData.Values["province"]) == "x" ? "" : Convert.ToString(Page.RouteData.Values["province"]); }
        }
        protected long EmployeeId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); }
        }
        protected Decimal Year
        {
            get { return Convert.ToDecimal(Page.RouteData.Values["year"]); }
        }
        protected long KeyId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["keyId"]); }
        }
        protected String Action
        {
            get { return Convert.ToString(Page.RouteData.Values["action"]).ToLower(); }
        }
        protected int Revision
        {
            get { return Convert.ToInt16(Page.RouteData.Values["revision"]); }
        }
        protected int CurrentRevision
        {
            get { return Convert.ToInt16(Page.RouteData.Values["currentRevision"]); }
        }
        public bool IsFormInsertMode
        {
            get { return YearEndAdjustmentView.CurrentMode.Equals(FormViewMode.Insert); }
        }
        public String DupeUserMsg
        {
            get { return _dupeEmpMsg; }
            set { _dupeEmpMsg = value; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();

            if (!IsPostBack)
            {
                if (Action.Trim().Equals("add"))
                    YearEndAdjustmentView.ChangeMode(FormViewMode.Insert);
                else //we are in view or add revision mode
                {
                    LoadYearEndAdjustmentHeader();
                    LoadYearEndAdjustmentDetailsGrid(EmployeeId, Year, YearEndAdjustmentTableId, LanguageCode, BusinessNumber, Province, Revision, CurrentRevision, Action.Equals("addrevision") ? true : false);

                    //initialize controls
                    Initialize();
                }
            }
        }
        protected void WireEvents()
        {
            YearEndAdjustmentView.Inserting += new WLPFormView.ItemInsertingEventHandler(YearEndAdjustmentView_Inserting);

            //only subscribed to when in edit mode
            EmployeeControl employeeControl = (EmployeeControl)YearEndAdjustmentView.FindControl("EmployeeIdControl");
            if (employeeControl != null)
                employeeControl.EmployeeChanged += new EmployeeControl.EmployeeChangedEventHandler(EmployeeControl_Handler);
        }
        private void Initialize()
        {
            YearEndAdjustmentDetailsControl1.Enabled = !(Action.ToLower() == "add");
        }
        protected void LoadYearEndAdjustmentHeader()
        {
            YearEndAdjustmentView.DataBind();
        }
        protected void LoadYearEndAdjustmentDetailsGrid(long employeeId, Decimal year, long yearEndAdjustmentTableId, String language, String employerNumber, String provinceStateCode, int revision, int currentRevision, bool addRevision)
        {
            YearEndAdjustmentDetailsControl1.LoadYearEndAdjustmentDetailsGrid(employeeId, year, yearEndAdjustmentTableId, language, employerNumber, provinceStateCode, revision, currentRevision, addRevision);
        }
        #endregion

        #region event handlers
        protected void YearEndAdjustmentView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            if (KeyId > 0) //view or add revision mode
            {
                YearEndAdjustmentsSearchReportCollection temp = new YearEndAdjustmentsSearchReportCollection();
                YearEndAdjustmentsSearchReport item = new YearEndAdjustmentsSearchReport();

                if (Action.Equals("addrevision"))
                    item.YearEndAdjustmentTableId = YearEndAdjustmentTableId;
                else
                    item.KeyId = KeyId;

                item.Year = Year;
                item.EmployeeId = EmployeeId;
                item.TaxableCodeProvinceStateCd = Province;
                item.EmployerNumber = BusinessNumber;
                item.YearEndTypeForm = YearEndTypeForm;

                temp.Add(item);
                Data = temp;
                YearEndAdjustmentView.DataSource = Data;
            }
            else //add mode
            {
                YearEndAdjustmentsSearchReportCollection collection = new YearEndAdjustmentsSearchReportCollection();
                collection.Load(new YearEndAdjustmentsSearchReport());

                //set the year with the current open year
                YearEndCollection yearEndcollection = new YearEndCollection();
                yearEndcollection.Load(Common.ServiceWrapper.HumanResourcesClient.GetYearEnd(null));

                foreach (BusinessLayer.BusinessObjects.YearEnd yearEnd in yearEndcollection)
                {
                    if (yearEnd.CodeYearEndStatusCd == "OPEN")
                    {
                        collection[0].Year = yearEnd.Year;
                        break;
                    }
                }

                //assign to datasource
                YearEndAdjustmentView.DataSource = collection;
            }
        }
        private void EmployeeControl_Handler(object sender, EventArgs e)
        {
            //this will remove the error message if it has been shown to the user and they have then selected a new employee
            if (DupeUserMsg != "")
            {
                DupeUserMsg = "";
                WLPLabel messageLabel = (WLPLabel)YearEndAdjustmentView.FindControl("Message");
                if (messageLabel != null)
                    messageLabel.DataBind();
            }
        }
        protected void YearEndTypeForm_NeedDataSource(object sender, EventArgs e)
        {
            if (((ComboBoxControl)sender).ReadOnly)
                ((ComboBoxControl)sender).DataSource = new CodeCollection() { new CodeObject() { Code = YearEndTypeForm, Description = YearEndTypeForm } };
            else
            {
                CodeCollection yearEndFormTypeCombo = new CodeCollection();
                YearEndAdjustmentTableCollection collection = new YearEndAdjustmentTableCollection();
                collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetYearEndAdjustmentTables());

                foreach (YearEndAdjustmentTable obj in collection)
                    yearEndFormTypeCombo.Add(new CodeObject() { Code = obj.YearEndAdjustmentTableId.ToString(), Description = obj.Description });

                ((ComboBoxControl)sender).DataSource = yearEndFormTypeCombo;
            }
        }
        protected void YearCode_NeedDataSource(object sender, EventArgs e)
        {
            ((ComboBoxControl)sender).DataSource = new CodeCollection() { new CodeObject() { Code = ((YearEndAdjustmentsSearchReport)YearEndAdjustmentView.DataSource[0]).Year.ToString(), Description = ((YearEndAdjustmentsSearchReport)YearEndAdjustmentView.DataSource[0]).Year.ToString() } };
        }
        protected void ProvinceCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateProvince((ICodeControl)sender);
        }
        protected void BusinessNumberCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateEmployerNumber((ICodeControl)sender);
        }
        protected void YearEndTypeForm_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            bool t4Form = ((ComboBoxControl)sender).Value.ToString() == "1";
            bool r1r2Form = (((ComboBoxControl)sender).Value.ToString() == "3" || ((ComboBoxControl)sender).Value.ToString() == "4");
            ComboBoxControl provinceControl = (ComboBoxControl)YearEndAdjustmentView.FindControl("Province");
            provinceControl.Mandatory = t4Form;
            provinceControl.Value = "";
            provinceControl.ReadOnly = !t4Form;
            
            ComboBoxControl businessNumberControl = (ComboBoxControl)YearEndAdjustmentView.FindControl("BusinessNumber");
            businessNumberControl.Mandatory = !r1r2Form;
            businessNumberControl.ReadOnly = r1r2Form;
            if (r1r2Form)
                businessNumberControl.Value = "";
         }
        protected void EmployeeId_DataBinding(object sender, EventArgs e)
        {
            PopulateEmployeeControl((KeyValueControl)sender);
        }
        protected void PopulateEmployeeControl(KeyValueControl control)
        {
            Common.CodeHelper.PopulateEmployeeControl(control, false);
        }
        #endregion

        #region handles updates
        public YearEndAdjustmentsSearchReport AddRevisionInsert()
        {
            YearEndAdjustmentsSearchReport item = (YearEndAdjustmentsSearchReport)Data[0];
            String key = KeyId.ToString().Substring(0, KeyId.ToString().Length - 1); //remove the last digit as it is corresponding to the form type

            //get current revision from YEAR_END for year being inserted with
            int currentRevision = (Common.ServiceWrapper.HumanResourcesClient.GetYearEnd(Convert.ToInt64(item.Year)))[0].CurrentRevision;
            item.Revision = currentRevision;

            if (item.YearEndAdjustmentTableId != 1)
                item.TaxableCodeProvinceStateCd = "";

            return Common.ServiceWrapper.HumanResourcesClient.InsertYearEndAdjustment(item, Convert.ToInt64(key));
        }
        void YearEndAdjustmentView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            YearEndAdjustmentsSearchReport item = (YearEndAdjustmentsSearchReport)e.DataItem;

            //get current revision from YEAR_END for year being inserted with
            int currentRevision = (Common.ServiceWrapper.HumanResourcesClient.GetYearEnd(Convert.ToInt64(item.Year)))[0].CurrentRevision;
            item.Revision = currentRevision;

            if (item.YearEndAdjustmentTableId != 1)
                item.TaxableCodeProvinceStateCd = "";

            //validate that the combination of employee_id, year, form type (YearEndAdjustmentTableId), employer_number, province is unique, display a validation message if not.
            YearEndAdjustmentCollection temp = Common.ServiceWrapper.HumanResourcesClient.GetYearEndAdjustment(item.EmployeeId, item.Year, item.YearEndAdjustmentTableId, item.EmployerNumber, item.TaxableCodeProvinceStateCd, item.Revision, null);

            if (temp.Count == 0)
            {
                item = Common.ServiceWrapper.HumanResourcesClient.InsertYearEndAdjustment(item);

                if (item.YearEndAdjustmentTableId != 1)
                    item.TaxableCodeProvinceStateCd = "x"; //used in routing
                if(item.YearEndAdjustmentTableId == 3 || item.YearEndAdjustmentTableId== 4)
                    item.EmployerNumber = "x";
                Response.Redirect(String.Format("~/Admin/YearEndAdjustments/YearEndAdjustment/View/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}/{8}", item.KeyId, item.Year, item.EmployeeId, item.TaxableCodeProvinceStateCd, item.EmployerNumber, item.YearEndTypeForm, item.YearEndAdjustmentTableId, item.Revision, currentRevision));
            }
            else
            {
                //dupe found
                DupeUserMsg = String.Format("{0}", GetGlobalResourceObject("ErrorMessages", "DupeT4Adjustment"));
                e.Cancel = true;
            }
        }
        #endregion
    }
}