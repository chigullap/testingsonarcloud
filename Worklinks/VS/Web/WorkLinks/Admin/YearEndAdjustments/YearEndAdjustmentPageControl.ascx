﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="YearEndAdjustmentPageControl.ascx.cs" Inherits="WorkLinks.Admin.YearEndAdjustments.YearEndAdjustmentPageControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Src="YearEndAdjustmentDetailsControl.ascx" TagName="YearEndAdjustmentDetailsControl" TagPrefix="uc2" %>
<%@ Register Src="../../Payroll/PayrollBatch/EmployeeControl.ascx" TagName="EmployeeControl" TagPrefix="uc1" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy11" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="HeaderPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="HeaderPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="HeaderPanel" runat="server">
    <wasp:WLPFormView ID="YearEndAdjustmentView" runat="server" RenderOuterTable="false" DataKeyNames="Key" OnNeedDataSource="YearEndAdjustmentView_NeedDataSource">
        <ItemTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="Year" runat="server" ResourceName="Year" Value='<%# Year %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:KeyValueControl ID="EmployeeId" runat="server" OnDataBinding="EmployeeId_DataBinding" ResourceName="EmployeeId" Value='<%# EmployeeId %>' ReadOnly="true"></wasp:KeyValueControl>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="Province" ClientIDMode="Static" runat="server" Type="Province" OnDataBinding="ProvinceCode_NeedDataSource" Value='<%# Bind("TaxableCodeProvinceStateCd") %>' ResourceName="Province" ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="BusinessNumber" runat="server" ResourceName="BusinessNumber" Value='<%# BusinessNumber %>' ReadOnly="true" IncludeEmpty="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="YearEndTypeForm" runat="server" Type="YearEndTypeForm" OnDataBinding="YearEndTypeForm_NeedDataSource" IncludeEmptyItem="false" ResourceName="YearEndTypeForm" ReadOnly="true" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </ItemTemplate>
        <EditItemTemplate>
            <wasp:WLPToolBar ID="PayrollBatchToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
                <Items>
                    <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsFormInsertMode %>' CommandName="insert" ResourceName="Insert" />
                    <wasp:WLPToolBarButton Text="Cancel" onclick="Close()" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="Close" CausesValidation="false" ResourceName="Cancel" />
                </Items>
            </wasp:WLPToolBar>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="Year" runat="server" Type="Year" OnDataBinding="YearCode_NeedDataSource" Value='<%# Bind("Year") %>' IncludeEmptyItem="false" ResourceName="Year" ReadOnly="true" TabIndex="010" />
                        </td>
                        <td>
                            <uc1:EmployeeControl ID="EmployeeControl1" runat="server" Value='<%# Bind("EmployeeId") %>' ResourceName="EmployeeControl1" TabIndex="020" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="Province" ClientIDMode="Static" runat="server" Type="Province" OnDataBinding="ProvinceCode_NeedDataSource" Value='<%# Bind("TaxableCodeProvinceStateCd") %>' ResourceName="Province" TabIndex="030" />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="BusinessNumber" runat="server" Type="BusinessNumber" OnDataBinding="BusinessNumberCode_NeedDataSource" Value='<%# Bind("EmployerNumber") %>' IncludeEmptyItem="true" ResourceName="BusinessNumber" TabIndex="040" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="YearEndTypeForm" runat="server" Type="YearEndTypeForm" OnDataBinding="YearEndTypeForm_NeedDataSource" Value='<%# Bind("YearEndAdjustmentTableId") %>' IncludeEmptyItem="false" AutoPostback="true" OnSelectedIndexChanged="YearEndTypeForm_SelectedIndexChanged" ResourceName="YearEndTypeForm" TabIndex="050" />
                        </td>
                        <td>
                            <wasp:WLPLabel ID="Message" runat="server" CssClass="failureNotification" Text='<%# DupeUserMsg %>' />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </EditItemTemplate>
    </wasp:WLPFormView>

    <uc2:YearEndAdjustmentDetailsControl ID="YearEndAdjustmentDetailsControl1" runat="server" />
</asp:Panel>

<telerik:RadScriptBlock ID="RadScriptBlock11" runat="server">
    <script type="text/javascript">
        function Close(button, args) {
            var popup = getRadWindow();
            setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
        }

        function getRadWindow() {
            var popup = null;

            if (window.radWindow)
                popup = window.radWindow;
            else if (window.frameElement.radWindow)
                popup = window.frameElement.radWindow;

            return popup;
        }

        function processClick(commandName) {
            var arg = new Object;
            arg.closeWithChanges = true;
            getRadWindow().argument = arg;
        }

       function ToolBarClick(sender, args) {
            var button = args.get_item();
            var commandName = button.get_commandName();

            processClick(commandName);

            if (<%= IsFormInsertMode.ToString().ToLower() %>) {
                if (commandName == "cancel") {
                    window.close();
                }
            }
        }
    </script>
</telerik:RadScriptBlock>