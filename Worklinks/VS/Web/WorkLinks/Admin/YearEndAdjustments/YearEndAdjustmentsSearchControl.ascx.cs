﻿using System;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.YearEndAdjustments
{
    public partial class YearEndAdjustmentsSearchControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected bool EnableReportPrintButton
        {
            get
            {
                return Common.Security.RoleForm.T4Report.ViewFlag || Common.Security.RoleForm.T4AReport.ViewFlag || Common.Security.RoleForm.R1Report.ViewFlag ||
                    Common.Security.RoleForm.R2Report.ViewFlag || Common.Security.RoleForm.T4RSPReport.ViewFlag || Common.Security.RoleForm.T4ARCAReport.ViewFlag ||
                    Common.Security.RoleForm.NR4Report.ViewFlag;
            }
        }
        protected bool EnableYearEndAdjustmentsAuditDetailButton { get { return Common.Security.RoleForm.YearEndAdjustmentsAudit.ViewFlag; } }
        protected bool EnableYearEndAdjustmentsAuditButton { get { return Common.Security.RoleForm.YearEndAdjustmentReport.ViewFlag; } }
        protected YearEndAdjustmentsSearchCriteria Criteria
        {
            get
            {
                YearEndAdjustmentsSearchCriteria criteria = new YearEndAdjustmentsSearchCriteria();
                criteria.Year = (String)Year.Value;
                criteria.EmployeeNumber = (String)EmployeeNumber.Value;
                criteria.LastName = (String)LastName.Value;
                criteria.SIN = (String)SIN.Value;
                criteria.PayrollProcessGroupCode = (String)PayrollProcessGroupCode.Value;
                criteria.FormType = (String)FormType.Value;

                return criteria;
            }
            set
            {
                Year.Value = value.Year;
                EmployeeNumber.Value = value.EmployeeNumber;
                LastName.Value = value.LastName;
                SIN.Value = value.SIN;
                PayrollProcessGroupCode.Value = value.PayrollProcessGroupCode;
                FormType.Value = value.FormType;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.YearEndAdjustmentsSearch.ViewFlag);

            WireEvents();

            if (!IsPostBack)
                Panel1.DataBind();

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.Equals("refresh"))
            {
                //this is executed after an "Add" or "Add Revision"
                long keyId = -1;

                //if a row was selected, get its key
                if (YearEndAdjustmentSearchGrid.SelectedItems.Count != 0)
                    keyId = Convert.ToInt64(((GridDataItem)YearEndAdjustmentSearchGrid.SelectedItems[0]).GetDataKeyValue(YearEndAdjustmentSearchGrid.MasterTableView.DataKeyNames[0]));

                //refresh data
                Search(Criteria);

                //if a row was selected, reselect and update client java variables
                if (keyId != -1)
                {
                    //re-select the selected row
                    YearEndAdjustmentSearchGrid.SelectRowByFirstDataKey(keyId);
                    
                    //populate with values
                    UpdateVariablesEventArgs clientArgs = PopluateValues(((GridDataItem)YearEndAdjustmentSearchGrid.SelectedItems[0]));
                    
                    //call client side method
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "stacyTest", String.Format("UpdateVariablesAfterAddRevision({0},'{1}','{2}','{3}','{4}','{5}',{6},'{7}',{8},{9},{10},{11})", 
                        clientArgs.KeyId, clientArgs.EmployeeNumber, clientArgs.EmployerNumber, clientArgs.YearEndTypeForm, clientArgs.Year, clientArgs.CodePayrollProcessGroupCd,
                        clientArgs.EmployeeId, clientArgs.TaxableCodeProvinceStateCd, clientArgs.YearEndAdjustmentTableId, clientArgs.Revision, clientArgs.CurrentRevision, clientArgs.MaxRevision), true);
                }
            }
        }
        protected void WireEvents()
        {
            YearEndAdjustmentSearchGrid.NeedDataSource += new GridNeedDataSourceEventHandler(YearEndAdjustmentSearchGrid_NeedDataSource);
            YearEndAdjustmentSearchGrid.ItemDataBound += new GridItemEventHandler(YearEndAdjustmentSearchGrid_ItemDataBound);
            YearEndAdjustmentSearchGrid.PreRender += new EventHandler(YearEndAdjustmentSearchGrid_PreRender);
            YearEndAdjustmentSearchGrid.AllowPaging = true;
        }
        public void Search(YearEndAdjustmentsSearchCriteria criteria)
        {
            YearEndAdjustmentsSearchReportCollection collection = new YearEndAdjustmentsSearchReportCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetYearEndAdjustmentsSearchReport(criteria));
            Data = collection;

            YearEndAdjustmentSearchGrid.Rebind();
        }
        #endregion

        #region event handlers
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search(Criteria);
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Criteria = new YearEndAdjustmentsSearchCriteria();
            Data = null;
            YearEndAdjustmentSearchGrid.DataBind();
        }
        protected void YearEndAdjustmentSearchGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            YearEndAdjustmentSearchGrid.DataSource = Data;
        }
        protected void YearEndAdjustmentSearchGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                //work around to remove empty rows caused by the OnCommand Telerik bug
                if (String.Equals((dataItem["EmployeeNumber"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["LastName"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["FirstName"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["YearEndTypeForm"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["CodePayrollProcessGroupCd"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["TaxableCodeProvinceStateCd"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["EmployerNumber"].Text).ToUpper(), "&NBSP;")
                    )
                {
                    //hide the row
                    e.Item.Display = false;
                }
            }
        }
        void YearEndAdjustmentSearchGrid_PreRender(object sender, EventArgs e)
        {
            //work around to remove empty rows caused by the OnCommand Telerik bug
            if (Data == null)
                YearEndAdjustmentSearchGrid.AllowPaging = false;
            else
                YearEndAdjustmentSearchGrid.AllowPaging = true;
        }
        protected void YearCode_NeedDataSource(object sender, EventArgs e)
        {
            //select from the "year_end" table and use the year entries to populate the dropdown
            YearEndCollection collection = new YearEndCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetYearEnd(null));

            bool openStatusFound = false;
            CodeCollection yearCombo = new CodeCollection();

            foreach (BusinessLayer.BusinessObjects.YearEnd obj in collection)
            {
                if (obj.CodeYearEndStatusCd == "OPEN")
                    openStatusFound = true;

                CodeObject codeObj = new CodeObject();
                codeObj.Code = obj.Year.ToString();
                codeObj.Description = obj.Year.ToString();

                yearCombo.Add(codeObj);
            }

            ((ComboBoxControl)sender).DataSource = yearCombo;

            EnableDisableButtons(openStatusFound);
        }
        private void EnableDisableButtons(bool enabled)
        {
            ((WLPToolBarButton)YearEndAdjustmentSearchToolBar.FindButtonByCommandName("add")).Enabled = enabled;
            ((WLPToolBarButton)YearEndAdjustmentSearchToolBar.FindButtonByCommandName("Delete")).Enabled = enabled;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void YearEndAdjustmentSearchToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("delete") && YearEndAdjustmentSearchGrid.SelectedValue != null)
                DeleteYearEndAdjustment((YearEndAdjustmentsSearchReport)Data[YearEndAdjustmentSearchGrid.SelectedValue.ToString()]);
        }
        protected void DeleteYearEndAdjustment(YearEndAdjustmentsSearchReport reportData)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteYearEndReport(reportData);
                Data.Remove(reportData.Key);

                YearEndAdjustmentSearchGrid.SelectedIndexes.Clear();
                Search(Criteria);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region security handlers
        protected void AddToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.YearEndAdjustmentsSearch.AddFlag; }
        protected void AddRevisionToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.YearEndAdjustmentsSearch.AddFlag; }
        protected void DeleteToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.YearEndAdjustmentsSearch.DeleteFlag; }
        protected void DetailsToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.YearEndAdjustmentsSearch.ViewFlag; }
        protected void ReportToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableReportPrintButton; }
        protected void YearEndAdjustmentsAuditDetailToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableYearEndAdjustmentsAuditDetailButton; }
        protected void YearEndAdjustmentsAuditToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableYearEndAdjustmentsAuditButton; }
        #endregion

        private UpdateVariablesEventArgs PopluateValues(GridDataItem item)
        {
            UpdateVariablesEventArgs vars = new UpdateVariablesEventArgs();

            vars.KeyId = item.GetDataKeyValue("KeyId").ToString();
            vars.EmployeeNumber = (item.GetDataKeyValue("EmployeeNumber") != null) ? item.GetDataKeyValue("EmployeeNumber").ToString() : "null";
            vars.EmployerNumber = (item.GetDataKeyValue("EmployerNumber") != null) ? item.GetDataKeyValue("EmployerNumber").ToString() : "null";
            vars.YearEndTypeForm = (item.GetDataKeyValue("YearEndTypeForm") != null) ? item.GetDataKeyValue("YearEndTypeForm").ToString() : "null";
            vars.Year = (item.GetDataKeyValue("Year") != null) ? item.GetDataKeyValue("Year").ToString() : "null";
            vars.CodePayrollProcessGroupCd = (item.GetDataKeyValue("CodePayrollProcessGroupCd") != null) ? item.GetDataKeyValue("CodePayrollProcessGroupCd").ToString() : "null";
            vars.EmployeeId = (item.GetDataKeyValue("EmployeeId") != null) ? item.GetDataKeyValue("EmployeeId").ToString() : "null";
            vars.TaxableCodeProvinceStateCd = (item.GetDataKeyValue("TaxableCodeProvinceStateCd") != null) ? item.GetDataKeyValue("TaxableCodeProvinceStateCd").ToString() : "null";
            vars.YearEndAdjustmentTableId = (item.GetDataKeyValue("YearEndAdjustmentTableId") != null) ? item.GetDataKeyValue("YearEndAdjustmentTableId").ToString() : "null";
            vars.Revision = (item.GetDataKeyValue("Revision") != null) ? item.GetDataKeyValue("Revision").ToString() : "null";
            vars.CurrentRevision = (item.GetDataKeyValue("CurrentRevision") != null) ? item.GetDataKeyValue("CurrentRevision").ToString() : "null";
            vars.MaxRevision = (item.GetDataKeyValue("MaxRevision") != null) ? item.GetDataKeyValue("MaxRevision").ToString() : "null";

            return vars;
        }

        public class UpdateVariablesEventArgs : EventArgs
        {
            public string KeyId;
            public string EmployeeNumber;
            public string EmployerNumber;
            public string YearEndTypeForm;
            public string Year;
            public string CodePayrollProcessGroupCd;
            public string EmployeeId;
            public string TaxableCodeProvinceStateCd;
            public string YearEndAdjustmentTableId;
            public string Revision;
            public string CurrentRevision;
            public string MaxRevision;
        }
    }
}