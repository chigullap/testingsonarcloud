﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HumanResources/Employee.Master" AutoEventWireup="true" CodeBehind="YearEndAdjustmentPage.aspx.cs" Inherits="WorkLinks.Admin.YearEndAdjustments.YearEndAdjustmentPage" %>
<%@ Register Src="YearEndAdjustmentPageControl.ascx" TagName="YearEndAdjustmentPageControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <uc1:YearEndAdjustmentPageControl ID="YearEndAdjustmentPageControl1" runat="server" />
</asp:Content>