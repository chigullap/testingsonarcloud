﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="YearEndAdjustmentsSearchPage.aspx.cs" Inherits="WorkLinks.Admin.YearEndAdjustments.YearEndAdjustmentsSearchPage" %>
<%@ Register Src="YearEndAdjustmentsSearchControl.ascx" TagName="YearEndAdjustmentsSearchControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:YearEndAdjustmentsSearchControl ID="YearEndAdjustmentsSearchControl1" runat="server" />        
    </ContentTemplate>
</asp:Content>