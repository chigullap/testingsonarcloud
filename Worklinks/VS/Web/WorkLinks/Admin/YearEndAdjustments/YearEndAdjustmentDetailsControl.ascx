﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="YearEndAdjustmentDetailsControl.ascx.cs" Inherits="WorkLinks.Admin.YearEndAdjustments.YearEndAdjustmentDetailsControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="YearEndAdjustmentDetailsGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="YearEndAdjustmentDetailsGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<wasp:WLPGrid
    ID="YearEndAdjustmentDetailsGrid"
    EditMode="InPlace"
    runat="server"
    GridLines="None"
    Width="100%"
    Height="450px"
    AutoGenerateColumns="false"
    AutoInsertOnAdd="false"
    AllowMultiRowEdit="true"
    Skin=""
    OnItemDataBound="YearEndAdjustmentDetailsGrid_ItemDataBound">

    <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
        <Selecting AllowRowSelect="false" />
        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
        <CommandItemTemplate>
            <wasp:WLPToolBar ID="YearEndAdjustmentDetailsToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
                <Items>
                    <wasp:WLPToolBarButton Text="*Edit" ImageUrl="~/App_Themes/Default/edit.gif" CommandName="EditAll" Visible='<%# IsViewMode && UpdateFlag && AllowUpdate %>' Enabled="false" CausesValidation="true" ResourceName="Edit" />
                    <wasp:WLPToolBarButton Text="*Update" ImageUrl="~/App_Themes/Default/update.gif" CommandName="UpdateAll" Visible='<%# !IsViewMode %>' CausesValidation="true" ResourceName="Update" />
                    <wasp:WLPToolBarButton Text="*Cancel" ImageUrl="~/App_Themes/Default/cancel.gif" CommandName="CancelAll" Visible='<%# !IsViewMode && !IsAddRevision %>' CausesValidation="false" ResourceName="Cancel" />
                    <wasp:WLPToolBarButton Text="*Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" Visible='<%# IsAddRevision %>' onClick="Close()" CausesValidation="false" ResourceName="Cancel" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>
        <Columns>
            <wasp:GridTemplateControl UniqueName="HeaderColumns" Reorderable="false">
                <HeaderTemplate>
                    <table width="100%">
                        <tr>
                            <td width="30%" align="left">
                                <wasp:WLPLabel ID="Column" runat="server" ResourceName="ColumnText" Text="*Column" />
                            </td>
                            <td width="17%" align="right">
                                <wasp:WLPLabel ID="Field" runat="server" ResourceName="FieldText" Text="*Amount" />
                            </td>
                            <td width="17%" align="right">
                                <wasp:WLPLabel ID="Adjustment" runat="server" ResourceName="AdjustmentText" Text="*Adjustment" Visible='<%# !IsViewMode %>' />
                            </td>
                            <td width="17%" align="right">
                                <wasp:WLPLabel ID="Total" runat="server" ResourceName="TotalText" Text="*Total" Visible='<%# !IsViewMode %>' />
                            </td>
                            <td width="17%" align="right">
                                <wasp:WLPLabel ID="Maximum" runat="server" ResourceName="MaximumText" Text="*Maximum" Visible='<%# !IsViewMode %>' />
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table width="100%">
                        <tr>
                            <td width="30%" align="left">
                                <wasp:TextBoxControl ID="Description" Width="285px" HideLabel="true" runat="server" Value='<%# Bind("Description") %>' ReadOnly="true" />
                            </td>
                            <td width="17%" align="right">
                                <wasp:NumericControl ID="ColumnValue" GroupSeparator="" Width="166px" HideLabel="true" runat="server" Visible='<%# Bind("IsNumericFlag") %>' MaxLength='<%# Bind("FieldPrecision") %>' DecimalDigits='<%# Bind("FieldScale") %>' Value='<%# Bind("NumericValue") %>' ReadOnly="false" OnValueChanged="AdjustmentAmountValue_OnValueChanged" />
                                <wasp:TextBoxControl ID="StringValue"                   Width="166px" HideLabel="true" runat="server" Visible='<%# Bind("IsStringFlag") %>' MaxLength='<%# Bind("FieldPrecision") %>'                                            Value='<%# Bind("StringValue") %>' />
                            </td>
                            <td width="17%" align="right">
                                <wasp:NumericControl ID="AdjustmentValue" GroupSeparator="" Width="166px" HideLabel="true" runat="server" Visible='<%# Bind("IsNumericFlag") %>' MaxLength='<%# Bind("FieldPrecision") %>' DecimalDigits='<%# Bind("FieldScale") %>' Value='<%# Bind("AdjustmentValue") %>' ReadOnly="true" OnValueChanged="AdjustmentAmountValue_OnValueChanged" />
                            </td>
                            <td width="17%" align="right">
                                <wasp:NumericControl ID="TotalValue" GroupSeparator="" Width="166px" HideLabel="true" runat="server" Visible='<%# !IsViewMode %>' DecimalDigits='<%# Bind("FieldScale") %>' Value='<%# Eval("TotalValue") %>' ReadOnly="true" />
                            </td>
                            <td width="17%" align="right">
                                <wasp:NumericControl ID="MaximumValue" GroupSeparator="" Width="166px" HideLabel="true" runat="server" Visible='<%# !IsViewMode %>' MaxLength='<%# Bind("FieldPrecision") %>' DecimalDigits='<%# Bind("FieldScale") %>' Value='<%# Bind("MaximumValue") %>' ReadOnly="true" />
                            </td>
                        </tr>
                        <%--<tr>
                            <td colspan="3"></td>
                            <%--<td colspan="2">
                                <asp:RangeValidator CultureInvariantValues="true" ID="MinimumValidator" Enabled='<%# Bind("EnableMinimumValidator") %>' Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="TotalValue" ResourceName="TotalRangeValidator" Text="<%$ Resources:WarningMessages, MinimumExceededWarning %>" MinimumValue='<%# Bind("Minimum") %>' MaximumValue='999999999999' Type="Double" />
                                <asp:RangeValidator CultureInvariantValues="true" ID="MaximumValidator" Enabled='<%# Bind("EnableMaximumValidator") %>' Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="TotalValue" ResourceName="TotalRangeValidator" Text="<%$ Resources:WarningMessages, MaximumExceededWarning %>" MaximumValue='<%# Bind("Maximum") %>' Type="Double" />
                            </td>
                        </tr>--%>
                    </table>
                </ItemTemplate>
                <EditItemTemplate>
                    <table width="100%">
                        <tr>
                            <td width="30%" align="left">
                                <wasp:TextBoxControl ID="Description" Width="285px" HideLabel="true" runat="server" Value='<%# Bind("Description") %>' ReadOnly="true" />
                            </td>
                            <td width="17%" align="right">
                                <wasp:NumericControl ID="ColumnValue" GroupSeparator="" Width="166px" HideLabel="true" runat="server" Visible='<%# Bind("IsNumericFlag") %>' MaxLength='<%# Bind("FieldPrecision") %>' DecimalDigits='<%# Bind("FieldScale") %>' Value='<%# Bind("NumericValue") %>' ReadOnly='<%# Bind("AllowEdit") %>' TabIndex="010" />
                                <wasp:TextBoxControl ID="StringValue"                   Width="166px" HideLabel="true" runat="server" Visible='<%# Bind("IsStringFlag") %>' MaxLength='<%# Bind("FieldPrecision") %>'                                            Value='<%# Bind("StringValue") %>'                                                               ReadOnly='<%# Bind("AllowEdit") %>' TabIndex="010" />
                            </td>
                            <td width="17%" align="right">
                                <wasp:NumericControl ID="AdjustmentValue" GroupSeparator="" Width="166px" HideLabel="true" runat="server" Visible='<%# Bind("IsNumericFlag") %>' MaxLength='<%# Bind("FieldPrecision") %>' DecimalDigits='<%# Bind("FieldScale") %>' Value='<%# Bind("AdjustmentValue") %>' ReadOnly='<%# Bind("AllowEdit") %>' OnValueChanged="AdjustmentAmountValue_OnValueChanged" TabIndex="020" />
                            </td>
                            <td width="17%" align="right">
                                <wasp:NumericControl ID="TotalValue" GroupSeparator="" Width="166px" HideLabel="true" runat="server" Visible='<%# !IsViewMode %>' DecimalDigits='<%# Bind("FieldScale") %>' Value='<%# Eval("TotalValue") %>' ReadOnly="true" />
                            </td>
                            <td width="17%" align="right">
                                <wasp:NumericControl ID="MaximumValue" GroupSeparator="" Width="166px" HideLabel="true" runat="server" Visible='<%# !IsViewMode %>' MaxLength='<%# Bind("FieldPrecision") %>' DecimalDigits='<%# Bind("FieldScale") %>' Value='<%# Bind("MaximumValue") %>' ReadOnly="true" />
                            </td>
                        </tr>
                        <%--<tr>
                            <td colspan="3"></td>
                            <td colspan="2">
                                <asp:RangeValidator CultureInvariantValues="true" ID="MinimumValidator" Enabled='<%# Bind("EnableMinimumValidator") %>' Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="TotalValue" ResourceName="TotalRangeValidator" Text="<%$ Resources:WarningMessages, MinimumExceededWarning %>" MinimumValue='<%# Bind("Minimum") %>' Type="Double" />
                                <asp:RangeValidator CultureInvariantValues="true" ID="MaximumValidator" Enabled='<%# Bind("EnableMaximumValidator") %>' Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="TotalValue" ResourceName="TotalRangeValidator" Text="<%$ Resources:WarningMessages, MaximumExceededWarning %>" MaximumValue='<%# Bind("Maximum") %>' Type="Double" />
                            </td>
                        </tr>--%>
                    </table>
                </EditItemTemplate>
            </wasp:GridTemplateControl>
        </Columns>
    </MasterTableView>

    <HeaderContextMenu EnableAutoScroll="true" />

</wasp:WLPGrid>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function Close(button, args) {
            var popup = getRadWindow();
            setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
        }

        function getRadWindow() {
            var popup = null;

            if (window.radWindow)
                popup = window.radWindow;
            else if (window.frameElement.radWindow)
                popup = window.frameElement.radWindow;

            return popup;
        }

        function processClick(commandName) {
            var arg = new Object;
            arg.closeWithChanges = true;
            getRadWindow().argument = arg;
        }

        function ToolBarClick(sender, args) {
            var button = args.get_item();
            var commandName = button.get_commandName();
            processClick(commandName);
        }

        function AdjustmentAmountValue_OnValueChanged(sender, eventArgs) {
            var amountControl = $find(sender.get_element().attributes["amountControlName"].value);
            var adjustmentControl = $find(sender.get_element().attributes["adjustmentControlName"].value);
            var totalControl = $find(sender.get_element().attributes["totalControlName"].value);

            var amount = amountControl._value ? amountControl._value : 0;
            var adjustment = adjustmentControl._value ? adjustmentControl._value : 0;
            var total = amount + adjustment;

            totalControl._element.value = total.toFixed(2);
        }
    </script>
</telerik:RadScriptBlock>