﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="YearEndAdjustmentsSearchControl.ascx.cs" Inherits="WorkLinks.Admin.YearEndAdjustments.YearEndAdjustmentsSearchControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="SearchPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SearchPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="SearchPanel" runat="server">
    <table width="100%">
        <tr valign="top">
            <td>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                    <table width="800px">
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="Year" LabelText="*Year" runat="server" Type="Year" OnDataBinding="YearCode_NeedDataSource" IncludeEmptyItem="false" ResourceName="Year" TabIndex="010" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="EmployeeNumber" LabelText="*EmployeeNumber" runat="server" ResourceName="EmployeeNumber" TabIndex="020" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="LastName" LabelText="*LastName" runat="server" ResourceName="LastName" TabIndex="030" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="SIN" LabelText="*SIN" runat="server" ResourceName="SIN" TabIndex="040" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="PayrollProcessGroupCode" LabelText="*ProcessGroup" runat="server" Type="PayrollProcessGroupCode" OnDataBinding="Code_NeedDataSource" ResourceName="PayrollProcessGroupCode" TabIndex="050" />
                            </td>
                            <td>
                                <wasp:ComboBoxControl ID="FormType" LabelText="*FormType" runat="server" Type="YearEndFormCode" OnDataBinding="Code_NeedDataSource" ResourceName="YearEndTypeForm" TabIndex="060" />
                            </td>
                        </tr>
                    </table>
                    <div class="SearchCriteriaButtons">
                        <wasp:WLPButton ID="btnClear" Icon-PrimaryIconUrl="~/App_Themes/Default/clear.gif" runat="server" Text="Clear" ResourceName="btnClear" OnClick="btnClear_Click" OnClientClicked="clear" CssClass="button" />
                        <wasp:WLPButton ID="btnSearch" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" runat="server" Text="Search" ResourceName="btnSearch" OnClick="btnSearch_Click" />
                    </div>
                </asp:Panel>
            </td>
        </tr>
        <tr valign="top">
            <td>
                <wasp:WLPToolBar ID="YearEndAdjustmentSearchToolBar" runat="server" Width="100%" OnClientLoad="YearEndAdjustmentSearchToolBar_init" OnButtonClick="YearEndAdjustmentSearchToolBar_ButtonClick" OnClientButtonClicking="onClientButtonClicking">
                    <items>
                        <wasp:WLPToolBarButton OnPreRender="AddToolBar_PreRender" Text="*Add" ImageUrl="~/App_Themes/Default/Add.gif" onclick="return Add()" Enabled="false" CommandName="add" ResourceName="Add" />
                        <wasp:WLPToolBarButton OnPreRender="AddRevisionToolBar_PreRender" Text="*AddRevision" ImageUrl="~/App_Themes/Default/Add.gif" onclick="return AddRevision()" Enabled="false" CommandName="addRevision" ResourceName="AddRevision" />
                        <wasp:WLPToolBarButton OnPreRender="DeleteToolBar_PreRender" Text="*Delete" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ResourceName="Delete" />
                        <wasp:WLPToolBarButton OnPreRender="DetailsToolBar_PreRender" Text="*Details" onclick="Open()" CommandName="details" ResourceName="Details" />
                        <wasp:WLPToolBarButton OnPreRender="ReportToolBar_PreRender" Text="*Report" onclick="openReportPrint();" runat="server" CommandName="Report" ResourceName="Report" />
                        <wasp:WLPToolBarButton OnPreRender="YearEndAdjustmentsAuditDetailToolBar_PreRender" Text="*YearEndAdjustmentsAuditDetail" onclick="openYearEndAdjustmentsAuditDetail();" runat="server" CommandName="YearEndAdjustmentsAuditDetail" ResourceName="YearEndAdjustmentsAudit" />
                        <wasp:WLPToolBarButton OnPreRender="YearEndAdjustmentsAuditToolBar_PreRender" Text="*YearEndAdjustmentsAudit" onclick="openYearEndAdjustmentsAudit();" runat="server" CommandName="YearEndAdjustmentsAudit" ResourceName="YearEndAdjustmentsAudit2" />
                    </items>
                </wasp:WLPToolBar>
                <wasp:WLPGrid
                    ID="YearEndAdjustmentSearchGrid"
                    runat="server"
                    AllowPaging="true"
                    PagerStyle-AlwaysVisible="true"
                    AllowSorting="true"
                    PageSize="100"
                    GridLines="None"
                    Height="400px"
                    AutoAssignModifyProperties="true">

                    <clientsettings allowcolumnsreorder="true" reordercolumnsonclient="true">
                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                        <Selecting AllowRowSelect="true" />
                        <ClientEvents OnRowClick="OnRowClick" OnRowDblClick="OnRowDblClick" OnGridCreated="onGridCreated" />
                    </clientsettings>

                    <mastertableview clientdatakeynames="EmployeeId, KeyId, EmployeeNumber, Year, CodePayrollProcessGroupCd, TaxableCodeProvinceStateCd, EmployerNumber, YearEndTypeForm, YearEndAdjustmentTableId, Revision, CurrentRevision, MaxRevision" autogeneratecolumns="false" datakeynames="KeyId" commanditemdisplay="Top">
                        <CommandItemTemplate></CommandItemTemplate>
                        <Columns>
                            <wasp:GridBoundControl DataField="Revision" LabelText="*Revision" SortExpression="Revision" UniqueName="Revision" ResourceName="Revision" />
                            <wasp:GridBoundControl DataField="EmployeeNumber" LabelText="*EmployeeNumber" SortExpression="EmployeeNumber" UniqueName="EmployeeNumber" ResourceName="EmployeeNumber" HeaderStyle-Width="18%" />
                            <wasp:GridBoundControl DataField="LastName" LabelText="*LastName" SortExpression="LastName" UniqueName="LastName" ResourceName="LastName" />
                            <wasp:GridBoundControl DataField="FirstName" LabelText="*FirstName" SortExpression="FirstName" UniqueName="FirstName" ResourceName="FirstName" />
                            <wasp:GridBoundControl DataField="YearEndTypeForm" LabelText="*YearEndTypeForm" SortExpression="YearEndTypeForm" UniqueName="YearEndTypeForm" ResourceName="YearEndTypeForm" />
                            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="*CodePayrollProcessGroupCd" DataField="CodePayrollProcessGroupCd" Type="PayrollProcessGroupCode" ResourceName="CodePayrollProcessGroupCd" />
                            <wasp:GridBoundControl DataField="TaxableCodeProvinceStateCd" LabelText="*TaxableCodeProvinceStateCd" SortExpression="TaxableCodeProvinceStateCd" UniqueName="TaxableCodeProvinceStateCd" ResourceName="TaxableCodeProvinceStateCd" />
                            <wasp:GridBoundControl DataField="EmployerNumber" LabelText="*EmployerNumber" SortExpression="EmployerNumber" UniqueName="EmployerNumber" ResourceName="EmployerNumber" />
                        </Columns>
                    </mastertableview>

                    <headercontextmenu enableautoscroll="true" />

                </wasp:WLPGrid>
            </td>
        </tr>
    </table>
</asp:Panel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="YearEndWindows"
    Width="1050"
    Height="600"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    OnClientClose="onClientClose"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="YearEndReportWindows"
    Width="1050"
    Height="600"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    OnClientClose="onClientClose"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var toolbar;
        var keyId;
        var employeeNumber;
        var formType;
        var processGroup;
        var year;
        var employerNumber;
        var province;
        var revision;
        var currentRevision;
        var maxRevision;
        var addRevisionButtonEnabled;

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function clear() {
            employeeId = null;
            keyId = null;
            addRevisionButtonEnabled = false;
        }

        function YearEndAdjustmentSearchToolBar_init(sender) {
            initializeControls();
        }

        function getSelectedRow() {
            var yearEndAdjustmentSearchGrid = $find('<%= YearEndAdjustmentSearchGrid.ClientID %>');
            if (yearEndAdjustmentSearchGrid != null) {
                var MasterTable = yearEndAdjustmentSearchGrid.get_masterTableView();
                if (MasterTable != null)
                    return MasterTable.get_selectedItems()[0];
                else
                    return null;
            }
            else
                return null;
        }

        function rowIsSelected() {
            var getSeletecteRow = getSelectedRow();
            return (getSeletecteRow != null);
        }

        function initializeControls() {
            if ($find('<%= YearEndAdjustmentSearchToolBar.ClientID %>') != null)
                toolbar = $find('<%= YearEndAdjustmentSearchToolBar.ClientID %>');

            enableButtons(rowIsSelected());
        }

        function OnRowDblClick(sender, eventArgs) {
            if (toolbar.findButtonByCommandName('details') != null)
                Open(sender);
        }

        function onGridCreated(sender, eventArgs) {
            if (rowIsSelected()) {
                rowSelected = true;
                var selectedRow = getSelectedRow();

                if (eventArgs.getDataKeyValue != null) {
                    keyId = eventArgs.getDataKeyValue('KeyId');
                    employeeNumber = eventArgs.getDataKeyValue('EmployeeNumber');
                    employerNumber = eventArgs.getDataKeyValue('EmployerNumber');
                    formType = eventArgs.getDataKeyValue('YearEndTypeForm');
                    year = eventArgs.getDataKeyValue('Year');
                    processGroup = eventArgs.getDataKeyValue('CodePayrollProcessGroupCd');
                    employeeId = eventArgs.getDataKeyValue('EmployeeId');
                    province = eventArgs.getDataKeyValue('TaxableCodeProvinceStateCd');
                    businessNumber = eventArgs.getDataKeyValue('EmployerNumber');
                    yearEndTypeForm = eventArgs.getDataKeyValue('YearEndTypeForm');
                    yearEndAdjustmentTableId = eventArgs.getDataKeyValue('YearEndAdjustmentTableId');
                    revision = eventArgs.getDataKeyValue('Revision');
                    currentRevision = eventArgs.getDataKeyValue('CurrentRevision');
                    maxRevision = eventArgs.getDataKeyValue('MaxRevision');
                    enableButtons(true);
                }
            }
            else {
                employeeId = null;
                lastName = "";
                firstName = "";
                employeeNumber = "";
                employerNumber = "";
                formType = "";
                processGroup = "";
                revision = "";
                currentRevision = "";
                maxRevision = "";
                keyId = null;
                addRevisionButtonEnabled = false;
            }
        }

        function OnRowClick(sender, eventArgs) {
            keyId = eventArgs.getDataKeyValue('KeyId');
            employeeNumber = eventArgs.getDataKeyValue('EmployeeNumber');
            employerNumber = eventArgs.getDataKeyValue('EmployerNumber');
            formType = eventArgs.getDataKeyValue('YearEndTypeForm');
            year = eventArgs.getDataKeyValue('Year');
            processGroup = eventArgs.getDataKeyValue('CodePayrollProcessGroupCd');
            employeeId = eventArgs.getDataKeyValue('EmployeeId');
            province = eventArgs.getDataKeyValue('TaxableCodeProvinceStateCd');
            businessNumber = eventArgs.getDataKeyValue('EmployerNumber');
            yearEndTypeForm = eventArgs.getDataKeyValue('YearEndTypeForm');
            yearEndAdjustmentTableId = eventArgs.getDataKeyValue('YearEndAdjustmentTableId');
            revision = eventArgs.getDataKeyValue('Revision');
            currentRevision = eventArgs.getDataKeyValue('CurrentRevision');
            maxRevision = eventArgs.getDataKeyValue('MaxRevision');

            rowSelected = true;

            enableButtons(true);
        }

        function UpdateVariablesAfterAddRevision(KeyId, EmployeeNumber, EmployerNumber, YearEndTypeForm, Year, CodePayrollProcessGroupCd, EmployeeId, TaxableCodeProvinceStateCd, 
            YearEndAdjustmentTableId, Revision, CurrentRevision, MaxRevision){

            keyId = (KeyId == "null") ? null : KeyId;
            employeeNumber = (EmployeeNumber == "null") ? null : EmployeeNumber;
            employerNumber = (EmployerNumber == "null") ? null : EmployerNumber;
            formType = (YearEndTypeForm == "null") ? null : YearEndTypeForm;
            year = (Year == "null") ? null : Year;
            processGroup = (CodePayrollProcessGroupCd == "null") ? null : CodePayrollProcessGroupCd;
            employeeId = (EmployeeId == "null") ? null : EmployeeId;
            province = (TaxableCodeProvinceStateCd == "null") ? null : TaxableCodeProvinceStateCd;
            businessNumber = (EmployerNumber == "null") ? null : EmployerNumber;
            yearEndTypeForm = (YearEndTypeForm == "null") ? null : YearEndTypeForm;
            yearEndAdjustmentTableId = (YearEndAdjustmentTableId == "null") ? null : YearEndAdjustmentTableId;
            revision = (Revision == "null") ? null : Revision;
            currentRevision = (CurrentRevision == "null") ? null : CurrentRevision;
            maxRevision = (MaxRevision == "null") ? null : MaxRevision;
        }

        function enableButtons(enable) {
            var addRevisionButton = toolbar.findButtonByCommandName('addRevision');
            var detailsButton = toolbar.findButtonByCommandName('details');
            var deleteButton = toolbar.findButtonByCommandName('Delete');
            var reportButton = toolbar.findButtonByCommandName('Report');
            var adjustmentsAuditDetailButton = toolbar.findButtonByCommandName('YearEndAdjustmentsAuditDetail');
            var adjustmentsAuditButton = toolbar.findButtonByCommandName('YearEndAdjustmentsAudit');

            //for adding a revision, only enable the button if the revision number on the row click is not equal to the YEAR END current revision AND the revision number is the max for this form/employee/province
            if (addRevisionButton != null)
                addRevisionButton.set_enabled(enable && (revision != currentRevision) && (revision == maxRevision));

            addRevisionButtonEnabled = (enable && (revision != currentRevision) && (revision == maxRevision));

            if (detailsButton != null)
                detailsButton.set_enabled(enable);

            //new rules to enable delete button.  The form revision must match the year end current revision in order to be able to delete it.
            if (deleteButton != null)
                deleteButton.set_enabled(enable &&(revision == currentRevision));

            if (reportButton != null)
                reportButton.set_enabled(enable);

            if (adjustmentsAuditDetailButton != null)
                adjustmentsAuditDetailButton.set_enabled(enable);

            if (adjustmentsAuditButton != null)
                adjustmentsAuditButton.set_enabled(enable);
        }

        function Open() {
            if (keyId != null) {
                if (province == null) province = 'x'; //t4a records don't have province returned in the search results
                if (businessNumber == null || businessNumber == '') businessNumber = 'x';
                openWindowWithManager('YearEndWindows', String.format('/Admin/YearEndAdjustments/YearEndAdjustment/View/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}/{8}', keyId, year, employeeId, province, businessNumber, yearEndTypeForm, yearEndAdjustmentTableId, revision, currentRevision), false);
            }
        }

        function Add() {
            var addButton = toolbar.findButtonByCommandName('add');
            if (addButton._control._itemData[0].enabled == undefined) //this property wont exist unless the control is disabled by the C# side
                openWindowWithManager('YearEndWindows', String.format('/Admin/YearEndAdjustments/YearEndAdjustment/Add/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}/{8}', '0', -1, '1', 'x', '1', 'x', '1', -1, -1), false); //all dummy values in this string to prevent routing crash
        }

        function AddRevision(){
            if (addRevisionButtonEnabled && keyId != null) {
                if (province == null) province = 'x'; //t4a records don't have province returned in the search results
                if (businessNumber == null || businessNumber == '') businessNumber = 'x'
                openWindowWithManager('YearEndWindows', String.format('/Admin/YearEndAdjustments/YearEndAdjustment/AddRevision/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}/{8}', keyId, year, employeeId, province, businessNumber, yearEndTypeForm, yearEndAdjustmentTableId, revision, currentRevision), false);
            }
        }

        function openReportPrint() {
            if (keyId != null) {
                if (province == null) province = 'x'; //t4a records don't have province returned in the search results
                if (employerNumber == null || employerNumber == '') employerNumber = 'x';
                openWindowWithManager('YearEndReportWindows', String.format('/Report/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}', formType, keyId, employeeId, 'PDF', year, employerNumber, province, revision), true);
                return false;
            }
        }

        function openYearEndAdjustmentsAuditDetail() {
            if (keyId != null) {
                if (province == null) province = 'x'; //t4a records don't have province returned in the search results
                if (employerNumber == null || employerNumber == '') employerNumber = 'x';
                var frame = document.createElement("frame");
                frame.src = getUrl(String.format('/Report/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}', 'YeAdjustAudit', keyId, employeeNumber, 'EXCEL', year, employerNumber, province, revision)), null;
                frame.style.display = "none";
                document.body.appendChild(frame);
                return false;
            }
        }
        
        function openYearEndAdjustmentsAudit() { 
            if (keyId != null) {
                if (province == null) province = 'x';
                if (employerNumber == null || employerNumber == '') employerNumber = 'R1R1';
                var frame = document.createElement("frame");
                frame.src = getUrl(String.format('/Report/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}', 'YearEndAdjustmentReport', keyId, employeeNumber, 'EXCEL', year, employerNumber, province, revision)), null;
                frame.style.display = "none";
                document.body.appendChild(frame);
                return false;
            }
        }

        function onClientButtonClicking(sender, args) {
            if (keyId != null) {
                var comandName = args.get_item().get_commandName();
                if (comandName == "Delete") {
                    var message = "<asp:Literal runat="server" Text="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />";
                    args.set_cancel(!confirm(message));
                }
            }
            else
                args.set_cancel(true);
        }

        function onClientClose(sender, eventArgs) {
            var arg = sender.argument;
            if (arg != null && arg.closeWithChanges)
                __doPostBack('<%= SearchPanel.ClientID %>', 'refresh');
        }
    </script>
</telerik:RadScriptBlock>
