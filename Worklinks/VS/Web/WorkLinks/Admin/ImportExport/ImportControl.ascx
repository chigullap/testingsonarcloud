﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImportControl.ascx.cs" Inherits="WorkLinks.Admin.ImportExport.ImportControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style>
    TextArea:not(#ProcessReportMessageTextBox) {
        width: 653px !important;
    }
</style>

<table>
    <tr>
        <td>
            <wasp:ComboBoxControl ID="ImportExportId" FieldWidth="350px" Width="600px" runat="server" Type="ImportExportId" ResourceName="ImportExportId" Mandatory="true" ReadOnly="false"></wasp:ComboBoxControl>
        </td>
    </tr>
    <tr>
        <td>
            <wasp:WLPUpload ID="UploadControl" MaxFileSize="0" dir="rtl" ResourceName="UploadControl" runat="server" ControlObjectsVisibility="None" style="width:100%;float:left;text-align:left;margin:0;padding:0;"></wasp:WLPUpload>
            <asp:CustomValidator ID="CustomValidatorForUpload" runat="server" ForeColor="Red" Display="Dynamic" ClientValidationFunction="validateRadUpload" ErrorMessage='<%$ Resources:ErrorMessages, SelectAFile %>'></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            <wasp:WLPButton ID="Submit" runat="server" ResourceName="Submit" OnClick="Submit_Click" OnClientLoad="enableControl" OnClientClicked="disableControl"></wasp:WLPButton>
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="Output" TextMode="MultiLine" runat="server" Height="300px"></asp:TextBox>
        </td>
    </tr>
</table>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <style>
        /*override for the padding on rad control.  this will allow the radupload control to left align.  Put the css inline for the upload control. */
        .RadUpload_rtl .ruFileWrap{
            padding-left:0;
        }
    </style>
    <script type="text/javascript">
        window.onload = function(){
          ///add functions to enable elements here.  actually there is only one control that needs this.
        }

        function validateRadUpload(source, e) {
            e.IsValid = false;

            var upload = $find("<%= UploadControl.ClientID %>");
            var inputs = upload.getFileInputs();

            for (var i = 0; i < inputs.length; i++) {
                //check for empty string or invalid extension
                if (inputs[i].value != "") {
                    e.IsValid = true;
                    break;
                }
            }
        }

        function enableControl(source, e) {
            source.set_enabled(true);
        }

        function disableControl(source, e) {
            if (Page_IsValid)
                source.set_enabled(false);
        }
    </script>
</telerik:RadScriptBlock>