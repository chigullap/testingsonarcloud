﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ImportPage.aspx.cs" Inherits="WorkLinks.Admin.ImportExport.ImportPage" %>
<%@ Register Src="~/Admin/ImportExport/ImportControl.ascx" TagPrefix="uc1" TagName="ImportControl" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
       
    <% if(1==2 && Convert.ToBoolean(ConfigurationManager.AppSettings.Get("ANGULAR"))) { %>
        <iframe class="w-100" style="height:600px; border:none;" src="<%=ConfigurationManager.AppSettings.Get("ANGULARURL")%>/admin/import?language=<%=LanguageCode.ToLower()%>"></iframe>
        
    <% } %>
    <% else {%>
            <uc1:ImportControl runat="server" id="ImportControl" />
    <% } %>
    </ContentTemplate>
</asp:Content>