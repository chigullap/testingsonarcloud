﻿using System;
using System.Security.Claims;

namespace WorkLinks.Admin.ImportExport
{
    public partial class ImportControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected String ImportType { get { return Convert.ToString(Page.RouteData.Values["importType"]); } }
        #endregion

        #region main
        protected void Page_Load(Object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (ImportType.ToLower() == "remittance")
                {
                    AssemblyCache.CodeHelper.PopulateComboBoxWithRemittanceImportExport(ImportExportId);

                    ImportExportId.IncludeEmptyItem = false;
                    ImportExportId.ReadOnly = true;
                }
                else
                    AssemblyCache.CodeHelper.PopulateComboBoxWithImportExport(ImportExportId);

                ImportExportId.DataBind();
            }
        }
        protected void Submit_Click(Object sender, EventArgs e)
        {
            Telerik.Web.UI.UploadedFile file = UploadControl.UploadedFiles[0];
            byte[] fileContent = new byte[file.ContentLength];
            file.InputStream.Read(fileContent, 0, (int)file.ContentLength);

            //saxon has an issue when a file has two spaces in a row, so now we are going to replace all spaces with underscores to avoid issues
            String importFileName = file.GetName().Replace(' ', '_');

            String output = Common.ServiceWrapper.XmlClient.ImportFile(Convert.ToInt64(ImportExportId.SelectedValue), fileContent, importFileName, Common.ApplicationParameter.ImportExportProcessingDirectory, Common.ApplicationParameter.AutoGenerateEmployeeNumber, Common.ApplicationParameter.AutoGenerateEmployeeNumberFormat, Convert.ToBoolean(Common.ApplicationParameter.ImportDirtySave), Common.ApplicationParameter.AutoAddSecondaryPositions);
            String outputSummary = "----------"
                                 + "\nImport type: " + ImportExportId.SelectedText
                                 + "\nFile name: " + file.GetName()
                                 + "\nProcessing date: " + DateTime.Now
                                  + "\nUsername: " + System.Web.HttpContext.Current.User.Identity.Name.ToString();
            //+ "\nUsername: " + ((ClaimsIdentity)System.Web.HttpContext.Current.User.Identity).FindFirst("uname").Value;//yuc
            Output.Text = output + outputSummary;
        }
        #endregion
    }
}