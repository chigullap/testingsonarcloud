﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.HealthTax
{
    public partial class HealthTaxControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public bool IsViewMode { get { return HealthTaxGrid.IsViewMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.HealthTax.AddFlag; } }
        public bool ViewFlag { get { return Common.Security.RoleForm.HealthTax.ViewFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.HealthTax.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.HealthTax.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.HealthTax.ViewFlag);

            WireEvents();

            if (!IsPostBack)
                LoadHealthTaxInformation();

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.StartsWith("refresh"))
            {
                LoadHealthTaxInformation();
                HealthTaxGrid.Rebind();
            }
        }
        protected void WireEvents()
        {
            HealthTaxGrid.NeedDataSource += new GridNeedDataSourceEventHandler(HealthTaxGrid_NeedDataSource);
        }
        protected void LoadHealthTaxInformation()
        {
            Data = HealthTaxCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetHealthTax(null));
        }
        #endregion

        #region event handlers
        void HealthTaxGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            HealthTaxGrid.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void ProvinceStateCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateProvince((ICodeControl)sender);
        }
        #endregion

        #region handle updates
        protected void HealthTaxToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("delete") && HealthTaxGrid.SelectedValue != null)
                DeleteHealthTax((BusinessLayer.BusinessObjects.HealthTax)Data[HealthTaxGrid.SelectedValue.ToString()]);
        }
        protected void DeleteHealthTax(BusinessLayer.BusinessObjects.HealthTax item)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteHealthTax(item);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    HealthTaxGrid.SelectedIndexes.Clear();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            LoadHealthTaxInformation();
            HealthTaxGrid.Rebind();
        }
        #endregion

        #region security handlers
        protected void AddButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && AddFlag; }
        protected void DetailsButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && ViewFlag; }
        protected void DeleteButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && DeleteFlag; }
        #endregion
    }
}