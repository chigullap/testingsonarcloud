﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.HealthTax
{
    public partial class HealthTaxDetailControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        String _healthTaxId = null;
        private const String _healthTaxIdKey = "HealthTaxIdKey";
        #endregion

        #region properties
        public String HealthTaxId
        {
            get
            {
                if (_healthTaxId == null)
                {
                    Object obj = ViewState[_healthTaxIdKey];
                    if (obj != null)
                        _healthTaxId = (String)obj;
                }

                return _healthTaxId;
            }
            set
            {
                _healthTaxId = value;
                ViewState[_healthTaxIdKey] = _healthTaxId;
            }
        }
        public bool IsViewMode { get { return HealthTaxDetailGrid.IsViewMode; } }
        public bool IsEditMode { get { return HealthTaxDetailGrid.IsEditMode; } }
        public bool IsInsertMode { get { return HealthTaxDetailGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.HealthTax.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.HealthTax.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.HealthTax.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.HealthTax.ViewFlag);

            WireEvents();
        }
        private void WireEvents()
        {
            HealthTaxDetailGrid.NeedDataSource += new GridNeedDataSourceEventHandler(HealthTaxDetailGrid_NeedDataSource);
            HealthTaxDetailGrid.ClientSettings.EnablePostBackOnRowClick = false;
        }
        public void LoadHealthTaxDetailInformation(long healthTaxId)
        {
            Data = HealthTaxDetailCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetHealthTaxDetail(healthTaxId));
        }
        #endregion

        #region event handlers
        void HealthTaxDetailGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            HealthTaxDetailGrid.DataSource = Data;
        }
        protected void HealthTaxDetailGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                if (dataItem.DataItem != null)
                {
                    //you are only allowed to update the effective row (as long as the original row had a year = current / future)
                    ImageButton editButton = (ImageButton)dataItem["editButton"].Controls[0];
                    editButton.Visible = ((HealthTaxDetail)dataItem.DataItem).EffectiveYear >= DateTime.Today.Year;

                    ImageButton deleteButton = (ImageButton)dataItem["deleteButton"].Controls[0];
                    deleteButton.Visible = ((HealthTaxDetail)dataItem.DataItem).EffectiveYear >= DateTime.Today.Year;
                }
            }
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        #endregion

        #region handle updates
        protected void HealthTaxDetailGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                HealthTaxDetail item = (HealthTaxDetail)e.Item.DataItem;
                item.HealthTaxId = Convert.ToInt64(HealthTaxId);
                Common.ServiceWrapper.HumanResourcesClient.InsertHealthTaxDetail(item).CopyTo((HealthTaxDetail)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.UniqueIndexFault)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "UniqueIndex")), true);

                LoadHealthTaxDetailInformation(Convert.ToInt64(HealthTaxId));
                e.Canceled = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void HealthTaxDetailGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                HealthTaxDetail item = (HealthTaxDetail)e.Item.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.UpdateHealthTaxDetail(item).CopyTo((HealthTaxDetail)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                else if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.UniqueIndexFault)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "UniqueIndex")), true);

                LoadHealthTaxDetailInformation(Convert.ToInt64(HealthTaxId));
                e.Canceled = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void HealthTaxDetailGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteHealthTaxDetail(((HealthTaxDetail)e.Item.DataItem));
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}