﻿using System;
using System.Web.UI.WebControls;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.HealthTax
{
    public partial class HealthTaxEditControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public BusinessLayer.BusinessObjects.HealthTax HealthTax
        {
            get
            {
                if (Data.Count > 0)
                    return (BusinessLayer.BusinessObjects.HealthTax)Data[0];
                else
                    return null;
            }
        }
        private String Action { get { return Page.RouteData.Values["action"].ToString().ToLower(); } }
        private long HealthTaxId { get { return Convert.ToInt64(Page.RouteData.Values["healthTaxId"]); } }
        public bool IsEditMode { get { return HealthTaxDetailsView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool IsInsertMode { get { return HealthTaxDetailsView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsViewMode { get { return HealthTaxDetailsView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool AddFlag { get { return Common.Security.RoleForm.HealthTax.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.HealthTax.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.HealthTax.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.HealthTax.ViewFlag);

            WireEvents();

            if (Action == "view")
            {
                this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "EditHealthTax"));

                if (!IsPostBack)
                {
                    LoadHealthTaxInformation();
                    LoadHealthTaxEffective();
                    Initialize();
                }
            }
            else if (Action == "add")
            {
                if (!IsPostBack)
                {
                    this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "AddHealthTax"));

                    HealthTaxCollection newCollection = new HealthTaxCollection();
                    newCollection.AddNew();
                    Data = newCollection;

                    //change formview's mode to Insert mode
                    HealthTaxDetailsView.ChangeMode(FormViewMode.Insert);
                    HealthTaxDetailsView.DataBind();
                }
                else if (IsInsertMode)
                {
                    //do nothing
                }
                else
                {
                    //we are loading the edit page after an add
                    this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "EditHealthTax"));

                    LoadHealthTaxEffective();
                    Initialize();
                }
            }
        }
        protected void WireEvents()
        {
            HealthTaxDetailsView.NeedDataSource += new WLPFormView.NeedDataSourceEventHandler(HealthTaxDetailsView_NeedDataSource);
            HealthTaxDetailsView.Updating += HealthTaxDetailsView_Updating;
            HealthTaxDetailsView.Inserting += HealthTaxDetailsView_Inserting;
        }
        protected void Initialize()
        {
            //find the HealthTaxDetailGrid
            WLPGrid grid = (WLPGrid)HealthTaxDetailControl.FindControl("HealthTaxDetailGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        protected void LoadHealthTaxInformation()
        {
            //create business object and fill it with the data passed to the page, then bind the grid.
            HealthTaxCollection collection = new HealthTaxCollection();
            collection.Add(LoadDataIntoBusinessClass());

            Data = collection;
            HealthTaxDetailsView.DataBind();
        }
        protected void LoadHealthTaxEffective()
        {
            HealthTaxDetailControl.HealthTaxId = Convert.ToString(HealthTax.HealthTaxId);
            HealthTaxDetailControl.LoadHealthTaxDetailInformation(HealthTax.HealthTaxId);
        }
        BusinessLayer.BusinessObjects.HealthTax LoadDataIntoBusinessClass()
        {
            HealthTaxCollection temp = new HealthTaxCollection();
            temp.Load(Common.ServiceWrapper.HumanResourcesClient.GetHealthTax(HealthTaxId));
            return temp[0]; //will only ever have 1 item in this collection
        }
        #endregion

        #region event handlers
        void HealthTaxDetailsView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            HealthTaxDetailsView.DataSource = Data;
        }
        protected void VendorCombo_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateVendorComboControl((ICodeControl)sender);

            CheckBoxControl generateChequeForRemittanceFlag = (CheckBoxControl)HealthTaxDetailsView.FindControl("GenerateChequeForRemittanceFlag");
            ComboBoxControl vendorCombo = (ComboBoxControl)sender;

            if (generateChequeForRemittanceFlag != null && vendorCombo != null)
            {
                vendorCombo.Visible = generateChequeForRemittanceFlag.Checked;
                vendorCombo.Mandatory = generateChequeForRemittanceFlag.Checked;
            }
        }
        protected void ProvinceStateCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateProvince((ICodeControl)sender);
        }
        protected void GenerateChequeForRemittanceFlag_CheckedChanged(object sender, EventArgs e)
        {
            //if checked, the VendorId combo will be enabled
            ComboBoxControl vendor = (ComboBoxControl)HealthTaxDetailsView.FindControl("VendorId");
            if (vendor != null)
            {
                vendor.Visible = ((CheckBoxControl)sender).Checked;
                vendor.Mandatory = ((CheckBoxControl)sender).Checked;
                vendor.Value = (((CheckBoxControl)sender).Checked) ? vendor.Value : null;
            }
        }
        #endregion

        #region handle updates
        void HealthTaxDetailsView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            try
            {
                BusinessLayer.BusinessObjects.HealthTax item = (BusinessLayer.BusinessObjects.HealthTax)e.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.InsertHealthTax(item).CopyTo((BusinessLayer.BusinessObjects.HealthTax)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.UniqueIndexFault)
                {
                    e.Cancel = true;
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "UniqueIndex")), true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void HealthTaxDetailsView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                BusinessLayer.BusinessObjects.HealthTax item = (BusinessLayer.BusinessObjects.HealthTax)e.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.UpdateHealthTax(item).CopyTo((BusinessLayer.BusinessObjects.HealthTax)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, GetType(), "PopupScript", string.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
                else if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.UniqueIndexFault)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "UniqueIndex")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
