﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HealthTaxEditControl.ascx.cs" Inherits="WorkLinks.Admin.HealthTax.HealthTaxEditControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="HealthTaxDetailControl.ascx" TagName="HealthTaxDetailControl" TagPrefix="uc1" %>

<wasp:WLPFormView
    ID="HealthTaxDetailsView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key">

    <ItemTemplate>
        <wasp:WLPToolBar ID="HealthTaxDetailsToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
            <Items>
                <wasp:WLPToolBarButton Text="**Edit**" Visible='<%# UpdateFlag %>' ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="Edit" ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="CodeHealthTaxCd" runat="server" ResourceName="CodeHealthTaxCd" Value='<%# Eval("CodeHealthTaxCd") %>' Mandatory="true" ReadOnly="true"></wasp:TextBoxControl>
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="ImportExternalIdentifier" runat="server" ResourceName="ImportExternalIdentifier" Value='<%# Eval("ImportExternalIdentifier") %>' Mandatory="true" ReadOnly="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="ProvinceStateCode" runat="server" Type="ProvinceStateCode" OnDataBinding="ProvinceStateCode_NeedDataSource" ResourceName="ProvinceStateCode" Value='<%# Eval("ProvinceStateCode") %>' Mandatory="true" ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="ActiveFlag" runat="server" ResourceName="ActiveFlag" Value='<%# Eval("ActiveFlag") %>' ReadOnly="true"></wasp:CheckBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="HealthRemittanceAccountNumber" runat="server" ResourceName="HealthRemittanceAccountNumber" Value='<%# Eval("HealthRemittanceAccountNumber") %>' Mandatory="true" ReadOnly="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="EnglishDescription" runat="server" ResourceName="EnglishDescription" Value='<%# Eval("EnglishDescription") %>' Mandatory="true" ReadOnly="true"></wasp:TextBoxControl>
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="FrenchDescription" runat="server" ResourceName="FrenchDescription" Value='<%# Eval("FrenchDescription") %>' Mandatory="true" ReadOnly="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="GenerateChequeForRemittanceFlag" runat="server" ResourceName="GenerateChequeForRemittanceFlag" Value='<%# Eval("GenerateChequeForRemittanceFlag") %>' ReadOnly="true"></wasp:CheckBoxControl>
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="VendorId" runat="server" OnDataBinding="VendorCombo_NeedDataSource" ResourceName="VendorId" Value='<%# Eval("VendorId") %>' ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar ID="HealthTaxDetailsToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
            <Items>
                <wasp:WLPToolBarButton Text="**Insert**" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode  %>' ResourceName="Insert" CommandName="insert"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="**Update**" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" ResourceName="Update" Visible='<%# IsEditMode %>'></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="**Cancel**" ImageUrl="~/App_Themes/Default/Cancel.gif" Visible='<%# !IsInsertMode  %>' CommandName="cancel" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="**Cancel**" ImageUrl="~/App_Themes/Default/Cancel.gif" Visible='<%# IsInsertMode  %>' onClick="Close()" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                         <wasp:TextBoxControl ID="CodeHealthTaxCd" runat="server" ResourceName="CodeHealthTaxCd" Value='<%# Bind("CodeHealthTaxCd") %>' Mandatory="true"></wasp:TextBoxControl>
                    </td>
                    <td>
                         <wasp:TextBoxControl ID="ImportExternalIdentifier" runat="server" ResourceName="ImportExternalIdentifier" Value='<%# Bind("ImportExternalIdentifier") %>' ></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="ProvinceStateCode" runat="server" Type="ProvinceStateCode" OnDataBinding="ProvinceStateCode_NeedDataSource" ResourceName="ProvinceStateCode" Value='<%# Bind("ProvinceStateCode") %>' Mandatory="true"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="ActiveFlag" runat="server" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>'></wasp:CheckBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="HealthRemittanceAccountNumber" runat="server" ResourceName="HealthRemittanceAccountNumber" Value='<%# Bind("HealthRemittanceAccountNumber") %>' Mandatory="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="EnglishDescription" runat="server" ResourceName="EnglishDescription" Value='<%# Bind("EnglishDescription") %>' Mandatory="true"></wasp:TextBoxControl>
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="FrenchDescription" runat="server" ResourceName="FrenchDescription" Value='<%# Bind("FrenchDescription") %>' Mandatory="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="GenerateChequeForRemittanceFlag" runat="server" OnCheckedChanged="GenerateChequeForRemittanceFlag_CheckedChanged" AutoPostBack="true" ResourceName="GenerateChequeForRemittanceFlag" Value='<%# Bind("GenerateChequeForRemittanceFlag") %>'></wasp:CheckBoxControl>
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="VendorId" runat="server" OnDataBinding="VendorCombo_NeedDataSource" ResourceName="VendorId" Value='<%# Bind("VendorId") %>'></wasp:ComboBoxControl>
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>
</wasp:WLPFormView>

<uc1:HealthTaxDetailControl ID="HealthTaxDetailControl" runat="server"></uc1:HealthTaxDetailControl>

<telerik:RadScriptBlock runat="server">
    <script type="text/javascript">
        function processClick(commandName) {
            var arg = new Object;
            arg.closeWithChanges = true;
            getRadWindow().argument = arg;
        }

        function getRadWindow() {
            var popup = null;

            if (window.radWindow)
                popup = window.radWindow;
            else if (window.frameElement.radWindow)
                popup = window.frameElement.radWindow;

            return popup;
        }

        function Close(button, args) {
            var popup = getRadWindow();
            setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
        }

        function ToolBarClick(sender, args) {
            var button = args.get_item();
            var commandName = button.get_commandName();
            processClick(commandName);

            if (<%= IsInsertMode.ToString().ToLower() %>) {
                if (commandName == "cancel")
                    window.close();
            }
        }
    </script>
</telerik:RadScriptBlock>