﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HealthTaxControl.ascx.cs" Inherits="WorkLinks.Admin.HealthTax.HealthTaxControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPToolBar ID="HealthTaxToolBar" runat="server" Width="100%" OnButtonClick="HealthTaxToolBar_ButtonClick" OnClientLoad="HealthTaxToolBar_init">
        <Items>
            <wasp:WLPToolBarButton Text="**Add**" ImageUrl="~/App_Themes/Default/Add.gif" onclick="Add()" CommandName="add" ResourceName="Add" OnPreRender="AddButton_PreRender" />
            <wasp:WLPToolBarButton Text="**Details**" onclick="Open();" CommandName="details" ResourceName="Details" OnPreRender="DetailsButton_PreRender" />
            <wasp:WLPToolBarButton Text="**Delete**" ImageUrl="~\App_Themes\Default\Delete.gif" CommandName="delete" ResourceName="Delete" OnPreRender="DeleteButton_PreRender" />
        </Items>
    </wasp:WLPToolBar>

    <wasp:WLPGrid
        ID="HealthTaxGrid"
        runat="server"
        AllowPaging="true"
        PagerStyle-AlwaysVisible="true"
        PageSize="100"
        AllowSorting="true"
        GridLines="None"
        AutoAssignModifyProperties="true">

        <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="false">
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
            <ClientEvents OnRowDblClick="OnRowDblClick" OnRowClick="OnRowClick" OnGridCreated="onGridCreated" OnCommand="onCommand" />
        </ClientSettings>

        <MasterTableView
            ClientDataKeyNames="HealthTaxId"
            AutoGenerateColumns="false"
            DataKeyNames="HealthTaxId"
            CommandItemDisplay="Top"
            AllowSorting="false">

            <CommandItemTemplate></CommandItemTemplate>

            <Columns>
                <wasp:GridBoundControl DataField="CodeHealthTaxCd" LabelText="**CodeHealthTaxCd**" SortExpression="CodeHealthTaxCd" UniqueName="CodeHealthTaxCd" ResourceName="CodeHealthTaxCd" />
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="ProvinceStateCode" LabelText="**ProvinceStateCode**" Type="ProvinceStateCode" ResourceName="ProvinceStateCode" />
                <wasp:GridBoundControl DataField="EnglishDescription" LabelText="**EnglishDescription**" SortExpression="EnglishDescription" UniqueName="EnglishDescription" ResourceName="EnglishDescription" />
                <wasp:GridBoundControl DataField="FrenchDescription" LabelText="**FrenchDescription**" SortExpression="FrenchDescription" UniqueName="FrenchDescription" ResourceName="FrenchDescription" />
                <wasp:GridBoundControl DataField="HealthRemittanceAccountNumber" LabelText="**HealthRemittanceAccountNumber**" SortExpression="HealthRemittanceAccountNumber" UniqueName="HealthRemittanceAccountNumber" ResourceName="HealthRemittanceAccountNumber" />
                <wasp:GridCheckBoxControl DataField="ActiveFlag" LabelText="**ActiveFlag**" SortExpression="ActiveFlag" UniqueName="ActiveFlag" ResourceName="ActiveFlag" />
                <wasp:GridBoundControl DataField="ImportExternalIdentifier" LabelText="**ImportExternalIdentifier**" SortExpression="ImportExternalIdentifier" UniqueName="ImportExternalIdentifier" ResourceName="ImportExternalIdentifier" />
            </Columns>
        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true" />

    </wasp:WLPGrid>
</asp:Panel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="HealthTaxWindows"
    Width="1000"
    Height="600"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientClose="onClientClose"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var healthTaxId;
        var toolbar = null;

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function HealthTaxToolBar_init(sender) {
            initializeControls();
        }

        function initializeControls() {
            if ($find('<%= HealthTaxToolBar.ClientID %>') != null)
                toolbar = $find('<%= HealthTaxToolBar.ClientID %>');

            enableButtons(getSelectedRow() != null);
        }

        function OnRowDblClick(sender, eventArgs) {
            var toolbar = $find('<%= HealthTaxToolBar.ClientID %>');

            if (toolbar.findButtonByCommandName('details') != null)
                Open(sender);
        }

        function getSelectedRow() {
            var healthTaxGrid = $find('<%= HealthTaxGrid.ClientID %>');
            if (healthTaxGrid == null)
                return null;

            var masterTable = healthTaxGrid.get_masterTableView();
            if (masterTable == null)
                return null;

            var selectedRow = masterTable.get_selectedItems();
            if (selectedRow.length == 1)
                return selectedRow[0];
            else
                return null;
        }

        function onGridCreated(sender, eventArgs) {
            var selectedRow = getSelectedRow();
            if (selectedRow != null) {
                healthTaxId = selectedRow.getDataKeyValue('HealthTaxId');
                enableButtons(true);
            }
            else
                healthTaxId = null;
        }

        function onCommand(sender, args) {
            if (args.get_commandName() == 'Page') {
                healthTaxId = null;
                initializeControls();
            }
        }

        function OnRowClick(sender, eventArgs) {
            healthTaxId = eventArgs.getDataKeyValue('HealthTaxId');
            enableButtons(true);
        }

        function enableButtons(enable) {
            var detailsButton = toolbar.findButtonByCommandName('details');
            var deleteButton = toolbar.findButtonByCommandName('delete');

            if (detailsButton != null)
                detailsButton.set_enabled(enable);

            if (deleteButton != null)
                deleteButton.set_enabled(enable);
        }

        function Open() {
            if (healthTaxId != null) {
                openWindowWithManager('HealthTaxWindows', String.format('/Admin/HealthTax/{0}/{1}', 'view', healthTaxId), false);
                return false;
            }
        }

        function Add() {
            openWindowWithManager('HealthTaxWindows', String.format('/Admin/HealthTax/{0}/{1}', 'add', 'x'), false);
            return false;
        }

        function onClientClose(sender, eventArgs) {
            var arg = sender.argument;
            if (arg != null && arg.closeWithChanges)
                __doPostBack('<%= MainPanel.ClientID %>', 'refresh');
        }
    </script>
</telerik:RadScriptBlock>