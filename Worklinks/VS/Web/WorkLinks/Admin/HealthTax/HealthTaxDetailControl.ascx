﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HealthTaxDetailControl.ascx.cs" Inherits="WorkLinks.Admin.HealthTax.HealthTaxDetailControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPGrid
        ID="HealthTaxDetailGrid"
        runat="server"
        GridLines="None"
        AutoGenerateColumns="false"
        AutoAssignModifyProperties="true"
        OnInsertCommand="HealthTaxDetailGrid_InsertCommand"
        OnUpdateCommand="HealthTaxDetailGrid_UpdateCommand"
        OnDeleteCommand="HealthTaxDetailGrid_DeleteCommand"
        OnItemDataBound="HealthTaxDetailGrid_ItemDataBound"
        Style="margin-top: 17px">

        <ClientSettings EnablePostBackOnRowClick="false" AllowColumnsReorder="true" ReorderColumnsOnClient="true">
            <Selecting AllowRowSelect="false" />
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="EffectiveYear" SortOrder="Descending" />
            </SortExpressions>

            <CommandItemTemplate>
                <wasp:WLPToolBar ID="HealthTaxDetailToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
                    <Items>
                        <wasp:WLPToolBarButton Text="**Add**" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add"></wasp:WLPToolBarButton>
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif"></wasp:GridEditCommandControl>
                <wasp:GridBoundControl DataField="EffectiveYear" LabelText="**EffectiveYear**" SortExpression="EffectiveYear" UniqueName="EffectiveYear" ResourceName="EffectiveYear"></wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="ExemptionAmount" DataFormatString="{0:###,##0.00}" LabelText="**ExemptionAmount**" SortExpression="ExemptionAmount" UniqueName="ExemptionAmount" ResourceName="ExemptionAmount"></wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="RatePercentage" DataFormatString="{0:###0.00}" LabelText="**RatePercentage**" SortExpression="RatePercentage" UniqueName="RatePercentage" ResourceName="RatePercentage"></wasp:GridBoundControl>
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="**WcbChequeRemittanceFrequencyCode**" DataField="WcbChequeRemittanceFrequencyCode" Type="WcbChequeRemittanceFrequencyCode" UniqueName="WcbChequeRemittanceFrequencyCode" ResourceName="WcbChequeRemittanceFrequencyCode"></wasp:GridKeyValueControl>
                <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>"></wasp:GridButtonControl>
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:NumericControl ID="EffectiveYear" runat="server" MinValue='<%# DateTime.Now.Year %>' MaxLength="4" DecimalDigits="0" GroupSeparator="" ResourceName="EffectiveYear" Value='<%# Bind("EffectiveYear") %>' Mandatory="true"></wasp:NumericControl>
                            </td>
                            <td>
                                <wasp:NumericControl ID="RatePercentage" runat="server" DecimalDigits="2" ResourceName="RatePercentage" Value='<%# Bind("RatePercentage") %>' Mandatory="true"></wasp:NumericControl>
                                <asp:RangeValidator CultureInvariantValues="true" ID="PercentageValidator" Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="RatePercentage" ResourceName="PercentageValidator" Text='<%$ Resources:ErrorMessages, PercentRange %>' EnableClientScript="false" MinimumValue="0" MaximumValue="100" Type="Double"></asp:RangeValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:NumericControl ID="ExemptionAmount" runat="server" DecimalDigits="2" ResourceName="ExemptionAmount" Value='<%# Bind("ExemptionAmount") %>' Mandatory="true"></wasp:NumericControl>
                            </td>
                            <td>
                                <wasp:ComboBoxControl ID="WcbChequeRemittanceFrequencyCode" runat="server" Type="WcbChequeRemittanceFrequencyCode" OnDataBinding="Code_NeedDataSource" ResourceName="WcbChequeRemittanceFrequencyCode" Value='<%# Bind("WcbChequeRemittanceFrequencyCode") %>'></wasp:ComboBoxControl>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="**Update**" runat="server" CommandName="Update" Visible='<%# IsEditMode %>' ResourceName="Update"></wasp:WLPButton>
                                            <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="**Insert**" runat="server" CommandName="PerformInsert" Visible='<%# IsInsertMode %>' ResourceName="Insert"></wasp:WLPButton>
                                        </td>
                                        <td>
                                            <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="**Cancel**" runat="server" CommandName="Cancel" CausesValidation="false" ResourceName="Cancel"></wasp:WLPButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true"></HeaderContextMenu>

    </wasp:WLPGrid>
</asp:Panel>