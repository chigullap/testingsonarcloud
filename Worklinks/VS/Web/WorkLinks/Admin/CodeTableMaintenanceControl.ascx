﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CodeTableMaintenanceControl.ascx.cs" Inherits="WorkLinks.Admin.CodeTableMaintenanceControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:ComboBoxControl ID="CodeTables" runat="server" Text="Code Table:" OnSelectedIndexChanged="CodeTables_SelectedIndexChanged" AutoPostback="true" IncludeEmptyItem="false" FieldWidth="350px" Width="600px" ResourceName="CodeTablesLabel" />

    <wasp:WLPGrid
        ID="CodeGrid"
        runat="server"
        GridLines="None"
        AllowPaging="true"
        PagerStyle-AlwaysVisible="true"
        PageSize="100"
        AutoGenerateColumns="false"
        AutoAssignModifyProperties="true"
        OnUpdateCommand="CodeGrid_UpdateCommand"
        OnInsertCommand="CodeGrid_InsertCommand"
        OnDeleteCommand="CodeGrid_DeleteCommand"
        OnItemDataBound="CodeGrid_ItemDataBound"
        Style="margin-top: 5px">

        <ClientSettings EnablePostBackOnRowClick="false">
            <Selecting AllowRowSelect="false" />
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
            <CommandItemTemplate>
                <wasp:WLPToolBar ID="CodeToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
                    <Items>
                        <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsCodeGridViewMode && AddFlag %>' ResourceName="Add" />
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="EditButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
                <wasp:GridBoundControl DataField="CodeTableId" LabelText="Code" SortExpression="CodeTableId" UniqueName="CodeTableId" ResourceName="CodeTableId" />
                <wasp:GridBoundControl DataField="EnglishDesc" LabelText="Description" UniqueName="Description" ResourceName="Description">
                    <HeaderStyle Width="200px" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="FrenchDesc" LabelText="Description" UniqueName="Description" ResourceName="Description">
                    <HeaderStyle Width="200px" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="ImportExternalIdentifier" LabelText="Import External Identifier" UniqueName="ImportExternalIdentifier" ResourceName="ImportExternalIdentifier" />
                <wasp:GridCheckBoxControl DataField="ActiveFlag" LabelText="Active" SortExpression="ActiveFlag" UniqueName="ActiveFlag" ResourceName="ActiveFlag" />
                <wasp:GridNumericControl DataField="SortOrder" LabelText="Sort Order" SortExpression="SortOrder" UniqueName="SortOrder" DataType="System.Int64" ResourceName="SortOrder" />
                <wasp:GridButtonControl UniqueName="DeleteButton" ButtonType="ImageButton" CommandName="Delete" ImageUrl="~/App_Themes/Default/Delete.gif" />
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="CodeTableId" Text="Code" runat="server" Mandatory="true" ResourceName="CodeTableId" Value='<%# Bind("CodeTableId") %>' ReadOnly='<%# IsCodeGridUpdate %>' TabIndex="010" />
                            </td>
                            <td>
                                <wasp:CheckBoxControl ID="ActiveFlag" runat="server" Mandatory="true" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>' LabelText="Active" ReadOnly="false" TabIndex="020" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:NumericControl ID="SortOrder" runat="server" DecimalDigits="0" ResourceName="SortOrder" Value='<%# Bind("SortOrder") %>' ReadOnly="false" TabIndex="030" />
                            </td>
                            <td>
                                <wasp:ComboBoxControl ID="ParentCode" runat="server" Text="Parent value:" IncludeEmptyItem="True" ResourceName="ParentCode" Value='<%# Bind("ParentCodeTableId") %>' OnDataBinding="ParentCode_DataBinding" OnInit="ParentCode_Init" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="EnglishDesc" Text="EN Description:" runat="server" Mandatory="true" ResourceName="EnglishDesc" Value='<%# Bind("EnglishDesc") %>' ReadOnly="false" TabIndex="040" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="FrenchDesc" Text="FR Description:" runat="server" Mandatory="true" ResourceName="FrenchDesc" Value='<%# Bind("FrenchDesc") %>' ReadOnly="false" TabIndex="050" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="ImportExternalIdentifier" FieldStyle="width: 173px !important;" TextMode="multiline" Width="100%" Rows="3" Text="Import External Identifier:" runat="server" ResourceName="ImportExternalIdentifier" Value='<%# Bind("ImportExternalIdentifier") %>' ReadOnly="false" TabIndex="060" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsCodeGridUpdate %>' ResourceName="Update" />
                                            <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsCodeGridInsert %>' ResourceName="Insert" />
                                        </td>
                                        <td>
                                            <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>

    </wasp:WLPGrid>
</asp:Panel>