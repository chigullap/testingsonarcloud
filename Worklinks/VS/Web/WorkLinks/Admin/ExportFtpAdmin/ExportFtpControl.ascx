﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExportFtpControl.ascx.cs" Inherits="WorkLinks.Admin.ExportFtpAdmin.ExportFtpControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPGrid
        ID="ExportFtpGrid"
        runat="server"
        GridLines="None"
        AutoGenerateColumns="false"
        AllowPaging="true"
        PagerStyle-AlwaysVisible="true"
        PageSize="100"
        OnDeleteCommand="ExportFtpGrid_DeleteCommand"
        OnInsertCommand="ExportFtpGrid_InsertCommand"
        OnUpdateCommand="ExportFtpGrid_UpdateCommand"
        AutoAssignModifyProperties="true">

        <ClientSettings EnablePostBackOnRowClick="false" AllowColumnsReorder="true" ReorderColumnsOnClient="true">
            <Selecting AllowRowSelect="false" />
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="ExportFtpTypeCode" SortOrder="Ascending" />
            </SortExpressions>

            <CommandItemTemplate>
                <wasp:WLPToolBar ID="ExportFtpToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
                    <Items>
                        <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag%>' ResourceName="Add" />
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="FTP Type" DataField="ExportFtpTypeCode" Type="ExportFtpTypeCode" ResourceName="ExportFtpTypeCode" />
                <wasp:GridBoundControl DataField="FtpName" LabelText="FtpName" SortExpression="FtpName" UniqueName="FtpName" ResourceName="FtpName" />
                <wasp:GridBoundControl DataField="UriName" LabelText="UriName" SortExpression="UriName" UniqueName="UriName" ResourceName="UriName" />
                <wasp:GridBoundControl DataField="Description" LabelText="Description" SortExpression="Description" UniqueName="Description" ResourceName="Description" />
                <wasp:GridBoundControl DataField="Login" LabelText="Login" SortExpression="Login" UniqueName="Login" ResourceName="Login" />
                <wasp:GridBoundControl DataField="Password" LabelText="Password" SortExpression="Password" UniqueName="Password" ResourceName="Password" />
                <wasp:GridBoundControl DataField="Port" LabelText="Port" SortExpression="Port" UniqueName="Port" ResourceName="Port" />
                <wasp:GridBoundControl DataField="Directory" LabelText="*Directory" SortExpression="Directory" UniqueName="Directory" ResourceName="Directory" />
                <wasp:GridCheckBoxControl DataField="UnixPathFlag" LabelText="*UnixPathFlag" SortExpression="UnixPathFlag" UniqueName="UnixPathFlag" ResourceName="UnixPathFlag" />
                <wasp:GridCheckBoxControl DataField="EnableSslFlag" LabelText="EnableSsl" SortExpression="EnableSslFlag" UniqueName="EnableSslFlag" ResourceName="EnableSslFlag" />
                <wasp:GridCheckBoxControl DataField="BinaryModeFlag" LabelText="BinaryMode" SortExpression="BinaryModeFlag" UniqueName="BinaryModeFlag" ResourceName="BinaryModeFlag" />
                <wasp:GridCheckBoxControl DataField="PassiveModeFlag" LabelText="PassiveMode" SortExpression="PassiveModeFlag" UniqueName="PassiveModeFlag" ResourceName="PassiveModeFlag" />
                <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning%>" />
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="ExportFtpTypeCode" runat="server" LabelText="FTP Type:" Type="ExportFtpTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="ExportFtpTypeCode" Value='<%# Bind("ExportFtpTypeCode") %>' Mandatory="true" ReadOnly="false" TabIndex="010" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="Login" Text="Login:" runat="server" ResourceName="Login" Value='<%# Bind("Login") %>' ReadOnly="false" TabIndex="040" />
                            </td>
                            <td>
                                <wasp:CheckBoxControl ID="EnableSslFlag" LabelText="EnableSslFlag" runat="server" ResourceName="EnableSslFlag" Value='<%# Bind("EnableSslFlag") %>' ReadOnly="false" TabIndex="080" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="FtpName" Text="FTP Name:" runat="server" ResourceName="FtpName" Value='<%# Bind("FtpName") %>' ReadOnly="false" Mandatory="true" TabIndex="020" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="Password" Text="Password:" runat="server" ResourceName="Password" Value='<%# Bind("Password") %>' ReadOnly="false" TabIndex="050" />
                            </td>
                            <td>
                                <wasp:CheckBoxControl ID="BinaryModeFlag" LabelText="BinaryModeFlag" runat="server" ResourceName="BinaryModeFlag" Value='<%# Bind("BinaryModeFlag") %>' ReadOnly="false" TabIndex="090" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="UriName" Text="URI Name:" runat="server" ResourceName="UriName" Value='<%# Bind("UriName") %>' ReadOnly="false" Mandatory="true" TabIndex="020" />
                            </td>
                            <td>
                                <wasp:NumericControl ID="Port" runat="server" DecimalDigits="0" MinValue="1" MaxValue="999999" LabelText="Port:" ResourceName="Port" Value='<%# Bind("Port") %>' ReadOnly="false" TabIndex="060" />
                            </td>
                            <td>
                                <wasp:CheckBoxControl ID="PassiveModeFlag" LabelText="PassiveModeFlag" runat="server" ResourceName="PassiveModeFlag" Value='<%# Bind("PassiveModeFlag") %>' ReadOnly="false" TabIndex="100" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="Description" Text="Description:" runat="server" ResourceName="Description" Value='<%# Bind("Description") %>' ReadOnly='false' Mandatory="true" TabIndex="030" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="Directory" Text="*Directory:" runat="server" ResourceName="Directory" Value='<%# Bind("Directory") %>' ReadOnly="false" TabIndex="070" />
                            </td>
                            <td>
                                <wasp:CheckBoxControl ID="UnixPathFlag" LabelText="*UnixPathFlag" runat="server" ResourceName="UnixPathFlag" Value='<%# Bind("UnixPathFlag") %>' ReadOnly="false" TabIndex="110" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                            <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsInsert %>' ResourceName="Insert" />
                                        </td>
                                        <td>
                                            <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="Cancel" CausesValidation="false" ResourceName="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true" />

    </wasp:WLPGrid>
</asp:Panel>