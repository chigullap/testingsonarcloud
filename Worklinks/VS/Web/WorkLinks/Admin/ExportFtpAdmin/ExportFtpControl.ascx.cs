﻿using System;
using System.Web.UI;
using Telerik.Web.UI;
using WLP.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Common;

namespace WorkLinks.Admin.ExportFtpAdmin
{
    public partial class ExportFtpControl : WLPUserControl
    {
        #region properties
        public bool IsViewMode { get { return ExportFtpGrid.IsViewMode; } }
        public bool IsUpdate { get { return ExportFtpGrid.IsEditMode; } }
        public bool IsInsert { get { return ExportFtpGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.ExportFtp.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.ExportFtp.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.ExportFtp.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.ExportFtp.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                LoadExportFtpInformation();
                Initialize();
            }
        }

        protected void WireEvents()
        {
            ExportFtpGrid.NeedDataSource += new GridNeedDataSourceEventHandler(ExportFtpGrid_NeedDataSource);
            ExportFtpGrid.ItemDataBound += ReportExportGrid_ItemDataBound;
        }

        protected void Initialize()
        {
            //Find the PayrollProcessGroupGrid
            WLPGrid grid = (WLPGrid)this.FindControl("ExportFtpGrid");

            //hide the edit image in the rows.
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
        }

        void ReportExportGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                //hide the delete image if there is a dependency.
                GridDataItem dataItem = e.Item as GridDataItem;
                long? keyId = Convert.ToInt64(dataItem.GetDataKeyValue("Key").ToString());
                ExportFtp export = ServiceWrapper.HumanResourcesClient.GetExportFtp(keyId, null, null)[0];
                dataItem["deleteButton"].Controls[0].Visible = !export.HasChildrenFlag;
            }
        }

        protected void LoadExportFtpInformation()
        {
            Data = ExportFtpCollection.ConvertCollection(ServiceWrapper.HumanResourcesClient.GetExportFtp(null, null, null));
        }
        #endregion

        #region event handlers
        void ExportFtpGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            ExportFtpGrid.DataSource = Data;
        }

        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        #endregion

        #region handle updates
        protected void ExportFtpGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                ServiceWrapper.HumanResourcesClient.DeleteExportFtp((ExportFtp)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ExportFtpGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            ExportFtp exportFtp = (ExportFtp)e.Item.DataItem;

            //If the port is null, the NumericControl will set it to 0. To fix this, we have to manually overwrite it.
            exportFtp.Port = (exportFtp.Port == 0) ? 1 : exportFtp.Port;

            ServiceWrapper.HumanResourcesClient.InsertExportFtp(exportFtp).CopyTo((ExportFtp)Data[exportFtp.Key]);
        }

        protected void ExportFtpGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                ExportFtp exportFtp = (ExportFtp)e.Item.DataItem;
                ServiceWrapper.HumanResourcesClient.UpdateExportFtp(exportFtp).CopyTo((ExportFtp)Data[exportFtp.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}