﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.SalaryPlan
{
    public partial class GradeControl : WLP.Web.UI.WLPUserControl
    {
        public bool IsViewMode { get { return GradeGrid.IsViewMode; } }
        public bool IsUpdate { get { return GradeGrid.IsEditMode; } }
        public bool IsInsert { get { return GradeGrid.IsInsertMode; } }

        public long SalaryPlanId { get; set; }

        public string LinkedOrgUnitCodeSystem
        {
            get
            {
                return Common.ApplicationParameter.SalaryPlanGradeOrganizationUnitLevel;
            }
        }

        public bool LinkedOrgUnitValue
        {
            get
            {
                return LinkedOrgUnitCodeSystem != "0" ? true : false; //0 in code system if not linked to org unit
            }
        }

        public string OrgLevelName
        {
            get
            {
                if (LinkedOrgUnitValue)
                {
                    return Common.CodeHelper.GetOrganizationLevelDescription(Convert.ToInt64(Common.ApplicationParameter.SalaryPlanGradeOrganizationUnitLevel), LanguageCode);
                }
                else
                    return "NONE";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();

            LinkedOrgUnit.Text = string.Format(LinkedOrgUnit.Text, OrgLevelName);

            Page.ClientScript.GetPostBackEventReference(this, "");
            string args = Request["__EVENTARGUMENT"];

            if (args != null && args.Equals(String.Format("refresh")))
                RefreshGrid();

            if (!IsPostBack)
            {
                //grid disabled on load
                GradeToolBar.Visible = false;
                GradeGrid.Enabled = false;

                //load drop down
                LoadSalaryPlanCombo();
            }
        }

        protected void RefreshGrid()
        {
            SalaryPlanId = Convert.ToInt64(SalaryPlanCode.Value);
            LoadGrades(SalaryPlanId);
            GradeGrid.Rebind();
        }

        private void LoadSalaryPlanCombo()
        {
            Common.CodeHelper.PopulateSalaryPlan(SalaryPlanCode, LanguageCode);
            SalaryPlanCode.DataBind();
        }
        private void WireEvents()
        {
            GradeGrid.NeedDataSource += new GridNeedDataSourceEventHandler(GradeGrid_NeedDataSource);
            GradeGrid.PreRender += new EventHandler(GradeGrid_PreRender);
        }

        void GradeGrid_PreRender(object sender, EventArgs e)
        {
            GradeGrid.ClientSettings.EnablePostBackOnRowClick = false;
            GradeGrid.ClientSettings.Selecting.AllowRowSelect = true;
        }
        protected void GradeGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            GradeGrid.DataSource = Data;
        }
        private void LoadGrades(long salaryPlanId)
        {
            Data = Common.ServiceWrapper.HumanResourcesClient.GetSalaryPlanGradeAndEffectiveDetail(salaryPlanId);
        }

        protected void SalaryPlanCodeCriteria_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //enable grid
            GradeGrid.Enabled = true;
            GradeToolBar.Visible = true;

            //populate grid
            SalaryPlanId = Convert.ToInt64(SalaryPlanCode.Value);
            LoadGrades(SalaryPlanId);
            
            GradeGrid.Rebind();
        }
    }
}
 