﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.SalaryPlan
{
    public partial class SalaryPlanAddControl : WLP.Web.UI.WLPUserControl
    {
        #region properties

        private long SalaryPlanId { get { return Convert.ToInt64(Page.RouteData.Values["salaryPlanId"]); } }

        public string LinkedOrgUnitCodeSystem
        {
            get
            {
                return Common.ApplicationParameter.SalaryPlanGradeOrganizationUnitLevel;
            }
        }
        public bool IsInsertMode { get { return GradeDetailView.CurrentMode.Equals(FormViewMode.Insert); } }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();

            Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "SalaryPlanAddControl"));

            if (!IsPostBack)
            {
                OpenInsertMode();
                ToggleEffectiveDateCombo();
            }
        }

        private void ToggleEffectiveDateCombo()
        {
            ComboBoxControl combo = ((ComboBoxControl)GradeDetailView.FindControl("NameCombo"));
            DateControl effDate = (DateControl)GradeDetailView.FindControl("EffectiveDate");
            DateControl retroDate = (DateControl)GradeDetailView.FindControl("RetroDate");

            if (combo != null && effDate != null && retroDate != null)
            {
                //if nothing is selected, null out date, disable
                if (combo.SelectedIndex == 0)
                {
                    effDate.Value = null;
                    effDate.ReadOnly = true;
                    retroDate.Value = null;
                    retroDate.ReadOnly = true;
                }
                else
                {
                    effDate.ReadOnly = false;
                    retroDate.ReadOnly = false;

                    //check and set MIN date allowed based on grade chosen
                    SalaryPlanGradeMinEffectiveDateCollection coll = Common.ServiceWrapper.HumanResourcesClient.SelectMinEffectiveDate(Convert.ToInt64(combo.Value));
                    if (coll != null && coll.Count != 0)
                    {
                        //this is for plan grades that already exist.  
                        effDate.MinDate = Convert.ToDateTime(coll[0].EffectiveDate);
                        
                    }
                }
            }
        }

        protected void EffectiveDate_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            DateControl effDate = (DateControl)GradeDetailView.FindControl("EffectiveDate");
            DateControl retroDate = (DateControl)GradeDetailView.FindControl("RetroDate");
            //retroDate:  Any date can be entered here as long as it is equal to or before the effective date.
            if (effDate != null && retroDate != null)
                retroDate.MaxDate = (DateTime)effDate.Value;
        }

        protected void NameCombo_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ToggleEffectiveDateCombo();
        }

        private void OpenInsertMode()
        {
            SalaryPlanGradeAndEffectiveDetailCollection coll = new SalaryPlanGradeAndEffectiveDetailCollection();
            SalaryPlanGradeAndEffectiveDetail tmp = new SalaryPlanGradeAndEffectiveDetail();
            coll.Add(tmp);
            Data = coll;

            GradeDetailView.ChangeMode(FormViewMode.Insert);
        }

        protected void WireEvents()
        {
            GradeDetailView.Inserting += new WLPFormView.ItemInsertingEventHandler(GradeDetailView_Inserting);
        }

        #region handles updates
        void GradeDetailView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            SalaryPlanGradeAndEffectiveDetail info = (SalaryPlanGradeAndEffectiveDetail)e.DataItem;
            info.SalaryPlanId = SalaryPlanId;
            Common.ServiceWrapper.HumanResourcesClient.AddNewSalaryPlanGradeAndSteps(info);
        }

        protected void GradeDetailView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            GradeDetailView.DataSource = Data;
        }

        public void SalaryPlanGrade_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateComboBoxWithSalaryPlanGrades((ICodeControl)sender, LanguageCode, SalaryPlanId, LinkedOrgUnitCodeSystem);
        }
        #endregion
    }
}