﻿using System;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.SalaryPlan
{
    public partial class RulePayCodeAccumulationPageControl : WLP.Web.UI.WLPUserControl
    {
        protected CodeCollection PayCodes { get; private set; }
   
        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();

            Page.Title = (string)GetGlobalResourceObject("PageTitle", "RulePayCodeAccumulationPageControl");

            if (!IsPostBack)
            {
                SetAuthorized(Common.Security.RoleForm.SalaryPlanCodeMaintenance.ViewFlag);
                LoadRulePaycodeControls();
                Initialize();
            }
        }

        private void LoadRulePaycodeControls()
        {
            cboRuleMinimumHours.DataSource = Common.ServiceWrapper.HumanResourcesClient.GetSalaryPlanGradeRules()?.ToCodeCollection();
            cboRuleMinimumHours.DataBind();
        }

        private void WireEvents()
        {
            RulePayCodeGrid.NeedDataSource += RulePaycodeGrid_NeedDataSource;
            PayCodes = Common.ServiceWrapper.CodeClient.GetPaycodeByCode(null);
        }

        protected void Initialize()
        {
            //find the rules grid
            WLPGrid grid = (WLPGrid)FindControl("RulePayCodeGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }

        private void SetGridData(long? ruleId)
        {
            RulePayCodeGrid.DataSource = ruleId == null 
                ? null 
                : Common.ServiceWrapper.HumanResourcesClient.GetSalaryPlanGradeRuleCodes().Filter(ruleId.Value);
        }

        protected void RulePayCodeGrid_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.UpdateGradeRuleCode((SalaryPlanGradeRuleCode)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, GetType(), "PopupScript", $"alert('{GetGlobalResourceObject("ErrorMessages", "FKConstraint")}');", true);
                    e.Canceled = true;
                }
            }
        }

        protected void RulePayCodeGrid_InsertCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                // Unbox the new code and assign the select rule to it;
                SalaryPlanGradeRuleCode newRule = (SalaryPlanGradeRuleCode) e.Item.DataItem;
                newRule.SalaryPlanGradeRuleId = long.Parse(cboRuleMinimumHours.SelectedValue);

                // The saved rule returned has all the values we need, but when I add it to
                // the grid's datasource rather than refresh from the db it shows in the grid
                // as a blank row. So the rebind will refresh from the database
                Common.ServiceWrapper.HumanResourcesClient.InsertGradeRuleCode(newRule);
               
                SetGridData(newRule.SalaryPlanGradeRuleId); 
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, GetType(), "PopupScript", $"alert('{GetGlobalResourceObject("ErrorMessages", "FKConstraint")}');", true);
                    e.Canceled = true;
                }
            }
        }

        protected void RulePayCodeGrid_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteGradeRuleCode((SalaryPlanGradeRuleCode)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, GetType(), "PopupScript", $"alert('{GetGlobalResourceObject("ErrorMessages", "FKConstraint")}');", true);
                    e.Canceled = true;
                }
            }
        }

        protected void RulePaycodeGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (long.TryParse(cboRuleMinimumHours.SelectedValue, out long ruleId))
            {
                SetGridData(ruleId);
            }
            else
            {
                SetGridData(null);
            }
        }

        protected void cboRuleMinimumHours_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RulePayCodeGrid.Rebind();
        }

        protected void EditInputPayCode_NeedDataSource(object sender, EventArgs e)
        {
            ((ComboBoxControl) sender).DataSource = PayCodes.ActiveCodeCollection;
        }

        public bool AddFlag => Common.Security.RoleForm.RulePaycodeAccumulation.AddFlag;
        public bool UpdateFlag => Common.Security.RoleForm.RulePaycodeAccumulation.UpdateFlag;
        public bool DeleteFlag => Common.Security.RoleForm.RulePaycodeAccumulation.DeleteFlag;
        public bool IsViewMode => RulePayCodeGrid.IsViewMode;
        public bool IsUpdate => RulePayCodeGrid.IsEditMode;
        public bool IsInsert => RulePayCodeGrid.IsInsertMode;
    }
}