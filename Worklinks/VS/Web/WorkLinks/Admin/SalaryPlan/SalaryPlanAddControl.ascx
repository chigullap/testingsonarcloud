﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SalaryPlanAddControl.ascx.cs" Inherits="WorkLinks.Admin.SalaryPlan.SalaryPlanAddControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="SearchPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SearchPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>


<asp:Panel ID="SearchPanel" runat="server">
    <wasp:WLPFormView
        ID="GradeDetailView"
        runat="server"
        RenderOuterTable="False"
        DataKeyNames="Key"
        OnNeedDataSource="GradeDetailView_NeedDataSource"
        >
        
        <ItemTemplate>
            <fieldset>
                <table width="100%">
                      <tr>
                        <td>
                            <wasp:ComboBoxControl ID="NameCombo" runat="server" Type="SalaryPlanGrade" OnDataBinding="SalaryPlanGrade_NeedDataSource" ResourceName="Name" Value='<%# Bind("Name") %>' ReadOnly="True" />
                        </td>
                        <td>
                            <wasp:NumericControl ID="Step1" runat="server" LabelText="Step1" ResourceName="Step1" Value='<%# Bind("Step1") %>' ReadOnly="True" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="EffectiveDate" Text="**EffectiveDate**" runat="server" ResourceName="EffectiveDate" Value='<%# Bind("EffectiveDate") %>' ReadOnly="True" />
                        </td>
                        <td>
                            <wasp:NumericControl ID="Step2" runat="server" LabelText="Step2" ResourceName="Step2" Value='<%# Bind("Step2") %>' ReadOnly="True" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="RetroDate" Text="**RetroDate**" runat="server" ResourceName="RetroDate" Value='<%# Bind("RetroactiveDate") %>' ReadOnly="True" />
                        </td>
                        <td>
                            <wasp:NumericControl ID="Step3" runat="server" LabelText="Step3" ResourceName="Step3" Value='<%# Bind("Step3") %>' ReadOnly="True" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </ItemTemplate>

        <EditItemTemplate>
            <wasp:WLPToolBar ID="GradeDetailToolBar2" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
                <Items>
                    <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert" />
                    <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" Visible='<%# IsInsertMode %>' onClick="Close()" CausesValidation="False" ResourceName="Cancel"></wasp:WLPToolBarButton>
                </Items>
            </wasp:WLPToolBar>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="NameCombo" runat="server" Type="SalaryPlanGrade" AutoPostBack="true" OnSelectedIndexChanged="NameCombo_SelectedIndexChanged" OnDataBinding="SalaryPlanGrade_NeedDataSource" ResourceName="Name" Value='<%# Bind("Name") %>' Mandatory="true" TabIndex="010" />
                        </td>
                        <td>
                            <wasp:NumericControl ID="Step1" runat="server" LabelText="Step1" ResourceName="Step1" Value='<%# Bind("Step1") %>' ReadOnly="False" TabIndex="040" Mandatory="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="EffectiveDate" Text="**EffectiveDate**" runat="server" OnSelectedDateChanged="EffectiveDate_SelectedDateChanged" AutoPostback="true" ResourceName="EffectiveDate" Value='<%# Bind("EffectiveDate") %>' Mandatory="true" TabIndex="020" />
                        </td>
                        <td>
                            <wasp:NumericControl ID="Step2" runat="server" LabelText="Step2" ResourceName="Step2" Value='<%# Bind("Step2") %>' ReadOnly="False" TabIndex="050" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="RetroDate" Text="**RetroDate**" runat="server" ResourceName="RetroDate" Value='<%# Bind("RetroactiveDate") %>' TabIndex="030" />
                        </td>
                        <td>
                            <wasp:NumericControl ID="Step3" runat="server" LabelText="Step3" ResourceName="Step3" Value='<%# Bind("Step3") %>' ReadOnly="False" TabIndex="060" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </EditItemTemplate>
    </wasp:WLPFormView>
</asp:Panel>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function Close(button, args) {
            var popup = getRadWindow();
            setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
        }

         function getRadWindow() {
            var popup = null;

            if (window.radWindow)
                popup = window.radWindow;
            else if (window.frameElement.radWindow)
                popup = window.frameElement.radWindow;

            return popup;
        }

         function processClick(commandName) {
            var arg = new Object;
            arg.closeWithChanges = true;
            getRadWindow().argument = arg;
         }

        function ToolBarClick(sender, args) {
            var button = args.get_item();
            var commandName = button.get_commandName();

            processClick(commandName);

            if (<%= IsInsertMode.ToString().ToLower() %>) {
                if (commandName == "Cancel") {
                    window.close();
                }
            }
        }
    </script>
</telerik:RadScriptBlock>
