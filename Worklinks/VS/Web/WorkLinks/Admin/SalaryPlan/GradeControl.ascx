﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GradeControl.ascx.cs" Inherits="WorkLinks.Admin.SalaryPlan.GradeControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<div class="simpleTable">
    <table width="100%">
        <tr>
            <td>
                <wasp:ComboBoxControl ID="SalaryPlanCode" runat="server" ResourceName="SalaryPlanCode" Type="SalaryPlanCode" AutoPostback="true" ReadOnly="false" OnSelectedIndexChanged="SalaryPlanCodeCriteria_SelectedIndexChanged" />
            </td>
            <td>
                <wasp:WLPLabel ID="LinkedOrgUnit" runat="server" Text="**OrgMsg**" ResourceName="LinkedOrgUnit"></wasp:WLPLabel>
            </td>
        </tr>
    </table>
</div>
<br />

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPToolBar ID="GradeToolBar" runat="server" Width="100%">
        <Items>
            <wasp:WLPToolBarButton Text="**Add**" ImageUrl="~/App_Themes/Default/Add.gif" onclick="Add()" CommandName="add" ResourceName="Add" />
            <wasp:WLPToolBarButton Text="**AddRule**" ImageUrl="~/App_Themes/Default/Add.gif" onclick="AddRule()" CommandName="addRule" ResourceName="AddRule" />
            <wasp:WLPToolBarButton Text="**PaycodeAccum**" ImageUrl="~/App_Themes/Default/PayCodes.gif" onclick="loadRulePaycodeAccum()" CommandName="loadRulePaycodeAccum" ResourceName="RulePaycodeAcum" />
        </Items>
    </wasp:WLPToolBar>

    <wasp:WLPGrid
        ID="GradeGrid"
        runat="server"
        AllowPaging="true"
        AutoGenerateColumns="false"
        AutoInsertOnAdd="false"
        PagerStyle-AlwaysVisible="true"
        PageSize="100"
        AllowSorting="true"
        GridLines="None"
        AutoAssignModifyProperties="true"
        AllowFilteringByColumn="true"
        FilterType="CheckList">

        <GroupingSettings CaseSensitive="false" />

        <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="false">
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView
            CommandItemDisplay="Top"
            AutoGenerateColumns="false"
            DataKeyNames="Key"
            AllowSorting="true"
            AllowFilteringByColumn="true">

            <SortExpressions>
                <telerik:GridSortExpression FieldName="Name, EffectiveDate" SortOrder="Ascending" />
            </SortExpressions>

            <CommandItemTemplate>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridBoundControl DataField="Name" LabelText="**Name**" SortExpression="Name" UniqueName="Name" ResourceName="Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" />
                <wasp:GridBoundControl DataField="EnglishDescription" LabelText="**EnglishDescription**" SortExpression="EnglishDescription" UniqueName="EnglishDescription" ResourceName="EnglishDescription" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" />
                <wasp:GridBoundControl DataField="FrenchDescription" LabelText="**FrenchDescription**" SortExpression="FrenchDescription" UniqueName="FrenchDescription" ResourceName="FrenchDescription" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" />
                <wasp:GridDateTimeControl DataField="EffectiveDate" DataType="System.DateTime" DataFormatString="{0:yyyy-MM-dd}" EnableTimeIndependentFiltering="true" FilterDateFormat="yyyy-MM-dd" LabelText="EffectiveDate" UniqueName="EffectiveDate" ResourceName="EffectiveDate" AutoPostBackOnFilter="true" ShowFilterIcon="false" />
                <wasp:GridDateTimeControl DataField="RetroactiveDate" DataType="System.DateTime" DataFormatString="{0:yyyy-MM-dd}" EnableTimeIndependentFiltering="true" FilterDateFormat="yyyy-MM-dd" LabelText="RetroactiveDate" UniqueName="RetroactiveDate" ResourceName="RetroactiveDate" AutoPostBackOnFilter="true" ShowFilterIcon="false" />
                <wasp:GridBoundControl DataField="Step1" LabelText="**Step1**" DataFormatString="{0:$###,##0.00}" SortExpression="Step1" UniqueName="Step1" ResourceName="Step1" AutoPostBackOnFilter="true" ShowFilterIcon="false" />
                <wasp:GridBoundControl DataField="Step2" LabelText="**Step2**" DataFormatString="{0:$###,##0.00}" SortExpression="Step2" UniqueName="Step2" ResourceName="Step2" AutoPostBackOnFilter="true" ShowFilterIcon="false" />
                <wasp:GridBoundControl DataField="Step3" LabelText="**Step3**" DataFormatString="{0:$###,##0.00}" SortExpression="Step3" UniqueName="Step3" ResourceName="Step3" AutoPostBackOnFilter="true" ShowFilterIcon="false" />
            </Columns>
        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true" />

    </wasp:WLPGrid>
</asp:Panel>


<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="SalaryPlanWindows"
    Width="1000"
    Height="600"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientClose="onClientClose"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>


<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function Add() {
            openWindowWithManager('SalaryPlanWindows', String.format('/Admin/SalaryPlan/Add/{0}/', '<%= SalaryPlanId %>'), false);
            return false;
        }

        function AddRule() {
            openWindowWithManager('SalaryPlanWindows', '/Admin/SalaryPlan/AddRule/', false);
            return false;
        }

        function loadRulePaycodeAccum() {
            openWindowWithManager('SalaryPlanWindows', '/Admin/SalaryPlan/RulePayCodeAccum/', false);
            return false;
        }

        function onClientClose(sender, eventArgs) {
            var arg = sender.argument;
            if (arg != null && arg.closeWithChanges)
                __doPostBack('<%= MainPanel.ClientID %>', 'refresh');
        }
    </script>
</telerik:RadScriptBlock>
