﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.SalaryPlanCodeMaintenance
{
    public partial class SalaryPlanCodeMaintenanceControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private SalaryPlanCodeTableCollection _gradeCollection = new SalaryPlanCodeTableCollection(); //used to populate child grid
        private SalaryPlanCodeTableCollection _stepCollection = new SalaryPlanCodeTableCollection(); //used to populate child grid
        #endregion

        #region properties
        private String SelectedSalaryPlan { get { return SalaryPlanGrid.SelectedValue == null ? null : Convert.ToString(SalaryPlanGrid.SelectedValue); } }
        private String SelectedGrade { get { return GradeGrid.SelectedValue == null ? null : Convert.ToString(GradeGrid.SelectedValue); } }
        public bool IsViewMode { get { return IsSalaryPlanGridViewMode && IsGradeGridViewMode && IsStepGridViewMode; } }
        public bool IsSalaryPlanGridViewMode { get { return SalaryPlanGrid.IsViewMode; } }
        public bool IsSalaryPlanGridUpdate { get { return SalaryPlanGrid.IsEditMode; } }
        public bool IsSalaryPlanGridInsert { get { return SalaryPlanGrid.IsInsertMode; } }
        public bool IsGradeGridViewMode { get { return GradeGrid.IsViewMode; } }
        public bool IsGradeGridUpdate { get { return GradeGrid.IsEditMode; } }
        public bool IsGradeGridInsert { get { return GradeGrid.IsInsertMode; } }
        public bool IsStepGridViewMode { get { return StepGrid.IsViewMode; } }
        public bool IsStepGridUpdate { get { return StepGrid.IsEditMode; } }
        public bool IsStepGridInsert { get { return StepGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.SalaryPlanCodeMaintenance.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.SalaryPlanCodeMaintenance.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.SalaryPlanCodeMaintenance.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.SalaryPlanCodeMaintenance.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                LoadSalaryPlanGrid();
                Initialize();
            }
        }
        protected void WireEvents()
        {
            SalaryPlanGrid.NeedDataSource += new GridNeedDataSourceEventHandler(SalaryPlanGrid_NeedDataSource);
            SalaryPlanGrid.ItemDataBound += SalaryPlanGrid_ItemDataBound;
            SalaryPlanGrid.SelectedIndexChanged += new EventHandler(SalaryPlanGrid_SelectedIndexChanged);
            SalaryPlanGrid.PreRender += new EventHandler(SalaryPlanGrid_PreRender);

            GradeGrid.NeedDataSource += new GridNeedDataSourceEventHandler(GradeGrid_NeedDataSource);
            GradeGrid.ItemDataBound += SalaryPlanGrid_ItemDataBound;
            GradeGrid.SelectedIndexChanged += new EventHandler(GradeGrid_SelectedIndexChanged);
            GradeGrid.PreRender += new EventHandler(GradeGrid_PreRender);

            StepGrid.NeedDataSource += new GridNeedDataSourceEventHandler(StepGrid_NeedDataSource);
            StepGrid.ItemDataBound += SalaryPlanGrid_ItemDataBound;
            StepGrid.PreRender += new EventHandler(StepGrid_PreRender);
        }
        protected void LoadSalaryPlanGrid()
        {
            Data = SalaryPlanCodeTableCollection.ConvertCollection(Common.ServiceWrapper.CodeClient.GetSalaryPlanCodeTableRows("code_salary_plan", null));
        }
        protected void Initialize()
        {
            //find the grids
            WLPGrid salaryPlanGrid = (WLPGrid)this.FindControl("SalaryPlanGrid");
            WLPGrid gradeGrid = (WLPGrid)this.FindControl("GradeGrid");
            WLPGrid stepGrid = (WLPGrid)this.FindControl("StepGrid");

            //hide the edit images in the rows
            salaryPlanGrid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            gradeGrid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            stepGrid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
        }
        #endregion

        #region event handlers
        private void SetGridEnabledMode(WLPGrid grid, bool isEnabled, bool isSelectable)
        {
            grid.Enabled = isEnabled;
            grid.ClientSettings.EnablePostBackOnRowClick = isSelectable;
            grid.ClientSettings.Resizing.AllowColumnResize = isEnabled;
            grid.ClientSettings.Selecting.AllowRowSelect = isSelectable;
            grid.ClientSettings.AllowKeyboardNavigation = isEnabled;
        }
        void SalaryPlanGrid_PreRender(object sender, EventArgs e)
        {
            SetGridEnabledMode(GradeGrid, IsSalaryPlanGridViewMode, IsViewMode);
        }
        void GradeGrid_PreRender(object sender, EventArgs e)
        {
            SetGridEnabledMode(StepGrid, IsGradeGridViewMode, false);
        }
        void StepGrid_PreRender(object sender, EventArgs e)
        {
            SetGridEnabledMode(SalaryPlanGrid, IsStepGridViewMode, IsViewMode);
        }
        protected void SalaryPlanGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            SalaryPlanGrid.DataSource = Data;
        }
        protected void GradeGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (SelectedSalaryPlan != null)
            {
                _gradeCollection.Clear();
                _gradeCollection.Load(Common.ServiceWrapper.CodeClient.GetSalaryPlanCodeTableRows("code_salary_plan", Convert.ToString(SelectedSalaryPlan)));

                GradeGrid.DataSource = _gradeCollection;
            }
        }
        protected void StepGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (SelectedGrade != null)
            {
                _stepCollection.Clear();
                _stepCollection.Load(Common.ServiceWrapper.CodeClient.GetSalaryPlanCodeTableRows("code_salary_plan", Convert.ToString(SelectedGrade)));

                StepGrid.DataSource = _stepCollection;
            }
        }
        protected void SalaryPlanGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditFormInsertItem)
            {
                GridEditFormItem item = (GridEditFormItem)e.Item;

                //set default values for user
                CheckBoxControl activeFlag = (CheckBoxControl)item.FindControl("ActiveFlag");
                if (activeFlag != null)
                    activeFlag.Value = true;

                NumericControl sortOrder = (NumericControl)item.FindControl("SortOrder");
                if (sortOrder != null)
                    sortOrder.Value = 0;
            }
            else if (e.Item is GridDataItem)
            {
                //hide the delete image if there is a dependency
                GridDataItem item = (GridDataItem)e.Item;
                SalaryPlanCodeTable dataItem = (SalaryPlanCodeTable)item.DataItem;

                item["deleteButton"].Controls[0].Visible = !dataItem.HasChildrenFlag && !dataItem.SystemFlag;
            }
        }
        protected void SalaryPlanGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            GradeGrid.Rebind();
            StepGrid.Rebind();
        }
        protected void GradeGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            StepGrid.Rebind();
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        #endregion

        #region handle updates
        protected void SalaryPlanGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            SalaryPlanCodeTable item = (SalaryPlanCodeTable)e.Item.DataItem;
            item = Common.ServiceWrapper.CodeClient.InsertSalaryPlanCodeTableData(item);

            //clear all codes from assembly 
            Common.CodeHelper.ResetCodeTableCache();

            item.CopyTo((SalaryPlanCodeTable)Data[String.Empty]);
        }
        protected void GradeGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            SalaryPlanCodeTable item = (SalaryPlanCodeTable)e.Item.DataItem;
            item.ParentCodeTableId = SelectedSalaryPlan;

            item = Common.ServiceWrapper.CodeClient.InsertSalaryPlanCodeTableData(item);

            //clear all codes from assembly 
            Common.CodeHelper.ResetCodeTableCache();

            item.CopyTo(_gradeCollection[String.Empty]);
        }
        protected void StepGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            SalaryPlanCodeTable item = (SalaryPlanCodeTable)e.Item.DataItem;
            item.ParentCodeTableId = SelectedGrade;

            item = Common.ServiceWrapper.CodeClient.InsertSalaryPlanCodeTableData(item);

            //clear all codes from assembly 
            Common.CodeHelper.ResetCodeTableCache();

            item.CopyTo(_stepCollection[String.Empty]);
        }
        protected void CodeGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                SalaryPlanCodeTable item = (SalaryPlanCodeTable)e.Item.DataItem;
                item = Common.ServiceWrapper.CodeClient.UpdateSalaryPlanCodeTableData(item);

                //clear all codes from assembly 
                Common.CodeHelper.ResetCodeTableCache();

                item.CopyTo((SalaryPlanCodeTable)Data[item.Key]);

                //if descriptions were updated to be "", they would have been deleted from the db, so we remove them from our Data.Desc collection
                //using a foreach will not work if the collection is modified during the loop, so using an IF, starting at the end of the collection to traverse to the first of the collection
                for (int i = item.Descriptions.Count - 1; i > -1; i--)
                {
                    if (((SalaryPlanCodeTable)Data[item.Key]).Descriptions[i].TableDescription == "" && ((SalaryPlanCodeTable)Data[item.Key]).Descriptions[i].Id > 0)
                        ((SalaryPlanCodeTable)Data[item.Key]).Descriptions.RemoveAt(i);
                }

                //cleanup objects
                item = null;
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void SalaryPlanGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.CodeClient.DeleteSalaryPlanCodeTableData((SalaryPlanCodeTable)e.Item.DataItem);

                //clear all codes from assembly 
                Common.CodeHelper.ResetCodeTableCache();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void GradeGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.CodeClient.DeleteSalaryPlanCodeTableData((SalaryPlanCodeTable)e.Item.DataItem);

                //clear all codes from assembly 
                Common.CodeHelper.ResetCodeTableCache();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void StepGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.CodeClient.DeleteSalaryPlanCodeTableData((SalaryPlanCodeTable)e.Item.DataItem);

                //clear all codes from assembly 
                Common.CodeHelper.ResetCodeTableCache();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}