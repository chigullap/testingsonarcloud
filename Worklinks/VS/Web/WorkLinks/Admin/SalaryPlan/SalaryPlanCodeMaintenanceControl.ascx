﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SalaryPlanCodeMaintenanceControl.ascx.cs" Inherits="WorkLinks.Admin.SalaryPlanCodeMaintenance.SalaryPlanCodeMaintenanceControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPGrid
        ID="SalaryPlanGrid"
        runat="server"
        GridLines="None"
        Width="100%"
        AutoGenerateColumns="false"
        OnUpdateCommand="CodeGrid_UpdateCommand"
        OnInsertCommand="SalaryPlanGrid_InsertCommand"
        OnDeleteCommand="SalaryPlanGrid_DeleteCommand">

        <ClientSettings EnablePostBackOnRowClick="true" AllowKeyboardNavigation="true">
            <Selecting AllowRowSelect="true" />
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">

            <CommandItemTemplate>
                <wasp:WLPToolBar ID="CodeToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
                    <Items>
                        <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsSalaryPlanGridViewMode && AddFlag %>' ResourceName="Add" />
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="EditButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif">
                    <HeaderStyle Width="30px" />
                </wasp:GridEditCommandControl>
                <wasp:GridBoundControl DataField="CodeTableId" LabelText="**Code**" SortExpression="CodeTableId" UniqueName="CodeTableId" ResourceName="SalaryPlanCodeTableId" />
                <wasp:GridBoundControl DataField="EnglishDesc" LabelText="**EnglishDesc**" UniqueName="Description" ResourceName="EnglishDesc">
                    <HeaderStyle Width="200px" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="FrenchDesc" LabelText="**FrenchDesc**" UniqueName="Description" ResourceName="FrenchDesc">
                    <HeaderStyle Width="200px" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="ImportExternalIdentifier" LabelText="**ImportExternalIdentifier**" UniqueName="ImportExternalIdentifier" ResourceName="ImportExternalIdentifier" />
                <wasp:GridCheckBoxControl DataField="ActiveFlag" LabelText="**ActiveFlag**" SortExpression="ActiveFlag" UniqueName="ActiveFlag" ResourceName="ActiveFlag" />
                <wasp:GridCheckBoxControl DataField="SystemFlag" LabelText="**SystemFlag**" SortExpression="SystemFlag" UniqueName="SystemFlag" ResourceName="SystemFlag" />
                <wasp:GridNumericControl DataField="SortOrder" LabelText="**SortOrder**" SortExpression="SortOrder" UniqueName="SortOrder" DataType="System.Int64" ResourceName="SortOrder" />
                <wasp:GridButtonControl UniqueName="DeleteButton" ButtonType="ImageButton" CommandName="Delete" ImageUrl="~/App_Themes/Default/Delete.gif">
                    <HeaderStyle Width="30px" />
                </wasp:GridButtonControl>
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="SalaryPlanCodeTableId" runat="server" Text="**Code**" ResourceName="SalaryPlanCodeTableId" Value='<%# Bind("CodeTableId") %>' ReadOnly='<%# IsSalaryPlanGridUpdate %>' Mandatory="true" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="SalaryPlanImportExternalIdentifier" runat="server" Text="**ImportExternalIdentifier**" ResourceName="ImportExternalIdentifier" Value='<%# Bind("ImportExternalIdentifier") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="SalaryPlanEnglishDesc" runat="server" Text="**EnglishDesc**" ResourceName="EnglishDesc" Value='<%# Bind("EnglishDesc") %>' Mandatory="true" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="SalaryPlanFrenchDesc" runat="server" Text="**FrenchDesc**" ResourceName="FrenchDesc" Value='<%# Bind("FrenchDesc") %>' Mandatory="true" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:CheckBoxControl ID="SalaryPlanActiveFlag" runat="server" LabelText="**ActiveFlag**" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>' />
                            </td>
                            <td>
                                <wasp:CheckBoxControl ID="SalaryPlanSystemFlag" runat="server" LabelText="**SystemFlag**" ResourceName="SystemFlag" Value='<%# Bind("SystemFlag") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:NumericControl ID="SalaryPlanSortOrder" runat="server" DecimalDigits="0" ResourceName="SortOrder" Value='<%# Bind("SortOrder") %>' Mandatory="true" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:WLPButton ID="SalaryPlanBtnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsSalaryPlanGridUpdate %>' ResourceName="Update" />
                                            <wasp:WLPButton ID="SalaryPlanBtnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsSalaryPlanGridInsert %>' ResourceName="Insert" />
                                        </td>
                                        <td>
                                            <wasp:WLPButton ID="SalaryPlanBtnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>

        </MasterTableView>

    </wasp:WLPGrid>

    <wasp:WLPGrid
        ID="GradeGrid"
        runat="server"
        GridLines="None"
        Width="100%"
        AutoGenerateColumns="false"
        OnUpdateCommand="CodeGrid_UpdateCommand"
        OnInsertCommand="GradeGrid_InsertCommand"
        OnDeleteCommand="GradeGrid_DeleteCommand">

        <ClientSettings EnablePostBackOnRowClick="false" AllowKeyboardNavigation="true">
            <Selecting AllowRowSelect="false" />
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">

            <CommandItemTemplate>
                <wasp:WLPToolBar ID="GradeGridToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
                    <Items>
                        <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsGradeGridViewMode && AddFlag %>' ResourceName="Add" />
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="EditButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif">
                    <HeaderStyle Width="30px" />
                </wasp:GridEditCommandControl>
                <wasp:GridBoundControl DataField="CodeTableId" LabelText="**Code**" SortExpression="CodeTableId" UniqueName="CodeTableId" ResourceName="GradeCodeTableId" />
                <wasp:GridBoundControl DataField="EnglishDesc" LabelText="**EnglishDesc**" UniqueName="Description" ResourceName="EnglishDesc">
                    <HeaderStyle Width="200px" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="FrenchDesc" LabelText="**FrenchDesc**" UniqueName="Description" ResourceName="FrenchDesc">
                    <HeaderStyle Width="200px" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="ImportExternalIdentifier" LabelText="**ImportExternalIdentifier**" UniqueName="ImportExternalIdentifier" ResourceName="ImportExternalIdentifier" />
                <wasp:GridCheckBoxControl DataField="ActiveFlag" LabelText="**ActiveFlag**" SortExpression="ActiveFlag" UniqueName="ActiveFlag" ResourceName="ActiveFlag" />
                <wasp:GridCheckBoxControl DataField="SystemFlag" LabelText="**SystemFlag**" SortExpression="SystemFlag" UniqueName="SystemFlag" ResourceName="SystemFlag" />
                <wasp:GridNumericControl DataField="SortOrder" LabelText="**SortOrder**" SortExpression="SortOrder" UniqueName="SortOrder" DataType="System.Int64" ResourceName="SortOrder" />
                <wasp:GridButtonControl UniqueName="DeleteButton" ButtonType="ImageButton" CommandName="Delete" ImageUrl="~/App_Themes/Default/Delete.gif">
                    <HeaderStyle Width="30px" />
                </wasp:GridButtonControl>
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="GradeCodeTableId" runat="server" Text="**Code**" ResourceName="GradeCodeTableId" Value='<%# Bind("CodeTableId") %>' ReadOnly='<%# IsGradeGridUpdate %>' Mandatory="true" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="GradeImportExternalIdentifier" runat="server" Text="**ImportExternalIdentifier**" ResourceName="ImportExternalIdentifier" Value='<%# Bind("ImportExternalIdentifier") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="GradeEnglishDesc" runat="server" Text="**EnglishDesc**" ResourceName="EnglishDesc" Value='<%# Bind("EnglishDesc") %>' Mandatory="true" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="GradeFrenchDesc" runat="server" Text="**FrenchDesc**" ResourceName="FrenchDesc" Value='<%# Bind("FrenchDesc") %>' Mandatory="true" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:CheckBoxControl ID="GradeActiveFlag" runat="server" LabelText="**ActiveFlag**" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>' />
                            </td>
                            <td>
                                <wasp:CheckBoxControl ID="GradeSystemFlag" runat="server" LabelText="**SystemFlag**" ResourceName="SystemFlag" Value='<%# Bind("SystemFlag") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:NumericControl ID="GradeSortOrder" runat="server" DecimalDigits="0" ResourceName="SortOrder" Value='<%# Bind("SortOrder") %>' Mandatory="true" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:WLPButton ID="GradeBtnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsGradeGridUpdate %>' ResourceName="Update" />
                                            <wasp:WLPButton ID="GradeBtnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsGradeGridInsert %>' ResourceName="Insert" />
                                        </td>
                                        <td>
                                            <wasp:WLPButton ID="GradeBtnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>

        </MasterTableView>

    </wasp:WLPGrid>

    <wasp:WLPGrid
        ID="StepGrid"
        runat="server"
        GridLines="None"
        Width="100%"
        AutoGenerateColumns="false"
        OnUpdateCommand="CodeGrid_UpdateCommand"
        OnInsertCommand="StepGrid_InsertCommand"
        OnDeleteCommand="StepGrid_DeleteCommand">

        <ClientSettings EnablePostBackOnRowClick="false" AllowKeyboardNavigation="true">
            <Selecting AllowRowSelect="false" />
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">

            <CommandItemTemplate>
                <wasp:WLPToolBar ID="StepGridToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
                    <Items>
                        <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsStepGridViewMode && AddFlag %>' ResourceName="Add" />
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="EditButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif">
                    <HeaderStyle Width="30px" />
                </wasp:GridEditCommandControl>
                <wasp:GridBoundControl DataField="CodeTableId" LabelText="**Code**" SortExpression="CodeTableId" UniqueName="CodeTableId" ResourceName="StepCodeTableId" />
                <wasp:GridBoundControl DataField="EnglishDesc" LabelText="**EnglishDesc**" UniqueName="Description" ResourceName="EnglishDesc">
                    <HeaderStyle Width="200px" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="FrenchDesc" LabelText="**FrenchDesc**" UniqueName="Description" ResourceName="FrenchDesc">
                    <HeaderStyle Width="200px" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="ImportExternalIdentifier" LabelText="**ImportExternalIdentifier**" UniqueName="ImportExternalIdentifier" ResourceName="ImportExternalIdentifier" />
                <wasp:GridCheckBoxControl DataField="ActiveFlag" LabelText="**ActiveFlag**" SortExpression="ActiveFlag" UniqueName="ActiveFlag" ResourceName="ActiveFlag" />
                <wasp:GridCheckBoxControl DataField="SystemFlag" LabelText="**SystemFlag**" SortExpression="SystemFlag" UniqueName="SystemFlag" ResourceName="SystemFlag" />
                <wasp:GridNumericControl DataField="SortOrder" LabelText="**SortOrder**" SortExpression="SortOrder" UniqueName="SortOrder" DataType="System.Int64" ResourceName="SortOrder" />
                <wasp:GridButtonControl UniqueName="DeleteButton" ButtonType="ImageButton" CommandName="Delete" ImageUrl="~/App_Themes/Default/Delete.gif">
                    <HeaderStyle Width="30px" />
                </wasp:GridButtonControl>
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="StepCodeTableId" runat="server" Text="**Code**" ResourceName="StepCodeTableId" Value='<%# Bind("CodeTableId") %>' ReadOnly='<%# IsStepGridUpdate %>' Mandatory="true" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="StepImportExternalIdentifier" runat="server" Text="**ImportExternalIdentifier**" ResourceName="ImportExternalIdentifier" Value='<%# Bind("ImportExternalIdentifier") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="StepEnglishDesc" runat="server" Text="**EnglishDesc**" ResourceName="EnglishDesc" Value='<%# Bind("EnglishDesc") %>' Mandatory="true" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="StepFrenchDesc" runat="server" Text="**FrenchDesc**" ResourceName="FrenchDesc" Value='<%# Bind("FrenchDesc") %>' Mandatory="true" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:CheckBoxControl ID="StepActiveFlag" runat="server" LabelText="**ActiveFlag**" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>' />
                            </td>
                            <td>
                                <wasp:CheckBoxControl ID="StepSystemFlag" runat="server" LabelText="**SystemFlag**" ResourceName="SystemFlag" Value='<%# Bind("SystemFlag") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:NumericControl ID="StepSortOrder" runat="server" DecimalDigits="0" ResourceName="SortOrder" Value='<%# Bind("SortOrder") %>' Mandatory="true" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:WLPButton ID="StepBtnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsStepGridUpdate %>' ResourceName="Update" />
                                            <wasp:WLPButton ID="StepBtnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsStepGridInsert %>' ResourceName="Insert" />
                                        </td>
                                        <td>
                                            <wasp:WLPButton ID="StepBtnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>

        </MasterTableView>

    </wasp:WLPGrid>
</asp:Panel>