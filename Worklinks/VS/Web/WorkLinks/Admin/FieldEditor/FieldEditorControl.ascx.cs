﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.FieldEditor
{
    public partial class FieldEditorControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private FieldEntityCollection _collection = new FieldEntityCollection(); //used to populate child grid
        private const String _attributeIdentifier = "Mandatory";
        private const int _roleId = 1;
        #endregion

        #region properties
        public bool IsViewMode
        {
            get { return FieldGrid.IsViewMode; }
        }
        public bool UpdateFlag
        {
            get { return Common.Security.RoleForm.MandatoryField.UpdateFlag; }
        }
        private long? SelectedFormId
        {
            get { return FormGrid.SelectedValue == null ? (long?)null : Convert.ToInt64(FormGrid.SelectedValue); }
        }
        public bool MandatoryFieldsExist
        {
            get { return FieldGrid.DataSource.Count > 0; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.MandatoryField.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                LoadFormGridInfo();
                RemoveParentScrollBar();
            }
        }
        protected void WireEvents()
        {
            FormGrid.NeedDataSource += new GridNeedDataSourceEventHandler(FormGrid_NeedDataSource);
            FormGrid.SelectedIndexChanged += new EventHandler(FormGrid_SelectedIndexChanged);

            FieldGrid.NeedDataSource += new GridNeedDataSourceEventHandler(FieldGrid_NeedDataSource);
            FieldGrid.UpdateAllCommand += new WLPGrid.UpdateAllCommandEventHandler(FieldGrid_UpdateAllCommand);
            FieldGrid.PreRender += new EventHandler(FieldGrid_PreRender);
        }
        protected void LoadFormGridInfo()
        {
            Data = FormDescriptionCollection.ConvertCollection(Common.ServiceWrapper.CodeClient.GetFormInfo(null));
        }
        private void RemoveParentScrollBar()
        {
            //remove the scrollbar for this control by overriding the RadPane settings in Site.Master
            System.Web.UI.Control parentPane = FormGrid.Parent.Parent.Parent;
            if (parentPane is RadPane)
                ((RadPane)parentPane).Scrolling = SplitterPaneScrolling.None;
        }
        #endregion

        #region event handlers
        void FormGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            FormGrid.DataSource = Data;
        }
        protected void FormGrid_DataBound(object sender, EventArgs e)
        {
            if (FormGrid.Items.Count > 0)
                FormGrid.Items[0].Selected = true;
        }
        protected void FormGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            FieldGrid.Rebind();
        }
        private void SetGridEnabledMode(WLPGrid grid, bool isEnabled)
        {
            grid.Enabled = isEnabled;
            grid.ClientSettings.EnablePostBackOnRowClick = isEnabled;
            grid.ClientSettings.Resizing.AllowColumnResize = isEnabled;
            grid.ClientSettings.Selecting.AllowRowSelect = isEnabled;
            grid.ClientSettings.AllowKeyboardNavigation = isEnabled;
        }
        void FieldGrid_PreRender(object sender, EventArgs e)
        {
            foreach (GridItem item in FieldGrid.MasterTableView.Items)
            {
                WLPCheckBox control = (WLPCheckBox)item.FindControl("FieldValue");
                control.ReadOnly = IsViewMode;
            }

            SetGridEnabledMode(FormGrid, IsViewMode);
        }
        void FieldGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (SelectedFormId != null)
            {
                _collection.Load(Common.ServiceWrapper.CodeClient.GetFieldInfo(Convert.ToDecimal(SelectedFormId), _attributeIdentifier, _roleId));
                FieldGrid.DataSource = _collection;
            }
        }
        void FieldGrid_UpdateAllCommand(object sender, UpdateAllCommandEventArgs e)
        {
            try
            {
                //pass the collection to WS, determine if updates or inserts are needed, then return collection
                FieldEntityCollection collection = new FieldEntityCollection();

                foreach (FieldEntity entity in e.Items)
                    collection.Add(entity);

                Common.ServiceWrapper.CodeClient.UpdateFieldEntity(collection.ToArray());
                Common.Resources.DatabaseResourceCache.ClearCache();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}