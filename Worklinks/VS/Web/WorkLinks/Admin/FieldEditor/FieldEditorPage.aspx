﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FieldEditorPage.aspx.cs" Inherits="WorkLinks.Admin.FieldEditor.FieldEditorPage" %>
<%@ Register Src="FieldEditorControl.ascx" TagName="FieldEditorControl" TagPrefix="fec1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <fec1:FieldEditorControl ID="FieldEditorControl1" runat="server" />        
    </ContentTemplate>
</asp:Content>