﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FieldEditorControl.ascx.cs" Inherits="WorkLinks.Admin.FieldEditor.FieldEditorControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="FormGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="FieldGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy2" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="FieldGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="FormGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<wasp:WLPGrid
    ID="FormGrid"
    runat="server"
    GridLines="None"
    Width="100%"
    Height="200px"
    AutoGenerateColumns="false"
    AutoInsertOnAdd="false"
    OnDataBound="FormGrid_DataBound"
    AllowMultiRowEdit="true">

    <ClientSettings EnablePostBackOnRowClick="true" AllowKeyboardNavigation="true">
        <Selecting AllowRowSelect="true" />
        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="FormId">
        <CommandItemTemplate></CommandItemTemplate>

        <Columns>
            <wasp:GridBoundControl DataField="FormDescriptionField" LabelText="**FormName" SortExpression="FormDescriptionField" UniqueName="FormDescriptionField" ResourceName="FormDescriptionField" />
        </Columns>
    </MasterTableView>

    <HeaderContextMenu EnableAutoScroll="true" />

</wasp:WLPGrid>

<wasp:WLPGrid
    ID="FieldGrid"
    EditMode="InPlace"
    runat="server"
    GridLines="None"
    Width="100%"
    Height="400px"
    AutoGenerateColumns="false"
    AutoInsertOnAdd="false"
    AllowMultiRowEdit="true"
    Skin="">

    <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
        <Selecting AllowRowSelect="false" />
        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
        <CommandItemTemplate>
            <wasp:WLPToolBar ID="FieldGridToolBar" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="**Edit" ImageUrl="~/App_Themes/Default/edit.gif" CommandName="EditAll" Visible='<%# IsViewMode && UpdateFlag && MandatoryFieldsExist %>' CausesValidation="true" ResourceName="Edit" />
                    <wasp:WLPToolBarButton Text="**UpdateAll" ImageUrl="~/App_Themes/Default/update.gif" CommandName="UpdateAll" Visible='<%# !IsViewMode %>' CausesValidation="true" ResourceName="Update" />
                    <wasp:WLPToolBarButton Text="**Cancel" ImageUrl="~/App_Themes/Default/cancel.gif" CommandName="CancelAll" Visible='<%# !IsViewMode %>' ResourceName="Cancel" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <Columns>
            <wasp:GridTemplateControl UniqueName="HeaderColumns" Reorderable="false">
                <HeaderTemplate>
                    <table width="100%">
                        <tr>
                            <td align="center" width="25%">
                                <wasp:WLPLabel ID="Mandatory" runat="server" ResourceName="MandatoryText" />
                            </td>
                            <td align="left" width="75%">
                                <wasp:WLPLabel ID="Field" runat="server" ResourceName="FieldText" />
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table width="100%">
                        <tr>
                            <td width="25%" align="center">
                                <wasp:WLPCheckBox ID="FieldValue" HideLabel="true" runat="server" ResourceName="FieldValue" Value='<%# Bind("FieldValueAsBoolean") %>' ReadOnly="true" TabIndex="010"></wasp:WLPCheckBox>
                            </td>
                            <td width="75%">
                                <wasp:WLPLabel ID="FieldIdentifier" runat="server" Text='<%# Bind("FieldIdentifier") %>' />
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <EditItemTemplate>
                    <table width="100%">
                        <tr>
                            <td width="25%" align="center">
                                <wasp:WLPCheckBox ID="FieldValue" HideLabel="true" runat="server" ResourceName="FieldValue" Value='<%# Bind("FieldValueAsBoolean") %>' ReadOnly="false" TabIndex="010"></wasp:WLPCheckBox>
                            </td>
                            <td width="75%">
                                <wasp:WLPLabel ID="FieldIdentifier" runat="server" Text='<%# Bind("FieldIdentifier") %>' />
                            </td>
                        </tr>
                    </table>
                </EditItemTemplate>
            </wasp:GridTemplateControl>
        </Columns>
    </MasterTableView>

    <HeaderContextMenu EnableAutoScroll="true" />

</wasp:WLPGrid>