﻿using System;
using Telerik.Web.UI;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.RoleEditor
{
    public partial class RoleDatabaseControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private long RoleId { get { return Convert.ToInt64(Page.RouteData.Values["roleId"]); } }
        protected bool IsViewMode { get { return !RoleDataSecurityTree.Enabled; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetAuthorized(Common.Security.RoleForm.RoleSecurityData.ViewFlag);

                this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "DataSecurity"));

                ReloadTree();
            }
        }
        private void ReloadTree()
        {
            RoleDataSecurityTree.Nodes.Clear();
            Prepopulate();
            RoleDataSecurityTree.Enabled = false;
        }
        private void Prepopulate()
        {
            Data = SecurityLabelRoleCollection.ConvertCollection(Common.ServiceWrapper.SecurityClient.GetSecurityLabelRole(RoleId));

            // Load first row nodes
            PopulateNodes(1, RoleDataSecurityTree.Nodes);

            // Load selected notes
            foreach (SecurityLabelRole label in Data)
            {
                String[] markingIds = label.LabelPath.Substring(1).Split('/');
                SetMarkings(RoleDataSecurityTree.Nodes, markingIds);
            }
        }
        private void PopulateNodes(int nodeLevel, RadTreeNodeCollection parentNodecollection)
        {
            // Load first nodes
            foreach (SecurityMarking marking in Common.ServiceWrapper.SecurityClient.GetSecurityMarking(nodeLevel))
            {
                if (parentNodecollection.FindNodeByValue(marking.Key) == null)
                {
                    RadTreeNode node = new RadTreeNode { Text = marking.Description, Value = marking.Key };

                    if (marking.HasChildrenFlag)
                        node.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;

                    parentNodecollection.Add(node);
                }
            }
        }
        private void SetMarkings(RadTreeNodeCollection nodes, String[] markingIds)
        {
            RadTreeNodeCollection currentNodeCollection = nodes;
            RadTreeNode currentNode;

            foreach (String marking in markingIds)
            {
                currentNode = GetNode(currentNodeCollection, marking);
                currentNode.Checked = true;
                currentNode.Expanded = true;
                currentNodeCollection = currentNode.Nodes;

                PopulateNodes(currentNode.Level + 2, currentNodeCollection);
            }
        }
        private RadTreeNode GetNode(RadTreeNodeCollection nodes, String value)
        {
            foreach (RadTreeNode node in nodes)
            {
                if (node.Value.Equals(value))
                    return node;
            }

            return null;
        }
        #endregion

        #region event handlers
        protected void RoleDataSecurityTree_NodeExpand(object sender, RadTreeNodeEventArgs e)
        {
            PopulateNodes(e.Node.Level + 2, e.Node.Nodes);
        }
        protected void ToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            String commandName = ((RadToolBarButton)e.Item).CommandName.ToLower();

            if (commandName.Equals("update"))
            {
                System.Collections.Generic.IList<RadTreeNode> checkedNodes = RoleDataSecurityTree.CheckedNodes;
                SecurityLabelRoleCollection labels = new SecurityLabelRoleCollection();
                int i = -1;

                foreach (RadTreeNode node in checkedNodes)
                {
                    if (!HasCheckedChildren(node))
                    {
                        SecurityLabelRole label = new SecurityLabelRole();
                        label.SecurityLabelRoleId = i--;
                        label.SecurityRoleId = RoleId;
                        label.LabelPath = GetFullValuePath(node);
                        label.UpdateDatetime = DateTime.Now;
                        labels.Add(label);
                    }
                }

                try
                {
                    Common.ServiceWrapper.SecurityClient.UpdateSecurityLabelRole(RoleId, labels);
                }
                catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
                {
                    if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                ReloadTree();
            }
            else if (commandName.Equals("edit"))
                RoleDataSecurityTree.Enabled = true;
            else if (commandName.Equals("cancel"))
                ReloadTree();
        }
        private bool HasCheckedChildren(RadTreeNode node)
        {
            foreach (RadTreeNode childNode in node.Nodes)
            {
                if (childNode.Checked)
                    return true;
            }

            return false;
        }
        private String GetFullValuePath(RadTreeNode node)
        {
            return (node.ParentNode == null ? String.Empty : GetFullValuePath(node.ParentNode)) + @"/" + node.Value;
        }
        protected void ToolBar_PreRender(object sender, EventArgs e)
        {
            ToolBar.FindItemByValue("Edit").Visible = Common.Security.RoleForm.RoleSecurityData.UpdateFlag && IsViewMode;
            ToolBar.FindItemByValue("Update").Visible = !IsViewMode;
            ToolBar.FindItemByValue("Cancel").Visible = !IsViewMode;
        }
        #endregion
    }
}