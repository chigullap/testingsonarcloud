﻿<%@ Page Language="C#" MasterPageFile="~/HumanResources/Employee.Master" AutoEventWireup="true" CodeBehind="RoleDatabasePage.aspx.cs" Inherits="WorkLinks.Admin.RoleEditor.RoleDatabasePage" %>
<%@ Register Src="RoleDatabaseControl.ascx" TagName="RoleDatabaseControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <uc1:RoleDatabaseControl ID="RoleDatabaseControl" runat="server" />
</asp:Content>