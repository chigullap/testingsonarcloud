﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RoleSearchPage.aspx.cs" Inherits="WorkLinks.Admin.RoleEditor.RoleSearchPage" %>
<%@ Register Src="RoleSearchControl.ascx" TagName="RoleSearchControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:RoleSearchControl ID="RoleSearchControl1" SearchLocation="Admin" runat="server" />        
    </ContentTemplate>
</asp:Content>