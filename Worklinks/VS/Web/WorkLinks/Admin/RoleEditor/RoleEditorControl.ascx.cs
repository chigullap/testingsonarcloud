﻿using System;
using System.Security.Claims;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Common;

namespace WorkLinks.Admin.RoleEditor
{
    public partial class RoleEditorControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private long RoleId { get { return Convert.ToInt64(Page.RouteData.Values["roleId"]); } }
        public bool IsEditMode { get { return RoleInfoView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.Edit); } }
        public bool IsInsertMode { get { return RoleInfoView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.Insert); } }
        public bool IsViewMode { get { return RoleInfoView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.ReadOnly); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.RoleEdit.UpdateFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.RoleEdit.ViewFlag);

            WireEvents();

            if ((Page.RouteData.Values["action"].ToString().ToLower() == "view"))
            {
                this.Page.Title = "Edit a Role";

                if (!IsPostBack) LoadRoleInformation();
            }
            else if ((Page.RouteData.Values["action"].ToString().ToLower() == "add"))
            {
                if (!IsPostBack)
                {
                    this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "AddARole"));

                    RoleDescriptionCollection newRoleCollection = new RoleDescriptionCollection();
                    newRoleCollection.AddNew();
                    Data = newRoleCollection;

                    // Change formview's mode to Insert mode
                    RoleInfoView.ChangeMode(System.Web.UI.WebControls.FormViewMode.Insert);

                    // Databind
                    RoleInfoView.DataBind();
                }
                else
                    this.Page.Title = "Edit a Role"; // We are loading the edit page after an add
            }

            if (!checkForWorkLinksAdministrationFlagIsValid()) SetAuthorized(false);
        }
        protected bool checkForWorkLinksAdministrationFlagIsValid()
        {
            bool checkForWorkLinksAdministrationFlag = false;

            if (GetSecurityUser().WorklinksAdministrationFlag)
                checkForWorkLinksAdministrationFlag = true;
            else
                checkForWorkLinksAdministrationFlag = !((RoleDescription)Data[0]).WorklinksAdministrationFlag;

            return checkForWorkLinksAdministrationFlag;
        }
        protected void WireEvents()
        {
            RoleInfoView.PreRender += new EventHandler(RoleInfoView_PreRender);
            RoleInfoView.NeedDataSource += new WLP.Web.UI.Controls.WLPFormView.NeedDataSourceEventHandler(RoleInfoView_NeedDataSource);
            RoleInfoView.Updating += new WLP.Web.UI.Controls.WLPFormView.ItemUpdatingEventHandler(RoleInfoView_Updating);
            RoleInfoView.Inserting += new WLP.Web.UI.Controls.WLPFormView.ItemInsertingEventHandler(RoleInfoView_Inserting);
            RoleInfoView.ItemCreated += new EventHandler(RoleInfoView_ItemCreated);
        }
        protected SecurityUser GetSecurityUser()
        {
            //String loggedInUser = ((ClaimsIdentity)System.Web.HttpContext.Current.User.Identity).FindFirst("uname").Value;//yuc
            String loggedInUser = System.Web.HttpContext.Current.User.Identity.Name.ToString();
            SecurityUser user = Common.ServiceWrapper.SecurityClient.GetSecurityUser(loggedInUser)[0];

            return user;
        }
        protected void LoadRoleInformation()
        {
            // Create business object and fill it with the data passed to the page, then bind the grid
            RoleDescriptionCollection role = new RoleDescriptionCollection();
            role.Add(LoadDataIntoBusinessClass());

            Data = role;
            RoleInfoView.DataBind();
        }
        private RoleDescription LoadDataIntoBusinessClass()
        {
            RoleDescriptionCollection temp = new RoleDescriptionCollection();
            temp.Load(Common.ServiceWrapper.SecurityClient.GetRoleDescriptions(null, RoleId, null));

            return temp[0]; // Will only ever have 1 item in this collection
        }
        #endregion

        #region event handlers
        void RoleInfoView_PreRender(object sender, EventArgs e)
        {
            WLP.Web.UI.Controls.CheckBoxControl worklinksAdmin = (WLP.Web.UI.Controls.CheckBoxControl)RoleInfoView.FindControl("WorklinksAdministrationFlag");

            if (worklinksAdmin != null)
                if (!GetSecurityUser().WorklinksAdministrationFlag) worklinksAdmin.Style.Add("display", "none");
        }
        void RoleInfoView_NeedDataSource(object sender, WLP.Web.UI.Controls.WLPNeedDataSourceEventArgs e)
        {
            RoleInfoView.DataSource = Data;
        }
        void RoleInfoView_ItemCreated(object sender, EventArgs e)
        {
            if (IsInsertMode)
            {
                WLP.Web.UI.Controls.ComboBoxControl roleType = (WLP.Web.UI.Controls.ComboBoxControl)RoleInfoView.FindControl("RoleTypeSecurityRoleId");
                roleType.Value = "1"; // 1 = normal
            }
        }
        protected void PopulateCombo(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateComboBoxWithRoleDescription((WLP.Web.UI.Controls.ICodeControl)sender);
        }
        #endregion

        #region handle updates
        void RoleInfoView_Updating(object sender, WLP.Web.UI.Controls.WLPItemUpdatingEventArgs e)
        {
            try
            {
                RoleDescription roleDesc = (RoleDescription)e.DataItem; // One description being updated
                Common.ServiceWrapper.SecurityClient.UpdateRoleDesc(ApplicationParameter.AuthDatabaseName, ApplicationParameter.SecurityClientUsedByAngular, roleDesc).CopyTo((RoleDescription)Data[roleDesc.Key]); //yuc

                if (e.CommandArgument.ToString() == "close")
                {
                    System.Web.UI.ClientScriptManager manager = Page.ClientScript;
                    manager.RegisterStartupScript(this.GetType(), "close and return", "closeAndReturn();", true);
                }
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void RoleInfoView_Inserting(object sender, WLP.Web.UI.Controls.WLPItemInsertingEventArgs e)
        {
            RoleDescription roleDesc = (RoleDescription)e.DataItem;
            Common.ServiceWrapper.SecurityClient.InsertRoleDescription(ApplicationParameter.AuthDatabaseName, ApplicationParameter.SecurityClientUsedByAngular, roleDesc).CopyTo((RoleDescription)Data[roleDesc.Key]);

            if (e.CommandArgument.ToString() == "close")
            {
                System.Web.UI.ClientScriptManager manager = Page.ClientScript;
                manager.RegisterStartupScript(this.GetType(), "close and return", "closeAndReturn();", true);
            }
        }
        #endregion
    }
}