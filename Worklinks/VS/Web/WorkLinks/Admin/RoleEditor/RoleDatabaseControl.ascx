﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RoleDatabaseControl.ascx.cs" Inherits="WorkLinks.Admin.RoleEditor.RoleDatabaseControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPToolBar ID="ToolBar" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="ToolBar_ButtonClick" OnPreRender="ToolBar_PreRender">
        <Items>
            <wasp:WLPToolBarButton Text="Edit" ImageUrl="~/App_Themes/Default/update.gif" CommandName="Edit" Value="Edit" ResourceName="Edit"></wasp:WLPToolBarButton>
            <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/update.gif" CommandName="Update" Value="Update" CausesValidation="true" ResourceName="Update"></wasp:WLPToolBarButton>
            <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/cancel.gif" CommandName="Cancel" Value="Cancel" ResourceName="Cancel"></wasp:WLPToolBarButton>
        </Items>
    </wasp:WLPToolBar>

    <wasp:WLPTreeView ID="RoleDataSecurityTree" runat="server" CheckBoxes="true" Height="100%" OnNodeExpand="RoleDataSecurityTree_NodeExpand" OnClientNodeChecked="clientNodeChecked"></wasp:WLPTreeView>
</asp:Panel>

<script type="text/javascript" language="javascript">
    // Enable scrollbar
    function clientNodeChecked(sender, eventArgs) {
        var node = eventArgs.get_node();
        if (node.get_checked()) {
            while (node.get_parent().set_checked != null) {
                node = node.get_parent();
                node.set_checked(true);
            }
        }
        else if (!node.get_checked())
            uncheckChildren(node);
    }

    function uncheckChildren(node) {
        var childNodes = node.get_allNodes();
        if (childNodes != null) {
            for (i = 0; i < childNodes.length; i++) {
                var currentChildNode = childNodes[i];
                currentChildNode.set_checked(false);
            }
        }
    }

    function closeWindow(button, args) {
        button.set_autoPostBack(false);
        var popup = getRadWindow();
        setTimeout(function () { popup.close(); }, 0); // Have to use "setTimeout" to get around an IE9 bug
    }

    function getRadWindow() {
        var popup = null;
        if (window.radWindow)
            popup = window.radWindow;
        else if (window.frameElement.radWindow)
            popup = window.frameElement.radWindow;
        return popup;
    }
</script>