﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.WsibAdmin
{
    public partial class WsibEffectiveControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        String _codeWsib = null;
        private const String _codeWsibKey = "CodeWsibKey";
        #endregion

        #region properties
        public String Wsib
        {
            get
            {
                if (_codeWsib == null)
                {
                    Object obj = ViewState[_codeWsibKey];
                    if (obj != null)
                        _codeWsib = (String)obj;
                }

                return _codeWsib;
            }
            set
            {
                _codeWsib = value;
                ViewState[_codeWsibKey] = _codeWsib;
            }
        }
        public bool IsViewMode { get { return WsibEffectiveGrid.IsViewMode; } }
        public bool IsEditMode { get { return WsibEffectiveGrid.IsEditMode; } }
        public bool IsInsertMode { get { return WsibEffectiveGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.Wsib.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.Wsib.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.Wsib.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.Wsib.ViewFlag);

            WireEvents();
        }
        private void WireEvents()
        {
            WsibEffectiveGrid.NeedDataSource += new GridNeedDataSourceEventHandler(WsibEffectiveGrid_NeedDataSource);
            WsibEffectiveGrid.ClientSettings.EnablePostBackOnRowClick = false;
        }
        public void LoadWsibEffectiveInformation(String wsibCode)
        {
            Data = CodeWsibEffectiveCollection.ConvertCollection(Common.ServiceWrapper.CodeClient.GetCodeWsibEffective(wsibCode));
        }
        #endregion

        #region event handlers
        void WsibEffectiveGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            WsibEffectiveGrid.DataSource = Data;
        }
        protected void WsibEffectiveGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                if (dataItem.DataItem != null)
                {
                    //if the record is effective you cannot delete it, only future dated records can be deleted
                    if (((CodeWsibEffective)dataItem.DataItem).EffectiveDate < DateTime.Now)
                    {
                        ImageButton deleteButton = (ImageButton)dataItem["deleteButton"].Controls[0];
                        deleteButton.Visible = false;
                    }
                }
            }

            if ((e.Item is GridEditableItem) && (e.Item.IsInEditMode))
            {
                GridEditableItem editItem = (GridEditableItem)e.Item;
                DateControl effectiveControl = (DateControl)editItem.FindControl("EffectiveDate");
                int numOfItems = WsibEffectiveGrid.Items.Count;

                //get the last record and set the MinDate to a day after the EffectiveDate
                if (numOfItems > 0)
                {
                    CodeWsibEffective lastItem = (CodeWsibEffective)Data[numOfItems - 1];
                    effectiveControl.MinDate = lastItem.EffectiveDate.AddDays(1);
                }
            }
        }
        #endregion

        #region handle updates
        protected void WsibEffectiveGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            CodeWsibEffective item = (CodeWsibEffective)e.Item.DataItem;
            item.WsibCode = Wsib;
            Common.ServiceWrapper.CodeClient.InsertCodeWsibEffective(item).CopyTo((CodeWsibEffective)Data[item.Key]);
        }
        protected void WsibEffectiveGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                CodeWsibEffective item = (CodeWsibEffective)e.Item.DataItem;
                Common.ServiceWrapper.CodeClient.UpdateCodeWsibEffective(item).CopyTo((CodeWsibEffective)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void WsibEffectiveGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.CodeClient.DeleteCodeWsibEffective(((CodeWsibEffective)e.Item.DataItem));
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}