﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WsibControl.ascx.cs" Inherits="WorkLinks.Admin.WsibAdmin.WsibControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPToolBar ID="WsibToolBar" runat="server" Width="100%" OnButtonClick="WsibToolBar_ButtonClick" OnClientLoad="WsibToolBar_init">
        <Items>
            <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" onclick="Add()" CommandName="add" ResourceName="Add" OnPreRender="AddButton_PreRender" />
            <wasp:WLPToolBarButton Text="Details" onclick="Open();" CommandName="details" ResourceName="Details" OnPreRender="DetailsButton_PreRender" />
            <wasp:WLPToolBarButton Text="Delete" ImageUrl="~\App_Themes\Default\Delete.gif" CommandName="delete" ResourceName="Delete" OnPreRender="DeleteButton_PreRender" />
        </Items>
    </wasp:WLPToolBar>

    <wasp:WLPGrid
        ID="WsibGrid"
        runat="server"
        AllowPaging="true"
        PagerStyle-AlwaysVisible="true"
        PageSize="100"
        AllowSorting="true"
        GridLines="None"
        AutoAssignModifyProperties="true">

        <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="false">
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
            <ClientEvents OnRowDblClick="OnRowDblClick" OnRowClick="OnRowClick" OnGridCreated="onGridCreated" OnCommand="onCommand" />
        </ClientSettings>

        <MasterTableView
            ClientDataKeyNames="WsibCode, EnglishDescription, FrenchDescription, WorkersCompensationAccountNumber, ExcludeFromRemittanceExportFlag, ImportExternalIdentifier"
            AutoGenerateColumns="false"
            DataKeyNames="WsibCode"
            CommandItemDisplay="Top"
            AllowSorting="false">

            <SortExpressions>
                <telerik:GridSortExpression FieldName="WsibCode" SortOrder="Ascending" />
            </SortExpressions>

            <CommandItemTemplate></CommandItemTemplate>

            <Columns>
                <wasp:GridBoundControl DataField="WsibCode" LabelText="**WsibCode**" SortExpression="WsibCode" UniqueName="WsibCode" ResourceName="WsibCode" />
                <wasp:GridBoundControl DataField="EnglishDescription" LabelText="**EnglishDescription**" SortExpression="EnglishDescription" UniqueName="EnglishDescription" ResourceName="EnglishDescription" />
                <wasp:GridBoundControl DataField="FrenchDescription" LabelText="**FrenchDescription**" SortExpression="FrenchDescription" UniqueName="FrenchDescription" ResourceName="FrenchDescription" />
                <wasp:GridBoundControl DataField="WorkersCompensationAccountNumber" LabelText="**WorkersCompensationAccountNumber**" SortExpression="WorkersCompensationAccountNumber" UniqueName="WorkersCompensationAccountNumber" ResourceName="WorkersCompensationAccountNumber" />
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="ProvinceStateCode" LabelText="**ProvinceStateCode**" Type="ProvinceStateCode" ResourceName="ProvinceStateCode" />
                <wasp:GridKeyValueControl OnNeedDataSource="PaycodeCode_NeedDataSource" DataField="PaycodeCode" LabelText="**PaycodeCode**" Type="PaycodeCode" ResourceName="PaycodeCode" />
                <wasp:GridCheckBoxControl DataField="ExcludeFromRemittanceExportFlag" LabelText="**ExcludeFromRemittanceExportFlag**" SortExpression="ExcludeFromRemittanceExportFlag" UniqueName="ExcludeFromRemittanceExportFlag" ResourceName="ExcludeFromRemittanceExportFlag">
                     <HeaderStyle Width="100px" />
                </wasp:GridCheckBoxControl>
                <wasp:GridBoundControl DataField="ImportExternalIdentifier" SortExpression="ImportExternalIdentifier" UniqueName="ImportExternalIdentifier" ResourceName="ImportExternalIdentifier" />
            </Columns>
        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true" />

    </wasp:WLPGrid>
</asp:Panel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="WsibWindows"
    Width="1000"
    Height="600"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientClose="onClientClose"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var wsibCode;
        var toolbar = null;

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function WsibToolBar_init(sender) {
            initializeControls();
        }

        function initializeControls() {
            if ($find('<%= WsibToolBar.ClientID %>') != null)
                toolbar = $find('<%= WsibToolBar.ClientID %>');

            enableButtons(getSelectedRow() != null);
        }

        function OnRowDblClick(sender, eventArgs) {
            var toolbar = $find('<%= WsibToolBar.ClientID %>');

            if (toolbar.findButtonByCommandName('details') != null)
                Open(sender);
        }

        function getSelectedRow() {
            var wsibGrid = $find('<%= WsibGrid.ClientID %>');
            if (wsibGrid == null)
                return null;

            var MasterTable = wsibGrid.get_masterTableView();
            if (MasterTable == null)
                return null;

            var selectedRow = MasterTable.get_selectedItems();
            if (selectedRow.length == 1)
                return selectedRow[0];
            else
                return null;
        }

        function onGridCreated(sender, eventArgs) {
            var selectedRow = getSelectedRow();
            if (selectedRow != null) {
                wsibCode = selectedRow.getDataKeyValue('WsibCode');
                enableButtons(true);
            }
            else
                wsibCode = null;
        }

        function onCommand(sender, args) {
            if (args.get_commandName() == 'Page') {
                wsibCode = null;
                initializeControls();
            }
        }

        function OnRowClick(sender, eventArgs) {
            wsibCode = eventArgs.getDataKeyValue('WsibCode');
            enableButtons(true);
        }

        function enableButtons(enable) {
            var detailsButton = toolbar.findButtonByCommandName('details');
            var deleteButton = toolbar.findButtonByCommandName('delete');

            if (detailsButton != null)
                detailsButton.set_enabled(enable);

            if (deleteButton != null)
                deleteButton.set_enabled(enable);
        }

        function Open() {
            if (wsibCode != null) {
                openWindowWithManager('WsibWindows', String.format('/Admin/Wsib/{0}/{1}', 'view', wsibCode), false);
                return false;
            }
        }

        function Add() {
            openWindowWithManager('WsibWindows', String.format('/Admin/Wsib/{0}/{1}', 'add', 'x'), false);
            return false;
        }

        function onClientClose(sender, eventArgs) {
            var arg = sender.argument;
            if (arg != null && arg.closeWithChanges)
                __doPostBack('<%= MainPanel.ClientID %>', 'refresh');
        }
    </script>
</telerik:RadScriptBlock>