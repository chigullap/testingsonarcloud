﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WsibEffectiveControl.ascx.cs" Inherits="WorkLinks.Admin.WsibAdmin.WsibEffectiveControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPGrid
        ID="WsibEffectiveGrid"
        runat="server"
        GridLines="None"
        AutoGenerateColumns="false"
        AutoAssignModifyProperties="true"
        OnInsertCommand="WsibEffectiveGrid_InsertCommand"
        OnUpdateCommand="WsibEffectiveGrid_UpdateCommand"
        OnDeleteCommand="WsibEffectiveGrid_DeleteCommand"
        OnItemDataBound="WsibEffectiveGrid_ItemDataBound"
        Style="margin-top: 17px">

        <ClientSettings EnablePostBackOnRowClick="false" AllowColumnsReorder="true" ReorderColumnsOnClient="true">
            <Selecting AllowRowSelect="false" />
            <Scrolling AllowScroll="false" UseStaticHeaders="True" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="EffectiveDate" SortOrder="Descending" />
            </SortExpressions>

            <CommandItemTemplate>
                <wasp:WLPToolBar ID="WsibEffectiveToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
                    <Items>
                        <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add"></wasp:WLPToolBarButton>
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif"></wasp:GridEditCommandControl>
                <wasp:GridDateTimeControl DataField="EffectiveDate" SortExpression="EffectiveDate" UniqueName="EffectiveDate" ResourceName="EffectiveDate"></wasp:GridDateTimeControl>
                <wasp:GridBoundControl DataField="WorkersCompensationRate" DataFormatString="{0:###,##0.0000}" SortExpression="WorkersCompensationRate" UniqueName="WorkersCompensationRate" ResourceName="WorkersCompensationRate"></wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="EstimatedValue" DataFormatString="{0:###,##0.00}" LabelText="*EstimatedValue" SortExpression="EstimatedValue" UniqueName="EstimatedValue" ResourceName="EstimatedValue"></wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="MaximumAssessableEarning" DataFormatString="{0:###,##0.00}" SortExpression="MaximumAssessableEarning" UniqueName="MaximumAssessableEarning" ResourceName="MaximumAssessableEarning"></wasp:GridBoundControl>
                <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>"></wasp:GridButtonControl>
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:DateControl ID="EffectiveDate" runat="server" ResourceName="EffectiveDate" Value='<%# Bind("EffectiveDate") %>' Mandatory="true" ReadOnly="<%# IsEditMode %>" TabIndex="010" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <wasp:NumericControl ID="WorkersCompensationRate" runat="server" MinValue="0" MaxValue="999.9999" DecimalDigits="4" ResourceName="WorkersCompensationRate" Value='<%# Bind("WorkersCompensationRate") %>' Mandatory="true" TabIndex="020" />
                                        </td>
                                    </tr>
                                     <tr>
                                        <td>
                                            <wasp:NumericControl ID="EstimatedValue" runat="server" MinValue="0" MaxValue="9999999.99" DecimalDigits="2" ResourceName="EstimatedValue" Value='<%# Bind("EstimatedValue") %>' TabIndex="025" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <wasp:NumericControl ID="MaximumAssessableEarning" runat="server" MinValue="0" MaxValue="9999999.99" DecimalDigits="2" Mandatory="true" ResourceName="MaximumAssessableEarning" Value='<%# Bind("MaximumAssessableEarning") %>' TabIndex="030" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsEditMode %>' ResourceName="Update" />
                                            <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsInsertMode %>' ResourceName="Insert" />
                                        </td>
                                        <td>
                                            <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="Cancel" CausesValidation="false" ResourceName="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true"></HeaderContextMenu>

    </wasp:WLPGrid>
</asp:Panel>