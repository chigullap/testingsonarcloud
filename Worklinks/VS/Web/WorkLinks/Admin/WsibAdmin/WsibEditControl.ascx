﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WsibEditControl.ascx.cs" Inherits="WorkLinks.Admin.WsibAdmin.WsibEditControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="WsibEffectiveControl.ascx" TagName="WsibEffectiveControl" TagPrefix="uc2" %>

<wasp:WLPFormView
    ID="WsibDetailsView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key">

    <ItemTemplate>
        <wasp:WLPToolBar ID="WsibDetailsToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
            <Items>
                <wasp:WLPToolBarButton Text="Edit" Visible='<%# UpdateFlag %>' ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="Edit" ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="WsibCode" ReadOnly="true" runat="server" ResourceName="WsibCode" Value='<%# Eval("WsibCode") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="ImportExternalIdentifier" ReadOnly="true" runat="server" ResourceName="ImportExternalIdentifier" Value='<%# Eval("ImportExternalIdentifier") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="EnglishDescription" ReadOnly="true" runat="server" ResourceName="EnglishDescription" Value='<%# Eval("EnglishDescription") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="FrenchDescription" ReadOnly="true" runat="server" ResourceName="FrenchDescription" Value='<%# Eval("FrenchDescription") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="WorkersCompensationAccountNumber" ReadOnly="true" runat="server" ResourceName="WorkersCompensationAccountNumber" Value='<%# Eval("WorkersCompensationAccountNumber") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="SortOrder" ReadOnly="true" runat="server" DecimalDigits="0" ResourceName="SortOrder" Value='<%# Eval("SortOrder") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="ProvinceStateCode" ReadOnly="true" runat="server" Type="ProvinceStateCode" OnDataBinding="ProvinceStateCode_NeedDataSource" ResourceName="ProvinceStateCode" Value='<%# Eval("ProvinceStateCode") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="PaycodeCode" ReadOnly="true" runat="server" Type="PaycodeCode" OnDataBinding="PaycodeCode_NeedDataSource" ResourceName="PaycodeCode" Value='<%# Eval("PaycodeCode") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ActiveFlag" ReadOnly="true" runat="server" ResourceName="ActiveFlag" Value='<%# Eval("ActiveFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="SystemFlag" ReadOnly="true" runat="server" ResourceName="SystemFlag" Value='<%# Eval("SystemFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ExcludeFromRemittanceExportFlag" ReadOnly="true" runat="server" ResourceName="ExcludeFromRemittanceExportFlag" Value='<%# Eval("ExcludeFromRemittanceExportFlag") %>' />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="GenerateChequeForRemittanceFlag" ReadOnly="true" runat="server" ResourceName="GenerateChequeForRemittanceFlag" Value='<%# Eval("GenerateChequeForRemittanceFlag") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="CodeWcbChequeRemittanceFrequencyCd" ReadOnly="true" runat="server" Type="WcbChequeRemittanceFrequencyCode" OnDataBinding="CodeWcbChequeRemittanceFrequencyCode_NeedDataSource" ResourceName="CodeWcbChequeRemittanceFrequency" Value='<%# Eval("CodeWcbChequeRemittanceFrequencyCd") %>' />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="VendorId" runat="server" OnDataBinding="VendorCombo_NeedDataSource" ResourceName="VendorId" Value='<%# Eval("VendorId") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                     <td>
                        <wasp:ComboBoxControl ID="BusinessNumberId" runat="server" OnDataBinding="BusinessNumberId_NeedDataSource" ResourceName="BusinessNumberId" Value='<%# Eval("BusinessNumberId") %>' ReadOnly="true" />
                    </td>
                       <td>
                        <wasp:CheckBoxControl ID="HideInEmploymentScreenFlag" ReadOnly="true" runat="server" ResourceName="HideInEmploymentScreenFlag" Value='<%# Eval("HideInEmploymentScreenFlag") %>' />
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar ID="WsibToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
            <Items>
                <wasp:WLPToolBarButton ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode  %>' ResourceName="Insert" CommandName="insert"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" ResourceName="Update" Visible='<%# IsEditMode %>'></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" Visible='<%# !IsInsertMode  %>' CommandName="cancel" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" Visible='<%# IsInsertMode  %>' onClick="Close()" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="WsibCode" ReadOnly='<%# IsEditMode || IsViewMode %>' runat="server" ResourceName="WsibCode" Value='<%# Bind("WsibCode") %>' Mandatory="true" TabIndex="010" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="ImportExternalIdentifier" runat="server" ResourceName="ImportExternalIdentifier" Value='<%# Bind("ImportExternalIdentifier") %>' TabIndex="020" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="EnglishDescription" runat="server" ResourceName="EnglishDescription" Value='<%# Bind("EnglishDescription") %>' Mandatory="true" TabIndex="030" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="FrenchDescription" runat="server" ResourceName="FrenchDescription" Value='<%# Bind("FrenchDescription") %>' Mandatory="true" TabIndex="040" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="WorkersCompensationAccountNumber" runat="server" ResourceName="WorkersCompensationAccountNumber" Value='<%# Bind("WorkersCompensationAccountNumber") %>' Mandatory="true" TabIndex="050" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="SortOrder" runat="server" DecimalDigits="0" ResourceName="SortOrder" Value='<%# Bind("SortOrder") %>' Mandatory="true" TabIndex="060" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="ProvinceStateCode" runat="server" Type="ProvinceStateCode" OnDataBinding="ProvinceStateCode_NeedDataSource" ResourceName="ProvinceStateCode" Value='<%# Bind("ProvinceStateCode") %>' Mandatory="true" TabIndex="070" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="PaycodeCode" runat="server" Type="PaycodeCode" OnDataBinding="PaycodeCode_NeedDataSource" ResourceName="PaycodeCode" Value='<%# Bind("PaycodeCode") %>' Mandatory="true" TabIndex="080" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ActiveFlag" runat="server" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>' TabIndex="090" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="SystemFlag" runat="server" ResourceName="SystemFlag" Value='<%# Bind("SystemFlag") %>' TabIndex="100" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ExcludeFromRemittanceExportFlag" runat="server" ResourceName="ExcludeFromRemittanceExportFlag" Value='<%# Bind("ExcludeFromRemittanceExportFlag") %>' TabIndex="110" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="GenerateChequeForRemittanceFlag" OnCheckedChanged="GenerateChequeForRemittanceFlag_CheckedChanged" AutoPostBack="true" runat="server" ResourceName="GenerateChequeForRemittanceFlag" Value='<%# Bind("GenerateChequeForRemittanceFlag") %>' TabIndex="120" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="CodeWcbChequeRemittanceFrequencyCd" runat="server" Type="WcbChequeRemittanceFrequencyCode" OnDataBinding="CodeWcbChequeRemittanceFrequencyCode_NeedDataSource" ResourceName="CodeWcbChequeRemittanceFrequency" Value='<%# Bind("CodeWcbChequeRemittanceFrequencyCd") %>' TabIndex="130" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="VendorId" runat="server" OnDataBinding="VendorCombo_NeedDataSource" ResourceName="VendorId" Value='<%# Bind("VendorId") %>' TabIndex="140" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="BusinessNumberId" runat="server" OnDataBinding="BusinessNumberId_NeedDataSource" ResourceName="BusinessNumberId" Value='<%# Bind("BusinessNumberId") %>' TabIndex="150" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="HideInEmploymentScreenFlag" runat="server" ResourceName="HideInEmploymentScreenFlag" Value='<%# Bind("HideInEmploymentScreenFlag") %>' TabIndex="160" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>
</wasp:WLPFormView>

<uc2:WsibEffectiveControl ID="WsibEffectiveControl" runat="server" />

<telerik:RadScriptBlock runat="server">
    <script type="text/javascript">
        function processClick(commandName) {
            var arg = new Object;
            arg.closeWithChanges = true;
            getRadWindow().argument = arg;
        }

        function getRadWindow() {
            var popup = null;

            if (window.radWindow)
                popup = window.radWindow;
            else if (window.frameElement.radWindow)
                popup = window.frameElement.radWindow;

            return popup;
        }

        function Close(button, args) {
            var popup = getRadWindow();
            setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
        }

        function ToolBarClick(sender, args) {
            var button = args.get_item();
            var commandName = button.get_commandName();
            processClick(commandName);

            if (<%= IsInsertMode.ToString().ToLower() %>) {
                if (commandName=="cancel") {
                    window.close();
                }
            }
        }
    </script>
</telerik:RadScriptBlock>