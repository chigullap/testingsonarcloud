﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.WsibAdmin
{
    public partial class WsibControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public bool IsViewMode { get { return WsibGrid.IsViewMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.Wsib.AddFlag; } }
        public bool ViewFlag { get { return Common.Security.RoleForm.Wsib.ViewFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.Wsib.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.Wsib.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.Wsib.ViewFlag);

            WireEvents();

            if (!IsPostBack)
                LoadWsibInformation();

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.StartsWith("refresh"))
            {
                LoadWsibInformation();
                WsibGrid.Rebind();
            }
        }
        protected void WireEvents()
        {
            WsibGrid.NeedDataSource += new GridNeedDataSourceEventHandler(WsibGrid_NeedDataSource);
        }
        protected void LoadWsibInformation()
        {
            Data = CodeWsibCollection.ConvertCollection(Common.ServiceWrapper.CodeClient.GetCodeWsib(null));
        }
        #endregion

        #region event handlers
        void WsibGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            WsibGrid.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void PaycodeCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateComboBoxWithPaycodesByType((ICodeControl)sender, "6");
        }
        protected void ProvinceStateCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateProvince((ICodeControl)sender);
        }
        #endregion

        #region handle updates
        protected void WsibToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("delete") && WsibGrid.SelectedValue != null)
                DeleteWsib((CodeWsib)Data[WsibGrid.SelectedValue.ToString()]);
        }
        protected void DeleteWsib(CodeWsib wsibCode)
        {
            try
            {
                Common.ServiceWrapper.CodeClient.DeleteCodeWsib(wsibCode);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    WsibGrid.SelectedIndexes.Clear();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            LoadWsibInformation();
            WsibGrid.Rebind();
        }
        #endregion

        #region security handlers
        protected void AddButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && AddFlag; }
        protected void DetailsButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && ViewFlag; }
        protected void DeleteButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && DeleteFlag; }
        #endregion
    }
}