﻿using System;
using System.Web.UI.WebControls;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.WsibAdmin
{
    public partial class WsibEditControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public CodeWsib Wsib
        {
            get
            {
                if (Data.Count > 0)
                    return (CodeWsib)Data[0];
                else
                    return null;
            }
        }
        private string Action { get { return Page.RouteData.Values["action"].ToString().ToLower(); } }
        private string CodeWsibCd { get { return Convert.ToString(Page.RouteData.Values["wsibCode"]); } }
        public bool PreAuthorizedDebitFlag { get { return Common.ApplicationParameter.PreAuthorizedDebitFlag; } }
        public bool IsEditMode { get { return WsibDetailsView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool IsInsertMode { get { return WsibDetailsView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsViewMode { get { return WsibDetailsView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool AddFlag { get { return Common.Security.RoleForm.Wsib.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.Wsib.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.Wsib.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.Wsib.ViewFlag);

            WireEvents();

            if (Action == "view")
            {
                Page.Title = "Edit a WCB Code";

                if (!IsPostBack)
                {
                    LoadWsibInformation();
                    LoadWsibEffective();
                    Initialize();
                }
            }
            else if (Action == "add")
            {
                if (!IsPostBack)
                {
                    Page.Title = "Add a WCB Code";

                    CodeWsibCollection newCollection = new CodeWsibCollection();
                    newCollection.AddNew();
                    Data = newCollection;

                    //change formview's mode to Insert mode
                    WsibDetailsView.ChangeMode(FormViewMode.Insert);

                    //databind
                    WsibDetailsView.DataBind();
                }
                else if (IsInsertMode)
                {
                    //do nothing
                }
                else
                {
                    //we are loading the edit page after an add
                    Page.Title = "Edit a WCB Code";

                    LoadWsibEffective();
                    Initialize();
                }
            }
        }
        protected void WireEvents()
        {
            WsibDetailsView.NeedDataSource += new WLPFormView.NeedDataSourceEventHandler(WsibDetailsView_NeedDataSource);
            WsibDetailsView.Updating += WsibDetailsView_Updating;
            WsibDetailsView.Inserting += WsibDetailsView_Inserting;
        }
        protected void Initialize()
        {
            //find the WsibEffectiveGrid
            WLPGrid grid = (WLPGrid)WsibEffectiveControl.FindControl("WsibEffectiveGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        protected void LoadWsibInformation()
        {
            //create business object and fill it with the data passed to the page, then bind the grid
            CodeWsibCollection collection = new CodeWsibCollection();
            collection.Add(LoadDataIntoBusinessClass());

            Data = collection;
            WsibDetailsView.DataBind();
        }
        protected void LoadWsibEffective()
        {
            WsibEffectiveControl.Wsib = Wsib.WsibCode;
            WsibEffectiveControl.LoadWsibEffectiveInformation(Wsib.WsibCode);
        }
        protected CodeWsib LoadDataIntoBusinessClass()
        {
            CodeWsibCollection temp = new CodeWsibCollection();
            temp.Load(Common.ServiceWrapper.CodeClient.GetCodeWsib(CodeWsibCd));

            return temp[0]; //will only ever have 1 item in this collection
        }
        #endregion

        #region event handlers
        void WsibDetailsView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            WsibDetailsView.DataSource = Data;
        }
        protected void ProvinceStateCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateProvince((ICodeControl)sender);
        }
        protected void PaycodeCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateComboBoxWithPaycodesByType((ComboBoxControl)sender, "6");
        }
        protected void CodeWcbChequeRemittanceFrequencyCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);

            CheckBoxControl generateChequeFlag = (CheckBoxControl)WsibDetailsView.FindControl("GenerateChequeForRemittanceFlag");
            ComboBoxControl remitFrequencyCombo = (ComboBoxControl)sender;

            if (generateChequeFlag != null && remitFrequencyCombo != null)
                showHideCombo(remitFrequencyCombo, generateChequeFlag.Checked, false);
        }
        protected void VendorCombo_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateVendorComboControl((ICodeControl)sender);

            CheckBoxControl generateChequeFlag = (CheckBoxControl)WsibDetailsView.FindControl("GenerateChequeForRemittanceFlag");
            ComboBoxControl vendorCombo = (ComboBoxControl)sender;

            if (generateChequeFlag != null && vendorCombo != null)
                showHideCombo(vendorCombo, generateChequeFlag.Checked, false);
        }
        protected void BusinessNumberId_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateBusinessNumber((ICodeControl)sender);

            CheckBoxControl generateChequeFlag = (CheckBoxControl)WsibDetailsView.FindControl("GenerateChequeForRemittanceFlag");
            ComboBoxControl businessNumberCombo = (ComboBoxControl)sender;

            if (generateChequeFlag != null && businessNumberCombo != null)
                showHideCombo(businessNumberCombo, generateChequeFlag.Checked, false);
        }
        protected void GenerateChequeForRemittanceFlag_CheckedChanged(object sender, EventArgs e)
        {
            //if checked, the CodeWcbChequeRemittanceFrequencyCd, VendorId and BusinessNumberId combos will be enabled
            ComboBoxControl remitFrequencyCombo = (ComboBoxControl)WsibDetailsView.FindControl("CodeWcbChequeRemittanceFrequencyCd");
            ComboBoxControl vendorCombo = (ComboBoxControl)WsibDetailsView.FindControl("VendorId");
            ComboBoxControl businessNumberCombo = (ComboBoxControl)WsibDetailsView.FindControl("BusinessNumberId");

            if (remitFrequencyCombo != null)
                showHideCombo(remitFrequencyCombo, ((CheckBoxControl)sender).Checked, true);

            if (vendorCombo != null)
                showHideCombo(vendorCombo, ((CheckBoxControl)sender).Checked, true);

            if (businessNumberCombo != null)
                showHideCombo(businessNumberCombo, ((CheckBoxControl)sender).Checked, true);
        }
        protected void showHideCombo(ComboBoxControl comboBox, bool isChecked, bool includeValue)
        {
            if (comboBox.ResourceName.Equals("BusinessNumberId")) //client must also be a PAD client to show/hide the business number
                isChecked = isChecked && PreAuthorizedDebitFlag;

            comboBox.Visible = isChecked;
            comboBox.Mandatory = isChecked;

            if (includeValue)
                comboBox.Value = isChecked ? comboBox.Value : null;
        }
        #endregion

        #region handle updates
        void WsibDetailsView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            CodeWsib item = (CodeWsib)e.DataItem;
            Common.ServiceWrapper.CodeClient.InsertCodeWsib(item).CopyTo((CodeWsib)Data[item.Key]);
        }
        void WsibDetailsView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                CodeWsib item = (CodeWsib)e.DataItem;
                Common.ServiceWrapper.CodeClient.UpdateCodeWsib(item).CopyTo((CodeWsib)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, GetType(), "PopupScript", string.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}