﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserAdminPersonControl.ascx.cs" Inherits="WorkLinks.Admin.UserAdmin.UserAdminPersonControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Src="../../Payroll/PayrollBatch/EmployeeControl.ascx" TagName="EmployeeControl" TagPrefix="uc1" %>

<wasp:WLPFormView
    ID="UserPersonDetailView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key"
    OnNeedDataSource="UserPersonDetailView_NeedDataSource"
    OnUpdating="UserPersonDetailView_Updating">

    <ItemTemplate>
        <wasp:WLPToolBar ID="UserPersonDetailToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Text="Edit" ImageUrl="~/App_Themes/Default/Edit.gif" Visible='<%# UpdateFlag %>' CommandName="edit" ResourceName="Edit" CausesValidation="false"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="IsEmployee" LabelText="Is Employee" runat="server" ResourceName="IsEmployee" Value='<%# Eval("IsEmployee") %>' ReadOnly="true"></wasp:CheckBoxControl>
                    </td>
                    <td>
                        <wasp:WLPKeyValue Width="180px" ID="EmployeeId" runat="server" OnDataBinding="EmployeeId_DataBinding" ResourceName="EmployeeId" Value='<%# Eval("EmployeeId") %>' ReadOnly="true"></wasp:WLPKeyValue>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="WorklinksAdministrationFlag" LabelText="WorkLinks Admin" runat="server" ResourceName="WorklinksAdministrationFlag" Value='<%# Eval("WorklinksAdministrationFlag") %>' ReadOnly="true"></wasp:CheckBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="FirstName" runat="server" LabelText="First Name" ResourceName="FirstName" Value='<%# Eval("FirstName") %>' ReadOnly="true"></wasp:TextBoxControl>
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="LastName" runat="server" LabelText="Last Name" ResourceName="LastName" Value='<%# Eval("LastName") %>' ReadOnly="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="UserName" runat="server" LabelText="User Name" ResourceName="UserName" Value='<%# Eval("UserName") %>' ReadOnly="true"></wasp:TextBoxControl>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="IsLockedOutFlag" LabelText="Locked Out" runat="server" ResourceName="IsLockedOut" Value='<%# Eval("IsLockedOutFlag") %>' ReadOnly="true"></wasp:CheckBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label Text="Signature:" runat="server" />
                        <telerik:RadBinaryImage runat="server" ID="Signature" DataValue='<%# Eval("Signature") %>' AutoAdjustImageControlSize="false" Width="200px" Height="100px" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar ID="PayrollBatchToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
            <Items>
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" ValidationGroup="UserValidationGroup" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" ValidationGroup="UserValidationGroup" Visible='<%# IsInsertMode  %>' CommandName="insert" ResourceName="Insert" />
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" Visible='<%# !IsInsertMode %>' CommandName="cancel" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" Visible='<%# IsInsertMode %>' CommandName="cancel" onclick="Close()" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="IsEmployee" Checked="true" LabelText="Is Employee" OnCheckedChanged="IsEmployee_CheckedChanged" AutoPostBack="true" runat="server" ResourceName="IsEmployee" Value='<%# Bind("IsEmployee") %>' ReadOnly='<%# !IsInsertMode %>' TabIndex="010"></wasp:CheckBoxControl>
                    </td>
                    <td>
                        <uc1:EmployeeControl ID="EmployeeControl1" runat="server" OnClientEmployeeChanged="setEmailDefaultOnAddNew(emailControl.value);" Value='<%# Bind("EmployeeId") %>' Visible='<%# IsEmployeeChecked %>' TabIndex="020" Mandatory="true"></uc1:EmployeeControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="WorklinksAdministrationFlag" LabelText="WorkLinks Admin" runat="server" ResourceName="WorklinksAdministrationFlag" Value='<%# Bind("WorklinksAdministrationFlag") %>' ReadOnly="<%# IsUserEditingTheirOwnInfo %>" TabIndex="080"></wasp:CheckBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="FirstName" runat="server" LabelText="First Name" ResourceName="FirstName" Value='<%# Bind("FirstName") %>' ReadOnly='<%# IsEmployeeProperty %>' TabIndex="030"></wasp:TextBoxControl>
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="LastName" runat="server" LabelText="Last Name" ResourceName="LastName" Value='<%# Bind("LastName") %>' ReadOnly='<%# IsEmployeeProperty %>' TabIndex="040"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="UserName" runat="server" LabelText="User Name" ResourceName="UserName" Value='<%# Bind("UserName") %>' ReadOnly='<%# !IsInsertMode %>' TabIndex="050"></wasp:TextBoxControl>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ResetPassword" LabelText="Reset Password" onPreRender="ResetPassword_PreRender" OnCheckedChanged="IsResetPassword_CheckedChanged" AutoPostBack="true" runat="server" ResourceName="ResetPassword" Value='<%# IsResetPasswordChecked %>' ReadOnly='<%# IsInsertMode %>' TabIndex="070"></wasp:CheckBoxControl>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="IsLockedOutFlag" LabelText="Locked Out" runat="server" ResourceName="IsLockedOut" Value='<%# Bind("IsLockedOutFlag") %>' Visible='<%# !IsInsertMode %>' ReadOnly="false" TabIndex="080"></wasp:CheckBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PasswordEntry" runat="server" LabelText="Password" ResourceName="Password" Visible='<%# IsResetPasswordChecked %>' Value='<%# Bind("PasswordEntry") %>' ReadOnly="false" TabIndex="090"></wasp:TextBoxControl>
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="ConfirmPasswordEntry" runat="server" LabelText="Confirm Password" ResourceName="PasswordConfirm" Visible='<%# IsResetPasswordChecked %>' Value='<%# Bind("ConfirmPasswordEntry") %>' ReadOnly="false" TabIndex="100"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label Text="Signature:" runat="server" />
                        <telerik:RadBinaryImage runat="server" ID="RadBinaryImage1" DataValue='<%# Eval("Signature") %>' AutoAdjustImageControlSize="false" Width="200px" Height="100px" Style="margin-bottom: 10px" />
                        <telerik:RadAsyncUpload runat="server" ID="UploadAttachment" MaxFileInputsCount="1" HideFileInput="true" OnFileUploaded="UploadAttachment_FileUploaded" />
                        <wasp:WLPButton runat="server" ID="RemoveAttachment" ResourceName="RemoveAttachment" OnClick="RemoveAttachment_Click" CausesValidation="false" />
                    </td>
                </tr>
            </table>
            <wasp:WLPLabel ID="Message" runat="server" CssClass="failureNotification" Text='<%# DupeUserMsg %>' />
            <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="PasswordEntry" ControlToValidate="ConfirmPasswordEntry" CssClass="failureNotification" Display="Dynamic" ErrorMessage="* The New Password and the Confirm Password entries must match." ValidationGroup="UserValidationGroup"></asp:CompareValidator>
            <asp:RequiredFieldValidator ID="PasswordEntryValidator" CssClass="failureNotification" Display="Dynamic" ControlToValidate="PasswordEntry" ValidationGroup="UserValidationGroup" ErrorMessage="* Enter a password.  " runat="Server" Enabled='<%# IsResetPasswordChecked %>'></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="ConfirmPasswordEntryValidator" CssClass="failureNotification" Display="Dynamic" ControlToValidate="ConfirmPasswordEntry" ValidationGroup="UserValidationGroup" ErrorMessage="* Enter a confirmation password.  " runat="Server" Enabled='<%# IsResetPasswordChecked %>'></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="UserNameValidator" CssClass="failureNotification" Display="Dynamic" ControlToValidate="UserName" ValidationGroup="UserValidationGroup" ErrorMessage="* Enter a user name.  " runat="Server"></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="UserNameInUseValidator" runat="server" ErrorMessage="**Username in use**" CssClass="failureNotification" Display="Dynamic" OnServerValidate="UsernameValidator_ServerValidate" ValidationGroup="UserValidationGroup">* Username in use</asp:CustomValidator>
            <asp:CustomValidator ID="EmailValidator" runat="server" ErrorMessage="<%$ Resources:ErrorMessages, MissingEmployeeEmail %>" CssClass="failureNotification" Display="Dynamic" OnServerValidate="EmailValidator_ServerValidate" ValidationGroup="UserValidationGroup"></asp:CustomValidator>
        </fieldset>
    </EditItemTemplate>

</wasp:WLPFormView>

Groups:
<hr />
<wasp:WLPGrid
    ID="GroupGrid"
    EditMode="InPlace"
    runat="server"
    GridLines="None"
    AutoGenerateColumns="false"
    Width="100%"
    AutoInsertOnAdd="false"
    AllowMultiRowEdit="true"
    Skin="">

    <ClientSettings>
        <Selecting AllowRowSelect="true" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key" Width="100%">
        <SortExpressions>
            <telerik:GridSortExpression FieldName="IsGroupSelected" SortOrder="Descending" />
        </SortExpressions>

        <CommandItemTemplate></CommandItemTemplate>

        <Columns>
            <wasp:GridTemplateControl UniqueName="HeaderColumns">
                <ItemTemplate>
                    <table width="100%">
                        <tr>
                            <td colspan="2">
                                <wasp:CheckBoxControl ID="IsGroupSelected" LabelText="Selected" runat="server" ResourceName="IsGroupSelected" Value='<%# Bind("IsGroupSelected") %>' ReadOnly="true"></wasp:CheckBoxControl>
                            </td>
                            <% if (LanguageCode.ToLower().Equals("en"))
                                {%>
                            <td>
                                <wasp:TextBoxControl ID="EnglishDesc" LabelText="Description" runat="server" ResourceName="EnglishDesc" Value='<%# Bind("EnglishDesc") %>' ReadOnly="true"></wasp:TextBoxControl>
                            </td>
                            <%}
                                else
                                {%>
                            <td>
                                <wasp:TextBoxControl ID="FrenchDesc" LabelText="Description" runat="server" ResourceName="FrenchDesc" Value='<%# Bind("FrenchDesc") %>' ReadOnly="true"></wasp:TextBoxControl>
                            </td>
                            <%} %>
                        </tr>
                    </table>
                    <hr />
                </ItemTemplate>
                <EditItemTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:CheckBoxControl ID="IsGroupSelected" LabelText="Selected" runat="server" ResourceName="IsGroupSelected" Value='<%# Bind("IsGroupSelected") %>' ReadOnly="false" TabIndex="110"></wasp:CheckBoxControl>
                            </td>
                            <% if (LanguageCode.ToLower().Equals("en"))
                                {%>
                            <td>
                                <wasp:TextBoxControl ID="EnglishDesc" LabelText="English Desc" runat="server" ResourceName="EnglishDesc" Value='<%# Bind("EnglishDesc") %>' ReadOnly="true"></wasp:TextBoxControl>
                            </td>
                            <%}
                                else
                                {%>
                            <td>
                                <wasp:TextBoxControl ID="FrenchDesc" LabelText="French Desc" runat="server" ResourceName="FrenchDesc" Value='<%# Bind("FrenchDesc") %>' ReadOnly="true"></wasp:TextBoxControl>
                            </td>
                            <%} %>
                        </tr>
                    </table>
                    <hr />
                </EditItemTemplate>
            </wasp:GridTemplateControl>
        </Columns>
    </MasterTableView>

    <HeaderContextMenu EnableAutoScroll="true" />

</wasp:WLPGrid>

<script type="text/javascript">

    function Close(button, args) {
        var popup = getRadWindow();
        setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
    }

    function processClick(commandName) {
        var arg = new Object;
        arg.closeWithChanges = true;
        getRadWindow().argument = arg;
    }

    function getRadWindow() {
        var popup = null;

        if (window.radWindow)
            popup = window.radWindow;
        else if (window.frameElement.radWindow)
            popup = window.frameElement.radWindow;

        return popup;
    }

    function ToolBarClick(sender, args) {
        var button = args.get_item();
        var commandName = button.get_commandName();

        if (commandName != "cancel") {
            processClick(commandName);
        }

        if (<%= IsInsertMode.ToString().ToLower() %> && commandName == "cancel") {
            window.close();
        }
    }

    function setEmailDefaultOnAddNew(email) {
        
        var usernameControl = document.getElementById('<%= UserPersonDetailView.FindControl("UserName").ClientID %>');
        if (usernameControl != null) {
            var usernameControlValue = document.getElementById(usernameControl.attributes['fieldClientId'].value);

            usernameControlValue.value = email;
        }
    }

   
</script>