﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserAdminSearchControl.ascx.cs" Inherits="WorkLinks.Admin.UserAdmin.UserAdminSearchControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <table width="100%">
        <tr valign="top">
            <td>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="LastName" runat="server" ResourceName="LastName" TabIndex="010" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="FirstName" runat="server" ResourceName="FirstName" TabIndex="020" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="UserName" runat="server" ResourceName="UserName" TabIndex="030" />
                            </td>
                            <td>
                                <wasp:CheckBoxControl ID="IsLockedOutFlag" runat="server" ResourceName="IsLockedOutFlag" TabIndex="040" />
                            </td>
                        </tr>
                    </table>
                    <div class="SearchCriteriaButtons">
                        <wasp:WLPButton ID="btnClear" Icon-PrimaryIconUrl="~/App_Themes/Default/clear.gif" ResourceName="btnClear" runat="server" Text="Clear" OnClientClicked="clear" OnClick="btnClear_Click" CssClass="button" />
                        <wasp:WLPButton ID="btnSearch" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" ResourceName="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                    </div>
                </asp:Panel>
            </td>
        </tr>
        <tr valign="top">
            <td>
                <telerik:RadToolBar ID="UserSummaryToolBar" runat="server" Width="100%" OnClientLoad="UserSummaryToolBar_init">
                    <Items>
                        <wasp:WLPToolBarButton OnPreRender="AddToolBar_PreRender" Text="Add" CssClass="" ImageUrl="~/App_Themes/Default/Add.gif" onclick="Add()" CommandName="add" ResourceName="Add" />
                        <wasp:WLPToolBarButton OnPreRender="Delete_PreRender" Text="Delete" ImageUrl="~\App_Themes\Default\Delete.gif" onclick="Delete()" CommandName="Delete" ResourceName="Delete" />
                        <wasp:WLPToolBarButton OnPreRender="DetailsToolBar_PreRender" Text="Details" ImageUrl="" onclick="Open()" CommandName="details" ResourceName="Details" />
                        <wasp:WLPToolBarButton OnPreRender="UserAccessInformation_PreRender" Text="UserAccessInformation" onclick="openUserAccessInformation();" runat="server" CommandName="UserAccessInformation" ResourceName="UserAccessInformation" />
                    </Items>
                </telerik:RadToolBar>

                <wasp:WLPGrid
                    ID="UserSummaryGrid"
                    runat="server"
                    AllowPaging="true"
                    PagerStyle-AlwaysVisible="true"
                    PageSize="100"
                    Height="400px"
                    AllowSorting="true"
                    GridLines="None"
                    AutoAssignModifyProperties="true">

                    <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                        <Selecting AllowRowSelect="true" />
                        <ClientEvents OnRowDblClick="OnRowDblClick" OnRowClick="OnRowClick" OnCommand="onCommand" />
                    </ClientSettings>

                    <MasterTableView
                        ClientDataKeyNames="PersonId, LastName"
                        AutoGenerateColumns="false"
                        CommandItemDisplay="Top">

                        <CommandItemTemplate></CommandItemTemplate>

                        <Columns>
                            <wasp:GridBoundControl DataField="UserName" LabelText="**UserName**" SortExpression="UserName" UniqueName="UserName" ResourceName="UserName" />
                            <wasp:GridBoundControl DataField="LastName" LabelText="**LastName**" SortExpression="LastName" UniqueName="LastName" ResourceName="LastName" />
                            <wasp:GridBoundControl DataField="FirstName" LabelText="**FirstName**" SortExpression="FirstName" UniqueName="FirstName" ResourceName="FirstName" />
                            <wasp:GridDateTimeControl DataField="LastLoginDatetime" LabelText="**LastLogin**" SortExpression="LastLoginDatetime" UniqueName="LastLoginDatetime" ResourceName="LastLoginDatetime">
                                <HeaderStyle Width="120px" />
                            </wasp:GridDateTimeControl>
                            <wasp:GridDateTimeControl DataField="LoginExpiryDatetime" LabelText="**LoginExpiry**" SortExpression="LoginExpiryDatetime" UniqueName="LoginExpiryDatetime" ResourceName="LoginExpiryDatetime">
                                <HeaderStyle Width="120px" />
                            </wasp:GridDateTimeControl>
                            <wasp:GridCheckBoxControl DataField="IsLockedOutFlag" LabelText="**IsLockedOut**" SortExpression="IsLockedOutFlag" UniqueName="IsLockedOutFlag" ResourceName="IsLockedOutFlag" />
                            <wasp:GridDateTimeControl DataField="LastLockoutDatetime" LabelText="**LastLockout**" SortExpression="LastLockoutDatetime" UniqueName="LastLockoutDatetime" ResourceName="LastLockoutDatetime">
                                <HeaderStyle Width="120px" />
                            </wasp:GridDateTimeControl>
                            <wasp:GridDateTimeControl DataField="LastPasswordChangedDatetime" LabelText="**LastPasswordChanged**" SortExpression="LastPasswordChangedDatetime" UniqueName="LastPasswordChangedDatetime" ResourceName="LastPasswordChangedDatetime">
                                <HeaderStyle Width="120px" />
                            </wasp:GridDateTimeControl>
                            <wasp:GridDateTimeControl DataField="PasswordExpiryDatetime" LabelText="**PasswordExpiry**" SortExpression="PasswordExpiryDatetime" UniqueName="PasswordExpiryDatetime" ResourceName="PasswordExpiryDatetime">
                                <HeaderStyle Width="120px" />
                            </wasp:GridDateTimeControl>
                            <wasp:GridDateTimeControl DataField="LastLogoutDatetime" LabelText="**LastLogout**" SortExpression="LastLogoutDatetime" UniqueName="LastLogoutDatetime" ResourceName="LastLogoutDatetime">
                                <HeaderStyle Width="120px" />
                            </wasp:GridDateTimeControl>
                        </Columns>

                    </MasterTableView>

                    <HeaderContextMenu EnableAutoScroll="true" />
                </wasp:WLPGrid>
            </td>
        </tr>
    </table>
</asp:Panel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="UserAdminWindow"
    Width="1000"
    Height="600"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize,Minimize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientClose="onClientClose"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var personId;
        var lastName;
        var toolbar;

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function clear() {
            personId = null;
        }

        function UserSummaryToolBar_init(sender) {
            initializeControls();
        }

        function initializeControls() {
            toolbar = $find('<%= UserSummaryToolBar.ClientID %>');

            addButton = toolbar.findButtonByCommandName('add');
            detailsButton = toolbar.findButtonByCommandName('details');
            deleteButton = toolbar.findButtonByCommandName('Delete');

            if (detailsButton != null)
                detailsButton.disable();

            if (deleteButton != null)
                deleteButton.disable();

            personId = null;
            lastName = "";
        }

        function OnRowDblClick(sender, eventArgs) {
            if (toolbar.findButtonByCommandName('details')!=null)
                Open(sender);
        }

        function OnRowClick(sender, eventArgs) {
            personId = eventArgs.getDataKeyValue('PersonId');
            lastName = eventArgs.getDataKeyValue('LastName');

            rowSelected = true;

            if (detailsButton != null)
                detailsButton.enable();

            if (deleteButton != null)
                deleteButton.enable();
        }

        function Open() {
            if (personId != null) {
                openWindowWithManager('UserAdminWindow', String.format('/Admin/UserAdmin/View/{0}', personId), false);
            }
        }

        function openUserAccessInformation() {
            openWindowWithManager('UserAdminWindow', String.format('/Report/UserAccessInformation/{0}/{1}/{2}/{3}/{4}/{5}/{6}', -1, -1, 'PDF', 'x', 'x', 'x', 'x'), true);
            return false;
        }

        function Add() {
            openWindowWithManager('UserAdminWindow', String.format('/Admin/UserAdmin/Add/{0}', -1), false);
        }

        function Delete() {
            if (personId != null) {
                var message = "<asp:Literal runat="server" Text="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />";
                if (confirm(message))
                    __doPostBack('<%= this.ClientID %>', String.format('deletePersonId={0}', personId));
            }
        }

        function RowSelected() {
            return rowSelected;
        }

        function userDeleteMessage(){
            var message = "<asp:Literal runat="server" Text="<%$ Resources:ErrorMessages, UserDeleteFailed %>" />";
            alert(message);
        }

        function onCommand(sender, args) {
            if (args.get_commandName() == 'Page') {
                personId = null;
                initializeControls();
            }
        }
        
        function onClientClose(sender, eventArgs) {
            var arg = sender.argument;
            if (arg != null && arg.closeWithChanges)
                __doPostBack('<%= MainPanel.ClientID %>', 'refresh');
        }
    </script>
</telerik:RadScriptBlock>