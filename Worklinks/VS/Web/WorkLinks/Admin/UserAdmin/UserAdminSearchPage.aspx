﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserAdminSearchPage.aspx.cs" Inherits="WorkLinks.Admin.UserAdmin.UserAdminSearchPage" %>
<%@ Register src="UserAdminSearchControl.ascx" tagname="UserAdminSearchControl" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate> 
        <uc1:UserAdminSearchControl ID="UserAdminSearchControl1" runat="server" />        
    </ContentTemplate>
</asp:Content>