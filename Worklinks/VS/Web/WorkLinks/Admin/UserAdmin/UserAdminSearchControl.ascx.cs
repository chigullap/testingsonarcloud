﻿using System;
using System.Security.Claims;
using System.Web;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.UserAdmin
{
    public partial class UserAdminSearchControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected UserAdminSearchCriteria Criteria
        {
            get
            {
                UserAdminSearchCriteria criteria = new UserAdminSearchCriteria();
                criteria.LastName = LastName.TextValue;
                criteria.FirstName = FirstName.TextValue;
                criteria.UserName = UserName.TextValue;
                criteria.IsLockedOutFlag = IsLockedOutFlag.Checked;
//                criteria.WorklinksAdministrationFlag = ((ClaimsIdentity)System.Web.HttpContext.Current.User.Identity).FindFirst("uname") !=null;
                //!Common.ServiceWrapper.SecurityClient.GetSecurityUser(HttpContext.Current.User.Identity.Name.ToString())[0].WorklinksAdministrationFlag;
                //criteria.WorklinksAdministrationFlag = !Common.ServiceWrapper.SecurityClient.GetSecurityUser(((ClaimsIdentity)System.Web.HttpContext.Current.User.Identity).FindFirst("uname").Value)[0].WorklinksAdministrationFlag;//yuc
                criteria.WorklinksAdministrationFlag = !Common.ServiceWrapper.SecurityClient.GetSecurityUser(System.Web.HttpContext.Current.User.Identity.Name .ToString())[0].WorklinksAdministrationFlag;

                return criteria;
            }
            set
            {
                LastName.TextValue = value.LastName;
                FirstName.TextValue = value.FirstName;
                UserName.TextValue = value.UserName;
                IsLockedOutFlag.Checked = value.IsLockedOutFlag;
            }
        }
        protected bool EnableUserAccessInformationMenuItem { get { return Common.Security.RoleForm.UserAccessInformationReport.ViewFlag; } }
        protected bool AddFlag { get { return Common.Security.RoleForm.UserEdit.AddFlag; } }
        protected bool DeleteFlag { get { return Common.Security.RoleForm.UserEdit.DeleteFlag; } }
        protected bool ViewFlag { get { return Common.Security.RoleForm.UserEdit.ViewFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.UserSearch.ViewFlag);

            WireEvents();

            if (!IsPostBack)
                Search(Criteria);

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null)
            {
                if (args.StartsWith("refresh"))
                {
                    Search(Criteria);
                    UserSummaryGrid.SelectedIndexes.Clear();
                    UserSummaryGrid.Rebind();
                }
                else if (args.Length > 15 && args.Substring(0, 15).ToLower().Equals("deletepersonid="))
                {
                    long personId = Convert.ToInt64(args.Substring(15));
                    try
                    {
                        //attempt to delete the user then refresh
                        Common.ServiceWrapper.SecurityClient.DeleteUser(personId);
                        Data.Remove(personId.ToString());
                        UserSummaryGrid.SelectedIndexes.Clear();
                        UserSummaryGrid.Rebind();
                    }
                    catch (Exception ex)
                    {
                        System.Web.UI.ClientScriptManager manager = Page.ClientScript;
                        manager.RegisterStartupScript(this.GetType(), "userDeleteMessage", "userDeleteMessage();", true);
                    }
                }
            }
        }
        protected void WireEvents()
        {
            UserSummaryGrid.NeedDataSource += new GridNeedDataSourceEventHandler(UserSummaryGrid_NeedDataSource);

            //work around to remove empty rows caused by the OnCommand Telerik bug
            UserSummaryGrid.ItemDataBound += new GridItemEventHandler(UserSummaryGrid_ItemDataBound);
            UserSummaryGrid.PreRender += new EventHandler(UserSummaryGrid_PreRender);
            UserSummaryGrid.AllowPaging = true;
        }
        protected void UserSummaryGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            UserSummaryGrid.DataSource = Data;
        }
        protected void UserSummaryGrid_PreRender(object sender, EventArgs e)
        {
            //work around to remove empty rows caused by the OnCommand Telerik bug
            if (Data == null)
                UserSummaryGrid.AllowPaging = false;
            else
                UserSummaryGrid.AllowPaging = true;
        }
        protected void UserSummaryGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //work around to remove empty rows caused by the OnCommand Telerik bug
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                if (String.Equals((dataItem["UserName"].Text).ToUpper(), "&nbsp;") && String.Equals((dataItem["LastName"].Text).ToUpper(), "&nbsp;") &&
                    String.Equals((dataItem["FirstName"].Text).ToUpper(), "&nbsp;") && String.Equals((dataItem["LastLoginDatetime"].Text).ToUpper(), "&nbsp;") &&
                    String.Equals((dataItem["LoginExpiryDatetime"].Text).ToUpper(), "&nbsp;") && String.Equals((dataItem["IsLockedOutFlag"].Text).ToUpper(), "&nbsp;") &&
                    String.Equals((dataItem["LastLockoutDatetime"].Text).ToUpper(), "&nbsp;") && String.Equals((dataItem["LastPasswordChangedDatetime"].Text).ToUpper(), "&nbsp;") &&
                    String.Equals((dataItem["PasswordExpiryDatetime"].Text).ToUpper(), "&nbsp;") && String.Equals((dataItem["LastLogoutDatetime"].Text).ToUpper(), "&nbsp;"))
                {
                    e.Item.Display = false;
                }
            }
        }
        #endregion

        #region event handlers
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search(Criteria);
        }
        public void Search(UserAdminSearchCriteria criteria)
        {
            UserSummaryCollection collection = new UserSummaryCollection();
            collection.Load(Common.ServiceWrapper.SecurityClient.GetUserSummary(criteria));
            Data = collection;
            UserSummaryGrid.Rebind();
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Criteria = new UserAdminSearchCriteria();
            Data = null;
            UserSummaryGrid.DataBind();
        }
        #endregion

        #region security handlers
        protected void AddToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && AddFlag; }
        protected void Delete_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && DeleteFlag; }
        protected void DetailsToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && ViewFlag; }
        protected void UserAccessInformation_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableUserAccessInformationMenuItem; }
        #endregion
    }
}