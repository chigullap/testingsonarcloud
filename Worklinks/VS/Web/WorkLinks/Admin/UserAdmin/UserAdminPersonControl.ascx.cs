﻿using System;
using System.Security.Claims;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Payroll.PayrollBatch;

namespace WorkLinks.Admin.UserAdmin
{
    public partial class UserAdminPersonControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private bool? _isEmployeeProperty = null;
        private const String _isEmployeePropertyKey = "_isEmployeePropertyKey";
        private bool _isEmployeeChecked = false;
        private bool _isResetPasswordChecked = false;
        private long _securityUserId;
        private string _securityUserName;
        private bool? _needDataSourceFlag = null;
        private const String _dataSourceKey = "_dataSourceKey";
        private String _dupeEmpMsg = "";

        protected static bool removeAttachmentClicked = false;
        protected static bool validFileType = false;
        protected static bool invalidSignature = false;
        #endregion

        #region properties
        public String Action { get { return (String)Page.RouteData.Values["action"]; } }
        private long PersonId { get { return Convert.ToInt64(Page.RouteData.Values["personId"]); } }
        public bool? NeedDataSourceFlag //keeps track of the previous value for IsViewMode.  We want to avoid the Grid performing 2 need datasource calls when "Update" is used.
        {
            get
            {
                if (_needDataSourceFlag == null)
                {
                    Object obj = ViewState[_dataSourceKey];
                    if (obj != null)
                        _needDataSourceFlag = (bool)obj;
                }

                return _needDataSourceFlag;
            }
            set
            {
                _needDataSourceFlag = value;
                ViewState[_dataSourceKey] = _needDataSourceFlag;
            }
        }
        public bool IsEditMode { get { return UserPersonDetailView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool IsInsertMode { get { return UserPersonDetailView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsViewMode { get { return UserPersonDetailView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.UserEdit.UpdateFlag; } }
        public bool? IsEmployeeProperty
        {
            get
            {
                if (_isEmployeeProperty == null)
                {
                    Object obj = ViewState[_isEmployeePropertyKey];
                    if (obj != null)
                        _isEmployeeProperty = (bool)obj;
                }

                return _isEmployeeProperty;
            }
            set
            {
                _isEmployeeProperty = value;
                ViewState[_isEmployeePropertyKey] = _isEmployeeProperty;
            }
        }
        public bool IsEmployeeChecked
        {
            get { return _isEmployeeChecked; }
            set { _isEmployeeChecked = value; }
        }
        public bool IsResetPasswordChecked
        {
            get { return _isResetPasswordChecked; }
            set { _isResetPasswordChecked = value; }
        }
        public int MinRequiredPasswordLength { get { return Convert.ToInt16(System.Web.Security.Membership.Provider.MinRequiredPasswordLength); } }
        public long SecurityUserId
        {
            get { return _securityUserId; }
            set { _securityUserId = value; }
        }
        public string SecurityUserName
        {
            get { return _securityUserName; }
            set { _securityUserName = value; }
        }
        public String DupeUserMsg
        {
            get { return _dupeEmpMsg; }
            set { _dupeEmpMsg = value; }
        }
        public bool IsUserEditingTheirOwnInfo
        {
            get
            {
                if (GetSecurityUser().UserName == ((SecurityUser)Data[0]).UserName)
                    return true;
                else
                    return false;
            }
        }

        public string[] AllowedFileExtensions { get { return Common.CodeHelper.PopulateAllowedFileExtensions(LanguageCode); } }

        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.UserEdit.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                if (Action.ToLower().Trim().Equals("add"))
                {
                    UserPersonDetailView.ChangeMode(FormViewMode.Insert);
                    UserPersonDetailView.DataBind();

                    //generate temp password, 8 characters, 2 non-alphas
                    TextBoxControl passwordEntry = (TextBoxControl)UserPersonDetailView.FindControl("PasswordEntry");
                    passwordEntry.TextValue = System.Web.Security.Membership.GeneratePassword(8, 2);

                    TextBoxControl passwordConfirmEntry = (TextBoxControl)UserPersonDetailView.FindControl("ConfirmPasswordEntry");
                    passwordConfirmEntry.TextValue = passwordEntry.TextValue;
                }
                else if (Action.ToLower().Trim().Equals("view"))
                    UserPersonDetailView.DataBind(); //load data
            }

            if (!CheckForWorkLinksAdministrationFlagIsValid())
                SetAuthorized(false);
        }
        protected bool CheckForWorkLinksAdministrationFlagIsValid()
        {
            bool checkForWorkLinksAdministrationFlag = false;

            if (GetSecurityUser().WorklinksAdministrationFlag)
                checkForWorkLinksAdministrationFlag = true;
            else
                checkForWorkLinksAdministrationFlag = !((SecurityUser)Data[0]).WorklinksAdministrationFlag;

            return checkForWorkLinksAdministrationFlag;
        }
        protected SecurityUser GetSecurityUser()
        {
            //String loggedInUser = ((ClaimsIdentity)System.Web.HttpContext.Current.User.Identity).FindFirst("uname").Value;//yuc
            String loggedInUser = System.Web.HttpContext.Current.User.Identity.Name.ToString();
            SecurityUser user = Common.ServiceWrapper.SecurityClient.GetSecurityUser(loggedInUser)[0];
            return user;
        }
        protected void WireEvents()
        {
            UserPersonDetailView.Inserting += new WLPFormView.ItemInsertingEventHandler(UserPersonDetailView_Inserting);
            UserPersonDetailView.DataBound += UserPersonDetailView_DataBound;

            GroupGrid.PreRender += new EventHandler(GroupGrid_PreRender);
            GroupGrid.NeedDataSource += new GridNeedDataSourceEventHandler(GroupGrid_NeedDataSource);
            GroupGrid.UpdateAllCommand += new WLPGrid.UpdateAllCommandEventHandler(GroupGrid_UpdateAllCommand);
        }

        private void UserPersonDetailView_DataBound(object sender, EventArgs e)
        {
            if (UserPersonDetailView.CurrentMode == FormViewMode.Edit)
            {
                RadAsyncUpload uploadPhoto = (RadAsyncUpload)UserPersonDetailView.FindControl("UploadAttachment");
                uploadPhoto.Localization.Select = String.Format("{0}", GetGlobalResourceObject("Messages", "AttachEmployeePhoto"));
                uploadPhoto.Localization.Remove = String.Format("{0}", GetGlobalResourceObject("Messages", "RemoveEmployeePhoto"));
            }
        }

        protected void UploadAttachment_FileUploaded(object sender, FileUploadedEventArgs e)
        {
            removeAttachmentClicked = false;

            UploadedFile file = e.File;
            int contentLength = (int)file.ContentLength;
            String contentType = file.ContentType;
            String[] validFileTypes = { "bmp", "gif", "png", "jpg", "jpeg" };

            for (int i = 0; i < validFileTypes.Length; i++)
            {
                if (contentType == "image/" + validFileTypes[i])
                    validFileType = true;
            }

            if (contentLength > 2097152 || !validFileType)
                invalidSignature = true;
            else
            {
                byte[] fileData = new byte[contentLength];
                file.InputStream.Read(fileData, 0, contentLength);
                ((SecurityUser)Data[0]).Signature = fileData;
            }
        }

        protected void RemoveAttachment_Click(object sender, EventArgs e)
        {
            removeAttachmentClicked = true;
            ((SecurityUser)Data[0]).AttachmentObject.Data = null;
            ((WLPButton)sender).Visible = false;
        }

        #endregion

        #region checked changed
        protected void IsEmployee_CheckedChanged(object sender, EventArgs e)
        {
            DupeUserMsg = "";

            if (((CheckBoxControl)sender).Checked)
            {
                ShowMiniSearch();
                IsEmployeeProperty = true;
            }
            else
            {
                HideMiniSearch();
                IsEmployeeProperty = false;
            }

            WLPLabel messageLabel = (WLPLabel)UserPersonDetailView.FindControl("Message");
            if (messageLabel != null)
                messageLabel.DataBind();
        }
        private void ShowMiniSearch()
        {
            //show the employee mini-search control
            EmployeeControl temp = (EmployeeControl)UserPersonDetailView.FindControl("EmployeeControl1");
            if (temp != null)
                temp.Visible = true;

            //render the LastName, FirstName controls readonly.
            TextBoxControl firstName = (TextBoxControl)UserPersonDetailView.FindControl("FirstName");
            if (firstName != null)
            {
                firstName.ReadOnly = true;
                firstName.TextValue = "";
            }

            TextBoxControl lastName = (TextBoxControl)UserPersonDetailView.FindControl("LastName");
            if (lastName != null)
            {
                lastName.ReadOnly = true;
                lastName.TextValue = "";
            }
        }
        private void HideMiniSearch()
        {
            //hide the employee mini-search control
            EmployeeControl temp = (EmployeeControl)UserPersonDetailView.FindControl("EmployeeControl1");
            if (temp != null)
                temp.Visible = false;

            //render the //render the LastName, FirstName controls.
            TextBoxControl firstName = (TextBoxControl)UserPersonDetailView.FindControl("FirstName");
            if (firstName != null)
                firstName.ReadOnly = false;

            TextBoxControl lastName = (TextBoxControl)UserPersonDetailView.FindControl("LastName");
            if (lastName != null)
                lastName.ReadOnly = false;
        }
        protected void IsResetPassword_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBoxControl)sender).Checked)
            {
                //render the Password and ConfirmPassword controls visible
                TextBoxControl passwordEntry = (TextBoxControl)UserPersonDetailView.FindControl("PasswordEntry");
                passwordEntry.Visible = true;

                //generate temp password, 8 characters, 2 non-alphas
                passwordEntry.TextValue = System.Web.Security.Membership.GeneratePassword(8, 2);

                RequiredFieldValidator password1 = (RequiredFieldValidator)UserPersonDetailView.FindControl("PasswordEntryValidator");
                if (password1 != null)
                    password1.Enabled = true;

                TextBoxControl passwordConfirmEntry = (TextBoxControl)UserPersonDetailView.FindControl("ConfirmPasswordEntry");
                passwordConfirmEntry.Visible = true;
                passwordConfirmEntry.TextValue = passwordEntry.TextValue;

                RequiredFieldValidator password2 = (RequiredFieldValidator)UserPersonDetailView.FindControl("ConfirmPasswordEntryValidator");
                if (password2 != null)
                    password2.Enabled = true;

                //uncheck the IsLockedOutFlag if it's checked
                CheckBoxControl isLockedOutFlag = (CheckBoxControl)UserPersonDetailView.FindControl("IsLockedOutFlag");
                isLockedOutFlag.Checked = false;
            }
            else
            {
                //render the Password and ConfirmPassword controls invisible
                TextBoxControl passwordEntry = (TextBoxControl)UserPersonDetailView.FindControl("PasswordEntry");
                passwordEntry.Visible = false;
                passwordEntry.TextValue = "";

                RequiredFieldValidator password1 = (RequiredFieldValidator)UserPersonDetailView.FindControl("PasswordEntryValidator");
                if (password1 != null)
                    password1.Enabled = false;

                TextBoxControl passwordConfirmEntry = (TextBoxControl)UserPersonDetailView.FindControl("ConfirmPasswordEntry");
                passwordConfirmEntry.Visible = false;
                passwordConfirmEntry.TextValue = "";

                RequiredFieldValidator password2 = (RequiredFieldValidator)UserPersonDetailView.FindControl("ConfirmPasswordEntryValidator");
                if (password2 != null)
                    password2.Enabled = false;
            }
        }
        #endregion

        #region event handlers

        //if the user exists on more than one client on the auth db, disable password change.
        protected void ResetPassword_PreRender(object sender, EventArgs e)
        {
            //query to see how many environments this user has access to
            int rowCount = 1; //yuc Common.ServiceWrapper.SecurityClient.SelectSecurityClientUser(Common.ApplicationParameter.AuthDatabaseName, SecurityUserId);
            
            //if more than 1 row, hide checkbox
            if (rowCount > 1)
                ((CheckBoxControl)sender).Visible = false;
        }

        //if the user exists on more than one client on the auth db, lockout should only be applied to the current database, not the auth database.
        private bool UserExistsOnMoreThanOneClient(SecurityUser user)
        {
            //yuc
            //if (Common.ServiceWrapper.SecurityClient.SelectSecurityClientUser(Common.ApplicationParameter.AuthDatabaseName, SecurityUserId) > 0 && user.IsLockedOutFlag)
            //    return true;
            //else
                return false;
        }

        private void empControl_Handler(object sender, EventArgs e)
        {
            //this will remove the error message of "employee already exists" if it has been shown to the user and they have then selected a new employee
            if (DupeUserMsg != "")
            {
                DupeUserMsg = "";

                WLPLabel messageLabel = (WLPLabel)UserPersonDetailView.FindControl("Message");
                if (messageLabel != null)
                    messageLabel.DataBind();
            }
        }
        void GroupGrid_PreRender(object sender, EventArgs e)
        {
            foreach (GridItem item in GroupGrid.MasterTableView.Items)
            {
                CheckBoxControl control = (CheckBoxControl)item.FindControl("IsGroupSelected");
                if (control != null)
                    control.ReadOnly = IsViewMode;
            }

            //if "add" occurs, we will be in InsertMode and IsEmployeeProperty will be coded to "true".  If user unchecks "IsEmployee", IsEmployeeProperty will be false
            if (IsInsertMode && (IsEmployeeProperty != null && (bool)IsEmployeeProperty))
            {
                //set defaults...
                CheckBoxControl temp = (CheckBoxControl)UserPersonDetailView.FindControl("IsEmployee");
                if (temp != null && temp.Checked)
                    ShowMiniSearch();

                if (temp != null && !Page.IsPostBack)
                    temp.Checked = true;
            }

            ShowHideWorklinksAdminCheckbox();
        }
        protected void ShowHideWorklinksAdminCheckbox()
        {
            CheckBoxControl worklinksAdmin = (CheckBoxControl)UserPersonDetailView.FindControl("WorklinksAdministrationFlag");
            if (worklinksAdmin != null)
            {
                //do not show the WorklinksAdministrationFlag if the logged in user is not a "WorkLinks Admin"
                if (!GetSecurityUser().WorklinksAdministrationFlag)
                    worklinksAdmin.Style.Add("display", "none");
            }
        }
        void GroupGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            //load the grid with the users group/roles
            GroupGrid.DataSource = GroupDescriptionCollection.ConvertCollection(Common.ServiceWrapper.SecurityClient.GetUserGroups(SecurityUserId, IsViewMode, !GetSecurityUser().WorklinksAdministrationFlag));
        }
        private void SetData()
        {
            IsEmployeeProperty = ((SecurityUser)Data[0]).IsEmployee;
            SecurityUserId = ((SecurityUser)Data[0]).SecurityUserId;
            SecurityUserName = ((SecurityUser)Data[0]).UserName;

            //display a message for the user.  Email updates will be avoided as ContactChannelId will be missing as well
            if (((SecurityUser)Data[0]).Email == null)
                ((SecurityUser)Data[0]).Email = String.Format("{0}", GetGlobalResourceObject("NoEmail", "NoEmailMessage"));
        }
        protected void UserPersonDetailView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            if (PersonId > 0) //view mode
            {
                //load the formview with the persons personal info
                Data = SecurityUserCollection.ConvertCollection(Common.ServiceWrapper.SecurityClient.GetPersonUserDetails(PersonId));
                UserPersonDetailView.DataSource = Data;

                //set some defaults
                SetData();

                //if formview is in edit mode, make the grid "edit all" mode as well
                if (!IsViewMode)
                    GroupGrid.SetEditAllMode(true);
                else
                    GroupGrid.SetEditAllMode(false);

                //load the grid with the users group/roles
                if (IsViewMode != NeedDataSourceFlag) //prevent Grid from getting 2 datasource calls during Update otherwise changed data will be lost
                {
                    NeedDataSourceFlag = IsViewMode;
                    GroupGrid.DataSource = GroupDescriptionCollection.ConvertCollection(Common.ServiceWrapper.SecurityClient.GetUserGroups(SecurityUserId, IsViewMode, !GetSecurityUser().WorklinksAdministrationFlag));
                    GroupGrid.DataBind();
                }
            }
            else //add mode
            {
                SecurityUserCollection securityCollection = new SecurityUserCollection();
                securityCollection.Load(new SecurityUser() { EmployeeId = -1, IsEmployee = true });
                Data = securityCollection;
                UserPersonDetailView.DataSource = Data;
                IsResetPasswordChecked = true;

                //set this on the load
                if (IsEmployeeProperty == null || IsEmployeeProperty == true)
                {
                    IsEmployeeChecked = true;
                    IsEmployeeProperty = securityCollection[0].IsEmployee;
                }

                SecurityUserId = -1;
                SecurityUserName = null;

                //set EditAllMode for the Grid
                GroupGrid.SetEditAllMode(true);

                //load the grid with the users group/roles
                if (IsViewMode != NeedDataSourceFlag && !IsInsertMode) //prevent Grid from getting 2 datasource calls during Update otherwise changed data will be lost
                {
                    NeedDataSourceFlag = IsViewMode;
                    GroupGrid.DataSource = GroupDescriptionCollection.ConvertCollection(Common.ServiceWrapper.SecurityClient.GetUserGroups(SecurityUserId, IsViewMode, !GetSecurityUser().WorklinksAdministrationFlag));
                    GroupGrid.DataBind();
                }
            }
        }

        protected void UserPersonDetailView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    SecurityUser info = (SecurityUser)e.DataItem; //only one user being updated
                    PopulateAttachmentObjectCollection(info);
                    bool onlyUpdateLocalDbForLockedOutFlag = UserExistsOnMoreThanOneClient(info);

                    Common.ServiceWrapper.SecurityClient.UpdateSecurityUser(info.UserName, info.PasswordEntry, info, Common.ApplicationParameter.PasswordExpiryDays, Common.ApplicationParameter.LoginExpiryPasswordChangeHours, onlyUpdateLocalDbForLockedOutFlag); //yuc

                    //Fire the update all for the Grid
                    GroupGrid.UpdateAll();
                }
                else
                    e.Cancel = true;
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void UserPersonDetailView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            if (Page.IsValid)
            {
                SecurityUser user = (SecurityUser)e.DataItem;
                PopulateAttachmentObjectCollection(user);
                
                //For employees, make sure employee isn't already in the system
                if (user.EmployeeId > 0 && !Common.ServiceWrapper.SecurityClient.DoesSecurityUserAlreadyExist(user.EmployeeId) || Convert.ToInt16(user.Key) < 0)
                {
                    user = Common.ServiceWrapper.SecurityClient.InsertUser(user, Common.ApplicationParameter.AuthDatabaseName, Common.ApplicationParameter.SecurityClientUsedByAngular,
                        Common.ApplicationParameter.LoginExpiryPasswordChangeHours, Common.ApplicationParameter.NgSecurity, Common.ApplicationParameter.NgDbSecurity, Common.ApplicationParameter.NgApiSecurity);
                    SecurityUserId = user.SecurityUserId;
                    SecurityUserName = user.UserName;

                    //Fire the update all for the Grid
                    GroupGrid.UpdateAll();

                    Response.Redirect(String.Format("~/Admin/UserAdmin/{0}/{1}", "View", user.PersonId));
                }
                else
                {
                    DupeUserMsg = String.Format("{0}", GetGlobalResourceObject("ErrorMessages", "DupeEmployee"));
                    e.Cancel = true;
                }
            }
            else
                e.Cancel = true;
        }
        void GroupGrid_UpdateAllCommand(object sender, UpdateAllCommandEventArgs e)
        {
            //pass the collection to WS, determine if updates or inserts are needed, then return collection.
            try
            {
                GroupDescriptionCollection collection = new GroupDescriptionCollection();

                foreach (GroupDescription entity in e.Items)
                    collection.Add(entity);

                Common.ServiceWrapper.SecurityClient.UpdateUserGroups(SecurityUserName, SecurityUserId, collection.ToArray());
                Common.Resources.DatabaseResourceCache.ClearCache();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void EmployeeId_DataBinding(object sender, EventArgs e)
        {
            PopulateEmployeeControl((ICodeControl)sender);
        }
        protected void PopulateEmployeeControl(ICodeControl control)
        {
            Common.CodeHelper.PopulateEmployeeControl(control, false);
        }
        #endregion

        #region server validate

        protected void EmailValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = false;

            //code below...ELSE...will also allow previous entered usernames to be valid if they arent emails.  good for crossover to new auth??  or remove else?
            //username is ony editable in insert mode. Check if username is a valid email
            if (IsInsertMode)
            {
                TextBoxControl usernameControl = (TextBoxControl)UserPersonDetailView.FindControl("UserName");
                if (usernameControl != null)
                {
                    if (usernameControl.Value != null)
                    {
                        string email = usernameControl.Value.ToString();

                        if (!string.IsNullOrEmpty(email))
                            //username must not be email for now
                            args.IsValid = true;
                            //args.IsValid = System.Text.RegularExpressions.Regex.IsMatch(email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
                        else
                            args.IsValid = false;
                    }
                }
            }
            else
                args.IsValid = true; //doing an update, no need to validate as it cannot be changed
        }
        protected void UsernameValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //username is ony editable in insert mode. Check if username already exists 
            if (IsInsertMode)
            {
                args.IsValid = false;

                TextBoxControl userNameControl = (TextBoxControl)UserPersonDetailView.FindControl("UserName");
                if (userNameControl != null && !string.IsNullOrEmpty(userNameControl.Value.ToString()))
                    args.IsValid = Common.ServiceWrapper.SecurityClient.ValidateUserName(userNameControl.Value.ToString());
            }
            else
                args.IsValid = true; //doing an update, no need to validate as it cannot be changed
        }

        #endregion

        protected void PopulateAttachmentObjectCollection(SecurityUser user)
        {
            RadAsyncUpload uploadControl = (RadAsyncUpload)UserPersonDetailView.FindControl("UploadAttachment");

            if (uploadControl.UploadedFiles.Count > 0 && !removeAttachmentClicked)
            {
                //get the uploaded file from the UploadAttachment control and the description from the Description control
                UploadedFile file = uploadControl.UploadedFiles[0];
                byte[] bytes = new byte[file.ContentLength];
                file.InputStream.Read(bytes, 0, bytes.Length);

                //populate the AttachmentObject object
                user.AttachmentObject = new Attachment();
                user.AttachmentObject.Data = bytes;
                user.AttachmentObject.FileName = file.GetNameWithoutExtension();
                user.AttachmentObject.FileTypeCode = file.GetExtension().Replace(".", "").ToUpper();
                user.AttachmentObject.Description = "";
            }
            else if (removeAttachmentClicked)
            {
                user.AttachmentId = null;
                user.AttachmentObject = null;
            }
        }
    }
}