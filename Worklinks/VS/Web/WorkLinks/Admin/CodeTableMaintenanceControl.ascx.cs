﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin
{
    public partial class CodeTableMaintenanceControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private const String _codeTableNameKey = "CodeTableName";
        private const String _parentCodeTableNameKey = "ParentCodeTableName";
        private string _codeTableName = "";
        private String _parentCodeTableName = String.Empty;
        #endregion

        #region properties
        private String CodeTableName
        {
            get
            {
                if (_codeTableName == "")
                {
                    Object obj = ViewState[_codeTableNameKey];

                    if (obj != null)
                        _codeTableName = obj.ToString();
                }

                return _codeTableName;
            }
            set
            {
                _codeTableName = value;
                ViewState[_codeTableNameKey] = _codeTableName;
            }
        }
        private String ParentCodeTableName
        {
            get
            {
                if (_parentCodeTableName == String.Empty)
                {
                    Object obj = ViewState[_parentCodeTableNameKey];

                    if (obj != null)
                        _parentCodeTableName = obj.ToString();
                }

                return _parentCodeTableName;
            }
            set
            {
                _parentCodeTableName = value;
                ViewState[_parentCodeTableNameKey] = _parentCodeTableName;
            }
        }
        public bool IsCodeGridViewMode { get { return CodeGrid.IsViewMode; } }
        public bool IsCodeGridUpdate { get { return CodeGrid.IsEditMode; } }
        public bool IsCodeGridInsert { get { return CodeGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.CodeMaintenance.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.CodeMaintenance.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.CodeMaintenance.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.CodeMaintenance.ViewFlag);

            if (!Page.IsPostBack)
            {
                //populate the code table dropdown
                CodeTables.Sort = RadComboBoxSort.Ascending;
                AssemblyCache.CodeHelper.PopulateComboBoxWithCodeTableIndex(CodeTables);
                CodeTables.DataBind();
                CodeTables.Items.Sort();
                LoadGrids(CodeTables.SelectedValue);

                CodeTableName = CodeTables.SelectedValue;

                Initialize();
            }

            WireEvents();
        }
        protected void Initialize()
        {
            //Find the CodeGrid
            WLPGrid grid = (WLPGrid)this.FindControl("CodeGrid");

            //hide the edit/delete images in the rows.
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        protected void LoadGrids(String tablename)
        {
            //load the data rows for the selected code table
            GetGridData(tablename);
            CodeGrid.DataSource = Data;
            CodeGrid.DataBind();

            ////select the first row then load its description
            //if (CodeGrid.Items.Count > 0)
            //    CodeGrid.Items[0].Selected = true;
        }
        protected void GetGridData(String tableName)
        {
            //populate the grid with the selected code table rows
            Data = CodeTableCollection.ConvertCollection(Common.ServiceWrapper.CodeClient.GetCodeTableRows(tableName, ParentCodeTableName));
        }
        protected void WireEvents()
        {
            CodeGrid.NeedDataSource += new GridNeedDataSourceEventHandler(CodeGrid_NeedDataSource);
            CodeTables.PreRender += new EventHandler(CodeTables_PreRender);
        }
        protected void CodeGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditFormInsertItem)
            {
                GridEditFormItem item = e.Item as GridEditFormItem;

                //set default values for user
                CheckBoxControl tempCBC = (CheckBoxControl)item.FindControl("ActiveFlag");
                if (tempCBC != null)
                    tempCBC.Value = true;

                NumericControl tempNC = (NumericControl)item.FindControl("SortOrder");
                if (tempNC != null)
                    tempNC.Value = 0;
            }
            else if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                ImageButton button = (ImageButton)dataItem["EditButton"].Controls[0];
                button.Visible = !((CodeTable)dataItem.DataItem).SystemFlag;

                button = (ImageButton)dataItem["DeleteButton"].Controls[0];
                button.Visible = !((CodeTable)dataItem.DataItem).SystemFlag;
            }
        }
        #endregion

        #region events
        void CodeTables_PreRender(object sender, EventArgs e)
        {
            CodeTables.Enabled = IsCodeGridViewMode;
        }
        protected void CodeTables_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //keep track of the key of the code table that is selected
            CodeTableName = e.Value;
            ParentCodeTableName = Common.ServiceWrapper.CodeClient.GetCodeTableIndex(CodeTableName)[0].ParentTableName;
            LoadGrids(CodeTableName);
        }
        void CodeGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            CodeGrid.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        #endregion

        #region button actions
        protected void CodeGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                CodeTable cTable = (CodeTable)e.Item.DataItem;
                cTable = Common.ServiceWrapper.CodeClient.UpdateCodeTableData(cTable, CodeTableName, ParentCodeTableName);

                //clear all codes from assembly 
                Common.CodeHelper.ResetCodeTableCache();

                cTable.CopyTo((CodeTable)Data[cTable.Key]);

                //If descriptions were updated to be "", they would have been deleted from the db, so we remove them from our Data.Desc collection.
                //Using a foreach will not work if the collection is modified during the loop, so using an IF, starting at the end of the collection to traverse to the first of the collection
                for (int i = cTable.Descriptions.Count - 1; i > -1; i--)
                {
                    if (((CodeTable)Data[cTable.Key]).Descriptions[i].TableDescription == "" && ((CodeTable)Data[cTable.Key]).Descriptions[i].Id > 0)
                        ((CodeTable)Data[cTable.Key]).Descriptions.RemoveAt(i);
                }

                //cleanup objects
                cTable = null;
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void CodeGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.CodeClient.DeleteCodeTableData((CodeTable)e.Item.DataItem, CodeTableName);

                //clear all codes from assembly 
                Common.CodeHelper.ResetCodeTableCache();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void CodeGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            CodeTable cTable = (CodeTable)e.Item.DataItem;
            cTable = Common.ServiceWrapper.CodeClient.InsertCodeTableData(cTable, CodeTableName, ParentCodeTableName);

            //clear all codes from assembly 
            Common.CodeHelper.ResetCodeTableCache();

            cTable.CopyTo((CodeTable)Data[String.Empty]);  //john changed this last year...
        }
        #endregion

        protected void ParentCode_Init(object sender, EventArgs e)
        {
            if (ParentCodeTableName == null || ParentCodeTableName.Equals(String.Empty))
                ((WebControl)sender).Visible = false;
            else
                ((WebControl)sender).Visible = true;
        }
        protected void ParentCode_DataBinding(object sender, EventArgs e)
        {
            ComboBoxControl control = (ComboBoxControl)sender;
            if (control.Visible)
            {
                CodeCollection collection = new CodeCollection();
                foreach (CodeTable table in Common.ServiceWrapper.CodeClient.GetCodeTableRows(ParentCodeTableName, ParentCodeTableName))
                {
                    if (table.ActiveFlag)
                    {
                        CodeObject code = new CodeObject() { Code = table.CodeTableId, Description = table.Descriptions[LanguageCode].TableDescription };
                        collection.Add(code);
                    }
                }

                control.DataSource = collection;
            }
        }
    }
}