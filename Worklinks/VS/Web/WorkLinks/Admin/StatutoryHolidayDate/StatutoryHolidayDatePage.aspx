﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StatutoryHolidayDatePage.aspx.cs" Inherits="WorkLinks.Admin.StatutoryHolidayDate.StatutoryHolidayDatePage" %>
<%@ Register Src="StatutoryHolidayDateControl.ascx" TagName="StatutoryHolidayDateControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:StatutoryHolidayDateControl ID="StatutoryHolidayDateControl1" SearchLocation="StatutoryHolidayDate" runat="server" />        
    </ContentTemplate>
</asp:Content>