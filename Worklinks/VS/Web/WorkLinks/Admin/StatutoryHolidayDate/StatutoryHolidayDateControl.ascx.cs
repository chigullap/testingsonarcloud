﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.StatutoryHolidayDate
{
    public partial class StatutoryHolidayDateControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected StatutoryDeductionCriteria Criteria
        {
            get
            {
                StatutoryDeductionCriteria criteria = new StatutoryDeductionCriteria();
                criteria.Year = (String)Year.Value;
                criteria.ProvinceStateCode = (String)Province.Value;
                criteria.Description = Description.TextValue;

                return criteria;
            }
            set
            {
                Year.Value = value.Year;
                Province.Value = value.ProvinceStateCode;
                Description.TextValue = value.Description;
            }
        }
        public bool IsViewMode { get { return StatutoryHolidayDateGrid.IsViewMode; } }
        public bool IsUpdate { get { return StatutoryHolidayDateGrid.IsEditMode; } }
        public bool IsInsert { get { return StatutoryHolidayDateGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.StatutoryHolidayDate.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.StatutoryHolidayDate.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.StatutoryHolidayDate.DeleteFlag; } }
        public String DropDownTreeDefaultMessage { get { return String.Format(GetGlobalResourceObject("PageContent", "ChooseADepartment").ToString()); } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.StatutoryHolidayDate.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                Panel1.DataBind();
                Year.Value = DateTime.Now.Year.ToString();
                Initialize();
            }

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.Equals(String.Format("refresh")))
                RefreshGrid();
        }
        protected void WireEvents()
        {
            StatutoryHolidayDateGrid.NeedDataSource += new GridNeedDataSourceEventHandler(StatutoryHolidayDateGrid_NeedDataSource);
            StatutoryHolidayDateGrid.ItemDataBound += StatutoryHolidayDateGrid_ItemDataBound;
        }
        protected void Initialize()
        {
            //find the StatutoryHolidayDateGrid
            WLPGrid grid = (WLPGrid)this.FindControl("StatutoryHolidayDateGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        protected void RefreshGrid()
        {
            long keyId = -1;

            //if a row was selected
            if (StatutoryHolidayDateGrid.SelectedItems.Count != 0)
                keyId = Convert.ToInt64(((GridDataItem)StatutoryHolidayDateGrid.SelectedItems[0]).GetDataKeyValue(StatutoryHolidayDateGrid.MasterTableView.DataKeyNames[0]));

            Search(Criteria);

            if (keyId != -1)
                StatutoryHolidayDateGrid.SelectRowByFirstDataKey(keyId); //re-select the selected row
        }
        public void Search(StatutoryDeductionCriteria criteria)
        {
            Data = StatutoryHolidayDateCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetStatutoryHolidayDateReport(criteria));
            StatutoryHolidayDateGrid.Rebind();
        }
        #endregion

        #region event handlers
        void StatutoryHolidayDateGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //if in edit mode, show the RadDropDownTree and select the associated department
            if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
            {
                GridEditFormItem item = (GridEditFormItem)e.Item;
                RadDropDownTree organizationUnitDropDownTree = (RadDropDownTree)item.FindControl("OrganizationUnitDropDownTree");

                if (organizationUnitDropDownTree != null)
                {
                    organizationUnitDropDownTree.DataSource = Common.ServiceWrapper.HumanResourcesClient.GetOrganizationUnitAssociationTree(LanguageCode, null);
                    organizationUnitDropDownTree.DefaultMessage = DropDownTreeDefaultMessage;
                    organizationUnitDropDownTree.DataBind();

                    if (!IsInsert && !IsViewMode)
                    {
                        BusinessLayer.BusinessObjects.StatutoryHolidayDate statHoliday = (BusinessLayer.BusinessObjects.StatutoryHolidayDate)item.DataItem;

                        //asp hidden field to hold org hierarchy
                        HiddenField orgHierarchy = (HiddenField)item.FindControl("OrganizationUnitDropDownValues");
                        if (orgHierarchy != null)
                            orgHierarchy.Value = statHoliday.OrganizationUnit;
                    }
                }
            }
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void ProvinceStateCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateProvince((ICodeControl)sender);
        }
        protected void YearCode_NeedDataSource(object sender, EventArgs e)
        {
            CodeCollection collection = new CodeCollection();

            //for now use a formula for the year, one ahead, current, and 2 back
            int currentYear = DateTime.Now.Year;
            int[] years = new int[] { currentYear + 1, currentYear, currentYear - 1, currentYear - 2 };

            foreach (int year in years)
                collection.Add(new CodeObject() { Code = year.ToString(), Description = year.ToString() });

            ((ComboBoxControl)sender).DataSource = collection;
        }
        void StatutoryHolidayDateGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            StatutoryHolidayDateGrid.DataSource = Data;
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search(Criteria);
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Year.Value = DateTime.Now.Year.ToString();
            Province.Value = null;
            Description.TextValue = null;
            Data = null;

            StatutoryHolidayDateGrid.DataBind();
        }
        #endregion

        #region handles updates
        protected void StatutoryHolidayDateGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            BusinessLayer.BusinessObjects.StatutoryHolidayDate item = (BusinessLayer.BusinessObjects.StatutoryHolidayDate)e.Item.DataItem;

            HiddenField hiddenOrg = (HiddenField)e.Item.FindControl("OrganizationUnitDropDownValues");
            if (hiddenOrg != null)
                item.OrganizationUnit = hiddenOrg.Value;

            Common.ServiceWrapper.HumanResourcesClient.InsertStatutoryHolidayDate(item).CopyTo((BusinessLayer.BusinessObjects.StatutoryHolidayDate)Data[item.Key]);

            //reload screen
            Search(Criteria);
            StatutoryHolidayDateGrid_NeedDataSource(null, null);
        }
        protected void StatutoryHolidayDateGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                BusinessLayer.BusinessObjects.StatutoryHolidayDate item = (BusinessLayer.BusinessObjects.StatutoryHolidayDate)e.Item.DataItem;

                HiddenField hiddenOrg = (HiddenField)e.Item.FindControl("OrganizationUnitDropDownValues");
                if (hiddenOrg != null)
                    item.OrganizationUnit = hiddenOrg.Value;

                Common.ServiceWrapper.HumanResourcesClient.UpdateStatutoryHolidayDate(item).CopyTo((BusinessLayer.BusinessObjects.StatutoryHolidayDate)Data[item.Key]);

                //reload screen
                Search(Criteria);
                StatutoryHolidayDateGrid_NeedDataSource(null, null);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void StatutoryHolidayDateGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteStatutoryHolidayDate((BusinessLayer.BusinessObjects.StatutoryHolidayDate)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    Search(Criteria);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}