﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StatutoryHolidayDateControl.ascx.cs" Inherits="WorkLinks.Admin.StatutoryHolidayDate.StatutoryHolidayDateControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <table width="100%">
        <tr valign="top">
            <td>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                    <table width="800px">
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="Year" LabelText="**Year**" runat="server" Type="Year" OnDataBinding="YearCode_NeedDataSource" IncludeEmptyItem="false" ResourceName="Year" TabIndex="010" />
                            </td>
                            <td>
                                <wasp:ComboBoxControl ID="Province" LabelText="**ProvinceStateCode**" runat="server" Type="ProvinceStateCode" OnDataBinding="ProvinceStateCode_NeedDataSource" ResourceName="ProvinceStateCode" TabIndex="020" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="Description" LabelText="**Description**" runat="server" ResourceName="Description" TabIndex="030" />
                            </td>
                        </tr>
                    </table>
                    <div class="SearchCriteriaButtons">
                        <wasp:WLPButton ID="btnClear" runat="server" Text="**Clear**" Icon-PrimaryIconUrl="~/App_Themes/Default/clear.gif" CssClass="button" OnClick="btnClear_Click" ResourceName="Clear" />
                        <wasp:WLPButton ID="btnSearch" runat="server" Text="**Search**" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" OnClick="btnSearch_Click" ResourceName="Search" />
                    </div>
                </asp:Panel>
            </td>
        </tr>
        <tr valign="top">
            <td>
                <wasp:WLPGrid
                    ID="StatutoryHolidayDateGrid"
                    runat="server"
                    AllowSorting="true"
                    GridLines="None"
                    Height="500px"
                    AutoAssignModifyProperties="true"
                    OnDeleteCommand="StatutoryHolidayDateGrid_DeleteCommand"
                    OnInsertCommand="StatutoryHolidayDateGrid_InsertCommand"
                    OnUpdateCommand="StatutoryHolidayDateGrid_UpdateCommand">

                    <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="true">
                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                        <Selecting AllowRowSelect="false" />
                    </ClientSettings>

                    <MasterTableView ClientDataKeyNames="StatutoryHolidayDateId" AutoGenerateColumns="false" DataKeyNames="StatutoryHolidayDateId" CommandItemDisplay="Top">
                        <CommandItemTemplate>
                            <wasp:WLPToolBar ID="StatutoryHolidayDateToolBar" runat="server" Width="100%" AutoPostBack="true">
                                <Items>
                                    <wasp:WLPToolBarButton Text="**Add**" ImageUrl="~/App_Themes/Default/add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add" />
                                </Items>
                            </wasp:WLPToolBar>
                        </CommandItemTemplate>

                        <Columns>
                            <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif">
                                <HeaderStyle Width="5%" />
                            </wasp:GridEditCommandControl>
                            <wasp:GridBoundControl DataField="EnglishDescription" LabelText="**EnglishDescription**" SortExpression="EnglishDescription" UniqueName="EnglishDescription" ResourceName="EnglishDescription" />
                            <wasp:GridBoundControl DataField="FrenchDescription" LabelText="**FrenchDescription**" SortExpression="FrenchDescription" UniqueName="FrenchDescription" ResourceName="FrenchDescription" />
                            <wasp:GridKeyValueControl OnNeedDataSource="ProvinceStateCode_NeedDataSource" DataField="ProvinceStateCode" LabelText="**Province**" SortExpression="ProvinceStateCode" Type="ProvinceStateCode" ResourceName="ProvinceStateCode" />
                            <wasp:GridDateTimeControl DataField="HolidayDate" LabelText="**HolidayDate**" SortExpression="HolidayDate" UniqueName="HolidayDate" ResourceName="HolidayDate" />
                            <wasp:GridBoundControl DataField="Description" LabelText="**OrganizationUnit**" SortExpression="Description" UniqueName="OrganizationUnit" ResourceName="OrganizationUnit">
                                <HeaderStyle Width="36%" />
                            </wasp:GridBoundControl>
                            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>">
                                <HeaderStyle Width="5%" />
                            </wasp:GridButtonControl>
                        </Columns>

                        <EditFormSettings EditFormType="Template">
                            <FormTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <wasp:TextBoxControl ID="EnglishDescription" Text="**EnglishDescription**" runat="server" ResourceName="EnglishDescription" Value='<%# Bind("EnglishDescription") %>' Mandatory="true" TabIndex="010" />
                                        </td>
                                        <td>
                                            <wasp:TextBoxControl ID="FrenchDescription" Text="**FrenchDescription**" runat="server" ResourceName="FrenchDescription" Value='<%# Bind("FrenchDescription") %>' Mandatory="true" TabIndex="020" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <wasp:ComboBoxControl ID="ProvinceStateCode" LabelText="**ProvinceStateCode**" runat="server" Type="ProvinceStateCode" OnDataBinding="ProvinceStateCode_NeedDataSource" ResourceName="ProvinceStateCode" Value='<%# Bind("ProvinceStateCode") %>' Mandatory="true" TabIndex="030" />
                                        </td>
                                        <td>
                                            <wasp:DateControl ID="HolidayDate" Text="**HolidayDate**" runat="server" ResourceName="HolidayDate" Value='<%# Bind("HolidayDate") %>' Mandatory="true" TabIndex="040" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form_control">
                                                <span class="label">
                                                    <wasp:WLPLabel ID="OrganizationUnitLabel" runat="server" Text="**OrganizationUnit**" ResourceName="OrganizationUnit" />
                                                </span>
                                                <span class="field">
                                                    <telerik:RadDropDownTree ID="OrganizationUnitDropDownTree" ClientIDMode="Static" runat="server" DataTextField="Description" DataFieldID="OrganizationUnitId" DataFieldParentID="ParentOrganizationUnitId" TextMode="FullPath" DataValueField="OrganizationUnitId" OnClientEntryAdding="OnClientEntryAdding" OnClientLoad="onClientLoad" OnClientClearButtonClicking="onClientClearButtonClicking">
                                                        <DropDownSettings Height="200px" Width="340px" CloseDropDownOnSelection="true" />
                                                        <ButtonSettings ShowClear="true" />
                                                    </telerik:RadDropDownTree>
                                                    <asp:HiddenField ID="OrganizationUnitDropDownValues" ClientIDMode="Static" runat="server" Value="" />
                                                </span>  
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsInsert %>' ResourceName="Insert" />
                                                    </td>
                                                    <td>
                                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="Cancel" CausesValidation="false" ResourceName="Cancel" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </FormTemplate>
                        </EditFormSettings>
                    </MasterTableView>

                    <HeaderContextMenu EnableAutoScroll="true" />

                </wasp:WLPGrid>
            </td>
        </tr>
    </table>
</asp:Panel>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function OnClientEntryAdding(sender, args) {
            var node = args.get_node();

            //get count of child nodes of the selected node, if it has children, don't allow it to be selected
            if (node.get_allNodes().length > 0) {
                args.set_cancel(true);
            }
            else {
                var parentNode = node;
                //get id of the node selected
                var ids = [node.get_value()];

                while (parentNode.get_level() > 0) {
                    parentNode = parentNode.get_parent();
                    //get parent node id 
                    ids.push(parentNode.get_value());
                }
                var hiddenOrgControl = document.getElementById('OrganizationUnitDropDownValues');
                //reverse the array so it's parent->child order
                hiddenOrgControl.value = ids.reverse();
            }
        }

        function onClientLoad(sender, args) {
            var hiddenOrgControl = document.getElementById('OrganizationUnitDropDownValues');
            var orgUnits = hiddenOrgControl.value.split(",");
            var currentNode = sender.get_embeddedTree();
            var orgUnitPathTextDescription = "";

            currentNode = currentNode.findNodeByValue(orgUnits[0]);

            if (currentNode != null) {
                currentNode.expand();
                currentNode.select();
                //get text of parent node
                orgUnitPathTextDescription = currentNode.get_text();

                for (var i = 1; i < orgUnits.length; i++) {
                    for (var j = 0; j < currentNode._children.get_count(); j++) {
                        if (currentNode._children._array[j].get_value() == orgUnits[i]) {
                            currentNode = currentNode._children._array[j];
                            currentNode.expand();
                            currentNode.select();
                            //get text of child node
                            orgUnitPathTextDescription += '/' + currentNode.get_text();

                            break;
                        }
                    }
                }
            }

            //set the path as the default message for the RadDropDownTree
            if (orgUnitPathTextDescription != null)
                sender._defaultMessage = orgUnitPathTextDescription;
        }

        function onClientClearButtonClicking(sender, args) {
            //clear the default message
            sender._defaultMessage = '<%= DropDownTreeDefaultMessage %>';

            var hiddenOrgControl = document.getElementById('OrganizationUnitDropDownValues');
            var orgUnits = hiddenOrgControl.value.split(",");

            var currentNode = sender.get_embeddedTree();
            currentNode = currentNode.findNodeByValue(orgUnits[0]);

            //collapse all nodes in the path that were previously expanded
            if (currentNode != null) {
                currentNode.collapse();

                for (var i = 1; i < orgUnits.length; i++) {
                    for (var j = 0; j < currentNode._children.get_count() ; j++) {
                        if (currentNode._children._array[j].get_value() == orgUnits[i]) {
                            currentNode = currentNode._children._array[j];
                            currentNode.collapse();
                            break;
                        }
                    }
                }
            }

            //set org units to nothing
            hiddenOrgControl.value = null;
        }
    </script>
</telerik:RadScriptBlock>