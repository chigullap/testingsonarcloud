﻿using System;

namespace WorkLinks.Admin.Export
{
    public partial class XmlCreateControl : WLP.Web.UI.WLPUserControl
    {
        protected String ExportType { get { return (String)Page.RouteData.Values["exportType"]; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            byte[] output = (byte[])Session["HandOff"];
            Session.Remove("HandOff");

            Response.Clear();
            Response.ContentType = "application/zip";

            if (ExportType == "T4" || ExportType == "T4A" || ExportType == "R1" || ExportType == "R2" || ExportType == "NR4")
                Response.AppendHeader("content-disposition", "attachment; filename=" + String.Format("{0}_{1}.zip", ExportType, Common.ApplicationParameter.CeridianCompanyNumber));
            else if (ExportType == "CICT4")
                Response.AppendHeader("content-disposition", "attachment; filename=" + String.Format("{0}_{1}.zip", DateTime.Now.ToString("yyyyMMddHHmmssfff"), "T4"));
            else if (ExportType == "CICT4A")
                Response.AppendHeader("content-disposition", "attachment; filename=" + String.Format("{0}_{1}.zip", DateTime.Now.ToString("yyyyMMddHHmmssfff"), "T4A"));
            else if (ExportType == "CICR1")
                Response.AppendHeader("content-disposition", "attachment; filename=" + String.Format("{0}_{1}.zip", DateTime.Now.ToString("yyyyMMddHHmmssfff"), "R1"));

            Response.OutputStream.Write(output, 0, output.Length);
            Response.Flush();
            Response.End();
        }
    }
}