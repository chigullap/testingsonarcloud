﻿using System;
using System.Web.UI.WebControls;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.Export
{
    public partial class ExportControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private const String _provinceCodeKey = "ProvinceCodeKey";
        private String _provinceCode;
        #endregion

        #region properties
        protected bool EnableT4CICExportButton { get { return Common.Security.RoleForm.T4CICExport.ViewFlag && ExportType == "T4"; } }
        protected bool EnableT4ACICExportButton { get { return Common.Security.RoleForm.T4ACICExport.ViewFlag && ExportType == "T4A"; } }
        public bool IsEditMode { get { return ExportDetailView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool IsInsertMode { get { return ExportDetailView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsViewMode { get { return ExportDetailView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.T4Export.UpdateFlag; } }
        protected String ExportType { get { return (String)Page.RouteData.Values["exportType"]; } }
        private long YearEndId { get { return Convert.ToInt64(Page.RouteData.Values["yearEndId"]); } }
        private String Year { get { return Convert.ToString(Page.RouteData.Values["year"]); } }
        public String CurrentProvince
        {
            get
            {
                if (_provinceCode == null)
                {
                    Object obj = ViewState[_provinceCodeKey];

                    if (obj != null)
                        _provinceCode = (String)(obj);
                }

                return _provinceCode;
            }
            set
            {
                _provinceCode = value;
                ViewState[_provinceCodeKey] = _provinceCode;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.T4Export.ViewFlag);

            if (ExportType == "T4")
                this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "T4Export"));
            else if (ExportType == "T4A")
                this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "T4AExport"));
            else if (ExportType == "NR4")
                this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "NR4Export"));

            WireEvents();

            if (!IsPostBack)
            {
                if (YearEndId > 0) //should always be > 0 at this point
                {
                    //load data
                    LoadDetails();

                    if (Data.Count == 0)
                    {
                        GenerateDefaultObject();
                        ExportDetailView.ChangeMode(FormViewMode.Insert);
                    }
                }
            }
        }
        private void GenerateDefaultObject()
        {
            /*if no record exists in CanadaRevenueAgencyT4ExportCollection for supplied year_end_id, generate empty business objects with the following defaults:
                a)  CanadaRevenueAgencyT4Export YearEndFormCode=ExportType (passed in), YearEndId=current year_end_id
                b)  CanadaRevenueAgencyT619 CanadaRevenueAgencyT619TransmitterTypeCode="2", LanguageCode="EN"
                c)  PersonContactChannel (EMAIL) PrimaryFlag=1, ContactChannelTypeCode='BUSINESS'
                d)  PersonContactChannel (Phone) PrimaryFlag=1, ContactChannelTypeCode='WORK'
                e)  PersonAddress PersonAddressTypeCode="CA", PrimaryFlag=1
             */

            CanadaRevenueAgencyT4ExportCollection exportCollection = new CanadaRevenueAgencyT4ExportCollection();
            CanadaRevenueAgencyT4Export temp = new CanadaRevenueAgencyT4Export();

            //CanadaRevenueAgencyT4Export fields
            temp.YearEndFormCode = ExportType;
            temp.YearEndId = YearEndId;

            //RevenueCanadaAgencyT619 fields
            temp.CanadaRevenueAgencyT619 = new CanadaRevenueAgencyT619();
            temp.CanadaRevenueAgencyT619.CanadaRevenueAgencyT619TransmitterTypeCode = "2"; //2 = Service provider
            temp.CanadaRevenueAgencyT619.LanguageCode = "EN";

            //PersonContactChannel (Email)
            temp.CanadaRevenueAgencyT619.Email = new PersonContactChannel();
            temp.CanadaRevenueAgencyT619.Email.PrimaryFlag = true;
            temp.CanadaRevenueAgencyT619.Email.ContactChannelTypeCode = "BUSINESS";

            //PersonContactChannel (Phone)
            temp.CanadaRevenueAgencyT619.Phone = new PersonContactChannel();
            temp.CanadaRevenueAgencyT619.Phone.PrimaryFlag = true;
            temp.CanadaRevenueAgencyT619.Phone.ContactChannelTypeCode = "WORK";

            //PersonAddress
            temp.CanadaRevenueAgencyT619.PersonAddress = new PersonAddress();
            temp.CanadaRevenueAgencyT619.PersonAddress.PrimaryFlag = true;
            temp.CanadaRevenueAgencyT619.PersonAddress.PersonAddressTypeCode = "CA";

            //instantiate Person object
            temp.CanadaRevenueAgencyT619.Person = new Person();

            exportCollection.Add(temp);
            Data = exportCollection;
        }
        protected void WireEvents()
        {
            ExportDetailView.Inserting += new WLPFormView.ItemInsertingEventHandler(ExportDetailView_Inserting);
        }
        protected void LoadDetails()
        {
            Data = CanadaRevenueAgencyT4ExportCollection.ConvertCollection(Common.ServiceWrapper.XmlClient.ExportT4Details(Year, ExportType));
            ExportDetailView.DataBind();
        }
        #endregion

        #region event handlers
        protected void ExportDetailView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            ExportDetailView.DataSource = Data;
        }
        protected void CountryCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
            BindProvinceCombo(sender);
        }
        protected void CountryCode_SelectedIndexChanged(object o, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindProvinceCombo(o);
            ((ComboBoxControl)o).BindingContainer.FindControl("Email").Focus();
        }
        protected void ProvinceStateCode_SelectedIndexChanged(object o, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            CurrentProvince = e.Value;
        }
        protected void ExportDetailToolBar1_ButtonClick(object sender, Telerik.Web.UI.RadToolBarEventArgs e)
        {
            if (((Telerik.Web.UI.RadToolBarButton)(e.Item)).CommandName.Equals("Export"))
            {
                if (ExportType == "T4")
                    Session["HandOff"] = Common.ServiceWrapper.XmlClient.ExportT4XmlFile(Convert.ToInt32(Year), Common.ApplicationParameter.ImportExportProcessingDirectory,
                        Common.ApplicationParameter.ProprietorSocialInsuranceNumber1, Common.ApplicationParameter.ProprietorSocialInsuranceNumber2, ExportType);
                else if (ExportType == "T4A")
                    Session["HandOff"] = Common.ServiceWrapper.XmlClient.ExportT4aXmlFile(Convert.ToInt32(Year), Common.ApplicationParameter.ImportExportProcessingDirectory,
                        Common.ApplicationParameter.ProprietorSocialInsuranceNumber1, Common.ApplicationParameter.ProprietorSocialInsuranceNumber2, ExportType);
                else if (ExportType == "NR4")
                    Session["HandOff"] = Common.ServiceWrapper.XmlClient.ExportNR4XmlFile(Convert.ToInt32(Year), Common.ApplicationParameter.ImportExportProcessingDirectory,
                        Common.ApplicationParameter.ProprietorSocialInsuranceNumber1, Common.ApplicationParameter.ProprietorSocialInsuranceNumber2, ExportType);

                System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "invokeDownload", "invokeDownload('" + ExportType + "');", true);
            }
            //CIC T4 export
            else if (((Telerik.Web.UI.RadToolBarButton)(e.Item)).CommandName.Equals("T4CICExport"))
            {
                Session["HandOff"] = Common.ServiceWrapper.XmlClient.ExportT4CIC(Convert.ToInt32(Year), Common.ApplicationParameter.ProprietorSocialInsuranceNumber1,
                    Common.ApplicationParameter.ProprietorSocialInsuranceNumber2) ?? new byte[0];
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "invokeDownload", "invokeDownload('CICT4');", true);
            }
            //CIC T4A export
            else if (((Telerik.Web.UI.RadToolBarButton)(e.Item)).CommandName.Equals("T4ACICExport"))
            {
                Session["HandOff"] = Common.ServiceWrapper.XmlClient.ExportT4aCIC(Convert.ToInt32(Year), Common.ApplicationParameter.ProprietorSocialInsuranceNumber1,
                    Common.ApplicationParameter.ProprietorSocialInsuranceNumber2) ?? new byte[0];
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "invokeDownload", "invokeDownload('CICT4A');", true);
            }
        }
        protected void T4CICExport_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableT4CICExportButton; }
        protected void T4ACICExport_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableT4ACICExportButton; }
        #endregion

        #region handles updates
        void ExportDetailView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            CanadaRevenueAgencyT4Export info = (CanadaRevenueAgencyT4Export)e.DataItem;

            //since fields dont bind automatically, we cant use a cast on the e.DataItem correctly, so we are finding each textbox and extracting its value
            getData(info);

            //call the insert
            Common.ServiceWrapper.XmlClient.InsertExportT4Details(info, Year).CopyTo((CanadaRevenueAgencyT4Export)Data[info.Key]);
        }
        protected void ExportDetailView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                CanadaRevenueAgencyT4Export info = (CanadaRevenueAgencyT4Export)e.DataItem;

                //since fields dont bind automatically, we cant use a cast on the e.DataItem correctly, so we are finding each textbox and extracting its value
                getData(info);

                //call the update
                Common.ServiceWrapper.XmlClient.UpdateExportT4Details(info).CopyTo((CanadaRevenueAgencyT4Export)Data[info.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        private void BindProvinceCombo(object sender)
        {
            if (sender is ComboBoxControl)
            {
                ComboBoxControl countryCombo = (ComboBoxControl)sender;
                WLPFormView formItem = countryCombo.BindingContainer as WLPFormView;
                ComboBoxControl provinceCombo = (ComboBoxControl)formItem.FindControl("ProvinceCode");
                provinceCombo.DataSource = Common.ServiceWrapper.CodeClient.GetProvinceStateCode(countryCombo.SelectedValue);
                provinceCombo.DataBind();

                if (formItem.DataItem != null && formItem.DataItem is CanadaRevenueAgencyT4Export)
                {
                    provinceCombo.Value = ((CanadaRevenueAgencyT4Export)formItem.DataItem).CanadaRevenueAgencyT619.PersonAddress.ProvinceStateCode;
                    CurrentProvince = ((CanadaRevenueAgencyT4Export)formItem.DataItem).CanadaRevenueAgencyT619.PersonAddress.ProvinceStateCode;
                }
                else
                {
                    provinceCombo.Value = "";
                    CurrentProvince = null;
                }
            }
        }
        private CanadaRevenueAgencyT4Export getData(CanadaRevenueAgencyT4Export info)
        {
            //transmitter number
            BaseControl tempObj = (BaseControl)ExportDetailView.FindControl("TransmitterNumber");
            if (tempObj != null) info.CanadaRevenueAgencyT619.TransmitterNumber = (string)tempObj.Value;


            //string.IsNullOrEmpty(someString) ? "0" : someString;

            //address line 1
            tempObj = (BaseControl)ExportDetailView.FindControl("AddressLine1");
            if (tempObj != null) info.CanadaRevenueAgencyT619.PersonAddress.AddressLine1 = (string)tempObj.Value;

            //transmitter name
            tempObj = (BaseControl)ExportDetailView.FindControl("TransmitterName");
            if (tempObj != null) info.CanadaRevenueAgencyT619.TransmitterName = (string)tempObj.Value;

            //address line 2
            tempObj = (BaseControl)ExportDetailView.FindControl("AddressLine2");
            if (tempObj != null) info.CanadaRevenueAgencyT619.PersonAddress.AddressLine2 = (string)tempObj.Value;

            //first name
            tempObj = (BaseControl)ExportDetailView.FindControl("ContactFirstName");
            if (tempObj != null) info.CanadaRevenueAgencyT619.Person.FirstName = (string)tempObj.Value;

            //city
            tempObj = (BaseControl)ExportDetailView.FindControl("City");
            if (tempObj != null) info.CanadaRevenueAgencyT619.PersonAddress.City = (string)tempObj.Value;

            //last name
            tempObj = (BaseControl)ExportDetailView.FindControl("ContactLastName");
            if (tempObj != null) info.CanadaRevenueAgencyT619.Person.LastName = (string)tempObj.Value;

            //country
            ComboBoxControl tempCombo = (ComboBoxControl)ExportDetailView.FindControl("CountryCode");
            if (tempCombo != null) info.CanadaRevenueAgencyT619.PersonAddress.CountryCode = tempCombo.SelectedValue.ToString();

            //phone number
            tempObj = (BaseControl)ExportDetailView.FindControl("Phone");
            if (tempObj != null) info.CanadaRevenueAgencyT619.Phone.PrimaryContactValue = (string)tempObj.Value;

            //email
            tempObj = (BaseControl)ExportDetailView.FindControl("Email");
            if (tempObj != null) info.CanadaRevenueAgencyT619.Email.PrimaryContactValue = (string)tempObj.Value;

            //province, kept track in session
            info.CanadaRevenueAgencyT619.PersonAddress.ProvinceStateCode = CurrentProvince;

            //phone extension
            tempObj = (BaseControl)ExportDetailView.FindControl("Extension");
            if (tempObj != null) info.CanadaRevenueAgencyT619.Phone.SecondaryContactValue = (string)tempObj.Value;

            //postal/zip code
            tempObj = (BaseControl)ExportDetailView.FindControl("Postal");
            if (tempObj != null) info.CanadaRevenueAgencyT619.PersonAddress.PostalZipCode = (string)tempObj.Value;

            return info;
        }
    }
}