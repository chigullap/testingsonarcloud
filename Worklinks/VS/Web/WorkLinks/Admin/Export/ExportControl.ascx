﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExportControl.ascx.cs" Inherits="WorkLinks.Admin.Export.ExportControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="SearchPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SearchPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="SearchPanel" runat="server">
    <wasp:WLPFormView
        ID="ExportDetailView"
        runat="server"
        RenderOuterTable="False"
        DataKeyNames="Key"
        OnNeedDataSource="ExportDetailView_NeedDataSource"
        OnUpdating="ExportDetailView_Updating">

        <ItemTemplate>
            <wasp:WLPToolBar ID="ExportDetailToolBar1" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="ExportDetailToolBar1_ButtonClick">
                <Items>
                    <wasp:WLPToolBarButton Text="Edit" ImageUrl="~/App_Themes/Default/Edit.gif" Visible='<%# UpdateFlag %>' CommandName="edit" ResourceName="Edit" CausesValidation="false"></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton Text="Export" CommandName="Export" ResourceName="Export" CausesValidation="false"></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton Text="*T4CICExport" CommandName="T4CICExport" ResourceName="T4CICExport" CausesValidation="false" OnPreRender="T4CICExport_PreRender"></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton Text="*T4ACICExport" CommandName="T4ACICExport" ResourceName="T4ACICExport" CausesValidation="false" OnPreRender="T4ACICExport_PreRender"></wasp:WLPToolBarButton>
                </Items>
            </wasp:WLPToolBar>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="TransmitterNumber" runat="server" LabelText="TransmitterNumber" ResourceName="TransmitterNumber" Value='<%# Bind("CanadaRevenueAgencyT619.TransmitterNumber") %>' ReadOnly="True" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="AddressLine1" runat="server" LabelText="AddressLine1" ResourceName="AddressLine1" Value='<%# Bind("CanadaRevenueAgencyT619.PersonAddress.AddressLine1") %>' ReadOnly="True" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="TransmitterName" runat="server" LabelText="TransmitterName" ResourceName="TransmitterName" Value='<%# Bind("CanadaRevenueAgencyT619.TransmitterName") %>' ReadOnly="True" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="AddressLine2" runat="server" LabelText="AddressLine2" ResourceName="AddressLine2" Value='<%# Bind("CanadaRevenueAgencyT619.PersonAddress.AddressLine2") %>' ReadOnly="True" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="ContactFirstName" runat="server" LabelText="ContactFirstName" ResourceName="ContactFirstName" Value='<%# Bind("CanadaRevenueAgencyT619.Person.FirstName") %>' ReadOnly="True" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="City" runat="server" LabelText="City" ResourceName="City" Value='<%# Bind("CanadaRevenueAgencyT619.PersonAddress.City") %>' ReadOnly="True" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="ContactLastName" runat="server" LabelText="ContactLastName" ResourceName="ContactLastName" Value='<%# Bind("CanadaRevenueAgencyT619.Person.LastName") %>' ReadOnly="True" />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="CountryCode" runat="server" Type="CountryCode" AutoPostback="true" OnDataBinding="CountryCode_NeedDataSource" OnSelectedIndexChanged="CountryCode_SelectedIndexChanged" ResourceName="CountryCode" Value='<%# Bind("CanadaRevenueAgencyT619.PersonAddress.CountryCode") %>' ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="Email" runat="server" LabelText="Email" ResourceName="Email" Value='<%# Bind("CanadaRevenueAgencyT619.Email.PrimaryContactValue") %>' ReadOnly="True" />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="ProvinceCode" runat="server" Type="ProvinceStateCode" ResourceName="ProvinceStateCode" OnSelectedIndexChanged="ProvinceStateCode_SelectedIndexChanged" ReadOnly="true"></wasp:ComboBoxControl>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:MaskedControl Mask="###-###-####" ID="Phone" runat="server" LabelText="Phone" ResourceName="Phone" Value='<%# Bind("CanadaRevenueAgencyT619.Phone.PrimaryContactValue") %>' ReadOnly="True" />

                        </td>
                        <td>
                            <wasp:TextBoxControl ID="Postal" runat="server" LabelText="Postal" ResourceName="Postal" Value='<%# Bind("CanadaRevenueAgencyT619.PersonAddress.PostalZipCode") %>' ReadOnly="True" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="Extension" runat="server" LabelText="Extension" ResourceName="Extension" Value='<%# Bind("CanadaRevenueAgencyT619.Phone.SecondaryContactValue") %>' ReadOnly="True" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </ItemTemplate>

        <EditItemTemplate>
            <wasp:WLPToolBar ID="ExportDetailToolBar2" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update"></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert" />
                    <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" Visible='<%# !IsInsertMode %>' CommandName="cancel" CausesValidation="False" ResourceName="Cancel"></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" Visible='<%# IsInsertMode %>' onClick="Close()" CausesValidation="False" ResourceName="Cancel"></wasp:WLPToolBarButton>
                </Items>
            </wasp:WLPToolBar>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="TransmitterNumber" runat="server" LabelText="TransmitterNumber" ResourceName="TransmitterNumber" Value='<%# Bind("CanadaRevenueAgencyT619.TransmitterNumber") %>' ReadOnly="False" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="AddressLine1" runat="server" LabelText="AddressLine1" ResourceName="AddressLine1" Value='<%# Bind("CanadaRevenueAgencyT619.PersonAddress.AddressLine1") %>' ReadOnly="False" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="TransmitterName" runat="server" LabelText="TransmitterName" ResourceName="TransmitterName" Value='<%# Bind("CanadaRevenueAgencyT619.TransmitterName") %>' ReadOnly="False" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="AddressLine2" runat="server" LabelText="AddressLine2" ResourceName="AddressLine2" Value='<%# Bind("CanadaRevenueAgencyT619.PersonAddress.AddressLine2") %>' ReadOnly="False" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="ContactFirstName" runat="server" LabelText="ContactFirstName" ResourceName="ContactFirstName" Value='<%# Bind("CanadaRevenueAgencyT619.Person.FirstName") %>' ReadOnly="False" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="City" runat="server" LabelText="City" ResourceName="City" Value='<%# Bind("CanadaRevenueAgencyT619.PersonAddress.City") %>' ReadOnly="False" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="ContactLastName" runat="server" LabelText="ContactLastName" ResourceName="ContactLastName" Value='<%# Bind("CanadaRevenueAgencyT619.Person.LastName") %>' ReadOnly="False" />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="CountryCode" runat="server" Type="CountryCode" AutoPostback="true" OnDataBinding="CountryCode_NeedDataSource" OnSelectedIndexChanged="CountryCode_SelectedIndexChanged" ResourceName="CountryCode" Value='<%# Bind("CanadaRevenueAgencyT619.PersonAddress.CountryCode") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="Email" runat="server" LabelText="Email" ResourceName="Email" Value='<%# Bind("CanadaRevenueAgencyT619.Email.PrimaryContactValue") %>' ReadOnly="False" />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="ProvinceCode" runat="server" Type="ProvinceStateCode" ResourceName="ProvinceStateCode" OnSelectedIndexChanged="ProvinceStateCode_SelectedIndexChanged" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:MaskedControl Mask="###-###-####" ID="Phone" runat="server" LabelText="Phone" ResourceName="Phone" Value='<%# Bind("CanadaRevenueAgencyT619.Phone.PrimaryContactValue") %>' ReadOnly="False" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="Postal" runat="server" LabelText="Postal" ResourceName="Postal" Value='<%# Bind("CanadaRevenueAgencyT619.PersonAddress.PostalZipCode") %>' ReadOnly="False" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="float: left">
                                <wasp:TextBoxControl ID="Extension" runat="server" ResourceName="Extension" Value='<%# Bind("CanadaRevenueAgencyT619.Phone.SecondaryContactValue") %>' ReadOnly="False" />
                            </div>
                            <div style="float: left; padding-left: 50px;">
                                <asp:RegularExpressionValidator ID="ExtensionLength" runat="server" ForeColor="Red" ControlToValidate="Extension" ErrorMessage="Not Valid" ValidationExpression="^[0-9]{0,9}$">* Not Valid</asp:RegularExpressionValidator>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </EditItemTemplate>
    </wasp:WLPFormView>
</asp:Panel>

<script type="text/javascript">
    function getRadWindow() {
        var popup = null;
        if (window.radWindow)
            popup = window.radWindow;
        else if (window.frameElement.radWindow)
            popup = window.frameElement.radWindow;
        return popup;
    }

    function Close(button, args) {
        var popup = getRadWindow();
        setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
    }

    function invokeDownload(type) {
        var frame = document.createElement("frame");
        frame.src = getUrl('/YearEnd/DownLoadXml/' + type), null;
        frame.style.display = "none";
        document.body.appendChild(frame);
    }

</script>