﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using System.Web.UI.WebControls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.VendorAdmin
{
    public partial class VendorControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public bool IsViewMode { get { return VendorInfoGrid.IsViewMode; } }
        public bool IsUpdate { get { return VendorInfoGrid.IsEditMode; } }
        public bool IsInsert { get { return VendorInfoGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.Vendor.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.Vendor.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.Vendor.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.Vendor.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                LoadVendor();
                Initialize();
            }
        }
        private void WireEvents()
        {
            VendorInfoGrid.NeedDataSource += new GridNeedDataSourceEventHandler(VendorInfoGrid_NeedDataSource);
        }
        protected void LoadVendor()
        {
            Data = VendorCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetVendor(-1)); //-1 gets all vendors
        }
        protected void Initialize()
        {
            //find the VendorInfoGrid
            WLPGrid grid = (WLPGrid)this.FindControl("VendorInfoGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        #endregion

        #region event handlers
        void VendorInfoGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            VendorInfoGrid.DataSource = Data;
        }
        protected void CountryCode_NeedDataSource(object sender, EventArgs e)
        {
            Code_NeedDataSource(sender, e);
            BindProvinceCombo(sender);
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        private void BindProvinceCombo(object sender)
        {
            if (sender is ComboBoxControl)
            {
                ComboBoxControl countryCombo = (ComboBoxControl)sender;
                GridEditFormItem formItem = (GridEditFormItem)countryCombo.BindingContainer;
                ComboBoxControl provinceCombo = (ComboBoxControl)formItem.FindControl("ProvinceStateCode");

                provinceCombo.DataSource = Common.ServiceWrapper.CodeClient.GetProvinceStateCode(countryCombo.SelectedValue);
                provinceCombo.DataBind();

                SetProvincePostalMandatory(formItem, countryCombo, provinceCombo);

                if (formItem.DataItem != null && formItem.DataItem is Vendor)
                    provinceCombo.Value = ((Vendor)formItem.DataItem).ProvinceStateCode;
            }
        }
        protected void SetProvincePostalMandatory(GridEditFormItem item, ComboBoxControl countryCode, ComboBoxControl provinceStateCode)
        {
            //if country is CA or US, set province and postal/zip to mandatory = true
            TextBoxControl postalZipCode = (TextBoxControl)item.FindControl("PostalZipCode");

            if (countryCode.Value != null && (countryCode.Value.ToString() == "CA" || countryCode.Value.ToString() == "US"))
            {
                provinceStateCode.Mandatory = true;

                if (postalZipCode != null)
                    postalZipCode.Mandatory = true;
            }
            else
            {
                provinceStateCode.Mandatory = false;

                if (postalZipCode != null)
                    postalZipCode.Mandatory = false;
            }
        }
        protected void PostalZipCodeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = false;
            string postalZipCode = "";
            string countryCode = "";

            if (args.Value != null)
                postalZipCode = args.Value.ToString();

            ComboBoxControl countryCodeCombo = (ComboBoxControl)((WebControl)source).NamingContainer.FindControl("CountryCode");
            if (countryCodeCombo.Value != null)
                countryCode = countryCodeCombo.Value.ToString();

            if (!String.IsNullOrEmpty(postalZipCode) && (!String.IsNullOrEmpty(countryCode)))
            {
                //CAN Postal Code or US Zip Code?
                if (countryCode == "CA")
                    args.IsValid = System.Text.RegularExpressions.Regex.IsMatch(postalZipCode, @"\A[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY]\d[a-zA-Z] ?\d[a-zA-Z]\d\z");
                else if (countryCode == "US")
                    args.IsValid = System.Text.RegularExpressions.Regex.IsMatch(postalZipCode, @"^(\d{5}-\d{4}|\d{5}|\d{9})$|^([a-zA-Z]\d[a-zA-Z] \d[a-zA-Z]\d)$");
                else
                    args.IsValid = true; //no validation on other countries
            }
            else if (String.IsNullOrEmpty(postalZipCode))
                args.IsValid = true; //no validation on empty entries; if postal is mandatory, the mandatory validator will catch it
            else
                args.IsValid = false;
        }
        protected void PhoneNumberValidator_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
        {
            String phoneNumber = args.Value.ToString().Replace(" ", "").ToUpper();
            args.IsValid = (phoneNumber.Length == 10 || phoneNumber.Length == 0);
        }
        protected void VendorInfoGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //use the default address from the address table
            if (e.Item is GridEditFormInsertItem)
                SetAddress((GridEditFormItem)e.Item, Common.ServiceWrapper.HumanResourcesClient.GetAddress(Convert.ToInt64(Common.ApplicationParameter.DefaultCRAAddress)));
        }
        protected void CountryCode_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindProvinceCombo(sender);
        }
        protected void GarnishmentTypeCode_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (sender is ComboBoxControl)
            {
                ComboBoxControl garnishmentTypeCode = (ComboBoxControl)sender;

                if (garnishmentTypeCode.BindingContainer is GridEditFormInsertItem)
                {
                    AddressCollection collection = new AddressCollection();

                    //if the garnishment type is CRA, use the default address from the address table
                    if (garnishmentTypeCode.Value.ToString() == "CRA")
                        collection = Common.ServiceWrapper.HumanResourcesClient.GetAddress(Convert.ToInt64(Common.ApplicationParameter.DefaultCRAAddress));
                    else
                        collection.AddNew();

                    SetAddress((GridEditFormItem)garnishmentTypeCode.BindingContainer, collection);
                }
            }
        }
        protected void SetAddress(GridEditFormItem item, AddressCollection collection)
        {
            TextBoxControl addressLine1 = (TextBoxControl)item.FindControl("AddressLine1");
            if (addressLine1 != null)
                addressLine1.Value = collection[0].AddressLine1;

            TextBoxControl addressLine2 = (TextBoxControl)item.FindControl("AddressLine2");
            if (addressLine2 != null)
                addressLine2.Value = collection[0].AddressLine2;

            TextBoxControl city = (TextBoxControl)item.FindControl("City");
            if (city != null)
                city.Value = collection[0].City;

            ComboBoxControl countryCode = (ComboBoxControl)item.FindControl("CountryCode");
            if (countryCode != null)
                countryCode.Value = collection[0].CountryCode;

            ComboBoxControl provinceStateCode = (ComboBoxControl)item.FindControl("ProvinceStateCode");
            if (provinceStateCode != null)
            {
                provinceStateCode.DataSource = Common.ServiceWrapper.CodeClient.GetProvinceStateCode(countryCode.SelectedValue);
                provinceStateCode.DataBind();
                provinceStateCode.Value = collection[0].ProvinceStateCode;
            }

            TextBoxControl postalZipCode = (TextBoxControl)item.FindControl("PostalZipCode");
            if (postalZipCode != null)
                postalZipCode.Value = collection[0].PostalZipCode;

            SetProvincePostalMandatory(item, countryCode, provinceStateCode);
        }
        #endregion

        #region handle updates
        private void FormatPostalZipCode(Vendor vendor)
        {
            //remove spaces and then capitalize the letters in the postal code
            if (vendor.CountryCode == "CA")
            {
                if (vendor.PostalZipCode != null)
                {
                    vendor.PostalZipCode = vendor.PostalZipCode.Replace(" ", "");
                    vendor.PostalZipCode = vendor.PostalZipCode.ToUpper();
                }
            }
            else if (vendor.CountryCode == "US")
            {
                if (vendor.PostalZipCode != null && vendor.PostalZipCode.Length > 5)
                {
                    vendor.PostalZipCode = vendor.PostalZipCode.Replace(" ", "");
                    vendor.PostalZipCode = vendor.PostalZipCode.Replace("-", "");
                }
            }
            else
            {
                if (vendor.PostalZipCode != null)
                    vendor.PostalZipCode = vendor.PostalZipCode.ToUpper();
            }
        }
        protected void VendorInfoGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            Vendor vendor = (Vendor)e.Item.DataItem;
            FormatPostalZipCode(vendor);
            Common.ServiceWrapper.HumanResourcesClient.InsertVendor(vendor).CopyTo((Vendor)Data[vendor.Key]);
        }
        protected void VendorInfoGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Vendor vendor = (Vendor)e.Item.DataItem;
                FormatPostalZipCode(vendor);
                Common.ServiceWrapper.HumanResourcesClient.UpdateVendor(vendor).CopyTo((Vendor)Data[vendor.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void VendorInfoGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteVendor((Vendor)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    LoadVendor();
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}