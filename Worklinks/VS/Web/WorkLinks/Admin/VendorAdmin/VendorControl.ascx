﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VendorControl.ascx.cs" Inherits="WorkLinks.Admin.VendorAdmin.VendorControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPGrid
        ID="VendorInfoGrid"
        runat="server"
        AllowPaging="true"
        PagerStyle-AlwaysVisible="true"
        PageSize="100"
        GridLines="None"
        AutoGenerateColumns="false"
        OnDeleteCommand="VendorInfoGrid_DeleteCommand"
        OnInsertCommand="VendorInfoGrid_InsertCommand"
        OnUpdateCommand="VendorInfoGrid_UpdateCommand"
        OnItemDataBound="VendorInfoGrid_ItemDataBound"
        AutoAssignModifyProperties="true">

        <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="true">
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="false" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="Number" SortOrder="Ascending" />
            </SortExpressions>

            <CommandItemTemplate>
                <wasp:WLPToolBar ID="EmployeeSummaryToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
                    <Items>
                        <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add" />
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
                <wasp:GridBoundControl DataField="Number" LabelText="**Number**" SortExpression="Number" UniqueName="Number" ResourceName="Number">
                    <HeaderStyle Width="10%" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="Name" LabelText="**Name**" SortExpression="Name" UniqueName="Name" ResourceName="Name">
                    <HeaderStyle Width="15%" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="MailingLabel" LabelText="**MailingLabel**" SortExpression="MailingLabel" UniqueName="MailingLabel" ResourceName="MailingLabel">
                    <HeaderStyle Width="20%" />
                </wasp:GridBoundControl>
                <wasp:GridKeyValueControl OnNeedDataSource="CountryCode_NeedDataSource" LabelText="**CountryCode**" DataField="CountryCode" Type="CountryCode" ResourceName="CountryCode">
                    <HeaderStyle Width="10%" />
                </wasp:GridKeyValueControl>
                <wasp:GridBoundControl DataField="PhoneNumber" LabelText="**PhoneNumber**" SortExpression="PhoneNumber" UniqueName="PhoneNumber" ResourceName="PhoneNumber">
                    <HeaderStyle Width="10%" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="PhoneNumberExtension" LabelText="**PhoneNumberExtension**" SortExpression="PhoneNumberExtension" UniqueName="PhoneNumberExtension" ResourceName="PhoneExtension">
                    <HeaderStyle Width="10%" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="EmailAddress" LabelText="**EmailAddress**" SortExpression="EmailAddress" UniqueName="EmailAddress" ResourceName="Email">
                    <HeaderStyle Width="15%" />
                </wasp:GridBoundControl>
                <wasp:GridKeyValueControl DataField="GarnishmentTypeCode" OnNeedDataSource="Code_NeedDataSource" LabelText="**GarnishmentTypeCode**" Type="GarnishmentTypeCode" ResourceName="GarnishmentTypeCode">
                    <HeaderStyle Width="10%" />
                </wasp:GridKeyValueControl>
                <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="Number" Text="**Number**" runat="server" MaxLength="53" ResourceName="Number" Value='<%# Bind("Number") %>' Mandatory="true" TabIndex="010" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="Name" Text="**Name**" runat="server" MaxLength="30" ResourceName="Name" Value='<%# Bind("Name") %>' Mandatory="true" TabIndex="020" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="AddressLine1" Text="**AddressLine1**" MaxLength="34" runat="server" ResourceName="AddressLine1" Value='<%# Bind("AddressLine1") %>' Mandatory="true" TabIndex="030" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="City" Text="**City**" runat="server" MaxLength="30" ResourceName="City" Value='<%# Bind("City") %>' Mandatory="true" TabIndex="040" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="AddressLine2" Text="**AddressLine2**" MaxLength="128" runat="server" ResourceName="AddressLine2" Value='<%# Bind("AddressLine2") %>' TabIndex="050" />
                            </td>
                            <td>
                                <wasp:ComboBoxControl ID="CountryCode" Text="**CountryCode**" runat="server" Type="CountryCode" OnDataBinding="CountryCode_NeedDataSource" AutoPostback="true" OnSelectedIndexChanged="CountryCode_SelectedIndexChanged" ResourceName="CountryCode" Value='<%# Bind("CountryCode") %>' Mandatory="true" TabIndex="060" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="GarnishmentTypeCode" Text="**GarnishmentTypeCode**" runat="server" Type="GarnishmentTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="GarnishmentTypeCode" Value='<%# Bind("GarnishmentTypeCode") %>' AutoPostback="true" OnSelectedIndexChanged="GarnishmentTypeCode_SelectedIndexChanged" Mandatory="true" TabIndex="070" IncludeEmptyItem="false" />
                            </td>
                            <td>
                                <wasp:ComboBoxControl ID="ProvinceStateCode" Text="**ProvinceStateCode**" runat="server" Type="ProvinceStateCode" ResourceName="ProvinceStateCode" Value='<%# Bind("ProvinceStateCode") %>' TabIndex="080" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="Email" Text="**Email**" runat="server" ResourceName="Email" Value='<%# Bind("EmailAddress") %>' ReadOnly="false" TabIndex="090" />
                                <asp:RegularExpressionValidator ID="EmailValidator" runat="server" Display="Dynamic" ForeColor="Red" ControlToValidate="Email" ErrorMessage="Not Valid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">* Invalid Email Address</asp:RegularExpressionValidator>
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="PostalZipCode" Text="**PostalZipCode**" MaxLength="7" runat="server" ResourceName="PostalZipCode" Value='<%# Bind("PostalZipCode") %>' TabIndex="100" />
                                <asp:CustomValidator ID="PostalZipCodeValidator" ControlToValidate="PostalZipCode" runat="server" Display="Dynamic" ErrorMessage="**Invalid Postal/Zip Code**" ForeColor="Red" OnServerValidate="PostalZipCodeValidator_ServerValidate">* Invalid Postal/Zip Code</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:MaskedControl ID="PhoneNumber" Text="**PhoneNumber**" Mask="###-###-####" runat="server" ResourceName="PhoneNumber" Value='<%# Bind("PhoneNumber") %>' ReadOnly="false" TabIndex="110" />
                                <asp:CustomValidator ID="PhoneNumberValidator" ControlToValidate="PhoneNumber" runat="server" Display="Dynamic" ErrorMessage="**Invalid Phone Number**" ForeColor="Red" OnServerValidate="PhoneNumberValidator_ServerValidate">* Invalid Phone Number</asp:CustomValidator>
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="PhoneExtension" Text="**PhoneExtension**" runat="server" ResourceName="PhoneExtension" Value='<%# Bind("PhoneNumberExtension") %>' ReadOnly="false" TabIndex="120"  />
                                <asp:RegularExpressionValidator ID="PhoneExtensionValidator" runat="server" Display="Dynamic" ForeColor="Red" ControlToValidate="PhoneExtension" ErrorMessage="Not Valid" ValidationExpression="^[0-9]{0,9}$">* Invalid Phone Extension</asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                            <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsInsert %>' ResourceName="Insert" />
                                        </td>
                                        <td>
                                            <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="Cancel" CausesValidation="false" ResourceName="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true" />

    </wasp:WLPGrid>
</asp:Panel>