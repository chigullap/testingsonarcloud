﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaycodeAssociationControl.ascx.cs" Inherits="WorkLinks.Admin.PaycodeAssociationAdmin.PaycodeAssociationControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="PaycodeAssociationTypeGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="PaycodeAssociationTypeGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy2" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="PaycodeAssociationTypeGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="PaycodeAssociationGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy3" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="PaycodeAssociationGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="PaycodeAssociationTypeGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy4" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="PaycodeAssociationGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="PaycodeAssociationGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<wasp:WLPGrid
    ID="PaycodeAssociationTypeGrid"
    runat="server"
    GridLines="None"
    Width="100%"
    AutoGenerateColumns="false"
    OnDataBound="PaycodeAssociationTypeGrid_DataBound"
    OnInsertCommand="PaycodeAssociationTypeGrid_InsertCommand"
    OnUpdateCommand="PaycodeAssociationTypeGrid_UpdateCommand"
    OnDeleteCommand="PaycodeAssociationTypeGrid_DeleteCommand">

    <ClientSettings EnablePostBackOnRowClick="true" AllowKeyboardNavigation="true">
        <Selecting AllowRowSelect="true" />
        <Scrolling AllowScroll="false" UseStaticHeaders="true" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
        <SortExpressions>
            <telerik:GridSortExpression FieldName="PaycodeAssociationTypeCode" SortOrder="Ascending" />
        </SortExpressions>

        <CommandItemTemplate>
            <wasp:WLPToolBar ID="PaycodeAssociationTypeGridToolBar" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewModeType && AddFlag %>' ResourceName="Add" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <Columns>
            <wasp:GridEditCommandControl UniqueName="EditButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif">
                <HeaderStyle Width="5%" />
            </wasp:GridEditCommandControl>
            <wasp:GridBoundControl DataField="PaycodeAssociationTypeCode" LabelText="Type*" SortExpression="PaycodeAssociationTypeCode" UniqueName="PaycodeAssociationTypeCode" ResourceName="PaycodeAssociationTypeCode">
                <HeaderStyle Width="20%" />
            </wasp:GridBoundControl>
            <wasp:GridBoundControl DataField="Description" LabelText="Description*" SortExpression="Description" UniqueName="Description" ResourceName="Description">
                <HeaderStyle Width="70%" />
            </wasp:GridBoundControl>
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>">
                <HeaderStyle Width="5%" />
            </wasp:GridButtonControl>
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="PaycodeAssociationTypeCode" Text="**PaycodeAssociationTypeCode**" runat="server" ResourceName="PaycodeAssociationTypeCode" Value='<%# Bind("PaycodeAssociationTypeCode") %>' ReadOnly='<%# !IsInsertType %>' Mandatory="true" TabIndex="010" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="Description" Text="**Description**" FieldWidth="350px" Width="700px" runat="server" ResourceName="Description" Value='<%# Bind("Description") %>' ReadOnly="false" Mandatory="true" TabIndex="020" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdateType %>' ResourceName="Update" />
                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsInsertType %>' ResourceName="Insert" />
                                    </td>
                                    <td>
                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="Cancel" CausesValidation="false" ResourceName="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>
    </MasterTableView>

    <HeaderContextMenu EnableAutoScroll="true" />

</wasp:WLPGrid>

<wasp:WLPGrid
    ID="PaycodeAssociationGrid"
    runat="server"
    GridLines="None"
    Width="100%"
    AutoGenerateColumns="false"
    OnInsertCommand="PaycodeAssociationGrid_InsertCommand"
    OnUpdateCommand="PaycodeAssociationGrid_UpdateCommand"
    OnDeleteCommand="PaycodeAssociationGrid_DeleteCommand">

    <ClientSettings EnablePostBackOnRowClick="false" AllowKeyboardNavigation="true">
        <Selecting AllowRowSelect="false" />
        <Scrolling AllowScroll="false" UseStaticHeaders="true" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
        <SortExpressions>
            <telerik:GridSortExpression FieldName="PaycodeCode" SortOrder="Ascending" />
        </SortExpressions>

        <CommandItemTemplate>
            <wasp:WLPToolBar ID="PaycodeAssociationGridToolBar" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <Columns>
            <wasp:GridEditCommandControl UniqueName="EditButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif">
                <HeaderStyle Width="5%" />
            </wasp:GridEditCommandControl>
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="PaycodeCode" LabelText="**PaycodeCode**" Type="PaycodeCode" ResourceName="PaycodeCode">
                <HeaderStyle Width="30%" />
            </wasp:GridKeyValueControl>
            <wasp:GridBoundControl DataField="ExternalCode" LabelText="**ExternalCode**" SortExpression="ExternalCode" UniqueName="ExternalCode" ResourceName="ExternalCode">
                <HeaderStyle Width="30%" />
            </wasp:GridBoundControl>
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="ProvinceStateCode" LabelText="**ProvinceStateCode**" Type="ProvinceStateCode" ResourceName="ProvinceStateCode">
                <HeaderStyle Width="30%" />
            </wasp:GridKeyValueControl>
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>">
                <HeaderStyle Width="5%" />
            </wasp:GridButtonControl>
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table width="100%">
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <wasp:ComboBoxControl ID="PaycodeCode" LabelText="**PaycodeCode**" runat="server" Type="PaycodeCode" OnDataBinding="PaycodeCode_DataBinding" ResourceName="PaycodeCode" Value='<%# Bind("PaycodeCode") %>' ReadOnly='<%# !IsInsert %>' Mandatory="true" TabIndex="010" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <wasp:TextBoxControl ID="ExternalCode" Text="**ExternalCode**" runat="server" ResourceName="ExternalCode" Value='<%# Bind("ExternalCode") %>' TabIndex="020" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <wasp:ComboBoxControl ID="ProvinceStateCode" LabelText="**ProvinceStateCode**" runat="server" Type="ProvinceStateCode" OnDataBinding="ProvinceStateCode_NeedDataSource" ResourceName="ProvinceStateCode" Value='<%# Bind("ProvinceStateCode") %>' TabIndex="030" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsInsert %>' ResourceName="Insert" />
                                    </td>
                                    <td>
                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="Cancel" CausesValidation="false" ResourceName="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>
    </MasterTableView>

    <HeaderContextMenu EnableAutoScroll="true" />

</wasp:WLPGrid>