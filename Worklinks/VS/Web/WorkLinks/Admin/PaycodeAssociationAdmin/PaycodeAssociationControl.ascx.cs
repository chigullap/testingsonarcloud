﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.PaycodeAssociationAdmin
{
    public partial class PaycodeAssociationControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private PaycodeAssociationCollection _collection = new PaycodeAssociationCollection(); //used to populate child grid
        #endregion

        #region properties
        private String SelectedType { get { return PaycodeAssociationTypeGrid.SelectedValue == null ? null : Convert.ToString(PaycodeAssociationTypeGrid.SelectedValue); } }
        public bool IsViewModeType { get { return PaycodeAssociationTypeGrid.IsViewMode; } }
        public bool IsUpdateType { get { return PaycodeAssociationTypeGrid.IsEditMode; } }
        public bool IsInsertType { get { return PaycodeAssociationTypeGrid.IsInsertMode; } }
        public bool IsViewMode { get { return PaycodeAssociationGrid.IsViewMode; } }
        public bool IsUpdate { get { return PaycodeAssociationGrid.IsEditMode; } }
        public bool IsInsert { get { return PaycodeAssociationGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.PaycodeAssociation.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.PaycodeAssociation.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.PaycodeAssociation.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.PaycodeAssociation.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                LoadPaycodeAssociationTypeGridInfo();
                Initialize();
            }
        }
        protected void WireEvents()
        {
            PaycodeAssociationTypeGrid.NeedDataSource += new GridNeedDataSourceEventHandler(PaycodeAssociationTypeGrid_NeedDataSource);
            PaycodeAssociationTypeGrid.ItemDataBound += PaycodeAssociationTypeGrid_ItemDataBound;
            PaycodeAssociationTypeGrid.SelectedIndexChanged += new EventHandler(PaycodeAssociationTypeGrid_SelectedIndexChanged);
            PaycodeAssociationTypeGrid.PreRender += new EventHandler(PaycodeAssociationTypeGrid_PreRender);

            PaycodeAssociationGrid.NeedDataSource += new GridNeedDataSourceEventHandler(PaycodeAssociationGrid_NeedDataSource);
            PaycodeAssociationGrid.PreRender += new EventHandler(PaycodeAssociationGrid_PreRender);
        }
        protected void Initialize()
        {
            //hide the edit images on the type grid
            WLPGrid paycodeAssociationTypeGrid = (WLPGrid)this.FindControl("PaycodeAssociationTypeGrid");
            paycodeAssociationTypeGrid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;

            //hide the edit/delete images on the association grid
            WLPGrid paycodeAssociationGrid = (WLPGrid)this.FindControl("PaycodeAssociationGrid");
            paycodeAssociationGrid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            paycodeAssociationGrid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        protected void LoadPaycodeAssociationTypeGridInfo()
        {
            Data = PaycodeAssociationTypeCollection.ConvertCollection(Common.ServiceWrapper.CodeClient.GetPaycodeAssociationType(null));
        }
        #endregion

        #region event handlers
        void PaycodeAssociationTypeGrid_PreRender(object sender, EventArgs e)
        {
            SetGridEnabledMode(PaycodeAssociationGrid, IsViewModeType, false);
        }
        void PaycodeAssociationTypeGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            PaycodeAssociationTypeGrid.DataSource = Data;
        }
        protected void PaycodeAssociationTypeGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            PaycodeAssociationGrid.Rebind();
        }
        protected void PaycodeAssociationTypeGrid_DataBound(object sender, EventArgs e)
        {
            if (!IsPostBack && PaycodeAssociationTypeGrid.Items.Count > 0) //select the first row when loading the screen for the first time
                PaycodeAssociationTypeGrid.Items[0].Selected = true;
        }
        void PaycodeAssociationTypeGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                //hide the delete image if there is a dependency
                GridDataItem dataItem = e.Item as GridDataItem;
                String keyId = dataItem.GetDataKeyValue("Key").ToString();
                PaycodeAssociationType paycodeAssociationType = Common.ServiceWrapper.CodeClient.GetPaycodeAssociationType(keyId)[0];

                dataItem["deleteButton"].Controls[0].Visible = !paycodeAssociationType.HasChildrenFlag;
            }
        }
        void PaycodeAssociationGrid_PreRender(object sender, EventArgs e)
        {
            SetGridEnabledMode(PaycodeAssociationTypeGrid, IsViewMode, IsViewModeType ? IsViewMode : IsViewModeType);
        }
        void PaycodeAssociationGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (SelectedType != null)
            {
                _collection.Clear();
                _collection.Load(Common.ServiceWrapper.CodeClient.GetPaycodeAssociation(Convert.ToString(SelectedType)));

                PaycodeAssociationGrid.DataSource = _collection;
            }
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void PaycodeCode_DataBinding(object sender, EventArgs e)
        {
            if (sender is ComboBoxControl)
            {
                if (IsInsert)
                    Common.CodeHelper.GetPaycodesRemovingAssociationsInUse((ComboBoxControl)sender, SelectedType, true);
                else
                    Common.CodeHelper.PopulateComboBoxWithPaycodesByType((ComboBoxControl)sender, null);
            }
        }
        protected void ProvinceStateCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateProvince((ICodeControl)sender);
        }
        private void SetGridEnabledMode(WLPGrid grid, bool isEnabled, bool isSelectable)
        {
            grid.Enabled = isEnabled;
            grid.ClientSettings.EnablePostBackOnRowClick = isSelectable;
            grid.ClientSettings.Resizing.AllowColumnResize = isEnabled;
            grid.ClientSettings.Selecting.AllowRowSelect = isSelectable;
            grid.ClientSettings.AllowKeyboardNavigation = isEnabled;
        }
        #endregion

        #region handle updates
        protected void PaycodeAssociationTypeGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            PaycodeAssociationType paycodeAssociationType = (PaycodeAssociationType)e.Item.DataItem;
            Common.ServiceWrapper.CodeClient.InsertPaycodeAssociationType(paycodeAssociationType).CopyTo((PaycodeAssociationType)Data[String.Empty]);
            ResetGrids();
        }
        protected void PaycodeAssociationTypeGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                PaycodeAssociationType paycodeAssociationType = (PaycodeAssociationType)e.Item.DataItem;
                Common.ServiceWrapper.CodeClient.UpdatePaycodeAssociationType(paycodeAssociationType).CopyTo((PaycodeAssociationType)Data[paycodeAssociationType.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void PaycodeAssociationTypeGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            Common.ServiceWrapper.CodeClient.DeletePaycodeAssociationType((PaycodeAssociationType)e.Item.DataItem);
            ResetGrids();
        }
        protected void PaycodeAssociationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            PaycodeAssociation paycodeAssociation = (PaycodeAssociation)e.Item.DataItem;
            paycodeAssociation.PaycodeAssociationTypeCode = SelectedType;

            Common.ServiceWrapper.CodeClient.InsertPaycodeAssociation(paycodeAssociation).CopyTo(_collection[paycodeAssociation.Key]);
            ResetGrids();
        }
        protected void PaycodeAssociationGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                PaycodeAssociation paycodeAssociation = (PaycodeAssociation)e.Item.DataItem;
                Common.ServiceWrapper.CodeClient.UpdatePaycodeAssociation(paycodeAssociation).CopyTo(_collection[paycodeAssociation.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void PaycodeAssociationGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.CodeClient.DeletePaycodeAssociation((PaycodeAssociation)e.Item.DataItem);
                ResetGrids();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void ResetGrids()
        {
            String key = null;

            //if a row was selected
            if (PaycodeAssociationTypeGrid.SelectedItems.Count != 0)
                key = Convert.ToString(((GridDataItem)PaycodeAssociationTypeGrid.SelectedItems[0]).GetDataKeyValue(PaycodeAssociationTypeGrid.MasterTableView.DataKeyNames[0]));

            //redind parent to show/hide delete icons
            PaycodeAssociationTypeGrid.Rebind();

            if (key != null) //re-select the selected row
                PaycodeAssociationTypeGrid.SelectRowByFirstDataKey(key);

            //rebind child grid
            PaycodeAssociationGrid.Rebind();
        }
        #endregion
    }
}