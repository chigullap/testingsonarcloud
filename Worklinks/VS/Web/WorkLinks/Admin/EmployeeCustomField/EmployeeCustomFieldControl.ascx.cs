﻿using System;
using System.Web.UI.WebControls;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.EmployeeCustomField
{
    public partial class EmployeeCustomFieldControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        [System.ComponentModel.Bindable(true)]
        public EmployeeCustomFieldCollection EmployeeCustomFields
        {
            get
            {
                return (EmployeeCustomFieldCollection)Data;
            }
            set
            {
                InitialData = value;
                Data = value;
                EmployeeCustomFieldGrid.DataSource = Data;
            }
        }
        public bool IsEditMode { get; set; }
        public bool ReadOnly { get; set; }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EmployeeEmployment.ViewFlag);

            WireEvents();
        }
        protected void WireEvents()
        {
            EmployeeCustomFieldGrid.NeedDataSource += new Telerik.Web.UI.GridNeedDataSourceEventHandler(EmployeeCustomFieldGrid_NeedDataSource);
            EmployeeCustomFieldGrid.PreRender += new EventHandler(EmployeeCustomFieldGrid_PreRender);
        }
        protected void EmployeeCustomFieldGrid_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            EmployeeCustomFieldGrid.DataSource = Data;
        }
        protected void EmployeeCustomFieldGrid_PreRender(object sender, EventArgs e)
        {
            EmployeeCustomFieldConfigCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetEmployeeCustomFieldConfig(null);

            if (collection != null && collection.Count > 0)
            {
                System.Collections.Generic.List<PlaceHolder> placeHolders = GetPlaceHolders();

                if (placeHolders != null && placeHolders.Count > 0)
                {
                    foreach (EmployeeCustomFieldConfig item in collection)
                    {
                        //find the position of the control
                        int position = Convert.ToInt32(item.EmployeeCustomFieldNameCode.Substring(5)) - 1;
                        PlaceHolder placeHolder = placeHolders[position];

                        //create the control at the position
                        CreateControl(item, placeHolder, GetValue(placeHolder.ID));

                        //update the EmployeeCustomFields if coming from the wizard as the datatypes will be set to NULL and will blowup in the GetValueFromRequest method
                        UpdateDataTypes(item);
                    }
                }
            }
        }

        //If coming from the wizard, the data types fields will be NULL in EmployeeCustomFields, which will cause an exception in GetValueFromRequest.  So populate them.
        private void UpdateDataTypes(EmployeeCustomFieldConfig item)
        {
            if (EmployeeCustomFields.Count > 0)
            {
                //create a string that represents the name of the datatype field in EmployeeCustomFields object
                string propertyName = "Custom" + item.EmployeeCustomFieldNameCode.Substring(0, 1) + item.EmployeeCustomFieldNameCode.Substring(1, item.EmployeeCustomFieldNameCode.Length - 1).ToLower() + "DatatypeCode";

                //find the property
                if (EmployeeCustomFields[0].GetType().GetProperty(propertyName) != null)
                {
                    //get value
                    object propertyValue = EmployeeCustomFields[0].GetType().GetProperty(propertyName).GetValue(EmployeeCustomFields[0]);

                    //no datatype assigned (null), assign it here
                    if (propertyValue == null)
                        EmployeeCustomFields[0].GetType().GetProperty(propertyName).SetValue(EmployeeCustomFields[0], item.DataTypeCode);
                }
            }
        }

        private System.Collections.Generic.List<PlaceHolder> GetPlaceHolders()
        {
            System.Collections.Generic.List<PlaceHolder> placeHolders = new System.Collections.Generic.List<PlaceHolder>();

            if (Data.Count == 0)
            {
                Data = new EmployeeCustomFieldCollection { new BusinessLayer.BusinessObjects.EmployeeCustomField() { EmployeeCustomFieldId = -1, EmployeeId = -1 } };
                EmployeeCustomFieldGrid_NeedDataSource(null, null);
                EmployeeCustomFieldGrid.DataBind();
            }

            foreach (Telerik.Web.UI.GridDataItem item in EmployeeCustomFieldGrid.Items)
            {
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField01PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField02PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField03PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField04PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField05PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField06PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField07PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField08PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField09PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField10PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField11PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField12PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField13PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField14PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField15PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField16PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField17PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField18PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField19PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField20PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField21PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField22PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField23PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField24PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField25PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField26PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField27PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField28PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField29PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField30PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField31PlaceHolder"));
                placeHolders.Add((PlaceHolder)item.FindControl("CustomField32PlaceHolder"));
            }

            return placeHolders;
        }
        private String GetValue(String placeHolder)
        {
            String value = null;

            switch (placeHolder)
            {
                case "CustomField01PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField01;
                    break;
                case "CustomField02PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField02;
                    break;
                case "CustomField03PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField03;
                    break;
                case "CustomField04PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField04;
                    break;
                case "CustomField05PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField05;
                    break;
                case "CustomField06PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField06;
                    break;
                case "CustomField07PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField07;
                    break;
                case "CustomField08PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField08;
                    break;
                case "CustomField09PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField09;
                    break;
                case "CustomField10PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField10;
                    break;
                case "CustomField11PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField11;
                    break;
                case "CustomField12PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField12;
                    break;
                case "CustomField13PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField13;
                    break;
                case "CustomField14PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField14;
                    break;
                case "CustomField15PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField15;
                    break;
                case "CustomField16PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField16;
                    break;
                case "CustomField17PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField17;
                    break;
                case "CustomField18PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField18;
                    break;
                case "CustomField19PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField19;
                    break;
                case "CustomField20PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField20;
                    break;
                case "CustomField21PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField21;
                    break;
                case "CustomField22PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField22;
                    break;
                case "CustomField23PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField23;
                    break;
                case "CustomField24PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField24;
                    break;
                case "CustomField25PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField25;
                    break;
                case "CustomField26PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField26;
                    break;
                case "CustomField27PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField27;
                    break;
                case "CustomField28PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField28;
                    break;
                case "CustomField29PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField29;
                    break;
                case "CustomField30PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField30;
                    break;
                case "CustomField31PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField31;
                    break;
                case "CustomField32PlaceHolder":
                    value = ((EmployeeCustomFieldCollection)Data)[0].CustomField32;
                    break;
                default:
                    break;
            }

            return value;
        }
        #endregion

        #region handles updates
        public void Initialize()
        {
            EmployeeCustomFieldGrid.Rebind();

            //put the EmployeeCustomFieldGrid in EditAllMode if the EntitlementDetailView is in EditMode
            EmployeeCustomFieldGrid.SetEditAllMode(IsEditMode);
        }
        public void PopulateEmployeeCustomFields(String uniqueID)
        {
            if (EmployeeCustomFields != null && EmployeeCustomFields.Count > 0)
            {
                EmployeeCustomFields[0].CustomField01 = GetValueFromRequest("CustomField01", uniqueID, EmployeeCustomFields[0].CustomField01DatatypeCode);
                EmployeeCustomFields[0].CustomField02 = GetValueFromRequest("CustomField02", uniqueID, EmployeeCustomFields[0].CustomField02DatatypeCode);
                EmployeeCustomFields[0].CustomField03 = GetValueFromRequest("CustomField03", uniqueID, EmployeeCustomFields[0].CustomField03DatatypeCode);
                EmployeeCustomFields[0].CustomField04 = GetValueFromRequest("CustomField04", uniqueID, EmployeeCustomFields[0].CustomField04DatatypeCode);
                EmployeeCustomFields[0].CustomField05 = GetValueFromRequest("CustomField05", uniqueID, EmployeeCustomFields[0].CustomField05DatatypeCode);
                EmployeeCustomFields[0].CustomField06 = GetValueFromRequest("CustomField06", uniqueID, EmployeeCustomFields[0].CustomField06DatatypeCode);
                EmployeeCustomFields[0].CustomField07 = GetValueFromRequest("CustomField07", uniqueID, EmployeeCustomFields[0].CustomField07DatatypeCode);
                EmployeeCustomFields[0].CustomField08 = GetValueFromRequest("CustomField08", uniqueID, EmployeeCustomFields[0].CustomField08DatatypeCode);
                EmployeeCustomFields[0].CustomField09 = GetValueFromRequest("CustomField09", uniqueID, EmployeeCustomFields[0].CustomField09DatatypeCode);
                EmployeeCustomFields[0].CustomField10 = GetValueFromRequest("CustomField10", uniqueID, EmployeeCustomFields[0].CustomField10DatatypeCode);
                EmployeeCustomFields[0].CustomField11 = GetValueFromRequest("CustomField11", uniqueID, EmployeeCustomFields[0].CustomField11DatatypeCode);
                EmployeeCustomFields[0].CustomField12 = GetValueFromRequest("CustomField12", uniqueID, EmployeeCustomFields[0].CustomField12DatatypeCode);
                EmployeeCustomFields[0].CustomField13 = GetValueFromRequest("CustomField13", uniqueID, EmployeeCustomFields[0].CustomField13DatatypeCode);
                EmployeeCustomFields[0].CustomField14 = GetValueFromRequest("CustomField14", uniqueID, EmployeeCustomFields[0].CustomField14DatatypeCode);
                EmployeeCustomFields[0].CustomField15 = GetValueFromRequest("CustomField15", uniqueID, EmployeeCustomFields[0].CustomField15DatatypeCode);
                EmployeeCustomFields[0].CustomField16 = GetValueFromRequest("CustomField16", uniqueID, EmployeeCustomFields[0].CustomField16DatatypeCode);
                EmployeeCustomFields[0].CustomField17 = GetValueFromRequest("CustomField17", uniqueID, EmployeeCustomFields[0].CustomField17DatatypeCode);
                EmployeeCustomFields[0].CustomField18 = GetValueFromRequest("CustomField18", uniqueID, EmployeeCustomFields[0].CustomField18DatatypeCode);
                EmployeeCustomFields[0].CustomField19 = GetValueFromRequest("CustomField19", uniqueID, EmployeeCustomFields[0].CustomField19DatatypeCode);
                EmployeeCustomFields[0].CustomField20 = GetValueFromRequest("CustomField20", uniqueID, EmployeeCustomFields[0].CustomField20DatatypeCode);
                EmployeeCustomFields[0].CustomField21 = GetValueFromRequest("CustomField21", uniqueID, EmployeeCustomFields[0].CustomField21DatatypeCode);
                EmployeeCustomFields[0].CustomField22 = GetValueFromRequest("CustomField22", uniqueID, EmployeeCustomFields[0].CustomField22DatatypeCode);
                EmployeeCustomFields[0].CustomField23 = GetValueFromRequest("CustomField23", uniqueID, EmployeeCustomFields[0].CustomField23DatatypeCode);
                EmployeeCustomFields[0].CustomField24 = GetValueFromRequest("CustomField24", uniqueID, EmployeeCustomFields[0].CustomField24DatatypeCode);
                EmployeeCustomFields[0].CustomField25 = GetValueFromRequest("CustomField25", uniqueID, EmployeeCustomFields[0].CustomField25DatatypeCode);
                EmployeeCustomFields[0].CustomField26 = GetValueFromRequest("CustomField26", uniqueID, EmployeeCustomFields[0].CustomField26DatatypeCode);
                EmployeeCustomFields[0].CustomField27 = GetValueFromRequest("CustomField27", uniqueID, EmployeeCustomFields[0].CustomField27DatatypeCode);
                EmployeeCustomFields[0].CustomField28 = GetValueFromRequest("CustomField28", uniqueID, EmployeeCustomFields[0].CustomField28DatatypeCode);
                EmployeeCustomFields[0].CustomField29 = GetValueFromRequest("CustomField29", uniqueID, EmployeeCustomFields[0].CustomField29DatatypeCode);
                EmployeeCustomFields[0].CustomField30 = GetValueFromRequest("CustomField30", uniqueID, EmployeeCustomFields[0].CustomField30DatatypeCode);
                EmployeeCustomFields[0].CustomField31 = GetValueFromRequest("CustomField31", uniqueID, EmployeeCustomFields[0].CustomField31DatatypeCode);
                EmployeeCustomFields[0].CustomField32 = GetValueFromRequest("CustomField32", uniqueID, EmployeeCustomFields[0].CustomField32DatatypeCode);
            }
        }
        private String GetValueFromRequest(String controlName, String uniqueID, String datatypeCode)
        {
            String value = Request.Form[uniqueID + "$EmployeeCustomFieldGrid$ctl00$ctl04$" + controlName + "$Field"];

            if (value != null)
            {
                if (value.Equals(""))
                    value = null;
                else if (datatypeCode.Equals("BOOL") && value.Equals("on"))
                    value = "1";
                else if (datatypeCode.Equals("DECIMAL"))
                    value = value.Replace(",", "");
            }

            return value;
        }
        #endregion

        #region create controls
        private void CreateControl(EmployeeCustomFieldConfig item, PlaceHolder placeHolder, String value)
        {
            switch (item.DataTypeCode.ToLower())
            {
                case "bool": //add checkbox control
                    CheckBoxControl checkBoxControl = CreateCheckBoxControl(item, value, placeHolder.ID.Substring(0, 13));
                    placeHolder.Controls.Add(checkBoxControl);
                    break;
                case "date": //add date control
                    DateControl dateControl = CreateDateControl(item, value, placeHolder.ID.Substring(0, 13));
                    placeHolder.Controls.Add(dateControl);
                    break;
                case "decimal": //add numeric control
                    NumericControl numericControl = CreateNumericControl(item, value, placeHolder.ID.Substring(0, 13));
                    placeHolder.Controls.Add(numericControl);
                    break;
                case "dropdown": //add combobox control -- future functionality
                    break;
                case "string": //add textbox control
                    TextBoxControl textBoxControl = CreateTextBoxControl(item, value, placeHolder.ID.Substring(0, 13));
                    placeHolder.Controls.Add(textBoxControl);
                    break;
                default:
                    break;
            }
        }
        private CheckBoxControl CreateCheckBoxControl(EmployeeCustomFieldConfig item, String value, String controlName)
        {
            bool isChecked = false;

            if (value != null)
                isChecked = (value == "1" ? true : false);

            CheckBoxControl control = new CheckBoxControl
            {
                ID = controlName,
                LabelText = LanguageCode == "EN" ? item.EnglishDescription : item.FrenchDescription,
                LabelValue = LanguageCode == "EN" ? item.EnglishDescription : item.FrenchDescription,
                ReadOnly = ReadOnly,
                Checked = isChecked,
                Value = isChecked
            };

            return control;
        }
        private DateControl CreateDateControl(EmployeeCustomFieldConfig item, String value, String controlName)
        {
            DateTime? dateTime = null;

            if (value != null)
                dateTime = Convert.ToDateTime(value);

            DateControl control = new DateControl
            {
                ID = controlName,
                LabelText = LanguageCode == "EN" ? item.EnglishDescription : item.FrenchDescription,
                LabelValue = LanguageCode == "EN" ? item.EnglishDescription : item.FrenchDescription,
                ReadOnly = ReadOnly,
                Value = dateTime
            };

            return control;
        }
        private NumericControl CreateNumericControl(EmployeeCustomFieldConfig item, String value, String controlName)
        {
            NumericControl control = new NumericControl
            {
                ID = controlName,
                LabelText = LanguageCode == "EN" ? item.EnglishDescription : item.FrenchDescription,
                LabelValue = LanguageCode == "EN" ? item.EnglishDescription : item.FrenchDescription,
                MaxLength = item.Length,
                DecimalDigits = item.Precision,
                ReadOnly = ReadOnly,
                Value = value
            };

            return control;
        }
        private TextBoxControl CreateTextBoxControl(EmployeeCustomFieldConfig item, String value, String controlName)
        {
            TextBoxControl control = new TextBoxControl
            {
                ID = controlName,
                LabelText = LanguageCode == "EN" ? item.EnglishDescription : item.FrenchDescription,
                LabelValue = LanguageCode == "EN" ? item.EnglishDescription : item.FrenchDescription,
                MaxLength = item.Length,
                ReadOnly = ReadOnly,
                Value = value
            };

            return control;
        }
        #endregion
    }
}
