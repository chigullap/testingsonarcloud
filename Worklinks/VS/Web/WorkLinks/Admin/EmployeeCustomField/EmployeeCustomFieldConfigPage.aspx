﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeeCustomFieldConfigPage.aspx.cs" Inherits="WorkLinks.Admin.EmployeeCustomField.EmployeeCustomFieldConfigPage" %>
<%@ Register Src="EmployeeCustomFieldConfigControl.ascx" TagName="EmployeeCustomFieldConfigControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server" />
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:EmployeeCustomFieldConfigControl ID="EmployeeCustomFieldConfigControl1" runat="server" />
</asp:Content>