﻿using System;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.EmployeeCustomField
{
    public partial class EmployeeCustomFieldConfigControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public bool IsViewMode { get { return EmployeeCustomFieldConfigGrid.IsViewMode; } }
        public bool IsUpdate { get { return EmployeeCustomFieldConfigGrid.IsEditMode; } }
        public bool IsInsert { get { return EmployeeCustomFieldConfigGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.EmployeeCustomFieldConfig.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeCustomFieldConfig.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.EmployeeCustomFieldConfig.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EmployeeCustomFieldConfig.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                LoadEmployeeCustomFieldConfig();
                Initialize();
            }
        }
        protected void WireEvents()
        {
            EmployeeCustomFieldConfigGrid.NeedDataSource += new Telerik.Web.UI.GridNeedDataSourceEventHandler(EmployeeCustomFieldConfigGrid_NeedDataSource);
        }
        protected void LoadEmployeeCustomFieldConfig()
        {
            Data = EmployeeCustomFieldConfigCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetEmployeeCustomFieldConfig(null));
        }
        protected void Initialize()
        {
            //find the EmployeeCustomFieldConfigGrid
            WLPGrid grid = (WLPGrid)this.FindControl("EmployeeCustomFieldConfigGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        #endregion

        #region event handlers
        protected void EmployeeCustomFieldConfigGrid_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            EmployeeCustomFieldConfigGrid.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void EmployeeCustomFieldNameCode_NeedDataSource(object sender, EventArgs e)
        {
            if (IsInsert)
                Common.CodeHelper.PopulateControlWithEmployeeCustomFieldNameCodes((ICodeControl)sender);
            else
                Code_NeedDataSource(sender, e);
        }
        #endregion

        #region handle updates
        protected void EmployeeCustomFieldConfigGrid_InsertCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            EmployeeCustomFieldConfig item = (EmployeeCustomFieldConfig)e.Item.DataItem;
            Common.ServiceWrapper.HumanResourcesClient.InsertEmployeeCustomFieldConfig(item).CopyTo((EmployeeCustomFieldConfig)Data[item.Key]);
        }
        protected void EmployeeCustomFieldConfigGrid_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                EmployeeCustomFieldConfig item = (EmployeeCustomFieldConfig)e.Item.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.UpdateEmployeeCustomFieldConfig(item).CopyTo((EmployeeCustomFieldConfig)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void EmployeeCustomFieldConfigGrid_DeleteCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteEmployeeCustomFieldConfig((EmployeeCustomFieldConfig)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    LoadEmployeeCustomFieldConfig();
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}