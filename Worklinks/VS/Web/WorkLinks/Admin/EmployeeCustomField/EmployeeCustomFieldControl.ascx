﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeCustomFieldControl.ascx.cs" Inherits="WorkLinks.Admin.EmployeeCustomField.EmployeeCustomFieldControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style>
    .RadGrid_Silk .rgMasterTable,
    .RadGrid_Silk .rgDetailTable,
    .RadGrid_Silk .rgGroupPanel table,
    .RadGrid_Silk .rgCommandRow table,
    .RadGrid_Silk .rgEditForm table,
    .RadGrid_Silk .rgPager table
    {
        font-size: 14px !important;
        line-height: 18px !important;
    }
</style>

<wasp:WLPGrid
    ID="EmployeeCustomFieldGrid"
    runat="server"
    AutoGenerateColumns="false"
    AlternatingItemStyle-BackColor="Transparent"
    ItemStyle-BackColor="Transparent"
    ShowHeader="false"
    AllowMultiRowEdit="true"
    BorderStyle="none"
    ClientIDMode="Static">

    <MasterTableView DataKeyNames="Key" AllowFilteringByColumn="true">
        <Columns>
           <wasp:GridTemplateControl UniqueName="HeaderColumns" Reorderable="false">
                <ItemTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:PlaceHolder id="CustomField01PlaceHolder" runat="server" />
                            </td>
                            <td>
                                <asp:PlaceHolder id="CustomField02PlaceHolder" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:PlaceHolder id="CustomField03PlaceHolder" runat="server" />
                            </td>
                            <td>
                                <asp:PlaceHolder id="CustomField04PlaceHolder" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:PlaceHolder id="CustomField05PlaceHolder" runat="server" />
                            </td>
                            <td>
                                <asp:PlaceHolder id="CustomField06PlaceHolder" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:PlaceHolder id="CustomField07PlaceHolder" runat="server" />
                            </td>
                            <td>
                                <asp:PlaceHolder id="CustomField08PlaceHolder" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:PlaceHolder id="CustomField09PlaceHolder" runat="server" />
                            </td>
                            <td>
                                <asp:PlaceHolder id="CustomField10PlaceHolder" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:PlaceHolder id="CustomField11PlaceHolder" runat="server" />
                            </td>
                            <td>
                                <asp:PlaceHolder id="CustomField12PlaceHolder" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:PlaceHolder id="CustomField13PlaceHolder" runat="server" />
                            </td>
                            <td>
                                <asp:PlaceHolder id="CustomField14PlaceHolder" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:PlaceHolder id="CustomField15PlaceHolder" runat="server" />
                            </td>
                            <td>
                                <asp:PlaceHolder id="CustomField16PlaceHolder" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:PlaceHolder id="CustomField17PlaceHolder" runat="server" />
                            </td>
                            <td>
                                <asp:PlaceHolder id="CustomField18PlaceHolder" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:PlaceHolder id="CustomField19PlaceHolder" runat="server" />
                            </td>
                            <td>
                                <asp:PlaceHolder id="CustomField20PlaceHolder" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:PlaceHolder id="CustomField21PlaceHolder" runat="server" />
                            </td>
                            <td>
                                <asp:PlaceHolder id="CustomField22PlaceHolder" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:PlaceHolder id="CustomField23PlaceHolder" runat="server" />
                            </td>
                            <td>
                                <asp:PlaceHolder id="CustomField24PlaceHolder" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:PlaceHolder id="CustomField25PlaceHolder" runat="server" />
                            </td>
                            <td>
                                <asp:PlaceHolder id="CustomField26PlaceHolder" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:PlaceHolder id="CustomField27PlaceHolder" runat="server" />
                            </td>
                            <td>
                                <asp:PlaceHolder id="CustomField28PlaceHolder" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:PlaceHolder id="CustomField29PlaceHolder" runat="server" />
                            </td>
                            <td>
                                <asp:PlaceHolder id="CustomField30PlaceHolder" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:PlaceHolder id="CustomField31PlaceHolder" runat="server" />
                            </td>
                            <td>
                                <asp:PlaceHolder id="CustomField32PlaceHolder" runat="server" />
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </wasp:GridTemplateControl>
        </Columns>
    </MasterTableView>

    <ItemStyle BackColor="Transparent" />
    <HeaderContextMenu Enabled="false" />
</wasp:WLPGrid>