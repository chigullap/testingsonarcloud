﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeCustomFieldConfigControl.ascx.cs" Inherits="WorkLinks.Admin.EmployeeCustomField.EmployeeCustomFieldConfigControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPGrid
        ID="EmployeeCustomFieldConfigGrid"
        runat="server"
        AllowPaging="true"
        PagerStyle-AlwaysVisible="true"
        PageSize="100"
        GridLines="None"
        AutoGenerateColumns="false"
        OnDeleteCommand="EmployeeCustomFieldConfigGrid_DeleteCommand"
        OnInsertCommand="EmployeeCustomFieldConfigGrid_InsertCommand"
        OnUpdateCommand="EmployeeCustomFieldConfigGrid_UpdateCommand"
        AutoAssignModifyProperties="true">

        <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="true">
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="false" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="EmployeeCustomFieldNameCode" SortOrder="Ascending" />
            </SortExpressions>

            <CommandItemTemplate>
                <wasp:WLPToolBar ID="EmployeeCustomFieldConfigGridToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
                    <Items>
                        <wasp:WLPToolBarButton Text="**Add**" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add" />
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
                <wasp:GridKeyValueControl DataField="EmployeeCustomFieldNameCode" OnNeedDataSource="Code_NeedDataSource" LabelText="**EmployeeCustomFieldNameCode**" Type="EmployeeCustomFieldNameCode" ResourceName="EmployeeCustomFieldNameCode" />
                <wasp:GridCheckBoxControl DataField="ActiveFlag" LabelText="**ActiveFlag**" SortExpression="ActiveFlag" UniqueName="ActiveFlag" ResourceName="ActiveFlag" />
                <wasp:GridBoundControl DataField="EnglishDescription" LabelText="**EnglishDescription**" SortExpression="EnglishDescription" UniqueName="EnglishDescription" ResourceName="EnglishDescription">
                    <HeaderStyle Width="300px" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="FrenchDescription" LabelText="**FrenchDescription**" SortExpression="FrenchDescription" UniqueName="FrenchDescription" ResourceName="FrenchDescription">
                    <HeaderStyle Width="300px" />
                </wasp:GridBoundControl>
                <wasp:GridKeyValueControl DataField="DataTypeCode" OnNeedDataSource="Code_NeedDataSource" LabelText="**DataTypeCode**" Type="DataTypeCode" ResourceName="DataTypeCode" />
                <wasp:GridNumericControl DataField="Length" LabelText="**Length**" SortExpression="Length" UniqueName="Length" DataType="System.Int64" ResourceName="Length" />
                <wasp:GridNumericControl DataField="Precision" LabelText="**Precision**" SortExpression="Precision" UniqueName="Precision" DataType="System.Int64" ResourceName="Precision" />
                <wasp:GridNumericControl DataField="SortOrder" LabelText="**SortOrder**" SortExpression="SortOrder" UniqueName="SortOrder" DataType="System.Int64" ResourceName="SortOrder" />
                <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="EmployeeCustomFieldNameCode" AutoPostback="true" OnDataBinding="EmployeeCustomFieldNameCode_NeedDataSource" Text="**EmployeeCustomFieldNameCode**" runat="server" Type="EmployeeCustomFieldNameCode" ResourceName="EmployeeCustomFieldNameCode" Value='<%# Bind("EmployeeCustomFieldNameCode") %>' ReadOnly='<%# IsUpdate %>' Mandatory="true" />
                            </td>
                            <td>
                                <wasp:CheckBoxControl ID="ActiveFlag" Text="**ActiveFlag**" runat="server" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="EnglishDescription" Text="**EnglishDescription**" runat="server" ResourceName="EnglishDescription" Value='<%# Bind("EnglishDescription") %>' Mandatory="true" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="FrenchDescription" Text="**FrenchDescription**" runat="server" ResourceName="FrenchDescription" Value='<%# Bind("FrenchDescription") %>' Mandatory="true" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="DataTypeCode" AutoPostback="true" OnDataBinding="Code_NeedDataSource" Text="**DataTypeCode**" runat="server" Type="DataTypeCode" ResourceName="DataTypeCode" Value='<%# Bind("DataTypeCode") %>' Mandatory="true" />
                            </td>
                            <td>
                                <wasp:NumericControl ID="Length" Text="**Length**" DecimalDigits="0" runat="server" ResourceName="Length" Value='<%# Bind("Length") %>' Mandatory="true" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:NumericControl ID="Precision" Text="**Precision**" runat="server" MaxValue="4" DecimalDigits="0" ResourceName="Precision" Value='<%# Bind("Precision") %>' />
                            </td>
                            <td>
                                <wasp:NumericControl ID="SortOrder" Text="**SortOrder**" runat="server" DecimalDigits="0" ResourceName="SortOrder" Value='<%# Bind("SortOrder") %>' Mandatory="true" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="**Update**" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                            <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="**Insert**" runat="server" CommandName="PerformInsert" Visible='<%# IsInsert %>' ResourceName="Insert" />
                                        </td>
                                        <td>
                                            <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="**Cancel**" runat="server" CommandName="Cancel" CausesValidation="false" ResourceName="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true" />

    </wasp:WLPGrid>
</asp:Panel>