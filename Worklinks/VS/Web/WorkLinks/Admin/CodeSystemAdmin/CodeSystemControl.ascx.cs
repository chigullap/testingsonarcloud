﻿using System;
using System.Web.UI;
using Telerik.Web.UI;
using WLP.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Common;

namespace WorkLinks.Admin.CodeSystemAdmin
{
    public partial class CodeSystemControl : WLPUserControl
    {
        #region properties
        public bool IsViewMode { get { return CodeSystemGrid.IsViewMode; } }
        public bool IsUpdate { get { return CodeSystemGrid.IsEditMode; } }
        public bool IsInsert { get { return CodeSystemGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.CodeSystem.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.CodeSystem.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.CodeSystem.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.CodeSystem.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                LoadCodeSystemInformation();
                Initialize();
            }
        }

        protected void WireEvents()
        {
            CodeSystemGrid.NeedDataSource += new GridNeedDataSourceEventHandler(CodeSystemGrid_NeedDataSource);
        }

        protected void Initialize()
        {
            //Find the CodeSystemGrid
            WLPGrid grid = (WLPGrid)this.FindControl("CodeSystemGrid");

            //hide the edit/delete images in the rows.
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
        }

        protected void LoadCodeSystemInformation()
        {
            Data = CodeSystemCollection.ConvertCollection(ServiceWrapper.CodeClient.GetCodeSystem());
        }
        #endregion

        #region event handlers
        void CodeSystemGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            CodeSystemGrid.DataSource = Data;
        }
        #endregion

        #region handle updates
        protected void CodeSystemGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                ServiceWrapper.CodeClient.DeleteCodeSystem((CodeSystem)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    LoadCodeSystemInformation();
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void CodeSystemGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            CodeSystem codeSystem = (CodeSystem)e.Item.DataItem;

            //WD629 - now allowing empty string to be saved. (CodeSystem)e.Item.DataItem will make it NULL so convert to empty string or db will reject value.
            if (codeSystem.Description == null) codeSystem.Description = String.Empty;

            ServiceWrapper.CodeClient.InsertCodeSystem(codeSystem).CopyTo((CodeSystem)Data[String.Empty]);
        }

        protected void CodeSystemGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                CodeSystem codeSystem = (CodeSystem)e.Item.DataItem;

                //WD629 - now allowing empty string to be saved. (CodeSystem)e.Item.DataItem will make it NULL so convert to empty string or db will reject value.
                if (codeSystem.Description == null) codeSystem.Description = String.Empty;

                ServiceWrapper.CodeClient.UpdateCodeSystem(codeSystem).CopyTo((CodeSystem)Data[codeSystem.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}