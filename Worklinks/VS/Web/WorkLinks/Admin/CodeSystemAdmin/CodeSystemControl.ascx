﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CodeSystemControl.ascx.cs" Inherits="WorkLinks.Admin.CodeSystemAdmin.CodeSystemControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPGrid
        ID="CodeSystemGrid"
        runat="server"
        GridLines="None"
        AutoGenerateColumns="false"
        Width="100%"
        OnDeleteCommand="CodeSystemGrid_DeleteCommand"
        OnInsertCommand="CodeSystemGrid_InsertCommand"
        OnUpdateCommand="CodeSystemGrid_UpdateCommand"
        AutoAssignModifyProperties="true"
        AllowPaging="true"
        PagerStyle-AlwaysVisible="true"
        PageSize="100">

        <ClientSettings EnablePostBackOnRowClick="false" AllowColumnsReorder="true" ReorderColumnsOnClient="true">
            <Selecting AllowRowSelect="false" />
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">

            <SortExpressions>
                <telerik:GridSortExpression FieldName="CodeSystemCd" SortOrder="Ascending" />
            </SortExpressions>

            <CommandItemTemplate></CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
                <wasp:GridBoundControl DataField="CodeSystemCd" LabelText="**CodeSystemCd**" SortExpression="CodeSystemCd" UniqueName="CodeSystemCd" ResourceName="CodeSystemCd" />
                <wasp:GridBoundControl DataField="Description" LabelText="**Description**" SortExpression="Description" UniqueName="Description" ResourceName="Description" />
                <wasp:GridBoundControl DataField="Notes" LabelText="**Notes**" SortExpression="Notes" UniqueName="Notes" ResourceName="Notes" />
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="CodeSystemCd" FieldWidth="205px" Width="700px" runat="server" ResourceName="CodeSystemCd" Value='<%# Bind("CodeSystemCd") %>' ReadOnly='<%# IsUpdate %>' Mandatory="true" TabIndex="010" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="Description" FieldWidth="425px" Width="850px" runat="server" ResourceName="Description" Value='<%# Bind("Description") %>' TabIndex="020" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="Notes" Width="600px" LabelStyle="width:75px;" MaxLength="120" FieldStyle="width:425px;" runat="server" TextMode="multiline" Rows="3" Columns="40" ResourceName="Notes" Value='<%# Bind("Notes") %>' Mandatory="true" TabIndex="030" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                        </td>
                                        <td>
                                            <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="Cancel" CausesValidation="false" ResourceName="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>

        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true" />

    </wasp:WLPGrid>
</asp:Panel>