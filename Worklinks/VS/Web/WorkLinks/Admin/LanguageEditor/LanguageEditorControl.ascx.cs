﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.LanguageEditor
{
    public partial class LanguageEditorControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private String _englishLanguageCode = "EN";
        private String _frenchLanguageCode = "FR";
        private String _attributeIdentifier = "LabelText";
        #endregion

        #region properties
        private long FormId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["formId"]); }
        }
        public bool IsViewMode
        {
            get { return LanguageGrid.IsViewMode; }
        }
        public bool UpdateFlag
        {
            get { return Common.Security.RoleForm.LanguageEdit.UpdateFlag; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.LanguageEdit.ViewFlag);

            WireEvents();

            this.Page.Title = "Edit a Form";

            if (!IsPostBack)
                LoadFormGridInfo();
        }
        protected void WireEvents()
        {
            LanguageGrid.NeedDataSource += new GridNeedDataSourceEventHandler(LanguageGrid_NeedDataSource);
            LanguageGrid.UpdateAllCommand += new WLPGrid.UpdateAllCommandEventHandler(LanguageGrid_UpdateAllCommand);
            LanguageGrid.PreRender += new EventHandler(LanguageGrid_PreRender);
        }
        protected void LoadFormGridInfo()
        {
            LanguageEntityCollection collection = new LanguageEntityCollection();
            collection.Load(Common.ServiceWrapper.CodeClient.GetFieldLanguageInfo(Convert.ToDecimal(FormId), _attributeIdentifier, _englishLanguageCode));
            Data = collection;

            LanguageGrid.DataBind();
        }
        #endregion

        #region event handlers
        void LanguageGrid_PreRender(object sender, EventArgs e)
        {
            foreach (GridItem item in LanguageGrid.MasterTableView.Items)
            {
                TextBoxControl controlEn = (TextBoxControl)item.FindControl("EnglishFieldValue");
                controlEn.ReadOnly = IsViewMode;

                TextBoxControl controlFr = (TextBoxControl)item.FindControl("FrenchFieldValue");
                controlFr.ReadOnly = IsViewMode;
            }
        }
        void LanguageGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            LanguageGrid.DataSource = Data;
        }
        void LanguageGrid_UpdateAllCommand(object sender, UpdateAllCommandEventArgs e)
        {
            try
            {
                //pass the collection to the web service, determine if updates or inserts are needed, then return the collection
                LanguageEntityCollection collection = new LanguageEntityCollection();

                foreach (LanguageEntity entity in e.Items)
                    collection.Add(entity);

                Common.ServiceWrapper.CodeClient.UpdateFieldLanguage(collection.ToArray(), _englishLanguageCode, _frenchLanguageCode);
                Common.Resources.DatabaseResourceCache.ClearCache();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}