﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormSearchControl.ascx.cs" Inherits="WorkLinks.Admin.LanguageEditor.FormSearchControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="SearchPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SearchPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="SearchPanel" runat="server">
    <table width="100%">
        <tr valign="top">
            <td>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="FormDescription" runat="server" ResourceName="FormDescription" TabIndex="010" />
                            </td>
                        </tr>
                    </table>
                    <div class="SearchCriteriaButtons">
                        <wasp:WLPButton ID="btnClear" runat="server" Text="**Clear**" Icon-PrimaryIconUrl="~/App_Themes/Default/clear.gif" CssClass="button" OnClientClicked="clear" OnClick="btnClear_Click" ResourceName="Clear" />
                        <wasp:WLPButton ID="btnSearch" runat="server" Text="**Search**" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" OnClick="btnSearch_Click" ResourceName="Search" />
                    </div>
                </asp:Panel>
            </td>
        </tr>
        <tr valign="top">
            <td>
                <wasp:WLPToolBar ID="FormSummaryToolBar" runat="server" Width="100%" OnClientLoad="FormSummaryToolBar_init">
                    <Items>
                        <wasp:WLPToolBarButton OnPreRender="DetailsToolBar_PreRender" Text="**Details**" onclick="Open()" CommandName="details" ResourceName="Details" />
                    </Items>
                </wasp:WLPToolBar>

                <wasp:WLPGrid
                    ID="FormSummaryGrid"
                    runat="server"
                    AllowPaging="true"
                    PagerStyle-AlwaysVisible="true"
                    PageSize="100"
                    AllowSorting="true"
                    GridLines="None"
                    Height="460px"
                    AutoAssignModifyProperties="true">

                    <ClientSettings>
                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                        <Selecting AllowRowSelect="true" />
                        <ClientEvents OnRowDblClick="OnRowDblClick" OnRowClick="OnRowClick" />
                    </ClientSettings>

                    <MasterTableView ClientDataKeyNames="FormId" AutoGenerateColumns="false" DataKeyNames="FormId" CommandItemDisplay="Top">
                        <CommandItemTemplate></CommandItemTemplate>
                        <Columns>
                            <wasp:GridBoundControl DataField="FormDescriptionField" LabelText="**FormDescription**" SortExpression="FormDescriptionField" UniqueName="FormDescriptionField" ResourceName="FormDescription" />
                        </Columns>
                    </MasterTableView>

                    <HeaderContextMenu EnableAutoScroll="true" />

                </wasp:WLPGrid>
            </td>
        </tr>
    </table>
</asp:Panel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="FormWindows"
    Width="1000"
    Height="600"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var roleId;
        var rowSelected;
        var detailsButton;
        var toolbar;

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function clear() {
            formId = null;
        }

        function FormSummaryToolBar_init(sender) {
            InitializeControls();
        }

        function InitializeControls() {
            toolbar = $find('<%= FormSummaryToolBar.ClientID %>');
            detailsButton = toolbar.findButtonByCommandName('details');
            formId = null;

            if (detailsButton != null)
                detailsButton.disable();
        }

        function OnRowDblClick(sender, eventArgs) {
            if (toolbar.findButtonByCommandName('details') != null)
                Open(sender);
        }

        function OnRowClick(sender, eventArgs) {
            formId = eventArgs.getDataKeyValue('FormId');
            rowSelected = true;

            if (detailsButton != null)
                detailsButton.enable();
        }

        function Open() {
            if (formId != null)
                openWindowWithManager('FormWindows', String.format('/Admin/Form/{0}', formId), false);
        }

        function RowSelected() {
            return rowSelected;
        }

        function onClientClose(sender, eventArgs) {
            __doPostBack('<%= FormSummaryGrid.ClientID %>', '<%= String.Format("refresh") %>');
        }
    </script>
</telerik:RadScriptBlock>