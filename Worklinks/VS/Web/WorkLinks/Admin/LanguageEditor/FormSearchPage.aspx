﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FormSearchPage.aspx.cs" Inherits="WorkLinks.Admin.LanguageEditor.FormSearchPage" %>
<%@ Register Src="FormSearchControl.ascx" TagName="FormSearchControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:FormSearchControl ID="FormSearchControl1" SearchLocation="Admin" runat="server" />        
    </ContentTemplate>
</asp:Content>