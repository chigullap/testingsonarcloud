﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LanguageEditorControl.ascx.cs" Inherits="WorkLinks.Admin.LanguageEditor.LanguageEditorControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPGrid
    ID="LanguageGrid"
    runat="server"
    EditMode="InPlace"
    GridLines="None"
    Height="550px"
    AutoGenerateColumns="false"
    AutoInsertOnAdd="false"
    AllowMultiRowEdit="true"
    Skin="">

    <ClientSettings>
        <Selecting AllowRowSelect="false" />
        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key" TableLayout="Fixed">
        <CommandItemTemplate>
            <wasp:WLPToolBar ID="LanguageGridToolBar" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="Edit" ImageUrl="~/App_Themes/Default/edit.gif" CommandName="EditAll" Visible='<%# IsViewMode && UpdateFlag %>' CausesValidation="true" ResourceName="Edit" />
                    <wasp:WLPToolBarButton Text="Update All" ImageUrl="~/App_Themes/Default/update.gif" CommandName="UpdateAll" Visible='<%# !IsViewMode %>' CausesValidation="true" ResourceName="Update" />
                    <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/cancel.gif" CommandName="CancelAll" Visible='<%# !IsViewMode %>' ResourceName="Cancel" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>
        <Columns>
            <wasp:GridTemplateControl UniqueName="HeaderColumns" Reorderable="false">
                <HeaderTemplate>
                    <table width="100%">
                        <tr>
                            <td width="33%">
                                <wasp:WLPLabel ID="FieldIdentifier" runat="server" ResourceName="FieldIdentifier" Text="*FieldIdentifier" />
                            </td>
                            <td width="33%">
                                <wasp:WLPLabel ID="EnglishFieldValue" runat="server" ResourceName="EnglishFieldValue" Text="*EnglishFieldValue" />
                            </td>
                            <td width="33%">
                               <wasp:WLPLabel ID="FrenchFieldValue" runat="server" ResourceName="FrenchFieldValue" Text="*FrenchFieldValue" />
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table width="100%">
                        <tr>
                            <td width="33%">
                                <wasp:TextBoxControl ID="FieldIdentifier" runat="server" HideLabel="true" Value='<%# Bind("FieldIdentifier") %>' ReadOnly="true" />
                            </td>
                            <td width="33%">
                                <wasp:TextBoxControl ID="EnglishFieldValue" runat="server" HideLabel="true" FieldStyle="width: 300px !important;" Value='<%# Bind("EnglishFieldValue") %>' ReadOnly="false" />
                            </td>
                            <td width="33%">
                                <wasp:TextBoxControl ID="FrenchFieldValue" runat="server" HideLabel="true" FieldStyle="width: 300px !important;" Value='<%# Bind("FrenchFieldValue") %>' ReadOnly="false" />
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <EditItemTemplate>
                    <table width="100%">
                        <tr>
                            <td width="33%">
                                <wasp:TextBoxControl ID="FieldIdentifier" runat="server" HideLabel="true" Value='<%# Bind("FieldIdentifier") %>' ReadOnly="true" />
                            </td>
                            <td width="33%">
                                <wasp:TextBoxControl ID="EnglishFieldValue" runat="server" HideLabel="true" FieldStyle="width: 300px !important;" Value='<%# Bind("EnglishFieldValue") %>' ReadOnly="false" TabIndex="010" />
                            </td>
                            <td width="33%">
                                <wasp:TextBoxControl ID="FrenchFieldValue" runat="server" HideLabel="true" FieldStyle="width: 300px !important;" Value='<%# Bind("FrenchFieldValue") %>' ReadOnly="false" TabIndex="020" />
                            </td>
                        </tr>
                    </table>
                </EditItemTemplate>
            </wasp:GridTemplateControl>
        </Columns>
    </MasterTableView>

    <HeaderContextMenu EnableAutoScroll="true" />

</wasp:WLPGrid>