﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace WorkLinks.Admin.LanguageEditor {
    
    
    public partial class LanguageEditorControl {
        
        /// <summary>
        /// LanguageGrid control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPGrid LanguageGrid;
        
        /// <summary>
        /// LanguageGridToolBar control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPToolBar LanguageGridToolBar;
        
        /// <summary>
        /// FieldIdentifier control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPLabel FieldIdentifier;
        
        /// <summary>
        /// EnglishFieldValue control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPLabel EnglishFieldValue;
        
        /// <summary>
        /// FrenchFieldValue control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPLabel FrenchFieldValue;
    }
}
