﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.LanguageEditor
{
    public partial class FormSearchControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected String Criteria
        {
            get
            {
                String searchEntry = FormDescription.TextValue;
                return searchEntry;
            }
            set
            {
                FormDescription.TextValue = value;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.LanguageSearch.ViewFlag);

            WireEvents();

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.Equals(String.Format("refresh")))
            {
                long keyId = -1;

                //if a row was selected
                if (FormSummaryGrid.SelectedItems.Count != 0)
                    keyId = Convert.ToInt64(((GridDataItem)FormSummaryGrid.SelectedItems[0]).GetDataKeyValue(FormSummaryGrid.MasterTableView.DataKeyNames[0]));

                Search(Criteria);

                if (keyId != -1)
                    FormSummaryGrid.SelectRowByFirstDataKey(keyId); //re-select the selected row
            }
        }
        protected void WireEvents()
        {
            FormSummaryGrid.NeedDataSource += new GridNeedDataSourceEventHandler(FormSummaryGrid_NeedDataSource);
        }
        public void Search(String criteria)
        {
            Data = FormDescriptionCollection.ConvertCollection(Common.ServiceWrapper.CodeClient.GetFormInfo(criteria));
            FormSummaryGrid.Rebind();
        }
        #endregion

        #region event handlers
        void FormSummaryGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            FormSummaryGrid.DataSource = Data;
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search(Criteria);
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            FormDescription.TextValue = "";
            Data = null;
            FormSummaryGrid.DataBind();
        }
        protected void DetailsToolBar_PreRender(object sender, EventArgs e)
        {
            ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.LanguageSearch.ViewFlag;
        }
        #endregion
    }
}