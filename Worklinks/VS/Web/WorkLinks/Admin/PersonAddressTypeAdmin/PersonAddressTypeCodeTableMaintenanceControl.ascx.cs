﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.PersonAddressTypeAdmin
{
    public partial class PersonAddressTypeCodeTableMaintenanceControl : WLP.Web.UI.WLPUserControl
    {
        enum Operation {View, Add, Edit};

        #region fields

        private const String _parentCodeTableNameKey = "ParentCodeTableName";
        private const String _operationKey = "Operation";
        private const String _editPersonAddressTypeCodeTableKey = "EditPersonAddressTypeCodeTable";

        private String _parentCodeTableName = String.Empty;
        private Operation _operation = Operation.View;
        private PersonAddressTypeCodeTable _editPersonAddressTypeCodeTable = null;

        #endregion


        #region properties

        private Operation Mode
        {
            get
            {
                Object obj = ViewState[_operationKey];
                if (obj != null)
                {
                    _operation = (Operation) Enum.ToObject(typeof(Operation), obj);
                }
                return _operation;
            }
            set
            {
                _operation = value;
                ViewState[_operationKey] = _operation;
            }
        }

        private PersonAddressTypeCodeTable EditPersonAddressTypeCodeTable
        {
            get
            {
                Object obj = ViewState[_editPersonAddressTypeCodeTableKey];
                if (obj != null)
                {
                    _editPersonAddressTypeCodeTable = (PersonAddressTypeCodeTable) obj;
                }
                return _editPersonAddressTypeCodeTable;
            }
            set
            {
                _editPersonAddressTypeCodeTable = value;
                ViewState[_editPersonAddressTypeCodeTableKey] = _editPersonAddressTypeCodeTable;
            }
        }

        private String ParentCodeTableName
        {
            get
            {
                if (_parentCodeTableName == String.Empty)
                {
                    Object obj = ViewState[_parentCodeTableNameKey];

                    if (obj != null)
                        _parentCodeTableName = obj.ToString();
                }

                return _parentCodeTableName;
            }
            set
            {
                _parentCodeTableName = value;
                ViewState[_parentCodeTableNameKey] = _parentCodeTableName;
            }
        }
        
        public bool IsCodeGridViewMode { get { return CodeGrid.IsViewMode; } }

        public bool IsCodeGridUpdate { get { return CodeGrid.IsEditMode; } }

        public bool IsCodeGridInsert { get { return CodeGrid.IsInsertMode; } }

        public bool AddFlag { get { return Common.Security.RoleForm.PersonAddressTypeCodeMaintenance.AddFlag; } }

        public bool UpdateFlag { get { return Common.Security.RoleForm.PersonAddressTypeCodeMaintenance.UpdateFlag; } }

        public bool DeleteFlag { get { return Common.Security.RoleForm.PersonAddressTypeCodeMaintenance.DeleteFlag; } }

        #endregion


        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.PersonAddressTypeCodeMaintenance.ViewFlag);

            WireEvents();

            if (!Page.IsPostBack)
            {
                LoadGrid();
                Initialize();
            }
        }

        protected void LoadGrid()
        {
            //load the data rows for the selected PersonAddressType code table
            GetGridData();
            CodeGrid.DataSource = Data;
            CodeGrid.DataBind();
        }

        protected void GetGridData()
        {
            //populate the grid with the selected PersonAddressType code table rows
            Data = PersonAddressTypeCodeTableCollection.ConvertCollection(Common.ServiceWrapper.CodeClient.GetPersonAddressTypeCodeTableRows());
        }

        protected void Initialize()
        {
            //Find the CodeGrid
            WLPGrid grid = (WLPGrid)this.FindControl("CodeGrid");

            //hide the edit/delete images in the rows.
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }

        protected void WireEvents()
        {
            CodeGrid.NeedDataSource += new GridNeedDataSourceEventHandler(CodeGrid_NeedDataSource);
        }

        protected void CodeGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditFormInsertItem)
            {
                GridEditFormItem item = e.Item as GridEditFormItem;

                //set default values for user
                CheckBoxControl tempCBC = (CheckBoxControl)item.FindControl("ActiveFlag");
                if (tempCBC != null)
                    tempCBC.Value = true;

                NumericControl tempNC = (NumericControl)item.FindControl("SortOrder");
                if (tempNC != null)
                    tempNC.Value = 0;

                NumericControl priorityNC = (NumericControl)item.FindControl("Priority");
                if (priorityNC != null)
                    priorityNC.Value = 0;
            }
            else if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                ImageButton button = (ImageButton)dataItem["EditButton"].Controls[0];
                button.Visible = !((PersonAddressTypeCodeTable)dataItem.DataItem).SystemFlag;

                button = (ImageButton)dataItem["DeleteButton"].Controls[0];
                button.Visible = !((PersonAddressTypeCodeTable)dataItem.DataItem).SystemFlag;

                // Edit mode and dataItem is editable
                if (Mode == Operation.Edit && dataItem.Edit)
                {
                    //PersonAddressTypeCodeTable patct = (PersonAddressTypeCodeTable)dataItem.DataItem;
                    //EditPersonAddressTypeCodeTable = patct;
                    EditPersonAddressTypeCodeTable = (PersonAddressTypeCodeTable)dataItem.DataItem;
                }
            }
        }

        #endregion


        #region events

        void CodeGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            CodeGrid.DataSource = Data;

            // Add button is clicked
            if (IsCodeGridInsert)
            {
                Mode = Operation.Add;
            }
            // Edit button is clicked
            else if (IsCodeGridUpdate)
            {
                Mode = Operation.Edit;
            }
            else
            {
                Mode = Operation.View;
            }
        }

        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }

        protected void ValidatePriority(object source, ServerValidateEventArgs args)
        {
            short priority = -1;
            CustomValidator customValidator = (CustomValidator)source;
            PersonAddressTypeCodeTable codeTablePriority = null;
            String errorMessage = null;


            // get the priority user input as a number; if its not a number then return
            if (!short.TryParse(args.Value, out priority))
                return;

            // Add mode
            if (Mode == Operation.Add)
            {
                // get the PersonAddressTypeCodeTable object of the priority user input
                codeTablePriority = Common.ServiceWrapper.CodeClient.GetPersonAddressTypeCodeTableRow(priority);

                // The priority user input is already used; give an error message
                if (codeTablePriority != null)
                {
                    errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "DupePriority");
                    args.IsValid = false;
                    customValidator.ErrorMessage = errorMessage;
                }
            }
            // Edit mode
            else if (Mode == Operation.Edit)
            {
                // priority has been changed by the user
                if (priority != EditPersonAddressTypeCodeTable.Priority)
                {
                    // get the PersonAddressTypeCodeTable object of the priority user changed to 
                    codeTablePriority = Common.ServiceWrapper.CodeClient.GetPersonAddressTypeCodeTableRow(priority);

                    // The priority user changed to is already used; give an error message
                    if (codeTablePriority != null)
                    {
                        errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "DupePriority");
                        args.IsValid = false;
                        customValidator.ErrorMessage = errorMessage;
                    }
                }
            }
        }

        #endregion

        #region button action

        protected void CodeGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                PersonAddressTypeCodeTable cTable = (PersonAddressTypeCodeTable)e.Item.DataItem;
                cTable = Common.ServiceWrapper.CodeClient.UpdatePersonAddressTypeCodeTableData(cTable);

                //clear all codes from assembly 
                Common.CodeHelper.ResetCodeTableCache();

                cTable.CopyTo((PersonAddressTypeCodeTable)Data[cTable.Key]);

                //If descriptions were updated to be "", they would have been deleted from the db, so we remove them from our Data.Desc collection.
                //Using a foreach will not work if the collection is modified during the loop, so using an IF, starting at the end of the collection to traverse to the first of the collection
                for (int i = cTable.Descriptions.Count - 1; i > -1; i--)
                {
                    if (((PersonAddressTypeCodeTable)Data[cTable.Key]).Descriptions[i].TableDescription == "" && ((PersonAddressTypeCodeTable)Data[cTable.Key]).Descriptions[i].Id > 0)
                        ((PersonAddressTypeCodeTable)Data[cTable.Key]).Descriptions.RemoveAt(i);
                }

                //cleanup objects
                cTable = null;
                EditPersonAddressTypeCodeTable = null;
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void CodeGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.CodeClient.DeletePersonAddressTypeCodeTableData((PersonAddressTypeCodeTable)e.Item.DataItem);

                //clear all codes from assembly 
                Common.CodeHelper.ResetCodeTableCache();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void CodeGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            PersonAddressTypeCodeTable cTable = (PersonAddressTypeCodeTable)e.Item.DataItem;
            cTable = Common.ServiceWrapper.CodeClient.InsertPersonAddressTypeCodeTableData(cTable);

            //clear all codes from assembly 
            Common.CodeHelper.ResetCodeTableCache();

            cTable.CopyTo((PersonAddressTypeCodeTable)Data[String.Empty]);  //john changed this last year...
        }

        #endregion

        protected void ParentCode_Init(object sender, EventArgs e)
        {
            if (ParentCodeTableName == null || ParentCodeTableName.Equals(String.Empty))
                ((WebControl)sender).Visible = false;
            else
                ((WebControl)sender).Visible = true;
        }

        protected void ParentCode_DataBinding(object sender, EventArgs e)
        {
            ComboBoxControl control = (ComboBoxControl)sender;
            if (control.Visible)
            {
                CodeCollection collection = new CodeCollection();
                foreach (CodeTable table in Common.ServiceWrapper.CodeClient.GetCodeTableRows(ParentCodeTableName, ParentCodeTableName))
                {
                    if (table.ActiveFlag)
                    {
                        CodeObject code = new CodeObject() { Code = table.CodeTableId, Description = table.Descriptions[LanguageCode].TableDescription };
                        collection.Add(code);
                    }
                }

                control.DataSource = collection;
            }
        }
    }
}