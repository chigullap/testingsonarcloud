﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WcbChequeRemittanceFrequencyEditControl.ascx.cs" Inherits="WorkLinks.Admin.WcbChequeRemittanceFrequency.WcbChequeRemittanceFrequencyEditControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="WcbChequeRemittanceFrequencyDetailControl.ascx" TagName="WcbChequeRemittanceFrequencyDetailControl" TagPrefix="uc2" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy11" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPFormView
        ID="WcbChequeRemittanceFrequencyView"
        runat="server"
        RenderOuterTable="false"
        DataKeyNames="Key">

        <ItemTemplate>
            <wasp:WLPToolBar ID="WcbChequeRemittanceFrequencyToolBar" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="Edit" Visible='<%# UpdateFlag %>' ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="edit" ResourceName="Edit"></wasp:WLPToolBarButton>
                </Items>
            </wasp:WLPToolBar>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="WcbChequeRemittanceFrequencyCode" ReadOnly="true" runat="server" ResourceName="WcbChequeRemittanceFrequencyCode" Value='<%# Eval("WcbChequeRemittanceFrequencyCode") %>' Mandatory="true" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="ImportExternalIdentifier" ReadOnly="true" runat="server" ResourceName="ImportExternalIdentifier" Value='<%# Eval("ImportExternalIdentifier") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="EnglishDescription" ReadOnly="true" runat="server" ResourceName="EnglishDescription" Value='<%# Eval("EnglishDescription") %>' Mandatory="true" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="FrenchDescription" ReadOnly="true" runat="server" ResourceName="FrenchDescription" Value='<%# Eval("FrenchDescription") %>' Mandatory="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:CheckBoxControl ID="ActiveFlag" ReadOnly="true" runat="server" ResourceName="ActiveFlag" Value='<%# Eval("ActiveFlag") %>' />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </ItemTemplate>

        <EditItemTemplate>
            <wasp:WLPToolBar ID="WcbChequeRemittanceFrequencyToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
                <Items>
                    <wasp:WLPToolBarButton Text="**Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' ResourceName="Insert" CommandName="insert"></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton Text="**Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" ResourceName="Update" Visible='<%# IsEditMode %>'></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton Text="**Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" Visible='<%# !IsInsertMode  %>' CommandName="cancel" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton Text="**Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" Visible='<%# IsInsertMode  %>' onClick="Close()" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
                </Items>
            </wasp:WLPToolBar>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="WcbChequeRemittanceFrequencyCode" ReadOnly='<%# IsEditMode || IsViewMode %>' runat="server" ResourceName="WcbChequeRemittanceFrequencyCode" Value='<%# Bind("WcbChequeRemittanceFrequencyCode") %>' Mandatory="true" TabIndex="010" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="ImportExternalIdentifier" runat="server" ResourceName="ImportExternalIdentifier" Value='<%# Bind("ImportExternalIdentifier") %>' TabIndex="020" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="EnglishDescription" runat="server" ResourceName="EnglishDescription" Value='<%# Bind("EnglishDescription") %>' Mandatory="true" TabIndex="030" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="FrenchDescription" runat="server" ResourceName="FrenchDescription" Value='<%# Bind("FrenchDescription") %>' Mandatory="true" TabIndex="040" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:CheckBoxControl ID="ActiveFlag" runat="server" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>' TabIndex="050" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </EditItemTemplate>
    </wasp:WLPFormView>

    <uc2:WcbChequeRemittanceFrequencyDetailControl ID="WcbChequeRemittanceFrequencyDetailControl" runat="server" />
</asp:Panel>

<telerik:RadScriptBlock runat="server">
    <script type="text/javascript">
        function processClick(commandName) {
            var arg = new Object;
            arg.closeWithChanges = true;
            getRadWindow().argument = arg;
        }

        function getRadWindow() {
            var popup = null;

            if (window.radWindow)
                popup = window.radWindow;
            else if (window.frameElement.radWindow)
                popup = window.frameElement.radWindow;

            return popup;
        }

        function Close(button, args) {
            var popup = getRadWindow();
            setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
        }

        function ToolBarClick(sender, args) {
            var button = args.get_item();
            var commandName = button.get_commandName();
            processClick(commandName);

            if (<%= IsInsertMode.ToString().ToLower() %>) {
                if (commandName=="cancel") {
                    window.close();
                }
            }
        }
    </script>
</telerik:RadScriptBlock>