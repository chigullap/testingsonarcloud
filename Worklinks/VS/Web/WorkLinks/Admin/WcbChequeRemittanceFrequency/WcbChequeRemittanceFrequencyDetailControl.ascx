﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WcbChequeRemittanceFrequencyDetailControl.ascx.cs" Inherits="WorkLinks.Admin.WcbChequeRemittanceFrequency.WcbChequeRemittanceFrequencyDetailControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPGrid
        ID="WcbChequeRemittanceFrequencyDetailGrid"
        runat="server"
        GridLines="None"
        AutoGenerateColumns="false"
        AutoAssignModifyProperties="true"
        OnInsertCommand="WcbChequeRemittanceFrequencyDetailGrid_InsertCommand"
        OnUpdateCommand="WcbChequeRemittanceFrequencyDetailGrid_UpdateCommand"
        OnDeleteCommand="WcbChequeRemittanceFrequencyDetailGrid_DeleteCommand"
        OnItemDataBound="WcbChequeRemittanceFrequencyDetailGrid_ItemDataBound"
        Style="margin-top: 17px">

        <ClientSettings EnablePostBackOnRowClick="false" AllowColumnsReorder="true" ReorderColumnsOnClient="true">
            <Selecting AllowRowSelect="false" />
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="ReportingPeriodStartDate" SortOrder="Ascending" />
            </SortExpressions>

            <CommandItemTemplate>
                <wasp:WLPToolBar ID="WcbChequeRemittanceFrequencyDetailToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
                    <Items>
                        <wasp:WLPToolBarButton Text="**Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add"></wasp:WLPToolBarButton>
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif"></wasp:GridEditCommandControl>
                
                <wasp:GridDateTimeControl DataField="RemittanceDate" DataFormatString="{0:MMM dd}" LabelText="**RemittanceDate**" SortExpression="RemittanceDate" UniqueName="RemittanceDate" ResourceName="RemittanceDate"></wasp:GridDateTimeControl>
                <wasp:GridDateTimeControl DataField="ReportingPeriodStartDate" DataFormatString="{0:MMM dd}" LabelText="**ReportingPeriodStartDate**" SortExpression="ReportingPeriodStartDate" UniqueName="ReportingPeriodStartDate" ResourceName="ReportingPeriodStartDate"></wasp:GridDateTimeControl>
                <wasp:GridDateTimeControl DataField="ReportingPeriodEndDate" DataFormatString="{0:MMM dd}" LabelText="**ReportingPeriodEndDate**" SortExpression="ReportingPeriodEndDate" UniqueName="ReportingPeriodEndDate" ResourceName="ReportingPeriodEndDate"></wasp:GridDateTimeControl>
                <wasp:GridCheckBoxControl DataField="BasedOnEstimateFlag" LabelText="**BasedOnEstimateFlag**" SortExpression="BasedOnEstimateFlag" UniqueName="BasedOnEstimateFlag" ResourceName="BasedOnEstimateFlag" />
                <wasp:GridCheckBoxControl DataField="RemitInCurrentYearFlag" LabelText="**RemitInCurrentYearFlag**" SortExpression="RemitInCurrentYearFlag" UniqueName="RemitInCurrentYearFlag" ResourceName="RemitInCurrentYearFlag" />

                <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>"></wasp:GridButtonControl>
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:ComboBoxControl ID="RemittanceDateMonthCode" runat="server" Type="MonthCode" AutoPostback="true" OnDataBinding="MonthCode_NeedDataSource" OnSelectedIndexChanged="MonthCode_SelectedIndexChanged" ResourceName="RemittanceDateMonthCode" Value='<%# Bind("RemittanceDateMonthCode") %>' Mandatory="true" TabIndex="010" />
                                        </td>
                                        <td>
                                            <wasp:ComboBoxControl ID="RemittanceDateDayCode" runat="server" Type="DayCode" ResourceName="RemittanceDateDayCode" Value='<%# Bind("RemittanceDateDayCode") %>' Mandatory="true" TabIndex="020" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <wasp:ComboBoxControl ID="ReportingPeriodStartDateMonthCode" runat="server" Type="MonthCode" AutoPostback="true" OnDataBinding="MonthCode_NeedDataSource" OnSelectedIndexChanged="MonthCode_SelectedIndexChanged" ResourceName="ReportingPeriodStartDateMonthCode" Value='<%# Bind("ReportingPeriodStartDateMonthCode") %>' Mandatory="true" TabIndex="030" />
                                        </td>
                                        <td>
                                            <wasp:ComboBoxControl ID="ReportingPeriodStartDateDayCode" runat="server" Type="DayCode" ResourceName="ReportingPeriodStartDateDayCode" Value='<%# Bind("ReportingPeriodStartDateDayCode") %>' Mandatory="true" TabIndex="040" />
                                        </td>
                                    </tr>
                                     <tr>
                                        <td>
                                            <wasp:CheckBoxControl ID="BasedOnEstimateFlag" LabelText="*BasedOnEstimateFlag" runat="server" ResourceName="BasedOnEstimateFlag" Value='<%# Bind("BasedOnEstimateFlag") %>'></wasp:CheckBoxControl>
                                        </td>
                                        <td>
                                             <wasp:CheckBoxControl ID="RemitInCurrentYearFlag" LabelText="*RemitInCurrentYearFlag" runat="server" ResourceName="RemitInCurrentYearFlag" Value='<%# Bind("RemitInCurrentYearFlag") %>'></wasp:CheckBoxControl>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="**Update" runat="server" CommandName="Update" Visible='<%# IsEditMode %>' ResourceName="Update" />
                                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="**Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsInsertMode %>' ResourceName="Insert" />
                                                    </td>
                                                    <td>
                                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="**Cancel" runat="server" CommandName="Cancel" CausesValidation="false" ResourceName="Cancel" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true"></HeaderContextMenu>

    </wasp:WLPGrid>
</asp:Panel>