﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.WcbChequeRemittanceFrequency
{
    public partial class WcbChequeRemittanceFrequencyControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public bool IsViewMode { get { return WcbChequeRemittanceFrequencyGrid.IsViewMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.WcbChequeRemittanceFrequency.AddFlag; } }
        public bool ViewFlag { get { return Common.Security.RoleForm.WcbChequeRemittanceFrequency.ViewFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.WcbChequeRemittanceFrequency.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.WcbChequeRemittanceFrequency.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.WcbChequeRemittanceFrequency.ViewFlag);

            WireEvents();

            if (!IsPostBack)
                LoadWcbChequeRemittanceFrequencyInformation();

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.StartsWith("refresh"))
            {
                LoadWcbChequeRemittanceFrequencyInformation();
                WcbChequeRemittanceFrequencyGrid.Rebind();
            }
        }
        protected void WireEvents()
        {
            WcbChequeRemittanceFrequencyGrid.NeedDataSource += new GridNeedDataSourceEventHandler(WcbChequeRemittanceFrequencyGrid_NeedDataSource);
        }
        protected void LoadWcbChequeRemittanceFrequencyInformation()
        {
            Data = CodeWcbChequeRemittanceFrequencyCollection.ConvertCollection(Common.ServiceWrapper.CodeClient.GetCodeWcbChequeRemittanceFrequency(null));
        }
        #endregion

        #region event handlers
        void WcbChequeRemittanceFrequencyGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            WcbChequeRemittanceFrequencyGrid.DataSource = Data;
        }
        #endregion

        #region handle updates
        protected void WcbChequeRemittanceFrequencyToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("delete") && WcbChequeRemittanceFrequencyGrid.SelectedValue != null)
                DeleteWcbChequeRemittanceFrequency((CodeWcbChequeRemittanceFrequency)Data[WcbChequeRemittanceFrequencyGrid.SelectedValue.ToString()]);
        }
        protected void DeleteWcbChequeRemittanceFrequency(CodeWcbChequeRemittanceFrequency WcbChequeRemittanceFrequencyCode)
        {
            try
            {
                Common.ServiceWrapper.CodeClient.DeleteCodeWcbChequeRemittanceFrequency(WcbChequeRemittanceFrequencyCode);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    WcbChequeRemittanceFrequencyGrid.SelectedIndexes.Clear();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            LoadWcbChequeRemittanceFrequencyInformation();
            WcbChequeRemittanceFrequencyGrid.Rebind();
        }
        #endregion

        #region security handlers
        protected void AddButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && AddFlag; }
        protected void DetailsButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && ViewFlag; }
        protected void DeleteButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && DeleteFlag; }
        #endregion
    }
}