﻿using System;
using System.Web.UI.WebControls;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.WcbChequeRemittanceFrequency
{
    public partial class WcbChequeRemittanceFrequencyEditControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public CodeWcbChequeRemittanceFrequency WcbChequeRemittanceFrequency
        {
            get
            {
                if (Data.Count > 0)
                    return (CodeWcbChequeRemittanceFrequency)Data[0];
                else
                    return null;
            }
        }
        private String Action { get { return Page.RouteData.Values["action"].ToString().ToLower(); } }
        private String RemittanceFrequencyCode { get { return Convert.ToString(Page.RouteData.Values["WcbChequeRemittanceFrequencyCode"]); } }
        public bool IsEditMode { get { return WcbChequeRemittanceFrequencyView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool IsInsertMode { get { return WcbChequeRemittanceFrequencyView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsViewMode { get { return WcbChequeRemittanceFrequencyView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool AddFlag { get { return Common.Security.RoleForm.WcbChequeRemittanceFrequency.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.WcbChequeRemittanceFrequency.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.WcbChequeRemittanceFrequency.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.WcbChequeRemittanceFrequency.ViewFlag);

            WireEvents();

            if (Action == "view")
            {
                Page.Title = "Edit a Remittance Frequency";

                if (!IsPostBack)
                {
                    LoadWcbChequeRemittanceFrequencyInformation();
                    LoadWcbChequeRemittanceFrequencyEffective();
                    Initialize();
                }
            }
            else if (Action == "add")
            {
                if (!IsPostBack)
                {
                    Page.Title = "Add a Remittance Frequency";

                    CodeWcbChequeRemittanceFrequencyCollection newCollection = new CodeWcbChequeRemittanceFrequencyCollection();
                    newCollection.AddNew();
                    Data = newCollection;

                    //change formview's mode to insert mode
                    WcbChequeRemittanceFrequencyView.ChangeMode(FormViewMode.Insert);

                    //databind
                    WcbChequeRemittanceFrequencyView.DataBind();
                }
                else
                {
                    //we are loading the edit page after an add
                    Page.Title = "Edit a Remittance Frequency";

                    LoadWcbChequeRemittanceFrequencyEffective();
                    Initialize();
                }
            }
        }
        protected void WireEvents()
        {
            WcbChequeRemittanceFrequencyView.NeedDataSource += new WLPFormView.NeedDataSourceEventHandler(WcbChequeRemittanceFrequencyView_NeedDataSource);
            WcbChequeRemittanceFrequencyView.Updating += WcbChequeRemittanceFrequencyView_Updating;
            WcbChequeRemittanceFrequencyView.Inserting += WcbChequeRemittanceFrequencyView_Inserting;
        }
        protected void Initialize()
        {
            //find the WcbChequeRemittanceFrequencyDetailGrid
            WLPGrid grid = (WLPGrid)WcbChequeRemittanceFrequencyDetailControl.FindControl("WcbChequeRemittanceFrequencyDetailGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        protected void LoadWcbChequeRemittanceFrequencyInformation()
        {
            //create business object and fill it with the data passed to the page then bind the grid
            CodeWcbChequeRemittanceFrequencyCollection collection = new CodeWcbChequeRemittanceFrequencyCollection();
            collection.Add(LoadDataIntoBusinessClass());

            Data = collection;
            WcbChequeRemittanceFrequencyView.DataBind();
        }
        protected void LoadWcbChequeRemittanceFrequencyEffective()
        {
            WcbChequeRemittanceFrequencyDetailControl.WcbChequeRemittanceFrequencyCode = WcbChequeRemittanceFrequency.WcbChequeRemittanceFrequencyCode;
            WcbChequeRemittanceFrequencyDetailControl.LoadWcbChequeRemittanceFrequencyDetailInformation(WcbChequeRemittanceFrequency.WcbChequeRemittanceFrequencyCode);
        }
        CodeWcbChequeRemittanceFrequency LoadDataIntoBusinessClass()
        {
            CodeWcbChequeRemittanceFrequencyCollection temp = new CodeWcbChequeRemittanceFrequencyCollection();
            temp.Load(Common.ServiceWrapper.CodeClient.GetCodeWcbChequeRemittanceFrequency(RemittanceFrequencyCode));

            //will only ever have 1 item in this collection
            return temp[0];
        }
        #endregion

        #region event handlers
        void WcbChequeRemittanceFrequencyView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            WcbChequeRemittanceFrequencyView.DataSource = Data;
        }
        #endregion

        #region handle updates
        void WcbChequeRemittanceFrequencyView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            CodeWcbChequeRemittanceFrequency item = (CodeWcbChequeRemittanceFrequency)e.DataItem;
            Common.ServiceWrapper.CodeClient.InsertCodeWcbChequeRemittanceFrequency(item).CopyTo((CodeWcbChequeRemittanceFrequency)Data[item.Key]);
        }
        void WcbChequeRemittanceFrequencyView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                CodeWcbChequeRemittanceFrequency item = (CodeWcbChequeRemittanceFrequency)e.DataItem;
                Common.ServiceWrapper.CodeClient.UpdateCodeWcbChequeRemittanceFrequency(item).CopyTo((CodeWcbChequeRemittanceFrequency)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}