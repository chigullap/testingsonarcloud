﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WcbChequeRemittanceFrequencyControl.ascx.cs" Inherits="WorkLinks.Admin.WcbChequeRemittanceFrequency.WcbChequeRemittanceFrequencyControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPToolBar ID="WcbChequeRemittanceFrequencyToolBar" runat="server" Width="100%" OnButtonClick="WcbChequeRemittanceFrequencyToolBar_ButtonClick" OnClientLoad="WcbChequeRemittanceFrequencyToolBar_init">
        <Items>
            <wasp:WLPToolBarButton Text="**Add" ImageUrl="~/App_Themes/Default/Add.gif" onclick="Add()" CommandName="add" ResourceName="Add" OnPreRender="AddButton_PreRender" />
            <wasp:WLPToolBarButton Text="**Details" onclick="Open();" CommandName="details" ResourceName="Details" OnPreRender="DetailsButton_PreRender" />
            <wasp:WLPToolBarButton Text="**Delete" ImageUrl="~\App_Themes\Default\Delete.gif" CommandName="delete" ResourceName="Delete" OnPreRender="DeleteButton_PreRender" />
        </Items>
    </wasp:WLPToolBar>

    <wasp:WLPGrid
        ID="WcbChequeRemittanceFrequencyGrid"
        runat="server"
        AllowPaging="true"
        PagerStyle-AlwaysVisible="true"
        PageSize="100"
        AllowSorting="true"
        GridLines="None"
        AutoAssignModifyProperties="true">

        <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="false">
            <Scrolling AllowScroll="false" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
            <ClientEvents OnRowDblClick="OnRowDblClick" OnRowClick="OnRowClick" OnGridCreated="onGridCreated" OnCommand="onCommand" />
        </ClientSettings>

        <MasterTableView
            ClientDataKeyNames="WcbChequeRemittanceFrequencyCode, EnglishDescription, FrenchDescription, ActiveFlag, HasChildrenFlag"
            AutoGenerateColumns="false"
            DataKeyNames="WcbChequeRemittanceFrequencyCode"
            CommandItemDisplay="Top"
            AllowSorting="false">

            <SortExpressions>
                <telerik:GridSortExpression FieldName="WcbChequeRemittanceFrequencyCode" SortOrder="Ascending" />
            </SortExpressions>

            <CommandItemTemplate></CommandItemTemplate>

            <Columns>
                <wasp:GridBoundControl DataField="WcbChequeRemittanceFrequencyCode" LabelText="**WcbChequeRemittanceFrequencyCode**" SortExpression="WcbChequeRemittanceFrequencyCode" UniqueName="WcbChequeRemittanceFrequencyCode" ResourceName="WcbChequeRemittanceFrequencyCode" />
                <wasp:GridBoundControl DataField="EnglishDescription" LabelText="**EnglishDescription**" SortExpression="EnglishDescription" UniqueName="EnglishDescription" ResourceName="EnglishDescription" />
                <wasp:GridBoundControl DataField="FrenchDescription" LabelText="**FrenchDescription**" SortExpression="FrenchDescription" UniqueName="FrenchDescription" ResourceName="FrenchDescription" />
                <wasp:GridBoundControl DataField="ScheduleType" LabelText="**ScheduleType**" SortExpression="ScheduleType" UniqueName="ScheduleType" ResourceName="ScheduleType" />
                <wasp:GridCheckBoxControl DataField="ActiveFlag" LabelText="**ActiveFlag**" SortExpression="ActiveFlag" UniqueName="ActiveFlag" ResourceName="ActiveFlag" />
            </Columns>
        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true" />

    </wasp:WLPGrid>
</asp:Panel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="WcbChequeRemittanceFrequencyWindows"
    Width="1000"
    Height="600"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientClose="onClientClose"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var wcbChequeRemittanceFrequencyCode;
        var hasChildrenFlag;
        var toolbar = null;

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function WcbChequeRemittanceFrequencyToolBar_init(sender) {
            initializeControls();
        }

        function initializeControls() {
            if ($find('<%= WcbChequeRemittanceFrequencyToolBar.ClientID %>') != null)
                toolbar = $find('<%= WcbChequeRemittanceFrequencyToolBar.ClientID %>');

            enableButtons(getSelectedRow() != null);
        }

        function OnRowDblClick(sender, eventArgs) {
            var toolbar = $find('<%= WcbChequeRemittanceFrequencyToolBar.ClientID %>');

            if (toolbar.findButtonByCommandName('details') != null)
                Open(sender);
        }

        function getSelectedRow() {
            var grid = $find('<%= WcbChequeRemittanceFrequencyGrid.ClientID %>');
            if (grid == null)
                return null;

            var masterTable = grid.get_masterTableView();
            if (masterTable == null)
                return null;

            var selectedRow = masterTable.get_selectedItems();
            if (selectedRow.length == 1)
                return selectedRow[0];
            else
                return null;
        }

        function onGridCreated(sender, eventArgs) {
            var selectedRow = getSelectedRow();
            if (selectedRow != null) {
                wcbChequeRemittanceFrequencyCode = selectedRow.getDataKeyValue('WcbChequeRemittanceFrequencyCode');
                hasChildrenFlag = selectedRow.getDataKeyValue('HasChildrenFlag');

                enableButtons(true);
            }
        }

        function onCommand(sender, args) {
            if (args.get_commandName() == 'Page') {
                wcbChequeRemittanceFrequencyCode = null;
                hasChildrenFlag = null;

                initializeControls();
            }
        }

        function OnRowClick(sender, eventArgs) {
            wcbChequeRemittanceFrequencyCode = eventArgs.getDataKeyValue('WcbChequeRemittanceFrequencyCode');
            hasChildrenFlag = eventArgs.getDataKeyValue('HasChildrenFlag');

            enableButtons(true);
        }

        function enableButtons(enable) {
            var detailsButton = toolbar.findButtonByCommandName('details');
            var deleteButton = toolbar.findButtonByCommandName('delete');

            if (detailsButton != null)
                detailsButton.set_enabled(enable);

            if (deleteButton != null)
                deleteButton.set_enabled(enable && hasChildrenFlag != "True");
        }

        function Open() {
            if (wcbChequeRemittanceFrequencyCode != null) {
                openWindowWithManager('WcbChequeRemittanceFrequencyWindows', String.format('/Admin/WcbChequeRemittanceFrequency/{0}/{1}', 'view', wcbChequeRemittanceFrequencyCode), false);
                return false;
            }
        }

        function Add() {
            openWindowWithManager('WcbChequeRemittanceFrequencyWindows', String.format('/Admin/WcbChequeRemittanceFrequency/{0}/{1}', 'add', 'x'), false);
            return false;
        }

        function onClientClose(sender, eventArgs) {
            var arg = sender.argument;
            if (arg != null && arg.closeWithChanges)
                __doPostBack('<%= MainPanel.ClientID %>', 'refresh');
        }
    </script>
</telerik:RadScriptBlock>