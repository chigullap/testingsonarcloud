﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.WcbChequeRemittanceFrequency
{
    public partial class WcbChequeRemittanceFrequencyDetailControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        String _wcbChequeRemittanceFrequencyCode = null;
        private const String _wcbChequeRemittanceFrequencyCodeKey = "WcbChequeRemittanceFrequencyCodeKey";
        #endregion

        #region properties
        public String WcbChequeRemittanceFrequencyCode
        {
            get
            {
                if (_wcbChequeRemittanceFrequencyCode == null)
                {
                    Object obj = ViewState[_wcbChequeRemittanceFrequencyCodeKey];
                    if (obj != null)
                        _wcbChequeRemittanceFrequencyCode = (String)obj;
                }

                return _wcbChequeRemittanceFrequencyCode;
            }
            set
            {
                _wcbChequeRemittanceFrequencyCode = value;
                ViewState[_wcbChequeRemittanceFrequencyCodeKey] = _wcbChequeRemittanceFrequencyCode;
            }
        }
        public bool IsViewMode { get { return WcbChequeRemittanceFrequencyDetailGrid.IsViewMode; } }
        public bool IsEditMode { get { return WcbChequeRemittanceFrequencyDetailGrid.IsEditMode; } }
        public bool IsInsertMode { get { return WcbChequeRemittanceFrequencyDetailGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.WcbChequeRemittanceFrequency.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.WcbChequeRemittanceFrequency.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.WcbChequeRemittanceFrequency.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.WcbChequeRemittanceFrequency.ViewFlag);

            WireEvents();

            if (!IsPostBack)
                Initialize();
        }
        private void WireEvents()
        {
            WcbChequeRemittanceFrequencyDetailGrid.NeedDataSource += new GridNeedDataSourceEventHandler(WcbChequeRemittanceFrequencyDetailGrid_NeedDataSource);
            WcbChequeRemittanceFrequencyDetailGrid.ClientSettings.EnablePostBackOnRowClick = false;
        }
        public void LoadWcbChequeRemittanceFrequencyDetailInformation(String wcbChequeRemittanceFrequencyCode)
        {
            Data = WcbChequeRemittanceFrequencyDetailCollection.ConvertCollection(Common.ServiceWrapper.CodeClient.GetWcbChequeRemittanceFrequencyDetail(wcbChequeRemittanceFrequencyCode));
        }
        protected void Initialize()
        {
            //find the WcbChequeRemittanceFrequencyDetailGrid
            WLPGrid grid = (WLPGrid)this.FindControl("WcbChequeRemittanceFrequencyDetailGrid");

            //hide the edit image in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
        }
        #endregion

        #region event handlers
        void WcbChequeRemittanceFrequencyDetailGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            WcbChequeRemittanceFrequencyDetailGrid.DataSource = Data;
        }
        protected void WcbChequeRemittanceFrequencyDetailGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                //can delete any row except row 1 with period start = Jan 1
                GridDataItem item = (GridDataItem)e.Item;
                item["deleteButton"].Controls[0].Visible = ((WcbChequeRemittanceFrequencyDetail)item.DataItem).ReportingPeriodStartDateDayCode != "01_01";
            }
            else if (e.Item is GridEditFormInsertItem)
            {
                //the first period start date of any remittance frequency must be Jan 1
                if (Data.Count == 0)
                {
                    GridEditFormItem item = (GridEditFormItem)e.Item;

                    ComboBoxControl reportingPeriodStartDateMonthCode = (ComboBoxControl)item.FindControl("ReportingPeriodStartDateMonthCode");
                    if (reportingPeriodStartDateMonthCode != null)
                        reportingPeriodStartDateMonthCode.Value = "01";

                    MonthCode_SelectedIndexChanged(reportingPeriodStartDateMonthCode, null);

                    ComboBoxControl reportingPeriodStartDateDayCode = (ComboBoxControl)item.FindControl("ReportingPeriodStartDateDayCode");
                    if (reportingPeriodStartDateDayCode != null)
                        reportingPeriodStartDateDayCode.Value = "01_01";

                    reportingPeriodStartDateMonthCode.ReadOnly = true;
                    reportingPeriodStartDateDayCode.ReadOnly = true;
                }
            }
            else if (e.Item is GridEditFormItem)
            {
                //can edit Jan 1 row, but only the remittance date, not period start date
                GridEditFormItem item = (GridEditFormItem)e.Item;

                ComboBoxControl reportingPeriodStartDateMonthCode = (ComboBoxControl)item.FindControl("ReportingPeriodStartDateMonthCode");
                if (reportingPeriodStartDateMonthCode != null)
                {
                    if (reportingPeriodStartDateMonthCode.Value.ToString() == "01")
                        reportingPeriodStartDateMonthCode.ReadOnly = true;
                }

                ComboBoxControl reportingPeriodStartDateDayCode = (ComboBoxControl)item.FindControl("ReportingPeriodStartDateDayCode");
                if (reportingPeriodStartDateDayCode != null)
                {
                    if (reportingPeriodStartDateDayCode.Value.ToString() == "01_01")
                        reportingPeriodStartDateDayCode.ReadOnly = true;
                }
            }
        }
        protected void MonthCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
            BindDateDayCombo(sender);
        }
        protected void MonthCode_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindDateDayCombo(sender);
        }
        private void BindDateDayCombo(object sender)
        {
            if (sender is ComboBoxControl)
            {
                ComboBoxControl monthCodeCombo = (ComboBoxControl)sender;
                GridEditFormItem item = (GridEditFormItem)monthCodeCombo.BindingContainer;
                ComboBoxControl dayCodeCombo = null;

                if (((ComboBoxControl)sender).ID == "RemittanceDateMonthCode")
                    dayCodeCombo = (ComboBoxControl)item.FindControl("RemittanceDateDayCode");
                else if (((ComboBoxControl)sender).ID == "ReportingPeriodStartDateMonthCode")
                    dayCodeCombo = (ComboBoxControl)item.FindControl("ReportingPeriodStartDateDayCode");
                else if (((ComboBoxControl)sender).ID == "ReportingPeriodEndDateMonthCode")
                    dayCodeCombo = (ComboBoxControl)item.FindControl("ReportingPeriodEndDateDayCode");

                dayCodeCombo.DataSource = Common.ServiceWrapper.CodeClient.GetDayCode(monthCodeCombo.SelectedValue);
                dayCodeCombo.DataBind();
            }
        }
        #endregion

        #region handle updates
        protected void WcbChequeRemittanceFrequencyDetailGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            WcbChequeRemittanceFrequencyDetail item = (WcbChequeRemittanceFrequencyDetail)e.Item.DataItem;
            item.WcbChequeRemittanceFrequencyCode = WcbChequeRemittanceFrequencyCode;
            Common.ServiceWrapper.CodeClient.InsertWcbChequeRemittanceFrequencyDetail(item).CopyTo((WcbChequeRemittanceFrequencyDetail)Data[item.Key]);

            //rebind the grid
            LoadWcbChequeRemittanceFrequencyDetailInformation(WcbChequeRemittanceFrequencyCode);
            WcbChequeRemittanceFrequencyDetailGrid_NeedDataSource(source, null);
        }
        protected void WcbChequeRemittanceFrequencyDetailGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                WcbChequeRemittanceFrequencyDetail item = (WcbChequeRemittanceFrequencyDetail)e.Item.DataItem;
                Common.ServiceWrapper.CodeClient.UpdateWcbChequeRemittanceFrequencyDetail(item).CopyTo((WcbChequeRemittanceFrequencyDetail)Data[item.Key]);

                //rebind the grid
                LoadWcbChequeRemittanceFrequencyDetailInformation(WcbChequeRemittanceFrequencyCode);
                WcbChequeRemittanceFrequencyDetailGrid_NeedDataSource(source, null);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void WcbChequeRemittanceFrequencyDetailGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.CodeClient.DeleteWcbChequeRemittanceFrequencyDetail(((WcbChequeRemittanceFrequencyDetail)e.Item.DataItem));

                //rebind the grid
                LoadWcbChequeRemittanceFrequencyDetailInformation(WcbChequeRemittanceFrequencyCode);
                WcbChequeRemittanceFrequencyDetailGrid_NeedDataSource(source, null);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}