﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrganizationalUnitGridControl.ascx.cs" Inherits="WorkLinks.Admin.OrganizationalUnit.OrganizationalUnitGridControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPGrid
    ID="OrganizationUnitGrid"
    runat="server"
    GridLines="None"
    AutoGenerateColumns="false"
    OnNeedDataSource="OrganizationUnitGrid_NeedDataSource"
    OnInsertCommand="OrganizationUnitGrid_InsertCommand"
    OnUpdateCommand="OrganizationUnitGrid_UpdateCommand"
    OnDeleteCommand="OrganizationUnitGrid_DeleteCommand">
    <clientsettings allowcolumnsreorder="false" reordercolumnsonclient="true">
        <Resizing AllowColumnResize="true" />
        <ClientEvents OnRowClick="OnRowClick" OnRowDblClick="OnRowDblClick" />
         <Selecting AllowRowSelect="true" />
    </clientsettings>

    <mastertableview showheaderswhennorecords="true" commanditemdisplay="Top" datakeynames="OrganizationUnitId" clientdatakeynames="Key,OrganizationUnitLevelId">
        <SortExpressions>
            <telerik:GridSortExpression FieldName="EnglishDescription" SortOrder="Ascending" />
        </SortExpressions>

        <CommandItemTemplate>
            <wasp:WLPToolBar ID="OrganizationUnitToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientLoad="ToolBarInit">
                <Items>
                     <wasp:WLPToolBarButton ImageUrl="~/App_Themes/Default/add.gif" onclick="Add()" Visible='<%# IsViewMode && AddFlag && EnableAddButton%>' ResourceName="Add" PostBack="false"></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton Text="Details" ImageUrl="" onclick="Open()" PostBack="false"  CommandName="details" ResourceName="Details" Enabled="false" ></wasp:WLPToolBarButton>
   
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <Columns>

            <wasp:GridBoundControl DataField="EnglishDescription" HeaderText="EN Description" UniqueName="EnglishDescription" ResourceName="EnglishDescription"></wasp:GridBoundControl>
            <wasp:GridBoundControl DataField="FrenchDescription" HeaderText="FR Description" UniqueName="FrenchDescription" ResourceName="FrenchDescription"></wasp:GridBoundControl>
            <wasp:GridBoundControl DataField="GeneralLedgerSegment" HeaderText="GeneralLedgerSegment" UniqueName="GeneralLedgerSegment" ResourceName="GeneralLedgerSegment"></wasp:GridBoundControl>
            <wasp:GridCheckBoxControl DataField="ActiveFlag" HeaderText="Active" SortExpression="ActiveFlag" UniqueName="ActiveFlag" ResourceName="ActiveFlag"></wasp:GridCheckBoxControl>
            <wasp:GridButtonControl UniqueName="DeleteButton" ButtonType="ImageButton" CommandName="Delete" ImageUrl="~/App_Themes/Default/Delete.gif" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText='<%$ Resources:WarningMessages, DeleteNotificationWarning%>' ></wasp:GridButtonControl>
        </Columns>

     
    </mastertableview>
</wasp:WLPGrid>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="OrganizaitionalUnitEditor"
    Width="1000"
    MinWidth="700"
    Height="600"
    MinHeight="400"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientClose="onClientClose"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var Key = null;
        var OrganizationUnitLevelId = <%= OrganizationUnitLevelId %>;
        var detailButton;
        var toolbar;
        var enableDetail;

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function ReSelectRow(key1, key2) {
            Key = key1;
            OrganizationUnitLevelId = key2;
            enableDetail = true;
        }

        function OnRowClick(sender, eventArgs) {
            Key = eventArgs.getDataKeyValue('Key');
            OrganizationUnitLevelId = eventArgs.getDataKeyValue('OrganizationUnitLevelId');
            detailButton = toolbar.findButtonByCommandName('details');

            if (detailButton != null)
                detailButton.enable();
        }

        function OnRowDblClick(sender, eventArgs) {
            Open();
        }

        function Open() {
            if (Key != null)
                openWindowWithManager('OrganizaitionalUnitEditor', String.format('/Admin/OrganizationalUnitEdit/view/{0}/{1}', Key, OrganizationUnitLevelId), false);
        }

        function Add() {
            var OrganizationUnitGrid = $find('ctl00_MainContent_OrganizationalUnitLevelControl_OrganizationalUnitGridControl_OrganizationUnitGrid');

            if (OrganizationUnitGrid.get_masterTableView().get_dataItems().length > 0) {
                Key = OrganizationUnitGrid.get_masterTableView().get_dataItems()[0].getDataKeyValue("Key");
                OrganizationUnitLevelId = OrganizationUnitGrid.get_masterTableView().get_dataItems()[0].getDataKeyValue("OrganizationUnitLevelId");
            }

            if (Key == null)
                Key = -1;

            if (OrganizationUnitLevelId == null)
                OrganizationUnitLevelId = -1;

            openWindowWithManager('OrganizaitionalUnitEditor', String.format('/Admin/OrganizationalUnitEdit/add/{0}/{1}', Key, OrganizationUnitLevelId), false);
        }

        function onClientClose(sender, eventArgs) {
            var arg = sender.argument;
            if (arg != null && arg.closeWithChanges)
                __doPostBack();
        }

        function ToolBarInit(sender) {
            toolbar = sender;
            detailButton = toolbar.findButtonByCommandName('details');

            if (detailButton != null) {
                if (enableDetail) {
                    detailButton.enable();
                    enableDetail = false;
                }
                else {
                    detailButton.disable();
                    enableDetail = false;
                }
            }
        }
    </script>
</telerik:RadScriptBlock>