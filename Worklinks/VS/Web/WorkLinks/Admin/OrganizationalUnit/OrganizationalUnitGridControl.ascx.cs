﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.OrganizationalUnit
{
    public delegate void ItemCommandEventHandler(object sender, GridCommandEventArgs e);

    public partial class OrganizationalUnitGridControl : WLP.Web.UI.WLPUserControl
    {
        public event ItemCommandEventHandler ItemCommand;

        #region fields
        //TODO ADD needs to be french and english
        private string _addButtonLabelText = "Add ";
        private bool _enableAddButton = false;
        private long _organizationUnitLevelId = -1;
        private string _organizationUnitLevelIdKey = "OrganizationUnitLevelId";
        private long key = -1;
        #endregion

        #region properties
        public string AddButtonLabelText
        {
            get { return _addButtonLabelText; }
            set { _addButtonLabelText = value; }
        }
        //set in parent control, this is used to show the Add button and hide it if "company" is selected as only one company can exist
        public bool EnableAddButton
        {
            get { return _enableAddButton; }
            set { _enableAddButton = value; }
        }
        public bool IsViewMode { get { return OrganizationUnitGrid.IsViewMode; } }
        public bool IsUpdate { get { return OrganizationUnitGrid.IsEditMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.OrganizationUnitLevel.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.OrganizationUnitLevel.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.OrganizationUnitLevel.DeleteFlag; } }
        private string CommandName { get; set; }
        public long OrganizationUnitLevelId
        {
            get
            {
                if (_organizationUnitLevelId == -1)
                {
                    Object obj = ViewState[_organizationUnitLevelIdKey];
                    if (obj != null)
                        _organizationUnitLevelId = (long)obj;
                }
                return _organizationUnitLevelId;
            }
            set
            {
                _organizationUnitLevelId = value;
                ViewState[_organizationUnitLevelIdKey] = _organizationUnitLevelId;
            }
        }

        private bool _clearKey = false;
        public bool ClearKey
        {
            get { return _clearKey; }
            set { _clearKey = value; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            SetAuthorized(Common.Security.RoleForm.OrganizationUnitLevel.ViewFlag);
            WireEvents();

            if (!IsPostBack)
            {
                LoadOrganizationUnit();
            }

            OrganizationUnitGrid.MasterTableView.GetColumn("DeleteButton").Visible = DeleteFlag;
        }
        private void LoadOrganizationUnit()
        {
            //if a row was selected
            if (OrganizationUnitGrid.SelectedItems.Count != 0)
                key = Convert.ToInt64(((GridDataItem)OrganizationUnitGrid.SelectedItems[0]).GetDataKeyValue(OrganizationUnitGrid.MasterTableView.DataKeyNames[0]));

            Data = Common.ServiceWrapper.HumanResourcesClient.GetOrganizationUnitByOrganizationUnitLevelId(new OrganizationUnitCriteria() { OrganizationUnitLevelId = OrganizationUnitLevelId });

            OrganizationUnitGrid.DataSource = Data;
            OrganizationUnitGrid.Rebind();

            if (ClearKey)
            {
                key = -1; //user selected a diff org unit level so clear the selection
                OrganizationUnitGrid.SelectedIndexes.Clear();
            }
            else if (key != -1)
            {
                OrganizationUnitGrid.SelectRowByFirstDataKey(key); //re-select the selected row
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "ReSelectRow", $"ReSelectRow({key}, {OrganizationUnitLevelId});", true);
            }
        }
        private void WireEvents()
        {
            OrganizationUnitGrid.ItemCommand += new GridCommandEventHandler(OrganizationUnitGrid_ItemCommand);
            OrganizationUnitGrid.ItemDataBound += new GridItemEventHandler(OrganizationUnitGrid_ItemDataBound);
        }
        public void Rebind()
        {
            LoadOrganizationUnit();
        }
        protected void ShowHideButton(GridDataItem dataItem, string buttonName, bool isVisible)
        {
            ImageButton button = (ImageButton)dataItem[buttonName].Controls[0];
            button.Visible = isVisible;
        }
        #endregion

        #region event handlers
        protected virtual void OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (ItemCommand != null)
                ItemCommand(sender, e);
        }
        protected void OrganizationUnitGrid_InsertCommand(object sender, GridCommandEventArgs e)
        {
            OrganizationUnit organizationUnit = (OrganizationUnit)e.Item.DataItem;
            organizationUnit.OrganizationUnitLevelId = OrganizationUnitLevelId;
            Common.ServiceWrapper.HumanResourcesClient.InsertOrganizationUnit(organizationUnit).CopyTo((OrganizationUnit)Data[organizationUnit.Key]);
            OnItemCommand(sender, e);
        }
        protected void OrganizationUnitGrid_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                OrganizationUnit organizationUnit = (OrganizationUnit)e.Item.DataItem;
                organizationUnit = Common.ServiceWrapper.HumanResourcesClient.UpdateOrganizationUnit(organizationUnit);
                organizationUnit.CopyTo((OrganizationUnit)Data[organizationUnit.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void OrganizationUnitGrid_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                OrganizationUnit item = (OrganizationUnit)e.Item.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.DeleteOrganizationUnit(item);

                //if a row was selected
                if (OrganizationUnitGrid.SelectedItems.Count != 0)
                {
                    key = Convert.ToInt64(((GridDataItem)OrganizationUnitGrid.SelectedItems[0]).GetDataKeyValue(OrganizationUnitGrid.MasterTableView.DataKeyNames[0]));

                    if (key == item.OrganizationUnitId) //if the selected row is the same as the row that was deleted, set the key to -1
                        key = -1;
                }

                OnItemCommand(sender, e);
            }
            catch
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                LoadOrganizationUnit();
                e.Canceled = true;
            }
        }
        protected void OrganizationUnitGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            OrganizationUnitGrid.DataSource = Data;
        }
        public void OrganizationUnitGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditFormInsertItem)
            {
                GridEditFormItem item = e.Item as GridEditFormItem;

                //set default values for user
                CheckBoxControl tempCBC = (CheckBoxControl)item.FindControl("ActiveFlag");
                if (tempCBC != null)
                    tempCBC.Value = true;
            }
            else if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                //if only one row, do not allow it to be deleted so hide the delete button
                if (Data.Count == 1)
                    ShowHideButton(dataItem, "DeleteButton", false);
            }
        }
        protected void OrganizationUnitGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() != "performinsert" && e.CommandName.ToLower() != "delete")
                OnItemCommand(sender, e); //disable parent grid (level) which is listening
        }
        #endregion
    }
}