﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="~/Admin/OrganizationalUnit/OrganizationalUnitLevelPage.aspx.cs" Inherits="WorkLinks.Admin.OrganizationalUnit.OrganizationalUnitLevelPage" %>
<%@ Register Src="~/Admin/OrganizationalUnit/OrganizationalUnitLevelControl.ascx" TagName="OrganizationalUnitLevelControl" TagPrefix="oulc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <oulc:OrganizationalUnitLevelControl ID="OrganizationalUnitLevelControl" runat="server" />       
    </ContentTemplate>
</asp:Content>