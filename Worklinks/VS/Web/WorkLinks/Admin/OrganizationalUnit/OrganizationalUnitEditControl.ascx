﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrganizationalUnitEditControl.ascx.cs" Inherits="WorkLinks.Admin.OrganizationalUnit.OrganizationalUnitEditControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<div style="width: 75%; float: left; text-align: left;">
    <wasp:WLPFormView ID="OganizationalUnitView" runat="server" RenderOuterTable="false" DataKeyNames="Key" onmodechanging="OrganizaitonalUnitView_ModeChanged" >
        <itemtemplate>
        <wasp:WLPToolBar ID="OganizationalUnitSummaryToolBar" runat="server" Width="100%"  height="28px" AutoPostBack="true" Style="margin-bottom: 0">
            <Items>
                <wasp:WLPToolBarButton Text="Edit" ImageUrl="~/App_Themes/Default/Edit.gif" Visible='<%# UpdateFlag %>' CommandName="Edit" ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
                <div style="width:100%">
                    <div style="width:50%; float:left; text-align:left;">
                        <span style="padding:2px;">
                            <wasp:TextBoxControl ID="EnglishDesc" runat="server" Mandatory="true" ResourceName="EnglishDesc" Value='<%# Eval("EnglishDescription") %>' ReadOnly="true" />
                        </span>
                        <span style="padding:2px;">
                            <wasp:TextBoxControl ID="GeneralLedgerSegment" runat="server" ResourceName="GeneralLedgerSegment" Value='<%# Eval("GeneralLedgerSegment") %>' ReadOnly ="true"/>
                        </span>
                    </div>
                    <div style="width:50%; float:left; text-align:left;">
                        <span style="padding:2px;">
                            <wasp:TextBoxControl ID="FrenchDesc" runat="server" Mandatory="true" ResourceName="FrenchDesc" Value='<%# Eval("FrenchDescription") %>' ReadOnly="true" />
                        </span>
                        <span style="padding:2px;">
                            <wasp:CheckBoxControl ID="ActiveFlag" runat="server" ResourceName="ActiveFlag" Value='<%# Eval("ActiveFlag") %>' ReadOnly="true" />
                        </span>
                    </div>
                    <div style="width:50%; float:left; text-align:left;">
                        <span style="padding:2px;">
                            <wasp:TextBoxControl ID="ImportExternalIdentifier" runat="server" ResourceName="ImportExternalIdentifier" Value='<%# Eval("ImportExternalIdentifier") %>' ReadOnly="true" />
                        </span>
                        <span style="padding:2px;">
                        </span>
                    </div>
               </div>
        </fieldset>
    </itemtemplate>
        <edititemtemplate>
        <wasp:WLPToolBar ID="OganizationalUnitToolBar" runat="server" Width="100%"  height="28px"  AutoPostBack="true" Style="margin-bottom: 0" OnClientButtonClicked="ToolBarClick"  OnButtonClick="OganizationalUnitToolBar_OnButtonClick">
            <Items>
                <wasp:WLPToolBarButton ImageUrl="~/App_Themes/Default/Update.gif" ID ="btnUpdate"  CommandName="Update" CommandArgument="cancel" Visible='<%# IsEditMode %>' ResourceName="UpdateAndClose"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton ImageUrl="~/App_Themes/Default/Add.gif" ID ="btnInsert" Visible='<%# IsInsertMode %>' CommandName="insert" CommandArgument="cancel" ResourceName="Insert" ></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton ImageUrl="~/App_Themes/Default/Cancel.gif" onclick="closeAndReturn()" CommandName="cancel" CausesValidation="False" ResourceName="Cancel"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
             <div style="width:100%">
                    <span style="width:50%; float:left; text-align:left;">
                        <span style="padding:2px;">
                            <wasp:TextBoxControl ID="EnglishDesc" runat="server"  Mandatory="true" ResourceName="EnglishDesc" Value='<%# Bind("EnglishDescription") %>' TabIndex="010" />
                        </span>
                        <span style="padding:2px;">
                            <wasp:TextBoxControl ID="GeneralLedgerSegment" MaxLength="32" runat="server" ResourceName="GeneralLedgerSegment" TextValue='<%# Bind("GeneralLedgerSegment") %>' TabIndex="030" />
                        </span>
                    </div>
                    <div style="width:50%; float:left; text-align:left;">
                        <span style="padding:2px;">
                            <wasp:TextBoxControl ID="FrenchDesc" runat="server"  Mandatory="true" ResourceName="FrenchDesc" Value='<%# Bind("FrenchDescription") %>' TabIndex="020" />
                        </span>
                        <span style="padding:2px;">
                            <wasp:CheckBoxControl ID="ActiveFlag" runat="server" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>' TabIndex="040" />
                        </span>
                    </div>
                    <div style="width:50%;">
                        <span>
                            <wasp:TextBoxControl ID="ImportExternalIdentifier" runat="server" ResourceName="ImportExternalIdentifier" Value='<%# Bind("ImportExternalIdentifier") %>' TabIndex="050" />
                        </span>
                    </div>
            </div>
        </fieldset>
    </edititemtemplate> 
    </wasp:WLPFormView>
</div>

<div style="width: 25%; float: left; text-align: left;">
<div style="width: 100%;">
    <wasp:WLPToolBar ID="WLPToolBar1" runat="server" Width="100%" height="28px" AutoPostBack="true" Style="margin-bottom: 0">
        <items>
        <telerik:RadToolBarButton>
            <ItemTemplate>
                <wasp:WLPLabel ID="OrganizationUnitAssociationGridMenu" runat="server" ResourceName="OrganizationUnitAssociationGridMenu" Text="Select Organization Units"></wasp:WLPLabel>
            </ItemTemplate>
        </telerik:RadToolBarButton>
    </items>
    </wasp:WLPToolBar>

    <telerik:RadGrid ID="OrganizationUnitAssociationGrid" runat="server" AutoGenerateColumns="false" GridLines="None"
        AllowMultiRowEdit="True"
        OnNeedDataSource="OrganizationUnitAssociationGrid_NeedDataSource"
        OnItemDataBound="OrganizationUnitAssociationGrid_ItemDataBound"
        ShowHeader="false" style="min-width:100px">
        <MasterTableView EditMode="InPlace" >
            <Columns>
                <telerik:GridCheckBoxColumn DataField="Assigned" UniqueName="Assigned" AllowFiltering="false" ReadOnly="false">
                    <HeaderStyle Width="5%" />

                </telerik:GridCheckBoxColumn>
                <telerik:GridBoundColumn HeaderText="Description" DataField="Description" UniqueName="Description">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="OrganizationUnitId" UniqueName="OrganizationUnitId" Display="false">
                </telerik:GridBoundColumn>

            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
</div>
</div>

<script type="text/javascript">
    function closeAndReturn(button, args) {
        var popup = getRadWindow();
        setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
    }

    function getRadWindow() {
        var popup = null;

        if (window.radWindow)
            popup = window.radWindow;
        else if (window.frameElement.radWindow)
            popup = window.frameElement.radWindow;

        return popup;
    }

    function processClick(commandName) {
        var arg = new Object;
        arg.closeWithChanges = true;
        getRadWindow().argument = arg;
    }

    function ToolBarClick(sender, args) {
        var button = args.get_item();
        var commandName = button.get_commandName();

        if (commandName != "cancel")
            processClick(commandName);

        if (<%= IsInsertMode.ToString().ToLower() %> && commandName == "cancel") {
            window.close();
        }
    }
</script>