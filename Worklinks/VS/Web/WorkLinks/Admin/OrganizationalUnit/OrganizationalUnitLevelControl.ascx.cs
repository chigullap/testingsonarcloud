﻿using System;
using System.Linq;
using System.Web.UI;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.OrganizationalUnit
{
    public partial class OrganizationalUnitLevelControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private bool _override = false;
        private bool _employeesExist = true;
        private int _selectedIndexValue = -1;
        private string _selectedIndexValueKey = "SelectedIndexValue";
        #endregion

        #region properties
        public bool IsViewMode
        {
            get { return OrganizationalUnitLevelGrid.IsViewMode; }
        }
        public bool IsUpdate
        {
            get { return OrganizationalUnitLevelGrid.IsEditMode; }
        }
        public bool AddFlag
        {
            get { return Common.Security.RoleForm.OrganizationUnitLevel.AddFlag; }
        }
        public bool UpdateFlag
        {
            get { return Common.Security.RoleForm.OrganizationUnitLevel.UpdateFlag; }
        }
        public bool DeleteFlag
        {
            get { return Common.Security.RoleForm.OrganizationUnitLevel.DeleteFlag; }
        }
        private int SelectedIndexValue
        {
            get
            {
                if (_selectedIndexValue == -1)
                {
                    Object obj = ViewState[_selectedIndexValueKey];

                    if (obj != null)
                        _selectedIndexValue = (int)obj;
                }

                return _selectedIndexValue;
            }
            set
            {
                _selectedIndexValue = value;
                ViewState[_selectedIndexValueKey] = _selectedIndexValue;
            }
        }
        public bool OverrideFromDb
        {
            get { return _override; }
            set { _override = value; }
        }
        public bool EmployeesExist
        {
            get { return _employeesExist; }
            set { _employeesExist = value; }
        }

        public bool calledByPreRender { get; set; }

        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.OrganizationUnitLevel.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                LoadOrganizationUnitLevelGrid();
                Initialize();
            }
        }
        private void WireEvents()
        {
            OrganizationalUnitLevelGrid.NeedDataSource += new GridNeedDataSourceEventHandler(OrganizationalUnitLevelGrid_NeedDataSource);
            OrganizationalUnitLevelGrid.ItemDataBound += new GridItemEventHandler(OrganizationalUnitLevelGrid_ItemDataBound);
            OrganizationalUnitLevelGrid.ItemCommand += new GridCommandEventHandler(OrganizationalUnitLevelGrid_ItemCommand);
            OrganizationalUnitLevelGrid.UpdateCommand += new GridCommandEventHandler(OrganizationalUnitLevelGrid_UpdateCommand);
            OrganizationalUnitLevelGrid.InsertCommand += new GridCommandEventHandler(OrganizationalUnitLevelGrid_InsertCommand);
            OrganizationalUnitLevelGrid.DeleteCommand += new GridCommandEventHandler(OrganizationalUnitLevelGrid_DeleteCommand);
            OrganizationalUnitLevelGrid.SelectedIndexChanged += new EventHandler(OrganizationalUnitLevelGrid_SelectedIndexChanged);
            OrganizationalUnitGridControl.ItemCommand += new ItemCommandEventHandler(OrganizationalUnitGridControl_ItemCommand);
        }

        protected void Initialize()
        {
            //find the OrganizationalUnitLevelGrid
            WLPGrid grid = (WLPGrid)this.FindControl("OrganizationalUnitLevelGrid");

            //hide the edit images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
        }
        private void LoadOrganizationUnitLevelGrid()
        {
            Data = GetOrganizationUnitLevelCollection();

            if (Data.Count > 1)
                SelectedIndexValue = 0;

            //determine if Add/Delete buttons should be shown or not, based on the existance of employees or an override in the database.
            OverrideFromDb = Common.ApplicationParameter.OrganizationUnitOverride;
            EmployeesExist = Common.ServiceWrapper.HumanResourcesClient.CheckEmployeeExist();
        }
        private OrganizationUnitLevelCollection GetOrganizationUnitLevelCollection()
        {
            OrganizationUnitLevelCollection collection = new OrganizationUnitLevelCollection();

            foreach (OrganizationUnitLevel level in Common.ServiceWrapper.HumanResourcesClient.GetOrganizationUnitLevel())
                collection.Add(level);

            return collection;
        }
        protected void ShowHideButton(GridDataItem dataItem, String buttonName, bool isVisible)
        {
            System.Web.UI.WebControls.ImageButton button = (System.Web.UI.WebControls.ImageButton)dataItem[buttonName].Controls[0];
            button.Visible = isVisible;
        }
        #endregion

        #region event handlers
        protected void OrganizationalUnitLevelGrid_InsertCommand(object sender, GridCommandEventArgs e)
        {
            OrganizationUnitLevel item = (OrganizationUnitLevel)e.Item.DataItem;
            Common.ServiceWrapper.HumanResourcesClient.InsertOrganizationUnitLevel(item).CopyTo((OrganizationUnitLevel)Data[item.Key]);

            //ensure that the selected value will be the item just inserted
            SelectedIndexValue = ((WLPGrid)sender).Items.Count;
        }
        protected void OrganizationalUnitLevelGrid_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                OrganizationUnitLevel item = (OrganizationUnitLevel)e.Item.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.UpdateOrganizationUnitLevel(item).CopyTo((OrganizationUnitLevel)Data[item.Key]);

                //rebind the grid
                LoadOrganizationUnitLevelGrid();
                OrganizationalUnitLevelGrid_NeedDataSource(sender, null);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void OrganizationalUnitLevelGrid_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteOrganizationUnitLevel((OrganizationUnitLevel)e.Item.DataItem);

                //check see if the item being deleted is the last selected item ...if so make the previous item in the list seleted
                if (OrganizationalUnitLevelGrid.MasterTableView.Items[SelectedIndexValue].ItemIndex == e.Item.ItemIndex)
                    SelectedIndexValue = (OrganizationalUnitLevelGrid.MasterTableView.Items[SelectedIndexValue].ItemIndex) - 1;
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected void OrganizationalUnitLevelGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            var dataItem = OrganizationalUnitLevelGrid.SelectedItems[0] as GridDataItem;

            if (dataItem != null)
            {
                if (calledByPreRender)
                    OrganizationalUnitGridControl.ClearKey = false;
                else
                    OrganizationalUnitGridControl.ClearKey = true;

                SelectedIndexValue = dataItem.ItemIndex;
                OrganizationalUnitGridControl.OrganizationUnitLevelId = Convert.ToInt64(dataItem.GetDataKeyValue("Key"));
                OrganizationalUnitGridControl.AddButtonLabelText = "Add " + dataItem["Description"].Text; //TODO ADD needs to be french and english

                if (SelectedIndexValue == 0) //if "company" is selected, do not show the add button, only one company can exist.  This add button is on the child control "OrganizationalUnitGridControl"
                    OrganizationalUnitGridControl.EnableAddButton = false;
                else
                    OrganizationalUnitGridControl.EnableAddButton = true;

                //rebind child after "EnableAddButton" has been assigned
                OrganizationalUnitGridControl.Rebind();
            }
        }
        protected void OrganizationalUnitLevelGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            OrganizationalUnitLevelGrid.DataSource = Data;
        }
        protected void OrganizationalUnitLevelGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                bool showButtonFlag = false;
                GridDataItem dataItem = (GridDataItem)e.Item;

                if ((e.Item.ItemIndex + 1) != ((WLPGrid)sender).DataSource.Count)
                    showButtonFlag = false;
                else
                {
                    OrganizationUnitLevel item = ((OrganizationUnitLevel)dataItem.DataItem);

                    //This is the last item in the list of levels...we will now determine if there are any child units associated with this level...
                    //if they are ...remove the delete button...if not then show it..
                    if (item.ParentOrganizationUnitLevelId != null)
                    {
                        OrganizationUnitCriteria criteria = new OrganizationUnitCriteria() { OrganizationUnitLevelId = item.OrganizationUnitLevelId };
                        OrganizationUnitCollection organizationUnits = Common.ServiceWrapper.HumanResourcesClient.GetOrganizationUnitByOrganizationUnitLevelId(criteria);

                        if (organizationUnits.Count() <= 0)
                            showButtonFlag = true;
                        else
                            showButtonFlag = false;
                    }
                    else //upper most node can not be deleted...
                        showButtonFlag = false;
                }

                //if override was set in the database (or no employees exist) use the above logic in "showButtonFlag" to determine the show/hide of button
                if (OverrideFromDb || !EmployeesExist)
                    ShowHideButton(dataItem, "DeleteButton", showButtonFlag && DeleteFlag);
                else
                    ShowHideButton(dataItem, "DeleteButton", OverrideFromDb && DeleteFlag);
            }
        }
        protected void OrganizationalUnitLevelGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "initinsert":
                case "edit":
                    foreach (Control control in OrganizationalUnitGridControl.Controls)
                    {
                        if (control is WLPGrid)
                            ((WLPGrid)control).Enabled = false;
                    }

                    SetOrganizationalUnitLevelGridRowSelection(false);
                    break;
                case "cancel":
                case "delete":
                case "performinsert":
                case "update":
                    foreach (Control control in OrganizationalUnitGridControl.Controls)
                    {
                        if (control is WLPGrid)
                            ((WLPGrid)control).Enabled = true;
                    }

                    SetOrganizationalUnitLevelGridRowSelection(true);
                    break;
                default:
                    break;
            }
        }
        private void SetOrganizationalUnitLevelGridRowSelection(bool selectable)
        {
            OrganizationalUnitLevelGrid.ClientSettings.Selecting.AllowRowSelect = selectable;
            OrganizationalUnitLevelGrid.ClientSettings.EnablePostBackOnRowClick = selectable;
        }
        protected void OrganizationalUnitLevelGrid_PreRender(object sender, EventArgs e)
        {
            if (SelectedIndexValue == -1)
                SelectedIndexValue = 0;

            calledByPreRender = true;

            if (OrganizationalUnitLevelGrid.MasterTableView.Items.Count > 0)
            {
                OrganizationalUnitLevelGrid.MasterTableView.Items[SelectedIndexValue].Selected = true;
                OrganizationalUnitLevelGrid.MasterTableView.Items[SelectedIndexValue].FireCommandEvent(RadGrid.SelectCommandName, String.Empty);
            }
        }
        protected void OrganizationalUnitGridControl_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "initinsert":
                case "edit":
                    OrganizationalUnitLevelGrid.Enabled = false;
                    OrganizationalUnitLevelGrid.ClientSettings.Selecting.AllowRowSelect = false;
                    OrganizationalUnitLevelGrid.ClientSettings.EnablePostBackOnRowClick = false;
                    break;
                case "cancel":
                case "update":
                    OrganizationalUnitLevelGrid.ClientSettings.Selecting.AllowRowSelect = true;
                    OrganizationalUnitLevelGrid.ClientSettings.EnablePostBackOnRowClick = true;
                    OrganizationalUnitLevelGrid.Enabled = true;
                    break;
                case "delete":
                case "performinsert":
                    OrganizationalUnitLevelGrid.ClientSettings.Selecting.AllowRowSelect = true;
                    OrganizationalUnitLevelGrid.ClientSettings.EnablePostBackOnRowClick = true;
                    OrganizationalUnitLevelGrid.Enabled = true;
                    OrganizationalUnitLevelGrid.Rebind();
                    break;
                default:
                    break;
            }
        }
        #endregion
    }
}