﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrganizationalUnitLevelControl.ascx.cs" Inherits="WorkLinks.Admin.OrganizationalUnit.OrganizationalUnitLevelControl" %>
<%@ Register Src="~/Admin/OrganizationalUnit/OrganizationalUnitGridControl.ascx" TagName="OrganizationalUnitGridControl" TagPrefix="ougc" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="AjaxPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="AjaxPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="AjaxPanel" runat="server">
    <wasp:WLPGrid
        ID="OrganizationalUnitLevelGrid"
        runat="server"
        GridLines="none"
        OnPreRender="OrganizationalUnitLevelGrid_PreRender"
        AutoGenerateColumns="false">

        <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="true" EnablePostBackOnRowClick="true">
            <Resizing AllowColumnResize="true" />
            <Selecting AllowRowSelect="true" />
        </ClientSettings>

        <MasterTableView ShowHeadersWhenNoRecords="true" CommandItemDisplay="Top" DataKeyNames="Key">
            <CommandItemTemplate>
                <wasp:WLPToolBar ID="OrganizationalUnitLevelToolBar" runat="server" Width="100%" AutoPostBack="true">
                    <Items>
                        <wasp:WLPToolBarButton Text="Add Level" ImageUrl="~/App_Themes/Default/add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && OverrideFromDb && AddFlag %>' ResourceName="Add"></wasp:WLPToolBarButton>
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="EditButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" HeaderStyle-Width="3%" HeaderStyle-VerticalAlign="Top"></wasp:GridEditCommandControl>
                <wasp:GridBoundControl DataField="Description" LabelText="**Description**" UniqueName="Description" ResourceName="Description" Visible="false"></wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="OrganizationUnitLevelId" LabelText="**OrganizationUnitLevelId**" UniqueName="OrganizationUnitLevelId" ResourceName="OrganizationUnitLevelId" Visible="false"></wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="ParentOrganizationUnitLevelId" LabelText="**ParentOrganizationUnitLevelId**" UniqueName="ParentOrganizationUnitLevelId" ResourceName="ParentOrganizationUnitLevelId" Visible="false"></wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="EnglishDescription" LabelText="**EnglishDescription**" UniqueName="EnglishDescription" ResourceName="EnglishDescription"></wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="FrenchDescription" LabelText="**FrenchDescription**" UniqueName="FrenchDescription" ResourceName="FrenchDescription"></wasp:GridBoundControl>
                <wasp:GridCheckBoxControl DataField="UsedOnPayslipFlag" LabelText="**UsedOnPayslipFlag**" SortExpression="UsedOnPayslipFlag" UniqueName="UsedOnPayslipFlag" ResourceName="UsedOnPayslipFlag"></wasp:GridCheckBoxControl>
                <wasp:GridButtonControl UniqueName="DeleteButton" ButtonType="ImageButton" CommandName="Delete" ImageUrl="~/App_Themes/Default/Delete.gif" ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ConfirmText='<%$ Resources:WarningMessages, DeleteNotificationWarning%>'></wasp:GridButtonControl>
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="EnglishDescription" Text="EN Description:" runat="server" Mandatory="true" ResourceName="EnglishDescription" Value='<%# Bind("EnglishDescription") %>' ReadOnly="False" TabIndex="010" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="FrenchDescription" Text="FR Description:" runat="server" Mandatory="true" ResourceName="FrenchDescription" Value='<%# Bind("FrenchDescription") %>' ReadOnly="False" TabIndex="020" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:CheckBoxControl ID="UsedOnPayslipFlag" runat="server" ResourceName="UsedOnPayslipFlag" Value='<%# Bind("UsedOnPayslipFlag") %>' TabIndex="030" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" CausesValidation="true" />
                                            <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Add" runat="server" CommandName="PerformInsert" Visible='<%# !IsUpdate %>' ResourceName="Insert" />
                                        </td>
                                        <td>
                                            <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" Visible='<%# !IsViewMode %>' CommandName="Cancel" CausesValidation="False" ResourceName="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>
    </wasp:WLPGrid>

    <ougc:OrganizationalUnitGridControl ID="OrganizationalUnitGridControl" runat="server" />
</asp:Panel>