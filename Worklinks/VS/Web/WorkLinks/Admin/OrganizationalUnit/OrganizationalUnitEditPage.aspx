﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HumanResources/Employee.Master" AutoEventWireup="true" CodeBehind="~/Admin/OrganizationalUnit/OrganizationalUnitEditPage.aspx.cs" Inherits="WorkLinks.Admin.OrganizationalUnit.OrganizationalUnitEditPage" %>
<%@ Register Src="OrganizationalUnitEditControl.ascx" TagName="OrganizationalUnitEditControl" TagPrefix="oulc" %>

<asp:Content ID="content" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ContentTemplate>
       <oulc:OrganizationalUnitEditControl ID="OrganizationalUnitEditControl" runat="server" />    
    </ContentTemplate>
</asp:Content>