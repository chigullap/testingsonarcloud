﻿using System;
using System.Security.Claims;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.OrganizationalUnit
{
    public partial class OrganizationalUnitEditControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private long _organizationUnitId = -1;
        private string _organizationUnitIdKey = "OrganizationUnitId";
        #endregion

        #region properties
        private long OrganizationUnitLevelId { get { return Convert.ToInt64(Page.RouteData.Values["OrganizationUnitLevelId"]); } }
        public bool IsEditMode { get { return OganizationalUnitView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool IsInsertMode { get { return OganizationalUnitView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsViewMode { get { return OganizationalUnitView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.OrganizationUnitLevel.UpdateFlag; } }
        public long RoutedOrganizationUnitId { get { return Convert.ToInt64(Page.RouteData.Values["OrganizationUnitId"]); } }
        public string Action { get { return Page.RouteData.Values["action"].ToString(); } }
        public long OrganizationUnitId
        {
            get
            {
                if (_organizationUnitId == -1)
                {
                    Object obj = ViewState[_organizationUnitIdKey];
                    if (obj != null)
                        _organizationUnitId = (long)obj;
                }
                return _organizationUnitId;
            }
            set
            {
                _organizationUnitId = value;
                ViewState[_organizationUnitIdKey] = _organizationUnitId;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && RoutedOrganizationUnitId > 0)
                OrganizationUnitId = RoutedOrganizationUnitId;

            WireEvents();

            this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "OrganizaitonalUnitEdit"));

            if (Action.ToLower() == "view")
            {
                if (!Page.IsPostBack)
                    SetAuthorized(Common.Security.RoleForm.OrganizationUnitLevel.ViewFlag);

                if (!IsPostBack)
                    LoadOganizationalUnitInformation();
            }
            else
            {
                if (Action.ToLower() == "add")
                {
                    if (!IsPostBack)
                    {
                        OrganizationUnitCollection newOrganizationUnitCollection = new OrganizationUnitCollection();
                        newOrganizationUnitCollection.AddNew();
                        Data = newOrganizationUnitCollection;

                        OganizationalUnitView.ChangeMode(FormViewMode.Insert);
                        OganizationalUnitView.DataBind();
                    }
                }
            }
        }
        protected void OrganizaitonalUnitView_ModeChanged(object sender, FormViewModeEventArgs e)
        {
            OrganizationUnitAssociationGrid.Rebind();
        }
        protected void WireEvents()
        {
            OganizationalUnitView.PreRender += new EventHandler(OganizationalUnitView_PreRender);
            OganizationalUnitView.NeedDataSource += new WLPFormView.NeedDataSourceEventHandler(OganizationalUnitView_NeedDataSource);
            OganizationalUnitView.Updating += new WLPFormView.ItemUpdatingEventHandler(OganizationalUnitView_Updating);
            OganizationalUnitView.Inserting += new WLPFormView.ItemInsertingEventHandler(OganizationalUnitView_Inserting);
        }
        void OganizationalUnitView_PreRender(object sender, EventArgs e)
        {
            CheckBoxControl worklinksAdmin = (CheckBoxControl)OganizationalUnitView.FindControl("WorklinksAdministrationFlag");

            if (worklinksAdmin != null)
                if (!GetSecurityUser().WorklinksAdministrationFlag) worklinksAdmin.Style.Add("display", "none");
        }
        void OganizationalUnitView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            OganizationalUnitView.DataSource = Data;
        }
        protected void OrganizationUnitAssociationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (OrganizationUnitId > 0)
                OrganizationUnitAssociationGrid.DataSource = Common.ServiceWrapper.HumanResourcesClient.GetOrganizationUnitAssociation(new OrganizationUnitCriteria() { OrganizationUnitLevelId = OrganizationUnitId });
        }
        protected void OganizationalUnitToolBar_OnButtonClick(object sender, RadToolBarEventArgs e)
        {
            RadToolBarButton btn = e.Item as RadToolBarButton;

            if (btn != null)
            {
                if (btn.CommandName.ToLower() == "update" || btn.CommandName.ToLower() == "insert")
                {
                    foreach (GridDataItem rowItem in OrganizationUnitAssociationGrid.Items)
                        setRowCheckBox(rowItem, false);
                }
            }
        }
        protected void OrganizationUnitAssociationGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                if (!IsPostBack && Action.ToLower() == "add")
                    InitAssociationCheckBoxes((GridDataItem)e.Item);
                else
                    setRowCheckBox((GridDataItem)e.Item, IsEditMode);
            }
        }
        public void setRowCheckBox(GridDataItem rowItem, bool setValue)
        {
            if (rowItem is GridEditableItem)
            {
                GridEditableItem item = rowItem as GridEditableItem;
                CheckBox ck = (CheckBox)item["Assigned"].Controls[0]; // accessing the child GridCheckboxColumn
                ck.Enabled = setValue;
            }
        }
        protected void InitAssociationCheckBoxes(GridDataItem rowItem)
        {
            // used on insert to make sure all checks are enabled and unchecked.
            if (rowItem is GridEditableItem)
            {
                GridEditableItem item = rowItem as GridEditableItem;
                CheckBox ck = (CheckBox)item["Assigned"].Controls[0];
                ck.Enabled = true;
                ck.Checked = false;
            }
        }
        protected void LoadOganizationalUnitInformation()
        {
            if (OrganizationUnitId > 0)
            {
                OrganizationUnitCollection collection = new OrganizationUnitCollection();

                foreach (OrganizationUnit organizationUnit in Common.ServiceWrapper.HumanResourcesClient.GetOrganizationUnitByOrganizationUnitLevelId(new OrganizationUnitCriteria() { OrganizationUnitLevelId = OrganizationUnitLevelId }))
                {
                    if (organizationUnit.OrganizationUnitId == OrganizationUnitId)
                        collection.Add(organizationUnit);
                }

                Data = collection;
                OganizationalUnitView.DataBind();
            }
        }
        protected SecurityUser GetSecurityUser()
        {
            //return Common.ServiceWrapper.SecurityClient.GetSecurityUser(((ClaimsIdentity)System.Web.HttpContext.Current.User.Identity).FindFirst("uname").Value)[0];//yuc
            return Common.ServiceWrapper.SecurityClient.GetSecurityUser(System.Web.HttpContext.Current.User.Identity.Name.ToString())[0];
        }
        protected void OganizationalUnitView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                OrganizationUnit organizationUnit = (OrganizationUnit)e.DataItem;
                organizationUnit = Common.ServiceWrapper.HumanResourcesClient.UpdateOrganizationUnit(organizationUnit);
                organizationUnit.CopyTo((OrganizationUnit)Data[organizationUnit.Key]);

                OrganizationUnitId = Convert.ToInt64(organizationUnit.Key);

                SaveAssociations(OrganizationUnitId);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                else if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.UniqueIndexFault)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "UniqueIndex")), true);

                e.Cancel = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void OganizationalUnitView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            try
            {
                OrganizationUnit organizationUnit = (OrganizationUnit)e.DataItem;
                organizationUnit.OrganizationUnitLevelId = OrganizationUnitLevelId;

                Common.ServiceWrapper.HumanResourcesClient.InsertOrganizationUnit(organizationUnit).CopyTo((OrganizationUnit)Data[organizationUnit.Key]);
                OrganizationUnitId = Convert.ToInt64(organizationUnit.Key);

                SaveAssociations(OrganizationUnitId);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.UniqueIndexFault)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "UniqueIndex")), true);

                e.Cancel = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void SaveAssociations(long orgUnitId)
        {
            OrganizationUnitAssociation[] organizationUnitAssociation = new OrganizationUnitAssociation[OrganizationUnitAssociationGrid.Items.Count];
            int i = 0;

            foreach (GridDataItem row in OrganizationUnitAssociationGrid.Items)
            {
                organizationUnitAssociation[i] = LoadCollection(row, orgUnitId);
                OrganizationUnitAssociationCollection tmp = new OrganizationUnitAssociationCollection();

                if (i == 0)
                    Common.ServiceWrapper.HumanResourcesClient.DeleteOrganizationUnitAssociation(organizationUnitAssociation[i]);
                if (organizationUnitAssociation[i].Assigned)
                    tmp = Common.ServiceWrapper.HumanResourcesClient.InsertOrganizationUnitAssociation(organizationUnitAssociation[i]);

                i++;
            }
        }
        private OrganizationUnitAssociation LoadCollection(GridDataItem row, long orgUnitId)
        {
            OrganizationUnitAssociation tmp = new OrganizationUnitAssociation();

            tmp.LevelId = orgUnitId;
            tmp.OrganizationUnitId = Convert.ToInt64(row["OrganizationUnitId"].Text);
            tmp.Assigned = ((CheckBox)row["Assigned"].Controls[0]).Checked;

            return tmp;
        }
    }
}