﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Wizard;

namespace WorkLinks.Admin.Union
{
    public partial class CollectiveAgreementControl : WizardUserControl
    {
        #region fields
        private const String _labourUnionIdKey = "LabourUnionId";
        private long _labourUnionId = -1;
        protected bool _updateExterallyControlled = false;
        protected static bool removeAttachmentClicked = false;
        #endregion

        #region properties
        public long LabourUnionId
        {
            get
            {
                if (_labourUnionId == -1)
                {
                    Object obj = ViewState[_labourUnionIdKey];

                    if (obj != null)
                        _labourUnionId = Convert.ToInt64(obj);
                }

                return _labourUnionId;
            }
            set
            {
                _labourUnionId = value;
                ViewState[_labourUnionIdKey] = _labourUnionId;
            }
        }
        public String[] AllowedFileExtensions { get { return Common.CodeHelper.PopulateAllowedFileExtensions(LanguageCode); } }
        public bool IsViewMode { get { return CollectiveAgreementGrid.IsViewMode; } }
        public bool IsUpdate { get { return CollectiveAgreementGrid.IsEditMode; } }
        public bool IsInsert { get { return CollectiveAgreementGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.UnionCollectiveAgreemnt.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.UnionCollectiveAgreemnt.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.UnionCollectiveAgreemnt.DeleteFlag; } }
        #endregion

        #region main
        public void SetLabourUnionId(long labourUnionId)
        {
            LabourUnionId = labourUnionId;
            LoadCollectiveAgreement(labourUnionId);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (IsDataExternallyLoaded)
                    CollectiveAgreementGrid.Rebind();

                Initialize();
            }
        }
        protected void Initialize()
        {
            //find the CollectiveAgreementGrid
            WLPGrid grid = (WLPGrid)this.FindControl("CollectiveAgreementGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        protected void LoadCollectiveAgreement(long labourUnionId)
        {
            DataItemCollection = UnionCollectiveAgreementCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetUnionCollectiveAgreement(labourUnionId));
            CollectiveAgreementGrid.Rebind();
        }
        #endregion main

        #region event handlers
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void CollectiveAgreementGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            CollectiveAgreementGrid.DataSource = DataItemCollection;
        }
        public override void Update(bool updateExterallyControlled)
        {
        }
        public override void AddNewDataItem()
        {
            if (DataItemCollection == null)
                DataItemCollection = new UnionCollectiveAgreementCollection();
        }
        public override void ChangeModeEdit()
        {
        }
        protected void CollectiveAgreementGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "cancel":
                case "performinsert":
                case "update":
                    OnItemChangingComplete(null);
                    break;
                case "edit":
                case "initinsert":
                    OnItemChanging(null);
                    break;
            }
        }
        protected void RemoveAttachment_Click(object sender, EventArgs e)
        {
            removeAttachmentClicked = true;
            ((WLPButton)sender).Visible = false;
        }
        protected void CollectiveAgreementGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                HyperLink attachmentLink = (HyperLink)e.Item.FindControl("AttachmentLink");

                if (e.Item.DataItem != null)
                {
                    long? attachmentId = ((UnionCollectiveAgreement)e.Item.DataItem).AttachmentId;

                    if ((attachmentId != null && attachmentId != -1) || (attachmentId != null && IsDataExternallyLoaded))
                    {
                        attachmentLink.Visible = true;
                        attachmentLink.Attributes["href"] = "javascript:void(0);";
                        attachmentLink.Attributes["onclick"] = String.Format("return ShowAttachment('{0}');", attachmentId);
                    }
                }
            }
        }
        protected void CollectiveAgreementGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditFormItem && !(e.Item is GridEditFormInsertItem))
            {
                //only display the clear button if an binary attachment exists
                GridEditFormItem item = e.Item as GridEditFormItem;
                WLPButton removeAttachment = (WLPButton)item.FindControl("RemoveAttachment");

                if (removeAttachment != null && ((UnionCollectiveAgreement)item.DataItem).AttachmentObjectCollection != null)
                    removeAttachment.Visible = true;
            }
        }
        #endregion

        #region handle updates
        protected void PopulateAttachmentObjectCollection(UnionCollectiveAgreement item, GridCommandEventArgs e)
        {
            RadAsyncUpload uploadControl = (RadAsyncUpload)e.Item.FindControl("UploadAttachment");
            TextBoxControl descriptionControl = (TextBoxControl)e.Item.FindControl("Description");

            if (uploadControl.UploadedFiles.Count > 0 && !removeAttachmentClicked)
            {
                //get the uploaded file from the UploadAttachment control and the description from the Description control
                UploadedFile file = uploadControl.UploadedFiles[0];
                byte[] bytes = new byte[file.ContentLength];
                file.InputStream.Read(bytes, 0, bytes.Length);

                //populate the AttachmentObjectCollection object
                item.AttachmentObjectCollection = new AttachmentCollection();
                item.AttachmentObjectCollection.AddNew();
                item.AttachmentObjectCollection[0].Data = bytes;
                item.AttachmentObjectCollection[0].FileName = file.GetNameWithoutExtension();
                item.AttachmentObjectCollection[0].FileTypeCode = file.GetExtension().Replace(".", "").ToUpper();
                item.AttachmentObjectCollection[0].Description = descriptionControl.Value != null ? descriptionControl.Value.ToString() : "";
            }
            else if (removeAttachmentClicked)
            {
                item.AttachmentId = null;
                item.AttachmentObjectCollection = null;
            }
        }
        protected void CollectiveAgreementGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            UnionCollectiveAgreement item = (UnionCollectiveAgreement)e.Item.DataItem;
            item.LabourUnionId = LabourUnionId;
            PopulateAttachmentObjectCollection(item, e);

            if (IsDataExternallyLoaded)
            {
                if (DataItemCollection.Count > 0)
                    item.UnionCollectiveAgreementId = ((UnionCollectiveAgreement)DataItemCollection[DataItemCollection.Count - 1]).UnionCollectiveAgreementId - 1;

                ItemChangedEventArgs args = new ItemChangedEventArgs() { Items = DataItemCollection };
                ((UnionCollectiveAgreementCollection)DataItemCollection).Add(item);
                OnItemChanged(args);
            }
            else
                ((DataItemCollection<UnionCollectiveAgreement>)DataItemCollection).Add(Common.ServiceWrapper.HumanResourcesClient.InsertUnionCollectiveAgreement(item));
        }
        protected void CollectiveAgreementGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                UnionCollectiveAgreement item = (UnionCollectiveAgreement)e.Item.DataItem;
                PopulateAttachmentObjectCollection(item, e);

                if (IsDataExternallyLoaded)
                {
                    ItemChangedEventArgs args = new ItemChangedEventArgs() { Items = DataItemCollection };
                    OnItemChanged(args);
                }
                else
                    Common.ServiceWrapper.HumanResourcesClient.UpdateUnionCollectiveAgreement(item).CopyTo((UnionCollectiveAgreement)DataItemCollection[item.Key]);

                removeAttachmentClicked = false;
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void CollectiveAgreementGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (IsDataExternallyLoaded)
                {
                    ItemChangedEventArgs args = new ItemChangedEventArgs() { Items = DataItemCollection };
                    OnItemChanged(args);
                }
                else
                    Common.ServiceWrapper.HumanResourcesClient.DeleteUnionCollectiveAgreement((UnionCollectiveAgreement)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}