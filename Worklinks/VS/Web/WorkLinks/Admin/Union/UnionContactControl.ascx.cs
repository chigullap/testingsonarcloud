﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.Union
{
    public partial class UnionContactControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private int selectIndex;
        #endregion

        #region properties
        private long PersonId { get { return ((LabourUnionSummary)Data[UnionContactInfoGrid.SelectedValue.ToString()]).PersonId; } }
        private long LabourUnionId
        {
            get
            {
                if (Data != null)
                {
                    if (Data.Count > 0)
                        return ((LabourUnionSummary)Data[UnionContactInfoGrid.SelectedValue.ToString()]).LabourUnionId;
                    else
                        return -1;
                }
                else
                    return 0;
            }
        }
        public bool IsViewMode { get { return UnionDetailControl.IsViewMode && AddressDetailControl.IsViewMode && PersonPhoneControl.IsViewMode && CollectiveAgreementControl.IsViewMode; } }
        public bool IsUpdate { get { return UnionContactInfoGrid.IsEditMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.UnionContact.AddFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.UnionContact.DeleteFlag; } }
        protected LabourUnionSummary UnionContactRecordSummary
        {
            get
            {
                LabourUnionSummary UnionContactRecord = null;

                if (UnionDetailControl.IsInsertMode)
                    UnionContactRecord = new LabourUnionSummary() { LabourUnionId = LabourUnionId };
                else
                {
                    if (UnionContactInfoGrid.SelectedValue != null)
                        UnionContactRecord = (LabourUnionSummary)Data[UnionContactInfoGrid.SelectedValue.ToString()];
                }

                return UnionContactRecord;
            }
        }
        protected LabourUnionSummaryCollection UnionContactRecordCollection
        {
            get
            {
                LabourUnionSummaryCollection unionSummary = new LabourUnionSummaryCollection();

                if (UnionContactRecordSummary != null)
                    unionSummary.Load(UnionContactRecordSummary);

                return unionSummary;
            }
        }
        public int SelectIndex
        {
            get { return selectIndex; }
            set { selectIndex = value; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.UnionContact.ViewFlag);

            WireEvents();

            SelectIndex = 0;

            if (!IsPostBack)
            {
                if (LabourUnionId == 0)
                    LoadUnionContactInformation(null);
                else
                    LoadUnionContactInformation(LabourUnionId);
            }
        }
        protected void InitializeControls()
        {
            ContactDetailTabStrip.Enabled = IsViewMode;
            SetGridEnabledMode(UnionContactInfoGrid, IsViewMode);

            //enable/disable toolbar buttons
            for (int i = 0; i < UnionContactToolBar.Items.Count; i++)
                UnionContactToolBar.Items[i].Enabled = IsViewMode;
        }
        protected override void OnPreRender(EventArgs e)
        {
            InitializeControls();
            base.OnPreRender(e);
        }
        protected void WireEvents()
        {
            UnionDetailControl.NeedDataSource += new Union.UnionDetailControl.NeedDataSourceEventHandler(UnionDetailControl_NeedDataSource);
            UnionContactInfoGrid.NeedDataSource += new GridNeedDataSourceEventHandler(UnionContactInfoGrid_NeedDataSource);
            UnionDetailControl.Updating += new Union.UnionDetailControl.UpdatingEventHandler(UnionDetailControl_Updating);
            UnionDetailControl.Inserting += new Union.UnionDetailControl.InsertingEventHandler(UnionDetailControl_Inserting);
        }
        protected void LoadUnionContactInformation(long? labourUnionId)
        {
            Data = LabourUnionSummaryCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetLabourUnion(labourUnionId));

            ((WLPToolBarButton)UnionContactToolBar.FindButtonByCommandName("Add")).Visible = AddFlag;
            ((WLPToolBarButton)UnionContactToolBar.FindButtonByCommandName("Delete")).Visible = DeleteFlag;
        }
        #endregion

        #region button click
        protected void UnionContactToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("add"))
            {
                ContactDetailTabStrip.SelectedIndex = 0;
                ContactDetailMultiPage.SelectedIndex = 0;
                UnionDetailControl.ChangeModeInsert();
            }
            else
                DeleteContact(UnionContactInfoGrid.SelectedValue.ToString());
        }
        protected void DeleteContact(String contactKey)
        {
            try
            {
                LabourUnion unionContact = (LabourUnion)Data[contactKey];
                Common.ServiceWrapper.HumanResourcesClient.DeleteUnionContact(unionContact);
                Data.Remove(unionContact.Key);

                UnionContactInfoGrid.SelectedIndexes.Clear();
                UnionContactInfoGrid.Rebind();
                ContactDetailMultiPage.DataBind();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region event handlers
        void UnionDetailControl_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            LabourUnionSummary unionContactInfo = (LabourUnionSummary)e.DataItem;
            LabourUnionSummary newSummaryObject = new LabourUnionSummary();
            newSummaryObject = Common.ServiceWrapper.HumanResourcesClient.InsertUnionContactInformationSummaryData(unionContactInfo);

            //add to collection and refresh main grid
            ((DataItemCollection<LabourUnionSummary>)Data).Add(newSummaryObject);

            //get Last Item addred index
            SelectIndex = ((DataItemCollection<LabourUnionSummary>)Data).Count - 1;
            UnionContactInfoGrid.Rebind();
            SelectIndex = 0;

            //set vars for garbage cleanup since we are done with them
            newSummaryObject = null;
            unionContactInfo = null;
        }
        void UnionDetailControl_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                LabourUnionSummary unionContactInfo = (LabourUnionSummary)e.DataItem;
                unionContactInfo = Common.ServiceWrapper.HumanResourcesClient.UpdateUnionContactData(unionContactInfo);
                unionContactInfo.CopyTo(UnionContactRecordSummary);

                UnionContactInfoGrid.Rebind();

                unionContactInfo = null;
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void UnionDetailControl_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            UnionDetailControl.UnionContact = UnionContactRecordCollection;
        }
        void UnionContactInfoGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            UnionContactInfoGrid.DataSource = Data;
        }
        protected void UnionContactInfoGrid_DataBound(object sender, EventArgs e)
        {
            if (UnionContactInfoGrid.Items.Count > 0)
            {
                UnionContactInfoGrid.Items[SelectIndex].Selected = true;
                GetSelectedUnionContact();
            }
        }
        protected void UnionContactInfoGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetSelectedUnionContact();
        }
        private void GetSelectedUnionContact()
        {
            if (UnionContactInfoGrid.SelectedItems.Count > 0)
            {
                UnionDetailControl.DataBind();                                  //load the union summary information
                AddressDetailControl.SetPersonId(PersonId);                     //load the address information
                PersonPhoneControl.SetPersonId(PersonId);                       //load the phone information
                CollectiveAgreementControl.SetLabourUnionId(LabourUnionId);     //load the union information
            }
        }
        private void SetGridEnabledMode(WLPGrid grid, bool isEnabled)
        {
            grid.Enabled = isEnabled;
            grid.ClientSettings.EnablePostBackOnRowClick = isEnabled;
            grid.ClientSettings.Resizing.AllowColumnResize = isEnabled;
            grid.ClientSettings.Selecting.AllowRowSelect = isEnabled;
            grid.ClientSettings.AllowKeyboardNavigation = isEnabled;
        }
        protected void CollectiveAgreementView_OnInit(object sender, EventArgs e)
        {
            ((WebControl)sender).Visible = Common.Security.RoleForm.UnionCollectiveAgreemnt.ViewFlag;
        }
        #endregion
    }
}