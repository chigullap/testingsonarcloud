﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UnionDetailControl.ascx.cs" Inherits="WorkLinks.Admin.Union.UnionDetailControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPFormView 
    ID="UnionView" 
    runat="server" 
    RenderOuterTable="false" 
    DataKeyNames="Key"
    OnNeedDataSource="UnionView_NeedDataSource"  
    OnUpdating="UnionView_Updating" 
    OnInserting="UnionView_Inserting">

    <ItemTemplate>
        <wasp:WLPToolBar ID="UnionDetailToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Text="Edit" Visible='<%# UpdateFlag %>' ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="edit" ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="EnglishDescription" runat="server" Value='<%# Eval("EnglishDescription") %>' ResourceName="EnglishDescription" ReadOnly="true" Mandatory="true"></wasp:TextBoxControl>
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="FrenchDescription" runat="server" Value='<%# Eval("FrenchDescription") %>' ResourceName="FrenchDescription" ReadOnly="true" Mandatory="true"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="EffectiveDate" runat="server" Value='<%# Bind("EffectiveDate") %>' ResourceName="EffectiveDate" ReadOnly="true" Mandatory="true"></wasp:DateControl>
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="TitleCode" runat="server" Value='<%# Bind("TitleCode") %>' Type="TitleCode" OnDataBinding="Code_NeedDataSource" ResourceName="ContactTitleCode" ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="FirstName" runat="server" Value='<%# Eval("FirstName") %>' ResourceName="FirstName" ReadOnly="true" Mandatory="true"></wasp:TextBoxControl>
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="LastName" runat="server" Value='<%# Eval("LastName") %>' ResourceName="LastName" ReadOnly="true" Mandatory="true"></wasp:TextBoxControl>
                    </td>
                </tr>
            </table>
        </fieldset> 
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar ID="UnionDetailToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="EnglishDescription" runat="server" Value='<%# Bind("EnglishDescription") %>' ResourceName="EnglishDescription" Mandatory="true" TabIndex="010"></wasp:TextBoxControl>
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="FrenchDescription" runat="server" Value='<%# Bind("FrenchDescription") %>' ResourceName="FrenchDescription" Mandatory="true" TabIndex="020"></wasp:TextBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="EffectiveDate" runat="server" Value='<%# Bind("EffectiveDate") %>' ResourceName="EffectiveDate" Mandatory="true" TabIndex="030"></wasp:DateControl>
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="TitleCode" runat="server" Value='<%# Bind("TitleCode") %>' Type="TitleCode" OnDataBinding="Code_NeedDataSource" ResourceName="ContactTitleCode" TabIndex="040"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="FirstName" runat="server" Value='<%# Bind("FirstName") %>' ResourceName="FirstName" Mandatory="true" TabIndex="050"></wasp:TextBoxControl>
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="LastName" runat="server" Value='<%# Bind("LastName") %>' ResourceName="LastName" Mandatory="true" TabIndex="060"></wasp:TextBoxControl>
                    </td>
                </tr>
            </table>
        </fieldset> 
    </EditItemTemplate>
</wasp:WLPFormView>