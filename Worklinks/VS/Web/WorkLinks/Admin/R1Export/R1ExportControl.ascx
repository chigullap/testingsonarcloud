﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="R1ExportControl.ascx.cs" Inherits="WorkLinks.Admin.R1Export.R1ExportControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="SearchPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SearchPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="SearchPanel" runat="server">
    <wasp:WLPFormView
        ID="R1ExportDetailView"
        runat="server"
        RenderOuterTable="false"
        DataKeyNames="Key"
        OnNeedDataSource="R1ExportDetailView_NeedDataSource"
        OnUpdating="R1ExportDetailView_Updating">

        <ItemTemplate>
            <wasp:WLPToolBar ID="R1ExportDetailToolBar1" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="ExportDetailToolBar1_ButtonClick">
                <Items>
                    <wasp:WLPToolBarButton Text="Edit" ImageUrl="~/App_Themes/Default/Edit.gif" Visible='<%# UpdateFlag %>' CommandName="edit" ResourceName="Edit" CausesValidation="false"></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton Text="Export" CommandName="Export" ResourceName="Export" CausesValidation="false"></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton Text="*R1CICExport" CommandName="R1CICExport" ResourceName="R1CICExport" CausesValidation="false" OnPreRender="R1CICExport_PreRender"></wasp:WLPToolBarButton>
                </Items>
            </wasp:WLPToolBar>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="TransmitterNumber" runat="server" LabelText="TransmitterNumber" ResourceName="TransmitterNumber" Value='<%# Bind("RevenueQuebecTransmitterRecord.TransmitterNumber") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="AddressLine1" runat="server" LabelText="AddressLine1" ResourceName="AddressLine1" Value='<%# Bind("RevenueQuebecTransmitterRecord.PersonAddress.AddressLine1") %>' ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                       <td>
                            <wasp:TextBoxControl ID="TransmitterName" runat="server" LabelText="TransmitterName" ResourceName="TransmitterName" Value='<%# Bind("RevenueQuebecTransmitterRecord.TransmitterName") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="AddressLine2" runat="server" LabelText="AddressLine2" ResourceName="AddressLine2" Value='<%# Bind("RevenueQuebecTransmitterRecord.PersonAddress.AddressLine2") %>' ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="CertificationNumber" runat="server" LabelText="CertificationNumber" ResourceName="CertificationNumber" Value='<%# Bind("RevenueQuebecTransmitterRecord.CertificationNumber") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="City" runat="server" LabelText="City" ResourceName="City" Value='<%# Bind("RevenueQuebecTransmitterRecord.PersonAddress.City") %>' ReadOnly="True" />
                        </td>
                    </tr>
                    <tr>
                         <td>
                            <wasp:TextBoxControl ID="ContactFirstName" runat="server" LabelText="ContactFirstName" ResourceName="ContactFirstName" Value='<%# Bind("RevenueQuebecTransmitterRecord.Person.FirstName") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="CountryCode" runat="server" Type="CountryCode" AutoPostback="true" OnDataBinding="CountryCode_NeedDataSource" OnSelectedIndexChanged="CountryCode_SelectedIndexChanged" ResourceName="CountryCode" Value='<%# Bind("RevenueQuebecTransmitterRecord.PersonAddress.CountryCode") %>' ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="ContactLastName" runat="server" LabelText="ContactLastName" ResourceName="ContactLastName" Value='<%# Bind("RevenueQuebecTransmitterRecord.Person.LastName") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="ProvinceCode" runat="server" Type="ProvinceStateCode" ResourceName="ProvinceStateCode" OnSelectedIndexChanged="ProvinceStateCode_SelectedIndexChanged" ReadOnly="true"></wasp:ComboBoxControl>
                        </td>
                    </tr>
                    <tr>
                         <td>
                            <wasp:MaskedControl Mask="###-###-####" ID="Phone" runat="server" LabelText="Phone" ResourceName="Phone" Value='<%# Bind("RevenueQuebecTransmitterRecord.Phone.PrimaryContactValue") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="Postal" runat="server" LabelText="Postal" ResourceName="Postal" Value='<%# Bind("RevenueQuebecTransmitterRecord.PersonAddress.PostalZipCode") %>' ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="Email" runat="server" LabelText="Email" ResourceName="Email" Value='<%# Bind("RevenueQuebecTransmitterRecord.Email.PrimaryContactValue") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="Extension" runat="server" LabelText="Extension" ResourceName="Extension" Value='<%# Bind("RevenueQuebecTransmitterRecord.Phone.SecondaryContactValue") %>' ReadOnly="true" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </ItemTemplate>

        <EditItemTemplate>
            <wasp:WLPToolBar ID="R1ExportDetailToolBar2" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update"></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert" />
                    <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" Visible='<%# !IsInsertMode %>' CommandName="cancel" CausesValidation="False" ResourceName="Cancel"></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" Visible='<%# IsInsertMode %>' onClick="Close()" CausesValidation="False" ResourceName="Cancel"></wasp:WLPToolBarButton>
                </Items>
            </wasp:WLPToolBar>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="TransmitterNumber" runat="server" LabelText="TransmitterNumber" ResourceName="TransmitterNumber" Value='<%# Bind("RevenueQuebecTransmitterRecord.TransmitterNumber") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="AddressLine1" runat="server" LabelText="AddressLine1" ResourceName="AddressLine1" Value='<%# Bind("RevenueQuebecTransmitterRecord.PersonAddress.AddressLine1") %>' />
                        </td>
                    </tr>
                    <tr>
                       <td>
                            <wasp:TextBoxControl ID="TransmitterName" runat="server" LabelText="TransmitterName" ResourceName="TransmitterName" Value='<%# Bind("RevenueQuebecTransmitterRecord.TransmitterName") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="AddressLine2" runat="server" LabelText="AddressLine2" ResourceName="AddressLine2" Value='<%# Bind("RevenueQuebecTransmitterRecord.PersonAddress.AddressLine2") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="CertificationNumber" runat="server" LabelText="CertificationNumber" ResourceName="CertificationNumber" Value='<%# Bind("RevenueQuebecTransmitterRecord.CertificationNumber") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="City" runat="server" LabelText="City" ResourceName="City" Value='<%# Bind("RevenueQuebecTransmitterRecord.PersonAddress.City") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="ContactFirstName" runat="server" LabelText="ContactFirstName" ResourceName="ContactFirstName" Value='<%# Bind("RevenueQuebecTransmitterRecord.Person.FirstName") %>' />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="CountryCode" runat="server" Type="CountryCode" AutoPostback="true" OnDataBinding="CountryCode_NeedDataSource" OnSelectedIndexChanged="CountryCode_SelectedIndexChanged" ResourceName="CountryCode" Value='<%# Bind("RevenueQuebecTransmitterRecord.PersonAddress.CountryCode") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="ContactLastName" runat="server" LabelText="ContactLastName" ResourceName="ContactLastName" Value='<%# Bind("RevenueQuebecTransmitterRecord.Person.LastName") %>' />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="ProvinceCode" runat="server" Type="ProvinceStateCode" ResourceName="ProvinceStateCode" OnSelectedIndexChanged="ProvinceStateCode_SelectedIndexChanged" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:MaskedControl Mask="###-###-####" ID="Phone" runat="server" LabelText="Phone" ResourceName="Phone" Value='<%# Bind("RevenueQuebecTransmitterRecord.Phone.PrimaryContactValue") %>' />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="Postal" runat="server" LabelText="Postal" ResourceName="Postal" Value='<%# Bind("RevenueQuebecTransmitterRecord.PersonAddress.PostalZipCode") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="Email" runat="server" LabelText="Email" ResourceName="Email" Value='<%# Bind("RevenueQuebecTransmitterRecord.Email.PrimaryContactValue") %>' />
                        </td>
                        <td>
                            <div style="float: left">
                                <wasp:TextBoxControl ID="Extension" runat="server" ResourceName="Extension" Value='<%# Bind("RevenueQuebecTransmitterRecord.Phone.SecondaryContactValue") %>' />
                            </div>
                            <div style="float: left; padding-left: 50px;">
                                <asp:RegularExpressionValidator ID="ExtensionLength" runat="server" ForeColor="Red" ControlToValidate="Extension" ErrorMessage="Not Valid" ValidationExpression="^[0-9]{0,9}$">* Not Valid</asp:RegularExpressionValidator>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </EditItemTemplate>
    </wasp:WLPFormView>
</asp:Panel>

<script type="text/javascript">
    function getRadWindow() {
        var popup = null;
        if (window.radWindow)
            popup = window.radWindow;
        else if (window.frameElement.radWindow)
            popup = window.frameElement.radWindow;
        return popup;
    }

    function Close(button, args) {
        var popup = getRadWindow();
        setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
    }

    function invokeDownload(type) {
        var frame = document.createElement("frame");
        frame.src = getUrl('/YearEnd/DownLoadXml/' + type), null;
        frame.style.display = "none";
        document.body.appendChild(frame);
    }

</script>