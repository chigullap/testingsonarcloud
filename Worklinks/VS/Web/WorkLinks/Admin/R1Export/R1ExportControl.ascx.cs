﻿using System;
using System.Web.UI.WebControls;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Admin.R1Export
{
    public partial class R1ExportControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private const String _provinceCodeKey = "ProvinceCodeKey";
        private String _provinceCode;
        #endregion

        #region properties
        protected bool EnableR1CICExportButton { get { return Common.Security.RoleForm.R1CICExport.ViewFlag && ExportType == "R1"; } }
        public bool IsEditMode { get { return R1ExportDetailView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool IsInsertMode { get { return R1ExportDetailView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsViewMode { get { return R1ExportDetailView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.R1Export.UpdateFlag; } }
        protected String ExportType { get { return (String)Page.RouteData.Values["exportType"]; } }
        private long YearEndId { get { return Convert.ToInt64(Page.RouteData.Values["yearEndId"]); } }
        private String Year { get { return Convert.ToString(Page.RouteData.Values["year"]); } }
        public String CurrentProvince
        {
            get
            {
                if (_provinceCode == null)
                {
                    Object obj = ViewState[_provinceCodeKey];

                    if (obj != null)
                        _provinceCode = (String)(obj);
                }

                return _provinceCode;
            }
            set
            {
                _provinceCode = value;
                ViewState[_provinceCodeKey] = _provinceCode;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.R1Export.ViewFlag);

            if (ExportType == "R1")
                this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "R1Export"));
            else if (ExportType == "R2")
                this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "R2Export"));

            WireEvents();

            if (!IsPostBack)
            {
                if (YearEndId > 0) //should always be > 0 at this point
                {
                    //load data
                    LoadDetails();

                    if (Data.Count == 0)
                    {
                        GenerateDefaultObject();
                        R1ExportDetailView.ChangeMode(FormViewMode.Insert);
                    }
                }
            }
        }
        private void GenerateDefaultObject()
        {
            /*  if no record exists in RevenueQuebecR1Export for supplied year_end_id, generate empty business objects with the following defaults:
                 a)  RevenueQuebecR1Export  YearEndId=current year_end_id
                 b)  RevenueQuebecTransmitterRecord  RevenueQuebecTransmitterTypeCode="2", LanguageCode="EN"
                 c)  PersonContactChannel (EMAIL) PrimaryFlag=1, ContactChannelTypeCode='BUSINESS'
                 d)  PersonContactChannel (Phone) PrimaryFlag=1, ContactChannelTypeCode='WORK'
                 e)  PersonAddress PersonAddressTypeCode="CA", PrimaryFlag=1
             */

            RevenueQuebecR1ExportCollection exportCollection = new RevenueQuebecR1ExportCollection();
            RevenueQuebecR1Export temp = new RevenueQuebecR1Export();

            //RevenueQuebecR1Export fields
            temp.YearEndFormCode = ExportType;
            temp.YearEndId = YearEndId;

            //RevenueQuebecTransmitterRecord fields
            temp.RevenueQuebecTransmitterRecord = new RevenueQuebecTransmitterRecord();
            temp.RevenueQuebecTransmitterRecord.RevenueQuebecTransmitterTypeCode = "2";
            temp.RevenueQuebecTransmitterRecord.LanguageCode = "EN";
            temp.RevenueQuebecTransmitterRecord.TransmitterNumber = Common.ApplicationParameter.TransmitterNumber;
            temp.RevenueQuebecTransmitterRecord.TransmitterName = Common.ApplicationParameter.TransmitterName;

            if (ExportType == "R1")
                temp.RevenueQuebecTransmitterRecord.CertificationNumber = Common.ApplicationParameter.RevenuQuebecR1CertificationNumber;
            else
                temp.RevenueQuebecTransmitterRecord.CertificationNumber = Common.ApplicationParameter.RevenuQuebecR2CertificationNumber;

            //PersonContactChannel (Email)
            temp.RevenueQuebecTransmitterRecord.Email = new PersonContactChannel();
            temp.RevenueQuebecTransmitterRecord.Email.PrimaryFlag = true;
            temp.RevenueQuebecTransmitterRecord.Email.ContactChannelTypeCode = "BUSINESS";

            //PersonContactChannel (Phone)
            temp.RevenueQuebecTransmitterRecord.Phone = new PersonContactChannel();
            temp.RevenueQuebecTransmitterRecord.Phone.PrimaryFlag = true;
            temp.RevenueQuebecTransmitterRecord.Phone.ContactChannelTypeCode = "WORK";

            //PersonAddress
            temp.RevenueQuebecTransmitterRecord.PersonAddress = new PersonAddress();
            temp.RevenueQuebecTransmitterRecord.PersonAddress.PrimaryFlag = true;
            temp.RevenueQuebecTransmitterRecord.PersonAddress.PersonAddressTypeCode = "CA";

            //instantiate Person object
            temp.RevenueQuebecTransmitterRecord.Person = new Person();

            exportCollection.Add(temp);
            Data = exportCollection;
        }
        protected void WireEvents()
        {
            R1ExportDetailView.Inserting += new WLPFormView.ItemInsertingEventHandler(R1ExportDetailView_Inserting);
            R1ExportDetailView.ItemCreated += new EventHandler(R1ExportDetailView_ItemCreated);
        }
        protected void LoadDetails()
        {
            Data = RevenueQuebecR1ExportCollection.ConvertCollection(Common.ServiceWrapper.XmlClient.ExportR1Details(Year, ExportType));
            R1ExportDetailView.DataBind();
        }
        #endregion

        #region event handlers
        protected void R1ExportDetailView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            R1ExportDetailView.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void CountryCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
            BindProvinceCombo(sender);
        }
        protected void CountryCode_SelectedIndexChanged(object o, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindProvinceCombo(o);
            ((ComboBoxControl)o).BindingContainer.FindControl("ContactFirstName").Focus();
        }
        protected void ProvinceStateCode_SelectedIndexChanged(object o, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            CurrentProvince = e.Value;
        }
        protected void ExportDetailToolBar1_ButtonClick(object sender, Telerik.Web.UI.RadToolBarEventArgs e)
        {
            if (((Telerik.Web.UI.RadToolBarButton)e.Item).CommandName.ToLower().Equals("export"))
            {
                if (ExportType == "R1")
                {
                    Session["HandOff"] = Common.ServiceWrapper.XmlClient.ExportR1XmlFile(Convert.ToInt32(Year), Common.ApplicationParameter.ImportExportProcessingDirectory,
                        Common.ApplicationParameter.ProprietorSocialInsuranceNumber1, Common.ApplicationParameter.ProprietorSocialInsuranceNumber2,
                        Common.ApplicationParameter.RevenueQuebecTaxId);

                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "invokeDownload", "invokeDownload('R1');", true);
                }
                else if (ExportType == "R2")
                {
                    Session["HandOff"] = Common.ServiceWrapper.XmlClient.ExportR2XmlFile(Convert.ToInt32(Year), Common.ApplicationParameter.ImportExportProcessingDirectory,
                        Common.ApplicationParameter.ProprietorSocialInsuranceNumber1, Common.ApplicationParameter.ProprietorSocialInsuranceNumber2,
                        Common.ApplicationParameter.RevenueQuebecTaxId);

                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "invokeDownload", "invokeDownload('R2');", true);
                }
            }
            //CIC R1 export
            else if (((Telerik.Web.UI.RadToolBarButton)e.Item).CommandName.ToLower().Equals("r1cicexport"))
            {
                Session["HandOff"] = Common.ServiceWrapper.XmlClient.ExportR1CIC(Convert.ToInt32(Year), Common.ApplicationParameter.ProprietorSocialInsuranceNumber1,
                    Common.ApplicationParameter.ProprietorSocialInsuranceNumber2, Common.ApplicationParameter.RevenueQuebecTaxId) ?? new byte[0];
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "invokeDownload", "invokeDownload('CICR1');", true);
            }
        }
        void R1ExportDetailView_ItemCreated(object sender, EventArgs e)
        {
            if (IsInsertMode)
            {
                TextBoxControl transmitterNumber = (TextBoxControl)R1ExportDetailView.FindControl("TransmitterNumber");
                transmitterNumber.Value = Common.ApplicationParameter.TransmitterNumber;

                TextBoxControl transmitterName = (TextBoxControl)R1ExportDetailView.FindControl("TransmitterName");
                transmitterName.Value = Common.ApplicationParameter.TransmitterName;

                TextBoxControl certificationNumber = (TextBoxControl)R1ExportDetailView.FindControl("CertificationNumber");
                if (ExportType == "R1")
                    certificationNumber.Value = Common.ApplicationParameter.RevenuQuebecR1CertificationNumber;
                else if (ExportType == "R2")
                    certificationNumber.Value = Common.ApplicationParameter.RevenuQuebecR2CertificationNumber;
            }
        }
        protected void R1CICExport_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableR1CICExportButton; }
        #endregion

        #region handle updates
        void R1ExportDetailView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            RevenueQuebecR1Export info = (RevenueQuebecR1Export)e.DataItem;

            //since fields dont bind automatically, we cant use a cast on the e.DataItem correctly, so we are finding each textbox and extracting its value
            getData(info);

            //call the insert
            Common.ServiceWrapper.XmlClient.InsertExportR1Details(info);

            LoadDetails();
        }
        protected void R1ExportDetailView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                RevenueQuebecR1Export info = (RevenueQuebecR1Export)e.DataItem;

                //since fields dont bind automatically, we cant use a cast on the e.DataItem correctly, so we are finding each textbox and extracting its value
                getData(info);

                ////call the update
                Common.ServiceWrapper.XmlClient.UpdateExportR1Details(info).CopyTo((RevenueQuebecR1Export)Data[info.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        private void BindProvinceCombo(object sender)
        {
            if (sender is ComboBoxControl)
            {
                ComboBoxControl countryCombo = (ComboBoxControl)sender;
                WLPFormView formItem = countryCombo.BindingContainer as WLPFormView;
                ComboBoxControl provinceCombo = (ComboBoxControl)formItem.FindControl("ProvinceCode");
                provinceCombo.DataSource = Common.ServiceWrapper.CodeClient.GetProvinceStateCode(countryCombo.SelectedValue);
                provinceCombo.DataBind();

                if (formItem.DataItem != null && formItem.DataItem is RevenueQuebecR1Export)
                {
                    provinceCombo.Value = ((RevenueQuebecR1Export)formItem.DataItem).RevenueQuebecTransmitterRecord.PersonAddress.ProvinceStateCode;
                    CurrentProvince = ((RevenueQuebecR1Export)formItem.DataItem).RevenueQuebecTransmitterRecord.PersonAddress.ProvinceStateCode;
                }
                else
                {
                    provinceCombo.Value = "";
                    CurrentProvince = null;
                }
            }
        }
        private RevenueQuebecR1Export getData(RevenueQuebecR1Export info)
        {
            //address line 1
            BaseControl tempObj = (BaseControl)R1ExportDetailView.FindControl("AddressLine1");
            if (tempObj != null) info.RevenueQuebecTransmitterRecord.PersonAddress.AddressLine1 = (string)tempObj.Value;

            //transmitter number
            tempObj = (BaseControl)R1ExportDetailView.FindControl("TransmitterNumber");
            if (tempObj != null) info.RevenueQuebecTransmitterRecord.TransmitterNumber = (string)tempObj.Value;

            //address line 2
            tempObj = (BaseControl)R1ExportDetailView.FindControl("AddressLine2");
            if (tempObj != null) info.RevenueQuebecTransmitterRecord.PersonAddress.AddressLine2 = (string)tempObj.Value;

            //transmitter name
            tempObj = (BaseControl)R1ExportDetailView.FindControl("TransmitterName");
            if (tempObj != null) info.RevenueQuebecTransmitterRecord.TransmitterName = (string)tempObj.Value;

            //city
            tempObj = (BaseControl)R1ExportDetailView.FindControl("City");
            if (tempObj != null) info.RevenueQuebecTransmitterRecord.PersonAddress.City = (string)tempObj.Value;

            //CertificationNumber
            tempObj = (BaseControl)R1ExportDetailView.FindControl("CertificationNumber");
            if (tempObj != null) info.RevenueQuebecTransmitterRecord.CertificationNumber = (string)tempObj.Value;

            //country
            ComboBoxControl tempCombo = (ComboBoxControl)R1ExportDetailView.FindControl("CountryCode");
            if (tempCombo != null) info.RevenueQuebecTransmitterRecord.PersonAddress.CountryCode = tempCombo.SelectedValue.ToString();

            //first name
            tempObj = (BaseControl)R1ExportDetailView.FindControl("ContactFirstName");
            if (tempObj != null) info.RevenueQuebecTransmitterRecord.Person.FirstName = (string)tempObj.Value;

            //province, kept track in session
            info.RevenueQuebecTransmitterRecord.PersonAddress.ProvinceStateCode = CurrentProvince;

            //last name
            tempObj = (BaseControl)R1ExportDetailView.FindControl("ContactLastName");
            if (tempObj != null) info.RevenueQuebecTransmitterRecord.Person.LastName = (string)tempObj.Value;

            //postal/zip code
            tempObj = (BaseControl)R1ExportDetailView.FindControl("Postal");
            if (tempObj != null) info.RevenueQuebecTransmitterRecord.PersonAddress.PostalZipCode = (string)tempObj.Value;

            //phone number
            tempObj = (BaseControl)R1ExportDetailView.FindControl("Phone");
            if (tempObj != null) info.RevenueQuebecTransmitterRecord.Phone.PrimaryContactValue = (string)tempObj.Value;

            //phone extension
            tempObj = (BaseControl)R1ExportDetailView.FindControl("Extension");
            if (tempObj != null) info.RevenueQuebecTransmitterRecord.Phone.SecondaryContactValue = (string)tempObj.Value;

            //email
            tempObj = (BaseControl)R1ExportDetailView.FindControl("Email");
            if (tempObj != null) info.RevenueQuebecTransmitterRecord.Email.PrimaryContactValue = (string)tempObj.Value;

            return info;
        }
    }
}