﻿//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net.Http.Formatting;
//using System.Web.Http;
//using WorkLinks.BusinessLayer.BusinessObjects;

//namespace WorkLinks.Controllers
//{
//    [RoutePrefix("api/employee")]
//    [Authorize] 
//    public class EmployeeController : BaseController
//    {
//        private BusinessLayer.BusinessLogic.EmployeeManagement _employeeManagement;
        
//        private BusinessLayer.BusinessLogic.EmployeeManagement EmployeeManagement => _employeeManagement ?? (_employeeManagement = new BusinessLayer.BusinessLogic.EmployeeManagement());

//        // GET: api/Employee
//        /// <summary>
//        /// Retrieves a list of employees matching the passed criteria
//        /// </summary>
//        /// <param name="searchCriteria"></param>
//        /// <returns></returns>
//        [HttpGet]
//        public IEnumerable<Employee> Get(string searchCriteria)
//        {
//            EmployeeCriteria criteria = JsonConvert.DeserializeObject<EmployeeCriteria>(searchCriteria);
//            return Common.ServiceWrapper.HumanResourcesClient.GetEmployee(DatabaseUser, criteria);
//        }

//        /// <summary>
//        /// Retrieves the employee that matches the passed id
//        /// </summary>
//        /// <param name="id">Unique employee identifier</param>
//        /// <returns>The matched Employee object if found, null if not</returns>
//        /// <example>GET: api/Employee/5</example>
//        [HttpGet]
//        public Employee Get(int id)
//        {
//            EmployeeCriteria criteria = new EmployeeCriteria {EmployeeId = id};
//            return Common.ServiceWrapper.HumanResourcesClient.GetEmployee(DatabaseUser, criteria).FirstOrDefault();
//        }

//        /// <summary>
//        /// Creates a new employee using the passed information
//        /// </summary>
//        /// <param name="value">String representation of an Employee object</param>
//        /// <returns>The newly created Employee object with all identifiers</returns>
//        /// <example>POST: api/Employee</example>
//        [HttpPost]
//        public Employee Post([FromBody]FormDataCollection value)
//        {
//            Employee employee = JsonConvert.DeserializeObject<Employee>(ExtractFormDataPayload(value));
//            return Common.ServiceWrapper.HumanResourcesClient.InsertEmployee(DatabaseUser, employee);
//        }

//        /// <summary>
//        /// Updates the passed employee
//        /// </summary>
//        /// <param name="id">Employee identifier</param>
//        /// <param name="value"></param>
//        /// <example>PUT: api/Employee/5</example>
//        [HttpPut]
//        public IHttpActionResult Put(int id, [FromBody]FormDataCollection value)
//        {
//            string uc = ExtractFormDataPayload(value);
//            UpdateEmployeeCriteria criteria = JsonConvert.DeserializeObject<UpdateEmployeeCriteria>(uc);

//            if (criteria.Employee?.EmployeeId != id)
//            {
//                return BadRequest("The id in the route does not match the employee object passed");
//            }

//            Common.ServiceWrapper.HumanResourcesClient.UpdateEmployee(DatabaseUser, criteria.Employee, criteria.DeletedItems);
//            return Ok();
//        }

//        /// <summary>
//        /// Deletes the Employee identified by the passed id
//        /// </summary>
//        /// <param name="id">Employee identifier</param>
//        /// <example>DELETE: api/Employee/5</example>
//        [HttpDelete]
//        public void Delete(int id)
//        {
//            EmployeeCriteria criteria = new EmployeeCriteria { EmployeeId = id };
//            Employee employee = EmployeeManagement.GetEmployee(DatabaseUser, criteria).FirstOrDefault();
//            if (employee != null)
//            {
//                Common.ServiceWrapper.HumanResourcesClient.DeleteEmployee(DatabaseUser, employee);
//            }
//        }

//        public class UpdateEmployeeCriteria
//        {
//            public Employee Employee { get; set; }
//            public List<ContactType> DeletedItems { get; set; }
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="value"></param>
//        /// <returns></returns>
//        private string ExtractFormDataPayload(FormDataCollection value)
//        {
//            if(value == null )
//                throw new ArgumentNullException(nameof(value));

//            KeyValuePair<string, string> val = value.FirstOrDefault();
//            if (string.IsNullOrWhiteSpace(val.Key))
//                throw new ArgumentException("No payload object was posted in the body", nameof(value));

//            return val.Key;

//        }
//    }
//}
