﻿//using Newtonsoft.Json;
//using Newtonsoft.Json.Linq;
//using System;
//using System.Collections.Generic;
//using System.Collections.Specialized;
//using System.IO;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Net.Http.Formatting;
//using System.Web.Http;
//using System.Web.Mvc;
//using WorkLinks.Models.Criteria;

//namespace WorkLinks.Controllers
//{
//    [System.Web.Http.Authorize]
//    public class ImportController : BaseController
//    {


//        // POST: api/Import
//        [System.Web.Http.HttpPost]
//        [System.Web.Http.Route("api/Import")]
//        public String Post(FormDataCollection formData)
//        {
//            if (Common.Security.RoleForm.Import.ViewFlag)
//            {
//                IEnumerator<KeyValuePair<string, string>> pairs = formData.GetEnumerator();

//                NameValueCollection collection = new NameValueCollection();

//                while (pairs.MoveNext())
//                {
//                    KeyValuePair<string, string> pair = pairs.Current;
//                    collection.Add(pair.Key, pair.Value);
//                }

//                ImportFileCriteria payload = JsonConvert.DeserializeObject<ImportFileCriteria>(collection["payload"]);

//                byte[] fileContents = Convert.FromBase64String(payload.File.FileData);
//                return Common.ServiceWrapper.XmlClient.ImportFile(payload.ImportExportId, fileContents, payload.File.FileName, Common.ApplicationParameter.ImportExportProcessingDirectory, Common.ApplicationParameter.AutoGenerateEmployeeNumber, Common.ApplicationParameter.AutoGenerateEmployeeNumberFormat, Convert.ToBoolean(Common.ApplicationParameter.ImportDirtySave), Common.ApplicationParameter.AutoAddSecondaryPositions);
//            }
//            else
//            {
//                throw new Exception("Not Authorized");
//            }


//        }
//    }
//}

