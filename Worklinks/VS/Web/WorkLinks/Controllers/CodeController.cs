﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Threading.Tasks;
//using System.Web.Http;
//using WLP.BusinessLayer.BusinessObjects;
//using WorkLinks.Models;

//namespace WorkLinks.Controllers
//{
//    [Authorize]
//    public class CodeController : BaseController
//    {

//        // GET: api/Code/codetable/code/en
//        [HttpGet]
//        [Route("api/Code/{codeTableName}/{languageCode}")]
//        public CodeSelectTable Get(String codeTableName, String languageCode)
//        {
//            //currently we are overriding language, future we go with browser language
//            CodeCollection codes = null;
//            if (codeTableName == "importExport")
//            {
//                codes = Common.CodeHelper.GetImportExportCodeCollection();
//            }
//            else
//            {
//                codes = Common.CodeHelper.GetCode(codeTableName, languageCode, null);
//            }
//            CodeSelect[] minCodes = new CodeSelect[codes.Count];
//            for (int i = 0; i < codes.Count; i++)
//            {
//                minCodes[i] = new CodeSelect()
//                {
//                    Code = codes[i].Code,
//                    Description = codes[i].Description,
//                    ParentCode = codes[i].ParentCode,
//                    ActiveFlag = codes[i].ActiveFlag
//                };
//            }

//            CodeSelectTable rtn = new CodeSelectTable() { CodeTableName = codeTableName, Codes = minCodes };

//            return rtn;
//        }

//        // POST: api/Code
//        public void Post([FromBody]string value)
//        {
//        }

//        // PUT: api/Code/5
//        public void Put(int id, [FromBody]string value)
//        {
//        }

//        // DELETE: api/Code/5
//        public void Delete(int id)
//        {
//        }
//    }
//}
