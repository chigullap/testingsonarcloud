﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Policy
{
    public partial class PolicySearchControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected String Criteria
        {
            get
            {
                String searchEntry = PolicyDescription.TextValue;
                return searchEntry;
            }
            set
            {
                PolicyDescription.TextValue = value;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.PolicySearch.ViewFlag);

            WireEvents();

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.Equals(String.Format("refresh")))
                RefreshGrid();
        }
        protected void WireEvents()
        {
            PolicySummaryGrid.NeedDataSource += new GridNeedDataSourceEventHandler(PolicySummaryGrid_NeedDataSource);

            //work around to remove empty rows caused by the OnCommand Telerik bug
            PolicySummaryGrid.ItemDataBound += new GridItemEventHandler(PolicySummaryGrid_ItemDataBound);
            PolicySummaryGrid.PreRender += new EventHandler(PolicySummaryGrid_PreRender);
            PolicySummaryGrid.AllowPaging = true;
        }
        protected void RefreshGrid()
        {
            long keyId = -1;

            //if a row was selected
            if (PolicySummaryGrid.SelectedItems.Count != 0)
                keyId = Convert.ToInt64(((GridDataItem)PolicySummaryGrid.SelectedItems[0]).GetDataKeyValue(PolicySummaryGrid.MasterTableView.DataKeyNames[0]));

            Search(Criteria);

            if (keyId != -1)
                PolicySummaryGrid.SelectRowByFirstDataKey(keyId); //re-select the selected row
        }
        public void Search(String criteria)
        {
            Data = AccrualPolicySearchCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetAccrualPolicyReport(criteria));
            PolicySummaryGrid.Rebind();
        }
        protected void Delete(AccrualPolicySearch policySearch)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteAccrualPolicyReport(Convert.ToInt64(policySearch.Key));
                Data.Remove(policySearch.Key);

                PolicySummaryGrid.SelectedIndexes.Clear();
                PolicySummaryGrid.Rebind();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    PolicySummaryGrid.SelectedIndexes.Clear();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region event handlers
        void PolicySummaryGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            PolicySummaryGrid.DataSource = Data;
        }
        protected void PolicySummaryToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("delete") && PolicySummaryGrid.SelectedValue != null)
                Delete((AccrualPolicySearch)Data[PolicySummaryGrid.SelectedValue.ToString()]);
        }
        protected void PolicySummaryGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                //work around to remove empty rows caused by the OnCommand Telerik bug 
                if (String.Equals((dataItem["AccrualPolicyDescriptionField"].Text).ToUpper(), "&NBSP;"))
                {
                    //hide the row
                    e.Item.Display = false;
                }
            }
        }
        void PolicySummaryGrid_PreRender(object sender, EventArgs e)
        {
            //work around to remove empty rows caused by the OnCommand Telerik bug
            if (Data == null)
                PolicySummaryGrid.AllowPaging = false;
            else
                PolicySummaryGrid.AllowPaging = true;
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search(Criteria);
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            PolicyDescription.TextValue = "";
            Data = null;
            PolicySummaryGrid.DataBind();
        }
        #endregion

        #region security handlers
        protected void AddToolBar_PreRender(object sender, EventArgs e)
        {
            ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.PolicySearch.AddFlag;
        }
        protected void DetailsToolBar_PreRender(object sender, EventArgs e)
        {
            ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.PolicySearch.ViewFlag;
        }
        protected void DeleteToolBar_PreRender(object sender, EventArgs e)
        {
            ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.PolicySearch.DeleteFlag;
        }
        #endregion
    }
}