﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Policy
{
    public partial class PolicyEntitlementControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private long _accrualPolicyId = -1;
        private const String _accrualPolicyIdKey = "_accrualPolicyIdKey";
        private AccrualPolicyEntitlementCollection _accrualPolicyEntitlementCollection = null;
        private CodeCollection _checkedEntitlements = null;
        #endregion

        #region properties
        public long AccrualPolicyId
        {
            get
            {
                if (_accrualPolicyId == -1)
                    _accrualPolicyId = Convert.ToInt64(ViewState[_accrualPolicyIdKey]);

                return _accrualPolicyId;
            }
            set
            {
                _accrualPolicyId = value;
                ViewState[_accrualPolicyIdKey] = _accrualPolicyId;
            }
        }
        public AccrualPolicyEntitlementCollection PolicyEntitlementCollection
        {
            get
            {
                if (_accrualPolicyEntitlementCollection == null)
                    _accrualPolicyEntitlementCollection = new AccrualPolicyEntitlementCollection();

                return _accrualPolicyEntitlementCollection;
            }
        }
        public CodeCollection CheckedEntitlements
        {
            get
            {
                if (_checkedEntitlements == null)
                    _checkedEntitlements = new CodeCollection();

                return _checkedEntitlements;
            }
        }
        public bool IsViewMode { get; set; }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        public void LoadPolicyEntitlement()
        {
            AccrualPolicyEntitlementCollection collection = new AccrualPolicyEntitlementCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetAccrualPolicyEntitlement(AccrualPolicyId));
            Data = collection;

            PolicyEntitlement.DataBind();
        }
        public void InsertPolicyEntitlement()
        {
            CodeCollection policyEntitlement = (CodeCollection)PolicyEntitlement.Value;

            for (int i = 0; i < policyEntitlement.Count; i++)
            {
                PolicyEntitlementCollection.Add(new AccrualPolicyEntitlement
                {
                    AccrualPolicyEntitlementId = -i - 1,
                    AccrualEntitlementId = Convert.ToInt64(policyEntitlement[i].Code)
                });
            }
        }
        #endregion

        #region event handlers
        protected void PolicyEntitlement_NeedDataSource(object sender, EventArgs e)
        {
            AccrualEntitlementSearchCollection entitlementSearchCollection = Common.ServiceWrapper.HumanResourcesClient.GetAccrualEntitlementReport(null);
            CodeCollection entitlements = new CodeCollection();

            foreach (AccrualEntitlementSearch entitlement in entitlementSearchCollection)
            {
                entitlements.Add(new CodeObject()
                {
                    Code = entitlement.AccrualEntitlementId.ToString(),
                    Description = entitlement.AccrualEntitlementDescriptionField,
                    LanguageCode = LanguageCode
                });
            }

            foreach (CodeObject code in entitlements)
            {
                foreach (AccrualPolicyEntitlement policyEntitlement in (AccrualPolicyEntitlementCollection)Data)
                {
                    if (policyEntitlement.AccrualEntitlementId.ToString() == code.Code)
                        CheckedEntitlements.Add(new CodeObject() { Code = code.Code, Description = code.Description });
                }
            }

            if (IsViewMode)
                entitlements = CheckedEntitlements;

            ((ICodeControl)sender).DataSource = entitlements;
            ((CheckedListBoxControl)sender).Value = CheckedEntitlements;
        }
        #endregion
    }
}