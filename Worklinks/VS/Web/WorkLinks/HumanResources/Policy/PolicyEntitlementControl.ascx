﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PolicyEntitlementControl.ascx.cs" Inherits="WorkLinks.HumanResources.Policy.PolicyEntitlementControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<style>
    .RadListBox .rlbGroup {
        border-width: 0 !important;
    }
</style>

<table width="100%">
    <tr>
        <td style="padding: 10px 34px 20px 34px;">
            <wasp:WLPLabel ID="EntitlementsLabel" runat="server" Text="**Entitlements" Font-Bold="true" ResourceName="Entitlements" />

            <wasp:CheckedListBoxControl
                ID="PolicyEntitlement"
                runat="server"
                LabelText=""
                ListBoxHeight="215px"
                ListBoxWidth="640px"
                ResourceName="PolicyEntitlement"
                Type="PolicyEntitlement"
                OnDataBinding="PolicyEntitlement_NeedDataSource"
                Value='<%# CheckedEntitlements %>'
                ReadOnly='<%# IsViewMode %>' />
        </td>
    </tr>
</table>