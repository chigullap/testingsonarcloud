﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DisciplinaryResolutionControl.ascx.cs" Inherits="WorkLinks.HumanResources.DisciplinaryAction.DisciplinaryResolutionControl" ResourceName="DisciplinaryResolutionControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPFormView
    ID="DisciplinaryResolutionView"
    RenderOuterTable="false"
    runat="server"
    OnInserting="DisciplinaryResolutionView_Inserting"
    OnNeedDataSource="DisciplinaryResolutionView_NeedDataSource"
    OnUpdating="DisciplinaryResolutionView_Updating">

    <ItemTemplate>
        <wasp:WLPToolBar ID="DiscipinaryResolutionToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Visible='<%# UpdateFlag %>' CommandName="Edit" ImageUrl="~/App_Themes/Default/Edit.gif" Text="Edit" ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="DisciplineTypeCode" Text="Discipline type:" runat="server" Type="DisciplineTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="DisciplineTypeCode" Value='<%# Bind("DisciplineTypeCode") %>' ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:DateControl ID="DisciplineDate" Text="Discipline date:" runat="server" ResourceName="DisciplineDate" Value='<%# Bind("DisciplineDate") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="ResolutionDate" Text="Resolution Date:" runat="server" ResourceName="ResolutionDate" Value='<%# Bind("ResolutionDate") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:ComboBoxControl ID="DisciplineResolutionCode" Width="700px" FieldWidth="150px" runat="server" Type="DisciplineResolutionCode" OnDataBinding="Code_NeedDataSource" ResourceName="DisciplineResolutionCode" Value='<%# Bind("DisciplineResolutionCode") %>' ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:TextBoxControl ID="ResolutionNote" Width="700px" FieldWidth="550px" runat="server" ResourceName="ResolutionNote" Value='<%# Bind("ResolutionNote") %>' Rows="2" TextMode="multiline" ReadOnly="true" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>
    <EditItemTemplate>
        <wasp:WLPToolBar ID="DiscipinaryResolutionToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode%>' ResourceName="Update"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="DisciplineTypeCode" Text="Discipline type:" runat="server" Type="DisciplineTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="DisciplineTypeCode" Value='<%# Bind("DisciplineTypeCode") %>' ReadOnly="true" TabIndex="010"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:DateControl ID="DisciplineDate" Text="Discipline date:" runat="server" ResourceName="DisciplineDate" Value='<%# Bind("DisciplineDate") %>' ReadOnly="true" TabIndex="020" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="ResolutionDate" Text="Resolution Date:" runat="server" ResourceName="ResolutionDate" Value='<%# Bind("ResolutionDate") %>' ReadOnly="false" TabIndex="030" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:ComboBoxControl ID="DisciplineResolutionCode" Width="700px" FieldWidth="150px" runat="server" Type="DisciplineResolutionCode" OnDataBinding="Code_NeedDataSource" ResourceName="DisciplineResolutionCode" Value='<%# Bind("DisciplineResolutionCode") %>' TabIndex="040"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:TextBoxControl ID="ResolutionNote" Width="700px" FieldWidth="550px" runat="server" ResourceName="ResolutionNote" Value='<%# Bind("ResolutionNote") %>' Rows="2" TextMode="multiline" ReadOnly="false" TabIndex="050" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>

</wasp:WLPFormView>