﻿using System;
using System.Web.UI.WebControls;
using WLP.Web.UI.Controls;

namespace WorkLinks.HumanResources.DisciplinaryAction
{
    public partial class DisciplinaryResolutionControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public BusinessLayer.BusinessObjects.EmployeeDisciplineCollection DisciplineCollection { get; set; }
        public bool IsViewMode { get { return this.DisciplinaryResolutionView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool IsInsertMode { get { return this.DisciplinaryResolutionView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsEditMode { get { return this.DisciplinaryResolutionView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeDisciplinary.UpdateFlag; } }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void DisciplinaryResolutionView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            OnNeedDataSource(e);
            DisciplinaryResolutionView.DataSource = DisciplineCollection;
        }
        protected void DisciplinaryResolutionView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            OnInserting(e);
        }
        protected void DisciplinaryResolutionView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            OnUpdating(e);
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
    }
}