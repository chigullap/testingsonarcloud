﻿using System;
using System.Web.UI.WebControls;
using WLP.Web.UI.Controls;

namespace WorkLinks.HumanResources.DisciplinaryAction
{
    public partial class IncidentDetailControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public BusinessLayer.BusinessObjects.EmployeeDisciplineCollection DisciplineCollection { get; set; }
        public bool IsViewMode { get { return this.IncidentDetailView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool IsInsertMode { get { return this.IncidentDetailView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsEditMode { get { return this.IncidentDetailView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeDisciplinary.UpdateFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.Length > 11 && args.Substring(0, 11).ToLower().Equals("employeeid="))
            {
                //ChangeVisibility("EMP");
            }
        }
        public void ChangeModeInsert()
        {
            IncidentDetailView.ChangeMode(FormViewMode.Insert);
            IncidentDetailView.DataBind();
        }
        #endregion

        #region event handlers
        protected void ReportedByEmployeeId_DataBinding(object sender, EventArgs e)
        {
            PopulateEmployeeControl((ICodeControl)sender);
        }
        protected void PopulateEmployeeControl(ICodeControl control)
        {
            Common.CodeHelper.PopulateEmployeeControl(control, true);
        }
        protected void IncidentDetailView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            OnNeedDataSource(e);
            IncidentDetailView.DataSource = DisciplineCollection;
        }
        protected void IncidentDetailView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            OnInserting(e);
        }
        protected void IncidentDetailView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            OnUpdating(e);
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void DisciplineCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
            BindTypeCombo(sender);
        }
        private void BindTypeCombo(object sender)
        {
            if (sender is ComboBoxControl)
            {
                ComboBoxControl disciplineCombo = (ComboBoxControl)sender;
                WLPFormView formItem = (WLPFormView)disciplineCombo.BindingContainer;
                ComboBoxControl disciplineTypeCombo = (ComboBoxControl)formItem.FindControl("DisciplineTypeCode");

                if (disciplineTypeCombo != null)
                {
                    disciplineTypeCombo.DataSource = Common.ServiceWrapper.CodeClient.GetDisciplineTypeCode(disciplineCombo.SelectedValue);
                    disciplineTypeCombo.DataBind();
                }
            }
        }
        protected void DisciplineCode_SelectedIndexChanged(object o, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindTypeCombo(o);
        }
        #endregion
    }
}