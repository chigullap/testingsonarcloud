﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DisciplinaryActionModuleControl.ascx.cs" Inherits="WorkLinks.HumanResources.DisciplinaryAction.DisciplinaryActionModuleControl" ResourceName="DisciplinaryActionModuleControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="IncidentDetailControl.ascx" TagName="IncidentDetailControl" TagPrefix="uc1" %>
<%@ Register Src="DisciplinaryActionTakenControl.ascx" TagName="DisciplinaryActionTakenControl" TagPrefix="uc2" %>
<%@ Register Src="DisciplinaryResolutionControl.ascx" TagName="DisciplinaryResolutionControl" TagPrefix="uc3" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="DisciplinaryPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="DisciplinaryPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="DisciplinaryPanel">
    <wasp:WLPToolBar ID="DisciplinaryActionsToolbar" runat="server" Width="100%" OnButtonClick="DisciplinaryActionsToolbar_ButtonClick" OnClientButtonClicking="onClientButtonClicking">
        <Items>
            <wasp:WLPToolBarButton Visible='<%# AddFlag %>' Text="**Add**" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="Add" ResourceName="Add" />
            <wasp:WLPToolBarButton Visible='<%# DeleteFlag %>' Text="**Delete**" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ResourceName="Delete" />
        </Items>
    </wasp:WLPToolBar>

    <wasp:WLPGrid
        ID="DisciplinaryActionsGrid"
        runat="server"
        OnSelectedIndexChanged="DisciplinaryActionsGrid_SelectedIndexChanged"
        OnNeedDataSource="DisciplinaryActionsGrid_NeedDataSource"
        GridLines="None"
        OnDeleteCommand="DisciplinaryActionsGrid_DeleteCommand"
        OnInsertCommand="DisciplinaryActionsGrid_InsertCommand"
        OnUpdateCommand="DisciplinaryActionsGrid_UpdateCommand"
        Height="160px"
        AutoAssignModifyProperties="true"
        AutoGenerateColumns="false"
        OnDataBound="DisciplinaryActionsGrid_DataBound">

        <ClientSettings EnablePostBackOnRowClick="true" AllowKeyboardNavigation="true">
            <Selecting AllowRowSelect="true" />
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
            <RowIndicatorColumn>
                <HeaderStyle Width="20px" />
            </RowIndicatorColumn>

            <ExpandCollapseColumn>
                <HeaderStyle Width="20px" />
            </ExpandCollapseColumn>

            <Columns>
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="**DisciplineCode**" DataField="DisciplineCode" Type="DisciplineCode" ResourceName="DisciplineCode" HeaderStyle-Width="140px" />
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="**DisciplineTypeCode**" DataField="DisciplineTypeCode" Type="DisciplineTypeCode" ResourceName="DisciplineTypeCode" HeaderStyle-Width="140px" />
                <wasp:GridDateTimeControl DataField="DisciplineDate" LabelText="**DisciplineDate**" SortExpression="DisciplineDate" UniqueName="DisciplineDate" ResourceName="DisciplineDate" HeaderStyle-Width="140px" />
                <wasp:GridDateTimeControl DataField="ReportedDate" LabelText="**ReportedDate**" SortExpression="ReportedDate" UniqueName="ReportedDate" ResourceName="ReportedDate" HeaderStyle-Width="140px" />
                <wasp:GridDateTimeControl DataField="ExpiryDate" LabelText="**ExpiryDate**" SortExpression="ExpiryDate" UniqueName="ExpiryDate" ResourceName="ExpiryDate" HeaderStyle-Width="140px" />
                <wasp:GridBoundControl DataField="Note" LabelText="**Note**" SortExpression="Note" UniqueName="Note" ResourceName="Note" HeaderStyle-Width="200px" />
            </Columns>

            <EditFormSettings>
                <EditColumn UniqueName="EditCommandColumn1" />
            </EditFormSettings>

            <CommandItemTemplate></CommandItemTemplate>
        </MasterTableView>
    </wasp:WLPGrid>

    <wasp:WLPTabStrip ID="IncidentDetailTabStrip" runat="server" SelectedIndex="0" MultiPageID="IncidentDetailMultiPage">
        <Tabs>
            <wasp:WLPTab Text="**IncidentDetailTab**" Selected="True" ResourceName="IncidentDetailTab" />
            <wasp:WLPTab Text="**ActionStepTab**" ResourceName="ActionStepTab" />
            <wasp:WLPTab Text="**ResolutionTab**" ResourceName="ResolutionTab" />
        </Tabs>
    </wasp:WLPTabStrip>

    <telerik:RadMultiPage ID="IncidentDetailMultiPage" runat="server" SelectedIndex="0">
        <telerik:RadPageView ID="IncidentDetailPageView" runat="server" Selected="true">
            <uc1:IncidentDetailControl ID="IncidentDetailControl" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="DisciplinaryActionTakenPageView" runat="server">
            <uc2:DisciplinaryActionTakenControl ID="DisciplinaryActionTakenControl" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="DisciplinaryResolutionPageView" runat="server">
            <uc3:DisciplinaryResolutionControl ID="DisciplinaryResolutionControl" runat="server" />
        </telerik:RadPageView>
    </telerik:RadMultiPage>
</asp:Panel>

<script type="text/javascript">
    function onClientButtonClicking(sender, args) {
        var comandName = args.get_item().get_commandName();
        if (comandName == "Delete") {
            var message = "<asp:Literal runat="server" Text="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />";
            args.set_cancel(!confirm(message));
        }
    }  
</script>