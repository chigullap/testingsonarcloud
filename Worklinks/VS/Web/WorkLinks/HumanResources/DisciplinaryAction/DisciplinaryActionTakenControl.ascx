﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DisciplinaryActionTakenControl.ascx.cs" Inherits="WorkLinks.HumanResources.DisciplinaryAction.DisciplinaryActionTakenControl" ResourceName="DisciplinaryActionTakenControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPGrid
    ID="DisciplinaryActionsTakenGrid"
    runat="server"
    AutoGenerateColumns="false"
    GridLines="None"
    OnDataBound="DisciplinaryActionsTakenGrid_DataBound"
    AutoAssignModifyProperties="true"
    OnDeleteCommand="DisciplinaryActionsTakenGrid_DeleteCommand"
    OnInsertCommand="DisciplinaryActionsTakenGrid_InsertCommand"
    OnUpdateCommand="DisciplinaryActionsTakenGrid_UpdateCommand"
    OnNeedDataSource="DisciplinaryActionsTakenGrid_NeedDataSource">

    <ClientSettings AllowKeyboardNavigation="true">
        <Selecting AllowRowSelect="false" />
        <Scrolling AllowScroll="true" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
        <CommandItemTemplate>
            <wasp:WLPToolBar ID="EmployeeSummaryToolBar" runat="server" AutoPostBack="true" Width="100%">
                <Items>
                    <wasp:WLPToolBarButton CommandName="InitInsert" ResourceName="Add" ImageUrl="~/App_Themes/Default/Add.gif" Text="Add" Visible="<%# IsViewMode && AddFlag %>" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <RowIndicatorColumn>
            <HeaderStyle Width="20px" />
        </RowIndicatorColumn>

        <ExpandCollapseColumn>
            <HeaderStyle Width="20px" />
        </ExpandCollapseColumn>

        <Columns>
            <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" HeaderStyle-Width="8px" />
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="**DisciplineActionStepCode**" DataField="DisciplineActionStepCode" Type="DisciplineActionStepCode" ResourceName="DisciplineActionStepCode" HeaderStyle-Width="140px" />
            <wasp:GridDateTimeControl DataField="ActionDate" LabelText="**ActionDate**" SortExpression="ActionDate" UniqueName="ActionDate" ResourceName="ActionDate" HeaderStyle-Width="140px" />
            <wasp:GridBoundControl DataField="DiscussedWith" LabelText="**DiscussedWith**" SortExpression="DiscussedWith" UniqueName="DiscussedWith" ResourceName="DiscussedWith" HeaderStyle-Width="140px" />
            <wasp:GridBoundControl DataField="Note" LabelText="**Note**" SortExpression="Note" UniqueName="Note" ResourceName="Note" HeaderStyle-Width="200px" />
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" CommandName="Delete" HeaderStyle-Width="8px" />
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="DisciplineActionStepCode" runat="server" Type="DisciplineActionStepCode" OnDataBinding="Code_NeedDataSource" ResourceName="DisciplineActionStepCode" Value='<%# Bind("DisciplineActionStepCode") %>' TabIndex="010" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="ActionDate" runat="server" ResourceName="ActionDate" Value='<%# Bind("ActionDate") %>' ReadOnly="false" TabIndex="020" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl Width="700px" ID="DiscussedWith" runat="server" ResourceName="DiscussedWith" Value='<%# Bind("DiscussedWith") %>' LabelStyle="width:75px;" FieldStyle="width: 525px;" ReadOnly="false" TabIndex="030" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl Width="700px" ID="Note" runat="server" ResourceName="Note" Value='<%# Bind("Note") %>' LabelStyle="width:75px;" FieldStyle="width: 525px;" Rows="2" TextMode="multiline" ReadOnly="false" TabIndex="040" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# !IsUpdate %>' ResourceName="Insert" />
                                    </td>
                                    <td>
                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>
    </MasterTableView>
</wasp:WLPGrid>