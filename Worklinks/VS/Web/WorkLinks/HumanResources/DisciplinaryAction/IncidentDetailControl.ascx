﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IncidentDetailControl.ascx.cs" Inherits="WorkLinks.HumanResources.DisciplinaryAction.IncidentDetailControl" ResourceName="IncidentDetailControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../../Payroll/PayrollBatch/EmployeeControl.ascx" TagName="EmployeeControl" TagPrefix="uc1" %>

<wasp:WLPFormView
    ID="IncidentDetailView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key"
    OnInserting="IncidentDetailView_Inserting"
    OnNeedDataSource="IncidentDetailView_NeedDataSource"
    OnUpdating="IncidentDetailView_Updating">

    <ItemTemplate>
        <wasp:WLPToolBar ID="IncidentDetailToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Visible='<%# UpdateFlag %>' CommandName="Edit" ImageUrl="~/App_Themes/Default/Edit.gif" Text="**Edit**" ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="DisciplineCode" runat="server" Mandatory="true" Type="DisciplineCode" OnDataBinding="Code_NeedDataSource" ResourceName="DisciplineCode" Value='<%# Bind("DisciplineCode") %>' ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="DisciplineTypeCode" runat="server" Mandatory="true" Type="DisciplineTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="DisciplineTypeCode" Value='<%# Bind("DisciplineTypeCode") %>' ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="DisciplineDate" runat="server" Mandatory="true" ResourceName="DisciplineDate" Value='<%# Eval("DisciplineDate") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:KeyValueControl ID="ReportedByEmployeeId" ClientIDMode="Static" runat="server" Type="ReportedByEmployeeId" OnDataBinding="ReportedByEmployeeId_DataBinding" ResourceName="ReportedByEmployeeId" Value='<%# Bind("ReportedByEmployeeId") %>' ReadOnly="true"></wasp:KeyValueControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="ReportedDate" runat="server" ResourceName="ReportedDate" Value='<%# Eval("ReportedDate") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:DateControl ID="ExpiryDate" runat="server" ResourceName="ExpiryDate" Value='<%# Eval("ExpiryDate") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:TextBoxControl ID="Note" ReadOnly="True" Width="700px" FieldWidth="550px" runat="server" ResourceName="Note" Value='<%# Eval("Note") %>' Rows="2" TextMode="multiline" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>
    <EditItemTemplate>
        <wasp:WLPToolBar ID="IncidentDetailToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert" />
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update" />
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="DisciplineCode" runat="server" Mandatory="true" Type="DisciplineCode" AutoPostback="true" OnSelectedIndexChanged="DisciplineCode_SelectedIndexChanged" OnDataBinding="DisciplineCode_NeedDataSource" ResourceName="DisciplineCode" Value='<%# Bind("DisciplineCode") %>' ReadOnly="false" TabIndex="010" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="DisciplineTypeCode" runat="server" Mandatory="true" Type="DisciplineTypeCode" ResourceName="DisciplineTypeCode" Value='<%# Bind("DisciplineTypeCode") %>' ReadOnly="false" TabIndex="020" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="DisciplineDate" runat="server" Mandatory="true" ResourceName="DisciplineDate" Value='<%# Bind("DisciplineDate") %>' ReadOnly="false" TabIndex="030" />
                    </td>
                    <td>
                        <div class="form_control">
                            <span class="label">
                                <wasp:WLPLabel ID="ReportedByEmployeeIdLabel" runat="server" Text="**ReportedByEmployeeId**" ResourceName="ReportedByEmployeeId" />
                            </span>
                            <span class="field">
                                <uc1:EmployeeControl ID="EmployeeControl1" runat="server" Value='<%# Bind("ReportedByEmployeeId") %>' TabIndex="040" />
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="ReportedDate" runat="server" ResourceName="ReportedDate" Value='<%# Bind("ReportedDate") %>' ReadOnly="false" TabIndex="050" />
                    </td>
                    <td>
                        <wasp:DateControl ID="ExpiryDate" runat="server" ResourceName="ExpiryDate" Value='<%# Bind("ExpiryDate") %>' ReadOnly="false" TabIndex="060" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:TextBoxControl ID="Note" Width="700px" FieldWidth="550px" runat="server" ResourceName="Note" Value='<%# Bind("Note") %>' Rows="2" TextMode="multiline" ReadOnly="false" TabIndex="070" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>
</wasp:WLPFormView>

<script type="text/javascript">
    function onClientClose(sender, eventArgs) {
        var arg = eventArgs.get_argument();
        if (arg != null && arg.isUpdate)
            __doPostBack('<%= IncidentDetailView.ClientID %>', String.format('EmployeeId={0}', arg.employeeId));
    }
</script>