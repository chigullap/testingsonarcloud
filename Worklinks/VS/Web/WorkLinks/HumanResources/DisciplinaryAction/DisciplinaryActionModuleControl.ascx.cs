﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.DisciplinaryAction
{
    public partial class DisciplinaryActionModuleControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private int _rowIndex = -1;
        private string _rowIndexKey = "RowIndexKey";
        private bool _updatePerformed = false;
        private bool _insertPerformed = false;
        #endregion

        #region properties
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        private String LastName { get { return Page.RouteData.Values["lastName"].ToString(); } }
        private String FirstName { get { return Page.RouteData.Values["firstName"].ToString(); } }
        protected bool IsViewMode { get { return IncidentDetailControl.IsViewMode && DisciplinaryActionTakenControl.IsViewMode && DisciplinaryResolutionControl.IsViewMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.EmployeeDisciplinary.AddFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.EmployeeDisciplinary.DeleteFlag; } }
        public int RowIndex
        {
            get
            {
                if (_rowIndex == -1)
                {
                    Object obj = ViewState[_rowIndexKey];

                    if (obj != null)
                        _rowIndex = (int)obj;
                }

                return _rowIndex;
            }
            set
            {
                _rowIndex = value;
                ViewState[_rowIndexKey] = _rowIndex;
            }
        }
        /// <summary>
        /// get current contact data
        /// </summary>
        protected EmployeeDiscipline CurrentDiscipline
        {
            get
            {
                EmployeeDiscipline discipline = null;

                if (IncidentDetailControl.IsInsertMode && !_insertPerformed)
                    discipline = new EmployeeDiscipline() { EmployeeId = EmployeeId };
                else
                {
                    if (DisciplinaryActionsGrid.SelectedValue != null)
                        discipline = (EmployeeDiscipline)Data[DisciplinaryActionsGrid.SelectedValue.ToString()];
                }

                return discipline;
            }
        }
        protected EmployeeDisciplineCollection CurrentDisciplineCollection
        {
            get
            {
                EmployeeDisciplineCollection collection = new EmployeeDisciplineCollection();

                if (CurrentDiscipline != null)
                    collection.Load(CurrentDiscipline);

                return collection;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            //does the user have access rights?
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.EmployeeDisciplinary.ViewFlag);

            if (!IsPostBack)
            {
                //does the user have rights to seet his employee?
                SetAuthorized(Common.ServiceWrapper.HumanResourcesClient.GetEmployee(new EmployeeCriteria() { EmployeeId = EmployeeId }).Count > 0);
                LoadDisciplinaryAction(EmployeeId);
            }

            this.Page.Title = "Disciplinary - " + LastName + ", " + FirstName;
            WireEvents();
        }
        private void WireEvents()
        {
            IncidentDetailControl.NeedDataSource += new NeedDataSourceEventHandler(IncidentDetailControl_NeedDataSource);
            IncidentDetailControl.Updating += new UpdatingEventHandler(UserControl_Updating);
            IncidentDetailControl.Inserting += new InsertingEventHandler(UserControl_Inserting);

            DisciplinaryResolutionControl.NeedDataSource += new NeedDataSourceEventHandler(DisciplinaryResolutionControl_NeedDataSource);
            DisciplinaryResolutionControl.Updating += new UpdatingEventHandler(UserControl_Updating);
        }
        protected override void OnPreRender(EventArgs e)
        {
            InitializeControls();
            base.OnPreRender(e);
        }
        protected void InitializeControls()
        {
            IncidentDetailTabStrip.Enabled = IsViewMode;
            DisciplinaryActionsGrid.Enabled = IsViewMode;

            DisciplinaryActionsToolbar.Items[0].Enabled = IsViewMode; //add button
            DisciplinaryActionsToolbar.Items[1].Enabled = IsViewMode && RowIndex >= 0; //delete button
        }
        protected void LoadDisciplinaryAction(long employeeId)
        {
            Data = EmployeeDisciplineCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetEmployeeDiscipline(EmployeeId));

            ((WLPToolBarButton)DisciplinaryActionsToolbar.FindButtonByCommandName("Add")).Visible = AddFlag;
            ((WLPToolBarButton)DisciplinaryActionsToolbar.FindButtonByCommandName("Delete")).Visible = DeleteFlag;
        }
        #endregion

        #region event handlers
        void DisciplinaryResolutionControl_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            DisciplinaryResolutionControl.DisciplineCollection = CurrentDisciplineCollection;
        }
        void IncidentDetailControl_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            IncidentDetailControl.DisciplineCollection = CurrentDisciplineCollection;
        }
        protected bool UpdateDisciplinaryAction(EmployeeDiscipline discipline)
        {
            bool noError = true;

            try
            {
                Common.ServiceWrapper.HumanResourcesClient.UpdateEmployeeDiscipline(discipline);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    noError = false;
                }
            }
            catch (Exception ex)
            {
                noError = false;
                throw ex;
            }

            return noError;
        }
        protected EmployeeDiscipline InsertDisciplinaryAction(EmployeeDiscipline discipline)
        {
            return Common.ServiceWrapper.HumanResourcesClient.InsertEmployeeDiscipline(discipline);
        }
        protected void DeleteDisciplinaryAction(String disciplineKey)
        {
            try
            {
                EmployeeDiscipline discipline = (EmployeeDiscipline)Data[disciplineKey];
                Common.ServiceWrapper.HumanResourcesClient.DeleteEmployeeDiscipline(discipline);
                Data.Remove(discipline.Key);
                DisciplinaryActionsGrid.SelectedIndexes.Clear();
                DisciplinaryActionsGrid.Rebind();

                //rebind the tabs - this fixes issue where after a delete, 0 rows exist.  Tabs were still bound to old data
                IncidentDetailControl.DataBind();
                DisciplinaryResolutionControl.DataBind();
                DisciplinaryActionTakenControl.DataBind();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected bool UpdateDisciplinaryActionStep(EmployeeDisciplineAction disciplineAction)
        {
            bool noError = true;

            try
            {
                Common.ServiceWrapper.HumanResourcesClient.UpdateEmployeeDisciplineAction(disciplineAction);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    noError = false;
                }
            }
            catch (Exception ex)
            {
                noError = false;
                throw ex;
            }

            return noError;
        }
        protected void DisciplinaryActionsGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            DisciplinaryActionsGrid.DataSource = Data;
        }
        protected void DisciplinaryActionsGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Get index of item being edited, will only be 1 item
            GridIndexCollection rows = DisciplinaryActionsGrid.ItemIndexes;
            RowIndex = Convert.ToInt16(rows[0]);

            GetSelectedDisciplinaryAction();
        }
        private void GetSelectedDisciplinaryAction()
        {
            if (DisciplinaryActionsGrid.SelectedItems.Count > 0)
            {
                IncidentDetailControl.DataBind();
                DisciplinaryResolutionControl.DataBind();
                DisciplinaryActionTakenControl.SetIncidentId(CurrentDiscipline.EmployeeDisciplineId);
                _insertPerformed = false;
            }
        }
        protected void DisciplinaryActionsToolbar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            RadToolBarButton Button = ((RadToolBarButton)e.Item);

            if (Button.CommandName.ToLower().Equals("add"))
            {
                IncidentDetailTabStrip.SelectedIndex = 0;
                IncidentDetailMultiPage.SelectedIndex = 0;
                IncidentDetailControl.ChangeModeInsert();
            }
            else
                DeleteDisciplinaryAction(DisciplinaryActionsGrid.SelectedValue.ToString());
        }
        protected void DisciplinaryActionsGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            EmployeeDiscipline discipline = (EmployeeDiscipline)e.Item.DataItem;
            discipline.EmployeeId = EmployeeId;

            Common.ServiceWrapper.HumanResourcesClient.InsertEmployeeDiscipline(discipline).CopyTo((EmployeeDiscipline)Data[discipline.Key]);
        }
        protected void DisciplinaryActionsGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteEmployeeDiscipline((EmployeeDiscipline)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void DisciplinaryActionsGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                EmployeeDiscipline discipline = (EmployeeDiscipline)e.Item.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.UpdateEmployeeDiscipline(discipline).CopyTo((EmployeeDiscipline)Data[discipline.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void UserControl_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            EmployeeDiscipline discipline = (EmployeeDiscipline)e.DataItem;  //only one contact in the collection

            if (UpdateDisciplinaryAction(discipline))
            {
                discipline.CopyTo(CurrentDiscipline);
                _updatePerformed = true;
                DisciplinaryActionsGrid.Rebind();
            }
            else
                e.Cancel = true;
        }
        void UserControl_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            EmployeeDiscipline discipline = (EmployeeDiscipline)e.DataItem;  //only one contact in the collection
            EmployeeDiscipline updatedDiscipline = InsertDisciplinaryAction(discipline);

            _insertPerformed = true;

            ((WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeeDiscipline>)Data).Add(updatedDiscipline);
            DisciplinaryActionsGrid.Rebind();
        }
        void UserControl_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            //(sender as WLP.Web.UI.WorkLinksUserControl).Data = CurrentDisciplineCollection;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void DisciplinaryActionsGrid_DataBound(object sender, EventArgs e)
        {
            if (DisciplinaryActionsGrid.Items.Count > 0)
            {
                if (!_updatePerformed)
                {
                    DisciplinaryActionsGrid.Items[0].Selected = true;
                    RowIndex = 0;
                }
                else
                {
                    DisciplinaryActionsGrid.Items[RowIndex].Selected = true;
                    _updatePerformed = false;
                }

                GetSelectedDisciplinaryAction();
            }
            else
                RowIndex = -1;
        }
        #endregion
    }
}