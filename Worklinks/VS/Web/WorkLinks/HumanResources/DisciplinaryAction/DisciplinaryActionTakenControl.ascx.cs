﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.DisciplinaryAction
{
    public partial class DisciplinaryActionTakenControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private const String _incidentIdKey = "IncidentIdKey";
        private long _incidentId = -1;
        #endregion

        #region properties
        public long IncidentId
        {
            get
            {
                if (_incidentId == -1)
                {
                    Object obj = ViewState[_incidentIdKey];

                    if (obj != null)
                        _incidentId = Convert.ToInt64(obj);
                }

                return _incidentId;
            }
            set
            {
                _incidentId = value;
                ViewState[_incidentIdKey] = _incidentId;
            }
        }
        public bool IsViewMode { get { return DisciplinaryActionsTakenGrid.IsViewMode; } }
        public bool IsUpdate { get { return DisciplinaryActionsTakenGrid.IsEditMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.EmployeeDisciplinary.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeDisciplinary.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.EmployeeDisciplinary.DeleteFlag; } }
        public EmployeeDisciplineActionCollection DisciplineActionStep { get; set; }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();

            if (!IsPostBack)
                Initialize();
        }
        protected void Initialize()
        {
            //Find the DisciplinaryActionsTakenGrid
            WLPGrid grid = (WLPGrid)this.FindControl("DisciplinaryActionsTakenGrid");

            //hide the edit/delete images in the rows.
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        private void WireEvents()
        {
            DisciplinaryActionsTakenGrid.ItemDataBound += DisciplinaryActionsTakenGrid_ItemDataBound;
        }
        void DisciplinaryActionsTakenGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
                ((ComboBoxControl)e.Item.FindControl("DisciplineActionStepCode")).Focus();
        }
        protected void DisciplinaryActionsTakenGrid_DataBound(object sender, EventArgs e)
        {
        }
        protected void DisciplinaryActionsTakenGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            EmployeeDisciplineAction disciplineAction = (EmployeeDisciplineAction)e.Item.DataItem;
            disciplineAction.EmployeeDisciplineId = IncidentId;
            Common.ServiceWrapper.HumanResourcesClient.InsertEmployeeDisciplineAction(disciplineAction).CopyTo((EmployeeDisciplineAction)Data[disciplineAction.Key]);
        }
        protected void DisciplinaryActionsTakenGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteEmployeeDisciplineAction((EmployeeDisciplineAction)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void DisciplinaryActionsTakenGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                EmployeeDisciplineAction disciplineAction = (EmployeeDisciplineAction)e.Item.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.UpdateEmployeeDisciplineAction(disciplineAction).CopyTo((EmployeeDisciplineAction)Data[disciplineAction.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void DisciplinaryActionTakenView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            OnInserting(e);
        }
        protected void DisciplinaryActionTakenView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            OnUpdating(e);
        }
        protected void LoadActionStep(long incidentId)
        {
            Data = EmployeeDisciplineActionCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetEmployeeDisciplineAction(incidentId));
            DisciplinaryActionsTakenGrid.Rebind();
        }
        public void SetIncidentId(long incidentId)
        {
            IncidentId = incidentId;
            LoadActionStep(IncidentId);
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void DisciplinaryActionsTakenGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            DisciplinaryActionsTakenGrid.DataSource = Data;
        }
    }
}