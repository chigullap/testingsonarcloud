﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HumanResources/Employee.Master" AutoEventWireup="true" CodeBehind="EmployeePositionPage.aspx.cs" Inherits="WorkLinks.HumanResources.EmployeePosition.EmployeePositionPage" ResourceName="Title" %>
<%@ Register Src="EmployeePositionControl.ascx" TagName="EmployeePositionControl" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:EmployeePositionControl ID="EmployeePositionControl1" runat="server" />
</asp:Content>