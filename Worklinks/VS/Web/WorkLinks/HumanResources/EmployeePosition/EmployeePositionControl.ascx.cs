﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.EmployeePosition
{
    public partial class EmployeePositionControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private BusinessLayer.BusinessObjects.EmployeePosition _insertCache;
        private const String _insertCacheKey = "_insertCacheKey";
        private const String _payrollPeriodCutoffDateCacheKey = "_payrollPeriodStartDateCacheKey";
        private DateTime? _payrollPeriodCutoffDate = null;
        #endregion

        #region properties
        private DateTime? _PayrollPeriodCutoffDate
        {
            get
            {
                if (_payrollPeriodCutoffDate == null)
                {
                    Object obj = ViewState[_payrollPeriodCutoffDateCacheKey];

                    if (obj != null)
                        _payrollPeriodCutoffDate = (DateTime)obj;
                }

                return _payrollPeriodCutoffDate;
            }
            set
            {
                _payrollPeriodCutoffDate = value;
                ViewState[_payrollPeriodCutoffDateCacheKey] = _payrollPeriodCutoffDate;
            }
        }
        private DateTime PayrollPeriodCutoffDate
        {
            get
            {
                if (_PayrollPeriodCutoffDate == null)
                {
                    PayrollPeriodCollection periods = Common.ServiceWrapper.PayrollPeriod.Select(new PayrollPeriodCriteria() { PayrollProcessGroupCode = PayrollProcessGroupCode, GetMinOpenPeriodFlag = true });

                    if (periods.Count > 0)
                        _PayrollPeriodCutoffDate = periods[0].CutoffDate;
                    else
                        _PayrollPeriodCutoffDate = DateTime.Now;
                }

                return (DateTime)_PayrollPeriodCutoffDate;
            }
        }
        protected bool EditButtonEnabled
        {
            get
            {
                if (SelectedEmployeePosition != null && SelectedEmployeePosition.PayrollProcessId == null)
                {
                    //any current or future dated records where the Event is Leave, Long Term Disability, or Terminate and the status is also Leave, Long Term Disability, or Terminated must have the edit button disabled
                    if ((SelectedEmployeePosition.EmployeePositionActionCode == "LEAV" && SelectedEmployeePosition.EmployeePositionStatusCode == "L") ||
                        (SelectedEmployeePosition.EmployeePositionActionCode == "TERM" && SelectedEmployeePosition.EmployeePositionStatusCode == "TER") ||
                        (SelectedEmployeePosition.EmployeePositionActionCode == "LD0" && SelectedEmployeePosition.EmployeePositionStatusCode == "LD0"))
                        return false;
                    else
                        return true;
                }
                else
                    return false;
            }
        }
        protected bool DeleteButtonEnabled
        {
            get
            {
                if (SelectedEmployeePosition != null && SelectedEmployeePosition.PayrollProcessId == null && SelectedEmployeePosition.NextEmployeePositionId == null)
                {
                    //any current or future dated records where the Event is Leave, Long Term Disability, or Terminate and the status is also Leave, Long Term Disability, or Terminated must have the delete button disabled
                    if ((SelectedEmployeePosition.EmployeePositionActionCode == "LEAV" && SelectedEmployeePosition.EmployeePositionStatusCode == "L") ||
                        (SelectedEmployeePosition.EmployeePositionActionCode == "TERM" && SelectedEmployeePosition.EmployeePositionStatusCode == "TER") ||
                        (SelectedEmployeePosition.EmployeePositionActionCode == "LD0" && SelectedEmployeePosition.EmployeePositionStatusCode == "LD0"))
                        return false;
                    else if (Data.Count == 1)
                        return false;
                    else
                        return true;
                }
                else
                    return false;
            }
        }
        private String PayrollProcessGroupCode { get { return Page.RouteData.Values["payrollProcessGroupCode"].ToString(); } }
        private String LastName { get { return Page.RouteData.Values["lastName"].ToString(); } }
        private String FirstName { get { return Page.RouteData.Values["firstName"].ToString(); } }
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        public BusinessLayer.BusinessObjects.EmployeePosition InsertCache
        {
            get
            {
                if (_insertCache == null)
                    _insertCache = (BusinessLayer.BusinessObjects.EmployeePosition)ViewState[_insertCacheKey];

                return _insertCache;
            }
            set
            {
                _insertCache = value;
                ViewState[_insertCacheKey] = _insertCache;
            }
        }
        private BusinessLayer.BusinessObjects.EmployeePosition SelectedEmployeePosition
        {
            get
            {
                BusinessLayer.BusinessObjects.EmployeePosition position = null;

                if (IsInsertMode)
                    position = InsertCache;
                else
                {
                    if (EmployeePositionGrid.SelectedValue != null)
                        position = (BusinessLayer.BusinessObjects.EmployeePosition)Data[EmployeePositionGrid.SelectedValue.ToString()];
                }

                return position;
            }
        }
        public EmployeePositionCollection CurrentEmployeePositionCollection
        {
            get
            {
                EmployeePositionCollection collection = new EmployeePositionCollection();
                BusinessLayer.BusinessObjects.EmployeePosition selectedEmployeePosition = SelectedEmployeePosition;

                if (selectedEmployeePosition != null)
                    collection.Add(selectedEmployeePosition);

                return collection;
            }
        }
        private BusinessLayer.BusinessObjects.EmployeePosition LastEmployeePosition
        {
            get
            {
                BusinessLayer.BusinessObjects.EmployeePosition lastPosition = null;

                if (Data != null)
                {
                    foreach (BusinessLayer.BusinessObjects.EmployeePosition position in Data)
                    {
                        if (position.NextEmployeePositionId == null)
                        {
                            lastPosition = position;
                            break;
                        }
                    }
                }

                return lastPosition;
            }
        }
        public bool IsViewMode { get { return EmployeePositionViewControl.IsViewMode && EmployeePositionLabourDistributionControl.IsViewMode && EmployeePositionAlternateLabourControl.IsViewMode; } }
        public bool IsInsertMode { get { return EmployeePositionViewControl.IsInsertMode || EmployeePositionLabourDistributionControl.IsInsertMode || EmployeePositionAlternateLabourControl.IsInsertMode; } }
        public bool IsEditMode { get { return EmployeePositionViewControl.IsEditMode || EmployeePositionLabourDistributionControl.IsEditMode || EmployeePositionAlternateLabourControl.IsEditMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.EmployeePosition.AddFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.EmployeePosition.DeleteFlag; } }
        public long keyId = -1; //used for row reselect

        #endregion

        #region main
        private void InitializeControls()
        {
            EmployeeSummaryToolBar.Enabled = IsViewMode;
            EmployeePositionGrid.Enabled = IsViewMode;
            RadTabStrip1.Enabled = IsViewMode;
        }
        private void Page_Load(object sender, EventArgs e)
        {
            //does the user have access rights?
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EmployeePosition.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                //does the user have rights to see this employee?
                SetAuthorized(Common.ServiceWrapper.HumanResourcesClient.GetEmployee(new EmployeeCriteria() { EmployeeId = EmployeeId }).Count > 0);
                LoadEmployeePosition(EmployeeId);
            }

            this.Page.Title = String.Format(GetGlobalResourceObject("PageTitle", "EmployeePositionControl").ToString(), LastName, FirstName, PayrollPeriodCutoffDate);

            EmployeePositionViewControl.PayrollPeriodCutoffDate = PayrollPeriodCutoffDate;
        }
        private void WireEvents()
        {
            EmployeePositionViewControl.NeedDataSource += new NeedDataSourceEventHandler(EmployeePositionViewControl_NeedDataSource);
            EmployeePositionViewControl.Updating += new UpdatingEventHandler(EmployeePositionViewControl_Updating);
            EmployeePositionViewControl.ItemCommand += new FormViewCommandEventArgs(EmployeePositionViewControl_ItemCommand);
            EmployeePositionViewControl.SelectedDateChanged += new SelectedDateChangedEventArgs(EmployeePositionViewControl_SelectedDateChanged);

            //subscribe to inserts/updates/delete of secondary position
            EmployeePositionAlternateLabourControl.RefreshParent += new EmployeePositionAlternateLabourControl.RefreshParentEventHandler(EmployeePositionGrid_Rebind);

            EmployeePositionGrid.SelectedIndexChanged += new EventHandler(EmployeePositionGrid_SelectedIndexChanged);
        }

        private void EmployeePositionGrid_Rebind()
        {
            LoadEmployeePosition(EmployeeId);
            EmployeePositionGrid.Rebind();
        }

        private void LoadEmployeePosition(long employeeId)
        {
            Data = Common.ServiceWrapper.EmployeePosition.SelectSummary(new EmployeePositionCriteria { EmployeeId = employeeId, PayrollProcessGroupCode = PayrollProcessGroupCode });

            ((WLPToolBarButton)EmployeeSummaryToolBar.FindButtonByCommandName("Add")).Visible = AddFlag;
            ((WLPToolBarButton)EmployeeSummaryToolBar.FindButtonByCommandName("Delete")).Visible = DeleteFlag;
        }
        private void SelectedCurrentEmployeePosition()
        {
            int index = -1;

            if (EmployeePositionGrid.Items.Count > 0)
            {
                index = 0;
                BusinessLayer.BusinessObjects.EmployeePosition currentPosition = null;

                if (Data != null && Data.Count > 0)
                {
                    foreach (GridDataItem item in EmployeePositionGrid.Items)
                    {
                        BusinessLayer.BusinessObjects.EmployeePosition position = (BusinessLayer.BusinessObjects.EmployeePosition)item.DataItem;

                        //if a row was selected and updated, reselect it after the update
                        if (keyId != -1)
                        {
                            if (position.EmployeePositionId == keyId)
                            {
                                currentPosition = position;
                                index = item.ItemIndex;
                                break;
                            }
                        }
                        //this logic selects the current employee position
                        else if (position.EffectiveDate <= PayrollPeriodCutoffDate)
                        {
                            if (currentPosition == null)
                            {
                                currentPosition = position;
                                index = item.ItemIndex;
                            }
                            else
                            {
                                if (position.EffectiveDate == currentPosition.EffectiveDate)
                                {
                                    if (position.EffectiveSequence > currentPosition.EffectiveSequence)
                                        currentPosition = position;
                                }
                                else if (position.EffectiveDate > currentPosition.EffectiveDate)
                                {
                                    currentPosition = position;
                                    index = item.ItemIndex;
                                }
                            }
                        }
                    }
                }
            }

            if (index > -1)
                EmployeePositionGrid.Items[index].Selected = true;

            //reset the delete button toggle
            ((WLPToolBarButton)EmployeeSummaryToolBar.FindButtonByCommandName("Delete")).Enabled = DeleteButtonEnabled;
        }
        private void UpdateEmployeePosition(BusinessLayer.BusinessObjects.EmployeePosition position)
        {
            try
            {
                if (position.EmployeePositionId < 0)
                    InsertEmployeePosition(position);
                else
                {
                    Common.ServiceWrapper.EmployeePosition.Update(SelectedEmployeePosition);

                    InsertCache = null;
                    EmployeePositionViewControl.InsertCache = null;

                    LoadEmployeePosition(EmployeeId);

                    //if a row was selected
                    if (EmployeePositionGrid.SelectedItems.Count != 0)
                        keyId = Convert.ToInt64(((GridDataItem)EmployeePositionGrid.SelectedItems[0]).GetDataKeyValue(EmployeePositionGrid.MasterTableView.DataKeyNames[0]));

                    EmployeePositionGrid.Rebind();
                    EmployeePositionViewControl.DataBind();                    
                }
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void InsertEmployeePosition(BusinessLayer.BusinessObjects.EmployeePosition position)
        {
            position.EmployeeId = EmployeeId;
            Common.ServiceWrapper.EmployeePosition.Insert(SelectedEmployeePosition, ((EmployeePositionSummaryCollection)Data).GetFormerPosition(null), PayrollPeriodCutoffDate, PayrollProcessGroupCode);

            InsertCache = null;
            EmployeePositionViewControl.InsertCache = null;

            //refresh from database
            LoadEmployeePosition(EmployeeId);
            EmployeePositionGrid.Rebind();
        }
        private void DeleteEmployeePosition(BusinessLayer.BusinessObjects.EmployeePosition position)
        {
            try
            {
                Common.ServiceWrapper.EmployeePosition.Delete(SelectedEmployeePosition, ((EmployeePositionSummaryCollection)Data).GetFormerPosition(position.EmployeePositionId));

                Data.Remove(SelectedEmployeePosition.Key);
                EmployeePositionViewControl.DataBind();

                //refresh from database
                LoadEmployeePosition(EmployeeId);
                EmployeePositionGrid.Rebind();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void CreateInsertCache()
        {
            //populate the inserted cache 
            InsertCache = Common.ServiceWrapper.EmployeePosition.Create();

            //if exists copy over last info as default
            if (LastEmployeePosition != null)
                InsertCache = LastEmployeePosition.CreateNew();

            //set InsertCache in the EmployeePositionViewControl
            EmployeePositionViewControl.InsertCache = InsertCache;
        }
        #endregion

        #region event handlers
        private void SetTabProperties()
        {
            EmployeePositionViewControl.DataBind();

            EmployeePositionLabourDistributionControl.EmployeePositionId = SelectedEmployeePosition.EmployeePositionId;
            EmployeePositionLabourDistributionControl.EditButtonEnabled = EditButtonEnabled;
            EmployeePositionLabourDistributionControl.RefreshLabourDistrubution();

            EmployeePositionAlternateLabourControl.EmployeePositionId = SelectedEmployeePosition.EmployeePositionId;
            EmployeePositionAlternateLabourControl.EffectiveDate = SelectedEmployeePosition.EffectiveDate;
            EmployeePositionAlternateLabourControl.EmployeePositionOrganizationUnits = SelectedEmployeePosition.EmployeePositionOrganizationUnits;
            EmployeePositionAlternateLabourControl.EditButtonEnabled = EditButtonEnabled;

            EmployeePositionAlternateLabourControl.RefreshAlternateLabour();
        }
        protected void EmployeePositionGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((WLPToolBarButton)EmployeeSummaryToolBar.FindButtonByCommandName("Delete")).Enabled = DeleteButtonEnabled;
            SetTabProperties();
        }
        protected void EmployeePositionGrid_DataBound(object sender, EventArgs e)
        {
            SelectedCurrentEmployeePosition();
            SetTabProperties();
        }
        protected void EmployeePositionGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EmployeePositionGrid.DataSource = Data;
        }
        protected void EmployeePositionViewControl_ItemCommand(object sender, System.Web.UI.WebControls.FormViewCommandEventArgs e)
        {
            //listens for child form cancel command (ItemCommand) to clear the InsertCache 
            InsertCache = null;
            EmployeePositionViewControl.InsertCache = null;
        }
        protected void EmployeePositionViewControl_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            //update the InsertCache in Child form with View State InsertCache of parent 
            EmployeePositionViewControl.InsertCache = InsertCache;
            SelectedEmployeePosition.EffectiveDate = e.NewDate;
        }
        protected void EmployeePositionViewControl_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            EmployeePositionViewControl.LastEmployeePosition = LastEmployeePosition;
            EmployeePositionViewControl.CurrentEmployeePositionCollection = CurrentEmployeePositionCollection;
        }
        protected void EmployeePositionViewControl_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            UpdateEmployeePosition((BusinessLayer.BusinessObjects.EmployeePosition)e.DataItem);

            //reset the delete button toggle
            ((WLPToolBarButton)EmployeeSummaryToolBar.FindButtonByCommandName("Delete")).Enabled = DeleteButtonEnabled;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void EmployeeSummaryToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            String commandName = ((RadToolBarButton)e.Item).CommandName.ToLower();

            if (commandName.Equals("add"))
            {
                CreateInsertCache();

                //make sure we put focus on the view control
                RadTabStrip1.SelectedIndex = 0;
                EmployeePositionMultiPage.SelectedIndex = 0;

                EmployeePositionViewControl.ChangeMode(FormViewMode.Edit);
                EmployeePositionViewControl.DataBind();
            }
            else if (commandName.Equals("delete"))
                DeleteEmployeePosition(SelectedEmployeePosition);
        }
        protected void LabourDistribution_OnInit(object sender, EventArgs e)
        {
            ((WebControl)sender).Visible = Common.Security.RoleForm.EmployeePositionLabourDistrubution.ViewFlag;
        }
        protected void AlternateLabour_OnInit(object sender, EventArgs e)
        {
            ((WebControl)sender).Visible = Common.Security.RoleForm.EmployeePositionAlternateLabour.ViewFlag;
        }
        protected void RadTabStrip_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                RadTabStrip1.SelectedIndex = 0;
        }
        protected void EmployeePositionMultiPage_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                EmployeePositionMultiPage.SelectedIndex = 0;
        }
        #endregion

        #region override
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            InitializeControls();
        }
        #endregion
    }
}