﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.EmployeePosition
{
    public partial class EmployeePositionAlternateLabourControl : WLP.Web.UI.WLPUserControl
    {
        private enum Operation { View, Add, Edit };

        public delegate void RefreshParentEventHandler();
        public event RefreshParentEventHandler RefreshParent;

        #region fields
        private long _employeePositionId = -1;
        private const String _employeePositionIdKey = "EmployeePositionIdKey";
        private DateTime? _effectiveDate = null;
        private const String _effectiveDateKey = "EffectiveDateKey";
        private bool _editButtonEnabled = false;
        private const String _editButtonEnabledKey = "EditButtonEnabledKey";
        private EmployeePositionOrganizationUnitCollection _employeePositionOrganizationUnits = null;
        private const String _employeePositionOrganizationUnitsKey = "EmployeePositionOrganizationUnitsKey";

        private const String _operationKey = "OperationKey";
        private Operation _operation = Operation.View;
        private const String _originalEmployeePositionSecondaryForEditKey = "OriginalEmployeePositionSecondaryForEditKey";
        private EmployeePositionSecondary _originalEmployeePositionSecondaryForEdit = null;
        #endregion

        #region properties
        public bool EditButtonEnabled
        {
            get
            {
                if (_editButtonEnabled == false)
                {
                    Object obj = ViewState[_editButtonEnabledKey];
                    if (obj != null)
                        _editButtonEnabled = Convert.ToBoolean(obj);
                }

                return _editButtonEnabled;
            }
            set
            {
                _editButtonEnabled = value;
                ViewState[_editButtonEnabledKey] = _editButtonEnabled;
            }
        }
        public long EmployeePositionId
        {
            get
            {
                if (_employeePositionId == -1)
                {
                    Object obj = ViewState[_employeePositionIdKey];
                    if (obj != null)
                        _employeePositionId = Convert.ToInt64(obj);
                }

                return _employeePositionId;
            }
            set
            {
                _employeePositionId = value;
                ViewState[_employeePositionIdKey] = _employeePositionId;
            }
        }
        public DateTime? EffectiveDate
        {
            get
            {
                if (_effectiveDate == null)
                {
                    Object obj = ViewState[_effectiveDateKey];
                    if (obj != null)
                        _effectiveDate = Convert.ToDateTime(obj);
                }

                return _effectiveDate;
            }
            set
            {
                _effectiveDate = value;
                ViewState[_effectiveDateKey] = _effectiveDate;
            }
        }
        public EmployeePositionOrganizationUnitCollection EmployeePositionOrganizationUnits
        {
            get
            {
                if (_employeePositionOrganizationUnits == null)
                {
                    Object obj = ViewState[_employeePositionOrganizationUnitsKey];
                    if (obj != null)
                        _employeePositionOrganizationUnits = (EmployeePositionOrganizationUnitCollection)obj;
                }

                return _employeePositionOrganizationUnits;
            }
            set
            {
                _employeePositionOrganizationUnits = value;
                ViewState[_employeePositionOrganizationUnitsKey] = _employeePositionOrganizationUnits;
            }
        }
        public bool IsViewMode { get { return AlternateLabourGrid.IsViewMode; } }
        public bool IsEditMode { get { return AlternateLabourGrid.IsEditMode; } }
        public bool IsInsertMode { get { return AlternateLabourGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.EmployeePositionAlternateLabour.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeePositionAlternateLabour.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.EmployeePositionAlternateLabour.DeleteFlag; } }

        private Operation CurrentOperation
        {
            get
            {
                Object obj = ViewState[_operationKey];
                if (obj != null)
                {
                    _operation = (Operation)Enum.ToObject(typeof(Operation), obj);
                }
                return _operation;
            }
            set
            {
                _operation = value;
                ViewState[_operationKey] = _operation;
            }
        }

        private EmployeePositionSecondary OriginalEmployeePositionSecondaryForEdit
        {
            get
            {
                Object obj = ViewState[_originalEmployeePositionSecondaryForEditKey];
                if (obj != null)
                {
                    _originalEmployeePositionSecondaryForEdit = (EmployeePositionSecondary)obj;
                }
                return _originalEmployeePositionSecondaryForEdit;
            }
            set
            {
                _originalEmployeePositionSecondaryForEdit = value;
                ViewState[_originalEmployeePositionSecondaryForEditKey] = _originalEmployeePositionSecondaryForEdit;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();

            if (!IsPostBack)
            {
                LoadAlternateLabour();
                Initialize();
            }
        }

        protected void Initialize()
        {
            //find the AlternateLabour Grid
            WLPGrid grid = (WLPGrid)this.FindControl("AlternateLabourGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag && EditButtonEnabled;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag && EditButtonEnabled;
        }

        protected void WireEvents()
        {
            AlternateLabourGrid.ItemDataBound += AlternateLabourGrid_ItemDataBound;

            SecondaryOrganizationUnitControl.OrganizationUnitChanged += SecondaryOrganizationUnitControl_OrganizationUnitChanged;
        }

        public void LoadAlternateLabour()
        {
            Data = Common.ServiceWrapper.HumanResourcesClient.GetEmployeePositionSecondary(null, EmployeePositionId, LanguageCode, null);
        }

        protected void AlternateLabourGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem) //hide the edit/delete images in the rows
            {
                GridDataItem item = (GridDataItem)e.Item;
                ImageButton editButton = (ImageButton)item["editButton"].Controls[0];
                ImageButton deleteButton = (ImageButton)item["deleteButton"].Controls[0];

                editButton.Visible = UpdateFlag && EditButtonEnabled;
                deleteButton.Visible = DeleteFlag && EditButtonEnabled;

                // Remember the original record being edited 
                if (CurrentOperation == Operation.Edit && item.Edit)
                {
                    OriginalEmployeePositionSecondaryForEdit = (EmployeePositionSecondary) item.DataItem;
                }
            }
            else if (e.Item is GridEditFormInsertItem && IsInsertMode || e.Item is GridEditFormItem && IsEditMode) //insert or edit mode
            {
                GridEditFormItem item = (GridEditFormItem)e.Item;
                SecondaryOrganizationUnitControl organizationUnitControl = (SecondaryOrganizationUnitControl)item.FindControl("SecondaryOrganizationUnitControl");

                if (organizationUnitControl != null)
                {
                    organizationUnitControl.OrganizationUnits = PopulateOrganizationUnits(item);
                    organizationUnitControl.DefaultDate = EffectiveDate;
                    organizationUnitControl.DataBind();
                }
            }
        }

        private EmployeePositionSecondaryOrganizationUnitCollection PopulateOrganizationUnits(GridEditFormItem item)
        {
            EmployeePositionSecondaryOrganizationUnitCollection collection = new EmployeePositionSecondaryOrganizationUnitCollection();

            if (IsInsertMode)
            {
                int i = -1;

                foreach (EmployeePositionOrganizationUnit unit in EmployeePositionOrganizationUnits)
                {
                    collection.Add(new EmployeePositionSecondaryOrganizationUnit()
                    {
                        EmployeePositionSecondaryOrganizationUnitId = i--,
                        EmployeePositionSecondaryId = -1,
                        OrganizationUnitLevelId = unit.OrganizationUnitLevelId,
                        OrganizationUnitId = (IsInsertMode && unit.UsedOnSecondaryEmployeePositionFlag) ? null : unit.OrganizationUnitId,
                        OrganizationUnitStartDate = (IsInsertMode && unit.UsedOnSecondaryEmployeePositionFlag) ? null : unit.OrganizationUnitStartDate,
                        UsedOnSecondaryEmployeePositionFlag = unit.UsedOnSecondaryEmployeePositionFlag
                    });
                }
            }
            else
                collection = ((EmployeePositionSecondary)item.DataItem).OrganizationUnits;

            return collection;
        }

        public void RefreshAlternateLabour()
        {
            LoadAlternateLabour();
            //force a call to AlternateLabourGrid_NeedDataSource from Rebind().  If it is not set to NULL, it wont call it.
            AlternateLabourGrid.DataSource = null; 
            AlternateLabourGrid.Rebind();

            /*Even though the Rebind() call above will trigger AlternateLabourGrid_ItemDataBound and assign the correct values to editButton.Visible, it seems they get lost
              for some reason.  The call below to Initialize will restore it but we shouldnt need to do this.
            */
            Initialize(); 
        }
        #endregion

        #region event handlers
        private void SecondaryOrganizationUnitControl_OrganizationUnitChanged(object sender, OrganizationUnitChangedEventArgs e)
        {
            GridEditFormItem item = (GridEditFormItem)((SecondaryOrganizationUnitControl)sender).NamingContainer;
            ComboBoxControl salaryPlan = (ComboBoxControl)item.FindControl("SalaryPlanId");
            ComboBoxControl salaryPlanGradeId = (ComboBoxControl)item.FindControl("SalaryPlanGradeId");

            if (salaryPlan != null)
            {
                salaryPlan.Value = e.SalaryPlanId;
                BindGradeCombo(sender);
            }

            if (salaryPlanGradeId != null)
            {
                salaryPlanGradeId.Value = e.SalaryPlanGradeId;
                BindStepCombo(sender);
            }
        }

        protected void SalaryPlan_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ComboBoxControl control = (ComboBoxControl)sender;

            if (control.ID.Equals("SalaryPlanId"))
                BindGradeCombo(sender);
            else if (control.ID.Equals("SalaryPlanGradeId"))
                BindStepCombo(sender);
        }

        protected void SalaryPlanId_NeedDataSource(object sender, EventArgs e)
        {
            if (sender is ComboBoxControl)
            {
                Common.CodeHelper.PopulateComboBoxWithSalaryPlan((ComboBoxControl)sender, LanguageCode);
                BindGradeCombo(sender);
            }
            else
                Common.CodeHelper.PopulateSalaryPlan((ICodeControl)sender, LanguageCode);
        }

        protected void SalaryPlanGradeId_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateSalaryPlanGrade((ICodeControl)sender, LanguageCode);
        }

        protected void SalaryPlanGradeStepId_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateSalaryPlanGradeStep((ICodeControl)sender, LanguageCode);
        }

        private void BindGradeCombo(object sender)
        {
            GridEditFormItem item = null;
            ComboBoxControl salaryPlanCombo = null;

            if (sender is ComboBoxControl)
            {
                salaryPlanCombo = (ComboBoxControl)sender;
                item = (GridEditFormItem)salaryPlanCombo.BindingContainer;
            }
            else if (sender is SecondaryOrganizationUnitControl)
            {
                item = (GridEditFormItem)((SecondaryOrganizationUnitControl)sender).NamingContainer;
                salaryPlanCombo = (ComboBoxControl)item.FindControl("SalaryPlanId");
            }

            ComboBoxControl gradeCombo = (ComboBoxControl)item.FindControl("SalaryPlanGradeId");
            if (gradeCombo != null)
            {
                SetSalaryPlanGrade(item.DataItem, salaryPlanCombo, gradeCombo);

                ComboBoxControl stepCombo = (ComboBoxControl)item.FindControl("SalaryPlanGradeStepId");
                if (stepCombo != null)
                    SetSalaryPlanGradeStep(item.DataItem, gradeCombo, stepCombo);
            }
        }

        private void BindStepCombo(object sender)
        {
            GridEditFormItem item = null;
            ComboBoxControl gradeCombo = null;

            if (sender is ComboBoxControl)
            {
                gradeCombo = (ComboBoxControl)sender;
                item = (GridEditFormItem)gradeCombo.BindingContainer;
            }
            else if (sender is SecondaryOrganizationUnitControl)
            {
                item = (GridEditFormItem)((SecondaryOrganizationUnitControl)sender).NamingContainer;
                gradeCombo = (ComboBoxControl)item.FindControl("SalaryPlanGradeId");
            }

            ComboBoxControl stepCombo = (ComboBoxControl)item.FindControl("SalaryPlanGradeStepId");
            if (stepCombo != null)
                SetSalaryPlanGradeStep(item.DataItem, gradeCombo, stepCombo);
        }

        private void SetSalaryPlanGrade(object item, ComboBoxControl salaryPlanCombo, ComboBoxControl gradeCombo)
        {
            //have to set the value of the grade combo here because it's not picked up on the ASP side
            if (item != null && item is EmployeePositionSecondary)
                gradeCombo.Value = ((EmployeePositionSecondary)item).SalaryPlanGradeId;
            else
                gradeCombo.Value = null;

            Common.CodeHelper.PopulateComboBoxWithSalaryPlanGrade(gradeCombo, salaryPlanCombo.SelectedValue != null ? Convert.ToInt64(salaryPlanCombo.SelectedValue) : -1, LanguageCode);
            gradeCombo.DataBind();
        }

        private void SetSalaryPlanGradeStep(object item, ComboBoxControl gradeCombo, ComboBoxControl stepCombo)
        {
            //have to set the value of the step combo here because it's not picked up on the ASP side
            if (item != null && item is EmployeePositionSecondary)
                stepCombo.Value = ((EmployeePositionSecondary)item).SalaryPlanGradeStepId;
            else
                stepCombo.Value = null;

            Common.CodeHelper.PopulateComboBoxWithSalaryPlanGradeStep(stepCombo, gradeCombo.SelectedValue != null ? Convert.ToInt64(gradeCombo.SelectedValue) : -1, LanguageCode);
            stepCombo.DataBind();
        }

        protected void AlternateLabourGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            AlternateLabourGrid.DataSource = Data;
        }

        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }

        protected void AlternateLabourGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string commandNameLower = e.CommandName.ToLower();

            // Add button is clicked
            if (commandNameLower.Equals("initinsert"))
            {
                CurrentOperation = Operation.Add;
                OriginalEmployeePositionSecondaryForEdit = null;
            }
            // Edit button is clicked
            else if (commandNameLower.Equals("edit"))
            {
                CurrentOperation = Operation.Edit;
                OriginalEmployeePositionSecondaryForEdit = null;
            }
            // Update button is clicked for Edit operation
            else if (commandNameLower.Equals("update"))
            {

            }
            // Insert button is clicked for Add operation
            else if (commandNameLower.Equals("performinsert"))
            {

            }
            else
            {
                CurrentOperation = Operation.View;
                OriginalEmployeePositionSecondaryForEdit = null;
            }
        }
        #endregion

        #region handle updates
        protected void AlternateLabourGrid_InsertCommand(object sender, GridCommandEventArgs e)
        {
            EmployeePositionSecondary item = (EmployeePositionSecondary)e.Item.DataItem;
            item.EmployeePositionId = EmployeePositionId;
            item.OrganizationUnits = ((SecondaryOrganizationUnitControl)e.Item.FindControl("SecondaryOrganizationUnitControl")).OrganizationUnits;

            Common.ServiceWrapper.HumanResourcesClient.InsertEmployeePositionSecondary(item).CopyTo((EmployeePositionSecondary)Data[item.Key]);
            RefreshAlternateLabour();

            //update parent position object to include this new secondary position
            RefreshParent?.Invoke();
        }
        protected void AlternateLabourGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                EmployeePositionSecondary item = (EmployeePositionSecondary)e.Item.DataItem;
                item.OrganizationUnits = ((SecondaryOrganizationUnitControl)e.Item.FindControl("SecondaryOrganizationUnitControl")).OrganizationUnits;

                Common.ServiceWrapper.HumanResourcesClient.UpdateEmployeePositionSecondary(item).CopyTo((EmployeePositionSecondary)Data[item.Key]);
                RefreshAlternateLabour();

                //update parent position object to include changes to this secondary position
                RefreshParent?.Invoke();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void AlternateLabourGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteEmployeePositionSecondary((EmployeePositionSecondary)e.Item.DataItem);

                //update parent position object to remove this secondary position
                RefreshParent?.Invoke();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void SecondaryOrganizationUnitControlValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            String errorMessage = null;
            CustomValidator customValidator = null;
            Control namingContainer = null;
            GridEditFormItem gridEditFormItem = null;
            SecondaryOrganizationUnitControl secondaryOrganizationUnitControl = null;
            EmployeePositionSecondaryOrganizationUnitCollection employeePositionSecondaryOrganizationUnits = null;


            customValidator = (CustomValidator)source;
            namingContainer = customValidator.NamingContainer;

            // Add operation
            if (CurrentOperation == Operation.Add)
            {
                gridEditFormItem = (GridEditFormItem)namingContainer;
                secondaryOrganizationUnitControl = (SecondaryOrganizationUnitControl)gridEditFormItem.FindControl("SecondaryOrganizationUnitControl");
                employeePositionSecondaryOrganizationUnits = secondaryOrganizationUnitControl.OrganizationUnits;
                
                // Check if there is already an existing record with the new Organization Unit
                EmployeePositionSecondaryCollection existingRecords = Common.ServiceWrapper.HumanResourcesClient.GetEmployeePositionSecondary(null, EmployeePositionId, LanguageCode, employeePositionSecondaryOrganizationUnits.OrganizationUnit);

                // there is already an existing record with the new Organization Unit
                if (existingRecords != null && existingRecords.Count > 0)
                {
                    errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "DupeOrganizationUnit");
                    args.IsValid = false;
                    customValidator.ErrorMessage = errorMessage;
                }
            }
            // Edit operation
            else if (CurrentOperation == Operation.Edit)
            {
                gridEditFormItem = (GridEditFormItem)namingContainer;
                secondaryOrganizationUnitControl = (SecondaryOrganizationUnitControl)gridEditFormItem.FindControl("SecondaryOrganizationUnitControl");
                employeePositionSecondaryOrganizationUnits = secondaryOrganizationUnitControl.OrganizationUnits;

                bool organizationUnitChanged = false;

                // current has no null/empty organization unit
                if (employeePositionSecondaryOrganizationUnits.OrganizationUnit != null && !(employeePositionSecondaryOrganizationUnits.OrganizationUnit.Trim().Equals("")))
                {
                    // orginal has null/empty organization unit
                    if (OriginalEmployeePositionSecondaryForEdit.OrganizationUnit == null || OriginalEmployeePositionSecondaryForEdit.OrganizationUnit.Trim().Equals(""))
                    {
                        organizationUnitChanged = true;
                    }
                    // orginal has no null/empty organization unit
                    // orginal has different organization unit from current 
                    else if (!(OriginalEmployeePositionSecondaryForEdit.OrganizationUnit.Trim().Equals(employeePositionSecondaryOrganizationUnits.OrganizationUnit.Trim())))
                    {
                        organizationUnitChanged = true;
                    }
                }

                if (organizationUnitChanged)
                {
                    // Check if there is already an existing record with the new Organization Unit
                    EmployeePositionSecondaryCollection existingRecords = Common.ServiceWrapper.HumanResourcesClient.GetEmployeePositionSecondary(null, EmployeePositionId, LanguageCode, employeePositionSecondaryOrganizationUnits.OrganizationUnit);

                    // there is already an existing record with the new Organization Unit
                    if (existingRecords != null && existingRecords.Count > 0)
                    {
                        errorMessage = (String)GetGlobalResourceObject("ErrorMessages", "DupeOrganizationUnit");
                        args.IsValid = false;
                        customValidator.ErrorMessage = errorMessage;
                    }
                }
            }
        }
        #endregion
    }
}