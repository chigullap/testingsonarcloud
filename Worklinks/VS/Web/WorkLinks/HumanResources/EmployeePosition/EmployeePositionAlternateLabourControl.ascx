﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeePositionAlternateLabourControl.ascx.cs" Inherits="WorkLinks.HumanResources.EmployeePosition.EmployeePositionAlternateLabourControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="SecondaryOrganizationUnitControl.ascx" TagName="SecondaryOrganizationUnitControl" TagPrefix="uc1" %>

<wasp:WLPGrid
    ID="AlternateLabourGrid"
    runat="server"
    GridLines="None"
    AutoGenerateColumns="false"
    AutoAssignModifyProperties="true"
    OnUpdateCommand="AlternateLabourGrid_UpdateCommand"
    OnInsertCommand="AlternateLabourGrid_InsertCommand"
    OnDeleteCommand="AlternateLabourGrid_DeleteCommand"
    OnItemCommand="AlternateLabourGrid_ItemCommand"
    OnNeedDataSource="AlternateLabourGrid_NeedDataSource">

    <ClientSettings EnablePostBackOnRowClick="false" AllowColumnsReorder="true" ReorderColumnsOnClient="true">
        <Selecting AllowRowSelect="false" />
        <Scrolling AllowScroll="false" UseStaticHeaders="true" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">

        <CommandItemTemplate>
            <wasp:WLPToolBar ID="AlternateLabourToolBar" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="**Add**" CommandName="InitInsert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsViewMode && AddFlag && EditButtonEnabled %>' ResourceName="Add" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <Columns>
            <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
            <wasp:GridBoundControl DataField="OrganizationUnitDescription" LabelText="**OrganizationUnit**" SortExpression="OrganizationUnitDescription" UniqueName="OrganizationUnitDescription" ResourceName="OrganizationUnitDescription" />
            <wasp:GridNumericControl DataField="CompensationAmount" LabelText="**CompensationAmount**" DataFormatString="{0:$###,##0.0000}" SortExpression="CompensationAmount" UniqueName="CompensationAmount" DataType="System.Int64" ResourceName="CompensationAmount" />
            <wasp:GridKeyValueControl OnNeedDataSource="SalaryPlanId_NeedDataSource" DataField="SalaryPlanId" LabelText="**SalaryPlanId**" Type="SalaryPlanId" ResourceName="SalaryPlanId" />
            <wasp:GridKeyValueControl OnNeedDataSource="SalaryPlanGradeId_NeedDataSource" DataField="SalaryPlanGradeId" LabelText="**SalaryPlanGradeId**" Type="SalaryPlanGradeId" ResourceName="SalaryPlanGradeId" />
            <wasp:GridKeyValueControl OnNeedDataSource="SalaryPlanGradeStepId_NeedDataSource" DataField="SalaryPlanGradeStepId" LabelText="**SalaryPlanGradeStepId**" Type="SalaryPlanGradeStepId" ResourceName="SalaryPlanGradeStepId" DataType="System.Decimal" />
            <wasp:GridNumericControl DataField="AccumulatedHours" LabelText="**AccumulatedHours**" DataFormatString="{0:###,##0.00}" UniqueName="AccumulatedHours" ResourceName="AccumulatedHours" />
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table width="100%">
                    <tr>
                        <td colspan="2">
                            <div style="margin-left: -8px;">
                                <uc1:SecondaryOrganizationUnitControl ID="SecondaryOrganizationUnitControl" runat="server" TabIndex="010" />
                            </div>
                            <asp:CustomValidator ID="SecondaryOrganizationUnitControlValidator" runat="server" ForeColor="Red" OnServerValidate="SecondaryOrganizationUnitControlValidator_ServerValidate"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="SalaryPlanId" Text="**SalaryPlanId**" runat="server" ResourceName="SalaryPlanId" OnDataBinding="SalaryPlanId_NeedDataSource" OnSelectedIndexChanged="SalaryPlan_SelectedIndexChanged" Value='<%# Bind("SalaryPlanId") %>' AutoPostback="true" TabIndex="030" />
                        </td>
                        <td>
                            <wasp:NumericControl ID="CompensationAmount" ClientIDMode="Static" Text="**CompensationAmount**" runat="server" ResourceName="CompensationAmount" DecimalDigits="4" Value='<%# Bind("CompensationAmount") %>' Mandatory="true" TabIndex="020" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="SalaryPlanGradeId" Text="**SalaryPlanGradeId**" runat="server" ResourceName="SalaryPlanGradeId" OnSelectedIndexChanged="SalaryPlan_SelectedIndexChanged" Value='<%# Bind("SalaryPlanGradeId") %>' AutoPostback="true" TabIndex="040" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="SalaryPlanGradeStepId" ClientIDMode="Static" Text="**SalaryPlanGradeStepId**" runat="server" OnClientSelectedIndexChanged="setRateAltLabor" ResourceName="SalaryPlanGradeStepId" Value='<%# Bind("SalaryPlanGradeStepId") %>' TabIndex="050" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="**Update**" runat="server" CommandName="Update" Visible='<%# IsEditMode %>' ResourceName="Update" />
                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="**Insert**" runat="server" CommandName="PerformInsert" Visible='<%# !IsEditMode %>' ResourceName="Insert" />
                                    </td>
                                    <td>
                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="**Cancel**" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>
    </MasterTableView>
    
    <HeaderContextMenu EnableAutoScroll="true" />

</wasp:WLPGrid>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js" type="text/javascript"></script>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function setRateAltLabor(source, args) {
            
            var compAmountControl = document.getElementById(document.getElementById('CompensationAmount').attributes['fieldClientId'].value);
            var stepControl = document.getElementById(document.getElementById('SalaryPlanGradeStepId').attributes['fieldClientId'].value);
            var salaryPlanGradeStepId = stepControl.control._value;
            
            $.ajax
            ({
                type: "POST",
                url: getUrl("/HumanResources/EmployeePosition/EmployeePositionPage.aspx/GetRate"),
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: "{'salaryPlanGradeStepId': '" + salaryPlanGradeStepId + "'}",
                success: function (msg) {
                    if (msg.d != null) {
                        compAmountControl.control.set_value(msg.d.Amount);
                    }
                }
            });

            return false;
            
        }
    </script>
</telerik:RadScriptBlock>