﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.EmployeePosition
{
    public partial class SecondaryOrganizationUnitControl : WLP.Web.UI.WLPUserControl
    {
        #region delegates
        public delegate void OrganizationUnitChangedEventHandler(object sender, OrganizationUnitChangedEventArgs e);
        public static event OrganizationUnitChangedEventHandler OrganizationUnitChanged;
        #endregion

        #region fields
        private short _tabIndex = 0;
        private bool _readOnly;
        private DateTime? _defaultDate = null;
        private const String _defaultDateKey = "defaultDateKey";
        #endregion

        #region properties
        public short TabIndex
        {
            get { return _tabIndex; }
            set { _tabIndex = value; }
        }
        public DateTime? DefaultDate
        {
            get
            {
                if (_defaultDate == null)
                    _defaultDate = (DateTime?)ViewState[_defaultDateKey];

                return _defaultDate;
            }
            set
            {
                _defaultDate = value;
                ViewState[_defaultDateKey] = _defaultDate;
            }
        }
        public EmployeePositionSecondaryOrganizationUnitCollection OrganizationUnits
        {
            get
            {
                return (EmployeePositionSecondaryOrganizationUnitCollection)Data;
            }
            set
            {
                InitialData = value;
                Data = value;
                OrganizationUnitGrid.DataSource = Data;
            }
        }
        public bool ReadOnly
        {
            get { return _readOnly; }
            set { _readOnly = value; }
        }
        protected bool IsDateVisible { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]) > 0; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();
        }
        protected void WireEvents()
        {
            OrganizationUnitGrid.ItemDataBound += OrganizationUnitGrid_ItemDataBound;
        }
        #endregion

        #region event handlers
        void OrganizationUnitGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;

                ComboBoxControl combo = (ComboBoxControl)item.FindControl("OrganizationUnitLevelId");
                if (combo != null)
                    combo.TabIndex = _tabIndex++;

                DateControl date = (DateControl)item.FindControl("OrganizationUnitStartDate");
                if (date != null)
                {
                    date.TabIndex = _tabIndex++;
                    date.MinDate = DefaultDate;
                }

                item.Display = ((EmployeePositionSecondaryOrganizationUnit)item.DataItem).UsedOnSecondaryEmployeePositionFlag;
                combo.Mandatory = ((EmployeePositionSecondaryOrganizationUnit)item.DataItem).UsedOnSecondaryEmployeePositionFlag;
                date.Mandatory = ((EmployeePositionSecondaryOrganizationUnit)item.DataItem).UsedOnSecondaryEmployeePositionFlag;
            }
        }
        protected void OrganizationUnitLevelId_DataBinding(object sender, EventArgs e)
        {
            //get parent
            String levelKey = ((ComboBoxControl)sender).LabelValue;
            int index = ((EmployeePositionSecondaryOrganizationUnitCollection)Data).IndexOf(levelKey);
            long? parentId = null;

            if (index > 0)
                parentId = ((EmployeePositionSecondaryOrganizationUnit)Data[index - 1]).OrganizationUnitId ?? -1;

            AssemblyCache.CodeHelper.PopulateComboBoxWithOrganizationUnitByLevel((ComboBoxControl)sender, Convert.ToInt64(levelKey));
            AssemblyCache.CodeHelper.PopulateComboBoxLabelWithOrganizationUnitLevel((ComboBoxControl)sender);
        }
        protected void OrganizationUnitLevelId_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //get index of changed dropdown
            String levelKey = ((ComboBoxControl)sender).LabelValue;
            int index = Data.IndexOf(levelKey);

            //clear current date
            ((EmployeePositionSecondaryOrganizationUnit)Data[index]).OrganizationUnitStartDate = DefaultDate;

            EmployeePositionSecondaryOrganizationUnit currentItem = ((EmployeePositionSecondaryOrganizationUnit)Data[index]);

            if (e.Value == null || e.Value.Equals(String.Empty))
                currentItem.OrganizationUnitId = null;
            else
            {
                currentItem.OrganizationUnitId = Convert.ToInt64(e.Value);
                EmployeePositionSecondaryOrganizationUnit originalItem = (EmployeePositionSecondaryOrganizationUnit)InitialData[index];

                if (currentItem.OrganizationUnitId.Equals(originalItem.OrganizationUnitId))
                    ((EmployeePositionSecondaryOrganizationUnit)Data[index]).OrganizationUnitStartDate = originalItem.OrganizationUnitStartDate;
            }

            //null out the org units below the changed one
            for (int i = index + 1; i < Data.Count; i++)
            {
                ((EmployeePositionSecondaryOrganizationUnit)Data[i]).OrganizationUnitId = null;
                ((EmployeePositionSecondaryOrganizationUnit)Data[i]).OrganizationUnitStartDate = DefaultDate;
            }

            OrganizationUnitGrid.DataSource = Data;
            OrganizationUnitGrid.Rebind();

            if (index + 1 == Data.Count)
                OnOrganizationUnitChanged();
        }
        private void OnOrganizationUnitChanged()
        {
            OrganizationUnitChangedEventArgs args = new OrganizationUnitChangedEventArgs();

            SalaryPlanOrganizationUnitCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetSalaryPlanOrganizationUnit(((EmployeePositionSecondaryOrganizationUnitCollection)Data).OrganizationUnit);
            if (collection != null && collection.Count > 0)
            {
                args.SalaryPlanId = collection[0].SalaryPlanId;
                args.SalaryPlanGradeId = collection[0].SalaryPlanGradeId;
            }

            OnOrganizationUnitChanged(args);
        }
        protected void OrganizationUnitStartDate_SelectedDateChanged(object source, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs args)
        {
            //get index of changed date
            DateControl control = (DateControl)source;
            String levelKey = control.LabelValue;
            int index = Data.IndexOf(levelKey);

            ((EmployeePositionSecondaryOrganizationUnit)Data[index]).OrganizationUnitStartDate = (DateTime?)control.Value;
        }
        #endregion

        #region event triggers
        protected virtual void OnOrganizationUnitChanged(OrganizationUnitChangedEventArgs args)
        {
            if (OrganizationUnitChanged != null)
                OrganizationUnitChanged(this, args);
        }
        #endregion
    }
}