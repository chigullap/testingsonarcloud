﻿using System;
using System.ComponentModel;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.EmployeePosition
{
    public partial class OrganizationUnitControl : WLP.Web.UI.WLPUserControl
    {
        #region delegates
        public delegate void OrganizationUnitChangedEventHandler(object sender, OrganizationUnitChangedEventArgs e);
        public static event OrganizationUnitChangedEventHandler OrganizationUnitChanged;
        #endregion

        #region fields
        //private long _employeePositionId;
        private bool _readOnly;
        private DateTime? _defaultDate = null;
        private const String _defaultDateKey = "defaultDateKey";
        private short _tabIndex = 0;
        #endregion

        #region properties
        public short TabIndex
        {
            get { return _tabIndex; }
            set { _tabIndex = value; }
        }
        protected bool IsDateVisible { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]) > 0; } }
        [Bindable(true)]
        public DateTime? DefaultDate
        {
            get
            {
                if (_defaultDate == null)
                    _defaultDate = (DateTime?)ViewState[_defaultDateKey];

                return _defaultDate;
            }
            set
            {
                _defaultDate = value;
                ViewState[_defaultDateKey] = _defaultDate;
            }
        }
        [Bindable(true)]
        public EmployeePositionOrganizationUnitCollection EmployeePositionOrganizationUnits
        {
            get
            {
                return (EmployeePositionOrganizationUnitCollection)Data;
            }
            set
            {
                InitialData = value;
                Data = value;
                OrganizationUnitGrid.DataSource = Data;
            }
        }
        public bool ReadOnly
        {
            get { return _readOnly; }
            set { _readOnly = value; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();
        }
        protected void WireEvents()
        {
            OrganizationUnitGrid.UpdateCommand += new GridCommandEventHandler(OrganizationUnitGrid_UpdateCommand);
            OrganizationUnitGrid.ItemDataBound += OrganizationUnitGrid_ItemDataBound;
        }
        #endregion

        #region handle events
        void OrganizationUnitGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                ComboBoxControl combo = (ComboBoxControl)item.FindControl("OrganizationUnitLevelId");
                if (combo != null)
                    combo.TabIndex = _tabIndex++;

                DateControl date = (DateControl)item.FindControl("OrganizationUnitStartDate");
                if (date != null)
                    date.TabIndex = _tabIndex++;
            }
        }
        void OrganizationUnitGrid_UpdateCommand(object sender, GridCommandEventArgs e)
        {
        }
        protected void OrganizationUnitLevelId_DataBinding(object sender, EventArgs e)
        {
            //get parent 
            //long levelKey = Convert.ToInt64(((ComboBoxControl)sender).LabelValue);
            //int index = ((EmployeePositionOrganizationUnitCollection)Data).GetIndexOfLevel(levelKey);
            String levelKey = ((ComboBoxControl)sender).LabelValue;
            int index = ((EmployeePositionOrganizationUnitCollection)Data).IndexOf(levelKey);
            long? parentId = null;

            if (index > 0)
                parentId = ((EmployeePositionOrganizationUnit)Data[index - 1]).OrganizationUnitId ?? -1;

            AssemblyCache.CodeHelper.PopulateComboBoxWithOrganizationUnit((ComboBoxControl)sender, parentId);
            AssemblyCache.CodeHelper.PopulateComboBoxLabelWithOrganizationUnitLevel((ComboBoxControl)sender);
        }
        /// <summary>
        /// fires when any combobox selection is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OrganizationUnitLevelId_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //get index of changed dropdown
            String levelKey = (String)((ComboBoxControl)sender).LabelValue;
            int index = Data.IndexOf(levelKey);

            EmployeePositionOrganizationUnit currentItem = ((EmployeePositionOrganizationUnit)Data[index]);

            //clear current date
            ((EmployeePositionOrganizationUnit)Data[index]).OrganizationUnitStartDate = DefaultDate;

            if (e.Value == null || e.Value.Equals(String.Empty))
                currentItem.OrganizationUnitId = null;
            else
            {
                currentItem.OrganizationUnitId = Convert.ToInt64(e.Value);
                EmployeePositionOrganizationUnit originalItem = (EmployeePositionOrganizationUnit)InitialData[index];

                if (currentItem.OrganizationUnitId.Equals(originalItem.OrganizationUnitId))
                    ((EmployeePositionOrganizationUnit)Data[index]).OrganizationUnitStartDate = originalItem.OrganizationUnitStartDate;
            }

            //null out the org units below the changed one
            for (int i = index + 1; i < Data.Count; i++)
            {
                ((EmployeePositionOrganizationUnit)Data[i]).OrganizationUnitId = null;
                ((EmployeePositionOrganizationUnit)Data[i]).OrganizationUnitStartDate = DefaultDate;
            }

            OrganizationUnitGrid.DataSource = Data;
            OrganizationUnitGrid.Rebind();

            if (index + 1 == Data.Count)
                OnOrganizationUnitChanged();
        }
        private void OnOrganizationUnitChanged()
        {
            OrganizationUnitChangedEventArgs args = new OrganizationUnitChangedEventArgs();

            SalaryPlanOrganizationUnitCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetSalaryPlanOrganizationUnit(((EmployeePositionOrganizationUnitCollection)Data).OrganizationUnit);
            if (collection != null && collection.Count > 0)
            {
                args.SalaryPlanId = collection[0].SalaryPlanId;
                args.SalaryPlanGradeId = collection[0].SalaryPlanGradeId;
            }

            OnOrganizationUnitChanged(args);
        }
        protected void OrganizationUnitStartDate_SelectedDateChanged(object source, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs args)
        {
            //get index of changed date
            DateControl control = (DateControl)source;
            String levelKey = (String)(control).LabelValue;
            int index = Data.IndexOf(levelKey);

            ((EmployeePositionOrganizationUnit)Data[index]).OrganizationUnitStartDate = (DateTime?)control.Value;
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            // add JS event handler for the OnGridCreated event of the RadGrid   
            //     - the JS method will remove the tabindex on the grid control so when tabbing there is no tab stop on the grid   
            //this.OrganizationUnitGrid.ClientSettings.ClientEvents.OnGridCreated = "grid_OnGridCreated";
        }
        #endregion

        #region event triggers
        protected virtual void OnOrganizationUnitChanged(OrganizationUnitChangedEventArgs args)
        {
            if (OrganizationUnitChanged != null)
                OrganizationUnitChanged(this, args);
        }
        #endregion
    }

    public class OrganizationUnitChangedEventArgs : EventArgs
    {
        public long SalaryPlanId { get; set; }
        public long SalaryPlanGradeId { get; set; }
    }
}