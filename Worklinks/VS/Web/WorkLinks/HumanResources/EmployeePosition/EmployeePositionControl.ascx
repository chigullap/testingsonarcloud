﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeePositionControl.ascx.cs" Inherits="WorkLinks.HumanResources.EmployeePosition.EmployeePositionControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="EmployeePositionViewControl.ascx" TagName="EmployeePositionViewControl" TagPrefix="uc2" %>
<%@ Register Src="EmployeePositionLabourDistributionControl.ascx" TagName="EmployeePositionLabourDistributionControl" TagPrefix="uc3" %>
<%@ Register Src="EmployeePositionAlternateLabourControl.ascx" TagName="EmployeePositionAlternateLabourControl" TagPrefix="uc4" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="AjaxPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="AjaxPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="AjaxPanel" runat="server">
    <wasp:WLPToolBar ID="EmployeeSummaryToolBar" OnClientButtonClicking="confirmation" runat="server" Width="100%" OnButtonClick="EmployeeSummaryToolBar_ButtonClick">
        <Items>
            <wasp:WLPToolBarButton Visible='<%# AddFlag %>' Text="Add" CausesValidation="false" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="Add" ResourceName="Add" />
            <wasp:WLPToolBarButton Visible='<%# DeleteFlag %>' Text="Delete" CausesValidation="false" ImageUrl="~/App_Themes/Default/Delete.gif" Enabled="false" CommandName="Delete" ResourceName="Delete" />
        </Items>
    </wasp:WLPToolBar>

    <wasp:WLPGrid
        ID="EmployeePositionGrid"
        runat="server"
        GridLines="None"
        Height="120px"
        AutoGenerateColumns="false"
        OnNeedDataSource="EmployeePositionGrid_NeedDataSource"
        OnDataBound="EmployeePositionGrid_DataBound">

        <ClientSettings EnablePostBackOnRowClick="true" AllowKeyboardNavigation="true">
            <Selecting AllowRowSelect="true" />
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView DataKeyNames="Key" CommandItemDisplay="Top">
            <CommandItemTemplate></CommandItemTemplate>
            <Columns>
                <wasp:GridBoundControl DataField="EmployeePositionId" LabelText="EmployeePositionId" SortExpression="EmployeePositionId" UniqueName="EmployeePositionId" ResourceName="EmployeePositionId" Display="false" />
                <wasp:GridDateTimeControl DataField="EffectiveDate" LabelText="EffectiveDate" SortExpression="EffectiveDate" UniqueName="EffectiveDate" ResourceName="EffectiveDate" HeaderStyle-Width="100px" />
                <wasp:GridBoundControl DataField="EffectiveSequence" LabelText="EffectiveSequence" SortExpression="EffectiveSequence" UniqueName="EffectiveSequence" ResourceName="EffectiveSequence" HeaderStyle-Width="50px" />
                <wasp:GridBoundControl DataField="OrganizationUnitDescription" LabelText="OrganizationUnitDescription" SortExpression="OrganizationUnitDescription" UniqueName="OrganizationUnitDescription" ResourceName="OrganizationUnitDescription" />
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="EmployeePositionStatusCode" Type="EmployeePositionStatusCode" ResourceName="EmployeePositionStatusCode" HeaderStyle-Width="140px" />
            </Columns>
        </MasterTableView>

    </wasp:WLPGrid>
    <div style="overflow-y: auto"; class="TabPage">
        <wasp:WLPTabStrip ID="RadTabStrip1" runat="server" SelectedIndex="0" MultiPageID="EmployeePositionMultiPage" Style="margin-bottom: 0" OnPreRender="RadTabStrip_PreRender">
            <Tabs>
                <wasp:WLPTab Text="Position View" PageViewID="EmployeePositionPageView" Selected="true" ResourceName="EmployeePositionTab" />
                <wasp:WLPTab Text="Labour Distribution" OnInit="LabourDistribution_OnInit" ResourceName="EmployeePositionLabourDistributionControlTab" />
                <wasp:WLPTab Text="Alternate Labour" OnInit="AlternateLabour_OnInit" ResourceName="EmployeePositionAlternateLabourControlTab" />
            </Tabs>
        </wasp:WLPTabStrip>

        <telerik:RadMultiPage ID="EmployeePositionMultiPage" runat="server" SelectedIndex="0" OnPreRender="EmployeePositionMultiPage_PreRender">
            <telerik:RadPageView ID="EmployeePositionPageView" runat="server" Selected="true">
                <uc2:EmployeePositionViewControl runat="server" ID="EmployeePositionViewControl" Visible="true" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="EmployeePositionLabourDistributionControlView" OnInit="LabourDistribution_OnInit" runat="server">
                <uc3:EmployeePositionLabourDistributionControl ID="EmployeePositionLabourDistributionControl" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="EmployeePositionAlternateLabourControlView" OnInit="AlternateLabour_OnInit" runat="server">
                <uc4:EmployeePositionAlternateLabourControl ID="EmployeePositionAlternateLabourControl" runat="server" />
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
</asp:Panel>

<script type="text/javascript">
    function onClientClose(sender, eventArgs) {
        var arg = eventArgs.get_argument();
        if (arg != null && arg.isUpdate)
            __doPostBack('<# EmployeePositionView.ClientID >', String.format('EmployeeId={0}', arg.employeeId));
    }

    function confirmation(sender, args) {
        var button = args.get_item();
        if (button.get_commandName() == "Delete") {
            var message = "<asp:Literal runat="server" Text="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />";
            args.set_cancel(!confirm(message));
        }
    }
</script>