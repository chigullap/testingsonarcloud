﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrganizationUnitControl.ascx.cs" Inherits="WorkLinks.HumanResources.EmployeePosition.OrganizationUnitControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<wasp:WLPGrid
    ID="OrganizationUnitGrid"
    runat="server"
    AutoGenerateColumns="false"
    AlternatingItemStyle-BackColor="Transparent"
    ItemStyle-BackColor="Transparent"
    ShowHeader="false"
    AllowMultiRowEdit="true"
    BorderStyle="none"
    ClientIDMode="Static">

    <MasterTableView DataKeyNames="Key, OrganizationUnitId" AllowFilteringByColumn="true">
        <Columns>
            <wasp:GridTemplateControl UniqueName="TemplateOrganizationUnitLevelId">
                <ItemTemplate>
                    <wasp:ComboBoxControl ReadOnly='<%# ReadOnly %>' ID="OrganizationUnitLevelId" runat="server" AutoPostback="true" ResourceName="OrganizationUnitLevelId" OnSelectedIndexChanged="OrganizationUnitLevelId_SelectedIndexChanged" OnDataBinding="OrganizationUnitLevelId_DataBinding" Sort="Ascending" Value='<%# Bind("OrganizationUnitId") %>' LabelValue='<%# Bind("OrganizationUnitLevelId") %>' TabIndex="51"></wasp:ComboBoxControl>
                </ItemTemplate>
            </wasp:GridTemplateControl>
            <wasp:GridTemplateControl UniqueName="Template">
                <ItemTemplate>
                    <wasp:DateControl ReadOnly='<%# ReadOnly %>' Visible='<%# IsDateVisible %>' ID="OrganizationUnitStartDate" Text="Start Date" runat="server" LabelValue='<%# Bind("OrganizationUnitLevelId") %>' ResourceName="OrganizationUnitHierarchyStartDate" OnSelectedDateChanged="OrganizationUnitStartDate_SelectedDateChanged" Value='<%# Bind("OrganizationUnitStartDate") %>' TabIndex="55" />
                </ItemTemplate>
            </wasp:GridTemplateControl>
        </Columns>
    </MasterTableView>

    <ItemStyle BackColor="Transparent"></ItemStyle>
    <HeaderContextMenu Enabled="false"></HeaderContextMenu>

</wasp:WLPGrid>

<%--<script type="text/javascript" language="javascript">
//function grid_OnGridCreated() {
//    var gridElement = document.getElementById('OrganizationUnitGrid');
//    if (gridElement != null)
//        gridElement.tabIndex = -1;
//}   
</script>--%>