﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeePositionLabourDistributionControl.ascx.cs" Inherits="WorkLinks.HumanResources.EmployeePosition.EmployeePositionLabourDistributionControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<wasp:WLPGrid
    ID="EmployeePositionLabourDistributionGrid"
    runat="server"
    AutoAssignModifyProperties="true"
    AutoGenerateColumns="false"
    GridLines="None"
    OnUpdateCommand="EmployeePositionLabourDistributionGrid_UpdateCommand"
    OnInsertCommand="EmployeePositionLabourDistributionGrid_InsertCommand"
    OnDeleteCommand="EmployeePositionLabourDistributionGrid_DeleteCommand"
    OnNeedDataSource="EmployeePositionLabourDistributionGrid_NeedDataSource">

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
        <RowIndicatorColumn>
            <HeaderStyle Width="20px" />
        </RowIndicatorColumn>

        <CommandItemTemplate>
            <wasp:WLPToolBar ID="EmployeePositionLabourDistributionControlToolBar" runat="server" AutoPostBack="true" Width="100%">
                <Items>
                    <wasp:WLPToolBarButton CommandName="InitInsert" ResourceName="Add" ImageUrl="~/App_Themes/Default/Add.gif" Text="Add" Visible="<%# IsViewMode && AddFlag && EditButtonEnabled %>" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <ExpandCollapseColumn>
            <HeaderStyle Width="20px" />
        </ExpandCollapseColumn>

        <Columns>
            <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
            <wasp:GridBoundControl DataField="Description" LabelText="*Description" SortExpression="Description" UniqueName="Description" ResourceName="Description" />
            <wasp:GridBoundControl DataField="Percentage" LabelText="*Percentage" SortExpression="Percentage" UniqueName="Percentage" ResourceName="Percentage" />
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            <div class="form_control">
                                <span class="label">
                                    <wasp:WLPLabel ID="OrganizationUnitLabel" runat="server" Text="**OrganizationUnit**" ResourceName="OrganizationUnit" />
                                </span>
                                <span class="field">
                                    <telerik:RadDropDownTree ID="OrganizationUnitDropDownTree" ClientIDMode="Static" runat="server" DataTextField="Description" DataFieldID="OrganizationUnitId" DataFieldParentID="ParentOrganizationUnitId" TextMode="FullPath" DataValueField="OrganizationUnitId" OnClientEntryAdding="OnClientEntryAdding" OnClientLoad="onClientLoad" OnClientClearButtonClicking="onClientClearButtonClicking">
                                        <DropDownSettings Height="200px" Width="340px" CloseDropDownOnSelection="true" />
                                        <ButtonSettings ShowClear="true" />
                                    </telerik:RadDropDownTree>
                                    <asp:HiddenField ID="OrganizationUnitDropDownValues" ClientIDMode="Static" runat="server" Value="" />
                                </span>  
                            </div>
                        </td>
                        <td>
                            <wasp:NumericControl ID="Percentage" runat="server" DecimalDigits="2" MinValue="0" MaxValue="100.00" ResourceName="Percentage" Value='<%# Bind("Percentage") %>' ReadOnly="false" Mandatory="true" TabIndex="020" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsEditMode %>' ResourceName="Update" />
                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# !IsEditMode %>' ResourceName="Insert" />
                                    </td>
                                    <td>
                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>
    </MasterTableView>

</wasp:WLPGrid>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function OnClientEntryAdding(sender, args) {
            var node = args.get_node();

            //get count of child nodes of the selected node, if it has children, don't allow it to be selected
            if (node.get_allNodes().length > 0) {
                args.set_cancel(true);
            }
            else {
                var parentNode = node;
                //get id of the node selected
                var ids = [node.get_value()];

                while (parentNode.get_level() > 0) {
                    parentNode = parentNode.get_parent();
                    //get parent node id 
                    ids.push(parentNode.get_value());
                }
                var hiddenOrgControl = document.getElementById('OrganizationUnitDropDownValues');
                //reverse the array so it's parent->child order
                hiddenOrgControl.value = ids.reverse();
            }
        }

        function onClientLoad(sender, args) {
            var hiddenOrgControl = document.getElementById('OrganizationUnitDropDownValues');
            var orgUnits = hiddenOrgControl.value.split(",");
            var currentNode = sender.get_embeddedTree();
            var orgUnitPathTextDescription = "";

            currentNode = currentNode.findNodeByValue(orgUnits[0]);

            if (currentNode != null) {
                currentNode.expand();
                currentNode.select();
                //get text of parent node
                orgUnitPathTextDescription = currentNode.get_text();

                for (var i = 1; i < orgUnits.length; i++) {
                    for (var j = 0; j < currentNode._children.get_count(); j++) {
                        if (currentNode._children._array[j].get_value() == orgUnits[i]) {
                            currentNode = currentNode._children._array[j];
                            currentNode.expand();
                            currentNode.select();
                            //get text of child node
                            orgUnitPathTextDescription += '/' + currentNode.get_text();

                            break;
                        }
                    }
                }
            }

            //set the path as the default message for the RadDropDownTree
            if (orgUnitPathTextDescription != null)
                sender._defaultMessage = orgUnitPathTextDescription;
        }

        function onClientClearButtonClicking(sender, args) {
            //clear the default message
            sender._defaultMessage = '<%= DropDownTreeDefaultMessage %>';

            var hiddenOrgControl = document.getElementById('OrganizationUnitDropDownValues');
            var orgUnits = hiddenOrgControl.value.split(",");

            var currentNode = sender.get_embeddedTree();
            currentNode = currentNode.findNodeByValue(orgUnits[0]);

            //collapse all nodes in the path that were previously expanded
            if (currentNode != null) {
                currentNode.collapse();

                for (var i = 1; i < orgUnits.length; i++) {
                    for (var j = 0; j < currentNode._children.get_count() ; j++) {
                        if (currentNode._children._array[j].get_value() == orgUnits[i]) {
                            currentNode = currentNode._children._array[j];
                            currentNode.collapse();
                            break;
                        }
                    }
                }
            }

            //set org units to nothing
            hiddenOrgControl.value = null;
        }
    </script>
</telerik:RadScriptBlock>