﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Payroll.PayrollBatch;

namespace WorkLinks.HumanResources.EmployeePosition
{
    public delegate void FormViewCommandEventArgs(object sender, System.Web.UI.WebControls.FormViewCommandEventArgs e);
    public delegate void SelectedDateChangedEventArgs(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e);

    public partial class EmployeePositionViewControl : Wizard.WizardUserControl
    {
        public event FormViewCommandEventArgs ItemCommand;
        public event SelectedDateChangedEventArgs SelectedDateChanged;

        #region fields
        private BusinessLayer.BusinessObjects.EmployeePosition _insertCache;
        private const String _insertCacheKey = "_insertCacheKey";
        private bool _updateExterallyControlled = false;
        private CodeCollection _daysOfWeek = null;
        #endregion

        #region properties

        //security option to allow a user to edit employee position 
        public bool OverrideEditButtonSecurity { get { return Common.Security.RoleForm.EmployeePositionOverrideEditButtonSecurity.ViewFlag; } }

        protected bool CurrentPayrollPendingFlag
        {
            get
            {
                PayrollProcessCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetPayrollProcess(new PayrollProcessCriteria() { PayrollProcessId = null, GetLastOpenPayrollProcessFlag = true });

                if (collection != null && collection.Count > 0)
                {
                    if (collection[0].PayrollProcessStatusCode == "CP")
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
        }

        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeePosition.UpdateFlag; } }
        public bool IsViewMode { get { return EmployeePositionView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool IsInsertMode { get { return InsertCache != null; } }
        public bool IsEditMode
        {
            get
            {
                if (EnableWizardFunctionalityFlag)
                    return false;
                else
                    return EmployeePositionView.CurrentMode.Equals(FormViewMode.Edit) && InsertCache == null;
            }
        }
        protected DateTime MinAddEffectiveDate
        {
            get
            {
                if (LastEmployeePosition != null && LastEmployeePosition.EffectiveDate != null)
                    return (DateTime)LastEmployeePosition.EffectiveDate;
                else
                    return DateTime.MinValue;
            }
        }
        protected bool EffectiveDateEnabled
        {
            get
            {
                BusinessLayer.BusinessObjects.EmployeePosition selectedEmployeePosition = SelectedEmployeePosition;

                //only allow effective date changes on the last record
                if (selectedEmployeePosition.PayrollProcessId == null && selectedEmployeePosition.NextEmployeePositionId == null)
                    return false;
                else
                    return true;
            }
        }
        protected bool EditButtonEnabled
        {
            get
            {
                //Security option to allow a user to edit employee position.  
                //Only exceptions are to not to allow edit on a record with payroll_process_id !=null and a calc is in progress (calc status=CP) or when the ACTION = "RHRESET"
                if (OverrideEditButtonSecurity && SelectedEmployeePosition.EmployeePositionActionCode != "RHRESET")
                {
                    if (CurrentPayrollPendingFlag && SelectedEmployeePosition.PayrollProcessId != null)
                        return false;
                    else
                        return true;
                }
                else
                {
                    BusinessLayer.BusinessObjects.EmployeePosition selectedEmployeePosition = SelectedEmployeePosition;

                    if (selectedEmployeePosition != null && selectedEmployeePosition.PayrollProcessId == null && selectedEmployeePosition.EmployeePositionActionCode != "RHRESET")
                    {
                        //any current or future dated records where the Event is Leave, Long Term Disability, or Terminate and the status is also Leave, Long Term Disability, or Terminated must have the edit button disabled
                        if ((selectedEmployeePosition.EmployeePositionActionCode == "LEAV" && selectedEmployeePosition.EmployeePositionStatusCode == "L") ||
                            (selectedEmployeePosition.EmployeePositionActionCode == "TERM" && selectedEmployeePosition.EmployeePositionStatusCode == "TER") ||
                            (selectedEmployeePosition.EmployeePositionActionCode == "LD0" && selectedEmployeePosition.EmployeePositionStatusCode == "LD0"))
                            return false;
                        else
                            return true;
                    }
                    else
                        return false;
                }
            }
        }
        private String PayrollProcessGroupCode { get { return Page.RouteData.Values["payrollProcessGroupCode"].ToString(); } }
        protected long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        protected bool IsDateVisible { get { return EmployeeId > 0; } }
        public BusinessLayer.BusinessObjects.EmployeePosition InsertCache
        {
            get
            {
                if (_insertCache == null)
                    _insertCache = (BusinessLayer.BusinessObjects.EmployeePosition)ViewState[_insertCacheKey];

                return _insertCache;
            }
            set
            {
                _insertCache = value;
                ViewState[_insertCacheKey] = _insertCache;
            }
        }
        public BusinessLayer.BusinessObjects.EmployeePosition LastEmployeePosition { get; set; }
        private BusinessLayer.BusinessObjects.EmployeePosition SelectedEmployeePosition
        {
            get
            {
                if (DataItemCollection.Count < 1)
                    ((EmployeePositionCollection)DataItemCollection).Add(new BusinessLayer.BusinessObjects.EmployeePosition() { EmployeeId = EmployeeId });

                return (BusinessLayer.BusinessObjects.EmployeePosition)DataItemCollection[0];
            }
        }
        public DateTime PayrollPeriodCutoffDate { get; set; }
        public EmployeePositionCollection CurrentEmployeePositionCollection
        {
            get { return (EmployeePositionCollection)DataItemCollection; }
            set { DataItemCollection = value; }
        }
        public CodeCollection CheckedWorkdays;
        protected CodeCollection DaysOfWeek
        {
            get
            {
                if (_daysOfWeek == null)
                    _daysOfWeek = Common.CodeHelper.GetCodeTable(LanguageCode, CodeTableType.EmployeePositionDayOfWeekCode);

                return _daysOfWeek;
            }
        }
        #endregion

        #region main
        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (IsDataExternallyLoaded)
                    EmployeePositionView.DataBind();
            }

            WireEvents();
        }
        private void WireEvents()
        {
            EmployeePositionView.Updating += new WLPFormView.ItemUpdatingEventHandler(EmployeePositionView_Updating);
            EmployeePositionView.ItemCommand += new FormViewCommandEventHandler(EmployeePositionView_ItemCommand);
            EmployeePositionView.PreRender += new EventHandler(EmployeePositionView_PreRender);

            OrganizationUnitControl.OrganizationUnitChanged += OrganizationUnitControl_OrganizationUnitChanged;
        }
        protected internal void ChangeMode(FormViewMode formViewMode)
        {
            EmployeePositionView.ChangeMode(formViewMode);
        }
        #endregion

        #region event handlers
        private void OrganizationUnitControl_OrganizationUnitChanged(object sender, OrganizationUnitChangedEventArgs e)
        {
            WLPFormView item = (WLPFormView)((OrganizationUnitControl)sender).NamingContainer;
            ComboBoxControl salaryPlan = (ComboBoxControl)item.FindControl("SalaryPlanId");
            ComboBoxControl salaryPlanGradeId = (ComboBoxControl)item.FindControl("SalaryPlanGradeId");

            if (salaryPlan != null)
            {
                salaryPlan.Value = e.SalaryPlanId;
                BindGradeCombo(sender);
            }

            if (salaryPlanGradeId != null)
            {
                salaryPlanGradeId.Value = e.SalaryPlanGradeId;
                BindStepCombo(sender);
            }
        }
        protected void SalaryPlan_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ComboBoxControl control = (ComboBoxControl)sender;

            if (control.ID.Equals("SalaryPlanId"))
                BindGradeCombo(sender);
            else if (control.ID.Equals("SalaryPlanGradeId"))
                BindStepCombo(sender);
        }
        protected void SalaryPlanId_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateComboBoxWithSalaryPlan((ComboBoxControl)sender, LanguageCode);
            BindGradeCombo(sender);
        }
        private void BindGradeCombo(object sender)
        {
            WLPFormView item = null;
            ComboBoxControl salaryPlanCombo = null;

            if (sender is ComboBoxControl)
            {
                salaryPlanCombo = (ComboBoxControl)sender;
                item = (WLPFormView)salaryPlanCombo.BindingContainer;
            }
            else if (sender is OrganizationUnitControl)
            {
                item = (WLPFormView)((OrganizationUnitControl)sender).NamingContainer;
                salaryPlanCombo = (ComboBoxControl)item.FindControl("SalaryPlanId");
            }

            ComboBoxControl gradeCombo = (ComboBoxControl)item.FindControl("SalaryPlanGradeId");
            if (gradeCombo != null)
            {
                SetSalaryPlanGrade(item.DataItem, salaryPlanCombo, gradeCombo);

                ComboBoxControl stepCombo = (ComboBoxControl)item.FindControl("SalaryPlanGradeStepId");
                if (stepCombo != null)
                    SetSalaryPlanGradeStep(item.DataItem, gradeCombo, stepCombo);
            }
        }
        private void BindStepCombo(object sender)
        {
            WLPFormView item = null;
            ComboBoxControl gradeCombo = null;

            if (sender is ComboBoxControl)
            {
                gradeCombo = (ComboBoxControl)sender;
                item = (WLPFormView)gradeCombo.BindingContainer;
            }
            else if (sender is OrganizationUnitControl)
            {
                item = (WLPFormView)((OrganizationUnitControl)sender).NamingContainer;
                gradeCombo = (ComboBoxControl)item.FindControl("SalaryPlanGradeId");
            }

            ComboBoxControl stepCombo = (ComboBoxControl)item.FindControl("SalaryPlanGradeStepId");
            if (stepCombo != null)
                SetSalaryPlanGradeStep(item.DataItem, gradeCombo, stepCombo);
        }
        private void SetSalaryPlanGrade(IDataItem item, ComboBoxControl salaryPlanCombo, ComboBoxControl gradeCombo)
        {
            //have to set the value of the grade combo here because it's not picked up on the ASP side
            if (item != null && item is BusinessLayer.BusinessObjects.EmployeePosition)
                gradeCombo.Value = ((BusinessLayer.BusinessObjects.EmployeePosition)item).SalaryPlanGradeId;
            else
                gradeCombo.Value = null;

            Common.CodeHelper.PopulateComboBoxWithSalaryPlanGrade(gradeCombo, salaryPlanCombo.SelectedValue != null ? Convert.ToInt64(salaryPlanCombo.SelectedValue) : -1, LanguageCode);
            gradeCombo.DataBind();
        }
        private void SetSalaryPlanGradeStep(IDataItem item, ComboBoxControl gradeCombo, ComboBoxControl stepCombo)
        {
            //have to set the value of the step combo here because it's not picked up on the ASP side
            if (item != null && item is BusinessLayer.BusinessObjects.EmployeePosition)
                stepCombo.Value = ((BusinessLayer.BusinessObjects.EmployeePosition)item).SalaryPlanGradeStepId;
            else
                stepCombo.Value = null;

            //make the step combo mandatory if the grade combo is not null
            stepCombo.Mandatory = gradeCombo.Value != null && stepCombo.Value == null;

            Common.CodeHelper.PopulateComboBoxWithSalaryPlanGradeStep(stepCombo, gradeCombo.SelectedValue != null ? Convert.ToInt64(gradeCombo.SelectedValue) : -1, LanguageCode);
            stepCombo.DataBind();
        }
        protected void EmployeePositionView_PreRender(object sender, EventArgs e)
        {
            //make the employee control not mandatory
            EmployeeControl employeeControl = (EmployeeControl)EmployeePositionView.FindControl("EmployeeControl");
            if (employeeControl != null)
            {
                TextBoxControl textBoxControl = (TextBoxControl)employeeControl.FindControl("EmployeeNumber");
                if (textBoxControl != null)
                    textBoxControl.Mandatory = false;
            }
        }
        protected void EmployeePositionView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            if (!IsDataExternallyLoaded)
                OnNeedDataSource(e);

            EmployeePositionView.DataSource = CurrentEmployeePositionCollection;
        }
        protected void EmployeePositionView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            BusinessLayer.BusinessObjects.EmployeePosition employeePosition = (BusinessLayer.BusinessObjects.EmployeePosition)e.DataItem;
            CheckedListBoxControl days = (CheckedListBoxControl)EmployeePositionView.FindControl("Workdays");
            EmployeePositionWorkdayCollection workdays = new EmployeePositionWorkdayCollection();

            if (_updateExterallyControlled)
            {
                for (int i = 1; i <= 7; i++)
                {
                    workdays.Add(new EmployeePositionWorkday()
                    {
                        EmployeePositionWorkdayId = -(i + 1),
                        EmployeePositionId = employeePosition.EmployeePositionId,
                        DayOfWeek = Convert.ToString(i)
                    });
                }
            }
            else
                workdays = employeePosition.Workdays;

            if (days != null)
            {
                //set the IsWorkDayFlag to the days of the week to false
                foreach (EmployeePositionWorkday workday in workdays)
                    workday.IsWorkdayFlag = false;

                //add the checked workdays
                foreach (CodeObject checkedDay in (CodeCollection)days.Value)
                {
                    foreach (EmployeePositionWorkday workday in workdays)
                    {
                        if (workday.DayOfWeek == checkedDay.Code)
                        {
                            workday.IsWorkdayFlag = true;
                            break;
                        }
                    }
                }
            }

            //if the reports to employee id is -1, set it to null
            if (employeePosition.ReportsToEmployeeId == -1)
                employeePosition.ReportsToEmployeeId = null;

            if (_updateExterallyControlled)
            {
                employeePosition.Workdays = workdays;
                employeePosition.CopyTo(SelectedEmployeePosition);
                e.Cancel = true;
            }
            else
                OnUpdating(e);

            EmployeePositionView.DataBind();

            //reload the alternate labour tab if it exists (it wont on the employee hire wizard)
            EmployeePositionAlternateLabourControl alternateLabourControl = (EmployeePositionAlternateLabourControl)EmployeePositionView.NamingContainer.NamingContainer.FindControl("EmployeePositionAlternateLabourControl");
            if (alternateLabourControl != null)
                alternateLabourControl.RefreshAlternateLabour();
        }
        protected void EmployeePositionView_ItemCommand(object sender, System.Web.UI.WebControls.FormViewCommandEventArgs e)
        {
            //set InsertCache to null in Parent
            if (e.CommandName.ToLower().Equals("cancel"))
                OnItemCommand(sender, e);
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void EVENTCode_NeedDataSource(object sender, EventArgs e)
        {
            String currentEmployeePositionStatusCode = null;
            String currentEmployeePositionActionCode = null;

            if (EmployeeId > 0) //if true, employee exists
            {
                currentEmployeePositionStatusCode = CurrentEmployeePositionCollection[0].EmployeePositionStatusCode;
                currentEmployeePositionActionCode = CurrentEmployeePositionCollection[0].EmployeePositionActionCode;
            }

            Common.CodeHelper.PopulateComboWithEvent((ICodeControl)sender, LanguageCode, currentEmployeePositionStatusCode, currentEmployeePositionActionCode, IsEditMode);
        }
        protected void REASONCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateComboWithReason((ICodeControl)sender, LanguageCode, CurrentEmployeePositionCollection[0].EmployeePositionActionCode);
        }
        protected void BenefitPolicy_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateComboBoxWithBenefitPolicy((ComboBoxControl)sender, LanguageCode);
        }
        protected void LabourUnion_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateComboBoxWithUnion((ComboBoxControl)sender, LanguageCode);
        }
        protected void EmployeePositionDayOfWeekCode_NeedDataSource(object sender, EventArgs e)
        {
            CodeCollection dayOfWeekCodes = DaysOfWeek;
            CheckedWorkdays = new CodeCollection();

            if (!IsDataExternallyLoaded)
            {
                //check the checkboxes for days of the week that have the IsWorkdayFlag set to true
                foreach (CodeObject code in dayOfWeekCodes)
                {
                    foreach (EmployeePositionWorkday workday in SelectedEmployeePosition.Workdays)
                    {
                        if (workday.DayOfWeek == code.Code)
                        {
                            if (workday.IsWorkdayFlag)
                                CheckedWorkdays.Add(new CodeObject() { Code = code.Code, Description = code.Description });

                            break;
                        }
                    }
                }
            }
            else
            {
                BusinessLayer.BusinessObjects.EmployeePosition employeePosition = ((BusinessLayer.BusinessObjects.EmployeePosition)EmployeePositionView.DataItem);
                EmployeePositionWorkdayCollection workdays = employeePosition.Workdays;

                if (workdays != null && workdays.Count > 0)
                {
                    //add the checked workdays from the wizard cache
                    for (int i = 0; i < employeePosition.Workdays.Count; i++)
                    {
                        if (workdays[i].IsWorkdayFlag)
                            CheckedWorkdays.Add(new CodeObject() { Code = dayOfWeekCodes[i].Code, Description = dayOfWeekCodes[i].Description });
                    }
                }
                else
                {
                    //add the checked workdays from the code system
                    Char[] defaultWorkdays = Common.ApplicationParameter.DefaultWorkday.ToCharArray();

                    for (int i = 0; i < defaultWorkdays.Length; i++)
                    {
                        if (defaultWorkdays[i].ToString() == "1")
                            CheckedWorkdays.Add(new CodeObject() { Code = dayOfWeekCodes[i].Code, Description = dayOfWeekCodes[i].Description });
                    }
                }
            }

            ((ICodeControl)sender).DataSource = dayOfWeekCodes;
            ((CheckedListBoxControl)sender).Value = CheckedWorkdays;
        }
        protected void EmployeePositionAction_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ComboBoxControl control = (ComboBoxControl)o;
            ComboBoxControl statusCombo = (ComboBoxControl)control.BindingContainer.FindControl("EmployeePositionStatusCode"); //find status combo

            if (control.Value != null)
            {
                //if moving from TERminated, Leave, or Long Term Disability status into ReHire, update the combo for Status to contain ACtive
                if ((CurrentEmployeePositionCollection[0].EmployeePositionStatusCode == "TER" || CurrentEmployeePositionCollection[0].EmployeePositionStatusCode == "L" ||
                    CurrentEmployeePositionCollection[0].EmployeePositionStatusCode == "LD0") && (control.Value.ToString() == "RH" || control.Value.ToString() == "RHRESET"))
                {
                    //populate with "Active" status only
                    Common.CodeHelper.PopulateComboWithStatus(statusCombo, LanguageCode);

                    //select the "Active" status for display
                    statusCombo.Value = "AC";

                    if (control.Value.ToString() == "RHRESET")
                    {
                        ComboBoxControl combo = (ComboBoxControl)control.BindingContainer.FindControl("SalaryPlanGradeStepId");
                        if (combo != null)
                        {
                            //select the first item
                            combo.SelectedIndex = 1;
                            //trigger the client side javascript function
                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "setRate", string.Format("setRate(null,null,'{0}');", combo.SelectedValue), true);
                        }
                    }
                }
                else
                {
                    string statusToDisplay = "";

                    //populate the status based on the event/action chosen
                    Common.CodeHelper.PopulateControlWithStatusBaseOnEvent(statusCombo, LanguageCode, control, SelectedEmployeePosition.EmployeePositionStatusCode, out statusToDisplay);

                    //select the last used status for display
                    statusCombo.Value = statusToDisplay;
                }

                //update the reason combo to contain reasons associated to the Action/Event
                ComboBoxControl reasonCombo = (ComboBoxControl)control.BindingContainer.FindControl("EmployeePositionReasonCode");
                Common.CodeHelper.PopulateComboWithReason(reasonCombo, LanguageCode, e.Value);
                reasonCombo.DataBind();
            }
            else
                statusCombo.Value = null;
        }
        protected void SetCompensationAmountLabel(ComboBoxControl control)
        {
            NumericControl compensationAmount = (NumericControl)control.BindingContainer.FindControl("CompensationAmount");

            if (compensationAmount == null)
                compensationAmount = (NumericControl)control.BindingContainer.FindControl("CompensationAmountItem");

            if (control.Value != null)
            {
                if (control.Value.ToString().ToLower() == "h")
                    compensationAmount.Text = String.Format("{0}", GetGlobalResourceObject("PageContent", "HourlyRate"));
                else
                    compensationAmount.Text = String.Format("{0}", GetGlobalResourceObject("PageContent", "CompensationAmount"));
            }
            else
                compensationAmount.Text = String.Format("{0}", GetGlobalResourceObject("PageContent", "CompensationAmount"));
        }
        protected void PaymentMethodCode_OnPreRender(object sender, EventArgs e)
        {
            SetCompensationAmountLabel((ComboBoxControl)sender);
        }
        protected void PaymentMethodCode_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            SetCompensationAmountLabel((ComboBoxControl)sender);
        }
        protected void EffectiveDate_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            if (!IsDataExternallyLoaded)
            {
                OnSelectedDateChanged(sender, e);
                EmployeePositionView.DataBind();
            }
            else
            {
                SelectedEmployeePosition.EffectiveDate = e.NewDate;
                EmployeePositionView.DataBind();
            }
        }
        protected virtual void OnItemCommand(object sender, System.Web.UI.WebControls.FormViewCommandEventArgs e)
        {
            if (ItemCommand != null)
                ItemCommand(sender, e);
        }
        protected virtual void OnSelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            if (SelectedDateChanged != null)
                SelectedDateChanged(sender, e);
        }
        protected void EmployeePositionAction_DataBound(object sender, EventArgs e)
        {
            if (IsDataExternallyLoaded)
            {
                //set the Action code to New Hire and the control to readonly
                ((ComboBoxControl)sender).SelectedIndex = ((ComboBoxControl)sender).FindItemByValue("NEW").Index;
                ((ComboBoxControl)sender).ReadOnly = true;
                ((EmployeePositionCollection)EmployeePositionView.DataSource)[0].EmployeePositionStatusCode = "AC";
            }
            else if (SelectedEmployeePosition != null && SelectedEmployeePosition.EmployeePositionActionCode == "NEW")
                ((ComboBoxControl)sender).ReadOnly = true;

            DateControl effDate = (DateControl)EmployeePositionView.FindControl("EffectiveDate");

            if (!effDate.ReadOnly)
                effDate.Focus();
            else
                ((ComboBoxControl)EmployeePositionView.FindControl("EmployeePositionReasonCode")).Focus();
        }
        protected void ReportsToEmployeeId_DataBinding(object sender, EventArgs e)
        {
            PopulateEmployeeControl((ICodeControl)sender);
        }
        protected void PopulateEmployeeControl(ICodeControl control)
        {
            Common.CodeHelper.PopulateEmployeeControl(control, false);
        }
        #endregion

        #region override
        public override void Update(bool updateExterallyControlled)
        {
            _updateExterallyControlled = updateExterallyControlled;
            EmployeePositionView.UpdateItem(true);
        }
        public override void AddNewDataItem()
        {
            if (DataItemCollection == null)
            {
                DataItemCollection = new EmployeePositionCollection();
                DataItemCollection.AddNew();
                BusinessLayer.BusinessObjects.EmployeePosition addedPosition = ((BusinessLayer.BusinessObjects.EmployeePosition)DataItemCollection[0]);
                addedPosition.EmployeePositionOrganizationUnits = Common.ServiceWrapper.EmployeePosition.SelectNewEmployeePositionOrganizationUnitLevelSummary();
            }
        }
        public override void ChangeModeEdit()
        {
            if (!EmployeePositionView.CurrentMode.Equals(FormViewMode.Edit))
                ChangeMode(FormViewMode.Edit);
        }
        #endregion
    }
}