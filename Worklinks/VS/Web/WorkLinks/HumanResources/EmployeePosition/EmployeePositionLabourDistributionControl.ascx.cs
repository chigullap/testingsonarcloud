﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.EmployeePosition
{
    public partial class EmployeePositionLabourDistributionControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private long _employeePositionId = -1;
        private bool _editButtonEnabled = false;
        private const String _editButtonEnabledKey = "EditButtonEnabledKey";
        #endregion

        #region properties
        public bool EditButtonEnabled
        {
            get
            {
                if (_editButtonEnabled == false)
                {
                    Object obj = ViewState[_editButtonEnabledKey];
                    if (obj != null)
                        _editButtonEnabled = Convert.ToBoolean(obj);
                }

                return _editButtonEnabled;
            }
            set
            {
                _editButtonEnabled = value;
                ViewState[_editButtonEnabledKey] = _editButtonEnabled;
            }
        }
        public long EmployeePositionId
        {
            get { return _employeePositionId; }
            set { _employeePositionId = value; }
        }
        public bool IsEditMode { get { return EmployeePositionLabourDistributionGrid.IsEditMode; } }
        public bool IsInsertMode { get { return EmployeePositionLabourDistributionGrid.IsInsertMode; } }
        public bool IsViewMode { get { return EmployeePositionLabourDistributionGrid.IsViewMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.EmployeePositionLabourDistrubution.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeePositionLabourDistrubution.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.EmployeePositionLabourDistrubution.DeleteFlag; } }
        public String DropDownTreeDefaultMessage { get { return String.Format(GetGlobalResourceObject("PageContent", "ChooseADepartment").ToString()); } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();

            if (!IsPostBack)
                LoadEmployeeLabourDistribution();
        }
        private void WireEvents()
        {
            EmployeePositionLabourDistributionGrid.ItemDataBound += EmployeePositionLabourDistributionGrid_ItemDataBound;
        }
        public void LoadEmployeeLabourDistribution()
        {
            Data = Common.ServiceWrapper.HumanResourcesClient.GetEmployeePositionLabourDistribution(EmployeePositionId, LanguageCode);
        }
        public void RefreshLabourDistrubution()
        {
            LoadEmployeeLabourDistribution();
            EmployeePositionLabourDistributionGrid.Rebind();
        }
        #endregion

        #region event handlers
        protected void EmployeePositionLabourDistributionGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EmployeePositionLabourDistributionGrid.DataSource = Data;
        }
        void EmployeePositionLabourDistributionGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem) //hide the edit/delete images in the rows and do not allow the first row to be edited or deleted
            {
                GridDataItem item = (GridDataItem)e.Item;
                ImageButton editButton = (ImageButton)item["editButton"].Controls[0];
                ImageButton deleteButton = (ImageButton)item["deleteButton"].Controls[0];

                editButton.Visible = UpdateFlag && EditButtonEnabled && !(item.ItemIndex == 0);
                deleteButton.Visible = DeleteFlag && EditButtonEnabled && !(item.ItemIndex == 0);
            }
            else if (e.Item is GridEditFormItem && e.Item.IsInEditMode) //if in edit mode, show the RadDropDownTree and select the associated department
            {
                GridEditFormItem item = (GridEditFormItem)e.Item;
                RadDropDownTree organizationUnitDropDownTree = (RadDropDownTree)item.FindControl("OrganizationUnitDropDownTree");

                if (organizationUnitDropDownTree != null)
                {
                    organizationUnitDropDownTree.DataSource = Common.ServiceWrapper.HumanResourcesClient.GetOrganizationUnitAssociationTree(LanguageCode, null);
                    organizationUnitDropDownTree.DefaultMessage = DropDownTreeDefaultMessage;
                    organizationUnitDropDownTree.DataBind();

                    if (!IsInsertMode && !IsViewMode)
                    {
                        EmployeePositionLabourDistributionSummary labourDist = (EmployeePositionLabourDistributionSummary)item.DataItem;

                        //asp hidden field to hold org hierarchy
                        HiddenField orgHierarchy = (HiddenField)item.FindControl("OrganizationUnitDropDownValues");
                        if (orgHierarchy != null)
                            orgHierarchy.Value = labourDist.OrganizationUnit;
                    }
                }

                //set max pecentage value as all rows must be <= 100%
                NumericControl percent = (NumericControl)item.FindControl("Percentage");
                percent.MaxValue = Convert.ToDouble(((EmployeePositionLabourDistributionSummary)Data[0]).Percentage);
            }
        }
        #endregion

        #region handle updates
        protected void EmployeePositionLabourDistributionGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                EmployeePositionLabourDistributionSummary item = (EmployeePositionLabourDistributionSummary)e.Item.DataItem;

                HiddenField hiddenOrg = (HiddenField)e.Item.FindControl("OrganizationUnitDropDownValues");
                if (hiddenOrg != null)
                    item.OrganizationUnit = hiddenOrg.Value;

                Common.ServiceWrapper.HumanResourcesClient.UpdateEmployeePositionLabourDistribution(item);

                //reload screen
                EmployeePositionId = item.EmployeePositionId;
                LoadEmployeeLabourDistribution();
                EmployeePositionLabourDistributionGrid_NeedDataSource(null, null);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void EmployeePositionLabourDistributionGrid_InsertCommand(object sender, GridCommandEventArgs e)
        {
            EmployeePositionLabourDistributionSummary item = (EmployeePositionLabourDistributionSummary)e.Item.DataItem;
            item.EmployeePositionId = ((EmployeePositionLabourDistributionSummary)Data[0]).EmployeePositionId;

            HiddenField hiddenOrg = (HiddenField)e.Item.FindControl("OrganizationUnitDropDownValues");
            if (hiddenOrg != null)
                item.OrganizationUnit = hiddenOrg.Value;

            Common.ServiceWrapper.HumanResourcesClient.InsertEmployeePositionLabourDistribution(item);

            //reload screen
            EmployeePositionId = item.EmployeePositionId;
            LoadEmployeeLabourDistribution();
            EmployeePositionLabourDistributionGrid_NeedDataSource(null, null);
        }
        protected void EmployeePositionLabourDistributionGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                EmployeePositionLabourDistributionSummary summary = (EmployeePositionLabourDistributionSummary)e.Item.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.DeleteEmployeePositionLabourDistribution(summary);

                //reload screen
                EmployeePositionId = summary.EmployeePositionId;
                LoadEmployeeLabourDistribution();
                EmployeePositionLabourDistributionGrid_NeedDataSource(null, null);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}