﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeePositionViewControl.ascx.cs" Inherits="WorkLinks.HumanResources.EmployeePosition.EmployeePositionViewControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="OrganizationUnitControl.ascx" TagName="OrganizationUnitControl" TagPrefix="uc2" %>
<%@ Register Src="../../Payroll/PayrollBatch/EmployeeControl.ascx" TagName="EmployeeControl" TagPrefix="uc1" %>

<style type="text/css">
    .horizontalListbox .rlbItem { float: left !important; }
    .horizontalListbox .rlbGroup { border: none !important; }
    .horizontalListbox .rlbGroup, .RadListBox { width: auto !important; }
    .horizontalListbox .label { color: Gray !important; }
    .RadGrid_Silk .rgRow>td, .RadGrid_Silk .rgAltRow>td, .RadGrid_Silk .rgEditRow>td, .RadGrid_Silk .rgFooter>td { border-width: 0 !important; }
</style>

<wasp:WLPFormView ID="EmployeePositionView" runat="server" RenderOuterTable="false" DataKeyNames="Key" OnNeedDataSource="EmployeePositionView_NeedDataSource">
    <ItemTemplate>
        <wasp:WLPToolBar ID="EmployeeDetailToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Text="Edit" ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="edit" Visible='<%# IsViewMode && UpdateFlag %>' Enabled='<%# EditButtonEnabled %>' ResourceName="Edit" CausesValidation="false" />
            </Items>
        </wasp:WLPToolBar>

        <fieldset style="border-style: none; margin-top: -12px;">
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="EffectiveSequence" Text="**EffectiveSequence**" runat="server" ResourceName="EffectiveSequence" Value='<%# Eval("EffectiveSequence") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="EffectiveDate" Text="**EffectiveDate**" runat="server" ResourceName="EffectiveDate" Value='<%# Eval("EffectiveDate") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="EmployeePositionReasonCode" Text="**EmployeePositionReasonCode**" runat="server" Type="EmployeePositionReasonCode" OnDataBinding="REASONCode_NeedDataSource" ResourceName="EmployeePositionReasonCode" Value='<%# Eval("EmployeePositionReasonCode") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="EmployeePositionActionCode" Text="**EmployeePositionActionCode**" runat="server" Type="EmployeePositionActionCode" OnDataBinding="EVENTCode_NeedDataSource" ResourceName="EmployeePositionActionCode" Value='<%# Eval("EmployeePositionActionCode") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="EmployeePositionStatusCode" Text="**EmployeePositionStatusCode**" runat="server" Type="EmployeePositionStatusCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmployeePositionStatusCode" Value='<%# Eval("EmployeePositionStatusCode") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="margin-left: -8px;">
                            <uc2:OrganizationUnitControl ID="OrganizationUnitControl" runat="server" EmployeePositionOrganizationUnits='<%# Bind("EmployeePositionOrganizationUnits") %>' ReadOnly="true" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PermanencyTypeCode" Text="**PermanencyTypeCode**" runat="server" ResourceName="PermanencyTypeCode" OnDataBinding="Code_NeedDataSource" Type="PermanencyTypeCode" Value='<%# Eval("PermanencyTypeCode") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="EmploymentTypeCode" Text="**EmploymentTypeCode**" runat="server" ResourceName="EmploymentTypeCode" OnDataBinding="Code_NeedDataSource" Type="EmploymentTypeCode" Value='<%# Eval("EmploymentTypeCode") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="ShiftCode" Text="**ShiftCode**" runat="server" ResourceName="ShiftCode" OnDataBinding="Code_NeedDataSource" Type="ShiftCode" Value='<%# Eval("ShiftCode") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="SalaryPlanId" Text="**SalaryPlanId**" runat="server" ResourceName="AdminSalaryPlanCode" OnDataBinding="SalaryPlanId_NeedDataSource" Value='<%# Eval("SalaryPlanId") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="EmployeeTypeCode" Text="**EmployeeTypeCode**" runat="server" ResourceName="EmployeeTypeCode" OnDataBinding="Code_NeedDataSource" Type="EmployeeTypeCode" Value='<%# Eval("EmployeeTypeCode") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="SalaryPlanGradeId" Text="**SalaryPlanGradeId**" runat="server" ResourceName="GradeSalaryPlanCode" Value='<%# Eval("SalaryPlanGradeId") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PaymentMethodCode" Text="**PaymentMethodCode**" runat="server" ResourceName="PaymentMethodCode" OnPreRender="PaymentMethodCode_OnPreRender" OnSelectedIndexChanged="PaymentMethodCode_SelectedIndexChanged" OnDataBinding="Code_NeedDataSource" Type="PaymentMethodCode" Value='<%# Eval("PaymentMethodCode") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="SalaryPlanGradeStepId" Text="**SalaryPlanGradeStepId**" runat="server" ResourceName="StepSalaryPlanCode" Value='<%# Eval("SalaryPlanGradeStepId") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="StandardHours" Text="**StandardHours**" DecimalDigits="2" runat="server" ResourceName="StandardHours" Value='<%# Bind("StandardHours") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="CompensationAmount" Text="**CompensationAmount**" runat="server" ResourceName="CompensationAmount" DecimalDigits="4" Value='<%# Bind("CompensationAmount") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="RatePerHour" Text="**RatePerHour**" runat="server" ResourceName="RatePerHour" DecimalDigits="4" Value='<%# Bind("RatePerHour") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="AnnualSalary" Text="**AnnualSalary**" runat="server" ResourceName="AnnualSalary" DecimalDigits="2" Value='<%# Bind("AnnualSalary") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="BenefitPolicy" Text="**BenefitPolicy**" runat="server" ResourceName="BenefitPolicy" OnDataBinding="BenefitPolicy_NeedDataSource" Type="BenefitPolicy" Value='<%# Eval("BenefitPolicyId") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:DateControl ID="CreditedServiceDate" Text="**CreditedServiceDate**" runat="server" ResourceName="CreditedServiceDate" Value='<%# Eval("CreditedServiceDate") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="LabourUnion" Text="**LabourUnion**" runat="server" ResourceName="LabourUnion" OnDataBinding="LabourUnion_NeedDataSource" Type="LabourUnion" Value='<%# Eval("LabourUnionId") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:DateControl ID="UnionSeniorityDate" Text="**UnionSeniorityDate**" runat="server" ResourceName="UnionSeniorityDate" Value='<%# Eval("UnionSeniorityDate") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                       <wasp:NumericControl ID="SeniorityRank" runat="server" DecimalDigits="0" MinValue="0" MaxValue="2147483647" LabelText="**SeniorityRank**" ResourceName="SeniorityRank" Value='<%# Bind("SeniorityRank") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <div class="form_control">
                            <span class="label">
                                <wasp:WLPLabel ID="ReportsToEmployeeIdLabel" runat="server" Text="**ReportsToEmployeeId**" ResourceName="ReportsToEmployeeIdLabel" />
                            </span>
                            <span class="field">
                                <wasp:WLPKeyValue Width="180px" ID="ReportsToEmployeeId" runat="server" OnDataBinding="ReportsToEmployeeId_DataBinding" ResourceName="ReportsToEmployeeId" Value='<%# Eval("ReportsToEmployeeId") %>' ReadOnly="true" />
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="margin-left: 76px;">
                            <wasp:CheckedListBoxControl ID="Workdays" LabelText="**Workdays**" runat="server" CssClass="horizontalListbox" ResourceName="Workdays" Type="EmployeePositionDayOfWeekCode" OnDataBinding="EmployeePositionDayOfWeekCode_NeedDataSource" Value='<%# CheckedWorkdays %>' ReadOnly="true" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="CreateUser" Text="**CreateUser**" runat="server" ResourceName="CreateUser" Value='<%# Eval("CreateUser") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:DateControl ID="CreateDatetime" Text="**CreateDatetime**" runat="server" ResourceName="CreateDatetime" Value='<%# Eval("CreateDatetime") %>' ReadOnly="true" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar Visible='<%# !EnableWizardFunctionalityFlag %>' ID="EmployeDetailToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="toolBarClick">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="update" ResourceName="Insert" />
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update" />
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
            </Items>
        </wasp:WLPToolBar>

        <fieldset style="border-style: none; margin-top: -12px;">
            <table width="100%">
                <tr>
                    <td>
                        <wasp:DateControl ID="EffectiveDate" MinDate='<%# IsInsertMode?MinAddEffectiveDate:DateTime.MinValue %>' Text="**EffectiveDate**" runat="server" ResourceName="EffectiveDate" Value='<%# Bind("EffectiveDate") %>'  ReadOnly='<%# EffectiveDateEnabled %>' OnSelectedDateChanged="EffectiveDate_SelectedDateChanged" AutoPostback="true" TabIndex="010" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="EmployeePositionReasonCode" Text="**EmployeePositionReasonCode**" runat="server" Type="EmployeePositionReasonCode" OnDataBinding="REASONCode_NeedDataSource" ResourceName="EmployeePositionReasonCode" Value='<%# Bind("EmployeePositionReasonCode") %>' TabIndex="020" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="EmployeePositionActionCode" Text="**EmployeePositionActionCode**" runat="server" Type="EmployeePositionActionCode" AutoPostback="true" OnDataBinding="EVENTCode_NeedDataSource" ResourceName="EmployeePositionActionCode" Value='<%# Bind("EmployeePositionActionCode") %>' OnSelectedIndexChanged="EmployeePositionAction_SelectedIndexChanged" OnDataBound="EmployeePositionAction_DataBound" TabIndex="030" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="EmployeePositionStatusCode" Text="**EmployeePositionStatusCode**" runat="server" Type="EmployeePositionStatusCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmployeePositionStatusCode" Value='<%# Bind("EmployeePositionStatusCode") %>' ReadOnly="true" TabIndex="040" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="margin-left: -8px;">
                            <uc2:OrganizationUnitControl ID="EmployeePositionOrganizationUnits" runat="server" DefaultDate='<%# Bind("EffectiveDate") %>' EmployeePositionOrganizationUnits='<%# Bind("EmployeePositionOrganizationUnits") %>' TabIndex="050" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PermanencyTypeCode" Text="**PermanencyTypeCode**" runat="server" ResourceName="PermanencyTypeCode" OnDataBinding="Code_NeedDataSource" Type="PermanencyTypeCode" Value='<%# Bind("PermanencyTypeCode") %>' TabIndex="060" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="EmploymentTypeCode" Text="**EmploymentTypeCode**" runat="server" ResourceName="EmploymentTypeCode" OnDataBinding="Code_NeedDataSource" Type="EmploymentTypeCode" Value='<%# Bind("EmploymentTypeCode") %>' TabIndex="070" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="ShiftCode" Text="**ShiftCode**" runat="server" ResourceName="ShiftCode" OnDataBinding="Code_NeedDataSource" Type="ShiftCode" Value='<%# Bind("ShiftCode") %>' TabIndex="080" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="SalaryPlanId" Text="**SalaryPlanId**" runat="server" ResourceName="AdminSalaryPlanCode" OnDataBinding="SalaryPlanId_NeedDataSource" OnSelectedIndexChanged="SalaryPlan_SelectedIndexChanged" Value='<%# Bind("SalaryPlanId") %>' AutoPostback="true" TabIndex="090" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="EmployeeTypeCode" Text="**EmployeeTypeCode**" runat="server" ResourceName="EmployeeTypeCode" OnDataBinding="Code_NeedDataSource" Type="EmployeeTypeCode" Value='<%# Bind("EmployeeTypeCode") %>' TabIndex="100" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="SalaryPlanGradeId" Text="**SalaryPlanGradeId**" runat="server" ResourceName="GradeSalaryPlanCode" OnSelectedIndexChanged="SalaryPlan_SelectedIndexChanged" Value='<%# Bind("SalaryPlanGradeId") %>' AutoPostback="true" TabIndex="110" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PaymentMethodCode" Text="**PaymentMethodCode**" runat="server" ResourceName="PaymentMethodCode" OnPreRender="PaymentMethodCode_OnPreRender" OnSelectedIndexChanged="PaymentMethodCode_SelectedIndexChanged" OnDataBinding="Code_NeedDataSource" Type="PaymentMethodCode" AutoPostback="true" Value='<%# Bind("PaymentMethodCode") %>' TabIndex="120" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="SalaryPlanGradeStepId" ClientIDMode="Static" Text="**SalaryPlanGradeStepId**" runat="server" ResourceName="StepSalaryPlanCode" OnClientSelectedIndexChanged="setRate" Value='<%# Bind("SalaryPlanGradeStepId") %>' TabIndex="130" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="StandardHours" Text="**StandardHours**" runat="server" ResourceName="StandardHours" Value='<%# Bind("StandardHours") %>' TabIndex="140" />
                        <asp:RangeValidator ID="StandardHoursRange" runat="server" CultureInvariantValues="true" Display="Dynamic" ForeColor="Red" ControlToValidate="StandardHours" ResourceName="StandardHoursRange" Text='<%$ Resources:ErrorMessages, StandardHoursRange %>' MinimumValue="0.00" MaximumValue="9999.99" Type="Double" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="CompensationAmount" ClientIDMode="Static" Text="**CompensationAmount**" runat="server" ResourceName="CompensationAmount" DecimalDigits="4" Value='<%# Bind("CompensationAmount") %>' OnValueChanged="CompensationAmount_OnValueChanged" TabIndex="150" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <wasp:NumericControl ID="AnnualSalary" Visible='<%# IsDateVisible %>' Text="**AnnualSalary**" runat="server" ResourceName="AnnualSalary" DecimalDigits="2" Value='<%# Bind("AnnualSalary") %>' OnValueChanged="AnnualSalary_OnValueChanged" TabIndex="160" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="BenefitPolicy" Text="**BenefitPolicy**" runat="server" ResourceName="BenefitPolicy" OnDataBinding="BenefitPolicy_NeedDataSource" Type="BenefitPolicy" Value='<%# Bind("BenefitPolicyId") %>' TabIndex="170" />
                    </td>
                    <td>
                        <wasp:DateControl ID="CreditedServiceDate" Text="**CreditedServiceDate**" runat="server" ResourceName="CreditedServiceDate" Value='<%# Bind("CreditedServiceDate") %>' TabIndex="180" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="LabourUnion" Text="**LabourUnion**" runat="server" ResourceName="LabourUnion" OnDataBinding="LabourUnion_NeedDataSource" Type="LabourUnion" Value='<%# Bind("LabourUnionId") %>' TabIndex="190" />
                    </td>
                    <td>
                        <wasp:DateControl ID="UnionSeniorityDate" Text="**UnionSeniorityDate**" runat="server" ResourceName="UnionSeniorityDate" Value='<%# Bind("UnionSeniorityDate") %>' TabIndex="200" />
                    </td>
                </tr>
                <tr valign="top">
                    <td>
                       <wasp:NumericControl ID="SeniorityRank" runat="server" DecimalDigits="0" MinValue="0" MaxValue="2147483647" LabelText="**SeniorityRank**" ResourceName="SeniorityRank" Value='<%# Bind("SeniorityRank") %>' TabIndex="210" />
                    </td>
                    <td>
                        <div class="form_control">
                            <span class="label">
                                <wasp:WLPLabel ID="ReportsToEmployeeIdLabel" runat="server" Text="**ReportsToEmployeeId**" ResourceName="ReportsToEmployeeIdLabel" />
                            </span>
                            <span class="field">
                                <uc1:EmployeeControl ID="EmployeeControl" runat="server" Value='<%# Bind("ReportsToEmployeeId") %>' TabIndex="220" />
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="margin-left: 76px;">
                            <wasp:CheckedListBoxControl ID="Workdays" LabelText="**Workdays**" runat="server" CssClass="horizontalListbox" ResourceName="Workdays" Type="EmployeePositionDayOfWeekCode" OnDataBinding="EmployeePositionDayOfWeekCode_NeedDataSource" Value='<%# CheckedWorkdays %>' TabIndex="230" />
                        </div>
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>
</wasp:WLPFormView>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js" type="text/javascript"></script>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var _employeeId = <%= EmployeeId %>;

        function getRadWindow() {
            var popup = null;

            if (window.radWindow)
                popup = window.radWindow;
            else if (window.frameElement.radWindow)
                popup = window.frameElement.radWindow;
            else if (window.parent.window.frameElement.radWindow)
                popup = window.parent.window.frameElement.radWindow;

            return popup;
        }

        function processClick(commandName) {
            if (commandName.toLowerCase() == 'insert' || commandName.toLowerCase() == 'update') {
                var arg = new Object;

                if (_employeeId != null && _employeeId > 0) {
                    arg.closeWithChanges = true;
                    arg.employeeId = _employeeId;
                    getRadWindow().argument = arg;
                }
            }
        }

        function toolBarClick(sender, args) {
            var button = args.get_item();
            var commandName = button.get_commandName();
            processClick(commandName);
        }

        function setRate(source, args, idFromServerCode) {
            var compAmountControl = document.getElementById(document.getElementById('CompensationAmount').attributes['fieldClientId'].value);
            var stepControl = document.getElementById(document.getElementById('SalaryPlanGradeStepId').attributes['fieldClientId'].value);
            var salaryPlanGradeStepId;
            if (stepControl.control == undefined)
                salaryPlanGradeStepId = idFromServerCode;
            else
                salaryPlanGradeStepId = stepControl.control._value;
            
            $.ajax
            ({
                type: "POST",
                url: getUrl("/HumanResources/EmployeePosition/EmployeePositionPage.aspx/GetRate"),
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: "{'salaryPlanGradeStepId': '" + salaryPlanGradeStepId + "'}",
                success: function (msg) {
                    if (msg.d != null) {
                        compAmountControl.control.set_value(msg.d.Amount);
                    }
                }
            });
            
            return false;
        }

        function AnnualSalary_OnValueChanged(sender, eventArgs) {
            var annualSalaryControl = document.getElementById('<%= EmployeePositionView.FindControl("AnnualSalary").ClientID %>');
            var compensationAmountControl = document.getElementById('<%= EmployeePositionView.FindControl("CompensationAmount").ClientID %>');
            var paymentMethodCodeControl = document.getElementById('<%= EmployeePositionView.FindControl("PaymentMethodCode").ClientID %>');
            var standardHoursControl = document.getElementById('<%= EmployeePositionView.FindControl("StandardHours").ClientID %>');
            var periodsPerYear = <%= CurrentEmployeePositionCollection[0].PeriodsPerYear %>;

            if (annualSalaryControl != null && compensationAmountControl != null && paymentMethodCodeControl != null && standardHoursControl != null && periodsPerYear != 0) {
                var annualSalary = document.getElementById(annualSalaryControl.attributes['fieldClientId'].value);
                var compensationAmount = document.getElementById(compensationAmountControl.attributes['fieldClientId'].value);
                var paymentMethodCode = document.getElementById(paymentMethodCodeControl.attributes['fieldClientId'].value);
                var standardHours = document.getElementById(standardHoursControl.attributes['fieldClientId'].value);

                setCompensationAmount(annualSalary.value.replace(/,/g, ""), compensationAmount, paymentMethodCode, standardHours.value.replace(/,/g, ""), periodsPerYear);
            }
        }

        function setCompensationAmount(annualSalary, compensationAmount, paymentMethodCode, standardHours, periodsPerYear) {
            var amount = 0;

            if (paymentMethodCode.value != null) {
                if (paymentMethodCode.value.toLowerCase() == 'hourly')
                    amount = ((parseFloat(annualSalary) || 0) / parseFloat(standardHours)).toFixed(4) / parseFloat(periodsPerYear);
                else
                    amount = (parseFloat(annualSalary) || 0) / parseFloat(periodsPerYear);
            }
        
            compensationAmount.value = addCommas(amount.toFixed(4));
        }

        function CompensationAmount_OnValueChanged(sender, eventArgs) {
            var annualSalaryControl = document.getElementById('<%= EmployeePositionView.FindControl("AnnualSalary").ClientID %>');
            var compensationAmountControl = document.getElementById('<%= EmployeePositionView.FindControl("CompensationAmount").ClientID %>');
            var paymentMethodCodeControl = document.getElementById('<%= EmployeePositionView.FindControl("PaymentMethodCode").ClientID %>');
            var standardHoursControl = document.getElementById('<%= EmployeePositionView.FindControl("StandardHours").ClientID %>');
            var periodsPerYear = <%= CurrentEmployeePositionCollection[0].PeriodsPerYear %>;

            if (annualSalaryControl != null && compensationAmountControl != null && paymentMethodCodeControl != null && standardHoursControl != null && periodsPerYear != 0) {
                var annualSalary = document.getElementById(annualSalaryControl.attributes['fieldClientId'].value);
                var compensationAmount = document.getElementById(compensationAmountControl.attributes['fieldClientId'].value);
                var paymentMethodCode = document.getElementById(paymentMethodCodeControl.attributes['fieldClientId'].value);
                var standardHours = document.getElementById(standardHoursControl.attributes['fieldClientId'].value);

                setAnnualSalary(annualSalary, compensationAmount.value.replace(/,/g, ""), paymentMethodCode, standardHours.value.replace(/,/g, ""), periodsPerYear);
            }
        }

        function setAnnualSalary(annualSalary, compensationAmount, paymentMethodCode, standardHours, periodsPerYear) {
            var salary = 0;

            if (paymentMethodCode.value != null) {
                if (paymentMethodCode.value.toLowerCase() == 'hourly')
                    salary = ((parseFloat(compensationAmount) || 0) * parseFloat(standardHours)).toFixed(4) * parseFloat(periodsPerYear);
                else
                    salary = (parseFloat(compensationAmount) || 0) * parseFloat(periodsPerYear);
            }
        
            annualSalary.value = addCommas(salary.toFixed(4));
        }

        function addCommas(nStr) {
            nStr += '';
            var x = nStr.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;

            while (rgx.test(x1))
                x1 = x1.replace(rgx, '$1' + ',' + '$2');

            return x1 + x2;
        }
    </script>
</telerik:RadScriptBlock>