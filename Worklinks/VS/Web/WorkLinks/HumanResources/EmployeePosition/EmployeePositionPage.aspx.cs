﻿using System;
using System.Web.Services;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.EmployeePosition
{
    public partial class EmployeePositionPage : WorkLinksPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        [WebMethod]
        public static SalaryPlanGradeStepDetail GetRate(long salaryPlanGradeStepId)
        {
            if (Common.Security.RoleForm.EmployeePosition.ViewFlag)
            {
                SalaryPlanGradeStepDetailCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetSalaryPlanGradeStepDetail(null, salaryPlanGradeStepId);
                SalaryPlanGradeStepDetail gradeStepDetail = null;

                if (collection != null && collection.Count > 0)
                    gradeStepDetail = collection[0];

                return gradeStepDetail;
            }
            else
                throw new Exception("Not Authorized");
        }
    }
}