﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Entitlement
{
    public partial class EntitlementDetailPaycodeControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private long _accrualEntitlementDetailId = -1;
        private const String _accrualEntitlementDetailIdKey = "_accrualEntitlementDetailIdKey";
        private AccrualEntitlementDetailPaycodeCollection _accrualEntitlementDetailPaycodeCollection = null;
        private CodeCollection _checkedPaycodes = null;
        #endregion

        #region properties
        public long AccrualEntitlementDetailId
        {
            get
            {
                if (_accrualEntitlementDetailId == -1)
                    _accrualEntitlementDetailId = Convert.ToInt64(ViewState[_accrualEntitlementDetailIdKey]);

                return _accrualEntitlementDetailId;
            }
            set
            {
                _accrualEntitlementDetailId = value;
                ViewState[_accrualEntitlementDetailIdKey] = _accrualEntitlementDetailId;
            }
        }
        public AccrualEntitlementDetailPaycodeCollection EntitlementDetailPaycodeCollection
        {
            get
            {
                if (_accrualEntitlementDetailPaycodeCollection == null)
                    _accrualEntitlementDetailPaycodeCollection = new AccrualEntitlementDetailPaycodeCollection();

                return _accrualEntitlementDetailPaycodeCollection;
            }
        }
        public CodeCollection CheckedPaycodes
        {
            get
            {
                if (_checkedPaycodes == null)
                    _checkedPaycodes = new CodeCollection();

                return _checkedPaycodes;
            }
        }
        public AccrualEntitlementDetail LastEntitlementDetail { get; set; }
        public bool IsEditMode { get; set; }
        public bool IsInsertMode
        {
            get { return AccrualEntitlementDetailId == -1; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        public void LoadEntitlementDetailPaycode()
        {
            AccrualEntitlementDetailPaycodeCollection collection = new AccrualEntitlementDetailPaycodeCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetAccrualEntitlementDetailPaycode(IsInsertMode && LastEntitlementDetail != null ? LastEntitlementDetail.AccrualEntitlementDetailId : AccrualEntitlementDetailId));

            //if we are doing an insert and there is a previous entitlement detail record
            if (IsInsertMode && LastEntitlementDetail != null)
            {
                //reset the AccrualEntitlementDetailPaycodeId and AccrualEntitlementDetailId for the insert
                for (int i = 0; i < collection.Count; i++)
                {
                    collection[i].AccrualEntitlementDetailPaycodeId = -i - 1;
                    collection[i].AccrualEntitlementDetailId = AccrualEntitlementDetailId;
                }
            }

            Data = collection;

            EntitlementDetailPaycode.DataBind();
        }
        public void InsertEntitlementDetailPaycode()
        {
            CodeCollection entitlementDetailPaycode = (CodeCollection)EntitlementDetailPaycode.Value;

            for (int i = 0; i < entitlementDetailPaycode.Count; i++)
            {
                EntitlementDetailPaycodeCollection.Add(new AccrualEntitlementDetailPaycode
                {
                    AccrualEntitlementDetailPaycodeId = -i - 1,
                    AccrualEntitlementDetailId = AccrualEntitlementDetailId,
                    PaycodeCode = entitlementDetailPaycode[i].Code
                });
            }
        }
        #endregion

        #region event handlers
        protected void EntitlementDetailPaycode_NeedDataSource(object sender, EventArgs e)
        {
            CodeCollection paycodes = Common.CodeHelper.GetCodeTable(LanguageCode, CodeTableType.PaycodeCode);

            foreach (CodeObject code in paycodes)
            {
                foreach (AccrualEntitlementDetailPaycode paycode in (AccrualEntitlementDetailPaycodeCollection)Data)
                {
                    if (paycode.PaycodeCode == code.Code)
                        CheckedPaycodes.Add(new CodeObject() { Code = code.Code, Description = code.Description });
                }
            }

            if (!IsEditMode)
                paycodes = CheckedPaycodes;

            ((WLP.Web.UI.Controls.ICodeControl)sender).DataSource = paycodes;
            ((WLP.Web.UI.Controls.CheckedListBoxControl)sender).Value = CheckedPaycodes;
        }
        #endregion
    }
}