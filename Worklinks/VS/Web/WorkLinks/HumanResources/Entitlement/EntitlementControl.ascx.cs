﻿using System;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Entitlement
{
    public partial class EntitlementControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private String Action
        {
            get { return Page.RouteData.Values["action"].ToString().ToLower(); }
        }
        private long EntitlementId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["entitlementId"]); }
        }
        public bool IsEditMode
        {
            get { return EntitlementView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.Edit); }
        }
        public bool IsInsertMode
        {
            get { return EntitlementView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.Insert); }
        }
        public bool IsViewMode
        {
            get { return EntitlementView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.ReadOnly); }
        }
        public bool UpdateFlag
        {
            get { return Common.Security.RoleForm.EntitlementEdit.UpdateFlag; }
        }
        public bool IsEntitlementDetailViewMode
        {
            get { return EntitlementDetailControl.IsViewMode && EntitlementDetailControl.IsDetailEntryViewMode; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EntitlementEdit.ViewFlag);

            WireEvents();

            if (Action == "view")
            {
                this.Page.Title = "Edit an Entitlement";

                if (!IsPostBack)
                {
                    LoadEntitlement();
                    LoadEntitlementDetailControl();
                }
            }
            else if (Action == "add")
            {
                if (!IsPostBack)
                {
                    this.Page.Title = "Add an Entitlement";
                    LoadNewEntitlement();
                }
                else
                {
                    this.Page.Title = "Edit an Entitlement";
                    LoadEntitlementDetailControl();
                }
            }
        }
        protected void WireEvents()
        {
            EntitlementView.NeedDataSource += new WLPFormView.NeedDataSourceEventHandler(EntitlementView_NeedDataSource);
            EntitlementView.Inserting += new WLPFormView.ItemInsertingEventHandler(EntitlementView_Inserting);
            EntitlementView.Updating += new WLPFormView.ItemUpdatingEventHandler(EntitlementView_Updating);
        }
        protected void LoadEntitlement()
        {
            AccrualEntitlementCollection collection = new AccrualEntitlementCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetAccrualEntitlement(EntitlementId));
            Data = collection;

            EntitlementView.DataBind();
        }
        protected void LoadNewEntitlement()
        {
            AccrualEntitlementCollection collection = new AccrualEntitlementCollection();
            collection.AddNew();
            Data = collection;

            EntitlementView.ChangeMode(System.Web.UI.WebControls.FormViewMode.Insert);
            EntitlementView.DataBind();
        }
        protected void LoadEntitlementDetailControl()
        {
            EntitlementDetailControl.Visible = true;
            EntitlementDetailControl.AccrualEntitlementId = ((AccrualEntitlementCollection)Data)[0].AccrualEntitlementId;
            EntitlementDetailControl.LoadEntitlementDetail();
            EntitlementDetailControl.Initialize();
        }
        #endregion

        #region handles updates
        void EntitlementView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            AccrualEntitlement entitlement = (AccrualEntitlement)e.DataItem;
            Common.ServiceWrapper.HumanResourcesClient.InsertAccrualEntitlement(entitlement).CopyTo((AccrualEntitlement)Data[entitlement.Key]);
        }
        void EntitlementView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                AccrualEntitlement entitlement = (AccrualEntitlement)e.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.UpdateAccrualEntitlement(entitlement).CopyTo((AccrualEntitlement)Data[entitlement.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region event handlers
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        void EntitlementView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            EntitlementView.DataSource = Data;
        }
        protected void InitializeControls()
        {
            //this code makes sure the RadToolbar is set to the proper status before/during/after update/insert operations
            WLPToolBar toolbar = (WLPToolBar)EntitlementView.FindControl("EntitlementToolBar");
            toolbar.Enabled = IsEntitlementDetailViewMode;
        }
        #endregion

        #region override
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            InitializeControls();
        }
        #endregion
    }
}