﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace WorkLinks.HumanResources.Entitlement {
    
    
    public partial class EntitlementDetailControl {
        
        /// <summary>
        /// EntitlementDetailGrid control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPGrid EntitlementDetailGrid;
        
        /// <summary>
        /// EntitlementDetailToolBar control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPToolBar EntitlementDetailToolBar;
        
        /// <summary>
        /// EntitlementDetailEntryControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WorkLinks.HumanResources.Entitlement.EntitlementDetailEntryControl EntitlementDetailEntryControl;
    }
}
