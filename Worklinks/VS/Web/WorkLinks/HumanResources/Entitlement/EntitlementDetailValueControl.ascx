﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EntitlementDetailValueControl.ascx.cs" Inherits="WorkLinks.HumanResources.Entitlement.EntitlementDetailValueControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPGrid
    ID="EntitlementDetailValueGrid"
    EditMode="InPlace"
    runat="server"
    GridLines="None"
    Width="100%"
    Height="100%"
    AutoGenerateColumns="false"
    AutoInsertOnAdd="false"
    AllowMultiRowEdit="true"
    Skin="">

    <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
        <Selecting AllowRowSelect="false" />
        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
        <CommandItemTemplate></CommandItemTemplate>

        <Columns>
            <wasp:GridTemplateControl UniqueName="HeaderColumns" Reorderable="false">
                <HeaderTemplate>
                    <table width="100%">
                        <tr>
                            <td align="left" width="50%">
                                <wasp:WLPLabel ID="EffectiveMonth" runat="server" Text="**EffectiveMonth" ResourceName="EffectiveMonth" />
                            </td>
                            <td align="left" width="50%">
                                <wasp:WLPLabel ID="Amount" runat="server" Text="**Amount" ResourceName="Amount" Visible="false" />
                                <wasp:WLPLabel ID="AmountPercentage" runat="server" Text="**AmountPercentage" ResourceName="AmountPercentage" Visible="false" />
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>

                <ItemTemplate>
                    <table width="100%">
                        <tr>
                            <td width="50%">
                                <wasp:NumericControl ID="EffectiveMonthValue" runat="server" GroupSeparator="" Width="160px" LabelWidth="0" LabelText="" DecimalDigits="0" Value='<%# Bind("EffectiveMonth") %>' ReadOnly="true" />
                            </td>
                            <td width="50%">
                                <wasp:NumericControl ID="AmountPercentageValue" runat="server" GroupSeparator="" Width="160px" LabelWidth="0" LabelText="" DecimalDigits="4" Value='<%# Bind("AmountPercentage") %>' ReadOnly="true" />
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>

                <EditItemTemplate>
                    <table width="100%">
                        <tr>
                            <td width="50%">
                                <wasp:NumericControl ID="EffectiveMonthValue" runat="server" GroupSeparator="" Width="160px" LabelWidth="0" LabelText="" MaxValue="9999" DecimalDigits="0" Value='<%# Bind("EffectiveMonth") %>' TabIndex="010" />
                            </td>
                            <td width="50%">
                                <wasp:NumericControl ID="AmountPercentageValue" runat="server" GroupSeparator="" Width="160px" LabelWidth="0" LabelText="" MaxValue="999.9999" DecimalDigits="4" Value='<%# Bind("AmountPercentage") %>' TabIndex="020" />
                            </td>
                        </tr>
                    </table>
                </EditItemTemplate>
            </wasp:GridTemplateControl>
        </Columns>
    </MasterTableView>

    <HeaderContextMenu EnableAutoScroll="true"></HeaderContextMenu>

</wasp:WLPGrid>