﻿using System;
using Telerik.Web.UI;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Entitlement
{
    public partial class EntitlementDetailControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private long _accrualEntitlementId = -1;
        private long _accrualEntitlementDetailId = -1;
        private const String _accrualEntitlementIdKey = "_accrualEntitlementIdKey";
        private bool _isRowClickFired = false;
        #endregion

        #region properties
        public long AccrualEntitlementId
        {
            get
            {
                if (_accrualEntitlementId == -1)
                    _accrualEntitlementId = Convert.ToInt64(ViewState[_accrualEntitlementIdKey]);

                return _accrualEntitlementId;
            }
            set
            {
                _accrualEntitlementId = value;
                ViewState[_accrualEntitlementIdKey] = _accrualEntitlementId;
            }
        }
        public long AccrualEntitlementDetailId
        {
            get { return _accrualEntitlementDetailId; }
            set { _accrualEntitlementDetailId = value; }
        }
        public bool AddFlag
        {
            get { return Common.Security.RoleForm.EntitlementEdit.AddFlag; }
        }
        public bool DeleteFlag
        {
            get { return Common.Security.RoleForm.EntitlementEdit.DeleteFlag; }
        }
        public bool IsInsertMode
        {
            get { return EntitlementDetailGrid.IsInsertMode; }
        }
        public bool IsViewMode
        {
            get { return EntitlementDetailGrid.IsViewMode; }
        }
        public bool IsEntitlementViewMode
        {
            get { return ((EntitlementControl)this.Parent.Parent).IsViewMode; }
        }
        public bool IsDetailEntryViewMode
        {
            get { return EntitlementDetailEntryControl.IsViewMode; }
        }
        public bool IsRowClickFired
        {
            get { return _isRowClickFired; }
            set { _isRowClickFired = value; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();
        }
        public void Initialize()
        {
            //find the EntitlementDetailGrid
            WLP.Web.UI.Controls.WLPGrid grid = (WLP.Web.UI.Controls.WLPGrid)this.FindControl("EntitlementDetailGrid");

            //hide the delete image in the rows based on permissions
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        public void LoadEntitlementDetail()
        {
            Data = Common.ServiceWrapper.HumanResourcesClient.GetAccrualEntitlementDetail(AccrualEntitlementId, -1); //-1 gets everything
        }
        protected void LoadEntitlementDetailEntryControl()
        {
            //load the EntitlementDetailEntryControl
            EntitlementDetailEntryControl.Visible = true;
            EntitlementDetailEntryControl.AccrualEntitlementId = AccrualEntitlementId;
            EntitlementDetailEntryControl.AccrualEntitlementDetailId = AccrualEntitlementDetailId;
            EntitlementDetailEntryControl.LoadEntitlementDetailEntry();
        }
        protected void WireEvents()
        {
            this.EntitlementDetailEntryControl.EntitlementUpdated += EntitlementDetailEntryControl_EntitlementUpdated;
            this.EntitlementDetailEntryControl.EntitlementCancelled += EntitlementDetailEntryControl_EntitlementCancelled;
        }
        private void InitializeControls()
        {
            //this code makes sure the RadToolbar is set to the proper status before/during/after update/insert operations
            GridCommandItem cmdItem = (GridCommandItem)EntitlementDetailGrid.MasterTableView.GetItems(GridItemType.CommandItem)[0];
            RadToolBar toolBar = (RadToolBar)cmdItem.FindControl("EntitlementDetailToolBar");
            toolBar.Enabled = IsEntitlementViewMode && IsDetailEntryViewMode;
        }
        #endregion

        #region event handlers
        private void EntitlementDetailEntryControl_EntitlementUpdated(object sender, EventArgs e)
        {
            //refresh the EntitlementDetailGrid
            LoadEntitlementDetail();
            EntitlementDetailGrid.Rebind();
        }
        private void EntitlementDetailEntryControl_EntitlementCancelled(object sender, EventArgs e)
        {
            EntitlementDetailEntryControl.Visible = false;
        }
        protected void EntitlementDetailGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteAccrualEntitlementDetail((AccrualEntitlementDetail)e.Item.DataItem);

                //refresh the EntitlementDetailGrid
                LoadEntitlementDetail();
                EntitlementDetailGrid_NeedDataSource(null, null);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void EntitlementDetailGrid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Add")
            {
                EntitlementDetailGrid.SelectedIndexes.Clear();
                LoadEntitlementDetailEntryControl();
            }
            else if (e.CommandName == "RowClick")
            {
                IsRowClickFired = true;
                AccrualEntitlementDetailId = ((AccrualEntitlementDetailCollection)Data)[e.Item.ItemIndex].AccrualEntitlementDetailId;
                LoadEntitlementDetailEntryControl();
            }
            else
                EntitlementDetailEntryControl.Visible = false;
        }
        protected void EntitlementDetailGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EntitlementDetailGrid.DataSource = Data;
        }
        protected void EntitlementDetailGrid_PreRender(object sender, EventArgs e)
        {
            if (!IsRowClickFired)
            {
                if (IsViewMode && EntitlementDetailGrid.MasterTableView.Items.Count > 0)
                {
                    AccrualEntitlementDetail data = (AccrualEntitlementDetail)EntitlementDetailGrid.MasterTableView.Items[0].DataItem;
                    if (data != null)
                    {
                        EntitlementDetailGrid.MasterTableView.Items[0].Selected = true; //select the first row
                        AccrualEntitlementDetailId = data.AccrualEntitlementDetailId;
                        LoadEntitlementDetailEntryControl();
                    }
                }
            }
        }
        #endregion

        #region override
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            InitializeControls();
        }
        #endregion
    }
}