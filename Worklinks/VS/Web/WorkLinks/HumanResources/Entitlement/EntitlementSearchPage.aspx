﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EntitlementSearchPage.aspx.cs" Inherits="WorkLinks.HumanResources.Entitlement.EntitlementSearchPage" %>
<%@ Register Src="EntitlementSearchControl.ascx" TagName="EntitlementSearchControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:EntitlementSearchControl ID="EntitlementSearchControl1" SearchLocation="Entitlement" runat="server" />        
    </ContentTemplate>
</asp:Content>