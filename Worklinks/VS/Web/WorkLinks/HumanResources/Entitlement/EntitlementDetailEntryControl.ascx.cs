﻿using System;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Entitlement
{
    public partial class EntitlementDetailEntryControl : WLP.Web.UI.WLPUserControl
    {
        #region delegates
        public delegate void EntitlementUpdatedEventHandler(object sender, EventArgs e);
        public delegate void EntitlementCancelledEventHandler(object sender, EventArgs e);
        public event EntitlementUpdatedEventHandler EntitlementUpdated;
        public event EntitlementCancelledEventHandler EntitlementCancelled;
        #endregion

        #region fields
        private long _accrualEntitlementId = -1;
        private long _accrualEntitlementDetailId = -1;
        private const String _accrualEntitlementIdKey = "_accrualEntitlementIdKey";
        private const String _accrualEntitlementDetailIdKey = "_accrualEntitlementDetailIdKey";
        #endregion

        #region properties     
        public bool IsEditMode
        {
            get { return EntitlementDetailView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.Edit); }
        }
        public bool IsViewMode
        {
            get { return EntitlementDetailView.CurrentMode.Equals(System.Web.UI.WebControls.FormViewMode.ReadOnly); }
        }
        public bool IsEntitlementViewMode
        {
            get { return ((EntitlementControl)this.Parent.Parent.Parent).IsViewMode; }
        }
        public bool IsInsertMode
        {
            get { return AccrualEntitlementDetailId == -1; }
        }
        public bool UpdateFlag
        {
            get { return Common.Security.RoleForm.EntitlementEdit.UpdateFlag; }
        }
        public long AccrualEntitlementId
        {
            get
            {
                if (_accrualEntitlementId == -1)
                    _accrualEntitlementId = Convert.ToInt64(ViewState[_accrualEntitlementIdKey]);

                return _accrualEntitlementId;
            }
            set
            {
                _accrualEntitlementId = value;
                ViewState[_accrualEntitlementIdKey] = _accrualEntitlementId;
            }
        }
        public long AccrualEntitlementDetailId
        {
            get
            {
                if (_accrualEntitlementDetailId == -1)
                    _accrualEntitlementDetailId = Convert.ToInt64(ViewState[_accrualEntitlementDetailIdKey]);

                return _accrualEntitlementDetailId;
            }
            set
            {
                _accrualEntitlementDetailId = value;
                ViewState[_accrualEntitlementDetailIdKey] = _accrualEntitlementDetailId;
            }
        }
        public AccrualEntitlementDetail LastEntitlementDetail
        {
            get
            {
                AccrualEntitlementDetail lastEntitlementDetail = null;
                AccrualEntitlementDetailCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetAccrualEntitlementDetail(AccrualEntitlementId, -1); //-1 gets everything

                if (collection.Count > 0)
                    lastEntitlementDetail = collection[0];

                return lastEntitlementDetail;
            }
        }
        public AccrualEntitlementDetailValueCollection EntitlementDetailValueCollection { get; set; }
        public AccrualEntitlementDetailPaycodeCollection EntitlementDetailPaycodeCollection { get; set; }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();
        }
        protected void WireEvents()
        {
            EntitlementDetailView.NeedDataSource += new WLPFormView.NeedDataSourceEventHandler(EntitlementDetailView_NeedDataSource);
            EntitlementDetailView.Updating += new WLPFormView.ItemUpdatingEventHandler(EntitlementDetailView_Updating);
            EntitlementDetailView.DataBound += EntitlementDetailView_DataBound;
            EntitlementDetailView.ItemCommand += EntitlementDetailView_ItemCommand;
            EntitlementDetailView.PreRender += EntitlementDetailView_PreRender;
        }
        private void InitializeControls()
        {
            //this code makes sure the RadToolbar is set to the proper status before/during/after update/insert operations
            WLPToolBar toolbar = (WLPToolBar)EntitlementDetailView.FindControl("EntitlementDetailToolBar");
            toolbar.Enabled = IsEntitlementViewMode;
        }
        public void LoadEntitlementDetailEntry()
        {
            AccrualEntitlementDetailCollection collection = new AccrualEntitlementDetailCollection();

            if (IsInsertMode) //if we are doing an insert
            {
                if (LastEntitlementDetail != null) //if there is a previous entitlement detail record
                {
                    collection.Add(LastEntitlementDetail);

                    //reset the AccrualEntitlementDetailId and EffectiveDate
                    collection[0].AccrualEntitlementDetailId = AccrualEntitlementDetailId;
                    collection[0].EffectiveDate = null;
                }
                else
                {
                    collection.AddNew();
                    collection[0].AccrualEntitlementId = AccrualEntitlementId; //set the AccrualEntitlementId
                }
            }
            else
                collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetAccrualEntitlementDetail(AccrualEntitlementId, AccrualEntitlementDetailId));

            Data = collection;

            if (IsInsertMode) //if we are doing an insert
                EntitlementDetailView.ChangeMode(System.Web.UI.WebControls.FormViewMode.Edit);

            EntitlementDetailView.DataBind();
        }
        private void LoadEntitlementDetailValueControl()
        {
            //load the EntitlementDetailValueControl
            EntitlementDetailValueControl.AccrualEntitlementId = AccrualEntitlementId;
            EntitlementDetailValueControl.AccrualEntitlementDetailId = AccrualEntitlementDetailId;
            EntitlementDetailValueControl.LastEntitlementDetail = LastEntitlementDetail;
            EntitlementDetailValueControl.IsEditMode = IsEditMode;
            EntitlementDetailValueControl.LoadEntitlementDetailValue();
            EntitlementDetailValueControl.Initialize();
        }
        private void LoadEntitlementDetailPaycodeControl()
        {
            //load the EntitlementDetailPaycodeControl
            EntitlementDetailPaycodeControl.AccrualEntitlementDetailId = AccrualEntitlementDetailId;
            EntitlementDetailPaycodeControl.LastEntitlementDetail = LastEntitlementDetail;
            EntitlementDetailPaycodeControl.IsEditMode = IsEditMode;
            EntitlementDetailPaycodeControl.LoadEntitlementDetailPaycode();
        }
        #endregion

        #region event handlers
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void EntitlementRecurrenceCode_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            EntitlementDetailPaycodeControl.Visible = e.Value == "ACCRUAL";
        }
        void EntitlementDetailView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            EntitlementDetailView.DataSource = Data;
        }
        private void EntitlementDetailView_DataBound(object sender, EventArgs e)
        {
            //show/hide the paycode selection based on EntitlementRecurrenceCode
            ComboBoxControl entitlementRecurrenceCode = (ComboBoxControl)EntitlementDetailView.FindControl("EntitlementRecurrenceCode");
            if (entitlementRecurrenceCode != null)
                EntitlementDetailPaycodeControl.Visible = entitlementRecurrenceCode.Value == null ? false : entitlementRecurrenceCode.Value.ToString() == "ACCRUAL";

            if (IsEditMode && IsInsertMode)
            {
                //get the latest entitlement detail record and set the MinDate to a day after the EffectiveDate
                DateControl dateControl = (DateControl)EntitlementDetailView.FindControl("EffectiveDate");
                if (dateControl != null && LastEntitlementDetail != null)
                    dateControl.MinDate = ((DateTime)LastEntitlementDetail.EffectiveDate).AddDays(1);
            }
        }
        private void EntitlementDetailView_ItemCommand(object sender, System.Web.UI.WebControls.FormViewCommandEventArgs e)
        {
            if (e.CommandName == "Cancel")
            {
                if (IsInsertMode) //if we are doing an insert
                    OnEntitlementCancelled(null);
            }
        }
        private void EntitlementDetailView_PreRender(object sender, EventArgs e)
        {
            //load the child controls
            LoadEntitlementDetailValueControl();
            LoadEntitlementDetailPaycodeControl();
        }
        void EntitlementDetailView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                //get the values to update from the EntitlementDetailValueControl
                EntitlementDetailValueControl.UpdateEntitlementDetailValue();
                EntitlementDetailValueCollection = EntitlementDetailValueControl.EntitlementDetailValueCollection;

                ComboBoxControl entitlementRecurrenceCode = (ComboBoxControl)EntitlementDetailView.FindControl("EntitlementRecurrenceCode");
                if (entitlementRecurrenceCode != null)
                {
                    if (entitlementRecurrenceCode.Value.ToString() == "ACCRUAL")
                    {
                        //get the values to insert from the EntitlementDetailPaycodeControl
                        EntitlementDetailPaycodeControl.InsertEntitlementDetailPaycode();
                        EntitlementDetailPaycodeCollection = EntitlementDetailPaycodeControl.EntitlementDetailPaycodeCollection;
                    }
                    else
                        EntitlementDetailPaycodeCollection = new AccrualEntitlementDetailPaycodeCollection();
                }

                AccrualEntitlementDetail entitlementDetail = (AccrualEntitlementDetail)e.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.UpdateAccrualEntitlementDetail(entitlementDetail, EntitlementDetailValueCollection, EntitlementDetailPaycodeCollection).CopyTo((AccrualEntitlementDetail)Data[entitlementDetail.Key]);

                //refresh the grid on the EntitlementDetailValueControl and EntitlementDetailControl
                EntitlementDetailValueControl.RefreshLoadEntitlementDetailValue();
                OnEntitlementUpdated(null);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region event triggers
        protected virtual void OnEntitlementUpdated(EventArgs args)
        {
            if (EntitlementUpdated != null)
                EntitlementUpdated(this, args);
        }
        protected virtual void OnEntitlementCancelled(EventArgs args)
        {
            if (EntitlementCancelled != null)
                EntitlementCancelled(this, args);
        }
        #endregion

        #region override
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            InitializeControls();
        }
        #endregion
    }
}