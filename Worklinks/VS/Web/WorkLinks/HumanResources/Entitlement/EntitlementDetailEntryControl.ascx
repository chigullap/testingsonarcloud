﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EntitlementDetailEntryControl.ascx.cs" Inherits="WorkLinks.HumanResources.Entitlement.EntitlementDetailEntryControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Src="EntitlementDetailValueControl.ascx" TagName="EntitlementDetailValueControl" TagPrefix="uc1" %>
<%@ Register Src="EntitlementDetailPaycodeControl.ascx" TagName="EntitlementDetailPaycodeControl" TagPrefix="uc2" %>

<wasp:WLPFormView
    ID="EntitlementDetailView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key">

    <ItemTemplate>
        <wasp:WLPToolBar ID="EntitlementDetailToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0">
            <Items>
                <wasp:WLPToolBarButton Visible="<%# UpdateFlag %>" ImageUrl="~/App_Themes/Default/Edit.gif" Text="**Edit" CommandName="Edit" ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>

        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:DateControl ID="EffectiveDate" runat="server" Value='<%# Bind("EffectiveDate") %>' ResourceName="EffectiveDate" Mandatory="true" ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="EmploymentDateCode" runat="server" Type="EmploymentDateCode" Value='<%# Bind("EmploymentDateCode") %>' ResourceName="EmploymentDateCode" OnDataBinding="Code_NeedDataSource" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="EntitlementRecurrenceCode" runat="server" Type="EntitlementRecurrenceCode" Value='<%# Bind("EntitlementRecurrenceCode") %>' ResourceName="EntitlementRecurrenceCode" OnDataBinding="Code_NeedDataSource" ReadOnly="true" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar ID="EntitlementDetailToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
            <Items>
                <wasp:WLPToolBarButton Visible="<%# IsEditMode %>" ImageUrl="~/App_Themes/Default/Update.gif" Text="**Update" CommandName="Update" ResourceName="Update"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton ImageUrl="~/App_Themes/Default/Cancel.gif" Text="**Cancel" CausesValidation="false" CommandName="Cancel" ResourceName="Cancel"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>

        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:DateControl ID="EffectiveDate" runat="server" Value='<%# Bind("EffectiveDate") %>' ResourceName="EffectiveDate" Mandatory="true" ReadOnly="<%# !IsInsertMode %>" TabIndex="010" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="EmploymentDateCode" runat="server" Type="EmploymentDateCode" Value='<%# Bind("EmploymentDateCode") %>' ResourceName="EmploymentDateCode" OnDataBinding="Code_NeedDataSource" TabIndex="020" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="EntitlementRecurrenceCode" runat="server" Type="EntitlementRecurrenceCode" Value='<%# Bind("EntitlementRecurrenceCode") %>' ResourceName="EntitlementRecurrenceCode" OnDataBinding="Code_NeedDataSource" AutoPostBack="true" OnSelectedIndexChanged="EntitlementRecurrenceCode_SelectedIndexChanged" Mandatory="true" ReadOnly="<%# !IsInsertMode %>" TabIndex="030" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>

</wasp:WLPFormView>

<table width="100%">
    <tr>
        <td colspan="2">
            <table>
                <tr>
                    <td style="width: 400px; height: 355px;">
                        <uc1:EntitlementDetailValueControl ID="EntitlementDetailValueControl" runat="server" />
                    </td>
                    <td style="width: 400px; height: 355px;">
                        <uc2:EntitlementDetailPaycodeControl ID="EntitlementDetailPaycodeControl" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>