﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EntitlementDetailPaycodeControl.ascx.cs" Inherits="WorkLinks.HumanResources.Entitlement.EntitlementDetailPaycodeControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<style>
    .RadListBox .rlbGroup {
        border-width: 0 !important;
    }
</style>

<table width="100%">
    <tr>
        <td>
            <wasp:WLPLabel ID="PaycodesLabel" runat="server" Text="**Paycodes" Font-Bold="true" ResourceName="Paycodes" />

            <wasp:CheckedListBoxControl
                ID="EntitlementDetailPaycode"
                runat="server"
                LabelText=""
                ListBoxHeight="310px"
                ListBoxWidth="360px"
                ResourceName="EntitlementDetailPaycode"
                Type="PaycodeCode"
                OnDataBinding="EntitlementDetailPaycode_NeedDataSource"
                Value='<%# CheckedPaycodes %>'
                ReadOnly='<%# !IsEditMode %>' />
        </td>
    </tr>
</table>