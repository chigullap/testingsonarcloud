﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Entitlement
{
    public partial class EntitlementDetailValueControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private long _accrualEntitlementId = -1;
        private long _accrualEntitlementDetailId = -1;
        private const String _accrualEntitlementIdKey = "_accrualEntitlementIdKey";
        private const String _accrualEntitlementDetailIdKey = "_accrualEntitlementDetailIdKey";
        private AccrualEntitlementDetailValueCollection _accrualEntitlementDetailValueCollection = null;
        #endregion

        #region properties
        public long AccrualEntitlementId
        {
            get
            {
                if (_accrualEntitlementId == -1)
                    _accrualEntitlementId = Convert.ToInt64(ViewState[_accrualEntitlementIdKey]);

                return _accrualEntitlementId;
            }
            set
            {
                _accrualEntitlementId = value;
                ViewState[_accrualEntitlementIdKey] = _accrualEntitlementId;
            }
        }
        public long AccrualEntitlementDetailId
        {
            get
            {
                if (_accrualEntitlementDetailId == -1)
                    _accrualEntitlementDetailId = Convert.ToInt64(ViewState[_accrualEntitlementDetailIdKey]);

                return _accrualEntitlementDetailId;
            }
            set
            {
                _accrualEntitlementDetailId = value;
                ViewState[_accrualEntitlementDetailIdKey] = _accrualEntitlementDetailId;
            }
        }
        public AccrualEntitlementDetailValueCollection EntitlementDetailValueCollection
        {
            get
            {
                if (_accrualEntitlementDetailValueCollection == null)
                    _accrualEntitlementDetailValueCollection = new AccrualEntitlementDetailValueCollection();

                return _accrualEntitlementDetailValueCollection;
            }
        }
        public AccrualEntitlementDetail LastEntitlementDetail { get; set; }
        public bool IsEditMode { get; set; }
        public bool IsInsertMode
        {
            get { return AccrualEntitlementDetailId == -1; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();
        }
        public AccrualEntitlementDetailValueCollection AddRowsToCollection(AccrualEntitlementDetailValueCollection collection)
        {
            //get the number of rows that need to be generated
            int numberOfRowsToGenerate = 5 - collection.Count;

            //add generated rows to the collection
            for (int i = 0; i < numberOfRowsToGenerate; i++)
            {
                collection.Add(new AccrualEntitlementDetailValue()
                {
                    AccrualEntitlementDetailValueId = -i - (collection.Count + 1),
                    AccrualEntitlementDetailId = AccrualEntitlementDetailId
                });
            }

            return collection;
        }
        public void Initialize()
        {
            EntitlementDetailValueGrid.Rebind();

            //put the EntitlementDetailValueGrid in EditAllMode if the EntitlementDetailView is in EditMode
            EntitlementDetailValueGrid.SetEditAllMode(IsEditMode);
        }
        public void LoadEntitlementDetailValue()
        {
            AccrualEntitlementDetailValueCollection collection = new AccrualEntitlementDetailValueCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetAccrualEntitlementDetailValue(IsInsertMode && LastEntitlementDetail != null ? LastEntitlementDetail.AccrualEntitlementDetailId : AccrualEntitlementDetailId));

            //if we are doing an insert and there is a previous entitlement detail record
            if (IsInsertMode && LastEntitlementDetail != null)
            {
                //reset the AccrualEntitlementDetailValueId and AccrualEntitlementDetailId for the insert
                for (int i = 0; i < collection.Count; i++)
                {
                    collection[i].AccrualEntitlementDetailValueId = -i - 1;
                    collection[i].AccrualEntitlementDetailId = AccrualEntitlementDetailId;
                }
            }

            if (IsEditMode)
                Data = AddRowsToCollection(collection);
            else
                Data = collection;

            EntitlementDetailValueGrid.DataBind();
        }
        public void RefreshLoadEntitlementDetailValue()
        {
            //refresh the EntitlementDetailValueGrid
            LoadEntitlementDetailValue();

            if (IsInsertMode)
                EntitlementDetailValueGrid.Rebind();
            else
                EntitlementDetailValueGrid_NeedDataSource(null, null);
        }
        public void UpdateEntitlementDetailValue()
        {
            //fire the UpdateAll for the EntitlementDetailValueGrid
            EntitlementDetailValueGrid.UpdateAll();
        }
        protected void WireEvents()
        {
            EntitlementDetailValueGrid.NeedDataSource += new GridNeedDataSourceEventHandler(EntitlementDetailValueGrid_NeedDataSource);
            EntitlementDetailValueGrid.PreRender += new EventHandler(EntitlementDetailValueGrid_PreRender);
            EntitlementDetailValueGrid.UpdateAllCommand += new WLPGrid.UpdateAllCommandEventHandler(EntitlementDetailValueGrid_UpdateAllCommand);
        }
        #endregion

        #region event handlers
        protected void EntitlementDetailValueGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EntitlementDetailValueGrid.DataSource = Data;
        }
        protected void EntitlementDetailValueGrid_PreRender(object sender, EventArgs e)
        {
            foreach (GridHeaderItem headerItem in EntitlementDetailValueGrid.MasterTableView.GetItems(GridItemType.Header))
            {
                String entitlementBasedOnCode = Common.ServiceWrapper.HumanResourcesClient.GetAccrualEntitlement(AccrualEntitlementId)[0].EntitlementBasedOnCode;
                WLPLabel amountLabel = (WLPLabel)headerItem.FindControl("Amount");
                WLPLabel amountPercentageLabel = (WLPLabel)headerItem.FindControl("AmountPercentage");

                if (amountLabel != null && amountPercentageLabel != null)
                {
                    //Amount should be visible if EntitlementBasedOnCode is "HOURS" or "DAYS"; AmountPercentage should be visible otherwise
                    if (entitlementBasedOnCode == "HOURS" || entitlementBasedOnCode == "DAYS")
                        amountLabel.Visible = true;
                    else
                        amountPercentageLabel.Visible = true;
                }
            }

            foreach (GridItem item in EntitlementDetailValueGrid.MasterTableView.Items)
            {
                NumericControl effectiveMonthValue = (NumericControl)item.FindControl("EffectiveMonthValue");
                if (effectiveMonthValue != null)
                    effectiveMonthValue.ReadOnly = !IsEditMode;

                NumericControl amountPercentageValue = (NumericControl)item.FindControl("AmountPercentageValue");
                if (amountPercentageValue != null)
                    amountPercentageValue.ReadOnly = !IsEditMode;
            }
        }
        protected void EntitlementDetailValueGrid_UpdateAllCommand(object sender, UpdateAllCommandEventArgs e)
        {
            try
            {
                foreach (AccrualEntitlementDetailValue detailValue in e.Items)
                    EntitlementDetailValueCollection.Add(detailValue);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}