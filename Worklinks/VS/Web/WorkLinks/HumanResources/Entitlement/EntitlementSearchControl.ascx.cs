﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Entitlement
{
    public partial class EntitlementSearchControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected String Criteria
        {
            get
            {
                String searchEntry = EntitlementDescription.TextValue;
                return searchEntry;
            }
            set
            {
                EntitlementDescription.TextValue = value;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EntitlementSearch.ViewFlag);

            WireEvents();

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.Equals(String.Format("refresh")))
                RefreshGrid();
        }
        protected void WireEvents()
        {
            EntitlementSummaryGrid.NeedDataSource += new GridNeedDataSourceEventHandler(EntitlementSummaryGrid_NeedDataSource);

            //work around to remove empty rows caused by the OnCommand Telerik bug
            EntitlementSummaryGrid.ItemDataBound += new GridItemEventHandler(EntitlementSummaryGrid_ItemDataBound);
            EntitlementSummaryGrid.PreRender += new EventHandler(EntitlementSummaryGrid_PreRender);
            EntitlementSummaryGrid.AllowPaging = true;
        }
        protected void RefreshGrid()
        {
            long keyId = -1;

            //if a row was selected
            if (EntitlementSummaryGrid.SelectedItems.Count != 0)
                keyId = Convert.ToInt64(((GridDataItem)EntitlementSummaryGrid.SelectedItems[0]).GetDataKeyValue(EntitlementSummaryGrid.MasterTableView.DataKeyNames[0]));

            Search(Criteria);

            if (keyId != -1)
                EntitlementSummaryGrid.SelectRowByFirstDataKey(keyId); //re-select the selected row
        }
        public void Search(String criteria)
        {
            Data = AccrualEntitlementSearchCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetAccrualEntitlementReport(criteria));
            EntitlementSummaryGrid.Rebind();
        }
        protected void Delete(AccrualEntitlementSearch entitlementSearch)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteAccrualEntitlementReport(Convert.ToInt64(entitlementSearch.Key));
                Data.Remove(entitlementSearch.Key);

                EntitlementSummaryGrid.SelectedIndexes.Clear();
                EntitlementSummaryGrid.Rebind();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    EntitlementSummaryGrid.SelectedIndexes.Clear();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region event handlers
        void EntitlementSummaryGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EntitlementSummaryGrid.DataSource = Data;
        }
        protected void EntitlementSummaryToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("delete") && EntitlementSummaryGrid.SelectedValue != null)
                Delete((AccrualEntitlementSearch)Data[EntitlementSummaryGrid.SelectedValue.ToString()]);
        }
        protected void EntitlementSummaryGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                //work around to remove empty rows caused by the OnCommand Telerik bug 
                if (String.Equals((dataItem["AccrualEntitlementDescriptionField"].Text).ToUpper(), "&NBSP;"))
                {
                    //hide the row
                    e.Item.Display = false;
                }
            }
        }
        void EntitlementSummaryGrid_PreRender(object sender, EventArgs e)
        {
            //work around to remove empty rows caused by the OnCommand Telerik bug
            if (Data == null)
                EntitlementSummaryGrid.AllowPaging = false;
            else
                EntitlementSummaryGrid.AllowPaging = true;
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search(Criteria);
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            EntitlementDescription.TextValue = "";
            Data = null;
            EntitlementSummaryGrid.DataBind();
        }
        #endregion

        #region security handlers
        protected void AddToolBar_PreRender(object sender, EventArgs e)
        {
            ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.EntitlementSearch.AddFlag;
        }
        protected void DetailsToolBar_PreRender(object sender, EventArgs e)
        {
            ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.EntitlementSearch.ViewFlag;
        }
        protected void DeleteToolBar_PreRender(object sender, EventArgs e)
        {
            ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.EntitlementSearch.DeleteFlag;
        }
        #endregion
    }
}