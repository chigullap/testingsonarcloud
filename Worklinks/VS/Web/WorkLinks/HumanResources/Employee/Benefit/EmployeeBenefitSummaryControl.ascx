﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeBenefitSummaryControl.ascx.cs" Inherits="WorkLinks.HumanResources.Employee.Benefit.EmployeeBenefitSummaryControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<wasp:WLPGrid
    ID="EmployeeBenefitSummaryGrid"
    runat="server"
    AutoAssignModifyProperties="true"
    AutoGenerateColumns="false"
    GridLines="None"
    OnDeleteCommand="EmployeeBenefitSummaryGrid_DeleteCommand"
    OnInsertCommand="EmployeeBenefitSummaryGrid_InsertCommand"
    OnNeedDataSource="EmployeeBenefitSummaryGrid_NeedDataSource"
    OnUpdateCommand="EmployeeBenefitSummaryGrid_UpdateCommand"
    OnItemCreated="EmployeeBenefitSummaryGrid_ItemCreated"
    OnItemDataBound="EmployeeBenefitSummaryGrid_ItemDataBound">

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">

        <RowIndicatorColumn>
            <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <CommandItemTemplate>
            <wasp:WLPToolBar ID="EmployeeBenefitSummaryToolBar" runat="server" AutoPostBack="true" Width="100%">
                <Items>
                    <wasp:WLPToolBarButton CommandName="InitInsert" ResourceName="Add" ImageUrl="~/App_Themes/Default/Add.gif" Text="Add" Visible="<%# IsViewMode && AddFlag %>" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <ExpandCollapseColumn>
            <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>

        <Columns>
            <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
            <wasp:GridDateTimeControl DataField="EffectiveDate" DataType="System.DateTime" LabelText="EffectiveDate" SortExpression="EffectiveDate" UniqueName="EffectiveDate" ResourceName="EffectiveDate" />
            <wasp:GridKeyValueControl DataField="BenefitPolicyId" OnNeedDataSource="BenefitPolicy_NeedDataSource" ResourceName="BenefitPolicy" />
            <wasp:GridKeyValueControl DataField="BenefitStatusCode" OnNeedDataSource="Code_NeedDataSource" Type="BenefitStatusCode" ResourceName="BenefitStatusCode" />
            <wasp:GridKeyValueControl DataField="BenefitCoverageCode" OnNeedDataSource="Code_NeedDataSource" Type="BenefitCoverageCode" ResourceName="BenefitCoverageCode" />
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:DateControl ID="EffectiveDate" runat="server" ResourceName="EffectiveDate" Value='<%# Bind("EffectiveDate") %>' ReadOnly="<%# IsUpdate %>" TabIndex="010" Mandatory="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="BenefitPolicyId" runat="server" OnDataBinding="BenefitPolicy_NeedDataSource" ResourceName="BenefitPolicy" Value='<%# Bind("BenefitPolicyId") %>' ReadOnly='<%# IsUpdate %>' TabIndex="020" Mandatory="true" />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="BenefitStatusCode" runat="server" Type="BenefitStatusCode" OnDataBinding="Code_NeedDataSource" ResourceName="BenefitStatusCode" Value='<%# Bind("BenefitStatusCode") %>' ReadOnly='<%# IsUpdate %>' TabIndex="030" Mandatory="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="BenefitCoverageCode" runat="server" Type="BenefitCoverageCode" OnDataBinding="Code_NeedDataSource" ResourceName="BenefitCoverageCode" Value='<%# Bind("BenefitCoverageCode") %>' ReadOnly='<%# IsUpdate %>' TabIndex="040" Mandatory="true" />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="BenefitSmokerCode" runat="server" Type="BenefitSmokerCode" OnDataBinding="Code_NeedDataSource" ResourceName="BenefitSmokerCode" Value='<%# Bind("BenefitSmokerCode") %>' ReadOnly='<%# IsUpdate %>' TabIndex="050" Mandatory="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="EligibilityDate" runat="server" ResourceName="EligibilityDate" Value='<%# Bind("EligibilityDate") %>' ReadOnly="<%# IsUpdate %>" TabIndex="060" Mandatory="false" />
                        </td>
                        <td>
                            <wasp:DateControl ID="EnrolmentDate" runat="server" ResourceName="EnrolmentDate" Value='<%# Bind("EnrolmentDate") %>' ReadOnly="<%# IsUpdate %>" TabIndex="070" Mandatory="false" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# !IsUpdate %>' ResourceName="Insert" />
                                    </td>
                                    <td>
                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>

    </MasterTableView>

</wasp:WLPGrid>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
    </script>
</telerik:RadScriptBlock>
