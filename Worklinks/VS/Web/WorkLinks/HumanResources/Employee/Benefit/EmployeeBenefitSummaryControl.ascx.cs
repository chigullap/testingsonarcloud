﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Employee.Benefit
{
    public partial class EmployeeBenefitSummaryControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }

        public bool IsInsert { get { return EmployeeBenefitSummaryGrid.IsInsertMode; } }

        public bool IsUpdate { get { return EmployeeBenefitSummaryGrid.IsEditMode; } }

        public bool IsViewMode { get { return EmployeeBenefitSummaryGrid.IsViewMode; } }

        public bool AddFlag { get { return Common.Security.RoleForm.EmployeeBenefitSummary.AddFlag; } }

        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeBenefitSummary.UpdateFlag; } }

        public bool DeleteFlag { get { return Common.Security.RoleForm.EmployeeBenefitSummary.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadEmployeeBenefitSummary(EmployeeId);
                Initialize();
            }
        }

        private void LoadEmployeeBenefitSummary(long employeeId)
        {
            Data = EmployeeBenefitCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetEmployeeBenefit(employeeId));
        }

        protected void Initialize()
        {
            //find the EmployeeBenefitSummaryGrid
            WLPGrid grid = (WLPGrid)this.FindControl("EmployeeBenefitSummaryGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        #endregion

        #region event handlers
        protected void EmployeeBenefitSummaryGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EmployeeBenefitSummaryGrid.DataSource = Data;
        }

        protected void BenefitPolicy_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateComboBoxWithBenefitPolicy((ICodeControl)sender, LanguageCode);
        }

        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }

        protected void EmployeeBenefitSummaryGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
        }

        protected void EmployeeBenefitSummaryGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
        }
        #endregion

        #region handle updates
        protected void EmployeeBenefitSummaryGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            EmployeeBenefit item = (EmployeeBenefit)e.Item.DataItem;
            item.EmployeeId = EmployeeId;
            Common.ServiceWrapper.HumanResourcesClient.InsertEmployeeBenefit(item).CopyTo((EmployeeBenefit)Data[item.Key]);
        }

        protected void EmployeeBenefitSummaryGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                EmployeeBenefit item = (EmployeeBenefit)e.Item.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.UpdateEmployeeBenefit(item).CopyTo((EmployeeBenefit)Data[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void EmployeeBenefitSummaryGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteEmployeeBenefit((EmployeeBenefit)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}