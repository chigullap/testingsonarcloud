﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Employee.Benefit
{
    public partial class EmployeeBenefitModuleControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected bool EnableEmployeeBenefitSummaryTab { get { return Common.Security.RoleForm.EmployeeBenefitSummary.ViewFlag; } }

        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }

        private string LastName
        {
            get
            {
                if (Page.RouteData.Values["lastName"] == null)
                    return null;
                else
                    return Page.RouteData.Values["lastName"].ToString();
            }
        }

        private string FirstName
        {
            get
            {
                if (Page.RouteData.Values["firstName"] == null)
                    return null;
                else
                    return Page.RouteData.Values["firstName"].ToString();
            }
        }

        private int FirstVisibleTab
        {
            get
            {
                if (EnableEmployeeBenefitSummaryTab)
                    return 0;

                return -1;
            }
        }

        protected bool IsViewMode
        {
            get
            {
                return EmployeeBenefitSummaryControl.IsViewMode;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            //does the user have access rights?
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EmployeeBenefit);

            if (!Page.IsPostBack)
            {
                //does the user have rights to see this employee?
                SetAuthorized(Common.ServiceWrapper.HumanResourcesClient.GetEmployee(new EmployeeCriteria() { EmployeeId = EmployeeId }).Count > 0);
            }

            this.Page.Title = String.Format(Page.Title, LastName + ", " + FirstName);
        }
        #endregion

        #region event handlers
        protected override void OnPreRender(EventArgs e)
        {
            InitializeControls();
            base.OnPreRender(e);
        }

        protected void InitializeControls()
        {
            EmployeeBenefitModuleTabStrip.Enabled = IsViewMode;
        }

        protected void EmployeeBenefitModuleMultiPage_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                EmployeeBenefitModuleMultiPage.SelectedIndex = FirstVisibleTab;
        }

        protected void EmployeeBenefitModuleTabStrip_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                EmployeeBenefitModuleTabStrip.SelectedIndex = FirstVisibleTab;
        }
        #endregion

        #region security handlers
        protected void EmployeeBenefitSummary_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = ((WebControl)sender).Visible && EnableEmployeeBenefitSummaryTab; }
        #endregion
    }
}