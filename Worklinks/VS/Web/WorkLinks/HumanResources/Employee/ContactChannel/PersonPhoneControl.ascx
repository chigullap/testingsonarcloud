﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PersonPhoneControl.ascx.cs" Inherits="WorkLinks.HumanResources.Employee.ContactChannel.PersonPhoneControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

    function EndRequestHandler(sender, args) {
        if (args.get_error() != undefined && args.get_error().httpStatusCode == '500') {
            var errorMessage = args.get_error();
            args.set_errorHandled(true);
            alert(errorMessage);
        }
    }
</script>

<wasp:WLPGrid
    ID="PersonContactChannelGrid"
    runat="server"
    AutoGenerateColumns="false"
    GridLines="None"
    CommandItemDisplay="Top"
    AutoInsertOnAdd="false"
    OnNeedDataSource="PersonContactChannelGrid_NeedDataSource"
    OnInsertCommand="PersonContactChannelGrid_InsertCommand"
    OnUpdateCommand="PersonContactChannelGrid_UpdateCommand"
    OnDeleteCommand="PersonContactChannelGrid_DeleteCommand"
    OnItemCommand="PersonContactChannelGrid_ItemCommand">

    <MasterTableView DataKeyNames="Key" CommandItemDisplay="Top">
        <SortExpressions>
            <telerik:GridSortExpression FieldName="PrimaryFlag" SortOrder="Descending" />
        </SortExpressions>

        <EditFormSettings>
            <EditColumn UniqueName="EditCommandColumn1"></EditColumn>
        </EditFormSettings>

        <CommandItemTemplate>
            <wasp:WLPToolBar ID="EmployeeSummaryToolBar" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add" CausesValidation="false" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <Columns>
            <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
            <wasp:GridCheckBoxControl DataField="PrimaryFlag" LabelText="PrimaryFlag" SortExpression="PrimaryFlag" UniqueName="PrimaryFlag" ResourceName="PrimaryFlag" />
            <wasp:GridKeyValueControl OnNeedDataSource="PhoneType_NeedDataSource" DataField="ContactChannelTypeCode" Type="ContactChannelTypeCode" ResourceName="ContactChannelTypeCode" />
            <wasp:GridMaskedControl DataFormatString="{0:###-###-####}" DataType="System.Int64" DataField="PrimaryContactValueAsLong" LabelText="PrimaryContactValue" SortExpression="PrimaryContactValue" UniqueName="PrimaryContactValue" ResourceName="PrimaryContactValue" />
            <wasp:GridBoundControl DataField="SecondaryContactValue" LabelText="SecondaryContactValue" SortExpression="SecondaryContactValue" UniqueName="SecondaryContactValue" ResourceName="SecondaryContactValue" />
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:CheckBoxControl ID="PrimaryFlag" runat="server" ResourceName="PrimaryFlag" Value='<%# Bind("PrimaryFlag") %>' ReadOnly="false" OnDataBinding="PrimaryFlag_OnDataBind" TabIndex="010" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="ContactChannelTypeCode" runat="server" Type="ContactChannelTypeCode" OnDataBinding="PhoneTypeEdit_NeedDataSource" ResourceName="ContactChannelTypeCode" Value='<%# Bind("ContactChannelTypeCode") %>' TabIndex="020" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="float: left">
                                <wasp:MaskedControl Mask="###-###-####" ID="PrimaryContactValue" runat="server" ResourceName="PrimaryContactValue" Value='<%# Bind("PrimaryContactValue") %>' ReadOnly="false" TabIndex="030" />
                            </div>
                            <div style="float: left; padding-left: 50px;">
                                <asp:RegularExpressionValidator ID="PhoneLength" runat="server" ForeColor="Red" ControlToValidate="PrimaryContactValue" ErrorMessage="Not Valid" ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$">* Not Valid</asp:RegularExpressionValidator>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="float: left">
                                <wasp:TextBoxControl ID="SecondaryContactValue" runat="server" ResourceName="SecondaryContactValue" Value='<%# Bind("SecondaryContactValue") %>' ReadOnly="false" TabIndex="040" />
                            </div>
                            <div style="float: left; padding-left: 50px;">
                                <asp:RegularExpressionValidator ID="ExtensionLength" runat="server" ForeColor="Red" ControlToValidate="SecondaryContactValue" ErrorMessage="Not Valid" ValidationExpression="^[0-9]{0,9}$">* Not Valid</asp:RegularExpressionValidator>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# !IsUpdate %>' ResourceName="Insert" />
                                    </td>
                                    <td>
                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>
    </MasterTableView>
</wasp:WLPGrid>