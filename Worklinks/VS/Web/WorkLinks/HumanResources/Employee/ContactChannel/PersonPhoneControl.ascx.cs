﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Wizard;

namespace WorkLinks.HumanResources.Employee.ContactChannel
{
    public partial class PersonPhoneControl : ContactChannelControl
    {
        #region fields
        private const String _contactChannelTypeCode = "Phone"; /**HACK**hard code*/
        private const String _personIdKey = "PersonIdKey";
        private long _personId = -1;
        #endregion

        #region properties
        public long PersonId
        {
            get
            {
                if (_personId == -1)
                {
                    Object obj = ViewState[_personIdKey];

                    if (obj != null)
                        _personId = Convert.ToInt64(obj);
                }

                return _personId;
            }
            set
            {
                _personId = value;
                ViewState[_personIdKey] = _personId;
            }
        }
        public bool IsUpdate { get { return PersonContactChannelGrid.IsEditMode; } }
        public bool IsViewMode { get { return PersonContactChannelGrid.IsViewMode; } }
        public bool IsInsert { get { return PersonContactChannelGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.EmployeePhone.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeePhone.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.EmployeePhone.DeleteFlag; } }
        #endregion

        #region main
        protected void LoadPhone(long personId)
        {
            DataItemCollection = PersonContactChannelCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetPersonContactChannel(personId, _contactChannelTypeCode));
            PersonContactChannelGrid.Rebind();
        }
        public void SetPersonId(long personId)
        {
            PersonId = personId;
            LoadPhone(PersonId);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (IsDataExternallyLoaded)
                    PersonContactChannelGrid.Rebind();

                Initialize();
            }
        }
        protected void Initialize()
        {
            //find the PersonContactChannelGrid
            WLPGrid grid = (WLPGrid)this.FindControl("PersonContactChannelGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        #endregion

        #region event handlers
        protected void PersonContactChannelGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            PersonContactChannelGrid.DataSource = DataItemCollection;
        }
        protected void PrimaryFlag_OnDataBind(object sender, EventArgs e)
        {
            CheckBoxControl primaryFlagControl = (CheckBoxControl)FindControlRecursive(PersonContactChannelGrid, "PrimaryFlag");
            SetPersonContactChannelPrimaryFlag(primaryFlagControl, DataItemCollection);

            if (!primaryFlagControl.ReadOnly)
                primaryFlagControl.Focus();
            else
                ((ComboBoxControl)FindControlRecursive(PersonContactChannelGrid, "ContactChannelTypeCode")).Focus();
        }
        protected void PhoneType_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateComboBoxWithPhoneType((ICodeControl)sender, LanguageCode);
        }
        protected void PhoneTypeEdit_NeedDataSource(object sender, EventArgs e)
        {
            bool leaveSelected = false;

            if (IsDataExternallyLoaded)
            {
                if (IsInsert)
                    Common.CodeHelper.PopulateComboBoxWithTypeForWizardRemovingTypesInUse((ICodeControl)sender, DataItemCollection, LanguageCode, this.WizardItemName, leaveSelected);
                else
                {
                    leaveSelected = true;
                    Common.CodeHelper.PopulateComboBoxWithTypeForWizardRemovingTypesInUse((ICodeControl)sender, DataItemCollection, LanguageCode, this.WizardItemName, leaveSelected);
                }
            }
            else
            {
                if (IsInsert || ((ICodeControl)sender).SelectedValue == null)
                    Common.CodeHelper.PopulateComboBoxWithPhoneTypeRemovingTypesInUse((ICodeControl)sender, LanguageCode, PersonId);
                else
                    Common.CodeHelper.PopulateComboBoxWithPhoneTypeRemovingTypesInUseLeavingSelected((ICodeControl)sender, LanguageCode, PersonId, ((ICodeControl)sender).SelectedValue.ToString());
            }
        }
        #endregion

        #region handle updates
        protected void ResetPrimaryFlags(PersonContactChannel pcc, bool isInserting)
        {
            String key = null;

            //Insert from grid is always -1, as we are dealing with negative longs in our CachedItems (they still need to be inserted) 
            //This will trick the ResetPrimaryFlags function into handling the primary check box correctly
            if (IsDataExternallyLoaded && isInserting)
                key = "1";
            else
                key = pcc.Key;

            //if the record we are editing has Primary checked, set the others to not-checked (ie. false)
            if (pcc.PrimaryFlag)
            {
                foreach (WLP.BusinessLayer.BusinessObjects.IDataItem di in DataItemCollection)
                {
                    if (di.Key != key) //if not the current record we are editing
                        if (((PersonContactChannel)di).PrimaryFlag) //if the primaryflag is true
                            ((PersonContactChannel)di).PrimaryFlag = false; //set it to false
                }
            }
        }
        protected void PersonContactChannelGrid_InsertCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            PersonContactChannel channel = (PersonContactChannel)e.Item.DataItem;
            channel.PersonId = PersonId;
            ResetPrimaryFlags(channel, true);

            if (IsDataExternallyLoaded)
            {
                if (DataItemCollection.Count > 0)
                    channel.PersonContactChannelId = ((PersonContactChannel)DataItemCollection[DataItemCollection.Count - 1]).PersonContactChannelId - 1;

                ((PersonContactChannelCollection)DataItemCollection).Add(channel);

                ItemChangedEventArgs args = new ItemChangedEventArgs() { Items = DataItemCollection };
                FormatPhoneNumber(channel);
                OnItemChanged(args);
            }
            else
            {
                FormatPhoneNumber(channel);
                ((WLP.BusinessLayer.BusinessObjects.DataItemCollection<PersonContactChannel>)DataItemCollection).Add(Common.ServiceWrapper.HumanResourcesClient.InsertPersonContactChannel(channel, new System.Collections.Generic.List<PersonContactChannel>((PersonContactChannel[])DataItemCollection.ToArray())));
                LoadPhone(PersonId);
            }
        }
        protected void PersonContactChannelGrid_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                PersonContactChannel channel = (PersonContactChannel)e.Item.DataItem;
                ResetPrimaryFlags(channel, false);

                if (IsDataExternallyLoaded)
                {
                    channel.CopyTo((PersonContactChannel)DataItemCollection[channel.Key]);
                    ItemChangedEventArgs args = new ItemChangedEventArgs() { Items = DataItemCollection };
                    FormatPhoneNumber(channel);
                    OnItemChanged(args);
                }
                else
                {
                    FormatPhoneNumber(channel);
                    Common.ServiceWrapper.HumanResourcesClient.UpdatePersonContactChannel(channel, new System.Collections.Generic.List<PersonContactChannel>((PersonContactChannel[])DataItemCollection.ToArray())).CopyTo((PersonContactChannel)DataItemCollection[channel.Key]);
                    LoadPhone(PersonId);
                }
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FormatPhoneNumber(PersonContactChannel channel)
        {
            ////use StringBuilder for performance so a bunch of Strings aren't created in memory using String.Replace over and over.
            //StringBuilder sb = new StringBuilder(channel.PrimaryContactValue.ToString());

            //sb.Replace("-", "");
            //sb.Replace(" ", "");
            //sb.Replace("(", "");
            //sb.Replace(")", "");

            //String phone = sb.ToString();
            //phone = String.Format("{0}-{1}-{2}", phone.Substring(0, 3), phone.Substring(3, 3), phone.Substring(6, 4));
            //channel.PrimaryContactValue = phone;
        }
        protected void PersonContactChannelGrid_DeleteCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (IsDataExternallyLoaded)
                {
                    ItemChangedEventArgs args = new ItemChangedEventArgs() { Items = DataItemCollection };
                    OnItemChanged(args);
                }
                else
                {
                    PersonContactChannel channel = (PersonContactChannel)e.Item.DataItem;
                    Common.ServiceWrapper.HumanResourcesClient.DeletePersonContactChannel((PersonContactChannel)e.Item.DataItem);
                    LoadPhone(PersonId);
                }
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        protected void PersonContactChannelGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (IsDataExternallyLoaded)
            {
                switch (e.CommandName.ToLower())
                {
                    case "cancel":
                    case "performinsert":
                    case "update":
                        OnItemChangingComplete(null);
                        break;
                    case "edit":
                    case "initinsert":
                        OnItemChanging(null);
                        break;
                }
            }
        }
    }
}