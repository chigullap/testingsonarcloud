﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeSearchControl.ascx.cs" Inherits="WorkLinks.HumanResources.Employee.EmployeeSearchControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<style type="text/css">
    .RadListBox { min-width: 200px !important; min-height: 80px !important; }
</style>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="SearchPanel">
            <UpdatedControls>
                <%--<telerik:AjaxUpdatedControl ControlID="SearchPanel" />--%>
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>


<asp:Panel ID="SearchPanel" runat="server">
    <table width="100%">
        <tr valign="top">
            <td>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="EmployeeNumber" runat="server" ResourceName="EmployeeNumber" ReadOnly="false" TabIndex="010" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="SocialInsuranceNumber" runat="server" ResourceName="SocialInsuranceNumber" ReadOnly="false" TabIndex="040" />
                            </td>
                            <td rowspan="3">
                                <wasp:CheckedListBoxControl ID="EmployeePositionStatusCode" runat="server" ListBoxHeight="80px" ResourceName="CodeEmployeePositionStatusDescription" Type="EmployeePositionStatusCode" ReadOnly="false" OnDataBinding="EmployeePositionStatusCode_NeedDataSource" TabIndex="070" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="LastName" runat="server" ResourceName="LastName" ReadOnly="false" TabIndex="020" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="OrganizationUnitDescription" runat="server" ResourceName="OrganizationUnitDescription" ReadOnly="false" TabIndex="050" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="FirstName" runat="server" ResourceName="FirstName" ReadOnly="false" TabIndex="030" />
                            </td>
                            <td>
                                <wasp:ComboBoxControl ID="PayrollProcessGroupCodeControl" LabelText="Process Group" runat="server" Type="PayrollProcessGroupCode" OnDataBinding="Code_NeedDataSource" ResourceName="PayrollProcessGroupCode" ReadOnly="false" />
                            </td>
                            <td>
                                <wasp:CheckBoxControl ID="IncludeWizards" Visible="false" runat="server" ResourceName="IncludeWizards" LabelText="Include Wizards" ReadOnly="false" TabIndex="080" />
                            </td>
                        </tr>
                    </table>
                    <div class="SearchCriteriaButtons" style="float: left;">
                        <wasp:WLPButton ID="btnClear" Icon-PrimaryIconUrl="~/App_Themes/Default/clear.gif" ResourceName="btnClear" runat="server" Text="Clear" OnClientClicked="clear" OnClick="BtnClear_Click" CssClass="button"/>
                        <wasp:WLPButton ID="btnSearch" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" ResourceName="btnSearch" runat="server" Text="Search" OnClick="BtnSearch_Click"/>
                        <asp:Label ID="lblTransMessage" runat="server" Text="" ForeColor="Red" ClientIDMode="Static" />
                    </div>
                    <div style="float: right">
                        <wasp:WLPButton ID="btnExport" runat="server" ResourceName="btnExport" Text="Export" Icon-PrimaryIconUrl="~\App_Themes\Default\PayReg.gif" OnPreRender="ExportButton_PreRender"/>
                        <wasp:ComboBoxControl 
                            ID="cboEmployeeExportControl" 
                            runat="server" 
                            Text="" 
                            OnDataBinding="Code_NeedDataSource" 
                            OnClientSelectedIndexChanged="onExportIndexChanged"
                            Type="EmployeeExportTypeCode"
                            CssClass="employee-search-export-combo"
                            OnPreRender="cboEmployeeExportControl_OnPreRender"
                            />
                    </div>
                </asp:Panel>
            </td>
        </tr>
        <tr valign="top">
            <td>
                <wasp:WLPToolBar ID="EmployeeSummaryToolBar" runat="server" Width="100%" OnClientLoad="EmployeeSummaryToolBar_init">
                    <Items>
                        <wasp:WLPToolBarButton OnPreRender="Delete_PreRender" Text="Delete" ImageUrl="~\App_Themes\Default\Delete.gif" onclick="Delete()" CommandName="Delete" ResourceName="Delete" />
                        <wasp:WLPToolBarButton OnPreRender="EmployeeDetailsToolBar_PreRender" Text="Details" ImageUrl="~\App_Themes\Default\Employee.gif" onclick="Open()" CommandName="details" ResourceName="Details" />
                        <wasp:WLPToolBarButton OnPreRender="PositionToolBar_PreRender" Text="Position" ImageUrl="~\App_Themes\Default\Position.gif" onclick="OpenEmployeeHistory()" CommandName="employeeHistory" ResourceName="Position" />
                        <wasp:WLPToolBarButton OnPreRender="EmploymentToolBar_PreRender" Text="Employment" ImageUrl="~\App_Themes\Default\Employment.gif" onclick="OpenEmployment()" CommandName="employment" ResourceName="Employment" />
                        <wasp:WLPToolBarButton OnPreRender="ContactsToolBar_PreRender" Text="Contacts" ImageUrl="~\App_Themes\Default\Contact.gif" onclick="OpenContact()" CommandName="contacts" ResourceName="Contacts" />
                        <wasp:WLPToolBarButton OnPreRender="EducationToolBar_PreRender" Text="Education" ImageUrl="~\App_Themes\Default\Competency.gif" onclick="OpenEducation()" CommandName="education" ResourceName="Education" />
                        <wasp:WLPToolBarButton OnPreRender="CompanyPropertyToolBar_PreRender" Text="Company Property" ImageUrl="~\App_Themes\Default\Property.gif" onclick="OpenCompanyProperty()" CommandName="companyProperty" ResourceName="CompanyProperty" />
                        <wasp:WLPToolBarButton OnPreRender="DisciplinaryToolBar_PreRender" Text="Disciplinary" ImageUrl="~\App_Themes\Default\Discipline.gif" onclick="OpenDisciplinaryAction()" CommandName="disciplinaryActions" ResourceName="DisciplinaryActions" />
                        <wasp:WLPToolBarButton OnPreRender="TerminationToolBar_PreRender" Text="Termination" ImageUrl="~\App_Themes\Default\Termination.gif" onclick="OpenTermination()" CommandName="termination" ResourceName="Termination" />
                        <wasp:WLPToolBarButton OnPreRender="WsibHealthAndSafetyReportToolBar_PreRender" Text="WCB Health and Safety" ImageUrl="~\App_Themes\Default\StatDeduction.gif" onclick="OpenWsibHealthAndSafetyReport()" CommandName="wsibHealthAndSafetyReport" ResourceName="WsibHealthAndSafetyReport" />
                        <wasp:WLPToolBarButton OnPreRender="DirectDepositToolBar_PreRender" Text="Direct Deposit" ImageUrl="~\App_Themes\Default\DirectDeposit.gif" onclick="OpenBanking()" CommandName="banking" ResourceName="EmployeeBanking" />
                        <wasp:WLPToolBarButton OnPreRender="StatDeductionsToolBar_PreRender" Text="Stat Deductions" ImageUrl="~\App_Themes\Default\StatDeduction.gif" onclick="OpenEmployeeStatutoryDeductions()" CommandName="employeeStatutoryDeductions" ResourceName="EmployeeStatutoryDeductions" />
                        <wasp:WLPToolBarButton OnPreRender="PaycodesToolBar_PreRender" Text="Paycodes" ImageUrl="~\App_Themes\Default\PayCodes.gif" onclick="OpenPaycodes()" CommandName="paycodes" ResourceName="EmployeePaycodes" />
                        <wasp:WLPToolBarButton OnPreRender="NHTToolBar_PreRender" Text="NHT" ImageUrl="~\App_Themes\Default\NHT.gif" onclick="openNHT()" CommandName="NHT" ResourceName="NHT" />
                        <wasp:WLPToolBarButton OnPreRender="PaySlipToolBar_PreRender" Text="Pay Slips" ImageUrl="~\App_Themes\Default\PaySlips.gif" onclick="openPaySlip()" CommandName="PaySlip" ResourceName="PaySlip" />
                        <wasp:WLPToolBarButton OnPreRender="PaySlipMailingLabelToolBar_PreRender" Text="MailingLabel" ImageUrl="~\App_Themes\Default\Label.gif" onclick="openPaySlipMailingLabel()" CommandName="PaySlipMailingLabel" ResourceName="PaySlipMailingLabel" />
                        <wasp:WLPToolBarButton OnPreRender="BenefitToolBar_PreRender" Text="Benefit" ImageUrl="~\App_Themes\Default\Label.gif" onclick="openBenefit()" CommandName="Benefit" ResourceName="Benefit" />
                    </Items>
                </wasp:WLPToolBar>
                <wasp:WLPGrid
                    ID="EmployeeSummaryGrid"
                    runat="server"
                    AllowPaging="true"
                    PagerStyle-AlwaysVisible="true"
                    PageSize="100"
                    AllowSorting="true"
                    GridLines="None"
                    AutoAssignModifyProperties="true"
                    OnPageIndexChanged="EmployeeSummaryGrid_PageIndexChanged"
                    OnPageSizeChanged="EmployeeSummaryGrid_PageSizeChanged"
                    OnSortCommand="EmployeeSummaryGrid_SortCommand">

                    <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
                        <Scrolling AllowScroll="false" UseStaticHeaders="true" />
                        <Selecting AllowRowSelect="true" />
                        <ClientEvents OnRowDblClick="OnRowDblClick" OnRowClick="OnRowClick" OnGridCreated="onGridCreated" OnCommand="onCommand" />
                    </ClientSettings>

                    <MasterTableView
                        AllowCustomSorting="true"
                        AllowCustomPaging="true"
                        ClientDataKeyNames="EmployeeId, LastName, FirstName, PayrollProcessGroupCode, EmployeeNumber"
                        AutoGenerateColumns="false"
                        DataKeyNames="EmployeeId"
                        CommandItemDisplay="Top">

                        <CommandItemTemplate></CommandItemTemplate>

                        <CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>

                        <Columns>
                            <wasp:GridBoundControl DataField="EmployeeNumber" LabelText="EmployeeNumber" SortExpression="EmployeeNumber" UniqueName="EmployeeNumber" ResourceName="EmployeeNumber" />
                            <wasp:GridBoundControl DataField="LastName" LabelText="LastName" SortExpression="LastName" UniqueName="LastName" ResourceName="LastName" />
                            <wasp:GridBoundControl DataField="FirstName" SortExpression="FirstName" UniqueName="FirstName" ResourceName="FirstName" />
                            <wasp:GridBoundControl DataField="CodeEmployeePositionStatusDescription" SortExpression="CodeEmployeePositionStatusDescription" UniqueName="CodeEmployeePositionStatusDescription" ResourceName="CodeEmployeePositionStatusDescription" />
                            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" SortExpression="PayrollProcessGroupCode" DataField="PayrollProcessGroupCode" Type="PayrollProcessGroupCode" ResourceName="PayrollProcessGroupCode" />
                            <wasp:GridBoundControl DataField="OrganizationUnitDescription" LabelText="OrganizationUnitDescription" SortExpression="OrganizationUnitDescription" UniqueName="OrganizationUnitDescription" ResourceName="OrganizationUnitDescription">
                                <HeaderStyle Width="36%" />
                            </wasp:GridBoundControl>
                        </Columns>

                    </MasterTableView>

                    <HeaderContextMenu EnableAutoScroll="true" />

                </wasp:WLPGrid>
            </td>
        </tr>
    </table>
</asp:Panel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="EmployeeWindows"
    Width="1200"
    Height="700"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    OnClientClose="onClientClose"
    EnableViewState="false"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="PositionWindows"
    Width="1200"
    Height="800"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    OnClientClose="onClientClose"
    EnableViewState="false"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="PayrollWindows"
    Width="1000"
    Height="800"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    OnClientClose="onClientClose"
    EnableViewState="false"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var toolbar;

        (function initializeExport() {
            var btn = document.getElementById('<%= cboEmployeeExportControl.ClientID %>')
            if (btn) {
                var rcbInput = btn.getElementsByClassName('rcbInput')[0];
                if (rcbInput)
                    if (rcbInput.value === "")
                        toggleExportButton(true);
            }
        })();

        function toggleExportButton(disabled) {
            var btn = document.getElementById('<%= btnExport.ClientID %>').getElementsByClassName('rbPrimary')[0];
            if (btn) {
                btn.disabled = disabled;
            }
        }

        function onExportIndexChanged(e, arg) {
            toggleExportButton(e._selectedIndex === 0);
        }

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function clear() {
            employeeId = null;
        }

        function EmployeeSummaryToolBar_init(sender) {
            initializeControls();
        }

        function getSelectedRow() {
            var employeeSummaryGrid = $find('<%= EmployeeSummaryGrid.ClientID %>');
            if (employeeSummaryGrid != null) {
                var MasterTable = employeeSummaryGrid.get_masterTableView();
                if (MasterTable != null)
                    return MasterTable.get_selectedItems()[0];
                else
                    return null;
            }

            return null;
        }

        function rowIsSelected() {
            var getSeletecteRow = getSelectedRow();
            return (getSeletecteRow != null);
        }

        function initializeControls() {
            if ($find('<%= EmployeeSummaryToolBar.ClientID %>') != null)
                toolbar = $find('<%= EmployeeSummaryToolBar.ClientID %>');

            enableButtons(rowIsSelected());
        }

        function OnRowDblClick(sender, eventArgs) {
            if (toolbar.findButtonByCommandName('details') != null)
                Open(sender);
        }

        function onGridCreated(sender, eventArgs) {
            if (rowIsSelected()) {
                rowSelected = true;

                var selectedRow = getSelectedRow();

                employeeId = selectedRow.getDataKeyValue('EmployeeId');
                employeeNumber = selectedRow.getDataKeyValue('EmployeeNumber');
                lastName = selectedRow.getDataKeyValue('LastName');
                firstName = selectedRow.getDataKeyValue('FirstName');
                payrollProcessGroupCode = selectedRow.getDataKeyValue('PayrollProcessGroupCode');

                if (payrollProcessGroupCode == null)
                    payrollProcessGroupCode = 'null';

                enableButtons(true);
            }
            else {
                employeeId = null;
                lastName = "";
                firstName = "";
            }
        }

        function OnRowClick(sender, eventArgs) {
            employeeId = eventArgs.getDataKeyValue('EmployeeId');
            employeeNumber = eventArgs.getDataKeyValue('EmployeeNumber');
            lastName = eventArgs.getDataKeyValue('LastName');
            firstName = eventArgs.getDataKeyValue('FirstName');
            payrollProcessGroupCode = eventArgs.getDataKeyValue('PayrollProcessGroupCode');

            if (payrollProcessGroupCode == null)
                payrollProcessGroupCode = 'null';

            rowSelected = true;

            enableButtons(true);
        }

        function enableButtons(enable) {
            var detailsButton = toolbar.findButtonByCommandName('details');
            var deleteButton = toolbar.findButtonByCommandName('Delete');
            var historyButton = toolbar.findButtonByCommandName('employeeHistory');
            var employmentButton = toolbar.findButtonByCommandName('employment');
            var contactsButton = toolbar.findButtonByCommandName('contacts');
            var educationButton = toolbar.findButtonByCommandName('education');
            var compPropButton = toolbar.findButtonByCommandName('companyProperty');
            var disciplinaryActionButton = toolbar.findButtonByCommandName('disciplinaryActions');
            var terminationButton = toolbar.findButtonByCommandName('termination');
            var wsibHealthAndSafetyReportButton = toolbar.findButtonByCommandName('wsibHealthAndSafetyReport');
            var bankingButton = toolbar.findButtonByCommandName('banking');
            var employeeStatutoryDeductionsButton = toolbar.findButtonByCommandName('employeeStatutoryDeductions');
            var paycodesButton = toolbar.findButtonByCommandName('paycodes');
            var nhtButton = toolbar.findButtonByCommandName('NHT');
            var paySlipButton = toolbar.findButtonByCommandName('PaySlip');
            var paySlipMailingLabelButton = toolbar.findButtonByCommandName('PaySlipMailingLabel');
            var benefitButton = toolbar.findButtonByCommandName('Benefit');
           
            if (detailsButton != null)
                detailsButton.set_enabled(enable);

            if (deleteButton != null)
                deleteButton.set_enabled(enable);

            if (contactsButton != null)
                contactsButton.set_enabled(enable);

            if (disciplinaryActionButton != null)
                disciplinaryActionButton.set_enabled(enable);

            if (educationButton != null)
                educationButton.set_enabled(enable);

            if (historyButton != null)
                historyButton.set_enabled(enable);

            if (employmentButton != null)
                employmentButton.set_enabled(enable);

            if (terminationButton != null)
                terminationButton.set_enabled(enable);

            if (wsibHealthAndSafetyReportButton != null)
                wsibHealthAndSafetyReportButton.set_enabled(enable);

            if (compPropButton != null)
                compPropButton.set_enabled(enable);

            if (bankingButton != null)
                bankingButton.set_enabled(enable);

            if (employeeStatutoryDeductionsButton != null)
                employeeStatutoryDeductionsButton.set_enabled(enable);

            if (paycodesButton != null)
                paycodesButton.set_enabled(enable);

            if (nhtButton != null)
                nhtButton.set_enabled(enable);

            if (paySlipButton != null)
                paySlipButton.set_enabled(enable);

            if (paySlipMailingLabelButton != null)
                paySlipMailingLabelButton.set_enabled(enable);

            if (benefitButton != null)
                benefitButton.set_enabled(enable);
        }

        function Open() {
            if (employeeId != null) {
                if (employeeId < 0)
                    openWindowWithManager('EmployeeWindows', String.format('/HumanResources/Employee/Edit/{0}', employeeId), false);
                else
                    openWindowWithManager('EmployeeWindows', String.format('/HumanResources/Employee/View/{0}/{1}/{2}', employeeId, lastName, firstName), false);
            }
        }

        function Delete() {
            if (employeeId != null) {
                var message = "<asp:Literal runat="server" Text="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />";
                if (confirm(message))
                    __doPostBack('<%= this.ClientID %>', String.format('deleteEmployeeId={0}', employeeId));
            }
        }

        function OpenContact() {
            if (employeeId != null) {
                if (employeeId < 0)
                    Open();
                else
                    openWindowWithManager('EmployeeWindows', String.format('/HumanResources/Contact/{0}/{1}/{2}', employeeId, lastName, firstName), false);
            }
        }

        function OpenEmployment() {
            if (employeeId != null) {
                if (employeeId < 0)
                    Open();
                else
                    openWindowWithManager('PayrollWindows', String.format('/HumanResources/Employment/{0}/{1}/{2}', employeeId, lastName, firstName), false);
            }
        }

        function OpenEducation() {
            if (employeeId != null) {
                if (employeeId < 0)
                    Open();
                else
                    openWindowWithManager('EmployeeWindows', String.format('/HumanResources/Education/{0}/{1}/{2}', employeeId, lastName, firstName), false);
            }
        }

        function OpenDisciplinaryAction() {
            if (employeeId != null) {
                if (employeeId < 0)
                    Open();
                else
                    openWindowWithManager('PayrollWindows', String.format('/HumanResources/EmployeeDiscipline/{0}/{1}/{2}', employeeId, lastName, firstName), false);
            }
        }

        function OpenContactType() {
            if (employeeId != null) {
                if (employeeId < 0)
                    Open();
                else
                    openWindowWithManager('EmployeeWindows', String.format('/HumanResources/ContactType/{0}/{1}', 'EMER', employeeId), false);
            }
        }

        function OpenEmployeeHistory() {
            if (employeeId != null) {
                if (employeeId < 0)
                    Open();
                else
                    openWindowWithManager('PositionWindows', String.format('/HumanResources/EmployeePosition/{0}/{1}/{2}/{3}', employeeId, lastName, firstName, payrollProcessGroupCode), false);
            }
        }

        function OpenCompanyProperty() {
            if (employeeId != null) {
                if (employeeId < 0)
                    Open();
                else
                    openWindowWithManager('PayrollWindows', String.format('/HumanResources/CompanyProperty/{0}/{1}/{2}', employeeId, lastName, firstName), false);
            }
        }

        function OpenTermination() {
            if (employeeId != null) {
                if (employeeId < 0)
                    Open();
                else
                    openWindowWithManager('PayrollWindows', String.format('/HumanResources/Terminations/{0}/{1}/{2}/{3}', employeeId, lastName, firstName, "x"), false);
            }
        }

        function OpenWsibHealthAndSafetyReport() {
            if (employeeId != null) {
                if (employeeId < 0)
                    Open();
                else
                    openWindowWithManager('EmployeeWindows', String.format('/HumanResources/WsibHealthAndSafetyReport/{0}/{1}/{2}', employeeId, lastName, firstName), false);
            }
        }

        function OpenBanking() {
            if (employeeId != null) {
                if (employeeId < 0)
                    Open();
                else
                    openWindowWithManager('PayrollWindows', String.format('/Payroll/Banking/EmployeeBankingPage/{0}/{1}/{2}', employeeId, lastName, firstName), false);
            }
        }

        function OpenPaycodes() {
            if (employeeId != null) {
                if (employeeId < 0)
                    Open();
                else
                    openWindowWithManager('EmployeeWindows', String.format('/Payroll/Paycode/EmployeePaycodePage/{0}/{1}/{2}', employeeId, lastName, firstName), false);
            }
        }

        function openNHT() {
            if (employeeId != null) {
                if (employeeId < 0)
                    Open();
                else
                    openWindowWithManager('EmployeeWindows', String.format('/Report/NationalHousingTrust/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}', -1, employeeNumber, 'PDF', 'x', 'x', 'x', 'x', employeeId), true);
            }
        }

        function openPaySlip() {
            if (employeeId != null) {
                if (employeeId < 0)
                    Open();
                else
                    openWindowWithManager('EmployeeWindows', String.format('/Payroll/PaySlip/EmployeePaySlipPage/{0}/{1}/{2}/{3}', employeeId, employeeNumber, lastName, firstName), true);
            }
        }

        function OpenEmployeeStatutoryDeductions() {
            if (employeeId != null) {
                if (employeeId < 0)
                    Open();
                else
                    openWindowWithManager('PayrollWindows', String.format('/Payroll/StatutoryDeduction/{0}/{1}/{2}', employeeId, lastName, firstName), false);
            }
        }

        function openPaySlipMailingLabel() {
            if (employeeId != null) {
                if (employeeId < 0)
                    Open();
                else
                    openWindowWithManager('PayrollWindows', String.format('/Report/PaySlipMailingLabel/{0}/{1}/{2}/{3}/{4}/{5}/{6}', -1, employeeNumber, 'PDF', 'x', 'x', 'x', 'x'), true);
            }
        }

        function openBenefit() {
            if (employeeId != null) {
                if (employeeId < 0)
                    Open();
                else
                    openWindowWithManager('EmployeeWindows', String.format('/HumanResources/Benefit/{0}/{1}/{2}', employeeId, lastName, firstName), false);
            }
        }

        function RowSelected() {
            return rowSelected;
        }

        function onCommand(sender, args) {
            if (args.get_commandName() == 'Page') {
                employeeId = null;
                initializeControls();
            }
        }

        function onClientClose(sender, eventArgs) {
            var arg = sender.argument;
            if (arg != null && arg.closeWithChanges)
                __doPostBack('<%= SearchPanel.ClientID %>', String.format('refreshEmployee={0}', arg.employeeId));
        }

    
</script>
</telerik:RadScriptBlock>