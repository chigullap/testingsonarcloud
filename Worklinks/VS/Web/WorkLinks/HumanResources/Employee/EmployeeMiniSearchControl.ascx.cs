﻿using System;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Employee
{
    public partial class EmployeeMiniSearchControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected EmployeeCriteria Criteria
        {
            get
            {
                EmployeeCriteria criteria = new EmployeeCriteria();
                criteria.EmployeeNumber = (String)EmployeeNumber.Value;
                criteria.LastName = (String)LastName.Value;
                criteria.SocialInsuranceNumber = (String)SIN.Value;
                criteria.PayrollProcessGroupCode = (String)PayrollProcessGroupCodeControl.Value;
                criteria.LanguageCode = LanguageCode;

                return criteria;
            }
            set
            {
                EmployeeNumber.Value = value.EmployeeId;
                LastName.Value = value.LastName;
                SIN.Value = value.SocialInsuranceNumber;
                PayrollProcessGroupCodeControl.Value = value.PayrollProcessGroupCode;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            EmployeeSummaryGrid.NeedDataSource += new Telerik.Web.UI.GridNeedDataSourceEventHandler(EmployeeSummaryGrid_NeedDataSource);

            this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "EmployeeMiniSearch"));

            if (!IsPostBack)
            {
                if (Request["payrollProcessGroupCode"] != null)
                {
                    PayrollProcessGroupCodeControl.Value = Request["payrollProcessGroupCode"];
                    PayrollProcessGroupCodeControl.ReadOnly = true;
                }

                Panel1.DataBind();
            }
        }
        void EmployeeSummaryGrid_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            EmployeeSummaryGrid.DataSource = Data;
        }
        public void Search(EmployeeCriteria criteria)
        {
            EmployeeSummaryCollection collection = new EmployeeSummaryCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetEmployeeSummary(criteria));
            Data = collection;
            EmployeeSummaryGrid.Rebind();
        }
        #endregion

        #region event handlers
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search(Criteria);
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Criteria = new EmployeeCriteria();
            Data = null;
            EmployeeSummaryGrid.DataBind();
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        #endregion
    }
}