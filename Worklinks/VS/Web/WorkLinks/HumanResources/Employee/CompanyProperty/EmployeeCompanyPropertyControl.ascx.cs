﻿using System;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Wizard;

namespace WorkLinks.HumanResources.Employee.CompanyProperty
{
    public partial class EmployeeCompanyPropertyControl : WizardUserControl
    {
        #region properties
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        private string LastName { get { return Page.RouteData.Values["lastName"].ToString(); } }
        private string FirstName { get { return Page.RouteData.Values["firstName"].ToString(); } }
        public bool IsViewMode { get { return EmployeeCompanyPropertyGrid.IsViewMode; } }
        public bool IsUpdate { get { return EmployeeCompanyPropertyGrid.IsEditMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.EmployeeProperty.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeProperty.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.EmployeeProperty.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            //does the user have access rights?
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EmployeeProperty.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                LoadEmployeeEducation(EmployeeId);
                Initialize();
            }

            if (!IsDataExternallyLoaded)
                this.Page.Title = String.Format("Company Property - {0}, {1}", LastName, FirstName);
        }
        private void WireEvents()
        {
            EmployeeCompanyPropertyGrid.NeedDataSource += new GridNeedDataSourceEventHandler(EmployeeCompanyPropertyGrid_NeedDataSource);
            EmployeeCompanyPropertyGrid.ItemDataBound += new GridItemEventHandler(EmployeeCompanyPropertyGrid_ItemDataBound);
        }
        protected void Initialize()
        {
            //find the EmployeeCompanyPropertyGrid
            WLPGrid grid = (WLPGrid)this.FindControl("EmployeeCompanyPropertyGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        private void LoadEmployeeEducation(long employeeId)
        {
            if (!IsDataExternallyLoaded)
                DataItemCollection = EmployeeCompanyPropertyCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetEmployeeCompanyProperty(employeeId));
            else
                EmployeeCompanyPropertyGrid.Rebind();
        }
        #endregion

        #region event handlers
        protected void EmployeeCompanyPropertyGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EmployeeCompanyPropertyGrid.DataSource = DataItemCollection;
        }
        protected void EmployeeCompanyPropertyGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                if (IsDataExternallyLoaded && TemplateWizardFlag)
                {
                    if ((WLPButton)e.Item.FindControl("btnUpdate") != null)
                        ((WLPButton)e.Item.FindControl("btnUpdate")).CausesValidation = false;

                    if ((WLPButton)e.Item.FindControl("btnInsert") != null)
                        ((WLPButton)e.Item.FindControl("btnInsert")).CausesValidation = false;
                }
            }
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        public override void Update(bool updateExterallyControlled)
        {
        }
        public override void AddNewDataItem()
        {
            if (DataItemCollection == null)
                DataItemCollection = new EmployeeCompanyPropertyCollection();
        }
        public override void ChangeModeEdit()
        {
        }
        #endregion

        #region handle updates
        protected void EmployeeCompanyPropertyGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (IsDataExternallyLoaded)
                {
                    ItemChangedEventArgs args = new ItemChangedEventArgs() { Items = DataItemCollection };
                    OnItemChanged(args);
                }
                else
                    Common.ServiceWrapper.HumanResourcesClient.DeleteEmployeeCompanyProperty((EmployeeCompanyProperty)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void EmployeeCompanyPropertyGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            EmployeeCompanyProperty companyProperty = (EmployeeCompanyProperty)e.Item.DataItem;
            companyProperty.EmployeeId = EmployeeId;

            if (IsDataExternallyLoaded)
            {
                if (DataItemCollection.Count > 0)
                    companyProperty.EmployeeCompanyPropertyId = ((EmployeeCompanyProperty)DataItemCollection[DataItemCollection.Count - 1]).EmployeeCompanyPropertyId - 1;

                ItemChangedEventArgs args = new ItemChangedEventArgs() { Items = DataItemCollection };
                ((EmployeeCompanyPropertyCollection)DataItemCollection).Add(companyProperty);
                OnItemChanged(args);
            }
            else
                ((DataItemCollection<EmployeeCompanyProperty>)DataItemCollection).Add(Common.ServiceWrapper.HumanResourcesClient.InsertEmployeeCompanyProperty(companyProperty));
        }
        protected void EmployeeCompanyPropertyGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                EmployeeCompanyProperty companyProperty = (EmployeeCompanyProperty)e.Item.DataItem;

                if (IsDataExternallyLoaded)
                {
                    companyProperty.CopyTo((EmployeeCompanyProperty)DataItemCollection[companyProperty.Key]);
                    ItemChangedEventArgs args = new ItemChangedEventArgs() { Items = DataItemCollection };
                    OnItemChanged(args);
                }
                else
                    Common.ServiceWrapper.HumanResourcesClient.UpdateEmployeeCompanyProperty(companyProperty).CopyTo((EmployeeCompanyProperty)DataItemCollection[companyProperty.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        protected void EmployeeCompanyPropertyGrid_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "cancel":
                case "performinsert":
                case "update":
                    OnItemChangingComplete(null);
                    break;
                case "edit":
                case "initinsert":
                    OnItemChanging(null);
                    break;
            }
        }
    }
}