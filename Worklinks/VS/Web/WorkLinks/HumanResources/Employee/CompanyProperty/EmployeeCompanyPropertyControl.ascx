﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeCompanyPropertyControl.ascx.cs" Inherits="WorkLinks.HumanResources.Employee.CompanyProperty.EmployeeCompanyPropertyControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="PropertyPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="PropertyPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="PropertyPanel" runat="server">
    <wasp:WLPGrid
        ID="EmployeeCompanyPropertyGrid"
        runat="server"
        GridLines="None"
        AutoGenerateColumns="false"
        AutoInsertOnAdd="false"
        OnDeleteCommand="EmployeeCompanyPropertyGrid_DeleteCommand"
        OnInsertCommand="EmployeeCompanyPropertyGrid_InsertCommand"
        OnUpdateCommand="EmployeeCompanyPropertyGrid_UpdateCommand"
        OnItemCommand="EmployeeCompanyPropertyGrid_ItemCommand">

        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
            <Selecting AllowRowSelect="false" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
            <CommandItemTemplate>
                <wasp:WLPToolBar ID="EmployeeSummaryToolBar" runat="server" Width="100%" AutoPostBack="true">
                    <Items>
                        <wasp:WLPToolBarButton Text="Add" CommandName="InitInsert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add" />
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="EmployeeCompanyPropertyCode" LabelText="**EmployeeCompanyPropertyCode" Type="EmployeeCompanyPropertyCode" ResourceName="EmployeeCompanyPropertyCode" />
                <wasp:GridBoundControl DataField="Description" LabelText="**Description" SortExpression="Description" UniqueName="Description" ResourceName="Description" />
                <wasp:GridDateTimeControl DataField="IssueDate" DataType="System.DateTime" LabelText="**IssueDate" SortExpression="IssueDate" UniqueName="IssueDate" ResourceName="IssueDate" />
                <wasp:GridDateTimeControl DataField="ReturnDate" DataType="System.DateTime" LabelText="**ReturnDate" SortExpression="ReturnDate" UniqueName="ReturnDate" ResourceName="ReturnDate" />
                <wasp:GridDateTimeControl DataField="ReissueDate" DataType="System.DateTime" LabelText="**ReissueDate" SortExpression="ReissueDate" UniqueName="ReissueDate" ResourceName="ReissueDate" />
                <wasp:GridDateTimeControl DataField="ExpiryDate" DataType="System.DateTime" LabelText="**ExpiryDate" SortExpression="ExpiryDate" UniqueName="ExpiryDate" ResourceName="ExpiryDate" />
                <wasp:GridNumericControl DataField="Cost" DataType="System.Int64" DataFormatString="{0:$###,##0.00}" LabelText="**Cost**" SortExpression="Cost" UniqueName="Cost" ResourceName="Cost" />
                <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="EmployeeCompanyPropertyCode" runat="server" Type="EmployeeCompanyPropertyCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmployeeCompanyPropertyCode" Value='<%# Bind("EmployeeCompanyPropertyCode") %>' Mandatory="true" TabIndex="010" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="Description" runat="server" ResourceName="Description" Value='<%# Bind("Description") %>' Mandatory="true" TabIndex="020" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:DateControl ID="IssueDate" runat="server" ResourceName="IssueDate" Value='<%# Bind("IssueDate") %>' Mandatory="true" TabIndex="030" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="IssueCondition" runat="server" ResourceName="IssueCondition" Value='<%# Bind("IssueCondition") %>' TabIndex="040" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:DateControl ID="ReturnDate" runat="server" ResourceName="ReturnDate" Value='<%# Bind("ReturnDate") %>' TabIndex="050" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="ReturnCondition" runat="server" ResourceName="ReturnCondition" Value='<%# Bind("ReturnCondition") %>' TabIndex="060" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:DateControl ID="ReissueDate" runat="server" ResourceName="ReissueDate" Value='<%# Bind("ReissueDate") %>' TabIndex="070" />
                            </td>
                            <td>
                                <wasp:DateControl ID="ExpiryDate" runat="server" ResourceName="ExpiryDate" Value='<%# Bind("ExpiryDate") %>' TabIndex="080" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:NumericControl ID="Cost" runat="server" DecimalDigits="2" ResourceName="Cost" Value='<%# Bind("Cost") %>' TabIndex="090" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="Notes" runat="server" ResourceName="Notes" Value='<%# Bind("Notes") %>' TabIndex="100" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                            <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# !IsUpdate %>' ResourceName="Insert" />
                                        </td>
                                        <td>
                                            <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true" />

    </wasp:WLPGrid>
</asp:Panel>