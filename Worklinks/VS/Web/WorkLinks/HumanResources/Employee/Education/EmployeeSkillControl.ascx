﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeSkillControl.ascx.cs" Inherits="WorkLinks.HumanResources.Employee.Education.EmployeeSkillControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<wasp:WLPGrid
    ID="EmployeeSkillsGrid"
    runat="server"
    GridLines="None"
    AutoGenerateColumns="false"
    AutoInsertOnAdd="false"
    AutoAssignModifyProperties="true"
    OnDeleteCommand="EmployeeSkillsGrid_DeleteCommand"
    OnInsertCommand="EmployeeSkillsGrid_InsertCommand"
    OnUpdateCommand="EmployeeSkillsGrid_UpdateCommand"
    OnItemCommand="EmployeeSkillsGrid_ItemCommand"
    OnItemCreated="EmployeeSkillsGrid_ItemCreated"
    OnItemDataBound="EmployeeSkillsGrid_ItemDataBound">

    <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
        <Selecting AllowRowSelect="false" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">

        <CommandItemTemplate>
            <wasp:WLPToolBar ID="EmployeeSummaryToolBar" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="Add" CommandName="InitInsert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <Columns>
            <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="EmployeeSkillCode" Type="EmployeeSkillCode" ResourceName="EmployeeSkillCode" />
            <wasp:GridDateTimeControl DataField="DateAcquired" DataType="System.DateTime" LabelText="DateAcquired" SortExpression="DateAcquired" UniqueName="DateAcquired" ResourceName="DateAcquired" />
            <wasp:GridDateTimeControl DataField="ExpiryDate" DataType="System.DateTime" LabelText="ExpiryDate" SortExpression="ExpiryDate" UniqueName="ExpiryDate" ResourceName="ExpiryDate" />
            <wasp:GridCheckBoxControl DataField="JobRelatedFlag" LabelText="JobRelatedFlag" SortExpression="JobRelatedFlag" UniqueName="JobRelatedFlag" ResourceName="JobRelatedFlag" />
            <wasp:GridCheckBoxControl DataField="JobRequiredFlag" LabelText="JobRequiredFlag" SortExpression="JobRequiredFlag" UniqueName="JobRequiredFlag" ResourceName="JobRequiredFlag" />
            <telerik:GridTemplateColumn UniqueName="TemplateAttachmentColumn" HeaderText="Attachment">
                <ItemTemplate>
                    <asp:HyperLink ID="AttachmentLink" runat="server" Text="Link" Visible="false" />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table width="50%" align="left">
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="EmployeeSkillCode" runat="server" Type="EmployeeSkillCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmployeeSkillCode" Value='<%# Bind("EmployeeSkillCode") %>' TabIndex="010" Mandatory="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="DateAcquired" runat="server" ResourceName="DateAcquired" Value='<%# Bind("DateAcquired") %>' ReadOnly="false" TabIndex="020" Mandatory="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="ExpiryDate" runat="server" ResourceName="ExpiryDate" Value='<%# Bind("ExpiryDate") %>' ReadOnly="false" TabIndex="030" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:CheckBoxControl ID="JobRelatedFlag" runat="server" ResourceName="JobRelatedFlag" Value='<%# Bind("JobRelatedFlag") %>' ReadOnly="false" TabIndex="040" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:CheckBoxControl ID="JobRequiredFlag" runat="server" ResourceName="JobRequiredFlag" Value='<%# Bind("JobRequiredFlag") %>' ReadOnly="false" TabIndex="050" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# !IsUpdate %>' ResourceName="Insert" />
                                    </td>
                                    <td>
                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table align="right">
                    <tr>
                        <td>
                            <telerik:RadAsyncUpload runat="server" ID="UploadAttachment" MaxFileInputsCount="1" HideFileInput="true" OnClientValidationFailed="OnClientValidationFailed" AllowedFileExtensions="<%# AllowedFileExtensions %>" />
                            <wasp:WLPButton runat="server" ID="RemoveAttachment" ResourceName="RemoveAttachment" OnClick="RemoveAttachment_Click" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl Visible="false" runat="server" ID="Description" ResourceName="Description" Value='<%# Bind("Description") %>' ReadOnly="false" TabIndex="080" />
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>
    </MasterTableView>
</wasp:WLPGrid>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="SkillAttachmentWindows"
    Width="800"
    Height="500"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function ShowAttachment(attachmentId) {
            openWindowWithManager('SkillAttachmentWindows', String.format('/Attachment/{0}', attachmentId), false);
            return false;
        }

        function OnClientValidationFailed(sender, args) {
            var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
            if (args.get_fileName().lastIndexOf('.') != -1) {
                if (sender.get_allowedFileExtensions().indexOf(fileExtention) == -1) {
                    alert("Invalid file selected.");
                    sender.deleteFileInputAt(0);
                }
            }
        }
    </script>
</telerik:RadScriptBlock>