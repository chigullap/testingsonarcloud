﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeAwardHonourControl.ascx.cs" Inherits="WorkLinks.HumanResources.Employee.Education.EmployeeAwardHonourControl" ResourceName="EmployeeAwardHonourControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<wasp:WLPGrid
    ID="EmployeeAwardHonourGrid"
    runat="server"
    AutoAssignModifyProperties="true"
    AutoGenerateColumns="false"
    GridLines="None"
    OnDeleteCommand="EmployeeAwardHonourGrid_DeleteCommand"
    OnInsertCommand="EmployeeAwardHonourGrid_InsertCommand"
    OnNeedDataSource="EmployeeAwardHonourGrid_NeedDataSource"
    OnUpdateCommand="EmployeeAwardHonourGrid_UpdateCommand"
    OnItemCreated="EmployeeAwardHonourGrid_ItemCreated"
    OnItemDataBound="EmployeeAwardHonourGrid_ItemDataBound">

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">

        <RowIndicatorColumn>
            <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <CommandItemTemplate>
            <wasp:WLPToolBar ID="EmployeeAwardHonourToolBar" runat="server" AutoPostBack="true" Width="100%">
                <Items>
                    <wasp:WLPToolBarButton CommandName="InitInsert" ResourceName="Add" ImageUrl="~/App_Themes/Default/Add.gif" Text="Add" Visible="<%# IsViewMode && AddFlag %>" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <ExpandCollapseColumn>
            <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>

        <Columns>
            <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="EmployeeAwardHonourTypeCode" Type="EmployeeAwardHonourTypeCode" ResourceName="EmployeeAwardHonourTypeCode" />
            <wasp:GridDateTimeControl DataField="IssueDate" DataType="System.DateTime" LabelText="IssueDate" SortExpression="IssueDate" UniqueName="IssueDate" ResourceName="IssueDate" />
            <wasp:GridBoundControl DataField="Grantor" LabelText="MailingLabel" SortExpression="MailingLabel" UniqueName="Grantor" ResourceName="Grantor" />
            <telerik:GridTemplateColumn UniqueName="TemplateAttachmentColumn" HeaderText="Attachment">
                <ItemTemplate>
                    <asp:HyperLink ID="AttachmentLink" runat="server" Text="Link" Visible="false" />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="EmployeeAwardHonourTypeCode" runat="server" Type="EmployeeAwardHonourTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmployeeAwardHonourTypeCode" Value='<%# Bind("EmployeeAwardHonourTypeCode") %>' ReadOnly='<%# IsUpdate %>' TabIndex="010" Mandatory="true" />
                        </td>
                        <td>
                            <telerik:RadAsyncUpload runat="server" ID="UploadAttachment" MaxFileInputsCount="1" HideFileInput="true" OnClientValidationFailed="OnClientValidationFailed" AllowedFileExtensions="<%# AllowedFileExtensions %>" />
                            <wasp:WLPButton runat="server" ID="RemoveAttachment" ResourceName="RemoveAttachment" OnClick="RemoveAttachment_Click" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="IssueDate" runat="server" ResourceName="IssueDate" Value='<%# Bind("IssueDate") %>' ReadOnly="false" TabIndex="020" Mandatory="true" />
                        </td>
                        <td rowspan="2" valign="top">
                            <wasp:TextBoxControl visible="false" runat="server" ID="Description" ResourceName="Description" Value='<%# Bind("Description") %>' ReadOnly="false" TabIndex="030" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="Grantor" runat="server" ResourceName="Grantor" Value='<%# Bind("Grantor") %>' ReadOnly="false" TabIndex="040" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="Notes" runat="server" ResourceName="Notes" Value='<%# Bind("Notes") %>' ReadOnly="false" TabIndex="050" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# !IsUpdate %>' ResourceName="Insert" />
                                    </td>
                                    <td>
                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>
    </MasterTableView>
</wasp:WLPGrid>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="AwardAttachmentWindows"
    Width="800"
    Height="500"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function ShowAttachment(attachmentId) {
            openWindowWithManager('AwardAttachmentWindows', String.format('/Attachment/{0}', attachmentId), false);
            return false;
        }

        function OnClientValidationFailed(sender, args) {
            var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
            if (args.get_fileName().lastIndexOf('.') != -1) {
                if (sender.get_allowedFileExtensions().indexOf(fileExtention) == -1) {
                    alert("Invalid file selected.");
                    sender.deleteFileInputAt(0);
                }
            }
        }
    </script>
</telerik:RadScriptBlock>