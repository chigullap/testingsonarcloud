﻿using System;
using System.Web.UI.WebControls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Employee.Education
{
    public partial class EmployeeEducationModuleControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected bool EnableEmployeeEducationTab { get { return Common.Security.RoleForm.EmployeeEducationDetail.ViewFlag; } }
        protected bool EnableEmployeeSkillTab { get { return Common.Security.RoleForm.EmployeeSkill.ViewFlag; } }
        protected bool EnableEmployeeLicenseTab { get { return Common.Security.RoleForm.EmployeeLicense.ViewFlag; } }
        protected bool EnableEmployeeAwardsTab { get { return Common.Security.RoleForm.EmployeeAwards.ViewFlag; } }
        protected bool EnableEmployeeCourseTab { get { return Common.Security.RoleForm.EmployeeCourse.ViewFlag; } }
        protected bool EnableEmployeeMembershipTab { get { return Common.Security.RoleForm.EmployeeMembership.ViewFlag; } }
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        private string LastName
        {
            get
            {
                if (Page.RouteData.Values["lastName"] == null)
                    return null;
                else
                    return Page.RouteData.Values["lastName"].ToString();
            }
        }
        private string FirstName
        {
            get
            {
                if (Page.RouteData.Values["firstName"] == null)
                    return null;
                else
                    return Page.RouteData.Values["firstName"].ToString();
            }
        }
        private int FirstVisibleTab
        {
            get
            {
                if (EnableEmployeeEducationTab)
                    return 0;
                if (EnableEmployeeSkillTab)
                    return 1;
                if (EnableEmployeeLicenseTab)
                    return 2;
                if (EnableEmployeeAwardsTab)
                    return 3;
                if (EnableEmployeeCourseTab)
                    return 4;
                if (EnableEmployeeMembershipTab)
                    return 5;

                return -1;
            }
        }
        protected bool IsViewMode
        {
            get
            {
                return EmployeeEducationControl.IsViewMode && EmployeeSkillControl.IsViewMode && EmployeeLicenseCertificateControl.IsViewMode &&
                    EmployeeAwardHonourControl.IsViewMode && EmployeeCourseControl.IsViewMode && EmployeeMembershipControl.IsViewMode;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            //does the user have access rights?
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EmployeeEducation);

            if (!Page.IsPostBack)
            {
                //does the user have rights to see this employee?
                SetAuthorized(Common.ServiceWrapper.HumanResourcesClient.GetEmployee(new EmployeeCriteria() { EmployeeId = EmployeeId }).Count > 0);
            }

            this.Page.Title = String.Format(Page.Title, LastName + ", " + FirstName);
        }
        #endregion

        #region event handlers
        protected override void OnPreRender(EventArgs e)
        {
            InitializeControls();
            base.OnPreRender(e);
        }
        protected void InitializeControls()
        {
            EmployeeEducationModuleTabStrip.Enabled = IsViewMode;
        }
        protected void EmployeeEducationModuleMultiPage_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                EmployeeEducationModuleMultiPage.SelectedIndex = FirstVisibleTab;
        }
        protected void EmployeeEducationModuleTabStrip_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                EmployeeEducationModuleTabStrip.SelectedIndex = FirstVisibleTab;
        }
        #endregion

        #region security handlers
        protected void EmployeeEducation_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = ((WebControl)sender).Visible && EnableEmployeeEducationTab; }
        protected void EmployeeSkill_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = ((WebControl)sender).Visible && EnableEmployeeSkillTab; }
        protected void EmployeeCertificate_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = ((WebControl)sender).Visible && EnableEmployeeLicenseTab; }
        protected void EmployeeAward_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = ((WebControl)sender).Visible && EnableEmployeeAwardsTab; }
        protected void EmployeeCourse_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = ((WebControl)sender).Visible && EnableEmployeeCourseTab; }
        protected void EmployeeMembership_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = ((WebControl)sender).Visible && EnableEmployeeMembershipTab; }
        #endregion
    }
}