﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeCourseControl.ascx.cs" Inherits="WorkLinks.HumanResources.Employee.Education.EmployeeCourseControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<wasp:WLPGrid
    ID="EmployeeCourseGrid"
    runat="server"
    GridLines="None"
    AutoGenerateColumns="false"
    AutoInsertOnAdd="false"
    OnDeleteCommand="EmployeeCourseGrid_DeleteCommand"
    OnInsertCommand="EmployeeCourseGrid_InsertCommand"
    OnUpdateCommand="EmployeeCourseGrid_UpdateCommand"
    OnItemCommand="EmployeeCourseGrid_ItemCommand"
    OnItemCreated="EmployeeCourseGrid_ItemCreated"
    OnItemDataBound="EmployeeCourseGrid_ItemDataBound">

    <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
        <Selecting AllowRowSelect="false" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">

        <CommandItemTemplate>
            <wasp:WLPToolBar ID="EmployeeCourseToolBar" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="Add" CommandName="InitInsert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <Columns>
            <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="EmployeeCourseNameCode" Type="EmployeeCourseNameCode" ResourceName="EmployeeCourseNameCode" />
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="EmployeeCourseTypeCode" Type="EmployeeCourseTypeCode" ResourceName="EmployeeCourseTypeCode" />
            <wasp:GridDateTimeControl DataField="CompletionDate" LabelText="CompletionDate" SortExpression="CompletionDate" UniqueName="CompletionDate" ResourceName="CompletionDate" />
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="EmployeeCourseResultCode" Type="EmployeeCourseResultCode" ResourceName="EmployeeCourseResultCode" />
            <wasp:GridCheckBoxControl DataField="JobRequiredFlag" LabelText="JobRequiredFlag" UniqueName="JobRequiredFlag" ResourceName="JobRequiredFlag" />
            <telerik:GridTemplateColumn UniqueName="TemplateAttachmentColumn" HeaderText="Attachment">
                <ItemTemplate>
                    <asp:HyperLink ID="AttachmentLink" runat="server" Text="Link" Visible="false" />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="EmployeeCourseNameCode" runat="server" Type="EmployeeCourseNameCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmployeeCourseNameCode" Value='<%# Bind("EmployeeCourseNameCode") %>' Mandatory="true" TabIndex="010" />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="EmployeeCourseTypeCode" runat="server" Type="EmployeeCourseTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmployeeCourseTypeCode" Value='<%# Bind("EmployeeCourseTypeCode") %>' TabIndex="020" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="RegistrationDate" runat="server" ResourceName="RegistrationDate" Value='<%# Bind("RegistrationDate") %>' TabIndex="030" />
                        </td>
                        <td>
                            <wasp:DateControl ID="StartDate" runat="server" ResourceName="StartDate" Value='<%# Bind("StartDate") %>' TabIndex="040" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:CheckBoxControl ID="AttendedFlag" runat="server" ResourceName="AttendedFlag" Value='<%# Bind("AttendedFlag") %>' TabIndex="050" />
                        </td>
                        <td>
                            <wasp:CheckBoxControl ID="CertificateIssued" runat="server" ResourceName="CertificateIssued" Value='<%# Bind("CertificateIssued") %>' TabIndex="060" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:NumericControl ID="Duration" runat="server" ResourceName="Duration" Value='<%# Bind("Duration") %>' DecimalDigits="2" TabIndex="070" />
                        </td>
                        <td>
                            <wasp:NumericControl ID="Mark" runat="server" ResourceName="Mark" Value='<%# Bind("Mark") %>' DecimalDigits="2" TabIndex="080" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:NumericControl ID="EmployeeTuitionAmount" runat="server" ResourceName="EmployeeTuitionAmount" Value='<%# Bind("EmployeeTuitionAmount") %>' DecimalDigits="2" TabIndex="090" />
                        </td>
                        <td>
                            <wasp:NumericControl ID="EmployerTuitionAmount" runat="server" ResourceName="EmployerTuitionAmount" Value='<%# Bind("EmployerTuitionAmount") %>' DecimalDigits="2" TabIndex="100" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:NumericControl ID="EmployeeOtherAmount" runat="server" ResourceName="EmployeeOtherAmount" Value='<%# Bind("EmployeeOtherAmount") %>' DecimalDigits="2" TabIndex="110" />
                        </td>
                        <td>
                            <wasp:NumericControl ID="EmployerOtherAmount" runat="server" ResourceName="EmployerOtherAmount" Value='<%# Bind("EmployerOtherAmount") %>' DecimalDigits="2" TabIndex="120" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="CourseExpirationDate" runat="server" ResourceName="CourseExpirationDate" Value='<%# Bind("CourseExpirationDate") %>' TabIndex="130" />
                        </td>
                        <td>
                            <wasp:DateControl ID="CompletionDate" runat="server" ResourceName="CompletionDate" Value='<%# Bind("CompletionDate") %>' TabIndex="140" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:CheckBoxControl ID="CompletedFlag" runat="server" ResourceName="CompletedFlag" Value='<%# Bind("CompletedFlag") %>' TabIndex="150" />
                        </td>
                        <td>
                            <wasp:CheckBoxControl ID="JobRequiredFlag" runat="server" ResourceName="JobRequiredFlag" Value='<%# Bind("JobRequiredFlag") %>' TabIndex="160" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="EmployeeCourseResultCode" runat="server" Type="EmployeeCourseResultCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmployeeCourseResultCode" Value='<%# Bind("EmployeeCourseResultCode") %>' TabIndex="170" />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="EmployeeCoursePenaltyCode" runat="server" Type="EmployeeCoursePenaltyCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmployeeCoursePenaltyCode" Value='<%# Bind("EmployeeCoursePenaltyCode") %>' TabIndex="180" />
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <wasp:TextBoxControl ID="Note" runat="server" ResourceName="Note" Value='<%# Bind("Note") %>' Rows="4" Columns="200" TextMode="MultiLine" TabIndex="190" />
                        </td>
                        <td>
                            <wasp:TextBoxControl ID="AlternateAttendant" runat="server" ResourceName="AlternateAttendant" Value='<%# Bind("AlternateAttendant") %>' TabIndex="200" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadAsyncUpload runat="server" ID="UploadAttachment" MaxFileInputsCount="1" HideFileInput="true" OnClientValidationFailed="OnClientValidationFailed" AllowedFileExtensions="<%# AllowedFileExtensions %>" />
                            <wasp:WLPButton runat="server" ID="RemoveAttachment" ResourceName="RemoveAttachment" OnClick="RemoveAttachment_Click" Visible="false" />
                        </td>
                        <td>
                            <wasp:TextBoxControl Visible="false" runat="server" ID="Description" ResourceName="Description" Value='<%# Bind("Description") %>' TabIndex="210" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# !IsUpdate %>' ResourceName="Insert" />
                                    </td>
                                    <td>
                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>
    </MasterTableView>
    
    <HeaderContextMenu EnableAutoScroll="true" />

</wasp:WLPGrid>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="CourseAttachmentWindows"
    Width="800"
    Height="500"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function ShowAttachment(attachmentId) {
            openWindowWithManager('CourseAttachmentWindows', String.format('/Attachment/{0}', attachmentId), false);
            return false;
        }

        function OnClientValidationFailed(sender, args) {
            var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
            if (args.get_fileName().lastIndexOf('.') != -1) {
                if (sender.get_allowedFileExtensions().indexOf(fileExtention) == -1) {
                    alert("Invalid file selected.");
                    sender.deleteFileInputAt(0);
                }
            }
        }
    </script>
</telerik:RadScriptBlock>