﻿using System;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Employee.Education
{
    public partial class EmployeeCourseControl : Wizard.WizardUserControl
    {
        #region fields
        protected static bool removeAttachmentClicked = false;
        #endregion

        #region properties
        public String[] AllowedFileExtensions { get { return Common.CodeHelper.PopulateAllowedFileExtensions(LanguageCode); } }
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        public bool IsViewMode { get { return EmployeeCourseGrid.IsViewMode; } }
        public bool IsUpdate { get { return EmployeeCourseGrid.IsEditMode; } }
        public bool IsInsert { get { return EmployeeCourseGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.EmployeeCourse.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeCourse.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.EmployeeCourse.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();

            if (!IsPostBack)
            {
                LoadEmployeeCourse(EmployeeId);
                Initialize();
            }
        }
        protected void WireEvents()
        {
            EmployeeCourseGrid.NeedDataSource += new GridNeedDataSourceEventHandler(EmployeeCourseGrid_NeedDataSource);
        }
        protected void Initialize()
        {
            //find the EmployeeCourseGrid
            WLPGrid grid = (WLPGrid)this.FindControl("EmployeeCourseGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        protected void LoadEmployeeCourse(long employeeId)
        {
            if (!IsDataExternallyLoaded)
                DataItemCollection = EmployeeCourseCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetEmployeeCourse(employeeId));
            else
                EmployeeCourseGrid.Rebind();
        }
        #endregion

        #region event handlers
        void EmployeeCourseGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EmployeeCourseGrid.DataSource = DataItemCollection;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        public override void Update(bool updateExterallyControlled)
        {
        }
        public override void AddNewDataItem()
        {
            if (DataItemCollection == null) DataItemCollection = new EmployeeCourseCollection();
        }
        public override void ChangeModeEdit()
        {
        }
        protected void EmployeeCourseGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "cancel":
                case "performinsert":
                case "update":
                    OnItemChangingComplete(null);
                    break;
                case "edit":
                case "initinsert":
                    OnItemChanging(null);
                    break;
            }
        }
        protected void EmployeeCourseGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            GridTemplateColumn attachmentColumn = (GridTemplateColumn)EmployeeCourseGrid.MasterTableView.GetColumn("TemplateAttachmentColumn");
            if (attachmentColumn != null)
                attachmentColumn.HeaderText = String.Format("{0}", GetGlobalResourceObject("PageContent", "Attachment"));

            if (e.Item is GridDataItem && e.Item.DataItem != null)
            {
                long? attachmentId = ((EmployeeCourse)e.Item.DataItem).AttachmentId;
                if ((attachmentId != null && attachmentId != -1) || (attachmentId != null && IsDataExternallyLoaded))
                {
                    System.Web.UI.WebControls.HyperLink attachmentLink = (System.Web.UI.WebControls.HyperLink)e.Item.FindControl("AttachmentLink");
                    attachmentLink.Visible = true;
                    attachmentLink.Attributes["href"] = "javascript:void(0);";
                    attachmentLink.Attributes["onclick"] = String.Format("return ShowAttachment('{0}');", attachmentId);
                }
            }
        }
        protected void EmployeeCourseGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditFormItem item = e.Item as GridEditFormItem;

                RadAsyncUpload uploadPhoto = (RadAsyncUpload)item.FindControl("UploadAttachment");
                uploadPhoto.Localization.Select = String.Format("{0}", GetGlobalResourceObject("Messages", "AttachEmployeePhoto"));
                uploadPhoto.Localization.Remove = String.Format("{0}", GetGlobalResourceObject("Messages", "RemoveEmployeePhoto"));

                if (!(e.Item is GridEditFormInsertItem))
                {
                    //only display the clear button if an attachment exists
                    WLPButton removeAttachment = (WLPButton)item.FindControl("RemoveAttachment");
                    if (removeAttachment != null && ((EmployeeCourse)item.DataItem).AttachmentObjectCollection != null)
                        removeAttachment.Visible = true;
                }
            }
        }
        protected void RemoveAttachment_Click(object sender, EventArgs e)
        {
            removeAttachmentClicked = true;
            ((WLPButton)sender).Visible = false;
        }
        #endregion

        #region handle updates
        protected void PopulateAttachmentObjectCollection(EmployeeCourse item, GridCommandEventArgs e)
        {
            RadAsyncUpload uploadControl = (RadAsyncUpload)e.Item.FindControl("UploadAttachment");
            TextBoxControl descriptionControl = (TextBoxControl)e.Item.FindControl("Description");

            if (uploadControl.UploadedFiles.Count > 0 && !removeAttachmentClicked)
            {
                //get the uploaded file from the UploadAttachment control and the description from the Description control
                UploadedFile file = uploadControl.UploadedFiles[0];
                byte[] bytes = new byte[file.ContentLength];
                file.InputStream.Read(bytes, 0, bytes.Length);

                //populate the AttachmentObjectCollection object
                item.AttachmentObjectCollection = new AttachmentCollection();
                item.AttachmentObjectCollection.AddNew();
                item.AttachmentObjectCollection[0].Data = bytes;
                item.AttachmentObjectCollection[0].FileName = file.GetNameWithoutExtension();
                item.AttachmentObjectCollection[0].FileTypeCode = file.GetExtension().Replace(".", "").ToUpper();
                item.AttachmentObjectCollection[0].Description = descriptionControl.Value != null ? descriptionControl.Value.ToString() : "";
            }
            else if (removeAttachmentClicked)
            {
                item.AttachmentId = null;
                item.AttachmentObjectCollection = null;
            }
        }
        protected void EmployeeCourseGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            EmployeeCourse item = (EmployeeCourse)e.Item.DataItem;
            item.EmployeeId = EmployeeId;
            PopulateAttachmentObjectCollection(item, e);

            if (IsDataExternallyLoaded)
            {
                if (DataItemCollection.Count > 0)
                    item.EmployeeCourseId = ((EmployeeCourse)DataItemCollection[DataItemCollection.Count - 1]).EmployeeCourseId - 1;

                Wizard.ItemChangedEventArgs args = new Wizard.ItemChangedEventArgs() { Items = DataItemCollection };
                ((EmployeeCourseCollection)DataItemCollection).Add(item);
                OnItemChanged(args);
            }
            else
                ((DataItemCollection<EmployeeCourse>)DataItemCollection).Add(Common.ServiceWrapper.HumanResourcesClient.InsertEmployeeCourse(item));
        }
        protected void EmployeeCourseGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                EmployeeCourse item = (EmployeeCourse)e.Item.DataItem;
                PopulateAttachmentObjectCollection(item, e);

                if (IsDataExternallyLoaded)
                {
                    Wizard.ItemChangedEventArgs args = new Wizard.ItemChangedEventArgs() { Items = DataItemCollection };
                    OnItemChanged(args);
                }
                else
                    Common.ServiceWrapper.HumanResourcesClient.UpdateEmployeeCourse(item).CopyTo((EmployeeCourse)DataItemCollection[item.Key]);

                removeAttachmentClicked = false;
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void EmployeeCourseGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (IsDataExternallyLoaded)
                {
                    Wizard.ItemChangedEventArgs args = new Wizard.ItemChangedEventArgs() { Items = DataItemCollection };
                    OnItemChanged(args);
                }
                else
                    Common.ServiceWrapper.HumanResourcesClient.DeleteEmployeeCourse((EmployeeCourse)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}