﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Employee.Education
{
    public partial class EmployeeAwardHonourControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        protected static bool removeAttachmentClicked = false;
        #endregion

        #region properties
        public String[] AllowedFileExtensions { get { return Common.CodeHelper.PopulateAllowedFileExtensions(LanguageCode); } }
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        public bool IsInsert { get { return EmployeeAwardHonourGrid.IsInsertMode; } }
        public bool IsUpdate { get { return EmployeeAwardHonourGrid.IsEditMode; } }
        public bool IsViewMode { get { return EmployeeAwardHonourGrid.IsViewMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.EmployeeAwards.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeAwards.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.EmployeeAwards.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadEmployeeAwardHonour(EmployeeId);
                Initialize();
            }
        }
        protected void Initialize()
        {
            //find the EmployeeAwardHonourGrid
            WLPGrid grid = (WLPGrid)this.FindControl("EmployeeAwardHonourGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        private void LoadEmployeeAwardHonour(long employeeId)
        {
            Data = EmployeeAwardHonourCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetEmployeeAwardHonour(employeeId));
        }
        #endregion

        #region event handlers
        protected void EmployeeAwardHonourGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EmployeeAwardHonourGrid.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void EmployeeAwardHonourGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            GridTemplateColumn attachmentColumn = (GridTemplateColumn)EmployeeAwardHonourGrid.MasterTableView.GetColumn("TemplateAttachmentColumn");
            if (attachmentColumn != null)
                attachmentColumn.HeaderText = String.Format("{0}", GetGlobalResourceObject("PageContent", "Attachment"));

            if (e.Item is GridDataItem && e.Item.DataItem != null)
            {
                long? attachmentId = ((EmployeeAwardHonour)e.Item.DataItem).AttachmentId;
                if (attachmentId != null && attachmentId != -1)
                {
                    System.Web.UI.WebControls.HyperLink attachmentLink = (System.Web.UI.WebControls.HyperLink)e.Item.FindControl("AttachmentLink");
                    attachmentLink.Visible = true;
                    attachmentLink.Attributes["href"] = "javascript:void(0);";
                    attachmentLink.Attributes["onclick"] = String.Format("return ShowAttachment('{0}');", attachmentId);
                }
            }
        }
        protected void EmployeeAwardHonourGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditFormItem item = e.Item as GridEditFormItem;

                RadAsyncUpload uploadPhoto = (RadAsyncUpload)item.FindControl("UploadAttachment");
                uploadPhoto.Localization.Select = String.Format("{0}", GetGlobalResourceObject("Messages", "AttachEmployeePhoto"));
                uploadPhoto.Localization.Remove = String.Format("{0}", GetGlobalResourceObject("Messages", "RemoveEmployeePhoto"));

                if (!(e.Item is GridEditFormInsertItem))
                {
                    //only display the clear button if an attachment exists
                    WLPButton removeAttachment = (WLPButton)item.FindControl("RemoveAttachment");
                    if (removeAttachment != null && ((EmployeeAwardHonour)item.DataItem).AttachmentObjectCollection != null)
                        removeAttachment.Visible = true;
                }
            }
        }
        protected void RemoveAttachment_Click(object sender, EventArgs e)
        {
            removeAttachmentClicked = true;
            ((WLPButton)sender).Visible = false;
        }
        #endregion

        #region handle updates
        protected void PopulateAttachmentObjectCollection(EmployeeAwardHonour item, GridCommandEventArgs e)
        {
            RadAsyncUpload uploadControl = (RadAsyncUpload)e.Item.FindControl("UploadAttachment");
            TextBoxControl descriptionControl = (TextBoxControl)e.Item.FindControl("Description");

            if (uploadControl.UploadedFiles.Count > 0 && !removeAttachmentClicked)
            {
                //get the uploaded file from the UploadAttachment control and the description from the Description control
                UploadedFile file = uploadControl.UploadedFiles[0];
                byte[] bytes = new byte[file.ContentLength];
                file.InputStream.Read(bytes, 0, bytes.Length);

                //populate the AttachmentObjectCollection object
                item.AttachmentObjectCollection = new AttachmentCollection();
                item.AttachmentObjectCollection.AddNew();
                item.AttachmentObjectCollection[0].Data = bytes;
                item.AttachmentObjectCollection[0].FileName = file.GetNameWithoutExtension();
                item.AttachmentObjectCollection[0].FileTypeCode = file.GetExtension().Replace(".", "").ToUpper();
                item.AttachmentObjectCollection[0].Description = descriptionControl.Value != null ? descriptionControl.Value.ToString() : "";
            }
            else if (removeAttachmentClicked)
            {
                item.AttachmentId = null;
                item.AttachmentObjectCollection = null;
            }
        }
        protected void EmployeeAwardHonourGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            EmployeeAwardHonour item = (EmployeeAwardHonour)e.Item.DataItem;
            item.EmployeeId = EmployeeId;
            PopulateAttachmentObjectCollection(item, e);
            Common.ServiceWrapper.HumanResourcesClient.InsertEmployeeAwardHonour(item).CopyTo((EmployeeAwardHonour)Data[item.Key]);
        }
        protected void EmployeeAwardHonourGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                EmployeeAwardHonour item = (EmployeeAwardHonour)e.Item.DataItem;
                PopulateAttachmentObjectCollection(item, e);
                Common.ServiceWrapper.HumanResourcesClient.UpdateEmployeeAwardHonour(item).CopyTo((EmployeeAwardHonour)Data[item.Key]);
                removeAttachmentClicked = false;
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void EmployeeAwardHonourGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteEmployeeAwardHonour((EmployeeAwardHonour)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}