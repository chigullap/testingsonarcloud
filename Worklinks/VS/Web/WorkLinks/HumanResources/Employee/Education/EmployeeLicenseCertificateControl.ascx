﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeLicenseCertificateControl.ascx.cs" Inherits="WorkLinks.HumanResources.Employee.Education.EmployeeLicenseCertificateControl" ResourceName="EmployeeLicenseCertificateControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<wasp:WLPGrid
    ID="LicenseCertificateGrid"
    runat="server"
    AutoGenerateColumns="false"
    GridLines="None"
    AutoAssignModifyProperties="true"
    AutoInsertOnAdd="false"
    OnNeedDataSource="LicenseCertificateGrid_NeedDataSource"
    OnDeleteCommand="LicenseCertificateGrid_DeleteCommand"
    OnUpdateCommand="LicenseCertificateGrid_UpdateCommand"
    OnInsertCommand="LicenseCertificateGrid_InsertCommand"
    OnItemCommand="LicenseCertificateGrid_ItemCommand"
    OnItemCreated="LicenseCertificateGrid_ItemCreated"
    OnItemDataBound="LicenseCertificateGrid_ItemDataBound">

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
        <CommandItemTemplate>
            <wasp:WLPToolBar ID="EmployeeSummaryToolBar" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <RowIndicatorColumn>
            <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn>
            <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>

        <Columns>
            <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="**EmployeeLicenseCertificateCode**" DataField="EmployeeLicenseCertificateCode" Type="EmployeeLicenseCertificateCode" ResourceName="EmployeeLicenseCertificateCode" />
            <wasp:GridBoundControl DataField="EmployeeLicenseCertificateNumber" LabelText="**EmployeeLicenseCertificateNumber**" SortExpression="EmployeeLicenseCertificateNumber" UniqueName="EmployeeLicenseCertificateNumber" ResourceName="EmployeeLicenseCertificateNumber" />
            <wasp:GridDateTimeControl DataField="DateIssued" LabelText="**DateIssued**" SortExpression="DateIssued" UniqueName="DateIssued" ResourceName="DateIssued" />
            <wasp:GridDateTimeControl DataField="ExpiryDate" LabelText="**ExpiryDate**" SortExpression="ExpiryDate" UniqueName="ExpiryDate" ResourceName="ExpiryDate" />
            <wasp:GridCheckBoxControl DataField="JobRelated" LabelText="**JobRelated**" SortExpression="JobRelated" UniqueName="JobRelated" ResourceName="JobRelated" />
            <wasp:GridCheckBoxControl DataField="JobRequired" LabelText="**JobRequired**" UniqueName="JobRequired" ResourceName="JobRequired" />
            <telerik:GridTemplateColumn UniqueName="TemplateAttachmentColumn" HeaderText="Attachment">
                <ItemTemplate>
                    <asp:HyperLink ID="AttachmentLink" runat="server" Text="Link" Visible="false" />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table width="50%" align="left">
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="EmployeeLicenseCertificateCode" runat="server" Type="EmployeeLicenseCertificateCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmployeeLicenseCertificateCode" Value='<%# Bind("EmployeeLicenseCertificateCode") %>' ReadOnly='<%# IsUpdate %>' TabIndex="010" Mandatory="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="EmployeeLicenseCertificateNumber" runat="server" ResourceName="EmployeeLicenseCertificateNumber" Value='<%# Bind("EmployeeLicenseCertificateNumber") %>' ReadOnly='<%# IsUpdate %>' TabIndex="020" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="DateIssued" runat="server" ResourceName="DateIssued" Value='<%# Bind("DateIssued") %>' ReadOnly="false" TabIndex="030" Mandatory="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="ExpiryDate" runat="server" ResourceName="ExpiryDate" Value='<%# Bind("ExpiryDate") %>' ReadOnly="false" TabIndex="040" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:NumericControl ID="CostEmployee" runat="server" DecimalDigits="2" ResourceName="CostEmployee" Value='<%# Bind("CostEmployee") %>' ReadOnly="False" TabIndex="050" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:NumericControl ID="CostEmployer" runat="server" DecimalDigits="2" ResourceName="CostEmployer" Value='<%# Bind("CostEmployer") %>' ReadOnly="False" TabIndex="060" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:CheckBoxControl ID="JobRelated" runat="server" ResourceName="JobRelated" Value='<%# Bind("JobRelated") %>' ReadOnly="false" TabIndex="070" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:CheckBoxControl ID="JobRequired" runat="server" ResourceName="JobRequired" Value='<%# Bind("JobRequired") %>' ReadOnly="false" TabIndex="080" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# !IsUpdate %>' ResourceName="Insert" />
                                    </td>
                                    <td>
                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table align="right">
                    <tr>
                        <td>
                            <telerik:RadAsyncUpload runat="server" ID="UploadAttachment" MaxFileInputsCount="1" HideFileInput="true" OnClientValidationFailed="OnClientValidationFailed" AllowedFileExtensions="<%# AllowedFileExtensions %>" />
                            <wasp:WLPButton runat="server" ID="RemoveAttachment" ResourceName="RemoveAttachment" OnClick="RemoveAttachment_Click" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl Visible="false" runat="server" ID="Description" ResourceName="Description" Value='<%# Bind("Description") %>' ReadOnly="false" TabIndex="080" />
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>
    </MasterTableView>
</wasp:WLPGrid>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="LicenseAttachmentWindows"
    Width="800"
    Height="500"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function ShowAttachment(attachmentId) {
            openWindowWithManager('LicenseAttachmentWindows', String.format('/Attachment/{0}', attachmentId), false);
            return false;
        }

        function OnClientValidationFailed(sender, args) {
            var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
            if (args.get_fileName().lastIndexOf('.') != -1) {
                if (sender.get_allowedFileExtensions().indexOf(fileExtention) == -1) {
                    alert("Invalid file selected.");
                    sender.deleteFileInputAt(0);
                }
            }
        }
    </script>
</telerik:RadScriptBlock>