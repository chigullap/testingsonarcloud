﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeEducationControl.ascx.cs" Inherits="WorkLinks.HumanResources.Employee.Education.EmployeeEducationControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<wasp:WLPGrid
    ID="EmployeeEducationGrid"
    runat="server"
    GridLines="None"
    AutoGenerateColumns="false"
    AutoInsertOnAdd="false"
    OnDeleteCommand="EmployeeEducationGrid_DeleteCommand"
    OnInsertCommand="EmployeeEducationGrid_InsertCommand"
    OnUpdateCommand="EmployeeEducationGrid_UpdateCommand"
    OnItemCommand="EmployeeEducationGrid_ItemCommand"
    OnItemCreated="EmployeeEducationGrid_ItemCreated"
    OnItemDataBound="EmployeeEducationGrid_ItemDataBound">

    <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
        <Selecting AllowRowSelect="false" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">

        <CommandItemTemplate>
            <wasp:WLPToolBar ID="EmployeeSummaryToolBar" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <Columns>
            <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="EmployeeEducationDegreeCode" Type="EmployeeEducationDegreeCode" ResourceName="EmployeeEducationDegreeCode" />
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="EmployeeEducationMajorCode" Type="EmployeeEducationMajorCode" ResourceName="EmployeeEducationMajorCode" />
            <wasp:GridBoundControl DataField="School" LabelText="School" SortExpression="School" UniqueName="School" ResourceName="School" />
            <wasp:GridBoundControl DataField="YearEarned" LabelText="YearEarned" SortExpression="YearEarned" UniqueName="YearEarned" ResourceName="YearEarned" />
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="CountryCode" Type="CountryCode" ResourceName="CountryCode" />
            <telerik:GridTemplateColumn UniqueName="TemplateAttachmentColumn" HeaderText="Attachment">
                <ItemTemplate>
                    <asp:HyperLink ID="AttachmentLink" runat="server" Text="Link" Visible="false" />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table width="50%" align="left">
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="EmployeeEducationDegreeCode" runat="server" Type="EmployeeEducationDegreeCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmployeeEducationDegreeCode" Value='<%# Bind("EmployeeEducationDegreeCode") %>' TabIndex="010" Mandatory="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="EmployeeEducationMajorCode" runat="server" Type="EmployeeEducationMajorCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmployeeEducationMajorCode" Value='<%# Bind("EmployeeEducationMajorCode") %>' TabIndex="020" Mandatory="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="School" runat="server" ResourceName="School" Value='<%# Bind("School") %>' ReadOnly="false" TabIndex="030" Mandatory="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="float: left">
                                <wasp:NumericControl ID="YearEarned" runat="server" ResourceName="YearEarned" Value='<%# Bind("YearEarned") %>' ReadOnly="false" MaxLength="4" GroupSeparator="" DecimalDigits="0" TabIndex="040" />
                            </div>
                            <div style="float: left; padding-left: 50px;">
                                <asp:RangeValidator CultureInvariantValues="true" ID="YearEarnedrange" Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="YearEarned" ResourceName="YearEarnedrange" Text='Year must be between 1940 and the current year.' MinimumValue="1940" MaximumValue='<%# DateTime.Now.Year %>' Type="Integer" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="CountryCode" runat="server" Type="CountryCode" OnDataBinding="Code_NeedDataSource" ResourceName="CountryCode" Value='<%# Bind("CountryCode") %>' TabIndex="050" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:NumericControl ID="CreditHour" runat="server" ResourceName="CreditHour" Value='<%# Bind("CreditHour") %>' ReadOnly="false" TabIndex="060" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="EmployeeEducationCreditHourCode" runat="server" Type="EmployeeEducationCreditHourCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmployeeEducationCreditHourCode" Value='<%# Bind("EmployeeEducationCreditHourCode") %>' TabIndex="070" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:CheckBoxControl ID="JobRelatedFlag" runat="server" ResourceName="JobRelatedFlag" Value='<%# Bind("JobRelatedFlag") %>' TabIndex="080" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:CheckBoxControl ID="JobRequiredFlag" runat="server" ResourceName="JobRequiredFlag" Value='<%# Bind("JobRequiredFlag") %>' TabIndex="090" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# !IsUpdate %>' ResourceName="Insert" />
                                    </td>
                                    <td>
                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table align="right">
                    <tr>
                        <td>
                            <telerik:RadAsyncUpload runat="server" ID="UploadAttachment" MaxFileInputsCount="1" HideFileInput="true" OnClientValidationFailed="OnClientValidationFailed" AllowedFileExtensions="<%# AllowedFileExtensions %>" />
                            <wasp:WLPButton runat="server" ID="RemoveAttachment" ResourceName="RemoveAttachment" OnClick="RemoveAttachment_Click" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl Visible="false" runat="server" ID="Description" ResourceName="Description" Value='<%# Bind("Description") %>' ReadOnly="false" TabIndex="080" />
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>
    </MasterTableView>

    <HeaderContextMenu EnableAutoScroll="true" />

</wasp:WLPGrid>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="EducationAttachmentWindows"
    Width="800"
    Height="500"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function ShowAttachment(attachmentId) {
            openWindowWithManager('EducationAttachmentWindows', String.format('/Attachment/{0}', attachmentId), false);
            return false;
        }

        function OnClientValidationFailed(sender, args) {
            var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
            if (args.get_fileName().lastIndexOf('.') != -1) {
                if (sender.get_allowedFileExtensions().indexOf(fileExtention) == -1) {
                    alert("Invalid file selected.");
                    sender.deleteFileInputAt(0);
                }
            }
        }
    </script>
</telerik:RadScriptBlock>