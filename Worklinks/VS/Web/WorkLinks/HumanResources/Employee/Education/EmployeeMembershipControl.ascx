﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeMembershipControl.ascx.cs" Inherits="WorkLinks.HumanResources.Employee.Education.EmployeeMembershipControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<wasp:WLPGrid
    ID="EmployeeMembershipGrid"
    runat="server"
    GridLines="None"
    AutoGenerateColumns="false"
    AutoInsertOnAdd="false"
    OnDeleteCommand="EmployeeMembershipGrid_DeleteCommand"
    OnInsertCommand="EmployeeMembershipGrid_InsertCommand"
    OnUpdateCommand="EmployeeMembershipGrid_UpdateCommand"
    OnItemCommand="EmployeeMembershipGrid_ItemCommand">

    <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
        <Selecting AllowRowSelect="false" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">

        <CommandItemTemplate>
            <wasp:WLPToolBar ID="EmployeeMembershipToolBar" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="**Add**" CommandName="InitInsert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add"></wasp:WLPToolBarButton>
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <Columns>
            <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif"></wasp:GridEditCommandControl>
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="EmployeeMembershipCode" LabelText="**EmployeeMembershipCode" Type="EmployeeMembershipCode" ResourceName="EmployeeMembershipCode"></wasp:GridKeyValueControl>
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="EmployeeAssociationCode" LabelText="**EmployeeAssociationCode" Type="EmployeeAssociationCode" ResourceName="EmployeeAssociationCode"></wasp:GridKeyValueControl>
            <wasp:GridDateTimeControl DataField="StartDate" LabelText="**StartDate**" SortExpression="StartDate" UniqueName="StartDate" ResourceName="StartDate"></wasp:GridDateTimeControl>
            <wasp:GridDateTimeControl DataField="ExpiryDate" LabelText="**ExpiryDate**" SortExpression="ExpiryDate" UniqueName="ExpiryDate" ResourceName="ExpiryDate"></wasp:GridDateTimeControl>
            <wasp:GridCheckBoxControl DataField="JobRelatedFlag" LabelText="**JobRelatedFlag**" UniqueName="JobRelatedFlag" ResourceName="JobRelatedFlag"></wasp:GridCheckBoxControl>
            <wasp:GridCheckBoxControl DataField="JobRequiredFlag" LabelText="**JobRequiredFlag**" UniqueName="JobRequiredFlag" ResourceName="JobRequiredFlag"></wasp:GridCheckBoxControl>
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>"></wasp:GridButtonControl>
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="EmployeeMembershipCode" runat="server" Type="EmployeeMembershipCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmployeeMembershipCode" Value='<%# Bind("EmployeeMembershipCode") %>' Mandatory="true" TabIndex="010"></wasp:ComboBoxControl>
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="EmployeeAssociationCode" runat="server" Type="EmployeeAssociationCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmployeeAssociationCode" Value='<%# Bind("EmployeeAssociationCode") %>' Mandatory="true" TabIndex="020"></wasp:ComboBoxControl>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="StartDate" runat="server" ResourceName="StartDate" Value='<%# Bind("StartDate") %>' TabIndex="030"></wasp:DateControl>
                        </td>
                        <td>
                            <wasp:DateControl ID="ExpiryDate" runat="server" ResourceName="ExpiryDate" Value='<%# Bind("ExpiryDate") %>' TabIndex="040"></wasp:DateControl>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:NumericControl ID="EmployerCost" runat="server" ResourceName="EmployerCost" Value='<%# Bind("EmployerCost") %>' DecimalDigits="2" TabIndex="050"></wasp:NumericControl>
                        </td>
                        <td>
                            <wasp:NumericControl ID="EmployeeCost" runat="server" ResourceName="EmployeeCost" Value='<%# Bind("EmployeeCost") %>' DecimalDigits="2" TabIndex="060"></wasp:NumericControl>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:CheckBoxControl ID="JobRelatedFlag" runat="server" ResourceName="JobRelatedFlag" Value='<%# Bind("JobRelatedFlag") %>' TabIndex="070"></wasp:CheckBoxControl>
                        </td>
                        <td>
                            <wasp:CheckBoxControl ID="JobRequiredFlag" runat="server" ResourceName="JobRequiredFlag" Value='<%# Bind("JobRequiredFlag") %>' TabIndex="080"></wasp:CheckBoxControl>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="**Update**" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update"></wasp:WLPButton>
                                        <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="**Insert**" runat="server" CommandName="PerformInsert" Visible='<%# !IsUpdate %>' ResourceName="Insert"></wasp:WLPButton>
                                    </td>
                                    <td>
                                        <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="**Cancel**" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel"></wasp:WLPButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>
    </MasterTableView>
    
    <HeaderContextMenu EnableAutoScroll="true"></HeaderContextMenu>

</wasp:WLPGrid>