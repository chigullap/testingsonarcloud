﻿using System;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Employee.Education
{
    public partial class EmployeeMembershipControl : Wizard.WizardUserControl
    {
        #region properties
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        public bool IsViewMode { get { return EmployeeMembershipGrid.IsViewMode; } }
        public bool IsUpdate { get { return EmployeeMembershipGrid.IsEditMode; } }
        public bool IsInsert { get { return EmployeeMembershipGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.EmployeeMembership.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeMembership.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.EmployeeMembership.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();

            if (!IsPostBack)
            {
                LoadEmployeeMembership(EmployeeId);
                Initialize();
            }
        }
        protected void WireEvents()
        {
            EmployeeMembershipGrid.NeedDataSource += new GridNeedDataSourceEventHandler(EmployeeMembershipGrid_NeedDataSource);
        }
        protected void Initialize()
        {
            //find the EmployeeMembershipGrid
            WLPGrid grid = (WLPGrid)this.FindControl("EmployeeMembershipGrid");

            //hide the edit/delete images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }
        protected void LoadEmployeeMembership(long employeeId)
        {
            if (!IsDataExternallyLoaded)
                DataItemCollection = EmployeeMembershipCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetEmployeeMembership(employeeId));
            else
                EmployeeMembershipGrid.Rebind();
        }
        #endregion

        #region event handlers
        void EmployeeMembershipGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EmployeeMembershipGrid.DataSource = DataItemCollection;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        public override void Update(bool updateExterallyControlled)
        {
        }
        public override void AddNewDataItem()
        {
            if (DataItemCollection == null)
                DataItemCollection = new EmployeeMembershipCollection();
        }
        public override void ChangeModeEdit()
        {
        }
        protected void EmployeeMembershipGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "cancel":
                case "performinsert":
                case "update":
                    OnItemChangingComplete(null);
                    break;
                case "edit":
                case "initinsert":
                    OnItemChanging(null);
                    break;
            }
        }
        #endregion

        #region handle updates
        protected void EmployeeMembershipGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            EmployeeMembership item = (EmployeeMembership)e.Item.DataItem;
            item.EmployeeId = EmployeeId;

            if (IsDataExternallyLoaded)
            {
                if (DataItemCollection.Count > 0)
                    item.EmployeeMembershipId = ((EmployeeMembership)DataItemCollection[DataItemCollection.Count - 1]).EmployeeMembershipId - 1;

                Wizard.ItemChangedEventArgs args = new Wizard.ItemChangedEventArgs() { Items = DataItemCollection };
                ((EmployeeMembershipCollection)DataItemCollection).Add(item);
                OnItemChanged(args);
            }
            else
                ((DataItemCollection<EmployeeMembership>)DataItemCollection).Add(Common.ServiceWrapper.HumanResourcesClient.InsertEmployeeMembership(item));
        }
        protected void EmployeeMembershipGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                EmployeeMembership item = (EmployeeMembership)e.Item.DataItem;

                if (IsDataExternallyLoaded)
                {
                    Wizard.ItemChangedEventArgs args = new Wizard.ItemChangedEventArgs() { Items = DataItemCollection };
                    OnItemChanged(args);
                }
                else
                    Common.ServiceWrapper.HumanResourcesClient.UpdateEmployeeMembership(item).CopyTo((EmployeeMembership)DataItemCollection[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void EmployeeMembershipGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (IsDataExternallyLoaded)
                {
                    Wizard.ItemChangedEventArgs args = new Wizard.ItemChangedEventArgs() { Items = DataItemCollection };
                    OnItemChanged(args);
                }
                else
                    Common.ServiceWrapper.HumanResourcesClient.DeleteEmployeeMembership((EmployeeMembership)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}