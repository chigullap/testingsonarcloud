﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeEducationModuleControl.ascx.cs" Inherits="WorkLinks.HumanResources.Employee.Education.EmployeeEducationModuleControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Src="../../Employee/Education/EmployeeEducationControl.ascx" TagName="EmployeeEducationControl" TagPrefix="uc1" %>
<%@ Register Src="../../Employee/Education/EmployeeSkillControl.ascx" TagName="EmployeeSkillControl" TagPrefix="uc2" %>
<%@ Register Src="../../Employee/Education/EmployeeLicenseCertificateControl.ascx" TagName="EmployeeLicenseCertificateControl" TagPrefix="uc3" %>
<%@ Register Src="../../Employee/Education/EmployeeAwardHonourControl.ascx" TagName="EmployeeAwardHonourControl" TagPrefix="uc4" %>
<%@ Register Src="../../Employee/Education/EmployeeCourseControl.ascx" TagName="EmployeeCourseControl" TagPrefix="uc5" %>
<%@ Register Src="../../Employee/Education/EmployeeMembershipControl.ascx" TagName="EmployeeMembershipControl" TagPrefix="uc6" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="EducationPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="EducationPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="EducationPanel" runat="server">
    <table width="100%">
        <tr>
            <td width="10%" valign="top">
                <wasp:WLPTabStrip ID="EmployeeEducationModuleTabStrip" runat="server" Width="100%" Orientation="VerticalLeft" MultiPageID="EmployeeEducationModuleMultiPage" OnPreRender="EmployeeEducationModuleTabStrip_PreRender" SelectedIndex="3">
                    <Tabs>
                        <wasp:WLPTab Text="**Education**" OnInit="EmployeeEducation_OnInit" PageViewID="EmployeeEducationPageView" ResourceName="Education" />
                        <wasp:WLPTab Text="**Skills**" OnInit="EmployeeSkill_OnInit" PageViewID="SkillPageView" ResourceName="Skills" />
                        <wasp:WLPTab Text="**License/Certification**" OnInit="EmployeeCertificate_OnInit" PageViewID="LicenseCertificationPageView" ResourceName="LicenseCertification" />
                        <wasp:WLPTab Text="**Award/Honour**" OnInit="EmployeeAward_OnInit" PageViewID="EmployeeAwardPageView" ResourceName="AwardHonour" />
                        <wasp:WLPTab Text="**Course**" OnInit="EmployeeCourse_OnInit" PageViewID="EmployeeCoursePageView" ResourceName="Course" />
                        <wasp:WLPTab Text="**Membership**" OnInit="EmployeeMembership_OnInit" PageViewID="EmployeeMembershipPageView" ResourceName="Membership" />
                    </Tabs>
                </wasp:WLPTabStrip>
            </td>
            <td width="90%" valign="top">
                <telerik:RadMultiPage ID="EmployeeEducationModuleMultiPage" runat="server" OnPreRender="EmployeeEducationModuleMultiPage_PreRender" SelectedIndex="3">
                    <telerik:RadPageView ID="EmployeeEducationPageView" OnInit="EmployeeEducation_OnInit" runat="server">
                        <uc1:EmployeeEducationControl ID="EmployeeEducationControl" runat="server" />
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="SkillPageView" OnInit="EmployeeSkill_OnInit" runat="server">
                        <uc2:EmployeeSkillControl ID="EmployeeSkillControl" runat="server" />
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="LicenseCertificationPageView" OnInit="EmployeeCertificate_OnInit" runat="server">
                        <uc3:EmployeeLicenseCertificateControl ID="EmployeeLicenseCertificateControl" runat="server" />
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="EmployeeAwardPageView" OnInit="EmployeeAward_OnInit" runat="server">
                        <uc4:EmployeeAwardHonourControl ID="EmployeeAwardHonourControl" runat="server" />
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="EmployeeCoursePageView" OnInit="EmployeeCourse_OnInit" runat="server">
                        <uc5:EmployeeCourseControl ID="EmployeeCourseControl" runat="server" />
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="EmployeeMembershipPageView" OnInit="EmployeeMembership_OnInit" runat="server">
                        <uc6:EmployeeMembershipControl ID="EmployeeMembershipControl" runat="server" />
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </td>
        </tr>
    </table>
</asp:Panel>