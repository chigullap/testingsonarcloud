﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeControl.ascx.cs" Inherits="WorkLinks.HumanResources.Employee.EmployeeControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Src="../Employee/Biographical/EmployeeBiographicalControl.ascx" TagName="EmployeeBiographicalControl" TagPrefix="uc1" %>
<%@ Register Src="../Employee/Address/AddressListControl.ascx" TagName="AddressListControl" TagPrefix="uc5" %>
<%@ Register Src="../Employee/ContactChannel/PersonEmailControl.ascx" TagName="PersonEmailControl" TagPrefix="uc6" %>
<%@ Register Src="../Employee/ContactChannel/PersonPhoneControl.ascx" TagName="PersonPhoneControl" TagPrefix="uc7" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <table width="100%">
        <tr>
            <td width="10%" valign="top">
                <wasp:WLPTabStrip ID="RadTabStrip1" runat="server" Width="100%" Orientation="VerticalLeft" SelectedIndex="0" MultiPageID="EmployeeMultiPage" OnPreRender="RadTabStrip_PreRender">
                    <Tabs>
                        <wasp:WLPTab Text="General" OnInit="EmployeeBiographical_OnInit" PageViewID="EmployeeBiographicalPageView" Selected="true" ResourceName="General" />
                        <wasp:WLPTab Text="Address" OnInit="EmployeeAddressList_OnInit" ResourceName="Address" />
                        <wasp:WLPTab Text="Phones" OnInit="PersonPhone_OnInit" ResourceName="Phones" />
                        <wasp:WLPTab Text="Email" OnInit="PersonEmail_OnInit" ResourceName="Email" />
                    </Tabs>
                </wasp:WLPTabStrip>
            </td>
            <td width="90%" valign="top">
                <telerik:RadMultiPage ID="EmployeeMultiPage" runat="server" SelectedIndex="0" OnPreRender="EmployeeMultiPage_PreRender">
                    <telerik:RadPageView OnInit="EmployeeBiographical_OnInit" ID="EmployeeBiographicalPageView" runat="server" Selected="true">
                        <uc1:EmployeeBiographicalControl ID="EmployeeBiographicalControl" runat="server" />
                    </telerik:RadPageView>
                    <telerik:RadPageView OnInit="EmployeeAddressList_OnInit" ID="EmployeeAddressListPageView" runat="server">
                        <uc5:AddressListControl ID="EmployeeAddressListControl" runat="server" />
                    </telerik:RadPageView>
                    <telerik:RadPageView OnInit="PersonPhone_OnInit" ID="PersonPhonePageView" runat="server">
                        <uc7:PersonPhoneControl ID="PersonPhoneControl" runat="server" />
                    </telerik:RadPageView>
                    <telerik:RadPageView OnInit="PersonEmail_OnInit" ID="PersonEmailPageView7" runat="server">
                        <uc6:PersonEmailControl ID="PersonEmailControl" runat="server" />
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </td>
        </tr>
    </table>
</asp:Panel>