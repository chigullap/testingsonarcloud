﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeeSearchPage.aspx.cs" Inherits="WorkLinks.HumanResources.Employee.WebForm1" %>
<%@ Register Src="EmployeeSearchControl.ascx" TagName="EmployeeSearchControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate> 
        <uc1:EmployeeSearchControl ID="EmployeeSearchControl1" SearchLocation="Employee" runat="server" />        
    </ContentTemplate>
</asp:Content>