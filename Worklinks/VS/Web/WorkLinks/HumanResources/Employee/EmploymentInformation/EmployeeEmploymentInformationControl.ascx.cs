﻿using System;
using System.Web.UI.WebControls;
using WLP.Web.UI.Controls;
using WorkLinks.Admin.EmployeeCustomField;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Employee.EmploymentInformation
{
    public delegate void GetEmployeePositionEffectiveDateEventHandler(object sender, EmployeeEmploymentInformationControl.GetEmployeePositionEffectiveDateEventArgs e);

    public partial class EmployeeEmploymentInformationControl : Wizard.WizardUserControl
    {
        public event GetEmployeePositionEffectiveDateEventHandler GetEmployeePositionEffectiveDate;

        #region fields
        protected bool _updateExterallyControlled = false;
        #endregion

        #region properties
        public DateTime? EmployeePositionEffectiveDate { get; set; }
        protected EmployeeEmploymentInformation EmployeeEmploymentInformation
        {
            get
            {
                if (DataItemCollection.Count < 1)
                    ((EmployeeEmploymentInformationCollection)DataItemCollection).Add(new EmployeeEmploymentInformation() { EmployeeId = EmployeeId });

                return (EmployeeEmploymentInformation)DataItemCollection[0];
            }
        }
        private EmployeeEmploymentInformationCollection EmployeeEmploymentInformationCollection
        {
            get
            {
                EmployeeEmploymentInformationCollection collection = new EmployeeEmploymentInformationCollection();
                collection.Add(EmployeeEmploymentInformation);
                return collection;
            }
        }
        public bool IsEditMode { get { return EmployeeEmploymentInformationView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool IsInsertMode { get { return EmployeeEmploymentInformationView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsViewMode { get { return EmployeeEmploymentInformationView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        protected long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        private string LastName { get { return Page.RouteData.Values["lastName"].ToString(); } }
        private string FirstName { get { return Page.RouteData.Values["firstName"].ToString(); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeEmployment.UpdateFlag; } }
        private String PreviousProcessGroupCode { get; set; } //used to keep track of process group, if it changes, the user is shown a message
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            //does the user have access rights?
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EmployeeEmployment.ViewFlag);

            EmployeeEmploymentInformationView.PreRender += EmployeeEmploymentInformationView_PreRender;

            if (!IsPostBack)
            {
                //does the user have rights to see this employee? 
                if (!IsDataExternallyLoaded)
                    SetAuthorized(Common.ServiceWrapper.HumanResourcesClient.GetEmployee(new EmployeeCriteria() { EmployeeId = EmployeeId }).Count > 0);

                LoadEmployeeEmploymentInformation(EmployeeId);
            }

            if (!IsDataExternallyLoaded)
                this.Page.Title = String.Format(GetGlobalResourceObject("PageTitle", "EmploymentInfo").ToString(), LastName, FirstName);
            else
                InitializeControlsForWizard();

            ((ComboBoxControl)EmployeeEmploymentInformationView.FindControl("PayrollProcessGroupCode")).Focus();
        }
        void EmployeeEmploymentInformationView_PreRender(object sender, EventArgs e)
        {
            InitControlsNonWizard();
            LoadEmployeeCustomFieldControl();
        }
        private void LoadEmployeeCustomFieldControl()
        {
            //load the EmployeeCustomFieldControl
            EmployeeCustomFieldControl customFieldControl = (EmployeeCustomFieldControl)EmployeeEmploymentInformationView.FindControl("EmployeeCustomFieldControl");

            if (customFieldControl != null)
            {
                customFieldControl.Visible = true;
                customFieldControl.IsEditMode = IsEditMode;
                customFieldControl.Initialize();
            }
        }
        private void InitControlsNonWizard()
        {
            DateControl tmpTermDateControl = null;
            DateControl tmpReHireDateControl = null;

            bool isTermDateNull = EmployeeEmploymentInformation.TerminationDate == null;
            bool isRehireDateNull = EmployeeEmploymentInformation.RehireDate == null;

            tmpTermDateControl = (DateControl)(EmployeeEmploymentInformationView.FindControl("TerminationDate"));
            tmpReHireDateControl = (DateControl)(EmployeeEmploymentInformationView.FindControl("RehireDate"));

            //set properties
            tmpTermDateControl.ReadOnly = isTermDateNull;
            tmpTermDateControl.Mandatory = !isTermDateNull;

            tmpReHireDateControl.ReadOnly = isRehireDateNull;
            tmpReHireDateControl.Mandatory = !isRehireDateNull;
        }
        private void InitializeControlsForWizard()
        {
            DateControl tmpControl = null;

            if (IsPostBack)
            {
                //TODO JOHN WOULD YOU RATHER THESE SET UP AS PROPERTIES AND THEN ENABLE ? DISABLE IN MARKUP
                //Wizard Control .. Hide Termination and Rehire Date
                tmpControl = (DateControl)(EmployeeEmploymentInformationView.FindControl("TerminationDate"));
                tmpControl.Visible = false;

                tmpControl = (DateControl)(EmployeeEmploymentInformationView.FindControl("RehireDate"));
                tmpControl.Visible = false;
            }

            //Populates the Hire Date using the efectivedate from EmployeePositionInfo 
            GetEmployeePositionEffectiveDateEventArgs args = new GetEmployeePositionEffectiveDateEventArgs() { WizardItemName = "EmployeePositionViewControl" };
            OnGetEmployeePositionEffectiveDate(this, args);

            tmpControl = (DateControl)(EmployeeEmploymentInformationView.FindControl("HireDate"));
            tmpControl.Value = EmployeePositionEffectiveDate.Value;
            tmpControl.ReadOnly = true;

            //Populates the Probation Date
            DateControl probationDateControl = (DateControl)(EmployeeEmploymentInformationView.FindControl("ProbationDate"));
            probationDateControl.Value = AddDaysMonthsYears((DateTime)tmpControl.Value, Common.ApplicationParameter.ProbationDateAmountType, Common.ApplicationParameter.ProbationDateAmount);

            //Populates the Seniority Date
            DateControl seniorityDateControl = (DateControl)(EmployeeEmploymentInformationView.FindControl("SeniorityDate"));
            seniorityDateControl.Value = AddDaysMonthsYears((DateTime)tmpControl.Value, Common.ApplicationParameter.SeniorityDateAmountType, Common.ApplicationParameter.SeniorityDateAmount);

            //Populates the Next Review Date
            DateControl nextReviewDateControl = (DateControl)(EmployeeEmploymentInformationView.FindControl("NextReviewDate"));
            nextReviewDateControl.Value = AddDaysMonthsYears((DateTime)tmpControl.Value, Common.ApplicationParameter.NextReviewDateAmountType, Common.ApplicationParameter.NextReviewDateAmount);

            //Populates the Last Review Date
            DateControl lastReviewDateControl = (DateControl)(EmployeeEmploymentInformationView.FindControl("LastReviewDate"));
            lastReviewDateControl.Value = AddDaysMonthsYears((DateTime)tmpControl.Value, Common.ApplicationParameter.LastReviewDateAmountType, Common.ApplicationParameter.LastReviewDateAmount);

            //Populates the Increase Date
            DateControl increaseDateControl = (DateControl)(EmployeeEmploymentInformationView.FindControl("IncreaseDate"));
            increaseDateControl.Value = AddDaysMonthsYears((DateTime)tmpControl.Value, Common.ApplicationParameter.IncreaseDateAmountType, Common.ApplicationParameter.IncreaseDateAmount);

            //Populates the Anniversary Date
            DateControl anniversaryDateControl = (DateControl)(EmployeeEmploymentInformationView.FindControl("AnniversaryDate"));
            anniversaryDateControl.Value = AddDaysMonthsYears((DateTime)tmpControl.Value, Common.ApplicationParameter.AnniversaryDateAmountType, Common.ApplicationParameter.AnniversaryDateAmount);
        }
        protected DateTime? AddDaysMonthsYears(DateTime dateTime, String amountType, String amount)
        {
            DateTime? tempDateTime = null;

            if (amount != "Z")
            {
                if (amountType.ToLower() == "days")
                    tempDateTime = dateTime.AddDays(int.Parse(amount));
                else if (amountType.ToLower() == "months")
                    tempDateTime = dateTime.AddMonths(int.Parse(amount));
                else if (amountType.ToLower() == "years")
                    tempDateTime = dateTime.AddYears(int.Parse(amount));
            }

            return tempDateTime;
        }
        protected void LoadEmployeeEmploymentInformation(long employeeId)
        {
            if (!IsDataExternallyLoaded)
                DataItemCollection = Common.ServiceWrapper.EmployeeEmploymentInformation.Select(employeeId);

            EmployeeEmploymentInformationView.DataBind();
        }
        #endregion

        #region event handlers
        protected void OnGetEmployeePositionEffectiveDate(object sender, GetEmployeePositionEffectiveDateEventArgs e)
        {
            if (GetEmployeePositionEffectiveDate != null)
                GetEmployeePositionEffectiveDate(sender, e);
        }
        protected void EmployeeEmploymentInformationView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            EmployeeEmploymentInformationView.DataSource = EmployeeEmploymentInformationCollection;
            PreviousProcessGroupCode = EmployeeEmploymentInformationCollection[0].PayrollProcessGroupCode;
        }
        protected void EmployeeEmploymentInformationView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                EmployeeEmploymentInformation info = (EmployeeEmploymentInformation)e.DataItem; //only one employee
                CheckBoxControl excludeMassPayslipFileFlag = (CheckBoxControl)EmployeeEmploymentInformationView.FindControl("ExcludeMassPayslipFileFlag");

                EmployeeCustomFieldControl customFieldControl = (EmployeeCustomFieldControl)EmployeeEmploymentInformationView.FindControl("EmployeeCustomFieldControl");
                if (customFieldControl != null)
                    customFieldControl.PopulateEmployeeCustomFields(customFieldControl.UniqueID);

                if (excludeMassPayslipFileFlag != null)
                    info.ExcludeMassPayslipFileFlag = excludeMassPayslipFileFlag.Checked;

                if (_updateExterallyControlled)
                {
                    info.CopyTo(EmployeeEmploymentInformation);
                    e.Cancel = true;
                }
                else
                {
                    if (info.PayrollProcessGroupCode != PreviousProcessGroupCode)
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScriptMsg", String.Format("alert('{0}');", GetGlobalResourceObject("WarningMessages", "ProcessGroupChangeWarning")), true);

                    Common.ServiceWrapper.EmployeeEmploymentInformation.Update(info).CopyTo(EmployeeEmploymentInformation);
                }

                EmployeeEmploymentInformationView.DataBind();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ChangeModeView()
        {
            EmployeeEmploymentInformationView.ChangeMode(FormViewMode.ReadOnly);
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }

        protected void WSIB_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateWsibControlRemovingEstimates((ICodeControl)sender, LanguageCode);
        }
        
        #endregion

        #region override
        public override void AddNewDataItem()
        {
            if (DataItemCollection == null)
            {
                DataItemCollection = new EmployeeEmploymentInformationCollection();
                DataItemCollection.AddNew();

                EmployeeEmploymentInformation addedEmploymentInformation = ((EmployeeEmploymentInformation)DataItemCollection[0]);
                addedEmploymentInformation.EmployeeCustomFields = new EmployeeCustomFieldCollection { new EmployeeCustomField() { EmployeeCustomFieldId = -1, EmployeeId = -1 } };
            }
        }
        public override void Update(bool updateExterallyControlled)
        {
            _updateExterallyControlled = updateExterallyControlled;
            EmployeeEmploymentInformationView.UpdateItem(true);
        }
        public override void ChangeModeEdit()
        {
            if (!EmployeeEmploymentInformationView.CurrentMode.Equals(FormViewMode.Edit))
                EmployeeEmploymentInformationView.ChangeMode(FormViewMode.Edit);
        }
        #endregion

        public class GetEmployeePositionEffectiveDateEventArgs : EventArgs
        {
            public string WizardItemName { get; set; }
        }
    }
}