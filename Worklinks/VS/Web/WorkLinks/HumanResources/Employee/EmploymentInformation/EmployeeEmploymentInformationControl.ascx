﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeEmploymentInformationControl.ascx.cs" Inherits="WorkLinks.HumanResources.Employee.EmploymentInformation.EmployeeEmploymentInformationControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../../../Admin/EmployeeCustomField/EmployeeCustomFieldControl.ascx" TagName="EmployeeCustomFieldControl" TagPrefix="uc1" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="EmploymentPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="EmploymentPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel runat="server" ID="EmploymentPanel">
    <wasp:WLPFormView
        ID="EmployeeEmploymentInformationView"
        runat="server"
        RenderOuterTable="false"
        DataKeyNames="Key"
        OnNeedDataSource="EmployeeEmploymentInformationView_NeedDataSource"
        OnUpdating="EmployeeEmploymentInformationView_Updating">

        <ItemTemplate>
            <wasp:WLPToolBar ID="EmployeeEmploymentInformationToolBar" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="Edit" ImageUrl="~/App_Themes/Default/Edit.gif" CausesValidation="false" Visible='<%# UpdateFlag %>' CommandName="edit" ResourceName="Edit" />
                </Items>
            </wasp:WLPToolBar>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="PayrollProcessGroupCode" runat="server" LabelText="*PayrollProcessGroupCode" Type="PayrollProcessGroupCode" OnDataBinding="Code_NeedDataSource" ResourceName="PayrollProcessGroupCode" Value='<%# Eval("PayrollProcessGroupCode") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="WSIBCode" runat="server" LabelText="*WSIBCode" Type="WSIBCode" OnDataBinding="Code_NeedDataSource" ResourceName="WSIBCode" Value='<%# Eval("WSIBCode") %>' ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="HireDate" runat="server" LabelText="*HireDate" ResourceName="HireDate" Value='<%# Eval("HireDate") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:DateControl ID="ProbationDate" runat="server" LabelText="*ProbationDate" ResourceName="ProbationDate" Value='<%# Eval("ProbationDate") %>' ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="TerminationDate" runat="server" LabelText="*TerminationDate" ResourceName="TerminationDate" Value='<%# Eval("TerminationDate") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:DateControl ID="SeniorityDate" runat="server" LabelText="*SeniorityDate" ResourceName="SeniorityDate" Value='<%# Eval("SeniorityDate") %>' ReadOnly="true" />
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <wasp:DateControl ID="RehireDate" runat="server" LabelText="*RehireDate" ResourceName="RehireDate" Value='<%# Eval("RehireDate") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:DateControl ID="NextReviewDate" runat="server" LabelText="*NextReviewDate" ResourceName="NextReviewDate" Value='<%# Eval("NextReviewDate") %>' ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:NumericControl ID="LunchDuration" runat="server" MinValue="0" MaxValue="9.99" LabelText="*LunchDuration" ResourceName="LunchDuration" Value='<%# Eval("LunchDuration") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:DateControl ID="LastReviewDate" runat="server" LabelText="*LastReviewDate" ResourceName="LastReviewDate" Value='<%# Eval("LastReviewDate") %>' ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:NumericControl ID="DaysPerWeek" LabelText="*DaysPerWeek" MinValue="0" MaxValue="9.99" runat="server" ResourceName="DaysPerWeek" Value='<%# Eval("DaysPerWeek") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:DateControl ID="IncreaseDate" runat="server" LabelText="*IncreaseDate" ResourceName="IncreaseDate" Value='<%# Eval("IncreaseDate") %>' ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="PensionEffectiveDate" runat="server" LabelText="*PensionEffectiveDate" ResourceName="PensionEffectiveDate" Value='<%# Eval("PensionEffectiveDate") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:DateControl ID="AnniversaryDate" runat="server" LabelText="*AnniversaryDate" ResourceName="AnniversaryDate" Value='<%# Eval("AnniversaryDate") %>' ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:CheckBoxControl ID="ExcludeMassPayslipFileFlag" LabelText="*ExcludeMassPayslipFileFlag" runat="server" ResourceName="ExcludeMassPayslipFileFlag" Value='<%# Eval("ExcludeMassPayslipFileFlag") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:CheckBoxControl ID="ExcludeMassPayslipPrintFlag" LabelText="*ExcludeMassPayslipPrintFlag" runat="server" ResourceName="ExcludeMassPayslipPrintFlag" Value='<%# Eval("ExcludeMassPayslipPrintFlag") %>' ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="CodeProbationStatusCode" runat="server" LabelText="*CodeProbationStatusCode" Type="CodeProbationStatusCode" OnDataBinding="Code_NeedDataSource" ResourceName="CodeProbationStatusCode" Value='<%# Eval("CodeProbationStatusCode") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:NumericControl ID="ProbationHour" LabelText="*ProbationHour" MinValue="0" MaxValue="65000" DecimalDigits="0" runat="server" ResourceName="ProbationHour" Value='<%# Eval("ProbationHour") %>' ReadOnly="true" />
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <div style="margin-left: -10px; margin-top: -5px;">
                                <uc1:EmployeeCustomFieldControl ID="EmployeeCustomFieldControl" runat="server" EmployeeCustomFields='<%# Bind("EmployeeCustomFields") %>' ReadOnly="true" Visible="false" />
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </ItemTemplate>
        <EditItemTemplate>
            <wasp:WLPToolBar Visible='<%# !EnableWizardFunctionalityFlag %>' ID="EmployeeEmploymentInformationToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="toolBarClick">
                <Items>
                    <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert" />
                    <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update" />
                    <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" CausesValidation="False" ResourceName="Cancel" />
                </Items>
            </wasp:WLPToolBar>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="PayrollProcessGroupCode" runat="server" LabelText="*PayrollProcessGroupCode" Type="PayrollProcessGroupCode" OnDataBinding="Code_NeedDataSource" ResourceName="PayrollProcessGroupCode" Value='<%# Bind("PayrollProcessGroupCode") %>' ReadOnly="false" TabIndex="010" />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="WSIBCode" runat="server" LabelText="*WSIBCode" Type="WSIBCode" OnDataBinding="WSIB_NeedDataSource" ResourceName="WSIBCode" Value='<%# Bind("WSIBCode") %>' ReadOnly="false" TabIndex="020" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="HireDate" runat="server" LabelText="*HireDate" ResourceName="HireDate" Value='<%# Bind("HireDate") %>' ReadOnly="false" TabIndex="030" />
                        </td>
                        <td>
                            <wasp:DateControl ID="ProbationDate" runat="server" LabelText="*ProbationDate" ResourceName="ProbationDate" Value='<%# Bind("ProbationDate") %>' ReadOnly="false" TabIndex="040" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="TerminationDate" runat="server" LabelText="*TerminationDate" ResourceName="TerminationDate" Value='<%# Bind("TerminationDate") %>' ReadOnly="false" TabIndex="050" />
                        </td>
                        <td>
                            <wasp:DateControl ID="SeniorityDate" runat="server" LabelText="*SeniorityDate" ResourceName="SeniorityDate" Value='<%# Bind("SeniorityDate") %>' ReadOnly="false" TabIndex="060" />
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <wasp:DateControl ID="RehireDate" runat="server" LabelText="*RehireDate" ResourceName="RehireDate" Value='<%# Bind("RehireDate") %>' ReadOnly="false" TabIndex="070" />
                        </td>
                        <td>
                            <wasp:DateControl ID="NextReviewDate" runat="server" LabelText="*NextReviewDate" ResourceName="NextReviewDate" Value='<%# Bind("NextReviewDate") %>' ReadOnly="false" TabIndex="080" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                          <wasp:NumericControl ID="LunchDuration" runat="server" MinValue="0" MaxValue="9.99" LabelText="*LunchDuration" ResourceName="LunchDuration" Value='<%# Bind("LunchDuration") %>' ReadOnly="false" TabIndex="090" />
                        </td>
                        <td>
                            <wasp:DateControl ID="LastReviewDate" runat="server" LabelText="*LastReviewDate" ResourceName="LastReviewDate" Value='<%# Bind("LastReviewDate") %>' ReadOnly="false" TabIndex="100" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:NumericControl ID="DaysPerWeek" LabelText="*DaysPerWeek" MinValue="0" MaxValue="9.99" runat="server" ResourceName="DaysPerWeek" Value='<%# Bind("DaysPerWeek") %>' ReadOnly="false" TabIndex="110" />
                        </td>
                        <td>
                            <wasp:DateControl ID="IncreaseDate" runat="server" LabelText="*IncreaseDate" ResourceName="IncreaseDate" Value='<%# Bind("IncreaseDate") %>' ReadOnly="false" TabIndex="120" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="PensionEffectiveDate" runat="server" LabelText="*PensionEffectiveDate" ResourceName="PensionEffectiveDate" Value='<%# Bind("PensionEffectiveDate") %>' ReadOnly="false" TabIndex="130" />
                        </td>
                        <td>
                            <wasp:DateControl ID="AnniversaryDate" runat="server" LabelText="*AnniversaryDate" ResourceName="AnniversaryDate" Value='<%# Bind("AnniversaryDate") %>' ReadOnly="false" TabIndex="140" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:CheckBoxControl ID="ExcludeMassPayslipFileFlag" LabelText="*ExcludeMassPayslipFileFlag" runat="server" ResourceName="ExcludeMassPayslipFileFlag" Value='<%# Bind("ExcludeMassPayslipFileFlag") %>' ReadOnly="false" TabIndex="150" />
                        </td>
                        <td>
                            <wasp:CheckBoxControl ID="ExcludeMassPayslipPrintFlag" LabelText="*ExcludeMassPayslipPrintFlag" runat="server" ResourceName="ExcludeMassPayslipPrintFlag" Value='<%# Bind("ExcludeMassPayslipPrintFlag") %>' ReadOnly="false" TabIndex="160" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="CodeProbationStatusCode" runat="server" LabelText="*CodeProbationStatusCode" Type="CodeProbationStatusCode" OnDataBinding="Code_NeedDataSource" ResourceName="CodeProbationStatusCode" Value='<%# Bind("CodeProbationStatusCode") %>' ReadOnly="false" TabIndex="165" />
                        </td>
                         <td>
                            <wasp:NumericControl ID="ProbationHour" LabelText="*ProbationHour" DecimalDigits="0" MinValue="0" MaxValue="65000" runat="server" ResourceName="ProbationHour" Value='<%# Bind("ProbationHour") %>' ReadOnly="false" TabIndex="169" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="margin-left: -10px; margin-top: -5px;">
                                <uc1:EmployeeCustomFieldControl ID="EmployeeCustomFieldControl" EmployeeCustomFields='<%# Bind("EmployeeCustomFields") %>' runat="server" Visible="false" />
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </EditItemTemplate>
    </wasp:WLPFormView>
</asp:Panel>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var _employeeId = <%= EmployeeId %>;

        function getRadWindow() {
            var popup = null;

            if (window.radWindow)
                popup = window.radWindow;
            else if (window.frameElement.radWindow)
                popup = window.frameElement.radWindow;
            else if (window.parent.window.frameElement.radWindow)
                popup = window.parent.window.frameElement.radWindow;

            return popup;
        }

        function processClick(commandName) {
            if (commandName.toLowerCase() == 'insert' || commandName.toLowerCase() == 'update') {
                var arg = new Object;

                if (_employeeId != null && _employeeId > 0) {
                    arg.closeWithChanges = true;
                    arg.employeeId = _employeeId;
                    getRadWindow().argument = arg;
                }
            }
        }

        function toolBarClick(sender, args) {
            var button = args.get_item();
            var commandName = button.get_commandName();
            processClick(commandName);
        }
    </script>
</telerik:RadScriptBlock>