﻿using System;
using System.Web.UI.WebControls;

namespace WorkLinks.HumanResources.Employee
{
    public partial class EmployeeControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private int FirstVisibleTab
        {
            get
            {
                if (Common.Security.RoleForm.EmployeeBiographical.ViewFlag)
                    return 0;
                if (Common.Security.RoleForm.EmployeeAddress.ViewFlag)
                    return 1;
                if (Common.Security.RoleForm.EmployeePhone.ViewFlag)
                    return 2;
                if (Common.Security.RoleForm.EmployeeEmail.ViewFlag)
                    return 3;

                return -1;
            }
        }
        private string LastName
        {
            get
            {
                if (Page.RouteData.Values["lastName"] == null)
                    return null;
                else
                    return Page.RouteData.Values["lastName"].ToString();
            }
        }
        private string FirstName
        {
            get
            {
                if (Page.RouteData.Values["firstName"] == null)
                    return null;
                else
                    return Page.RouteData.Values["firstName"].ToString();
            }
        }
        protected bool IsViewMode { get { return EmployeeBiographicalControl.IsViewMode && EmployeeAddressListControl.IsViewMode && PersonPhoneControl.IsViewMode && PersonEmailControl.IsViewMode; } }
        protected bool IsEditMode { get { return EmployeeBiographicalControl.IsEditMode; } }
        protected bool IsInsertMode { get { return EmployeeBiographicalControl.IsInsertMode; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.Employee);

            if (!IsPostBack)
                WireEvents();
        }
        protected void WireEvents()
        {
            this.EmployeeBiographicalControl.EmployeeSelected += new Biographical.EmployeeBiographicalControl.EmployeeSelectedEventHandler(EmployeeBiographicalControl_EmployeeSelected);
        }
        void EmployeeBiographicalControl_EmployeeSelected(object sender, Biographical.EmployeeSelectedEventArgs e)
        {
            this.EmployeeAddressListControl.SetPersonId(e.PersonId);
            this.PersonEmailControl.SetPersonId(e.PersonId);
            this.PersonPhoneControl.SetPersonId(e.PersonId);
        }
        protected override void OnPreRender(EventArgs e)
        {
            InitializeControls();
            base.OnPreRender(e);
            this.Page.Title = String.Format(Page.Title, LastName + ", " + FirstName);
        }
        /// <summary>
        /// sets control attributes
        /// </summary>
        protected void InitializeControls()
        {
            RadTabStrip1.Enabled = IsViewMode;
        }
        protected void RadTabStrip_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                RadTabStrip1.SelectedIndex = FirstVisibleTab;
        }
        protected void EmployeeMultiPage_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                EmployeeMultiPage.SelectedIndex = FirstVisibleTab;
        }
        #endregion

        #region security handlers
        protected void EmployeeBiographical_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = Common.Security.RoleForm.EmployeeBiographical.ViewFlag; }
        protected void EmployeeAddressList_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = Common.Security.RoleForm.EmployeeAddress.ViewFlag; }
        protected void PersonPhone_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = Common.Security.RoleForm.EmployeePhone.ViewFlag; }
        protected void PersonEmail_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = Common.Security.RoleForm.EmployeeEmail.ViewFlag; }
        #endregion
    }
}