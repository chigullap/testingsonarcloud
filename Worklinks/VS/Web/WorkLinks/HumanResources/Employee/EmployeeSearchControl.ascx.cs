﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using System.Web.UI;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Employee
{
    public partial class EmployeeSearchControl : WLP.Web.UI.WLPUserControl
    {
        public enum SearchLocationType
        {
            Employee,
            Payroll,
        }

        #region properties
        protected EmployeeCriteria Criteria
        {
            get
            {
                EmployeeCriteria criteria = new EmployeeCriteria();
                criteria.EmployeeNumber = EmployeeNumber.TextValue;
                criteria.SocialInsuranceNumber = SocialInsuranceNumber.TextValue;
                criteria.FirstName = FirstName.TextValue;
                criteria.LastName = LastName.TextValue;
                criteria.OrganizationUnitName = OrganizationUnitDescription.TextValue;
                criteria.PayrollProcessGroupCode = (String)PayrollProcessGroupCodeControl.Value;
                criteria.LanguageCode = LanguageCode;
                criteria.IsWizardSearch = (bool)IncludeWizards.Value;
                criteria.UseLikeStatementInProcSelect = true;
                criteria.PageSize = CurrentPageSize;
                criteria.PageIndex = CurrentPageIndex;
                criteria.SortColumn = SortColumn;
                criteria.SortDirection = SortDirection;
                criteria.EmployeePositionStatusCode = (CodeCollection)EmployeePositionStatusCode.Value;

                return criteria;
            }
            set
            {
                EmployeeNumber.TextValue = value.EmployeeNumber;
                SocialInsuranceNumber.TextValue = value.SocialInsuranceNumber;
                FirstName.TextValue = value.FirstName;
                LastName.TextValue = value.LastName;
                OrganizationUnitDescription.TextValue = value.OrganizationUnitName;
                PayrollProcessGroupCodeControl.Value = value.PayrollProcessGroupCode;
            }
        }
        private int CurrentPageIndex
        {
            get { return EmployeeSummaryGrid.CurrentPageIndex; }
            set { EmployeeSummaryGrid.CurrentPageIndex = value; }
        }
        private String SortColumn
        {
            get
            {
                if (EmployeeSummaryGrid != null && EmployeeSummaryGrid.MasterTableView != null && EmployeeSummaryGrid.MasterTableView.SortExpressions.Count > 0)
                    return EmployeeSummaryGrid.MasterTableView.SortExpressions[0].FieldName;
                else
                    return "EmployeeNumber"; //business class property name, sql layer will change it to employee_number
            }
        }
        private String SortDirection
        {
            get
            {
                String sort = String.Empty;

                if ((EmployeeSummaryGrid != null && EmployeeSummaryGrid.MasterTableView != null && EmployeeSummaryGrid.MasterTableView.SortExpressions.Count > 0))
                {
                    switch (EmployeeSummaryGrid.MasterTableView.SortExpressions[0].SortOrder)
                    {
                        case GridSortOrder.Ascending:
                            sort = "asc";
                            break;
                        case GridSortOrder.Descending:
                            sort = "desc";
                            break;
                    }
                }

                return sort;
            }
        }
        public SearchLocationType SearchLocation { get; set; }
        protected bool IsEmployeeLocation { get { return SearchLocation.Equals(SearchLocationType.Employee); } }
        protected bool IsPayrollLocation { get { return SearchLocation.Equals(SearchLocationType.Payroll); } }
        protected bool EnableDeleteButton { get { return (IsEmployeeLocation || IsPayrollLocation) && Common.Security.RoleForm.EmployeeDeleteButton.ViewFlag; } }
        protected bool EnableDetailsButton { get { return (IsEmployeeLocation || IsPayrollLocation) && Common.Security.RoleForm.Employee; } }
        protected bool EnableEmployeeHistoryButton { get { return (IsEmployeeLocation || IsPayrollLocation) && Common.Security.RoleForm.EmployeePosition.ViewFlag; } }
        protected bool EnableEmploymentButton { get { return (IsEmployeeLocation || IsPayrollLocation) && Common.Security.RoleForm.EmployeeEmployment.ViewFlag; } }
        protected bool EnableContactsButton { get { return IsEmployeeLocation && Common.Security.RoleForm.EmployeeContacts.ViewFlag; } }
        protected bool EnableEducationButton { get { return IsEmployeeLocation && Common.Security.RoleForm.EmployeeEducation; } }
        protected bool EnableCompanyPropertyButton { get { return IsEmployeeLocation && Common.Security.RoleForm.EmployeeProperty.ViewFlag; } }
        protected bool EnableDisciplinaryActionsButton { get { return IsEmployeeLocation && Common.Security.RoleForm.EmployeeDisciplinary.ViewFlag; } }
        protected bool EnableTerminationButton { get { return (IsEmployeeLocation || IsPayrollLocation) && Common.Security.RoleForm.EmployeeTermination; } }
        protected bool EnableWsibHealthAndSafetyReportButton { get { return IsEmployeeLocation && Common.Security.RoleForm.EmployeeWsibHealthAndSafetyReport.ViewFlag; } }
        protected bool EnableBankingButton { get { return IsPayrollLocation && Common.Security.RoleForm.EmployeeBanking.ViewFlag; } }
        protected bool EnableEmployeeStatutoryDeductionsButton { get { return IsPayrollLocation && Common.Security.RoleForm.EmployeeStatutoryDeduction; } }
        protected bool EnablePaycodesButton { get { return IsPayrollLocation && Common.Security.RoleForm.EmployeePaycode.ViewFlag; } }
        protected bool EnableNHTButton { get { return IsPayrollLocation && Common.Security.RoleForm.PayrollEmployeeSearchNHT.ViewFlag; } }
        protected bool EnablePayslipButton { get { return IsPayrollLocation && Common.Security.RoleForm.PayrollEmployeeSearchPaySlip.ViewFlag; } }
        protected bool EnablePayslipMailingLabelButton { get { return IsPayrollLocation && Common.Security.RoleForm.PayrollEmployeeSearchPaySlipMailingFlag.ViewFlag; } }
        protected bool EnableBenefitButton { get { return IsEmployeeLocation && Common.Security.RoleForm.EmployeeBenefit; } }
        protected bool EnableExportButton { get { return IsEmployeeLocation && Common.Security.RoleForm.EmployeeSearchExport.ViewFlag; } }
        private int CurrentPageSize { get { return EmployeeSummaryGrid.PageSize; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager?.RegisterPostBackControl(this.btnExport);

            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EmployeeSearch.ViewFlag);

            WireEvents();

            if (!IsPostBack)
                Panel1.DataBind();
            
            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];
            string controlId = Request.Form["__EVENTTARGET"];

            if (args != null && args.StartsWith("refresh"))
            {
                long selectedEmployeeId = Convert.ToInt64(((GridDataItem)EmployeeSummaryGrid.SelectedItems[0]).GetDataKeyValue(EmployeeSummaryGrid.MasterTableView.DataKeyNames[0]));
                EmployeeSummaryGrid.Rebind();
                EmployeeSummaryGrid.SelectRowByFirstDataKey(selectedEmployeeId);
            }
            else if (args != null && args.Length > 17 && args.Substring(0, 17).ToLower().Equals("deleteemployeeid="))
            {
                long employeeId = Convert.ToInt64(args.Substring(17));

                try
                {
                    //attempt to delete the employee and then refresh the grid
                    Common.ServiceWrapper.HumanResourcesClient.DeleteEmployee(new BusinessLayer.BusinessObjects.Employee() { EmployeeId = employeeId });
                    EmployeeSummaryGrid.Rebind();
                }
                catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
                {
                    if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            //else if (IsPostBack && (args?.StartsWith("employeeExport")).GetValueOrDefault())
            else if(IsPostBack && controlId?.IndexOf("btnExport") != -1)
            {
                string value = cboEmployeeExportControl.SelectedValue;
                byte[] export = null;
                string fileName = null;

                switch (value?.ToUpperInvariant())
                {
                    case "BIOGRAPH":
                        export = CreateBiographicalFile(out fileName);
                        break;
                }

                if (export != null)
                {
                    Response.Clear();

                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition", $"attachment;filename=\"{fileName}\"");
                    Response.AddHeader("Content-Length", export.Length.ToString());

                    Response.BinaryWrite(export);
                    Response.Flush();
                    Response.End();
                }
            }
        }
        protected void WireEvents()
        {
            EmployeeSummaryGrid.NeedDataSource += new GridNeedDataSourceEventHandler(EmployeeSummaryGrid_NeedDataSource);
            EmployeeSummaryGrid.ItemDataBound += new GridItemEventHandler(EmployeeSummaryGrid_ItemDataBound);
            EmployeeSummaryGrid.PreRender += new EventHandler(EmployeeSummaryGrid_PreRender);
            EmployeeSummaryGrid.AllowPaging = true;
        }
        void EmployeeSummaryGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EmployeeSummaryCollection collection = new EmployeeSummaryCollection();

            if (Page.IsPostBack)
            {
                collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetEmployeeSummary(Criteria));

                if (collection.Count > 0)
                    EmployeeSummaryGrid.VirtualItemCount = Convert.ToInt32(collection[0].NumberOfRows);
                else
                    EmployeeSummaryGrid.VirtualItemCount = 0;
            }

            EmployeeSummaryGrid.DataSource = collection;
        }
        
        public byte[] CreateBiographicalFile(out string fileName)
        {
            EmployeeCriteria criteria = Criteria;
            criteria.PageIndex = 0;
            criteria.PageSize = int.MaxValue;
            fileName = null;
            
            List<long> employees = Common.ServiceWrapper.HumanResourcesClient.GetEmployeeSummary(criteria).Select(se => se.EmployeeId).ToList();
            
            if (employees.Any())
            {
                fileName = Common.ApplicationParameter.EmployeeBiographicFilename;
                return Common.ServiceWrapper.HumanResourcesClient.GenerateEmployeeBiographicsFile(employees) ?? new byte[0];
            }
           
            // Do we want to throw an exception if they sent us nothing?
            return null;
        }

        #endregion

        #region event handlers
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            CurrentPageIndex = 0;
            EmployeeSummaryGrid.Rebind();
        }
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            EmployeePositionStatusCode.ClearCheckboxes();
            Criteria = new EmployeeCriteria();
            EmployeeSummaryGrid.DataBind();
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void EmployeePositionStatusCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateEmployeePositionStatusCodeControl((ICodeControl)sender, LanguageCode);
        }
        protected void EmployeeSummaryGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //work around to remove empty rows caused by the OnCommand Telerik bug
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                if (String.Equals((dataItem["EmployeeNumber"].Text).ToUpper(), "&NBSP;") && String.Equals((dataItem["LastName"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["FirstName"].Text).ToUpper(), "&NBSP;") && String.Equals((dataItem["OrganizationUnitDescription"].Text).ToUpper(), "&NBSP;"))
                {
                    e.Item.Display = false;
                }
            }
        }
        void EmployeeSummaryGrid_PreRender(object sender, EventArgs e)
        {
            ////work around to remove empty rows caused by the OnCommand Telerik bug
            //if (Data == null)
            //{
            //    EmployeeSummaryGrid.AllowPaging = false;
            //}
            //else
            //{
            //    EmployeeSummaryGrid.AllowPaging = true;
            //}
        }
        protected void EmployeeSummaryGrid_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            int i = 0;
        }
        protected void EmployeeSummaryGrid_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            int i = 0;
        }
        protected void EmployeeSummaryGrid_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            int i = 0;
        }
        #endregion

        #region security handlers
        protected void EmployeeDetailsToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableDetailsButton; }
        protected void Delete_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableDeleteButton; }
        protected void PositionToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableEmployeeHistoryButton; }
        protected void EmploymentToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableEmploymentButton; }
        protected void ContactsToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableContactsButton; }
        protected void EducationToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableEducationButton; }
        protected void CompanyPropertyToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableCompanyPropertyButton; }
        protected void DisciplinaryToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableDisciplinaryActionsButton; }
        protected void TerminationToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableTerminationButton; }
        protected void WsibHealthAndSafetyReportToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableWsibHealthAndSafetyReportButton; }
        protected void DirectDepositToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableBankingButton; }
        protected void StatDeductionsToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableEmployeeStatutoryDeductionsButton; }
        protected void PaycodesToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnablePaycodesButton; }
        protected void NHTToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableNHTButton; }
        protected void PaySlipToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnablePayslipButton; }
        protected void PaySlipMailingLabelToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnablePayslipMailingLabelButton; }
        protected void BenefitToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableBenefitButton; }
        protected void ExportButton_PreRender(object sender, EventArgs e) {((WLPButton)sender).Visible = ((WLPButton)sender).Visible && EnableExportButton; }
        protected void cboEmployeeExportControl_OnPreRender(object sender, EventArgs e) { ((ComboBoxControl)sender).Visible = ((ComboBoxControl)sender).Visible && EnableExportButton; }
        #endregion
    }
}