﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeBiographicalControl.ascx.cs" Inherits="WorkLinks.HumanResources.Employee.Biographical.EmployeeBiographicalControl" ResourceName="EmployeeBiographicalControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style type="text/css">
    .RadUpload { width: 180px !important; }
    .horizontalListbox .rlbItem { float: left !important; }
    .horizontalListbox .rlbGroup { border: none !important; }
    .horizontalListbox .rlbGroup, .RadListBox { width: auto !important; }
    .horizontalListbox .label { color: Gray !important; }
    .RadGrid_Silk .rgRow>td, .RadGrid_Silk .rgAltRow>td, .RadGrid_Silk .rgEditRow>td, .RadGrid_Silk .rgFooter>td { border-width: 0 !important; }
</style>

<wasp:WLPFormView
    ID="EmployeeView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key"
    ItemType="WorkLinks.BusinessLayer.BusinessObjects.Employee">

    <ItemTemplate>
        <wasp:WLPToolBar ID="EmployeDetailToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Text="Edit" ImageUrl="~/App_Themes/Default/Edit.gif" Visible='<%# UpdateFlag %>' CommandName="edit" ResourceName="Edit" CausesValidation="false" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%" align="center">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td></td>
                                <td>
                                    <wasp:TextBoxControl ID="EmployeeNumber" runat="server" ResourceName="EmployeeNumber" Value='<%# Item.EmployeeNumber %>' ReadOnly="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:ComboBoxControl ID="TitleCode" runat="server" Type="TitleCode" ResourceName="TitleCode" Value='<%# Item.TitleCode %>' ReadOnly="true" OnDataBinding="Code_NeedDataSource" />
                                </td>
                                <td>
                                    <wasp:TextBoxControl ID="ImportExternalIdentifier" runat="server" ResourceName="ImportExternalIdentifier" Value='<%# Item.ImportExternalIdentifier %>' ReadOnly="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:TextBoxControl ID="FirstName" runat="server" ResourceName="FirstName" Value='<%# Item.FirstName %>' ReadOnly="true" />
                                </td>
                                <td>
                                    <wasp:TextBoxControl ID="MiddleName" runat="server" ResourceName="MiddleName" Value='<%# Item.MiddleName %>' ReadOnly="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:TextBoxControl ID="LastName" runat="server" ResourceName="LastName" Value='<%# Item.LastName %>' ReadOnly="true" />
                                </td>
                                <td>
                                    <wasp:TextBoxControl ID="KnownAsName" runat="server" ResourceName="KnownAsName" Value='<%# Item.KnownAsName %>' ReadOnly="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:ComboBoxControl ID="LanguageCode" runat="server" Type="LanguageCode" OnDataBinding="Code_NeedDataSource" ResourceName="LanguageCode" Value='<%# Item.LanguageCode %>' ReadOnly="true" />
                                </td>
                                <td>
                                    <wasp:ComboBoxControl ID="GenderCode" runat="server" ResourceName="GenderCode" Type="GenderCode" ReadOnly="true" OnDataBinding="Code_NeedDataSource" Value='<%# Item.GenderCode %>' />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <wasp:ComboBoxControl ID="GovernmentIdentificationNumberTypeCode" runat="server" Type="GovernmentIdentificationNumberTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="GovernmentIdentificationNumberTypeCode" Value='<%# Item.GovernmentIdentificationNumberTypeCode %>' Mandatory="true" ReadOnly="true" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <wasp:MaskedControl ID="GovernmentIdentificationNumber1" runat="server" Mask='<%# Item.EntryFormat1 %>' LabelText='<%# Item.Label1 %>' Value='<%# Item.GovernmentIdentificationNumber1 %>' Mandatory='<%# Item.Mandatory1Flag %>' Visible='<%# Item.Visible1Flag %>' ReadOnly="true" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <wasp:MaskedControl ID="GovernmentIdentificationNumber2" runat="server" Mask='<%# Item.EntryFormat2 %>' LabelText='<%# Item.Label2 %>' Value='<%# Item.GovernmentIdentificationNumber2 %>' Mandatory='<%# Item.Mandatory2Flag %>' Visible='<%# Item.Visible2Flag %>' ReadOnly="true" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <wasp:MaskedControl ID="GovernmentIdentificationNumber3" runat="server" Mask='<%# Item.EntryFormat3 %>' LabelText='<%# Item.Label3 %>' Value='<%# Item.GovernmentIdentificationNumber3 %>' Mandatory='<%# Item.Mandatory3Flag %>' Visible='<%# Item.Visible3Flag %>' ReadOnly="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:DateControl ID="BirthDate" runat="server" ResourceName="BirthDate" Value='<%# Item.BirthDate %>' ReadOnly="true" />
                                </td>
                                <td>
                                    <wasp:TextBoxControl ID="Age" runat="server" ResourceName="Age" Value='<%# Item.Age %>' ReadOnly="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:ComboBoxControl ID="MaritalStatusCode" runat="server" Type="MaritalStatusCode" OnDataBinding="Code_NeedDataSource" ResourceName="MaritalStatusCode" Value='<%# Item.MaritalStatusCode %>' ReadOnly="true" />
                                </td>
                                <td>
                                    <wasp:ComboBoxControl ID="SmokerCode" runat="server" Type="SmokerCode" OnDataBinding="Code_NeedDataSource" ResourceName="SmokerCode" Value='<%# Item.SmokerCode %>' ReadOnly="true"></wasp:ComboBoxControl>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div>
                                        <wasp:CheckedListBoxControl ID="EmploymentEquity" LabelText="EmploymentEquity" runat="server" CssClass="horizontalListbox" ResourceName="EmploymentEquity" Type="EmploymentEquityCode" OnDataBinding="EmploymentEquityCode_NeedDataSource" Value='<%# CheckedEmploymentEquities %>' ReadOnly="true" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%">
                            <tr>
                                <td>
                                    <telerik:RadBinaryImage runat="server" ID="EmployeePhoto" CssClass="img-circle" DataValue='<%# Eval("EmployeePhoto") %>' AutoAdjustImageControlSize="false" Width="180px" Height="180px" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <hr />
            <table width="78%">
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="BilingualismCode" runat="server" Type="BilingualismCode" OnDataBinding="Code_NeedDataSource" ResourceName="BilingualismCode" Value='<%# Item.BilingualismCode %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="CitizenshipCode" runat="server" Type="CitizenshipCode" OnDataBinding="Code_NeedDataSource" ResourceName="CitizenshipCode" Value='<%# Item.CitizenshipCode %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="HealthCareProvinceStateCode" runat="server" Type="ProvinceStateCode" OnDataBinding="CodeProvinces_NeedDataSource" ResourceName="HealthCareProvinceStateCode" Value='<%# Item.HealthCareProvinceStateCode %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="HealthCareNumber" runat="server" ResourceName="HealthCareNumber" Value='<%# Item.HealthCareNumber %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="BadgeNumber" runat="server" ResourceName="BadgeNumber" Value='<%# Item.BadgeNumber %>' ReadOnly="true" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar Visible='<%# !EnableWizardFunctionalityFlag %>' ID="EmployeDetailToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Add" />
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update" />
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%" align="center">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:RegularExpressionValidator ForeColor="Red" ID="empNumberValidate" runat="server" ErrorMessage='<%$ Resources:ErrorMessages, EmployeeNumberLimit %>' ControlToValidate="EmployeeNumber" ValidationExpression="^[a-zA-Z\d]{1,15}$" />
                                    <asp:CustomValidator ForeColor="Red" ID="EmployeeNumberValidator" runat="server" ControlToValidate="EmployeeNumber" ErrorMessage='<%$ Resources:ErrorMessages, DupeEmployeeNumber %>' OnServerValidate="ValidateEmployeeNumber" />
                                </td>
                                <td>
                                    <wasp:TextBoxControl ID="EmployeeNumber" runat="server" ResourceName="EmployeeNumber" Value='<%# BindItem.EmployeeNumber %>' ReadOnly='<%# IsEditMode && (!EnableWizardFunctionalityFlag || IsEmployeeNumberAutoGenerated) %>' Visible='<%# !IsEmployeeNumberAutoGenerated || !EnableWizardFunctionalityFlag %>' TabIndex="005" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:ComboBoxControl ID="TitleCode" runat="server" Type="TitleCode" OnDataBinding="Code_NeedDataSource" ResourceName="TitleCode" Value='<%# Bind("TitleCode") %>' ReadOnly="false" TabIndex="010" />
                                </td>
                                <td>
                                    <wasp:TextBoxControl ID="ImportExternalIdentifier" runat="server" ResourceName="ImportExternalIdentifier" Value='<%# BindItem.ImportExternalIdentifier %>' ReadOnly="false" TabIndex="15" />
                                    <asp:RegularExpressionValidator ForeColor="Red" ID="ImportExternalIdentifierFormatValidator" runat="server" Display="Dynamic" ErrorMessage='<%$ Resources:ErrorMessages, ExternalIdentifierLimit %>' ControlToValidate="ImportExternalIdentifier" ValidationExpression="^[a-zA-Z\d]{1,15}$" />
                                    <asp:CustomValidator ForeColor="Red" ID="ImportExternalIdentifierValidator" runat="server" Display="Dynamic" ControlToValidate="ImportExternalIdentifier" ErrorMessage='<%$ Resources:ErrorMessages, DupeExternalIdentifier %>' OnServerValidate="ValidateImportExternalIdentifier" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:TextBoxControl ID="FirstName" runat="server" ResourceName="FirstName" Value='<%# BindItem.FirstName %>' ReadOnly="false" TabIndex="030" />
                                </td>
                                <td>
                                    <wasp:TextBoxControl ID="MiddleName" runat="server" ResourceName="MiddleName" Value='<%# BindItem.MiddleName %>' ReadOnly="false" TabIndex="040" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:TextBoxControl ID="LastName" runat="server" ResourceName="LastName" Value='<%# BindItem.LastName %>' ReadOnly="false" TabIndex="050" />
                                </td>
                                <td>
                                    <wasp:TextBoxControl ID="KnownAsName" runat="server" ResourceName="KnownAsName" Value='<%# BindItem.KnownAsName %>' ReadOnly="false" TabIndex="060" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:ComboBoxControl ID="LanguageCode" runat="server" Type="LanguageCode" OnDataBinding="Code_NeedDataSource" ResourceName="LanguageCode" Value='<%# BindItem.LanguageCode %>' ReadOnly="false" TabIndex="070" />
                                </td>
                                <td>
                                    <wasp:ComboBoxControl ID="GenderCode" runat="server" Type="GenderCode" OnDataBinding="Code_NeedDataSource" ResourceName="GenderCode" Value='<%# BindItem.GenderCode %>' ReadOnly="false" TabIndex="080" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <wasp:ComboBoxControl ID="GovernmentIdentificationNumberTypeCode"  ClientIDMode="Static" runat="server" AutoSelect="true" OnSelectedIndexChanged="GovernmentIdentificationNumberTypeCode_SelectedIndexChanged" AutoPostback="true" Type="GovernmentIdentificationNumberTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="GovernmentIdentificationNumberTypeCode" Value='<%# BindItem.GovernmentIdentificationNumberTypeCode %>' Mandatory="true" ReadOnly="false" TabIndex="090" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <wasp:MaskedControl ID="GovernmentIdentificationNumber1" runat="server" Mask='<%# BindItem.EntryFormat1 %>' LabelText='<%# BindItem.Label1 %>' Value='<%# BindItem.GovernmentIdentificationNumber1 %>' Mandatory='<%# BindItem.Mandatory1Flag %>' Visible='<%# BindItem.Visible1Flag %>' ReadOnly="false" TabIndex="100" />
                                    <asp:RegularExpressionValidator ID="GovernmentIdentificationNumberValidator1" runat="server" Display="Dynamic" ForeColor="Red" ControlToValidate="GovernmentIdentificationNumber1" ErrorMessage="Not Valid" ValidationExpression='<%# BindItem.RegularExpression1 %>'>* Invalid Format</asp:RegularExpressionValidator>
                                    <asp:CustomValidator ID="SinValidatorMod10" runat="server" Display="Dynamic" ErrorMessage="**Invalid**" ForeColor="Red" ClientValidationFunction="validateSin" ControlToValidate="GovernmentIdentificationNumber1">* Invalid</asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <wasp:MaskedControl ID="GovernmentIdentificationNumber2" runat="server" Mask='<%# BindItem.EntryFormat2 %>' LabelText='<%# BindItem.Label2 %>' Value='<%# BindItem.GovernmentIdentificationNumber2 %>' Mandatory='<%# BindItem.Mandatory2Flag %>' Visible='<%# BindItem.Visible2Flag %>' ReadOnly="false" TabIndex="110" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <wasp:MaskedControl ID="GovernmentIdentificationNumber3" runat="server" Mask='<%# BindItem.EntryFormat3 %>' LabelText='<%# BindItem.Label3 %>' Value='<%# BindItem.GovernmentIdentificationNumber3 %>' Mandatory='<%# BindItem.Mandatory3Flag %>' Visible='<%# BindItem.Visible3Flag %>' ReadOnly="false" TabIndex="120" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:DateControl ID="BirthDate" runat="server" ResourceName="BirthDate" Value='<%# BindItem.BirthDate %>' ReadOnly="false" TabIndex="130" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:ComboBoxControl ID="MaritalStatusCode" runat="server" Type="MaritalStatusCode" OnDataBinding="Code_NeedDataSource" ResourceName="MaritalStatusCode" Value='<%# BindItem.MaritalStatusCode %>' ReadOnly="false" TabIndex="140" />
                                </td>
                                <td>
                                    <wasp:ComboBoxControl ID="SmokerCode" runat="server" Type="SmokerCode" OnDataBinding="Code_NeedDataSource" ResourceName="SmokerCode" Value='<%# BindItem.SmokerCode %>' ReadOnly="false" TabIndex="150" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div>
                                        <wasp:CheckedListBoxControl ID="EmploymentEquity" LabelText="EmploymentEquity" runat="server" CssClass="horizontalListbox" ResourceName="EmploymentEquity" Type="EmploymentEquityCode" OnDataBinding="EmploymentEquityCode_NeedDataSource" Value='<%# CheckedEmploymentEquities %>' TabIndex="210" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%">
                            <tr>
                                <td>
                                    <telerik:RadBinaryImage runat="server" ID="EmployeePhoto" CssClass="img-circle" DataValue='<%# Eval("EmployeePhoto") %>' AutoAdjustImageControlSize="false" Width="180px" Height="180px" Style="margin-bottom: 10px" />
                                    <telerik:RadAsyncUpload runat="server" ID="UploadEmployeePhoto" MaxFileInputsCount="1" HideFileInput="true" OnFileUploaded="UploadEmployeePhoto_FileUploaded" OnClientFileSelected="OnClientFileSelected" />
                                    <wasp:WLPButton runat="server" ID="RemoveEmployeePhoto" ResourceName="RemoveEmployeePhoto" OnClick="RemoveEmployeePhoto_Click" CausesValidation="false" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <hr />
            <table width="80.5%">
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="BilingualismCode" runat="server" Type="BilingualismCode" OnDataBinding="Code_NeedDataSource" ResourceName="BilingualismCode" Value='<%# BindItem.BilingualismCode %>' ReadOnly="false" TabIndex="160" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="CitizenshipCode" runat="server" Type="CitizenshipCode" OnDataBinding="Code_NeedDataSource" ResourceName="CitizenshipCode" Value='<%# BindItem.CitizenshipCode %>' ReadOnly="false" TabIndex="170" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="HealthCareProvinceStateCode" runat="server" Type="ProvinceStateCode" OnDataBinding="CodeProvinces_NeedDataSource" ResourceName="HealthCareProvinceStateCode" Value='<%# BindItem.HealthCareProvinceStateCode %>' ReadOnly="false" TabIndex="180" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="HealthCareNumber" runat="server" ResourceName="HealthCareNumber" Value='<%# BindItem.HealthCareNumber %>' ReadOnly="false" TabIndex="190" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="BadgeNumber" runat="server" ResourceName="BadgeNumber" Value='<%# BindItem.BadgeNumber %>' ReadOnly="false" TabIndex="200" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>

</wasp:WLPFormView>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">
    var _employeeId = <%= EmployeeId %>;
    var _okWithDuplicates = false;
    
    function getRadWindow() {
        var popup = null;

        if (window.radWindow)
            popup = window.radWindow;
        else if (window.frameElement.radWindow)
            popup = window.frameElement.radWindow;
        else if (window.parent.window.frameElement.radWindow)
            popup = window.parent.window.frameElement.radWindow;

        return popup;
    }

    function processClick(commandName) {
        if (commandName.toLowerCase() == 'insert' || commandName.toLowerCase() == 'update') {
            var arg = new Object;

            if (_employeeId != null && _employeeId > 0) {
                arg.closeWithChanges = true;
                arg.employeeId = _employeeId;
                getRadWindow().argument = arg;
            }
        }
    }

    function validateSin(source, args) {

        var sinNumber = $get($get(source.controltovalidate).attributes['fieldClientId'].value).value;
        var governmentIdentificationNumberTypeCode = document.getElementById(document.getElementById('GovernmentIdentificationNumberTypeCode').attributes['fieldClientId'].value).control._value;
        var employeeId = '<%=EmployeeId %>';

        var employeeNumberControl = document.getElementById('<%= EmployeeView.FindControl("EmployeeNumber").ClientID %>');

        if (employeeNumberControl != null) {
            var employeeNumberControl2 = document.getElementById(employeeNumberControl.attributes['fieldClientId'].value);
            var employeeNumber = employeeNumberControl2.value;
        }
        else
            var employeeNumber = 0;

        $.ajax
        ({
            type: "POST",
            url: getUrl("/HumanResources/Employee/EmployeePage.aspx/ValidateSIN"),
            data: "{}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            data: "{'sin': '" + sinNumber + "', 'governmentIdentificationNumberTypeCode': '" + governmentIdentificationNumberTypeCode + "', 'employeeId': '" + employeeId + "', 'employeeNumber': '" + employeeNumber + "' }",
            success: function (msg)
            {
                if (msg.d == "warning") {
                    var message = "<asp:Literal runat="server" Text="<%$ Resources:WarningMessages, DuplicateSINWarning %>" />";
                    var response = true;

                    if (!_okWithDuplicates) {
                        var response = confirm(message);
                        _okWithDuplicates = response;
                    }

                    if (response == true)
                        args.IsValid = true;
                    else
                        args.IsValid = false;
                }
                else {
                    if (msg.d == "true")
                        args.IsValid = true;
                    else
                        args.IsValid = false;
                }
            }
        });
    }

    function ToolBarClick(sender, args) {
        var button = args.get_item();
        var commandName = button.get_commandName();
        processClick(commandName);

        if (<%= IsInsertMode.ToString().ToLower() %>) {
            if (commandName == "cancel") {
                window.close();
            }
        }
    }

    function OnClientFileSelected(sender, args) {
        var file = args.get_fileInputField().files[0];
        var employeePhotoControl = document.getElementById('<%= EmployeeView.FindControl("EmployeePhoto").ClientID %>');
        var validFileTypes = ["image/bmp", "image/gif", "image/png", "image/jpg", "image/jpeg"];
        var maximumFileSize = 2097152;

        for (i = 0; i < validFileTypes.length; i++) {
            if (file.type == validFileTypes[i] && file.size < maximumFileSize) {
                employeePhotoControl.src = window.URL.createObjectURL(file);
                employeePhotoControl.href = window.URL.createObjectURL(file);
                break;
            }
        }
    }
</script>