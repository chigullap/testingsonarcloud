﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace WorkLinks.HumanResources.Employee {
    
    
    public partial class EmployeeMiniSearchControl {
        
        /// <summary>
        /// RadAjaxManagerProxy1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadAjaxManagerProxy RadAjaxManagerProxy1;
        
        /// <summary>
        /// Panel1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Panel1;
        
        /// <summary>
        /// LastName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.TextBoxControl LastName;
        
        /// <summary>
        /// SIN control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.TextBoxControl SIN;
        
        /// <summary>
        /// EmployeeNumber control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.TextBoxControl EmployeeNumber;
        
        /// <summary>
        /// PayrollProcessGroupCodeControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.ComboBoxControl PayrollProcessGroupCodeControl;
        
        /// <summary>
        /// btnClear control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPButton btnClear;
        
        /// <summary>
        /// btnSearch control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPButton btnSearch;
        
        /// <summary>
        /// EmployeSummaryToolBar control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPToolBar EmployeSummaryToolBar;
        
        /// <summary>
        /// EmployeeSummaryGrid control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPGrid EmployeeSummaryGrid;
    }
}
