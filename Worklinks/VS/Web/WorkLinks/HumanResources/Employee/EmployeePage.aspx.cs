﻿using System;
using System.Web.Services;

namespace WorkLinks.Web.WorkLinks.HumanResources.Employee
{
    public partial class EmployeePage : WorkLinksPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #region web methods
        [WebMethod]
        public static string ValidateSIN(string sin, string governmentIdentificationNumberTypeCode, string employeeId, string employeeNumber)
        {
            String rtn = Common.CodeHelper.CheckSINValidation(sin, governmentIdentificationNumberTypeCode, Convert.ToInt64(employeeId), employeeNumber);
            return rtn;
        }
        #endregion
    }
}