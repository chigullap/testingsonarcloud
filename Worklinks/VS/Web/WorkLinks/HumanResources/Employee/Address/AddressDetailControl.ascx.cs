﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Employee.Address
{
    public partial class AddressDetailControl : Wizard.WizardUserControl
    {
        #region fields
        protected bool _updateExterallyControlled = false;
        private const String _personIdKey = "PersonIdKey";
        private const String _addressKey = "addressKey";
        private long _personId = -1;
        private const String _provinceCodeKey = "ProvinceCodeKey";
        private String _provinceCode;
        #endregion

        #region properties
        public long PersonId
        {
            get
            {
                if (_personId == -1)
                {
                    Object obj = ViewState[_personIdKey];

                    if (obj != null)
                        _personId = Convert.ToInt64(obj);
                }

                return _personId;
            }
            set
            {
                _personId = value;
                ViewState[_personIdKey] = _personId;
            }
        }
        private PersonAddress Address { get { return (PersonAddress)DataItemCollection[0]; } }
        public String CurrentProvince
        {
            get
            {
                if (_provinceCode == null)
                {
                    Object obj = ViewState[_provinceCodeKey];

                    if (obj != null)
                        _provinceCode = (String)(obj);
                }

                return _provinceCode;
            }
            set
            {
                _provinceCode = value;
                ViewState[_provinceCodeKey] = _provinceCode;
            }
        }
        /// <summary>
        /// returns true if the control is in read-only mode
        /// </summary>
        public bool IsViewMode { get { return this.PersonAddressView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.UnionContact.UpdateFlag; } }
        #endregion

        #region event handlers
        protected void PersonAddressView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    PersonAddress address = (PersonAddress)e.DataItem; //only one address
                    address.ProvinceStateCode = CurrentProvince;

                    if (_updateExterallyControlled)
                    {
                        FormatPostalZipCode(address);
                        address.CopyTo(Address);
                        e.Cancel = true;
                    }
                    else
                    {
                        //Postal code is formatted in the biz layer for non-wizard updates
                        Common.ServiceWrapper.HumanResourcesClient.UpdatePersonAddress(address, true, new List<PersonAddress>((PersonAddress[])DataItemCollection.ToArray()).ToList()).CopyTo(Address);
                    }

                    PersonAddressView.DataBind();
                }
                catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
                {
                    if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                        e.Cancel = true;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
                e.Cancel = true;
        }
        private void FormatPostalZipCode(PersonAddress address)
        {
            if (address.CountryCode == "CA")
            {
                if (address.PostalZipCode != null)
                {
                    address.PostalZipCode = address.PostalZipCode.Replace(" ", "");
                    address.PostalZipCode = address.PostalZipCode.ToUpper();
                }
            }
            else if (address.CountryCode == "US")
            {
                if (address.PostalZipCode != null && address.PostalZipCode.Length > 5)
                {
                    address.PostalZipCode = address.PostalZipCode.Replace(" ", "");
                    address.PostalZipCode = address.PostalZipCode.Replace("-", "");
                }
            }
            else
            {
                if (address.PostalZipCode != null)
                    address.PostalZipCode = address.PostalZipCode.ToUpper();
            }
        }
        protected void PersonAddressView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            PersonAddressView.DataSource = DataItemCollection;
        }
        protected void PersonAddressView_ModeChanging(object sender, FormViewModeEventArgs e)
        {
            //if cancelling edit, reload address from db
            if (e.CancelingEdit)
                LoadAddress(PersonId);
        }
        protected void CountryCode_SelectedIndexChanged(object o, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindProvinceCombo(o);
            ((ComboBoxControl)o).BindingContainer.FindControl("AddressLine1").Focus();
        }
        #endregion

        #region main
        public override void AddNewDataItem()
        {
            if (DataItemCollection == null)
                DataItemCollection = new PersonAddressCollection();

            DataItemCollection.AddNew();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (IsDataExternallyLoaded)
                    PersonAddressView.DataBind();
            }

            ComboBoxControl personAddressTypeCode = (ComboBoxControl)PersonAddressView.FindControl("PersonAddressTypeCode");
            if (personAddressTypeCode != null)
            {
                personAddressTypeCode.Focus();
            }
            else
            {
                personAddressTypeCode = (ComboBoxControl)PersonAddressView.FindControl("PersonAddressTypeCodeItem");
                if (personAddressTypeCode != null)
                    personAddressTypeCode.Focus();
            }
        }
        protected void LoadAddress(long personId)
        {
            PersonAddressCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetPersonAddress(personId);

            //if no address exists, must create a new instance of one (gui business logic)
            if (collection.Count == 0)
                collection.Add(new PersonAddress() { PersonId = PersonId });

            DataItemCollection = PersonAddressCollection.ConvertCollection(collection);
            PersonAddressView.DataBind();
        }
        public void SetPersonId(long personId)
        {
            PersonId = personId;
            LoadAddress(PersonId);
        }
        private void BindProvinceCombo(object sender)
        {
            if (sender is ComboBoxControl)
            {
                ComboBoxControl countryCombo = (ComboBoxControl)sender;
                WLPFormView formItem = countryCombo.BindingContainer as WLPFormView;
                ComboBoxControl provinceCombo = (ComboBoxControl)formItem.FindControl("ProvinceStateCode");
                if (provinceCombo == null)
                {
                    provinceCombo = (ComboBoxControl)formItem.FindControl("ProvinceStateCodeItem");
                }

                provinceCombo.DataSource = Common.ServiceWrapper.CodeClient.GetProvinceStateCode(countryCombo.SelectedValue);
                provinceCombo.DataBind();

                //if country is CA or US, set province and postal/zip to mandatory = true
                TextBoxControl postalZipCode = (TextBoxControl)formItem.FindControl("PostalZipCode");

                if (countryCombo.Value != null && (countryCombo.Value.ToString() == "CA" || countryCombo.Value.ToString() == "US"))
                {
                    provinceCombo.Mandatory = true;

                    if (postalZipCode != null)
                        postalZipCode.Mandatory = true;
                }
                else
                {
                    provinceCombo.Mandatory = false;

                    if (postalZipCode != null)
                        postalZipCode.Mandatory = false;
                }

                if (formItem.DataItem != null && formItem.DataItem is PersonAddress)
                {
                    provinceCombo.Value = ((PersonAddress)formItem.DataItem).ProvinceStateCode;
                    CurrentProvince = ((PersonAddress)formItem.DataItem).ProvinceStateCode;
                }
                else
                {
                    provinceCombo.Value = "";
                    CurrentProvince = null;
                }
            }
        }
        #endregion

        protected void ProvinceStateCode_SelectedIndexChanged(object o, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            CurrentProvince = e.Value;
        }
        protected void CountryCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
            BindProvinceCombo(sender);
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        public override void Update(bool updateExterallyControlled)
        {
            _updateExterallyControlled = updateExterallyControlled;
            PersonAddressView.UpdateItem(true);
        }
        public override void ChangeModeEdit()
        {
            if (!PersonAddressView.CurrentMode.Equals(FormViewMode.Edit))
                PersonAddressView.ChangeMode(FormViewMode.Edit);
        }
        protected void PostalZipCodeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = false;
            String countryCode = "";
            String postalZipCode = "";

            TextBoxControl postalZipCodeControl = (TextBoxControl)PersonAddressView.FindControl("PostalZipCode");
            if (postalZipCodeControl != null && postalZipCodeControl.Value != null)
                postalZipCode = postalZipCodeControl.Value.ToString();

            ComboBoxControl countryCodeCombo = (ComboBoxControl)PersonAddressView.FindControl("CountryCode");
            if (countryCodeCombo != null && countryCodeCombo.Value != null)
                countryCode = countryCodeCombo.Value.ToString();

            if (!String.IsNullOrEmpty(postalZipCode) && (!String.IsNullOrEmpty(countryCode)))
            {
                //CAN Postal Code or US Zip Code?
                if (countryCode == "CA")
                    args.IsValid = Regex.IsMatch(postalZipCode, @"\A[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY]\d[a-zA-Z] ?\d[a-zA-Z]\d\z");
                else if (countryCode == "US")
                    args.IsValid = Regex.IsMatch(postalZipCode, @"^(\d{5}-\d{4}|\d{5}|\d{9})$|^([a-zA-Z]\d[a-zA-Z] \d[a-zA-Z]\d)$");
                else
                    args.IsValid = true;  //no validation on other countries
            }
            else if (String.IsNullOrEmpty(postalZipCode))
                args.IsValid = true;  //no validation on empty entries.  If postal is mandatory, the mandatory validator will catch it.
            else
                args.IsValid = false;
        }
    }
}