﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddressDetailControl.ascx.cs" Inherits="WorkLinks.HumanResources.Employee.Address.AddressDetailControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPFormView
    ID="PersonAddressView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key"
    OnNeedDataSource="PersonAddressView_NeedDataSource"
    OnUpdating="PersonAddressView_Updating"
    OnModeChanging="PersonAddressView_ModeChanging">

    <ItemTemplate>
        <telerik:RadToolBar ID="EmployeDetailToolBarItem" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <telerik:RadToolBarButton Text="Edit" Visible='<%# UpdateFlag %>' ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="edit" ResourceName="Edit"></telerik:RadToolBarButton>
            </Items>
        </telerik:RadToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PersonAddressTypeCodeItem" runat="server" Type="PersonAddressTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="PersonAddressTypeCode" Value='<%# Bind("PersonAddressTypeCode") %>' ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="CityItem" runat="server" ResourceName="City" Value='<%# Bind("City") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="AddressLine1Item" MaxLength="30" runat="server" ResourceName="AddressLine1" Value='<%# Bind("AddressLine1") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="CountryCodeItem" runat="server" Type="CountryCode" AutoPostback="true" OnDataBinding="CountryCode_NeedDataSource" OnSelectedIndexChanged="CountryCode_SelectedIndexChanged" ResourceName="CountryCode" Value='<%# Bind("CountryCode") %>' ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="AddressLine2Item" MaxLength="30" runat="server" ResourceName="AddressLine2" Value='<%# Bind("AddressLine2") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="ProvinceStateCodeItem" runat="server" Type="ProvinceStateCode" ResourceName="ProvinceStateCode" OnSelectedIndexChanged="ProvinceStateCode_SelectedIndexChanged" ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="PostalZipCodeItem" runat="server" ResourceName="PostalZipCode" Value='<%# Bind("PostalZipCode") %>' ReadOnly="true" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>
    <EditItemTemplate>
        <telerik:RadToolBar ID="EmployeDetailToolBar" runat="server" Width="100%" AutoPostBack="true" Visible='<%# !EnableWizardFunctionalityFlag %>'>
            <Items>
                <telerik:RadToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" ResourceName="Update"></telerik:RadToolBarButton>
                <telerik:RadToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" CausesValidation="False" ResourceName="Cancel"></telerik:RadToolBarButton>
            </Items>
        </telerik:RadToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PersonAddressTypeCode" runat="server" Type="PersonAddressTypeCode" AutoPostback="true" TabIndex="010" OnDataBinding="Code_NeedDataSource" ResourceName="PersonAddressTypeCode" Value='<%# Bind("PersonAddressTypeCode") %>'></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="City" runat="server" MaxLength="30" ResourceName="City" Value='<%# Bind("City") %>' ReadOnly="false" TabIndex="020" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="AddressLine1" runat="server" ResourceName="AddressLine1" Value='<%# Bind("AddressLine1") %>' TabIndex="030" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="CountryCode" runat="server" Type="CountryCode" AutoPostback="true" OnDataBinding="CountryCode_NeedDataSource" OnSelectedIndexChanged="CountryCode_SelectedIndexChanged" ResourceName="CountryCode" Value='<%# Bind("CountryCode") %>' TabIndex="040"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="AddressLine2" runat="server" ResourceName="AddressLine2" Value='<%# Bind("AddressLine2") %>' TabIndex="050" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="ProvinceStateCode" runat="server" Type="ProvinceStateCode" ResourceName="ProvinceStateCode" OnSelectedIndexChanged="ProvinceStateCode_SelectedIndexChanged" TabIndex="060"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="PostalZipCode" runat="server" ResourceName="PostalZipCode" Value='<%# Bind("PostalZipCode") %>' TabIndex="080" />
                       
                    </td>
                </tr>
                <tr>
                    <td>
                       &nbsp;
                    </td>
                    <td>
                     
                       <asp:CustomValidator ID="PostalZipCodeValidator" runat="server" ErrorMessage="**Invalid Postal/Zip Code**" ForeColor="Red" OnServerValidate="PostalZipCodeValidator_ServerValidate">* Invalid Postal/Zip Code</asp:CustomValidator> 
                    </td>
                </tr>
				
            </table>
        </fieldset>
    </EditItemTemplate>
</wasp:WLPFormView>