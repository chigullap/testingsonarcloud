﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Wizard;

namespace WorkLinks.HumanResources.Employee.Address
{
    public partial class AddressListControl : WizardUserControl
    {
        #region fields
        private const String _personIdKey = "PersonIdKey";
        private long _personId = -1;
        private const String _provinceCodeKey = "ProvinceCodeKey";
        private String _provinceCode;
        #endregion

        #region properties
        public String CurrentProvince
        {
            get
            {
                if (_provinceCode == null)
                {
                    Object obj = ViewState[_provinceCodeKey];
                    if (obj != null)
                        _provinceCode = (String)(obj);
                }

                return _provinceCode;
            }
            set
            {
                _provinceCode = value;
                ViewState[_provinceCodeKey] = _provinceCode;
            }
        }
        public long PersonId
        {
            get
            {
                if (_personId == -1)
                {
                    Object obj = ViewState[_personIdKey];
                    if (obj != null)
                        _personId = Convert.ToInt64(obj);
                }

                return _personId;
            }
            set
            {
                _personId = value;
                ViewState[_personIdKey] = _personId;
            }
        }
        public bool IsViewMode { get { return PersonAddressGrid.IsViewMode; } }
        public bool IsUpdate { get { return PersonAddressGrid.IsEditMode; } }
        public bool IsInsert { get { return PersonAddressGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.EmployeeAddress.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeAddress.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.EmployeeAddress.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();

            if (!IsPostBack)
            {
                Initialize();

                if (IsDataExternallyLoaded)
                    PersonAddressGrid.Rebind();
            }
        }
        protected void Initialize()
        {
            //find the PersonAddressGrid
            WLPGrid grid = (WLPGrid)this.FindControl("PersonAddressGrid");

            //hide the edit image in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
        }
        private void WireEvents()
        {
            PersonAddressGrid.NeedDataSource += new GridNeedDataSourceEventHandler(PersonAddressGrid_NeedDataSource);
            PersonAddressGrid.ItemDataBound += PersonAddressGrid_ItemDataBound;

            //if we are in the add wizard and there are no address records
            if (!IsPostBack)
            {
                if (IsDataExternallyLoaded && DataItemCollection.Count == 0)
                    PersonAddressGrid.PreRender += new EventHandler(PersonAddressGrid_PreRender);
            }
        }
        public void SetPersonId(long personId)
        {
            PersonId = personId;
            LoadAddress(PersonId);
        }
        private void LoadAddress(long personId)
        {
            DataItemCollection = PersonAddressCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetPersonAddress(personId));
        }
        private void BindProvinceCombo(object sender)
        {
            if (sender is ComboBoxControl)
            {
                ComboBoxControl countryCombo = (ComboBoxControl)sender;
                GridEditFormItem formItem = countryCombo.BindingContainer as GridEditFormItem;
                ComboBoxControl provinceCombo = (ComboBoxControl)formItem.FindControl("ProvinceStateCode");
                TextBoxControl postalZipCode = (TextBoxControl)formItem.FindControl("PostalZipCode");

                provinceCombo.DataSource = Common.ServiceWrapper.CodeClient.GetProvinceStateCode(countryCombo.SelectedValue);
                provinceCombo.DataBind();

                //if country is CA or US, set province and postal/zip to mandatory = true
                if (countryCombo.Value != null && (countryCombo.Value.ToString() == "CA" || countryCombo.Value.ToString() == "US"))
                {
                    provinceCombo.Mandatory = true;

                    if (postalZipCode != null)
                        postalZipCode.Mandatory = true;
                }
                else
                {
                    provinceCombo.Mandatory = false;

                    if (postalZipCode != null)
                        postalZipCode.Mandatory = false;
                }

                if (formItem.DataItem != null && formItem.DataItem is PersonAddress)
                {
                    provinceCombo.Value = ((PersonAddress)formItem.DataItem).ProvinceStateCode;
                    CurrentProvince = ((PersonAddress)formItem.DataItem).ProvinceStateCode;
                }
                else
                {
                    provinceCombo.Value = "";
                    CurrentProvince = null;
                }
            }
        }
        protected void ResetPrimaryFlags(PersonAddress address)
        {
            //if the record we are editing has the primary flag checked, set the others to not be checked (ie. false)
            if (address.PrimaryFlag)
            {
                foreach (IDataItem item in DataItemCollection)
                {
                    if (item.Key != address.Key) //if not the current record we are editing
                    {
                        if (((PersonAddress)item).PrimaryFlag) //if the primaryflag is true
                            ((PersonAddress)item).PrimaryFlag = false; //set it to false
                    }
                }
            }
        }
        #endregion

        #region handle updates
        protected void PersonAddressGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (IsDataExternallyLoaded)
                {
                    ItemChangedEventArgs args = new ItemChangedEventArgs() { Items = DataItemCollection };
                    OnItemChanged(args);
                }
                else
                    Common.ServiceWrapper.HumanResourcesClient.DeletePersonAddress((PersonAddress)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void PersonAddressGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            PersonAddress address = (PersonAddress)e.Item.DataItem;
            address.ProvinceStateCode = CurrentProvince;
            address.PersonId = PersonId;

            if (IsDataExternallyLoaded)
            {
                if (DataItemCollection.Count > 0)
                    address.PersonAddressId = ((PersonAddress)DataItemCollection[DataItemCollection.Count - 1]).PersonAddressId - 1;

                ResetPrimaryFlags(address);

                ItemChangedEventArgs args = new ItemChangedEventArgs() { Items = DataItemCollection };
                ((PersonAddressCollection)DataItemCollection).Add(address);
                OnItemChanged(args);
            }
            else
            {
                ResetPrimaryFlags(address);
                ((DataItemCollection<PersonAddress>)DataItemCollection).Add(Common.ServiceWrapper.HumanResourcesClient.InsertPersonAddress(address, new List<PersonAddress>((PersonAddress[])DataItemCollection.ToArray())));
            }
        }
        protected void PersonAddressGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                PersonAddress address = (PersonAddress)e.Item.DataItem;
                address.ProvinceStateCode = CurrentProvince;
                ResetPrimaryFlags(address);

                if (IsDataExternallyLoaded)
                {
                    address.CopyTo((PersonAddress)DataItemCollection[address.Key]);
                    ItemChangedEventArgs args = new ItemChangedEventArgs() { Items = DataItemCollection };
                    OnItemChanged(args);
                }
                else
                    Common.ServiceWrapper.HumanResourcesClient.UpdatePersonAddress(address, false, new List<PersonAddress>((PersonAddress[])DataItemCollection.ToArray())).CopyTo((PersonAddress)DataItemCollection[address.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region event handlers
        void PersonAddressGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            PersonAddressGrid.DataSource = DataItemCollection;
        }
        void PersonAddressGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                if (IsDataExternallyLoaded && TemplateWizardFlag)
                {
                    if ((WLPButton)e.Item.FindControl("btnUpdate") != null)
                        ((WLPButton)e.Item.FindControl("btnUpdate")).CausesValidation = false;

                    if ((WLPButton)e.Item.FindControl("btnInsert") != null)
                        ((WLPButton)e.Item.FindControl("btnInsert")).CausesValidation = false;
                }

                CheckBoxControl primaryFlag = (CheckBoxControl)e.Item.FindControl("PrimaryFlag");
                ComboBoxControl personAddressTypeCode = (ComboBoxControl)e.Item.FindControl("PersonAddressTypeCode");

                //if we are in the add wizard and there are no address records
                if (IsDataExternallyLoaded && DataItemCollection.Count == 0 && !TemplateWizardFlag)
                {
                    //check the primary flag
                    primaryFlag.Checked = true;

                    //set the type to "HOME"
                    personAddressTypeCode.Value = "HOME";

                    //disable the cancel button
                    if (((WLPButton)e.Item.FindControl("btnCancel")) != null)
                        ((WLPButton)e.Item.FindControl("btnCancel")).Enabled = false;
                }

                if (primaryFlag.Checked)
                    primaryFlag.ReadOnly = true;
                else
                    primaryFlag.Focus();

                if (personAddressTypeCode.Value != null)
                {
                    if (IsDataExternallyLoaded && personAddressTypeCode.Value.Equals("HOME"))
                        personAddressTypeCode.ReadOnly = true;
                }
            }
            else if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = e.Item as GridDataItem;

                //do not allow a delete icon to appear for the primary address
                CheckBox box = dataItem["PrimaryFlag"].Controls[0] as CheckBox;
                dataItem["deleteButton"].Controls[0].Visible = !box.Checked;
            }
        }
        protected void PersonAddressGrid_PreRender(object sender, EventArgs e)
        {
            //force the grid to be in insert mode
            if (!TemplateWizardFlag)
                ((GridCommandItem)PersonAddressGrid.MasterTableView.GetItems(GridItemType.CommandItem)[0]).FireCommandEvent(RadGrid.InitInsertCommandName, String.Empty);
        }
        protected void PersonAddressGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "cancel":
                case "performinsert":
                case "update":
                    OnItemChangingComplete(null);
                    break;
                case "edit":
                case "initinsert":
                    OnItemChanging(null);
                    break;
            }
        }
        protected void ProvinceStateCode_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            CurrentProvince = e.Value;
        }
        protected void CountryCode_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindProvinceCombo(o);
            ((ComboBoxControl)o).BindingContainer.FindControl("ProvinceStateCode").Focus();
        }
        protected void CountryCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
            BindProvinceCombo(sender);
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void CodeEdit_NeedDataSource(object sender, EventArgs e)
        {
            bool leaveSelected = false;

            if (IsDataExternallyLoaded)
            {
                if (IsInsert)
                    Common.CodeHelper.PopulateComboBoxWithTypeForWizardRemovingTypesInUse((ICodeControl)sender, DataItemCollection, LanguageCode, WizardItemName, leaveSelected);
                else
                {
                    leaveSelected = true;
                    Common.CodeHelper.PopulateComboBoxWithTypeForWizardRemovingTypesInUse((ICodeControl)sender, DataItemCollection, LanguageCode, WizardItemName, leaveSelected);
                }
            }
            else
            {
                if (IsInsert)
                    Common.CodeHelper.PopulateComboBoxWithAddressTypeRemovingTypesInUse((ICodeControl)sender, LanguageCode, PersonId);
                else
                    Common.CodeHelper.PopulateComboBoxWithAddressTypeRemovingTypesInUseLeavingSelected((ICodeControl)sender, LanguageCode, PersonId, ((ICodeControl)sender).SelectedValue.ToString());
            }
        }
        protected void PostalZipCodeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = false;
            String postalZipCode = "";
            String countryCode = "";

            if (((WebControl)source).NamingContainer is GridEditFormItem) //also lets in "GridEditFormInsertItem"
            {
                TextBoxControl postalZipCodeText = (TextBoxControl)((WebControl)source).NamingContainer.FindControl("PostalZipCode");
                if (postalZipCodeText.Value != null)
                    postalZipCode = postalZipCodeText.Value.ToString();

                ComboBoxControl countryCodeCombo = (ComboBoxControl)((WebControl)source).NamingContainer.FindControl("CountryCode");
                if (countryCodeCombo.Value != null)
                    countryCode = countryCodeCombo.Value.ToString();

                if (!String.IsNullOrEmpty(postalZipCode) && (!String.IsNullOrEmpty(countryCode)))
                {
                    //CAN Postal Code or US Zip Code?
                    if (countryCode == "CA")
                        args.IsValid = System.Text.RegularExpressions.Regex.IsMatch(postalZipCode, @"\A[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY]\d[a-zA-Z] ?\d[a-zA-Z]\d\z");
                    else if (countryCode == "US")
                        args.IsValid = System.Text.RegularExpressions.Regex.IsMatch(postalZipCode, @"^(\d{5}-\d{4}|\d{5}|\d{9})$|^([a-zA-Z]\d[a-zA-Z] \d[a-zA-Z]\d)$");
                    else
                        args.IsValid = true; //no validation on other countries
                }
                else if (String.IsNullOrEmpty(postalZipCode))
                    args.IsValid = true; //no validation on empty entries; if postal is mandatory, the mandatory validator will catch it
                else
                    args.IsValid = false;
            }
        }
        #endregion

        #region override
        public override void Update(bool updateExterallyControlled)
        {
        }
        public override void AddNewDataItem()
        {
            if (DataItemCollection == null)
                DataItemCollection = new PersonAddressCollection();
        }
        public override void ChangeModeEdit()
        {
        }
        #endregion
    }
}