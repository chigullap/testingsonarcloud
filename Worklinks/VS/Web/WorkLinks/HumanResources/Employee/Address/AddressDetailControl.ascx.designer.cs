﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace WorkLinks.HumanResources.Employee.Address {
    
    
    public partial class AddressDetailControl {
        
        /// <summary>
        /// PersonAddressView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPFormView PersonAddressView;
        
        /// <summary>
        /// EmployeDetailToolBarItem control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadToolBar EmployeDetailToolBarItem;
        
        /// <summary>
        /// PersonAddressTypeCodeItem control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.ComboBoxControl PersonAddressTypeCodeItem;
        
        /// <summary>
        /// CityItem control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.TextBoxControl CityItem;
        
        /// <summary>
        /// AddressLine1Item control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.TextBoxControl AddressLine1Item;
        
        /// <summary>
        /// CountryCodeItem control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.ComboBoxControl CountryCodeItem;
        
        /// <summary>
        /// AddressLine2Item control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.TextBoxControl AddressLine2Item;
        
        /// <summary>
        /// ProvinceStateCodeItem control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.ComboBoxControl ProvinceStateCodeItem;
        
        /// <summary>
        /// PostalZipCodeItem control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.TextBoxControl PostalZipCodeItem;
        
        /// <summary>
        /// EmployeDetailToolBar control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadToolBar EmployeDetailToolBar;
        
        /// <summary>
        /// PersonAddressTypeCode control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.ComboBoxControl PersonAddressTypeCode;
        
        /// <summary>
        /// City control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.TextBoxControl City;
        
        /// <summary>
        /// AddressLine1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.TextBoxControl AddressLine1;
        
        /// <summary>
        /// CountryCode control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.ComboBoxControl CountryCode;
        
        /// <summary>
        /// AddressLine2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.TextBoxControl AddressLine2;
        
        /// <summary>
        /// ProvinceStateCode control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.ComboBoxControl ProvinceStateCode;
        
        /// <summary>
        /// PostalZipCode control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.TextBoxControl PostalZipCode;
        
        /// <summary>
        /// PostalZipCodeValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator PostalZipCodeValidator;
    }
}
