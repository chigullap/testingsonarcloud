﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="WizardAddEmployeePage.aspx.cs" Inherits="WorkLinks.HumanResources.Wizards.WizardAddEmployeePage" %>
<%@ Register Src="WizardAddEmployeeControl.ascx" TagName="WizardAddEmployeeControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:WizardAddEmployeeControl ID="WizardAddEmployeeControl1" runat="server" />
</asp:Content>