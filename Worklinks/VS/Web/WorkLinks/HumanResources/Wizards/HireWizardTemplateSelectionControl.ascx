﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HireWizardTemplateSelectionControl.ascx.cs" Inherits="WorkLinks.HumanResources.Wizards.HireWizardTemplateSelectionControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="EmployeeSummaryGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="EmployeeSummaryGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<table width="100%">
    <tr>
        <td>
            <wasp:TextBoxControl ID="TemplateDescription" runat="server" ResourceName="Description" ReadOnly="False" TabIndex="010" Visible="false" />
        </td>
    </tr>
</table>

<wasp:WLPGrid
    ID="EmployeeSummaryGrid"
    runat="server"
    AllowPaging="True"
    PageSize="100"
    AllowSorting="True"
    GridLines="None"
    Height="500px"
    AutoAssignModifyProperties="True"
    ClientSettings-Selecting-AllowRowSelect="true">

    <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowClick="rowClick" OnRowSelecting="rowSelecting" />
    </ClientSettings>

    <MasterTableView
        ClientDataKeyNames="EmployeeId, Description, PayrollProcessGroupCode"
        DataKeyNames="EmployeeId"
        AutoGenerateColumns="False"
        CommandItemDisplay="Top">
        <CommandItemTemplate></CommandItemTemplate>
        <CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>
        <Columns>
            <wasp:GridBoundControl DataField="Description" SortExpression="Description" UniqueName="Description" ResourceName="Description">
                <HeaderStyle Width="20%" />
            </wasp:GridBoundControl>
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="PayrollProcessGroupCode" Type="PayrollProcessGroupCode" ResourceName="PayrollProcessGroupCode">
                <HeaderStyle Width="20%" />
            </wasp:GridKeyValueControl>
        </Columns>
    </MasterTableView>

    <HeaderContextMenu EnableAutoScroll="True"></HeaderContextMenu>
</wasp:WLPGrid>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var selectedState = false;

        function rowClick(sender, args) {
            selectedState = args.get_item().get_selected();
            sender.clearSelectedItems();
        }

        function rowSelecting(sender, args) {
            if (selectedState) {
                selectedState = false;
                args.set_cancel(true);
            }
        }
    </script>
</telerik:RadScriptBlock>