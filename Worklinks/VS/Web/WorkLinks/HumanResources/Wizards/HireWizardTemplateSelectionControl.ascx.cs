﻿using System;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Common;
using WorkLinks.Common.Security;
using WorkLinks.Wizard;

namespace WorkLinks.HumanResources.Wizards
{
    public partial class HireWizardTemplateSelectionControl : WizardUserControl
    {
        public override void AddNewDataItem()
        {
        }

        public override void ChangeModeEdit()
        {
        }

        public override void Update(bool updateExterallyControlled)
        {
        }

        protected EmployeeCriteria Criteria
        {
            get
            {
                EmployeeCriteria criteria = new EmployeeCriteria();
                criteria.LanguageCode = LanguageCode;
                criteria.TemplateFlag = true;
                criteria.IsWizardSearch = true;

                return criteria;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(RoleForm.WizardSelectTemplate.ViewFlag);

            EmployeeSummaryGrid.NeedDataSource += new GridNeedDataSourceEventHandler(EmployeeSummaryGrid_NeedDataSource);

            //Work Around to remove empty rows caused by the OnCommand Telerik Bug
            EmployeeSummaryGrid.ItemDataBound += new GridItemEventHandler(EmployeeSummaryGrid_ItemDataBound);
            EmployeeSummaryGrid.PreRender += new EventHandler(EmployeeSummaryGrid_PreRender);
            EmployeeSummaryGrid.AllowPaging = true;
        }

        void EmployeeSummaryGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EmployeeSummaryCollection collection = new EmployeeSummaryCollection();

            if (!Page.IsPostBack)
            {
                collection.Load(ServiceWrapper.HumanResourcesClient.GetEmployeeSummary(Criteria));
                Data = collection;
            }

            EmployeeSummaryGrid.DataSource = Data;
        }

        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }

        protected void EmployeeSummaryGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                //Work Around to remove empty rows caused by the OnCommand Telerik Bug 
                if (String.Equals((dataItem["Description"].Text).ToUpper(), "&NBSP;"))
                {
                    //Hide The Row
                    e.Item.Display = false;
                }
            }
        }

        void EmployeeSummaryGrid_PreRender(object sender, EventArgs e)
        {
            //Work Around to remove empty rows caused by the OnCommand Telerik Bug
            if (Data == null)
                EmployeeSummaryGrid.AllowPaging = false;
            else
                EmployeeSummaryGrid.AllowPaging = true;
        }
    }
}