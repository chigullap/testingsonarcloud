﻿using System;
using System.Web.UI;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Wizard;

namespace WorkLinks.HumanResources.Wizards
{
    public partial class WizardAddEmployeeControl : WLP.Web.UI.WLPUserControl
    {
        protected enum WizardIndex
        {
            HireWizardTemplateSelectionControl = 0,
            EmployeeBiographicalControl = 1,
            AddressDetailControl = 2,
            PersonPhoneControl = 3,
            PersonEmailControl = 4,
            EmployeePositionViewControl = 5,
            EmployeeEmploymentInformationControl = 6,
            StatutoryDeductionViewControl = 7,
            EmployeeBankingInformationControl = 8,
            EmployeePaycodeModuleControl = 9,
            EmployeeEducationControl = 10,
            EmployeeSkillControl = 11,
            EmployeeCompanyPropertyControl = 12,
            EmployeeLicenseCertificateControl = 13,
        }

        #region fields
        WizardUserControl _currentControl = null;
        protected TextBoxControl _description = null;
        protected static String _templateDescription = null;
        #endregion

        #region properties
        protected static String TemplateDescription
        {
            get { return _templateDescription; }
            set { _templateDescription = value; }
        }
        protected TextBoxControl Description
        {
            get { return _description; }
            set { _description = value; }
        }
        protected long WizardCacheId
        {
            get
            {
                long id = Convert.ToInt64(Page.RouteData.Values["wizardCacheId"]);

                if (id <= 0 && WizardCache != null)
                    return WizardCache.WizardCacheId;
                else
                    return id;
            }
        }
        protected String ParentPage
        {
            get
            {
                if (Page.RouteData.Values["parentPage"] == null)
                    return null;
                else
                    return Page.RouteData.Values["parentPage"].ToString();
            }
        }
        protected String IsHireWizardTemplate
        {
            get
            {
                if (Page.RouteData.Values["isTemplate"] == null)
                    return null;
                else
                    return Page.RouteData.Values["isTemplate"].ToString();
            }
        }
        protected String CountryCode
        {
            get
            {
                if (WizardCache != null)
                {
                    if (WizardCache.CountryCode != null)
                        return WizardCache.CountryCode;
                    else
                        return "x";
                }
                else
                    return Page.RouteData.Values["countryCode"].ToString();
            }
        }
        private WizardCache WizardCache
        {
            get
            {
                if (Data == null || Data.Count == 0)
                    return null;
                else
                    return ((WizardCache)Data[0]);
            }
        }
        private WizardCacheItemCollection WizardCacheItems
        {
            get
            {
                if (WizardCache == null)
                    return null;
                else
                    return WizardCache.Items;
            }
        }
        protected WizardCacheItem CurrentItem { get { return WizardCacheItems[_currentControl.WizardItemName]; } }
        protected WizardIndex CurrentWizardIndex { get { return (WizardIndex)Convert.ToInt16(Page.RouteData.Values["wizard_index"]); } }
        protected bool IsFirstIndex { get { return CurrentItem.PreviousWizardIndex == null || CurrentItem.PreviousWizardIndex == 0; } }
        protected bool IsLastIndex { get { return CurrentItem.NextWizardIndex == null; } }
        private long WizardId { get { return 1; } }
        public bool IsVisibleFlag { get { return CurrentItem.VisibleFlag; } }
        #endregion

        #region navigation buttons
        protected void PageToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (UpdateItemToDataBase()) //get next wizard_item object
                Response.Redirect(String.Format("~/HumanResources/EmployeeWizard/Edit/{0}/{1}/{2}/{3}/{4}", WizardCache.WizardCacheId, ((RadToolBarButton)e.Item).CommandName, ParentPage, IsHireWizardTemplate, CountryCode));
        }
        protected void MovePrevious_Click(object sender, EventArgs e)
        {
            if (UpdateItemToDataBase()) //get previous wizard_item object
                Response.Redirect(String.Format("~/HumanResources/EmployeeWizard/Edit/{0}/{1}/{2}/{3}/{4}", WizardCache.WizardCacheId, CurrentItem.PreviousWizardIndex, ParentPage, IsHireWizardTemplate, CountryCode));
        }
        protected void SaveAndClose_Click(object sender, EventArgs e)
        {
            if (UpdateItemToDataBase())
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "close", "closeWizard();", true);
        }
        protected void MoveNext_Click(object sender, EventArgs e)
        {
            if ((WizardCache.TemplateFlag) ? true : Page.IsValid)
            {
                if (CurrentItem.WizardIndex == 0)
                    CreateWizardCacheFromTemplate();
                else
                {
                    if (UpdateItemToDataBase()) //get next wizard_item object
                        Response.Redirect(String.Format("~/HumanResources/EmployeeWizard/Edit/{0}/{1}/{2}/{3}/{4}", WizardCache.WizardCacheId, CurrentItem.NextWizardIndex, ParentPage, IsHireWizardTemplate, CountryCode));
                }
            }
        }
        protected void Cancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "close", "closeWizard();", true);
        }
        protected void Insert_Click(object sender, EventArgs e)
        {
            if (UpdateItemToDataBase())
            {
                BusinessLayer.BusinessObjects.Employee employee = Common.ServiceWrapper.HumanResourcesClient.AddEmployee(WizardCache.WizardCacheId, Common.ApplicationParameter.AutoGenerateEmployeeNumber, Common.ApplicationParameter.AutoGenerateEmployeeNumberFormat, Common.ApplicationParameter.AutoAddSecondaryPositions);
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "close", "closeWizard();", true);
            }
        }
        protected void CreateWizardCacheFromTemplate()
        {
            foreach (Control control in WizardPlaceHolder.Controls[0].Controls)
            {
                if (control is WLPGrid)
                {
                    WLPGrid grid = (WLPGrid)control;

                    if (grid.SelectedItems.Count > 0) //get the selected template from the grid
                    {
                        long selectedTemplate = Convert.ToInt64(((Telerik.Web.UI.GridDataItem)grid.SelectedItems[0]).GetDataKeyValue(grid.MasterTableView.DataKeyNames[0]));
                        WizardCacheCollection wizardCacheCollection = Common.ServiceWrapper.HumanResourcesClient.GetWizardCache(WizardId, selectedTemplate, WizardCache.ItemType.None);

                        if (wizardCacheCollection != null)
                        {
                            //create a copy of the wizard cache and save it
                            WizardCache wizardCache = wizardCacheCollection[0];
                            wizardCache.WizardCacheId = WizardCacheId;

                            if (WizardCache.TemplateFlag)
                            {
                                wizardCache.Description = Description.Value.ToString();
                                wizardCache.TemplateFlag = true;
                            }
                            else
                            {
                                wizardCache.Description = null;
                                wizardCache.TemplateFlag = false;
                            }

                            Common.ServiceWrapper.HumanResourcesClient.UpdateWizardCache(wizardCache).CopyTo(WizardCache);

                            //create a copy of the wizard cache items and save them
                            foreach (WizardCacheItem wizardCacheItem in wizardCacheCollection[0].Items)
                            {
                                if (wizardCacheItem.WizardIndex != 0 && wizardCacheItem.DataType != null)
                                {
                                    wizardCacheItem.WizardCacheItemId = -1;
                                    wizardCacheItem.WizardCacheId = WizardCacheId;
                                    Common.ServiceWrapper.HumanResourcesClient.UpdateWizardCacheItem(wizardCacheItem);
                                }
                            }
                        }
                    }

                    break;
                }
            }

            if (WizardCache.TemplateFlag && WizardCache.WizardCacheId < 0)
                TemplateDescription = Description.Value.ToString();

            //get next wizard_item object
            Response.Redirect(String.Format("~/HumanResources/EmployeeWizard/Edit/{0}/{1}/{2}/{3}/{4}", WizardCache.WizardCacheId, CurrentItem.NextWizardIndex, ParentPage, IsHireWizardTemplate, CountryCode));
        }
        #endregion

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            SetPlaceHolder();
            SetActivePageTitle();
        }
        private void SetPlaceHolder()
        {
            switch (CurrentWizardIndex)
            {
                case WizardIndex.HireWizardTemplateSelectionControl:
                    _currentControl = (WizardUserControl)LoadControl("~/HumanResources/Wizards/HireWizardTemplateSelectionControl.ascx");
                    break;
                case WizardIndex.EmployeeBiographicalControl:
                    _currentControl = (WizardUserControl)LoadControl("~/HumanResources/Employee/Biographical/EmployeeBiographicalControl.ascx");
                    break;
                case WizardIndex.AddressDetailControl:
                    _currentControl = (WizardUserControl)LoadControl("~/HumanResources/Employee/Address/AddressListControl.ascx");
                    break;
                case WizardIndex.PersonPhoneControl:
                    _currentControl = (WizardUserControl)LoadControl("~/HumanResources/Employee/ContactChannel/PersonPhoneControl.ascx");
                    break;
                case WizardIndex.PersonEmailControl:
                    _currentControl = (WizardUserControl)LoadControl("~/HumanResources/Employee/ContactChannel/PersonEmailControl.ascx");
                    break;
                case WizardIndex.EmployeePositionViewControl:
                    _currentControl = (WizardUserControl)LoadControl("~/HumanResources/EmployeePosition/EmployeePositionViewControl.ascx");
                    break;
                case WizardIndex.EmployeeEmploymentInformationControl:
                    _currentControl = (WizardUserControl)LoadControl("~/HumanResources/Employee/EmploymentInformation/EmployeeEmploymentInformationControl.ascx");
                    break;
                case WizardIndex.StatutoryDeductionViewControl:
                    SelectStatutoryDeductionType();
                    break;
                case WizardIndex.EmployeeBankingInformationControl:
                    _currentControl = (WizardUserControl)LoadControl("~/Payroll/Banking/EmployeeBankingInformation.ascx");
                    break;
                case WizardIndex.EmployeePaycodeModuleControl:
                    _currentControl = (WizardUserControl)LoadControl("~/Payroll/Paycode/EmployeePaycodeModule.ascx");
                    break;
                case WizardIndex.EmployeeEducationControl:
                    _currentControl = (WizardUserControl)LoadControl("~/HumanResources/Employee/Education/EmployeeEducationControl.ascx");
                    break;
                case WizardIndex.EmployeeSkillControl:
                    _currentControl = (WizardUserControl)LoadControl("~/HumanResources/Employee/Education/EmployeeSkillControl.ascx");
                    break;
                case WizardIndex.EmployeeCompanyPropertyControl:
                    _currentControl = (WizardUserControl)LoadControl("~/HumanResources/Employee/CompanyProperty/EmployeeCompanyPropertyControl.ascx");
                    break;
                case WizardIndex.EmployeeLicenseCertificateControl:
                    _currentControl = (WizardUserControl)LoadControl("~/HumanResources/Employee/Education/EmployeeLicenseCertificateControl.ascx");
                    break;
            }

            WireCurrentControlEvents(_currentControl);
            _currentControl.WizardItemName = CurrentWizardIndex.ToString();
            _currentControl.EnableWizardFunctionalityFlag = true;
            _currentControl.TemplateWizardFlag = (ParentPage == "admin");
            _currentControl.CountryCode = CountryCode;
            _currentControl.ChangeModeEdit();
            WizardPlaceHolder.Controls.Add(_currentControl);
        }
        private void SelectStatutoryDeductionType()
        {
            if (CountryCode == "BB")
                _currentControl = (WizardUserControl)LoadControl("~/Payroll/StatDeduction/StatutoryDeductionBarbadosViewControl.ascx");
            else if (CountryCode == "LC")
                _currentControl = (WizardUserControl)LoadControl("~/Payroll/StatDeduction/StatutoryDeductionStLuciaViewControl.ascx");
            else if (CountryCode == "TT")
                _currentControl = (WizardUserControl)LoadControl("~/Payroll/StatDeduction/StatutoryDeductionTrinidadViewControl.ascx");
            else if (CountryCode == "JM")
                _currentControl = (WizardUserControl)LoadControl("~/Payroll/StatDeduction/StatutoryDeductionJamaicaViewControl.ascx");
            else
                _currentControl = (WizardUserControl)LoadControl("~/Payroll/StatDeduction/StatutoryDeductionViewControl.ascx");
        }
        private void WireCurrentControlEvents(WizardUserControl control)
        {
            control.NeedDataSource += new NeedDataSourceEventHandler(IWizardUserControl_NeedDataSource);
            control.ItemChanged += new WizardUserControl.ItemChangedEventHandler(IWizardUserControl_ItemChanged);
            control.ItemChanging += new WizardUserControl.ItemChangingEventHandler(IWizardUserControl_ItemChanging);
            control.ItemChangingComplete += new WizardUserControl.ItemChangingCompleteHandler(IWizardUserControl_ItemChangingComplete);

            if (control is Employee.EmploymentInformation.EmployeeEmploymentInformationControl)
                ((Employee.EmploymentInformation.EmployeeEmploymentInformationControl)control).GetEmployeePositionEffectiveDate += new Employee.EmploymentInformation.GetEmployeePositionEffectiveDateEventHandler(EmployeeEmploymentInformationControl_GetEmployeePositionEffectiveDate);
        }
        private void SetActivePageTitle()
        {
            this.Page.Title = (string)GetGlobalResourceObject("PageTitle", CurrentWizardIndex.ToString());
        }
        private bool UpdateItemToDataBase()
        {
            bool noError = true;

            try
            {
                WizardUserControl control = _currentControl;
                control.Update(true); //updates datasource only
                CurrentItem.Data = _currentControl.DataItemCollection; //overwrite data item

                if (WizardCache.WizardCacheId < 0 && WizardCache.TemplateFlag)
                {
                    WizardCache.Description = TemplateDescription;
                    WizardCache.TemplateFlag = true;
                }

                SetEmployeeNumberAndCountryCode();
                Common.ServiceWrapper.HumanResourcesClient.UpdateWizardCache(WizardCache).CopyTo(WizardCache);

                CurrentItem.WizardCacheId = WizardCacheId;
                WizardCacheItem updatedItem = Common.ServiceWrapper.HumanResourcesClient.UpdateWizardCacheItem(CurrentItem);
                updatedItem.CopyTo(WizardCacheItems[control.WizardItemName]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    noError = false;
                }
            }
            catch (Exception ex)
            {
                noError = false;
                throw ex;
            }

            return noError;
        }
        private void SetEmployeeNumberAndCountryCode()
        {
            BusinessLayer.BusinessObjects.Employee employee = null;

            if (WizardCacheItems[WizardCache.ItemType.EmployeeBiographicalControl] != null)
                employee = ((EmployeeCollection)WizardCacheItems[WizardCache.ItemType.EmployeeBiographicalControl].Data)[0];

            if (employee != null)
            {
                WizardCache.ImportExternalIdentifier = employee.EmployeeNumber;

                if (employee.GovernmentIdentificationNumberTypeCode != null)
                {
                    foreach (CodeObject code in Common.CodeHelper.GetCodeTable(LanguageCode, CodeTableType.GovernmentIdentificationNumberTypeCode, "code_country_cd", null))
                    {
                        if (code.Code == employee.GovernmentIdentificationNumberTypeCode)
                        {
                            WizardCache.CountryCode = code.ParentCode;
                            break;
                        }
                    }
                }
            }
        }

        #region events
        void IWizardUserControl_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            WizardUserControl control = (WizardUserControl)sender;

            if (CurrentItem.Data == null)
            {
                control.AddNewDataItem();
                CurrentItem.Data = control.DataItemCollection;
            }
            else
                control.DataItemCollection = (IDataItemCollection<IDataItem>)CurrentItem.Data;
        }
        void IWizardUserControl_ItemChanged(object sender, ItemChangedEventArgs e)
        {
            WizardCacheItems[0].Data = e.Items;
            EnableNavigation(true);
        }
        void IWizardUserControl_ItemChangingComplete(object sender, EventArgs e)
        {
            EnableNavigation(true);
        }
        void IWizardUserControl_ItemChanging(object sender, EventArgs e)
        {
            EnableNavigation(false);
        }
        protected void EmployeeEmploymentInformationControl_GetEmployeePositionEffectiveDate(object sender, Employee.EmploymentInformation.EmployeeEmploymentInformationControl.GetEmployeePositionEffectiveDateEventArgs e)
        {
            Employee.EmploymentInformation.EmployeeEmploymentInformationControl control = (Employee.EmploymentInformation.EmployeeEmploymentInformationControl)sender;
            WizardCache positionWizardCache = Common.ServiceWrapper.HumanResourcesClient.GetWizardCache(WizardId, WizardCacheId, WizardIndex.EmployeePositionViewControl.ToString())[0];
            DateTime? effectiveDate = ((EmployeePositionCollection)positionWizardCache.Items[0].Data)[0].EffectiveDate;

            if (effectiveDate == null)
                control.EmployeePositionEffectiveDate = DateTime.Now;
            else
                control.EmployeePositionEffectiveDate = (DateTime)effectiveDate;
        }
        protected void IncludeInTemplate_CheckedChanged(object sender, EventArgs e)
        {
            CurrentItem.VisibleFlag = IncludeInTemplate.Checked;
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }
        #endregion

        #region main
        private void EnableNavigation(bool enable)
        {
            MovePreviousUpper.Enabled = enable;
            SaveAndCloseUpper.Enabled = enable;
            MoveNextUpper.Enabled = enable;
            CancelUpper.Enabled = enable;
            InsertUpper.Enabled = enable;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Data = WizardCacheCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetWizardCache(WizardId, WizardCacheId, CurrentWizardIndex.ToString()));
                SetAuthorized(((WizardCache)Data[0]).ActiveFlag);
            }

            WizardCache.TemplateFlag = (ParentPage == "admin");

            if (CurrentItem.WizardIndex == 0 && WizardCache.TemplateFlag)
                EnableDescriptionTextBox();

            if (!IsPostBack)
            {
                InitializeNavigation();

                if (!WizardCache.TemplateFlag && !IsVisibleFlag) //get next wizard_item object
                    Response.Redirect(String.Format("~/HumanResources/EmployeeWizard/Edit/{0}/{1}/{2}/{3}/{4}", WizardCache.WizardCacheId, CurrentItem.NextWizardIndex, ParentPage, IsHireWizardTemplate, CountryCode));
            }
        }
        private void EnableDescriptionTextBox()
        {
            foreach (Control control in WizardPlaceHolder.Controls[0].Controls)
            {
                if (control is TextBoxControl)
                {
                    Description = (TextBoxControl)control;
                    Description.Visible = true;
                    Description.Mandatory = true;
                    break;
                }
            }
        }
        private void InitializeNavigation()
        {
            bool isValidEmployee = false;

            if (CurrentItem.WizardIndex > 0)
            {
                if (WizardCache.TemplateFlag)
                    EnableIncludeInTemplateCheckBox();

                WizardCacheCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetWizardCache(WizardId, WizardCacheId, WizardCache.ItemType.None);

                if (collection != null && collection.Count > 0)
                {
                    if (!IsFirstIndex)
                        PageToolBar.Items.Add(CreateToolBarButton("<", Convert.ToString(CurrentItem.PreviousWizardIndex), Convert.ToString(CurrentItem.PreviousWizardIndex)));

                    foreach (WizardCacheItem item in collection[0].Items)
                    {
                        if (item.Data != null && item.WizardIndex > 0 && (WizardCache.TemplateFlag || !WizardCache.TemplateFlag && item.VisibleFlag))
                            PageToolBar.Items.Add(CreateToolBarButton(Convert.ToString(item.WizardIndex), Convert.ToString(item.WizardIndex), Convert.ToString(item.WizardIndex)));
                    }

                    if (!IsLastIndex && PageToolBar.Items.Count > 0)
                        PageToolBar.Items.Add(CreateToolBarButton(">", Convert.ToString(CurrentItem.NextWizardIndex), Convert.ToString(CurrentItem.NextWizardIndex)));

                    if (IsLastIndex && !WizardCache.TemplateFlag)
                        isValidEmployee = Common.ServiceWrapper.HumanResourcesClient.ValidateWizardEmployee(collection);

                    foreach (WLPToolBarButton item in PageToolBar.Items)
                    {
                        if (WizardCache.TemplateFlag || (!WizardCache.TemplateFlag && Convert.ToInt64(item.Value) < CurrentItem.WizardIndex))
                            item.CausesValidation = false;
                    }
                }
            }

            PageToolBar.Visible = PageToolBar.Items.Count > 0;
            MovePreviousUpper.Visible = !IsFirstIndex;
            SaveAndCloseUpper.Visible = !IsFirstIndex;
            MoveNextUpper.Visible = !IsLastIndex;
            CancelUpper.Visible = IsFirstIndex;
            InsertUpper.Visible = !WizardCache.TemplateFlag && IsLastIndex && isValidEmployee;
        }
        private void EnableIncludeInTemplateCheckBox()
        {
            MoveNextUpper.CausesValidation = !WizardCache.TemplateFlag;
            IncludeInTemplate.Visible = WizardCache.TemplateFlag;

            if (IncludeInTemplate.Visible)
            {
                IncludeInTemplate.Value = IsVisibleFlag;
                IncludeInTemplate.Enabled = !CurrentItem.MandatoryFlag;
            }
        }
        private WLPToolBarButton CreateToolBarButton(String text, String value, String commandName)
        {
            WLPToolBarButton button = new WLPToolBarButton();
            button.Text = text;
            button.Value = value;
            button.CommandName = commandName;

            //bold and disable the selected page
            if (text == Convert.ToString(CurrentItem.WizardIndex))
            {
                button.Font.Bold = true;
                button.Enabled = false;
            }

            return button;
        }
        #endregion
    }
}