﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Wizards
{
    public partial class WizardSearchControl : WLP.Web.UI.WLPUserControl
    {
        public enum SearchLocationType
        {
            Employee,
            Payroll,
        }

        #region properties
        public bool AddFlag
        {
            get { return Common.Security.RoleForm.WizardSearch.AddFlag; }
        }
        public bool UpdateFlag
        {
            get { return Common.Security.RoleForm.WizardSearch.UpdateFlag; }
        }
        public bool DeleteFlag
        {
            get { return Common.Security.RoleForm.WizardSearch.DeleteFlag; }
        }
        protected int ViewHireWizardTemplateSelection
        {
            get
            {
                if (Common.Security.RoleForm.WizardSelectTemplate.ViewFlag)
                    return 0;
                else
                    return 1;
            }
        }
        public SearchLocationType SearchLocation { get; set; }
        protected EmployeeCriteria Criteria
        {
            get
            {
                EmployeeCriteria criteria = new EmployeeCriteria();
                criteria.EmployeeNumber = EmployeeNumber.TextValue;
                criteria.SocialInsuranceNumber = SocialInsuranceNumber.TextValue;
                criteria.FirstName = FirstName.TextValue;
                criteria.LastName = LastName.TextValue;
                criteria.LanguageCode = LanguageCode;
                criteria.IsWizardSearch = true;

                return criteria;
            }
            set
            {
                EmployeeNumber.TextValue = value.EmployeeNumber;
                SocialInsuranceNumber.TextValue = value.SocialInsuranceNumber;
                FirstName.TextValue = value.FirstName;
                LastName.TextValue = value.LastName;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.WizardSearch.ViewFlag);

            EmployeeSummaryGrid.NeedDataSource += new GridNeedDataSourceEventHandler(EmployeeSummaryGrid_NeedDataSource);

            //work around to remove empty rows caused by the OnCommand Telerik bug
            EmployeeSummaryGrid.ItemDataBound += new GridItemEventHandler(EmployeeSummaryGrid_ItemDataBound);
            EmployeeSummaryGrid.PreRender += new EventHandler(EmployeeSummaryGrid_PreRender);
            EmployeeSummaryGrid.AllowPaging = true;

            if (!IsPostBack)
                Panel1.DataBind();

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.StartsWith("refreshWizard="))
                RefreshWizard(args);
        }
        public void RefreshWizard(String args)
        {
            long wizardCacheId = Convert.ToInt64(args.Substring(14));

            EmployeeCriteria criteria = new EmployeeCriteria();
            criteria.LanguageCode = LanguageCode;
            criteria.IsWizardSearch = true;
            criteria.EmployeeId = wizardCacheId;

            EmployeeSummaryCollection collection = new EmployeeSummaryCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetEmployeeSummary(criteria));

            if (collection.Count == 0)
            {
                if (Data != null)
                {
                    try
                    {
                        Data.Remove((criteria.EmployeeId).ToString());
                        EmployeeSummaryGrid.Rebind();
                    }
                    catch
                    {
                        //skip
                    }
                }
            }
            else
            {
                if (Data == null)
                    Data = new EmployeeSummaryCollection();

                EmployeeSummary employee = collection[0];

                if (Data[employee.Key] == null)
                    ((EmployeeSummaryCollection)Data).Add(employee);
                else
                    employee.CopyTo(((EmployeeSummaryCollection)Data)[employee.Key]);

                EmployeeSummaryGrid.Rebind();
                EmployeeSummaryGrid.SelectRowByFirstDataKey(employee.Key);
            }
        }
        void EmployeeSummaryGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EmployeeSummaryGrid.DataSource = Data;
        }
        public void Search(EmployeeCriteria criteria)
        {
            EmployeeSummaryCollection collection = new EmployeeSummaryCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetEmployeeSummary(criteria));
            Data = collection;
            EmployeeSummaryGrid.Rebind();
        }
        #endregion

        #region event handlers
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search(Criteria);
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Criteria = new EmployeeCriteria();
            Data = null;
            EmployeeSummaryGrid.DataBind();
        }
        protected void EmployeeSummaryToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("delete") && EmployeeSummaryGrid.SelectedValue != null)
                DeleteWizardEmployee((EmployeeSummary)Data[EmployeeSummaryGrid.SelectedValue.ToString()]);
        }
        protected void DeleteWizardEmployee(EmployeeSummary wizardEmployeeData)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteWizardEmployee(Convert.ToInt64(wizardEmployeeData.Key));
                Data.Remove(wizardEmployeeData.Key);

                EmployeeSummaryGrid.SelectedIndexes.Clear();
                EmployeeSummaryGrid.Rebind();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void EmployeeSummaryGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                //work around to remove empty rows caused by the OnCommand Telerik bug 
                if (String.Equals((dataItem["EmployeeNumber"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["LastName"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["FirstName"].Text).ToUpper(), "&NBSP;"))
                {
                    //hide the row
                    e.Item.Display = false;
                }
            }
        }
        void EmployeeSummaryGrid_PreRender(object sender, EventArgs e)
        {
            //work around to remove empty rows caused by the OnCommand Telerik bug
            if (Data == null)
                EmployeeSummaryGrid.AllowPaging = false;
            else
                EmployeeSummaryGrid.AllowPaging = true;
        }
        #endregion

        #region security handlers
        protected void EmployeeAddToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && AddFlag; }
        protected void EmployeeDetailsToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && UpdateFlag; }
        protected void EmployeeDeleteToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && DeleteFlag; }
        #endregion
    }
}