﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardAddEmployeeControl.ascx.cs" Inherits="WorkLinks.HumanResources.Wizards.WizardAddEmployeeControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="WizardPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="WizardPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<style type="text/css">
    .displayNone { display: none; }
    .displayAll { display: inline; }
</style>

<asp:Panel runat="server" ID="WizardPanel" DefaultButton="MoveNextUpper">
    <table border="0" width="100%" height="765px">
        <tr align="center">
            <td valign="top" height="10px">
                <wasp:CheckBoxControl ID="IncludeInTemplate" runat="server" ResourceName="VisibleFlag" Value="<%# IsVisibleFlag %>" LabelText="Include in template" ReadOnly="false" Visible="false" AutoPostBack="true" OnCheckedChanged="IncludeInTemplate_CheckedChanged" />
                <table>
                    <tr>
                        <td width="33%">
                            <wasp:WLPButton ID="MovePreviousUpper" ResourceName="MovePrevious" runat="server" Text="Previous" CommandName="MovePrevious" Icon-PrimaryIconUrl="~/App_Themes/Default/left.png" CausesValidation="false" Width="125px" OnClick="MovePrevious_Click" />
                        </td>
                        <td width="33%">
                            <wasp:WLPButton ID="SaveAndCloseUpper" ResourceName="SaveAndClose" runat="server" Text="Save & Close" Icon-PrimaryIconUrl="~/App_Themes/Default/save.png" CausesValidation="false" Width="125px" OnClick="SaveAndClose_Click" />
                            <wasp:WLPButton ID="CancelUpper" ResourceName="Cancel" runat="server" Text="Cancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" CausesValidation="false" Width="125px" OnClick="Cancel_Click" />
                        </td>
                        <td width="33%">
                            <wasp:WLPButton ID="MoveNextUpper" ResourceName="MoveNext" runat="server" Text="Next" Icon-PrimaryIconUrl="~/App_Themes/Default/right.png" Width="125px" OnClick="MoveNext_Click" />
                            <wasp:WLPButton ID="InsertUpper" ResourceName="Insert" runat="server" Text="Insert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Width="125px" OnClick="Insert_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:PlaceHolder runat="server" ID="WizardPlaceHolder" />
            </td>
        </tr>
        <tr align="center">
            <td valign="bottom">
                <table>
                    <tr>
                        <wasp:WLPToolBar ID="PageToolBar" runat="server" Width="100%" OnButtonClick="PageToolBar_ButtonClick" />
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var _wizardCacheId = <%= WizardCacheId %>;

        function getRadWindow() {
            var popup = null;
            if (window.radWindow)
                popup = window.radWindow;
            else if (window.frameElement.radWindow)
                popup = window.frameElement.radWindow;
            return popup;
        }

        function closeWizard() {
            var popup = getRadWindow();
            setSaveArguments();
            setTimeout(function () { popup.close(); }, 0);
        }

        function onClientClickedValidateAndDisable(sender, eventArgs) {
            setSaveArguments();
            if (typeof (Page_Validators) != "undefined") {
                if (Page_ClientValidate())
                    sender.set_enabled(false);
            }
            else
                sender.set_enabled(false);
        }

        function onClientClickedDisable(sender, eventArgs) {
            setSaveArguments();
            Page_ValidationActive = false;
            sender.set_enabled(false);
        }

        function setSaveArguments() {
            var arg = new Object;
            if (_wizardCacheId != null && _wizardCacheId > 0) {
                arg.closeWithChanges = true;
                arg.wizardCacheId = _wizardCacheId;
                getRadWindow().argument=arg;
            }
        }
    </script>
</telerik:RadScriptBlock>