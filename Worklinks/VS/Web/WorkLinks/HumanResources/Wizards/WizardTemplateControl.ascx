﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardTemplateControl.ascx.cs" Inherits="WorkLinks.HumanResources.Wizards.WizardTemplateControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="TemplateSummaryGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="TemplateSummaryGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="TemplateSummaryGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<table width="100%">
    <tr valign="top">
        <td>
            <div>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="Description" runat="server" ResourceName="Description" ReadOnly="False" TabIndex="010" />
                            </td>
                        </tr>
                    </table>
                    <div class="SearchCriteriaButtons">
                        <wasp:WLPButton ID="btnClear" Icon-PrimaryIconUrl="~/App_Themes/Default/clear.gif" ResourceName="btnClear" runat="server" Text="Clear" OnClientClicked="clear" OnClick="btnClear_Click" CssClass="button" />
                        <wasp:WLPButton ID="btnSearch" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" ResourceName="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                    </div>
                </asp:Panel>
            </div>
        </td>
    </tr>
    <tr valign="top">
        <td>
            <div>
                <wasp:WLPToolBar ID="TemplateSummaryToolBar" runat="server" Width="100%" OnClientLoad="TemplateSummaryToolBar_init" OnButtonClick="TemplateSummaryToolBar_ButtonClick" OnClientButtonClicking="onClientButtonClicking">
                    <Items>
                        <wasp:WLPToolBarButton OnPreRender="TemplateAddToolBar_PreRender" Text="Add" CssClass="" ImageUrl="~/App_Themes/Default/Add.gif" onclick="Add()" CommandName="add" ResourceName="Add"></wasp:WLPToolBarButton>
                        <wasp:WLPToolBarButton OnPreRender="TemplateDetailsToolBar_PreRender" Text="Details" ImageUrl="" onclick="Open()" CommandName="details" ResourceName="Details"></wasp:WLPToolBarButton>
                        <wasp:WLPToolBarButton OnPreRender="TemplateDeleteToolBar_PreRender" Text="Delete" ImageUrl="~\App_Themes\Default\Delete.gif" CommandName="delete" ResourceName="Delete"></wasp:WLPToolBarButton>
                    </Items>
                </wasp:WLPToolBar>
                <wasp:WLPGrid
                    ID="TemplateSummaryGrid"
                    runat="server"
                    AllowPaging="true"
                    PagerStyle-AlwaysVisible="true"
                    PageSize="100"
                    AllowSorting="true"
                    GridLines="None"
                    Height="400px"
                    AutoAssignModifyProperties="true">

                    <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        <Selecting AllowRowSelect="True" />
                        <ClientEvents OnRowDblClick="OnRowDblClick" OnRowClick="OnRowClick" OnCommand="onCommand" />
                    </ClientSettings>

                    <MasterTableView
                        ClientDataKeyNames="EmployeeId, Description, PayrollProcessGroupCode, CountryCode"
                        DataKeyNames="EmployeeId"
                        AutoGenerateColumns="False"
                        CommandItemDisplay="Top">

                        <CommandItemTemplate></CommandItemTemplate>
                        <CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>
                        <Columns>
                            <wasp:GridBoundControl DataField="Description" SortExpression="Description" UniqueName="Description" ResourceName="Description"></wasp:GridBoundControl>
                            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="PayrollProcessGroupCode" SortExpression="PayrollProcessGroupCode" Type="PayrollProcessGroupCode" ResourceName="PayrollProcessGroupCode"></wasp:GridKeyValueControl>
                        </Columns>
                    </MasterTableView>

                    <HeaderContextMenu EnableAutoScroll="True"></HeaderContextMenu>
                </wasp:WLPGrid>
            </div>
        </td>
    </tr>
</table>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="EmployeeWindows"
    Width="1200"
    Height="800"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    OnClientClose="onClientClose"
    DestroyOnClose="True"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var toolbar;
        var countryCode;
        var employeeId;
        var description;
        var lastName;
        var firstName;
        var rowSelected;
        var addButton;
        var detailsButton;
        var educationButton;
        var contactsButton;
        var disciplinaryActionButton;
        var historyButton;
        var compPropButton;
        var employmentButton;
        var terminationButton;

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function onClientClose(sender, eventArgs) {
            var arg = sender.argument;
            if (arg != null && arg.closeWithChanges)
                __doPostBack('<%= this.ClientID %>', String.format('refreshWizard={0}', arg.wizardCacheId));
        }

        function clear() {
            employeeId = null;
        }

        function TemplateSummaryToolBar_init(sender) {
            initializeControls();
        }

        function initializeControls() {
            toolbar = $find('<%= TemplateSummaryToolBar.ClientID %>');
            addButton = toolbar.findButtonByCommandName('add');
            detailsButton = toolbar.findButtonByCommandName('details');
            deleteButton = toolbar.findButtonByCommandName('delete');

            if (detailsButton != null)
                detailsButton.disable();

            if (deleteButton != null)
                deleteButton.disable()

            employeeId = null;
            lastName = "";
            firstName = "";
        }

        function OnRowDblClick(sender, eventArgs) {
            if (toolbar.findButtonByCommandName('details')!=null)
                Open(sender);
        }

        function OnRowClick(sender, eventArgs) {
            employeeId = eventArgs.getDataKeyValue('EmployeeId');
            description = eventArgs.getDataKeyValue('Description');
            lastName = eventArgs.getDataKeyValue('LastName');
            firstName = eventArgs.getDataKeyValue('FirstName');
            payrollProcessGroupCode = eventArgs.getDataKeyValue('PayrollProcessGroupCode');
            countryCode = eventArgs.getDataKeyValue('CountryCode');

            if (payrollProcessGroupCode == null)
                payrollProcessGroupCode = 'null';

            if (countryCode == null)
                countryCode = 'x';

            rowSelected = true;

            if (detailsButton != null)
                detailsButton.enable();

            if (deleteButton != null)
                deleteButton.enable();
        }

        function Open() {
            if (employeeId != null)
                openWindowWithManager('EmployeeWindows', String.format('/HumanResources/EmployeeWizard/Edit/{0}/{1}/{2}/{3}/{4}', employeeId, 1, 'admin', 'false', countryCode), false);
        }

        function Add() {
            openWindowWithManager('EmployeeWindows', String.format('/HumanResources/EmployeeWizard/Add/{0}/{1}/{2}/{3}/{4}', -1, <%= ViewHireWizardTemplateSelection %>, 'admin', 'true', 'x'), false);
        }

        function onClientButtonClicking(sender, args) {
            if (employeeId != null) {
                var comandName = args.get_item().get_commandName();
                if (comandName == "delete") {
                    var message = "<asp:Literal runat="server" Text="<%$ Resources:WarningMessages, DeleteNotificationWarning%>" />";
                    args.set_cancel(!confirm(message));
                }
                else
                    args.set_cancel(true);
            }
            else
                args.set_cancel(true);
        }

        function RowSelected() {
            return rowSelected;
        }

        function onCommand(sender, args) {
            if (args.get_commandName() == 'Page') {
                employeeId = null;
                initializeControls();
            }
        }
    </script>
</telerik:RadScriptBlock>