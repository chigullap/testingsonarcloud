﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WizardTemplatePage.aspx.cs" Inherits="WorkLinks.HumanResources.Wizards.WizardSearchPage" %>
<%@ Register Src="WizardTemplateControl.ascx" TagName="WizardTemplateControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate></ContentTemplate>
    <uc1:WizardTemplateControl ID="WizardTemplateControl1" runat="server" />
</asp:Content>