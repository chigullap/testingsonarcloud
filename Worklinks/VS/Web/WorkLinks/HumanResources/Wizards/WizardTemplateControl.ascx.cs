﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Wizards
{
    public partial class WizardTemplateControl : WLP.Web.UI.WLPUserControl
    {
        public enum SearchLocationType
        {
            Employee,
            Payroll,
        }

        #region properties
        public bool AddFlag
        {
            get { return Common.Security.RoleForm.WizardTemplate.AddFlag; }
        }
        public bool UpdateFlag
        {
            get { return Common.Security.RoleForm.WizardTemplate.UpdateFlag; }
        }
        public bool DeleteFlag
        {
            get { return Common.Security.RoleForm.WizardTemplate.DeleteFlag; }
        }
        protected int ViewHireWizardTemplateSelection
        {
            get
            {
                if (Common.Security.RoleForm.WizardSelectTemplate.ViewFlag)
                    return 0;
                else
                    return 1;
            }
        }
        public SearchLocationType SearchLocation { get; set; }
        protected EmployeeCriteria Criteria
        {
            get
            {
                EmployeeCriteria criteria = new EmployeeCriteria();
                criteria.Description = Description.TextValue;
                criteria.LanguageCode = LanguageCode;
                criteria.TemplateFlag = true;
                criteria.IsWizardSearch = true;

                return criteria;
            }
            set
            {
                Description.TextValue = value.Description;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.WizardTemplate.ViewFlag);

            TemplateSummaryGrid.NeedDataSource += new GridNeedDataSourceEventHandler(TemplateSummaryGrid_NeedDataSource);

            //work around to remove empty rows caused by the OnCommand Telerik bug
            TemplateSummaryGrid.ItemDataBound += new GridItemEventHandler(TemplateSummaryGrid_ItemDataBound);
            TemplateSummaryGrid.PreRender += new EventHandler(TemplateSummaryGrid_PreRender);
            TemplateSummaryGrid.AllowPaging = true;

            if (!IsPostBack)
                Panel1.DataBind();

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.StartsWith("refreshWizard="))
                RefreshWizard(args);
        }
        public void RefreshWizard(String args)
        {
            long wizardCacheId = Convert.ToInt64(args.Substring(14));

            EmployeeCriteria criteria = new EmployeeCriteria();
            criteria.LanguageCode = LanguageCode;
            criteria.IsWizardSearch = true;
            criteria.TemplateFlag = true;
            criteria.EmployeeId = wizardCacheId;

            EmployeeSummaryCollection collection = new EmployeeSummaryCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetEmployeeSummary(criteria));

            if (collection.Count == 0)
            {
                if (Data != null)
                {
                    Data.Remove((criteria.EmployeeId).ToString());
                    TemplateSummaryGrid.Rebind();
                }
            }
            else
            {
                if (Data == null)
                    Data = new EmployeeSummaryCollection();

                EmployeeSummary employee = collection[0];

                if (Data[employee.Key] == null)
                    ((EmployeeSummaryCollection)Data).Add(employee);
                else
                    employee.CopyTo(((EmployeeSummaryCollection)Data)[employee.Key]);

                TemplateSummaryGrid.Rebind();
                TemplateSummaryGrid.SelectRowByFirstDataKey(employee.Key);
            }
        }
        void TemplateSummaryGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            TemplateSummaryGrid.DataSource = Data;
        }
        public void Search(EmployeeCriteria criteria)
        {
            EmployeeSummaryCollection collection = new EmployeeSummaryCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetEmployeeSummary(criteria));
            Data = collection;
            TemplateSummaryGrid.Rebind();
        }
        #endregion

        #region event handlers
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search(Criteria);
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Criteria = new EmployeeCriteria();
            Data = null;
            TemplateSummaryGrid.DataBind();
        }
        protected void TemplateSummaryToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("delete") && TemplateSummaryGrid.SelectedValue != null)
                DeleteWizardEmployee((EmployeeSummary)Data[TemplateSummaryGrid.SelectedValue.ToString()]);
        }
        protected void DeleteWizardEmployee(EmployeeSummary wizardEmployeeData)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteWizardEmployee(Convert.ToInt64(wizardEmployeeData.Key));
                Data.Remove(wizardEmployeeData.Key);

                TemplateSummaryGrid.SelectedIndexes.Clear();
                TemplateSummaryGrid.Rebind();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void TemplateSummaryGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                //work around to remove empty rows caused by the OnCommand Telerik bug 
                if (String.Equals((dataItem["Description"].Text).ToUpper(), "&NBSP;"))
                    e.Item.Display = false;
            }
        }
        void TemplateSummaryGrid_PreRender(object sender, EventArgs e)
        {
            //work around to remove empty rows caused by the OnCommand Telerik bug
            if (Data == null)
                TemplateSummaryGrid.AllowPaging = false;
            else
                TemplateSummaryGrid.AllowPaging = true;
        }
        #endregion

        #region security handlers
        protected void TemplateAddToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && AddFlag; }
        protected void TemplateDetailsToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && UpdateFlag; }
        protected void TemplateDeleteToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && DeleteFlag; }
        #endregion
    }
}