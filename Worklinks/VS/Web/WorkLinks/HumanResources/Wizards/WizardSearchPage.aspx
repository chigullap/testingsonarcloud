﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WizardSearchPage.aspx.cs" Inherits="WorkLinks.HumanResources.Wizards.WizardSearchPage" %>
<%@ Register Src="WizardSearchControl.ascx" TagName="WizardSearchControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate></ContentTemplate>
    <uc1:WizardSearchControl ID="WizardSearchControl1" runat="server" />
</asp:Content>