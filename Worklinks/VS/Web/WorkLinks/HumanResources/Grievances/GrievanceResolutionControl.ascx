﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GrievanceResolutionControl.ascx.cs" Inherits="WorkLinks.HumanResources.Grievances.GrievanceResolutionControl" %>
<%@ Register assembly="WLP.Web.UI" namespace="WLP.Web.UI.Controls" tagprefix="wasp" %>

<wasp:WLPFormView 
    ID="GrievanceResolutionFormView" 
    runat="server" 
    RenderOuterTable="False" 
    onneeddatasource="GrievanceResolutionFormView_NeedDataSource" 
    oninserting="GrievanceResolutionFormView_Inserting" onupdating="GrievanceResolutionFormView_Updating">

    <ItemTemplate>
        <wasp:WLPToolBar ID="GrievanceResolutionToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Text="Edit" Visible='<%# UpdateFlag %>' ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="edit"  ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <div>
        <table width="100%">
            <tr>
                <td>
                  <wasp:ComboBoxControl ID="GrievanceResolutionCode" runat="server" Type="GrievanceResolutionCode" ondatabinding="Code_NeedDataSource" 
                      ResourceName="GrievanceResolutionCode" Value='<%# Eval("GrievanceResolutionCode") %>' ReadOnly="True" ></wasp:ComboBoxControl>
                    
                </td>
                <td>
                  <wasp:DateControl ID="ResolutionDate" runat="server" ResourceName="ResolutionDate" Value='<%# Eval("ResolutionDate") %>'  ReadOnly="True"  />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <wasp:TextBoxControl ID="ResolutionNote" Width="600px" TextMode="multiline" rows="5" columns="50" LabelStyle="width:75px;" FieldStyle="width: 425px;" runat="server" ResourceName="ResolutionNote" Value='<%# Eval("ResolutionNote") %>'  ReadOnly="True"  />
                </td>
            </tr>
         </table>
         </div>
    </ItemTemplate>
    <EditItemTemplate>
        <wasp:WLPToolBar ID="GrievanceResolutionToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode  %>' CommandName="insert"  ResourceName="Insert"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode%>'  ResourceName="Update"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CausesValidation="False" CommandName="cancel" ResourceName="Cancel" ></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <div>
        <table width="100%">
            <tr>
                <td>
                  <wasp:ComboBoxControl ID="GrievanceResolutionCode" runat="server" Type="GrievanceResolutionCode" ondatabinding="Code_NeedDataSource" 
                      ResourceName="GrievanceResolutionCode" Value='<%# Bind("GrievanceResolutionCode") %>' ReadOnly="False" TabIndex="010"></wasp:ComboBoxControl>
                    
                </td>
                <td>
                  <wasp:DateControl ID="ResolutionDate" runat="server" ResourceName="ResolutionDate" Value='<%# Bind("ResolutionDate") %>' ReadOnly="False" TabIndex="020" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <wasp:TextBoxControl ID="ResolutionNote"  Width="600px"  TextMode="multiline" rows="5" columns="50" LabelStyle="width:75px;" FieldStyle="width: 425px;" runat="server" ResourceName="ResolutionNote" Value='<%# Bind("ResolutionNote") %>'  ReadOnly="False" TabIndex="030" />
                </td>
            </tr>
         </table>
         </div>
    </EditItemTemplate>
</wasp:WLPFormView>
