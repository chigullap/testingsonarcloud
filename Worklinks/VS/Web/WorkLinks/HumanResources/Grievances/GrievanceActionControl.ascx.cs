﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Grievances
{
    public partial class GrievanceActionControl : WLP.Web.UI.WLPUserControl
    {
        public delegate void NeedDataSourceEventHandler(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e);
        public event NeedDataSourceEventHandler NeedDataSource;

        #region properties
        public bool IsViewMode
        {
            get
            {
                return GrievanceActionGrid.IsViewMode;
            }
        }
        public bool IsUpdate
        {
            get { return GrievanceActionGrid.IsEditMode; }
        }
        public bool AddFlag
        {
            get { return WorkLinks.Common.Security.RoleForm.GrievanceDetail.AddFlag; }
        }
        public bool UpdateFlag
        {
            get { return WorkLinks.Common.Security.RoleForm.GrievanceDetail.UpdateFlag; }
        }
        public bool DeleteFlag
        {
            get { return WorkLinks.Common.Security.RoleForm.GrievanceDetail.DeleteFlag; }
        }

        public IDataItemCollection<IDataItem> DataSource
        {
            get
            {
                return GrievanceActionGrid.DataSource;
            }
            set
            {
                GrievanceActionGrid.DataSource = value;
            }
        }
        #endregion

        #region event triggers
        protected virtual void OnNeedDataSource(Telerik.Web.UI.GridNeedDataSourceEventArgs args)
        {
            if (NeedDataSource != null)
            {
                NeedDataSource(this, args);
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialize();
        }

        protected void Initialize()
        {
            //Find the GrievanceActionGrid
            WLPGrid grid = (WLPGrid)this.FindControl("GrievanceActionGrid");

            //hide the edit/delete images in the rows.
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
            grid.MasterTableView.GetColumn("deleteButton").Visible = DeleteFlag;
        }

        protected void GrievanceActionGrid_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            OnNeedDataSource(e);
        }
        protected void GrievanceActionGrid_InsertCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            OnInserting(new WLPItemInsertingEventArgs(e));
        }
        protected void GrievanceActionGrid_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            OnUpdating(new WLPItemUpdatingEventArgs(e));

        }
        protected void GrievanceActionGrid_DeleteCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            OnDeleting(new WLPItemDeletingEventArgs(e));
        }

        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
    }
}