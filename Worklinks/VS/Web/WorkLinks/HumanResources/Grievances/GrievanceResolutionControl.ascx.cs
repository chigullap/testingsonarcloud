﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;

namespace WorkLinks.HumanResources.Grievances
{
    public partial class GrievanceResolutionControl : WLP.Web.UI.WLPUserControl
    {
        public delegate void NeedDataSourceEventHandler(object sender, WLP.Web.UI.Controls.WLPNeedDataSourceEventArgs e);
        public event NeedDataSourceEventHandler NeedDataSource;

        #region properties
        public IDataItemCollection<IDataItem> DataSource
        {
            get
            {
                return GrievanceResolutionFormView.DataSource;
            }
            set
            {
                GrievanceResolutionFormView.DataSource = value;
            }
        }
        public bool IsViewMode
        {
            get { return GrievanceResolutionFormView.CurrentMode.Equals(FormViewMode.ReadOnly); }
        }
        public bool IsEditMode
        {
            get { return GrievanceResolutionFormView.CurrentMode.Equals(FormViewMode.Edit); }
        }
        public bool IsInsertMode
        {
            get { return GrievanceResolutionFormView.CurrentMode.Equals(FormViewMode.Insert); }
        }
        public bool UpdateFlag
        {
            get { return WorkLinks.Common.Security.RoleForm.GrievanceDetail.UpdateFlag; }
        }
        #endregion


        #region event triggers
        protected virtual void OnNeedDataSource(WLP.Web.UI.Controls.WLPNeedDataSourceEventArgs args)
        {
            if (NeedDataSource != null)
            {
                NeedDataSource(this, args);
            }
        }
        #endregion

        protected void GrievanceResolutionFormView_NeedDataSource(object sender, WLP.Web.UI.Controls.WLPNeedDataSourceEventArgs e)
        {
            OnNeedDataSource(e);
        }

        protected void GrievanceResolutionFormView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            OnUpdating(e);
        }

        protected void GrievanceResolutionFormView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            OnInserting(e);
        }


        #region load list controls
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        #endregion
    }
}