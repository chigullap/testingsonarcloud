﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Grievances
{
    public partial class GrievanceControl : WLP.Web.UI.WLPUserControl
    {

        #region fields
        #endregion

        #region properties
        /// <summary>
        /// returns true if the control is in read-only mode
        /// </summary>
        public bool IsViewMode
        {
            get
            {
                return GrievanceDetailControl.IsViewMode
                    && GrievanceActionControl.IsViewMode
                    && GrievanceResolutionControl.IsViewMode;
            }
        }
        private long GrievanceId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["grievanceId"]); }
        }
        private GrievanceActionCollection Actions
        {
            get
            {
                GrievanceActionCollection actions = null;
                if (Grievance != null)
                    actions = Grievance.Actions;
                return actions;
            }
        }
        private Grievance Grievance
        {
            get
            {
                Grievance grievance = null;
                if (Data.Count > 0)
                    grievance = (Grievance)Data[0];
                return grievance;
            }
        }
        #endregion

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            InitializeControls();
            if (GrievanceId < 0)
            {
                this.Page.Title = String.Format(Page.Title, String.Empty);
            }
            else
            {
                this.Page.Title = String.Format(Page.Title + " - {1:MM/dd/yyyy} - {2}", GrievanceId, Grievance.GrievanceDate, Common.CodeHelper.GetCodeDescription(LanguageCode, CodeTableType.GrievanceTypeCode, Grievance.GrievanceTypeCode.ToString()));
            }
        }

        /// <summary>
        /// sets control attributes
        /// </summary>
        protected void InitializeControls()
        {
            GrievanceTabStrip.Enabled = IsViewMode;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //does the user have access rights?
            if (!Page.IsPostBack) SetAuthorized(WorkLinks.Common.Security.RoleForm.GrievanceDetail.ViewFlag);

            WireEvents();
            if (!IsPostBack)
            {
                LoadGrievance(GrievanceId);
                this.DataBind();
            }
        }

        protected void WireEvents()
        {
            //wire detail control
            GrievanceDetailControl.Updating += new UpdatingEventHandler(GrievanceDetailControl_Updating);
            GrievanceDetailControl.Inserting += new InsertingEventHandler(GrievanceDetailControl_Inserting);
            GrievanceDetailControl.NeedDataSource += new Grievances.GrievanceDetailControl.NeedDataSourceEventHandler(GrievanceDetailControl_NeedDataSource);

            //wire action control
            GrievanceActionControl.NeedDataSource += new Grievances.GrievanceActionControl.NeedDataSourceEventHandler(GrievanceActionControl_NeedDataSource);
            GrievanceActionControl.Inserting += new InsertingEventHandler(GrievanceActionControl_Inserting);
            GrievanceActionControl.Updating += new UpdatingEventHandler(GrievanceActionControl_Updating);
            GrievanceActionControl.Deleting += new DeletingEventHandler(GrievanceActionControl_Deleting);

            //wire resolution control
            GrievanceResolutionControl.NeedDataSource += new Grievances.GrievanceResolutionControl.NeedDataSourceEventHandler(GrievanceResolutionControl_NeedDataSource);
            GrievanceResolutionControl.Updating += new UpdatingEventHandler(GrievanceResolutionControl_Updating);
        }

        #region Action control handlers
        void GrievanceActionControl_Deleting(object sender, WLPItemDeletingEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteGrievanceAction((GrievanceAction)e.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void GrievanceActionControl_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.UpdateGrievanceAction((GrievanceAction)e.DataItem).CopyTo(Actions[e.DataItem.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void GrievanceActionControl_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            ((GrievanceAction)e.DataItem).GrievanceId = GrievanceId;
            Common.ServiceWrapper.HumanResourcesClient.InsertGrievanceAction((GrievanceAction)e.DataItem).CopyTo(Actions[e.DataItem.Key]);
        }

        void GrievanceActionControl_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            GrievanceActionControl.DataSource = Actions;
        }
        #endregion


        void GrievanceDetailControl_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            Response.Redirect(String.Format("~/HumanResources/Grievance/View/{0}", Common.ServiceWrapper.HumanResourcesClient.InsertGrievance((Grievance)e.DataItem).GrievanceId));
        }

        void GrievanceDetailControl_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            UpdateGrievance();
        }
        void GrievanceResolutionControl_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            UpdateGrievance();
        }

        private void UpdateGrievance()
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.UpdateGrievance(Grievance).CopyTo(Grievance);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void GrievanceResolutionControl_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            GrievanceResolutionControl.DataSource = Data;
        }

        protected void LoadGrievance(long grievanceId)
        {
            if (grievanceId < 0)
                Data = new GrievanceCollection() { new Grievance() };
            else
                Data = GrievanceCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetGrievance(grievanceId));
        }

        protected void GrievanceDetailControl_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            GrievanceDetailControl.DataSource = Data;
        }
    }
}