﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Common.Security;


namespace WorkLinks.HumanResources.Grievances
{
    public partial class GrievanceSearchControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        #endregion

        protected GrievanceCriteria Criteria
        {
            get
            {
                GrievanceCriteria criteria = new GrievanceCriteria();
                criteria.GrievanceTypeCode = (String)GrievanceTypeCode.Value;
                criteria.GrievanceStatusCode = (String)GrievanceStatusCode.Value;
                criteria.GrievanceStartDate= (DateTime?)GrievanceStartDate.Value;
                criteria.GrievanceEndDate = (DateTime?)GrievanceEndDate.Value;
                criteria.Note = (String)Description.Value;

                return criteria;
            }
            set
            {
                GrievanceTypeCode.Value = value.GrievanceTypeCode;
                GrievanceStatusCode.Value = value.GrievanceStatusCode;
                GrievanceStartDate.Value = value.GrievanceStartDate;
                GrievanceEndDate.Value = value.GrievanceEndDate;
                Description.Value = value.Note;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Panel1.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search(Criteria);
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Criteria = new GrievanceCriteria();
            Data = null;
            GrievanceSummaryGrid.DataBind();
        }

        public void Search(GrievanceCriteria criteria)
        {
            GrievanceCollection collection = new GrievanceCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetGrievanceSummary(criteria));
            Data = collection;
            GrievanceSummaryGrid.Rebind();
        }

        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void GrievanceSummaryGrid_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            ((WLPGrid)source).DataSource = Data;
        }

        protected void AddToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && RoleForm.GrievanceSearch.AddFlag; }
        protected void DetailsToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && RoleForm.GrievanceDetail.ViewFlag; }
        
    }
}