﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GrievanceControl.ascx.cs" Inherits="WorkLinks.HumanResources.Grievances.GrievanceControl" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register assembly="WLP.Web.UI" namespace="WLP.Web.UI.Controls" tagprefix="wasp" %>
<%@ Register src="GrievanceActionControl.ascx" tagname="GrievanceActionControl" tagprefix="uc2" %>
<%@ Register src="GrievanceResolutionControl.ascx" tagname="GrievanceResolutionControl" tagprefix="uc3" %>
<%@ Register src="GrievanceDetailControl.ascx" tagname="GrievanceDetailControl" tagprefix="uc1" %>

<wasp:WLPTabStrip ID="GrievanceTabStrip" runat="server" SelectedIndex="0" MultiPageID="GrievanceMultiPage">
    <Tabs>
        <wasp:WLPTab runat="server" Text="Detail" PageViewID="DetailPageView" ResourceName="DetailTab" />
        <wasp:WLPTab runat="server" Text="Steps" PageViewID="ActionPageView" ResourceName="StepsTab" />
        <wasp:WLPTab runat="server" Text="Resolution" PageViewID="ResolutionPageView" ResourceName="ResolutionTab" />
    </Tabs>
</wasp:WLPTabStrip>

<telerik:RadMultiPage ID="GrievanceMultiPage" runat="server" SelectedIndex="0">
    <telerik:RadPageView ID="DetailPageView" runat="server">
        <uc1:GrievanceDetailControl ID="GrievanceDetailControl" runat="server" />
    </telerik:RadPageView>
    <telerik:RadPageView ID="ActionPageView" runat="server">
        <uc2:GrievanceActionControl ID="GrievanceActionControl" runat="server" />
    </telerik:RadPageView>
    <telerik:RadPageView ID="ResolutionPageView" runat="server">
        <uc3:GrievanceResolutionControl ID="GrievanceResolutionControl" runat="server" />
    </telerik:RadPageView>
</telerik:RadMultiPage>