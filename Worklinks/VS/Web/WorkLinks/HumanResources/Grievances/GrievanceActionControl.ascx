﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GrievanceActionControl.ascx.cs" Inherits="WorkLinks.HumanResources.Grievances.GrievanceActionControl" %>
<%@ Register assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPGrid 
    ID="GrievanceActionGrid" 
    runat="server"
    GridLines="None" 
    AutoGenerateColumns="False"
    onneeddatasource="GrievanceActionGrid_NeedDataSource" 
    ondeletecommand="GrievanceActionGrid_DeleteCommand" 
    oninsertcommand="GrievanceActionGrid_InsertCommand"
    onupdatecommand="GrievanceActionGrid_UpdateCommand">

    <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
        <Selecting AllowRowSelect="True" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">   
        <CommandItemTemplate>
            <wasp:WLPToolBar ID="EmployeeSummaryToolBar" runat="server" Width="100%" AutoPostBack="true">
                <Items>
                    <wasp:WLPToolBarButton Text="Add" CommandName="InitInsert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsViewMode && AddFlag%>' ResourceName="Add" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>
        
        <Columns>
            <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="GrievanceActionStepCode" Type="GrievanceActionStepCode" ResourceName="GrievanceActionStepCode" />
            <wasp:GridDateTimeControl DataField="ActionDate" DataType="System.DateTime" LabelText="ActionDate" SortExpression="ActionDate" UniqueName="ActionDate" ResourceName="ActionDate" />
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="GrievanceActionStepResolutonCode" Type="GrievanceActionStepResolutonCode" ResourceName="GrievanceActionStepResolutonCode" />
            <wasp:GridBoundControl DataField="Note" LabelText="Note" SortExpression="Note" UniqueName="Note" ResourceName="Note" />
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete"  ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning%>" ResourceName="Delete" />
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table width="100%" border="0">
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="GrievanceActionStepCode" runat="server" Type="GrievanceActionStepCode" ondatabinding="Code_NeedDataSource" ResourceName="GrievanceActionStepCode" Value='<%# Bind("GrievanceActionStepCode") %>' TabIndex="010" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="ActionDate" runat="server" ResourceName="ActionDate" Value='<%# Bind("ActionDate") %>'  ReadOnly="false" TabIndex="020" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="GrievanceActionStepResolutonCode" runat="server" Type="GrievanceActionStepResolutonCode" ondatabinding="Code_NeedDataSource" ResourceName="GrievanceActionStepResolutonCode" Value='<%# Bind("GrievanceActionStepResolutonCode") %>' TabIndex="030" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="Note" Width="600px" TextMode="multiline" LabelStyle="width:75px;" FieldStyle="width: 425px;" rows="3" columns="40" runat="server" ResourceName="Note" Value='<%# Bind("Note") %>' ReadOnly="false" TabIndex="040" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <wasp:WLPButton  id="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>'  ResourceName="Update" />
                                        <wasp:WLPButton id="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# !IsUpdate %>'  ResourceName="Insert" />
                                    </td>
                                    <td>
                                        <wasp:WLPButton id="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" text="Cancel" runat="server" CommandName="cancel" CausesValidation="False" ResourceName="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>
    </MasterTableView>

    <HeaderContextMenu EnableAutoScroll="True" />

</wasp:WLPGrid>