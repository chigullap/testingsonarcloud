﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GrievanceDetailControl.ascx.cs" Inherits="WorkLinks.HumanResources.Grievances.GrievanceDetailControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Src="../../Payroll/PayrollBatch/EmployeeControl.ascx" TagName="EmployeeControl" TagPrefix="uc1" %>

<wasp:WLPFormView
    ID="GrievanceDetailFormView"
    runat="server"
    RenderOuterTable="false"
    OnNeedDataSource="GrievanceDetailFormView_NeedDataSource"
    OnInserting="GrievanceDetailFormView_Inserting"
    OnUpdating="GrievanceDetailFormView_Updating">

    <ItemTemplate>
        <wasp:WLPToolBar ID="GrievanceDetailToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Text="Edit" Visible='<%# UpdateFlag %>' ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="edit" ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <div>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:RadioButtonControl ID="GrievanceFiledByCode" runat="server" Type="GrievanceFiledByCode" OnDataBinding="Code_NeedDataSource" LabelText="" Value='<%# Bind("GrievanceFiledByCode") %>' OnPreRender="GrievanceFiledByCode_PreRender" ReadOnly="true" ResourceName="GrievanceFiledByCode"></wasp:RadioButtonControl>
                        </td>
                        <td>
                            <span id="ReferenceSpan">
                                <wasp:TextBoxControl ID="Reference" runat="server" ResourceName="Reference" Value='<%# Bind("Reference") %>' ReadOnly="true" />
                            </span>
                            <span id="ReferenceLabourUnionIdSpan">
                                <wasp:ComboBoxControl ID="ReferenceLabourUnionId" runat="server" Type="CountryCode" OnDataBinding="ReferenceLabourUnionId_DataBinding" ResourceName="ReferenceLabourUnionId" Value='<%# Bind("ReferenceLabourUnionId") %>' ReadOnly="true"></wasp:ComboBoxControl>
                            </span>
                            <span id="ReferenceEmployeeIdSpan">
                                <wasp:KeyValueControl ID="ReferenceEmployeeId" runat="server" Type="ReferenceEmployeeId" OnDataBinding="ReferenceEmployeeId_DataBinding" ResourceName="ReferenceEmployeeId" Value='<%# Bind("ReferenceEmployeeId") %>' ReadOnly="true"></wasp:KeyValueControl>
                            </span>
                        </td>
                    </tr>
                </table>
                <hr />
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:DateControl ID="GrievanceDate" runat="server" ResourceName="GrievanceDate" Value='<%# Eval("GrievanceDate") %>' ReadOnly="true" />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="GrievanceTypeCode" runat="server" Type="GrievanceTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="GrievanceTypeCode" Value='<%# Eval("GrievanceTypeCode") %>' ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <wasp:TextBoxControl ID="Note" Width="550px" FieldWidth="400px" runat="server" ResourceName="Description" Value='<%# Eval("Note") %>' TextMode="multiline" rows="3" ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="ContactReference" runat="server" ResourceName="ContactReference" Value='<%# Eval("ContactReference") %>' ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="GrievanceStatusCode" runat="server" Type="GrievanceStatusCode" ondatabinding="Code_NeedDataSource" ResourceName="GrievanceStatusCode" Value='<%# Eval("GrievanceStatusCode") %>' ReadOnly="true"  />
                        </td>
                        <td>
                            <wasp:DateControl ID="LastStatusDatetime" runat="server" ResourceName="LastStatusDatetime" Value='<%# Eval("LastStatusDatetime") %>' ReadOnly="true" />
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar ID="GrievanceDetailToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton onclick="Close()" Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" ResourceName="Cancel" CausesValidation="false"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <div>
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:RadioButtonControl ID="GrievanceFiledByCode" runat="server" Type="GrievanceFiledByCode" OnDataBinding="GrievanceFiledByCode_NeedDataSource" LabelText="" Value='<%# Bind("GrievanceFiledByCode") %>' OnPreRender="GrievanceFiledByCode_PreRender" ReadOnly="false" ResourceName="GrievanceFiledByCode" TabIndex="010"></wasp:RadioButtonControl>
                        </td>
                        <td>
                            <span id="ReferenceSpan">
                                <wasp:TextBoxControl ID="Reference" runat="server" ResourceName="Reference" Value='<%# Bind("Reference") %>' TabIndex="020"  />
                            </span>
                            <span id="ReferenceLabourUnionIdSpan">
                                <wasp:ComboBoxControl ID="ReferenceLabourUnionId" runat="server" Type="CountryCode" OnDataBinding="ReferenceLabourUnionId_DataBinding" ResourceName="ReferenceLabourUnionId" Value='<%# Bind("ReferenceLabourUnionId") %>' ReadOnly="false" TabIndex="030"></wasp:ComboBoxControl>
                            </span>
                            <span id="ReferenceEmployeeIdSpan">
                                <uc1:EmployeeControl ID="EmployeeControl1" runat="server" Value='<%# Bind("ReferenceEmployeeId") %>' TabIndex="030"/>
                            </span>
                        </td>
                    </tr>
                </table>
                <hr />
                <table width="100%">
                    <tr>
                        <td>
                            <wasp:DateControl ID="GrievanceDate" runat="server" ResourceName="GrievanceDate" Value='<%# Bind("GrievanceDate") %>' ReadOnly="false" TabIndex="050" />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="GrievanceTypeCode" runat="server" Type="GrievanceTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="GrievanceTypeCode" Value='<%# Bind("GrievanceTypeCode") %>' ReadOnly="false" TabIndex="060" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <wasp:TextBoxControl ID="Note" Width="550px" FieldWidth="400px" runat="server" ResourceName="Description" Value='<%# Bind("Note") %>' TextMode="multiline" rows="3" ReadOnly="false" TabIndex="070" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:TextBoxControl ID="ContactReference" runat="server" ResourceName="ContactReference" Value='<%# Bind("ContactReference") %>' ReadOnly="false" TabIndex="080" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="GrievanceStatusCode" runat="server" Type="GrievanceStatusCode" OnDataBinding="Code_NeedDataSource" ResourceName="GrievanceStatusCode" Value='<%# Bind("GrievanceStatusCode") %>' ReadOnly="false" TabIndex="090" />
                        </td>
                        <td>
                            <wasp:DateControl ID="LastStatusDatetime" runat="server" ResourceName="LastStatusDatetime" Value='<%# Bind("LastStatusDatetime") %>' ReadOnly="false" TabIndex="100" />
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </EditItemTemplate>

</wasp:WLPFormView>

<script type="text/javascript">
    var isVisible = false;

    function checkedChanged(item) {
        changeVisibility(item.value);
    }

    function changeVisibility(type) {
        var showOther = (type == 'OTH') ? 'block' : 'none';
        var showEmployee = (type == 'EMP') ? 'inline' : 'none';
        var showUnion = (type == 'UNN') ? 'block' : 'none';

        //get controls to hide/show
        var otherText = $get('ReferenceSpan');
        var unionCombo = $get('ReferenceLabourUnionIdSpan');
        var employeeText = $get('ReferenceEmployeeIdSpan');

        otherText.style.display = showOther;
        unionCombo.style.display = showUnion;
        employeeText.style.display = showEmployee;

        var otherTextValidator = document.getElementById('<%= GrievanceDetailFormView.FindControl("Reference").FindControl("Validator").ClientID %>');
        var unionComboValidator = document.getElementById('<%= GrievanceDetailFormView.FindControl("ReferenceLabourUnionId").FindControl("Validator").ClientID %>');
        var employeeValidator = document.getElementById('<%= GrievanceDetailFormView.FindControl("EmployeeControl1") == null ? "" : GrievanceDetailFormView.FindControl("EmployeeControl1").FindControl("EmployeeNumber").FindControl("Validator").ClientID %>');

        if (otherTextValidator != null)
            ValidatorEnable(otherTextValidator, (showOther == 'block'));

        if (unionComboValidator != null)
            ValidatorEnable(unionComboValidator, (showUnion == 'block'));

        if (employeeValidator != null)
            ValidatorEnable(employeeValidator, (showEmployee == 'inline'));
    }

    function getRadWindow() {
        var popup = null;

        if (window.radWindow)
            popup = window.radWindow;
        else if (window.frameElement.radWindow)
            popup = window.frameElement.radWindow;

        return popup;
    }

    function Close(button, args) {
        var popup = getRadWindow();
        setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
    }

    function ToolBarClick(sender, args) {
        if (<%= IsInsertMode.ToString().ToLower() %>) {
            var button = args.get_item();
            var commandName = button.get_commandName();

            if (commandName == "cancel") {
                window.close();
            }
        }
    }
</script>