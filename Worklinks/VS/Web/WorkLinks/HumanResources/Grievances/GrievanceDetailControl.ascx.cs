﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;

namespace WorkLinks.HumanResources.Grievances
{
    public partial class GrievanceDetailControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public IDataItemCollection<IDataItem> DataSource
        {
            get { return GrievanceDetailFormView.DataSource; }
            set { GrievanceDetailFormView.DataSource = value; }
        }
        private String Action
        {
            get { return Page.RouteData.Values["action"].ToString().ToLower(); }
        }
        public bool IsViewMode
        {
            get { return GrievanceDetailFormView.CurrentMode.Equals(FormViewMode.ReadOnly); }
        }
        public bool IsEditMode
        {
            get { return GrievanceDetailFormView.CurrentMode.Equals(FormViewMode.Edit); }
        }
        public bool IsInsertMode
        {
            get { return GrievanceDetailFormView.CurrentMode.Equals(FormViewMode.Insert); }
        }
        public bool UpdateFlag
        {
            get { return Common.Security.RoleForm.GrievanceDetail.UpdateFlag; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.Length > 11 && args.Substring(0, 11).ToLower().Equals("employeeid="))
                ChangeVisibility("EMP");
        }
        protected void GrievanceDetailFormView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            if (!IsPostBack)
            {
                if (Action == "add")
                    GrievanceDetailFormView.ChangeMode(FormViewMode.Insert);
            }

            OnNeedDataSource(e);
        }
        protected void ReferenceLabourUnionId_DataBinding(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateComboBoxWithUnion((ComboBoxControl)sender, LanguageCode);
        }
        protected void GrievanceFiledByCode_PreRender(object sender, EventArgs e)
        {
            RadioButtonControl control = (RadioButtonControl)sender;

            foreach (ListItem item in control.Items)
                item.Attributes.Add("onclick", "checkedChanged(this)");

            ChangeVisibility((RadioButtonControl)sender);
        }
        protected void ChangeVisibility(RadioButtonControl list)
        {
            ChangeVisibility(list.SelectedValue);
        }
        protected void ChangeVisibility(String selectedValue)
        {
            ClientScriptManager manager = Page.ClientScript;
            manager.RegisterStartupScript(this.GetType(), "initialize", String.Format("changeVisibility('{0}');", selectedValue), true);
        }
        protected void ReferenceEmployeeId_DataBinding(object sender, EventArgs e)
        {
            PopulateEmployeeControl((KeyValueControl)sender);
        }
        protected void PopulateEmployeeControl(KeyValueControl control)
        {
            Common.CodeHelper.PopulateEmployeeControl(control, false);
        }
        protected void GrievanceDetailFormView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            OnUpdating(e);
        }
        protected void GrievanceDetailFormView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            OnInserting(e);
        }
        #endregion

        #region load list controls
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void GrievanceFiledByCode_NeedDataSource(object sender, EventArgs e)
        {
            if (GrievanceDetailFormView.CurrentMode.Equals(FormViewMode.Insert))
                ((RadioButtonControl)sender).Value = "OTH";

            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        #endregion
    }
}