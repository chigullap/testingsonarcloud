﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GrievanceSearchControl.ascx.cs" Inherits="WorkLinks.HumanResources.Grievances.GrievanceSearchControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<wasp:WLPAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="GrievanceSummaryGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="GrievanceSummaryGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="GrievanceSummaryGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</wasp:WLPAjaxManagerProxy>

<table width="100%">
    <tr valign="top">
        <td>
            <div>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                    <table width="800px">
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="GrievanceTypeCode" runat="server" ResourceName="GrievanceTypeCode" Type="GrievanceTypeCode" ReadOnly="False" OnDataBinding="Code_NeedDataSource" TabIndex="010" />
                            </td>
                            <td>
                                <wasp:ComboBoxControl ID="GrievanceStatusCode" runat="server" ResourceName="GrievanceStatusCode" Type="GrievanceStatusCode" ReadOnly="False" OnDataBinding="Code_NeedDataSource" TabIndex="020" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:DateControl ID="GrievanceStartDate" runat="server" ResourceName="GrievanceStartDate" ReadOnly="False" TabIndex="030" />
                            </td>
                            <td>
                                <wasp:DateControl ID="GrievanceEndDate" runat="server" ResourceName="GrievanceEndDate" ReadOnly="False" TabIndex="040" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="Description" runat="server" ResourceName="Description" ReadOnly="False" TabIndex="050" />
                            </td>
                        </tr>
                    </table>
                    <div class="SearchCriteriaButtons">
                        <wasp:WLPButton ID="btnClear" Icon-PrimaryIconUrl="~/App_Themes/Default/clear.gif" runat="server" Text="Clear" ResourceName="btnClear" OnClick="btnClear_Click" />
                        <wasp:WLPButton ID="btnSearch" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" runat="server" Text="Search" ResourceName="btnSearch" OnClick="btnSearch_Click" />
                    </div>
                </asp:Panel>
            </div>
        </td>
    </tr>
</table>

<wasp:WLPToolBar ID="GrievanceSummaryToolBar" runat="server" Width="100%" OnClientLoad="EmployeeSummaryToolBar_init">
    <Items>
        <wasp:WLPToolBarButton OnPreRender="AddToolBar_PreRender" Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" onclick="return Add()" CommandName="add" ResourceName="Add"></wasp:WLPToolBarButton>
        <wasp:WLPToolBarButton OnPreRender="DetailsToolBar_PreRender" Text="Details" ImageUrl="" onclick="Open()" CommandName="details" ResourceName="Details"></wasp:WLPToolBarButton>
    </Items>
</wasp:WLPToolBar>

<wasp:WLPGrid
    ID="GrievanceSummaryGrid"
    runat="server"
    AllowPaging="true"
    PagerStyle-AlwaysVisible="true"
    PageSize="100"
    AllowSorting="true"
    GridLines="None"
    Height="400px"
    OnNeedDataSource="GrievanceSummaryGrid_NeedDataSource">

    <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowClick="OnRowClick" OnRowDblClick="OnRowDblClick" />
    </ClientSettings>

    <MasterTableView ClientDataKeyNames="GrievanceId" AutoGenerateColumns="False" CommandItemDisplay="Top">

        <CommandItemTemplate></CommandItemTemplate>

        <Columns>
            <wasp:GridBoundControl DataField="GrievanceId" LabelText="GrievanceId" SortExpression="GrievanceId" UniqueName="GrievanceId" ResourceName="GrievanceId" HeaderStyle-Width="8%" />
            <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="GrievanceTypeCode" DataField="GrievanceTypeCode" SortExpression="GrievanceTypeCode" Type="GrievanceTypeCode" ResourceName="GrievanceTypeCode" HeaderStyle-Width="10%" />
            <wasp:GridDateTimeControl DataField="GrievanceDate" DataType="System.DateTime" LabelText="GrievanceDate" SortExpression="GrievanceDate" UniqueName="GrievanceDate" ResourceName="GrievanceDate" HeaderStyle-Width="13%" />
            <wasp:GridBoundControl DataField="Note" LabelText="Description" SortExpression="Note" UniqueName="Note" ResourceName="Description" HeaderStyle-Width="69%" />
        </Columns>

    </MasterTableView>

    <HeaderContextMenu EnableAutoScroll="true" />
</wasp:WLPGrid>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="GrievanceWindows"
    Width="1000"
    Height="600"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="True"
    Modal="true"
    ShowContentDuringLoad="false">
</wasp:WLPWindowManager>

<script type="text/javascript">
    var grievanceId;
    var rowSelected;
    var toolBar;

    function EmployeeSummaryToolBar_init(sender) {
        toolbar = sender;
        addButton = toolbar.findButtonByCommandName('add');
        detailsButton = toolbar.findButtonByCommandName('details');

        if (detailsButton != null)
            detailsButton.disable();
        GrievanceId = null;
    }

    function OnRowClick(sender, eventArgs) {
        grievanceId = eventArgs.getDataKeyValue('GrievanceId');
        rowSelected = true;
        if (detailsButton != null)
            detailsButton.enable();
    }

    function OnRowDblClick(sender, eventArgs) {
        if (toolbar.findButtonByCommandName('details') != null)
            Open(sender);
    }

    function Open() {
        if (grievanceId != null)
            openWindowWithManager('GrievanceWindows', String.format('/HumanResources/Grievance/View/{0}', grievanceId), false);
    }

    function Add() {
        openWindowWithManager('GrievanceWindows', '/HumanResources/Grievance/Add/-1'), false;
        return false;
    }
</script>