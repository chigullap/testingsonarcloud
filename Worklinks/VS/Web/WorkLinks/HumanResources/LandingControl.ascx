﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LandingControl.ascx.cs" Inherits="WorkLinks.HumanResources.LandingControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<wasp:WLPLabel ID="WelcomeLabel" runat="server" CssClass="welcome" OnPreRender="WelcomeLabel_PreRender" />

<table width="100%" style="border-spacing: 0 !important;">
    <tr>
        <td valign="top" width="50%">
            <asp:PlaceHolder ID="WelcomeWidgetPlaceHolder" runat="server" />
        </td>
        <td valign="top" width="50%">
            <asp:PlaceHolder ID="VacationSickTimePlaceHolder" runat="server" />
        </td>
    </tr>
</table>

<asp:PlaceHolder ID="EmployeeTimeReviewPlaceHolder" runat="server" />
<asp:PlaceHolder ID="EmployeePayslipWidgetPlaceHolder" runat="server" />

<table width="100%" style="border-spacing: 0 !important;">
    <tr>
        <td valign="top" width="50%">
            <asp:PlaceHolder ID="HeadcountWidgetPlaceHolder" runat="server" />
        </td>
        <td valign="top" width="50%">
            <asp:PlaceHolder ID="HeadcountBreakdownWidgetPlaceHolder" runat="server" />
        </td>
     </tr>
</table>

<asp:PlaceHolder ID="LabourCostWidgetPlaceHolder" runat="server" />
<asp:PlaceHolder ID="PayrollBreakdownWidgetPlaceHolder" runat="server" />
<asp:PlaceHolder ID="EmployeeListWidgetPlaceHolder" runat="server" />
<asp:PlaceHolder ID="UserManagementWidgetPlaceHolder" runat="server" />

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="WidgetWindows"
    Width="1200"
    Height="800"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }
    var expiry = new Date();
        expiry.setMinutes(expiry.getMinutes() + 15);
    document.cookie = "userx" + "=<%=Session["UserName"]%> ; path=/; expires=" + expiry.toGMTString();
    </script>
</telerik:RadScriptBlock>