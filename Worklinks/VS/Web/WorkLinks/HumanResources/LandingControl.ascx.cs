﻿using System;
using System.Security.Claims;
using System.Web.UI.WebControls;

namespace WorkLinks.HumanResources
{
    public partial class LandingControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private bool EnableWelcomeWidget { get { return Common.Security.RoleForm.WelcomeWidget.ViewFlag; } }
        private bool EnableVacationSickTimeWidget { get { return Common.Security.RoleForm.VacationSickTimeWidget.ViewFlag; } }
        private bool EnableEmployeeTimeReviewWidget { get { return Common.Security.RoleForm.EmployeeTimeReviewWidget.ViewFlag; } }
        private bool EnableEmployeePayslipWidget { get { return Common.Security.RoleForm.EmployeePayslipWidget.ViewFlag; } }
        private bool EnableHeadcount { get { return Common.Security.RoleForm.HeadcountWidget.ViewFlag; } }
        private bool EnablePayrollBreakdownWidget { get { return Common.Security.RoleForm.PayrollBreakdownWidget.ViewFlag; } }
        private bool EnableUserManagementWidget { get { return Common.Security.RoleForm.UserManagementWidget.ViewFlag; } }
        private bool EnableLabourCostWidget { get { return Common.Security.RoleForm.LabourCostWidget.ViewFlag; } }
        private bool EnableEmployeeListWidget { get { return Common.Security.RoleForm.EmployeeListWidget.ViewFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            loadWidget(EnableWelcomeWidget, WelcomeWidgetPlaceHolder, "~/Widgets/WelcomeWidget.ascx");
            loadWidget(EnableVacationSickTimeWidget, VacationSickTimePlaceHolder, "~/Widgets/VacationSickTimeWidget.ascx");
            loadWidget(EnableEmployeeTimeReviewWidget, EmployeeTimeReviewPlaceHolder, "~/Widgets/EmployeeTimeReviewWidget.ascx");
            loadWidget(EnableEmployeePayslipWidget, EmployeePayslipWidgetPlaceHolder, "~/Widgets/EmployeePayslipWidget.ascx");
            loadWidget(EnableHeadcount, HeadcountWidgetPlaceHolder, "~/Widgets/HeadcountWidget.ascx");
            loadWidget(EnableHeadcount, HeadcountBreakdownWidgetPlaceHolder, "~/Widgets/HeadcountBreakdownWidget.ascx");
            loadWidget(EnablePayrollBreakdownWidget, PayrollBreakdownWidgetPlaceHolder, "~/Widgets/PayrollBreakdownWidget.ascx");
            loadWidget(EnableUserManagementWidget, UserManagementWidgetPlaceHolder, "~/Widgets/UserManagementWidget.ascx");
            loadWidget(EnableLabourCostWidget, LabourCostWidgetPlaceHolder, "~/Widgets/LabourCostWidget.ascx");
            loadWidget(EnableEmployeeListWidget, EmployeeListWidgetPlaceHolder, "~/Widgets/EmployeeListWidget.ascx");
        }
        private void loadWidget(bool enabled, PlaceHolder placeHolder, string virtualPath)
        {
            if (enabled) placeHolder.Controls.Add(LoadControl(virtualPath));
        }
        protected void WelcomeLabel_PreRender(object sender, EventArgs e)
        {
            //EmployeeProfile employeeProfile = Common.ServiceWrapper.HumanResourcesClient.GetEmployeeProfile()[0];
            //((WLP.Web.UI.Controls.WLPLabel)sender).Text = (LanguageCode == "FR" ? "Bienvenue" : "Welcome") + " " + ((ClaimsIdentity)System.Web.HttpContext.Current.User.Identity).FindFirst("name").Value + "!";//yuc
            ((WLP.Web.UI.Controls.WLPLabel)sender).Text = (LanguageCode == "FR" ? "Bienvenue" : "Welcome") + " " + System.Web.HttpContext.Current.User.Identity.Name.ToString() + "!";
        }
        #endregion
    }
}