﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HealthAndSafetyReportControl.ascx.cs" Inherits="WorkLinks.HumanResources.HealthAndSafetyReport.HealthAndSafetyReportControl1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Src="HealthAndSafetyReportViewControl.ascx" TagName="HealthAndSafetyReportViewControl" TagPrefix="uc1" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="AjaxPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="AjaxPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="AjaxPanel" runat="server">
    <div style="width: 99.8%">
        <wasp:WLPToolBar ID="HealthAndSafetyReportSummaryToolBar" runat="server" Width="100%" OnClientLoad="HealthAndSafetyReportSummaryToolBar_init" OnClientButtonClicking="confirmation" OnButtonClick="HealthAndSafetyReportSummaryToolBar_ButtonClick">
            <Items>
                <wasp:WLPToolBarButton OnPreRender="AddToolBar_PreRender" Text="**Add**" CausesValidation="false" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="Add" ResourceName="Add"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton OnPreRender="Delete_PreRender" Text="**Delete**" CausesValidation="false" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ResourceName="Delete"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="**Report**" CausesValidation="false" onclick="openReport();" runat="server" CommandName="Report" ResourceName="Report"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>

        <wasp:WLPGrid
            ID="HealthAndSafetyReportGrid"
            runat="server"
            GridLines="None"
            Width="100%"
            Height="180px"
            AutoGenerateColumns="false"
            OnNeedDataSource="HealthAndSafetyReportGrid_NeedDataSource"
            OnDataBound="HealthAndSafetyReportGrid_DataBound">

            <ClientSettings EnablePostBackOnRowClick="true" AllowKeyboardNavigation="true">
                <Selecting AllowRowSelect="true" />
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <ClientEvents OnRowClick="onRowClick" OnGridCreated="onGridCreated" OnCommand="onCommand" />
            </ClientSettings>

            <MasterTableView ClientDataKeyNames="EmployeeWsibHealthAndSafetyReportId" DataKeyNames="Key" CommandItemDisplay="Top">
                <CommandItemTemplate></CommandItemTemplate>

                <SortExpressions>
                    <telerik:GridSortExpression FieldName="IncidentDate" SortOrder="Descending" />
                </SortExpressions>

                <Columns>
                    <wasp:GridKeyValueControl LabelText="**NameOfEmployer**" DataField="NameOfEmployer" Type="OrganizationUnitId" OnNeedDataSource="NameOfEmployer_NeedDataSource" UniqueName="NameOfEmployer" ResourceName="GridNameOfEmployer"></wasp:GridKeyValueControl>
                    <wasp:GridBoundControl DataField="Branch" LabelText="**Branch**" SortExpression="Branch" UniqueName="Branch" ResourceName="GridBranch"></wasp:GridBoundControl>
                    <wasp:GridDateTimeControl DataField="IncidentDate" LabelText="**IncidentDate**" SortExpression="IncidentDate" DataFormatString="{0:yyyy-MM-dd HH:mm}" UniqueName="IncidentDate" ResourceName="GridIncidentDate"></wasp:GridDateTimeControl>
                    <wasp:GridBoundControl DataField="CauseOfIncident" LabelText="**CauseOfIncident**" SortExpression="CauseOfIncident" UniqueName="CauseOfIncident" ResourceName="GridCauseOfIncident">
                        <HeaderStyle Width="400px" />
                    </wasp:GridBoundControl>
                    <wasp:GridKeyValueControl LabelText="**CausedByMachinery**" DataField="CausedByMachinery" Type="YesNoTypeCode" OnNeedDataSource="Code_NeedDataSource" UniqueName="CausedByMachinery" ResourceName="GridCausedByMachinery"></wasp:GridKeyValueControl>
                </Columns>
            </MasterTableView>

        </wasp:WLPGrid>

        <uc1:HealthAndSafetyReportViewControl runat="server" ID="HealthAndSafetyReportViewControl" Visible="true" />
    </div>
</asp:Panel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="HealthAndSafetyReport"
    Width="900"
    Height="600"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize,Minimize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var toolbar = null;
        var employeeWsibHealthAndSafetyReportId;

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function HealthAndSafetyReportSummaryToolBar_init(sender) {
            initializeControls();
        }

        function initializeControls() {
            if ($find('<%= HealthAndSafetyReportSummaryToolBar.ClientID %>') != null)
                toolbar = $find('<%= HealthAndSafetyReportSummaryToolBar.ClientID %>');

            enableButtons(getSelectedRow() != null);
        }

        function onGridCreated(sender, eventArgs) {
            var selectedRow = getSelectedRow();
            if (selectedRow != null) {
                employeeWsibHealthAndSafetyReportId = selectedRow.getDataKeyValue('EmployeeWsibHealthAndSafetyReportId');
                enableButtons(true);
            }
            else
                employeeWsibHealthAndSafetyReportId = null;
        }

        function onCommand(sender, args) {
            if (args.get_commandName() == 'Page') {
                employeeWsibHealthAndSafetyReportId = null;
                initializeControls();
            }
        }

        function onRowClick(sender, eventArgs) {
            employeeWsibHealthAndSafetyReportId = eventArgs.getDataKeyValue('EmployeeWsibHealthAndSafetyReportId');
            enableButtons(true);
        }

        function getSelectedRow() {
            var grid = $find('<%= HealthAndSafetyReportGrid.ClientID %>');
            if (grid == null)
                return null;

            var masterTable = grid.get_masterTableView();
            if (masterTable == null)
                return null;

            var selectedRow = masterTable.get_selectedItems();
            if (selectedRow.length == 1)
                return selectedRow[0];
            else
                return null;
        }

        function enableButtons(enable) {
            var deleteButton = toolbar.findButtonByCommandName('Delete');
            var reportButton = toolbar.findButtonByCommandName('Report');

            if (deleteButton != null)
                deleteButton.set_enabled(enable && toolbar.get_enabled());

            if (reportButton != null)
                reportButton.set_enabled(enable && toolbar.get_enabled());
        }

        function confirmation(sender, args) {
            var button = args.get_item();
            if (button.get_commandName() == "Delete") {
                var message = "<asp:Literal runat="server" Text="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />";
                args.set_cancel(!confirm(message));
            }
        }

        function ButtonGetEnabled(button) {
            return toolbar.findButtonByCommandName('' + button + '').get_enabled();
        }

        function openReport() {
            if (employeeWsibHealthAndSafetyReportId != null && ButtonGetEnabled("Report")) {
                openWindowWithManager('HealthAndSafetyReport', String.format('/Report/AccidentDangerousOccurrence/{0}/{1}/{2}/{3}/{4}/{5}/{6}', employeeWsibHealthAndSafetyReportId, -1, 'PDF', 'x', 'x', 'x', 'x'), true);
                return false;
            }
        }
    </script>
</telerik:RadScriptBlock>