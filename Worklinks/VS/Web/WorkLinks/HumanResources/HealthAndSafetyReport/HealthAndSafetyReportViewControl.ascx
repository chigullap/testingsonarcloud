﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HealthAndSafetyReportViewControl.ascx.cs" Inherits="WorkLinks.HumanResources.HealthAndSafetyReport.HealthAndSafetyReportViewControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style>
    span.form_exception { color: red; }
    div.form_control span.field_mandatory { margin-left: 9px !important; }
</style>

<wasp:WLPFormView
    ID="HealthAndSafetyReportView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key"
    OnNeedDataSource="HealthAndSafetyReportView_NeedDataSource"
    OnDataBound="HealthAndSafetyReportView_DataBound"
    OnInserting="HealthAndSafetyReportView_Inserting"
    OnUpdating="HealthAndSafetyReportView_Updating">

    <ItemTemplate>
        <wasp:WLPToolBar ID="EmployeeDetailToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Text="Edit" ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="edit" Visible='<%# IsViewMode && UpdateFlag %>' ResourceName="Edit" CausesValidation="false" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset style="border-style: none; margin-top: -12px;">
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <wasp:WLPLabel ID="InjuredPersonLabel" runat="server" Font-Bold="true" ForeColor="Gray" Text="Injured Person's" ResourceName="InjuredPerson" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="LastName" runat="server" Text="Last name" ResourceName="LastName" ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="OtherNames" runat="server" Text="Other names" ResourceName="OtherNames" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="GenderCode" runat="server" Text="Sex" ResourceName="GenderCode" Type="GenderCode" OnDataBinding="Code_NeedDataSource" ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="Age" runat="server" Text="Age last birthday" ResourceName="Age" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PersonAddressLine1" runat="server" Text="Address line 1" ResourceName="PersonAddressLine1" ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="PersonCity" runat="server" Text="City" ResourceName="PersonCity" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PersonAddressLine2" runat="server" Text="Address line 2" ResourceName="PersonAddressLine2" ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="PersonProvinceStateCode" runat="server" Text="Province" Type="ProvinceStateCode" ResourceName="PersonProvinceStateCode" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="Occupation" runat="server" Text="Precise occupation" ResourceName="Occupation" ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="PersonCountryCode" runat="server" Text="Country" Type="CountryCode" OnDataBinding="Code_NeedDataSource" ResourceName="PersonCountryCode" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <wasp:TextBoxControl ID="PersonPostalZipCode" runat="server" Text="Postal code" ResourceName="PersonPostalZipCode" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="NameOfEmployerLabel" runat="server" ForeColor="Gray" Text="Name of Employer" ResourceName="NameOfEmployer" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:ComboBoxControl ID="NameOfEmployer" runat="server" HideLabel="true" FieldStyle="width: 762px !important;" Width="100%" Type="OrganizationUnitId" OnDataBinding="NameOfEmployer_NeedDataSource" ResourceName="NameOfEmployer" Value='<%# Bind("NameOfEmployer") %>' Mandatory="true" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="NatureOfIndustryLabel" runat="server" ForeColor="Gray" Text="Nature of Industry, occupation or business" ResourceName="NatureOfIndustry" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:TextBoxControl ID="NatureOfIndustry" runat="server" HideLabel="true" FieldStyle="width: 762px !important;" Width="100%" ResourceName="NatureOfIndustry" Value='<%# Bind("NatureOfIndustry") %>' Mandatory="true" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="BranchLabel" runat="server" ForeColor="Gray" Text="Branch or department and exactly where accident or dangerous occurrence happened" ResourceName="Branch" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:TextBoxControl ID="Branch" runat="server" HideLabel="true" FieldStyle="width: 762px !important;" Width="100%" ResourceName="Branch" Value='<%# Bind("Branch") %>' Mandatory="true" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="AddressLine1" runat="server" Text="Address line 1" ResourceName="AddressLine1" Value='<%# Bind("AddressLine1") %>' Mandatory="true" ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="City" runat="server" MaxLength="30" Text="City" ResourceName="City" Value='<%# Bind("City") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="AddressLine2" runat="server" Text="Address line 2" ResourceName="AddressLine2" Value='<%# Bind("AddressLine2") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="CountryCode" runat="server" Text="Country" Type="CountryCode" AutoPostback="true" OnDataBinding="CountryCode_NeedDataSource" OnSelectedIndexChanged="CountryCode_SelectedIndexChanged" ResourceName="CountryCode" Value='<%# Bind("CountryCode") %>' Mandatory="true" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PostalZipCode" runat="server" Text="Postal code" ResourceName="PostalZipCode" Value='<%# Bind("PostalZipCode") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="ProvinceStateCode" runat="server" Text="Province" Type="ProvinceStateCode" ResourceName="ProvinceStateCode" Value='<%# Bind("ProvinceStateCode") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:WLPLabel ID="IncidentDateLabel" runat="server" ForeColor="Gray" Text="Date and hour of accident or dangerous occurrence" ResourceName="IncidentDate" />
                    </td>
                    <td>
                        <wasp:WLPLabel ID="HourStartedWorkLabel" runat="server" ForeColor="Gray" Text="Hour at which injured person started work on day of accident" ResourceName="HourStartedWork" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="Date" runat="server" Text="Date" ResourceName="DateLabel" Value='<%# String.Format("{0:yyyy-MM-dd HH:mm}", Eval("IncidentDate")) %>' ReadOnly="true" Mandatory="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="Hour" runat="server" Text="Hour" ResourceName="HourLabel" Value='<%# String.Format(@"{0:hh\:mm}", Eval("HourStartedWork")) %>' ReadOnly="true" Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="CauseOfIncidentLabel" runat="server" ForeColor="Gray" Text="Cause or nature of accident or dangerous occurrence" ResourceName="CauseOfIncident" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:TextBoxControl ID="CauseOfIncident" runat="server" HideLabel="true" FieldStyle="width: 762px !important;" TextMode="multiline" Width="100%" Rows="3" ResourceName="CauseOfIncident" Value='<%# Bind("CauseOfIncident") %>' Mandatory="true" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="CausedByMachineryLabel" runat="server" ForeColor="Gray" Text="If caused by machinery" ResourceName="CausedByMachinery" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:ComboBoxControl ID="CausedByMachinery" runat="server" HideLabel="true" Type="YesNoTypeCode" AutoPostback="true" OnDataBinding="Code_NeedDataSource" OnSelectedIndexChanged="CausedByMachinery_SelectedIndexChanged" ResourceName="CausedByMachinery" Value='<%# Bind("CausedByMachinery") %>' Mandatory="true" ReadOnly="true" />
                    </td>
                </tr>
                <tr id="machineAndPartNameLabelRow" runat="server">
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="MachineAndPartNameLabel" runat="server" ForeColor="Gray" Text="Name of machine and part causing accident" ResourceName="MachineAndPartName" />
                    </td>
                </tr>
                <tr id="machineAndPartNameRow" runat="server">
                    <td colspan="2">
                        <wasp:TextBoxControl ID="MachineAndPartName" runat="server" HideLabel="true" FieldStyle="width: 762px !important;" TextMode="multiline" Width="100%" Rows="3" ResourceName="MachineAndPartName" Value='<%# Bind("MachineAndPartName") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr id="machineMovedByMechanicalPowerLabelRow" runat="server">
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="MachineMovedByMechanicalPowerLabel" runat="server" ForeColor="Gray" Text="State whether machine was moved by mechanical power at the time of the accident" ResourceName="MachineMovedByMechanicalPower" />
                    </td>
                </tr>
                <tr id="machineMovedByMechanicalPowerRow" runat="server">
                    <td colspan="2">
                        <wasp:TextBoxControl ID="MachineMovedByMechanicalPower" runat="server" HideLabel="true" FieldStyle="width: 762px !important;" TextMode="multiline" Width="100%" Rows="3" ResourceName="MachineMovedByMechanicalPower" Value='<%# Bind("MachineMovedByMechanicalPower") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="InjuriesLabel" runat="server" ForeColor="Gray" Text="Nature, location and extent of injuries" ResourceName="Injuries" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:TextBoxControl ID="Injuries" runat="server" HideLabel="true" FieldStyle="width: 762px !important;" TextMode="multiline" Width="100%" Rows="3" ResourceName="Injuries" Value='<%# Bind("Injuries") %>' Mandatory="true" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="DisabledFromEarningFullWagesLabel" runat="server" ForeColor="Gray" Text="If accident was not fatal, state whether injured person was disabled for more than three days from earning full wages at the work at which he was employed at the time of the accident" ResourceName="DisabledFromEarningFullWages" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:TextBoxControl ID="DisabledFromEarningFullWages" runat="server" HideLabel="true" FieldStyle="width: 762px !important;" TextMode="multiline" Width="100%" Rows="3" ResourceName="DisabledFromEarningFullWages" Value='<%# Bind("DisabledFromEarningFullWages") %>' Mandatory="true" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="DateSignedLabel" runat="server" ForeColor="Gray" Text="Date signed" ResourceName="DateSigned" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:DateControl ID="DateSigned" runat="server" HideLabel="true" ResourceName="DateSigned" Value='<%# Bind("DateSigned") %>' Mandatory="true" ReadOnly="true" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>
    <EditItemTemplate>
        <wasp:WLPToolBar ID="EmployeDetailToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="toolBarClick">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert" />
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update" />
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset style="border-style: none; margin-top: -12px;">
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <wasp:WLPLabel ID="InjuredPersonLabel" runat="server" Font-Bold="true" ForeColor="Gray" Text="Injured Person's" ResourceName="InjuredPerson" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="LastName" runat="server" Text="Last name" ResourceName="LastName" ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="OtherNames" runat="server" Text="Other names" ResourceName="OtherNames" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="GenderCode" runat="server" Text="Sex" ResourceName="GenderCode" Type="GenderCode" OnDataBinding="Code_NeedDataSource" ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="Age" runat="server" Text="Age last birthday" ResourceName="Age" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PersonAddressLine1" runat="server" Text="Address line 1" ResourceName="PersonAddressLine1" ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="PersonCity" runat="server" Text="City" ResourceName="PersonCity" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PersonAddressLine2" runat="server" Text="Address line 2" ResourceName="PersonAddressLine2" ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="PersonProvinceStateCode" runat="server" Text="Province" Type="ProvinceStateCode" ResourceName="PersonProvinceStateCode" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="Occupation" runat="server" Text="Precise occupation" ResourceName="Occupation" ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="PersonCountryCode" runat="server" Text="Country" Type="CountryCode" OnDataBinding="Code_NeedDataSource" ResourceName="PersonCountryCode" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <wasp:TextBoxControl ID="PersonPostalZipCode" runat="server" Text="Postal code" ResourceName="PersonPostalZipCode" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="NameOfEmployerLabel" runat="server" ForeColor="Gray" Text="Name of Employer" ResourceName="NameOfEmployer" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:ComboBoxControl ID="NameOfEmployer" runat="server" HideLabel="true" FieldStyle="width: 762px !important;" Width="100%" Type="OrganizationUnitId" OnDataBinding="NameOfEmployer_NeedDataSource" ResourceName="NameOfEmployer" Value='<%# Bind("NameOfEmployer") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="NatureOfIndustryLabel" runat="server" ForeColor="Gray" Text="Nature of Industry, occupation or business" ResourceName="NatureOfIndustry" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:TextBoxControl ID="NatureOfIndustry" runat="server" HideLabel="true" FieldStyle="width: 762px !important;" Width="100%" ResourceName="NatureOfIndustry" Value='<%# Bind("NatureOfIndustry") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="BranchLabel" runat="server" ForeColor="Gray" Text="Branch or department and exactly where accident or dangerous occurrence happened" ResourceName="Branch" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:TextBoxControl ID="Branch" runat="server" HideLabel="true" FieldStyle="width: 762px !important;" Width="100%" ResourceName="Branch" Value='<%# Bind("Branch") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="AddressLine1" runat="server" Text="Address line 1" ResourceName="AddressLine1" Value='<%# Bind("AddressLine1") %>' Mandatory="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="City" runat="server" MaxLength="30" Text="City" ResourceName="City" Value='<%# Bind("City") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="AddressLine2" runat="server" Text="Address line 2" ResourceName="AddressLine2" Value='<%# Bind("AddressLine2") %>' />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="CountryCode" runat="server" Text="Country" Type="CountryCode" AutoPostback="true" OnDataBinding="CountryCode_NeedDataSource" OnSelectedIndexChanged="CountryCode_SelectedIndexChanged" ResourceName="CountryCode" Value='<%# Bind("CountryCode") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PostalZipCode" runat="server" Text="Postal code" ResourceName="PostalZipCode" Value='<%# Bind("PostalZipCode") %>' />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="ProvinceStateCode" runat="server" Text="Province" Type="ProvinceStateCode" ResourceName="ProvinceStateCode" Value='<%# Bind("ProvinceStateCode") %>' />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:WLPLabel ID="IncidentDateLabel" runat="server" ForeColor="Gray" Text="Date and hour of accident or dangerous occurrence" ResourceName="IncidentDate" />
                    </td>
                    <td>
                        <wasp:WLPLabel ID="HourStartedWorkLabel" runat="server" ForeColor="Gray" Text="Hour at which injured person started work on day of accident" ResourceName="HourStartedWork" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="form_control">
                            <span class="label_mandatory">
                                <wasp:WLPLabel ID="DateLabel" runat="server" Text="Date" ResourceName="Date" />
                            </span>
                            <span class="field_mandatory">
                                <telerik:RadDateTimePicker ID="IncidentDate" Width="200px" runat="server" />
                            </span>
                            <span class="form_exception">
                                <asp:RequiredFieldValidator runat="server" ID="IncidentDateValidator" ControlToValidate="IncidentDate" Display="Dynamic" ErrorMessage="▲" />
                            </span>
                        </div>
                    </td>
                    <td>
                        <div class="form_control">
                            <span class="label_mandatory">
                                <wasp:WLPLabel ID="HourLabel" runat="server" Text="Hour" ResourceName="Hour" />
                            </span>
                            <span class="field_mandatory">
                                <telerik:RadTimePicker ID="HourStartedWork" Width="100px" runat="server" />
                            </span>
                            <span class="form_exception">
                                <asp:RequiredFieldValidator runat="server" ID="HourStartedWorkValidator" ControlToValidate="HourStartedWork" Display="Dynamic" ErrorMessage="▲" />
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="CauseOfIncidentLabel" runat="server" ForeColor="Gray" Text="Cause or nature of accident or dangerous occurrence" ResourceName="CauseOfIncident" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:TextBoxControl ID="CauseOfIncident" runat="server" HideLabel="true" FieldStyle="width: 762px !important;" TextMode="multiline" Width="100%" Rows="3" ResourceName="CauseOfIncident" Value='<%# Bind("CauseOfIncident") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="CausedByMachineryLabel" runat="server" ForeColor="Gray" Text="If caused by machinery" ResourceName="CausedByMachinery" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:ComboBoxControl ID="CausedByMachinery" runat="server" HideLabel="true" Type="YesNoTypeCode" AutoPostback="true" OnDataBinding="Code_NeedDataSource" OnSelectedIndexChanged="CausedByMachinery_SelectedIndexChanged" ResourceName="CausedByMachinery" Value='<%# Bind("CausedByMachinery") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr id="machineAndPartNameLabelRow" runat="server">
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="MachineAndPartNameLabel" runat="server" ForeColor="Gray" Text="Name of machine and part causing accident" ResourceName="MachineAndPartName" />
                    </td>
                </tr>
                <tr id="machineAndPartNameRow" runat="server">
                    <td colspan="2">
                        <wasp:TextBoxControl ID="MachineAndPartName" runat="server" HideLabel="true" FieldStyle="width: 762px !important;" TextMode="multiline" Width="100%" Rows="3" ResourceName="MachineAndPartName" Value='<%# Bind("MachineAndPartName") %>' />
                    </td>
                </tr>
                <tr id="machineMovedByMechanicalPowerLabelRow" runat="server">
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="MachineMovedByMechanicalPowerLabel" runat="server" ForeColor="Gray" Text="State whether machine was moved by mechanical power at the time of the accident" ResourceName="MachineMovedByMechanicalPower" />
                    </td>
                </tr>
                <tr id="machineMovedByMechanicalPowerRow" runat="server">
                    <td colspan="2">
                        <wasp:TextBoxControl ID="MachineMovedByMechanicalPower" runat="server" HideLabel="true" FieldStyle="width: 762px !important;" TextMode="multiline" Width="100%" Rows="3" ResourceName="MachineMovedByMechanicalPower" Value='<%# Bind("MachineMovedByMechanicalPower") %>' />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="InjuriesLabel" runat="server" ForeColor="Gray" Text="Nature, location and extent of injuries" ResourceName="Injuries" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:TextBoxControl ID="Injuries" runat="server" HideLabel="true" FieldStyle="width: 762px !important;" TextMode="multiline" Width="100%" Rows="3" ResourceName="Injuries" Value='<%# Bind("Injuries") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="DisabledFromEarningFullWagesLabel" runat="server" ForeColor="Gray" Text="If accident was not fatal, state whether injured person was disabled for more than three days from earning full wages at the work at which he was employed at the time of the accident" ResourceName="DisabledFromEarningFullWages" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:TextBoxControl ID="DisabledFromEarningFullWages" runat="server" HideLabel="true" FieldStyle="width: 762px !important;" TextMode="multiline" Width="100%" Rows="3" ResourceName="DisabledFromEarningFullWages" Value='<%# Bind("DisabledFromEarningFullWages") %>' Mandatory="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:WLPLabel Width="762px" ID="DateSignedLabel" runat="server" ForeColor="Gray" Text="Date signed" ResourceName="DateSigned" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <wasp:DateControl ID="DateSigned" runat="server" HideLabel="true" ResourceName="DateSigned" Value='<%# Bind("DateSigned") %>' Mandatory="true" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>
</wasp:WLPFormView>

<script type="text/javascript">
    var _employeeId = <%= EmployeeId %>;

    function getRadWindow() {
        var popup = null;

        if (window.radWindow)
            popup = window.radWindow;
        else if (window.frameElement.radWindow)
            popup = window.frameElement.radWindow;
        else if (window.parent.window.frameElement.radWindow)
            popup = window.parent.window.frameElement.radWindow;

        return popup;
    }

    function processClick(commandName) {
        if (commandName.toLowerCase() == 'insert' || commandName.toLowerCase() == 'update') {
            var arg = new Object;

            if (_employeeId != null && _employeeId > 0) {
                arg.closeWithChanges = true;
                arg.employeeId = _employeeId;
                getRadWindow().argument = arg;
            }
        }
    }

    function toolBarClick(sender, args) {
        var button = args.get_item();
        var commandName = button.get_commandName();
        processClick(commandName);
    }
</script>