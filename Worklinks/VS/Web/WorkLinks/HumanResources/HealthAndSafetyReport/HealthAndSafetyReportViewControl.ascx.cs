﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.HealthAndSafetyReport
{
    public partial class HealthAndSafetyReportViewControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public new IDataItemCollection<IDataItem> Data
        {
            get { return base.Data; }
            set { base.Data = value; }
        }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeePosition.UpdateFlag; } }
        public bool IsViewMode { get { return HealthAndSafetyReportView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool IsInsertMode { get { return HealthAndSafetyReportView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsEditMode { get { return HealthAndSafetyReportView.CurrentMode.Equals(FormViewMode.Edit); } }
        protected long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        #endregion

        #region main
        private void Page_Load(object sender, EventArgs e)
        {
        }
        protected internal void ChangeMode(FormViewMode formViewMode)
        {
            HealthAndSafetyReportView.ChangeMode(formViewMode);
        }
        #endregion

        #region event handlers
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void NameOfEmployer_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateCompanyFromOrganizationUnit((ICodeControl)sender);
        }
        protected void CountryCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
            BindProvinceCombo(sender);
        }
        protected void CountryCode_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindProvinceCombo(sender);
        }
        private void BindProvinceCombo(object sender)
        {
            if (sender is ComboBoxControl)
            {
                ComboBoxControl countryCombo = (ComboBoxControl)sender;
                ComboBoxControl provinceCombo = (ComboBoxControl)((WLPFormView)countryCombo.BindingContainer).FindControl("ProvinceStateCode");

                if (countryCombo.SelectedValue != null)
                {
                    provinceCombo.DataSource = Common.ServiceWrapper.CodeClient.GetProvinceStateCode(countryCombo.SelectedValue);
                    provinceCombo.DataBind();
                }
            }
        }
        protected void HealthAndSafetyReportView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            OnNeedDataSource(e);
            HealthAndSafetyReportView.DataSource = Data;
        }
        protected void HealthAndSafetyReportView_DataBound(object sender, EventArgs e)
        {
            LoadInjuredPerson(Common.ServiceWrapper.HumanResourcesClient.GetEmployeeWsibHealthAndSafetyReportPerson(EmployeeId));

            ComboBoxControl causedByMachinery = (ComboBoxControl)HealthAndSafetyReportView.FindControl("CausedByMachinery");
            if (causedByMachinery != null)
                ShowHideMachineSpecificFields(causedByMachinery.SelectedValue);

            if (IsEditMode)
            {
                RadDateTimePicker incidentDate = (RadDateTimePicker)HealthAndSafetyReportView.FindControl("IncidentDate");
                if (incidentDate != null)
                    incidentDate.SelectedDate = ((EmployeeWsibHealthAndSafetyReport)Data[0]).IncidentDate;

                RadTimePicker hourStartedWork = (RadTimePicker)HealthAndSafetyReportView.FindControl("HourStartedWork");
                if (hourStartedWork != null)
                    hourStartedWork.SelectedTime = ((EmployeeWsibHealthAndSafetyReport)Data[0]).HourStartedWork;
            }
        }
        protected void LoadInjuredPerson(EmployeeWsibHealthAndSafetyReportPersonCollection collection)
        {
            TextBoxControl lastName = (TextBoxControl)HealthAndSafetyReportView.FindControl("LastName");
            if (lastName != null)
                lastName.Value = collection[0].LastName;

            TextBoxControl otherNames = (TextBoxControl)HealthAndSafetyReportView.FindControl("OtherNames");
            if (otherNames != null)
                otherNames.Value = collection[0].OtherNames;

            ComboBoxControl genderCode = (ComboBoxControl)HealthAndSafetyReportView.FindControl("GenderCode");
            if (genderCode != null)
                genderCode.Value = collection[0].GenderCode;

            TextBoxControl age = (TextBoxControl)HealthAndSafetyReportView.FindControl("Age");
            if (age != null)
                age.Value = collection[0].Age;

            TextBoxControl personAddressLine1 = (TextBoxControl)HealthAndSafetyReportView.FindControl("PersonAddressLine1");
            if (personAddressLine1 != null)
                personAddressLine1.Value = collection[0].AddressLine1;

            ComboBoxControl personCountryCode = (ComboBoxControl)HealthAndSafetyReportView.FindControl("PersonCountryCode");
            if (personCountryCode != null)
                personCountryCode.Value = collection[0].CountryCode;

            TextBoxControl personAddressLine2 = (TextBoxControl)HealthAndSafetyReportView.FindControl("PersonAddressLine2");
            if (personAddressLine2 != null)
                personAddressLine2.Value = collection[0].AddressLine2;

            TextBoxControl personCity = (TextBoxControl)HealthAndSafetyReportView.FindControl("PersonCity");
            if (personCity != null)
                personCity.Value = collection[0].City;

            ComboBoxControl personProvinceStateCode = (ComboBoxControl)HealthAndSafetyReportView.FindControl("PersonProvinceStateCode");
            if (personProvinceStateCode != null)
            {
                personProvinceStateCode.DataSource = Common.ServiceWrapper.CodeClient.GetProvinceStateCode(personCountryCode.SelectedValue);
                personProvinceStateCode.DataBind();
                personProvinceStateCode.Value = collection[0].ProvinceStateCode;
            }

            TextBoxControl personPostalZipCode = (TextBoxControl)HealthAndSafetyReportView.FindControl("PersonPostalZipCode");
            if (personPostalZipCode != null)
                personPostalZipCode.Value = collection[0].PostalZipCode;

            TextBoxControl occupation = (TextBoxControl)HealthAndSafetyReportView.FindControl("Occupation");
            if (occupation != null)
                occupation.Value = collection[0].Occupation;
        }
        protected void CausedByMachinery_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ShowHideMachineSpecificFields(((ComboBoxControl)sender).SelectedValue);
        }
        protected void ShowHideMachineSpecificFields(String causedByMachinery)
        {
            bool causedByMachineryFlag = false;

            if (causedByMachinery != null)
                causedByMachineryFlag = causedByMachinery.ToLower() == "true" ? true : false;

            WLPLabel machineAndPartNameLabel = (WLPLabel)HealthAndSafetyReportView.FindControl("MachineAndPartNameLabel");
            HtmlTableRow machineAndPartNameLabelRow = (HtmlTableRow)HealthAndSafetyReportView.FindControl("machineAndPartNameLabelRow");

            if (machineAndPartNameLabel != null && machineAndPartNameLabelRow != null)
            {
                machineAndPartNameLabel.Visible = causedByMachineryFlag;
                machineAndPartNameLabelRow.Visible = causedByMachineryFlag;
            }

            TextBoxControl machineAndPartName = (TextBoxControl)HealthAndSafetyReportView.FindControl("MachineAndPartName");
            HtmlTableRow machineAndPartNameRow = (HtmlTableRow)HealthAndSafetyReportView.FindControl("machineAndPartNameRow");

            if (machineAndPartName != null && machineAndPartNameRow != null)
            {
                machineAndPartName.Visible = causedByMachineryFlag;
                machineAndPartName.Mandatory = causedByMachineryFlag;
                machineAndPartNameRow.Visible = causedByMachineryFlag;
            }

            WLPLabel machineMovedByMechanicalPowerLabel = (WLPLabel)HealthAndSafetyReportView.FindControl("MachineMovedByMechanicalPowerLabel");
            HtmlTableRow machineMovedByMechanicalPowerLabelRow = (HtmlTableRow)HealthAndSafetyReportView.FindControl("machineMovedByMechanicalPowerLabelRow");

            if (machineMovedByMechanicalPowerLabel != null && machineMovedByMechanicalPowerLabelRow != null)
            {
                machineMovedByMechanicalPowerLabel.Visible = causedByMachineryFlag;
                machineMovedByMechanicalPowerLabelRow.Visible = causedByMachineryFlag;
            }

            TextBoxControl machineMovedByMechanicalPower = (TextBoxControl)HealthAndSafetyReportView.FindControl("MachineMovedByMechanicalPower");
            HtmlTableRow machineMovedByMechanicalPowerRow = (HtmlTableRow)HealthAndSafetyReportView.FindControl("machineMovedByMechanicalPowerRow");

            if (machineMovedByMechanicalPower != null && machineMovedByMechanicalPowerRow != null)
            {
                machineMovedByMechanicalPower.Visible = causedByMachineryFlag;
                machineMovedByMechanicalPower.Mandatory = causedByMachineryFlag;
                machineMovedByMechanicalPowerRow.Visible = causedByMachineryFlag;
            }

            if (!causedByMachineryFlag)
            {
                machineAndPartName.Value = null;
                machineMovedByMechanicalPower.Value = null;
            }
        }
        #endregion

        #region handles updates
        protected void HealthAndSafetyReportView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            EmployeeWsibHealthAndSafetyReport item = ((EmployeeWsibHealthAndSafetyReport)e.DataItem);

            RadDateTimePicker incidentDate = (RadDateTimePicker)HealthAndSafetyReportView.FindControl("IncidentDate");
            if (incidentDate != null)
                item.IncidentDate = (DateTime)incidentDate.SelectedDate;

            RadTimePicker hourStartedWork = (RadTimePicker)HealthAndSafetyReportView.FindControl("HourStartedWork");
            if (hourStartedWork != null)
                item.HourStartedWork = (TimeSpan)hourStartedWork.SelectedTime;

            OnInserting(e);
        }
        protected void HealthAndSafetyReportView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            EmployeeWsibHealthAndSafetyReport item = ((EmployeeWsibHealthAndSafetyReport)e.DataItem);

            RadDateTimePicker incidentDate = (RadDateTimePicker)HealthAndSafetyReportView.FindControl("IncidentDate");
            if (incidentDate != null)
                item.IncidentDate = (DateTime)incidentDate.SelectedDate;

            RadTimePicker hourStartedWork = (RadTimePicker)HealthAndSafetyReportView.FindControl("HourStartedWork");
            if (hourStartedWork != null)
                item.HourStartedWork = (TimeSpan)hourStartedWork.SelectedTime;

            OnUpdating(e);
        }
        #endregion
    }
}