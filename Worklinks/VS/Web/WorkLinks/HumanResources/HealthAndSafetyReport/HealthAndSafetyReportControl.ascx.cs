﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.HealthAndSafetyReport
{
    public partial class HealthAndSafetyReportControl1 : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private EmployeeWsibHealthAndSafetyReport CurrentHealthAndSafetyReport
        {
            get
            {
                EmployeeWsibHealthAndSafetyReport report = null;

                if (IsInsertMode && !InsertComplete)
                    report = new EmployeeWsibHealthAndSafetyReport() { EmployeeWsibHealthAndSafetyReportId = -1, EmployeeId = EmployeeId };
                else
                {
                    if (HealthAndSafetyReportGrid.SelectedValue != null)
                        report = (EmployeeWsibHealthAndSafetyReport)Data[HealthAndSafetyReportGrid.SelectedValue.ToString()];
                }

                return report;
            }
        }
        protected EmployeeWsibHealthAndSafetyReportCollection CurrentEmployeeWsibHealthAndSafetyReportCollection
        {
            get
            {
                EmployeeWsibHealthAndSafetyReportCollection collection = new EmployeeWsibHealthAndSafetyReportCollection();

                if (CurrentHealthAndSafetyReport != null)
                    collection.Load(CurrentHealthAndSafetyReport);

                return collection;
            }
        }
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        private String LastName { get { return Page.RouteData.Values["lastName"].ToString(); } }
        private String FirstName { get { return Page.RouteData.Values["firstName"].ToString(); } }
        public bool IsViewMode { get { return HealthAndSafetyReportViewControl.IsViewMode; } }
        public bool IsInsertMode { get { return HealthAndSafetyReportViewControl.IsInsertMode; } }
        public bool IsEditMode { get { return HealthAndSafetyReportViewControl.IsEditMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.EmployeeWsibHealthAndSafetyReport.AddFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.EmployeeWsibHealthAndSafetyReport.DeleteFlag; } }
        public bool InsertComplete { get; set; }
        #endregion

        #region main
        private void InitializeControls()
        {
            HealthAndSafetyReportSummaryToolBar.Enabled = IsViewMode;
            HealthAndSafetyReportGrid.Enabled = IsViewMode;
        }
        private void Page_Load(object sender, EventArgs e)
        {
            //does the user have access rights?
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EmployeeWsibHealthAndSafetyReport.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                //does the user have rights to see this employee?
                SetAuthorized(Common.ServiceWrapper.HumanResourcesClient.GetEmployee(new EmployeeCriteria() { EmployeeId = EmployeeId }).Count > 0);
                LoadEmployeeWsibHealthAndSafetyReport(EmployeeId);
                Initialize();
            }

            this.Page.Title = String.Format("Employee WCB Health and Safety Report - {0}, {1}", LastName, FirstName);
        }
        private void WireEvents()
        {
            HealthAndSafetyReportGrid.SelectedIndexChanged += new EventHandler(HealthAndSafetyReportGrid_SelectedIndexChanged);

            HealthAndSafetyReportViewControl.NeedDataSource += new NeedDataSourceEventHandler(HealthAndSafetyReportViewControl_NeedDataSource);
            HealthAndSafetyReportViewControl.Updating += new UpdatingEventHandler(HealthAndSafetyReportViewControl_Updating);
            HealthAndSafetyReportViewControl.Inserting += new InsertingEventHandler(HealthAndSafetyReportViewControl_Inserting);
        }
        private void LoadEmployeeWsibHealthAndSafetyReport(long employeeId)
        {
            Data = Common.ServiceWrapper.HumanResourcesClient.GetEmployeeWsibHealthAndSafetyReport(employeeId);
        }
        protected void Initialize()
        {
            ((WLPToolBarButton)HealthAndSafetyReportSummaryToolBar.FindButtonByCommandName("Add")).Visible = AddFlag;
            ((WLPToolBarButton)HealthAndSafetyReportSummaryToolBar.FindButtonByCommandName("Delete")).Visible = DeleteFlag;
        }
        #endregion

        #region event handlers
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void NameOfEmployer_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateCompanyFromOrganizationUnit((ICodeControl)sender);
        }
        protected void HealthAndSafetyReportGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            HealthAndSafetyReportViewControl.Data = CurrentEmployeeWsibHealthAndSafetyReportCollection;
            HealthAndSafetyReportViewControl.DataBind();
        }
        protected void HealthAndSafetyReportGrid_DataBound(object sender, EventArgs e)
        {
            if (HealthAndSafetyReportGrid.Items.Count > 0)
            {
                if (Data != null && Data.Count > 0)
                    HealthAndSafetyReportGrid.Items[0].Selected = true;
            }

            HealthAndSafetyReportViewControl.DataBind();
        }
        protected void HealthAndSafetyReportGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            HealthAndSafetyReportGrid.DataSource = Data;
        }
        protected void HealthAndSafetyReportViewControl_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            HealthAndSafetyReportViewControl.Data = CurrentEmployeeWsibHealthAndSafetyReportCollection;
        }
        protected void HealthAndSafetyReportSummaryToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            String commandName = ((RadToolBarButton)e.Item).CommandName.ToLower();

            if (commandName.Equals("add"))
            {
                HealthAndSafetyReportViewControl.ChangeMode(FormViewMode.Insert);
                HealthAndSafetyReportViewControl.DataBind();
            }
            else if (commandName.Equals("delete"))
                DeleteEmployeeWsibHealthAndSafetyReport(CurrentHealthAndSafetyReport);
        }
        #endregion

        #region handles updates
        protected void HealthAndSafetyReportViewControl_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            EmployeeWsibHealthAndSafetyReport item = (EmployeeWsibHealthAndSafetyReport)e.DataItem;
            Common.ServiceWrapper.HumanResourcesClient.InsertEmployeeWsibHealthAndSafetyReport(item);
            InsertComplete = true;

            //refresh from database
            LoadEmployeeWsibHealthAndSafetyReport(EmployeeId);
            HealthAndSafetyReportViewControl.DataBind();
            HealthAndSafetyReportGrid.Rebind();
        }
        protected void HealthAndSafetyReportViewControl_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.UpdateEmployeeWsibHealthAndSafetyReport((EmployeeWsibHealthAndSafetyReport)e.DataItem);

                //refresh from database
                LoadEmployeeWsibHealthAndSafetyReport(EmployeeId);
                HealthAndSafetyReportViewControl.DataBind();
                HealthAndSafetyReportGrid.Rebind();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void DeleteEmployeeWsibHealthAndSafetyReport(EmployeeWsibHealthAndSafetyReport report)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeleteEmployeeWsibHealthAndSafetyReport(report);

                //refresh from database
                LoadEmployeeWsibHealthAndSafetyReport(EmployeeId);
                HealthAndSafetyReportViewControl.DataBind();
                HealthAndSafetyReportGrid.Rebind();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region security handlers
        protected void AddToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && AddFlag; }
        protected void Delete_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && DeleteFlag; }
        #endregion

        #region override
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            InitializeControls();
        }
        #endregion
    }
}