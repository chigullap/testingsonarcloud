﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HumanResources/Employee.Master" AutoEventWireup="true" CodeBehind="HealthAndSafetyReportPage.aspx.cs" Inherits="WorkLinks.HumanResources.HealthAndSafetyReport.HealthAndSafetyReportPage" %>
<%@ Register Src="HealthAndSafetyReportControl.ascx" TagName="HealthAndSafetyReportControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <uc1:HealthAndSafetyReportControl ID="HealthAndSafetyReportControl" runat="server" />
</asp:Content>