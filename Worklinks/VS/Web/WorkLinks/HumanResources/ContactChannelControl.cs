﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources
{
    public class ContactChannelControl : WorkLinks.Wizard.WizardUserControl
    {
        public Control FindControlRecursive(Control control, string id)
        {
            if (control == null) return null;

            //try to find the control at the current level
            Control ctrl = control.FindControl(id);

            if (ctrl == null)
            {
                //search the children
                foreach (Control child in control.Controls)
                {
                    ctrl = FindControlRecursive(child, id);
                    if (ctrl != null) break;
                }
            }

            return ctrl;
        }


        public void SetPersonContactChannelPrimaryFlag(CheckBoxControl primaryFlagControl, IDataItemCollection<IDataItem> DataItemCollection)
        {
            if (primaryFlagControl.Checked == true)
            {
                primaryFlagControl.ReadOnly = true;
            }
            else
            {
                //Check to see if New Record
                if (DataItemCollection.Count > 0)
                {
                    //Records exist (Not a new Item)
                    primaryFlagControl.ReadOnly = false;
                }
                else
                {
                    //New Record ....check and set to readonly
                    primaryFlagControl.Checked = true;
                    primaryFlagControl.ReadOnly = true;
                }

            }
        }


        public override void Update(bool updateExterallyControlled)
        {
            //nothing to update, grid already updated
        }

        public override void AddNewDataItem()
        {
            if (DataItemCollection == null)
            {
                DataItemCollection = new PersonContactChannelCollection();
            }
        }

        public override void ChangeModeEdit()
        {

        }
    }
}