﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeTerminationPageControl.ascx.cs" Inherits="WorkLinks.HumanResources.Terminations.EmployeeTerminationPageControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Src="EmployeeTerminationControl.ascx" TagName="EmployeeTerminationControl" TagPrefix="uc1" %>
<%@ Register Src="EmployeeTerminationOtherControl.ascx" TagName="EmployeeTerminationOtherControl" TagPrefix="uc2" %>
<%@ Register Src="EmployeeTerminationROEControl.ascx" TagName="EmployeeTerminationROEControl" TagPrefix="uc3" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="TerminationPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="TerminationPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="TerminationPanel" runat="server">
    <table width="100%">
        <tr>
            <td width="10%" valign="top">
                <wasp:WLPTabStrip ID="RadTabStrip1" runat="server" Width="100%" Orientation="VerticalLeft" MultiPageID="EmployeeMultiPage" OnPreRender="RadTabStrip1_PreRender">
                    <Tabs>
                        <wasp:WLPTab Text="Termination" OnInit="EmployeeTermination_OnInit" PageViewID="EmployeeTerminationControl" ResourceName="Termination" />
                        <wasp:WLPTab Text="ROE" OnInit="EmployeeTerminationROE_OnInit" ResourceName="ROE" />
                        <wasp:WLPTab Text="Other" OnInit="EmployeeTerminationOther_OnInit" ResourceName="Other" />
                    </Tabs>
                </wasp:WLPTabStrip>
            </td>
            <td width="90%" valign="top">
                <telerik:RadMultiPage ID="EmployeeMultiPage" runat="server" OnPreRender="EmployeeMultiPage_PreRender">
                    <telerik:RadPageView ID="EmployeeTerminationControlPage" OnInit="EmployeeTermination_OnInit" runat="server">
                        <uc1:EmployeeTerminationControl ID="EmployeeTerminationControl1" runat="server" />
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="EmployeeTerminationROEControlPage" OnInit="EmployeeTerminationROE_OnInit" runat="server">
                        <uc3:EmployeeTerminationROEControl ID="EmployeeTerminationROEControl1" runat="server" />
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="EmployeeTerminationOtherControlPage" OnInit="EmployeeTerminationOther_OnInit" runat="server">
                        <uc2:EmployeeTerminationOtherControl ID="EmployeeTerminationOtherControl1" runat="server" />
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </td>
        </tr>
    </table>
</asp:Panel>