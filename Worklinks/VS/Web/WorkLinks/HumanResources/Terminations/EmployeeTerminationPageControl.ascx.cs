﻿using System;
using System.Web.UI.WebControls;

namespace WorkLinks.HumanResources.Terminations
{
    public partial class EmployeeTerminationPageControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private int FirstVisibleTab
        {
            get
            {
                if (Common.Security.RoleForm.EmployeeTerminationDetail.ViewFlag)
                    return 0;
                if (Common.Security.RoleForm.EmployeeTerminationOther.ViewFlag)
                    return 2;

                return -1;
            }
        }
        private String LastName
        {
            get
            {
                if (Page.RouteData.Values["lastName"] == null)
                    return null;
                else
                    return Page.RouteData.Values["lastName"].ToString();
            }
        }
        private String FirstName
        {
            get
            {
                if (Page.RouteData.Values["firstName"] == null)
                    return null;
                else
                    return Page.RouteData.Values["firstName"].ToString();
            }
        }
        private String TabToSelect
        {
            get
            {
                if (Page.RouteData.Values["tabNameToSelect"] == null)
                    return null;
                else
                    return Page.RouteData.Values["tabNameToSelect"].ToString();
            }
        }
        protected bool IsViewMode { get { return EmployeeTerminationControl1.IsViewMode && EmployeeTerminationOtherControl1.IsViewMode; } }
        protected bool IsEditMode { get { return EmployeeTerminationControl1.IsEditMode; } }
        protected bool IsInsertMode { get { return EmployeeTerminationControl1.IsInsertMode; } }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.EmployeeTermination);

            if (!IsPostBack)
            {
                this.EmployeeTerminationControl1.EmployeeSelected += new EmployeeTerminationControl.EmployeeSelectedEventHandler(EmployeeTerminationControl1_EmployeeSelected);

                Page.Title = String.Format(Page.Title, LastName, FirstName);

                //select a tab if we are refreshing after an update
                SelectTab(TabToSelect);
            }

            //if this is an update, subscribe so we can refresh the data in the other tabs.
            if (IsEditMode)
                this.EmployeeTerminationControl1.EmployeeSelected += new EmployeeTerminationControl.EmployeeSelectedEventHandler(EmployeeTerminationControl1_EmployeeSelected);
        }
        protected override void OnPreRender(EventArgs e)
        {
            InitializeControls();
            base.OnPreRender(e);
        }
        protected void InitializeControls()
        {
            RadTabStrip1.Enabled = IsViewMode;
        }
        private void SelectTab(string tabToSelect)
        {
            if (tabToSelect == EmployeeTerminationOtherControl1.ID)
            {
                EmployeeTerminationOtherControlPage.Selected = true;
                RadTabStrip1.SelectedIndex = 2;
            }
            else //default to the terminations tab
            {
                EmployeeTerminationControlPage.Selected = true;
                RadTabStrip1.SelectedIndex = 0;
            }
        }
        void EmployeeTerminationControl1_EmployeeSelected(object sender, Employee.Biographical.EmployeeSelectedEventArgs e)
        {
            //pass the employee_position_id to the other controls in this page
            this.EmployeeTerminationOtherControl1.SetEmployeePositionId(e.EmployeePositionId, e.LastDayPaidDate);

            //enable or disable tabs
            if (this.EmployeeTerminationControl1.EmployeePositionExists)
                this.EmployeeTerminationOtherControlPage.Enabled = true;
            else
                this.EmployeeTerminationOtherControlPage.Enabled = false;
        }
        protected void EmployeeTermination_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = Common.Security.RoleForm.EmployeeTerminationDetail.ViewFlag; }
        protected void EmployeeTerminationROE_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = false; } //this is being hidden on TerminationPageControl as per wlinks604
        protected void EmployeeTerminationOther_OnInit(object sender, EventArgs e) { ((WebControl)sender).Visible = Common.Security.RoleForm.EmployeeTerminationOther.ViewFlag; }
        protected void RadTabStrip1_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                RadTabStrip1.SelectedIndex = FirstVisibleTab;
        }
        protected void EmployeeMultiPage_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                EmployeeMultiPage.SelectedIndex = FirstVisibleTab;
        }
    }
}