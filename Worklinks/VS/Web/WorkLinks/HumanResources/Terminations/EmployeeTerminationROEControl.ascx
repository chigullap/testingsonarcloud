﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeTerminationRoeControl.ascx.cs" Inherits="WorkLinks.HumanResources.Terminations.EmployeeTerminationRoeControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPFormView
    ID="EmployeeTerminationRoeView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key"
    OnNeedDataSource="EmployeeTerminationRoeView_NeedDataSource"
    OnUpdating="EmployeeTerminationRoeView_Updating">

    <ItemTemplate>
        <wasp:WLPToolBar ID="EmployeeTerminationRoeDetailToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Visible='<%# UpdateFlag %>' Text="Edit" ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="edit" ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:DateControl ID="TerminationDate" Text="Termination Date:" runat="server" ResourceName="TerminationDate" Value='<%# Eval ("TerminationDate") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:DateControl ID="FirstDayWorked" Text="First Day Worked:" runat="server" ResourceName="FirstDayWorked" Value='<%# Eval ("FirstDayWorked") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="RoeReasonForTermination" Text="Roe Reason For Termination:" runat="server" Type="EmployeeTerminationReasonType" OnDataBinding="Code_NeedDataSource" ResourceName="ROEReasonForTermination" Value='<%# Eval("CodeRoeReasonCd") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:DateControl ID="LastDayPaid" Text="LastDayPaid:" runat="server" ResourceName="LastDayPaid" Value='<%# Eval ("LastDayPaid") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <wasp:DateControl ID="FinalPayPeriodEndingDate" Text="Final Pay Period Ending Date:" runat="server" ResourceName="FinalPayPeriodEndingDate" Value='<%# Eval ("PayrollPeriodCutoffDate") %>' ReadOnly="true" />
                    </td>
                </tr>
            </table>
            <hr />
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <b>
                            <asp:Literal ID="Literal1" runat="server" Text='<%$ Resources:Messages, RoeCompleteOnly %>' /></b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="ExpectedDateOfRecall" Text="Expected Date Of Recall:" runat="server" ResourceName="ExpectedDateOfRecall" Value='<%# Eval ("ExpectedDateOfRecall") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="PaymentStartDate" Text="Payment Start Date:" runat="server" ResourceName="PaymentStartDate" ReadOnly="True" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="CodeEmployeeTerminationRoeDateRecallCd" Text="Date Recall:" runat="server" Type="ROEDateRecallTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="CodeEmployeeTerminationRoeDateRecallCd" Value='<%# Eval("CodeEmployeeTerminationRoeDateRecallCd") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="Amount" Text="Amount:" runat="server" DecimalDigits="2" ResourceName="Amount" Value='<%# Bind("Amount") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <b><asp:Literal ID="Literal2" runat="server" Text='<%$ Resources:Messages, RoeContact %>' /></b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="CodeEmployeeTerminationRoePaidLeaveCd" Text="Paid Leave:" runat="server" Type="ROEPaidLeaveTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="CodeEmployeeTerminationRoePaidLeaveCd" Value='<%# Eval("CodeEmployeeTerminationRoePaidLeaveCd") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="ContactLastName" Text="ContactLastName:" runat="server" ResourceName="ContactLastName" Value='<%# Eval ("ContactLastName") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <wasp:TextBoxControl ID="ContactFirstName" Text="ContactFirstName:" runat="server" ResourceName="ContactFirstName" Value='<%# Eval ("ContactFirstName") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <wasp:TextBoxControl ID="Telephone" Text="Telephone:" runat="server" ResourceName="Telephone" Value='<%# Eval ("ContactTelephone") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <wasp:TextBoxControl ID="ContactTelephoneExtension" Text="ContactTelephoneExtension:" runat="server" ResourceName="ContactTelephoneExtension" Value='<%# Eval ("ContactTelephoneExtension") %>' ReadOnly="true" />
                    </td>
                </tr>
            </table>
            <hr />
            <table width="100%">
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="CodeRoeVacationPayTypeCd" Text="CodeRoeVacationPayType" runat="server" Type="CodeRoeVacationPayType" OnDataBinding="Code_NeedDataSource" ResourceName="CodeRoeVacationPayTypeCd" Value='<%# Eval("CodeRoeVacationPayTypeCd") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="VacationPay" Text="A-Vacation Pay:" runat="server" DecimalDigits="2" ResourceName="VacationPay" Value='<%# Eval("VacationPay") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="Comment1" Text="Comment 1:" runat="server" ResourceName="Comment1" Value='<%# Eval ("Comment1") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:DateControl ID="StatutoryHolidayPay1Date" Text="Stat Holiday Pay 1 Date:" runat="server" ResourceName="StatutoryHolidayPay1Date" Value='<%# Eval ("StatutoryHolidayPay1Date") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <wasp:NumericControl ID="StatutoryHolidayPay1Amount" Text="StatutoryHolidayPay1Amount" runat="server" DecimalDigits="2" ResourceName="StatutoryHolidayPay1Amount" Value='<%# Eval("StatutoryHolidayPay1Amount") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="Comment2" Text="Comment 2:" runat="server" ResourceName="Comment2" Value='<%# Eval ("Comment2") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:DateControl ID="StatutoryHolidayPay2Date" Text="Stat Holiday Pay 2 Date:" runat="server" ResourceName="StatutoryHolidayPay2Date" Value='<%# Eval ("StatutoryHolidayPay2Date") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <wasp:NumericControl ID="StatutoryHolidayPay2Amount" Text="StatutoryHolidayPay2Amount" runat="server" DecimalDigits="2" ResourceName="StatutoryHolidayPay2Amount" Value='<%# Eval("StatutoryHolidayPay2Amount") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="Comment3" Text="Comment 3:" runat="server" ResourceName="Comment3" Value='<%# Eval ("Comment3") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:DateControl ID="StatutoryHolidayPay3Date" Text="Stat Holiday Pay 3 Date:" runat="server" ResourceName="StatutoryHolidayPay3Date" Value='<%# Eval ("StatutoryHolidayPay3Date") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <wasp:NumericControl ID="StatutoryHolidayPay3Amount" Text="StatutoryHolidayPay3Amount" runat="server" DecimalDigits="2" ResourceName="StatutoryHolidayPay3Amount" Value='<%# Eval("StatutoryHolidayPay3Amount") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="FirstOtherMoniesCd" Text="Other Monies 1:" runat="server" Type="OtherMoniesTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="FirstOtherMoniesCd" Value='<%# Eval("FirstOtherMoniesCd") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="SecondOtherMoniesCd" Text="Other Monies 2:" runat="server" Type="OtherMoniesTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="SecondOtherMoniesCd" Value='<%# Eval("SecondOtherMoniesCd") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="OtherMoniesFirstAmount" Text="&nbsp;" runat="server" DecimalDigits="2" ResourceName="OtherMoniesFirstAmount" Value='<%# Eval("OtherMoniesFirstAmount") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="OtherMoniesSecondAmount" Text="&nbsp;" runat="server" DecimalDigits="2" ResourceName="OtherMoniesSecondAmount" Value='<%# Eval("OtherMoniesSecondAmount") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="ThirdOtherMoniesCd" Text="Other Monies 3:" runat="server" Type="OtherMoniesTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="ThirdOtherMoniesCd" Value='<%# Eval("ThirdOtherMoniesCd") %>' ReadOnly="true" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="OtherMoniesThirdAmount" Text="&nbsp;" runat="server" DecimalDigits="2" ResourceName="OtherMoniesThirdAmount" Value='<%# Eval("OtherMoniesThirdAmount") %>' ReadOnly="true" />
                    </td>
                    <td></td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>
    <EditItemTemplate>
        <wasp:WLPToolBar ID="EmployeeDetailRoeToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert" />
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update" />
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" CausesValidation="False" ResourceName="Cancel" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:DateControl ID="TerminationDate" Text="Termination Date:" runat="server" ResourceName="TerminationDate" Value='<%# Eval ("TerminationDate") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:DateControl ID="FirstDayWorked" Text="First Day Worked:" runat="server" ResourceName="FirstDayWorked" Value='<%# Eval ("FirstDayWorked") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="RoeReasonForTermination" Text="Roe Reason For Termination:" runat="server" Type="EmployeeTerminationReasonType" OnDataBinding="Code_NeedDataSource" ResourceName="ROEReasonForTermination" Value='<%# Eval("CodeRoeReasonCd") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:DateControl ID="LastDayPaid" Text="LastDayPaid:" runat="server" ResourceName="LastDayPaid" Value='<%# Eval ("LastDayPaid") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <wasp:DateControl ID="FinalPayPeriodEndingDate" Text="Final Pay Period Ending Date:" runat="server" ResourceName="FinalPayPeriodEndingDate" Value='<%# Eval ("PayrollPeriodCutoffDate") %>' ReadOnly="true" />
                    </td>
                </tr>
            </table>
            <hr />
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <b><asp:Literal ID="Literal1" runat="server" Text='<%$ Resources:Messages, RoeCompleteOnly %>' /></b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="ExpectedDateOfRecall" Text="Expected Date Of Recall:" runat="server" AutoPostback="true" ResourceName="ExpectedDateOfRecall" Value='<%# Bind ("ExpectedDateOfRecall") %>' ReadOnly="false" TabIndex="010" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:CustomValidator ID="RequiredFieldsValidator" runat="server" ErrorMessage="****Expected Date or Date Recall needed****" ForeColor="Red" OnServerValidate="RequiredFieldsValidator_ServerValidate"></asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="PaymentStartDate" Text="Payment Start Date:" runat="server" ResourceName="PaymentStartDate" ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="CodeEmployeeTerminationRoeDateRecallCd" Text="Date Recall:" runat="server" AutoPostback="true" OnSelectedIndexChanged="Recall_SelectedIndexChanged" Type="ROEDateRecallTypeCode" OnDataBinding="Recall_DataBind" ResourceName="CodeEmployeeTerminationRoeDateRecallCd" Value='<%# Bind("CodeEmployeeTerminationRoeDateRecallCd") %>' ReadOnly="false" IncludeEmptyItem="false" TabIndex="020" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="Amount" Text="Amount:" runat="server" DecimalDigits="2" ResourceName="Amount" Value='<%# Bind("Amount") %>' ReadOnly="false" TabIndex="030" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="CodeEmployeeTerminationRoePaidLeaveCd" Text="Paid Leave:" runat="server" Type="ROEPaidLeaveTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="CodeEmployeeTerminationRoePaidLeaveCd" Value='<%# Bind("CodeEmployeeTerminationRoePaidLeaveCd") %>' ReadOnly="false" TabIndex="040" />
                    </td>
                    <td>
                        <b><asp:Literal ID="Literal2" runat="server" Text='<%$ Resources:Messages, RoeContact %>' /></b>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <wasp:TextBoxControl ID="ContactLastName" Text="ContactLastName:" runat="server" ResourceName="ContactLastName" Value='<%# Bind ("ContactLastName") %>' ReadOnly="false" TabIndex="050" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <wasp:TextBoxControl ID="ContactFirstName" Text="ContactFirstName:" runat="server" ResourceName="ContactFirstName" Value='<%# Bind ("ContactFirstName") %>' ReadOnly="false" TabIndex="055" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <div style="float: left">
                            <wasp:TextBoxControl ID="Telephone" Text="Telephone:" runat="server" ResourceName="Telephone" Value='<%# Bind ("ContactTelephone") %>' ReadOnly="false" TabIndex="060" />
                        </div>
                        <div style="float: left; padding-left: 50px;">
                            <asp:RegularExpressionValidator ID="PhoneLength" runat="server" ForeColor="Red" ControlToValidate="Telephone" ErrorMessage="Not Valid" ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$">* Not Valid</asp:RegularExpressionValidator>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <div style="float: left">
                            <wasp:TextBoxControl ID="ContactTelephoneExtension" Text="ContactTelephoneExtension:" runat="server" ResourceName="ContactTelephoneExtension" Value='<%# Bind ("ContactTelephoneExtension") %>' ReadOnly="false" TabIndex="065" />
                        </div>
                        <div style="float: left; padding-left: 50px;">
                            <asp:RegularExpressionValidator ID="ExtensionLength" runat="server" ForeColor="Red" ControlToValidate="ContactTelephoneExtension" ErrorMessage="Not Valid" ValidationExpression="^[0-9]{0,9}$">* Not Valid</asp:RegularExpressionValidator>
                        </div>
                    </td>
                </tr>
            </table>
            <hr />
            <table width="100%">
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="CodeRoeVacationPayTypeCd" OnPreRender="CodeRoeVacationPayTypeCd_OnPreRender" OnSelectedIndexChanged="CodeRoeVacationPayTypeCd_onSelectedIndexChanged" AutoPostback="true" Text="CodeRoeVacationPayType" runat="server" Type="CodeRoeVacationPayType" OnDataBinding="Code_NeedDataSource" ResourceName="CodeRoeVacationPayTypeCd" Value='<%# Bind("CodeRoeVacationPayTypeCd") %>' ReadOnly="false" TabIndex="68" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="VacationPay" Text="A-Vacation Pay:" runat="server" DecimalDigits="2" ResourceName="VacationPay" Value='<%# Bind("VacationPay") %>' ReadOnly="false" TabIndex="070" />
                        <asp:RangeValidator CultureInvariantValues="true" ID="VacationPayRange" EnableClientScript="false" Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="VacationPay" ResourceName="VacationPayRange" Text='Must be in the range of 0.01 and 999999.99' MinimumValue="0.01" MaximumValue="999999.99" Type="Double" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="Comment1" Text="Comment 1:" runat="server" ResourceName="Comment1" Value='<%# Bind ("Comment1") %>' ReadOnly="false" TabIndex="080" />
                    </td>
                    <td>
                        <wasp:DateControl ID="StatutoryHolidayPay1Date" OnPreRender="StatutoryHolidayPay1Date_OnPreRender" OnSelectedDateChanged="StatutoryHolidayPay1Date_onSelectedDateChanged" AutoPostback="true" Text="Stat Holiday Pay 1 Date:" runat="server" ResourceName="StatutoryHolidayPay1Date" Value='<%# Bind ("StatutoryHolidayPay1Date") %>' ReadOnly="false" TabIndex="090" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <wasp:NumericControl ID="StatutoryHolidayPay1Amount" Text="StatutoryHolidayPay1Amount" runat="server" DecimalDigits="2" ResourceName="StatutoryHolidayPay1Amount" Value='<%# Bind("StatutoryHolidayPay1Amount") %>' ReadOnly="false" TabIndex="100" />
                        <asp:RangeValidator CultureInvariantValues="true" ID="StatutoryHolidayPay1AmountRange" EnableClientScript="false" Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="StatutoryHolidayPay1Amount" ResourceName="StatutoryHolidayPay1AmountRange" Text='Must be in the range of 0.01 and 999999.99' MinimumValue="0.01" MaximumValue="999999.99" Type="Double" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="Comment2" Text="Comment 2:" runat="server" ResourceName="Comment2" Value='<%# Bind ("Comment2") %>' ReadOnly="false" TabIndex="110" />
                    </td>
                    <td>
                        <wasp:DateControl ID="StatutoryHolidayPay2Date" OnPreRender="StatutoryHolidayPay2Date_OnPreRender" OnSelectedDateChanged="StatutoryHolidayPay2Date_onSelectedDateChanged" AutoPostback="true" Text="Stat Holiday Pay 2 Date:" runat="server" ResourceName="StatutoryHolidayPay2Date" Value='<%# Bind ("StatutoryHolidayPay2Date") %>' ReadOnly="false" TabIndex="120" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <wasp:NumericControl ID="StatutoryHolidayPay2Amount" Text="StatutoryHolidayPay2Amount" runat="server" DecimalDigits="2" ResourceName="StatutoryHolidayPay2Amount" Value='<%# Bind("StatutoryHolidayPay2Amount") %>' ReadOnly="false" TabIndex="130" />
                        <asp:RangeValidator CultureInvariantValues="true" ID="StatutoryHolidayPay2AmountRange" EnableClientScript="false" Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="StatutoryHolidayPay2Amount" ResourceName="StatutoryHolidayPay2AmountRange" Text='Must be in the range of 0.01 and 999999.99' MinimumValue="0.01" MaximumValue="999999.99" Type="Double" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="Comment3" Text="Comment 3:" runat="server" ResourceName="Comment3" Value='<%# Bind ("Comment3") %>' ReadOnly="false" TabIndex="140" />
                    </td>
                    <td>
                        <wasp:DateControl ID="StatutoryHolidayPay3Date" OnPreRender="StatutoryHolidayPay3Date_OnPreRender" OnSelectedDateChanged="StatutoryHolidayPay3Date_onSelectedDateChanged" AutoPostback="true" Text="Stat Holiday Pay 3 Date:" runat="server" ResourceName="StatutoryHolidayPay3Date" Value='<%# Bind ("StatutoryHolidayPay3Date") %>' ReadOnly="false" TabIndex="150" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <wasp:NumericControl ID="StatutoryHolidayPay3Amount" Text="StatutoryHolidayPay3Amount" runat="server" DecimalDigits="2" ResourceName="StatutoryHolidayPay3Amount" Value='<%# Bind("StatutoryHolidayPay3Amount") %>' ReadOnly="false" TabIndex="160" />
                        <asp:RangeValidator CultureInvariantValues="true" ID="StatutoryHolidayPay3AmountRange" EnableClientScript="false" Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="StatutoryHolidayPay3Amount" ResourceName="StatutoryHolidayPay3AmountRange" Text='Must be in the range of 0.01 and 999999.99' MinimumValue="0.01" MaximumValue="999999.99" Type="Double" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="FirstOtherMoniesCd" OnPreRender="FirstOtherMoniesCd_OnPreRender" OnSelectedIndexChanged="FirstOtherMoniesCd_onSelectedIndexChanged" AutoPostback="true" Text="Other Monies 1:" runat="server" Type="OtherMoniesTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="FirstOtherMoniesCd" Value='<%# Bind("FirstOtherMoniesCd") %>' ReadOnly="false" TabIndex="170" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="SecondOtherMoniesCd" OnPreRender="SecondOtherMoniesCd_OnPreRender" OnSelectedIndexChanged="SecondOtherMoniesCd_onSelectedIndexChanged" AutoPostback="true" Text="Other Monies 2:" runat="server" Type="OtherMoniesTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="SecondOtherMoniesCd" Value='<%# Bind("SecondOtherMoniesCd") %>' ReadOnly="false" TabIndex="180" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="OtherMoniesFirstAmount" Text="&nbsp;" runat="server" DecimalDigits="2" ResourceName="OtherMoniesFirstAmount" Value='<%# Bind("OtherMoniesFirstAmount") %>' ReadOnly="false" TabIndex="190" />
                        <asp:RangeValidator CultureInvariantValues="true" ID="OtherMoniesFirstAmountRange" EnableClientScript="false" Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="OtherMoniesFirstAmount" ResourceName="OtherMoniesFirstAmountRange" Text='Must be in the range of 0.01 and 999999.99' MinimumValue="0.01" MaximumValue="999999.99" Type="Double" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="OtherMoniesSecondAmount" Text="&nbsp;" runat="server" DecimalDigits="2" ResourceName="OtherMoniesSecondAmount" Value='<%# Bind("OtherMoniesSecondAmount") %>' ReadOnly="false" TabIndex="200" />
                        <asp:RangeValidator CultureInvariantValues="true" ID="OtherMoniesSecondAmountRange" EnableClientScript="false" Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="OtherMoniesSecondAmount" ResourceName="OtherMoniesSecondAmountRange" Text='Must be in the range of 0.01 and 999999.99' MinimumValue="0.01" MaximumValue="999999.99" Type="Double" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="ThirdOtherMoniesCd" OnPreRender="ThirdOtherMoniesCd_OnPreRender" OnSelectedIndexChanged="ThirdOtherMoniesCd_onSelectedIndexChanged" AutoPostback="true" Text="Other Monies 3:" runat="server" Type="OtherMoniesTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="ThirdOtherMoniesCd" Value='<%# Bind("ThirdOtherMoniesCd") %>' ReadOnly="false" TabIndex="210" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="OtherMoniesThirdAmount" Text="&nbsp;" runat="server" DecimalDigits="2" ResourceName="OtherMoniesThirdAmount" Value='<%# Bind("OtherMoniesThirdAmount") %>' ReadOnly="false" TabIndex="220" />
                        <asp:RangeValidator CultureInvariantValues="true" ID="OtherMoniesThirdAmountRange" EnableClientScript="false" Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="OtherMoniesThirdAmount" ResourceName="OtherMoniesThirdAmountRange" Text='Must be in the range of 0.01 and 999999.99' MinimumValue="0.01" MaximumValue="999999.99" Type="Double" />
                    </td>
                    <td></td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>

</wasp:WLPFormView>