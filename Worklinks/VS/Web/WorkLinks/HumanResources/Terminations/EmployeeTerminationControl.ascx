﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeTerminationControl.ascx.cs" Inherits="WorkLinks.HumanResources.Terminations.EmployeeTerminationControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPFormView
    ID="EmployeeTerminationView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key"
    OnModeChanging="EmployeeTerminationView_ModeChanging"
    OnNeedDataSource="EmployeeTerminationView_NeedDataSource"
    OnUpdating="EmployeeTerminationView_Updating"
    OnButtonClick="EmployeeTerminationViewToolBar_ButtonClick">

    <ItemTemplate>
        <wasp:WLPToolBar ID="EmployeeTerminationDetailToolBar" OnClientButtonClicking="confirmation" OnButtonClick="EmployeeTerminationToolBar_ButtonClick" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Visible='<%# UpdateFlag %>' Text="Edit" ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="edit" ResourceName="Edit" Enabled='<%# EditButtonEnabled %>' />
                <wasp:WLPToolBarButton Visible='<%# DeleteFlag %>' Text="Delete" CausesValidation="false" ImageUrl="~/App_Themes/Default/Delete.gif" Enabled='<%# DeleteButtonEnabled %>' CommandName="Delete" ResourceName="Delete" />
                <wasp:WLPToolBarButton Visible='<%# BenefitedLeaveButtonEnabled %>' Text="BenefitedLeave" CausesValidation="false" Enabled='<%# BenefitedLeaveButtonEnabled %>' CommandName="BenefitedLeave" ResourceName="BenefitedLeaveButton" />
                <wasp:WLPToolBarButton Visible='<%# TerminateButtonEnabled %>' Text="Terminate" CausesValidation="false" Enabled='<%# TerminateButtonEnabled %>' CommandName="Terminate" ResourceName="TerminateButton" />
                <wasp:WLPToolBarButton Visible='<%# LeaveButtonEnabled %>' Text="Leave" CausesValidation="false" Enabled='<%# LeaveButtonEnabled %>' CommandName="Leave" ResourceName="LeaveButton" />
                <wasp:WLPToolBarButton Visible='<%# LongTermDisabilityButtonEnabled %>' Text="LongTermDisability" CausesValidation="false" Enabled='<%# LongTermDisabilityButtonEnabled %>' CommandName="LongTermDisability" ResourceName="LongTermDisabilityButton" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <% if (ShowMessage == 1)
                { %>
            <wasp:WLPLabel runat="server" Text='<%# EditMsg %>' />
            <% } %>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:DateControl ID="EffectiveDate" Text="Effective Date:" runat="server" ResourceName="EffectiveDate" Value='<%# Eval("EffectiveDate") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="LastDayPaid" Text="LastDayPaid:" runat="server" ResourceName="LastDayPaid" Value='<%# Eval("LastDayPaid") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="EmployeePositionActionCode" Text="Event:" runat="server" Type="EmployeePositionActionCode" ResourceName="EmployeePositionActionCode" Value='<%# Eval("EmployeePositionActionCode") %>' ReadOnly="true" OnDataBinding="Code_NeedDataSource" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="EmployeePositionReasonCode" Text="Reason:" runat="server" Type="EmployeePositionReasonCode" OnDataBinding="Code_NeedDataSource" ResourceName="EmployeePositionReasonCode" Value='<%# Eval("EmployeePositionReasonCode") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="ROEReasonForTermination" Text="ROE Reason For Termination:" runat="server" Type="EmployeeTerminationReasonType" OnDataBinding="Code_NeedDataSource" ResourceName="ROEReasonForTermination" Value='<%# Eval("CodeTerminationReasonCode") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="TerminationNotes" Width="600px" LabelStyle="width:75px;" MaxLength="120" FieldStyle="width: 425px;" Text="Notes:" runat="server" TextMode="multiline" Rows="3" Columns="40" ResourceName="TerminationNotes" Value='<%# Eval("Notes") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="ExtraNotes" Width="600px" LabelStyle="width:75px;" MaxLength="120" FieldStyle="width: 425px;" Text="ExtraNotes:" runat="server" TextMode="multiline" Rows="3" Columns="40" ResourceName="ExtraNotes" Value='<%# Eval("ExtraNotes") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="FinalPaymentMethod" Text="Final Pay Method:" runat="server" Type="EmployeeTerminationNetPayMethodType" OnDataBinding="Code_NeedDataSource" ResourceName="FinalPaymentMethod" Value='<%# Eval("CodeNetPayMethodCode") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="DeleteBankInfo" runat="server" Type="EmployeeBankingDeleteAccountCode" OnDataBinding="Code_NeedDataSource" ResourceName="DeleteBankInfo" Value='<%# Eval("EmployeeBankingDeleteAccountCode") %>' ReadOnly="true" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>
    <EditItemTemplate>
        <wasp:WLPToolBar ID="EmployeeDetailToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert" />
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update" />
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" CausesValidation="False" ResourceName="Cancel" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:DateControl Mandatory="true" ID="EffectiveDate" Text="Effective Date:" runat="server" ResourceName="EffectiveDate" Value='<%# Bind("EffectiveDate") %>' ReadOnly='<%# AllowEditOnField %>' TabIndex="010" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="LastDayPaid" Text="LastDayPaid:" runat="server" ResourceName="LastDayPaid" Value='<%# Bind("LastDayPaid") %>' ReadOnly='<%# LastPaidDayReadOnly %>' TabIndex="020" />
                        <asp:CustomValidator ID="dateComparer" runat="server" ControlToValidate="LastDayPaid" ForeColor="Red" OnServerValidate="ValidateDates"></asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="EmployeePositionActionCode" Text="Event:" runat="server" Type="EmployeePositionActionCode" ResourceName="EmployeePositionActionCode" Value='<%# Bind("EmployeePositionActionCode") %>' OnDataBinding="CodePositionAction_NeedDataSource" ReadOnly='<%# DisAllowEditByOverride %>' OnSelectedIndexChanged="EmployeePositionAction_SelectedIndexChanged" AutoPostback="true" IncludeEmptyItem="false" TabIndex="030" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="EmployeePositionReasonCode" Text="Reason:" runat="server" Type="EmployeePositionReasonCode" OnDataBinding="CodeReason_NeedDataSource" ResourceName="EmployeePositionReasonCode" Value='<%# Bind("EmployeePositionReasonCode") %>' ReadOnly='<%# AllowEditOnField %>' TabIndex="040" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="ROEReasonForTermination" OnDataBound="ROEReasonForTermination_onDataBound" OnSelectedIndexChanged="checkForReasonK" AutoPostback="true" Text="ROE Reason For Termination:" runat="server" Type="EmployeeTerminationReasonType" OnDataBinding="Code_NeedDataSource" ResourceName="ROEReasonForTermination" Value='<%# Bind("CodeTerminationReasonCode") %>' ReadOnly='<%# !EditButtonEnabled %>' TabIndex="050" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="TerminationNotes" Width="600px" LabelStyle="width:75px;" FieldStyle="width: 425px;" Text="Notes:" runat="server" TextMode="multiline" Rows="3" Columns="40" ResourceName="TerminationNotes" Value='<%# Bind("Notes") %>' ReadOnly='<%# AllowEditOnField %>' TabIndex="060" />
                        <asp:RegularExpressionValidator ID="TerminationNotesValidator" runat="server" ForeColor="Red" ControlToValidate="TerminationNotes" Display="Dynamic" ErrorMessage="<%$ Resources:ErrorMessages, TerminationNotesLimit %>" ValidationExpression="[\s\S]{0,160}" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="ExtraNotes" Width="600px" LabelStyle="width:75px;" FieldStyle="width: 425px;" Text="ExtraNotes:" runat="server" TextMode="multiline" Rows="3" Columns="40" ResourceName="ExtraNotes" Value='<%# Bind("ExtraNotes") %>' ReadOnly='<%# AllowEditOnField %>' TabIndex="070" />
                    </td>
                </tr> 
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="FinalPaymentMethod" Text="Final Pay Method:" runat="server" Type="EmployeeTerminationNetPayMethodType" OnDataBinding="Code_NeedDataSource" ResourceName="FinalPaymentMethod" Value='<%# Bind("CodeNetPayMethodCode") %>' ReadOnly='<%# AllowEditOnField %>' TabIndex="080" />
                    </td>
                </tr>
                <tr>
                    <td>                        
                        <wasp:ComboBoxControl ID="DeleteBankInfo" runat="server" Type="EmployeeBankingDeleteAccountCode" OnDataBinding="Code_NeedDataSource" ResourceName="DeleteBankInfo" Value='<%# Bind("EmployeeBankingDeleteAccountCode") %>' TabIndex="090" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>

</wasp:WLPFormView>

<script type="text/javascript">
    function ToolBarClick(sender, args) {
        if (<%= IsInsertMode.ToString().ToLower() %>) {
            var button = args.get_item();
            var commandName = button.get_commandName();

            if (commandName == "cancel")
                window.close();
        }
    }

    function confirmation(sender, args) {
        var button = args.get_item();
        if (button.get_commandName() == "Delete") {
            var message = "<asp:Literal runat="server" Text="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />";
            args.set_cancel(!confirm(message));
        }
    }  
</script>