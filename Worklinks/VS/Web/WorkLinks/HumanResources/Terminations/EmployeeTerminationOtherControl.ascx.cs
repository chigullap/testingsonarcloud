﻿using System;
using System.Web.UI.WebControls;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Terminations
{
    public partial class EmployeeTerminationOtherControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private const String _employeePositionIdKey = "EmployeePositionIdKey";
        private long _employeePositionId = -1;
        private const String _lastDayPaidDateKey = "LastDayPaidDateKey";
        private DateTime? _lastDayPaidDate = null;
        #endregion

        #region properties
        protected EmployeeTerminationOther EmployeeTerminationOtherVar
        {
            get
            {
                if (Data.Count < 1)
                    ((EmployeeTerminationOtherCollection)Data).Add(new EmployeeTerminationOther() { EmployeePositionId = EmployeePositionId });

                return (EmployeeTerminationOther)Data[0];
            }
        }
        private EmployeeTerminationOtherCollection EmployeeTerminationOtherCollectionVar
        {
            get
            {
                EmployeeTerminationOtherCollection collection = new EmployeeTerminationOtherCollection();
                collection.Add(EmployeeTerminationOtherVar);
                return collection;
            }
        }
        public bool IsInsertMode { get { return EmployeeTerminationOtherView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsViewMode { get { return EmployeeTerminationOtherView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool IsEditMode { get { return EmployeeTerminationOtherView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool UpateFlag { get { return Common.Security.RoleForm.EmployeeTerminationOther.UpdateFlag; } }
        public long EmployeePositionId //this is set in the termination control
        {
            get
            {
                if (_employeePositionId == -1)
                {
                    Object obj = ViewState[_employeePositionIdKey];

                    if (obj != null)
                        _employeePositionId = Convert.ToInt64(obj);
                }

                return _employeePositionId;
            }
            set
            {
                _employeePositionId = value;
                ViewState[_employeePositionIdKey] = _employeePositionId;
            }
        }
        public DateTime? LastDayPaidDate //this is set in the termination control
        {
            get
            {
                if (_lastDayPaidDate == null)
                {
                    Object obj = ViewState[_lastDayPaidDateKey];

                    if (obj != null)
                        _lastDayPaidDate = Convert.ToDateTime(obj);
                }

                return _lastDayPaidDate;
            }
            set
            {
                _lastDayPaidDate = value;
                ViewState[_lastDayPaidDateKey] = _lastDayPaidDate;
            }
        }
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        private String LastName
        {
            get
            {
                if (Page.RouteData.Values["lastName"] == null)
                    return null;
                else
                    return Page.RouteData.Values["lastName"].ToString();
            }
        }
        private String FirstName
        {
            get
            {
                if (Page.RouteData.Values["firstName"] == null)
                    return null;
                else
                    return Page.RouteData.Values["firstName"].ToString();
            }
        }
        #endregion

        protected void LoadEmployeeTerminationOtherInfo(long employeePositionId)
        {
            Data = Common.ServiceWrapper.EmployeeTerminations.Select(employeePositionId);
            EmployeeTerminationOtherView.DataBind();

            Label benefits = (Label)EmployeeTerminationOtherView.FindControl("lblBenefits");
            if (benefits != null)
                benefits.Text = String.Format("{0}", GetGlobalResourceObject("PageContent", "BenefitLabel"));
        }
        protected void EmployeeTerminationOtherView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            EmployeeTerminationOtherView.DataSource = EmployeeTerminationOtherCollectionVar;
        }
        protected void OtherFlag_OnInit(object sender, EventArgs e)
        {
            //enable the "other" textbox according to "OtherFlag" being checked or not.
            TextBoxControl control = (TextBoxControl)EmployeeTerminationOtherView.FindControl("OtherFlagText");
            if (control != null)
            {
                if (EmployeeTerminationOtherCollectionVar[0].OtherFlag != null)
                    control.Enabled = (bool)EmployeeTerminationOtherCollectionVar[0].OtherFlag;
                else
                    control.Enabled = false;
            }
        }
        protected void EmployeeTerminationOtherView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                EmployeeTerminationOther info = (EmployeeTerminationOther)e.DataItem; //only one employee
                EmployeeTerminationOther updatedOther = Common.ServiceWrapper.EmployeeTerminations.Update(info);
                updatedOther.CopyTo(EmployeeTerminationOtherVar);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SetEmployeePositionId(long employeePositionId, DateTime? lastPaidDate) //called from EmployeeTerminationPageControl which houses termination control, roe termination, and other termination.
        {
            LastDayPaidDate = lastPaidDate;
            EmployeePositionId = employeePositionId;
            LoadEmployeeTerminationOtherInfo(EmployeePositionId);
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void IsOther_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBoxControl)sender).Checked)
            {
                TextBoxControl otherText = (TextBoxControl)EmployeeTerminationOtherView.FindControl("OtherFlagText");
                otherText.Enabled = true;
            }
            else
            {
                TextBoxControl otherText = (TextBoxControl)EmployeeTerminationOtherView.FindControl("OtherFlagText");
                otherText.Enabled = false;
            }
        }
    }
}