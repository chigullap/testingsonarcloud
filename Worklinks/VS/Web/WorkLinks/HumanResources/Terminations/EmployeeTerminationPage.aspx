﻿<%@ Page Language="C#" MasterPageFile="~/HumanResources/Employee.Master" AutoEventWireup="true" CodeBehind="EmployeeTerminationPage.aspx.cs" Inherits="WorkLinks.HumanResources.Terminations.EmployeeTermination" ResourceName="Title" %>
<%@ Register Src="EmployeeTerminationPageControl.ascx" TagName="EmployeeTerminationPageControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <uc1:EmployeeTerminationPageControl ID="EmployeeTerminationPageControl1" runat="server" />
</asp:Content>