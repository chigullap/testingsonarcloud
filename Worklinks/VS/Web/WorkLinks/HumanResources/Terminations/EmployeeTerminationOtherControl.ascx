﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeTerminationOtherControl.ascx.cs" Inherits="WorkLinks.HumanResources.Terminations.EmployeeTerminationOtherControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPFormView
    ID="EmployeeTerminationOtherView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key"
    OnNeedDataSource="EmployeeTerminationOtherView_NeedDataSource"
    OnUpdating="EmployeeTerminationOtherView_Updating">

    <ItemTemplate>
        <wasp:WLPToolBar ID="EmployeeTerminationOtherDetailToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Visible='<%# UpateFlag %>' Text="Edit" ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="edit" ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:DateControl ID="LastDayPaid" Text="LastDayPaid:" runat="server" ResourceName="LastDayPaid" Value='<%# LastDayPaidDate %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="VoluntaryTermination" runat="server" ResourceName="VoluntaryTermination" Value='<%# Eval("VoluntaryTermination") %>' LabelText="Voluntary Termination:" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="RecommendForRehire" runat="server" ResourceName="RecommendForRehire" Value='<%# Eval("RecommendForRehire") %>' LabelText="Recommend As Rehire:" ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="TerminationLetterReceived" runat="server" ResourceName="TerminationLetterReceived" Value='<%# Eval("TerminationLetterReceived") %>' LabelText="Termination Letter Received:" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="WeeksOfTerminationPay" Text="Weeks Of Termination Pay:" runat="server" DecimalDigits="0" ResourceName="WeeksOfTerminationPay" Value='<%# Bind("WeeksOfTerminationPay") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="WeeksOfSeverancePay" Text="Weeks Of Severance Pay:" runat="server" DecimalDigits="0" ResourceName="WeeksOfSeverancePay" Value='<%# Bind("WeeksOfSeverancePay") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="VacationPay" Text="Vacation Pay:" runat="server" DecimalDigits="2" ResourceName="VacationPay" Value='<%# Bind("VacationPay") %>' ReadOnly="true" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="VacationDays" Text="Vacation Days:" runat="server" DecimalDigits="0" ResourceName="VacationDays" Value='<%# Bind("VacationDays") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="VacationPercent" Text="Vacation Percent:" runat="server" DecimalDigits="2" ResourceName="VacationPercent" Value='<%# Bind("VacationPercent") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="AnticipatedCommission" Text="Anticipated Commission:" runat="server" DecimalDigits="2" ResourceName="AnticipatedCommission" Value='<%# Bind("AnticipatedCommission") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="AnticipatedBonus" Text="Anticipated Bonus:" runat="server" DecimalDigits="2" ResourceName="AnticipatedBonus" Value='<%# Bind("AnticipatedBonus") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td><asp:Label ID="lblBenefits" runat="server" Text="" ClientIDMode="Static" /></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="MedicalFlag" runat="server" ResourceName="MedicalFlag" Value='<%# Eval("MedicalFlag") %>' LabelText="Medical:" ReadOnly="true" />
                    </td>
                    <td rowspan="3">
                        <wasp:TextBoxControl ID="Comments" Text="Comments:" TextMode="multiline" Rows="3" runat="server" ResourceName="Comments" Value='<%# Eval("Comments") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="DentalFlag" runat="server" ResourceName="DentalFlag" Value='<%# Eval("DentalFlag") %>' LabelText="Dental:" ReadOnly="true" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="LifeFlag" runat="server" ResourceName="LifeFlag" Value='<%# Eval("LifeFlag") %>' LabelText="Life:" ReadOnly="true" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="OtherFlag" runat="server" ResourceName="OtherFlag" Value='<%# Eval("OtherFlag") %>' LabelText="Other:" ReadOnly="true" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="OtherFlagText" Text="Other Text:" runat="server" ResourceName="OtherFlagText" Value='<%# Eval("OtherFlagText") %>' ReadOnly="true" />
                    </td>
                    <td></td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>
    <EditItemTemplate>
        <wasp:WLPToolBar ID="EmployeeDetailOtherToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert" />
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update" />
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:DateControl ID="LastDayPaid" Text="LastDayPaid:" runat="server" ResourceName="LastDayPaid" Value='<%# LastDayPaidDate %>' ReadOnly="true" TabIndex="010" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="VoluntaryTermination" runat="server" ResourceName="VoluntaryTermination" Value='<%# Bind("VoluntaryTermination") %>' LabelText="Voluntary Termination:" ReadOnly="false" TabIndex="020" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="RecommendForRehire" runat="server" ResourceName="RecommendForRehire" Value='<%# Bind("RecommendForRehire") %>' LabelText="Recommend As Rehire:" ReadOnly="false" TabIndex="030" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="TerminationLetterReceived" runat="server" ResourceName="TerminationLetterReceived" Value='<%# Bind("TerminationLetterReceived") %>' LabelText="Termination Letter Received:" ReadOnly="false" TabIndex="040" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="WeeksOfTerminationPay" Text="Weeks Of Termination Pay:" runat="server" DecimalDigits="0" ResourceName="WeeksOfTerminationPay" Value='<%# Bind("WeeksOfTerminationPay") %>' ReadOnly="false" TabIndex="050" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="WeeksOfSeverancePay" Text="Weeks Of Severance Pay:" runat="server" DecimalDigits="0" ResourceName="WeeksOfSeverancePay" Value='<%# Bind("WeeksOfSeverancePay") %>' ReadOnly="false" TabIndex="060" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="VacationPay" Text="Vacation Pay:" runat="server" DecimalDigits="2" ResourceName="VacationPay" Value='<%# Bind("VacationPay") %>' ReadOnly="false" TabIndex="070" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="VacationDays" Text="Vacation Days:" runat="server" DecimalDigits="0" ResourceName="VacationDays" Value='<%# Bind("VacationDays") %>' ReadOnly="false" TabIndex="080" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="VacationPercent" Text="Vacation Percent:" runat="server" DecimalDigits="2" ResourceName="VacationPercent" Value='<%# Bind("VacationPercent") %>' ReadOnly="false" TabIndex="090" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="AnticipatedCommission" Text="Anticipated Commission:" runat="server" DecimalDigits="2" ResourceName="AnticipatedCommission" Value='<%# Bind("AnticipatedCommission") %>' ReadOnly="false" TabIndex="100" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="AnticipatedBonus" Text="Anticipated Bonus:" runat="server" DecimalDigits="2" ResourceName="AnticipatedBonus" Value='<%# Bind("AnticipatedBonus") %>' ReadOnly="false" TabIndex="110" />
                    </td>
                </tr>
                <tr>
                    <td><asp:Label ID="lblBenefits" runat="server" Text="" ClientIDMode="Static" /></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="MedicalFlag" runat="server" ResourceName="MedicalFlag" Value='<%# Bind("MedicalFlag") %>' LabelText="Medical:" ReadOnly="false" TabIndex="120" />
                    </td>
                    <td rowspan="3">
                        <wasp:TextBoxControl ID="Comments" Text="Comments:" TextMode="multiline" Rows="3" runat="server" ResourceName="Comments" Value='<%# Bind("Comments") %>' ReadOnly="false" TabIndex="130" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="DentalFlag" runat="server" ResourceName="DentalFlag" Value='<%# Bind("DentalFlag") %>' LabelText="Dental:" ReadOnly="false" TabIndex="140" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="LifeFlag" runat="server" ResourceName="LifeFlag" Value='<%# Bind("LifeFlag") %>' LabelText="Life:" ReadOnly="false" TabIndex="150" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="OtherFlag" OnCheckedChanged='IsOther_CheckedChanged' AutoPostBack="true" OnInit="OtherFlag_OnInit" ClientIDMode="Static" runat="server" ResourceName="OtherFlag" Value='<%# Bind("OtherFlag") %>' LabelText="Other:" ReadOnly="false" TabIndex="160" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="OtherFlagText" Text="Other Text:" ClientIDMode="Static" runat="server" ResourceName="OtherFlagText" Value='<%# Bind("OtherFlagText") %>' ReadOnly="false" TabIndex="170" />
                    </td>
                    <td></td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>

</wasp:WLPFormView>