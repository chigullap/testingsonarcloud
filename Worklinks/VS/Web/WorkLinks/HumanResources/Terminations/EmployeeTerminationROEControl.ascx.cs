﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Terminations
{
    public partial class EmployeeTerminationRoeControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private const String _employeePositionIdKey = "EmployeePositionIdKey";
        private long _employeePositionId = -1;
        #endregion

        #region properties
        protected EmployeeTerminationRoe EmployeeTerminationRoeVar
        {
            get
            {
                if (Data.Count < 1)
                    ((EmployeeTerminationRoeCollection)Data).Add(new EmployeeTerminationRoe() { EmployeePositionId = EmployeePositionId });

                return (EmployeeTerminationRoe)Data[0];
            }
        }
        private EmployeeTerminationRoeCollection EmployeeTerminationRoeCollectionVar
        {
            get
            {
                EmployeeTerminationRoeCollection collection = new EmployeeTerminationRoeCollection();
                collection.Add(EmployeeTerminationRoeVar);
                return collection;
            }
        }
        public bool IsInsertMode { get { return EmployeeTerminationRoeView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsViewMode { get { return EmployeeTerminationRoeView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool IsEditMode { get { return EmployeeTerminationRoeView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeTerminationROE.UpdateFlag; } }
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        private String LastName
        {
            get
            {
                if (Page.RouteData.Values["lastName"] == null)
                    return null;
                else
                    return Page.RouteData.Values["lastName"].ToString();
            }
        }
        private String FirstName
        {
            get
            {
                if (Page.RouteData.Values["firstName"] == null)
                    return null;
                else
                    return Page.RouteData.Values["firstName"].ToString();
            }
        }
        public long EmployeePositionId //this is captured in the termination control
        {
            get
            {
                if (_employeePositionId == -1)
                {
                    Object obj = ViewState[_employeePositionIdKey];

                    if (obj != null)
                        _employeePositionId = Convert.ToInt64(obj);
                }

                return _employeePositionId;
            }
            set
            {
                _employeePositionId = value;
                ViewState[_employeePositionIdKey] = _employeePositionId;
            }
        }
        #endregion

        #region main
        protected void LoadEmployeeTerminationRoeInfo(long employeePositionId)
        {
            Data = Common.ServiceWrapper.EmployeeTerminations.SelectRoe(employeePositionId);
            EmployeeTerminationRoeView.DataBind();
        }
        protected void EmployeeTerminationRoeView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            EmployeeTerminationRoeView.DataSource = EmployeeTerminationRoeCollectionVar;
        }
        public void SetEmployeePositionId(long employeePositionId) //called from EmployeeTerminationPageControl which houses termination control, Roe termination, and other termination.
        {
            EmployeePositionId = employeePositionId;
            LoadEmployeeTerminationRoeInfo(EmployeePositionId);
        }
        protected void EmployeeTerminationRoeView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    EmployeeTerminationRoe info = (EmployeeTerminationRoe)e.DataItem; //only one record
                    ShiftMoneyDataBeforeUpdate(info);
                    ShiftStatHolidayDataBeforeUpdate(info);

                    EmployeeTerminationRoe updatedROE = Common.ServiceWrapper.EmployeeTerminations.UpdateRoe(info);
                    updatedROE.CopyTo(EmployeeTerminationRoeVar);
                }
                catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
                {
                    if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                        e.Cancel = true;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
                e.Cancel = true;
        }
        private void ShiftMoneyDataBeforeUpdate(EmployeeTerminationRoe info)
        {
            //if first is empty and second is full, shift data and empty #2
            if (info.FirstOtherMoniesCd == null && info.SecondOtherMoniesCd != null)
            {
                info.FirstOtherMoniesCd = info.SecondOtherMoniesCd;
                info.OtherMoniesFirstAmount = info.OtherMoniesSecondAmount;
                info.SecondOtherMoniesCd = null;
                info.OtherMoniesSecondAmount = null;
            }

            //if first and second are empty and third is full, shift data and empty #2
            if (info.FirstOtherMoniesCd == null && info.SecondOtherMoniesCd == null && info.ThirdOtherMoniesCd != null)
            {
                info.FirstOtherMoniesCd = info.ThirdOtherMoniesCd;
                info.OtherMoniesFirstAmount = info.OtherMoniesThirdAmount;
                info.ThirdOtherMoniesCd = null;
                info.OtherMoniesThirdAmount = null;
            }

            //if first is full and second is empty and third is full, shift data and empty #3
            if (info.SecondOtherMoniesCd == null && info.ThirdOtherMoniesCd != null)
            {
                info.SecondOtherMoniesCd = info.ThirdOtherMoniesCd;
                info.OtherMoniesSecondAmount = info.OtherMoniesThirdAmount;
                info.ThirdOtherMoniesCd = null;
                info.OtherMoniesThirdAmount = null;
            }
        }
        private void ShiftStatHolidayDataBeforeUpdate(EmployeeTerminationRoe info)
        {
            //if first is empty and second is full, shift data and empty #2
            if (info.StatutoryHolidayPay1Date == null && info.StatutoryHolidayPay2Date != null)
            {
                info.StatutoryHolidayPay1Date = info.StatutoryHolidayPay2Date;
                info.StatutoryHolidayPay1Amount = info.StatutoryHolidayPay2Amount;
                info.StatutoryHolidayPay2Date = null;
                info.StatutoryHolidayPay2Amount = null;
            }

            //if first and second are empty and third is full, shift data and empty #2
            if (info.StatutoryHolidayPay1Date == null && info.StatutoryHolidayPay2Date == null && info.StatutoryHolidayPay3Date != null)
            {
                info.StatutoryHolidayPay1Date = info.StatutoryHolidayPay3Date;
                info.StatutoryHolidayPay1Amount = info.StatutoryHolidayPay3Amount;
                info.StatutoryHolidayPay3Date = null;
                info.StatutoryHolidayPay3Amount = null;
            }

            //if first is full and second is empty and third is full, shift data and empty #3
            if (info.StatutoryHolidayPay2Date == null && info.StatutoryHolidayPay3Date != null)
            {
                info.StatutoryHolidayPay2Date = info.StatutoryHolidayPay3Date;
                info.StatutoryHolidayPay2Amount = info.StatutoryHolidayPay3Amount;
                info.StatutoryHolidayPay3Date = null;
                info.StatutoryHolidayPay3Amount = null;
            }
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void RequiredFieldsValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            WLPFormView formItem = EmployeeTerminationRoeView as WLPFormView;
            DateControl expectedDateOfRecall = (DateControl)formItem.FindControl("ExpectedDateOfRecall");
            ComboBoxControl roeDateRecallCode = (ComboBoxControl)formItem.FindControl("CodeEmployeeTerminationRoeDateRecallCd");

            if (expectedDateOfRecall.Value == null && roeDateRecallCode.Value.ToString() == "Y")
                args.IsValid = false;
        }
        protected void Recall_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //if the user has selected an empty entry, then enable the DateControl for ExpectedDateOfRecall.  Otherwise, if the user selected a value, disable the DateControl
            WLPFormView formItem = EmployeeTerminationRoeView as WLPFormView;
            DateControl expectedDateOfRecall = (DateControl)formItem.FindControl("ExpectedDateOfRecall");

            if (e.Value == "Y")
            {
                expectedDateOfRecall.Mandatory = true;
                expectedDateOfRecall.ReadOnly = false;
            }
            else
            {
                expectedDateOfRecall.Mandatory = false;
                expectedDateOfRecall.Value = null;
                expectedDateOfRecall.ReadOnly = true;
            }
        }
        protected void Recall_DataBind(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);

            WLPFormView formItem = EmployeeTerminationRoeView as WLPFormView;
            DateControl expectedDateOfRecall = (DateControl)formItem.FindControl("ExpectedDateOfRecall");

            if (((ComboBoxControl)sender).Value != null && ((ComboBoxControl)sender).Value.ToString() == "Y")
            {
                expectedDateOfRecall.ReadOnly = false;
                expectedDateOfRecall.Mandatory = true;
            }
            else
            {
                expectedDateOfRecall.Mandatory = false;
                expectedDateOfRecall.Value = null;
                expectedDateOfRecall.ReadOnly = true;
            }
        }
        protected void CodeRoeVacationPayTypeCd_OnPreRender(object sender, EventArgs e)
        {
            ComboBoxControl control = (ComboBoxControl)sender;
            CodeRoeVacationPayTypeCd_onSelectedIndexChanged(control, null);
        }
        protected void CodeRoeVacationPayTypeCd_onSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ComboBoxControl control = (ComboBoxControl)sender;
            NumericControl vacationPayTextBox = (NumericControl)control.BindingContainer.FindControl("VacationPay");

            //if control has a value and it is not "1", it should be mandatory and editable
            if (control.SelectedValue != null)
            {
                vacationPayTextBox.ReadOnly = (control.SelectedValue == "1");
                vacationPayTextBox.Mandatory = (control.SelectedValue != "1");
                vacationPayTextBox.Value = (control.SelectedValue == "1") ? null : vacationPayTextBox.Value;
            }
            else
            {
                vacationPayTextBox.Value = null;
                vacationPayTextBox.ReadOnly = true;
                vacationPayTextBox.Mandatory = false;
            }
        }
        protected void FirstOtherMoniesCd_OnPreRender(object sender, EventArgs e)
        {
            ComboBoxControl control = (ComboBoxControl)sender;
            FirstOtherMoniesCd_onSelectedIndexChanged(control, null);
        }
        protected void FirstOtherMoniesCd_onSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ComboBoxControl control = (ComboBoxControl)sender;
            NumericControl otherMoniesTextBox = (NumericControl)control.BindingContainer.FindControl("OtherMoniesFirstAmount");

            if (control.SelectedValue != null)
            {
                otherMoniesTextBox.ReadOnly = false;
                otherMoniesTextBox.Mandatory = true;
            }
            else
            {
                otherMoniesTextBox.Value = null;
                otherMoniesTextBox.ReadOnly = true;
                otherMoniesTextBox.Mandatory = false;
            }
        }
        protected void SecondOtherMoniesCd_OnPreRender(object sender, EventArgs e)
        {
            ComboBoxControl control = (ComboBoxControl)sender;
            SecondOtherMoniesCd_onSelectedIndexChanged(control, null);
        }
        protected void SecondOtherMoniesCd_onSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ComboBoxControl control = (ComboBoxControl)sender;
            NumericControl otherMonies2TextBox = (NumericControl)control.BindingContainer.FindControl("OtherMoniesSecondAmount");

            if (control.SelectedValue != null)
            {
                otherMonies2TextBox.ReadOnly = false;
                otherMonies2TextBox.Mandatory = true;
            }
            else
            {
                otherMonies2TextBox.Value = null;
                otherMonies2TextBox.ReadOnly = true;
                otherMonies2TextBox.Mandatory = false;
            }
        }
        protected void ThirdOtherMoniesCd_OnPreRender(object sender, EventArgs e)
        {
            ComboBoxControl control = (ComboBoxControl)sender;
            ThirdOtherMoniesCd_onSelectedIndexChanged(control, null);
        }
        protected void ThirdOtherMoniesCd_onSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ComboBoxControl control = (ComboBoxControl)sender;
            NumericControl otherMonies3TextBox = (NumericControl)control.BindingContainer.FindControl("OtherMoniesThirdAmount");

            if (control.SelectedValue != null)
            {
                otherMonies3TextBox.ReadOnly = false;
                otherMonies3TextBox.Mandatory = true;
            }
            else
            {
                otherMonies3TextBox.Value = null;
                otherMonies3TextBox.ReadOnly = true;
                otherMonies3TextBox.Mandatory = false;
            }
        }
        protected void StatutoryHolidayPay1Date_OnPreRender(object sender, EventArgs e)
        {
            DateControl control = (DateControl)sender;
            StatutoryHolidayPay1Date_onSelectedDateChanged(sender, null);
        }
        protected void StatutoryHolidayPay1Date_onSelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            DateControl control = (DateControl)sender;
            NumericControl statutoryHolidayPay1TextBox = (NumericControl)control.BindingContainer.FindControl("StatutoryHolidayPay1Amount");

            if (control.Value != null)
            {
                statutoryHolidayPay1TextBox.ReadOnly = false;
                statutoryHolidayPay1TextBox.Mandatory = true;
            }
            else
            {
                statutoryHolidayPay1TextBox.Value = null;
                statutoryHolidayPay1TextBox.ReadOnly = true;
                statutoryHolidayPay1TextBox.Mandatory = false;
            }
        }
        protected void StatutoryHolidayPay2Date_OnPreRender(object sender, EventArgs e)
        {
            DateControl control = (DateControl)sender;
            StatutoryHolidayPay2Date_onSelectedDateChanged(sender, null);
        }
        protected void StatutoryHolidayPay2Date_onSelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            DateControl control = (DateControl)sender;
            NumericControl statutoryHolidayPay2TextBox = (NumericControl)control.BindingContainer.FindControl("StatutoryHolidayPay2Amount");

            if (control.Value != null)
            {
                statutoryHolidayPay2TextBox.ReadOnly = false;
                statutoryHolidayPay2TextBox.Mandatory = true;
            }
            else
            {
                statutoryHolidayPay2TextBox.Value = null;
                statutoryHolidayPay2TextBox.ReadOnly = true;
                statutoryHolidayPay2TextBox.Mandatory = false;
            }
        }
        protected void StatutoryHolidayPay3Date_OnPreRender(object sender, EventArgs e)
        {
            DateControl control = (DateControl)sender;
            StatutoryHolidayPay3Date_onSelectedDateChanged(sender, null);
        }
        protected void StatutoryHolidayPay3Date_onSelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            DateControl control = (DateControl)sender;
            NumericControl statutoryHolidayPay3TextBox = (NumericControl)control.BindingContainer.FindControl("StatutoryHolidayPay3Amount");

            if (control.Value != null)
            {
                statutoryHolidayPay3TextBox.ReadOnly = false;
                statutoryHolidayPay3TextBox.Mandatory = true;
            }
            else
            {
                statutoryHolidayPay3TextBox.Value = null;
                statutoryHolidayPay3TextBox.ReadOnly = true;
                statutoryHolidayPay3TextBox.Mandatory = false;
            }
        }
        #endregion
    }
}