﻿using System;
using System.Web.UI.WebControls;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Terminations
{
    public partial class EmployeeTerminationControl : WLP.Web.UI.WLPUserControl
    {
        public delegate void EmployeeSelectedEventHandler(object sender, Employee.Biographical.EmployeeSelectedEventArgs e);
        public event EmployeeSelectedEventHandler EmployeeSelected;

        #region properties
        private String LastName
        {
            get
            {
                if (Page.RouteData.Values["lastName"] == null)
                    return null;
                else
                    return Page.RouteData.Values["lastName"].ToString();
            }
        }
        private String FirstName
        {
            get
            {
                if (Page.RouteData.Values["firstName"] == null)
                    return null;
                else
                    return Page.RouteData.Values["firstName"].ToString();
            }
        }
        private BusinessLayer.BusinessObjects.EmployeePosition CurrentPosition
        {
            get
            {
                if (Data != null && Data.Count > 0)
                    return (BusinessLayer.BusinessObjects.EmployeePosition)Data[0];
                else
                    return null;
            }
            set
            {
                Data = new EmployeePositionCollection();

                if (value != null)
                    ((EmployeePositionCollection)Data).Add(value);
            }
        }
        private bool IsActive //is the prev record ACtive? copied from prev record into base termination record, will be over written upon insert
        {
            get { return CurrentPosition.EmployeePositionStatusCode == "AC" || CurrentPosition.EmployeePositionStatusCode == "SL100" || CurrentPosition.EmployeePositionStatusCode == "SL70"; }
        }
        protected bool ProcessDateOpen
        {
            get
            {
                if (CurrentPosition.PayrollProcessId == null)
                    return true;
                else
                    return Common.ServiceWrapper.EmployeePosition.IsProcessDateOpen(CurrentPosition.PayrollProcessId);
            }
        }
        protected bool DeleteButtonEnabled //posId check so you cant have a "delete" on a new record
        {
            get { return CurrentPosition.PayrollProcessId == null && CurrentPosition.EmployeePositionId > 0; }
        }
        protected bool EditButtonEnabled
        {
            get
            {
                if (CurrentPosition.PayrollProcessId == null)
                    return true;
                else if (CurrentPosition.PayrollProcessId != null && !ProcessDateOpen && CurrentPosition.CodeRoeCreationStatusCd == "PEN")
                    return true;
                else
                    return false;
            }
        }
        protected bool BenefitedLeaveButtonEnabled
        {
            get
            {
                if (CurrentPosition.PayrollProcessId != null && CurrentPosition.CodeRoeCreationStatusCd != "PEN" && (CurrentPosition.EmployeePositionActionCode == "LEAV" || CurrentPosition.EmployeePositionActionCode == "LD0"))
                    return true;
                else
                    return false;
            }
        }
        protected bool TerminateButtonEnabled
        {
            get
            {
                if (CurrentPosition.PayrollProcessId != null && CurrentPosition.CodeRoeCreationStatusCd != "PEN" && (CurrentPosition.EmployeePositionActionCode == "LEAV" || CurrentPosition.EmployeePositionActionCode == "BENLEAV" || CurrentPosition.EmployeePositionActionCode == "LD0"))
                    return true;
                else
                    return false;
            }
        }
        protected bool LeaveButtonEnabled
        {
            get
            {
                if (CurrentPosition.PayrollProcessId != null && CurrentPosition.CodeRoeCreationStatusCd != "PEN" && (CurrentPosition.EmployeePositionActionCode == "BENLEAV" || CurrentPosition.EmployeePositionActionCode == "LD0"))
                    return true;
                else
                    return false;
            }
        }
        protected bool LongTermDisabilityButtonEnabled
        {
            get
            {
                if (CurrentPosition.PayrollProcessId != null && CurrentPosition.CodeRoeCreationStatusCd != "PEN" && (CurrentPosition.EmployeePositionActionCode == "LEAV" || CurrentPosition.EmployeePositionActionCode == "BENLEAV"))
                    return true;
                else
                    return false;
            }
        }
        protected bool DisAllowEditByOverride //property in business class that is set when using buttons to change status
        {
            get
            {
                if (CurrentPosition.StatusChange)
                    return true;
                else
                    return AllowEditOnField;
            }
        }
        protected int ShowMessage //determine if we show a message
        {
            get
            {
                if (CurrentPosition.PayrollProcessId != null && ProcessDateOpen)
                    return 1;
                else
                    return 0;
            }
        }
        public bool IsInsertMode { get { return EmployeeTerminationView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsViewMode { get { return EmployeeTerminationView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool IsEditMode { get { return EmployeeTerminationView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeTerminationDetail.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.EmployeeTerminationDetail.DeleteFlag; } }
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        public bool EmployeePositionExists { get { return CurrentPosition.EmployeePositionId > 0; } }
        protected bool AllowEditOnField { get { return !(IsActive || (EditButtonEnabled && ProcessDateOpen)); } }
        protected bool LastPaidDayReadOnly { get { return !EditButtonEnabled; } }
        protected String EditMsg { get { return "<span style=color:red;>" + GetGlobalResourceObject("WarningMessages", "MustRecalcToEdit") + "</span>"; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            //does the user have access rights?
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EmployeeTermination);

            WireEvents();

            if (!IsPostBack)
                LoadEmployeeTermination();
        }
        protected void WireEvents()
        {
            EmployeeTerminationView.DataBound += new EventHandler(EmployeeTerminationView_DataBound);
            EmployeeTerminationView.ItemDeleting += new FormViewDeleteEventHandler(EmployeeTerminationView_ItemDeleting);
        }
        protected void LoadEmployeeTermination()
        {
            BusinessLayer.BusinessObjects.EmployeePosition lastPosition = GetLastEmployeeRecord();
            EmployeePositionCollection collection = new EmployeePositionCollection();
            BusinessLayer.BusinessObjects.EmployeePosition termPosition = null;

            if (lastPosition.IsTermRelated)
                termPosition = lastPosition;
            else
                termPosition = lastPosition.CreateNew();

            collection.Add(termPosition);
            Data = collection;
            EmployeeTerminationView.DataBind();
        }
        private BusinessLayer.BusinessObjects.EmployeePosition GetLastEmployeeRecord()
        {
            //get latest record
            BusinessLayer.BusinessObjects.EmployeePosition record = Common.ServiceWrapper.EmployeePosition.SelectLatestRecord(new EmployeePositionCriteria { EmployeeId = EmployeeId, GetLatestRecordFlag = true })[0];

            //if put on a term related status (BL) then given a raise, we need to skip that record and get the previous one with a term related status
            if (record.EmployeePositionStatusCode != "AC" && record.EmployeePositionStatusCode != "SL100" && record.EmployeePositionStatusCode != "SL70" && record.EmployeePositionActionCode != "BENLEAV" && record.EmployeePositionActionCode != "LEAV" && record.EmployeePositionActionCode != "LD0" && record.EmployeePositionActionCode != "TERM")
                record = GetRecord(record.EmployeePositionId);

            return record;
        }
        //this is a recursive function to return the terminated related record if the employee had one and then was given a raise during that time.
        private BusinessLayer.BusinessObjects.EmployeePosition GetRecord(long nextEmployeePositionId)
        {
            BusinessLayer.BusinessObjects.EmployeePosition record = Common.ServiceWrapper.EmployeePosition.SelectLatestRecord(new EmployeePositionCriteria { EmployeeId = EmployeeId, NextEmployeePositionId = nextEmployeePositionId })[0];

            //if put on a term related status (BL) then given a raise, we need to skip that record and get the previous one with a term related status
            if (record.EmployeePositionStatusCode != "AC" && record.EmployeePositionStatusCode != "SL100" && record.EmployeePositionStatusCode != "SL70" && record.EmployeePositionActionCode != "BENLEAV" && record.EmployeePositionActionCode != "LEAV" && record.EmployeePositionActionCode != "LD0" && record.EmployeePositionActionCode != "TERM")
                return GetRecord(record.EmployeePositionId);

            return record;
        }
        #endregion

        #region event handlers
        void EmployeeTerminationView_ItemDeleting(object sender, FormViewDeleteEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.EmployeePosition.DeleteTermination(CurrentPosition);
                Response.Redirect(String.Format("~/HumanResources/Terminations/{0}/{1}/{2}/{3}", CurrentPosition.EmployeeId, LastName, FirstName, this.ID));
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void EmployeeTerminationView_DataBound(object sender, EventArgs e)
        {
            Employee.Biographical.EmployeeSelectedEventArgs args = new Employee.Biographical.EmployeeSelectedEventArgs();

            if (EmployeeTerminationView.DataItem is BusinessLayer.BusinessObjects.EmployeePosition)
            {
                BusinessLayer.BusinessObjects.EmployeePosition employeePositionRecord = ((BusinessLayer.BusinessObjects.EmployeePosition)EmployeeTerminationView.DataItem);
                args.EmployeePositionId = employeePositionRecord.EmployeePositionId;
                args.LastDayPaidDate = employeePositionRecord.LastDayPaid;

                OnEmployeeSelected(args);

                ComboBoxControl roeReason = (ComboBoxControl)EmployeeTerminationView.FindControl("ROEReasonForTermination");
                if (roeReason != null)
                {
                    BusinessLayer.BusinessObjects.Employee employee = Common.ServiceWrapper.HumanResourcesClient.GetEmployee(new EmployeeCriteria() { EmployeeId = EmployeeId })[0];
                    roeReason.Visible = employee.GovernmentIdentificationNumberTypeCode == "CAN";
                    roeReason.Mandatory = employee.GovernmentIdentificationNumberTypeCode == "CAN";
                }
            }
        }
        protected void EmployeeTerminationView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            EmployeeTerminationView.DataSource = Data;
        }
        protected void EmployeeTerminationView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    BusinessLayer.BusinessObjects.EmployeePosition info = (BusinessLayer.BusinessObjects.EmployeePosition)e.DataItem; //only one employee

                    if (info.LastDayPaid == null)
                        info.LastDayPaid = ((DateTime)info.EffectiveDate).AddDays(-1);

                    SetStatus(info);

                    if (IsThisNewTermination(info))
                    {
                        //info.CodeRoeCreationStatusCd = "PEN"; logic moved to business layer - MCKAYJ.
                        info.NextEmployeePositionId = null;

                        //get latest record
                        BusinessLayer.BusinessObjects.EmployeePosition latestPosition = Common.ServiceWrapper.EmployeePosition.SelectLatestRecord(new EmployeePositionCriteria { EmployeeId = EmployeeId, GetLatestRecordFlag = true })[0];

                        //insert
                        BusinessLayer.BusinessObjects.EmployeePosition insertedPosition = Common.ServiceWrapper.EmployeePosition.Insert(info, latestPosition, null, null); //last two args are used for benefited leave in employee position
                        Response.Redirect(String.Format("~/HumanResources/Terminations/{0}/{1}/{2}/{3}", insertedPosition.EmployeeId, LastName, FirstName, this.ID));
                    }
                    else
                    {
                        //update
                        BusinessLayer.BusinessObjects.EmployeePosition updatedPosition = Common.ServiceWrapper.EmployeePosition.Update(info);
                        updatedPosition.CopyTo(CurrentPosition);
                    }
                }
                catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
                {
                    if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                        e.Cancel = true;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
                e.Cancel = true;
        }
        private void SetStatus(BusinessLayer.BusinessObjects.EmployeePosition info)
        {
            //get code table info
            CodeCollection codeColl = Common.CodeHelper.GetCodeTable(LanguageCode, CodeTableType.EmployeePositionActionCode, null);

            //map status based on action chosen
            foreach (CodeObject obj in codeColl)
            {
                if (info.EmployeePositionActionCode == obj.Code)
                {
                    info.EmployeePositionStatusCode = obj.ParentCode;
                    break;
                }
            }
        }
        protected bool IsThisNewTermination(BusinessLayer.BusinessObjects.EmployeePosition tempEmpPos)
        {
            return tempEmpPos.EmployeePositionId < 1;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void CodeReason_NeedDataSource(object sender, EventArgs e)
        {
            ComboBoxControl control = (ComboBoxControl)sender;

            //update the reason combo to contain reasons associated to the Action/Event
            ComboBoxControl actionCombo = (ComboBoxControl)control.BindingContainer.FindControl("EmployeePositionActionCode");
            Common.CodeHelper.PopulateComboWithReason((ICodeControl)sender, LanguageCode, actionCombo.Value.ToString());
        }
        protected void CodePositionAction_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulatePositionActionControl((ICodeControl)sender, LanguageCode);
        }
        protected void EmployeePositionAction_SelectedIndexChanged(object o, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ComboBoxControl control = (ComboBoxControl)o;

            //update the reason combo to contain reasons associated to the Action/Event
            ComboBoxControl reasonCombo = (ComboBoxControl)control.BindingContainer.FindControl("EmployeePositionReasonCode");
            Common.CodeHelper.PopulateComboWithReason(reasonCombo, LanguageCode, e.Value);

            reasonCombo.DataBind();
        }
        protected void EmployeeTerminationView_ModeChanging(object sender, FormViewModeEventArgs e)
        {
            //set min termination id for editing
            if (e.NewMode.Equals(FormViewMode.Edit))
            {
                DateTime minDate = DateTime.Now;

                if (CurrentPosition.EmployeePositionId < 0)
                    minDate = (DateTime)GetLastEmployeeRecord().EffectiveDate;
                else
                    minDate = (DateTime)Common.ServiceWrapper.EmployeePosition.SelectLatestRecord(new EmployeePositionCriteria { NextEmployeePositionId = CurrentPosition.EmployeePositionId })[0].EffectiveDate;

                ((DateControl)((WebControl)sender).FindControl("EffectiveDate")).MinDate = minDate;
            }
        }
        protected void ROEReasonForTermination_onDataBound(object sender, EventArgs e)
        {
            ComboBoxControl control = (ComboBoxControl)sender;
            checkForReasonK(control, null);
        }
        protected void checkForReasonK(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ComboBoxControl control = (ComboBoxControl)sender;
            TextBoxControl notesTextBox = (TextBoxControl)control.BindingContainer.FindControl("TerminationNotes");

            //new roe 2.0 rules via cra
            if (control.SelectedValue != null)
            {
                if (control.SelectedValue.ToString() == "K00" || control.SelectedValue.ToString() == "K15")
                    notesTextBox.Mandatory = true;
                else
                    notesTextBox.Mandatory = false;
            }
            else
                notesTextBox.Mandatory = false;
        }
        protected void EmployeeTerminationToolBar_ButtonClick(object sender, Telerik.Web.UI.RadToolBarEventArgs e)
        {
            WLPToolBarButton button = ((WLPToolBarButton)e.Item);

            if (button.CommandName.ToLower().Equals("benefitedleave") || button.CommandName.ToLower().Equals("terminate") || button.CommandName.ToLower().Equals("leave") || button.CommandName.ToLower().Equals("longtermdisability"))
            {
                EmployeePositionCollection collection = new EmployeePositionCollection();
                BusinessLayer.BusinessObjects.EmployeePosition lastPosition = GetLastEmployeeRecord();
                BusinessLayer.BusinessObjects.EmployeePosition termPosition = lastPosition.CreateNew();

                //fill in data
                if (button.CommandName.ToLower().Equals("benefitedleave"))
                {
                    termPosition.EmployeePositionActionCode = "BENLEAV";
                    termPosition.EmployeePositionStatusCode = "BL";
                }
                else if (button.CommandName.ToLower().Equals("terminate"))
                {
                    termPosition.EmployeePositionActionCode = "TERM";
                    termPosition.EmployeePositionStatusCode = "TER";
                }
                else if (button.CommandName.ToLower().Equals("leave"))
                {
                    termPosition.EmployeePositionActionCode = "LEAV";
                    termPosition.EmployeePositionStatusCode = "L";
                }
                else if (button.CommandName.ToLower().Equals("longtermdisability"))
                {
                    termPosition.EmployeePositionActionCode = "LD0";
                    termPosition.EmployeePositionStatusCode = "LD0";
                }

                //set propert in biz object for toggle use in control
                termPosition.StatusChange = true;

                //add to collection and rebind grid
                collection.Add(termPosition);
                Data = collection;
                EmployeeTerminationView.DataBind();
            }
        }

        //check that the effective date is greater or equal to the last day paid
        public void ValidateDates(object sender, ServerValidateEventArgs args)
        {
            CustomValidator validator = (CustomValidator)sender;
            WLPFormView formItem = (WLPFormView)validator.BindingContainer;

            DateControl effectiveDate = (DateControl)formItem.FindControl("EffectiveDate");
            args.IsValid = (Convert.ToDateTime(effectiveDate.Value) >= Convert.ToDateTime(args.Value));

            if (!args.IsValid)
                ((CustomValidator)sender).ErrorMessage = "* The last day paid must be before or on the effective date";
        }

        #endregion

        #region event triggers
        protected virtual void OnEmployeeSelected(Employee.Biographical.EmployeeSelectedEventArgs args)
        {
            if (EmployeeSelected != null)
                EmployeeSelected(this, args);
        }
        #endregion
    }
}