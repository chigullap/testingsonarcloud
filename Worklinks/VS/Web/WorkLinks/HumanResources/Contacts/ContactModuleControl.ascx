﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactModuleControl.ascx.cs" Inherits="WorkLinks.HumanResources.Contacts.ContactModuleControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="ContactTypeListControl.ascx" TagName="ContactTypeListControl" TagPrefix="uc3" %>
<%@ Register Src="ContactControl.ascx" TagName="ContactControl" TagPrefix="uc1" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy2" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="ContactPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="ContactPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="ContactPanel" runat="server">
    <table width="100%">
        <tr>
            <td width="10%" valign="top">
                <wasp:WLPTabStrip ID="ContactTypeTabStrip" runat="server" Width="100%" Orientation="VerticalLeft" SelectedIndex="0" MultiPageID="MainMultiPage">
                    <Tabs>
                        <wasp:WLPTab Text="Contacts" PageViewID="ContactsPageView" Selected="True" ResourceName="Contacts" />
                        <wasp:WLPTab Text="Emergency Contacts" PageViewID="EmergencyPageView" ResourceName="EmergencyContacts" />
                        <wasp:WLPTab Text="Dependants" PageViewID="DependantsPageView" ResourceName="Dependants" />
                        <wasp:WLPTab Text="Beneficiaries" PageViewID="BeneficiariesPageView" ResourceName="Beneficiaries" />
                    </Tabs>
                </wasp:WLPTabStrip>
            </td>
            <td width="90%" valign="top">
                <telerik:RadMultiPage ID="MainMultiPage" runat="server" SelectedIndex="3">
                    <telerik:RadPageView ID="ContactsPageView" runat="server" Selected="true">
                        <uc1:ContactControl ID="ContactControl" runat="server" />
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="EmergencyPageView" runat="server">
                        <uc3:ContactTypeListControl ID="EmergencyContactTypeListControl" ContactTypeCode="EMER" runat="server" />
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="DependantsPageView" runat="server">
                        <uc3:ContactTypeListControl ID="DependantContactTypeListControl" ContactTypeCode="DEP" runat="server" />
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="BeneficiariesPageView" runat="server">
                        <uc3:ContactTypeListControl ID="BeneficiaryContactTypeListControl" ContactTypeCode="BENI" runat="server" />
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </td>
        </tr>
    </table>
</asp:Panel>