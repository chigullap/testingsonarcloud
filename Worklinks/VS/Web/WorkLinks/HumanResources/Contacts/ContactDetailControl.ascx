﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactDetailControl.ascx.cs" Inherits="WorkLinks.HumanResources.Contacts.ContactDetailControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPFormView 
    ID="ContactView" 
    runat="server" 
    RenderOuterTable="False" 
    DataKeyNames="Key" 
    OnNeedDataSource="ContactView_NeedDataSource" 
    OnUpdating="ContactView_Updating" 
    OnInserting="ContactView_Inserting"
    OnItemCommand="ContactView_ItemCommand">

    <ItemTemplate>
        <wasp:WLPToolBar ID="EmployeDetailToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Text="Edit" Visible='<%# UpdateFlag %>' ImageUrl="~/App_Themes/Default/Edit.gif" CommandName="edit" ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="ContactRelationshipCode" runat="server" Type="ContactRelationshipCode" OnDataBinding="Code_NeedDataSource" ResourceName="ContactRelationshipCode" Value='<%# Eval("ContactRelationshipCode") %>' ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:DateControl ID="EffectiveDate" runat="server" ResourceName="EffectiveDate" Value='<%# Eval("EffectiveDate") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="TitleCode" runat="server" Type="TitleCode" OnDataBinding="Code_NeedDataSource" ResourceName="TitleCode" Value='<%# Eval("TitleCode") %>' ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="FirstName" runat="server" ResourceName="FirstName" Value='<%# Eval("FirstName") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="MiddleName" runat="server" ResourceName="MiddleName" Value='<%# Eval("MiddleName") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="LastName" runat="server" ResourceName="LastName" Value='<%# Eval("LastName") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="KnownAsName" runat="server" ResourceName="KnownAsName" Value='<%# Eval("KnownAsName") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="GenderCode" runat="server" Type="GenderCode" OnDataBinding="Code_NeedDataSource" ResourceName="GenderCode" Value='<%# Eval("GenderCode") %>' ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="LanguageCode" runat="server" Type="LanguageCode" OnDataBinding="Code_NeedDataSource" ResourceName="LanguageCode" Value='<%# Eval("LanguageCode") %>' ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="BirthDate" runat="server" ResourceName="BirthDate" Value='<%# Eval("BirthDate") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="Age" runat="server" ResourceName="Age" Value='<%# Eval("Age") %>' DecimalDigits="0" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="SmokerCode" runat="server" Type="SmokerCode" OnDataBinding="Code_NeedDataSource" ResourceName="SmokerCode" Value='<%# Eval("SmokerCode") %>' ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="StudentFlag" runat="server" ResourceName="StudentFlag" Value='<%# Eval("StudentFlag") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="DisabledFlag" runat="server" ResourceName="DisabledFlag" Value='<%# Eval("DisabledFlag") %>' ReadOnly="true" />
                    </td>
                </tr>
            </table>
        </fieldset> 
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar ID="EmployeDetailToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert"  ResourceName="Insert"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table  width="100%">
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="ContactRelationshipCode" runat="server" Type="ContactRelationshipCode" OnDataBinding="Code_NeedDataSource" ResourceName="ContactRelationshipCode" Value='<%# Bind("ContactRelationshipCode") %>' TabIndex="010"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:DateControl ID="EffectiveDate" runat="server" ResourceName="EffectiveDate" Value='<%# Bind("EffectiveDate") %>' TabIndex="020" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="TitleCode" runat="server" Type="TitleCode" OnDataBinding="Code_NeedDataSource" ResourceName="TitleCode" Value='<%# Bind("TitleCode") %>' TabIndex="030"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="FirstName" runat="server" ResourceName="FirstName" Value='<%# Bind("FirstName") %>' TabIndex="040" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="MiddleName" runat="server" ResourceName="MiddleName" Value='<%# Bind("MiddleName") %>' TabIndex="050" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="LastName" runat="server" ResourceName="LastName" Value='<%# Bind("LastName") %>' TabIndex="060" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="KnownAsName" runat="server" ResourceName="KnownAsName" Value='<%# Bind("KnownAsName") %>' TabIndex="070" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="GenderCode" runat="server" Type="GenderCode" OnDataBinding="Code_NeedDataSource" ResourceName="GenderCode" Value='<%# Bind("GenderCode") %>' TabIndex="080"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="LanguageCode" runat="server" Type="LanguageCode" OnDataBinding="Code_NeedDataSource" ResourceName="LanguageCode" Value='<%# Bind("LanguageCode") %>' TabIndex="090"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:DateControl ID="BirthDate" runat="server" ResourceName="BirthDate" Value='<%# Bind("BirthDate") %>' TabIndex="100" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="SmokerCode" runat="server" Type="SmokerCode" OnDataBinding="Code_NeedDataSource" ResourceName="SmokerCode" Value='<%# Bind("SmokerCode") %>' TabIndex="110"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="StudentFlag" runat="server" ResourceName="StudentFlag" Value='<%# Bind("StudentFlag") %>' TabIndex="120" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="DisabledFlag" runat="server" ResourceName="DisabledFlag" Value='<%# Bind("DisabledFlag") %>' TabIndex="130" />
                    </td>
                </tr>
            </table>
        </fieldset> 
    </EditItemTemplate>
</wasp:WLPFormView>