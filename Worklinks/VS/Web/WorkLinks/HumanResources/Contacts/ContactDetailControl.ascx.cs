﻿using System;
using System.Web.UI.WebControls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Contacts
{
    public partial class ContactDetailControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public ContactCollection Contact { get; set; }
        public bool IsViewMode { get { return this.ContactView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool IsInsertMode { get { return this.ContactView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsEditMode { get { return this.ContactView.CurrentMode.Equals(FormViewMode.Edit); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeContacts.UpdateFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        public void ChangeModeInsert()
        {
            ContactView.ChangeMode(FormViewMode.Insert);
            ContactView.DataBind();
        }
        #endregion

        #region event handlers
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((WLP.Web.UI.Controls.ICodeControl)sender, base.LanguageCode);
        }
        protected void ContactView_NeedDataSource(object sender, WLP.Web.UI.Controls.WLPNeedDataSourceEventArgs e)
        {
            OnNeedDataSource(e);
            ContactView.DataSource = Contact;
        }
        protected void ContactView_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName.ToLower().Equals("cancel"))
                OnAbortTransaction(null);
        }
        protected void ContactView_Updating(object sender, WLP.Web.UI.Controls.WLPItemUpdatingEventArgs e)
        {
            OnUpdating(e);
        }
        protected void ContactView_Inserting(object sender, WLP.Web.UI.Controls.WLPItemInsertingEventArgs e)
        {
            OnInserting(e);
        }
        #endregion
    }
}