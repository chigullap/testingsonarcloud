﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactTypeControl.ascx.cs" Inherits="WorkLinks.HumanResources.Contacts.ContactTypeControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="AvailableContacts">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="AvailableContacts" />
                <telerik:AjaxUpdatedControl ControlID="AssignedContacts" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="AssignedContacts">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="AvailableContacts" />
                <telerik:AjaxUpdatedControl ControlID="AssignedContacts" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<table width="100%">
    <tr>
        <td colspan="2">
            <wasp:WLPToolBar ID="ContactTypeToolBar" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="ContactTypeToolBar_ButtonClick">
                <Items>
                    <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" ResourceName="Update"></wasp:WLPToolBarButton>
                    <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" CausesValidation="false" onclick="closeAndReturn()" ResourceName="Cancel"></wasp:WLPToolBarButton>
                </Items>
            </wasp:WLPToolBar>
        </td>
    </tr>
    <tr>
        <td>
            <div style="font-weight: bold;">
                <wasp:WLPLabel ResourceName="Available" runat="server" />
            </div>
            <wasp:WLPListBox ID="AvailableContacts" runat="server" Width="305px" Height="298px" AllowTransfer="true" AutoPostBackOnTransfer="true" TransferToID="AssignedContacts" OnTransferred="AssignedContacts_Transferred" EnableDragAndDrop="true" ResourceName="AvailableContacts">
                <ButtonSettings ShowTransferAll="false" />
            </wasp:WLPListBox>
        </td>
        <td>
            <div style="font-weight: bold;">
                <wasp:WLPLabel ResourceName="Assigned" runat="server" />
            </div>
            <wasp:WLPListBox ID="AssignedContacts" runat="server" Width="305px" Height="298px" AllowReorder="true" AutoPostBackOnReorder="true" OnReordered="AssignedContacts_Reordered" EnableDragAndDrop="true" ResourceName="AssignedContacts" />
        </td>
    </tr>
</table>

<script type="text/javascript">
    function getRadWindow() {
        var popup = null;

        if (window.radWindow)
            popup = window.radWindow;
        else if (window.frameElement.radWindow)
            popup = window.frameElement.radWindow;

        return popup;
    }

    function closeAndReturn() {
        var closeWithChanges = true;
        var popup = getRadWindow();

        setTimeout(function () { popup.close(closeWithChanges); }, 0); //have to use "setTimeout" to get around an IE9 bug
    }
</script>