﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;

namespace WorkLinks.HumanResources.Contacts
{
    public partial class ContactControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        #endregion

        #region properties
        /// <summary>
        /// returns true if the control is in read-only mode
        /// </summary>
        public bool IsViewMode
        {
            get { return ContactDetailControl.IsViewMode && ContactAddressDetailControl.IsViewMode && PersonPhoneControl.IsViewMode; }
        }
        private long EmployeeId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); }
        }

        /// <summary>
        /// get current contact data
        /// </summary>
        protected Contact CurrentContact
        {
            get
            {
                Contact contact = null;
                if (ContactGrid.SelectedValue != null)
                    contact = (Contact)Data[ContactGrid.SelectedValue.ToString()];
                return contact;
            }
        }
        protected ContactCollection CurrentContactCollection
        {
            get
            {
                ContactCollection collection = new ContactCollection();
                if (CurrentContact != null)
                    collection.Load(CurrentContact);
                return collection;
            }
        }
        public bool AddFlag
        {
            get { return WorkLinks.Common.Security.RoleForm.EmployeeContacts.AddFlag; }
        }
        public bool DeleteFlag
        {
            get { return WorkLinks.Common.Security.RoleForm.EmployeeContacts.DeleteFlag; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            //does the user have access rights?
            if (!Page.IsPostBack) SetAuthorized(WorkLinks.Common.Security.RoleForm.EmployeeContacts.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                //does the user have rights to seet his employee?
                SetAuthorized(Common.ServiceWrapper.HumanResourcesClient.GetEmployee(new EmployeeCriteria() { EmployeeId = EmployeeId }).Count > 0);
                LoadContacts(EmployeeId);
            }
        }

        /// <summary>
        /// sets control attributes
        /// </summary>
        protected void InitializeControls()
        {
            ContactDetailTabStrip.Enabled = IsViewMode;
            ContactGrid.Enabled = IsViewMode;

            //enable/disable toolbar buttons
            for (int i = 0; i < EmployeeSummaryToolBar.Items.Count; i++)
            {
                EmployeeSummaryToolBar.Items[i].Enabled = IsViewMode;
            }
        }
        #endregion

        #region dataaccess
        protected void LoadContacts(long employeeId)
        {
            Data = ContactCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetContact(EmployeeId, null));

            ((WLPToolBarButton)EmployeeSummaryToolBar.FindButtonByCommandName("Add")).Visible = AddFlag;
            ((WLPToolBarButton)EmployeeSummaryToolBar.FindButtonByCommandName("Delete")).Visible = DeleteFlag;
        }

        protected bool UpdateContact(Contact contact)
        {
            bool noError = true;
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.UpdateContact(contact);//only 1 contact to update
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    noError = false;
                }
            }
            catch (Exception ex)
            {
                noError = false;
                throw ex;
            }
            return noError;
        }

        protected Contact InsertContact(Contact contact)
        {
            return Common.ServiceWrapper.HumanResourcesClient.InsertContact(contact);//only 1 contact to update
        }
        #endregion

        #region override
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            InitializeControls();

            if (ContactGrid.SelectedItems.Count == 0)
                SetSelectedContact();
        }
        #endregion

        protected void WireEvents()
        {
            ContactDetailControl.NeedDataSource += new HumanResources.Contacts.ContactDetailControl.NeedDataSourceEventHandler(ContactDetailControl_NeedDataSource);
            ContactDetailControl.AbortTransaction += new EventHandler(ContactDetailControl_AbortTransaction);
            ContactGrid.NeedDataSource += new Telerik.Web.UI.GridNeedDataSourceEventHandler(ContactGrid_NeedDataSource);
            ContactDetailControl.Updating += new HumanResources.Contacts.ContactDetailControl.UpdatingEventHandler(ContactDetailControl_Updating);
            ContactDetailControl.Inserting += new HumanResources.Contacts.ContactDetailControl.InsertingEventHandler(ContactDetailControl_Inserting);
        }

        void ContactDetailControl_AbortTransaction(object sender, EventArgs e)
        {
            if (ContactDetailControl.IsInsertMode)
            {
                Data.Remove(CurrentContact.Key);
                ContactGrid.Rebind();
            }
        }

        void ContactGrid_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            ContactGrid.DataSource = Data;
        }

        void ContactDetailControl_Updating(object sender, WLP.Web.UI.Controls.WLPItemUpdatingEventArgs e)
        {
            Contact contact = (Contact)e.DataItem;  //only one contact in the collection

            if (UpdateContact(contact))
            {
                contact.CopyTo(CurrentContact);

                ContactGrid.Rebind();
                ContactGrid.SelectRowByFirstDataKey(contact.Key);
                SetSelectedContact();

                OnUpdating(null);
            }
            else
                e.Cancel = true;
        }

        void ContactDetailControl_Inserting(object sender, WLP.Web.UI.Controls.WLPItemInsertingEventArgs e)
        {
            Contact contact = (Contact)e.DataItem;  //only one contact in the collection
            contact.EmployeeId = EmployeeId;

            Contact updatedContact = InsertContact(contact);
            updatedContact.CopyTo(CurrentContact);

            ContactGrid.Rebind();
            ContactGrid.SelectRowByFirstDataKey(updatedContact.Key);
            SetSelectedContact();
        }

        void ContactDetailControl_NeedDataSource(object sender, WLP.Web.UI.Controls.WLPNeedDataSourceEventArgs e)
        {
            ContactDetailControl.Contact = CurrentContactCollection;
        }

        protected void ContactGrid_DataBound(object sender, EventArgs e)
        {
        }

        protected void ContactGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetSelectedContact();
        }

        private void SetSelectedContact()
        {
            if (ContactGrid.Items.Count > 0)
            {
                if (ContactGrid.SelectedItems.Count == 0)
                    ContactGrid.Items[0].Selected = true;

                ContactDetailControl.DataBind();
                /**HACK**this could be done better*/
                ContactAddressDetailControl.EmployeeId = EmployeeId;
                ContactAddressDetailControl.SetPersonId(CurrentContact.PersonId);

                PersonPhoneControl.EmployeeId = EmployeeId;
                PersonPhoneControl.SetPersonId(CurrentContact.PersonId);
                /**HACK**end*/
            }
            else
            {
                ((WLPToolBarButton)EmployeeSummaryToolBar.FindButtonByCommandName("Delete")).Enabled = false;
            }
        }

        protected void EmployeeSummaryToolBar_ButtonClick(object sender, Telerik.Web.UI.RadToolBarEventArgs e)
        {
            RadToolBarButton Button = ((RadToolBarButton)e.Item);

            if (Button.CommandName.ToLower().Equals("add"))
            {
                ContactDetailTabStrip.SelectedIndex = 0;
                ContactDetailMultiPage.SelectedIndex = 0;
                Contact contact = (Contact)Data.AddNew();
                ContactGrid.Rebind();
                ContactGrid.SelectRowByFirstDataKey(contact.Key);
                SetSelectedContact();
                ContactDetailControl.ChangeModeInsert();
            }
            else
            {
                DeleteContact(ContactGrid.SelectedValue.ToString());
                OnDeleting(null);
            }
        }

        protected void DeleteContact(String contactKey)
        {
            try
            {
                Contact contact = (Contact)Data[contactKey];
                Common.ServiceWrapper.HumanResourcesClient.DeleteContact(contact);

                Data.Remove(contact.Key);
                ContactGrid.SelectedIndexes.Clear();
                ContactGrid.Rebind();

                //reset the general, address, phone tabs
                ContactDetailControl.DataBind();

                ContactAddressDetailControl.EmployeeId = -1;
                ContactAddressDetailControl.SetPersonId(-1);

                PersonPhoneControl.DataBind();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
    }
}