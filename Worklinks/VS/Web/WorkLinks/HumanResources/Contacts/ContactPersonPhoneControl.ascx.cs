﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Contacts
{
    public partial class ContactPersonPhoneControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private const String _contactChannelTypeCode = "Phone";
        private const String _personIdKey = "PersonIdKey";
        private const String _employeeIdKey = "EmployeeIdKey";
        private long _personId = -1;
        private long _employeeId = -1;
        private const String _buttonDynamicAddCommandKey = "InitInsert";
        #endregion

        #region properties
        public long EmployeeId
        {
            get
            {
                if (_employeeId == -1)
                {
                    Object obj = ViewState[_employeeIdKey];

                    if (obj != null)
                        _employeeId = Convert.ToInt64(obj);
                }

                return _employeeId;
            }
            set
            {
                _employeeId = value;
                ViewState[_employeeIdKey] = _employeeId;
            }
        }
        public long PersonId
        {
            get
            {
                if (_personId == -1)
                {
                    Object obj = ViewState[_personIdKey];

                    if (obj != null)
                        _personId = Convert.ToInt64(obj);
                }

                return _personId;
            }
            set
            {
                _personId = value;
                ViewState[_personIdKey] = _personId;
            }
        }
        public bool IsViewMode
        {
            get { return this.PersonContactChannelGrid.IsViewMode; }
        }
        public bool IsUpdate
        {
            get { return this.PersonContactChannelGrid.IsEditMode; }
        }
        public bool IsInsert
        {
            get { return PersonContactChannelGrid.IsInsertMode; }
        }
        public bool AddFlag
        {
            get { return Common.Security.RoleForm.EmployeeContacts.AddFlag; }
        }
        public bool UpdateFlag
        {
            get { return Common.Security.RoleForm.EmployeeContacts.UpdateFlag; }
        }
        public bool DeleteFlag
        {
            get { return Common.Security.RoleForm.EmployeeContacts.DeleteFlag; }
        }
        #endregion

        #region main
        protected void LoadPhone(long personId)
        {
            Data = PersonContactChannelCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetPersonContactChannel(personId, _contactChannelTypeCode));
            PersonContactChannelGrid.Rebind();
        }
        public void SetPersonId(long personId)
        {
            PersonId = personId;
            LoadPhone(PersonId);
        }
        #endregion

        #region handle updates
        protected void PersonContactChannelGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            PersonContactChannel channel = (PersonContactChannel)e.Item.DataItem;
            channel.PersonId = PersonId;

            Common.ServiceWrapper.HumanResourcesClient.InsertPersonContactChannel(channel, new List<PersonContactChannel>((PersonContactChannel[])Data.ToArray())).CopyTo((PersonContactChannel)Data[channel.Key]);
            SetPersonId(PersonId);
        }
        protected void PersonContactChannelGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                PersonContactChannel channel = (PersonContactChannel)Data[e.Item.DataSetIndex];

                Common.ServiceWrapper.HumanResourcesClient.UpdatePersonContactChannel(channel, new List<PersonContactChannel>((PersonContactChannel[])Data.ToArray())).CopyTo((PersonContactChannel)Data[channel.Key]);
                SetPersonId(PersonId);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void PersonContactChannelGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeletePersonContactChannel((PersonContactChannel)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region event handlers
        protected void PersonContactChannelGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            PersonContactChannelGrid.DataSource = Data;
        }
        protected void EmployeeSummaryToolBar_Init(object sender, EventArgs e)
        {
            RadToolBar toolbar = (RadToolBar)sender;

            //get the first toolbar item - add
            RadToolBarDropDown addDropDown = (RadToolBarDropDown)toolbar.Items[0];

            //get employee to extract person id
            BusinessLayer.BusinessObjects.Employee employee = Common.ServiceWrapper.HumanResourcesClient.GetEmployee(new EmployeeCriteria() { EmployeeId = EmployeeId })[0];

            //get employees phones to populate drop down
            foreach (PersonContactChannel channel in Common.ServiceWrapper.HumanResourcesClient.GetPersonContactChannel(employee.PersonId, _contactChannelTypeCode))
            {
                bool isContainedInData = false;

                foreach (PersonContactChannel employeeChannel in Data)
                {
                    if (employeeChannel.ContactChannelTypeCode.ToLower().Equals(channel.ContactChannelTypeCode.ToLower()))
                    {
                        isContainedInData = true;
                        break;
                    }
                }

                if (!isContainedInData)
                {
                    RadToolBarButton button = new RadToolBarButton();
                    button.Text = String.Format("Attach Employee {0}{1}", channel.ContactChannelTypeCode.Substring(0, 1).ToUpper(), channel.ContactChannelTypeCode.Substring(1).ToLower());
                    button.CommandName = _buttonDynamicAddCommandKey;
                    button.CommandArgument = channel.ContactChannelId.ToString();
                    addDropDown.Buttons.Add(button);
                }
            }

            addDropDown.Visible = IsViewMode && AddFlag;
        }
        protected void PersonContactChannelGrid_ItemCommand(object source, GridCommandEventArgs e)
        {
            //inject default values into grid
            if (e.CommandName.ToLower().Equals(_buttonDynamicAddCommandKey.ToLower()))
            {
                e.Canceled = true;
                PersonContactChannel channel = new PersonContactChannel();

                if (!(e.CommandArgument ?? String.Empty).Equals(String.Empty))
                {
                    Common.ServiceWrapper.HumanResourcesClient.GetContactChannel(Convert.ToInt64(e.CommandArgument))[0].CopyTo(channel);
                    channel.SharedCount = 1;
                }

                e.Item.OwnerTableView.InsertItem(channel);
            }
        }
        protected void PhoneType_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateComboBoxWithPhoneType((ICodeControl)sender, LanguageCode);
        }
        protected void PhoneTypeEdit_NeedDataSource(object sender, EventArgs e)
        {
            if (IsInsert || ((ICodeControl)sender).SelectedValue == null)
                Common.CodeHelper.PopulateComboBoxWithPhoneTypeRemovingTypesInUse((ICodeControl)sender, LanguageCode, PersonId);
            else
                Common.CodeHelper.PopulateComboBoxWithPhoneTypeRemovingTypesInUseLeavingSelected((ICodeControl)sender, LanguageCode, PersonId, ((ICodeControl)sender).SelectedValue.ToString());
        }
        #endregion
    }
}