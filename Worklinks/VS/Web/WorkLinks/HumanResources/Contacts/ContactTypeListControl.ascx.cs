﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Telerik.Web.UI;
using WorkLinks.BusinessLayer.BusinessObjects;


namespace WorkLinks.HumanResources.Contacts
{
    public partial class ContactTypeListControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private WorkLinks.BusinessLayer.BusinessObjects.Employee _employee = null;
        private const String _employeeKey = "employeeKey";
        #endregion

        #region properties
        public String ContactTypeCode { get; set; }
        protected WorkLinks.BusinessLayer.BusinessObjects.Employee Employee
        {
            get
            {
                if (_employee == null)
                    _employee = (WorkLinks.BusinessLayer.BusinessObjects.Employee)ViewState[_employeeKey];
                return _employee;
            }
            set
            {
                _employee = value;
                ViewState[_employeeKey] = _employee;
            }
        }
        protected long EmployeeId
        {
            get
            {
                return Convert.ToInt64(Page.RouteData.Values["employeeId"]);
            }
        }
        public bool UpdateFlag
        {
            get { return WorkLinks.Common.Security.RoleForm.EmployeeContacts.UpdateFlag; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];
            if (args != null && args.Equals(String.Format("refresh")))
            {
                RefreshEmployee();
            }
            if (!IsPostBack)
            {
                LoadEmployee();
            }
        }

        public void RefreshEmployee()
        {
            LoadEmployee();
            ContactTypeGrid.Rebind();
        }

        //load employee and contact types
        private void LoadEmployee()
        {
            EmployeeCriteria criteria =
                new EmployeeCriteria() { EmployeeId = EmployeeId, IncludeContactTypes = true, IncludeContactTypeCode = ContactTypeCode };
            Employee = Common.ServiceWrapper.HumanResourcesClient.GetEmployee(criteria)[0];
        }

        protected void ContactTypeGrid_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            ContactTypeGrid.DataSource = Employee.ContactTypes;
        }

        protected void ContactTypeGridToolBar_Load(object sender, EventArgs e)
        {
            RadToolBar toolbar = (RadToolBar)sender;
            RadToolBarButton button = (RadToolBarButton)toolbar.FindButtonByCommandName("TypeEdit");
            button.Attributes["onclick"] = String.Format("OpenContactType('{0}')", ContactTypeCode);
        }

        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((WLP.Web.UI.Controls.ICodeControl)sender, LanguageCode);
        }

        //protected void EmployeeWindows_Load(object sender, EventArgs e)
        //{
        //    RadWindow window = (RadWindow) sender;
        //    window.Attributes["OnClientClose"] = String.Format("onClientClose_{0}", ClientID);
        //}
    }
}