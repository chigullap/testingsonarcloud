﻿using System;
using Telerik.Web.UI;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Contacts
{
    public partial class ContactTypeControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private const String _employeeKey = "employeeKey";
        private BusinessLayer.BusinessObjects.Employee _employee = null;
        #endregion

        #region properties
        protected BusinessLayer.BusinessObjects.Employee Employee
        {
            get
            {
                if (_employee == null)
                    _employee = (BusinessLayer.BusinessObjects.Employee)ViewState[_employeeKey];

                return _employee;
            }
            set
            {
                _employee = value;
                ViewState[_employeeKey] = _employee;
            }
        }
        protected long EmployeeId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); }
        }
        protected String ContactTypeCode
        {
            get { return (String)Page.RouteData.Values["type"]; }
        }
        protected ContactCollection DeltaContacts
        {
            get { return GetContactDelta(Employee.ContactTypes); }
        }
        protected ContactTypeCollection ContactTypes
        {
            get { return Employee.ContactTypes; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //temp until this is in the database
                //added to the database
                switch (ContactTypeCode)
                {
                    case "EMER":
                        Page.Title = GetGlobalResourceObject("PageTitle", "EmergencyContacts").ToString();
                        break;
                    case "BENI":
                        Page.Title = GetGlobalResourceObject("PageTitle", "Beneficiaries").ToString();
                        break;
                    case "DEP":
                        Page.Title = GetGlobalResourceObject("PageTitle", "Dependants").ToString();
                        break;
                }

                LoadEmployee();
                BindAvailableContacts();
                BindAssignedContacts();
            }
        }
        protected void LoadEmployee()
        {
            EmployeeCriteria criteria = new EmployeeCriteria()
            {
                EmployeeId = EmployeeId,
                IncludeContacts = true,
                IncludeContactTypes = true,
                IncludeContactTypeCode = ContactTypeCode
            };

            Employee = Common.ServiceWrapper.HumanResourcesClient.GetEmployee(criteria)[0];
        }
        protected void BindAvailableContacts()
        {
            AvailableContacts.DataSource = DeltaContacts;
            AvailableContacts.DataTextField = "ChequeName";
            AvailableContacts.DataValueField = "Key";
            AvailableContacts.DataBind();
        }
        protected void BindAssignedContacts()
        {
            AssignedContacts.DataSource = ContactTypes;
            AssignedContacts.DataTextField = "ChequeName";
            AssignedContacts.DataValueField = "Key";
            AssignedContacts.DataSortField = "SortOrder";
            AssignedContacts.DataBind();
        }
        protected ContactCollection GetContactDelta(ContactTypeCollection collection)
        {
            ContactCollection minus = new ContactCollection();

            foreach (Contact contact in Employee.Contacts)
            {
                bool addContact = true;

                foreach (Contact item in collection)
                {
                    if (item.ContactId == contact.ContactId)
                        addContact = false;
                }

                if (addContact)
                    minus.Add(contact);
            }

            return minus;
        }
        protected Contact GetContactToMove(String sourceListBox, String key)
        {
            Contact contactToMove = null;

            if (sourceListBox.Equals("AssignedContacts"))
                contactToMove = ContactTypes.Get(key);
            else
                contactToMove = DeltaContacts.Get(key);

            return contactToMove;
        }
        protected void ReorderAssignedContacts()
        {
            for (short i = 0; i < ContactTypes.Count;)
                ContactTypes[i].SortOrder = ++i;
            
            Employee.ContactTypes = ContactTypes.SortBySortOrder;
        }
        protected void SetContactTypeTemporaryKey(ContactType type)
        {
            long? minimumKey = ContactTypes.MinimumKeyValue;
            if (minimumKey < 0)
                type.ContactTypeId = (long)minimumKey - 1;
        }
        #endregion

        #region event handlers
        protected void ContactTypeToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("update"))
            {
                try
                {
                    Common.ServiceWrapper.HumanResourcesClient.UpdateEmployee(Employee, Employee.ContactTypes.DeletedItems);

                    System.Web.UI.ClientScriptManager manager = Page.ClientScript;
                    manager.RegisterStartupScript(this.GetType(), "close and return", "closeAndReturn();", true);
                }
                catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
                {
                    if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    else if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.UniqueIndexFault)
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "UniqueIndex")), true);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        protected void AssignedContacts_Transferred(object sender, RadListBoxTransferredEventArgs e)
        {
            if (e.DestinationListBox != null)
            {
                String sourceListBox = (e.DestinationListBox.ID == "AssignedContacts") ? "AvailableContacts" : "AssignedContacts";
                String sourceDataKey = e.Items[0].Value;
                Contact contactToMove = GetContactToMove(sourceListBox, sourceDataKey);

                if (e.DestinationListBox.Equals(AssignedContacts))
                {
                    ContactType type = new ContactType() { ContactTypeCode = this.ContactTypeCode };
                    contactToMove.CopyTo(type);

                    SetContactTypeTemporaryKey(type);
                    ContactTypes.Add(type);
                }
                else
                    ContactTypes.Remove((ContactType)contactToMove);
            }

            ReorderAssignedContacts();
            BindAvailableContacts();
            BindAssignedContacts();
        }
        protected void AssignedContacts_Reordered(object sender, RadListBoxEventArgs e)
        {
            RadListBox listBox = (RadListBox)sender;

            foreach (RadListBoxItem item in listBox.Items)
            {
                int index = AssignedContacts.FindItemIndexByValue(item.Value) + 1;

                foreach (ContactType contact in ContactTypes)
                {
                    if (item.Value == contact.Key)
                    {
                        contact.SortOrder = (short)index;
                        break;
                    }
                }
            }

            Employee.ContactTypes = ContactTypes.SortBySortOrder;
            BindAvailableContacts();
            BindAssignedContacts();
        }
        #endregion
    }
}