﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.HumanResources.Contacts
{
    public partial class ContactAddressDetailControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private long _personId = -1;
        private const String _personIdKey = "PersonIdKey";
        private long _employeeId = -1;
        private const String _employeeIdKey = "EmployeeIdKey";
        private const String _addressKey = "addressKey";
        #endregion

        #region properties
        public long PersonId
        {
            get
            {
                if (_personId == -1)
                {
                    Object obj = ViewState[_personIdKey];
                    if (obj != null)
                        _personId = Convert.ToInt64(obj);
                }

                return _personId;
            }
            set
            {
                _personId = value;
                ViewState[_personIdKey] = _personId;
            }
        }
        public long EmployeeId
        {
            get
            {
                if (_employeeId == -1)
                {
                    Object obj = ViewState[_employeeIdKey];
                    if (obj != null)
                        _employeeId = Convert.ToInt64(obj);
                }

                return _employeeId;
            }
            set
            {
                _employeeId = value;
                ViewState[_employeeIdKey] = _employeeId;
            }
        }
        private PersonAddress Address { get { return (PersonAddress)Data[0]; } }
        protected bool IsSharedFlag { get { return Address.IsShared; } }
        public bool IsViewMode { get { return this.PersonAddressView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeContacts.UpdateFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void SetPersonId(long personId)
        {
            PersonId = personId;
            LoadAddress(PersonId);
        }

        protected void LoadAddress(long personId)
        {
            PersonAddressCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetPersonAddress(personId);

            //do not create instance if delete has occured
            if (personId != -1)
            {
                //if no address exists, must create a new instance of one (gui business logic)
                if (collection.Count == 0)
                    collection.Add(new PersonAddress() { PersonId = PersonId });
            }

            Data = PersonAddressCollection.ConvertCollection(collection);
            PersonAddressView.DataBind();
        }
        #endregion

        #region handles updates
        protected void PersonAddressView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    PersonAddress address = (PersonAddress)e.DataItem; //only one address
                    Common.ServiceWrapper.HumanResourcesClient.UpdatePersonAddress(address, true, new List<PersonAddress>((PersonAddress[])Data.ToArray())).CopyTo(Address);
                    PersonAddressView.DataBind();
                }
                catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
                {
                    if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                        e.Cancel = true;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
                e.Cancel = true;
        }
        #endregion

        #region event handlers
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }

        protected void PersonAddressView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            PersonAddressView.DataSource = Data;
        }

        protected void PersonAddressView_ModeChanging(object sender, FormViewModeEventArgs e)
        {
            //if cancelling edit, reload address from db
            if (e.CancelingEdit)
                LoadAddress(PersonId);
        }

        protected void PersonAddressTypeCode_DataBound(object sender, EventArgs e)
        {
            ComboBoxControl combo = (ComboBoxControl)sender;

            if (IsSharedFlag)
            {
                BusinessLayer.BusinessObjects.Employee employee = Common.ServiceWrapper.HumanResourcesClient.GetEmployee(new EmployeeCriteria() { EmployeeId = EmployeeId })[0];

                foreach (RadComboBoxItem item in combo.Items)
                {
                    foreach (PersonAddress address in Common.ServiceWrapper.HumanResourcesClient.GetPersonAddress(employee.PersonId))
                    {
                        if (address.PersonAddressTypeCode.Equals(item.Value))
                        {
                            /**HACK**need to consider language*/
                            item.Text = String.Format("Employee - {0}", item.Text);
                            item.Attributes.Add(_addressKey, address.AddressId.ToString());
                            break;
                        }
                    }

                    if (item.Attributes[_addressKey] == null)
                        item.Visible = false;
                }
            }
            else
                combo.SelectedIndex = combo.FindItemByValue("CA").Index;
        }

        protected void PersonAddressTypeCode_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ComboBoxControl combo = (ComboBoxControl)o;
            RadComboBoxItem item = combo.Items[combo.SelectedIndex];
            PersonAddress currentAddress = (PersonAddress)Data[0];
            Address newAddress = null;

            if (item.Attributes[_addressKey] != null)
                newAddress = Common.ServiceWrapper.HumanResourcesClient.GetAddress(Convert.ToInt64(item.Attributes[_addressKey]))[0];
            else
                newAddress = new Address();

            newAddress.CopyTo(currentAddress);
            currentAddress.PersonAddressTypeCode = combo.SelectedValue;
            PersonAddressView.DataBind();
        }

        protected void IsShared_CheckedChanged(object sender, EventArgs e)
        {
            ((Address)Address).Clear();

            if (((CheckBoxControl)sender).Checked)
            {
                Address.SharedCount = 1;
                Address.PersonAddressTypeCode = null;
            }
            else
            {
                Address.SharedCount = 0;
                Address.PersonAddressTypeCode = "CA"; /***HACK**need another location for this***/
            }

            PersonAddressView.DataBind();
            ((CheckBoxControl)sender).BindingContainer.FindControl("City").Focus();
        }

        protected void ProvinceStateCode_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            Address.ProvinceStateCode = e.Value;
        }

        protected void CountryCode_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindProvinceCombo(o);
            ((ComboBoxControl)o).BindingContainer.FindControl("AddressLine1").Focus();
        }

        protected void CountryCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
            BindProvinceCombo(sender);
        }

        private void BindProvinceCombo(object sender)
        {
            if (sender is ComboBoxControl)
            {
                ComboBoxControl countryCombo = (ComboBoxControl)sender;
                WLPFormView formItem = countryCombo.BindingContainer as WLPFormView;
                ComboBoxControl provinceCombo = (ComboBoxControl)formItem.FindControl("ProvinceStateCode");
                if (provinceCombo == null)
                {
                    provinceCombo = (ComboBoxControl)formItem.FindControl("ProvinceStateCodeItem");
                }

                provinceCombo.DataSource = Common.ServiceWrapper.CodeClient.GetProvinceStateCode(countryCombo.SelectedValue);
                provinceCombo.ReadOnly = IsSharedFlag || IsViewMode;
                provinceCombo.DataBind();

                if (formItem.DataItem != null && formItem.DataItem is PersonAddress)
                    provinceCombo.Value = ((PersonAddress)formItem.DataItem).ProvinceStateCode;
            }
        }

        protected void PostalZipCodeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = false;
            String countryCode = "";
            String postalZipCode = "";

            TextBoxControl postalZipCodeControl = (TextBoxControl)PersonAddressView.FindControl("PostalZipCode");
            if (postalZipCodeControl != null && postalZipCodeControl.Value != null) postalZipCode = postalZipCodeControl.Value.ToString();

            ComboBoxControl countryCodeCombo = (ComboBoxControl)PersonAddressView.FindControl("CountryCode");
            if (countryCodeCombo != null && countryCodeCombo.Value != null) countryCode = countryCodeCombo.Value.ToString();

            if (!String.IsNullOrEmpty(postalZipCode) && (!String.IsNullOrEmpty(countryCode)))
            {
                //CAN Postal Code or US Zip Code?
                if (countryCode == "CA")
                    args.IsValid = Regex.IsMatch(postalZipCode, @"\A[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY]\d[a-zA-Z] ?\d[a-zA-Z]\d\z");
                else if (countryCode == "US")
                    args.IsValid = Regex.IsMatch(postalZipCode, @"^(\d{5}-\d{4}|\d{5}|\d{9})$|^([a-zA-Z]\d[a-zA-Z] \d[a-zA-Z]\d)$");
                else
                    args.IsValid = true;  //no validation on other countries
            }
            else if (String.IsNullOrEmpty(postalZipCode))
                args.IsValid = true;  //no validation on empty entries.  If postal is mandatory, the mandatory validator will catch it.
            else
                args.IsValid = false;
        }
        #endregion
    }
}