﻿using System;
using System.Web.UI;

namespace WorkLinks.HumanResources.Contacts
{
    public partial class ContactModuleControl : System.Web.UI.UserControl
    {
        #region properties
        private String LastName
        {
            get { return Page.RouteData.Values["lastName"].ToString(); }
        }
        private String FirstName
        {
            get { return Page.RouteData.Values["firstName"].ToString(); }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.Title = String.Format("Contacts - {0}, {1}", LastName, FirstName);

            ContactControl.Deleting += new WLP.Web.UI.WLPUserControl.DeletingEventHandler(ContactControl_Deleting);
            ContactControl.Updating += new WLP.Web.UI.WLPUserControl.UpdatingEventHandler(ContactControl_Updating);
        }
        void ContactControl_Updating(object sender, WLP.Web.UI.Controls.WLPItemUpdatingEventArgs e)
        {
            EmergencyContactTypeListControl.RefreshEmployee();
            DependantContactTypeListControl.RefreshEmployee();
            BeneficiaryContactTypeListControl.RefreshEmployee();
        }
        void ContactControl_Deleting(object sender, WLP.Web.UI.Controls.WLPItemDeletingEventArgs e)
        {
            EmergencyContactTypeListControl.RefreshEmployee();
            DependantContactTypeListControl.RefreshEmployee();
            BeneficiaryContactTypeListControl.RefreshEmployee();
        }
        protected override void OnPreRender(EventArgs e)
        {
            InitializeControls();
            base.OnPreRender(e);
        }
        /// <summary>
        /// sets control attributes
        /// </summary>
        protected void InitializeControls()
        {
            ContactTypeTabStrip.Enabled = ContactControl.IsViewMode;
        }
    }
}