﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactControl.ascx.cs" Inherits="WorkLinks.HumanResources.Contacts.ContactControl" %>
<%@ Register assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register src="ContactPersonPhoneControl.ascx" tagname="PersonPhoneControl" tagprefix="uc1" %>
<%@ Register src="ContactAddressDetailControl.ascx" tagname="ContactAddressDetailControl" tagprefix="uc2" %>
<%@ Register src="ContactDetailControl.ascx" tagname="ContactDetailControl" tagprefix="uc4" %>

<asp:Panel ID="ContactPanel" runat="server">
     <wasp:WLPToolBar ID="EmployeeSummaryToolBar" runat="server" Width="100%" onbuttonclick="EmployeeSummaryToolBar_ButtonClick" OnClientButtonClicking="onClientButtonClicking">
        <Items>
            <wasp:WLPToolBarButton Visible='<%# AddFlag %>' Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="Add" ResourceName="Add" />
            <wasp:WLPToolBarButton Visible='<%# DeleteFlag %>' Text="Delete"  ImageUrl="~/App_Themes/Default/Delete.gif" CausesValidation="false" CommandName="Delete" ResourceName="Delete" />
        </Items>
    </wasp:WLPToolBar>

    <wasp:WLPGrid
        ID="ContactGrid"
        runat="server"
        GridLines="None"
        Height="160px"
        ondatabound="ContactGrid_DataBound"
        AutoGenerateColumns="false"
        OnSelectedIndexChanged="ContactGrid_SelectedIndexChanged">

        <ClientSettings EnablePostBackOnRowClick="true" AllowKeyboardNavigation="true">
            <Selecting AllowRowSelect="true" />
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView DataKeyNames="Key" CommandItemDisplay="Top">
            <CommandItemTemplate />
    
            <RowIndicatorColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>

            <ExpandCollapseColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>

            <Columns>
                <wasp:GridBoundControl DataField="ChequeName" LabelText="ChequeName" SortExpression="ChequeName" UniqueName="ChequeName" ResourceName="ChequeName" />
                <wasp:GridDateTimeControl DataField="EffectiveDate" DataType="System.DateTime" LabelText="EffectiveDate" SortExpression="EffectiveDate" UniqueName="EffectiveDate" ResourceName="EffectiveDate" />
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="ContactRelationshipCode" Type="ContactRelationshipCode" ResourceName="ContactRelationshipCode" LabelText="ContactRelationshipCode" />
            </Columns>
        </MasterTableView>
    </wasp:WLPGrid>

    <wasp:WLPTabStrip ID="ContactDetailTabStrip" runat="server" SelectedIndex="0" MultiPageID="ContactDetailMultiPage" style="margin-bottom: 0">
        <Tabs>
            <wasp:WLPTab Text="General" PageViewID="GeneralPageView" Selected="True" ResourceName="General" />
            <wasp:WLPTab Text="Address" ResourceName="Address" />
            <wasp:WLPTab Text="Phone" ResourceName="Phone" />
        </Tabs>
    </wasp:WLPTabStrip>

    <telerik:RadMultiPage ID="ContactDetailMultiPage" runat="server" SelectedIndex="0">
        <telerik:RadPageView ID="GeneralPageView" runat="server" Selected="true">
            <uc4:ContactDetailControl ID="ContactDetailControl" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="AddressPageView" runat="server">
            <uc2:ContactAddressDetailControl ID="ContactAddressDetailControl" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="PhonePageView" runat="server">
            <uc1:PersonPhoneControl ID="PersonPhoneControl" runat="server" />
        </telerik:RadPageView>
    </telerik:RadMultiPage>
</asp:Panel>

<script type="text/javascript">
    function onClientButtonClicking(sender, args) {
        var comandName = args.get_item().get_commandName();
        if (comandName == "Delete") {
            var message = "<asp:Literal runat="server" Text="<%$ Resources:WarningMessages, DeleteNotificationWarning%>" />";
            args.set_cancel(!confirm(message));
        }
    }
</script>