﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactPersonPhoneControl.ascx.cs" Inherits="WorkLinks.HumanResources.Contacts.ContactPersonPhoneControl" %>
<%@ Register assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPGrid 
    ID="PersonContactChannelGrid" 
    runat="server"
    AutoGenerateColumns="False" 
    GridLines="None" 
    CommandItemDisplay="Top"
    ondeletecommand="PersonContactChannelGrid_DeleteCommand" 
    oninsertcommand="PersonContactChannelGrid_InsertCommand" 
    onneeddatasource="PersonContactChannelGrid_NeedDataSource" 
    onupdatecommand="PersonContactChannelGrid_UpdateCommand" 
    OnItemCommand="PersonContactChannelGrid_ItemCommand">

    <MasterTableView DataKeyNames="Key" CommandItemDisplay="Top">
        <CommandItemTemplate>
            <wasp:WLPToolBar ID="EmployeeSummaryToolBar" runat="server" Width="100%" AutoPostBack="true" OnInit="EmployeeSummaryToolBar_Init">
                <Items>
                    <wasp:WLPToolBarDropDown Text="Add..." ImageUrl="~/App_Themes/Default/Add.gif" ResourceName="Add">
                      <Buttons>
                        <wasp:WLPToolBarButton Text="Add"  CommandName="InitInsert" ResourceName="Add" />
                      </Buttons>
                    </wasp:WLPToolBarDropDown>
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <Columns>
            <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
            <wasp:GridCheckBoxControl DataField="PrimaryFlag" LabelText="PrimaryFlag" SortExpression="PrimaryFlag" UniqueName="PrimaryFlag" ResourceName="PrimaryFlag" />
            <wasp:GridCheckBoxControl DataField="IsShared" LabelText="IsShared" SortExpression="IsShared" UniqueName="IsShared" ResourceName="IsShared" />
            <wasp:GridKeyValueControl OnNeedDataSource="PhoneType_NeedDataSource" DataField="ContactChannelTypeCode" Type="ContactChannelTypeCode" ResourceName="ContactChannelTypeCode" />
            <wasp:GridMaskedControl DataFormatString="{0:###-###-####}" DataType="System.Int64" DataField="PrimaryContactValueAsLong" LabelText="PrimaryContactValue" SortExpression="PrimaryContactValue" UniqueName="PrimaryContactValue" ResourceName="PrimaryContactValue" />
            <wasp:GridBoundControl DataField="SecondaryContactValue" LabelText="SecondaryContactValue" SortExpression="SecondaryContactValue" UniqueName="SecondaryContactValue" ResourceName="SecondaryContactValue" />
            <wasp:GridNumericControl DataField="ContactChannelId" LabelText="ContactChannelId" SortExpression="ContactChannelId" UniqueName="ContactChannelId" DataType="System.Int64" Visible="false" Display="false" ResourceName="ContactChannelID" />
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning%>" ResourceName="Delete" />
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table>
                    <tr>
                        <td>
                            <wasp:CheckBoxControl ID="PrimaryFlag" runat="server" ResourceName="PrimaryFlag" Value='<%# Bind("PrimaryFlag") %>' ReadOnly="False" TabIndex="010" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:CheckBoxControl ID="IsShared" runat="server" ResourceName="IsShared" Value='<%# Eval("IsShared") %>' ReadOnly="true" TabIndex="020" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="ContactChannelTypeCode" runat="server" Type="ContactChannelTypeCode" ondatabinding="PhoneTypeEdit_NeedDataSource" ResourceName="ContactChannelTypeCode" Value='<%# Bind("ContactChannelTypeCode") %>' ReadOnly='<%# (Eval("IsShared") is bool && (bool)Eval("IsShared"))%>' TabIndex="030" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="float: left">
                                <wasp:MaskedControl Mask="###-###-####" ID="PrimaryContactValue" runat="server" ResourceName="PrimaryContactValue" Value='<%# Bind("PrimaryContactValue") %>'  ReadOnly='<%# (Eval("IsShared") is bool && (bool)Eval("IsShared"))%>' TabIndex="040" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="float: left">
                                <wasp:TextBoxControl ID="SecondaryContactValue" runat="server" ResourceName="SecondaryContactValue" Value='<%# Bind("SecondaryContactValue") %>'  ReadOnly='<%# (Eval("IsShared") is bool && (bool)Eval("IsShared"))%>' TabIndex="050" />
                            </div>
                            <div style="float: left; padding-left: 50px;">
                                <asp:RegularExpressionValidator ID="ExtensionLength" runat="server" ForeColor="Red" ControlToValidate="SecondaryContactValue" ErrorMessage="Not Valid" ValidationExpression="^[0-9]{0,9}$">* Not Valid</asp:RegularExpressionValidator>
                            </div>
                        </td>
                    </tr>
                    <asp:TextBox ID="ContactChannelId" runat="server" Text='<%# Bind( "ContactChannelId" ) %>' Visible="false">' ></asp:TextBox>
                    <asp:TextBox ID="SharedCount" runat="server" Text='<%# Bind( "SharedCount" ) %>' Visible="false">' ></asp:TextBox>
                </table>
                <table>
                    <tr>
                        <td>
                            <wasp:WLPButton id="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update" />
                            <wasp:WLPButton id="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# !IsUpdate %>' ResourceName="Insert" />
                        </td>
                        <td>    
                            <wasp:WLPButton id="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" text="Cancel" runat="server" CommandName="cancel" CausesValidation="False" ResourceName="Cancel" />
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>

    </MasterTableView>

</wasp:WLPGrid>