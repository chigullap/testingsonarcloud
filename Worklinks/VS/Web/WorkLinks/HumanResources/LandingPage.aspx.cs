﻿using System;
using System.Security.Claims;
using WorkLinks.Common;

namespace WorkLinks.Web.WorkLinks.HumanResources
{
    public partial class _LandingPage : WorkLinksPage
    {

        public _LandingPage()
        {
        }

        protected override void InitializeCulture()
        {
            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                Session["UserName"] = User.Identity.Name.ToString();
            //Session["UserName"] = ((ClaimsIdentity)User.Identity).FindFirst("uname").Value;
            //yuc
            base.InitializeCulture();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            
        }
    }
}