﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Wizard
{
    public abstract class WizardUserControl : WLP.Web.UI.WLPUserControl
    {
        #region delegates
        public delegate void ItemChangedEventHandler(object sender, ItemChangedEventArgs e);
        public delegate void ItemChangingEventHandler(object sender, EventArgs e);
        public delegate void ItemChangingCompleteHandler(object sender, EventArgs e);
        public event ItemChangedEventHandler ItemChanged;
        public event ItemChangingEventHandler ItemChanging;
        public event ItemChangingCompleteHandler ItemChangingComplete;
        #endregion

        #region fields
        private bool _isExternalDataSourceLoaded = false;
        private IDataItemCollection<IDataItem> _dataItemCollection;
        #endregion

        #region properties
        public bool EnableWizardFunctionalityFlag { get; set; }
        public bool TemplateWizardFlag { get; set; }
        public String CountryCode { get; set; }
        public String WizardItemName { get; set; }
        public bool IsDataExternallyLoaded { get { return EnableWizardFunctionalityFlag; } }
        public IDataItemCollection<IDataItem> DataItemCollection
        {
            get
            {
                if (IsDataExternallyLoaded)
                {
                    if (!_isExternalDataSourceLoaded)
                    {
                        _isExternalDataSourceLoaded = true;
                        OnNeedDataSource(null);
                    }

                    return _dataItemCollection;
                }
                else
                    return Data;
            }
            set
            {
                if (IsDataExternallyLoaded)
                    _dataItemCollection = value;
                else
                    Data = value;
            }
        }
        #endregion

        #region abstract functions
        public abstract void Update(bool updateExterallyControlled);
        public abstract void AddNewDataItem();
        public abstract void ChangeModeEdit();
        #endregion 

        #region invoke events
        public void OnItemChanged(ItemChangedEventArgs e)
        {
            if (ItemChanged != null)
                ItemChanged(this, e);
        }
        public void OnItemChanging(EventArgs e)
        {
            if (ItemChanging != null)
                ItemChanging(this, e);
        }
        public void OnItemChangingComplete(EventArgs e)
        {
            if (ItemChangingComplete != null)
                ItemChangingComplete(this, e);
        }
        #endregion

        #region override
        protected void SetAuthorized(FormSecurity authorized)
        {
            if (!this.EnableWizardFunctionalityFlag) //override on wizards
                base.SetAuthorized(authorized.ViewFlag);
        }
        #endregion 
    }

    public class ItemChangedEventArgs : EventArgs
    {
        public IDataItemCollection<IDataItem> Items { get; set; }
    }
}