﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using System.Web.Security;

namespace WorkLinks.Web.WorkLinks
{
    public partial class SiteMaster : WLP.Web.UI.WLPMasterPage
    {
        #region properties
        protected bool EnableHumanResources { get { return Common.Security.RoleForm.HumanResources; } }
        protected bool EnableEmployee { get { return Common.Security.RoleForm.EmployeeSearch.ViewFlag; } }
        protected bool EnableGrievance { get { return Common.Security.RoleForm.GrievanceSearch.ViewFlag; } }
        protected bool EnableAddWizard { get { return Common.Security.RoleForm.WizardSearch.ViewFlag; } }
        protected bool EnableHRSetup { get { return Common.Security.RoleForm.HRSetup; } }
        protected bool EnableUnion { get { return Common.Security.RoleForm.UnionContact.ViewFlag; } }
        protected bool EnablePolicy { get { return Common.Security.RoleForm.PolicySearch.ViewFlag; } }
        protected bool EnableEntitlement { get { return Common.Security.RoleForm.EntitlementSearch.ViewFlag; } }
        protected bool EnableTA { get { return Common.Security.RoleForm.TA; } }
        protected bool EnableSH { get { return Common.Security.RoleForm.SH; } }
        protected bool EnableScheduler { get { return Common.Security.RoleForm.Scheduler; } }
        protected bool EnableBudget { get { return Common.Security.RoleForm.Budget; } }
        protected bool EnablePayroll { get { return Common.Security.RoleForm.Payroll; } }
        protected bool EnablePayrollEmployee { get { return Common.Security.RoleForm.PayrollEmployee; } }
        protected bool EnablePayrollBatch { get { return Common.Security.RoleForm.PayrollBatchSearch.ViewFlag; } }
        protected bool EnablePayrollException { get { return Common.Security.RoleForm.PayrollException.ViewFlag; } }
        protected bool EnablePayrollProcess { get { return Common.Security.RoleForm.PayrollProcess.ViewFlag; } }
        protected bool RoeCreationSearch { get { return Common.Security.RoleForm.RoeCreationSearch.ViewFlag; } }
        protected bool EnablePayrollPayments { get { return Common.Security.RoleForm.PayrollPayments.ViewFlag; } }
        protected bool EnableRemittance { get { return Common.Security.RoleForm.Remittance.ViewFlag; } }
        protected bool EnableRemittanceImport { get { return Common.Security.RoleForm.RemittanceImport.ViewFlag; } }
        protected bool EnableReports { get { return Common.Security.RoleForm.Reports; } }
        protected bool EnableEmployeePhoneList { get { return Common.Security.RoleForm.EmployeePhoneList.ViewFlag; } }
        protected bool EnableEmployeeBirthdayList { get { return Common.Security.RoleForm.EmployeeBirthdayList.ViewFlag; } }
        protected bool EnableEmployeeNameAndAddressListing { get { return Common.Security.RoleForm.EmployeeNameAndAddressListing.ViewFlag; } }
        protected bool EnableEmployeeAnniversaryListing { get { return Common.Security.RoleForm.EmployeeAnniversaryListing.ViewFlag; } }
        protected bool EnableEmployeeEmergencyContacts { get { return Common.Security.RoleForm.EmployeeEmergencyContacts.ViewFlag; } }
        protected bool EnableEmployeeListing { get { return Common.Security.RoleForm.EmployeeListingReport.ViewFlag; } }
        protected bool EnableLicensesCertificates { get { return Common.Security.RoleForm.SkillsCertificateLicenseSample.ViewFlag; } }
        protected bool EnableTerminatedEmployees { get { return Common.Security.RoleForm.TerminatedEmployees.ViewFlag; } }
        protected bool EnableSalaryHourlyReport { get { return Common.Security.RoleForm.SalaryHourlyReport.ViewFlag; } }
        protected bool EnableStatusCodeReport { get { return Common.Security.RoleForm.StatusCodeReport.ViewFlag; } }
        protected bool EnablePaycodeReport { get { return Common.Security.RoleForm.PaycodeReport.ViewFlag; } }
        protected bool EnableAttritionReport { get { return Common.Security.RoleForm.AttritionReport.ViewFlag; } }
        protected bool EnablePaycodeYearEndMappingReport { get { return Common.Security.RoleForm.PaycodeYearEndMappingReport.ViewFlag; } }
        protected bool EnableNonStatDeductions { get { return Common.Security.RoleForm.NonStatDeductions.ViewFlag; } }
        protected bool EnableStatHoliday { get { return Common.Security.RoleForm.StatHoliday.ViewFlag; } }
        protected bool EnableAdmin { get { return Common.Security.RoleForm.Admin; } }
        protected bool EnableAdminSecurity { get { return Common.Security.RoleForm.AdminSecurity; } }
        protected bool EnableAdminPaycode { get { return Common.Security.RoleForm.AdminPaycode; } }
        protected bool EnableAdminYearEnd { get { return Common.Security.RoleForm.AdminYearEnd; } }
        protected bool EnableCodeMaintenance { get { return Common.Security.RoleForm.CodeMaintenance.ViewFlag; } }
        protected bool EnablePersonAddressTypeCodeMaintenance { get { return Common.Security.RoleForm.PersonAddressTypeCodeMaintenance.ViewFlag; } }
        protected bool EnableSalaryPlanCodeMaintenance { get { return Common.Security.RoleForm.SalaryPlanCodeMaintenance.ViewFlag; } }
        protected bool EnablePayGroup { get { return Common.Security.RoleForm.PayrollProcessingGroup.ViewFlag; } }
        protected bool EnableOrganizationalUnitLevel { get { return Common.Security.RoleForm.OrganizationUnitLevel.ViewFlag; } }
        protected bool EnableMandatory { get { return Common.Security.RoleForm.MandatoryField.ViewFlag; } }
        protected bool EnableGroup { get { return Common.Security.RoleForm.GroupSearch.ViewFlag; } }
        protected bool EnableLanguage { get { return Common.Security.RoleForm.LanguageSearch.ViewFlag; } }
        protected bool EnableRole { get { return Common.Security.RoleForm.RoleSearch.ViewFlag; } }
        protected bool EnableUser { get { return Common.Security.RoleForm.UserSearch.ViewFlag; } }
        protected bool EnableCategory { get { return Common.Security.RoleForm.Category.ViewFlag; } }
        protected bool EnableImport { get { return Common.Security.RoleForm.Import.ViewFlag; } }
        protected bool EnablePayrollReportsMenu { get { return Common.Security.RoleForm.PayrollReportMenu; } }
        protected bool EnableWageTypeCatalogMenuItem { get { return Common.Security.RoleForm.WageTypeCatalogReport.ViewFlag; } }
        protected bool EnableVendor { get { return Common.Security.RoleForm.Vendor.ViewFlag; } }
        protected bool EnableYearEndAdjustmentsSearch { get { return Common.Security.RoleForm.YearEndAdjustmentsSearch.ViewFlag; } }
        protected bool EnableYearEnd { get { return Common.Security.RoleForm.YearEnd.ViewFlag; } }
        protected bool EnableExportFtp { get { return Common.Security.RoleForm.ExportFtp.ViewFlag; } }
        protected bool EnableCodeSystem { get { return Common.Security.RoleForm.CodeSystem.ViewFlag; } }
        protected bool EnableReportExport { get { return Common.Security.RoleForm.ReportExport.ViewFlag; } }
        protected bool EnablePayRegisterExport { get { return Common.Security.RoleForm.PayRegisterExport.ViewFlag; } }
        protected bool EnableGlobalEmployeePaycode { get { return Common.Security.RoleForm.GlobalEmployeePaycode.ViewFlag; } }
        protected bool EnablePaycodeAssociation { get { return Common.Security.RoleForm.PaycodeAssociation.ViewFlag; } }
        protected bool EnablePaycodeMaintenance { get { return Common.Security.RoleForm.PaycodeMaintenance.ViewFlag; } }
        protected bool EnableWsib { get { return Common.Security.RoleForm.Wsib.ViewFlag; } }
        protected bool EnableWcbChequeRemittanceFrequency { get { return Common.Security.RoleForm.WcbChequeRemittanceFrequency.ViewFlag; } }
        protected bool EnableHealthTax { get { return Common.Security.RoleForm.HealthTax.ViewFlag; } }
        protected bool EnableRevenueQuebecBusiness { get { return Common.Security.RoleForm.RevenueQuebecBusinessForm.ViewFlag; } }
        protected bool EnableEmployeeCustomFieldConfig { get { return Common.Security.RoleForm.EmployeeCustomFieldConfig.ViewFlag; } }
        protected bool EnableThirdParty { get { return Common.Security.RoleForm.ThirdParty.ViewFlag; } }
        protected bool EnableWizardTemplate { get { return Common.Security.RoleForm.WizardTemplate.ViewFlag; } }
        protected bool EnableStatutoryHolidayDate { get { return Common.Security.RoleForm.StatutoryHolidayDate.ViewFlag; } }
        protected bool EnableEmail { get { return Common.Security.RoleForm.Email.ViewFlag; } }
        protected bool EnableChangePassword { get { return Common.Security.RoleForm.ChangePassword.ViewFlag; } }
        protected bool EnablePayrollExportsMenuItem { get { return Common.Security.RoleForm.PayrollMenuExport; } }
        protected bool EnableLaborLevelMenuItem { get { return Common.Security.RoleForm.LaborLevel.ViewFlag; } }
        protected bool EnableEmployeeMenuItem { get { return Common.Security.RoleForm.EmployeeExport.ViewFlag; } }
        protected bool EnableAdjustmentRuleMenuItem { get { return Common.Security.RoleForm.AdjustmentRuleExport.ViewFlag; } }
        protected bool EnableEmployeeAuditDetailReport { get { return Common.Security.RoleForm.EmployeeDetailAuditReport.ViewFlag; } }
        protected bool EnableProbationReport { get { return Common.Security.RoleForm.ProbationReport.ViewFlag; } }
        protected bool EnableHrChangesAuditReport { get { return Common.Security.RoleForm.HrChangesAuditReport.ViewFlag; } }
        protected bool EnableBenefit { get { return Common.Security.RoleForm.Benefit; } }
        protected bool EnableBenefitPolicy { get { return Common.Security.RoleForm.BenefitPolicySearch.ViewFlag; } }
        protected bool EnableBenefitPlan { get { return Common.Security.RoleForm.BenefitPlanSearch.ViewFlag; } }
        protected bool EnableSkillReport { get { return Common.Security.RoleForm.SkillReport.ViewFlag; } }
        public string AuthServerUrl { get { return Common.ApplicationParameter.SecurityAuthority; } }
        protected bool EnableSalaryPlan { get { return Common.Security.RoleForm.SalaryPlan.ViewFlag; } }
        
        //used for Stacy/John/Zach to flush cache
        protected bool ShowCacheFlush
        {
            get
            {
                return (HttpContext.Current.User.Identity.Name.ToLower() == "chaissons" || HttpContext.Current.User.Identity.Name.ToLower() == "mckayj" ||
                    HttpContext.Current.User.Identity.Name.ToLower() == "theriaultz" || HttpContext.Current.User.Identity.Name.ToLower() == "admin");
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = "WorkLinks";
            this.MainMenu.ItemClick += new RadMenuEventHandler(MainMenu_ItemClick);

            //lblLoggedInValue.Text = String.Format("{0}: {1}", GetGlobalResourceObject("LoggedInAs", "LoggedInAsMessage"), ((ClaimsIdentity)Page.User.Identity).FindFirst("uname").Value); //yuc
            lblLoggedInValue.Text = String.Format("{0}: {1}", GetGlobalResourceObject("LoggedInAs", "LoggedInAsMessage"), Page.User.Identity.Name.ToString() );
            ChangeLanguage.Text = String.Format("{0}", GetGlobalResourceObject("ChangeLanguage", "ChangeLanguageMessage"));
            HeadLoginStatus.LogoutText = String.Format("{0}", GetGlobalResourceObject("Logout", "LogoutMessage"));

            RadMenuItem currentItem = MainMenu.FindItemByUrl(Request.Url.PathAndQuery);
            if (currentItem != null)
            {
                //Select the current item and his parents
                currentItem.HighlightPath();
                //Update the title of the
                //Populate the breadcrumb
                DataBindBreadCrumbSiteMap(currentItem);
            }
            else
                MainMenu.Items[0].HighlightPath();

            if (!Page.IsPostBack)
            {
                if (((Common.Security.WorkLinksMembershipUser)Membership.GetUser()).IsPasswordExpired)//yuc
                {
                    RadWindow window = new RadWindow();
                    window.NavigateUrl = "~/Account/ChangePassword/true";
                    PasswordWindow.Windows.Add(window);
                    window.VisibleOnPageLoad = true;
                }
            }

            //used for Stacy/John/Zach to flush cache; only show option of one of these users is logged in
            if (MainMenu.Items[8].Items[2].Value == "CacheFlush")
                MainMenu.Items[8].Items[2].Visible = ShowCacheFlush;
        }
        private void DataBindBreadCrumbSiteMap(RadMenuItem currentItem)
        {
            RadMenuItem tempObj = new RadMenuItem();
            List<RadMenuItem> breadCrumbPath = new List<RadMenuItem>();

            while (currentItem != null)
            {
                tempObj = currentItem.Clone();
                breadCrumbPath.Insert(0, tempObj);
                currentItem = currentItem.Owner as RadMenuItem;
            }

            if (breadCrumbPath.Count > 0 && !breadCrumbPath[0].Text.Contains("&nbsp;"))
                breadCrumbPath[0].Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " + breadCrumbPath[0].Text; //added to line it up with the RadMenu

            BreadCrumbSiteMap.DataSource = breadCrumbPath;
            BreadCrumbSiteMap.DataBind();

            //couldn't use foreach because RadSiteMap doesn't have a GetEnumerator method...
            for (int i = 0; i < BreadCrumbSiteMap.Nodes.Count(); i++)
                BreadCrumbSiteMap.Nodes[i].Enabled = false;
        }
        #endregion

        #region events
        protected void ChangeLanguage_Click(object sender, EventArgs e)
        {
            String language = null;
            SecurityUserProfile currentProfile = ((SecurityUserProfile)System.Web.HttpContext.Current.Profile.GetPropertyValue("Profile"));

//            SecurityUserProfile currentProfile = ((SecurityUserProfile)HttpContext.Current.Profile.GetPropertyValue("Profile"));

            if (currentProfile.LanguageCode == null || currentProfile.LanguageCode.ToLower().Equals("fr"))
                language = "EN";
            else
                language = "FR";

            currentProfile.LanguageCode = language;
            currentProfile.ModifiedFlag = true;
            Response.Redirect(Request.Url.ToString());
        }
        void MainMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            if (e.Item.Value == "CacheFlush") //only used by logged in users 'chaissons', 'mckayj', 'theriaultz', 'admin'
            {
                Common.CodeHelper.ResetCodeTableCache(); //flush the code cache
                Common.Security.RoleForm.Clear(); //clear the RoleForm cache (in the case permissions were updated on db)
                Common.Resources.DatabaseResourceCache.ClearCache();
            }
        }
        public void CacheFlush()
        {

        }
        protected void Logout_User(object sender, System.Web.UI.WebControls.LoginCancelEventArgs e)
        {
            try
            {

                ProfileGroup.DestroyGroupCache();
//                Common.ServiceWrapper.SecurityClient.UpdateLogoutUser(HttpContext.Current.Profile.UserName);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Session.Abandon();
                Request.Cookies.Clear();
                //Request.GetOwinContext().Authentication.SignOut();//yuc
                /*
                    new AuthenticationProperties()
                    {
                         = ApplicationParameter.SecurityPostLogoutRedirectUri
                    }
                );*/
                                                                  //                FormsAuthentication.SignOut();

            }
        }
        protected void CompanyName_PreRender(object sender, EventArgs e)
        {
            WLPLabel label = ((WLPLabel)sender);
            label.Text = Common.ApplicationParameter.CompanyName;
        }
        #endregion

        #region security handlers
        protected void HumanResourcesMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableHumanResources; }
        protected void EmployeeMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableEmployee; }
        protected void GrievancesMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableGrievance; }
        protected void AddWizardMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableAddWizard; }
        protected void HRSetupMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableHRSetup; }
        protected void UnionMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableUnion; }
        protected void PolicyMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnablePolicy; }
        protected void EntitlementMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableEntitlement; }
        protected void TAMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableTA; }
        protected void SHMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableSH; }
        protected void SchedulerMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableScheduler; }
        protected void BudgetMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableBudget; }
        protected void PayrollMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnablePayroll; }
        protected void PayrollEmployeeMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnablePayrollEmployee; }
        protected void PayrollBatchMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnablePayrollBatch; }
        protected void PayrollExceptionMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnablePayrollException; }
        protected void PayrollProcessMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnablePayrollProcess; }
        protected void RoeCreationMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = RoeCreationSearch; }
        protected void PayrollPaymentsMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnablePayrollPayments; }
        protected void RemittanceMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableRemittance; }
        protected void RemittanceImportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableRemittanceImport; }
        protected void ReportsMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableReports; }
        protected void EmployeePhoneListMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableEmployeePhoneList; }
        protected void EmployeeBirthdayListMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableEmployeeBirthdayList; }
        protected void EmployeeNameAndAddressListingMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableEmployeeNameAndAddressListing; }
        protected void EmployeeAnniversaryListingMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableEmployeeAnniversaryListing; }
        protected void EmployeeEmergencyContactsMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableEmployeeEmergencyContacts; }
        protected void EmployeeListingMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableEmployeeListing; }
        protected void LicensesCertificatesMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableLicensesCertificates; }
        protected void TerminatedEmployeesMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableTerminatedEmployees; }
        protected void SalaryHourlyReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableSalaryHourlyReport; }
        protected void StatusCodeReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableStatusCodeReport; }
        protected void PaycodeReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnablePaycodeReport; }
        protected void AttritionReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableAttritionReport; }
        protected void PaycodeYearEndMappingReportPage_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnablePaycodeYearEndMappingReport; }
        protected void NonStatDeductionsPage_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableNonStatDeductions; }
        protected void StatHolidayMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableStatHoliday; }
        protected void AdminMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableAdmin; }
        protected void AdminSecurityMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableAdminSecurity; }
        protected void AdminPaycodeMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableAdminPaycode; }
        protected void AdminYearEndMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableAdminYearEnd; }
        protected void CodeMaintenanceMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableCodeMaintenance; }
        protected void PersonAddressTypeCodeMaintenanceMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnablePersonAddressTypeCodeMaintenance; }
        protected void SalaryPlanCodeMaintenanceMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableSalaryPlanCodeMaintenance; }
        protected void PayGroupMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnablePayGroup; }
        protected void OrganizationalUnitLevelMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableOrganizationalUnitLevel; }
        protected void MandatoryMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableMandatory; }
        protected void GroupMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableGroup; }
        protected void LanguageMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableLanguage; }
        protected void RoleMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableRole; }
        protected void UserMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableUser; }
        protected void CategoryMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableCategory; }
        protected void ImportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableImport; }
        protected void PayrollReportsMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnablePayrollReportsMenu; }
        protected void WageTypeCatalogMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableWageTypeCatalogMenuItem; }
        protected void VendorMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableVendor; }
        protected void YearEndAdjustmentsMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableYearEndAdjustmentsSearch; }
        protected void YearEndMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableYearEnd; }
        protected void ExportFtpMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableExportFtp; }
        protected void CodeSystemMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableCodeSystem; }
        protected void ReportExportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableReportExport; }
        protected void PayRegisterExportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnablePayRegisterExport; }
        protected void GlobalEmployeePaycodeMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableGlobalEmployeePaycode; }
        protected void PaycodeAssociationMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnablePaycodeAssociation; }
        protected void PaycodeMaintenanceMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnablePaycodeMaintenance; }
        protected void WsibMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableWsib; }
        protected void WcbChequeRemittanceFrequencyMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableWcbChequeRemittanceFrequency; }
        protected void HealthTaxMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableHealthTax; }
        protected void RevenueQuebecBusinessMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableRevenueQuebecBusiness; }
        protected void EmployeeCustomFieldConfigMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableEmployeeCustomFieldConfig; }
        protected void ThirdPartySearchMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableThirdParty; }
        protected void WizardTemplateMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableWizardTemplate; }
        protected void StatutoryHolidayDateMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableStatutoryHolidayDate; }
        protected void EmailMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableEmail; }
        protected void ChangePasswordMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableChangePassword; }
        protected void PayrollExportsMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnablePayrollExportsMenuItem; }
        protected void LaborLevelMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableLaborLevelMenuItem; }
        protected void EmployeeExportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableEmployeeMenuItem; }
        protected void EmployeeAuditDetailReport_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableEmployeeAuditDetailReport; }
        protected void AdjustmentRuleExportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableAdjustmentRuleMenuItem; }
        protected void ProbationReport_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableProbationReport; }
        protected void HrChangesAuditReport_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableHrChangesAuditReport; }
        protected void BenefitMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableBenefit; }
        protected void BenefitPolicyMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableBenefitPolicy; }
        protected void BenefitPlanMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableBenefitPlan; }
        protected void SkillReport_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableSkillReport; }
        protected void SalaryPlanMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = EnableSalaryPlan; }
        #endregion
    }
}