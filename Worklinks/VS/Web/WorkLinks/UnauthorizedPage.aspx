﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UnauthorizedPage.aspx.cs" Inherits="WorkLinks.UnauthorizedPage" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <wasp:WLPLabel ID="WLPLabel2" Font-Bold="true" Font-Size="Medium" Text="You are not authorized to view this content." runat="server"></wasp:WLPLabel>
        </div>
    </form>
</body>
</html>