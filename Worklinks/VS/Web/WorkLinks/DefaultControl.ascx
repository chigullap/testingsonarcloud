﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DefaultControl.ascx.cs" Inherits="WorkLinks.Web.WorkLinks.DefaultControl" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<telerik:RadDockLayout ID="RadDockLayout1" Runat="server">
    <table>
    <tr>
    <td>
        <telerik:RadDockZone ID="RadDockZone2" runat="server" Height="400" Width="300px">
            <telerik:RadDock ID="RadDock1" runat="server" Height="100" Text="Task List" 
                Index="0" Tag="" ResourceName="RadDock1">
            </telerik:RadDock> </telerik:RadDockZone>
    </td>
    <td>
        <telerik:RadDockZone ID="RadDockZone1" runat="server" Height="200px" Width="300px">
            <telerik:RadDock ID="RadDock2" runat="server" Height="100" Text="Calendar" ResourceName="RadDock2">
            </telerik:RadDock>
                <telerik:RadDock ID="RadDock3" Runat="server" Width="300px" Height="150px" Text="My Employees" ResourceName="RadDock3">
            </telerik:RadDock>
        </telerik:RadDockZone>
    </td>
    </tr>
    </table>
</telerik:RadDockLayout>

