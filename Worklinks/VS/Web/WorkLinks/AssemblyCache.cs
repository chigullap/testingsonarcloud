﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace WorkLinks
{
    /// <summary>
    /// Gui Specific caching functionality
    /// </summary>
    public static class AssemblyCache
    {
        #region fields
        private static Common.CodeHelper _codeHelper;
        #endregion 
        #region properties
        public static Common.CodeHelper CodeHelper
        {
            get 
            {
                if (_codeHelper ==null)
                    _codeHelper = new Common.CodeHelper();
                return _codeHelper;
            }
        }
        #endregion 
    }
}