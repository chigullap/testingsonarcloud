﻿using System;

namespace WorkLinks
{
    public partial class ErrorPage : WLP.Web.UI.WLPPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Exception exc = (Exception)Session["Exception"];

            Response.Write(String.Format("{0}<br /><hr />{1}", exc.InnerException.Message, exc.InnerException.StackTrace));
            Response.Write("<br />");
            Response.Write("<br />");
        }
    }
}