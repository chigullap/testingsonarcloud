﻿using System;
using System.Web.UI;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.PayrollBatch
{
    public partial class PayrollBatchTransactionEntryControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private CodePaycodeIncomeCollection _codePaycodeIncomeCollection = new CodePaycodeIncomeCollection();
        private PayrollBatchReport _parentBatch = null;
        private const String _parentBatchKey = "ParentBatchKey";
        private String _paycodeTypeCodeDesc = "";
        private String _paycodeTypeCodeDescKey = "PaycodeTypeDescKey";
        private PayrollPeriod _payrollPeriod = null;
        private String _paycodeTypeCode = "";
        private Decimal? _paycodeTotals = 0;
        private bool _addButtonOverride = true;
        #endregion

        #region properties
        public PayrollBatchReport PayrollBatch
        {
            get
            {
                if (_parentBatch == null)
                {
                    Object obj = ViewState[_parentBatchKey];
                    if (obj != null)
                        _parentBatch = (PayrollBatchReport)obj;
                }

                return _parentBatch;
            }
            set
            {
                _parentBatch = value;
                ViewState[_parentBatchKey] = _parentBatch;
            }
        }
        public String PaycodeTypeCodeDesc
        {
            get
            {
                if (_paycodeTypeCodeDesc == "")
                {
                    Object obj = ViewState[_paycodeTypeCodeDescKey];
                    if (obj != null)
                        _paycodeTypeCodeDesc = (string)obj;
                }

                return _paycodeTypeCodeDesc;
            }
            set
            {
                _paycodeTypeCodeDesc = value;
                ViewState[_paycodeTypeCodeDescKey] = _paycodeTypeCodeDesc;
            }
        }
        public bool IsAdjustmentBatchType
        {
            get
            {
                if (PayrollBatch != null)
                    return PayrollBatch.PayrollProcessRunTypeCode.Equals("ADJUST");
                else
                    return false;
            }
        }
        public Decimal? YearlyMaximumAmount
        {
            get
            {
                Decimal? yearlyMaximumAmount = null;

                if (CodePaycodeIncomeCollection.Count > 0)
                    yearlyMaximumAmount = CodePaycodeIncomeCollection[0].YearlyMaximumAmount;

                return yearlyMaximumAmount;
            }
        }
        public String PaycodeCode
        {
            get
            {
                String paycodeCode = null;

                if (CodePaycodeIncomeCollection.Count > 0)
                    paycodeCode = CodePaycodeIncomeCollection[0].PaycodeCode;

                return paycodeCode;
            }
        }
        public CodePaycodeIncomeCollection CodePaycodeIncomeCollection
        {
            get { return _codePaycodeIncomeCollection; }
            set { _codePaycodeIncomeCollection = value; }
        }
        public Decimal? PaycodeTotals
        {
            get { return _paycodeTotals; }
            set { _paycodeTotals = value; }
        }
        public PayrollPeriod PayrollPeriod
        {
            get { return _payrollPeriod; }
            set { _payrollPeriod = value; }
        }
        public String PaycodeTypeCode
        {
            get { return _paycodeTypeCode; }
            set { _paycodeTypeCode = value; }
        }
        public bool AddButtonOverride
        {
            get { return _addButtonOverride; }
            set { _addButtonOverride = value; }
        }
        public bool Enabled
        {
            get { return PayrollTransactionSummaryGrid.Enabled; }
            set { PayrollTransactionSummaryGrid.Enabled = value; }
        }
        private long PayrollBatchId { get { return Convert.ToInt64(Page.RouteData.Values["payrollBatchId"]); } }
        public bool IsViewMode { get { return PayrollTransactionSummaryGrid.IsViewMode; } }
        public bool IsUpdate { get { return PayrollTransactionSummaryGrid.IsEditMode; } }
        public bool IsInsert { get { return PayrollTransactionSummaryGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.PayrollBatch.AddFlag; } }
        public String PaycodeMaxWarning { get { return String.Format(GetGlobalResourceObject("WarningMessages", "PaycodeMaxWarning").ToString(), PaycodeCode); } }
        public String DropDownTreeDefaultMessage { get { return String.Format(GetGlobalResourceObject("PageContent", "ChooseADepartment").ToString(), PaycodeCode); } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.PayrollBatch.ViewFlag);

            WireEvents();
        }
        private void WireEvents()
        {
            PayrollTransactionSummaryGrid.NeedDataSource += new GridNeedDataSourceEventHandler(PayrollTransactionSummaryGrid_NeedDataSource);
            PayrollTransactionSummaryGrid.ClientSettings.EnablePostBackOnRowClick = false;
        }
        public void LoadPayrollTransactionSummary(long payrollBatchId)
        {
            Data = PayrollTransactionSummaryCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetPayrollTransactionSummary(payrollBatchId));

            if (IsAdjustmentBatchType)
            {
                //for adjustments, get totals for the transactions (income + benefits - deductions)
                foreach (PayrollTransactionSummary obj in Data)
                {
                    if ((CodePaycode.PaycodeType)Convert.ToInt16(obj.PaycodeTypeCode) == CodePaycode.PaycodeType.Income || (CodePaycode.PaycodeType)Convert.ToInt16(obj.PaycodeTypeCode) == CodePaycode.PaycodeType.Benefit)
                        PaycodeTotals = PaycodeTotals + (obj.Rate * obj.Units);
                    else if ((CodePaycode.PaycodeType)Convert.ToInt16(obj.PaycodeTypeCode) == CodePaycode.PaycodeType.Deduction)
                        PaycodeTotals = PaycodeTotals - (obj.Rate * obj.Units);
                }
            }
        }
        #endregion

        #region event handlers
        protected void PayrollTransactionSummaryGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditFormInsertItem)
            {
                GridEditFormItem item = (GridEditFormItem)e.Item;
                DateControl dateControl = (DateControl)item.FindControl("TransactionDate");

                //set default date
                dateControl.Value = PayrollPeriod.CutoffDate;

                //set min/max for the transactiondate
                dateControl.MinDate = PayrollPeriod.StartDate;
                dateControl.MaxDate = PayrollPeriod.CutoffDate;

                //pass the process group code to the employee control
                EmployeeControl employeeControl = (EmployeeControl)item.FindControl("EmployeeControl1");
                employeeControl.PayrollProcessGroupCode = PayrollBatch.PayrollProcessGroupCode;
            }

            //if in edit mode, show the RadDropDownTree and select the associated department
            if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
            {
                GridEditFormItem item = (GridEditFormItem)e.Item;

                if (!IsInsert && !IsViewMode)
                {
                    PayrollTransactionSummary summary = (PayrollTransactionSummary)item.DataItem;

                    //transaction time
                    RadTimePicker startTime = (RadTimePicker)item.FindControl("StartTime");
                    if (startTime != null)
                        startTime.SelectedTime = summary.TransactionStartTime;

                    //transaction end date/time
                    RadTimePicker endTime = (RadTimePicker)item.FindControl("EndTime");
                    if (endTime != null)
                        endTime.SelectedTime = summary.TransactionEndTime;
                }
            }
        }
        protected void PayrollTransactionSummaryGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            PayrollPeriodCollection periodCollection = new PayrollPeriodCollection();
            if (e.CommandName.ToLower() != "add")
                periodCollection = Common.ServiceWrapper.HumanResourcesClient.GetPayrollPeriodIdFromPayrollProcessGroupCode(new PayrollBatchCriteria() { PayrollProcessGroupCode = PayrollBatch.PayrollProcessGroupCode, PayrollProcessRunTypeCode = PayrollBatch.PayrollProcessRunTypeCode });

            if (periodCollection == null || periodCollection.Count == 0)
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "NoClosedPeriodForAdjustment")), true);
                e.Canceled = true;
            }
            else
            {
                PayrollPeriod = periodCollection[0];
            }
        }
        void PayrollTransactionSummaryGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            PayrollTransactionSummaryGrid.DataSource = Data;
        }
        protected void PaycodeTypeDesc_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);

            if (((ICodeControl)sender).SelectedValue != null) //will be null on insert
                PaycodeTypeCode = ((ICodeControl)sender).SelectedValue.ToString();

            //call to filter out paycodes by type
        }
        protected void PaycodeTypeDesc_DataBound(object sender, EventArgs e)
        {
            if (!IsInsert)
                PaycodeTypeCodeDesc = ((ComboBoxControl)sender).SelectedText.ToString();
            else //insert mode, select "Income" as default, then trigger a "PaycodeTypeCode_SelectedIndexChanged" event
            {
                ((ComboBoxControl)sender).SelectedIndex = 0;
                PaycodeTypeCodeDesc = ((ComboBoxControl)sender).SelectedText.ToString();
                //PaycodeTypeCode_SelectedIndexChanged(sender, null);
            }

            PaycodeCode_DataBinding((ComboBoxControl)sender, e);
        }
        protected void PaycodeCode_DataBinding(object sender, EventArgs e)
        {
            if (sender is ComboBoxControl)
            {
                //find the paycode dropdown and filter accordingly
                ComboBoxControl payCodeTypeCd = (ComboBoxControl)sender;
                GridEditFormItem gridItem = (GridEditFormItem)payCodeTypeCd.BindingContainer;
                ComboBoxControl payCodeCombo = (ComboBoxControl)gridItem.FindControl("PayCode");

                PaycodeTypeCode = ((ComboBoxControl)gridItem.FindControl("PayCodeTypeDescription")).SelectedValue;

                //enable the dropdown for editing
                if (PaycodeTypeCode != "")
                    payCodeCombo.ReadOnly = false;

                Common.CodeHelper.PopulateComboBoxWithPaycodesByType(payCodeCombo, PaycodeTypeCode, true);
                payCodeCombo.DataBind();

                ShowRetroactiveNumberOfPayPeriods(payCodeCombo, null, false);

                if (!IsInsert) //if editing a transaction, determine if we show or hide override fields
                {
                    String payCode = ((PayrollTransactionSummary)gridItem.DataItem).PaycodeCode;
                    CodePaycode code = Common.ServiceWrapper.CodeClient.GetCodePaycode(payCode, false)[0];

                    EmployeeControl emp = (EmployeeControl)gridItem.FindControl("EmployeeControl1");
                    StatutoryDeductionCollection statDedColl = null;

                    if (emp != null)
                    {
                        statDedColl = Common.ServiceWrapper.HumanResourcesClient.GetStatutoryDeduction(Convert.ToInt64(emp.Value));
                        ShowHideProvTaxOverride((code.TaxOverrideFlag && statDedColl.ActiveStatutoryDeduction.ProvinceStateCode == "QC") ? "show" : "hide");
                    }

                    ShowHideFedTaxOverride(code.TaxOverrideFlag ? "show" : "hide");
                    ShowHideStartEndTimes(code.PaycodeTypeCode == "1" ? "show" : "hide");
                    ShowHideDivShowEmploymentInsuranceHour(code.IncludeEmploymentInsuranceHoursFlag == true ? "show" : "hide");
                }
                else //hide fields
                {
                    ShowHideFedTaxOverride("hide");
                    ShowHideProvTaxOverride("hide");
                    ShowHideDivShowEmploymentInsuranceHour("hide");
                    ShowHideStartEndTimes("hide");
                }
            }
        }
        private void ShowHideStartEndTimes(String action)
        {
            ClientScriptManager manager = Page.ClientScript;
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "showTimes", $"showideStartEndTimes('{action}');", true);
        }
        private void ShowHideFedTaxOverride(String action)
        {
            ClientScriptManager manager = Page.ClientScript;
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "showFedTax", String.Format("showHideOverrideFedTax('{0}');", action), true);
        }
        private void ShowHideProvTaxOverride(String action)
        {
            ClientScriptManager manager = Page.ClientScript;
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "showProvTax", String.Format("showHideOverrideProvTax('{0}');", action), true);
        }
        private void ShowHideDivShowEmploymentInsuranceHour(String action)
        {
            ClientScriptManager manager = Page.ClientScript;
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "showEmploymentInsuranceHour", String.Format("showEmploymentInsuranceHour('{0}');", action), true);
        }
        protected void PayCode_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ShowRetroactiveNumberOfPayPeriods((ComboBoxControl)sender, e, true);
        }
        protected void ShowRetroactiveNumberOfPayPeriods(object sender, EventArgs e, bool isSelectedIndexChanged)
        {
            //when a retroactive pay type is selected, display the # of Retroactive Pay Periods field and make it mandatory
            ComboBoxControl paycodeCombo = (ComboBoxControl)sender;
            GridEditFormItem formItem = (GridEditFormItem)paycodeCombo.BindingContainer;
            String paycodeCode = null;

            if (!IsInsert && !isSelectedIndexChanged)
                paycodeCode = ((PayrollTransactionSummary)formItem.DataItem).PaycodeCode;
            else
                paycodeCode = paycodeCombo.Value != null ? paycodeCombo.Value.ToString() : null;

            if (paycodeCode != null)
            {
                CodePaycodeIncomeCollection = Common.ServiceWrapper.CodeClient.GetCodePaycodeIncome(paycodeCode);
                NumericControl retroPayCombo = (NumericControl)formItem.FindControl("RetroactiveNumberOfPayPeriods");
                retroPayCombo.Visible = CodePaycodeIncomeCollection[0].PaycodeIncomePayTypeCode == "RETRO";
                retroPayCombo.Mandatory = CodePaycodeIncomeCollection[0].PaycodeIncomePayTypeCode == "RETRO";
            }
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControlWithoutFiltering((ICodeControl)sender, LanguageCode);
        }
        protected void PayrollTransactionToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (((WLPToolBarButton)e.Item).CommandName.ToLower().Equals("export"))
            {
                PayrollTransactionSummaryGrid.MasterTableView.Columns.FindByUniqueName("editButton").Visible = false;
                PayrollTransactionSummaryGrid.MasterTableView.Columns.FindByUniqueName("deleteButton").Visible = false;

                PayrollTransactionSummaryGrid.ExportSettings.FileName = "Batch" + PayrollBatchId + "Export" + "_" + DateTime.Now.ToString("yyyyMMddHHmm");
                PayrollTransactionSummaryGrid.ExportSettings.ExportOnlyData = true;
                PayrollTransactionSummaryGrid.ExportSettings.HideStructureColumns = true;
                PayrollTransactionSummaryGrid.ExportSettings.IgnorePaging = true;
                PayrollTransactionSummaryGrid.ExportSettings.OpenInNewWindow = true;
                PayrollTransactionSummaryGrid.MasterTableView.ExportToExcel();
            }
        }
        protected void ExportToolBar_PreRender(object sender, EventArgs e)
        {
            ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && IsViewMode;
        }
        #endregion

        #region handles updates
        protected void GetValues(PayrollTransactionSummary payrollTrans, GridCommandEventArgs e)
        {
            CodePaycode paycode = Common.ServiceWrapper.CodeClient.GetCodePaycode(payrollTrans.PaycodeCode, false)[0];

            //if the override EI checkbox is NOT checked, the EI control is disabled and wont pass a value to the server.  It should be equal to units.
            //if the override EI checkbox IS checked, then the control is enabled and will pass the override value to the server.
            CheckBoxControl overrideEiHoursChkBx = (CheckBoxControl)e.Item.FindControl("ckShowEIHours");
            if (!overrideEiHoursChkBx.Checked)
            {
                payrollTrans.EmploymentInsuranceHour = payrollTrans.Units;
                payrollTrans.OverrideEiHours = false;
            }
            else
                payrollTrans.OverrideEiHours = true;

            //default the Fed Tax and Prov Tax if this is not a Tax Override
            if (!paycode.TaxOverrideFlag || IsAdjustmentBatchType)
            {
                payrollTrans.FederalTaxAmount = 0;
                payrollTrans.ProvincialTaxAmount = 0;
            }

            //start and end date/time are used only for paycode type of income (code is "1").  Set values to NULL if type was changed.
            RadTimePicker startTime = (RadTimePicker)e.Item.FindControl("StartTime");
            payrollTrans.TransactionStartTime = (startTime != null && paycode.PaycodeTypeCode == "1") ? startTime.SelectedTime : null;

            RadTimePicker endTime = (RadTimePicker)e.Item.FindControl("EndTime");
            payrollTrans.TransactionEndTime = (endTime != null && paycode.PaycodeTypeCode == "1") ? endTime.SelectedTime : null;
        }
        protected void PayrollTransactionSummaryGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            PayrollTransactionSummary payrollTrans = (PayrollTransactionSummary)e.Item.DataItem;
            payrollTrans.PayrollBatchId = PayrollBatchId;

            GetValues(payrollTrans, e);

            //insert call
            Common.ServiceWrapper.HumanResourcesClient.InsertPayrollTransaction(payrollTrans).CopyTo(payrollTrans);
            ((DataItemCollection<PayrollTransactionSummary>)Data).Add(payrollTrans);

            //reload the parent (so Approve/UnApprove are correctly shown)
            ReloadParent();
        }
        protected void PayrollTransactionSummaryGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                PayrollTransactionSummary payrollTrans = (PayrollTransactionSummary)e.Item.DataItem;
                GetValues(payrollTrans, e);

                //update call
                Common.ServiceWrapper.HumanResourcesClient.UpdatePayrollTransaction(payrollTrans).CopyTo((PayrollTransactionSummary)Data[payrollTrans.Key]);

                //reload the parent (so Approve/UnApprove are correctly shown)
                ReloadParent();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void PayrollTransactionSummaryGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeletePayrollTransaction(((PayrollTransactionSummary)e.Item.DataItem));

                //reload the parent (so Approve/UnApprove are correctly shown)
                ReloadParent();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void ReloadParent()
        {
            //find the parent control and refresh it, this is so the Approve/UnApprove buttons are enabled correctly in PayrollBatchTransactionControl
            System.Web.UI.Control parent = this.Parent;
            if (parent != null && parent is PayrollBatchTransactionControl)
                ((PayrollBatchTransactionControl)parent).ReloadPage();
        }
        #endregion
    }
}