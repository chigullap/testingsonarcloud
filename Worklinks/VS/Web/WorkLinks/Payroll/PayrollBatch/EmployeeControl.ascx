﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeControl.ascx.cs" Inherits="WorkLinks.Payroll.PayrollBatch.EmployeeControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<table width="100%" style="display:block;">
    <tr>
        <td>
            <wasp:TextBoxControl ID="EmployeeNumber" Width="110px" FieldStyle="width:80px;" LabelStyle="width:10px;" Text="" runat="server" Mandatory="true" OnChange="return matchThisEmployee(this,null);" />
            <wasp:WLPButton runat="server" ID="btnOpenEmployeeSearch" OnClientClicking="Open" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" CausesValidation="false" Width="26px" />
            <wasp:WLPLabel ID="EmployeeName" runat="server" />
            <asp:HiddenField ID="HiddenEmployeeId" runat="server" ClientIDMode="Static" OnDataBinding="HiddenEmployeeId_DataBinding" />
            <asp:HiddenField ID="Email" runat="server" ClientIDMode="Static" />
        </td>
    </tr>
</table>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="EmployeeMiniSearchWindows"
    Width="900"
    Height="500"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    OnClientClose="onClientClose"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var payrollProcessGroupCode = '<%= this.PayrollProcessGroupCode %>';
        var hiddenEmployeeIdControl = $get('<%= HiddenEmployeeId.ClientID %>');
        var employeeNameControl = $get('<%= EmployeeName.ClientID %>');
        var employeeNumberControl = $get('<%= EmployeeNumber.ClientID %>');
        var emailControl = $get('<%= Email.ClientID %>');
        var employeeNumberControlField = $get(employeeNumberControl.attributes['fieldClientId'].value);

        function Open(sender, args) {
            openWindowWithManager('EmployeeMiniSearchWindows', String.format('/HumanResources/Employee/EmployeeMiniSearchPage.aspx?payrollProcessGroupCode={0}', payrollProcessGroupCode), false);
            args.set_cancel(true);
        }

        function onClientClose(sender, eventArgs) {
            var arg = eventArgs.get_argument();
            if (arg != null && arg.isUpdate) {
                hiddenEmployeeIdControl.value = arg.employeeId;
                emailControl.value = arg.email;
                employeeNumberControlField.value = arg.employeeNumber;

                <%= OnClientEmployeeChanged %>
            }
        }

        function matchThisEmployee(source, args) {
            var payrollProcessgroup = '<%= PayrollProcessGroupCode %>';
            $.ajax
            ({
                type: "POST",
                url: getUrl("/Payroll/PayrollBatch/PayrollBatchTransactionPage.aspx/MatchEmployee"),
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                cache: false,
                data: "{'employeeNumber': '" + source.value + "' ,'payrollProcessGroup': '" + payrollProcessgroup + "'}",
                success: function (msg) {
                    hiddenEmployeeIdControl.value = msg.d.EmployeeId;
                    if (msg.d.EmployeeId < 0)
                        source.value = '';
                    employeeNameControl.innerText = msg.d.EmployeeName;
                    emailControl.value = msg.d.Email;

                    <%= this.OnClientEmployeeChanged %>
                }
            });
            return false;
        }
    </script>
</telerik:RadScriptBlock>