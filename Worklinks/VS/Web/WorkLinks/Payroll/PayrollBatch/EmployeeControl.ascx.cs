﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.PayrollBatch
{
    public partial class EmployeeControl : WLP.Web.UI.WLPUserControl
    {

        #region delegates
        public delegate void EmployeeChangedEventHandler(object sender, EventArgs e);
        [Browsable(true)]
        public event EmployeeChangedEventHandler EmployeeChanged;
        #endregion

        #region properties
        public bool ReadOnly
        {
            get { return !btnOpenEmployeeSearch.Visible; }
            set 
            { 
                btnOpenEmployeeSearch.Visible = !value;
                btnOpenEmployeeSearch.Enabled = !value;
                EmployeeNumber.Enabled = !value;
            }

        }

        //this is used to embed a method call on the clientside
        public string OnClientEmployeeChanged { get; set; }

        public string Value
        {
            get { return (string)HiddenEmployeeId.Value; }
            set { HiddenEmployeeId.Value = value; }
        }

        public string PayrollProcessGroupCode { get; set; }


        #endregion 

        protected void Page_Load(object sender, EventArgs e)
        {
            //Page.ClientScript.GetPostBackEventReference(this, "");
            //string args = Request["__EVENTARGUMENT"];
            //if (args != null && args.Length > 15 && args.Substring(0, 15).ToLower().Equals("employeenumber="))
            //{
            //    String employeeNumber = args.Substring(15);
            //    EmployeeNumber.Value = employeeNumber.ToString();
            //}
        }

        protected void EmployeeId_DataBinding(object sender, EventArgs e)
        {
            PopulateEmployeeControl((KeyValueControl)sender);
        }

        protected void PopulateEmployeeControl(KeyValueControl control)
        {
            Common.CodeHelper.PopulateEmployeeControl(control, true);
        }

        #region event triggers
        protected virtual void OnEmployeeChanged(EventArgs args)
        {
            if (EmployeeChanged != null)
            {
                EmployeeChanged(this, args);
            }
        }

        #endregion

        protected void HiddenEmployeeId_DataBinding(object sender, EventArgs e)
        {
            PopulateEmployeeDetail();
        }

        public void PopulateEmployeeDetail()
        {
            if (this.Value != null && !this.Value.Equals(String.Empty))
            {
                EmployeeCollection employees = Common.ServiceWrapper.HumanResourcesClient.GetEmployee(new EmployeeCriteria() { EmployeeId = Convert.ToInt64(this.Value) });
                if (employees.Count == 1)
                {
                    Employee employee = employees[0];
                    EmployeeNumber.Value = employee.EmployeeNumber;
                    EmployeeName.Text = employee.ChequeName;
                }
            }
        }


        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
        }
    }
}