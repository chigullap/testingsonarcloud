﻿using System;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.PayrollBatch
{
    public partial class PayrollBatchSearchResultsControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private long? _payrollPeriodId = null;
        private const String _payrollPeriodIdKey = "_payrollPeriodIdKey";

        public delegate void RefreshParentEventHandler(object sender, EventArgs e);
        public event RefreshParentEventHandler RefreshParent;

        //This _enabledByParent variable is only used to control the enable/disable of buttons via javascript when we are re-using this control on the PayrollProcessControl.
        //It is set to "true" in the Page_Load to prevent issues in it's original use
        bool _enabledByParent;

        //These values are used for when a user enters Pay Process (PayrollProcessControl) and decides to "Add" a new batch.
        //We want the PayrollProcessGroupCode and PayProcessRunTypeCode from the Pay Process screen to be passed to the PayrollBatchSearchResultsControl,
        //so it can pass it to the PayrollBatchTransactionControl. There, the comboboxes will be disabled and will default to the value from the PayrollProcessControl.
        String _payProcessGroupValue = null;
        String _payProcessGroupValueKey = "_payProcessGroupValueKey";
        String _payProcessRunTypeValue = null;
        String _payProcessRunTypeValueKey = "_payProcessRunTypeValueKey";
        #endregion

        #region event triggers
        //This is used to refresh parent controls (if this control is used as a child in PayrollProcessing or AdjustmentsProcessing).
        protected virtual void OnRefreshParent(EventArgs args)
        {
            if (RefreshParent != null) //determines if anyone is listening for this event
                RefreshParent(this, args);
        }
        #endregion

        #region properties
        protected long PayrollPeriodId
        {
            get
            {
                if (_payrollPeriodId == null)
                {
                    _payrollPeriodId = (long?)ViewState[_payrollPeriodIdKey];

                    if (_payrollPeriodId == null)
                        _payrollPeriodId = -1;
                }

                return (long)_payrollPeriodId;
            }
            set
            {
                _payrollPeriodId = value;
                ViewState[_payrollPeriodIdKey] = _payrollPeriodId;
            }
        }
        public System.Web.UI.WebControls.Unit ResultsGridHeight
        {
            get { return PayrollBatchSummaryGrid.Height; }
            set { PayrollBatchSummaryGrid.Height = value; }
        }
        public bool EnabledByParent
        {
            get { return _enabledByParent; }
            set { _enabledByParent = value; }
        }
        public String PayrollProcessControlPayProcessGrpCd
        {
            get
            {
                if (_payProcessGroupValue == null)
                    _payProcessGroupValue = (String)ViewState[_payProcessGroupValueKey];

                return _payProcessGroupValue;
            }
            set
            {
                _payProcessGroupValue = value;
                ViewState[_payProcessGroupValueKey] = _payProcessGroupValue;
            }
        }
        public String PayrollProcessControlPayProcessRunTypeCd
        {
            get
            {
                if (_payProcessRunTypeValue == null)
                    _payProcessRunTypeValue = (String)ViewState[_payProcessRunTypeValueKey];

                return _payProcessRunTypeValue;
            }
            set
            {
                _payProcessRunTypeValue = value;
                ViewState[_payProcessRunTypeValueKey] = _payProcessRunTypeValue;
            }
        }
        public bool Enabled
        {
            get { return PayrollBatchSummaryGrid.Enabled; }
            set { PayrollBatchSummaryGrid.Enabled = value; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            EnabledByParent = true;

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.StartsWith("refreshBatch="))
            {
                PayrollBatchCriteria criteria = new PayrollBatchCriteria();
                criteria.PayrollBatchId = Convert.ToInt64(args.Substring(13));

                PayrollBatchReportCollection batches = Common.ServiceWrapper.PayrollBatchReport.Select(criteria);

                if (batches.Count == 0)
                {
                    Data.Remove(criteria.PayrollBatchId.ToString());
                    PayrollBatchSummaryGrid.Rebind();
                }
                else
                {
                    PayrollBatchReport batch = batches[0];

                    if (Data != null)
                    {
                        if (Data[batch.Key] == null)
                            ((PayrollBatchReportCollection)Data).Add(batch);
                        else
                            batch.CopyTo(((PayrollBatchReportCollection)Data)[batch.Key]);
                    }

                    PayrollBatchSummaryGrid.Rebind();
                    PayrollBatchSummaryGrid.SelectRowByFirstDataKey(batch.Key);
                }

                //fire refresh event for parent (PayrollProcessControl/AdjustmentsProcessControl).  This will only execute code if a parent is listening for this event.
                OnRefreshParent(null);
            }
        }
        public void SetPayrollBatchData(PayrollBatchReportCollection collection)
        {
            Data = collection;

            if (collection == null)
                PayrollBatchSummaryGrid.DataBind();
            else
                PayrollBatchSummaryGrid.Rebind();
        }
        public void SetPagerStyle(bool isVisible)
        {
            PayrollBatchSummaryGrid.MasterTableView.PagerStyle.AlwaysVisible = isVisible;
        }
        #endregion

        #region event handlers
        protected void PayrollBatchSummaryGrid_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            ((WLPGrid)source).DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void PayrollBatchSummaryToolBar_ButtonClick(object sender, Telerik.Web.UI.RadToolBarEventArgs e)
        {
            if (((WLPToolBarButton)e.Item).CommandName.ToLower().Equals("export"))
            {
                System.Globalization.TextInfo textInfo = new System.Globalization.CultureInfo("en-US", false).TextInfo;

                PayrollBatchSummaryGrid.ExportSettings.FileName = textInfo.ToTitleCase("BatchExport" + "_" + DateTime.Now.ToString("yyyyMMddHHmm"));
                PayrollBatchSummaryGrid.ExportSettings.ExportOnlyData = true;
                PayrollBatchSummaryGrid.ExportSettings.HideStructureColumns = true;
                PayrollBatchSummaryGrid.ExportSettings.IgnorePaging = true;
                PayrollBatchSummaryGrid.ExportSettings.OpenInNewWindow = true;
                PayrollBatchSummaryGrid.MasterTableView.ExportToExcel();
            }
        }
        #endregion

        #region security handlers
        protected void DetailsToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.PayrollBatch.ViewFlag; }
        protected void AddToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.PayrollBatchSearch.AddFlag; }
        protected void DeleteToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.PayrollBatchSearch.DeleteFlag; }
        protected void ExportToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && Common.Security.RoleForm.PayrollBatchSearch.ViewFlag; }
        #endregion
    }
}