﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.PayrollBatch
{
    public partial class PayrollBatchTransactionControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private const String _batchStatusKey = "BatchStatusKey";
        private bool _hasTransactions = false;
        #endregion

        #region properties
        private bool HasTransactions
        {
            get { return _hasTransactions; }
            set { _hasTransactions = value; }
        }
        public String Action
        {
            get { return Page.RouteData.Values["action"].ToString().ToLower().Trim(); }
        }
        protected long PayrollBatchId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["payrollBatchId"]); }
        }
        protected String PayProcessGrpCd
        {
            get { return Convert.ToString(Page.RouteData.Values["PayProcessGrpCd"]); }
        }
        protected String PayProcessRunTypeCd
        {
            get { return Convert.ToString(Page.RouteData.Values["PayProcessRunTypeCd"]); }
        }
        public String StatusCode
        {
            get
            {
                if (PayrollBatch != null)
                    return PayrollBatch.PayrollBatchStatusCode;
                else
                    return null;
            }
        }
        public PayrollBatchReport PayrollBatch
        {
            get
            {
                if (Data.Count > 0)
                    return (PayrollBatchReport)Data[0];
                else
                    return null;
            }
        }
        public bool IsApproveVisible
        {
            get { return IsNewStatus && PayrollBatch.PayrollProcessStatusCode == null && HasTransactions; }
        }
        public bool IsUnApproveVisible
        {
            get { return IsActiveStatus && PayrollBatch.PayrollProcessStatusCode == null; }
        }
        public bool IsNewStatus
        {
            get { return PayrollBatch.PayrollBatchStatusCode.Equals(Common.ApplicationParameter.Code.PayrollBatchStatusCode.UnApproved); }
        }
        public bool IsActiveStatus
        {
            get { return StatusCode.Equals(Common.ApplicationParameter.Code.PayrollBatchStatusCode.Approved) && PayrollBatch.PayrollProcessId == null; }
        }
        public bool IsViewMode
        {
            get { return PayrollBatchTransactionEntryControl.IsViewMode; }
        }
        public bool IsUpdate
        {
            get { return PayrollBatchTransactionEntryControl.IsUpdate; }
        }
        public bool IsInsert
        {
            get { return PayrollBatchTransactionEntryControl.IsInsert; }
        }
        protected bool DisableDetails
        {
            get { return !IsActiveStatus && PayrollBatch.PayrollProcessStatusCode == null; }
        }
        public bool UpdateFlag
        {
            get { return Common.Security.RoleForm.PayrollBatch.UpdateFlag; }
        }
        public bool DeleteFlag
        {
            get { return Common.Security.RoleForm.PayrollBatch.DeleteFlag; }
        }
        public bool IsFormInsertMode
        {
            get { return PayrollBatchTransactionView.CurrentMode.Equals(FormViewMode.Insert); }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();

            if (!IsPostBack)
            {
                if (Action == "add")
                {
                    PayrollBatchTransactionView.ChangeMode(FormViewMode.Insert);

                    //if PayprocessGrpCd has a value other than "" or "x", then it was populated from PayProcess screen and we need to default the combo to this value and make it readonly.
                    if (PayProcessGrpCd != "x" && PayProcessGrpCd != "refresh")
                    {
                        ComboBoxControl payrollProcessGroupCodeCombo = (ComboBoxControl)PayrollBatchTransactionView.FindControl("PayrollProcessGroupCode");
                        payrollProcessGroupCodeCombo.Value = PayProcessGrpCd;
                        payrollProcessGroupCodeCombo.ReadOnly = true;
                    }

                    //if PayProcessRunTypeCd has a value other than "" or "x", then it was populated from PayProcess screen and we need to default the combo to this value and make it readonly.
                    if (PayProcessRunTypeCd != "x" && PayProcessRunTypeCd != "refresh")
                    {
                        ComboBoxControl payrollProcessRunTypeCodeCombo = (ComboBoxControl)PayrollBatchTransactionView.FindControl("PayrollProcessRunTypeCode");
                        payrollProcessRunTypeCodeCombo.Value = PayProcessRunTypeCd;
                        payrollProcessRunTypeCodeCombo.ReadOnly = true;
                    }
                }
                else //we are in view mode
                {
                    LoadPayrollBatchTransactionSummary(PayrollBatchId);
                    LoadPayrollTransaction(PayrollBatchId);

                    //initialize controls
                    Initialize();
                }
            }
        }
        private void Initialize()
        {
            //Find the PayrollBatchTransactionEntryControl
            WLPGrid grid = (WLPGrid)PayrollBatchTransactionEntryControl.FindControl("PayrollTransactionSummaryGrid");

            // If the batch is marked as locked then don't let the user add/edit/delete...they can approve though.
            bool isLocked = false;
            PayrollBatchReport batch = PayrollBatch;

            if (batch != null)
            {
                isLocked = batch.PayrollBatchTypeCode == "LOCKED";
            }
            
            //disable add button if approved
            PayrollBatchTransactionEntryControl.AddButtonOverride = DisableDetails && !isLocked;
            
            //hide the edit/delete images in the rows based on page logic (DisableDetails) and security (Update/Delete Flag)
            grid.MasterTableView.GetColumn("editButton").Visible = DisableDetails && UpdateFlag && !isLocked;    //position of edit image
            grid.MasterTableView.GetColumn("deleteButton").Visible = DisableDetails && DeleteFlag && !isLocked;  //position of delete image
        }
        protected void WireEvents()
        {
            PayrollBatchTransactionView.Inserting += new WLPFormView.ItemInsertingEventHandler(PayrollBatchTransactionView_Inserting);
        }
        protected void LoadPayrollBatchTransactionSummary(long PayrollBatchId)
        {
            PayrollBatchTransactionView.DataBind();
        }
        protected void LoadPayrollTransaction(long payrollBatchId)
        {
            PayrollBatchTransactionEntryControl.Visible = true;
            PayrollBatchTransactionEntryControl.PayrollBatch = PayrollBatch;
            PayrollBatchTransactionEntryControl.LoadPayrollTransactionSummary(payrollBatchId);
        }
        #endregion

        #region event handlers
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void PayrollBatchTransactionView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            if (PayrollBatchId > 0) //view mode or delete
            {
                Data = PayrollBatchCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetPayrollBatchReport(new PayrollBatchCriteria() { PayrollBatchId = PayrollBatchId }));
                PayrollBatchTransactionView.DataSource = Data;

                if (Data != null && Data.Count > 0)
                    HasTransactions = DoesBatchHaveTransactions(PayrollBatchId);

                this.Page.Title = String.Format(GetGlobalResourceObject("PageTitle", "PayrollBatchView").ToString(), ((BusinessLayer.BusinessObjects.PayrollBatch)PayrollBatchTransactionView.DataSource[0]).BatchSource, ((BusinessLayer.BusinessObjects.PayrollBatch)PayrollBatchTransactionView.DataSource[0]).Description);
                System.Web.UI.ClientScriptManager manager = Page.ClientScript;

                if (Action == "delete") //call the "confirmation()" javascript on the client
                    manager.RegisterStartupScript(this.GetType(), "confirm", "confirmation();", true);
            }
            else //add mode
            {
                this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "PayrollBatchAdd"));

                PayrollBatchCollection collection = new PayrollBatchCollection();
                collection.Load(new BusinessLayer.BusinessObjects.PayrollBatch());
                PayrollBatchTransactionView.DataSource = collection;
            }
        }
        private bool DoesBatchHaveTransactions(long payrollBatchId)
        {
            bool hasTrans = false;

            PayrollTransactionSummaryCollection coll = new PayrollTransactionSummaryCollection();
            coll.Load(Common.ServiceWrapper.HumanResourcesClient.GetPayrollTransactionSummary(PayrollBatchId));

            return hasTrans = coll.Count > 0;
        }
        protected void AddPayrollBatchToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            WLPToolBarButton button = ((WLPToolBarButton)e.Item);

            if (button.CommandName.ToLower().Equals("approve"))
                btnApprove_Click();
            else if (button.CommandName.ToLower().Equals("unapprove"))
                btnUnApprove_Click();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            BusinessLayer.BusinessObjects.PayrollBatch tempObj = PayrollBatchCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetPayrollBatch(PayrollBatchId))[0];

            //delete transactions for this batch, then the batch itself and close the page
            if (DeletePayrollBatch(tempObj))
                Page.ClientScript.RegisterStartupScript(this.GetType(), "close and return", String.Format("closeAndReturn({0});", PayrollBatchId), true);
        }
        protected void btnApprove_Click()
        {
            //change the PayrollBatchStatusCode from "NW" (New) to "AC" (Active) meaning it is ready to process to dynamics
            PayrollBatch.PayrollBatchStatusCode = Common.ApplicationParameter.Code.PayrollBatchStatusCode.Approved;
            Common.ServiceWrapper.HumanResourcesClient.UpdatePayrollBatch(PayrollBatch);

            //refresh the page data
            ReloadPage();
        }
        protected void btnUnApprove_Click()
        {
            //change the PayrollBatchStatusCode from "AC" (Active) to "NW" (New)
            PayrollBatch.PayrollBatchStatusCode = Common.ApplicationParameter.Code.PayrollBatchStatusCode.UnApproved;
            Common.ServiceWrapper.HumanResourcesClient.UpdatePayrollBatch(PayrollBatch);

            //refresh the page data
            ReloadPage();
        }
        public void ReloadPage()
        {
            LoadPayrollBatchTransactionSummary(PayrollBatchId);
            LoadPayrollTransaction(PayrollBatchId);
            Response.Redirect(Request.RawUrl);
        }
        protected void StartDatePeriodYear_DataBinding(object sender, EventArgs e)
        {
            PopulateStartDatePeriodYearControl((KeyValueControl)sender);
        }
        protected void PopulateStartDatePeriodYearControl(KeyValueControl control)
        {
            Common.CodeHelper.PopulateStartDatePeriodYearControl(control); //create own method in CodeHelper.cs
        }
        #endregion

        #region handles updates
        protected bool DeletePayrollBatch(BusinessLayer.BusinessObjects.PayrollBatch payrollBatch)
        {
            bool noError = true;

            try
            {
                Common.ServiceWrapper.HumanResourcesClient.DeletePayrollBatchCascadeTransactions(payrollBatch);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    noError = false;
                }
            }
            catch (Exception ex)
            {
                noError = false;
                throw ex;
            }

            return noError;
        }
        void PayrollBatchTransactionView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            BusinessLayer.BusinessObjects.PayrollBatch payrollBatch = (BusinessLayer.BusinessObjects.PayrollBatch)e.DataItem;
            payrollBatch.PayrollBatchStatusCode = Common.ApplicationParameter.Code.PayrollBatchStatusCode.UnApproved;
            Response.Redirect(String.Format("~/PayrollBatch/PayrollBatchTransaction/View/{0}/{1}/{2}", Common.ServiceWrapper.HumanResourcesClient.InsertPayrollBatch(payrollBatch).PayrollBatchId, "refresh", "refresh"));
        }
        #endregion
    }
}