﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayrollBatchGenericTimeEntryControl.ascx.cs" Inherits="WorkLinks.Payroll.PayrollBatch.PayrollBatchGenericTimeEntryControl" %>
<%--<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register src="EmployeeControl.ascx" tagname="EmployeeControl" tagprefix="uc1" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="PayrollTransactionWeekGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="PayrollTransactionWeekGrid" />
            </UpdatedControls>

        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<wasp:WLPGrid 
    ID="PayrollTransactionWeekGrid" 
    runat="server"
    Skin=""
    AutoGenerateColumns="False"
    onneeddatasource="PayrollTransactionWeekGrid_NeedDataSource" 
    onitemcommand="PayrollTransactionWeekGrid_ItemCommand" 
    OnItemDataBound="PayrollTransactionSummaryGrid_ItemDataBound"
    oninsertcommand="PayrollTransactionWeekGrid_InsertCommand" 
    onupdatecommand="PayrollTransactionWeekGrid_UpdateCommand" 
    OnDeleteCommand="PayrollTransactionWeekGrid_DeleteCommand" 
     >

    <ClientSettings >
     
        <Selecting AllowRowSelect="False"  />
        <Scrolling AllowScroll="True"  />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key" >

    
        <CommandItemTemplate>
            <wasp:WLPToolBar ID="PayrollTransactionToolBar" runat="server" Width="100%" AutoPostBack="true" style="margin-bottom: 0">
                <items>
                    <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsViewMode %>' CommandName="InitInsert"  ResourceName="Add"></wasp:WLPToolBarButton>
                </items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>
        <Columns>
            <wasp:GridEditCommandControl  ButtonType="ImageButton" EditImageUrl="~\App_Themes\Default\Edit.gif"><HeaderStyle Width="30px" /></wasp:GridEditCommandControl>
            <wasp:GridTemplateControl UniqueName="TemplateColumn">
                 <HeaderTemplate>            
                   <table id="Header" border="0" cellspacing="0" cellpadding="0">
                        <tr align="center">
                          <td align="center" colspan="1" width="180"><wasp:WLPLabel ID="lblEmployeeHeader" runat="server"  Text="Employee" Font-Bold="true" ResourceName="lblEmployee"></wasp:WLPLabel></td>
                          <td align="center" colspan="1" width="85px"><wasp:WLPLabel ID="lblDay1TransactionDateHeader" runat="server" Text="Start Date" Font-Bold="true" ResourceName="lblDay1TransactionDateHeader"></wasp:WLPLabel></td>
                          <td align="center" colspan="2" width="76px"><wasp:WLPKeyValue ID="Day1Code" Font-Bold="true" runat="server" Value='<%# Day1Code %>' ondatabinding="DayCode_NeedDataSource" ResourceName="Day1Code" ReadOnly="true" ></wasp:WLPKeyValue></td>
                          <td align="center" colspan="2" width="76px"><wasp:WLPKeyValue ID="Day2Code" Font-Bold="true" runat="server" Value='<%# Day2Code %>'  ondatabinding="DayCode_NeedDataSource" ResourceName="Day2Code" ReadOnly="true" ></wasp:WLPKeyValue></td>
                          <td align="center" colspan="2" width="76px"><wasp:WLPKeyValue ID="Day3Code" Font-Bold="true" runat="server" Value='<%# Day3Code %>' ondatabinding="DayCode_NeedDataSource" ResourceName="Day3Code" ReadOnly="true" ></wasp:WLPKeyValue></td>
                          <td align="center" colspan="2" width="76px"><wasp:WLPKeyValue ID="Day4Code" Font-Bold="true" runat="server" Value='<%# Day4Code %>' ondatabinding="DayCode_NeedDataSource" ResourceName="Day4Code" ReadOnly="true" ></wasp:WLPKeyValue></td>
                          <td align="center" colspan="2" width="76px"><wasp:WLPKeyValue ID="Day5Code" Font-Bold="true" runat="server" Value='<%# Day5Code %>' ondatabinding="DayCode_NeedDataSource" ResourceName="Day5Code" ReadOnly="true" ></wasp:WLPKeyValue></td>
                          <td align="center" colspan="2" width="76px"><wasp:WLPKeyValue ID="Day6Code" Font-Bold="true" runat="server" Value='<%# Day6Code %>' ondatabinding="DayCode_NeedDataSource" ResourceName="Day6Code" ReadOnly="true" ></wasp:WLPKeyValue></td>
                          <td align="center" colspan="2" width="76px"><wasp:WLPKeyValue ID="Day7Code" Font-Bold="true" runat="server" Value='<%# Day7Code %>' ondatabinding="DayCode_NeedDataSource" ResourceName="Day7Code" ReadOnly="true" ></wasp:WLPKeyValue></td>
                          <td align="center" colspan="2" width="76px"><wasp:WLPLabel ID="lblTotalHeader" runat="server" Text="Total" Font-Bold="true" ResourceName="lblTotalHeader"></wasp:WLPLabel></td>
                        </tr>
                        <tr align="center">
                          <td align="center"><wasp:WLPLabel ID="lblEmployee" runat="server" Text="" Font-Bold="true" ResourceName="lblEmployee"></wasp:WLPLabel></td>
                          <td align="center"><wasp:WLPLabel ID="lblDay1TransactionDate" runat="server" Text="" Font-Bold="true" ResourceName="lblDay1TransactionDate"></wasp:WLPLabel></td>
                          <td align="center" width="42px"><wasp:WLPLabel ID="lblDay1Units" runat="server" Text="Reg" Font-Bold="true" ResourceName="lblDay1Units"></wasp:WLPLabel></td>
                          <td align="center" width="42px"><wasp:WLPLabel ID="lblDay1OvertimeUnits" runat="server" Text="OT" Font-Bold="true" ResourceName="lblDay1OvertimeUnits"></wasp:WLPLabel></td>
                          <td align="center" width="42px"><wasp:WLPLabel ID="lblDay2Units" runat="server" Text="Reg" Font-Bold="true" ResourceName="lblDay2Units"></wasp:WLPLabel></td>
                          <td align="center" width="42px"><wasp:WLPLabel ID="lblDay2OvertimeUnits" runat="server" Text="OT" Font-Bold="true" ResourceName="lblDay2OvertimeUnits"></wasp:WLPLabel></td>
                          <td align="center" width="42px"><wasp:WLPLabel ID="lblDay3Units" runat="server" Text=" Reg" Font-Bold="true" ResourceName="lblDay3Units"></wasp:WLPLabel></td>
                          <td align="center" width="42px"><wasp:WLPLabel ID="lblDay3OvertimeUnits" runat="server" Text="OT" Font-Bold="true" ResourceName="lblDay3OvertimeUnits"></wasp:WLPLabel></td>
                          <td align="center" width="42px"><wasp:WLPLabel ID="lblDay4Units" runat="server" Text="Reg" Font-Bold="true" ResourceName="lblDay4Units"></wasp:WLPLabel></td>
                          <td align="center" width="42px"><wasp:WLPLabel ID="lblDay4OvertimeUnits" runat="server" Text="OT" Font-Bold="true" ResourceName="lblDay4OvertimeUnits"></wasp:WLPLabel></td>
                          <td align="center" width="42px"><wasp:WLPLabel ID="lblDay5Units" runat="server" Text="Reg" Font-Bold="true" ResourceName="lblDay5Units"></wasp:WLPLabel></td>
                          <td align="center" width="42px"><wasp:WLPLabel ID="lblDay5OvertimeUnits" runat="server" Text="OT" Font-Bold="true" ResourceName="lblDay5OvertimeUnits"></wasp:WLPLabel></td>
                          <td align="center" width="42px"><wasp:WLPLabel ID="lblDay6Units" runat="server" Text="Reg" Font-Bold="true" ResourceName="lblDay6Units"></wasp:WLPLabel></td>
                          <td align="center" width="42px"><wasp:WLPLabel ID="lblDay6OvertimeUnits" runat="server" Text="OT" Font-Bold="true" ResourceName="lblDay6OvertimeUnits"></wasp:WLPLabel></td>
                          <td align="center" width="42px"><wasp:WLPLabel ID="lblDay7Units" runat="server" Text="Reg" Font-Bold="true" ResourceName="lblDay7Units"></wasp:WLPLabel></td>
                          <td align="center" width="42px"><wasp:WLPLabel ID="lblDay7OvertimeUnits" runat="server" Text="OT" Font-Bold="true" ResourceName="lblDay7OvertimeUnits"></wasp:WLPLabel></td>
                          <td align="center" width="42px"><wasp:WLPLabel ID="lblTotalUnits" runat="server" Text="Reg" Font-Bold="true" ResourceName="lblTotalUnits"></wasp:WLPLabel></td>
                          <td align="center" width="42px"><wasp:WLPLabel ID="lblTotalOvertimeUnits" runat="server" Text="OT" Font-Bold="true" ResourceName="lblTotalOvertimeUnits"></wasp:WLPLabel></td>
                        </tr>
                   </table>
                </HeaderTemplate>
                <ItemTemplate>
                  <table id="Items"  border="0" cellspacing="0" cellpadding="0" >
                    <tr>
                        <td><wasp:WLPKeyValue Width="180px" ID="EmployeeId"  runat="server" ondatabinding="EmployeeId_DataBinding" ResourceName="EmployeeId" Value='<%# Bind("EmployeeId") %>' ReadOnly="true" ></wasp:WLPKeyValue></td>
                        <td><wasp:WLPComboBox Width="85px" ID="Day1TransactionDate" runat="server" Type="Day1TransactionDate" ondatabinding="Day1TransactionDate_NeedDataSource" ResourceName="Day1TransactionDate" Enabled="false" SelectedValue='<%# Bind("Day1TransactionDate") %>' ></wasp:WLPComboBox></td>
                        <td><wasp:WLPNumericTextBox Width="42px" Enabled="false" ID="Day1Units" runat="server" ResourceName="Day1Units" Text='<%# Bind("Day1Units") %>' /></td>
                        <td><wasp:WLPNumericTextBox Width="42px" Enabled="false" ID="Day1OvertimeUnits" runat="server" ResourceName="Day1OvertimeUnits" Text='<%# Bind("Day1OvertimeUnits") %>' /></td>
                        <td><wasp:WLPNumericTextBox Width="42px" Enabled="false" ID="Day2Units" runat="server" ResourceName="Day2Units" Text='<%# Bind("Day2Units") %>' /></td>
                        <td><wasp:WLPNumericTextBox Width="42px" Enabled="false" ID="Day2OvertimeUnits" runat="server" ResourceName="Day2OvertimeUnits" Text='<%# Bind("Day2OvertimeUnits") %>' /></td>
                        <td><wasp:WLPNumericTextBox Width="42px" Enabled="false" ID="Day3Units" runat="server" ResourceName="Day3Units" Text='<%# Bind("Day3Units") %>' /></td>
                        <td><wasp:WLPNumericTextBox Width="42px" Enabled="false" ID="Day3OvertimeUnits" runat="server" ResourceName="Day3OvertimeUnits" Text='<%# Bind("Day3OvertimeUnits") %>' /></td>
                        <td><wasp:WLPNumericTextBox Width="42px" Enabled="false" ID="Day4Units" runat="server" ResourceName="Day4Units" Text='<%# Bind("Day4Units") %>' /></td>
                        <td><wasp:WLPNumericTextBox Width="42px" Enabled="false" ID="Day4OvertimeUnits" runat="server" ResourceName="Day4OvertimeUnits" Text='<%# Bind("Day4OvertimeUnits") %>' /></td>
                        <td><wasp:WLPNumericTextBox Width="42px" Enabled="false" ID="Day5Units" runat="server" ResourceName="Day5Units" Text='<%# Bind("Day5Units") %>' /></td>
                        <td><wasp:WLPNumericTextBox Width="42px" Enabled="false" ID="Day5OvertimeUnits" runat="server" ResourceName="Day5OvertimeUnits" Text='<%# Bind("Day5OvertimeUnits") %>' /></td>
                        <td><wasp:WLPNumericTextBox Width="42px" Enabled="false" ID="Day6Units" runat="server" ResourceName="Day6Units" Text='<%# Bind("Day6Units") %>' /></td>
                        <td><wasp:WLPNumericTextBox Width="42px" Enabled="false" ID="Day6OvertimeUnits" runat="server" ResourceName="Day6OvertimeUnits" Text='<%# Bind("Day6OvertimeUnits") %>' /></td>
                        <td><wasp:WLPNumericTextBox Width="42px" Enabled="false" ID="Day7Units" runat="server" ResourceName="Day7Units" Text='<%# Bind("Day7Units") %>' /></td>
                        <td><wasp:WLPNumericTextBox Width="42px" Enabled="false" ID="Day7OvertimeUnits" runat="server" ResourceName="Day7OvertimeUnits" Text='<%# Bind("Day7OvertimeUnits") %>' /></td>
                        <td><wasp:WLPNumericTextBox Width="42px" Enabled="false" ID="TotalUnits" runat="server" ResourceName="TotalUnits" Text='<%# Bind("TotalUnits") %>' /></td>
                        <td><wasp:WLPNumericTextBox Width="42px" Enabled="false" ID="TotalOvertimeUnits" runat="server" ResourceName="TotalOvertimeUnits" Text='<%# Bind("TotalOvertimeUnits") %>' /></td>
                    </tr>
                  </table>
                </ItemTemplate>
              </wasp:GridTemplateControl>
              <wasp:GridButtonControl ButtonType="ImageButton" ImageUrl="~\App_Themes\Default\Delete.gif" CommandName="Delete"  ConfirmDialogType="Classic" UniqueName="deleteButton" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning%>" ResourceName="Delete"></wasp:GridButtonControl>              

        </Columns>

    <EditFormSettings EditFormType="Template">
    <FormTemplate>
            <table border="0">
                <tr>
                    <td width="204px">
                        <uc1:EmployeeControl ReadOnly='<%# IsUpdate %>'  OnEmployeeChanged="EmployeeControl_EmployeeChanged" OnPreRender="EmployeeControl_PreRender" ID="EmployeeControl1" runat="server" Value='<%# Bind("EmployeeId") %>' TabIndex="010"/>
                    </td>
                    <td><wasp:WLPComboBox ReadOnly='<%# IsUpdate %>' Width="85px" ID="Day1TransactionDate" runat="server" Type="Day1TransactionDate" ondatabinding="Day1TransactionDate_NeedDataSource" ResourceName="Day1TransactionDate" SelectedValue='<%# Bind("Day1TransactionDate") %>' TabIndex="020"></wasp:WLPComboBox></td>
                    <td><wasp:WLPNumericTextBox Width="38px" MaxLength="5" MaxValue="24" MinValue="0" ID="Day1Units" runat="server" ResourceName="Day1Units" Value='<%# Bind("Day1Units") %>' ReadOnly="false" Mandatory="true" TabIndex="030"/></td>
                    <td><wasp:WLPNumericTextBox Width="38px" MaxLength="5" MaxValue="24" MinValue="0" ID="Day1OvertimeUnits" runat="server" ResourceName="Day1OvertimeUnits" Value='<%# Bind("Day1OvertimeUnits") %>' ReadOnly="false" Mandatory="true" TabIndex="040"/></td>
                    <td><wasp:WLPNumericTextBox Width="38px" MaxLength="5" MaxValue="24" MinValue="0" ID="Day2Units"         runat="server" ResourceName="Day2Units" Value='<%# Bind("Day2Units") %>' ReadOnly="false" Mandatory="true" TabIndex="050"/></td>
                    <td><wasp:WLPNumericTextBox Width="38px" MaxLength="5" MaxValue="24" MinValue="0" ID="Day2OvertimeUnits" runat="server" ResourceName="Day2OvertimeUnits" Value='<%# Bind("Day2OvertimeUnits") %>' ReadOnly="false" Mandatory="true" TabIndex="060"/></td>
                    <td><wasp:WLPNumericTextBox Width="38px" MaxLength="5" MaxValue="24" MinValue="0" ID="Day3Units"         runat="server" ResourceName="Day3Units" Value='<%# Bind("Day3Units") %>' ReadOnly="false" Mandatory="true" TabIndex="070"/></td>
                    <td><wasp:WLPNumericTextBox Width="38px" MaxLength="5" MaxValue="24" MinValue="0" ID="Day3OvertimeUnits" runat="server" ResourceName="Day3OvertimeUnits" Value='<%# Bind("Day3OvertimeUnits") %>' ReadOnly="false" Mandatory="true" TabIndex="080"/></td>
                    <td><wasp:WLPNumericTextBox Width="38px" MaxLength="5" MaxValue="24" MinValue="0" ID="Day4Units"         runat="server" ResourceName="Day4Units" Value='<%# Bind("Day4Units") %>' ReadOnly="false" Mandatory="true" TabIndex="090"/></td>
                    <td><wasp:WLPNumericTextBox Width="38px" MaxLength="5" MaxValue="24" MinValue="0" ID="Day4OvertimeUnits" runat="server" ResourceName="Day4OvertimeUnits" Value='<%# Bind("Day4OvertimeUnits") %>' ReadOnly="false" Mandatory="true" TabIndex="100"/></td>
                    <td><wasp:WLPNumericTextBox Width="38px" MaxLength="5" MaxValue="24" MinValue="0" ID="Day5Units"         runat="server" ResourceName="Day5Units" Value='<%# Bind("Day5Units") %>' ReadOnly="false" Mandatory="true" TabIndex="110"/></td>
                    <td><wasp:WLPNumericTextBox Width="38px" MaxLength="5" MaxValue="24" MinValue="0" ID="Day5OvertimeUnits" runat="server" ResourceName="Day5OvertimeUnits" Value='<%# Bind("Day5OvertimeUnits") %>' ReadOnly="false" Mandatory="true" TabIndex="120"/></td>
                    <td><wasp:WLPNumericTextBox Width="38px" MaxLength="5" MaxValue="24" MinValue="0" ID="Day6Units"         runat="server" ResourceName="Day6Units" Value='<%# Bind("Day6Units") %>' ReadOnly="false" Mandatory="true" TabIndex="130"/></td>
                    <td><wasp:WLPNumericTextBox Width="38px" MaxLength="5" MaxValue="24" MinValue="0" ID="Day6OvertimeUnits" runat="server" ResourceName="Day6OvertimeUnits" Value='<%# Bind("Day6OvertimeUnits") %>' ReadOnly="false" Mandatory="true" TabIndex="140"/></td>
                    <td><wasp:WLPNumericTextBox Width="38px" MaxLength="5" MaxValue="24" MinValue="0" ID="Day7Units"         runat="server" ResourceName="Day7Units" Value='<%# Bind("Day7Units") %>' ReadOnly="false" Mandatory="true" TabIndex="150"/></td>
                    <td><wasp:WLPNumericTextBox Width="38px" MaxLength="5" MaxValue="24" MinValue="0" ID="Day7OvertimeUnits" runat="server" ResourceName="Day7OvertimeUnits" Value='<%# Bind("Day7OvertimeUnits") %>' ReadOnly="false" Mandatory="true" TabIndex="160"/></td>
                    <td align="right"><wasp:WLPNumericTextBox Width="70px" ID="Rate" runat="server" ResourceName="Rate" Value='<%# Bind("Rate") %>' ReadOnly="true" TabIndex="170"/></td>
                </tr>
            </table>
                    <table>
                        <tr>
                            <td>
                                <wasp:WLPButton id="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' OnClientClicked="buttonClick"  ResourceName="Update"/>
                                <wasp:WLPButton id="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsInsert %>' OnClientClicked="buttonClick" ResourceName="Insert"/>
                            </td>
                            <td>
                                <wasp:WLPButton id="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" text="Cancel" runat="server" CommandName="cancel" CausesValidation="False" ResourceName="Cancel"/>
                            </td>
                            <td>
                                <asp:CustomValidator ID="ValidateDateRange" ForeColor="Red" ControlToValidate="Day1TransactionDate" runat="server" ErrorMessage="CustomValidator" onservervalidate="ValidateDateRange_ServerValidate" ></asp:CustomValidator>
                            </td>
                        </tr>
                    </table>

        </FormTemplate>
    </EditFormSettings>

    </MasterTableView>


</wasp:WLPGrid>
--%>


