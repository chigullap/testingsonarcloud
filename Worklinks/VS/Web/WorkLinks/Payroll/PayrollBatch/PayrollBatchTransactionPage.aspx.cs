﻿using System;
using System.Web.Services;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.PayrollBatch
{
    public partial class PayrollBatchTransactionPage : WorkLinksPage
    {
        #region private
        static String _user = System.Threading.Thread.CurrentPrincipal.Identity.Name;
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        private static EmployeeCriteria getCriteria(String employeeNumber, String payrollProcessGroup)
        {
            EmployeeCriteria temp = new EmployeeCriteria();

            temp.EmployeeNumber = employeeNumber;
            temp.UseLikeStatementInProcSelect = false;
            temp.PayrollProcessGroupCode = payrollProcessGroup;

            return temp;
        }
        public class MiniEmployee
        {
            public long EmployeeId = -1;
            public string EmployeeName;
            public string Email;
        }
        [WebMethod]
        public static MiniEmployee MatchEmployee(String employeeNumber, String payrollProcessGroup)
        {
            /**HACK**move to business layer*/
            if (Common.Security.RoleForm.PayrollBatch.ViewFlag)
            {
                MiniEmployee miniEmployee = new MiniEmployee();

                if (employeeNumber.Length > 0)
                {
                    EmployeeSummaryCollection collection = new EmployeeSummaryCollection();
                    EmployeeCriteria tempEmpCriteria = getCriteria(employeeNumber, payrollProcessGroup);

                    collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetEmployeeSummary(tempEmpCriteria));

                    if (collection.Count > 0)
                    {
                        miniEmployee.EmployeeId = collection[0].EmployeeId;
                        miniEmployee.EmployeeName = collection[0].LastName + ", " + collection[0].FirstName;
                        miniEmployee.Email = collection[0].Email;
                    }
                    else
                        miniEmployee.EmployeeName = (String)System.Web.HttpContext.GetGlobalResourceObject("Messages", "NoMatchFound");

                    return miniEmployee;
                }
                else
                {
                    miniEmployee.EmployeeName = (String)System.Web.HttpContext.GetGlobalResourceObject("Messages", "NoMatchFound");
                    return miniEmployee;
                }
            }
            else
                throw new Exception("Not Authorized");
        }
        public class RateAndOverride
        {
            public Decimal Rate = -1;
            public Decimal BaseSalaryRate = -1;
            public bool OverrideFedTax = false;
            public bool OverrideProvTax = false;
            public bool StartTime = false;
            public bool IncludeEmploymentInsuranceHoursFlag = false;
        }
        [WebMethod]
        public static RateAndOverride GetRate(long employeeId, String payCode, String isAdjustment)
        {
            if (Common.Security.RoleForm.PayrollBatch.ViewFlag)
            {
                //get paycode
                CodePaycode code = Common.ServiceWrapper.CodeClient.GetCodePaycode(payCode, false)[0];
                RateAndOverride rateOverrideObj = new RateAndOverride();
                StatutoryDeductionCollection statDedColl = Common.ServiceWrapper.HumanResourcesClient.GetStatutoryDeduction(employeeId);

                //if autopopulate get hourly rate
                if (code.AutoPopulateRateFlag)
                {
                    //get rate
                    EmployeePositionCriteria criteria = new EmployeePositionCriteria();
                    criteria.EmployeeId = employeeId;
                    criteria.GetCurrentPositionFlag = true;

                    EmployeePosition position = Common.ServiceWrapper.HumanResourcesClient.GetEmployeePosition(criteria)[0];

                    rateOverrideObj.Rate = Common.ServiceWrapper.HumanResourcesClient.CalculateRate(employeeId, position.RatePerHour, code.AmountRateFactor, code.VacationCalculationOverrideFlag);
                    rateOverrideObj.BaseSalaryRate = position.RatePerHour;
                    rateOverrideObj.OverrideFedTax = (code.TaxOverrideFlag);
                    rateOverrideObj.OverrideProvTax = (code.TaxOverrideFlag && statDedColl.ActiveStatutoryDeduction.ProvinceStateCode == "QC");
                    rateOverrideObj.StartTime = (code.PaycodeTypeCode == "1");
                    rateOverrideObj.IncludeEmploymentInsuranceHoursFlag = code.IncludeEmploymentInsuranceHoursFlag; 

                    return rateOverrideObj;
                }
                else
                {
                    rateOverrideObj.Rate = -1;
                    rateOverrideObj.OverrideFedTax = (code.TaxOverrideFlag);
                    rateOverrideObj.OverrideProvTax = (code.TaxOverrideFlag && statDedColl.ActiveStatutoryDeduction.ProvinceStateCode == "QC");
                    rateOverrideObj.StartTime = (code.PaycodeTypeCode == "1");
                    rateOverrideObj.IncludeEmploymentInsuranceHoursFlag = code.IncludeEmploymentInsuranceHoursFlag;

                    return rateOverrideObj;
                }
            }
            else
                throw new Exception("Not Authorized");
        }
        [WebMethod]
        public static CodeCollection GetPaycodesByType(String codePaycodeType)
        {
            if (Common.Security.RoleForm.PayrollBatch.ViewFlag)
                return Common.CodeHelper.GetPaycodesByType(codePaycodeType, LanguageCode);
            else
                throw new Exception("Not Authorized");
        }

        #endregion


    }
}