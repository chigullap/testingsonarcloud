﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;

//using WorkLinks.BusinessLayer.BusinessObjects;
//using WLP.Web.UI.Controls;
//using Telerik.Web.UI;

//namespace WorkLinks.Payroll.PayrollBatch
//{
//    public partial class PayrollBatchGenericTimeEntryControl : WLP.Web.UI.WLPUserControl
//    {
//        #region fields
//        protected HumanResourcesReference.HumanResourcesClient _client = new HumanResourcesReference.HumanResourcesClient();
//        PayrollBatchReport _parentBatch = null;
//        private const String _parentBatchKey = "ParentBatchKey";
//        private const String _buttonDynamicAddCommandKey = "InitInsert";
//        private const String _dateFormat="dd/MM/yyyy";
//        #endregion

//        #region properties
//        public bool IsUpdate { get { return PayrollTransactionWeekGrid.IsEditMode; } }
//        public bool IsInsert { get { return PayrollTransactionWeekGrid.IsInsertMode; } }
//        public bool IsViewMode { get { return PayrollTransactionWeekGrid.IsViewMode; } }
//        //public bool Enabled
//        //{
//        //    get { return PayrollTransactionWeekGrid.Enabled; }
//        //    set { PayrollTransactionWeekGrid.Enabled = value; }
//        //}


//        protected DayOfWeek Day1
//        {
//            get { return ((DateTime)PayrollBatch.StartDate).DayOfWeek;  }
//        }

//        private DayOfWeek GetDayOfWeekByOffset(short offset)
//        {
//            return (DayOfWeek)(((int)Day1 + offset)%7);
//        }
//        private String GetDayOfWeekCode(DayOfWeek day)
//        {
//            return day.ToString().ToUpper().Substring(0,3);
//        }
//        private String GetDayOfWeekOffsetByCode(short offset)
//        {
//            return GetDayOfWeekCode(GetDayOfWeekByOffset(offset));
//        }

//        protected String Day1Code { get { return GetDayOfWeekOffsetByCode(0); } }
//        protected String Day2Code { get { return GetDayOfWeekOffsetByCode(1); } }
//        protected String Day3Code { get { return GetDayOfWeekOffsetByCode(2); } }
//        protected String Day4Code { get { return GetDayOfWeekOffsetByCode(3); } }
//        protected String Day5Code { get { return GetDayOfWeekOffsetByCode(4); } }
//        protected String Day6Code { get { return GetDayOfWeekOffsetByCode(5); } }
//        protected String Day7Code { get { return GetDayOfWeekOffsetByCode(6); } }

//        public PayrollBatchReport PayrollBatch
//        {
//            get
//            {
//                if (_parentBatch == null)
//                {
//                    Object obj = ViewState[_parentBatchKey];
//                    if (obj != null)
//                        _parentBatch = (PayrollBatchReport)obj;
//                }
//                return _parentBatch;
//            }
//            set
//            {
//                _parentBatch = value;
//                ViewState[_parentBatchKey] = _parentBatch;
//            }
//        }
//        private long SecurityRoleId
//        {
//            get
//            {
//                return (long)SecurityGroup.DatabaseSecurityRoleId;
//            }
//        }
//        private long SecurityUserId
//        {
//            get
//            {
//                return Convert.ToInt64(System.Web.Security.Membership.GetUser().ProviderUserKey);
//            }
//        }
//        #endregion

//        protected void Page_Load(object sender, EventArgs e)
//        {

//        }

//        protected void PayrollTransactionWeekGrid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
//        {
//            PayrollTransactionWeekGrid.DataSource = Data;
//        }

//        public void LoadPayrollTransactionSummary(long payrollBatchId)
//        {
//            Data = PayrollTransactionWeekCollection.ConvertCollection(_client.GetPayrollTransactionWeek(PayrollBatch));
//        }

//        protected void PayrollTransactionWeekGrid_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
//        {
//            if (e.CommandName.ToLower().Equals(_buttonDynamicAddCommandKey.ToLower()))
//            {
//                e.Item.OwnerTableView.InsertItem(new PayrollTransactionWeek() {PayrollBatchId=PayrollBatch.PayrollBatchId});
//            }
//        }

//        protected void PayrollTransactionSummaryGrid_ItemDataBound(object sender, GridItemEventArgs e)
//        {
//            if (e.Item is GridDataItem)
//            {
//                GridDataItem item = (GridDataItem)e.Item;
//                ImageButton lnkbtn = (ImageButton)item["deleteButton"].Controls[0];
//                lnkbtn.Attributes.Add("onClick", "return processClick('Delete');");
//            }
//        }


//        protected void PayrollTransactionWeekGrid_InsertCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
//        {
//            PayrollTransactionWeek record = (PayrollTransactionWeek)e.Item.DataItem;
//            //if (!ValidateDuplicates(record))
//            //{
//            //    e.Canceled = true;
//            //}
//            //else
//            //{
//                record.PayrollBatchId = PayrollBatch.PayrollBatchId;
//                PayrollTransactionWeek updatedRecord = _client.UpdatePayrollTransactionWeek(record, record.Transactions.DeletedItems.ToArray());
////                LoadPayrollTransactionSummary(PayrollBatch.PayrollBatchId);
//           //}
//            updatedRecord.CopyTo((PayrollTransactionWeek)Data[(new PayrollTransactionWeek()).Key]);
//        }

//        // should this be in the gui layer?
//        private bool ValidateDuplicates(long employeeId, DateTime date)
//        {
//            foreach(PayrollTransactionWeek item in Data)
//            {
//                if (item.Day1TransactionDate.Equals(date) && item.EmployeeId.Equals(employeeId))
//                    return false;
//            }
//            return true;
//        }

//        protected void PayrollTransactionWeekGrid_UpdateCommand(object sender, GridCommandEventArgs e)
//        {
//            PayrollTransactionWeek record = (PayrollTransactionWeek)e.Item.DataItem;
//            record.PayrollBatchId = PayrollBatch.PayrollBatchId;
//            PayrollTransactionWeek updatedRecord = _client.UpdatePayrollTransactionWeek(record, record.Transactions.DeletedItems.ToArray());
//            LoadPayrollTransactionSummary(PayrollBatch.PayrollBatchId);
//            //updatedRecord.CopyTo((PayrollTransactionWeek)Data[record.Key]);
//        }

//        protected void PayrollTransactionWeekGrid_DeleteCommand(object sender, GridCommandEventArgs e)
//        {
//            PayrollTransactionWeek record = (PayrollTransactionWeek)e.Item.DataItem;
//            _client.DeletePayrollTransactionWeek(record);
//        }

//        protected void Day1TransactionDate_NeedDataSource(object sender, EventArgs e)
//        {
//            foreach (DateTime date in  (_client.GetPayrollPeriod(new PayrollPeriodCriteria() { PayrollPeriodId = PayrollBatch.PayrollPeriodId }))[0].Day1TransactionDates)
//            {
//                ((WLPComboBox)sender).Items.Add(new RadComboBoxItem(date.ToString(_dateFormat), date.ToString()));
//            }
//        }


//        protected void EmployeeId_DataBinding(object sender, EventArgs e)
//        {
//            PopulateEmployeeControl((ICodeControl)sender);
            
//        }
//        protected void EmployeeControl_PreRender(object sender, EventArgs e)
//        {
//            EmployeeControl control = (EmployeeControl)sender;
//            control.PayrollProcessGroupCode = PayrollBatch.PayrollProcessGroupCode;
//        }
//        protected void EmployeeControl_EmployeeChanged(object sender, EventArgs e)
//        {
//            EmployeeControl control = (EmployeeControl)sender;
//            //get rate
//            EmployeePosition position = _client.GetEmployeePosition(
//                new EmployeePositionCriteria() 
//                { 
//                    EmployeeId = Convert.ToInt64(control.Value), 
//                    GetCurrentPositionFlag = true, 
//                    DataBaseSecurityRoleId = SecurityRoleId, 
//                    SecurityUserId= SecurityUserId 
//                })[0];
//            WLPNumericTextBox textBox = (WLPNumericTextBox)control.BindingContainer.FindControl("Rate");
//            textBox.Value = position.CompensationAmount == null ? null : (Decimal?)Convert.ToDecimal(position.CompensationAmount);
//        }

//        protected void DayCode_NeedDataSource(object sender, EventArgs e)
//        {
//            Common.CodeHelper.PopulateControl((ICodeControl)sender, Language,CodeTableType.DayOfWeekCode);
//        }

//        protected void PopulateEmployeeControl(ICodeControl control)
//        {
//            Common.CodeHelper.PopulateEmployeeControl(control, SecurityRoleId, SecurityUserId, false);
//        }

//        protected void ValidateDateRange_ServerValidate(object source, ServerValidateEventArgs args)
//        {
//            if (((WebControl)source).NamingContainer is GridEditFormInsertItem)
//            {
//                long employeeNumber = Convert.ToInt64(((EmployeeControl)((WebControl)source).NamingContainer.FindControl("EmployeeControl1")).Value);
//                DateTime startDate = Convert.ToDateTime(((WLPComboBox)((WebControl)source).NamingContainer.FindControl("Day1TransactionDate")).SelectedValue);
//                if (employeeNumber < 0)
//                {
//                    args.IsValid = false;
//                    ((CustomValidator)source).Text = (String)GetGlobalResourceObject("ErrorMessages", "EmployeeIsRequired");
//                }
//                if (args.IsValid && !ValidateDuplicates(employeeNumber, startDate))
//                {
//                    args.IsValid = false;
//                    ((CustomValidator)source).Text = (String)GetGlobalResourceObject("ErrorMessages", "DupeStartDateEmployee"); 
//                }
//            }
//        }


//    }
//}