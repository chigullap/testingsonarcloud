﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayrollBatchSearchControl.ascx.cs" Inherits="WorkLinks.Payroll.PayrollBatch.PayrollBatchSearchControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Src="PayrollBatchSearchResultsControl.ascx" TagName="PayrollBatchSearchResultsControl" TagPrefix="uc1" %>

<wasp:WLPAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="ResultsPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="ResultsPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="ResultsPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</wasp:WLPAjaxManagerProxy>

<table width="100%">
    <tr valign="top">
        <td>
            <div>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:TextBoxControl ID="BatchSource" runat="server" ResourceName="BatchSource" ReadOnly="false" TabIndex="010" />
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="Description" runat="server" ResourceName="Description" ReadOnly="false" TabIndex="020" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="PayGroupCode" runat="server" ResourceName="PayGroupCode" Type="PayrollProcessGroupCode" ReadOnly="false" OnDataBinding="Code_NeedDataSource" TabIndex="030" />
                            </td>
                            <td>
                                <wasp:ComboBoxControl ID="PayrollProcessRunTypeCode" runat="server" ResourceName="PayrollProcessRunTypeCode" Type="PayrollProcessRunTypeCode" ReadOnly="false" OnDataBinding="Code_NeedDataSource" TabIndex="040" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="PayrollBatchStatusCode" runat="server" ResourceName="PayrollBatchStatusCode" Type="PayrollBatchStatusCode" ReadOnly="false" OnDataBinding="Code_NeedDataSource" TabIndex="050" />
                            </td>
                            <td>
                                <wasp:CheckBoxControl ID="ProcessedFlag" runat="server" ResourceName="ProcessedFlag" LabelText="Processed" ReadOnly="false" TabIndex="060" />
                            </td>
                        </tr>
                    </table>
                    <div class="SearchCriteriaButtons">
                        <wasp:WLPButton ID="btnClear" Icon-PrimaryIconUrl="~/App_Themes/Default/clear.gif" runat="server" Text="Clear" ResourceName="btnClear" onclick="btnClear_Click" />
                        <wasp:WLPButton ID="btnSearch" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" runat="server" Text="Search" ResourceName="btnSearch" onclick="btnSearch_Click" />
                    </div>
                </asp:Panel>
            </div>
        </td>
    </tr>
    <tr valign="top">
        <td>
            <div>
                <uc1:PayrollBatchSearchResultsControl ID="PayrollBatchSearchResultsControl" runat="server" ResultsGridHeight="404" />
            </div>
        </td>
    </tr>
</table>