﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayrollBatchSearchResultsControl.ascx.cs" Inherits="WorkLinks.Payroll.PayrollBatch.PayrollBatchSearchResultsControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<asp:Panel ID="ResultsPanel" runat="server">
    <wasp:WLPToolBar ID="PayrollBatchSummaryToolBar" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="PayrollBatchSummaryToolBar_ButtonClick" OnClientLoad="PayrollBatchSummaryToolBar_init">
        <Items>
            <wasp:WLPToolBarButton runat="server" Text="*Details" onclick="Open()" PostBack="false" CommandName="details" ResourceName="Details" OnPreRender="DetailsToolBar_PreRender"></wasp:WLPToolBarButton>
            <wasp:WLPToolBarButton runat="server" Text="*Add" ImageUrl="~/App_Themes/Default/Add.gif" onclick="Add()" PostBack="false" CommandName="add" ResourceName="Add" OnPreRender="AddToolBar_PreRender"></wasp:WLPToolBarButton>
            <wasp:WLPToolBarButton runat="server" Text="*Delete" ImageUrl="~\App_Themes\Default\Delete.gif" onclick="Delete()" PostBack="false" CommandName="delete" ResourceName="Delete" OnPreRender="DeleteToolBar_PreRender"></wasp:WLPToolBarButton>
            <wasp:WLPToolBarButton Text="*Export" CommandName="export" ResourceName="Export" OnPreRender="ExportToolBar_PreRender"></wasp:WLPToolBarButton>
        </Items>
    </wasp:WLPToolBar>

    <wasp:WLPGrid
        ID="PayrollBatchSummaryGrid"
        runat="server"
        GridLines="None"
        AutoGenerateColumns="false"
        AllowPaging="true"
        PageSize="100"
        AllowSorting="true"
        OnNeedDataSource="PayrollBatchSummaryGrid_NeedDataSource">

        <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="true">
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
            <ClientEvents OnRowClick="OnRowClick" OnRowDblClick="OnRowDblClick" OnGridCreated="onGridCreated" />         
        </ClientSettings>

        <MasterTableView
            ClientDataKeyNames="PayrollBatchId,PayrollProcessRunTypeCode,PayrollBatchStatusCode"
            AutoGenerateColumns="false"
            CommandItemDisplay="Top"
            DataKeyNames="Key">

            <CommandItemTemplate></CommandItemTemplate>
        
            <Columns>
                <wasp:GridBoundControl DataField="PayrollBatchId" LabelText="*PayrollBatchId" DataType="System.Int64" SortExpression="PayrollBatchId" UniqueName="PayrollBatchId" ResourceName="PayrollBatchId">
                    <HeaderStyle Width="70px" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="BatchSource" LabelText="*BatchSource" SortExpression="BatchSource" UniqueName="BatchSource" ResourceName="BatchSource">
                    <HeaderStyle Width="180px" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="Description" LabelText="*Description" SortExpression="Description" UniqueName="Description" ResourceName="Description">
                    <HeaderStyle Width="180px" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="PayrollBatchStatusCodeDescription" LabelText="*PayrollBatchStatusCode" SortExpression="PayrollBatchStatusCode" UniqueName="PayrollBatchStatusCode" ResourceName="PayrollBatchStatusCode">
                    <HeaderStyle Width="120px" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="PayrollProcessGroupCodeDescription" LabelText="*PayrollProcessGroupCode" SortExpression="PayrollProcessGroupCode" UniqueName="PayrollProcessGroupCode" ResourceName="PayrollProcessGroupCode">
                    <HeaderStyle Width="140px" />
                </wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="PayrollProcessRunTypeCodeDescription" LabelText="*PayrollProcessRunTypeCode" SortExpression="PayrollProcessRunTypeCode" UniqueName="PayrollProcessRunTypeCode" ResourceName="PayrollProcessRunTypeCode">
                    <HeaderStyle Width="100px" />
                </wasp:GridBoundControl>
                <wasp:GridCheckBoxControl DataField="ProcessedFlag" LabelText="*ProcessedFlag" SortExpression="ProcessedFlag" UniqueName="ProcessedFlag" ResourceName="ProcessedFlag">
                    <HeaderStyle Width="70px" />
                </wasp:GridCheckBoxControl>
                <wasp:GridNumericControl DataField="TotalHours" LabelText="*TotalHours" SortExpression="TotalHours" UniqueName="TotHours" ResourceName="TotHours">
                    <HeaderStyle Width="70px" />
                </wasp:GridNumericControl>
                <wasp:GridNumericControl DataField="TotalAmount" DataFormatString="{0:$###,##0.00}" LabelText="*TotalAmount" SortExpression="TotalAmount" UniqueName="TotAmount" ResourceName="TotAmount">
                    <HeaderStyle Width="120px" />
                </wasp:GridNumericControl>
            </Columns>

        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true" />

    </wasp:WLPGrid>
</asp:Panel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="PayrollBatchWindows"
    Width="1100"
    Height="700"
    PreserveClientState="false"
    ShowContentDuringLoad="false"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    Modal="true"
    DestroyOnClose="true"
    OnClientClose="onClientClose">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">
    <script type="text/javascript">
        var bEnabled = <%= EnabledByParent.ToString().ToLower() %>;
        var sPayProcessGrpCd = '<%= PayrollProcessControlPayProcessGrpCd %>';
        var sPayProcessTypeCd = '<%= PayrollProcessControlPayProcessRunTypeCd %>';
        var _payrollPeriodId = '<%= PayrollPeriodId %>';
    </script>
</telerik:RadScriptBlock>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var payrollBatchId;
        var detailsButton;
        var addButton;
        var deleteButton;
        var exportButton;
        var toolbar;
        var _masterTable;
        var _canDelete = false;
        var _canExport = false;

        function PayrollBatchSummaryToolBar_init(sender) {
            toolbar = sender;
            detailsButton = toolbar.findButtonByCommandName('details');
            deleteButton = toolbar.findButtonByCommandName('delete');
            addButton = toolbar.findButtonByCommandName('add');
            exportButton = toolbar.findButtonByCommandName('export');

            enableButtons();
        }

        function onGridCreated(sender, eventArgs) {
            var employeeSummaryGrid = $find('<%= PayrollBatchSummaryGrid.ClientID %>');
            _masterTable = employeeSummaryGrid.get_masterTableView();
            _canExport = _masterTable.get_dataItems() != null;

            var selectedRow = _masterTable.get_selectedItems();
            if (selectedRow.length == 1) {
                payrollBatchId = selectedRow[0].getDataKeyValue('PayrollBatchId');
                payrollProcessRunTypeCode = selectedRow[0].getDataKeyValue('PayrollProcessRunTypeCode');
                _canDelete = (selectedRow[0].getDataKeyValue('PayrollBatchStatusCode') == 'UA');
            }

            if (exportButton != null)
                exportButton.set_enabled(_canExport);
        }

        function enableButtons() {
            if (detailsButton != null)
                detailsButton.set_enabled(payrollBatchId != null);

            if (deleteButton != null)
                deleteButton.set_enabled(_canDelete);

            if (exportButton != null)
                exportButton.set_enabled(_canExport);
        }

        function OnRowClick(sender, eventArgs) {
            payrollBatchId = eventArgs.getDataKeyValue('PayrollBatchId');
            payrollProcessRunTypeCode = eventArgs.getDataKeyValue('PayrollProcessRunTypeCode');
            _canDelete = (eventArgs.getDataKeyValue('PayrollBatchStatusCode') == 'UA');

            enableButtons();
        }

        function OnRowDblClick(sender, eventArgs) {
            if (toolbar.findButtonByCommandName('details') != null)
                Open(sender);
        }

        function Open() {
            if (payrollBatchId != null) {
                openWindowWithManager('PayrollBatchWindows', String.format('/PayrollBatch/PayrollBatchTransaction/View/{0}/{1}/{2}', payrollBatchId, 'x', 'x'), false);
                return false;
            }
        }

        function Add() {
            if (bEnabled) {
                if (sPayProcessGrpCd == '') sPayProcessGrpCd = 'x';
                if (sPayProcessTypeCd == '') sPayProcessTypeCd = 'x';

                openWindowWithManager('PayrollBatchWindows', String.format('/PayrollBatch/PayrollBatchTransaction/Add/{0}/{1}/{2}', '0', sPayProcessGrpCd, sPayProcessTypeCd), false);
                return false;
            }
        }

        function Delete() {
            if (bEnabled && _canDelete) {
                openWindowWithManager('PayrollBatchWindows', String.format('/PayrollBatch/PayrollBatchTransaction/Delete/{0}/{1}/{2}', payrollBatchId, 'x', 'x'), false);
                return false;
            }
        }

        function onClientClose(sender, eventArgs) {
            var arg = sender.argument;
            if (arg != null && arg.closeWithChanges)
                __doPostBack('<%= this.ClientID %>', String.format('refreshBatch={0}', arg.batchId));
        }
    </script>
</telerik:RadScriptBlock>