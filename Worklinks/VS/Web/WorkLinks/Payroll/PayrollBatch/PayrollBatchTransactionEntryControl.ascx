﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayrollBatchTransactionEntryControl.ascx.cs" Inherits="WorkLinks.Payroll.PayrollBatch.PayrollBatchTransactionEntryControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Src="../PayrollBatch/EmployeeControl.ascx" TagName="EmployeeControl" TagPrefix="uc1" %>

<wasp:WLPGrid
    ID="PayrollTransactionSummaryGrid"
    runat="server"
    GridLines="None"
    AllowPaging="true"
    PagerStyle-AlwaysVisible="true"
    PageSize="100"
    AutoGenerateColumns="false"
    AutoInsertOnAdd="false"
    AutoAssignModifyProperties="true"
    OnItemDataBound="PayrollTransactionSummaryGrid_ItemDataBound"
    OnUpdateCommand="PayrollTransactionSummaryGrid_UpdateCommand"
    OnInsertCommand="PayrollTransactionSummaryGrid_InsertCommand"
    OnDeleteCommand="PayrollTransactionSummaryGrid_DeleteCommand"
    OnItemCommand="PayrollTransactionSummaryGrid_ItemCommand"
    Style="margin-top: 17px">

    <clientsettings enablepostbackonrowclick="false" allowcolumnsreorder="true" reordercolumnsonclient="true">
        <Selecting AllowRowSelect="false" />
        <Scrolling AllowScroll="false" UseStaticHeaders="true" />
    </clientsettings>

    <mastertableview commanditemdisplay="Top" datakeynames="Key">

        <SortExpressions>
            <telerik:GridSortExpression FieldName="EmployeeName" SortOrder="Ascending" />
        </SortExpressions>
    
        <CommandItemTemplate>
            <wasp:WLPToolBar ID="PayrollTransactionToolBar" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="PayrollTransactionToolBar_ButtonClick">
                <Items>
                    <wasp:WLPToolBarButton Text="*Add" ImageUrl="~/App_Themes/Default/add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag && AddButtonOverride %>' ResourceName="Add" />
                    <wasp:WLPToolBarButton Text="*Export" CommandName="export" ResourceName="Export" OnPreRender="ExportToolBar_PreRender" />
                </Items>
            </wasp:WLPToolBar>
        </CommandItemTemplate>

        <Columns>
            <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif" />
            <wasp:GridBoundControl DataField="EmployeeNumber" LabelText="**EmployeeNumber**" SortExpression="EmployeeNumber" UniqueName="EmployeeNumber" ResourceName="EmployeeNumber" />
            <wasp:GridBoundControl DataField="EmployeeName" LabelText="**Name**" SortExpression="EmployeeName" UniqueName="Name" ResourceName="Name" />
            <wasp:GridDateTimeControl DataField="TransactionDate" DataType="System.DateTime" LabelText="**TransactionDate**" SortExpression="TransactionDate" UniqueName="TransactionDate" ResourceName="TransactionDate" />
            <wasp:GridBoundControl DataField="PaycodeTypeDescription" LabelText="**PaycodeTypeCode**" UniqueName="PaycodeTypeCode" ResourceName="PayCodeTypeDescription" />
            <wasp:GridBoundControl DataField="PaycodeDescription" LabelText="**PaycodeCode**" UniqueName="PaycodeCode" ResourceName="PayCodeDescription" />
            <wasp:GridBoundControl DataField="Units" LabelText="**Units**" SortExpression="Units" UniqueName="Units" ResourceName="Units" />
            <wasp:GridBoundControl DataField="Rate" LabelText="**Rate**" SortExpression="Rate" UniqueName="Rate" ResourceName="Rate" />
            <wasp:GridBoundControl DataField="OrganizationUnitDescription" LabelText="**OrganizationUnit**" SortExpression="OrganizationUnitDescription" UniqueName="OrganizationUnitDescription" ResourceName="OrganizationUnit">
                <HeaderStyle Width="18%" />
            </wasp:GridBoundControl>
            <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~\App_Themes\Default\Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>" />
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table width="100%" OnKeyPress="return disableEnterKey(event)">
                    <tr>
                        <td>
                            <uc1:EmployeeControl ID="EmployeeControl1" OnClientEmployeeChanged="setRate(null,null);" runat="server" Value='<%# Bind("EmployeeId") %>' ResourceName="EmployeeControl1" ReadOnly='<%# !IsInsert %>' TabIndex="020" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:DateControl ID="TransactionDate" HeaderText="Transaction Date" runat="server" ResourceName="TransactionDate" Value='<%# Bind("TransactionDate") %>' ReadOnly="false" TabIndex="030" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:ComboBoxControl ID="PayCodeTypeDescription" OnClientSelectedIndexChanged="loadPaycodeItems" ClientIDMode="Static" text="PayCodeType" runat="server" Type="PaycodeTypeCode" OnDataBinding="PaycodeTypeDesc_NeedDataSource" OnDataBound="PaycodeTypeDesc_DataBound" AutoPostBack="false" ResourceName="PayCodeTypeDescription" Value='<%# Bind("PaycodeTypeCode") %>' TabIndex="040" />
                        </td>
                        <td>
                            <wasp:ComboBoxControl ID="PayCode" ClientIDMode="Static" AutoPostBack="false" OnSelectedIndexChanged="PayCode_SelectedIndexChanged" OnClientSelectedIndexChanged="setRate" text="PayCode" runat="server" Type="PaycodeCode" ResourceName="PayCodeDescription" Value='<%# Bind("PaycodeCode") %>' ReadOnly="false" TabIndex="050" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:NumericControl ID="Units" ClientIDMode="Static" DecimalDigits="2" HeaderText="Units" runat="server" ResourceName="Units" Value='<%# Bind("Units") %>' ReadOnly="false" TabIndex="060" onkeyup="updateEIHours()" />
                        </td>
                        <td>
                            <wasp:NumericControl ID="Rate" ClientIDMode="Static" DecimalDigits="4" HeaderText="Rate" runat="server" ResourceName="Rate" Value='<%# Bind("Rate") %>' ReadOnly="false" TabIndex="070" />
                            <asp:HiddenField ID="HiddenBaseRate" ClientIDMode="Static" runat="server" Value='<%# Bind("BaseSalaryRate") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <wasp:NumericControl ID="RetroactiveNumberOfPayPeriods" Visible="false" Mandatory="false" ClientIDMode="Static" DecimalDigits="0" HeaderText="RetroactiveNumberOfPayPeriods" runat="server" ResourceName="RetroactiveNumberOfPayPeriods" Value='<%# Bind("RetroactiveNumberOfPayPeriods") %>' ReadOnly="false" TabIndex="080" />
                        </td>
                        <td>
                            <div id="DivShowEmploymentInsuranceHour">
                                <wasp:CheckBoxControl ID="ckShowEIHours" runat="server"  ResourceName="ShowEIHours" LabelText="*Override EI Hours" ReadOnly="false" onchange="ckShowEIHours(this)"  TabIndex="089" AutoPostBack="false"/>
                                <wasp:NumericControl ID="EmploymentInsuranceHour" ClientIDMode="Static" DecimalDigits="2" HeaderText="EmploymentInsuranceHour" runat="server" ResourceName="EmploymentInsuranceHour" Value='<%# Bind("EmploymentInsuranceHour") %>'  ReadOnly="false" TabIndex="090" onchange="validateEmploymentInsuranceHour(this)"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                            <td>
                                <span id="FedTaxOverrideSpan">
                                    <wasp:NumericControl ID="FederalTaxAmount" ClientIDMode="Static" DecimalDigits="2" HeaderText="FedTax" runat="server" ResourceName="FederalTaxAmount" Value='<%# Bind("FederalTaxAmount") %>' ReadOnly="false" TabIndex="090"/>
                                </span>
                            </td>
                            <td>
                                <span id="ProvTaxOverrideSpan">
                                    <wasp:NumericControl ID="ProvincialTaxAmount" ClientIDMode="Static" DecimalDigits="2" HeaderText="ProvTax" runat="server" ResourceName="ProvincialTaxAmount" Value='<%# Bind("ProvincialTaxAmount") %>' ReadOnly="false"  TabIndex="100"/>
                                </span>
                            </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form_control">
                                <span class="label" id="StartTimeLabelSpan">
                                    <wasp:WLPLabel ID="StartTimeLabel" runat="server" Text="StartTime" ResourceName="StartTimeLabel" />
                                </span>
                                <span class="field" id="StartTimeSpan">
                                    <telerik:RadTimePicker ID="StartTime" runat="server" ClientIDMode="AutoID" />
                                </span>
                            </div>
                        </td>
                        <td>
                            <div class="form_control">
                                <span class="label" id="EndTimeLabelSpan">
                                    <wasp:WLPLabel ID="EndTimeLabel" runat="server" Text="EndTime" ResourceName="EndTimeLabel" />
                                </span>
                                <span class="field" id="EndTimeSpan">
                                    <telerik:RadTimePicker ID="EndTime" runat="server" ClientIDMode="AutoID" />
                                </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <wasp:WLPButton id="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' OnClientClicking="PaycodeMaxWarning" ResourceName="Update" />
                                        <wasp:WLPButton id="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsInsert %>' OnClientClicking="PaycodeMaxWarning" ResourceName="Insert" />
                                    </td>
                                    <td>
                                        <wasp:WLPButton id="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" text="Cancel" runat="server" CommandName="cancel" CausesValidation="false" ResourceName="Cancel"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>

    </mastertableview>

    <headercontextmenu enableautoscroll="true" />

</wasp:WLPGrid>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js" type="text/javascript"></script>

<style type="text/css">
    input[type="text"][disabled] {
        background-color: #e5e5e5 !important;
        color: black !important;
    }

    input[type=text] {
        color: black !important;
    }
</style>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        //disable enter key from submitting form
        function disableEnterKey(e) {
            var key;

            if (window.event)
                key = window.event.keyCode; //IE
            else
                key = e.which; //firefox      

            return (key != 13);
        }

        function loadPaycodeItems(source, args) {
            var payCodeTypeControl = document.getElementById(document.getElementById('PayCodeTypeDescription').attributes['fieldClientId'].value);
            var payCodeControl = document.getElementById(document.getElementById('PayCode').attributes['fieldClientId'].value).control;

            $.ajax
            ({
                type: "POST",
                url: getUrl("/Payroll/PayrollBatch/PayrollBatchTransactionPage.aspx/GetPaycodesByType"),
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: "{'codePaycodeType': '" + payCodeTypeControl.control._value + "'}",
                success: function (msg) {
                    payCodeControl.clearItems();
                    payCodeControl.clearSelection();
                    for (var i = 0; i < msg.d.length; i++) {
                        if (msg.d[i].ActiveFlag)
                            AddNewItem(msg.d[i], payCodeControl);
                    }
                }
            });
        }

        function AddNewItem(code, combo) {
            combo.set_closeDropDownOnBlur(false);
            var comboItem = new Telerik.Web.UI.RadComboBoxItem();
            comboItem.set_text(code.Description);
            comboItem.set_value(code.Key);
            combo.trackChanges();
            combo.get_items().add(comboItem);
            combo.commitChanges();
        }

        function showHideOverrideFedTax(type) {
            var isAdjustment = '<%= IsAdjustmentBatchType.ToString().ToLower() %>';
            var fedTaxValidator = document.getElementById($get('FederalTaxAmount').attributes['validatorClientId'].value);
            var overrideFedTax = $get('FedTaxOverrideSpan');

            if (type == 'show' && isAdjustment == 'false') {
                overrideFedTax.style.display = 'block';

                if (fedTaxValidator != null)
                    ValidatorEnable(fedTaxValidator, true);
            }
            else {
                overrideFedTax.style.display = 'none';

                if (fedTaxValidator != null)
                    ValidatorEnable(fedTaxValidator, false);
            }
        }

        function showHideOverrideProvTax(type) {
            var isAdjustment = '<%= IsAdjustmentBatchType.ToString().ToLower() %>';
            var provTaxValidator = document.getElementById($get('ProvincialTaxAmount').attributes['validatorClientId'].value);
            var overrideProvTax = $get('ProvTaxOverrideSpan');

            if (type == 'show' && isAdjustment == 'false') {
                overrideProvTax.style.display = 'block';

                if (provTaxValidator != null)
                    ValidatorEnable(provTaxValidator, true);
            }
            else {
                overrideProvTax.style.display = 'none';

                if (provTaxValidator != null)
                    ValidatorEnable(provTaxValidator, false);
            }
        }

        function showEmploymentInsuranceHour(show) {
            var divShowEmploymentInsuranceHour = $get('DivShowEmploymentInsuranceHour');
            if (show == 'show')
                divShowEmploymentInsuranceHour.style.display = 'block';
            else
                divShowEmploymentInsuranceHour.style.display = 'none';
        }

        function validateEmploymentInsuranceHour(element) {
            if (document.getElementById(element.attributes['fieldClientId'].value).value == '')
                document.getElementById(element.attributes['fieldClientId'].value).value = 0;
        }

        function showideStartEndTimes(type) {
            var startTimeLabel = $get('StartTimeLabelSpan');
            var startTimeControl = $get('StartTimeSpan');

            var endTimeLabel = $get('EndTimeLabelSpan');
            var endTimeControl = $get('EndTimeSpan');

            if (type == 'show') {
                startTimeLabel.style.display = 'inline';
                startTimeControl.style.display = 'inline';
                endTimeLabel.style.display = 'inline';
                endTimeControl.style.display = 'inline';
            }
            else {
                startTimeLabel.style.display = 'none';
                startTimeControl.style.display = 'none';
                endTimeLabel.style.display = 'none';
                endTimeControl.style.display = 'none';
            }
        }

        function setRate(source, args) {
            var payCodeControl = document.getElementById(document.getElementById('PayCode').attributes['fieldClientId'].value);
            var rateControl = document.getElementById(document.getElementById('Rate').attributes['fieldClientId'].value);
            var hiddenEmployeeIdControl = document.getElementById('HiddenEmployeeId');
            var payCode = payCodeControl.control._value;
            var hiddenBaseRateControl = document.getElementById('HiddenBaseRate');
            var isAdjustment = '<%= IsAdjustmentBatchType.ToString().ToLower() %>';

            if (hiddenEmployeeIdControl.value > 0 && payCode != null && payCode != '') {
                $.ajax
                ({
                    type: "POST",
                    url: getUrl("/Payroll/PayrollBatch/PayrollBatchTransactionPage.aspx/GetRate"),
                    data: "{}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: "{'employeeId': '" + hiddenEmployeeIdControl.value + "' ,'payCode': '" + payCode + "' ,'isAdjustment': '" + isAdjustment + "'}",
                    success: function (msg) {
                        if (msg.d.Rate >= 0) {
                            rateControl.control.set_value(msg.d.Rate);
                            hiddenBaseRateControl.value = msg.d.BaseSalaryRate;
                        }
                        else
                            hiddenBaseRateControl.value = null;

                        //show or hide Federal Tax Override fields
                        showHideOverrideFedTax((msg.d.OverrideFedTax) ? 'show' : 'hide');

                        //show or hide Provincial Tax Override fields
                        showHideOverrideProvTax((msg.d.OverrideProvTax) ? 'show' : 'hide');

                        //show or hide start time and end date/time
                        showideStartEndTimes((msg.d.StartTime) ? 'show' : 'hide');
                        //show or hide include employment insurance hours
                        showEmploymentInsuranceHour((msg.d.IncludeEmploymentInsuranceHoursFlag) ? 'show' : 'hide');
                    }
                });
            }
            else {
                //hide Federal and Provincial Tax Override fields, start and end date/time
                showHideOverrideFedTax('hide');
                showHideOverrideProvTax('hide');
                showEmploymentInsuranceHour('hide');
                showideStartEndTimes('hide');
                showEmploymentInsuranceHour('hide');
            }

            return false;
        }

        function PaycodeMaxWarning(sender, args) {
            var rateControl = document.getElementById(document.getElementById('Rate').attributes['fieldClientId'].value);
            var rate = rateControl.control._value;
            var yearlyMaximum = '<%= YearlyMaximumAmount %>';

                    //if the server side variable is equal to null, then yearlyMaximum on the client side will equal ''.  This means (rate > yearlyMaximum) will always be true.  
                    //avoid comparison if yearlyMaximum doesnt have a value
                    if (yearlyMaximum != '') {
                        if (rate > yearlyMaximum) {
                            var message = '<%= PaycodeMaxWarning %>';
                    if (!confirm(message)) {
                        args.set_cancel(true);
                    }
                }
            }
        }

        function ckShowEIHours(ckBox) {
            var employmentInsuranceHourControl = document.getElementById(document.getElementById('EmploymentInsuranceHour').attributes['fieldClientId'].value);

            if (ckBox.checked)
                employmentInsuranceHourControl.disabled = false;
            else {
                employmentInsuranceHourControl.disabled = true;
                updateEIHours();
            }
        }

        function updateEIHours() {
            var employmentInsuranceHourControl = document.getElementById(document.getElementById('EmploymentInsuranceHour').attributes['fieldClientId'].value);
            var UnitsHourControl = document.getElementById(document.getElementById('Units').attributes['fieldClientId'].value);
            employmentInsuranceHourControl.value = UnitsHourControl.value;
        }
    </script>
</telerik:RadScriptBlock>