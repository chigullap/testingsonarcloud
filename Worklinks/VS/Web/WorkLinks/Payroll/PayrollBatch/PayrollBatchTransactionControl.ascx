﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayrollBatchTransactionControl.ascx.cs" Inherits="WorkLinks.Payroll.PayrollBatch.PayrollBatchTransactionControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%--<%@ Register src="PayrollBatchGenericTimeEntryControl.ascx" tagname="PayrollBatchGenericTimeEntryControl" tagprefix="uc1" %>--%>
<%@ Register src="PayrollBatchTransactionEntryControl.ascx" tagname="PayrollBatchTransactionEntryControl" tagprefix="uc2" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy11" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="AddPayrollBatchToolBar">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="AddPayrollBatchToolBar" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<wasp:WLPFormView
    ID="PayrollBatchTransactionView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key"
    OnNeedDataSource="PayrollBatchTransactionView_NeedDataSource">

    <ItemTemplate>
        <wasp:WLPToolBar ID="AddPayrollBatchToolBar" OnButtonClick="AddPayrollBatchToolBar_ButtonClick" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="toolBarClick">
            <Items>
                <wasp:WLPToolBarButton Text="Approve" visible='<%# IsApproveVisible %>' CommandName="Approve" ResourceName="Approve" />
                <wasp:WLPToolBarButton Text="UnApprove" visible='<%# IsUnApproveVisible %>' CommandName="UnApprove" ResourceName="UnApprove" />
            </Items>
        </wasp:WLPToolBar>

        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PayrollBatchId" runat="server" LabelText="Batch Id" ResourceName="PayrollBatchId" Value='<%# Eval("PayrollBatchId") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="BatchSource" runat="server" LabelText="Batch Source" ResourceName="BatchSource" Value='<%# Eval("BatchSource") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="Description" runat="server" LabelText="Description" ResourceName="Description" Value='<%# Eval("Description") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="PayrollProcessGroupCode" text="Process Group" runat="server" Type="PayrollProcessGroupCode" OnDataBinding="Code_NeedDataSource" ResourceName="PayrollProcessGroupCode" Value='<%# Bind("PayrollProcessGroupCode") %>' ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PayrollProcessRunTypeCode" runat="server" Type="PayrollProcessRunTypeCode" LabelText="Run Type" OnDataBinding="Code_NeedDataSource" ResourceName="PayrollProcessRunTypeCode" Value='<%# Bind("PayrollProcessRunTypeCode") %>' ReadOnly="true"></wasp:ComboBoxControl>
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar ID="PayrollBatchToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="toolBarClick">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsFormInsertMode %>' CommandName="insert" ResourceName="Insert" />
                <wasp:WLPToolBarButton Text="Cancel" onclick="Close()" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="Close" CausesValidation="false" ResourceName="Cancel" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PayrollBatchId" runat="server" LabelText="Batch Id" ResourceName="PayrollBatchId" Value='<%# Eval("PayrollBatchId") %>' ReadOnly="true" TabIndex="010"/>
                    </td>
                    <td>
                        <wasp:TextBoxControl ID="BatchSource" runat="server" LabelText="Batch Source" ResourceName="BatchSource" Value='<%# Bind("BatchSource") %>' ReadOnly="false" TabIndex="020"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="Description" runat="server" LabelText="Comment" ResourceName="Description" Value='<%# Bind("Description") %>' ReadOnly="false" Mandatory="true" TabIndex="030"/>
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="PayrollProcessGroupCode" text="Process Group:" runat="server" Type="PayrollProcessGroupCode" OnDataBinding="Code_NeedDataSource" ResourceName="PayrollProcessGroupCode" Value='<%# Bind("PayrollProcessGroupCode") %>' Mandatory="true" TabIndex="040"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PayrollProcessRunTypeCode" runat="server" Type="PayrollProcessRunTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="PayrollProcessRunTypeCode" Value='<%# Bind("PayrollProcessRunTypeCode") %>' ReadOnly="false" Mandatory="true" TabIndex="060"></wasp:ComboBoxControl>
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>

</wasp:WLPFormView>

<%--<uc1:PayrollBatchGenericTimeEntryControl ID="PayrollBatchGenericTimeEntryControl" runat="server" />--%>

<!-- use style tag instead of visible otherwise button is not rendered and cannot be located in Javascipt-->
<asp:button ID="btnDelete2" runat="server" OnClick="btnDelete_Click" style="display: none;" />

<uc2:PayrollBatchTransactionEntryControl ID="PayrollBatchTransactionEntryControl" runat="server" />

<telerik:RadScriptBlock ID="RadScriptBlock11" runat="server">
    <script type="text/javascript">
        var payrollBatchId = <%=PayrollBatchId %>;
        var _payProcessGrpCd = '<%= PayProcessGrpCd %>';

        function getRadWindow() {
            var popup = null;

            if (window.radWindow)
                popup = window.radWindow;
            else if (window.frameElement.radWindow)
                popup = window.frameElement.radWindow;

            return popup;
        }

        function pageLoad() {
            if (_payProcessGrpCd == 'refresh')
                processClick('insert');
        }

        function toolBarClick(sender, args) {
            var commandName = args.get_item().get_commandName();
            processClick(commandName);
        }

        function buttonClick(sender, args) {
            var commandName = args.get_commandName();
            processClick(commandName);
        }

        function processClick(commandName) {
            if (commandName == 'Approve' || commandName == 'UnApprove' || commandName.toLowerCase() == 'insert' || commandName == 'PerformInsert' ||
                commandName == 'Update' || commandName == 'Delete') {
                var arg = new Object;

                if (payrollBatchId != null  && payrollBatchId >0) {
                    arg.closeWithChanges = true;
                    arg.batchId = payrollBatchId;
                    getRadWindow().argument=arg;
                }
            }
        }

        function Close(button, args) {
            var popup = getRadWindow();
            setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
        }

        function closeAndReturn(batchId) {
            //Close the window with parms
            setTimeout(function () { getRadWindow().close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
        }

        //give a delete confirmation
        function confirmation() {
            var response = confirm("Are you sure you want to delete?");

            if (response == true) {
                var btn = document.getElementById('<%= btnDelete2.ClientID %>');
                processClick('Delete');
                btn.click();
            }
            else
                closeAndReturn(null);
        }
    </script>
</telerik:RadScriptBlock>