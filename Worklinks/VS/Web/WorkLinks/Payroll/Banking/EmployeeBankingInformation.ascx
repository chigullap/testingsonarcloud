﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeBankingInformation.ascx.cs" Inherits="WorkLinks.Payroll.Banking.EmployeeBankingInformation" %>
<%@ Register assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<wasp:WLPAjaxManagerProxy ID="WLPAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="BankingPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="BankingPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</wasp:WLPAjaxManagerProxy>

<asp:Panel ID="BankingPanel" runat="server">
    <wasp:WLPGrid 
        ID="EmployeeBankingInfoGrid"
        runat="server"
        GridLines="None"
        AutoGenerateColumns="false"
        AutoInsertOnAdd="false"
        OnDeleteCommand="EmployeeBankingInfoGrid_DeleteCommand"
        OnInsertCommand="EmployeeBankingInfoGrid_InsertCommand"
        OnUpdateCommand="EmployeeBankingInfoGrid_UpdateCommand"
        OnItemCommand="EmployeeBankingInfoGrid_ItemCommand"
        OnItemDataBound="EmployeeBankingInfoGrid_ItemDataBound"
        AutoAssignModifyProperties="true">

        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
            <Selecting AllowRowSelect="false" />
            <ClientEvents OnGridCreated="GridCreated" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="CodeEmployeeBankingSequenceCode" SortOrder="Ascending" />
            </SortExpressions>

            <CommandItemTemplate>
                <wasp:WLPToolBar ID="EmployeeSummaryToolBar" runat="server" Width="100%" AutoPostBack="true" style="margin-bottom: 0">
                    <Items>
                        <wasp:WLPToolBarButton Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" CommandName="InitInsert" Visible='<%# IsViewMode && AddFlag %>' ResourceName="Add"></wasp:WLPToolBarButton>
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridEditCommandControl UniqueName="editButton" ButtonType="ImageButton" EditImageUrl="~/App_Themes/Default/Edit.gif"></wasp:GridEditCommandControl>
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="**Sequence**" DataField="CodeEmployeeBankingSequenceCode" Type="EmployeeBankingSequenceNumberTypeCode" UniqueName="CodeEmployeeBankingSequenceCode" ResourceName="CodeEmployeeBankingSequenceCode"></wasp:GridKeyValueControl>
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="**Country**" DataField="BankingCountryCode" Type="BankingCountryCode" UniqueName="BankingCountryCode" ResourceName="BankingCountryCode"></wasp:GridKeyValueControl>
                <wasp:GridKeyValueControl OnNeedDataSource="EmployeeBankingCode_NeedDataSource" LabelText="**Bank**" DataField="EmployeeBankingCode" Type="EmployeeBankingTypeCode" UniqueName="EmployeeBankingCode" ResourceName="EmployeeBankingCode"></wasp:GridKeyValueControl>
                <wasp:GridBoundControl DataField="transit_number" LabelText="**Transit**" SortExpression="transit_number" UniqueName="transit_number" ResourceName="transit_number"></wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="account_number" LabelText="**Account**" SortExpression="account_number" UniqueName="account_number" ResourceName="account_number"></wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="percentage" DataFormatString="{0:###.00}" LabelText="**Percentage**" SortExpression="percentage" UniqueName="percentage" ResourceName="percentage"></wasp:GridBoundControl>
                <wasp:GridBoundControl DataField="Amount" DataFormatString="{0:$#####.00}" LabelText="**Amount**" SortExpression="Amount" UniqueName="Amount" ResourceName="Amount"></wasp:GridBoundControl>
                <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>"></wasp:GridButtonControl>
            </Columns>

            <EditFormSettings EditFormType="Template">
                <FormTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="EmployeeBankingSequenceCode" OnClientSelectedIndexChanged="checkForNET" runat="server" Type="EmployeeBankingSequenceNumberTypeCode" OnDataBinding="CodeSeq_NeedDataSource" ResourceName="CodeEmployeeBankingSequenceCode" Value='<%# Bind("CodeEmployeeBankingSequenceCode") %>' Mandatory="true" ReadOnly='<%# !IsInsert %>' TabIndex="010"></wasp:ComboBoxControl>
                            </td>
                            <td>
                                <wasp:TextBoxControl ID="Restriction" runat="server" ResourceName="CodeEmployeeBankingRestrictionCode" Value='<%# "Maximum" %>' ReadOnly="true" Disabled="true" TabIndex="080"></wasp:TextBoxControl>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:ComboBoxControl ID="BankingCountryCode" runat="server" Type="BankingCountryCode" AutoSelect="true" AutoPostback="true" OnDataBinding="BankingCountryCode_NeedDataSource" OnSelectedIndexChanged="BankingCountryCode_SelectedIndexChanged" ResourceName="BankingCountryCode" Value='<%# Bind("BankingCountryCode") %>' ReadOnly='<%# !IsInsert %>' Mandatory="true" TabIndex="020"></wasp:ComboBoxControl>
                            </td>
                            <td>
                                <wasp:ComboBoxControl ID="EmployeeBankingCode" runat="server" Type="EmployeeBankingTypeCode" ResourceName="EmployeeBankingCode" Value='<%# Bind("EmployeeBankingCode") %>' Mandatory="true" TabIndex="030"></wasp:ComboBoxControl>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:WLPRegularExpressionValidator ID="val1Transit" ResourceName="val1Transit" Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="Transit" ValidationExpression="^\d{5}$"></wasp:WLPRegularExpressionValidator>
                                <wasp:TextBoxControl ID="Transit" MaxLength="5" runat="server" ResourceName="transit_number" Value='<%# Bind("Transit_number") %>' ReadOnly="false" Mandatory="true" TabIndex="040"></wasp:TextBoxControl>
                            </td>
                            <td>
                                <wasp:WLPRegularExpressionValidator ID="val1AccountNumber" ResourceName="val1AccountNumber" Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="AccountNumber" ValidationExpression="^[0-9]{5,17}$"></wasp:WLPRegularExpressionValidator>
                                <wasp:TextBoxControl ID="AccountNumber" MaxLength="17" runat="server" ResourceName="account_number" Value='<%# Bind("Account_number") %>' ReadOnly="false" Mandatory="true" TabIndex="050"></wasp:TextBoxControl>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:WLPRegularExpressionValidator ID="PercentageValidate" ResourceName="PercentageValidate" Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="Percentage:Field" ValidationExpression="^\d{1,3}(\.\d{0,2})?$" ErrorMessage='<%$ Resources:ErrorMessages, InvalidFormat %>'></wasp:WLPRegularExpressionValidator>
                                <wasp:NumericControl ID="Percentage" onkeyup="disableAmount()" DecimalDigits="2" runat="server" ResourceName="percentage" Value='<%# Bind("Percentage") %>' ReadOnly="false" TabIndex="060"></wasp:NumericControl>
                                <asp:RangeValidator CultureInvariantValues="true" ID="PercentageValidator" Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="Percentage" ResourceName="PercentageValidator" Text='<%$ Resources:ErrorMessages, PercentRange %>' EnableClientScript="false" MinimumValue="0" MaximumValue="100" Type="Double"></asp:RangeValidator>
                            </td>
                            <td>
                                 <wasp:ComboBoxControl ID="CodeBankAccountTypeCode" runat="server" Type="CodeBankAccountTypeCode" ResourceName="CodeBankAccountTypeCode" OnDataBinding="Code_NeedDataSource" Value='<%# Bind("CodeBankAccountTypeCode") %>' TabIndex="65"></wasp:ComboBoxControl>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <wasp:WLPRegularExpressionValidator ID="val1AbaNumber" ResourceName="val1AbaNumber" Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="AbaNumber" ValidationExpression="^[0-9]{9}$"></wasp:WLPRegularExpressionValidator>
                                <wasp:TextBoxControl ID="AbaNumber" MaxLength="9" runat="server" ResourceName="AbaNumber" Value='<%# Bind("AbaNumber") %>' ReadOnly="false"  TabIndex="066"></wasp:TextBoxControl>
                            </td>
                            <td>
                                <wasp:WLPRegularExpressionValidator ID="AmountValidate" ResourceName="AmountValidate" Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="Amount:Field" ValidationExpression="^\d{0,11}(\.\d{0,2})?$" ErrorMessage='<%$ Resources:ErrorMessages, InvalidFormat %>'></wasp:WLPRegularExpressionValidator>
                                <wasp:NumericControl ID="Amount" onkeyup="disablePercent()" DecimalDigits="2" runat="server" ResourceName="Amount" Value='<%# Bind("Amount") %>' ReadOnly="false" TabIndex="070"></wasp:NumericControl>
                                <asp:RangeValidator CultureInvariantValues="true" ID="AmountValidator" Display="Dynamic" ForeColor="Red" runat="server" ControlToValidate="Amount" ResourceName="AmountValidator" Text='<%$ Resources:ErrorMessages, AmountRange %>' EnableClientScript="false" MinimumValue="0" MaximumValue="10000" Type="Double"></asp:RangeValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:CustomValidator ID="PercentageOrAmountValidator" Display="Dynamic" runat="server" ForeColor="Red" ErrorMessage='<%$ Resources:ErrorMessages, AmountOrPercentageRequired %>' ClientValidationFunction="checkForPercentageOrAmount"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <wasp:WLPButton ID="btnUpdate" Icon-PrimaryIconUrl="~/App_Themes/Default/update.gif" Text="Update" runat="server" CommandName="Update" Visible='<%# IsUpdate %>' ResourceName="Update"></wasp:WLPButton>
                                            <wasp:WLPButton ID="btnInsert" Icon-PrimaryIconUrl="~/App_Themes/Default/add.gif" Text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# IsInsert %>' ResourceName="Insert"></wasp:WLPButton>
                                        </td>
                                        <td>
                                            <wasp:WLPButton ID="btnCancel" Icon-PrimaryIconUrl="~/App_Themes/Default/cancel.gif" Text="Cancel" runat="server" CommandName="Cancel" CausesValidation="false" ResourceName="Cancel"></wasp:WLPButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>

        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true"></HeaderContextMenu>

    </wasp:WLPGrid>
</asp:Panel>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        //if an amount is entered, disable the percent textbox and vise versa
        function disableAmount(sender) {
            var masterTable = $find("<%= EmployeeBankingInfoGrid.ClientID %>").get_masterTableView();
            var percentTxb = $telerik.$(masterTable.get_element()).find('input[id*="Percentage"]')[0];
            var amountTxb = $telerik.$(masterTable.get_element()).find('input[id*="Amount"]')[0];
            var percentVal = percentTxb.value;

            if (percentVal.length > 0) {
                amountTxb.value = "";
                amountTxb.readOnly = true;
            }
            else
                amountTxb.readOnly = false;
        }

        function disablePercent(sender) {
            var masterTable = $find("<%= EmployeeBankingInfoGrid.ClientID %>").get_masterTableView();
            var percentTxb = $telerik.$(masterTable.get_element()).find('input[id*="Percentage"]')[0];
            var amountTxb = $telerik.$(masterTable.get_element()).find('input[id*="Amount"]')[0];
            var amountVal = amountTxb.value;

            if (amountVal.length > 0) {
                percentTxb.value = "";
                percentTxb.readOnly = true;
            }
            else
                percentTxb.readOnly = false;
        }

        function checkForNET(sender, eventArgs) {
            var masterTable = $find("<%= EmployeeBankingInfoGrid.ClientID %>").get_masterTableView();
            var sequenceCombo = $telerik.$(masterTable.get_element()).find('input[id*="Seq"]')[0];
            var sequenceValue = sequenceCombo.value;
            var percentTxb = $telerik.$(masterTable.get_element()).find('input[id*="Percentage"]')[0];
            var amountTxb = $telerik.$(masterTable.get_element()).find('input[id*="Amount"]')[0];

            if (sequenceValue == "NET") {
                //disable percentage and default to 100
                percentTxb.value = "100";
                percentTxb.disabled = true;

                //clear and disable amount
                amountTxb.value = "";
                amountTxb.disabled = true;
            }
            else {
                //re-enable percentage and amount textboxes
                percentTxb.disabled = false;
                amountTxb.disabled = false;
            }
        }

        function checkForPercentageOrAmount(sender, e) {
            e.IsValid = false;

            var masterTable = $find("<%= EmployeeBankingInfoGrid.ClientID %>").get_masterTableView();
            var sequenceCombo = $telerik.$(masterTable.get_element()).find('input[id*="Seq"]')[0];
            var sequenceValue = sequenceCombo.value;

            if (sequenceCombo.value != "NET") {
                var percentTxb = $telerik.$(masterTable.get_element()).find('input[id*="Percentage"]')[0];
                var amountTxb = $telerik.$(masterTable.get_element()).find('input[id*="Amount"]')[0];
                var amountVal = amountTxb.value;
                var percentVal = percentTxb.value;

                if (percentVal > 0 || percentVal.length > 0 || amountVal > 0 || amountVal.length > 0)
                    e.IsValid = true;
            }

            if (sequenceValue == "NET") 
            {
                var percentTxb = $telerik.$(masterTable.get_element()).find('input[id*="Percentage"]')[0];
                if (percentTxb.value > 0)
                    e.IsValid = true;
            }
        }

        function GridCreated(sender, eventArgs) {
            if (<%= IsUpdate.ToString().ToLower() %>)
                checkForNET(sender, eventArgs);

            //insert mode check for NET and disable controls
            var masterTable = $find("<%= EmployeeBankingInfoGrid.ClientID %>").get_masterTableView();
            var sequenceCombo = $telerik.$(masterTable.get_element()).find('input[id*="Seq"]')[0];
            var sequenceValue = "";

            if (sequenceCombo)
                sequenceValue = sequenceCombo.value;

            if (<%= IsInsert.ToString().ToLower() %> || sequenceValue == "NET")
                checkForNET(sender, eventArgs);
        }
    </script>
</telerik:RadScriptBlock>