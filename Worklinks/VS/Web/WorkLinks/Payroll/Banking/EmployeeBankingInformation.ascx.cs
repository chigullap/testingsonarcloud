﻿using System;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Wizard;

namespace WorkLinks.Payroll.Banking
{
    public partial class EmployeeBankingInformation : WizardUserControl
    {
        #region fields
        protected bool _updateExterallyControlled = false;
        #endregion

        #region properties
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        private String LastName { get { return Page.RouteData.Values["lastName"].ToString(); } }
        private String FirstName { get { return Page.RouteData.Values["firstName"].ToString(); } }
        public bool IsViewMode { get { return EmployeeBankingInfoGrid.IsViewMode; } }
        public bool IsUpdate { get { return EmployeeBankingInfoGrid.IsEditMode; } }
        public bool IsInsert { get { return EmployeeBankingInfoGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.EmployeeBanking.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeBanking.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.EmployeeBanking.DeleteFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            //does the user have access rights?
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EmployeeBanking.ViewFlag);

            WireEvents();

            if (!IsDataExternallyLoaded)
                Page.Title = String.Format(Page.Title, LastName + ", " + FirstName);

            if (!IsPostBack)
            {
                //does the user have rights to see this employee?
                if (!IsDataExternallyLoaded)
                    SetAuthorized(Common.ServiceWrapper.HumanResourcesClient.GetEmployee(new EmployeeCriteria() { EmployeeId = EmployeeId }).Count > 0);

                LoadEmployeeBankingInformation(EmployeeId);
                Initialize();
            }
        }
        protected void WireEvents()
        {
            EmployeeBankingInfoGrid.NeedDataSource += new GridNeedDataSourceEventHandler(EmployeeBankingInfoGrid_NeedDataSource);
        }
        protected void LoadEmployeeBankingInformation(long employeeId)
        {
            if (!IsDataExternallyLoaded)
                DataItemCollection = EmployeeBankingCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetEmployeeBanking(employeeId));
            else
                EmployeeBankingInfoGrid.Rebind();
        }
        protected void Initialize()
        {
            //find the EmployeeBankingInfoGrid
            WLPGrid grid = (WLPGrid)FindControl("EmployeeBankingInfoGrid");

            //hide the edit images in the rows
            grid.MasterTableView.GetColumn("editButton").Visible = UpdateFlag;
        }
        #endregion

        #region event handlers
        protected void EmployeeBankingInfoGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "cancel":
                case "performinsert":
                case "update":
                    OnItemChangingComplete(null);
                    break;
                case "edit":
                case "initinsert":
                    OnItemChanging(null);
                    break;
            }
        }
        protected void EmployeeBankingInfoGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;

                //hide the delete image in the rows
                if (EmployeeBankingInfoGrid.DataSource.Count > 1 && ((EmployeeBanking)item.DataItem).CodeEmployeeBankingSequenceCode == "99") //99 = NET
                    item["deleteButton"].Controls[0].Visible = false;
                else
                    item["deleteButton"].Controls[0].Visible = DeleteFlag;
            }

            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                if (IsDataExternallyLoaded && TemplateWizardFlag)
                {
                    if ((WLPButton)e.Item.FindControl("btnUpdate") != null)
                        ((WLPButton)e.Item.FindControl("btnUpdate")).CausesValidation = false;

                    if ((WLPButton)e.Item.FindControl("btnInsert") != null)
                        ((WLPButton)e.Item.FindControl("btnInsert")).CausesValidation = false;
                }

                if (e.Item.OwnerTableView.IsItemInserted)
                {
                    GridEditFormInsertItem insertItem = (GridEditFormInsertItem)e.Item;

                    //populate the EmployeeBankingCode combobox if there is only one BankingCountryCode record
                    ComboBoxControl bankingCountryCode = (ComboBoxControl)insertItem.FindControl("BankingCountryCode");
                    if (bankingCountryCode.Items.Count == 2)
                        BindEmployeeBankingCodeCombo(bankingCountryCode);

                    if (EmployeeBankingInfoGrid.Items.Count == 0)
                    {
                        ComboBoxControl sequenceCombo = (ComboBoxControl)insertItem.FindControl("EmployeeBankingSequenceCode");
                        NumericControl percentage = (NumericControl)insertItem.FindControl("Percentage");

                        sequenceCombo.SelectedIndex = sequenceCombo.FindItemByValue("99").Index;
                        sequenceCombo.ReadOnly = true;

                        percentage.Value = 100;
                    }
                }
            }
        }
        void EmployeeBankingInfoGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EmployeeBankingInfoGrid.DataSource = DataItemCollection;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void EmployeeBankingCode_NeedDataSource(object sender, EventArgs e)
        {
            if (sender is GridKeyValueControl)
            {
                GridKeyValueControl employeeBankingCode = (GridKeyValueControl)sender;
                GridTableView masterTableView = employeeBankingCode.Owner;
                GridKeyValueControl bankingCountryCode = (GridKeyValueControl)masterTableView.GetColumn("BankingCountryCode");

                employeeBankingCode.DataSource = Common.ServiceWrapper.CodeClient.GetEmployeeBankCode(bankingCountryCode.SelectedValue);
            }
        }
        protected void BankingCountryCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
            BindEmployeeBankingCodeCombo(sender);
        }
        private void BindEmployeeBankingCodeCombo(object sender)
        {
            if (sender is ComboBoxControl)
            {
                ComboBoxControl bankingCountryCombo = (ComboBoxControl)sender;
                GridEditFormItem formItem = (GridEditFormItem)bankingCountryCombo.BindingContainer;
                ComboBoxControl employeeBankingCombo = (ComboBoxControl)formItem.FindControl("EmployeeBankingCode");
                String bankingCountryCode = bankingCountryCombo.Items.Count == 2 ? bankingCountryCombo.Items[1].Value : bankingCountryCombo.SelectedValue;

                employeeBankingCombo.DataSource = Common.ServiceWrapper.CodeClient.GetEmployeeBankCode(bankingCountryCode);
                employeeBankingCombo.DataBind();

                //if US, make bank account type mandatory, else non-mandatory
                ComboBoxControl bankAccountTypeCombo = (ComboBoxControl)formItem.FindControl("CodeBankAccountTypeCode");
                if (bankAccountTypeCombo != null)
                    bankAccountTypeCombo.Mandatory = (bankingCountryCode == "US");

                TextBoxControl txtTransit = (TextBoxControl)formItem.FindControl("Transit");
                if (txtTransit != null)
                {
                    //if US, make it 9 digits
                    txtTransit.MaxLength = (bankingCountryCode == "US") ? 9 : 5;

                    //non-US transits must be 5 digits.  If user chose US, entered more than 5 digits, then changed to another country, if transit is more than 5 digits, clear it.
                    if (bankingCountryCode != "US" && txtTransit.Value != null && txtTransit.Value.ToString().Length > 5)
                        txtTransit.Value = null;
                }

                //if US, make transit be a range of 5 to 12 chars.
                WLPRegularExpressionValidator valTransit = (WLPRegularExpressionValidator)formItem.FindControl("val1Transit");
                if (valTransit != null)
                    valTransit.ValidationExpression = (bankingCountryCode == "US") ? "^[0-9]{5,12}$" : @"^\d{5}$";

                //if country is JM (Jamaica), make ABA Number Mandatory
                TextBoxControl txtAbaNumber = (TextBoxControl)formItem.FindControl("AbaNumber");
                if (txtAbaNumber != null)
                    txtAbaNumber.Mandatory = (bankingCountryCode == "JM");
            }
        }

        protected void CodeSeq_NeedDataSource(object sender, EventArgs e)
        {
            if (IsInsert)
            {
                if (IsDataExternallyLoaded)
                    Common.CodeHelper.PopulateComboBoxWithSeqCodesRemovingTypesInUseWizard((ICodeControl)sender, LanguageCode, (EmployeeBankingCollection)DataItemCollection);
                else
                    Common.CodeHelper.PopulateComboBoxWithSeqCodesRemovingTypesInUse((ICodeControl)sender, LanguageCode, EmployeeId);
            }
            else
                Common.CodeHelper.PopulateComboBoxWithSeqCodesRemovingTypesInUseLeavingSelected((ICodeControl)sender, LanguageCode, EmployeeId, ((ICodeControl)sender).SelectedValue.ToString());
        }
        protected void BankingCountryCode_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindEmployeeBankingCodeCombo(o);
        }
        #endregion

        #region handle updates
        protected void EmployeeBankingInfoGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            EmployeeBanking item = (EmployeeBanking)e.Item.DataItem;
            item.EmployeeId = EmployeeId;

            if (IsDataExternallyLoaded)
            {
                if (DataItemCollection.Count > 0)
                    item.EmployeeBankingId = ((EmployeeBanking)DataItemCollection[DataItemCollection.Count - 1]).EmployeeBankingId - 1;

                ItemChangedEventArgs args = new ItemChangedEventArgs() { Items = DataItemCollection };
                ((EmployeeBankingCollection)DataItemCollection).Add(item);
                OnItemChanged(args);
            }
            else
                ((DataItemCollection<EmployeeBanking>)DataItemCollection).Add(Common.ServiceWrapper.HumanResourcesClient.InsertEmployeeBanking(item));
        }
        protected void EmployeeBankingInfoGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                EmployeeBanking item = (EmployeeBanking)e.Item.DataItem;

                if (IsDataExternallyLoaded)
                {
                    item.CopyTo((EmployeeBanking)DataItemCollection[item.Key]);
                    ItemChangedEventArgs args = new ItemChangedEventArgs() { Items = DataItemCollection };
                    OnItemChanged(args);
                }
                else
                    Common.ServiceWrapper.HumanResourcesClient.UpdateEmployeeBanking(item).CopyTo((EmployeeBanking)DataItemCollection[item.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void EmployeeBankingInfoGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (IsDataExternallyLoaded)
                {
                    ItemChangedEventArgs args = new ItemChangedEventArgs() { Items = DataItemCollection };
                    OnItemChanged(args);
                }
                else
                    Common.ServiceWrapper.HumanResourcesClient.DeleteEmployeeBanking((EmployeeBanking)e.Item.DataItem);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region override
        public override void Update(bool updateExterallyControlled)
        {
            //nothing to update, grid already updated
        }
        public override void AddNewDataItem()
        {
            if (DataItemCollection == null)
                DataItemCollection = new EmployeeBankingCollection();
        }
        public override void ChangeModeEdit()
        {
        }
        #endregion
    }
}