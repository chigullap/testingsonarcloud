﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeRoeEarningsHoursControl.ascx.cs" Inherits="WorkLinks.Payroll.RoeCreation.EmployeeRoeEarningsHoursControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<wasp:WLPFormView
    ID="EmployeeRoeEarningsHoursView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key"
    OnNeedDataSource="EmployeeRoeEarningsHoursView_NeedDataSource"
    OnUpdating="EmployeeRoeEarningsHoursView_Updating">

    <ItemTemplate>
        <wasp:WLPToolBar ID="EmployeeRoeEarningsHoursToolBar" runat="server" Width="100%" AutoPostBack="true">
            <Items>
                <wasp:WLPToolBarButton Text="Edit" ImageUrl="~/App_Themes/Default/Edit.gif" CausesValidation="false" CommandName="edit" Visible='<%# UpdateFlag %>' ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PayrollProcessGroupCode" Text="Payroll Group" runat="server" ResourceName="PayrollProcessGroupCode" Type="PayrollProcessGroupCode" Value='<%# PayrollProcessGroupCode %>' ReadOnly="true" OnDataBinding="Code_NeedDataSource" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="TotalInsurableHours" Text="Total insurable Hours" runat="server" ResourceName="TotalInsurableHours" Value='<%# Eval("TotalInsurableHours") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="TotalInsurableEarningAmount" DecimalDigits="2" Text="Total insurable Earnings" runat="server" ResourceName="TotalInsurableEarningAmount" Value='<%# Eval("TotalInsurableEarningAmount") %>' ReadOnly="true" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar ID="EmployeeRoeEarningsHoursToolBar" runat="server" Width="100%" AutoPostBack="true" OnClientButtonClicked="ToolBarClick">
            <Items>
                <wasp:WLPToolBarButton Text="Insert" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert" />
                <wasp:WLPToolBarButton Text="Update" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update" />
                <wasp:WLPToolBarButton Text="Cancel" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PayrollProcessGroupCode" Text="Payroll Group" runat="server" ResourceName="PayrollProcessGroupCode" Type="PayrollProcessGroupCode" Value='<%# PayrollProcessGroupCode %>' ReadOnly="true" OnDataBinding="Code_NeedDataSource" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="TotalInsurableHours" Text="Total insurable Hours" runat="server" ResourceName="TotalInsurableHours" Value='<%# Bind("TotalInsurableHours") %>' ReadOnly="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="TotalInsurableEarningAmount" DecimalDigits="2" Text="Total insurable Earnings" runat="server" ResourceName="TotalInsurableEarningAmount" Value='<%# Bind("TotalInsurableEarningAmount") %>' ReadOnly="false" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>

</wasp:WLPFormView>

<wasp:WLPGrid
    ID="EmployeeRoeEarningsHoursGrid"
    runat="server"
    GridLines="None"
    AutoGenerateColumns="false"
    AutoInsertOnAdd="false"
    AllowMultiRowEdit="true"
    Skin="">

    <ClientSettings>
        <Selecting AllowRowSelect="false" />
        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key" TableLayout="Fixed" EditMode="InPlace">
        <CommandItemTemplate></CommandItemTemplate>

        <Columns>
            <wasp:GridNumericControl DataField="InsurableEarningAmount" DataFormatString="{0:$########.00}" LabelText="InsurableEarningAmount" SortExpression="InsurableEarningAmount" ReadOnly="false" UniqueName="InsurableEarningAmount" ResourceName="InsurableEarningAmount">
                <HeaderStyle Width="200px" />
            </wasp:GridNumericControl>
            <wasp:GridDateTimeControl ReadOnly="true" ItemStyle-HorizontalAlign="Center" DataField="ChequeDate" LabelText="ChequeDate" SortExpression="ChequeDate" UniqueName="ChequeDate" ResourceName="ChequeDate"></wasp:GridDateTimeControl>
            <wasp:GridNumericControl DataField="PeriodYear" LabelText="PeriodYear" SortExpression="PeriodYear" UniqueName="PeriodYear" ResourceName="PeriodYear" ReadOnly="true"></wasp:GridNumericControl>
            <wasp:GridNumericControl DataField="Period" LabelText="Period" SortExpression="Period" UniqueName="Period" ResourceName="Period" ReadOnly="true"></wasp:GridNumericControl>
            <wasp:GridDateTimeControl ReadOnly="true" ItemStyle-HorizontalAlign="Center" DataField="StartDate" LabelText="StartDate" SortExpression="StartDate" UniqueName="StartDate" ResourceName="StartDate"></wasp:GridDateTimeControl>
            <wasp:GridDateTimeControl ReadOnly="true" ItemStyle-HorizontalAlign="Center" DataField="CutoffDate" LabelText="CutoffDate" SortExpression="CutoffDate" UniqueName="CutoffDate" ResourceName="CutoffDate"></wasp:GridDateTimeControl>
        </Columns>
    </MasterTableView>

    <HeaderContextMenu EnableAutoScroll="True"></HeaderContextMenu>

</wasp:WLPGrid>