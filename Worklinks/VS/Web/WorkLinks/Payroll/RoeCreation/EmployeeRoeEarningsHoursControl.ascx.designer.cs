﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace WorkLinks.Payroll.RoeCreation {
    
    
    public partial class EmployeeRoeEarningsHoursControl {
        
        /// <summary>
        /// EmployeeRoeEarningsHoursView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPFormView EmployeeRoeEarningsHoursView;
        
        /// <summary>
        /// EmployeeRoeEarningsHoursGrid control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPGrid EmployeeRoeEarningsHoursGrid;
    }
}
