﻿using System;
using System.Collections.Generic;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.RoeCreation
{
    public partial class RoeCreationSearchControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected bool EnableAddButton { get { return Common.Security.RoleForm.RoeCreationSearch.AddFlag; } }
        protected bool EnableDetailsButton { get { return Common.Security.RoleForm.RoeCreationRoeDetails; } }
        protected bool EnableExportRoeButton { get { return Common.Security.RoleForm.RoeCreationSearch.ViewFlag; } }
        protected String Criteria
        {
            get { return (String)PayrollProcessGroupCodeControl.Value; }
            set { PayrollProcessGroupCodeControl.Value = null; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.RoeCreationSearch.ViewFlag);

            WireEvents();

            if (!IsPostBack)
                Panel1.DataBind();

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.StartsWith("refresh"))
            {
                try
                {
                    long selectedEmployeeId = Convert.ToInt64(args.Substring(16));

                    //do an update for the employee...
                    Common.ServiceWrapper.HumanResourcesClient.SetPendingRoeStatusForEmployeeId(selectedEmployeeId);

                    //refresh by calling Search
                    Search(Criteria);
                }
                catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
                {
                    if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.Other)
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "RoeConstraintErrorMessage")), true);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else if (args != null && args.StartsWith("employeePositionIds="))
            {
                List<long> employeePositionIds = new List<long>();
                bool missingRoeReason = false;
                String employeePositionIdsString = args.Substring(20);

                foreach (String employeePositionId in employeePositionIdsString.ToString().Split(','))
                    employeePositionIds.Add(Convert.ToInt64(employeePositionId));

                Session["HandOff"] = Common.ServiceWrapper.XmlClient.ExportRoe(employeePositionIds.ToArray(), Common.ApplicationParameter.ImportExportProcessingDirectory, Common.ApplicationParameter.RoeIssueCode, out missingRoeReason);

                if (!missingRoeReason)
                {
                    Search(Criteria);
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "invokeDownload", "invokeDownload(1650);", true); //1650 means nothing, routing needs a value to not fail/blowup
                }
                else
                {
                    Search(Criteria);
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "RoeMissingDataMessage")), true);
                }
            }
        }
        public void WireEvents()
        {
            EmployeeSummaryGrid.NeedDataSource += new GridNeedDataSourceEventHandler(EmployeeSummaryGrid_NeedDataSource);
            EmployeeSummaryGrid.ItemDataBound += new GridItemEventHandler(EmployeeSummaryGrid_ItemDataBound);
            EmployeeSummaryGrid.PreRender += new EventHandler(EmployeeSummaryGrid_PreRender);
            EmployeeSummaryGrid.AllowPaging = true;
        }
        public void Search(String criteria)
        {
            RoeCreationSearchResultsCollection collection = new RoeCreationSearchResultsCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetPendingRoeEmployeeSummary(criteria));
            Data = collection;
            EmployeeSummaryGrid.Rebind();
        }
        #endregion

        #region event handlers
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search(Criteria);
        }
        void EmployeeSummaryGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EmployeeSummaryGrid.DataSource = Data;
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Criteria = null;
            Data = null;
            EmployeeSummaryGrid.DataBind();
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void EmployeeSummaryGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                //Work Around to remove empty rows caused by the OnCommand Telerik Bug 
                if (String.Equals((dataItem["EmployeeNumber"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["LastName"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["FirstName"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["OrgUnitDescription"].Text).ToUpper(), "&NBSP;"))
                {
                    //Hide The Row                    
                    e.Item.Display = false;
                }
            }
        }
        void EmployeeSummaryGrid_PreRender(object sender, EventArgs e)
        {
            //Work Around to remove empty rows caused by the OnCommand Telerik Bug
            if (Data == null)
                EmployeeSummaryGrid.AllowPaging = false;
            else
                EmployeeSummaryGrid.AllowPaging = true;
        }
        #endregion

        #region security handlers
        protected void EmployeeAddToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableAddButton; }
        protected void EmployeeDetailsToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableDetailsButton; }
        protected void ExportROEToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableExportRoeButton; }
        #endregion
    }
}