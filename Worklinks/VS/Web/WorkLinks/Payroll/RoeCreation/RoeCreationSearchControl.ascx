﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RoeCreationSearchControl.ascx.cs" Inherits="WorkLinks.Payroll.RoeCreation.RoeCreationSearchControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="SearchPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SearchPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="SearchPanel" runat="server">
    <table width="100%">
        <tr valign="top">
            <td>
                <div>
                    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                        <table width="100%">
                            <tr>
                                <td>
                                    <wasp:ComboBoxControl ID="PayrollProcessGroupCodeControl" ClientIDMode="Static" LabelText="Process Group" runat="server" Type="PayrollProcessGroupCode" IncludeEmptyItem="false" OnClientSelectedIndexChanged="changeProcessGroup" OnDataBinding="Code_NeedDataSource" ResourceName="PayrollProcessGroupCode" ReadOnly="false" TabIndex="050"></wasp:ComboBoxControl>
                                </td>
                            </tr>
                        </table>
                        <div class="SearchCriteriaButtons">
                            <wasp:WLPButton ID="btnClear" Icon-PrimaryIconUrl="~/App_Themes/Default/clear.gif" ResourceName="btnClear" runat="server" Text="Clear" OnClientClicked="clear" OnClick="btnClear_Click" CssClass="button" />
                            <wasp:WLPButton ID="btnSearch" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" ResourceName="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                        </div>
                    </asp:Panel>
                </div>
            </td>
        </tr>
        <tr valign="top">
            <td>
                <div>
                    <wasp:WLPToolBar ID="EmployeeSummaryToolBar" runat="server" Width="100%" OnClientLoad="EmployeeSummaryToolBar_init">
                        <Items>
                            <wasp:WLPToolBarButton OnPreRender="EmployeeAddToolBar_PreRender" Text="Add" ImageUrl="~/App_Themes/Default/Add.gif" onclick="Add()" CommandName="add" ResourceName="Add"></wasp:WLPToolBarButton>
                            <wasp:WLPToolBarButton OnPreRender="EmployeeDetailsToolBar_PreRender" Text="Details" onclick="Open()" CommandName="details" ResourceName="Details"></wasp:WLPToolBarButton>
                            <wasp:WLPToolBarButton OnPreRender="ExportROEToolBar_PreRender" Text="Export ROE" onclick="OpenExportROE()" CommandName="exportROE" ResourceName="exportROE"></wasp:WLPToolBarButton>
                        </Items>
                    </wasp:WLPToolBar>

                    <wasp:WLPGrid
                        ID="EmployeeSummaryGrid"
                        runat="server"
                        AllowMultiRowSelection="true"
                        AllowSorting="true"
                        GridLines="None"
                        Height="400px"
                        AutoAssignModifyProperties="true">

                        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                            <ClientEvents OnRowDblClick="OnRowDblClick" OnRowClick="OnRowClick" OnGridCreated="onGridCreated" OnCommand="onCommand" OnRowSelected="rowSelectedDeselected" OnRowDeselected="rowSelectedDeselected" />
                        </ClientSettings>

                        <MasterTableView
                            ClientDataKeyNames="EmployeeId, LastName, FirstName, PayrollProcessGroupCode, EmployeeNumber, EmployeePositionId"
                            AutoGenerateColumns="false"
                            DataKeyNames="EmployeeId"
                            CommandItemDisplay="Top"
                            AllowPaging="false">

                            <CommandItemTemplate></CommandItemTemplate>

                            <Columns>
                                <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
                                    <HeaderStyle Width="6%" />
                                </telerik:GridClientSelectColumn>
                                <wasp:GridBoundControl DataField="EmployeeNumber" LabelText="EmployeeNumber" SortExpression="EmployeeNumber" UniqueName="EmployeeNumber" ResourceName="EmployeeNumber"></wasp:GridBoundControl>
                                <wasp:GridBoundControl DataField="LastName" LabelText="LastName" SortExpression="LastName" UniqueName="LastName" ResourceName="LastName"></wasp:GridBoundControl>
                                <wasp:GridBoundControl DataField="FirstName" SortExpression="FirstName" UniqueName="FirstName" ResourceName="FirstName"></wasp:GridBoundControl>
                                <wasp:GridDateTimeControl DataField="TerminationDate" SortExpression="TerminationDate" UniqueName="TerminationDate" ResourceName="TerminationDate"></wasp:GridDateTimeControl>
                                <wasp:GridBoundControl DataField="EmployeePositionStatusCode" SortExpression="EmployeePositionStatusCode" UniqueName="EmployeePositionStatusCode" ResourceName="CodeEmployeePositionStatusDescription"></wasp:GridBoundControl>
                                <wasp:GridBoundControl DataField="OrgUnitDescription" LabelText="OrgUnitDescription" SortExpression="OrgUnitDescription" UniqueName="OrgUnitDescription" ResourceName="OrganizationUnitDescription">
                                    <HeaderStyle Width="36%" />
                                </wasp:GridBoundControl> 
                            </Columns>

                        </MasterTableView>

                        <HeaderContextMenu EnableAutoScroll="true"></HeaderContextMenu>

                    </wasp:WLPGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="PayrollWindows"
    Width="1000"
    Height="700"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    OnClientClose="onClientClose"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false">
</wasp:WLPWindowManager>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="EmployeeMiniSearchWindows"
    Width="800"
    Height="500"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    OnClientClose="onClientClose"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false">
</wasp:WLPWindowManager>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="HiddenWindow"
    Width="0"
    Height="0"
    runat="server"
    DestroyOnClose="true"
    Modal="false">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var toolbar;
        var employeeId;
        var employeeNumber;
        var lastName;
        var firstName;
        var employeePositionId;
        var payrollProcessGroupCode;
        var initialPayrollProcessGroupCode;

        function clear() {
            employeeId = null;
            employeeNumber = null;
            lastName = null;
            firstName = null;
            employeePositionId = null;
            payrollProcessGroupCode = initialPayrollProcessGroupCode;
        }

        function EmployeeSummaryToolBar_init(sender) {
            initializeControls();
        }

        function getSelectedRow() {
            var employeeSummaryGrid = $find('<%= EmployeeSummaryGrid.ClientID %>');
            if (employeeSummaryGrid != null) {
                var MasterTable = employeeSummaryGrid.get_masterTableView();
                if (MasterTable != null)
                    return MasterTable.get_selectedItems()[0];
                else
                    return null;
            }

            return null;
        }

        function rowIsSelected() {
            var getSeletecteRow = getSelectedRow();
            return (getSeletecteRow != null);
        }

        function initializeControls() {
            if ($find('<%= EmployeeSummaryToolBar.ClientID %>') != null)
                toolbar = $find('<%= EmployeeSummaryToolBar.ClientID %>');

            enableButtons(rowIsSelected(), rowIsSelected());
        }

        function OnRowDblClick(sender, eventArgs) {
            if (toolbar.findButtonByCommandName('details') != null)
                Open(sender);
        }

        function onGridCreated(sender, eventArgs) {
            if (rowIsSelected()) {
                var selectedRow = getSelectedRow();

                employeeId = selectedRow.getDataKeyValue('EmployeeId');
                employeeNumber = selectedRow.getDataKeyValue('EmployeeNumber');
                lastName = selectedRow.getDataKeyValue('LastName');
                firstName = selectedRow.getDataKeyValue('FirstName');
                payrollProcessGroupCode = selectedRow.getDataKeyValue('PayrollProcessGroupCode');
                employeePositionId = selectedRow.getDataKeyValue('EmployeePositionId');

                if (payrollProcessGroupCode == null)
                    payrollProcessGroupCode = 'null';

                enableButtons(true, true);
            }
            else {
                employeeId = null;
                lastName = "";
                firstName = "";
                employeeNumber = "";
                employeePositionId = null;

                if (typeof document.getElementById(document.getElementById('PayrollProcessGroupCodeControl').attributes['fieldClientId'].value).control != 'undefined') {
                    payrollProcessGroupCode = document.getElementById(document.getElementById('PayrollProcessGroupCodeControl').attributes['fieldClientId'].value).control._value;
                    initialPayrollProcessGroupCode = payrollProcessGroupCode;
                }
            }
        }

        function OnRowClick(sender, eventArgs) {
            //keep track of args
            employeeId = eventArgs.getDataKeyValue('EmployeeId');
            employeeNumber = eventArgs.getDataKeyValue('EmployeeNumber');
            lastName = eventArgs.getDataKeyValue('LastName');
            firstName = eventArgs.getDataKeyValue('FirstName');
            payrollProcessGroupCode = eventArgs.getDataKeyValue('PayrollProcessGroupCode');
            employeePositionId = eventArgs.getDataKeyValue('EmployeePositionId');

            if (payrollProcessGroupCode == null)
                payrollProcessGroupCode = 'null';
        }

        function rowSelectedDeselected(sender, eventArgs) {
            var enableDetails = true;
            var enableROE = true;

            //keep track of args
            employeeId = eventArgs.getDataKeyValue('EmployeeId');
            employeeNumber = eventArgs.getDataKeyValue('EmployeeNumber');
            lastName = eventArgs.getDataKeyValue('LastName');
            firstName = eventArgs.getDataKeyValue('FirstName');
            payrollProcessGroupCode = eventArgs.getDataKeyValue('PayrollProcessGroupCode');
            employeePositionId = eventArgs.getDataKeyValue('EmployeePositionId');

            if (payrollProcessGroupCode == null)
                payrollProcessGroupCode = 'null';

            var employeeSummaryGrid = $find('<%= EmployeeSummaryGrid.ClientID %>');
            if (employeeSummaryGrid != null) {
                var MasterTable = employeeSummaryGrid.get_masterTableView();
                if (MasterTable != null) {
                    //if more than 1 row is selected, disable the details button
                    if (MasterTable.get_selectedItems().length != 1)
                        enableDetails = false;

                    //if no row is selected, disable the ROE button
                    if (MasterTable.get_selectedItems().length == 0)
                        enableROE = false;
                }
            }

            enableButtons(enableDetails, enableROE);
        }

        function changeProcessGroup(sender, eventArgs) {
            payrollProcessGroupCode = sender._value;
        }

        function enableButtons(enableDetails, enableROE) {
            var detailsButton = toolbar.findButtonByCommandName('details');
            var exportROEButton = toolbar.findButtonByCommandName('exportROE');

            if (detailsButton != null)
                detailsButton.set_enabled(enableDetails);

            if (exportROEButton != null)
                exportROEButton.set_enabled(enableROE);
        }

        function Add() {
            if (typeof payrollProcessGroupCode != 'undefined') {
                if (payrollProcessGroupCode != '' && payrollProcessGroupCode != null)
                    openWindowWithManager('EmployeeMiniSearchWindows', String.format('/HumanResources/Employee/EmployeeMiniSearchPage.aspx?payrollProcessGroupCode={0}', payrollProcessGroupCode), false);
            }
        }

        function Open() {
            if (employeeId != null)
                openWindowWithManager('PayrollWindows', String.format('/Payroll/RoeCreation/View/{0}/{1}/{2}/{3}/{4}/{5}', employeeId, lastName, firstName, employeePositionId, "x", payrollProcessGroupCode), false); //can't pass null or "" so using "x".  It has no meaning on this page.
        }

        function OpenExportROE() {
            if (employeeId != null) {
                var groupOfEmployeeIds = "";
                var groupOfEmployeePositionIds = "";
                var employeeSummaryGrid = $find('<%= EmployeeSummaryGrid.ClientID %>');

                if (employeeSummaryGrid != null) {
                    var MasterTable = employeeSummaryGrid.get_masterTableView();
                    if (MasterTable != null) {
                        for (i = 0; i < MasterTable.get_selectedItems().length; i++) {
                            var it = MasterTable.get_selectedItems()[i];

                            groupOfEmployeeIds = groupOfEmployeeIds + ',' + it.getDataKeyValue('EmployeeId');
                            groupOfEmployeePositionIds = groupOfEmployeePositionIds + ',' + it.getDataKeyValue('EmployeePositionId');
                        }

                        groupOfEmployeeIds = groupOfEmployeeIds.replace(',', '');
                        groupOfEmployeePositionIds = groupOfEmployeePositionIds.replace(',', '');
                    }
                }

                __doPostBack('<%= this.ClientID %>', String.format('employeePositionIds={0}', groupOfEmployeePositionIds));
            }
        }

        function invokeDownload(id) {
            var frame = document.createElement("frame");
            frame.src = getUrl(String.format('/Payroll/CreateROE/{0}', id)), null;
            frame.style.display = "none";
            document.body.appendChild(frame);
        }

        function onCommand(sender, args) {
            if (args.get_commandName() == 'Page') {
                employeeId = null;
                initializeControls();
            }
        }

        function onClientClose(sender, eventArgs) {
            var arg = eventArgs.get_argument();
            if (arg != null && arg.isUpdate)
                __doPostBack('<%= SearchPanel.ClientID %>', String.format('refreshEmployee={0}', arg.employeeId));
        }
    </script>
</telerik:RadScriptBlock>