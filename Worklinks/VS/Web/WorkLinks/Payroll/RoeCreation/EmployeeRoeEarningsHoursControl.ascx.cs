﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.RoeCreation
{
    public partial class EmployeeRoeEarningsHoursControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private const String _employeePositionIdKey = "EmployeePositionIdKey";
        private long _employeePositionId = -1;
        private const String _payrollProcessGroupCodeKey = "PayrollProcessGroupCodeeKey";
        private String _payrollProcessGroupCode = "";
        private bool? _needDataSourceFlag = null;
        private const String _dataSourceKey = "_dataSourceKey";
        #endregion

        #region properties
        public bool UpdateFlag
        {
            get { return Common.Security.RoleForm.EmployeeTerminationDetail.UpdateFlag; }
        }
        public bool IsInsertMode
        {
            get { return EmployeeRoeEarningsHoursView.CurrentMode.Equals(FormViewMode.Insert); }
        }
        public bool IsViewMode
        {
            get { return EmployeeRoeEarningsHoursView.CurrentMode.Equals(FormViewMode.ReadOnly); }
        }
        public bool IsEditMode
        {
            get { return EmployeeRoeEarningsHoursView.CurrentMode.Equals(FormViewMode.Edit); }
        }
        private long EmployeeId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); }
        }
        private string LastName
        {
            get
            {
                if (Page.RouteData.Values["lastName"] == null)
                    return null;
                else
                    return Page.RouteData.Values["lastName"].ToString();
            }
        }
        private string FirstName
        {
            get
            {
                if (Page.RouteData.Values["firstName"] == null)
                    return null;
                else
                    return Page.RouteData.Values["firstName"].ToString();
            }
        }
        public long EmployeePositionId //this is set in the EmployeeRoeCreationPageControl
        {
            get
            {
                if (_employeePositionId == -1)
                {
                    Object obj = ViewState[_employeePositionIdKey];
                    if (obj != null)
                        _employeePositionId = Convert.ToInt64(obj);
                }

                return _employeePositionId;
            }
            set
            {
                _employeePositionId = value;
                ViewState[_employeePositionIdKey] = _employeePositionId;
            }
        }
        public String PayrollProcessGroupCode //this is set in the EmployeeRoeCreationPageControl
        {
            get
            {
                if (_payrollProcessGroupCode == null || _payrollProcessGroupCode == "")
                {
                    Object obj = ViewState[_payrollProcessGroupCodeKey];
                    if (obj != null)
                        _payrollProcessGroupCode = obj.ToString();
                }

                return _payrollProcessGroupCode;
            }
            set
            {
                _payrollProcessGroupCode = value.ToString();
                ViewState[_payrollProcessGroupCodeKey] = _payrollProcessGroupCode;
            }
        }
        public bool? NeedDataSourceFlag //keeps track of the previous value for IsViewMode.  We want to avoid the Grid performing 2 need datasource calls when "Update" is used.
        {
            get
            {
                if (_needDataSourceFlag == null)
                {
                    Object obj = ViewState[_dataSourceKey];
                    if (obj != null)
                        _needDataSourceFlag = (bool)obj;
                }

                return _needDataSourceFlag;
            }
            set
            {
                _needDataSourceFlag = value;
                ViewState[_dataSourceKey] = _needDataSourceFlag;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) SetAuthorized(Common.Security.RoleForm.RoeCreationDetails.ViewFlag);

            WireEvents();
        }
        protected void WireEvents()
        {
            EmployeeRoeEarningsHoursGrid.NeedDataSource += new GridNeedDataSourceEventHandler(EmployeeRoeEarningsHoursGrid_NeedDataSource);
            EmployeeRoeEarningsHoursGrid.PreRender += new EventHandler(EmployeeRoeEarningsHoursGrid_PreRender);
            EmployeeRoeEarningsHoursGrid.UpdateAllCommand += new WLPGrid.UpdateAllCommandEventHandler(EmployeeRoeEarningsHoursGrid_UpdateAllCommand);
        }
        protected void LoadEmployeeEarningsHours(long employeePositionId)
        {
            //populate a EmployeeRoeAmount collection and bind that to formview, populate the prop collection and bind that to grid...
            Data = Common.ServiceWrapper.HumanResourcesClient.GetRoeData(EmployeePositionId, EmployeeId);

            EmployeeRoeEarningsHoursView.DataBind();
            EmployeeRoeEarningsHoursGrid.DataBind();
        }
        protected void EmployeeRoeEarningsHoursView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            try
            {
                EmployeeRoeAmount roeObj = (EmployeeRoeAmount)e.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.UpdateEmployeeRoeAmount(roeObj);

                //Fire the update all for the Grid
                EmployeeRoeEarningsHoursGrid.UpdateAll();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void EmployeeRoeEarningsHoursView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            EmployeeRoeEarningsHoursView.DataSource = Data;

            //prevent grid from getting 2 datasource calls during Update otherwise changed data will be lost
            if (IsViewMode != NeedDataSourceFlag)
            {
                NeedDataSourceFlag = IsViewMode;
                EmployeeRoeEarningsHoursGrid.Rebind();
            }
        }
        protected void EmployeeRoeEarningsHoursGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            EmployeeRoeEarningsHoursGrid.DataSource = ((EmployeeRoeAmount)Data[0]).AmountDetailSummaryCollection;
        }
        void EmployeeRoeEarningsHoursGrid_PreRender(object sender, EventArgs e)
        {
            foreach (GridItem item in EmployeeRoeEarningsHoursGrid.MasterTableView.Items)
            {
                if (item is GridEditableItem)
                {
                    GridEditableItem editableItem = item as GridDataItem;
                    editableItem.Edit = !IsViewMode;
                }
            }

            EmployeeRoeEarningsHoursGrid.Rebind();
        }
        void EmployeeRoeEarningsHoursGrid_UpdateAllCommand(object sender, UpdateAllCommandEventArgs e)
        {
            try
            {
                //pass the collection to WS, determine if deletes or inserts are needed, then return collection.
                EmployeeRoeAmountDetailCollection collection = new EmployeeRoeAmountDetailCollection();
                EmployeeRoeAmountDetail singleItem = new EmployeeRoeAmountDetail();

                foreach (EmployeeRoeAmountDetail entity in e.Items)
                {
                    //collection.Add(entity);
                    singleItem = fillCollectionWithData(entity);
                    collection.Add(singleItem);
                }

                Common.ServiceWrapper.HumanResourcesClient.UpdateEmployeeRoeDetail(collection.ToArray());

                LoadEmployeeEarningsHours(EmployeePositionId);
                EmployeeRoeEarningsHoursGrid.Rebind();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private EmployeeRoeAmountDetail fillCollectionWithData(EmployeeRoeAmountDetail singleItem)
        {
            EmployeeRoeAmountDetail tempObj = new EmployeeRoeAmountDetail();
            tempObj.EmployeeRoeAmountDetailId = singleItem.EmployeeRoeAmountDetailId;
            tempObj.EmployeeRoeAmountId = singleItem.EmployeeRoeAmountId;
            tempObj.PayrollPeriodId = singleItem.PayrollPeriodId;
            tempObj.InsurableEarningAmount = singleItem.InsurableEarningAmount;
            tempObj.RowVersion = singleItem.RowVersion;
            tempObj.UpdateUser = singleItem.UpdateUser;

            return tempObj;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        public void SetEmployeePositionId(long employeePositionId, String processGroupCode) //called from EmployeeRoeCreationPageControl which houses termination control, roe termination, and this control.
        {
            PayrollProcessGroupCode = processGroupCode;

            //stacy test -- could have a sync issue since terminations has pos_id of -1 since employee isnt terminated...
            if (employeePositionId == -1)
                EmployeePositionId = Convert.ToInt64(Page.RouteData.Values["employeePositionId"]);
            else
                EmployeePositionId = employeePositionId;

            LoadEmployeeEarningsHours(EmployeePositionId);
        }
    }
}