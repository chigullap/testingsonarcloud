﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateROEPage.aspx.cs" Inherits="WorkLinks.Payroll.RoeCreation.CreateROEPage" %>

<%@ Register Src="~/Payroll/RoeCreation/CreateROEControl.ascx" TagPrefix="uc1" TagName="CreateROEControl" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <uc1:CreateROEControl runat="server" id="CreateROEControl" />
    </form>
</body>
</html>
