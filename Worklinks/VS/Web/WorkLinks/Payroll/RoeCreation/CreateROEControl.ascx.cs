﻿using System;

namespace WorkLinks.Payroll.RoeCreation
{
    public partial class CreateROEControl : WLP.Web.UI.WLPUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            byte[] output = (byte[])Session["HandOff"];
            Session.Remove("HandOff");

            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AppendHeader("content-disposition", "attachment; filename=" + String.Format("roe_{0}.blk", Common.ApplicationParameter.CeridianCompanyNumber));
            Response.OutputStream.Write(output, 0, output.Length);
            Response.Flush();
            Response.End();
        }
    }
}