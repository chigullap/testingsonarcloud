﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeRoeCreationPageControl.ascx.cs" Inherits="WorkLinks.Payroll.RoeCreation.EmployeeRoeCreationPageControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register src="../../HumanResources/Terminations/EmployeeTerminationControl.ascx" tagname="EmployeeTerminationControl" tagprefix="uc1" %>
<%@ Register src="../../HumanResources/Terminations/EmployeeTerminationROEControl.ascx" tagname="EmployeeTerminationROEControl" tagprefix="uc2" %>
<%@ Register src="EmployeeRoeEarningsHoursControl.ascx" tagname="EmployeeRoeEarningsHoursControl" tagprefix="uc3" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="TerminationPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="TerminationPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="TerminationPanel" runat="server">
    <table width="100%">
        <tr>
            <td width="10%" valign="top">
                <wasp:WLPTabStrip ID="RadTabStrip1" runat="server" Width="100%" Orientation="VerticalLeft" MultiPageID="EmployeeMultiPage" OnPreRender="RadTabStrip1_PreRender">
                    <Tabs>
                        <wasp:WLPTab Text="Termination" OnInit="EmployeeTermination_OnInit" PageViewID="EmployeeTerminationControl" ResourceName="Termination" />
                        <wasp:WLPTab Text="ROE" OnInit="EmployeeTerminationROE_OnInit" ResourceName="ROE" />
                        <wasp:WLPTab Text="Earnings/Hours" ResourceName="EarningsHours" />
                    </Tabs>
                </wasp:WLPTabStrip>
            </td>
            <td width="90%" valign="top">
                <telerik:RadMultiPage Id="EmployeeMultiPage" runat="server" onprerender="EmployeeMultiPage_PreRender">
                    <telerik:RadPageView ID="EmployeeTerminationControlPage" OnInit="EmployeeTermination_OnInit" runat="server">
                        <uc1:EmployeeTerminationControl ID="EmployeeTerminationControl1" runat="server" />
                    </telerik:RadPageView>
                  <telerik:RadPageView ID="EmployeeTerminationROEControlPage" OnInit="EmployeeTerminationROE_OnInit" runat="server">
                      <uc2:EmployeeTerminationROEControl ID="EmployeeTerminationROEControl1" runat="server" />
                  </telerik:RadPageView>
                  <telerik:RadPageView ID="EmployeeRoeEarningsHoursControlPage" OnInit="EmployeeRoeEarningsHours_OnInit" runat="server">
                      <uc3:EmployeeRoeEarningsHoursControl ID="EmployeeRoeEarningsHoursControl1" runat="server" />
                  </telerik:RadPageView>
               </telerik:RadMultiPage>
            </td>
        </tr>
    </table>
</asp:Panel>