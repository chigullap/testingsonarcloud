﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExportChequeControl.ascx.cs" Inherits="WorkLinks.Payroll.ExportCheque.ExportChequeControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="MainPanel" runat="server">
    <wasp:WLPToolBar ID="ExportChequeToolBar" runat="server" Width="100%" AutoPostBack="true" Style="margin-bottom: 0" OnClientLoad="ExportChequeToolBar_init">
        <Items>
            <wasp:WLPToolBarButton Text="generateCheque" ImageUrl="" onclick="OpenExportCheque()" CommandName="generateCheque" Visible='<%# IsViewMode %>' ResourceName="GenerateCheque" />
            <wasp:WLPToolBarButton Text="Cancel" ImageUrl="" onclick="CloseWindow()" CommandName="cancel" Visible='<%# IsViewMode %>' ResourceName="Cancel" />
        </Items>
    </wasp:WLPToolBar>

    <wasp:WLPGrid
        ID="ExportChequeGrid"
        runat="server"
        AllowMultiRowSelection="true"
        AllowSorting="True"
        GridLines="None"
        AutoAssignModifyProperties="True">

        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
            <Selecting AllowRowSelect="True" />
            <ClientEvents OnGridCreated="onGridCreated" OnRowSelected="rowSelectedDeselected" OnRowDeselected="rowSelectedDeselected" />
        </ClientSettings>

        <MasterTableView ClientDataKeyNames="PayrollMasterPaymentId" CommandItemDisplay="Top" DataKeyNames="Key" AutoGenerateColumns="false" AllowPaging="false">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="Amount" SortOrder="Ascending" />
            </SortExpressions>

            <CommandItemTemplate></CommandItemTemplate>

            <Columns>
                <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
                    <HeaderStyle Width="6%" />
                </telerik:GridClientSelectColumn>
                <wasp:GridBoundControl DataField="Amount" LabelText="Amount" SortExpression="Amount" UniqueName="Amount" ResourceName="ChequeAmount" />
                <wasp:GridBoundControl DataField="EmployeeBankAccountNumber" LabelText="BankAccountNumber" SortExpression="EmployeeBankAccountNumber" UniqueName="EmployeeBankAccountNumber" ResourceName="BankAccountNumber" />
            </Columns>
        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="True" />

    </wasp:WLPGrid>
</asp:Panel>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function GetRadWindow() {
            var popup = null;
            if (window.radWindow)
                popup = window.radWindow;
            else if (window.frameElement.radWindow)
                popup = window.frameElement.radWindow;
            return popup;
        }

        function CloseWindow() {
            var window = GetRadWindow();
            window.Close();
        }

        function ExportChequeToolBar_init(sender) {
            initializeControls();
        }

        function OpenExportCheque() {
            var groupOfPayrollMasterPaymentIds = "";
            var exportChequeGrid = $find('<%= ExportChequeGrid.ClientID %>');

            if (exportChequeGrid != null) {
                var MasterTable = exportChequeGrid.get_masterTableView();

                if (MasterTable != null) {
                    if (MasterTable.get_selectedItems().length > 0) {

                        for (i = 0; i < MasterTable.get_selectedItems().length; i++) {
                            var it = MasterTable.get_selectedItems()[i];
                            groupOfPayrollMasterPaymentIds = groupOfPayrollMasterPaymentIds + ',' + it.getDataKeyValue('PayrollMasterPaymentId');
                        }

                        groupOfPayrollMasterPaymentIds = groupOfPayrollMasterPaymentIds.replace(',', '');
                        __doPostBack('<%= this.ClientID %>', String.format('groupOfPayrollMasterPaymentIds={0}', groupOfPayrollMasterPaymentIds));
                    }
                }
            }
        }

        function enableButtons(enableExport) {
            var generateChequeButton = toolbar.findButtonByCommandName('generateCheque');

            if (generateChequeButton != null)
                generateChequeButton.set_enabled(enableExport);
        }

        function rowIsSelected() {
            var getSeletecteRow = getSelectedRow();

            return (getSeletecteRow != null);
        }

        function getSelectedRow() {
            var exportChequeGrid = $find('<%=ExportChequeGrid.ClientID%>');
            if (exportChequeGrid != null) {
                var MasterTable = exportChequeGrid.get_masterTableView();

                if (MasterTable != null)
                    return MasterTable.get_selectedItems()[0];
                else
                    return null;
            }

            return null;
        }

        function onGridCreated(sender, eventArgs) {
            if (rowIsSelected())
                enableButtons(true);
        }

        function rowSelectedDeselected(sender, eventArgs) {
            var enableExport = true;
            var exportChequeGrid = $find('<%=ExportChequeGrid.ClientID%>');

            if (exportChequeGrid != null) {
                var MasterTable = exportChequeGrid.get_masterTableView();

                if (MasterTable != null) {
                    //if no row is selected, disable the export cheque button
                    if (MasterTable.get_selectedItems().length == 0)
                        enableExport = false;
                }
            }

            enableButtons(enableExport);
        }

        function initializeControls() {
            if ($find('<%=ExportChequeToolBar.ClientID%>') != null)
                toolbar = $find('<%=ExportChequeToolBar.ClientID%>');

            enableButtons(rowIsSelected());
        }
    </script>
</telerik:RadScriptBlock>