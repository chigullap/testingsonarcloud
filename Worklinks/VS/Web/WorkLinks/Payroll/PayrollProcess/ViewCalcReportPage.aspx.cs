﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace WorkLinks.Payroll.PayrollProcess
{
    public partial class ViewCalcReportPage : System.Web.UI.Page
    {
        public long PayrollProcessId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["PayrollProcessId"]); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadFileDropDownList();
               
            }
        }

        private void LoadFileDropDownList()
        {
            String path = Common.ApplicationParameter.DynamicsGPOutputFolder + "\\" + Common.ApplicationParameter.DynamicsDatabaseName + "\\" + Common.ApplicationParameter.DynamicsGPOutputName;

            KeyValuePair<String, String>[] dropDownValues = Common.ServiceWrapper.HumanResourcesClient.GetDynamicsOutputFileList(path, PayrollProcessId);

            FileDropDownList.Items.Add(new ListItem(null, null));

            foreach (KeyValuePair<String, String> file in dropDownValues)
            {
                FileDropDownList.Items.Add(new ListItem(file.Key, file.Value));
            }
        }

        protected void FileDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            String filePath = ((DropDownList)sender).SelectedValue;
            ProcessReportMessageTextBox.Text = Common.ServiceWrapper.HumanResourcesClient.getFileText(filePath);
        }
    }
}