﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewCalcReportPage.aspx.cs" Inherits="WorkLinks.Payroll.PayrollProcess.ViewCalcReportPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:DropDownList ID="FileDropDownList" runat="server" AutoPostBack="true" 
            onselectedindexchanged="FileDropDownList_SelectedIndexChanged">
        </asp:DropDownList>
        <asp:TextBox Height="600px" Width="100%" ID="ProcessReportMessageTextBox" runat="server" 
            TextMode="MultiLine"></asp:TextBox>
    </div>
    </form>
</body>
</html>
