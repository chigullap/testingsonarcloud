﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.PayrollProcess
{
    public partial class PayrollProcessControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private static String _statusNew = Common.ApplicationParameter.Code.PayrollProcessStatusCode.New;
        private static String _statusCalculationPending = Common.ApplicationParameter.Code.PayrollProcessStatusCode.CalculationPending;
        private static String _statusCalculationProcessed = Common.ApplicationParameter.Code.PayrollProcessStatusCode.CalculationProcessed;
        private static String _statusPostPending = Common.ApplicationParameter.Code.PayrollProcessStatusCode.PostPending;
        private static String _statusProcessed = Common.ApplicationParameter.Code.PayrollProcessStatusCode.Processed;
        private static String _statusArrears = Common.ApplicationParameter.Code.PayrollProcessStatusCode.Arrears;
        private static String _importError = Common.ApplicationParameter.Code.PayrollProcessStatusCode.ImportError;

        #endregion

        #region properties
        private BusinessLayer.BusinessObjects.PayrollProcess PayrollProcess
        {
            get
            {
                if (Data.Count > 0)
                    return (BusinessLayer.BusinessObjects.PayrollProcess)Data[0];
                else
                    return null;
            }
            set
            {
                if (Data == null)
                    Data = new PayrollProcessCollection();

                if (Data.Count == 0)
                    ((PayrollProcessCollection)Data).Add(value);
                else
                    ((PayrollProcessCollection)Data)[0] = value;
            }
        }
        protected DateTime MaxDate
        {
            get
            {
                if (PayrollProcess.CurrentPayrollYear.ToString() != "0")
                    return Convert.ToDateTime(PayrollProcess.CurrentPayrollYear.ToString() + "/12/31");
                else
                    return Convert.ToDateTime(DateTime.Now.Year + "/12/31");
            }
        }
        protected long PayrollProcessId
        {
            get
            {
                if (PayrollProcess == null)
                    return -1;
                else
                    return PayrollProcess.PayrollProcessId;
            }
        }
        private String PayrollProcessStatus
        {
            get
            {
                if (PayrollProcess != null && PayrollProcess.PayrollProcessStatusCode != null)
                    return PayrollProcess.PayrollProcessStatusCode;
                else
                    return String.Empty;
            }
        }
        private bool PostButtonEnabled { get { return PayrollProcessStatus.Equals(_statusCalculationProcessed); } }
        private bool LockButtonVisible { get { return (Common.Security.RoleForm.PayrollProcessLockUnlockButtons.ViewFlag && PayrollProcessStatus.Equals(_statusNew) && PayrollProcess.LockedFlag == false); } }
        private bool UnlockButtonVisible { get { return (Common.Security.RoleForm.PayrollProcessLockUnlockButtons.ViewFlag && PayrollProcessStatus.Equals(_statusNew) && PayrollProcess.LockedFlag == true); } }
        private bool LockButtonPressed { get; set; }
        private bool UnlockButtonPressed { get; set; }
        private bool CalculateButtonPressed { get; set; }
        private bool ReCalculateButtonPressed { get; set; }
        private bool PostButtonPressed { get; set; }
        private bool ImportButtonPressed { get; set; }
        private bool CalcButtonEnabled { get { return (PayrollProcessStatus.Equals(_statusNew) && PayrollBatchSearchResultsControl.Visible); } }
        private bool ImportButtonEnabled { get { return (PayrollProcessStatus.Equals(_statusNew) && Common.ApplicationParameter.ClientDoesNotCalcPayroll); } }
        private bool EditButtonEnabled { get { return PayrollProcessStatus.Equals(_statusNew); } }
        private bool ViewReportsButtonEnabled { get { return PayrollProcessStatus.Equals(_statusCalculationProcessed) || PayrollProcessStatus.Equals(_statusProcessed) || PayrollProcessStatus.Equals(_statusArrears); } }
        private bool RecalculateButtonEnabled { get { return PayrollProcessStatus.Equals(_statusCalculationProcessed) || PayrollProcessStatus.Equals(_statusArrears) || PayrollProcessStatus.Equals(_importError); } }
        private bool ProcessPending { get { return PayrollProcessStatus.Equals(_statusCalculationPending) || PayrollProcessStatus.Equals(_statusPostPending); } }
        protected bool IsEditMode { get { return PayrollProcessStatus.Equals(String.Empty) || EditButtonEnabled; } }
        private long SecurityUserId { get { return Convert.ToInt64(System.Web.Security.Membership.GetUser().ProviderUserKey); } }
        protected bool IsOffCycle { get { return ((String)PayrollProcessRunTypeCode.Value).ToLower().Equals("offcycle"); } }
        protected bool EnableReportsMenu { get { return Common.Security.RoleForm.PayRegisterReportMenu; } }
        private bool RealFtpImportEnabled { get { return (Common.Security.RoleForm.RealFtpImport.ViewFlag); } }

        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.PayrollProcess.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                //load drop downs
                Common.CodeHelper.PopulateControl(PayrollProcessGroupCode, LanguageCode);
                PayrollProcessGroupCode.DataBind();

                Common.CodeHelper.PopulateControl(PayrollProcessRunTypeCode, LanguageCode);
                PayrollProcessRunTypeCode.DataBind();

                PayrollProcessRunTypeCode.Value = "REGULAR";

                //try to get last open payroll process
                LoadPayrollProcess(new PayrollProcessCriteria() { GetLastOpenPayrollProcessFlag = true });

                if (ProcessPending && PayrollProcess != null && PayrollProcess.PayrollProcessStatusCode != "NW" && PayrollProcess.PayrollProcessRunTypeCode != "ADJUST")
                    Response.Redirect(String.Format("~/HumanResources/PayrollProcessing/View/{0}", PayrollProcess.PayrollProcessId));
            }
        }
        protected void WireEvents()
        {
            PayrollProcessView.NeedDataSource += new WLPFormView.NeedDataSourceEventHandler(PayrollProcessView_NeedDataSource);
            PayrollProcessView.Updating += new WLPFormView.ItemUpdatingEventHandler(PayrollProcessView_Updating);

            if (!Common.ApplicationParameter.ClientDoesNotCalcPayroll)
            {
                //subscribe to refresh event from child
                this.PayrollBatchSearchResultsControl.RefreshParent += new PayrollBatch.PayrollBatchSearchResultsControl.RefreshParentEventHandler(RefreshParent);
            }
        }
        void RefreshParent(object sender, EventArgs e)
        {
            //call this to determine whether the user can see the Calc button or not
            PopulatePayrollBatches();
        }
        protected void PopulatePayrollBatches()
        {
            if (Data.Count > 0 && ((PayrollProcessCollection)Data)[0].PayrollProcessGroupCode != null && !Common.ApplicationParameter.ClientDoesNotCalcPayroll)
            {
                PayrollBatchSearchResultsControl.Visible = true;

                PayrollBatchCriteria payrollBatchCriteria = new PayrollBatchCriteria()
                {
                    PayrollProcessRunTypeCode = PayrollProcess.PayrollProcessRunTypeCode,
                    PayrollProcessGroupCode = PayrollProcess.PayrollProcessGroupCode,
                    ProcessedFlag = false
                };

                PayrollBatchReportCollection collection = Common.ServiceWrapper.PayrollBatchReport.Select(payrollBatchCriteria);
                PayrollBatchSearchResultsControl.SetPayrollBatchData(collection);
            }
            else
                PayrollBatchSearchResultsControl.Visible = false;
        }
        protected void LoadPayrollProcess(PayrollProcessCriteria criteria)
        {
            //try to get a payroll process
            Data = Common.ServiceWrapper.PayrollProcess.Select(criteria);

            if (PayrollProcess != null)
            {
                if (PayrollProcess.ProcessException != null && PayrollProcess.ProcessException.Length > 0)
                    lblTransMessage.Text = PayrollProcess.ProcessException;
                else
                    lblTransMessage.Text = "";

                PayrollProcessRunTypeCode.Value = PayrollProcess.PayrollProcessRunTypeCode;

                MergePayrollPeriod(new PayrollPeriodCriteria() { PayrollPeriodId = PayrollProcess.PayrollPeriodId });

                //default the dropdowns
                PayrollProcessGroupCode.Value = PayrollProcess.PayrollProcessGroupCode;

                if (!Common.ApplicationParameter.ClientDoesNotCalcPayroll)
                {
                    PayrollBatchSearchResultsControl.PayrollProcessControlPayProcessGrpCd = PayrollProcessGroupCode.Value.ToString();
                    PayrollBatchSearchResultsControl.PayrollProcessControlPayProcessRunTypeCd = PayrollProcessRunTypeCode.Value.ToString();
                }
            }
            else
                PayrollBatchSearchResultsControl.Visible = false;
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            SetAttributes();
        }
        private void SetAttributes()
        {
            //set form edit mode
            if (IsEditMode)
                PayrollProcessView.ChangeMode(System.Web.UI.WebControls.FormViewMode.Edit);
            else
                PayrollProcessView.ChangeMode(System.Web.UI.WebControls.FormViewMode.ReadOnly);

            PayrollProcessView.DataBind();

            TextBoxControl lastCalcDate = (TextBoxControl)PayrollProcessView.FindControl("LastCalcDate");

            if (lastCalcDate != null)
                lastCalcDate.Value = String.Format("{0:yyyy-MM-dd HH:mm}", PayrollProcess.LastCalcDate);

            //buttons
            ((WLPToolBarButton)PayrollProcessToolBar.FindButtonByCommandName("Post")).Enabled = PostButtonEnabled && (String)PayrollProcessGroupCode.Value != null;
            ((WLPToolBarButton)PayrollProcessToolBar.FindButtonByCommandName("Calculate")).Enabled = CalcButtonEnabled && (String)PayrollProcessGroupCode.Value != null;
            ((WLPToolBarButton)PayrollProcessToolBar.FindButtonByCommandName("Calculate")).Visible = !Common.ApplicationParameter.ClientDoesNotCalcPayroll;
            ((WLPToolBarButton)PayrollProcessToolBar.FindButtonByCommandName("Import")).Enabled = ImportButtonEnabled && (String)PayrollProcessGroupCode.Value != null;
            ((WLPToolBarButton)PayrollProcessToolBar.FindButtonByCommandName("Import")).Visible = Common.ApplicationParameter.ClientDoesNotCalcPayroll;
            ((WLPToolBarButton)PayrollProcessToolBar.FindButtonByCommandName("Recalculate")).Enabled = RecalculateButtonEnabled && (String)PayrollProcessGroupCode.Value != null;
            ((WLPToolBarButton)PayrollProcessToolBar.FindButtonByCommandName("Lock")).Visible = LockButtonVisible && (String)PayrollProcessGroupCode.Value != null;
            ((WLPToolBarButton)PayrollProcessToolBar.FindButtonByCommandName("Unlock")).Visible = UnlockButtonVisible && (String)PayrollProcessGroupCode.Value != null;
            ((WLPToolBarButton)PayrollProcessToolBar.FindButtonByCommandName("RealFtpImport")).Enabled = RealFtpImportEnabled && ((WLPToolBarButton)PayrollProcessToolBar.FindButtonByCommandName("Calculate")).Enabled;
            ((WLPToolBarButton)PayrollProcessToolBar.FindButtonByCommandName("RealFtpImport")).Visible = RealFtpImportEnabled;

            //dropdowns
            PayrollProcessRunTypeCode.ReadOnly = !IsEditMode;
            PayrollProcessGroupCode.ReadOnly = !IsEditMode;

            /*  New business rules:
                a)  if 'Run Type' dropdown = 'REGULAR', 'Ignore CPP/QPP Exceptions' = unchecked (false)
                b)  if 'Run Type' dropdown = 'OFFCYCLE', 'Ignore CPP/QPP Exceptions' = checked (true)
            */
            CheckBoxControl cppQppFlag = (CheckBoxControl)PayrollProcessView.FindControl("IgnoreCppQppExemptionFlag");
            if (cppQppFlag != null)
            {
                //only show "IgnoreCppQppExemptionFlag" checkbox if process group belongs to the country Canada && not import remittance only client
                if (PayrollProcess.CodeCountryCd == "CA" && !Common.ApplicationParameter.ClientDoesNotCalcPayroll)
                {
                    cppQppFlag.Visible = true;

                    if (PayrollProcessId == -1)
                    {
                        if (PayrollProcessRunTypeCode.Value != null && PayrollProcessRunTypeCode.Value.ToString() == "OFFCYCLE")
                            cppQppFlag.Checked = true;
                        else
                            cppQppFlag.Checked = false;
                    }
                }
                else
                {
                    //if not a Canadian payroll process group, uncheck and hide the "IgnoreCppQppExemptionFlag" control
                    cppQppFlag.Checked = false;
                    cppQppFlag.Visible = false;
                }
            }

            if (!Common.ApplicationParameter.ClientDoesNotCalcPayroll)
            {
                //find the toolbar in the PayrollBatchSearchResultsControl
                WLPToolBar payrollBatchSummaryToolBar = (WLPToolBar)PayrollBatchSearchResultsControl.FindControl("PayrollBatchSummaryToolBar");
                if (payrollBatchSummaryToolBar != null)
                {
                    //toggle the buttons based on IsEditMode, the Details will be enabled always.
                    ((WLPToolBarButton)payrollBatchSummaryToolBar.FindButtonByCommandName("Add")).Enabled = IsEditMode;
                    ((WLPToolBarButton)payrollBatchSummaryToolBar.FindButtonByCommandName("Delete")).Enabled = IsEditMode;
                }

                //set property to be used in javascript in PayrollBatchSearchResultsControl client side
                PayrollBatchSearchResultsControl.EnabledByParent = IsEditMode;
            }

            //Remove the scroll bar for this Control by overriding the RadPane settings in Site.Master.
            System.Web.UI.Control parentPane = PayrollProcessView.Parent.Parent.Parent.Parent;

            if (parentPane is RadPane)
                ((RadPane)parentPane).Scrolling = SplitterPaneScrolling.None;


            TextBoxControl payslipNote = (TextBoxControl)PayrollProcessView.FindControl("PayslipNote");
            if (payslipNote != null) payslipNote.Visible = !Common.ApplicationParameter.ClientDoesNotCalcPayroll;
        }
        protected BusinessLayer.BusinessObjects.PayrollProcess CreateNewDefaultPayrollProcess()
        {
            BusinessLayer.BusinessObjects.PayrollProcess process = new BusinessLayer.BusinessObjects.PayrollProcess();
            process.PayrollProcessStatusCode = _statusNew;
            return process;
        }
        protected void MergePayrollPeriod(PayrollPeriodCriteria criteria)
        {
            if (Data.Count == 0)
                ((PayrollProcessCollection)Data).Add(CreateNewDefaultPayrollProcess());

            PayrollPeriodCollection periods = Common.ServiceWrapper.PayrollPeriod.Select(criteria);

            if (periods.Count > 0)
            {
                periods[0].CopyTo(PayrollProcess);
                PayrollProcess.PayrollProcessRunTypeCode = PayrollProcessRunTypeCode.Value != null ? PayrollProcessRunTypeCode.Value.ToString() : null;
                PayrollProcess.CurrentPayrollYear = PayrollProcess.PPCurrentPayrollYear;
            }
            else
                (new PayrollPeriod()).CopyTo(PayrollProcess);

            PopulatePayrollBatches();
        }
        #endregion

        #region handles updates
        void PayrollProcessView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            BusinessLayer.BusinessObjects.PayrollProcess process = (BusinessLayer.BusinessObjects.PayrollProcess)e.DataItem;

            try
            {
                process.PayrollProcessRunTypeCode = (String)PayrollProcessRunTypeCode.Value;
                process.PayrollProcessRunTypeCodeDescription = (String)PayrollProcessRunTypeCode.TextValue;

                //set locked flag = true for a calc or a "lock".  For "unlock", set flag false
                process.LockedFlag = !UnlockButtonPressed;

                //only change status on a calc, not when "lock/unlock" is clicked
                if (!LockButtonPressed && !UnlockButtonPressed)
                    process.PayrollProcessStatusCode = _statusCalculationPending;

                //figure out which command button was clicked and set type
                BusinessLayer.BusinessObjects.PayrollProcess.CalculationProcess processType = BusinessLayer.BusinessObjects.PayrollProcess.CalculationProcess.Unknown;

                byte[] importFile = null;
                String importFileName = null;

                if (LockButtonPressed)
                    processType = BusinessLayer.BusinessObjects.PayrollProcess.CalculationProcess.Lock;
                else if (UnlockButtonPressed)
                    processType = BusinessLayer.BusinessObjects.PayrollProcess.CalculationProcess.Unlock;
                else if (CalculateButtonPressed)
                    processType = BusinessLayer.BusinessObjects.PayrollProcess.CalculationProcess.Calculate;
                else if (ReCalculateButtonPressed)
                    processType = BusinessLayer.BusinessObjects.PayrollProcess.CalculationProcess.ReCalculate;
                else if (PostButtonPressed)
                    processType = BusinessLayer.BusinessObjects.PayrollProcess.CalculationProcess.Post;
                else if (ImportButtonPressed)
                {
                    processType = BusinessLayer.BusinessObjects.PayrollProcess.CalculationProcess.Import;
                    // import button pressed, there must be a file
                    importFile = new byte[FileUpload.UploadedFiles[0].ContentLength];
                    FileUpload.UploadedFiles[0].InputStream.Read(importFile, 0, (int)FileUpload.UploadedFiles[0].ContentLength);
                    //saxon has an issue when a file has two spaces in a row, so now we are going to replace all spaces with underscores to avoid issues
                    importFileName = FileUpload.UploadedFiles[0].FileName.Replace(' ', '_');
                }

                //dont calculate if the lock/unlock button was pressed
                BusinessLayer.BusinessObjects.PayrollProcess updatedProcess = Common.ServiceWrapper.PayrollProcess.Update(process, processType, Common.ApplicationParameter.DatabaseVersion, (LockButtonPressed || UnlockButtonPressed), importFile, importFileName);
                updatedProcess.CopyTo(PayrollProcess);

                //if trying to perform a calcuation and no transactions exist, reset the status as new, not calc pending
                if (process.PayrollProcessStatusCode == _statusCalculationPending && process.Transactions != null && process.Transactions.Count == 0)
                    process.PayrollProcessStatusCode = _statusNew;

                if (ProcessPending)
                    Response.Redirect(String.Format("~/HumanResources/PayrollProcessing/View/{0}", PayrollProcess.PayrollProcessId));
                else if (!LockButtonPressed && !UnlockButtonPressed && process.PayrollProcessStatusCode != _statusCalculationProcessed)
                {
                    Response.Redirect(String.Format("~/HumanResources/PayrollProcessing/View/{0}", PayrollProcess.PayrollProcessId));
                    //                lblTransMessage.Text = "* " + (String)GetGlobalResourceObject("ErrorMessages", "NoTransactionToProcess");
                }
                else //refresh after MET calc
                     /*JOHN KLUDGE FOR BUILD 2.0.  Reporting after inserting a new payroll process was getting a payroll process id of -1*/
                    Response.Redirect("~/Payroll/PayrollProcess/PayrollProcessPage.aspx");
                //LoadPayrollProcess(new PayrollProcessCriteria() { GetLastOpenPayrollProcessFlag = true });
            }
            catch (System.ServiceModel.FaultException<PayrollValidationFault> ex)
            {
                process.PayrollProcessStatusCode = _statusNew;
                lblTransMessage.Text = "* " + ex.Message.Replace(Environment.NewLine, "<br/>");
            }
        }
        #endregion

        #region event handlers
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        void PayrollProcessView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            PayrollProcessView.DataSource = Data;
        }
        protected void ReportMenuButton_PreRender(object sender, EventArgs e)
        {
            ((WLPToolBarButton)sender).Enabled = ViewReportsButtonEnabled && (String)PayrollProcessGroupCode.Value != null;
            ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableReportsMenu;
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            //called after "Yes" on the confirmation box is clicked.
            PopulatePayrollBatches();
        }
        protected void PayrollProcessToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            WLPToolBarButton button = ((WLPToolBarButton)e.Item);

            if (button.CommandName.ToLower().Equals("calculate"))
            {
                CalculateButtonPressed = true;
                //save/calc PayrollProcess
                PayrollProcessView.UpdateItem(true);
            }
            else if (button.CommandName.ToLower().Equals("post"))
            {
                PostButtonPressed = true;

                //only populate if using RBC_EDI eft type or if using pre-authorized debit for RBC(or both)
                RbcEftEdiParameters ediParms = null;
                if (Common.ApplicationParameter.EFTType == "RBC_EDI" || (Common.ApplicationParameter.PreAuthorizedDebitFlag && Common.ApplicationParameter.PreAuthorizedDebitFileType == "RBC_CANADA"))
                    ediParms = Common.ParameterHelper.CreateAndPopulateRbcDirectDepositChequeParameters(Common.ApplicationParameter.EFTType);

                //only populate if using SCOT_EDI eft type or if using pre-authorized debit for SCOTIABANK(or both)
                ScotiaBankEdiParameters scotiaEdiParms = null;
                if (Common.ApplicationParameter.EFTType == "SCOT_EDI" || (Common.ApplicationParameter.PreAuthorizedDebitFlag && Common.ApplicationParameter.PreAuthorizedDebitFileType == "SCOTIA_JAMAICA"))
                    scotiaEdiParms = Common.ParameterHelper.CreateAndPopulateScotiaBankDirectDepositParameters(Common.ApplicationParameter.EFTType);

                //only populate if using CRA Garnishments
                RbcEftSourceDeductionParameters garnishmentParms = null;
                if (Common.ApplicationParameter.CraGarnishmentRemitFlag)
                    garnishmentParms = Common.ParameterHelper.CreateAndPopulateRbcSourceDeductionParameters(Common.ApplicationParameter.GarnishmentEftType);

                //only populate if sending an invoice
                SendEmailParameters emailParms = null;
                if (Common.ApplicationParameter.SendInvoice)
                    emailParms = Common.ParameterHelper.CreateAndPopulateEmailParameters();

                Common.ServiceWrapper.PayrollProcess.PostTransaction(PayrollProcess, _statusProcessed, ediParms, scotiaEdiParms, emailParms, garnishmentParms, Common.ApplicationParameter.CraRemitFlag, Common.ApplicationParameter.RqRemitFlag, Common.ApplicationParameter.SendInvoice, Common.ApplicationParameter.CraGarnishmentRemitFlag, Common.ApplicationParameter.WcbChequeRemitFlag, Common.ApplicationParameter.HealthTaxChequeRemitFlag, Common.ApplicationParameter.AutoAddSecondaryPositions).CopyTo(PayrollProcess);
                LoadPayrollProcess(new PayrollProcessCriteria() { GetLastOpenPayrollProcessFlag = true });
                PayrollProcessGroupCode.Value = null;
                PayrollProcessView.DataBind();
            }
            else if (button.CommandName.ToLower().Equals("recalculate"))
            {
                ReCalculateButtonPressed = true;
                PayrollProcess.PayrollProcessStatusCode = _statusNew;
                Common.ServiceWrapper.PayrollProcess.Update(PayrollProcess, BusinessLayer.BusinessObjects.PayrollProcess.CalculationProcess.ReCalculate).CopyTo(PayrollProcess);
                Response.Redirect("~/Payroll/PayrollProcess/PayrollProcessPage.aspx");
            }
            else if (button.CommandName.ToLower().Equals("lock"))
            {
                LockButtonPressed = true;
                PayrollProcessView.UpdateItem(true);
            }
            else if (button.CommandName.ToLower().Equals("unlock"))
            {
                UnlockButtonPressed = true;
                PayrollProcessView.UpdateItem(true);
            }
            else if (button.CommandName.ToLower().Equals("realftpimport"))
            {
                Common.ServiceWrapper.XmlClient.ImportBatchesViaFtp
                (
                    "STDPAYIMP",
                    Common.ApplicationParameter.ImportExportProcessingDirectory,
                    Convert.ToBoolean(Common.ApplicationParameter.ImportDirtySave),
                    Common.ApplicationParameter.AutoAddSecondaryPositions,
                    Common.ApplicationParameter.RealFtp,
                    Common.ApplicationParameter.RealFtpFilePatternToMatch,
                    PayrollProcess.PayrollProcessRunTypeCode,
                    PayrollProcess.PayrollProcessGroupCode
                );

                //refresh the batch results
                PopulatePayrollBatches();
            }
        }
        protected void PayrollProcessCriteria_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //load criteria based on dropdowns
            PayrollPeriodCriteria criteria = new PayrollPeriodCriteria();
            criteria.PayrollProcessGroupCode = (String)PayrollProcessGroupCode.Value;

            if (!Common.ApplicationParameter.ClientDoesNotCalcPayroll)
            {
                PayrollBatchSearchResultsControl.PayrollProcessControlPayProcessGrpCd = criteria.PayrollProcessGroupCode;
                PayrollBatchSearchResultsControl.PayrollProcessControlPayProcessRunTypeCd = (String)PayrollProcessRunTypeCode.Value;
            }

            if (PayrollProcessRunTypeCode.Value != null)
            {
                if ((String)PayrollProcessRunTypeCode.Value != "ADJUST")
                {
                    criteria.GetMinOpenPeriodFlag = !IsOffCycle;
                    criteria.GetMaxClosedPeriodFlag = IsOffCycle;
                }
                else
                {
                    //adjustments use the previous closed payroll period
                    criteria.GetMinOpenPeriodFlag = false;
                    criteria.GetMaxClosedPeriodFlag = true;
                }
            }

            MergePayrollPeriod(criteria);

            if (criteria.PayrollProcessGroupCode == null)
                PayrollProcessView.Visible = false;
            else
                PayrollProcessView.Visible = true;
        }
        #endregion

        protected void RadAsyncUpload1_FileUploaded(object sender, FileUploadedEventArgs e)
        {
        }

        protected void Upload_Click(object sender, EventArgs e)
        {
            //placeholder to trigger post
        }

        protected void FileUpload_FileUploaded(object sender, FileUploadedEventArgs e)
        {
            ImportButtonPressed = true;
            PayrollProcessView.UpdateItem(true);


            //            //extract file
            //            byte[] fileData = new byte[e.File.ContentLength];
            //            e.File.InputStream.Read(fileData, 0, (int)e.File.ContentLength);

            //            PayrollProcess.PayrollProcessStatusCode = _statusCalculationPending;
            //            //dont calculate if the lock/unlock button was pressed
            //            int i = 0;
            ////            BusinessLayer.BusinessObjects.PayrollProcess updatedProcess = Common.ServiceWrapper.PayrollProcess.Update(PayrollProcess, (!LockButtonPressed && !UnlockButtonPressed), UserEmailAddress, Common.ApplicationParameter.DatabaseVersion, (LockButtonPressed || UnlockButtonPressed));


            //            Response.Redirect("~/Payroll/PayrollProcess/PayrollProcessPage.aspx");
        }
    }
}