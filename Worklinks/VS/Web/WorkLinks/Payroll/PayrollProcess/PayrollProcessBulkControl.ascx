﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayrollProcessBulkControl.ascx.cs" Inherits="WorkLinks.Payroll.PayrollProcess.PayrollProcessBulkControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<table>
    <tr>
        <td>
            <asp:Button ID="Refresh" runat="server" Text="Refresh" OnClick="Refresh_Click" />
        </td>
        <td>
            <asp:Button ID="Process" runat="server" Text="Process" OnClick="Calc_Click" />
        </td>
        <td>
            <asp:Button ID="Recalc" runat="server" Text="Recalc" OnClick="ReCalc_Click" />
        </td>
        <td>
            <asp:Button ID="Post" runat="server" Text="Post" OnClick="Post_Click" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <wasp:WLPGrid
                ID="BulkPayrollProcessGrid"
                runat="server"
                AllowSorting="true"
                GridLines="None"
                Width="100%"
                AllowMultiRowSelection="true"
                AutoAssignModifyProperties="true"
                ShowFooter="true">

                <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />  
                </ClientSettings>

                <MasterTableView ShowGroupFooter="true" ClientDataKeyNames="PayrollPeriodId" AutoGenerateColumns="false" DataKeyNames="PayrollPeriodId" CommandItemDisplay="Top">
                    <CommandItemTemplate></CommandItemTemplate>

                    <Columns>
                        <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
                            <HeaderStyle Width="6%" />
                        </telerik:GridClientSelectColumn>
                        <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="Payroll" SortExpression="PayrollProcessGroupCode" DataField="PayrollProcessGroupCode" Type="PayrollProcessGroupCode" ResourceName="PayrollProcessGroupCode"></wasp:GridKeyValueControl>
                        <wasp:GridNumericControl HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" Aggregate="Sum" DataFormatString="{0:###,##0}" DataField="EmployeeCount" LabelText="Pensioners" FooterAggregateFormatString="Total pensioners: {0:###,##0}" SortExpression="EmployeeCount" UniqueName="EmployeeCount" ResourceName="EmployeeCount"></wasp:GridNumericControl>
                        <wasp:GridNumericControl HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" Aggregate="Sum" DataFormatString="{0:###,##0}" DataField="TransactionCount" LabelText="Payroll Transactions" FooterAggregateFormatString="Total payroll transactions: {0:###,##0}" SortExpression="TransactionCount" UniqueName="TransactionCount" ResourceName="TransactionCount"></wasp:GridNumericControl>
                        <wasp:GridNumericControl HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" Aggregate="Sum" DataFormatString="{0:###,##0}" DataField="StatutoryDeductionCount" LabelText="TD1 Changes" FooterAggregateFormatString="Total TD1 changes: {0:###,##0}" SortExpression="StatutoryDeductionCount" UniqueName="StatutoryDeductionCount" ResourceName="StatutoryDeductionCount"></wasp:GridNumericControl>
                        <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="Status" SortExpression="PayrollProcessStatusCode" DataField="PayrollProcessStatusCode" Type="PayrollProcessStatusCode" ResourceName="PayrollProcessStatusCode"></wasp:GridKeyValueControl>
                    </Columns>
                </MasterTableView>

                <HeaderContextMenu EnableAutoScroll="true"></HeaderContextMenu>

            </wasp:WLPGrid>
        </td>
    </tr>
</table>