﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.PayrollProcess
{
    public partial class PayrollProcessSearchControl : WLP.Web.UI.WLPUserControl
    {
        public enum SearchLocationType
        {
            Employee,
            Payroll,
        }

        #region properties

        #region report menu items
        protected bool EnableReportsMenu { get { return Common.Security.RoleForm.PayRegisterReportMenu; } }
        protected bool EnableStandardMenuItem { get { return Common.Security.RoleForm.PayRegisterReportStandardMenu; } }
        protected bool EnableCustomMenuItem { get { return Common.Security.RoleForm.PayRegisterReportCustomMenu; } }
        protected bool EnableCustomerInvoiceReportMenuItem { get { return Common.Security.RoleForm.InvoiceCanada.ViewFlag; } }
        protected bool EnableEftCustomerInvoiceJamaicaReportMenuItem { get { return Common.Security.RoleForm.InvoiceJamaica.ViewFlag; } }
        protected bool EnablePayRegisterReportMenuItem { get { return Common.Security.RoleForm.PayrollPayRegisterDetails.ViewFlag; } }
        protected bool EnableRegisterSummaryReportMenuItem { get { return Common.Security.RoleForm.PayrollPayRegisterSummary.ViewFlag; } }
        protected bool EnableChangesReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterChangesReport.ViewFlag; } }
        protected bool EnableCompensationListReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterCompensationList.ViewFlag; } }
        protected bool EnableCurrentVsPriorReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterCurrentVsPrior.ViewFlag; } }
        protected bool EnableCurrentVsPriorNetReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterCurrentVsPriorNet.ViewFlag; } }
        protected bool EnableCurrentVsPriorPeriodVariancesMenuItem { get { return Common.Security.RoleForm.PayrollRegisterCurrentVsPriorPeriodVariances.ViewFlag; } }
        protected bool EnableGarnishmentReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterGarnishment.ViewFlag; } }
        protected bool EnablePayExceptionReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterPayException.ViewFlag; } }
        protected bool EnableGrossVsSalaryReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterGrossVsSalary.ViewFlag; } }
        protected bool EnablePD7AReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterPD7A.ViewFlag; } }
        protected bool EnableBarbadosDeductionSummaryMenuItem { get { return Common.Security.RoleForm.BarbadosDeductionSummary.ViewFlag; } }
        protected bool EnableStLuciaDeductionSummaryMenuItem { get { return Common.Security.RoleForm.StLuciaDeductionSummary.ViewFlag; } }
        protected bool EnableTrinidadDeductionSummaryMenuItem { get { return Common.Security.RoleForm.TrinidadDeductionSummary.ViewFlag; } }
        protected bool EnableJamaicaDeductionSummaryMenuItem { get { return Common.Security.RoleForm.JamaicaDeductionSummary.ViewFlag; } }
        protected bool EnableJamaicaGovMonthlyRemittanceMenuItem { get { return Common.Security.RoleForm.JamaicaGovernmentMonthlyRemittance.ViewFlag; } }
        protected bool EnableYtdBonusCommissionReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterYtdBonusCommission.ViewFlag; } }
        protected bool EnablePensionReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterPensionReport.ViewFlag; } }
        protected bool EnableWCBReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterWCB.ViewFlag; } }
        protected bool EnablePayDetailsMenuItem { get { return Common.Security.RoleForm.PayrollRegisterPayDetails.ViewFlag; } }
        protected bool EnableGLDetailsMenuItem { get { return Common.Security.RoleForm.PayrollRegisterGLDetails.ViewFlag; } }
        protected bool EnableFedProvRemitSummaryMenuItem { get { return Common.Security.RoleForm.PayrollRegisterFedProvRemitSummary.ViewFlag; } }
        protected bool EnableProvincialHealthSummaryMenuItem { get { return Common.Security.RoleForm.PayrollRegisterProvHealthSummary.ViewFlag; } }
        protected bool EnableResearchAndDevelopmentMenuItem { get { return Common.Security.RoleForm.ResearchAndDevelopment.ViewFlag; } }
        protected bool EnableQuebecRemitSummaryMenuItem { get { return Common.Security.RoleForm.PayrollRegisterQuebecRemitSummary.ViewFlag; } }
        protected bool EnablePreYET4ADetailMenuItem { get { return Common.Security.RoleForm.PayrollRegisterPreYET4ADetail.ViewFlag; } }
        protected bool EnablePreYET4DetailMenuItem { get { return Common.Security.RoleForm.PayrollRegisterPreYET4Detail.ViewFlag; } }
        protected bool EnablePreYER1DetailMenuItem { get { return Common.Security.RoleForm.PayrollRegisterPreYER1Detail.ViewFlag; } }
        protected bool EnablePreYER2DetailMenuItem { get { return Common.Security.RoleForm.PayrollRegisterPreYER2Detail.ViewFlag; } }
        protected bool EnablePIERPreYEMenuItem { get { return Common.Security.RoleForm.PayrollRegisterPIERPreYE.ViewFlag; } }
        protected bool EnableBenefitArrearsMenuItem { get { return Common.Security.RoleForm.PayrollRegisterBenefitArrears.ViewFlag; } }
        protected bool EnableExportMenu { get { return Common.Security.RoleForm.ExportMenu; } }
        protected bool EnableCSBReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterCSB.ViewFlag; } }
        protected bool EnableEmployeeSalaryReportMenuItem { get { return Common.Security.RoleForm.EmployeeSalaryReport.ViewFlag; } }
        protected bool EnableUnitedWayReportMenuItem { get { return Common.Security.RoleForm.UnitedWayReport.ViewFlag; } }
        protected bool EnableSerpYtdReportMenuItem { get { return Common.Security.RoleForm.SerpYtdReport.ViewFlag; } }
        protected bool EnablePensionDemographicsMenuItem { get { return Common.Security.RoleForm.PensionDemographicsReport.ViewFlag; } }
        protected bool EnableSrpReportMenuItem { get { return Common.Security.RoleForm.SrpReport.ViewFlag; } }
        protected bool EnablePreYearEndDetailMenuItem { get { return Common.Security.RoleForm.PreYearEndDetail.ViewFlag; } }
        protected bool EnableVacationReportMenuItem { get { return Common.Security.RoleForm.VacationReport.ViewFlag; } }
        protected bool EnablePaymentListReportMenuItem { get { return Common.Security.RoleForm.PaymentListReport.ViewFlag; } }
        protected bool EnableA1235GLDetailsReportMenuItem { get { return Common.Security.RoleForm.A1235GLDetailsReport.ViewFlag; } }
        protected bool EnableCurrentVsPriorTwoPeriodVariances { get { return Common.Security.RoleForm.CurrentVsPriorTwoPeriodVariances.ViewFlag; } }
        protected bool EnableMonthlyStatutoryRemittance { get { return Common.Security.RoleForm.MonthlyStatutoryRemittance.ViewFlag; } }
        protected bool EnableAccumulatedHoursReportMenuItem => Common.Security.RoleForm.AccumulatedHoursReportMenuItem.ViewFlag;
        protected bool EnablePayrollDetailsOrgUnitMenuItem { get { return Common.Security.RoleForm.PayrollDetailsOrgUnit.ViewFlag; } }
        #endregion

        protected PayrollProcessCriteria Criteria
        {
            get
            {
                PayrollProcessCriteria criteria = new PayrollProcessCriteria();
                criteria.PayrollProcessGroupCode = (String)PayrollProcessGroupCodeControl.Value;
                criteria.PayrollProcessRunTypeCode = (String)PayrollProcessRunTypeCodeControl.Value;

                return criteria;
            }
            set
            {
                PayrollProcessGroupCodeControl.Value = value.PayrollProcessGroupCode;
                PayrollProcessRunTypeCodeControl.Value = value.PayrollProcessRunTypeCode;
            }
        }
        public SearchLocationType SearchLocation { get; set; }
        protected bool EnablePaySlipButton { get { return Common.Security.RoleForm.PayrollPayRegisterPaySlip.ViewFlag; } }
        protected bool EnableMassPayslipPrintButton { get { return Common.Security.RoleForm.PayrollPayRegisterMassPaySlipPrint.ViewFlag; } }
        protected bool EnableCeridianExportButton { get { return Common.Security.RoleForm.PayrollCeridianExportButton.ViewFlag; } }
        protected bool EnableTransmitFilesButton { get { return Common.Security.RoleForm.PayrollPayRegisterTransmitFiles.ViewFlag; } }
        protected bool EnableEFTButton { get { return Common.Security.RoleForm.EFTButton.ViewFlag; } }
        protected bool EnablePensionExportButton { get { return Common.Security.RoleForm.PensionExportButton.ViewFlag; } }
        protected bool EnableCSBExportButton { get { return Common.Security.RoleForm.CSBExport.ViewFlag; } }
        protected bool EnableGLButton { get { return Common.Security.RoleForm.GLButton.ViewFlag; } }
        protected bool EnableRRSPButton { get { return Common.Security.RoleForm.RRSPButton.ViewFlag; } }
        protected bool EnableStockButton { get { return Common.Security.RoleForm.StockButton.ViewFlag; } }
        protected bool EnableEmpowerRemittanceButton { get { return Common.Security.RoleForm.EmpowerRemittanceButton.ViewFlag; } }
        protected bool EnableEmpowerGarnishmentButton { get { return Common.Security.RoleForm.EnableEmpowerGarnishmentButton.ViewFlag; } }
        protected bool EnableCICPlusWeeklyButton { get { return Common.Security.RoleForm.CICPlusWeeklyButton.ViewFlag; } }
        protected bool EnableCraGarnishmentButton { get { return Common.Security.RoleForm.CraGarnishmentButton.ViewFlag; } }
        protected bool EnableSocialCostsButton { get { return Common.Security.RoleForm.SocialCostsExport.ViewFlag; } }
        protected bool EnableRealCustomExportButton { get { return Common.Security.RoleForm.RealCustomExport.ViewFlag; } }
        

        protected bool EnableEFTPayment { get { return Common.Security.RoleForm.EFTPayment.ViewFlag; } }
        protected bool EnableRealExport { get { return Common.Security.RoleForm.RealExport.ViewFlag; } }
        protected String languageCode { get { return this.LanguageCode; } }
        protected String EmptyColumnText { get { return ""; } }
        public DateTime CeridianExportSupportDate { get { return Convert.ToDateTime(Common.CodeHelper.GetCodeDescription("EN", CodeTableType.System, "CERD_DT")); } }
        public String MonthlyStatutoryRemittanceURL { get { return Common.CodeHelper.GetCodeDescription("EN", CodeTableType.System, "MSR_URL"); } }

        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.PayrollPayments.ViewFlag);

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];
            String employeeNumber = null;

            //this code is only executed if redirected from EmployeePayslipControl to run an EFT for a SINGLE employee; 0 arg is processId, 1 is employeeNumber
            if (Request.QueryString.Count == 2)
            {
                args = "eftpayrollprocessid=" + Request.QueryString[0];
                employeeNumber = Request.QueryString[1];
            }

            if (args != null && args.Equals(String.Format("refresh")))
                RefreshGrid();
            else if (args != null && args.Length > 20 && args.Substring(0, 20).ToLower().Equals("eftpayrollprocessid="))
            {
                long payrollprocessid = Convert.ToInt64(args.Substring(20));
                String eftType = Common.ApplicationParameter.EFTType;

                if (eftType == "CITI")
                {
                    //load the EFT from the db if it exists, otherwise create it on the database and send it to the user
                    byte[] file = Common.ServiceWrapper.HumanResourcesClient.CreateCITIeft(payrollprocessid, Common.ApplicationParameter.EFTClientNumber, employeeNumber, Common.ParameterHelper.DetermineEftType(eftType)) ?? new byte[0];

                    Response.Clear();

                    //this will prompt the user to open or download the file
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition", "attachment;filename=EFT" + String.Format("{0:yyyyMMdd}-{1}.txt", DateTime.Now, payrollprocessid));
                    Response.AddHeader("Content-Length", file.Length.ToString());

                    Response.OutputStream.Write(file, 0, file.Length);
                    Response.Flush();
                    Response.End();
                }
                if (eftType == "HSBC")
                {
                    //load the EFT from the db if it exists, otherwise create it on the database and send it to the user
                    byte[] file = Common.ServiceWrapper.HumanResourcesClient.CreateHSBCEft(payrollprocessid, employeeNumber, Common.ParameterHelper.DetermineEftType(eftType)) ?? new byte[0];

                    Response.Clear();

                    //this will prompt the user to open or download the file
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition", "attachment;filename=EFT" + String.Format("{0:yyyyMMdd}-{1}.txt", DateTime.Now, payrollprocessid));
                    Response.AddHeader("Content-Length", file.Length.ToString());

                    Response.OutputStream.Write(file, 0, file.Length);
                    Response.Flush();
                    Response.End();
                }
                else if (eftType == "CITI_EFT")
                {
                    //load the EFT from the db if it exists, otherwise create it on the database and send it to the user
                    byte[] file = Common.ServiceWrapper.HumanResourcesClient.CreateCitiAchFile(payrollprocessid, Common.ApplicationParameter.EFTClientNumber, Common.ApplicationParameter.CompanyName, Common.ApplicationParameter.CompanyShortName, employeeNumber, Common.ParameterHelper.DetermineEftType(eftType)) ?? new byte[0];

                    Response.Clear();

                    String eftFileName = Common.ServiceWrapper.HumanResourcesClient.GetCitiNamingFormat(Common.ApplicationParameter.CitiEftFileName, payrollprocessid);

                    //this will prompt the user to open or download the file
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + eftFileName);
                    Response.AddHeader("Content-Length", file.Length.ToString());

                    Response.OutputStream.Write(file, 0, file.Length);
                    Response.Flush();
                    Response.End();
                }
                else if (eftType == "CITI_EFT_CHEQ")
                {
                    //load the EFT from the db if it exists, otherwise create it on the database and send it to the user
                    byte[] file = Common.ServiceWrapper.HumanResourcesClient.CreateCitiEftAndChequeFile(payrollprocessid, Common.ApplicationParameter.CitiEftFileName, Common.ApplicationParameter.CitiChequeFileName, Common.ApplicationParameter.EFTClientNumber, Common.ApplicationParameter.CitiChequeAccountNumber, Common.ApplicationParameter.CompanyName, Common.ApplicationParameter.CompanyShortName, employeeNumber, Common.ApplicationParameter.EftOrginatorId, Common.ParameterHelper.DetermineEftType(eftType)) ?? new byte[0];

                    Response.Clear();

                    String zipFileName = Common.ServiceWrapper.HumanResourcesClient.GetCitiNamingFormat(Common.ApplicationParameter.CitiEftZipFileName, payrollprocessid);

                    //this will prompt the user to open or download the file
                    Response.ContentType = "application/zip";
                    Response.AppendHeader("content-disposition", "attachment; filename=" + zipFileName);
                    Response.AddHeader("Content-Length", file.Length.ToString());

                    Response.OutputStream.Write(file, 0, file.Length);
                    Response.Flush();
                    Response.End();
                }
                else if (eftType == "CITI_CHEQ")
                {
                    //load the EFT from the db if it exists, otherwise create it on the database and send it to the user
                    byte[] file = Common.ServiceWrapper.HumanResourcesClient.CreateCitiChequeFile(payrollprocessid, Common.ApplicationParameter.EFTClientNumber, Common.ApplicationParameter.CompanyShortName, employeeNumber, Common.ApplicationParameter.EftOrginatorId, Common.ParameterHelper.DetermineEftType(eftType)) ?? new byte[0];

                    Response.Clear();

                    String chequeFileName = Common.ServiceWrapper.HumanResourcesClient.GetCitiNamingFormat(Common.ApplicationParameter.CitiChequeFileName, payrollprocessid);

                    //this will prompt the user to open or download the file
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + chequeFileName);
                    Response.AddHeader("Content-Length", file.Length.ToString());

                    Response.OutputStream.Write(file, 0, file.Length);
                    Response.Flush();
                    Response.End();
                }
                else if (eftType == "RBC_EDI")
                {
                    //load the EFT from the db (it would be created on the POST) and send it to the user
                    byte[] eftTextFile = Common.ServiceWrapper.HumanResourcesClient.GenerateRBCEftEdiFile(payrollprocessid, Common.ParameterHelper.CreateAndPopulateRbcDirectDepositChequeParameters(eftType), employeeNumber) ?? new byte[0];

                    String fileName = String.Format("{0}_{1}.txt", DateTime.Now.ToString("yyyyMMddHHmmssfff"), "EFT");

                    Response.Clear();

                    //this will prompt the user to open or download the file
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", fileName));
                    Response.AddHeader("Content-Length", eftTextFile.Length.ToString());

                    Response.OutputStream.Write(eftTextFile, 0, eftTextFile.Length);
                    Response.Flush();
                    Response.End();
                }
                else if (eftType == "SCOT_EDI")
                {
                    //load the EFT from the db (it would be created on the POST) and send it to the user
                    byte[] eftTextFile = Common.ServiceWrapper.HumanResourcesClient.GenerateScotiaBankEdiFile(payrollprocessid, Common.ParameterHelper.CreateAndPopulateScotiaBankDirectDepositParameters(eftType), employeeNumber) ?? new byte[0];

                    String fileName = String.Format("{0}_{1}.txt", DateTime.Now.ToString("yyyyMMddHHmmssfff"), "EFT");

                    Response.Clear();

                    //this will prompt the user to open or download the file
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", fileName));
                    Response.AddHeader("Content-Length", eftTextFile.Length.ToString());

                    Response.OutputStream.Write(eftTextFile, 0, eftTextFile.Length);
                    Response.Flush();
                    Response.End();
                }
                else if (eftType == "RBC_ACH")
                {
                    //load the EFT from the db if it exists, otherwise create it on the database and send it to the user
                    byte[] eftTextFile = Common.ServiceWrapper.HumanResourcesClient.GenerateEftFile(payrollprocessid, Common.ApplicationParameter.EFTClientNumber, Common.ApplicationParameter.EFTTransmissionFlag, Common.ApplicationParameter.EFTFileIndicator, Common.ApplicationParameter.CompanyName, Common.ApplicationParameter.CompanyShortName, employeeNumber, Common.ParameterHelper.DetermineEftType(eftType)) ?? new byte[0];
                    String fileName = String.Format("{0}_{1}.txt", DateTime.Now.ToString("yyyyMMddHHmmssfff"), "EFT");

                    Response.Clear();

                    //this will prompt the user to open or download the file
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", fileName));
                    Response.AddHeader("Content-Length", eftTextFile.Length.ToString());

                    Response.OutputStream.Write(eftTextFile, 0, eftTextFile.Length);
                    Response.Flush();
                    Response.End();
                }
            }
            else if (args != null && args.Length > 24 && args.Substring(0, 24).ToLower().Equals("pensionpayrollprocessid="))
            {
                long payrollprocessid = Convert.ToInt64(args.Substring(24));
                byte[] file = null;
                String pensionType = Common.CodeHelper.GetCodeDescription("EN", CodeTableType.System, "PENSTYPE");
                String fileType = "";

                //load an object from the db and create a pension file
                if (pensionType.ToLower() == "mercer")
                {
                    file = Common.ServiceWrapper.ReportClient.CreateMercerPensionExportFile(payrollprocessid, Common.ApplicationParameter.ReportShowDescriptionField, Common.ApplicationParameter.ReportPayRegisterSortOrder) ?? new byte[0];
                    fileType = ".xls";
                }
                else
                {
                    file = Common.ServiceWrapper.HumanResourcesClient.CreatePensionExportFile(payrollprocessid) ?? new byte[0];
                    fileType = ".txt";
                }

                Response.Clear();

                Response.ContentType = "application/octet-stream";

                if (Common.ApplicationParameter.ReportPensionExportFileFormat.Equals(String.Empty))
                    Response.AddHeader("Content-Disposition", "attachment;filename=PENSION" + String.Format("{0:yyyyMMdd}-{1}" + fileType, DateTime.Now, payrollprocessid));
                else
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + Common.ServiceWrapper.HumanResourcesClient.GenerateFileName(Common.ApplicationParameter.ReportPensionExportFileFormat, payrollprocessid));

                Response.AddHeader("Content-Length", file.Length.ToString());

                Response.OutputStream.Write(file, 0, file.Length);
                Response.Flush();
                Response.End();
            }
            else if (args != null && args.Length > 26 && args.Substring(0, 26).ToLower().Equals("exportftppayrollprocessid="))
            {
                long payrollProcessId = -1;
                String periodYear = "";

                seperateFtpArgs(args, out payrollProcessId, out periodYear);

                //load our export queue objects
                PayRegisterExportCollection collection = Common.ServiceWrapper.HumanResourcesClient.GetPayRegisterExport(null, "SFTP");

                //get pension type
                String pensionType = Common.CodeHelper.GetCodeDescription("EN", CodeTableType.System, "PENSTYPE");

                //get eft value from the code system table and determine which eft type to send to the database
                String eftTypeForDb = Common.ParameterHelper.DetermineEftType(Common.CodeHelper.GetCodeDescription("EN", CodeTableType.System, "EFTTYPE"));

                //call a method to cycle thru the reports/exports
                if (collection != null && collection.Count > 0)
                    Common.ServiceWrapper.ReportClient.CreateReportsAndExports(payrollProcessId, periodYear, collection, Common.ApplicationParameter.PexFtp, Common.ApplicationParameter.ReportShowDescriptionField, Common.ApplicationParameter.ReportPayRegisterSortOrder, Common.ApplicationParameter.RevenueQuebecTaxId, Common.ApplicationParameter.EmpowerUseQuebecBusinessNumber, Common.ApplicationParameter.CitiEftFileName, Common.ApplicationParameter.CitiChequeFileName, Common.ApplicationParameter.EFTClientNumber, Common.ApplicationParameter.CitiChequeAccountNumber, Common.ApplicationParameter.CompanyName, Common.ApplicationParameter.CompanyShortName, Common.ApplicationParameter.MassPayslipInternalFileFormat, Common.ApplicationParameter.EftOrginatorId, eftTypeForDb, Common.ApplicationParameter.MaxDegreeOfParallelism);

                //refresh the grid so the date shows
                RefreshGrid();
            }
            else if (args != null && args.Length > 17 && args.Substring(0, 17).ToLower().Equals("payrollprocessid="))
            {
                long payrollprocessid = Convert.ToInt64(args.Substring(17));
                byte[] file = Common.ServiceWrapper.ReportClient.GetReportForAllEmployeesOfThisPayProcess(Common.ApplicationParameter.MassPayslipInternalFileFormat, payrollprocessid, Common.ApplicationParameter.ReportShowDescriptionField, Common.ApplicationParameter.ReportPayRegisterSortOrder, Common.ApplicationParameter.MaxDegreeOfParallelism) ?? new byte[0];
                String zipFileName = Common.ServiceWrapper.HumanResourcesClient.GenerateFileName(Common.ApplicationParameter.MassPayslipFileZipFormat, payrollprocessid);

                Response.Clear();

                Response.ContentType = "application/zip";
                Response.AppendHeader("content-disposition", "attachment; filename=" + zipFileName);

                Response.OutputStream.Write(file, 0, file.Length);
                Response.Flush();
                Response.End();
            }
            else if (args != null && args.Length > 23 && args.Substring(0, 23).ToLower().Equals("exportpayrollprocessid="))
            {
                long exportPayrollprocessid = -1;
                String periodYear = "";
                String period = "";

                period = seperateArgs(args, out exportPayrollprocessid, out periodYear);

                //call the following functions to produce text files: DynamicsManagement -->GetCeridianBilling, GetCeridianCheque, GetCeridianStatement
                //if the function returns null or empty string - skip producing the file
                byte[] file = Common.ServiceWrapper.ReportClient.ExportCeridianPayProcess(exportPayrollprocessid, Common.ApplicationParameter.OntarioEHTAmount, Common.ApplicationParameter.CeridianCompanyNumber, Common.ApplicationParameter.CompanyName, Common.ApplicationParameter.CeridianBillingProcessType, Common.ApplicationParameter.WCBStartDate) ?? new byte[0];

                Response.Clear();

                Response.ContentType = "application/zip";
                Response.AppendHeader("content-disposition", "attachment; filename=" + String.Format("{0}{1}{2}.zip", Common.ApplicationParameter.CeridianCompanyNumber, periodYear, period));

                Response.OutputStream.Write(file, 0, file.Length);
                Response.Flush();
                Response.End();
            }
            else if (args != null && args.Length > 21 && args.Substring(0, 21).ToLower().Equals("rrsppayrollprocessid="))
            {
                long payrollprocessid = Convert.ToInt64(args.Substring(21));

                //load an object from the db and create an export file
                byte[] file = Common.ServiceWrapper.HumanResourcesClient.CreateRRSPExport(payrollprocessid, Common.ApplicationParameter.RRSPHeader, Common.ApplicationParameter.RRSPGroupNumber) ?? new byte[0];

                Response.Clear();

                //this will prompt the user to open or download the file
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment;filename=RRSP" + String.Format("{0:yyyyMMdd}-{1}.txt", DateTime.Now, payrollprocessid));
                Response.AddHeader("Content-Length", file.Length.ToString());

                Response.OutputStream.Write(file, 0, file.Length);
                Response.Flush();
                Response.End();
            }
            else if (args != null && args.Length > 31 && args.Substring(0, 31).ToLower().Equals("cragarnishmentpayrollprocessid="))
            {
                //load the EFT from the db (it would be created on the POST) and send it to the user
                long payrollprocessid = Convert.ToInt64(args.Substring(31));
                byte[] file = null;

                //if using garnishments, get the file, otherwise return an empty file
                if (Common.ApplicationParameter.CraGarnishmentRemitFlag)
                {
                    RbcEftSourceDeductionParameters garnishmentParms = Common.ParameterHelper.CreateAndPopulateRbcSourceDeductionParameters(Common.ApplicationParameter.GarnishmentEftType);
                    file = Common.ServiceWrapper.HumanResourcesClient.CreateCraEftGarnishmentFile(payrollprocessid, garnishmentParms, null) ?? new byte[0];
                }
                else
                {
                    file = new byte[0];
                }

                Response.Clear();

                //this will prompt the user to open or download the file
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + String.Format("{0:yyyyMMddHHmmssfff}_{1}_{2}_GARNISH_EFT.txt", DateTime.Now, Common.ApplicationParameter.DynamicsDatabaseName, payrollprocessid));
                Response.AddHeader("Content-Length", file.Length.ToString());

                Response.OutputStream.Write(file, 0, file.Length);
                Response.Flush();
                Response.End();
            }
            else if (args != null && args.Length > "socialCostPayrollProcessId=".Length && args.Substring(0, "socialCostPayrollProcessId=".Length).ToLower().Equals("socialcostpayrollprocessid="))
            {
                long payrollprocessid = Convert.ToInt64(args.Substring("socialCostPayrollProcessId=".Length));
                byte[] file = Common.ServiceWrapper.HumanResourcesClient.CreateSocialCostsExport(payrollprocessid) ?? new byte[0];

                Response.Clear();

                //this will prompt the user to open or download the file
                Response.ContentType = "application/octet-stream";

                Response.AddHeader("Content-Disposition", "attachment;filename=" + String.Format(Common.ApplicationParameter.SocialCostsExportFileName, DateTime.Now));
                Response.AddHeader("Content-Length", file.Length.ToString());

                Response.OutputStream.Write(file, 0, file.Length);
                Response.Flush();
                Response.End();
            }
            else if (args != null && args.Length > "openRealCustomExportPayrollProcessId=".Length && args.Substring(0, "openRealCustomExportPayrollProcessId=".Length).ToLower().Equals("openrealcustomexportpayrollprocessid="))
            {
                long payrollprocessid = Convert.ToInt64(args.Substring("openRealCustomExportPayrollProcessId=".Length));
                byte[] file = Common.ServiceWrapper.HumanResourcesClient.CreateRealCustomExport(payrollprocessid) ?? new byte[0];

                Response.Clear();

                //this will prompt the user to open or download the file
                Response.ContentType = "application/octet-stream";

                Response.AddHeader("Content-Disposition", "attachment;filename=" + string.Format(Common.ApplicationParameter.REALCustomExportFileName, payrollprocessid));
                Response.AddHeader("Content-Length", file.Length.ToString());
                
                Response.OutputStream.Write(file, 0, file.Length);
                Response.Flush();
                Response.End();
            }
            else if (args != null && args.Length > 22 && args.Substring(0, 22).ToLower().Equals("stockpayrollprocessid="))
            {
                long payrollprocessid = Convert.ToInt64(args.Substring("stockpayrollprocessid=".Length));
                byte[] file = Common.ServiceWrapper.ReportClient.CreateStockExportFile(payrollprocessid) ?? new byte[0];
                PayrollProcessSummaryCollection process = new PayrollProcessSummaryCollection();
                process.Load(Common.ServiceWrapper.HumanResourcesClient.GetPayrollProcessSummary(new PayrollProcessCriteria { PayrollProcessId = payrollprocessid }));
                DateTime chequeDate = Convert.ToDateTime(process[0].ChequeDate);

                Response.Clear();

                Response.ContentType = "application/zip";
                Response.AppendHeader("content-disposition", "attachment; filename=" + String.Format(Common.ApplicationParameter.StockExportFileFormat, chequeDate));
                Response.AddHeader("Content-Length", file.Length.ToString());

                Response.OutputStream.Write(file, 0, file.Length);
                Response.Flush();
                Response.End();
            }
            else if (args != null && args.Length > 29 && args.Substring(0, 29).ToLower().Equals("empowerremitpayrollprocessid="))
            {
                long payrollprocessid = Convert.ToInt64(args.Substring(29));

                //load an object from the db and create an export file
                byte[] file = Common.ServiceWrapper.HumanResourcesClient.CreateEmpowerRemittanceExport(payrollprocessid, Common.ApplicationParameter.RevenueQuebecTaxId, Common.ApplicationParameter.EmpowerUseQuebecBusinessNumber) ?? new byte[0];

                Response.Clear();

                //this will prompt the user to open or download the file
                Response.ContentType = "application/octet-stream";

                if (Common.ApplicationParameter.ReportEmpowerExportFileFormat.Equals(String.Empty))
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + String.Format("EmpowerTaxRecord{0}.txt", payrollprocessid));
                else
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + Common.ServiceWrapper.HumanResourcesClient.GenerateFileName(Common.ApplicationParameter.ReportEmpowerExportFileFormat, payrollprocessid));

                Response.AddHeader("Content-Length", file.Length.ToString());

                Response.OutputStream.Write(file, 0, file.Length);
                Response.Flush();
                Response.End();
            }
            else if (args != null && args.Length > 35 && args.Substring(0, 35).ToLower().Equals("empowergarnishmentpayrollprocessid="))
            {
                long payrollprocessid = Convert.ToInt64(args.Substring(35));
                byte[] file = Common.ServiceWrapper.HumanResourcesClient.CreateGarnishmentExportFile(payrollprocessid, Common.ApplicationParameter.EmpowerGarnishmentFein) ?? new byte[0];
                String fileName = Common.ServiceWrapper.HumanResourcesClient.GenerateFileName(Common.ApplicationParameter.EmpowerGarnishmentExportFileFormat, payrollprocessid);

                Response.Clear();

                //this will prompt the user to open or download the file
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName);
                Response.AddHeader("Content-Length", file.Length.ToString());

                Response.OutputStream.Write(file, 0, file.Length);
                Response.Flush();
                Response.End();
            }
            else if (args != null && args.Length > 24 && args.Substring(0, 24).ToLower().Equals("cicplusweeklyperiodyear="))
            {
                Decimal periodYear = Convert.ToDecimal(args.Substring(24));
                byte[] file = Common.ServiceWrapper.HumanResourcesClient.CreateCicPlusWeeklyExportFile(periodYear) ?? new byte[0];

                //{companyCode}AuthCA{mmddyyyy}.txt
                String fileName = String.Format("{0}AuthCA{1:mmddyyyy}.txt", Common.ApplicationParameter.DynamicsDatabaseName, DateTime.Now);

                Response.Clear();

                //this will prompt the user to open or download the file
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName);
                Response.AddHeader("Content-Length", file.Length.ToString());

                Response.OutputStream.Write(file, 0, file.Length);
                Response.Flush();
                Response.End();

            }
            else if (args != null && args.Length > 19 && args.Substring(0, 19).ToLower().Equals("glpayrollprocessid="))
            {
                long payrollprocessid = Convert.ToInt64(args.Substring(19));
                String glType = Common.CodeHelper.GetCodeDescription("EN", CodeTableType.System, "RPTGLEX");
                byte[] file = Common.ServiceWrapper.ReportClient.CreateGLExportFile(payrollprocessid, glType, Common.ApplicationParameter.ReportShowDescriptionField, Common.ApplicationParameter.ReportPayRegisterSortOrder) ?? new byte[0];

                Response.Clear();

                if (Common.ApplicationParameter.ReportGLExportFileFormat.Equals(String.Empty))
                {
                    Response.ContentType = "application/zip";
                    Response.AppendHeader("content-disposition", "attachment; filename=" + String.Format("{0:yyyyMMdd}-{1}.zip", DateTime.Now, payrollprocessid));
                }
                else
                {
                    Response.ContentType = "application/octet-stream";

                    if (glType.ToLower() == "gl_sap")
                        Response.AddHeader("Content-Disposition", "attachment;filename=" + Common.ServiceWrapper.HumanResourcesClient.GenerateSapFileName(Common.ApplicationParameter.ReportGLExportFileFormat, payrollprocessid));
                    else
                        Response.AddHeader("Content-Disposition", "attachment;filename=" + Common.ServiceWrapper.HumanResourcesClient.GenerateFileName(Common.ApplicationParameter.ReportGLExportFileFormat, payrollprocessid));
                }

                Response.OutputStream.Write(file, 0, file.Length);
                Response.Flush();
                Response.End();
            }

            PayrollProcessSummaryGrid.ItemDataBound += new GridItemEventHandler(PayrollProcessSummaryGrid_ItemDataBound);
            PayrollProcessSummaryGrid.NeedDataSource += new GridNeedDataSourceEventHandler(PayrollProcessSummaryGrid_NeedDataSource);
            PayrollProcessSummaryGrid.PreRender += new EventHandler(PayrollProcessSummaryGrid_PreRender);
            PayrollProcessSummaryGrid.AllowPaging = true;

            if (!IsPostBack)
                Panel1.DataBind();
        }
        //seperate the args when the Ceridian Export is selected
        private String seperateArgs(String args, out long processId, out String periodYr)
        {
            int prevIndex = 0;
            int nextIndex = args.IndexOf(",", 0);

            processId = Convert.ToInt64(args.Substring(23, nextIndex - 23));
            prevIndex = nextIndex;
            nextIndex = args.IndexOf(",", prevIndex + 1);
            periodYr = args.Substring(prevIndex + 12, nextIndex - prevIndex - 12);
            prevIndex = nextIndex + 1;

            String period = args.Substring(prevIndex + 7);

            return period;
        }
        
        private void seperateFtpArgs(String args, out long payrollProcessId, out String periodYr)
        {
            int prevIndex = 0;
            int nextIndex = args.IndexOf(",", 0);

            payrollProcessId = Convert.ToInt64(args.Substring(26, nextIndex - 26));
            prevIndex = nextIndex + 1;
            periodYr = args.Substring(prevIndex + 11);
        }
        void PayrollProcessSummaryGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            PayrollProcessSummaryGrid.DataSource = Data;
        }
        public void Search(PayrollProcessCriteria criteria)
        {
            PayrollProcessSummaryCollection collection = new PayrollProcessSummaryCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetPayrollProcessSummary(criteria));
            Data = collection;
            PayrollProcessSummaryGrid.Rebind();
        }
        #endregion

        #region event handlers
        public void RefreshGrid()
        {
            //the rebind in the search method isn't firing the need datasource event, so call it directly
            PayrollProcessSummaryCollection collection = new PayrollProcessSummaryCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetPayrollProcessSummary(Criteria));
            Data = collection;

            PayrollProcessSummaryGrid_NeedDataSource(null, null);
            PayrollProcessGroupCodeControl.Value = Criteria.PayrollProcessGroupCode;
            SearchPanel.DataBind();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search(Criteria);
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Criteria = new PayrollProcessCriteria();
            Data = null;
            PayrollProcessSummaryGrid.DataBind();
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void PayrollProcessSummaryGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                //work around to remove empty rows caused by the OnCommand Telerik bug 
                if (String.Equals((dataItem["PayrollProcessGroupCode"].Text).ToUpper(), "&NBSP;")) e.Item.Display = false;
            }
        }
        void PayrollProcessSummaryGrid_PreRender(object sender, EventArgs e)
        {
            //work around to remove empty rows caused by the OnCommand Telerik Bug
            if (Data == null)
                PayrollProcessSummaryGrid.AllowPaging = false;
            else
                PayrollProcessSummaryGrid.AllowPaging = true;
        }
        #endregion

        #region security handlers
        protected void PaySlipsToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnablePaySlipButton; }
        protected void MassPayslipPrintToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableMassPayslipPrintButton; }
        protected void TransmitFilesToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableTransmitFilesButton; }
        protected void EFTPaymentToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableEFTPayment; }
        protected void RealExportToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableRealExport; }

        //menu button
        protected void Export_PreRender(object sender, EventArgs e) { ((WLPToolBarDropDown)sender).Visible = EnableExportMenu; }

        //sub item of Export menu button
        protected void CSBExportToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableCSBExportButton; }
        protected void CeridianExportToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableCeridianExportButton; }
        protected void EFTToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableEFTButton; }
        protected void PensionExportToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnablePensionExportButton; }
        protected void GLToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableGLButton; }
        protected void RRSPToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableRRSPButton; }
        protected void StockToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableStockButton; }
        protected void EmpowerRemittanceToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableEmpowerRemittanceButton; }
        protected void EmpowerGarnishmentToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableEmpowerGarnishmentButton; }
        protected void CICPlusWeeklyToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableCICPlusWeeklyButton; }
        protected void CraGarnishmentToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableCraGarnishmentButton; }
        protected void SocialCostsToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableSocialCostsButton; }
        protected void RealCustomExportToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableRealCustomExportButton; }

        //reports menu section
        protected void ReportMenuButton_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableReportsMenu; }
        protected void ReportsToolBar_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableReportsMenu; }
        protected void StandardMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableStandardMenuItem; }
        protected void CustomMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableCustomMenuItem; }
        protected void CustomerInvoiceReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableCustomerInvoiceReportMenuItem; }
        protected void EftCustomerInvoiceJamaicaReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableEftCustomerInvoiceJamaicaReportMenuItem; }
        protected void PayRegisterReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePayRegisterReportMenuItem; }
        protected void RegisterSummaryReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableRegisterSummaryReportMenuItem; }
        protected void ChangesReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableChangesReportMenuItem; }
        protected void CompensationListReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableCompensationListReportMenuItem; }
        protected void CurrentVsPriorReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableCurrentVsPriorReportMenuItem; }
        protected void CurrentVsPriorNetReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableCurrentVsPriorNetReportMenuItem; }
        protected void CurrentVsPriorPeriodVariances_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableCurrentVsPriorPeriodVariancesMenuItem; }
        protected void GarnishmentMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableGarnishmentReportMenuItem; }
        protected void PayExceptionMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePayExceptionReportMenuItem; }
        protected void GrossVsSalaryMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableGrossVsSalaryReportMenuItem; }
        protected void PD7AMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePD7AReportMenuItem; }
        protected void BarbadosDeductionSummaryMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableBarbadosDeductionSummaryMenuItem; }
        protected void StLuciaDeductionSummaryMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableStLuciaDeductionSummaryMenuItem; }
        protected void TrinidadDeductionSummaryMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableTrinidadDeductionSummaryMenuItem; }
        protected void JamaicaDeductionSummaryMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableJamaicaDeductionSummaryMenuItem; }
        protected void JamaicaGovMonthlyRemittanceMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableJamaicaGovMonthlyRemittanceMenuItem; }
        protected void YtdBonusCommissionMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableYtdBonusCommissionReportMenuItem; }
        protected void PensionMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePensionReportMenuItem; }
        protected void WCBMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableWCBReportMenuItem; }
        protected void CSBMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableCSBReportMenuItem; }
        protected void PayDetailsMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePayDetailsMenuItem; }
        protected void GLEmployeeDetailsMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableGLDetailsMenuItem; }
        protected void FedProvRemitSummaryMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableFedProvRemitSummaryMenuItem; }
        protected void ProvincialHealthSummaryMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableProvincialHealthSummaryMenuItem; }
        protected void QuebecRemitSummaryMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableQuebecRemitSummaryMenuItem; }
        protected void ResearchAndDevelopmentMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableResearchAndDevelopmentMenuItem; }
        protected void PreYET4ADetailMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePreYET4ADetailMenuItem; }
        protected void PreYET4DetailMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePreYET4DetailMenuItem; }
        protected void PreYER1DetailMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePreYER1DetailMenuItem; }
        protected void PreYER2DetailMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePreYER2DetailMenuItem; }
        protected void PIERPreYEMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePIERPreYEMenuItem; }
        protected void BenefitArrearsMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableBenefitArrearsMenuItem; }
        protected void EmployeeSalaryReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableEmployeeSalaryReportMenuItem; }
        protected void UnitedWayReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableUnitedWayReportMenuItem; }
        protected void EnableSerpYtdReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableSerpYtdReportMenuItem; }
        protected void PensionDemographicsMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePensionDemographicsMenuItem; }
        protected void SrpReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableSrpReportMenuItem; }
        protected void PreYearEndDetailMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePreYearEndDetailMenuItem; }
        protected void PaymentListMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePaymentListReportMenuItem; }
        protected void VacationReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableVacationReportMenuItem; }
        protected void A1235GLDetailsReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableA1235GLDetailsReportMenuItem; }
        protected void CurrentVsPriorTwoPeriodVariances_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableCurrentVsPriorTwoPeriodVariances; }
        protected void MonthlyStatutoryRemittance_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableMonthlyStatutoryRemittance; }
        protected void AccumulatedHours_OnPreRender(object sender, EventArgs e){ ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableAccumulatedHoursReportMenuItem; }
        protected void PayrollDetailsOrgUnitMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePayrollDetailsOrgUnitMenuItem; }

        //end reports menu section
        #endregion

    }
}