﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="EftPaymentPage.aspx.cs" Inherits="WorkLinks.Payroll.PayrollProcess.EftPaymentPage" %>
<%@ Register Src="~/Payroll/PayrollProcess/EftPaymentControl.ascx" TagPrefix="uc1" TagName="EftPaymentControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:EftPaymentControl runat="server" id="EftPaymentControl" />
</asp:Content>