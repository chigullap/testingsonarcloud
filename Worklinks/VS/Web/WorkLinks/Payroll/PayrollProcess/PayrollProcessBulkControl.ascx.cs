﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.PayrollProcess
{
    public partial class PayrollProcessBulkControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public object Worker { get; private set; }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            WireEvents();

            if (!IsPostBack)
                LoadBulkPayrollProcessInformation();
        }
        protected void WireEvents()
        {
            BulkPayrollProcessGrid.NeedDataSource += new GridNeedDataSourceEventHandler(BulkPayrollProcessGrid_NeedDataSource);
        }
        protected void LoadBulkPayrollProcessInformation()
        {
            Data = TempBulkPayrollProcessCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetTempBulkPayrollProcess());
        }
        protected void RefreshGrid()
        {
            LoadBulkPayrollProcessInformation();
            BulkPayrollProcessGrid.Rebind();
        }
        #endregion

        #region event handlers
        protected void BulkPayrollProcessGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            BulkPayrollProcessGrid.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void Refresh_Click(object sender, EventArgs e)
        {
            RefreshGrid();
        }
        protected void Calc_Click(object sender, EventArgs e)
        {
            foreach (GridDataItem selectedItem in BulkPayrollProcessGrid.SelectedItems)
            {
                BusinessLayer.BusinessObjects.PayrollProcess process = new BusinessLayer.BusinessObjects.PayrollProcess();

                process.PayrollPeriodId = Convert.ToInt64(selectedItem.GetDataKeyValue("PayrollPeriodId"));
                process.ChequeDate = DateTime.Now;
                process.PayrollProcessRunTypeCode = "REGULAR";

                foreach (BusinessLayer.BusinessObjects.PayrollProcess item in Common.ServiceWrapper.PayrollProcess.Select(new PayrollProcessCriteria() { GetLastOpenPayrollProcessFlag = true, PayrollPeriodId = process.PayrollPeriodId }))
                {
                    process.PayrollProcessId = item.PayrollProcessId;
                    process.RowVersion = item.RowVersion;
                }

                Common.ServiceWrapper.PayrollProcess.Update(process, WorkLinks.BusinessLayer.BusinessObjects.PayrollProcess.CalculationProcess.Calculate);
            }

            RefreshGrid();
        }
        protected void ReCalc_Click(object sender, EventArgs e)
        {
            foreach (GridDataItem selectedItem in BulkPayrollProcessGrid.SelectedItems)
            {
                BusinessLayer.BusinessObjects.PayrollProcess process = new BusinessLayer.BusinessObjects.PayrollProcess();

                process.PayrollPeriodId = Convert.ToInt64(selectedItem.GetDataKeyValue("PayrollPeriodId"));
                process.ChequeDate = DateTime.Now;
                process.PayrollProcessRunTypeCode = "REGULAR";
                process.PayrollProcessStatusCode = "NW";

                foreach (BusinessLayer.BusinessObjects.PayrollProcess item in Common.ServiceWrapper.PayrollProcess.Select(new PayrollProcessCriteria() { GetLastOpenPayrollProcessFlag = true, PayrollPeriodId = process.PayrollPeriodId }))
                {
                    process.PayrollProcessId = item.PayrollProcessId;
                    process.RowVersion = item.RowVersion;
                }

                Common.ServiceWrapper.PayrollProcess.Update(process, WorkLinks.BusinessLayer.BusinessObjects.PayrollProcess.CalculationProcess.ReCalculate);
            }

            RefreshGrid();
        }
        protected void Post_Click(object sender, EventArgs e)
        {
            foreach (GridDataItem selectedItem in BulkPayrollProcessGrid.SelectedItems)
            {
                BusinessLayer.BusinessObjects.PayrollProcess process = null;

                foreach (BusinessLayer.BusinessObjects.PayrollProcess item in Common.ServiceWrapper.PayrollProcess.Select(new PayrollProcessCriteria() { GetLastOpenPayrollProcessFlag = true, PayrollPeriodId = Convert.ToInt64(selectedItem.GetDataKeyValue("PayrollPeriodId")) }))
                    process = item;

                foreach (PayrollPeriod item in Common.ServiceWrapper.PayrollPeriod.Select(new PayrollPeriodCriteria() { PayrollPeriodId = process.PayrollPeriodId }))
                    item.CopyTo(process);

                //only populate if using RBC_EDI eft type or if using pre-authorized debit for RBC(or both)
                RbcEftEdiParameters ediParms = null;
                if (Common.ApplicationParameter.EFTType == "RBC_EDI" || (Common.ApplicationParameter.PreAuthorizedDebitFlag && Common.ApplicationParameter.PreAuthorizedDebitFileType == "RBC_CANADA"))
                    ediParms = Common.ParameterHelper.CreateAndPopulateRbcDirectDepositChequeParameters(Common.ApplicationParameter.EFTType);

                //only populate if using SCOT_EDI eft type or if using pre-authorized debit for SCOTIABANK(or both)
                ScotiaBankEdiParameters scotiaEdiParms = null;
                if (Common.ApplicationParameter.EFTType == "SCOT_EDI" || (Common.ApplicationParameter.PreAuthorizedDebitFlag && Common.ApplicationParameter.PreAuthorizedDebitFileType == "SCOTIA_JAMAICA"))
                    scotiaEdiParms = Common.ParameterHelper.CreateAndPopulateScotiaBankDirectDepositParameters(Common.ApplicationParameter.EFTType);

                RbcEftSourceDeductionParameters garnishmentParms = null;
                if (Common.ApplicationParameter.CraGarnishmentRemitFlag)
                    garnishmentParms = Common.ParameterHelper.CreateAndPopulateRbcSourceDeductionParameters(Common.ApplicationParameter.GarnishmentEftType);

                //only populate if sending an invoice
                SendEmailParameters emailParms = null;
                if (Common.ApplicationParameter.SendInvoice)
                    emailParms = Common.ParameterHelper.CreateAndPopulateEmailParameters();

                Common.ServiceWrapper.PayrollProcess.PostTransaction(process, Common.ApplicationParameter.Code.PayrollProcessStatusCode.Processed, ediParms, scotiaEdiParms, emailParms, garnishmentParms, Common.ApplicationParameter.CraRemitFlag, Common.ApplicationParameter.RqRemitFlag, Common.ApplicationParameter.SendInvoice, Common.ApplicationParameter.CraGarnishmentRemitFlag, Common.ApplicationParameter.WcbChequeRemitFlag, Common.ApplicationParameter.HealthTaxChequeRemitFlag, Common.ApplicationParameter.AutoAddSecondaryPositions);
            }

            RefreshGrid();
        }
        #endregion
    }
}