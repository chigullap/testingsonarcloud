﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayrollProcessingControl.ascx.cs" Inherits="WorkLinks.Payroll.PayrollProcess.PayrollProcessingControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="cc1" %>


<div style="  height:100%; ">
  

<asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional" style="  height:100%; ">
<ContentTemplate>

    <asp:Timer ID="Timer1" runat="server" ontick="Timer1_Tick" Interval="5000">
    </asp:Timer>
  
  <table width="100%" height="100%">
    <tr align="center" valign="bottom" height="40%">
        <td align="center" >
            <cc1:WLPLabel ID="WLPLabel1" Font-Bold="true" Font-Size="X-Large" Text="Processing request, please wait....." runat="server"></cc1:WLPLabel>
    </tr>
    <tr align="center" valign="middle" height="10%">
        <td align="center" >
            <img style="width: 256px;height: 20px;" runat="server" src="~/App_Themes/Default/progressBarBert.gif" />
        </td>
    </tr>
    <tr align="center" valign="top" height="10%">
        <td align="center" >
            <cc1:WLPButton  ID="WLPButton2" runat="server" Text="Run In Background" 
                onclick="WLPButton2_Click"></cc1:WLPButton>
        </td>
    </tr>
    <tr align="center" valign="top">
        <td>
            <cc1:WLPLabel ID="WLPLabel2" Font-Bold="true" Font-Size="Small" Text="You will be notified via email when the process has completed." runat="server"></cc1:WLPLabel>
        </td>
    </tr>
  </table>
</ContentTemplate>
</asp:UpdatePanel>

</div>
