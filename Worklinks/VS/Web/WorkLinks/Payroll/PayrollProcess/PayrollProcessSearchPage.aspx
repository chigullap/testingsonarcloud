﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PayrollProcessSearchPage.aspx.cs" Inherits="WorkLinks.Payroll.PayrollProcess.PayrollProcessSearchPage" %>
<%@ Register src="PayrollProcessSearchControl.ascx" tagname="PayrollProcessSearchControl" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate> 
        <uc1:PayrollProcessSearchControl ID="PayrollProcessSearchControl" runat="server" />        
    </ContentTemplate>


</asp:Content>
