﻿using System;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.PayrollProcess
{
    public partial class EftPaymentControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private long PayrollProcessId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["payrollProcessId"]); }
        }
        public bool UsingDirectDeposit
        {
            get { return Common.ApplicationParameter.EFTType == "RBC_EDI"; }
        }
        public bool UsingGarnishment
        {
            get { return Common.ApplicationParameter.CraGarnishmentRemitFlag; }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EFTPayment.ViewFlag);

            this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "EftPayment"));
        }
        protected void EftPaymentViewToolBar_ButtonClick(object sender, Telerik.Web.UI.RadToolBarEventArgs e)
        {
            if (((Telerik.Web.UI.RadToolBarButton)(e.Item)).CommandName.Equals("submit"))
            {
                RbcEftEdiParameters directDepositParms = null;
                RbcEftSourceDeductionParameters garnishmentParms = null;

                //if customer using RBC EFT DIRECT DEPOSIT, populate the RbcEftEdiParameters object
                if (UsingDirectDeposit)
                {
                    //Only need the EftType populated in RbcEftEdiParameters object since file is already created and in the database
                    directDepositParms = new RbcEftEdiParameters() { EftType = Common.ParameterHelper.DetermineEftType(Common.ApplicationParameter.EFTType) };
                }

                //if customer using CRA GARNISHMENT, populate the RbcEftEdiParameters object
                if (UsingGarnishment)
                {
                    //Only need the GarnishmentEftType populated in RbcEftSourceDeductionParameters object since file is already created and in the database
                    garnishmentParms = new RbcEftSourceDeductionParameters() { GarnishmentEftType = Common.ParameterHelper.DetermineEftType(Common.ApplicationParameter.GarnishmentEftType) };
                }

                //ftp the eft file to the bank.  
                Common.ServiceWrapper.ReportClient.FtpRbcEdiExport(PayrollProcessId, directDepositParms, garnishmentParms, Common.ApplicationParameter.RbcFtp);

                //close the RadWindow
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "close and return", "closeAndReturn();", true);
            }
        }
        #endregion
        protected void DirectDepositListItem_PreRender(object sender, EventArgs e) { directDepositListItem.Visible = UsingDirectDeposit; }
    }
}