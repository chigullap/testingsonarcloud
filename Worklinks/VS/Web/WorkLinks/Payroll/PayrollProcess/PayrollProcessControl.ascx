﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayrollProcessControl.ascx.cs" Inherits="WorkLinks.Payroll.PayrollProcess.PayrollProcessControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Src="../../Payroll/PayrollBatch/PayrollBatchSearchResultsControl.ascx" TagName="PayrollBatchSearchResultsControl" TagPrefix="uc1" %>
<%@ Register Src="../../Payroll/PayrollReportsMenu.ascx" TagName="PayrollReportsMenu" TagPrefix="uc2" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="SearchPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SearchPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="SearchPanel" runat="server">

    <div style="display:none;">
        <telerik:RadAsyncUpload ID="FileUpload" RenderMode="Lightweight" runat="server" MultipleFileSelection="Disabled"  HideFileInput="true" OnFileUploaded="FileUpload_FileUploaded" OnClientFileUploaded="submitFileUpload" ></telerik:RadAsyncUpload>
        <asp:Button ID="SubmitFileUpload" runat="server" OnClick="Upload_Click"   />

        </div>
    <wasp:WLPToolBar ID="PayrollProcessToolBar" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="PayrollProcessToolBar_ButtonClick" OnClientButtonClicking="checkPostConfirmation">
        <Items>
            <wasp:WLPToolBarButton Text="Calculate" CommandName="Calculate" Enabled="true" Visible="true" ResourceName="Calculate"/>
            <wasp:WLPToolBarButton Text="Import Remittance" CommandName="Import" Enabled="true" Visible="true" ResourceName="Import" />
            <wasp:WLPToolBarButton Text="Recalculate" CommandName="ReCalculate" Enabled="false" ResourceName="Recalculate" />
            <wasp:WLPToolBarButton Text="Post" CommandName="Post" ResourceName="Post" />
            <wasp:WLPToolBarButton CommandName="ReportMenuButton" OnPreRender="ReportMenuButton_PreRender">
                <ItemTemplate>
                    <uc2:PayrollReportsMenu ID="PayrollReportsMenu" runat="server" />
                </ItemTemplate>
            </wasp:WLPToolBarButton>
            <wasp:WLPToolBarButton Text="Lock" CommandName="Lock" Enabled="true" ResourceName="Lock" />
            <wasp:WLPToolBarButton Text="Unlock" CommandName="Unlock" Enabled="true" ResourceName="Unlock" />
            <wasp:WLPToolBarButton Text="*Real Ftp Import" CommandName="RealFtpImport" Enabled="true" ResourceName="RealFtpImport" />
        </Items>
    </wasp:WLPToolBar>

    <div class="simpleTable">
        <table width="70%">
            <tr>
                <td>
                    <wasp:ComboBoxControl ID="PayrollProcessGroupCode" runat="server" Mandatory="true" ResourceName="PayrollProcessGroupCode" Type="PayrollProcessGroupCode" AutoPostback="true" ReadOnly="false" OnSelectedIndexChanged="PayrollProcessCriteria_SelectedIndexChanged" />
                </td>
                <td>
                    <wasp:ComboBoxControl ID="PayrollProcessRunTypeCode" runat="server" Mandatory="true" ResourceName="PayrollProcessRunTypeCode" Type="PayrollProcessRunTypeCode" AutoPostback="true" ReadOnly="false" OnSelectedIndexChanged="PayrollProcessCriteria_SelectedIndexChanged" />
                </td>
            </tr>
        </table>
    </div>

    <wasp:WLPFormView ID="PayrollProcessView" runat="server" RenderOuterTable="false" DataKeyNames="Key">
        <ItemTemplate>
            <table width="70%" style="float: left;">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <wasp:ComboBoxControl ID="PayrollProcessStatusCode" Text="Status" runat="server" ResourceName="PayrollProcessStatusCode" Type="PayrollProcessStatusCode" ReadOnly="true" OnDataBinding="Code_NeedDataSource" Value='<%# Bind("PayrollProcessStatusCode") %>' />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:DateControl ID="ChequeDate" runat="server" ResourceName="ChequeDate" Value='<%# Bind("ChequeDate") %>' ReadOnly="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:DateControl ID="StartDate" runat="server" ResourceName="StartDate" Value='<%# Bind("StartDate") %>' ReadOnly="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:DateControl ID="CutoffDate" runat="server" ResourceName="CutoffDate" Value='<%# Bind("CutoffDate") %>' ReadOnly="true" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table>
                            <tr>
                                <td>
                                    <wasp:CheckBoxControl ID="IgnoreCppQppExemptionFlag" ClientIDMode="Static" runat="server" ResourceName="IgnoreCppQppExemptionFlag" Value='<%# Bind("IgnoreCppQppExemptionFlag") %>' LabelText="*IgnoreCppQppExemptionFlag" ReadOnly="true" Visible="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:TextBoxControl ID="LastCalcDate" runat="server" ResourceName="LastCalcDate" Value='<%# Bind("LastCalcDate") %>' ReadOnly="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:TextBoxControl ID="LastCalcUser" runat="server" ResourceName="LastCalcUser" Value='<%# Bind("LastCalcUser") %>' ReadOnly="true" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PayrollProcessAutoSelectPaycodeModeCode" runat="server" ResourceName="PayrollProcessAutoSelectPaycodeModeCode" Type="PayrollProcessAutoSelectPaycodeModeCode" ReadOnly="true" OnDataBinding="Code_NeedDataSource" Value='<%# Bind("PayrollProcessAutoSelectPaycodeModeCode") %>' Visible="false" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="PreventReclaimsFlag" runat="server" ResourceName="PreventReclaimsFlag" Value='<%# Bind("PreventReclaimsFlag") %>' LabelText="PreventReclaimsFlag" ReadOnly="true" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="AutoPayVacationPayThisPeriodFlag" runat="server" ResourceName="AutoPayVacationPayThisPeriodFlag" Value='<%# Bind("AutoPayVacationPayThisPeriodFlag") %>' LabelText="AutoPayVacationPayThisPeriodFlag" ReadOnly="true" Visible="false" />
                    </td>
                </tr>
            </table>
            <table width="30%" style="float: right;">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PayslipNote" MaxLength="256" runat="server" FieldStyle="width: 215px !important;" TextMode="multiline" Width="100%" Rows="4" ResourceName="PayslipNote" Value='<%# Bind("PayslipNote") %>' ReadOnly="true" Visible="true" />
                    </td>
                </tr>
            </table>
        </ItemTemplate>

        <EditItemTemplate>
            <table width="70%" style="float: left;">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <wasp:ComboBoxControl ID="PayrollProcessStatusCode" Text="Status" runat="server" ResourceName="PayrollProcessStatusCode" Type="PayrollProcessStatusCode" ReadOnly="true" OnDataBinding="Code_NeedDataSource" Value='<%# Bind("PayrollProcessStatusCode") %>' TabIndex="010" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:DateControl ID="ChequeDate" runat="server" MinDate='<%# DateTime.Now %>' MaxDate='<%# MaxDate %>' ResourceName="ChequeDate" Value='<%# Bind("ChequeDate") %>' DateInputEnabled="false" ReadOnly="false" TabIndex="050" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:DateControl ID="StartDate" runat="server" ResourceName="StartDate" Value='<%# Bind("StartDate") %>' ReadOnly="true" TabIndex="030" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:DateControl ID="CutoffDate" runat="server" ResourceName="CutoffDate" Value='<%# Bind("CutoffDate") %>' ReadOnly="true" TabIndex="040" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table>
                            <tr>
                                <td>
                                    <wasp:CheckBoxControl ID="IgnoreCppQppExemptionFlag" ClientIDMode="Static" runat="server" ResourceName="IgnoreCppQppExemptionFlag" Value='<%# Bind("IgnoreCppQppExemptionFlag") %>' LabelText="*IgnoreCppQppExemptionFlag" ReadOnly="false" Visible="true" TabIndex="090" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:TextBoxControl ID="LastCalcDate" runat="server" ResourceName="LastCalcDate" Value='<%# Bind("LastCalcDate") %>' ReadOnly="true" TabIndex="050" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wasp:TextBoxControl ID="LastCalcUser" runat="server" ResourceName="LastCalcUser" Value='<%# Bind("LastCalcUser") %>' ReadOnly="true" TabIndex="060" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="PayrollProcessAutoSelectPaycodeModeCode" runat="server" ResourceName="PayrollProcessAutoSelectPaycodeModeCode" Type="PayrollProcessAutoSelectPaycodeModeCode" ReadOnly="false" OnDataBinding="Code_NeedDataSource" Value='<%# Bind("PayrollProcessAutoSelectPaycodeModeCode") %>' Visible="false" TabIndex="060" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="PreventReclaimsFlag" runat="server" ResourceName="PreventReclaimsFlag" Value='<%# Bind("PreventReclaimsFlag") %>' LabelText="PreventReclaimsFlag" ReadOnly="false" Visible="false" TabIndex="070" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="AutoPayVacationPayThisPeriodFlag" runat="server" ResourceName="AutoPayVacationPayThisPeriodFlag" Value='<%# Bind("AutoPayVacationPayThisPeriodFlag") %>' LabelText="AutoPayVacationPayThisPeriodFlag" ReadOnly="false" Visible="false" TabIndex="080" />
                    </td>
                </tr>
            </table>
            <table width="30%" style="float: right;">
                <tr>
                    <td>
                        <wasp:TextBoxControl ID="PayslipNote" MaxLength="256" runat="server" FieldStyle="width: 215px !important;" TextMode="multiline" Width="100%" Rows="4" ResourceName="PayslipNote" Value='<%# Bind("PayslipNote") %>' ReadOnly="false" TabIndex="100" />
                    </td>
                </tr>
            </table>
        </EditItemTemplate>
    </wasp:WLPFormView>

    <div style="clear: both;" />
    <asp:Label ID="lblTransMessage" runat="server" Text="" ForeColor="Red" />
    
    <uc1:PayrollBatchSearchResultsControl ID="PayrollBatchSearchResultsControl" Visible="false" runat="server" ResultsGridHeight="300" />

    <asp:Button ID="btnDelete2" runat="server" OnClick="btnDelete_Click" Style="display: none;" />
</asp:Panel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="RegisterReportWindow"
    Width="800"
    Height="600"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var payrollProcessId = <%= PayrollProcessId %>;

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function checkPostConfirmation(sender, args) {
            var button = args.get_item();
            if (button.get_commandName() == "Post") {
                var message = "<asp:Literal runat="server" Text="<%$ Resources:WarningMessages, PerformingPostWarning %>" />";
                args.set_cancel(!confirm(message));
            }
            else if (button.get_commandName() == "Import")
            {
                var test = $find('<%= FileUpload.ClientID %>')._fileInput.click();
                args.set_cancel(true);
            }
            else if (button.get_commandName() == "Calculate") {
                var runTypeControl = document.getElementById('<%= PayrollProcessRunTypeCode.ClientID %>');
                var runType = document.getElementById(runTypeControl.attributes['fieldClientId'].value).value;
                var ignoreCppQppControl = document.getElementById('IgnoreCppQppExemptionFlag');
                if (ignoreCppQppControl != null) {
                    var IsChecked = document.getElementById(ignoreCppQppControl.attributes['fieldClientId'].value).checked;

                    if (runType.toLowerCase() == "regular" && IsChecked == true) {
                        var message = "<asp:Literal runat="server" Text="<%$ Resources:WarningMessages, NegativeTaxCPPWarning %>" />";
                        args.set_cancel(!confirm(message));
                    }
                }
            }
        }

        function submitFileUpload(sender, e) {
            $get("<%= SubmitFileUpload.ClientID %>").click();
        }

    </script>
</telerik:RadScriptBlock>