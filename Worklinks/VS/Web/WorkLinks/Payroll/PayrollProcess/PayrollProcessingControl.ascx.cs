﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using WorkLinks.BusinessLayer.BusinessObjects;


namespace WorkLinks.Payroll.PayrollProcess
{
    public partial class PayrollProcessingControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private static String _statusCalculationPending = Common.ApplicationParameter.Code.PayrollProcessStatusCode.CalculationPending;
        private static String _statusPostPending = Common.ApplicationParameter.Code.PayrollProcessStatusCode.PostPending;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                LoadPayrollProcess();
        }


        protected void WLPButton2_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/HumanResources/LandingPage.aspx");
        }


        #region properties
        private long PayrollProcessId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["payrollProcessId"]); }
        }
        private WorkLinks.BusinessLayer.BusinessObjects.PayrollProcess PayrollProcess
        {
            get
            {
                if (Data.Count > 0)
                    return (WorkLinks.BusinessLayer.BusinessObjects.PayrollProcess)Data[0];
                else
                    return null;
            }
        }
        private bool ProcessPending
        {
            get
            {
                return PayrollProcessStatusCode.Equals(_statusCalculationPending) || PayrollProcessStatusCode.Equals(_statusPostPending);
            }
        }
        private String PayrollProcessStatusCode
        {
            get
            {
                if (PayrollProcess != null && PayrollProcess.PayrollProcessStatusCode != null)
                    return PayrollProcess.PayrollProcessStatusCode;
                else
                    return String.Empty;
            }
        }
        #endregion


        protected void LoadPayrollProcess()
        {
            Data = Common.ServiceWrapper.PayrollProcess.Select(new PayrollProcessCriteria() { PayrollProcessId = PayrollProcessId });
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            LoadPayrollProcess();
            RedirectBack();
        }

        private void RedirectBack()
        {
            if (!ProcessPending)
            {
                Response.Redirect("~/Payroll/PayrollProcess/PayrollProcessPage.aspx");
            }
        }
    }
}