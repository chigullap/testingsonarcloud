﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayrollProcessSearchControl.ascx.cs" Inherits="WorkLinks.Payroll.PayrollProcess.PayrollProcessSearchControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="SearchPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SearchPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="SearchPanel" runat="server">
    <table width="100%">
        <tr valign="top">
            <td>
                <div>
                    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                        <table width="100%">
                            <tr>
                                <td>
                                    <wasp:ComboBoxControl ID="PayrollProcessGroupCodeControl" LabelText="Process Group" runat="server" Type="PayrollProcessGroupCode" OnDataBinding="Code_NeedDataSource" ResourceName="PayrollProcessGroupCode" ReadOnly="false" TabIndex="010"></wasp:ComboBoxControl>
                                </td>
                                <td>
                                    <wasp:ComboBoxControl ID="PayrollProcessRunTypeCodeControl" LabelText="Run Type" runat="server" Type="PayrollProcessRunTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="PayrollProcessRunTypeCode" ReadOnly="false" TabIndex="020"></wasp:ComboBoxControl>
                                </td>
                            </tr>
                        </table>
                        <div class="SearchCriteriaButtons">
                            <wasp:WLPButton ID="btnClear" Icon-PrimaryIconUrl="~/App_Themes/Default/clear.gif" ResourceName="btnClear" runat="server" Text="Clear" OnClientClicked="clear" OnClick="btnClear_Click" CssClass="button" />
                            <wasp:WLPButton ID="btnSearch" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" ResourceName="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                        </div>
                    </asp:Panel>
                </div>
            </td>
        </tr>
        <tr valign="top">
            <td>
                <div>
                    <wasp:WLPToolBar ID="PayrollProcessSummaryToolBar" runat="server" Width="100%" OnClientLoad="PayrollProcessSummaryToolBar_init" Style="z-index: 1900">
                        <Items>
                            <wasp:WLPToolBarButton OnPreRender="PaySlipsToolBar_PreRender" Text="*PaySlips" onclick="openBulkPaySlip();" runat="server" CommandName="payslips" ResourceName="PaySlips"></wasp:WLPToolBarButton>
                            <wasp:WLPToolBarButton OnPreRender="MassPayslipPrintToolBar_PreRender" Text="*MassPayslipPrint" onclick="openMassPayslipPrint();" runat="server" CommandName="MassPayslipPrint" ResourceName="MassPayslipPrint"></wasp:WLPToolBarButton>
                            <wasp:WLPToolBarButton CommandName="ReportMenuButton" OnPreRender="ReportMenuButton_PreRender">
                                <ItemTemplate>
                                    <telerik:RadMenu ID="ReportRadMenu" ClickToOpen="true" runat="server" EnableRoundedCorners="true" OnClientItemClicked="reportItemClicked" OnClientLoad="reportOnInit" Style="z-index: 1900" EnableAutoScroll="true">
                                        <Items>
                                            <wasp:WLPMenuItem runat="server" Text="*Reports" OnPreRender="ReportsToolBar_PreRender" ResourceName="ReportsMenu">
                                                <Items>
                                                    <wasp:WLPMenuItem runat="server" Text="*Standard" ResourceName="StandardMenu" OnPreRender="StandardMenuItem_PreRender" >
                                                        <Items>
                                                            <wasp:WLPMenuItem runat="server" Text="*CustomerInvoice" ResourceName="CustomerInvoice" OnPreRender="CustomerInvoiceReportMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="CustomerInvoice_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableCustomerInvoiceReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*EftCustomerInvoiceJamaica" ResourceName="EftCustomerInvoiceJamaica" OnPreRender="EftCustomerInvoiceJamaicaReportMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="EftCustomerInvoiceJamaica_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableEftCustomerInvoiceJamaicaReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*Details" ResourceName="Details" OnPreRender="PayRegisterReportMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="PayRegister_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnablePayRegisterReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*RegisterSummary" ResourceName="RegisterSummary" OnPreRender="RegisterSummaryReportMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="RegisterSummary_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableRegisterSummaryReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*Changes" ResourceName="ChangesReport" OnPreRender="ChangesReportMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="Changes_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableChangesReportMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="Changes_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableChangesReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*CompensationList" ResourceName="CompensationListReport" OnPreRender="CompensationListReportMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="CompensationList_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableCompensationListReportMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="CompensationList_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableCompensationListReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*Current vs Prior" ResourceName="CurrentVsPriorReport" OnPreRender="CurrentVsPriorReportMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="CurrentVsPrior_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableCurrentVsPriorReportMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="CurrentVsPrior_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableCurrentVsPriorReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*Net Current vs Prior" ResourceName="NetCurrentVsPriorReport" OnPreRender="CurrentVsPriorNetReportMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="CurrentVsPriorNet_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableCurrentVsPriorNetReportMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="CurrentVsPriorNet_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableCurrentVsPriorNetReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*CurrentVsPriorPeriodVariances" ResourceName="CurrentVsPriorPeriodVariances" OnPreRender="CurrentVsPriorPeriodVariances_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="CurrentVsPriorPeriodVariances_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableCurrentVsPriorPeriodVariancesMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="CurrentVsPriorPeriodVariances_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableCurrentVsPriorPeriodVariancesMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*Garnishment" ResourceName="GarnishmentReport" OnPreRender="GarnishmentMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="Garnishment_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableGarnishmentReportMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="Garnishment_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableGarnishmentReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*PayException" ResourceName="GrossNetExceptionReport" OnPreRender="PayExceptionMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="PayException_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnablePayExceptionReportMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="PayException_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePayExceptionReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*Gross vs Salary" ResourceName="GrossVsSalaryReport" OnPreRender="GrossVsSalaryMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="GrossVsSalary_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableGrossVsSalaryReportMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="GrossVsSalary_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableGrossVsSalaryReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*PD7A" ResourceName="PD7AReport" OnPreRender="PD7AMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="PD7AReport_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnablePD7AReportMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="PD7AReport_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePD7AReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*BarbadosDeductionSummary" ResourceName="BarbadosDeductionSummary" OnPreRender="BarbadosDeductionSummaryMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="BarbadosDeductionSummary_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableBarbadosDeductionSummaryMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="BarbadosDeductionSummary_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableBarbadosDeductionSummaryMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*StLuciaDeductionSummary" ResourceName="StLuciaDeductionSummary" OnPreRender="StLuciaDeductionSummaryMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="StLuciaDeductionSummary_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableStLuciaDeductionSummaryMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="StLuciaDeductionSummary_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableStLuciaDeductionSummaryMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*TrinidadDeductionSummary" ResourceName="TrinidadDeductionSummary" OnPreRender="TrinidadDeductionSummaryMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="TrinidadDeductionSummary_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableTrinidadDeductionSummaryMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="TrinidadDeductionSummary_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableTrinidadDeductionSummaryMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*JamaicaDeductionSummary" ResourceName="JamaicaDeductionSummary" OnPreRender="JamaicaDeductionSummaryMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="JamaicaDeductionSummary_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableJamaicaDeductionSummaryMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="JamaicaDeductionSummary_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableJamaicaDeductionSummaryMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*JamaicaGovMonthlyRemittance" ResourceName="JamaicaGovMonthlyRemittance" OnPreRender="JamaicaGovMonthlyRemittanceMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="JamaicaGovMonthlyRemittance_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableJamaicaGovMonthlyRemittanceMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="JamaicaGovMonthlyRemittance_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableJamaicaGovMonthlyRemittanceMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*YTD Bonus Commission" ResourceName="YTDBonusCommishReport" OnPreRender="YtdBonusCommissionMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="YTDBonusCommish_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableYtdBonusCommissionReportMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="YTDBonusCommish_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableYtdBonusCommissionReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*WCB" ResourceName="WCB" OnPreRender="WCBMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="WCB_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableWCBReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*CSB" ResourceName="CSB" OnPreRender="CSBMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="CSB_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableCSBReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*PayDetails" ResourceName="PayDetails" OnPreRender="PayDetailsMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="PayrollDetailsReport_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePayDetailsMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*GLEmployeeDetails" ResourceName="GLEmployeeDetails" OnPreRender="GLEmployeeDetailsMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="GLEmployeeDetailsReport_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableGLDetailsMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*FedProvRemitSummary" ResourceName="FedProvRemitSummary" OnPreRender="FedProvRemitSummaryMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="FedProvRemitSummary_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableFedProvRemitSummaryMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*ProvincialHealthSummary" ResourceName="ProvincialHealthSummary" OnPreRender="ProvincialHealthSummaryMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="ProvincialHealthSummary_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableProvincialHealthSummaryMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*QuebecRemitSummary" ResourceName="QuebecRemitSummary" OnPreRender="QuebecRemitSummaryMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="QuebecRemitSummary_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableQuebecRemitSummaryMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*PreYET4ADetail" ResourceName="PreYET4ADetail" OnPreRender="PreYET4ADetailMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="PreYET4ADetail_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePreYET4ADetailMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*PreYET4Detail" ResourceName="PreYET4Detail" OnPreRender="PreYET4DetailMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="PreYET4Detail_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePreYET4DetailMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*PreYER1Detail" ResourceName="PreYER1Detail" OnPreRender="PreYER1DetailMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="PreYER1Detail_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePreYER1DetailMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*PreYER2Detail" ResourceName="PreYER2Detail" OnPreRender="PreYER2DetailMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="PreYER2DetailReport_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePreYER2DetailMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*PIERPreYE" ResourceName="PIERPreYE" OnPreRender="PIERPreYEMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="PreYearEndPIER_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePIERPreYEMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*BenefitArrears" ResourceName="BenefitArrears" OnPreRender="BenefitArrearsMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="BenefitArrearsReport_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableBenefitArrearsMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*PaymentList" ResourceName="PaymentList" OnPreRender="PaymentListMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="PaymentList_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnablePaymentListReportMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="PaymentList_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePaymentListReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*Vacation" ResourceName="VacationReport" OnPreRender="VacationReportMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="VacationReport_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableVacationReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*AccumulatedHours" ResourceName="AccumulatedHours" OnPreRender="AccumulatedHours_OnPreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="AccumHoursSummaryRateProgression_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableAccumulatedHoursReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                        </Items>
                                                    </wasp:WLPMenuItem>
                                                    <wasp:WLPMenuItem runat="server" Text="*Custom" ResourceName="CustomMenu" OnPreRender="CustomMenuItem_PreRender">
                                                        <Items>
                                                            <wasp:WLPMenuItem runat="server" Text="*Pension" ResourceName="PensionReport" OnPreRender="PensionMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="Pension_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnablePensionReportMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="Pension_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePensionReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*EmployeeSalaryReport" ResourceName="EmployeeSalaryReport" OnPreRender="EmployeeSalaryReportMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="EmployeeSalaryReport_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableEmployeeSalaryReportMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="EmployeeSalaryReport_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableEmployeeSalaryReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*UnitedWayReport" ResourceName="UnitedWayReport" OnPreRender="UnitedWayReportMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="UnitedWayReport_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableUnitedWayReportMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="UnitedWayReport_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableUnitedWayReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*SerpYtdReport" ResourceName="SerpYtdReport" OnPreRender="EnableSerpYtdReportMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="SerpYtdReport_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableSerpYtdReportMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="SerpYtdReport_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableSerpYtdReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*PensionDemographicsReport" ResourceName="PensionDemographicsReport" OnPreRender="PensionDemographicsMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="PensionDemographicsReport_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePensionDemographicsMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*SrpReport" ResourceName="SrpReport" OnPreRender="SrpReportMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="SrpReport_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableSrpReportMenuItem %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="SrpReport_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableSrpReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*PreYearEndDetail" ResourceName="PreYearEndDetail" OnPreRender="PreYearEndDetailMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="PreYearEndDetail_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePreYearEndDetailMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*A1235GLDetailsReport" ResourceName="A1235GLDetailsReport" OnPreRender="A1235GLDetailsReportMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="A1235GLDetailsReport_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableA1235GLDetailsReportMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*CurrentVsPriorTwoPeriodVariances" ResourceName="CurrentVsPriorTwoPeriodVariances" OnPreRender="CurrentVsPriorTwoPeriodVariances_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="CurrentVsPriorTwoPeriodVariances_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableCurrentVsPriorTwoPeriodVariances %>'></wasp:WLPMenuItem>
                                                                    <wasp:WLPMenuItem Value="CurrentVsPriorTwoPeriodVariances_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableCurrentVsPriorTwoPeriodVariances %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*MonthlyStatutoryRemittance" ResourceName="MonthlyStatutoryRemittance" OnPreRender="MonthlyStatutoryRemittance_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="MonthlyStatutoryRemittance_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableMonthlyStatutoryRemittance %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                            <wasp:WLPMenuItem runat="server" Text="*ResearchAndDevelopment" ResourceName="ResearchAndDevelopmentMenu" OnPreRender="ResearchAndDevelopmentMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="ResearchAndDevelopment_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableResearchAndDevelopmentMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem> 
                                                            <wasp:WLPMenuItem runat="server" Text="*PayrollDetailsOrgUnit" ResourceName="PayrollDetailsOrgUnit" OnPreRender="PayrollDetailsOrgUnitMenuItem_PreRender">
                                                                <Items>
                                                                    <wasp:WLPMenuItem Value="PayrollDetailsOrgUnit_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePayrollDetailsOrgUnitMenuItem %>'></wasp:WLPMenuItem>
                                                                </Items>
                                                            </wasp:WLPMenuItem>
                                                        </Items>
                                                    </wasp:WLPMenuItem>
                                                </Items>
                                            </wasp:WLPMenuItem>
                                        </Items>
                                    </telerik:RadMenu> 
                                </ItemTemplate>
                            </wasp:WLPToolBarButton>
                            <wasp:WLPToolBarDropDown runat="server" OnPreRender="Export_PreRender" Text="*Export" CommandName="ExportMenuButton" ResourceName="Export" Enabled="false">
                                <Buttons>
                                    <wasp:WLPToolBarButton OnPreRender="CSBExportToolBar_PreRender" Text="*CSBExport" onclick="openCSBExport();" runat="server" CommandName="CSBExport" ResourceName="CSBExport"></wasp:WLPToolBarButton>
                                    <wasp:WLPToolBarButton OnPreRender="CeridianExportToolBar_PreRender" Text="*CeridianExport" onclick="openCeridianExport();" runat="server" CommandName="CeridianExport" ResourceName="CeridianExport"></wasp:WLPToolBarButton>
                                    <wasp:WLPToolBarButton OnPreRender="EFTToolBar_PreRender" Text="*EFT" onclick="openEFT();" runat="server" CommandName="EFT" ResourceName="EFT"></wasp:WLPToolBarButton>
                                    <wasp:WLPToolBarButton OnPreRender="PensionExportToolBar_PreRender" Text="*PensionExport" onclick="openPensionExport();" runat="server" CommandName="PensionExport" ResourceName="PensionExport"></wasp:WLPToolBarButton>
                                    <wasp:WLPToolBarButton OnPreRender="GLToolBar_PreRender" Text="*GL" onclick="openGLExport();" runat="server" CommandName="GL" ResourceName="GL"></wasp:WLPToolBarButton>
                                    <wasp:WLPToolBarButton OnPreRender="RRSPToolBar_PreRender" Text="*RRSP" onclick="openRRSPExport();" runat="server" CommandName="RRSP" ResourceName="RRSP"></wasp:WLPToolBarButton>
                                    <wasp:WLPToolBarButton OnPreRender="StockToolBar_PreRender" Text="*STOCK" onclick="openStockExport();" runat="server" CommandName="STOCK" ResourceName="STOCK"></wasp:WLPToolBarButton>
                                    <wasp:WLPToolBarButton OnPreRender="EmpowerRemittanceToolBar_PreRender" Text="*EmpowerRemittance" onclick="openEmpowerRemittanceExport();" runat="server" CommandName="EmpowerRemittance" ResourceName="EmpowerRemittance"></wasp:WLPToolBarButton>
                                    <wasp:WLPToolBarButton OnPreRender="EmpowerGarnishmentToolBar_PreRender" Text="*EmpowerGarnishment" ImageUrl="" onclick="openEmpowerGarnishmentExport();" runat="server" CommandName="EmpowerGarnishment" ResourceName="EmpowerGarnishment"></wasp:WLPToolBarButton>
                                    <wasp:WLPToolBarButton OnPreRender="CICPlusWeeklyToolBar_PreRender" Text="*CICPlusWeekly" ImageUrl="" onclick="openCICPlusWeekly();" runat="server" CommandName="CICPlusWeekly" ResourceName="CICPlusWeekly"></wasp:WLPToolBarButton>
                                    <wasp:WLPToolBarButton OnPreRender="CraGarnishmentToolBar_PreRender" Text="*CraGarnishment" ImageUrl="" onclick="openCraGarnishment();" runat="server" CommandName="CraGarnishment" ResourceName="CraGarnishment"></wasp:WLPToolBarButton>
                                    <wasp:WLPToolBarButton OnPreRender="SocialCostsToolBar_PreRender" Text="*SocialCostExport" ImageUrl="" onclick="openSocialCost();" runat="server" CommandName="SocialCostExport" ResourceName="SocialCostExport"></wasp:WLPToolBarButton>
                                    <wasp:WLPToolBarButton OnPreRender="RealCustomExportToolBar_PreRender" Text="*RealCustomExport" ImageUrl="" onclick="openRealCustomExport();" runat="server" CommandName="RealCustomExport" ResourceName="RealCustomExport"></wasp:WLPToolBarButton>
                                </Buttons> 
                            </wasp:WLPToolBarDropDown>
                            <wasp:WLPToolBarButton OnPreRender="TransmitFilesToolBar_PreRender" Text="*TransmitFiles" onclick="openTransmitFiles();" CommandName="TransmitFiles" ResourceName="TransmitFiles"></wasp:WLPToolBarButton>
                            <wasp:WLPToolBarButton OnPreRender="EFTPaymentToolBar_PreRender" Text="*EFTPayment" onclick="openEFTPayment();" CommandName="EFTPayment" ResourceName="EFTPayment"></wasp:WLPToolBarButton>
                            <wasp:WLPToolBarButton OnPreRender="RealExportToolBar_PreRender" Text="*RealExport" onclick="openRealExport();" CommandName="RealExport" ResourceName="RealExport"></wasp:WLPToolBarButton>
                        </Items>
                    </wasp:WLPToolBar>
                    <wasp:WLPGrid
                        ID="PayrollProcessSummaryGrid"
                        runat="server"
                        AllowPaging="true"
                        PagerStyle-AlwaysVisible="true"
                        PageSize="100"
                        AllowSorting="true"
                        GridLines="None"
                        Height="400px"
                        AutoAssignModifyProperties="true">

                        <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="false">
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                            <ClientEvents OnRowClick="OnRowClick" OnCommand="onCommand" />
                        </ClientSettings>

                        <MasterTableView
                            ClientDataKeyNames="PayrollProcessId, PeriodYear, Period, ChequeDate, UpdateDateTime, FileTransmissionDate, EftFileTransmissionDate, PayrollProcessGroupCode, PayrollProcessRunTypeCode"
                            AutoGenerateColumns="false"
                            DataKeyNames="PayrollProcessId, PeriodYear, Period, ChequeDate, UpdateDateTime, FileTransmissionDate, EftFileTransmissionDate, PayrollProcessGroupCode, PayrollProcessRunTypeCode"
                            CommandItemDisplay="Top"
                            AllowSorting="false">

                            <CommandItemTemplate></CommandItemTemplate>
                            <CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>

                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="PeriodYear, Period, ChequeDate" SortOrder="Descending" />
                            </SortExpressions>

                            <Columns>
                                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="*Group" SortExpression="PayrollProcessGroupCode" UniqueName="PayrollProcessGroupCode" DataField="PayrollProcessGroupCode" Type="PayrollProcessGroupCode" ResourceName="PayrollProcessGroupCode">
                                    <HeaderStyle Width="140px" />
                                </wasp:GridKeyValueControl>
                                <wasp:GridNumericControl DataField="PeriodYear" LabelText="*PeriodYear" SortExpression="PeriodYear" UniqueName="PeriodYear" DataType="System.Int64" ResourceName="PeriodYear"></wasp:GridNumericControl>
                                <wasp:GridNumericControl DataField="Period" LabelText="*Period" SortExpression="Period" UniqueName="Period" DataType="System.Int64" ResourceName="Period">
                                    <HeaderStyle Width="70px" />
                                </wasp:GridNumericControl>
                                <wasp:GridNumericControl LabelText="" ResourceName="" DataField="EmptyColumnText">
                                    <HeaderStyle Width="40px" />
                                </wasp:GridNumericControl>
                                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="*RunType" SortExpression="PayrollProcessRunTypeCode" UniqueName="PayrollProcessRunTypeCode" DataField="PayrollProcessRunTypeCode" Type="PayrollProcessRunTypeCode" ResourceName="PayrollProcessRunTypeCode">
                                    <HeaderStyle Width="80px" />
                                </wasp:GridKeyValueControl>
                                <wasp:GridDateTimeControl DataField="StartDate" LabelText="*StartDate" SortExpression="StartDate" UniqueName="StartDate" ResourceName="StartDate"></wasp:GridDateTimeControl>
                                <wasp:GridDateTimeControl DataField="CutoffDate" LabelText="*CutoffDate" SortExpression="CutoffDate" UniqueName="CutoffDate" ResourceName="CutoffDate"></wasp:GridDateTimeControl>
                                <wasp:GridDateTimeControl DataField="ChequeDate" LabelText="*ChequeDate" SortExpression="ChequeDate" UniqueName="ChequeDate" ResourceName="ChequeDate"></wasp:GridDateTimeControl>
                                <wasp:GridDateTimeControl DataField="ProcessedDate" LabelText="*PostedTime" SortExpression="PostedTime" DataFormatString="{0:yyyy-MM-dd HH:mm}" UniqueName="PostedTime" ResourceName="PostedTime">
                                    <HeaderStyle Width="120px" />
                                </wasp:GridDateTimeControl>
                                <wasp:GridBoundControl DataField="PostedUser" LabelText="*PostedUser" SortExpression="PostedUser" UniqueName="PostedUser" ResourceName="PostedUser"></wasp:GridBoundControl>
                                <wasp:GridDateTimeControl DataField="FileTransmissionDate" LabelText="*TransDate" SortExpression="FileTransmissionDate" UniqueName="FileTransmissionDate" ResourceName="FileTransmissionDate"></wasp:GridDateTimeControl>
                                <wasp:GridDateTimeControl DataField="EftFileTransmissionDate" LabelText="*EftTransDate" SortExpression="EftFileTransmissionDate" UniqueName="EftFileTransmissionDate" ResourceName="EftFileTransmissionDate"></wasp:GridDateTimeControl>
                            </Columns>
                        </MasterTableView>

                        <HeaderContextMenu EnableAutoScroll="true"></HeaderContextMenu>
                    </wasp:WLPGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="PayrollProcessWindow"
    Width="800"
    Height="600"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="CSBExportWindow"
    Width="900"
    Height="600"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="SubmitPayment"
    Width="600"
    Height="300"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    OnClientClose="onClientClose"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="RealExportToFtp"
    Width="600"
    Height="300"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var payrollProcessId;
        var periodYear;
        var period;
        var payrollProcessRunTypeCode;
        var updateDateTime;
        var fileTransmissionDate;
        var eftTransmissionDate;
        var payrollProcessGroupCode;
        var reportMenu;
        var pensionExportSupportDate = new Date('<%= CeridianExportSupportDate %>');
        var toolbar = null;

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function clear() {
            payrollProcessId = null;
            periodYear = null;
            period = null;
            payrollProcessRunTypeCode = null;
            updateDateTime = null;
            fileTransmissionDate = null;
            eftTransmissionDate = null;
            payrollProcessGroupCode = null;
        }

        function PayrollProcessSummaryToolBar_init(sender) {
            initializeControls();
        }

        function initializeControls() {
            if ($find('<%= PayrollProcessSummaryToolBar.ClientID %>') != null)
                toolbar = $find('<%= PayrollProcessSummaryToolBar.ClientID %>');

            enableButtons(getSelectedRow() != null);

            payrollProcessId = null;
            periodYear = null;
            period = null;
            payrollProcessRunTypeCode = null;
            updateDateTime = null;
            fileTransmissionDate = null;
            eftTransmissionDate = null;
            payrollProcessGroupCode = null;
        }

        function getSelectedRow() {
            var payrollProcessSummaryGrid = $find('<%= PayrollProcessSummaryGrid.ClientID %>');
            if (payrollProcessSummaryGrid == null)
                return null;

            var MasterTable = payrollProcessSummaryGrid.get_masterTableView();
            if (MasterTable == null)
                return null;

            var selectedRow = MasterTable.get_selectedItems();
            if (selectedRow.length == 1)
                return selectedRow[0];
            else
                return null;
        }

        function OnRowClick(sender, eventArgs) {
            payrollProcessId = eventArgs.getDataKeyValue('PayrollProcessId');
            periodYear = eventArgs.getDataKeyValue('PeriodYear');
            period = eventArgs.getDataKeyValue('Period');
            payrollProcessRunTypeCode = eventArgs.getDataKeyValue('PayrollProcessRunTypeCode');
            updateDateTime = new Date(eventArgs.getDataKeyValue('UpdateDateTime'));
            fileTransmissionDate = eventArgs.getDataKeyValue('FileTransmissionDate');
            eftTransmissionDate = eventArgs.getDataKeyValue('EftFileTransmissionDate');
            payrollProcessGroupCode = eventArgs.getDataKeyValue('PayrollProcessGroupCode');

            enableButtons(true);
        }

        function enableButtons(enable) {
            
            var payslipsButton = toolbar.findButtonByCommandName('payslips');
            var massPayslipPrintButton = toolbar.findButtonByCommandName('MassPayslipPrint');
            var ceridianExportButton = toolbar.findButtonByCommandName('CeridianExport');
            var reportsMenuButton = toolbar.findButtonByCommandName('ReportMenuButton');
            var pensionExportButton = toolbar.findButtonByCommandName('PensionExport');
            var csbExportButton = toolbar.findButtonByCommandName('CSBExport');
            var exportMenuButton = null;
            var eftButton = toolbar.findButtonByCommandName('EFT');
            var glButton = toolbar.findButtonByCommandName('GL');
            var rrspButton = toolbar.findButtonByCommandName('RRSP');
            var stockButton = toolbar.findButtonByCommandName('STOCK');
            var empowerRemittanceButton = toolbar.findButtonByCommandName('EmpowerRemittance');
            var empowerGarnishmentButton = toolbar.findButtonByCommandName('EmpowerGarnishment');
            var transmitFilesButton = toolbar.findButtonByCommandName('TransmitFiles');
            var eftPaymentButton = toolbar.findButtonByCommandName('EFTPayment');
            var cicPlusWeeklyButton = toolbar.findButtonByCommandName('CICPlusWeekly');
            var craGarnishmentButton = toolbar.findButtonByCommandName('CraGarnishment');
            var socialCostsButton = toolbar.findButtonByCommandName('SocialCostExport');
            var realCustomExportDataButton = toolbar.findButtonByCommandName('RealCustomExport');

            if (payslipsButton != null)
                payslipsButton.set_enabled(enable);

            if (massPayslipPrintButton != null)
                massPayslipPrintButton.set_enabled(enable);

             if (transmitFilesButton != null) {
                if (fileTransmissionDate == null && payrollProcessRunTypeCode != "ADJUST")
                    transmitFilesButton.set_enabled(enable);
                else
                    transmitFilesButton.set_enabled(false);
            }

            if (eftPaymentButton != null) {
                if (eftTransmissionDate == null && payrollProcessRunTypeCode != "ADJUST")
                    eftPaymentButton.set_enabled(enable);
                else
                    eftPaymentButton.set_enabled(false);
            }

            if (reportsMenuButton != null) {
                reportsMenuButton.set_enabled(enable);
                reportMenu.set_enabled(enable);
            }

            //export items
            if (ceridianExportButton != null) {
                exportMenuButton = ceridianExportButton.get_parent();
                if (updateDateTime == null)
                    ceridianExportButton.set_enabled(enable);
                else if (updateDateTime >= pensionExportSupportDate)
                    ceridianExportButton.set_enabled(enable);
                else
                    ceridianExportButton.set_enabled(false);
            }

            if (pensionExportButton != null) {
                pensionExportButton.set_enabled(enable);
                exportMenuButton = pensionExportButton.get_parent();
            }

            if (csbExportButton != null) {
                csbExportButton.set_enabled(enable);
                exportMenuButton = csbExportButton.get_parent();
            }

            if (eftButton != null) {
                if (payrollProcessRunTypeCode != "ADJUST") {
                    eftButton.set_enabled(enable);
                    exportMenuButton = eftButton.get_parent();
                }
            }

            if (glButton != null) {
                glButton.set_enabled(enable);
                exportMenuButton = glButton.get_parent();
            }

            if (rrspButton != null) {
                rrspButton.set_enabled(enable);
                exportMenuButton = rrspButton.get_parent();
            }

            if (stockButton != null) {
                stockButton.set_enabled(enable);
                exportMenuButton = stockButton.get_parent();
            }

            if (empowerRemittanceButton != null) {
                empowerRemittanceButton.set_enabled(enable);
                exportMenuButton = empowerRemittanceButton.get_parent();
            }

            if (empowerGarnishmentButton != null) {
                empowerGarnishmentButton.set_enabled(enable);
                exportMenuButton = empowerGarnishmentButton.get_parent();
            }

            if (cicPlusWeeklyButton != null) {
                cicPlusWeeklyButton.set_enabled(enable);
                exportMenuButton = cicPlusWeeklyButton.get_parent();
            }

            if (craGarnishmentButton != null) {
                craGarnishmentButton.set_enabled(enable);
                exportMenuButton = craGarnishmentButton.get_parent();
            }

            if (socialCostsButton != null) {
                socialCostsButton.set_enabled(enable);
                exportMenuButton = socialCostsButton.get_parent();
            }

            if (realCustomExportDataButton != null) {
                realCustomExportDataButton.set_enabled(enable);
                exportMenuButton = realCustomExportDataButton.get_parent();
            }            

            //this has to be the last statement for the Export code
            if (exportMenuButton != null)
                exportMenuButton.set_enabled(enable);
        }

        function openTransmitFiles() {
            if (payrollProcessId != null && fileTransmissionDate == null && payrollProcessRunTypeCode != "ADJUST") {
                if (checkTransmitFilesConfirmation())
                    __doPostBack('<%= this.ClientID %>', String.format('exportFtpPayrollProcessId={0},periodYear={1}', payrollProcessId, periodYear));
            }
        }

        function openEFTPayment() {
            if (payrollProcessId != null && eftTransmissionDate == null && payrollProcessRunTypeCode != "ADJUST") {
                openWindowWithManager('SubmitPayment', String.format('/Payroll/PayrollProcess/EftPayment/{0}', payrollProcessId), false);
                return false;
            }
        }

        function openRealExport() {
            openWindowWithManager('RealExportToFtp', String.format('/Export/{0}', 'ALL'), false);
            return false;
        }

        function checkTransmitFilesConfirmation(sender, args) {
            var message = "<asp:Literal runat="server" Text="<%$ Resources:WarningMessages, TransmitFiles %>" />";
            return confirm(message);
        }

        function openBulkPaySlip() {
            if (payrollProcessId != null)
                __doPostBack('<%= this.ClientID %>', String.format('payrollProcessId={0}', payrollProcessId));
        }

        function openMassPayslipPrint() {
            if (payrollProcessId != null) {
                openWindowWithManager('PayrollProcessWindow', String.format('/Report/MassPaySlipPrint/{0}/{1}/{2}/{3}/{4}/{5}/{6}', payrollProcessId, 'mass', 'PDF', 'x', 'x', 'x', 'x'), true);
                return false;
            }
        }

        function openCeridianExport() {
            if (payrollProcessId != null && updateDateTime != null && updateDateTime >= pensionExportSupportDate)
                __doPostBack('<%= this.ClientID %>', String.format('exportPayrollProcessId={0},periodYear={1},period={2}', payrollProcessId, periodYear, period));
        }

        function openEFT() {
            if (payrollProcessId != null && payrollProcessRunTypeCode != "ADJUST")
                __doPostBack('<%= this.ClientID %>', String.format('eftPayrollProcessId={0}', payrollProcessId));
        }

        function openGLExport() {
            if (payrollProcessId != null)
                __doPostBack('<%= this.ClientID %>', String.format('GLPayrollProcessId={0}', payrollProcessId));
        }

        function openRRSPExport() {
            if (payrollProcessId != null)
                __doPostBack('<%= this.ClientID %>', String.format('RRSPPayrollProcessId={0}', payrollProcessId));
        }

        function openStockExport() {
            if (payrollProcessId != null)
                __doPostBack('<%= this.ClientID %>', String.format('StockPayrollProcessId={0}', payrollProcessId));
        }

        function openEmpowerRemittanceExport() {
            if (payrollProcessId != null)
                __doPostBack('<%= this.ClientID %>', String.format('EmpowerRemitPayrollProcessId={0}', payrollProcessId));
        }

        function openEmpowerGarnishmentExport() {
            if (payrollProcessId != null)
                __doPostBack('<%= this.ClientID %>', String.format('EmpowerGarnishmentPayrollProcessId={0}', payrollProcessId));
        }

        function openCICPlusWeekly(){
            if(periodYear != null)
                __doPostBack('<%= this.ClientID %>', String.format('CicPlusWeeklyPeriodYear={0}', periodYear));
        }

        function openCraGarnishment(){
            if (payrollProcessId != null)
                __doPostBack('<%= this.ClientID %>', String.format('craGarnishmentPayrollProcessId={0}', payrollProcessId));
        }       

        function openPensionExport() {
            if (payrollProcessId != null)
                __doPostBack('<%= this.ClientID %>', String.format('pensionPayrollProcessId={0}', payrollProcessId));
        }

        function openCSBExport() {
            if (payrollProcessId != null) {
                openWindowWithManager('CSBExportWindow', String.format('/CSB/CSBExport/{0}', payrollProcessId), false);
                return false;
            }
        }

        function openSocialCost(){
            if (payrollProcessId != null)
                __doPostBack('<%= this.ClientID %>', String.format('socialCostPayrollProcessId={0}', payrollProcessId));
        }		

        function openRealCustomExport(){
            if (payrollProcessId != null)
                __doPostBack('<%= this.ClientID %>', String.format('openRealCustomExportPayrollProcessId={0}', payrollProcessId));
        }	
        
        function onCommand(sender, args) {
            if (args.get_commandName() == 'Page')
                initializeControls();
        }

        //opens selected report
        function reportItemClicked(sender, eventArgs) {
            menuValue = eventArgs.get_item().get_value();
            if (menuValue != null && payrollProcessId != null) {
                var splitterIndex = menuValue.lastIndexOf("_");
                var reportName = menuValue.substr(0, splitterIndex);
                var outputType = menuValue.substr(splitterIndex + 1, 99);

                //if excel format, do not open a child window.
                if (outputType == 'EXCEL')
                    invokeDownload(reportName, outputType);
                else if (reportName == 'MonthlyStatutoryRemittance') {
                    openExternalLinkWithManager('<%= MonthlyStatutoryRemittanceURL %>');
                }
                else
                    openWindowWithManager('PayrollProcessWindow', String.format('/Report/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}', reportName, payrollProcessId, 'null', outputType, periodYear, 'x', 'x', 'x'), true);
            }
        }

        function invokeDownload(reportName, outputType) {
            var frame = document.createElement("frame");
            frame.src = getUrl(String.format('/Report/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}', reportName, payrollProcessId, 'null', outputType, periodYear, 'x', 'x', 'x')), null;
            frame.style.display = "none";
            document.body.appendChild(frame);
        }

        //sets the handler for report menu
        function reportOnInit(sender, eventArgs) {
            reportMenu = sender;
        }

        function onClientClose(sender, eventArgs) {
            __doPostBack('<%= PayrollProcessSummaryGrid.ClientID %>', '<%= String.Format("refresh") %>');
        }
    </script>
</telerik:RadScriptBlock>