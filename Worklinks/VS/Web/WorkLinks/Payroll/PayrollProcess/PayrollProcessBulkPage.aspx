﻿<%@ Page Title="" Async="true" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="PayrollProcessBulkPage.aspx.cs" Inherits="WorkLinks.Payroll.PayrollProcess.PayrollProcessBatchPage" %>

<%@ Register Src="~/Payroll/PayrollProcess/PayrollProcessBulkControl.ascx" TagPrefix="uc1" TagName="PayrollProcessBulkControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:PayrollProcessBulkControl runat="server" id="PayrollProcessBulkControl" />
</asp:Content>
