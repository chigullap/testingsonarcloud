﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PayrollExceptionPage.aspx.cs" Inherits="WorkLinks.Payroll.PayrollException.PayrollExceptionPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <iframe id="payrollException" class="w-100" onload="onLoadIFrame()" style="border: none;" src="<%= ConfigurationManager.AppSettings.Get("ANGULARURL") %>/payroll-exception"></iframe>    
    </ContentTemplate>
</asp:Content>