﻿//using System;
//using Telerik.Web.UI;
//using WorkLinks.BusinessLayer.BusinessObjects;

//namespace WorkLinks.Payroll.Remittance
//{
//    public partial class RemittanceImportApprovalControl : WLP.Web.UI.WLPUserControl
//    {
//        #region properties
//        protected String GrossPayroll
//        {
//            get
//            {
//                String grossPayroll = null;

//                if (Data != null && Data.Count > 0)
//                    grossPayroll = String.Format("{0:$###,##0.00}", ((RemittanceImportDetail)Data[0]).GrossPayroll);

//                return grossPayroll;
//            }
//        }
//        protected String EmployeeCount
//        {
//            get
//            {
//                String employeeCount = null;

//                if (Data != null && Data.Count > 0)
//                    employeeCount = Convert.ToString(((RemittanceImportDetail)Data[0]).EmployeeCount);

//                return employeeCount;
//            }
//        }
//        protected long RemittanceImportId { get { return Convert.ToInt64(Page.RouteData.Values["remittanceImportId"]); } }
//        #endregion

//        #region main
//        protected void Page_Load(object sender, EventArgs e)
//        {
//            if (!Page.IsPostBack)
//                SetAuthorized(Common.Security.RoleForm.RemittanceImportApproval.ViewFlag);

//            WireEvents();

//            if (!IsPostBack)
//                LoadRemittanceImportInformation();
//        }
//        protected void WireEvents()
//        {
//            RemittanceImportApprovalGrid.NeedDataSource += new GridNeedDataSourceEventHandler(RemittanceImportApprovalGrid_NeedDataSource);
//        }
//        protected void LoadRemittanceImportInformation()
//        {
//            Data = RemittanceImportDetailCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetRemittanceImportDetail(RemittanceImportId));
//        }
//        #endregion

//        #region event handlers
//        void RemittanceImportApprovalGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
//        {
//            RemittanceImportApprovalGrid.DataSource = Data;
//        }
//        protected void RemittanceImportApprovalToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
//        {
//            String remittanceImportStatusCode = null;

//            if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("approve"))
//                remittanceImportStatusCode = "APP";
//            else if (((RadToolBarButton)e.Item).CommandName.ToLower().Equals("reject"))
//                remittanceImportStatusCode = "REJ";

//            Common.ServiceWrapper.HumanResourcesClient.ProcessRemittanceImport(RemittanceImportId, remittanceImportStatusCode);

//            //close the RadWindow
//            System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "close and return", "closeAndReturn();", true);
//        }
//        #endregion
//    }
//}