﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="RemittanceImportApprovalPage.aspx.cs" Inherits="WorkLinks.Payroll.Remittance.RemittanceImportApprovalPage" %>
<%@ Register Src="~/Payroll/Remittance/RemittanceImportApprovalControl.ascx" TagPrefix="uc1" TagName="RemittanceImportApprovalControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:RemittanceImportApprovalControl runat="server" id="RemittanceImportApprovalControl" />
</asp:Content>