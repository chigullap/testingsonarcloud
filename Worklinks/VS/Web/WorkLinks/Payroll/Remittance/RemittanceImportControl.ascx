﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RemittanceImportControl.ascx.cs" Inherits="WorkLinks.Payroll.Remittance.RemittanceImportControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="SearchPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SearchPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="SearchPanel" runat="server">
    <wasp:WLPToolBar ID="RemittanceImportToolBar" runat="server" Width="100%" OnClientLoad="RemittanceImportToolBar_init" Style="z-index: 1900">
        <Items>
            <wasp:WLPToolBarButton OnPreRender="ImportToolBar_PreRender" Text="*Import" onclick="Import();" CommandName="import" ResourceName="Import"></wasp:WLPToolBarButton>
            <wasp:WLPToolBarButton OnPreRender="ApprovalToolBar_PreRender" Text="*Approval" onclick="Approval();" CommandName="approval" ResourceName="Approval"></wasp:WLPToolBarButton>
        </Items>
    </wasp:WLPToolBar>

    <wasp:WLPGrid
        ID="RemittanceImportGrid"
        runat="server"
        AllowPaging="true"
        PagerStyle-AlwaysVisible="true"
        PageSize="100"
        AllowSorting="true"
        GridLines="None"
        Height="400px"
        AutoAssignModifyProperties="true">

        <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="false">
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
            <ClientEvents OnRowClick="onRowClick" OnGridCreated="onGridCreated" OnCommand="onCommand" />
        </ClientSettings>

        <MasterTableView
            AutoGenerateColumns="false"
            ClientDataKeyNames="RemittanceImportId, RemittanceImportStatusCode"
            DataKeyNames="RemittanceImportId, RemittanceImportStatusCode"
            CommandItemDisplay="Top"
            AllowSorting="false">

            <CommandItemTemplate></CommandItemTemplate>

            <Columns>
                <wasp:GridDateTimeControl DataField="ImportDate" LabelText="*ImportDate" SortExpression="ImportDate" UniqueName="ImportDate" ResourceName="ImportDate">
                    <HeaderStyle Width="120px" />
                </wasp:GridDateTimeControl>
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="*RemittanceImportStatusCode" SortExpression="RemittanceImportStatusCode" DataField="RemittanceImportStatusCode" Type="RemittanceImportStatusCode" ResourceName="RemittanceImportStatusCode">
                    <HeaderStyle Width="140px" />
                </wasp:GridKeyValueControl>
                <wasp:GridBoundControl DataField="Notes" LabelText="*Notes" SortExpression="Notes" UniqueName="Notes" ResourceName="Notes">
                    <HeaderStyle Width="400px" />
                </wasp:GridBoundControl>
                <wasp:GridCheckBoxControl DataField="WarningFlag" LabelText="*WarningFlag" SortExpression="WarningFlag" UniqueName="WarningFlag" ResourceName="WarningFlag">
                    <HeaderStyle Width="100px" />
                </wasp:GridCheckBoxControl>
            </Columns>

        </MasterTableView>

        <HeaderContextMenu EnableAutoScroll="true"></HeaderContextMenu>

    </wasp:WLPGrid>
</asp:Panel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="RemittanceImportWindow"
    Width="800"
    Height="600"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientClose="onClientClose"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var toolbar = null;
        var remittanceImportId;
        var remittanceImportStatusCode;
        
        function onClientClose(sender, eventArgs) {
            __doPostBack('<%= SearchPanel.ClientID %>', 'refresh');
        }

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function RemittanceImportToolBar_init(sender) {
            initializeControls();
        }

        function initializeControls() {
            if ($find('<%= RemittanceImportToolBar.ClientID %>') != null)
                toolbar = $find('<%= RemittanceImportToolBar.ClientID %>');

            enableButtons(getSelectedRow() != null);

            remittanceImportId = null;
            remittanceImportStatusCode = null;
        }

        function getSelectedRow() {
            var remittanceImportGrid = $find('<%= RemittanceImportGrid.ClientID %>');
            if (remittanceImportGrid == null)
                return null;

            var masterTable = remittanceImportGrid.get_masterTableView();
            if (masterTable == null)
                return null;

            var selectedRow = masterTable.get_selectedItems();
            if (selectedRow.length == 1)
                return selectedRow[0];
            else
                return null;
        }

        function onGridCreated(sender, eventArgs) {
            var selectedRow = getSelectedRow();
            if (selectedRow != null) {
                remittanceImportId = selectedRow.getDataKeyValue('RemittanceImportId');
                remittanceImportStatusCode = selectedRow.getDataKeyValue('RemittanceImportStatusCode');

                enableButtons(true);
            }
        }

        function onRowClick(sender, eventArgs) {
            remittanceImportId = eventArgs.getDataKeyValue('RemittanceImportId');
            remittanceImportStatusCode = eventArgs.getDataKeyValue('RemittanceImportStatusCode');

            enableButtons(true);
        }

        function onCommand(sender, args) {
            if (args.get_commandName() == 'Page') {
                remittanceImportId = null;
                remittanceImportStatusCode = null;

                initializeControls();
            }
        }

        function enableButtons(enable) {
            var approvalButton = toolbar.findButtonByCommandName('approval');
            if (approvalButton != null)
                approvalButton.set_enabled(enable && remittanceImportStatusCode == "NEW");
        }

        function Import() {
            openWindowWithManager('RemittanceImportWindow', '/Admin/ImportExport/Remittance', false);
            return false;
        }

        function Approval() {
            if (remittanceImportId != null && remittanceImportStatusCode == "NEW") {
                openWindowWithManager('RemittanceImportWindow', String.format('/Payroll/Remittance/ImportApproval/{0}', remittanceImportId), false);
                return false;
            }
        }
    </script>
</telerik:RadScriptBlock>