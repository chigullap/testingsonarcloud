﻿using System;
using Telerik.Web.UI;

namespace WorkLinks.Payroll.Remittance.RemittanceDetail
{
    public partial class RemittanceDetailGarnishmentDeductionsControl : WLP.Web.UI.WLPUserControl
    {
        public string DetailType
        {
            get { return Convert.ToString(Page.RouteData.Values["detailType"]); }
        }
        private long? ExportId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["exportId"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.Remittance.ViewFlag);

            WireEvents();

            if (!IsPostBack)
                LoadDetails();

            this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "RemitDetailGarnDed"));
        }
        protected void WireEvents()
        {
            RemittanceDetailGarnishmentGrid.NeedDataSource += new GridNeedDataSourceEventHandler(RemittanceDetailGarnishmentGrid_NeedDataSource);
            RemittanceDetailGarnishmentGrid.AllowPaging = true;
        }

        protected void LoadDetails()
        {
            Data = Common.ServiceWrapper.HumanResourcesClient.GetRemittanceDetailGarnishmentDeductions(DetailType, ExportId);
        }

        protected void RemittanceDetailGarnishmentGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RemittanceDetailGarnishmentGrid.DataSource = Data;
        }
    }
}