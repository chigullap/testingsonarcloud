﻿using System;
using Telerik.Web.UI;

namespace WorkLinks.Payroll.Remittance.RemittanceDetail
{
    public partial class RemittanceDetailWcbDeductionsControl : WLP.Web.UI.WLPUserControl
    {
        public string DetailType
        {
            get { return Convert.ToString(Page.RouteData.Values["detailType"]); }
        }
        public string RemitScheduleCode
        {
            get { return Convert.ToString(Page.RouteData.Values["remitCode"]); }
        }
        private long? ExportId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["exportId"]); }
        }
        private DateTime RemitDate
        {
            get
            {
                if (DetailType == "CRA_WCB")
                    return Convert.ToDateTime(Page.RouteData.Values["remitDate"]);
                else
                    return DateTime.Now; //wont be used in query
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.Remittance.ViewFlag);

            WireEvents();

            if (!IsPostBack)
                LoadDetails();

            this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "RemitDetailWcbDed"));
        }
        protected void WireEvents()
        {
            RemittanceDetailWcbGrid.NeedDataSource += new GridNeedDataSourceEventHandler(RemittanceDetailWcbGrid_NeedDataSource);
            RemittanceDetailWcbGrid.AllowPaging = true;
        }

        protected void LoadDetails()
        {
            Data = Common.ServiceWrapper.HumanResourcesClient.GetRemittanceDetailWcbDeductions(DetailType, ExportId, RemitScheduleCode, RemitDate);
        }

        protected void RemittanceDetailWcbGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RemittanceDetailWcbGrid.DataSource = Data;
        }
    }
}