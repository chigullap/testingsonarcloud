﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RemittanceDetailRqSourceDeductionsControl.ascx.cs" Inherits="WorkLinks.Payroll.Remittance.RemittanceDetail.RemittanceDetailRqSourceDeductionsControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<table width="100%">
    <tr valign="top">
        <td>
            <div>
                <wasp:WLPGrid
                    ID="RemittanceDetailRqGrid"
                    runat="server"
                    AllowPaging="true"
                    PagerStyle-AlwaysVisible="true"
                    PageSize="50"
                    GridLines="None"
                    Height="404px"
                    AutoAssignModifyProperties="true">

                    <clientsettings allowcolumnsreorder="true" reordercolumnsonclient="true">
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="false" />
                        </clientsettings>

                    <mastertableview autogeneratecolumns="false" datakeynames="DummyKey" commanditemdisplay="Top" ShowFooter="true">

                            <CommandItemTemplate></CommandItemTemplate>

                            <Columns>
                                <wasp:GridBoundControl DataField="RevenuQuebecBusinessTaxNumber" LabelText="*RevenuQuebecBusinessTaxNumber" UniqueName="RevenuQuebecBusinessTaxNumber" ResourceName="RevenuQuebecBusinessTaxNumber"></wasp:GridBoundControl>
                                <wasp:GridBoundControl FooterText="Total" DataField="DatabaseColumns" LabelText="*DatabaseColumns" UniqueName="DatabaseColumns" ResourceName="DatabaseColumns">
                                    <HeaderStyle Width="20%" />
                                </wasp:GridBoundControl>
                                <wasp:GridDateTimeControl DataField="ChequeDate" LabelText="*ChequeDate" UniqueName="ChequeDate" ResourceName="ChequeDate"></wasp:GridDateTimeControl>
                                <wasp:GridBoundControl Aggregate="Sum" DataField="PaymentAmount" DataFormatString="{0:$###,##0.00}" LabelText="*PaymentAmount" UniqueName="PaymentAmount" ResourceName="PaymentAmount"></wasp:GridBoundControl>
                                <wasp:GridBoundControl DataField="Period" LabelText="*Period" UniqueName="Period" ResourceName="Period"></wasp:GridBoundControl>
                                <wasp:GridDateTimeControl DataField="StartDate" LabelText="*StartDate" UniqueName="StartDate" ResourceName="StartDate"></wasp:GridDateTimeControl>
                                <wasp:GridDateTimeControl DataField="CutoffDate" LabelText="*CutoffDate" UniqueName="CutoffDate" ResourceName="CutoffDate"></wasp:GridDateTimeControl>
                                <wasp:GridBoundControl DataField="PayrollProcessId" LabelText="*PayrollProcessId" UniqueName="PayrollProcessId" ResourceName="PayrollProcessId"></wasp:GridBoundControl>
                            </Columns>

                        </mastertableview>

                    <headercontextmenu enableautoscroll="true"></headercontextmenu>

                </wasp:WLPGrid>
            </div>
        </td>
    </tr>
</table>

