﻿using System;
using Telerik.Web.UI;


namespace WorkLinks.Payroll.Remittance.RemittanceDetail
{
    public partial class RemittanceDetailRqSourceDeductionsControl : WLP.Web.UI.WLPUserControl
    {
        private long? RqExportId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["exportId"]); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.Remittance.ViewFlag);

            WireEvents();

            if (!IsPostBack)
                LoadDetails();

            this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "RemitDetailRqSourceDed"));
        }

        protected void WireEvents()
        {
            RemittanceDetailRqGrid.NeedDataSource += new GridNeedDataSourceEventHandler(RemittanceDetailRqGrid_NeedDataSource);
            RemittanceDetailRqGrid.AllowPaging = true;
        }

        protected void LoadDetails()
        {
            Data = Common.ServiceWrapper.HumanResourcesClient.GetRemittanceDetailRqSourceDeductions(RqExportId);
        }

        protected void RemittanceDetailRqGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RemittanceDetailRqGrid.DataSource = Data;
        }
    }
}