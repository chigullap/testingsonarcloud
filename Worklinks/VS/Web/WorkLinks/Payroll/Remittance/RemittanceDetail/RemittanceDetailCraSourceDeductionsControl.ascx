﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RemittanceDetailCraSourceDeductionsControl.ascx.cs" Inherits="WorkLinks.Payroll.Remittance.RemittanceDetail.RemittanceDetailCraSourceDeductionsControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<table width="100%">
    <tr valign="top">
        <td>
            <div>
                <wasp:WLPGrid
                    ID="RemittanceDetailCraGrid"
                    runat="server"
                    AllowPaging="true"
                    PagerStyle-AlwaysVisible="true"
                    PageSize="50"
                    GridLines="None"
                    Height="404px"
                    AutoAssignModifyProperties="true">

                    <clientsettings allowcolumnsreorder="true" reordercolumnsonclient="true">
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="false" />
                        </clientsettings>

                    <mastertableview autogeneratecolumns="false" datakeynames="DummyKey" commanditemdisplay="Top" ShowFooter="true">

                            <CommandItemTemplate></CommandItemTemplate>

                            <Columns>
                                <wasp:GridBoundControl FooterText="Total" DataField="BusinessTaxNumber" LabelText="*BusinessTaxNumber" UniqueName="BusinessTaxNumber" ResourceName="BusinessTaxNumber">
                                    <HeaderStyle Width="10%" />
                                </wasp:GridBoundControl>
                                <wasp:GridDateTimeControl DataField="ChequeDate" LabelText="*ChequeDate" UniqueName="ChequeDate" ResourceName="ChequeDate"></wasp:GridDateTimeControl>
                                <wasp:GridBoundControl DataField="EmployeeCount" LabelText="*EmployeeCount" UniqueName="EmployeeCount" ResourceName="EmployeeCount"></wasp:GridBoundControl>
                                <wasp:GridBoundControl Aggregate="Sum" DataFormatString="{0:$###,##0.00}" DataField="PaymentAmount" LabelText="*PaymentAmount" UniqueName="PaymentAmount" ResourceName="PaymentAmount"></wasp:GridBoundControl>
                                <wasp:GridBoundControl DataField="TotalTaxableIncome" DataFormatString="{0:$###,##0.00}" LabelText="*TotalTaxableIncome" UniqueName="TotalTaxableIncome" ResourceName="TotalTaxableIncome"></wasp:GridBoundControl>
                                <wasp:GridBoundControl DataField="Period" LabelText="*Period" UniqueName="Period" ResourceName="Period"></wasp:GridBoundControl>
                                <wasp:GridDateTimeControl DataField="StartDate" LabelText="*StartDate" UniqueName="StartDate" ResourceName="StartDate"></wasp:GridDateTimeControl>
                                <wasp:GridDateTimeControl DataField="CutoffDate" LabelText="*CutoffDate" UniqueName="CutoffDate" ResourceName="CutoffDate"></wasp:GridDateTimeControl>
                                <wasp:GridBoundControl DataField="PayrollProcessId" LabelText="*PayrollProcessId" UniqueName="PayrollProcessId" ResourceName="PayrollProcessId"></wasp:GridBoundControl>
                            </Columns>

                        </mastertableview>

                    <headercontextmenu enableautoscroll="true"></headercontextmenu>

                </wasp:WLPGrid>
            </div>
        </td>
    </tr>
</table>
