﻿using System;
using Telerik.Web.UI;

namespace WorkLinks.Payroll.Remittance.RemittanceDetail
{
    public partial class RemittanceDetailCraSourceDeductionsControl : WLP.Web.UI.WLPUserControl
    {
        #region properties

        public string RemitScheduleCode
        {
            get { return Convert.ToString(Page.RouteData.Values["remitCode"]); }
        }
        private long? ExportId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["exportId"]); }
        }
        private DateTime RemitDate
        {
            get { return Convert.ToDateTime(Page.RouteData.Values["remitDate"]); }
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.Remittance.ViewFlag);

            WireEvents();

            if (!IsPostBack)
                LoadDetails();

            this.Page.Title = String.Format("{0}", GetGlobalResourceObject("PageTitle", "RemitDetailCraSourceDed"));
        }
        protected void WireEvents()
        {
            RemittanceDetailCraGrid.NeedDataSource += new GridNeedDataSourceEventHandler(RemittanceDetailCraGrid_NeedDataSource);
            RemittanceDetailCraGrid.AllowPaging = true;
        }

        protected void LoadDetails()
        {
            Data = Common.ServiceWrapper.HumanResourcesClient.GetRemittanceDetailCraSourceDeductions(ExportId, RemitScheduleCode, RemitDate);
        }

        protected void RemittanceDetailCraGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RemittanceDetailCraGrid.DataSource = Data;
        }

    }
}