﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RemittanceImportPage.aspx.cs" Inherits="WorkLinks.Payroll.Remittance.RemittanceImportPage" %>
<%@ Register Src="RemittanceImportControl.ascx" TagName="RemittanceImportControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate>
        <uc1:RemittanceImportControl ID="RemittanceImportControl1" runat="server" />        
    </ContentTemplate>
</asp:Content>