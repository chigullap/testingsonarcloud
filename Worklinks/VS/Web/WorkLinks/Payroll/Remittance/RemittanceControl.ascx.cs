﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.Remittance
{
    public partial class RemittanceControl : WLP.Web.UI.WLPUserControl
    {
        protected bool EnableDetailsButton { get { return Common.Security.RoleForm.Remittance.ViewFlag; } } //if you can see the summary, you can see the details, so reuse
        protected bool EnableRemittanceReportButton { get { return Common.Security.RoleForm.Remittance.ViewFlag; } } //if you can see the summary, you can see the report, so reuse

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.Remittance.ViewFlag);

            WireEvents();

            if (!IsPostBack)
            {
                btnRefresh_Click(null, null);
                Panel1.DataBind();
            }
        }

        protected void WireEvents()
        {
            RemittanceGrid.NeedDataSource += new GridNeedDataSourceEventHandler(RemittanceGrid_NeedDataSource);
            //RemittanceGrid.PreRender += new EventHandler(RemittanceGrid_PreRender);
            RemittanceGrid.AllowPaging = true;
        }

        protected void RemittanceGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RemittanceGrid.DataSource = Data;
        }

        //void RemittanceGrid_PreRender(object sender, EventArgs e)
        //{
        //    work around to remove empty rows caused by the OnCommand Telerik bug
        //    if (Data == null)
        //        RemittanceGrid.AllowPaging = false;
        //    else
        //        RemittanceGrid.AllowPaging = true;
        //}
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            RemittanceCollection collection = new RemittanceCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetRemittance());
            Data = collection;

            RemittanceGrid.Rebind();
        }

        protected void DetailsToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableDetailsButton; }
        protected void RemittanceReportToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableRemittanceReportButton; }
    }
}