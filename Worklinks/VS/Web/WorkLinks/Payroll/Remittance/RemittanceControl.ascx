﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RemittanceControl.ascx.cs" Inherits="WorkLinks.Payroll.Remittance.RemittanceControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="SearchPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SearchPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="SearchPanel" runat="server">
    <table width="100%">
        <tr valign="top">
            <td>
                <div>
                    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnRefresh">
                        <div class="SearchCriteriaButtons">
                            <wasp:WLPButton ID="btnRefresh" Icon-PrimaryIconUrl="~/App_Themes/Default/search.gif" runat="server" Text="Refresh" ResourceName="btnRefresh" OnClick="btnRefresh_Click" />
                        </div>
                    </asp:Panel>
                </div>
            </td>
        </tr>
        <tr valign="top">
            <td>
                <div>
                    <wasp:WLPToolBar ID="RemittanceToolBar" runat="server" Width="100%" OnClientLoad="RemittanceToolBar_init" Style="z-index: 1900">
                        <items>
                            <wasp:WLPToolBarButton OnPreRender="DetailsToolBar_PreRender" Text="*Details" onclick="Open();" CommandName="details" ResourceName="Details"></wasp:WLPToolBarButton>
                            <wasp:WLPToolBarButton OnPreRender="RemittanceReportToolBar_PreRender" Text="*RemittanceReport" onclick="openRemittanceReport();" runat="server" CommandName="RemittanceReport" ResourceName="RemittanceReport"></wasp:WLPToolBarButton>
                        </items>
                    </wasp:WLPToolBar>
                    <wasp:WLPGrid
                        ID="RemittanceGrid"
                        runat="server"
                        AllowPaging="true"
                        PagerStyle-AlwaysVisible="true"
                        PageSize="50"
                        GridLines="None"
                        Height="600px"
                        AutoAssignModifyProperties="true">

                        <clientsettings allowcolumnsreorder="true" reordercolumnsonclient="true">
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                            <ClientEvents OnRowClick="OnRowClick" OnRowDblClick="OnRowDblClick" OnCommand="onCommand" />
                        </clientsettings>

                        <mastertableview
                            autogeneratecolumns="false"
                            clientdatakeynames="DetailType, RemitCode, ExportId, RemitDateString"
                            datakeynames="DetailType, RemitCode, ExportId, RemitDateString"
                            commanditemdisplay="Top"
                            groupsdefaultexpanded="false">

                            <GroupByExpressions>
	                            <telerik:GridGroupByExpression>
		                            <SelectFields>
			                            <telerik:GridGroupByField FieldName="RemittanceType" HeaderText="Remittance Type" />
		                            </SelectFields>
		                            <GroupByFields>
			                            <telerik:GridGroupByField FieldName="RemittanceType" SortOrder="Ascending" />
		                            </GroupByFields>
	                            </telerik:GridGroupByExpression>
                            </GroupByExpressions> 

                            <CommandItemTemplate></CommandItemTemplate>

                            <Columns>
                                <wasp:GridBoundControl DataField="RemittanceType" LabelText="*RemittanceType" UniqueName="RemittanceType" ResourceName="RemittanceType">
                                    <HeaderStyle Width="10%" />
                                </wasp:GridBoundControl>
                                <wasp:GridBoundControl DataField="Description" LabelText="*Description" UniqueName="Description" ResourceName="Description">
                                    <HeaderStyle Width="10%" />
                                </wasp:GridBoundControl>
                                <wasp:GridDateTimeControl DataField="EffectiveEntryDate" LabelText="*RemittanceDate" SortExpression="RemittanceDate" UniqueName="RemittanceDate" ResourceName="RemittanceDate">
                                </wasp:GridDateTimeControl>
                                <wasp:GridDateTimeControl DataField="RemittancePeriodStartDate" LabelText="*RemittancePeriodStartDate" SortExpression="RemittancePeriodStartDate" UniqueName="RemittancePeriodStartDate" ResourceName="RemittancePeriodStartDate"></wasp:GridDateTimeControl>
                                <wasp:GridDateTimeControl DataField="RemittancePeriodEndDate" LabelText="*RemittancePeriodEndDate" SortExpression="RemittancePeriodEndDate" UniqueName="RemittancePeriodEndDate" ResourceName="RemittancePeriodEndDate"></wasp:GridDateTimeControl>
                                <wasp:GridNumericControl DataField="RemittanceAmount" DataFormatString="{0:$###,##0.00}" LabelText="*RemittanceAmount" UniqueName="RemittanceAmount" ResourceName="RemittanceAmount"></wasp:GridNumericControl>
                                <telerik:GridTemplateColumn>
	                                <ItemTemplate></ItemTemplate>
	                            </telerik:GridTemplateColumn>
                                <wasp:GridBoundControl DataField="FileTransmissionDateString" LabelText="*FileTransmissionDateString" UniqueName="FileTransmissionDateString" ResourceName="FileTransmissionDateString"></wasp:GridBoundControl>
                                <wasp:GridBoundControl DataField="RbcSequenceNumber" LabelText="*FileId" UniqueName="RbcSequenceNumber" ResourceName="RbcSequenceNumber"></wasp:GridBoundControl>
                            </Columns>

                        </mastertableview>

                        <headercontextmenu enableautoscroll="true"></headercontextmenu>

                    </wasp:WLPGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>

<br />
<br />
<wasp:WLPLabel ID="PendingMessage" runat="server" Text="**messageHere**" AssociatedControlID="PendingMessage" ResourceName="PendingMessage"></wasp:WLPLabel>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="RemittanceWindow"
    Width="1300"
    Height="450"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>


<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="RemittanceWindowReport"
    Width="1300"
    Height="450"
    VisibleStatusbar="false"
    Behaviors="Close,Move,Resize"
    runat="server"
    DestroyOnClose="true"
    Modal="true"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">

        //variable section
        var detailType;
        var remitCode;
        var exportId;
        var remitDate;
        var toolbar = null;

        function RemittanceToolBar_init(sender) {
            initializeControls();
        }

        function initializeControls() {
            if ($find('<%= RemittanceToolBar.ClientID %>') != null)
                toolbar = $find('<%= RemittanceToolBar.ClientID %>');

            enableButtons(getSelectedRow() != null);

            detailType = null;
            remitCode = null;
            exportId = null;
            remitDate = null;
        }

        function getSelectedRow() {
            var remittanceGridGrid = $find('<%= RemittanceGrid.ClientID %>');
            if (remittanceGridGrid == null)
                return null;

            var MasterTable = remittanceGridGrid.get_masterTableView();
            if (MasterTable == null)
                return null;

            var selectedRow = MasterTable.get_selectedItems();
            if (selectedRow.length == 1)
                return selectedRow[0];
            else
                return null;
        }

        function enableButtons(enable) {
            var detailsButton = toolbar.findButtonByCommandName('details');
            var reportButton = toolbar.findButtonByCommandName('RemittanceReport');

            if (detailsButton != null)
                detailsButton.set_enabled(enable);
            if (reportButton != null)
                reportButton.set_enabled(enable);
        }

        function OnRowClick(sender, eventArgs) {
            detailType = eventArgs.getDataKeyValue('DetailType');
            remitCode = eventArgs.getDataKeyValue('RemitCode');
            exportId = eventArgs.getDataKeyValue('ExportId');
            remitDate = eventArgs.getDataKeyValue('RemitDateString');

            enableButtons(true);
        }

        function Open() {
            //if exportId is null, the file is pending, so the detail page will make use of other parameters
            if (detailType != null && remitCode != null) {
                //cra source deductions
                if (detailType == 'CRA_SS') {
                    openWindowWithManager('RemittanceWindow', String.format('RemittanceDetailCraSourceDeductions/{0}/{1}/{2}', remitCode, exportId, remitDate), false);
                    return false;
                }
                //wcb deductions
                else if (detailType == 'CRA_WCB' || detailType == 'CHQ_WCB' || detailType == 'CHQ_WCBE') {
                    openWindowWithManager('RemittanceWindow', String.format('RemittanceDetailWcbDeductions/{0}/{1}/{2}/{3}', detailType, remitCode, exportId, remitDate), false);
                    return false;
                }
                //health tax deductions
                else if (detailType == 'CHQ_HEALTH') {
                    openWindowWithManager('RemittanceWindow', String.format('RemittanceDetailHealthDeductions/{0}/{1}', remitCode, exportId), false);
                    return false;
                }
                //garnishment deductions
                else if (detailType == 'CRA_GARN') {
                    openWindowWithManager('RemittanceWindow', String.format('RemittanceDetailGarnishmentDeductions/{0}/{1}', detailType, exportId), false);
                    return false;
                }
                //rq source deductions
                else if (detailType == 'RQ_SS') {
                    openWindowWithManager('RemittanceWindow', String.format('RemittanceDetailRqSourceDeductions/{0}', exportId), false);
                    return false;
                }
            }
        }

        function openRemittanceReport() {

            if (detailType != null && remitCode != null) {

                var reportName = '';
                var reportType = 'PDF';

                if (detailType == 'CRA_SS')                                 //cra source deductions
                {
                    reportName = 'CraSourceDeductions';
                    reportType = "EXCEL";
                }
                else if (detailType == 'CRA_WCB')                           //cra wcb deductions
                    reportName = 'CraWcbRemitDetailsReport';
                else if (detailType == 'CHQ_WCB' || detailType == 'CHQ_WCBE')//cheque wcb deductions (actual and estimates)
                    reportName = 'ChequeWcbRemitDetailsReport';
                else if (detailType == 'CRA_GARN')                          //cra garnishments
                    reportName = 'CraGarnishmentDetailsReport';
                else if (detailType == 'RQ_SS')                             //rq garnishments
                    reportName = 'RqSourceDeductions';
                else if (detailType == 'CHQ_HEALTH')                        //cheque health tax
                    reportName = 'ChequeHealthTaxRemit';

                if (reportType == "PDF")
                    openWindowWithManager('RemittanceWindowReport', String.format('/Report/{0}/{1}/{2}/{3}/{4}/{5}', reportName, detailType, remitCode, exportId, remitDate, reportType), true);
                else
                    invokeDownload(reportName, reportType)//if excel format, do not open a child window.
            }
        }

        function OnRowDblClick(sender, eventArgs) {
            if (toolbar.findButtonByCommandName('details') != null)
                Open(sender);
        }

        function invokeDownload(reportName, outputType) {
            var frame = document.createElement("frame");
            frame.src = getUrl(String.format('/Report/{0}/{1}/{2}/{3}/{4}/{5}', reportName, detailType, remitCode, exportId, remitDate, outputType)), null;
            frame.style.display = "none";
            document.body.appendChild(frame);
        }

        //if user hits paging buttons
        function onCommand(sender, args) {
            if (args.get_commandName() == 'Page')
                initializeControls();
        }

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

    </script>
</telerik:RadScriptBlock>