﻿<%--<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RemittanceImportApprovalControl.ascx.cs" Inherits="WorkLinks.Payroll.Remittance.RemittanceImportApprovalControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPToolBar ID="RemittanceImportApprovalToolBar" runat="server" Width="100%" OnButtonClick="RemittanceImportApprovalToolBar_ButtonClick" OnClientButtonClicked="toolBarClick" Style="z-index: 1900">
    <Items>
        <wasp:WLPToolBarButton Text="*Approve" CommandName="approve" ResourceName="Approve" />
        <wasp:WLPToolBarButton Text="*Reject" CommandName="reject" ResourceName="Reject" />
    </Items>
</wasp:WLPToolBar>

<wasp:WLPGrid
    ID="RemittanceImportApprovalGrid"
    runat="server"
    AllowSorting="true"
    GridLines="None"
    ShowFooter="true"
    Height="362px"
    AutoAssignModifyProperties="true">

    <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="false" EnableAlternatingItems="false">
        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
        <Selecting AllowRowSelect="false" />
    </ClientSettings>

    <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="Top" AllowSorting="false">
        <CommandItemTemplate></CommandItemTemplate>
        <Columns>
            <wasp:GridBoundControl HeaderStyle-HorizontalAlign="Left" DataField="RemittanceDestination" LabelText="*RemittanceDestination" Aggregate="First" FooterAggregateFormatString="Total Remittance" SortExpression="RemittanceDestination" UniqueName="RemittanceDestination" ResourceName="RemittanceDestination">
                <HeaderStyle Width="250px" />
            </wasp:GridBoundControl>
            <wasp:GridBoundControl HeaderStyle-HorizontalAlign="Left" DataField="AccountNumber" LabelText="*AccountNumber" SortExpression="AccountNumber" UniqueName="AccountNumber" ResourceName="AccountNumber">
                <HeaderStyle Width="250px" />
            </wasp:GridBoundControl>
            <wasp:GridNumericControl HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" Aggregate="Sum" FooterAggregateFormatString="{0:$###,##0.00}" DataField="RemittanceAmount" LabelText="*RemittanceAmount" DataFormatString="{0:$###,##0.00}" SortExpression="RemittanceAmount" UniqueName="RemittanceAmount" ResourceName="RemittanceAmount">
                <HeaderStyle Width="200px" />
            </wasp:GridNumericControl>
        </Columns>
    </MasterTableView>

    <HeaderContextMenu EnableAutoScroll="true" />

</wasp:WLPGrid>

<div class="RadGrid RadGrid_Silk">
    <table class="rgMasterTable rgClipCells rgClipCells" style="width: 100%; table-layout: fixed; overflow: hidden; empty-cells: show;">
        <tr class="rgRow">
            <td style="width: 250px;"><wasp:WLPLabel ID="GrossPayrollLabel" runat="server" Text="*GrossPayroll" ResourceName="GrossPayroll" /></td>
            <td style="width: 250px;"><%= GrossPayroll %></td>
            <td style="width: 200px;"></td>
        </tr>
        <tr class="rgRow">
            <td style="width: 250px;"><wasp:WLPLabel ID="EmployeeCountLabel" runat="server" Text="*EmployeeCount" ResourceName="EmployeeCount" /></td>
            <td style="width: 250px;"><%= EmployeeCount %></td>
            <td style="width: 200px;"></td>
        </tr>
    </table>
</div>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function toolBarClick(sender, args) {
            var button = args.get_item();
            var commandName = button.get_commandName();

            processClick(commandName);
        }

        function processClick(commandName) {
            var arg = new Object;

            arg.closeWithChanges = true;
            getRadWindow().argument = arg;
        }

        function getRadWindow() {
            var popup = null;

            if (window.radWindow)
                popup = window.radWindow;
            else if (window.frameElement.radWindow)
                popup = window.frameElement.radWindow;

            return popup;
        }

        function closeAndReturn() {
            var popup = getRadWindow();
            setTimeout(function () { popup.close(); }, 0); //have to use "setTimeout" to get around an IE9 bug
        }
    </script>
</telerik:RadScriptBlock>--%>