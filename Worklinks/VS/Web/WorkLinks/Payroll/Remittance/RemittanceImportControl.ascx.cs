﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.Remittance
{
    public partial class RemittanceImportControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public bool IsViewMode { get { return RemittanceImportGrid.IsViewMode; } }
        public bool IsUpdate { get { return RemittanceImportGrid.IsEditMode; } }
        public bool IsInsert { get { return RemittanceImportGrid.IsInsertMode; } }
        public bool AddFlag { get { return Common.Security.RoleForm.RemittanceImport.AddFlag; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.RemittanceImport.UpdateFlag; } }
        public bool DeleteFlag { get { return Common.Security.RoleForm.RemittanceImport.DeleteFlag; } }
        protected bool EnableImportButton { get { return Common.Security.RoleForm.Import.ViewFlag; } }
        protected bool EnableApprovalButton { get { return Common.Security.RoleForm.RemittanceImportApproval.ViewFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.RemittanceImport.ViewFlag);

            WireEvents();

            if (!IsPostBack)
                LoadRemittanceImportInformation();

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            if (args != null && args.Equals(String.Format("refresh")))
            {
                long keyId = -1;

                //if a row was selected
                if (RemittanceImportGrid.SelectedItems.Count != 0)
                    keyId = Convert.ToInt64(((GridDataItem)RemittanceImportGrid.SelectedItems[0]).GetDataKeyValue(RemittanceImportGrid.MasterTableView.DataKeyNames[0]));

                LoadRemittanceImportInformation();
                RemittanceImportGrid.Rebind();

                if (keyId != -1)
                    RemittanceImportGrid.SelectRowByFirstDataKey(keyId); //re-select the selected row
            }
        }
        protected void WireEvents()
        {
            RemittanceImportGrid.NeedDataSource += new GridNeedDataSourceEventHandler(RemittanceImportGrid_NeedDataSource);
        }
        protected void LoadRemittanceImportInformation()
        {
            Data = RemittanceImportCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetRemittanceImport(null));
        }
        #endregion

        #region event handlers
        void RemittanceImportGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RemittanceImportGrid.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        #endregion

        #region security handlers
        protected void ImportToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableImportButton; }
        protected void ApprovalToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableApprovalButton; }
        #endregion
    }
}