﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayrollReportsMenu.ascx.cs" Inherits="WorkLinks.Payroll.PayrollReportsMenu" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadMenu ID="ReportRadMenu" ClickToOpen="true" runat="server" EnableRoundedCorners="true" OnClientItemClicked="reportItemClicked" Style="z-index: 1900">
    <Items>
        <wasp:WLPMenuItem runat="server" Text="*Reports" ResourceName="ReportsMenu">
            <Items>
                <wasp:WLPMenuItem runat="server" Text="*Standard" ResourceName="StandardMenu" OnPreRender="StandardMenuItem_PreRender">
                    <Items>

                         <wasp:WLPMenuItem runat="server" Text="*CustomerInvoice" ResourceName="CustomerInvoice" OnPreRender="CustomerInvoiceReportMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="CustomerInvoice_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableCustomerInvoiceReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*EftCustomerInvoiceJamaica" ResourceName="EftCustomerInvoiceJamaica" OnPreRender="EftCustomerInvoiceJamaicaReportMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="EftCustomerInvoiceJamaica_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableEftCustomerInvoiceJamaicaReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*PayRegister" ResourceName="PayRegister" OnPreRender="PayRegisterReportMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="PayRegister_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnablePayRegisterReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*RegisterSummary" ResourceName="RegisterSummary" OnPreRender="RegisterSummaryReportMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="RegisterSummary_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableRegisterSummaryReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*ChangesReport" ResourceName="ChangesReport" OnPreRender="ChangesReportMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="Changes_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableChangesReportMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="Changes_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableChangesReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*CompensationListReport" ResourceName="CompensationListReport" OnPreRender="CompensationListReportMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="CompensationList_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableCompensationListReportMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="CompensationList_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableCompensationListReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*CurrentVsPriorReport" ResourceName="CurrentVsPriorReport" OnPreRender="CurrentVsPriorReportReportMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="CurrentVsPrior_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableCurrentVsPriorReportMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="CurrentVsPrior_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableCurrentVsPriorReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*NetCurrentVsPriorReport" ResourceName="NetCurrentVsPriorReport" OnPreRender="CurrentVsPriorNetReportMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="CurrentVsPriorNet_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableCurrentVsPriorNetReportMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="CurrentVsPriorNet_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableCurrentVsPriorNetReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*CurrentVsPriorPeriodVariances" ResourceName="CurrentVsPriorPeriodVariances" OnPreRender="CurrentVsPriorPeriodVariances_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="CurrentVsPriorPeriodVariances_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableCurrentVsPriorPeriodVariancesMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="CurrentVsPriorPeriodVariances_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableCurrentVsPriorPeriodVariancesMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*GarnishmentReport" ResourceName="GarnishmentReport" OnPreRender="GarnishmentMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="Garnishment_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableGarnishmentReportMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="Garnishment_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableGarnishmentReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*GrossNetExceptionReport" ResourceName="GrossNetExceptionReport" OnPreRender="PayExceptionMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="PayException_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnablePayExceptionReportMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="PayException_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePayExceptionReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*GrossVsSalaryReport" ResourceName="GrossVsSalaryReport" OnPreRender="GrossVsSalaryMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="GrossVsSalary_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableGrossVsSalaryReportMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="GrossVsSalary_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableGrossVsSalaryReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*PD7AReport" ResourceName="PD7AReport" OnPreRender="PD7AMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="PD7AReport_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnablePD7AReportMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="PD7AReport_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePD7AReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*BarbadosDeductionSummary" ResourceName="BarbadosDeductionSummary" OnPreRender="BarbadosDeductionSummaryMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="BarbadosDeductionSummary_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableBarbadosDeductionSummaryMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="BarbadosDeductionSummary_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableBarbadosDeductionSummaryMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*StLuciaDeductionSummary" ResourceName="StLuciaDeductionSummary" OnPreRender="StLuciaDeductionSummaryMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="StLuciaDeductionSummary_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableStLuciaDeductionSummaryMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="StLuciaDeductionSummary_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableStLuciaDeductionSummaryMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*TrinidadDeductionSummary" ResourceName="TrinidadDeductionSummary" OnPreRender="TrinidadDeductionSummaryMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="TrinidadDeductionSummary_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableTrinidadDeductionSummaryMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="TrinidadDeductionSummary_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableTrinidadDeductionSummaryMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*JamaicaDeductionSummary" ResourceName="JamaicaDeductionSummary" OnPreRender="JamaicaDeductionSummaryMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="JamaicaDeductionSummary_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableJamaicaDeductionSummaryMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="JamaicaDeductionSummary_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableJamaicaDeductionSummaryMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*JamaicaGovMonthlyRemittance" ResourceName="JamaicaGovMonthlyRemittance" OnPreRender="JamaicaGovMonthlyRemittanceMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="JamaicaGovMonthlyRemittance_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableJamaicaGovMonthlyRemittanceMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="JamaicaGovMonthlyRemittance_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableJamaicaGovMonthlyRemittanceMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*YTDBonusCommishReport" ResourceName="YTDBonusCommishReport" OnPreRender="YtdBonusCommissionMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="YTDBonusCommish_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableYtdBonusCommissionReportMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="YTDBonusCommish_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableYtdBonusCommissionReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*WCB" ResourceName="WCB" OnPreRender="WCBMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="WCB_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableWCBReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*CSB" ResourceName="CSB" OnPreRender="CSBMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="CSB_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableCSBReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*PayDetails" ResourceName="PayDetails" OnPreRender="PayDetailsMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="PayrollDetailsReport_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePayDetailsMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*GLEmployeeDetails" ResourceName="GLEmployeeDetails" OnPreRender="GLEmployeeDetailsMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="GLEmployeeDetailsReport_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableGLDetailsMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*BenefitArrears" ResourceName="BenefitArrears" OnPreRender="BenefitArrearsMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="BenefitArrearsReport_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableBenefitArrearsMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*Vacation" ResourceName="VacationReport" OnPreRender="VacationReportMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="VacationReport_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableVacationReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*AccumulatedHours" ResourceName="AccumulatedHours" OnPreRender="AccumulatedHours_OnPreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="AccumulatedHours_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableAccumulatedHoursReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                    </Items>
                </wasp:WLPMenuItem>
                <wasp:WLPMenuItem runat="server" Text="*Custom" ResourceName="CustomMenu" OnPreRender="CustomMenuItem_PreRender">
                    <Items>
                        <wasp:WLPMenuItem runat="server" Text="*Pension" ResourceName="PensionReport" OnPreRender="PensionMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="Pension_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnablePensionReportMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="Pension_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePensionReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*EmployeeSalaryReport" ResourceName="EmployeeSalaryReport" OnPreRender="EmployeeSalaryReportMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="EmployeeSalaryReport_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableEmployeeSalaryReportMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="EmployeeSalaryReport_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableEmployeeSalaryReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*UnitedWayReport" ResourceName="UnitedWayReport" OnPreRender="UnitedWayReportMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="UnitedWayReport_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableUnitedWayReportMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="UnitedWayReport_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableUnitedWayReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*SerpYtdReport" ResourceName="SerpYtdReport" OnPreRender="EnableSerpYtdReportMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="SerpYtdReport_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableSerpYtdReportMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="SerpYtdReport_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableSerpYtdReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*PensionDemographicsReport" ResourceName="PensionDemographicsReport" OnPreRender="PensionDemographicsMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="PensionDemographicsReport_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePensionDemographicsMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*SrpReport" ResourceName="SrpReport" OnPreRender="SrpReportMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="SrpReport_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableSrpReportMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="SrpReport_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableSrpReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*GrossUpReport" ResourceName="GrossUpReport" OnPreRender="GrossUpReportMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="GrossUpReport_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableGrossUpReportMenuItem %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="GrossUpReport_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableGrossUpReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*A1235GLDetailsReport" ResourceName="A1235GLDetailsReport" OnPreRender="A1235GLDetailsReportMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="A1235GLDetailsReport_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableA1235GLDetailsReportMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*CurrentVsPriorTwoPeriodVariances" ResourceName="CurrentVsPriorTwoPeriodVariances" OnPreRender="CurrentVsPriorTwoPeriodVariances_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="CurrentVsPriorTwoPeriodVariances_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableCurrentVsPriorTwoPeriodVariances %>'></wasp:WLPMenuItem>
                                <wasp:WLPMenuItem Value="CurrentVsPriorTwoPeriodVariances_EXCEL" runat="server" Text="*Excel" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnableCurrentVsPriorTwoPeriodVariances %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                        <wasp:WLPMenuItem runat="server" Text="*MonthlyStatutoryRemittance" ResourceName="MonthlyStatutoryRemittance" OnPreRender="MonthlyStatutoryRemittance_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="MonthlyStatutoryRemittance_PDF" runat="server" Text="*PDF" ImageUrl="~/App_Themes/Default/pdficon_small.png" ResourceName="PdfMenuItem" Visible='<%# EnableMonthlyStatutoryRemittance %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                         <wasp:WLPMenuItem runat="server" Text="*PayrollDetailsOrgUnit" ResourceName="PayrollDetailsOrgUnit" OnPreRender="PayrollDetailsOrgUnitMenuItem_PreRender">
                            <Items>
                                <wasp:WLPMenuItem Value="PayrollDetailsOrgUnit_EXCEL" runat="server" Text="*EXCEL" ImageUrl="~/App_Themes/Default/Microsoft-Office-Excel-icon.png" ResourceName="ExcelMenuItem" Visible='<%# EnablePayrollDetailsOrgUnitMenuItem %>'></wasp:WLPMenuItem>
                            </Items>
                        </wasp:WLPMenuItem>
                    </Items>
                </wasp:WLPMenuItem> 
            </Items> 
        </wasp:WLPMenuItem>
    </Items>
</telerik:RadMenu>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function reportItemClicked(sender, eventArgs) {
            menuValue = eventArgs.get_item().get_value();
            if (menuValue != null && payrollProcessId != null) {
                var splitterIndex = menuValue.lastIndexOf("_");
                var reportName = menuValue.substr(0, splitterIndex);
                var outputType = menuValue.substr(splitterIndex + 1, 99);

                //if excel format, do not open a child window.
                if (outputType == 'EXCEL')
                    invokeDownload(reportName, outputType);
                else if (reportName == 'MonthlyStatutoryRemittance') {
                    openExternalLinkWithManager('<%= MonthlyStatutoryRemittanceURL %>');
                }
                else
                    openWindowWithManager('RegisterReportWindow', String.format('/Report/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}', reportName, payrollProcessId, 'null', outputType, 'x', 'x', 'x', 'x'), true);
            }
        }

         function invokeDownload(reportName, outputType) {
            var frame = document.createElement("frame");
            frame.src = getUrl(String.format('/Report/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}', reportName, payrollProcessId, 'null', outputType, 'x', 'x', 'x', 'x')), null;
            frame.style.display = "none";
            document.body.appendChild(frame);
        }
    </script>
</telerik:RadScriptBlock>