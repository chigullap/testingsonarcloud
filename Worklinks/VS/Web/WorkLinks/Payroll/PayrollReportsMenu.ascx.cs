﻿using System;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll
{
    public partial class PayrollReportsMenu : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected bool EnableStandardMenuItem { get { return Common.Security.RoleForm.PayRegisterReportStandardMenu; } }
        protected bool EnableCustomMenuItem { get { return Common.Security.RoleForm.PayRegisterReportCustomMenu; } }
        protected bool EnablePayRegisterReportMenuItem { get { return Common.Security.RoleForm.PayrollPayRegisterDetails.ViewFlag; } }
        protected bool EnableRegisterSummaryReportMenuItem { get { return Common.Security.RoleForm.PayrollPayRegisterSummary.ViewFlag; } }
        protected bool EnableChangesReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterChangesReport.ViewFlag; } }
        protected bool EnableCompensationListReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterCompensationList.ViewFlag; } }
        protected bool EnableCurrentVsPriorReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterCurrentVsPrior.ViewFlag; } }
        protected bool EnableCurrentVsPriorNetReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterCurrentVsPriorNet.ViewFlag; } }
        protected bool EnableCurrentVsPriorPeriodVariancesMenuItem { get { return Common.Security.RoleForm.PayrollRegisterCurrentVsPriorPeriodVariances.ViewFlag; } }
        protected bool EnableGarnishmentReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterGarnishment.ViewFlag; } }
        protected bool EnablePayExceptionReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterPayException.ViewFlag; } }
        protected bool EnableGrossVsSalaryReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterGrossVsSalary.ViewFlag; } }
        protected bool EnablePD7AReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterPD7A.ViewFlag; } }
        protected bool EnableBarbadosDeductionSummaryMenuItem { get { return Common.Security.RoleForm.BarbadosDeductionSummary.ViewFlag; } }
        protected bool EnableStLuciaDeductionSummaryMenuItem { get { return Common.Security.RoleForm.StLuciaDeductionSummary.ViewFlag; } }
        protected bool EnableTrinidadDeductionSummaryMenuItem { get { return Common.Security.RoleForm.TrinidadDeductionSummary.ViewFlag; } }
        protected bool EnableJamaicaDeductionSummaryMenuItem { get { return Common.Security.RoleForm.JamaicaDeductionSummary.ViewFlag; } }
        protected bool EnableJamaicaGovMonthlyRemittanceMenuItem { get { return Common.Security.RoleForm.JamaicaGovernmentMonthlyRemittance.ViewFlag; } }
        protected bool EnableYtdBonusCommissionReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterYtdBonusCommission.ViewFlag; } }
        protected bool EnablePensionReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterPensionReport.ViewFlag; } }
        protected bool EnableWCBReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterWCB.ViewFlag; } }
        protected bool EnablePayDetailsMenuItem { get { return Common.Security.RoleForm.PayrollRegisterPayDetails.ViewFlag; } }
        protected bool EnableGLDetailsMenuItem { get { return Common.Security.RoleForm.PayrollRegisterGLDetails.ViewFlag; } }
        protected bool EnableBenefitArrearsMenuItem { get { return Common.Security.RoleForm.PayrollRegisterBenefitArrears.ViewFlag; } }
        protected bool EnableCSBReportMenuItem { get { return Common.Security.RoleForm.PayrollRegisterCSB.ViewFlag; } }
        protected bool EnableEmployeeSalaryReportMenuItem { get { return Common.Security.RoleForm.EmployeeSalaryReport.ViewFlag; } }
        protected bool EnableUnitedWayReportMenuItem { get { return Common.Security.RoleForm.UnitedWayReport.ViewFlag; } }
        protected bool EnableSerpYtdReportMenuItem { get { return Common.Security.RoleForm.SerpYtdReport.ViewFlag; } } 
        protected bool EnablePensionDemographicsMenuItem { get { return Common.Security.RoleForm.PensionDemographicsReport.ViewFlag; } }
        protected bool EnableSrpReportMenuItem { get { return Common.Security.RoleForm.SrpReport.ViewFlag; } }
        protected bool EnableGrossUpReportMenuItem { get { return Common.Security.RoleForm.GrossUpReport.ViewFlag; } }
        protected bool EnableVacationReportMenuItem { get { return Common.Security.RoleForm.VacationReport.ViewFlag; } }
        protected bool EnableCustomerInvoiceReportMenuItem { get { return Common.Security.RoleForm.InvoiceCanada.ViewFlag; } }
        protected bool EnableEftCustomerInvoiceJamaicaReportMenuItem { get { return Common.Security.RoleForm.InvoiceJamaica.ViewFlag; } }
        protected bool EnableA1235GLDetailsReportMenuItem { get { return Common.Security.RoleForm.A1235GLDetailsReport.ViewFlag; } }
        protected bool EnableCurrentVsPriorTwoPeriodVariances { get { return Common.Security.RoleForm.CurrentVsPriorTwoPeriodVariances.ViewFlag; } }
        protected bool EnableMonthlyStatutoryRemittance { get { return Common.Security.RoleForm.MonthlyStatutoryRemittance.ViewFlag; } }
        public String MonthlyStatutoryRemittanceURL { get { return Common.CodeHelper.GetCodeDescription("EN", CodeTableType.System, "MSR_URL"); } }
        protected bool EnableAccumulatedHoursReportMenuItem => Common.Security.RoleForm.AccumulatedHoursReportMenuItem.ViewFlag;
        protected bool EnablePayrollDetailsOrgUnitMenuItem { get { return Common.Security.RoleForm.PayrollDetailsOrgUnit.ViewFlag; } }

        #endregion

        #region event handlers
        protected void StandardMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableStandardMenuItem; }
        protected void CustomMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableCustomMenuItem; }
        protected void PayRegisterReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePayRegisterReportMenuItem; }
        protected void RegisterSummaryReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableRegisterSummaryReportMenuItem; }
        protected void ChangesReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableChangesReportMenuItem; }
        protected void CompensationListReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableCompensationListReportMenuItem; }
        protected void CurrentVsPriorReportReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableCurrentVsPriorReportMenuItem; }
        protected void CurrentVsPriorNetReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableCurrentVsPriorNetReportMenuItem; }
        protected void CurrentVsPriorPeriodVariances_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableCurrentVsPriorPeriodVariancesMenuItem; }
        protected void GarnishmentMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableGarnishmentReportMenuItem; }
        protected void PayExceptionMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePayExceptionReportMenuItem; }
        protected void GrossVsSalaryMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableGrossVsSalaryReportMenuItem; }
        protected void PD7AMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePD7AReportMenuItem; }
        protected void BarbadosDeductionSummaryMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableBarbadosDeductionSummaryMenuItem; }
        protected void StLuciaDeductionSummaryMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableStLuciaDeductionSummaryMenuItem; }
        protected void TrinidadDeductionSummaryMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableTrinidadDeductionSummaryMenuItem; }
        protected void JamaicaDeductionSummaryMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableJamaicaDeductionSummaryMenuItem; }
        protected void JamaicaGovMonthlyRemittanceMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableJamaicaGovMonthlyRemittanceMenuItem; }
        protected void YtdBonusCommissionMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableYtdBonusCommissionReportMenuItem; }
        protected void PensionMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePensionReportMenuItem; }
        protected void WCBMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableWCBReportMenuItem; }
        protected void CSBMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableCSBReportMenuItem; }
        protected void PayDetailsMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePayDetailsMenuItem; }
        protected void GLEmployeeDetailsMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableGLDetailsMenuItem; }
        protected void BenefitArrearsMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableBenefitArrearsMenuItem; }
        protected void EmployeeSalaryReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableEmployeeSalaryReportMenuItem; }
        protected void UnitedWayReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableUnitedWayReportMenuItem; }
        protected void EnableSerpYtdReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableSerpYtdReportMenuItem; }
        protected void PensionDemographicsMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePensionDemographicsMenuItem; }
        protected void SrpReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableSrpReportMenuItem; }
        protected void GrossUpReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableGrossUpReportMenuItem; }
        protected void VacationReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableVacationReportMenuItem; }
        protected void A1235GLDetailsReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableA1235GLDetailsReportMenuItem; }
        protected void CurrentVsPriorTwoPeriodVariances_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableCurrentVsPriorTwoPeriodVariances; }
        protected void MonthlyStatutoryRemittance_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableMonthlyStatutoryRemittance; }
        protected void CustomerInvoiceReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableCustomerInvoiceReportMenuItem; }
        protected void EftCustomerInvoiceJamaicaReportMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableEftCustomerInvoiceJamaicaReportMenuItem; }
        protected void AccumulatedHours_OnPreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnableAccumulatedHoursReportMenuItem; }
        protected void PayrollDetailsOrgUnitMenuItem_PreRender(object sender, EventArgs e) { ((WLPMenuItem)sender).Visible = ((WLPMenuItem)sender).Visible && EnablePayrollDetailsOrgUnitMenuItem; }


        #endregion

        //protected void AccumulatedHours_OnPreRender(object sender, EventArgs e)
        //{
        //    throw new NotImplementedException();
        //}
    }
}