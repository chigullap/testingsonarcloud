﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StatutoryDeductionJamaicaViewControl.ascx.cs" Inherits="WorkLinks.Payroll.StatDeduction.StatutoryDeductionJamaicaViewControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPFormView
    ID="StatutoryDeductionJamaicaView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key"
    OnNeedDataSource="StatutoryDeductionJamaicaView_NeedDataSource"
    OnInserting="StatutoryDeductionJamaicaView_Inserting"
    OnUpdating="StatutoryDeductionJamaicaView_Updating"
    OnDataBound="StatutoryDeductionJamaicaView_DataBound">

    <ItemTemplate>
        <wasp:WLPToolBar ID="FormViewStatutoryDeductionJamaicaToolBar" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="StatutoryDeductionDetailToolbar_ButtonClick">
            <Items>
                <wasp:WLPToolBarButton Text="**Edit**" ImageUrl="~/App_Themes/Default/Edit.gif" Visible='<%# IsViewMode && UpdateFlag %>' CommandName="edit" ResourceName="Edit" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ActiveFlag" runat="server" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="PayNationalInsuranceSchemeFlag" runat="server" ResourceName="PayNationalInsuranceSchemeFlag" Value='<%# Bind("PayNationalInsuranceSchemeFlag") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="StatutoryDeductionTypeCode" runat="server" Type="StatutoryDeductionTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="StatutoryDeductionTypeCode" Value='<%# Bind("StatutoryDeductionTypeCode") %>' ReadOnly="true" Mandatory="true" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="PayNationalHealthTaxFlag" runat="server" ResourceName="PayNationalHealthTaxFlag" Value='<%# Bind("PayNationalHealthTaxFlag") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="BusinessNumberId" runat="server" Type="BusinessNumberId" OnDataBinding="BusinessNumberId_NeedDataSource" ResourceName="BusinessNumberId" Value='<%# Bind("BusinessNumberId") %>' ReadOnly="true" Mandatory="true" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="PayEducationTaxFlag" runat="server" ResourceName="PayEducationTaxFlag" Value='<%# Bind("PayEducationTaxFlag") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="IsWeek1Month1EmployeeForCurrentYearFlag" runat="server" ResourceName="IsWeek1Month1EmployeeForCurrentYearFlag" Value='<%# Bind("IsWeek1Month1EmployeeForCurrentYearFlag") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="FederalPayTaxFlag" runat="server" ResourceName="FederalPayTaxFlag" Value='<%# Bind("FederalPayTaxFlag") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:WLPLabel ID="lblPriorEmploymentTaxInfo" runat="server" Text="**Prior Employment Tax Info from P45**" Font-Bold="true" ResourceName="lblPriorEmploymentTaxInfo" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="WeekNumber" runat="server" DecimalDigits="0" ResourceName="WeekNumber" Value='<%# Bind("WeekNumber") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="MonthNumber" runat="server" DecimalDigits="0" ResourceName="MonthNumber" Value='<%# Bind("MonthNumber") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="TotalGrossPayToDate" runat="server" ResourceName="TotalGrossPayToDate" Value='<%# Bind("TotalGrossPayToDate") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="Prerequisites" runat="server" ResourceName="Prerequisites" Value='<%# Bind("Prerequisites") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:WLPLabel ID="lblDeductions" runat="server" Text="**Deductions**" Font-Bold="true" ResourceName="lblDeductions" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="NiToDate" runat="server" ResourceName="NiToDate" Value='<%# Bind("NiToDate") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="GrossPayLessDeductions" runat="server" ResourceName="GrossPayLessDeductions" Value='<%# Bind("GrossPayLessDeductions") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="Superannuation" runat="server" ResourceName="Superannuation" Value='<%# Bind("Superannuation") %>' ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="ApprovedExp" runat="server" ResourceName="ApprovedExp" Value='<%# Bind("ApprovedExp") %>' ReadOnly="true" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="TotalTaxToDateDeterminedByTaxTable" runat="server" ResourceName="TotalTaxToDateDeterminedByTaxTable" Value='<%# Bind("TotalTaxToDateDeterminedByTaxTable") %>' ReadOnly="true" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar Visible='<%# !EnableWizardFunctionalityFlag %>' ID="FormViewStatutoryDeductionJamaicaToolBar" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="StatutoryDeductionDetailToolbar_ButtonClick">
            <Items>
                <wasp:WLPToolBarButton Text="**Insert**" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert" />
                <wasp:WLPToolBarButton Text="**Update**" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update" />
                <wasp:WLPToolBarButton Text="**Cancel**" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" CausesValidation="false" ResourceName="Cancel" />
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ActiveFlag" runat="server" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>' ReadOnly='<%# Eval("ActiveFlag")??false %>' Visible='<%# !EnableWizardFunctionalityFlag %>' TabIndex="010" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="PayNationalInsuranceSchemeFlag" runat="server" ResourceName="PayNationalInsuranceSchemeFlag" Value='<%# Bind("PayNationalInsuranceSchemeFlag")%>' TabIndex="020" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="StatutoryDeductionTypeCode" runat="server" Type="StatutoryDeductionTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="StatutoryDeductionTypeCode" Value='<%# Bind("StatutoryDeductionTypeCode") %>' ReadOnly="true" Mandatory="true" TabIndex="030" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="PayNationalHealthTaxFlag" runat="server" ResourceName="PayNationalHealthTaxFlag" Value='<%# Bind("PayNationalHealthTaxFlag")%>' TabIndex="040" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="BusinessNumberId" runat="server" Type="BusinessNumberId" OnDataBinding="BusinessNumberId_NeedDataSource" ResourceName="BusinessNumberId" Value='<%# Bind("BusinessNumberId") %>' ReadOnly='<%# IsEditMode %>' Mandatory="true" TabIndex="050" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="PayEducationTaxFlag" runat="server" ResourceName="PayEducationTaxFlag" Value='<%# Bind("PayEducationTaxFlag") %>' TabIndex="060" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="IsWeek1Month1EmployeeForCurrentYearFlag" runat="server" OnCheckedChanged="IsWeek1Month1EmployeeForCurrentYearFlag_CheckedChanged" AutoPostBack="true" ResourceName="IsWeek1Month1EmployeeForCurrentYearFlag" Value='<%# Bind("IsWeek1Month1EmployeeForCurrentYearFlag") %>' TabIndex="070" />
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="FederalPayTaxFlag" runat="server" ResourceName="FederalPayTaxFlag" Value='<%# Bind("FederalPayTaxFlag") %>' TabIndex="080" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:WLPLabel ID="lblPriorEmploymentTaxInfo" runat="server" Text="**Prior Employment Tax Info from P45**" Font-Bold="true" ResourceName="lblPriorEmploymentTaxInfo" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="WeekNumber" runat="server" MinValue="0" MaxValue="53" DecimalDigits="0" ResourceName="WeekNumber" Value='<%# Bind("WeekNumber") %>' TabIndex="090" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="MonthNumber" runat="server" MinValue="0" MaxValue="12" DecimalDigits="0" ResourceName="MonthNumber" Value='<%# Bind("MonthNumber") %>' TabIndex="100" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="TotalGrossPayToDate" runat="server" ResourceName="TotalGrossPayToDate" Value='<%# Bind("TotalGrossPayToDate") %>' TabIndex="110" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="Prerequisites" runat="server" ResourceName="Prerequisites" Value='<%# Bind("Prerequisites") %>' TabIndex="120" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:WLPLabel ID="lblDeductions" runat="server" Text="**Deductions**" Font-Bold="true" ResourceName="lblDeductions" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="NiToDate" runat="server" ResourceName="NiToDate" Value='<%# Bind("NiToDate") %>' TabIndex="130" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="GrossPayLessDeductions" runat="server" ResourceName="GrossPayLessDeductions" Value='<%# Bind("GrossPayLessDeductions") %>' TabIndex="140" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="Superannuation" runat="server" ResourceName="Superannuation" Value='<%# Bind("Superannuation") %>' TabIndex="150" />
                    </td>
                </tr>
                    <td>
                        <wasp:NumericControl ID="ApprovedExp" runat="server" ResourceName="ApprovedExp" Value='<%# Bind("ApprovedExp") %>' TabIndex="160" />
                    </td>
                    <td>
                        <wasp:NumericControl ID="TotalTaxToDateDeterminedByTaxTable" runat="server" ResourceName="TotalTaxToDateDeterminedByTaxTable" Value='<%# Bind("TotalTaxToDateDeterminedByTaxTable") %>' TabIndex="170" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>
</wasp:WLPFormView>