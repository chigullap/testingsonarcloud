﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StatutoryDeductionTaxOverrideViewControl.ascx.cs" Inherits="WorkLinks.Payroll.StatDeduction.StatutoryDeductionTaxOverrideViewControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPFormView
    ID="StatutoryDeductionTaxOverrideView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key"
    OnNeedDataSource="StatutoryDeductionTaxOverrideView_NeedDataSource"
    OnDataBound="StatutoryDeductionTaxOverrideView_DataBound"
    OnInserting="StatutoryDeductionTaxOverrideView_Inserting"
    OnUpdating="StatutoryDeductionTaxOverrideView_Updating">

    <ItemTemplate>
        <wasp:WLPToolBar ID="FormViewStatutoryDeductionTaxOverrideToolBar" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="StatutoryDeductionDetailToolbar_ButtonClick">
            <Items>
                <wasp:WLPToolBarButton Text="**Edit**" ImageUrl="~/App_Themes/Default/Edit.gif" Visible='<%# IsViewMode && UpdateFlag %>' CommandName="edit" ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ActiveFlag" runat="server" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>' ReadOnly="true"></wasp:CheckBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="StatutoryDeductionTypeCode" runat="server" Type="StatutoryDeductionTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="StatutoryDeductionTypeCode" Value='<%# Bind("StatutoryDeductionTypeCode") %>' ReadOnly="true" Mandatory="true"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="ProvinceStateCode" runat="server" Type="ProvinceStateCode" OnDataBinding="ProvinceStateCode_NeedDataSource" ResourceName="ProvinceStateCode" Value='<%# Bind("ProvinceStateCode") %>' ReadOnly="true" Mandatory="true"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="BusinessNumberId" runat="server" Type="BusinessNumberId" OnDataBinding="BusinessNumberId_NeedDataSource" ResourceName="BusinessNumberId" Value='<%# Bind("BusinessNumberId") %>' ReadOnly="true" Mandatory="true"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:NumericControl ID="OverridePercentage" runat="server" ResourceName="OverridePercentage" Value='<%# Bind("OverridePercentage") %>' ReadOnly="true" Mandatory="true"></wasp:NumericControl>
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar Visible='<%# !EnableWizardFunctionalityFlag %>' ID="FormViewStatutoryDeductionTaxOverrideToolBar" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="StatutoryDeductionDetailToolbar_ButtonClick">
            <Items>
                <wasp:WLPToolBarButton Text="**Insert**" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="**Update**" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="**Cancel**" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ActiveFlag" runat="server" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>' ReadOnly='<%# Eval("ActiveFlag")??false %>' Visible='<%# !EnableWizardFunctionalityFlag %>' TabIndex="010"></wasp:CheckBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="StatutoryDeductionTypeCode" runat="server" Type="StatutoryDeductionTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="StatutoryDeductionTypeCode" Value='<%# Bind("StatutoryDeductionTypeCode") %>' ReadOnly="true" Mandatory="true" TabIndex="050"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="ProvinceStateCode" runat="server" Type="ProvinceStateCode" OnDataBinding="ProvinceStateCode_NeedDataSource" ResourceName="ProvinceStateCode" Value='<%# Bind("ProvinceStateCode") %>' ReadOnly="true" Mandatory="true" TabIndex="060"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="BusinessNumberId" runat="server" Type="BusinessNumberId" OnDataBinding="BusinessNumberId_NeedDataSource" ResourceName="BusinessNumberId" Value='<%# Bind("BusinessNumberId") %>' ReadOnly='<%# IsEditMode %>' Mandatory="true" TabIndex="030"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:NumericControl ID="OverridePercentage" runat="server" ResourceName="OverridePercentage" Value='<%# Bind("OverridePercentage") %>' ReadOnly="false" Mandatory="true" TabIndex="040"></wasp:NumericControl>
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>

</wasp:WLPFormView>