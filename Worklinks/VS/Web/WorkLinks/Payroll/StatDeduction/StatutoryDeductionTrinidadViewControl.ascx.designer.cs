﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace WorkLinks.Payroll.StatDeduction {
    
    
    public partial class StatutoryDeductionTrinidadViewControl {
        
        /// <summary>
        /// StatutoryDeductionTrinidadView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPFormView StatutoryDeductionTrinidadView;
        
        /// <summary>
        /// FormViewStatutoryDeductionTrinidadToolBar control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.WLPToolBar FormViewStatutoryDeductionTrinidadToolBar;
        
        /// <summary>
        /// ActiveFlag control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.CheckBoxControl ActiveFlag;
        
        /// <summary>
        /// FederalPayTaxFlag control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.CheckBoxControl FederalPayTaxFlag;
        
        /// <summary>
        /// StatutoryDeductionTypeCode control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.ComboBoxControl StatutoryDeductionTypeCode;
        
        /// <summary>
        /// PayNationalInsuranceBoardFlag control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.CheckBoxControl PayNationalInsuranceBoardFlag;
        
        /// <summary>
        /// BusinessNumberId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.ComboBoxControl BusinessNumberId;
        
        /// <summary>
        /// TD1Amount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WLP.Web.UI.Controls.NumericControl TD1Amount;
    }
}
