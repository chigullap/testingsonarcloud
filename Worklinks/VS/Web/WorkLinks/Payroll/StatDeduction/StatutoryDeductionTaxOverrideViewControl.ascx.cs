﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.StatDeduction
{
    public partial class StatutoryDeductionTaxOverrideViewControl : Wizard.WizardUserControl
    {
        #region delegates
        public event StatutoryDeductionDetailToolbarButtonClickEventHandler StatutoryDeductionDetailToolbarButtonClick;
        #endregion

        #region fields
        private bool _updateExterallyControlled = false;
        #endregion

        #region properties
        private StatutoryDeduction StatutoryDeduction
        {
            get
            {
                if (DataItemCollection.Count < 1)
                    ((StatutoryDeductionCollection)DataItemCollection).Add(new StatutoryDeduction() { EmployeeId = EmployeeId, ActiveFlag = true });

                return (StatutoryDeduction)DataItemCollection[0];
            }
        }
        public StatutoryDeductionCollection StatutoryDeductionCollection
        {
            get
            {
                if (!IsDataExternallyLoaded)
                    return (StatutoryDeductionCollection)DataItemCollection;
                else
                {
                    StatutoryDeductionCollection collection = new StatutoryDeductionCollection();
                    collection.Add(StatutoryDeduction);
                    return collection;
                }
            }
            set
            {
                if (!IsDataExternallyLoaded)
                    DataItemCollection = value;
            }
        }
        public bool IsEditMode
        {
            get
            {
                if (EnableWizardFunctionalityFlag)
                    return false; //return false to force control to not be in read only mode 
                else
                    return StatutoryDeductionTaxOverrideView.CurrentMode.Equals(FormViewMode.Edit);
            }
        }
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeStatutoryDeductionTaxOverride.UpdateFlag; } }
        public bool IsInsertMode { get { return StatutoryDeductionTaxOverrideView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsViewMode { get { return StatutoryDeductionTaxOverrideView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        #endregion

        #region main
        private void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (IsDataExternallyLoaded)
                    StatutoryDeductionTaxOverrideView.DataBind();
            }
        }
        public void ChangeModeInsert()
        {
            StatutoryDeductionTaxOverrideView.ChangeMode(FormViewMode.Insert);
            StatutoryDeductionTaxOverrideView.DataBind();
        }
        #endregion

        #region event handlers
        protected void StatutoryDeductionTaxOverrideView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            if (!IsDataExternallyLoaded)
                OnNeedDataSource(e);

            StatutoryDeductionTaxOverrideView.DataSource = StatutoryDeductionCollection;

            if (!IsViewMode && !EnableWizardFunctionalityFlag)
            {
                if (IsInsertMode)
                {
                    CheckBoxControl activeFlag = (CheckBoxControl)StatutoryDeductionTaxOverrideView.FindControl("ActiveFlag");
                    if (activeFlag != null)
                        activeFlag.Focus();
                }
            }
        }
        protected void StatutoryDeductionTaxOverrideView_DataBound(object sender, EventArgs e)
        {
            ComboBoxControl statutoryDeductionTypeCode = (ComboBoxControl)StatutoryDeductionTaxOverrideView.FindControl("StatutoryDeductionTypeCode");
            if (statutoryDeductionTypeCode != null)
                statutoryDeductionTypeCode.Value = "TAX";

            ComboBoxControl provinceStateCode = (ComboBoxControl)StatutoryDeductionTaxOverrideView.FindControl("ProvinceStateCode");
            if (provinceStateCode != null)
                provinceStateCode.Value = "ZZ";

            if (IsInsertMode)
            {
                CheckBoxControl activeFlag = (CheckBoxControl)StatutoryDeductionTaxOverrideView.FindControl("ActiveFlag");
                if (activeFlag != null)
                    activeFlag.Checked = true;
            }
        }
        protected void StatutoryDeductionTaxOverrideView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            StatutoryDeduction statutoryDeduction = (StatutoryDeduction)e.DataItem;

            if (_updateExterallyControlled)
            {
                //wizard - ActiveFlag will always be true as we are only adding a single row 
                statutoryDeduction.ActiveFlag = true;
                statutoryDeduction.CopyTo(StatutoryDeduction);
                e.Cancel = true;
            }
            else
                OnUpdating(e);

            StatutoryDeductionTaxOverrideView.DataBind();
        }
        protected void StatutoryDeductionTaxOverrideView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            OnInserting(e);
        }
        protected void ProvinceStateCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateProvince((ICodeControl)sender);
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void BusinessNumberId_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateBusinessNumber((ICodeControl)sender);
        }
        protected virtual void OnStatutoryDeductionDetailToolbarButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (StatutoryDeductionDetailToolbarButtonClick != null)
                StatutoryDeductionDetailToolbarButtonClick(sender, e);
        }
        protected void StatutoryDeductionDetailToolbar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            OnStatutoryDeductionDetailToolbarButtonClick(sender, e);
        }
        #endregion

        #region override
        public override void Update(bool updateExterallyControlled)
        {
            _updateExterallyControlled = updateExterallyControlled;
            StatutoryDeductionTaxOverrideView.UpdateItem(true);
        }
        public override void AddNewDataItem()
        {
            if (DataItemCollection == null)
                DataItemCollection = new StatutoryDeductionCollection();
        }
        public override void ChangeModeEdit()
        {
            if (!StatutoryDeductionTaxOverrideView.CurrentMode.Equals(FormViewMode.Edit))
                StatutoryDeductionTaxOverrideView.ChangeMode(FormViewMode.Edit);
        }
        #endregion
    }
}