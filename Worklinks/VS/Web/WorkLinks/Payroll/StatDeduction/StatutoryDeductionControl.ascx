﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StatutoryDeductionControl.ascx.cs" Inherits="WorkLinks.Payroll.StatDeduction.StatutoryDeductionControl" %>
<%@ Register assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register src="~/Payroll/StatDeduction/StatutoryDeductionViewControl.ascx" TagName="StatutoryDeductionViewControl" TagPrefix="uc1" %>
<%@ Register src="~/Payroll/StatDeduction/StatutoryDeductionTaxOverrideViewControl.ascx" TagName="TaxOverrideViewControl" TagPrefix="uc2" %>
<%@ Register src="~/Payroll/StatDeduction/StatutoryDeductionStLuciaViewControl.ascx" TagName="StLuciaViewControl" TagPrefix="uc3" %>
<%@ Register src="~/Payroll/StatDeduction/StatutoryDeductionBarbadosViewControl.ascx" TagName="BarbadosViewControl" TagPrefix="uc4" %>
<%@ Register src="~/Payroll/StatDeduction/StatutoryDeductionTrinidadViewControl.ascx" TagName="TrinidadViewControl" TagPrefix="uc5" %>
<%@ Register src="~/Payroll/StatDeduction/StatutoryDeductionJamaicaViewControl.ascx" TagName="JamaicaViewControl" TagPrefix="uc6" %>

<wasp:WLPAjaxManagerProxy ID="WLPAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="StatDeductionPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="StatDeductionPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</wasp:WLPAjaxManagerProxy>

<asp:Panel ID="StatDeductionPanel" runat="server">
    <wasp:WLPGrid
        ID="StatutoryDeductionGrid"
        runat="server"
        GridLines="None"
        Width="100%"
        Height="194px"
        AutoAssignModifyProperties="true"
        AutoGenerateColumns="false"
        OnItemCommand="StatutoryDeductionGrid_ItemCommand"
        OnNeedDataSource="StatutoryDeductionGrid_NeedDataSource"
        OnSelectedIndexChanged="StatutoryDeductionGrid_SelectedIndexChanged"
        OnDataBound="StatutoryDeductionGrid_DataBound">

        <ClientSettings EnablePostBackOnRowClick="true" AllowKeyboardNavigation="true">
            <Selecting AllowRowSelect="true" />
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView DataKeyNames="Key" CommandItemDisplay="Top" AllowMultiColumnSorting="true">
            <SortExpressions>
                <telerik:GridSortExpression FieldName="ActiveFlag" SortOrder="Descending" />
                <telerik:GridSortExpression FieldName="StatutoryDeductionTypeCode" SortOrder="Ascending" />
                <telerik:GridSortExpression FieldName="ProvinceStateCode" SortOrder="Ascending" />
                <telerik:GridSortExpression FieldName="BusinessNumberId" SortOrder="Ascending" />
            </SortExpressions>

            <CommandItemTemplate>
                <wasp:WLPToolBar ID="StatutoryDeductionToolBar" runat="server" AutoPostBack="true" Width="100%" OnButtonClick="StatutoryDeductionToolBar_ButtonClick" Enabled="<%# IsViewMode && AddFlag %>">
                    <Items>
                        <wasp:WLPToolBarDropDown runat="server" ImageUrl="~/App_Themes/Default/Add.gif" ResourceName="Add" OnPreRender="AddDropDown_PreRender">
                            <Buttons>
                                <wasp:WLPToolBarButton runat="server" Text="**Canadian**" ResourceName="AddCanadian" CommandName="AddCanadian" OnPreRender="AddCanadian_PreRender" />
                                <wasp:WLPToolBarButton runat="server" Text="**TaxOverride**" ResourceName="AddTaxOverride" CommandName="AddTaxOverride" OnPreRender="AddTaxOverride_PreRender" />
                                <wasp:WLPToolBarButton runat="server" Text="**StLucia**" ResourceName="AddStLucia" CommandName="AddStLucia" OnPreRender="AddStLucia_PreRender" />
                                <wasp:WLPToolBarButton runat="server" Text="**Barbados**" ResourceName="AddBarbados" CommandName="AddBarbados" OnPreRender="AddBarbados_PreRender" />
                                <wasp:WLPToolBarButton runat="server" Text="**Trinidad**" ResourceName="AddTrinidad" CommandName="AddTrinidad" OnPreRender="AddTrinidad_PreRender" />
                                <wasp:WLPToolBarButton runat="server" Text="**Jamaica**" ResourceName="AddJamaica" CommandName="AddJamaica" OnPreRender="AddJamaica_PreRender" />
                            </Buttons>
                        </wasp:WLPToolBarDropDown>
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="StatutoryDeductionTypeCode" LabelText="**StatutoryDeductionTypeCode**" Type="StatutoryDeductionTypeCode" ResourceName="StatutoryDeductionTypeCode">
                    <HeaderStyle Width="200px" />
                </wasp:GridKeyValueControl>
                <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" DataField="ProvinceStateCode" LabelText="**ProvinceStateCode**" Type="ProvinceStateCode" ResourceName="ProvinceStateCode">
                    <HeaderStyle Width="200px" />
                </wasp:GridKeyValueControl>
                <wasp:GridKeyValueControl OnNeedDataSource="BusinessNumberId_NeedDataSource" DataField="BusinessNumberId" LabelText="**BusinessNumberId**" Type="BusinessNumberId" ResourceName="BusinessNumberId">
                    <HeaderStyle Width="300px" />
                </wasp:GridKeyValueControl>
                <wasp:GridCheckBoxControl DataField="ActiveFlag" LabelText="**ActiveFlag**" SortExpression="ActiveFlag" UniqueName="ActiveFlag" ResourceName="ActiveFlag">
                    <HeaderStyle Width="100px" />
                </wasp:GridCheckBoxControl>
            </Columns>
        </MasterTableView>

    </wasp:WLPGrid>

    <div>
        <telerik:RadMultiPage ID="RadMultiPage1" runat="server" Height="490px">
            <telerik:RadPageView ID="CanadianPageView" runat="server">
                <uc1:StatutoryDeductionViewControl ID="StatutoryDeductionCanadian" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="TaxOverridePageView" runat="server">
                <uc2:TaxOverrideViewControl ID="StatutoryDeductionTaxOverride" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="StLuciaPageView" runat="server">
                <uc3:StLuciaViewControl ID="StatutoryDeductionStLucia" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="BarbadosPageView" runat="server">
                <uc4:BarbadosViewControl ID="StatutoryDeductionBarbados" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="TrinidadPageView" runat="server">
                <uc5:TrinidadViewControl ID="StatutoryDeductionTrinidad" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="JamaicaPageView" runat="server">
                <uc6:JamaicaViewControl ID="StatutoryDeductionJamaica" runat="server" />
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
</asp:Panel>