﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.StatDeduction
{
    public partial class StatutoryDeductionViewControl : Wizard.WizardUserControl
    {
        #region delegates
        public event StatutoryDeductionDetailToolbarButtonClickEventHandler StatutoryDeductionDetailToolbarButtonClick;
        #endregion

        #region fields
        private bool _updateExterallyControlled = false;
        #endregion

        #region properties
        private StatutoryDeduction StatutoryDeduction
        {
            get
            {
                if (DataItemCollection.Count < 1)
                {
                    ((StatutoryDeductionCollection)DataItemCollection).Add(new StatutoryDeduction()
                    {
                        EmployeeId = EmployeeId,
                        ActiveFlag = true,
                        PayEmploymentInsuranceFlag = true,
                        PayCanadaPensionPlanFlag = true,
                        ParentalInsurancePlanFlag = true,
                        FederalPayTaxFlag = true,
                        ProvincialPayTaxFlag = true
                    });
                }

                return (StatutoryDeduction)DataItemCollection[0];
            }
        }
        public StatutoryDeductionCollection StatutoryDeductionCollection
        {
            get
            {
                if (!IsDataExternallyLoaded)
                    return (StatutoryDeductionCollection)DataItemCollection;
                else
                {
                    StatutoryDeductionCollection collection = new StatutoryDeductionCollection();
                    collection.Add(StatutoryDeduction);
                    return collection;
                }
            }
            set
            {
                if (!IsDataExternallyLoaded)
                    DataItemCollection = value;
            }
        }
        public bool IsEditMode
        {
            get
            {
                if (EnableWizardFunctionalityFlag)
                    return false; //return false to force control to not be in read only mode 
                else
                    return StatutoryDeductionView.CurrentMode.Equals(FormViewMode.Edit);
            }
        }
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeStatutoryDeductionCanadian.UpdateFlag; } }
        public bool IsInsertMode { get { return StatutoryDeductionView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsViewMode { get { return StatutoryDeductionView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        #endregion

        #region main
        private void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (IsDataExternallyLoaded)
                    StatutoryDeductionView.DataBind();
            }

            if (EnableWizardFunctionalityFlag)
                ((CheckBoxControl)StatutoryDeductionView.FindControl("PayEmploymentInsuranceFlag")).Focus();
        }
        public void ChangeModeInsert()
        {
            StatutoryDeductionView.ChangeMode(FormViewMode.Insert);
            StatutoryDeductionView.DataBind();
        }
        #endregion

        #region event handlers
        protected void EnableQuebecOnlyFields(ComboBoxControl provinceStateCode)
        {
            bool isProvinceQuebec = provinceStateCode.Value != null ? (provinceStateCode.Value.ToString() == "QC" ? true : false) : false;

            WLPLabel lblQuebecOnly = (WLPLabel)StatutoryDeductionView.FindControl("lblQuebecOnly");
            if (lblQuebecOnly != null)
                lblQuebecOnly.Visible = isProvinceQuebec;

            NumericControl estimatedNetIncome = (NumericControl)StatutoryDeductionView.FindControl("EstimatedNetIncome");
            if (estimatedNetIncome != null)
            {
                estimatedNetIncome.Visible = isProvinceQuebec;
                if (!isProvinceQuebec)
                    estimatedNetIncome.Value = 0;
            }

            NumericControl commissionPercentage = (NumericControl)StatutoryDeductionView.FindControl("CommissionPercentage");
            if (commissionPercentage != null)
            {
                commissionPercentage.Visible = isProvinceQuebec;
                if (!isProvinceQuebec)
                    commissionPercentage.Value = 0;
            }

            NumericControl provincialAdditionalTax = (NumericControl)StatutoryDeductionView.FindControl("ProvincialAdditionalTax");
            if (provincialAdditionalTax != null)
            {
                provincialAdditionalTax.Visible = isProvinceQuebec;
                if (!isProvinceQuebec)
                    provincialAdditionalTax.Value = 0;
            }
        }
        protected void StatutoryDeductionView_DataBound(object sender, EventArgs e)
        {
            ComboBoxControl statutoryDeductionTypeCode = (ComboBoxControl)StatutoryDeductionView.FindControl("StatutoryDeductionTypeCode");
            if (statutoryDeductionTypeCode != null)
                statutoryDeductionTypeCode.Value = "CAN";

            ComboBoxControl provinceStateCode = (ComboBoxControl)StatutoryDeductionView.FindControl("ProvinceStateCode");
            if (provinceStateCode != null)
                EnableQuebecOnlyFields(provinceStateCode);

            if (IsInsertMode)
            {
                CheckBoxControl activeFlag = (CheckBoxControl)StatutoryDeductionView.FindControl("ActiveFlag");
                if (activeFlag != null)
                    activeFlag.Checked = true;
            }
        }
        protected void StatutoryDeductionView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            if (!IsDataExternallyLoaded)
                OnNeedDataSource(e);

            StatutoryDeductionView.DataSource = StatutoryDeductionCollection;

            if (!IsViewMode && !EnableWizardFunctionalityFlag)
            {
                if (IsInsertMode)
                {
                    CheckBoxControl activeFlag = (CheckBoxControl)StatutoryDeductionView.FindControl("ActiveFlag");
                    if (activeFlag != null)
                        activeFlag.Focus();
                }
                else
                {
                    CheckBoxControl payEmploymentInsuranceFlag = (CheckBoxControl)StatutoryDeductionView.FindControl("PayEmploymentInsuranceFlag");
                    if (payEmploymentInsuranceFlag != null)
                        payEmploymentInsuranceFlag.Focus();
                }
            }
        }
        protected void StatutoryDeductionView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            StatutoryDeduction statutoryDeduction = (StatutoryDeduction)e.DataItem;

            if (_updateExterallyControlled)
            {
                //wizard - ActiveFlag will always be true as we are only adding a single row 
                statutoryDeduction.ActiveFlag = true;
                statutoryDeduction.CopyTo(StatutoryDeduction);
                e.Cancel = true;
            }
            else
                OnUpdating(e);

            StatutoryDeductionView.DataBind();
        }
        protected void StatutoryDeductionView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            OnInserting(e);
        }
        protected void ProvinceStateCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateProvince((ICodeControl)sender);

            CheckBoxControl parentalInsurancePlanFlag = (CheckBoxControl)StatutoryDeductionView.FindControl("ParentalInsurancePlanFlag");

            if (((ComboBoxControl)sender).Value != null) //will be NULL on "Add"
            {
                String provinceStateCode = ((ComboBoxControl)sender).Value.ToString();

                //if province is not quebec, hide this combo
                if (parentalInsurancePlanFlag != null && provinceStateCode != "QC")
                    parentalInsurancePlanFlag.Visible = false;
                else if (parentalInsurancePlanFlag != null && provinceStateCode == "QC")
                    parentalInsurancePlanFlag.Visible = true;
            }
            else //hide on "Add" by default until they pick a province from the combo, then the above "IF" will determine if it shows or not
                parentalInsurancePlanFlag.Visible = false;
        }
        protected void ProvinceStateCode_Changed(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //set defaults for fields based on province/state code
            SetStatDeductionDefaults(e.Value.ToString());
            EnableQuebecOnlyFields((ComboBoxControl)sender);
        }
        protected void SetStatDeductionDefaults(String provinceStateCode)
        {
            //get the data based on province/state code
            StatutoryDeductionDefaultCollection coll = new StatutoryDeductionDefaultCollection();
            coll.Load(Common.ServiceWrapper.HumanResourcesClient.GetStatutoryDeductionDefaults(provinceStateCode));

            if (coll.Count > 0)
            {
                //find controls and set values
                NumericControl federalTaxClaim = (NumericControl)StatutoryDeductionView.FindControl("FederalTaxClaim");
                if (federalTaxClaim != null) federalTaxClaim.Value = coll[0].FederalTaxClaim;

                NumericControl provincialTaxClaim = (NumericControl)StatutoryDeductionView.FindControl("ProvincialTaxClaim");
                if (provincialTaxClaim != null) provincialTaxClaim.Value = coll[0].ProvincialTaxClaim;

                NumericControl federalAdditionalTax = (NumericControl)StatutoryDeductionView.FindControl("FederalAdditionalTax");
                if (federalAdditionalTax != null) federalAdditionalTax.Value = coll[0].FederalAdditionalTax;

                NumericControl provincialAdditionalTax = (NumericControl)StatutoryDeductionView.FindControl("ProvincialAdditionalTax");
                if (provincialAdditionalTax != null) provincialAdditionalTax.Value = coll[0].ProvincialAdditionalTax;

                NumericControl federalDesignatedAreaDeduction = (NumericControl)StatutoryDeductionView.FindControl("FederalDesignatedAreaDeduction");
                if (federalDesignatedAreaDeduction != null) federalDesignatedAreaDeduction.Value = coll[0].FederalDesignatedAreaDeduction;

                NumericControl provincialDesignatedAreaDeduction = (NumericControl)StatutoryDeductionView.FindControl("ProvincialDesignatedAreaDeduction");
                if (provincialDesignatedAreaDeduction != null) provincialDesignatedAreaDeduction.Value = coll[0].ProvincialDesignatedAreaDeduction;

                NumericControl federalAuthorizedAnnualDeduction = (NumericControl)StatutoryDeductionView.FindControl("FederalAuthorizedAnnualDeduction");
                if (federalAuthorizedAnnualDeduction != null) federalAuthorizedAnnualDeduction.Value = coll[0].FederalAuthorizedAnnualDeduction;

                NumericControl provincialAuthorizedAnnualDeduction = (NumericControl)StatutoryDeductionView.FindControl("ProvincialAuthorizedAnnualDeduction");
                if (provincialAuthorizedAnnualDeduction != null) provincialAuthorizedAnnualDeduction.Value = coll[0].ProvincialAuthorizedAnnualDeduction;

                NumericControl federalAuthorizedAnnualTaxCredit = (NumericControl)StatutoryDeductionView.FindControl("FederalAuthorizedAnnualTaxCredit");
                if (federalAuthorizedAnnualTaxCredit != null) federalAuthorizedAnnualTaxCredit.Value = coll[0].FederalAuthorizedAnnualTaxCredit;

                NumericControl provincialAuthorizedAnnualTaxCredit = (NumericControl)StatutoryDeductionView.FindControl("ProvincialAuthorizedAnnualTaxCredit");
                if (provincialAuthorizedAnnualTaxCredit != null) provincialAuthorizedAnnualTaxCredit.Value = coll[0].ProvincialAuthorizedAnnualTaxCredit;

                NumericControl federalLabourTaxCredit = (NumericControl)StatutoryDeductionView.FindControl("FederalLabourTaxCredit");
                if (federalLabourTaxCredit != null) federalLabourTaxCredit.Value = coll[0].FederalLabourTaxCredit;

                NumericControl provincialLabourTaxCredit = (NumericControl)StatutoryDeductionView.FindControl("ProvincialLabourTaxCredit");
                if (provincialLabourTaxCredit != null) provincialLabourTaxCredit.Value = coll[0].ProvincialLabourTaxCredit;

                NumericControl estimatedAnnualIncome = (NumericControl)StatutoryDeductionView.FindControl("EstimatedAnnualIncome");
                if (estimatedAnnualIncome != null) estimatedAnnualIncome.Value = coll[0].EstimatedAnnualIncome;

                NumericControl estimatedNetIncome = (NumericControl)StatutoryDeductionView.FindControl("EstimatedNetIncome");
                if (estimatedNetIncome != null) estimatedNetIncome.Value = coll[0].EstimatedNetIncome;

                NumericControl estimatedAnnualExpense = (NumericControl)StatutoryDeductionView.FindControl("EstimatedAnnualExpense");
                if (estimatedAnnualExpense != null) estimatedAnnualExpense.Value = coll[0].EstimatedAnnualExpense;

                NumericControl commissionPercentage = (NumericControl)StatutoryDeductionView.FindControl("CommissionPercentage");
                if (commissionPercentage != null) commissionPercentage.Value = coll[0].CommissionPercentage;

                CheckBoxControl activeFlag = (CheckBoxControl)StatutoryDeductionView.FindControl("ActiveFlag");
                if (activeFlag != null) activeFlag.Checked = coll[0].ActiveFlag;

                CheckBoxControl payEmploymentInsuranceFlag = (CheckBoxControl)StatutoryDeductionView.FindControl("PayEmploymentInsuranceFlag");
                if (payEmploymentInsuranceFlag != null) payEmploymentInsuranceFlag.Checked = coll[0].PayEmploymentInsuranceFlag;

                CheckBoxControl payCanadaPensionPlanFlag = (CheckBoxControl)StatutoryDeductionView.FindControl("PayCanadaPensionPlanFlag");
                if (payCanadaPensionPlanFlag != null) payCanadaPensionPlanFlag.Checked = coll[0].PayCanadaPensionPlanFlag;

                CheckBoxControl federalPayTaxFlag = (CheckBoxControl)StatutoryDeductionView.FindControl("FederalPayTaxFlag");
                if (federalPayTaxFlag != null) federalPayTaxFlag.Checked = coll[0].FederalPayTaxFlag;

                CheckBoxControl provincialPayTaxFlag = (CheckBoxControl)StatutoryDeductionView.FindControl("ProvincialPayTaxFlag");
                if (provincialPayTaxFlag != null) provincialPayTaxFlag.Checked = coll[0].ProvincialPayTaxFlag;

                //if province is not quebec, hide ParentalInsurancePlanFlag combo
                CheckBoxControl parentalInsurancePlanFlag = (CheckBoxControl)StatutoryDeductionView.FindControl("ParentalInsurancePlanFlag");

                if (parentalInsurancePlanFlag != null && provinceStateCode != "QC")
                {
                    parentalInsurancePlanFlag.Checked = false;
                    parentalInsurancePlanFlag.Visible = false;
                }
                else if (parentalInsurancePlanFlag != null && provinceStateCode == "QC")
                {
                    parentalInsurancePlanFlag.Visible = true;
                    parentalInsurancePlanFlag.Checked = coll[0].ProvincialPayTaxFlag;
                }
            }
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void BusinessNumberId_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateBusinessNumber((ICodeControl)sender);
        }
        protected virtual void OnStatutoryDeductionDetailToolbarButtonClick(object sender, RadToolBarEventArgs e)
        {
            StatutoryDeductionDetailToolbarButtonClick?.Invoke(sender, e);
        }
        protected void StatutoryDeductionDetailToolbar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            OnStatutoryDeductionDetailToolbarButtonClick(sender, e);
        }
        #endregion

        #region override
        public override void Update(bool updateExterallyControlled)
        {
            _updateExterallyControlled = updateExterallyControlled;
            StatutoryDeductionView.UpdateItem(true);
        }
        public override void AddNewDataItem()
        {
            if (DataItemCollection == null)
                DataItemCollection = new StatutoryDeductionCollection();
        }
        public override void ChangeModeEdit()
        {
            if (!StatutoryDeductionView.CurrentMode.Equals(FormViewMode.Edit))
                StatutoryDeductionView.ChangeMode(FormViewMode.Edit);
        }
        #endregion
    }
}