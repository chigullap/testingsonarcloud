﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StatutoryDeductionViewControl.ascx.cs" Inherits="WorkLinks.Payroll.StatDeduction.StatutoryDeductionViewControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPFormView
    ID="StatutoryDeductionView"
    runat="server"
    RenderOuterTable="false"
    DataKeyNames="Key"
    OnDataBound="StatutoryDeductionView_DataBound"
    OnNeedDataSource="StatutoryDeductionView_NeedDataSource"
    OnInserting="StatutoryDeductionView_Inserting"
    OnUpdating="StatutoryDeductionView_Updating">

    <ItemTemplate>
        <wasp:WLPToolBar ID="FormViewStatutoryDeductionToolBar" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="StatutoryDeductionDetailToolbar_ButtonClick">
            <Items>
                <wasp:WLPToolBarButton Text="**Edit**" ImageUrl="~/App_Themes/Default/Edit.gif" Visible='<%# IsViewMode && UpdateFlag %>' CommandName="edit" ResourceName="Edit"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ActiveFlag" runat="server" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>' ReadOnly="true"></wasp:CheckBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="StatutoryDeductionTypeCode" runat="server" Type="StatutoryDeductionTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="StatutoryDeductionTypeCode" Value='<%# Bind("StatutoryDeductionTypeCode") %>' ReadOnly="true" Mandatory="true"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="ProvinceStateCode" runat="server" Type="ProvinceStateCode" OnDataBinding="ProvinceStateCode_NeedDataSource" ResourceName="ProvinceStateCode" Value='<%# Bind("ProvinceStateCode") %>' ReadOnly="true" Mandatory="true"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="BusinessNumberId" runat="server" Type="BusinessNumberId" OnDataBinding="BusinessNumberId_NeedDataSource" ResourceName="BusinessNumberId" Value='<%# Bind("BusinessNumberId") %>' ReadOnly="true" Mandatory="true"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="PayEmploymentInsuranceFlag" runat="server" ResourceName="PayEmploymentInsuranceFlag" Value='<%# Bind("PayEmploymentInsuranceFlag") %>' ReadOnly="true"></wasp:CheckBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="PayCanadaPensionPlanFlag" runat="server" ResourceName="PayCanadaPensionPlanFlag" Value='<%# Bind("PayCanadaPensionPlanFlag") %>' ReadOnly="true"></wasp:CheckBoxControl>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="ParentalInsurancePlanFlag" runat="server" ResourceName="ParentalInsurancePlanFlag" Value='<%# Bind("ParentalInsurancePlanFlag") %>' ReadOnly="true"></wasp:CheckBoxControl>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:WLPLabel ID="lblFederalHeader" runat="server" Text="Federal" Font-Bold="true" ResourceName="lblFederalHeader"></wasp:WLPLabel>
                    </td>
                    <td>
                        <wasp:WLPLabel ID="lblProvincialHeader" runat="server" Text="Provincial" Font-Bold="true" ResourceName="lblProvincialHeader"></wasp:WLPLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="FederalTaxClaim" DecimalDigits="0" runat="server" ResourceName="FederalTaxClaim" Value='<%# Bind("FederalTaxClaim") %>' ReadOnly="true"></wasp:NumericControl>
                    </td>
                    <td>
                        <wasp:NumericControl ID="ProvincialTaxClaim" DecimalDigits="0" runat="server" ResourceName="ProvincialTaxClaim" Value='<%# Bind("ProvincialTaxClaim") %>' ReadOnly="true"></wasp:NumericControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="FederalAdditionalTax" DecimalDigits="2" runat="server" ResourceName="FederalAdditionalTax" Value='<%# Bind("FederalAdditionalTax") %>' ReadOnly="true"></wasp:NumericControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="FederalDesignatedAreaDeduction" DecimalDigits="0" runat="server" ResourceName="FederalDesignatedAreaDeduction" Value='<%# Bind("FederalDesignatedAreaDeduction") %>' ReadOnly="true"></wasp:NumericControl>
                    </td>
                    <td>
                        <wasp:NumericControl ID="ProvincialDesignatedAreaDeduction" DecimalDigits="0" runat="server" ResourceName="ProvincialDesignatedAreaDeduction" Value='<%# Bind("ProvincialDesignatedAreaDeduction") %>' ReadOnly="true"></wasp:NumericControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="FederalAuthorizedAnnualDeduction" DecimalDigits="0" runat="server" ResourceName="FederalAuthorizedAnnualDeduction" Value='<%# Bind("FederalAuthorizedAnnualDeduction") %>' ReadOnly="true"></wasp:NumericControl>
                    </td>
                    <td>
                        <wasp:NumericControl ID="ProvincialAuthorizedAnnualDeduction" DecimalDigits="0" runat="server" ResourceName="ProvincialAuthorizedAnnualDeduction" Value='<%# Bind("ProvincialAuthorizedAnnualDeduction") %>' ReadOnly="true"></wasp:NumericControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="FederalAuthorizedAnnualTaxCredit" DecimalDigits="0" runat="server" ResourceName="FederalAuthorizedAnnualTaxCredit" Value='<%# Bind("FederalAuthorizedAnnualTaxCredit") %>' ReadOnly="true"></wasp:NumericControl>
                    </td>
                    <td>
                        <wasp:NumericControl ID="ProvincialAuthorizedAnnualTaxCredit" DecimalDigits="0" runat="server" ResourceName="ProvincialAuthorizedAnnualTaxCredit" Value='<%# Bind("ProvincialAuthorizedAnnualTaxCredit") %>' ReadOnly="true"></wasp:NumericControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="FederalLabourTaxCredit" DecimalDigits="0" runat="server" ResourceName="FederalLabourTaxCredit" Value='<%# Bind("FederalLabourTaxCredit") %>' ReadOnly="true"></wasp:NumericControl>
                    </td>
                    <td>
                        <wasp:NumericControl ID="ProvincialLabourTaxCredit" DecimalDigits="0" runat="server" ResourceName="ProvincialLabourTaxCredit" Value='<%# Bind("ProvincialLabourTaxCredit") %>' ReadOnly="true"></wasp:NumericControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="FederalPayTaxFlag" runat="server" ResourceName="FederalPayTaxFlag" Value='<%# Bind("FederalPayTaxFlag") %>' ReadOnly="true"></wasp:CheckBoxControl>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="ProvincialPayTaxFlag" runat="server" ResourceName="ProvincialPayTaxFlag" Value='<%# Bind("ProvincialPayTaxFlag") %>' ReadOnly="true"></wasp:CheckBoxControl>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:WLPLabel ID="lblCommission" runat="server" Text="Commission" Font-Bold="true" ResourceName="lblCommission"></wasp:WLPLabel>
                    </td>
                    <td>
                        <wasp:WLPLabel ID="lblQuebecOnly" runat="server" Text="Quebec Only" Font-Bold="true" ResourceName="lblQuebecOnly"></wasp:WLPLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="EstimatedAnnualIncome" DecimalDigits="0" runat="server" ResourceName="EstimatedAnnualIncome" Value='<%# Bind("EstimatedAnnualIncome") %>' ReadOnly="true"></wasp:NumericControl>
                    </td>
                    <td>
                        <wasp:NumericControl ID="EstimatedNetIncome" DecimalDigits="0" runat="server" ResourceName="EstimatedNetIncome" Value='<%# Bind("EstimatedNetIncome") %>' ReadOnly="true"></wasp:NumericControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="EstimatedAnnualExpense" DecimalDigits="0" runat="server" ResourceName="EstimatedAnnualExpense" Value='<%# Bind("EstimatedAnnualExpense") %>' ReadOnly="true"></wasp:NumericControl>
                    </td>
                    <td>
                        <wasp:NumericControl ID="CommissionPercentage" runat="server" ResourceName="CommissionPercentage" Value='<%# Bind("CommissionPercentage") %>' ReadOnly="true"></wasp:NumericControl>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <wasp:NumericControl ID="ProvincialAdditionalTax" DecimalDigits="2" runat="server" ResourceName="ProvincialAdditionalTax" Value='<%# Bind("ProvincialAdditionalTax") %>' ReadOnly="true"></wasp:NumericControl>
                    </td>
                </tr>
            </table>
        </fieldset>
    </ItemTemplate>

    <EditItemTemplate>
        <wasp:WLPToolBar Visible='<%# !EnableWizardFunctionalityFlag %>' ID="FormViewStatutoryDeductionToolBar" runat="server" Width="100%" AutoPostBack="true" OnButtonClick="StatutoryDeductionDetailToolbar_ButtonClick">
            <Items>
                <wasp:WLPToolBarButton Text="**Insert**" ImageUrl="~/App_Themes/Default/Add.gif" Visible='<%# IsInsertMode %>' CommandName="insert" ResourceName="Insert"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="**Update**" ImageUrl="~/App_Themes/Default/Update.gif" CommandName="update" Visible='<%# IsEditMode %>' ResourceName="Update"></wasp:WLPToolBarButton>
                <wasp:WLPToolBarButton Text="**Cancel**" ImageUrl="~/App_Themes/Default/Cancel.gif" CommandName="cancel" CausesValidation="false" ResourceName="Cancel"></wasp:WLPToolBarButton>
            </Items>
        </wasp:WLPToolBar>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="ActiveFlag" runat="server" ResourceName="ActiveFlag" Value='<%# Bind("ActiveFlag") %>' ReadOnly='<%# Eval("ActiveFlag")??false %>' Visible='<%# !EnableWizardFunctionalityFlag %>' TabIndex="010"></wasp:CheckBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="StatutoryDeductionTypeCode" runat="server" Type="StatutoryDeductionTypeCode" OnDataBinding="Code_NeedDataSource" ResourceName="StatutoryDeductionTypeCode" Value='<%# Bind("StatutoryDeductionTypeCode") %>' ReadOnly="true" Mandatory="true" TabIndex="020"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:ComboBoxControl ID="ProvinceStateCode" OnSelectedIndexChanged="ProvinceStateCode_Changed" AutoPostback="true" runat="server" Type="ProvinceStateCode" OnDataBinding="ProvinceStateCode_NeedDataSource" ResourceName="ProvinceStateCode" Value='<%# Bind("ProvinceStateCode") %>' ReadOnly='<%# IsEditMode %>' Mandatory="true" TabIndex="030"></wasp:ComboBoxControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:ComboBoxControl ID="BusinessNumberId" runat="server" Type="BusinessNumberId" OnDataBinding="BusinessNumberId_NeedDataSource" ResourceName="BusinessNumberId" Value='<%# Bind("BusinessNumberId") %>' ReadOnly='<%# IsEditMode %>' Mandatory="true" TabIndex="040"></wasp:ComboBoxControl>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="PayEmploymentInsuranceFlag" runat="server" ResourceName="PayEmploymentInsuranceFlag" Value='<%# Bind("PayEmploymentInsuranceFlag")%>' ReadOnly="false" TabIndex="050"></wasp:CheckBoxControl>
                    </td>
                </tr>
                    <td>
                        <wasp:CheckBoxControl ID="PayCanadaPensionPlanFlag" runat="server" ResourceName="PayCanadaPensionPlanFlag" Value='<%# Bind("PayCanadaPensionPlanFlag") %>' ReadOnly="false" TabIndex="060"></wasp:CheckBoxControl>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="ParentalInsurancePlanFlag" runat="server" ResourceName="ParentalInsurancePlanFlag" Value='<%# Bind("ParentalInsurancePlanFlag") %>' ReadOnly="false" TabIndex="070"></wasp:CheckBoxControl>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:WLPLabel ID="lblFederalHeader" runat="server" Text="Federal" Font-Bold="true" ResourceName="lblFederalHeader"></wasp:WLPLabel>
                    </td>
                    <td>
                        <wasp:WLPLabel ID="lblProvincialHeader" runat="server" Text="Provincial" Font-Bold="true" ResourceName="lblProvincialHeader"></wasp:WLPLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="FederalTaxClaim" DecimalDigits="0" runat="server" ResourceName="FederalTaxClaim" Value='<%# Bind("FederalTaxClaim") %>' ReadOnly="false" TabIndex="080"></wasp:NumericControl>
                    </td>
                    <td>
                        <wasp:NumericControl ID="ProvincialTaxClaim" DecimalDigits="0" runat="server" ResourceName="ProvincialTaxClaim" Value='<%# Bind("ProvincialTaxClaim") %>' ReadOnly="false" TabIndex="090"></wasp:NumericControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="FederalAdditionalTax" DecimalDigits="2" runat="server" ResourceName="FederalAdditionalTax" Value='<%# Bind("FederalAdditionalTax") %>' ReadOnly="false" TabIndex="100"></wasp:NumericControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="FederalDesignatedAreaDeduction" DecimalDigits="0" runat="server" ResourceName="FederalDesignatedAreaDeduction" Value='<%# Bind("FederalDesignatedAreaDeduction") %>' ReadOnly="false" TabIndex="110"></wasp:NumericControl>
                    </td>
                    <td>
                        <wasp:NumericControl ID="ProvincialDesignatedAreaDeduction" DecimalDigits="0" runat="server" ResourceName="ProvincialDesignatedAreaDeduction" Value='<%# Bind("ProvincialDesignatedAreaDeduction") %>' ReadOnly="false" TabIndex="120"></wasp:NumericControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="FederalAuthorizedAnnualDeduction" DecimalDigits="0" runat="server" ResourceName="FederalAuthorizedAnnualDeduction" Value='<%# Bind("FederalAuthorizedAnnualDeduction") %>' ReadOnly="false" TabIndex="130"></wasp:NumericControl>
                    </td>
                    <td>
                        <wasp:NumericControl ID="ProvincialAuthorizedAnnualDeduction" DecimalDigits="0" runat="server" ResourceName="ProvincialAuthorizedAnnualDeduction" Value='<%# Bind("ProvincialAuthorizedAnnualDeduction") %>' ReadOnly="false" TabIndex="140"></wasp:NumericControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="FederalAuthorizedAnnualTaxCredit" DecimalDigits="0" runat="server" ResourceName="FederalAuthorizedAnnualTaxCredit" Value='<%# Bind("FederalAuthorizedAnnualTaxCredit") %>' ReadOnly="false" TabIndex="150"></wasp:NumericControl>
                    </td>
                    <td>
                        <wasp:NumericControl ID="ProvincialAuthorizedAnnualTaxCredit" DecimalDigits="0" runat="server" ResourceName="ProvincialAuthorizedAnnualTaxCredit" Value='<%# Bind("ProvincialAuthorizedAnnualTaxCredit") %>' ReadOnly="false" TabIndex="160"></wasp:NumericControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="FederalLabourTaxCredit" DecimalDigits="0" runat="server" ResourceName="FederalLabourTaxCredit" Value='<%# Bind("FederalLabourTaxCredit") %>' ReadOnly="false" TabIndex="170"></wasp:NumericControl>
                    </td>
                    <td>
                        <wasp:NumericControl ID="ProvincialLabourTaxCredit" DecimalDigits="0" runat="server" ResourceName="ProvincialLabourTaxCredit" Value='<%# Bind("ProvincialLabourTaxCredit") %>' ReadOnly="false" TabIndex="180"></wasp:NumericControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:CheckBoxControl ID="FederalPayTaxFlag" runat="server" ResourceName="FederalPayTaxFlag" Value='<%# Bind("FederalPayTaxFlag") %>' ReadOnly="false" TabIndex="190"></wasp:CheckBoxControl>
                    </td>
                    <td>
                        <wasp:CheckBoxControl ID="ProvincialPayTaxFlag" runat="server" ResourceName="ProvincialPayTaxFlag" Value='<%# Bind("ProvincialPayTaxFlag") %>' ReadOnly="false" TabIndex="200"></wasp:CheckBoxControl>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td>
                        <wasp:WLPLabel ID="lblCommission" runat="server" Text="Commission" Font-Bold="true" ResourceName="lblCommission"></wasp:WLPLabel>
                    </td>
                    <td>
                        <wasp:WLPLabel ID="lblQuebecOnly" runat="server" Text="Quebec Only" Font-Bold="true" ResourceName="lblQuebecOnly"></wasp:WLPLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="EstimatedAnnualIncome" DecimalDigits="0" runat="server" ResourceName="EstimatedAnnualIncome" Value='<%# Bind("EstimatedAnnualIncome") %>' ReadOnly="false" TabIndex="210"></wasp:NumericControl>
                    </td>
                    <td>
                        <wasp:NumericControl ID="EstimatedNetIncome" DecimalDigits="0" runat="server" ResourceName="EstimatedNetIncome" Value='<%# Bind("EstimatedNetIncome") %>' ReadOnly="false" TabIndex="220"></wasp:NumericControl>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wasp:NumericControl ID="EstimatedAnnualExpense" DecimalDigits="0" runat="server" ResourceName="EstimatedAnnualExpense" Value='<%# Bind("EstimatedAnnualExpense") %>' ReadOnly="false" TabIndex="230"></wasp:NumericControl>
                    </td>
                    <td>
                        <wasp:NumericControl ID="CommissionPercentage" runat="server" ResourceName="CommissionPercentage" Value='<%# Bind("CommissionPercentage") %>' ReadOnly="false" TabIndex="240"></wasp:NumericControl>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <wasp:NumericControl ID="ProvincialAdditionalTax" DecimalDigits="2" runat="server" ResourceName="ProvincialAdditionalTax" Value='<%# Bind("ProvincialAdditionalTax") %>' ReadOnly="false" TabIndex="250"></wasp:NumericControl>
                    </td>
                </tr>
            </table>
        </fieldset>
    </EditItemTemplate>

</wasp:WLPFormView>