﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.StatDeduction
{
    public partial class StatutoryDeductionJamaicaViewControl : Wizard.WizardUserControl
    {
        #region delegates
        public event StatutoryDeductionDetailToolbarButtonClickEventHandler StatutoryDeductionDetailToolbarButtonClick;
        #endregion

        #region fields
        private bool _updateExterallyControlled = false;
        #endregion

        #region properties
        private StatutoryDeduction StatutoryDeduction
        {
            get
            {
                if (DataItemCollection.Count < 1)
                    ((StatutoryDeductionCollection)DataItemCollection).Add(new StatutoryDeduction() { EmployeeId = EmployeeId, ActiveFlag = true, ProvinceStateCode = "ZZ" });

                return (StatutoryDeduction)DataItemCollection[0];
            }
        }
        public StatutoryDeductionCollection StatutoryDeductionCollection
        {
            get
            {
                if (!IsDataExternallyLoaded)
                    return (StatutoryDeductionCollection)DataItemCollection;
                else
                {
                    StatutoryDeductionCollection collection = new StatutoryDeductionCollection();
                    collection.Add(StatutoryDeduction);
                    return collection;
                }
            }
            set
            {
                if (!IsDataExternallyLoaded)
                    DataItemCollection = value;
            }
        }
        public bool IsEditMode
        {
            get
            {
                if (EnableWizardFunctionalityFlag)
                    return false; //return false to force control to not be in read only mode 
                else
                    return StatutoryDeductionJamaicaView.CurrentMode.Equals(FormViewMode.Edit);
            }
        }
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeeStatutoryDeductionJamaica.UpdateFlag; } }
        public bool IsInsertMode { get { return StatutoryDeductionJamaicaView.CurrentMode.Equals(FormViewMode.Insert); } }
        public bool IsViewMode { get { return StatutoryDeductionJamaicaView.CurrentMode.Equals(FormViewMode.ReadOnly); } }
        #endregion

        #region main
        private void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (IsDataExternallyLoaded)
                    StatutoryDeductionJamaicaView.DataBind();
            }
        }
        public void ChangeModeInsert()
        {
            StatutoryDeductionJamaicaView.ChangeMode(FormViewMode.Insert);
            StatutoryDeductionJamaicaView.DataBind();
        }
        #endregion

        #region event handlers
        protected void StatutoryDeductionJamaicaView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            if (!IsDataExternallyLoaded)
                OnNeedDataSource(e);

            StatutoryDeductionJamaicaView.DataSource = StatutoryDeductionCollection;

            if (!IsViewMode && !EnableWizardFunctionalityFlag)
            {
                if (IsInsertMode)
                {
                    CheckBoxControl activeFlag = (CheckBoxControl)StatutoryDeductionJamaicaView.FindControl("ActiveFlag");
                    if (activeFlag != null)
                        activeFlag.Focus();
                }
            }
        }
        protected void StatutoryDeductionJamaicaView_DataBound(object sender, EventArgs e)
        {
            ComboBoxControl statutoryDeductionTypeCode = (ComboBoxControl)StatutoryDeductionJamaicaView.FindControl("StatutoryDeductionTypeCode");
            if (statutoryDeductionTypeCode != null)
                statutoryDeductionTypeCode.Value = "JM";

            if (!IsViewMode)
            {
                CheckBoxControl isWeek1Month1EmployeeForCurrentYearFlag = (CheckBoxControl)StatutoryDeductionJamaicaView.FindControl("IsWeek1Month1EmployeeForCurrentYearFlag");
                if (isWeek1Month1EmployeeForCurrentYearFlag != null)
                    ShowHideP45Controls(isWeek1Month1EmployeeForCurrentYearFlag.Checked);
            }

            if (IsInsertMode)
            {
                CheckBoxControl activeFlag = (CheckBoxControl)StatutoryDeductionJamaicaView.FindControl("ActiveFlag");
                if (activeFlag != null)
                    activeFlag.Checked = true;
            }
        }
        protected void StatutoryDeductionJamaicaView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            StatutoryDeduction statutoryDeduction = (StatutoryDeduction)e.DataItem;

            if (_updateExterallyControlled)
            {
                //wizard - ActiveFlag will always be true as we are only adding a single row 
                statutoryDeduction.ActiveFlag = true;
                statutoryDeduction.ProvinceStateCode = "ZZ";
                statutoryDeduction.CopyTo(StatutoryDeduction);
                e.Cancel = true;
            }
            else
                OnUpdating(e);

            StatutoryDeductionJamaicaView.DataBind();
        }
        protected void StatutoryDeductionJamaicaView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            OnInserting(e);
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void BusinessNumberId_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateBusinessNumber((ICodeControl)sender);
        }
        protected virtual void OnStatutoryDeductionDetailToolbarButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (StatutoryDeductionDetailToolbarButtonClick != null)
                StatutoryDeductionDetailToolbarButtonClick(sender, e);
        }
        protected void StatutoryDeductionDetailToolbar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            OnStatutoryDeductionDetailToolbarButtonClick(sender, e);
        }
        protected void IsWeek1Month1EmployeeForCurrentYearFlag_CheckedChanged(object sender, EventArgs e)
        {
            ShowHideP45Controls(((CheckBoxControl)sender).Checked);
        }
        private void ShowHideP45Controls(bool isChecked)
        {
            ShowHideNumericControl("WeekNumber", isChecked);
            ShowHideNumericControl("MonthNumber", isChecked);
            ShowHideNumericControl("TotalGrossPayToDate", isChecked);
            ShowHideNumericControl("Prerequisites", isChecked);
            ShowHideNumericControl("NiToDate", isChecked);
            ShowHideNumericControl("GrossPayLessDeductions", isChecked);
            ShowHideNumericControl("Superannuation", isChecked);
            ShowHideNumericControl("ApprovedExp", isChecked);
            ShowHideNumericControl("TotalTaxToDateDeterminedByTaxTable", isChecked);
        }
        private void ShowHideNumericControl(string controlName, bool isChecked)
        {
            NumericControl control = (NumericControl)StatutoryDeductionJamaicaView.FindControl(controlName);
            if (control != null)
            {
                control.ReadOnly = isChecked;

                if (isChecked)
                    control.Value = null;
            }
        }
        #endregion

        #region override
        public override void Update(bool updateExterallyControlled)
        {
            _updateExterallyControlled = updateExterallyControlled;
            StatutoryDeductionJamaicaView.UpdateItem(true);
        }
        public override void AddNewDataItem()
        {
            if (DataItemCollection == null)
                DataItemCollection = new StatutoryDeductionCollection();
        }
        public override void ChangeModeEdit()
        {
            if (!StatutoryDeductionJamaicaView.CurrentMode.Equals(FormViewMode.Edit))
                StatutoryDeductionJamaicaView.ChangeMode(FormViewMode.Edit);
        }
        #endregion
    }
}