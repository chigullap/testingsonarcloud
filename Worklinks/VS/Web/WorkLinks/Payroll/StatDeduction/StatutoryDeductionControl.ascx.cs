﻿using System;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.StatDeduction
{
    public delegate void StatutoryDeductionDetailToolbarButtonClickEventHandler(object sender, RadToolBarEventArgs e);
    public partial class StatutoryDeductionControl : WLP.Web.UI.WLPUserControl
    {
        #region fields
        private int _rowIndex = -1;
        private String _rowIndexKey = "RowIndexKey";
        private bool _updatePerformed = false;
        #endregion

        #region properties
        protected StatutoryDeduction CurrentStatutoryDeduction
        {
            get
            {
                StatutoryDeduction deduction = null;

                if (IsInsertMode && !InsertComplete)
                    deduction = new StatutoryDeduction() { EmployeeId = EmployeeId };
                else
                {
                    if (StatutoryDeductionGrid.SelectedValue != null)
                        deduction = (StatutoryDeduction)Data[StatutoryDeductionGrid.SelectedValue.ToString()];
                }

                return deduction;
            }
        }
        private StatutoryDeductionCollection CurrentStatutoryDeductionCollection
        {
            get
            {
                StatutoryDeductionCollection collection = new StatutoryDeductionCollection();

                if (CurrentStatutoryDeduction != null)
                    collection.Add(CurrentStatutoryDeduction);

                return collection;
            }
        }
        public int RowIndex
        {
            get
            {
                if (_rowIndex == -1)
                {
                    Object obj = ViewState[_rowIndexKey];
                    if (obj != null)
                        _rowIndex = (int)obj;
                }

                return _rowIndex;
            }
            set
            {
                _rowIndex = value;
                ViewState[_rowIndexKey] = _rowIndex;
            }
        }
        public bool IsInsertMode
        {
            get
            {
                return StatutoryDeductionCanadian.IsInsertMode || StatutoryDeductionTaxOverride.IsInsertMode || StatutoryDeductionStLucia.IsInsertMode ||
                    StatutoryDeductionBarbados.IsInsertMode || StatutoryDeductionTrinidad.IsInsertMode || StatutoryDeductionJamaica.IsInsertMode;
            }
        }
        public bool IsViewMode
        {
            get
            {
                return StatutoryDeductionCanadian.IsViewMode && StatutoryDeductionTaxOverride.IsViewMode && StatutoryDeductionStLucia.IsViewMode &&
                    StatutoryDeductionBarbados.IsViewMode && StatutoryDeductionTrinidad.IsViewMode && StatutoryDeductionJamaica.IsViewMode;
            }
        }
        public long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        private String LastName { get { return Page.RouteData.Values["lastName"].ToString(); } }
        private String FirstName { get { return Page.RouteData.Values["firstName"].ToString(); } }
        protected bool EnableAddCanadianButton { get { return Common.Security.RoleForm.EmployeeStatutoryDeductionCanadian.AddFlag; } }
        protected bool EnableAddTaxOverrideButton { get { return Common.Security.RoleForm.EmployeeStatutoryDeductionTaxOverride.AddFlag; } }
        protected bool EnableAddStLuciaButton { get { return Common.Security.RoleForm.EmployeeStatutoryDeductionStLucia.AddFlag; } }
        protected bool EnableAddBarbadosButton { get { return Common.Security.RoleForm.EmployeeStatutoryDeductionBarbados.AddFlag; } }
        protected bool EnableAddTrinidadButton { get { return Common.Security.RoleForm.EmployeeStatutoryDeductionTrinidad.AddFlag; } }
        protected bool EnableAddJamaicaButton { get { return Common.Security.RoleForm.EmployeeStatutoryDeductionJamaica.AddFlag; } }
        public bool AddFlag { get { return EnableAddCanadianButton || EnableAddTaxOverrideButton || EnableAddStLuciaButton || EnableAddBarbadosButton || EnableAddTrinidadButton || EnableAddJamaicaButton; } }
        public bool InsertComplete { get; set; }
        public bool CancelClicked { get; set; }
        #endregion

        #region main
        private void Page_Load(object sender, EventArgs e)
        {
            //does the user have access rights?
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EmployeeStatutoryDeduction);

            WireEvents();

            if (!Page.IsPostBack)
            {
                //does the user have rights to seet his employee?
                SetAuthorized(Common.ServiceWrapper.HumanResourcesClient.GetEmployee(new EmployeeCriteria() { EmployeeId = EmployeeId }).Count > 0);
                LoadStatutoryDeduction(EmployeeId);
            }

            this.Page.Title = String.Format(GetGlobalResourceObject("PageTitle", "StatDeduction").ToString(), LastName, FirstName);
        }
        private void LoadStatutoryDeduction(long employeeId)
        {
            Data = Common.ServiceWrapper.StatutoryDeduction.Select(employeeId);
        }
        private void WireEvents()
        {
            StatutoryDeductionCanadian.NeedDataSource += new NeedDataSourceEventHandler(LoadableControl_NeedDataSource);
            StatutoryDeductionCanadian.Updating += new UpdatingEventHandler(ILoadableControl_Updating);
            StatutoryDeductionCanadian.Inserting += new InsertingEventHandler(LoadableControl_Inserting);
            StatutoryDeductionCanadian.StatutoryDeductionDetailToolbarButtonClick += new StatutoryDeductionDetailToolbarButtonClickEventHandler(LoadableControl_StatutoryDeductionDetailToolbarButtonClick);

            StatutoryDeductionTaxOverride.NeedDataSource += new NeedDataSourceEventHandler(LoadableControl_NeedDataSource);
            StatutoryDeductionTaxOverride.Updating += new UpdatingEventHandler(ILoadableControl_Updating);
            StatutoryDeductionTaxOverride.Inserting += new InsertingEventHandler(LoadableControl_Inserting);
            StatutoryDeductionTaxOverride.StatutoryDeductionDetailToolbarButtonClick += new StatutoryDeductionDetailToolbarButtonClickEventHandler(LoadableControl_StatutoryDeductionDetailToolbarButtonClick);

            StatutoryDeductionBarbados.NeedDataSource += new NeedDataSourceEventHandler(LoadableControl_NeedDataSource);
            StatutoryDeductionBarbados.Updating += new UpdatingEventHandler(ILoadableControl_Updating);
            StatutoryDeductionBarbados.Inserting += new InsertingEventHandler(LoadableControl_Inserting);
            StatutoryDeductionBarbados.StatutoryDeductionDetailToolbarButtonClick += new StatutoryDeductionDetailToolbarButtonClickEventHandler(LoadableControl_StatutoryDeductionDetailToolbarButtonClick);

            StatutoryDeductionStLucia.NeedDataSource += new NeedDataSourceEventHandler(LoadableControl_NeedDataSource);
            StatutoryDeductionStLucia.Updating += new UpdatingEventHandler(ILoadableControl_Updating);
            StatutoryDeductionStLucia.Inserting += new InsertingEventHandler(LoadableControl_Inserting);
            StatutoryDeductionStLucia.StatutoryDeductionDetailToolbarButtonClick += new StatutoryDeductionDetailToolbarButtonClickEventHandler(LoadableControl_StatutoryDeductionDetailToolbarButtonClick);

            StatutoryDeductionTrinidad.NeedDataSource += new NeedDataSourceEventHandler(LoadableControl_NeedDataSource);
            StatutoryDeductionTrinidad.Updating += new UpdatingEventHandler(ILoadableControl_Updating);
            StatutoryDeductionTrinidad.Inserting += new InsertingEventHandler(LoadableControl_Inserting);
            StatutoryDeductionTrinidad.StatutoryDeductionDetailToolbarButtonClick += new StatutoryDeductionDetailToolbarButtonClickEventHandler(LoadableControl_StatutoryDeductionDetailToolbarButtonClick);

            StatutoryDeductionJamaica.NeedDataSource += new NeedDataSourceEventHandler(LoadableControl_NeedDataSource);
            StatutoryDeductionJamaica.Updating += new UpdatingEventHandler(ILoadableControl_Updating);
            StatutoryDeductionJamaica.Inserting += new InsertingEventHandler(LoadableControl_Inserting);
            StatutoryDeductionJamaica.StatutoryDeductionDetailToolbarButtonClick += new StatutoryDeductionDetailToolbarButtonClickEventHandler(LoadableControl_StatutoryDeductionDetailToolbarButtonClick);
        }
        private void InitializeControls()
        {
            StatutoryDeductionGrid.Enabled = IsViewMode;

            //this code makes sure the RadToolbar is set to the proper status before/during/after update/insert operations
            GridCommandItem cmdItem = (GridCommandItem)StatutoryDeductionGrid.MasterTableView.GetItems(GridItemType.CommandItem)[0];
            RadToolBar toolBar = (RadToolBar)cmdItem.FindControl("StatutoryDeductionToolBar");
            toolBar.Enabled = IsViewMode;
        }
        private void GetSelectedStatutoryDeduction()
        {
            if (StatutoryDeductionGrid.SelectedItems.Count > 0)
            {
                if (CurrentStatutoryDeduction.StatutoryDeductionTypeCode == "CAN")
                {
                    StatutoryDeductionCanadian.DataItemCollection = CurrentStatutoryDeductionCollection;
                    StatutoryDeductionCanadian.DataBind();
                    RadMultiPage1.FindPageViewByID("CanadianPageView").Selected = true;
                }
                else if (CurrentStatutoryDeduction.StatutoryDeductionTypeCode == "TAX")
                {
                    StatutoryDeductionTaxOverride.DataItemCollection = CurrentStatutoryDeductionCollection;
                    StatutoryDeductionTaxOverride.DataBind();
                    RadMultiPage1.FindPageViewByID("TaxOverridePageView").Selected = true;
                }
                else if (CurrentStatutoryDeduction.StatutoryDeductionTypeCode == "STLU")
                {
                    StatutoryDeductionStLucia.DataItemCollection = CurrentStatutoryDeductionCollection;
                    StatutoryDeductionStLucia.DataBind();
                    RadMultiPage1.FindPageViewByID("StLuciaPageView").Selected = true;
                }
                else if (CurrentStatutoryDeduction.StatutoryDeductionTypeCode == "BB")
                {
                    StatutoryDeductionBarbados.DataItemCollection = CurrentStatutoryDeductionCollection;
                    StatutoryDeductionBarbados.DataBind();
                    RadMultiPage1.FindPageViewByID("BarbadosPageView").Selected = true;
                }
                else if (CurrentStatutoryDeduction.StatutoryDeductionTypeCode == "TT")
                {
                    StatutoryDeductionTrinidad.DataItemCollection = CurrentStatutoryDeductionCollection;
                    StatutoryDeductionTrinidad.DataBind();
                    RadMultiPage1.FindPageViewByID("TrinidadPageView").Selected = true;
                }
                else if (CurrentStatutoryDeduction.StatutoryDeductionTypeCode == "JM")
                {
                    StatutoryDeductionJamaica.DataItemCollection = CurrentStatutoryDeductionCollection;
                    StatutoryDeductionJamaica.DataBind();
                    RadMultiPage1.FindPageViewByID("JamaicaPageView").Selected = true;
                }
            }
        }
        private bool CheckForUniqueValues(StatutoryDeduction item)
        {
            //make sure that the StatutoryDeductionTypeCode, ProvinceStateCode and BusinessNumberId values are unique
            bool bUnique = true;

            foreach (IDataItem di in Data)
            {
                if (item.StatutoryDeductionTypeCode == ((StatutoryDeduction)di).StatutoryDeductionTypeCode && item.ProvinceStateCode == ((StatutoryDeduction)di).ProvinceStateCode && item.BusinessNumberId == ((StatutoryDeduction)di).BusinessNumberId)
                {
                    bUnique = false;
                    break;
                }
            }

            return bUnique;
        }
        private void ResetActiveFlags(StatutoryDeduction item)
        {
            //if the record we are editing has ActiveFlag checked, set the others to not-checked (ie. false)
            if (item.ActiveFlag)
            {
                foreach (IDataItem di in Data)
                {
                    if (di.Key != item.Key) //if not the current record we are editing
                    {
                        if (((StatutoryDeduction)di).ActiveFlag) //if the ActiveFlag is true
                            ((StatutoryDeduction)di).ActiveFlag = false; //set it to false
                    }
                }
            }

            //if there is only one record (and user is updating), or there are 0 records (user is inserting) make sure ActiveFlag is true
            if (Data.Count == 0)
                item.ActiveFlag = true;
            else if (Data.Count >= 1)
            {
                foreach (IDataItem di in Data)
                {
                    if (di.Key == item.Key) //if not the current record we are editing
                        item.ActiveFlag = true;
                }
            }
        }
        #endregion

        #region event handlers
        void LoadableControl_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            if (CancelClicked && StatutoryDeductionGrid.Items.Count > 0)
                StatutoryDeductionGrid_DataBound(null, null);
            else
            {
                String controlId = ((WLP.Web.UI.WLPUserControl)sender).ID;

                if (controlId == "StatutoryDeductionCanadian")
                    StatutoryDeductionCanadian.StatutoryDeductionCollection = CurrentStatutoryDeductionCollection;
                else if (controlId == "StatutoryDeductionTaxOverride")
                    StatutoryDeductionTaxOverride.StatutoryDeductionCollection = CurrentStatutoryDeductionCollection;
                else if (controlId == "StatutoryDeductionStLucia")
                    StatutoryDeductionStLucia.StatutoryDeductionCollection = CurrentStatutoryDeductionCollection;
                else if (controlId == "StatutoryDeductionBarbados")
                    StatutoryDeductionBarbados.StatutoryDeductionCollection = CurrentStatutoryDeductionCollection;
                else if (controlId == "StatutoryDeductionTrinidad")
                    StatutoryDeductionTrinidad.StatutoryDeductionCollection = CurrentStatutoryDeductionCollection;
                else if (controlId == "StatutoryDeductionJamaica")
                    StatutoryDeductionJamaica.StatutoryDeductionCollection = CurrentStatutoryDeductionCollection;
            }
        }
        void ILoadableControl_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            _updatePerformed = true;
            StatutoryDeduction item = (StatutoryDeduction)e.DataItem;

            foreach (StatutoryDeduction stat in Data)
            {
                if (stat.ActiveFlag && item.ActiveFlag && (stat.StatutoryDeductionId != item.StatutoryDeductionId) && (stat.BusinessNumberId != item.BusinessNumberId))
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScriptMsg", String.Format("alert('{0}');", GetGlobalResourceObject("WarningMessages", "BusinessNumberChangeWarning")), true);
                    break;
                }
            }

            UpdateStatutoryDeduction(item, e);
        }
        void LoadableControl_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            StatutoryDeduction item = (StatutoryDeduction)e.DataItem;
            item.EmployeeId = EmployeeId;

            if (item.StatutoryDeductionTypeCode == "BB" || item.StatutoryDeductionTypeCode == "STLU" || item.StatutoryDeductionTypeCode == "TT" || item.StatutoryDeductionTypeCode == "JM")
                item.ProvinceStateCode = "ZZ";

            InsertStatutoryDeduction(item, e);
        }
        private void InsertStatutoryDeduction(StatutoryDeduction item, WLPItemInsertingEventArgs e)
        {
            if (CheckForUniqueValues(item))
            {
                foreach (StatutoryDeduction stat in Data)
                {
                    if (stat.ActiveFlag)
                    {
                        if (stat.BusinessNumberId != item.BusinessNumberId)
                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScriptMsg", String.Format("alert('{0}');", GetGlobalResourceObject("WarningMessages", "BusinessNumberChangeWarning")), true);
                        break;
                    }
                }

                ResetActiveFlags(item);

                if (item.StatutoryDeductionTypeCode == "BB")
                    ((DataItemCollection<StatutoryDeduction>)Data).Add(Common.ServiceWrapper.StatutoryDeduction.InsertBarbados(item));
                else if (item.StatutoryDeductionTypeCode == "STLU")
                    ((DataItemCollection<StatutoryDeduction>)Data).Add(Common.ServiceWrapper.StatutoryDeduction.InsertSaintLucia(item));
                else if (item.StatutoryDeductionTypeCode == "TT")
                    ((DataItemCollection<StatutoryDeduction>)Data).Add(Common.ServiceWrapper.StatutoryDeduction.InsertTrinidad(item));
                else if (item.StatutoryDeductionTypeCode == "JM")
                    ((DataItemCollection<StatutoryDeduction>)Data).Add(Common.ServiceWrapper.StatutoryDeduction.InsertJamaica(item));
                else
                    ((DataItemCollection<StatutoryDeduction>)Data).Add(Common.ServiceWrapper.StatutoryDeduction.Insert(item));

                InsertComplete = true;
                LoadStatutoryDeduction(EmployeeId);
                StatutoryDeductionGrid.Rebind();
            }
            else
            {
                //throw exception message
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "BusinessNumberProvinceMustBeUniqueErrorMessage")), true);
                LoadStatutoryDeduction(EmployeeId);
                e.Cancel = true;
            }
        }
        private void UpdateStatutoryDeduction(StatutoryDeduction item, WLPItemUpdatingEventArgs e)
        {
            try
            {
                if (item.StatutoryDeductionTypeCode == "BB")
                    Common.ServiceWrapper.StatutoryDeduction.UpdateBarbados(item).CopyTo(item);
                else if (item.StatutoryDeductionTypeCode == "STLU")
                    Common.ServiceWrapper.StatutoryDeduction.UpdateSaintLucia(item).CopyTo(item);
                else if (item.StatutoryDeductionTypeCode == "TT")
                    Common.ServiceWrapper.StatutoryDeduction.UpdateTrinidad(item).CopyTo(item);
                else if (item.StatutoryDeductionTypeCode == "JM")
                    Common.ServiceWrapper.StatutoryDeduction.UpdateJamaica(item).CopyTo(item);
                else
                    Common.ServiceWrapper.StatutoryDeduction.Update(item).CopyTo(item);

                LoadStatutoryDeduction(EmployeeId);
                StatutoryDeductionGrid.Rebind();
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void DeleteStatutoryDeduction(StatutoryDeduction item)
        {
            try
            {
                if (!item.ActiveFlag) //only delete if it is not an active record
                {
                    if (item.StatutoryDeductionTypeCode == "BB")
                        Common.ServiceWrapper.StatutoryDeduction.DeleteBarbados(item);
                    else if (item.StatutoryDeductionTypeCode == "STLU")
                        Common.ServiceWrapper.StatutoryDeduction.DeleteSaintLucia(item);
                    else if (item.StatutoryDeductionTypeCode == "TT")
                        Common.ServiceWrapper.StatutoryDeduction.DeleteTrinidad(item);
                    else if (item.StatutoryDeductionTypeCode == "JM")
                        Common.ServiceWrapper.StatutoryDeduction.DeleteJamaica(item);
                    else
                        Common.ServiceWrapper.StatutoryDeduction.Delete(item);

                    Data.Remove(item.Key);
                    StatutoryDeductionCanadian.DataBind();
                    StatutoryDeductionGrid.Rebind();
                }
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void StatutoryDeductionGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            //get index of item being edited, will only be 1 item
            GridIndexCollection rows = StatutoryDeductionGrid.ItemIndexes;
            RowIndex = Convert.ToInt16(rows[0]);

            GetSelectedStatutoryDeduction();
        }
        protected void StatutoryDeductionGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            StatutoryDeductionGrid.DataSource = Data;
        }
        protected void StatutoryDeductionGrid_DataBound(object sender, EventArgs e)
        {
            //if we arrived here because the CancelClicked variable was set to true, then set it back to false
            if (CancelClicked)
                CancelClicked = false;

            if (StatutoryDeductionGrid.Items.Count > 0)
            {
                if (!_updatePerformed)
                {
                    StatutoryDeductionGrid.Items[0].Selected = true;
                    RowIndex = 0;
                }
                else
                {
                    StatutoryDeductionGrid.Items[RowIndex].Selected = true;
                    _updatePerformed = false;
                }

                GetSelectedStatutoryDeduction();
            }
        }
        protected void StatutoryDeductionToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            RadToolBarButton button = ((RadToolBarButton)e.Item);

            if (button.CommandName.Equals("AddCanadian"))
            {
                RadMultiPage1.FindPageViewByID("CanadianPageView").Selected = true;
                StatutoryDeductionCanadian.ChangeModeInsert();
            }
            else if (button.CommandName.Equals("AddTaxOverride"))
            {
                RadMultiPage1.FindPageViewByID("TaxOverridePageView").Selected = true;
                StatutoryDeductionTaxOverride.ChangeModeInsert();
            }
            else if (button.CommandName.Equals("AddStLucia"))
            {
                RadMultiPage1.FindPageViewByID("StLuciaPageView").Selected = true;
                StatutoryDeductionStLucia.ChangeModeInsert();
            }
            else if (button.CommandName.Equals("AddBarbados"))
            {
                RadMultiPage1.FindPageViewByID("BarbadosPageView").Selected = true;
                StatutoryDeductionBarbados.ChangeModeInsert();
            }
            else if (button.CommandName.Equals("AddTrinidad"))
            {
                RadMultiPage1.FindPageViewByID("TrinidadPageView").Selected = true;
                StatutoryDeductionTrinidad.ChangeModeInsert();
            }
            else if (button.CommandName.Equals("AddJamaica"))
            {
                RadMultiPage1.FindPageViewByID("JamaicaPageView").Selected = true;
                StatutoryDeductionJamaica.ChangeModeInsert();
            }
        }
        void LoadableControl_StatutoryDeductionDetailToolbarButtonClick(object sender, RadToolBarEventArgs e)
        {
            switch (((RadToolBarButton)e.Item).CommandName.ToLower())
            {
                case "cancel":
                    {
                        CancelClicked = true;
                        break;
                    }
                case "insert":
                case "performinsert":
                case "update":
                case "edit":
                case "initinsert":
                    break;
            }
        }
        protected void StatutoryDeductionGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "addcanadian":
                case "addtaxoverride":
                case "addstlucia":
                case "addbarbados":
                case "addtrinidad":
                    break;
            }
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void BusinessNumberId_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateBusinessNumber((ICodeControl)sender);
        }
        #endregion

        #region security handlers
        protected void AddDropDown_PreRender(object sender, EventArgs e) { ((WLPToolBarDropDown)sender).Visible = ((WLPToolBarDropDown)sender).Visible && AddFlag; }
        protected void AddCanadian_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableAddCanadianButton; }
        protected void AddTaxOverride_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableAddTaxOverrideButton; }
        protected void AddStLucia_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableAddStLuciaButton; }
        protected void AddBarbados_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableAddBarbadosButton; }
        protected void AddTrinidad_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableAddTrinidadButton; }
        protected void AddJamaica_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableAddJamaicaButton; }
        #endregion

        #region override
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            InitializeControls();
        }
        #endregion
    }
}