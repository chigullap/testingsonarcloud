﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HumanResources/Employee.Master" AutoEventWireup="true" CodeBehind="EmployeePaySlipPage.aspx.cs" Inherits="WorkLinks.Payroll.PaySlip.EmployeePaySlipPage" %>
<%@ Register src="EmployeePaySlipControl.ascx" tagname="EmployeePaySlipControl" tagprefix="uc1" %>
<asp:Content ID="Content1" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
        <uc1:EmployeePaySlipControl ID="EmployeePaySlipControl" runat="server" />        
</asp:Content>
