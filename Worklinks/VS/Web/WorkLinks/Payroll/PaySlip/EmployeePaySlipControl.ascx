﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeePaySlipControl.ascx.cs" Inherits="WorkLinks.Payroll.PaySlip.EmployeePaySlipControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPToolBar ID="PayrollProcessSummaryToolBar" runat="server" Width="100%" OnClientLoad="PayrollProcessSummaryToolBar_init">
    <Items>
        <wasp:WLPToolBarButton OnPreRender="EftToolBar_PreRender" Text="*Eft*" onclick="OpenEft()" CommandName="eft" ResourceName="Eft" />
        <wasp:WLPToolBarButton OnPreRender="ExportCheque_PreRender" Text="*ExportCheque*" onclick="OpenExportCheque()" CommandName="exportCheque" ResourceName="ExportCheque" />
    </Items>
</wasp:WLPToolBar>

<table style="width: 100%; height: 88vh; border-spacing: 0; border-collapse: collapse; border: 0">
    <tr>
        <td style="width:20%; height:88vh">
            <wasp:WLPGrid
                ID="PayrollProcessSummaryGrid"
                runat="server"
                GridLines="None"
                Height="100%"
                AutoAssignModifyProperties="true">

                <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="false">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                    <ClientEvents OnRowClick="onRowClick" OnGridCreated="onGridCreated" OnCommand="onCommand" />
                </ClientSettings>

                <MasterTableView
                    ClientDataKeyNames="PayrollProcessId, PayrollProcessGroupCode"
                    AutoGenerateColumns="false"
                    DataKeyNames="PayrollProcessId, PeriodYear, Period"
                    CommandItemDisplay="Top"
                    AllowSorting="false">

                    <CommandItemTemplate></CommandItemTemplate>

                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="PeriodYear, Period, ChequeDate" SortOrder="Descending" />
                    </SortExpressions>

                    <Columns>
                        <wasp:GridBoundControl DataField="Period" LabelText="*Period*" SortExpression="Period" UniqueName="Period" ResourceName="Period" />
                        <wasp:GridDateTimeControl DataField="CutoffDate" LabelText="*CutoffDate*" SortExpression="CutoffDate" UniqueName="CutoffDate" ResourceName="CutoffDate" />
                        <wasp:GridDateTimeControl DataField="ChequeDate" LabelText="*ChequeDate*" SortExpression="ChequeDate" UniqueName="ChequeDate" ResourceName="ChequeDate" />
                    </Columns>
                </MasterTableView>

                <HeaderContextMenu EnableAutoScroll="true" />
            </wasp:WLPGrid>
        </td>
        <td style="width:80%; height:88vh">
            <telerik:RadSplitter ID="RadSplitter" runat="server" Orientation="Horizontal" Height="100%" Width="100%" VisibleDuringInit="false" OnClientLoad="onClientLoad" BorderSize="0">
                <telerik:RadPane ID="RadPane" runat="server" Width="100%" />
            </telerik:RadSplitter>
        </td>
    </tr>
</table>

<wasp:WLPWindowManager
    ClientIDMode="Static"
    ID="PaySlipWindows"
    Width="700"
    Height="400"
    VisibleStatusbar="false"
    Behaviors="Close"
    DestroyOnClose="true"
    Modal="true"
    Overlay="true"
    runat="server"
    ShowContentDuringLoad="false"
    OnClientShow="showContentForIE">
</wasp:WLPWindowManager>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var payrollProcessId;
        var periodYear;
        var period;
        var payrollProcessGroupCode;
        var employeeNumber = '<%= EmployeeNumber %>';
        var employeeId = '<%= EmployeeId %>';
        var rowSelected;

        function showContentForIE(wnd) {
            if ($telerik.isIE)
                wnd.view.onUrlChanged();
        }

        function clear() {
            payrollProcessId = null;
            periodYear = null;
            period = null;
            payrollProcessGroupCode = null;
        }

        function PayrollProcessSummaryToolBar_init(sender) {
            initializeControls();
        }

        function initializeControls() {
            enableButtons(false);

            payrollProcessId = null;
            periodYear = null;
            period = null;
        }

        function onGridCreated(sender, eventArgs) {
            var grid = $find('<%= PayrollProcessSummaryGrid.ClientID %>');
            var masterTable = grid.get_masterTableView();
            var selectedRow = masterTable.get_selectedItems();

            if (selectedRow.length == 1) {
                rowSelected = true;
                payrollProcessId = selectedRow[0].getDataKeyValue('PayrollProcessId');
                periodYear = selectedRow[0].getDataKeyValue('PeriodYear');
                period = selectedRow[0].getDataKeyValue('Period');
                payrollProcessGroupCode = selectedRow[0].getDataKeyValue('PayrollProcessGroupCode');
                enableButtons(true);
            }
        }

        function onRowClick(sender, eventArgs) {
            payrollProcessId = eventArgs.getDataKeyValue('PayrollProcessId');
            periodYear = eventArgs.getDataKeyValue('PeriodYear');
            period = eventArgs.getDataKeyValue('Period');
            payrollProcessGroupCode = eventArgs.getDataKeyValue('PayrollProcessGroupCode');

            rowSelected = true;

            enableButtons(true);
            processClick();
        }

        function onClientLoad(sender, eventArgs) {
            if (rowSelected)
                processClick();
        }

        function processClick() {
            var radPane = $find("<%= RadPane.ClientID %>");
            radPane.set_contentUrl(getUrl(String.format('/Report/EmployeePaySlip/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}', payrollProcessId, employeeNumber, "PDF", 'x', 'x', 'x', 'x', employeeId)));
        }

        function enableButtons(enable) {
            var toolbar = $find('<%= PayrollProcessSummaryToolBar.ClientID %>');
            var eftButton = toolbar.findButtonByCommandName('eft');
            var ceridianButton = toolbar.findButtonByCommandName('ceridian');
            var exportChequeButton = toolbar.findButtonByCommandName('exportCheque');

            if (eftButton != null) eftButton.set_enabled(enable);
            if (ceridianButton != null) ceridianButton.set_enabled(enable);
            if (exportChequeButton != null) exportChequeButton.set_enabled(enable);
        }

        function getRadWindow() {
            var popup = null;

            if (window.radWindow)
                popup = window.radWindow;
            else if (window.frameElement.radWindow)
                popup = window.frameElement.radWindow;

            return popup;
        }

        function OpenExportCheque() {
            if (payrollProcessId != null)
                openWindowWithManager('PaySlipWindows', String.format('/Payroll/ExportCheque/ExportCheque/{0}/{1}', payrollProcessId, employeeNumber), false);
        }

        function OpenEft() {
            if (payrollProcessId != null)
                __doPostBack('<%= this.ClientID %>', String.format('eftPayrollProcessId={0}', payrollProcessId));
        }

        function onCommand(sender, args) {
            if (args.get_commandName() == 'Page') {
                payrollProcessId = null;
                initializeControls();
            }
        }
    </script>
</telerik:RadScriptBlock>