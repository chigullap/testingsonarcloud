﻿using System;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.PaySlip
{
    public partial class EmployeePaySlipControl : WLP.Web.UI.WLPUserControl
    {
        public enum SearchLocationType
        {
            Employee,
            Payroll,
        }

        #region properties
        protected long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        protected String EmployeeNumber { get { return Convert.ToString(Page.RouteData.Values["employeeNumber"]); } }
        private String LastName { get { return Page.RouteData.Values["lastName"].ToString(); } }
        private String FirstName { get { return Page.RouteData.Values["firstName"].ToString(); } }
        public SearchLocationType SearchLocation { get; set; }
        protected bool EnableEftButton { get { return Common.Security.RoleForm.EFTButton.ViewFlag; } }
        protected bool EnableCeridianButton { get { return Common.Security.RoleForm.PayrollCeridianExportButton.ViewFlag; } }
        protected bool EnableExportChequeButton { get { return Common.Security.RoleForm.ExportCheque.ViewFlag; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.PayrollEmployeeSearchPaySlip.ViewFlag);

            WireEvents();

            this.Page.Title = String.Format(GetGlobalResourceObject("PageTitle", "PaySlips").ToString(), LastName, FirstName);

            Search(new PayrollProcessCriteria() { EmployeeId = EmployeeId, SecurityOverrideFlag = false });

            Page.ClientScript.GetPostBackEventReference(this, "");
            String args = Request["__EVENTARGUMENT"];

            //this block is only used when the EFT button is clicked
            if (args != null && args.Length > 20 && args.Substring(0, 20).ToLower().Equals("eftpayrollprocessid="))
            {
                long payrollprocessid = Convert.ToInt64(args.Substring(20));
                Response.Redirect(String.Format("~/Payroll/PayrollProcess/PayrollProcessSearchPage.aspx?eftPayrollProcessId={0}&employeeNumber={1}", payrollprocessid, EmployeeNumber));
            }
        }
        protected void WireEvents()
        {
            PayrollProcessSummaryGrid.NeedDataSource += new Telerik.Web.UI.GridNeedDataSourceEventHandler(PayrollProcessSummaryGrid_NeedDataSource);
            PayrollProcessSummaryGrid.PreRender += new EventHandler(PayrollProcessSummaryGrid_PreRender);
        }
        public void Search(PayrollProcessCriteria criteria)
        {
            PayrollProcessSummaryCollection collection = new PayrollProcessSummaryCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetPayrollProcessSummary(criteria));
            Data = collection;

            PayrollProcessSummaryGrid.Rebind();
        }
        #endregion

        #region event handlers
        protected void PayrollProcessSummaryGrid_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            PayrollProcessSummaryGrid.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void PayrollProcessSummaryGrid_PreRender(object sender, EventArgs e)
        {
            if (PayrollProcessSummaryGrid.MasterTableView.Items.Count > 0)
                PayrollProcessSummaryGrid.MasterTableView.Items[0].Selected = true;
        }
        #endregion

        #region security handlers
        protected void EftToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableEftButton; }
        protected void CeridianToolBar_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableCeridianButton; }
        protected void ExportCheque_PreRender(object sender, EventArgs e) { ((WLPToolBarButton)sender).Visible = ((WLPToolBarButton)sender).Visible && EnableExportChequeButton; }
        #endregion
    }
}