﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.Paycode
{
    public partial class EmployeePaycodeIncome : WLP.Web.UI.WLPUserControl
    {
        public event EmployeeDetailToolbarButtonClickEventHandler EmployeeDetailToolbarButtonClick;

        #region fields
        private const String _employeePaycodeTypeCode = "1";
        private String _paycodeDataValidateMsg = "";
        #endregion

        #region properties
        public EmployeePaycode EmployeePaycode
        {
            get
            {
                EmployeePaycode paycode = null;

                if (Data != null && Data.Count > 0)
                    paycode = (EmployeePaycode)Data[0];

                return paycode;
            }
        }
        public new IDataItemCollection<IDataItem> Data
        {
            get
            {
                return base.Data;
            }
            set
            {
                if (IsInsertMode)
                {
                    foreach (EmployeePaycode paycode in value)
                        paycode.PaycodeTypeCode = _employeePaycodeTypeCode;
                }

                base.Data = value;
            }
        }
        public String PaycodeDataValidateMsg
        {
            get { return _paycodeDataValidateMsg; }
            set { _paycodeDataValidateMsg = value; }
        }
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        public long EmployeePaycodeId { get { return EmployeePaycode.EmployeePaycodeId; } }
        public bool IsGlobalEmployeePaycode { get { return EmployeePaycode.GlobalEmployeePaycodeFlag; } }
        public bool IsViewMode { get { return EmployeePaycodeIncomeView.CurrentMode == FormViewMode.ReadOnly; } }
        public bool IsInsertMode { get { return EmployeePaycodeIncomeView.CurrentMode == FormViewMode.Insert; } }
        public bool IsEditMode { get { return EmployeePaycodeIncomeView.CurrentMode == FormViewMode.Edit; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeePaycode.UpdateFlag; } }
        public String DropDownTreeDefaultMessage { get { return String.Format(GetGlobalResourceObject("PageContent", "ChooseADepartment").ToString()); } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        public void ChangeModeInsert()
        {
            EmployeePaycodeIncomeView.ChangeMode(FormViewMode.Insert);
            EmployeePaycodeIncomeView.DataBind();
        }
        #endregion

        #region event handlers
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void PaycodeCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulatePaycodes((ICodeControl)sender, LanguageCode);
        }
        protected void PaycodeCode_DataBinding(object sender, EventArgs e)
        {
            if (sender is ComboBoxControl)
            {
                bool leaveSelected = false;
                EmployeePaycodeModule temp = (EmployeePaycodeModule)this.Parent.Parent.Parent.Parent;

                if (temp.IsDataExternallyLoaded)
                {
                    if (IsInsertMode)
                        Common.CodeHelper.PopulateComboBoxWithTypeForWizardRemovingTypesInUse((ICodeControl)sender, temp.DataItemCollection, LanguageCode, temp.WizardItemName, leaveSelected, _employeePaycodeTypeCode);
                    else
                    {
                        leaveSelected = true;
                        Common.CodeHelper.PopulateComboBoxWithTypeForWizardRemovingTypesInUse((ICodeControl)sender, temp.DataItemCollection, LanguageCode, temp.WizardItemName, leaveSelected, _employeePaycodeTypeCode);
                    }
                }
                else
                {
                    if (IsInsertMode)
                        Common.CodeHelper.PopulateComboBoxWithRecurringIncomePaycodesRemovingPaycodesInUse((ComboBoxControl)sender, _employeePaycodeTypeCode, LanguageCode, EmployeeId, true);
                    else
                        Common.CodeHelper.PopulateComboBoxWithPaycodesByType((ComboBoxControl)sender, _employeePaycodeTypeCode);
                }

                BindCodePayCodeTypeCd(sender);
            }
        }
        private void BindCodePayCodeTypeCd(object sender)
        {
            if (sender is ComboBoxControl)
            {
                ComboBoxControl paycodeCombo = (ComboBoxControl)sender;
                WLPFormView formItem = paycodeCombo.BindingContainer as WLPFormView;
                ComboBoxControl paycodeTypeCombo = (ComboBoxControl)formItem.FindControl("CodePaycodeTypeCd");

                paycodeTypeCombo.DataSource = Common.ServiceWrapper.CodeClient.GetPaycodeTypeCode(paycodeCombo.SelectedValue);
                paycodeTypeCombo.DataBind();

                if (formItem.DataItem != null && formItem.DataItem is CodePaycode)
                    paycodeTypeCombo.Value = 1;
            }
        }
        protected void EmployeePaycodeIncomeView_DataBound(object sender, EventArgs e)
        {
            RadDropDownTree organizationUnitDropDownTree = (RadDropDownTree)EmployeePaycodeIncomeView.FindControl("OrganizationUnitDropDownTree");

            if (organizationUnitDropDownTree != null)
            {
                organizationUnitDropDownTree.DataSource = Common.ServiceWrapper.HumanResourcesClient.GetOrganizationUnitAssociationTree(LanguageCode, null);
                organizationUnitDropDownTree.DefaultMessage = DropDownTreeDefaultMessage;
                organizationUnitDropDownTree.DataBind();

                if (!IsInsertMode && !IsViewMode)
                {
                    EmployeePaycode employeePaycode = ((EmployeePaycodeCollection)Data)[0];

                    //asp hidden field to hold org hierarchy
                    HiddenField orgHierarchy = (HiddenField)EmployeePaycodeIncomeView.FindControl("OrganizationUnitDropDownValues");
                    //if we have values, remove the leading / so the javascript can split the array properly
                    if (orgHierarchy != null)
                        orgHierarchy.Value = employeePaycode.OrganizationUnit != null ? employeePaycode.OrganizationUnit.Substring(1) : null;
                }
            }
        }
        protected void EmployeePaycodeIncomeView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            OnNeedDataSource(e);
            EmployeePaycodeIncomeView.DataSource = Data;
        }
        protected virtual void OnEmployeeDetailToolbarButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (EmployeeDetailToolbarButtonClick != null)
                EmployeeDetailToolbarButtonClick(sender, e);
        }
        protected void EmployeeDetailToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            OnEmployeeDetailToolbarButtonClick(sender, e);
        }
        #endregion

        #region handles updates
        protected void GetOrganizationUnit(EmployeePaycode item)
        {
            HiddenField hiddenOrg = (HiddenField)EmployeePaycodeIncomeView.FindControl("OrganizationUnitDropDownValues");
            if (hiddenOrg != null)
            {
                if (hiddenOrg.Value.Length > 0 && !hiddenOrg.Value.StartsWith("/"))
                    hiddenOrg.Value = "/" + hiddenOrg.Value;

                item.OrganizationUnit = (hiddenOrg.Value == String.Empty) ? null : hiddenOrg.Value;
            }
        }
        protected void EmployeePaycodeIncomeView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            if (Common.ServiceWrapper.HumanResourcesClient.ValidatePaycodeEntryData((EmployeePaycode)e.DataItem))
            {
                PaycodeDataValidateMsg = "";
                GetOrganizationUnit((EmployeePaycode)e.DataItem);
                OnInserting(e);
            }
            else
            {
                PaycodeDataValidateMsg = String.Format("{0}", GetGlobalResourceObject("ErrorMessages", "PaycodeDataValidate"));
                e.Cancel = true;
            }
        }
        protected void EmployeePaycodeIncomeView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            if (Common.ServiceWrapper.HumanResourcesClient.ValidatePaycodeEntryData((EmployeePaycode)e.DataItem))
            {
                PaycodeDataValidateMsg = "";
                GetOrganizationUnit((EmployeePaycode)e.DataItem);
                OnUpdating(e);
            }
            else
            {
                PaycodeDataValidateMsg = String.Format("{0}", GetGlobalResourceObject("ErrorMessages", "PaycodeDataValidate"));
                e.Cancel = true;
            }
        }
        #endregion
    }
}