﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeePaycodeModule.ascx.cs" Inherits="WorkLinks.Payroll.Paycode.EmployeePaycodeModule" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="EmployeePaycodeDeduction.ascx" TagName="EmployeePaycodeDeduction" TagPrefix="uc1" %>
<%@ Register Src="EmployeePaycodeBenefit.ascx" TagName="EmployeePaycodeBenefit" TagPrefix="uc2" %>
<%@ Register Src="EmployeePaycodeIncome.ascx" TagName="EmployeePaycodeIncome" TagPrefix="uc3" %>

<wasp:WLPAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="PaycodePanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="PaycodePanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</wasp:WLPAjaxManagerProxy>

<asp:Panel runat="server" ID="PaycodePanel">
    <wasp:WLPGrid
        ID="EmployeePaycodeGrid"
        runat="server"
        Height="300px"
        GridLines="None"
        AutoAssignModifyProperties="true"
        AutoGenerateColumns="false"
        OnDataBound="EmployeePaycodeGrid_DataBound"
        OnDeleteCommand="EmployeePaycodeGrid_DeleteCommand"
        OnInsertCommand="EmployeePaycodeGrid_InsertCommand"
        OnNeedDataSource="EmployeePaycodeGrid_NeedDataSource"
        OnUpdateCommand="EmployeePaycodeGrid_UpdateCommand"
        OnItemCommand="EmployeePaycodeGrid_ItemCommand"
        OnItemDataBound="EmployeePaycodeGrid_ItemDataBound"
        OnSelectedIndexChanged="EmployeePaycodeGrid_SelectedIndexChanged">

        <ClientSettings EnablePostBackOnRowClick="true">
            <Selecting AllowRowSelect="true" />
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
        </ClientSettings>

        <MasterTableView CommandItemDisplay="Top" DataKeyNames="Key">
            <RowIndicatorColumn>
                <HeaderStyle Width="20px" />
            </RowIndicatorColumn>
            <ExpandCollapseColumn>
                <HeaderStyle Width="20px" />
            </ExpandCollapseColumn>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="GlobalEmployeePaycodeFlag, PaycodeTypeCode, PaycodeCode" SortOrder="Ascending" />
            </SortExpressions>

            <CommandItemTemplate>
                <wasp:WLPToolBar ID="EmployeePaycodeToolBar" runat="server" AutoPostBack="true" Width="100%" OnButtonClick="EmployeePaycodeToolBar_ButtonClick" Enabled="<%# IsViewMode && AddFlag %>">
                    <Items>
                        <wasp:WLPToolBarDropDown runat="server" ImageUrl="~/App_Themes/Default/Add.gif" ResourceName="Add" OnPreRender="AddDropDown_PreRender">
                            <Buttons>
                                <wasp:WLPToolBarButton runat="server" Text="**Income**" ResourceName="AddIncome" CommandName="AddIncome" Visible='<%# IsRecurringIncome %>' />
                                <wasp:WLPToolBarButton runat="server" Text="**Deduction**" ResourceName="AddDeduction" CommandName="AddDeduction" Visible="true" />
                                <wasp:WLPToolBarButton runat="server" Text="**Benefit**" ResourceName="AddBenefit" CommandName="AddBenefit" Visible="true" />
                            </Buttons>
                        </wasp:WLPToolBarDropDown>
                    </Items>
                </wasp:WLPToolBar>
            </CommandItemTemplate>

            <Columns>
                <wasp:GridKeyValueControl DataField="PaycodeCode" LabelText="**Paycode" OnNeedDataSource="PaycodeCode_NeedDataSource" Type="PaycodeCode" ResourceName="PaycodeCode">
                    <HeaderStyle Width="20%" />
                </wasp:GridKeyValueControl>
                <wasp:GridKeyValueControl DataField="PaycodeTypeCode" LabelText="**PaycodeTypeCode**" OnNeedDataSource="Code_NeedDataSource" Type="PaycodeTypeCode" ResourceName="PaycodeTypeCode">
                    <HeaderStyle Width="10%" />
                </wasp:GridKeyValueControl>
                <wasp:GridNumericControl DataField="AmountRate" LabelText="**AmountRate**" DataFormatString="{0:$###,##0.00}" SortExpression="AmountRate" UniqueName="AmountRate" DataType="System.Int64" ResourceName="AmountRate" />
                <wasp:GridNumericControl DataField="AmountPercentage" LabelText="**AmountPercentage**" DataFormatString="{0:#####0.0000}" SortExpression="AmountPercentage" UniqueName="AmountPercentage" DataType="System.Int64" ResourceName="AmountPercentage" />
                <wasp:GridNumericControl DataField="YearlyMaximum" LabelText="**YearlyMaximum**" DataFormatString="{0:$###,##0.00}" SortExpression="YearlyMaximum" UniqueName="YearlyMaximum" DataType="System.Int64" ResourceName="YearlyMaximum" />
                <wasp:GridNumericControl LabelText="" ResourceName="" DataField="EmptyColumnText">
                    <HeaderStyle Width="1%" />
                </wasp:GridNumericControl>
                <wasp:GridBoundControl DataField="OrganizationUnitDescription" LabelText="**OrganizationUnit**" SortExpression="OrganizationUnitDescription" UniqueName="OrganizationUnitDescription" ResourceName="OrganizationUnitDescription">
                    <HeaderStyle Width="30%" />
                </wasp:GridBoundControl>
                <wasp:GridCheckBoxControl DataField="EligibleByStatus" LabelText="**EligibleByStatus**" SortExpression="EligibleByStatus" UniqueName="EligibleByStatus" ResourceName="EligibleByStatus">
                    <HeaderStyle Width="5%" />
                </wasp:GridCheckBoxControl>
                <wasp:GridCheckBoxControl DataField="GlobalEmployeePaycodeFlag" LabelText="**GlobalEmployeePaycodeFlag**" SortExpression="GlobalEmployeePaycodeFlag" UniqueName="GlobalEmployeePaycodeFlag" ResourceName="GlobalEmployeePaycodeFlag">
                    <HeaderStyle Width="5%" />
                </wasp:GridCheckBoxControl>
                <wasp:GridButtonControl UniqueName="deleteButton" ButtonType="ImageButton" ImageUrl="~/App_Themes/Default/Delete.gif" CommandName="Delete" ConfirmDialogType="Classic" ConfirmTitle="Delete" ConfirmText="<%$ Resources:WarningMessages, DeleteNotificationWarning %>">
                    <HeaderStyle Width="5%" />
                </wasp:GridButtonControl>
            </Columns>
        </MasterTableView>

    </wasp:WLPGrid>

    <telerik:RadMultiPage ID="RadMultiPage1" runat="server" Height="350px">
        <telerik:RadPageView ID="DeductionPageView" runat="server">
            <uc1:EmployeePaycodeDeduction ID="EmployeePaycodeDeduction" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="BenefitPageView" runat="server">
            <uc2:EmployeePaycodeBenefit ID="EmployeePaycodeBenefit" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="IncomePageView" runat="server">
            <uc3:EmployeePaycodeIncome ID="EmployeePaycodeIncome" runat="server" />
        </telerik:RadPageView>
    </telerik:RadMultiPage>
</asp:Panel>