﻿using System;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.Wizard;

namespace WorkLinks.Payroll.Paycode
{
    public delegate void EmployeeDetailToolbarButtonClickEventHandler(object sender, RadToolBarEventArgs e);

    public partial class EmployeePaycodeModule : WizardUserControl
    {
        #region fields
        private int _rowIndex = -1;
        private string _rowIndexKey = "RowIndexKey";
        private bool _updatePerformed = false;
        #endregion

        #region properties
        private long? SelectedEmployeePaycodeId
        {
            get
            {
                long? payCodeId = null;

                if (EmployeePaycodeGrid.SelectedValue != null)
                    payCodeId = Convert.ToInt64(EmployeePaycodeGrid.SelectedValue);

                return payCodeId;
            }
        }
        private long EmployeeId
        {
            get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); }
        }
        public bool IsViewMode
        {
            get { return EmployeePaycodeBenefit.IsViewMode && EmployeePaycodeIncome.IsViewMode && EmployeePaycodeDeduction.IsViewMode; }
        }
        public bool AddFlag
        {
            get { return Common.Security.RoleForm.EmployeePaycode.AddFlag; }
        }
        public bool DeleteFlag
        {
            get { return Common.Security.RoleForm.EmployeePaycode.DeleteFlag; }
        }
        private string LastName
        {
            get { return Page.RouteData.Values["lastName"].ToString(); }
        }
        private string FirstName
        {
            get { return Page.RouteData.Values["firstName"].ToString(); }
        }
        public bool IsInsertMode
        {
            get { return EmployeePaycodeBenefit.IsInsertMode || EmployeePaycodeDeduction.IsInsertMode || EmployeePaycodeIncome.IsInsertMode; }
        }
        public bool InsertComplete { get; set; }
        public bool CancelClicked { get; set; }
        public bool IsRecurringIncome
        {
            get { return Common.CodeHelper.CheckRecurringIncomeCode(); }
        }
        protected EmployeePaycode CurrentEmployeePaycode
        {
            get
            {
                EmployeePaycode paycode = null;

                if (IsInsertMode && !InsertComplete)
                    paycode = new EmployeePaycode() { PaycodeCode = String.Empty, EmployeeId = EmployeeId };
                else
                {
                    if (EmployeePaycodeGrid.SelectedValue != null)
                        paycode = (EmployeePaycode)DataItemCollection[EmployeePaycodeGrid.SelectedValue.ToString()];
                }

                return paycode;
            }
        }
        protected EmployeePaycodeCollection CurrentEmployeePaycodeCollection
        {
            get
            {
                EmployeePaycodeCollection collection = new EmployeePaycodeCollection();

                if (CurrentEmployeePaycode != null)
                    collection.Load(CurrentEmployeePaycode);

                return collection;
            }
        }
        public int RowIndex
        {
            get
            {
                if (_rowIndex == -1)
                {
                    Object obj = ViewState[_rowIndexKey];
                    if (obj != null)
                        _rowIndex = (int)obj;
                }

                return _rowIndex;
            }
            set
            {
                _rowIndex = value;
                ViewState[_rowIndexKey] = _rowIndex;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            //does the user have access rights?
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EmployeePaycode.ViewFlag);

            if (!IsPostBack)
            {
                //does the user have rights to see this employee?
                if (!IsDataExternallyLoaded)
                    SetAuthorized(Common.ServiceWrapper.HumanResourcesClient.GetEmployee(new EmployeeCriteria() { EmployeeId = EmployeeId }).Count > 0);

                LoadEmployeePaycode(EmployeeId);
            }

            if (!IsDataExternallyLoaded)
                this.Page.Title = String.Format(GetGlobalResourceObject("PageTitle", "EmployeePaycode").ToString(), LastName, FirstName);

            WireEvents();
        }
        private void LoadEmployeePaycode(long employeeId)
        {
            if (!IsDataExternallyLoaded)
                DataItemCollection = EmployeePaycodeCollection.ConvertCollection(Common.ServiceWrapper.HumanResourcesClient.GetEmployeePaycode(employeeId));
            else
                EmployeePaycodeGrid.Rebind();
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            InitializeControls();
        }
        protected void InitializeControls()
        {
            EmployeePaycodeGrid.Enabled = IsViewMode;

            //this code makes sure the RadToolbar is set to the proper status before/during/after update/insert operations
            GridCommandItem cmdItem = (GridCommandItem)EmployeePaycodeGrid.MasterTableView.GetItems(GridItemType.CommandItem)[0];
            RadToolBar toolBar = (RadToolBar)cmdItem.FindControl("EmployeePaycodeToolBar");
            toolBar.Enabled = IsViewMode;
        }
        private void WireEvents()
        {
            EmployeePaycodeDeduction.NeedDataSource += new NeedDataSourceEventHandler(LoadableControl_NeedDataSource);
            EmployeePaycodeDeduction.Inserting += new InsertingEventHandler(LoadableControl_Inserting);
            EmployeePaycodeDeduction.Updating += new UpdatingEventHandler(ILoadableControl_Updating);
            EmployeePaycodeDeduction.EmployeeDetailToolbarButtonClick += new EmployeeDetailToolbarButtonClickEventHandler(LoadableControl_EmployeeDetailToolbarButtonClick);

            EmployeePaycodeIncome.NeedDataSource += new NeedDataSourceEventHandler(LoadableControl_NeedDataSource);
            EmployeePaycodeIncome.Inserting += new InsertingEventHandler(LoadableControl_Inserting);
            EmployeePaycodeIncome.Updating += new UpdatingEventHandler(ILoadableControl_Updating);
            EmployeePaycodeIncome.EmployeeDetailToolbarButtonClick += new EmployeeDetailToolbarButtonClickEventHandler(LoadableControl_EmployeeDetailToolbarButtonClick);

            EmployeePaycodeBenefit.NeedDataSource += new NeedDataSourceEventHandler(LoadableControl_NeedDataSource);
            EmployeePaycodeBenefit.Inserting += new InsertingEventHandler(LoadableControl_Inserting);
            EmployeePaycodeBenefit.Updating += new UpdatingEventHandler(ILoadableControl_Updating);
            EmployeePaycodeBenefit.EmployeeDetailToolbarButtonClick += new EmployeeDetailToolbarButtonClickEventHandler(LoadableControl_EmployeeDetailToolbarButtonClick);
        }
        private void GetSelectedEmployeePaycode()
        {
            if (EmployeePaycodeGrid.SelectedItems.Count > 0)
            {
                switch (CurrentEmployeePaycode.PaycodeTypeCode)
                {
                    case "1":
                        EmployeePaycodeIncome.Data = CurrentEmployeePaycodeCollection;
                        EmployeePaycodeIncome.DataBind();
                        RadMultiPage1.FindPageViewByID("IncomePageView").Selected = true;
                        break;
                    case "2":
                        EmployeePaycodeBenefit.Data = CurrentEmployeePaycodeCollection;
                        EmployeePaycodeBenefit.DataBind();
                        RadMultiPage1.FindPageViewByID("BenefitPageView").Selected = true;
                        break;
                    case "3":
                        EmployeePaycodeDeduction.Data = CurrentEmployeePaycodeCollection;
                        EmployeePaycodeDeduction.DataBind();
                        RadMultiPage1.FindPageViewByID("DeductionPageView").Selected = true;
                        break;
                }
            }
        }
        protected bool UpdatePaycode(EmployeePaycode paycode)
        {
            bool noError = true;

            try
            {
                Common.ServiceWrapper.HumanResourcesClient.UpdateEmployeePaycode(paycode).CopyTo(paycode);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    noError = false;
                }
            }
            catch (Exception ex)
            {
                noError = false;
                throw ex;
            }

            return noError;
        }
        protected EmployeePaycode InsertPaycode(EmployeePaycode paycode)
        {
            return Common.ServiceWrapper.HumanResourcesClient.InsertEmployeePaycode(paycode);
        }
        #endregion

        #region event handlers
        void LoadableControl_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            if (CancelClicked && EmployeePaycodeGrid.Items.Count > 0)
                EmployeePaycodeGrid_DataBound(null, null);
            else
            {
                if (((WLP.Web.UI.WLPUserControl)sender).ID == "EmployeePaycodeIncome")
                    EmployeePaycodeIncome.Data = CurrentEmployeePaycodeCollection;
                else if (((WLP.Web.UI.WLPUserControl)sender).ID == "EmployeePaycodeBenefit")
                    EmployeePaycodeBenefit.Data = CurrentEmployeePaycodeCollection;
                else if (((WLP.Web.UI.WLPUserControl)sender).ID == "EmployeePaycodeDeduction")
                    EmployeePaycodeDeduction.Data = CurrentEmployeePaycodeCollection;
            }
        }
        void ILoadableControl_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            _updatePerformed = true;
            EmployeePaycode paycode = (EmployeePaycode)e.DataItem;

            if (IsDataExternallyLoaded)
            {
                paycode.CopyTo((EmployeePaycode)DataItemCollection[paycode.Key]);
                ItemChangedEventArgs args = new ItemChangedEventArgs() { Items = DataItemCollection };
                OnItemChanged(args);
            }
            else
            {
                UpdatePaycode(paycode);
                //paycode.CopyTo(CurrentEmployeePaycode);
            }

            LoadEmployeePaycode(EmployeeId);
            EmployeePaycodeGrid.Rebind();
        }
        void LoadableControl_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            EmployeePaycode paycode = (EmployeePaycode)e.DataItem;
            paycode.EmployeeId = EmployeeId;

            if (paycode.GarnishmentVendorId != null && paycode.GarnishmentVendorId > 0)
                paycode.GarnishmentFlag = true;

            if (IsDataExternallyLoaded)
            {
                if (DataItemCollection.Count > 0)
                    paycode.EmployeePaycodeId = ((EmployeePaycode)DataItemCollection[DataItemCollection.Count - 1]).EmployeePaycodeId - 1;

                ItemChangedEventArgs args = new ItemChangedEventArgs() { Items = DataItemCollection };
                ((EmployeePaycodeCollection)DataItemCollection).Add(paycode);
                OnItemChanged(args);
            }
            else
                ((DataItemCollection<EmployeePaycode>)DataItemCollection).Add(InsertPaycode(paycode));

            InsertComplete = true;
            LoadEmployeePaycode(EmployeeId);
            EmployeePaycodeGrid.Rebind();
        }
        protected void EmployeePaycodeGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                //hide the delete image if it is a global employee paycode
                GridDataItem item = (GridDataItem)e.Item;
                item["deleteButton"].Controls[0].Visible = DeleteFlag && !((EmployeePaycode)item.DataItem).GlobalEmployeePaycodeFlag;
            }
        }
        protected void EmployeePaycodeGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            //get index of item being edited, will only be 1 item
            GridIndexCollection rows = EmployeePaycodeGrid.ItemIndexes;
            RowIndex = Convert.ToInt16(rows[0]);

            GetSelectedEmployeePaycode();
        }
        protected void EmployeePaycodeGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            EmployeePaycode paycode = (EmployeePaycode)e.Item.DataItem;
            paycode.EmployeeId = EmployeeId;
            Common.ServiceWrapper.HumanResourcesClient.InsertEmployeePaycode(paycode).CopyTo((EmployeePaycode)DataItemCollection[paycode.Key]);
        }
        protected void EmployeePaycodeGrid_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                EmployeePaycode record = (EmployeePaycode)e.Item.DataItem;
                String paycodeTypeCode = record.PaycodeTypeCode;

                if (IsDataExternallyLoaded)
                {
                    ItemChangedEventArgs args = new ItemChangedEventArgs() { Items = DataItemCollection };
                    OnItemChanged(args);
                }
                else
                    Common.ServiceWrapper.HumanResourcesClient.DeleteEmployeePaycode(record);

                EmployeePaycodeGrid.Rebind();

                //check type and rebind associated control
                switch (paycodeTypeCode)
                {
                    case "1":
                        EmployeePaycodeIncome.DataBind();
                        break;
                    case "2":
                        EmployeePaycodeBenefit.DataBind();
                        break;
                    case "3":
                        EmployeePaycodeDeduction.DataBind();
                        break;
                    default:
                        break;
                }
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "FKConstraint")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void EmployeePaycodeGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EmployeePaycodeGrid.DataSource = DataItemCollection;
        }
        protected void EmployeePaycodeGrid_DataBound(object sender, EventArgs e)
        {
            //if we arrived here because the CancelClicked variable was set to true, then set it back to false
            if (CancelClicked)
                CancelClicked = false;

            if (EmployeePaycodeGrid.Items.Count > 0)
            {
                if (!_updatePerformed)
                {
                    EmployeePaycodeGrid.Items[0].Selected = true;
                    RowIndex = 0;
                }
                else
                {
                    EmployeePaycodeGrid.Items[RowIndex].Selected = true;
                    _updatePerformed = false;
                }

                GetSelectedEmployeePaycode();
            }
        }
        protected void EmployeePaycodeGrid_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                EmployeePaycode paycode = (EmployeePaycode)e.Item.DataItem;
                Common.ServiceWrapper.HumanResourcesClient.UpdateEmployeePaycode(paycode).CopyTo((EmployeePaycode)DataItemCollection[paycode.Key]);
            }
            catch (System.ServiceModel.FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.DataConcurrency)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "PopupScript", String.Format("alert('{0}');", GetGlobalResourceObject("ErrorMessages", "ConcurrenyErrorMessage")), true);
                    e.Canceled = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void EmployeePaycodeToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            RadToolBarButton button = ((RadToolBarButton)e.Item);

            if (button.CommandName.Equals("AddIncome"))
            {
                RadMultiPage1.FindPageViewByID("IncomePageView").Selected = true;
                EmployeePaycodeIncome.ChangeModeInsert();
            }
            else if (button.CommandName.Equals("AddDeduction"))
            {
                RadMultiPage1.FindPageViewByID("DeductionPageView").Selected = true;
                EmployeePaycodeDeduction.ChangeModeInsert();
            }
            else if (button.CommandName.Equals("AddBenefit"))
            {
                RadMultiPage1.FindPageViewByID("BenefitPageView").Selected = true;
                EmployeePaycodeBenefit.ChangeModeInsert();
            }
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControlWithoutFiltering((ICodeControl)sender, LanguageCode);
        }
        protected void PaycodeCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulatePaycodes((ICodeControl)sender, LanguageCode);
        }
        void LoadableControl_EmployeeDetailToolbarButtonClick(object sender, RadToolBarEventArgs e)
        {
            RadToolBarButton Button = ((RadToolBarButton)e.Item);

            switch (Button.CommandName.ToLower())
            {
                case "cancel":
                    {
                        CancelClicked = true;
                        OnItemChangingComplete(null);
                        break;
                    }
                case "insert":
                case "performinsert":
                case "update":
                    OnItemChangingComplete(null);
                    break;
                case "edit":
                case "initinsert":
                    OnItemChanging(null);
                    break;
            }
        }
        protected void EmployeePaycodeGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "addincome":
                case "adddeduction":
                case "addbenefit":
                    OnItemChanging(null);
                    break;
            }
        }
        protected void AddDropDown_PreRender(object sender, EventArgs e)
        {
            ((WLPToolBarDropDown)sender).Visible = ((WLPToolBarDropDown)sender).Visible && AddFlag;
        }
        #endregion

        #region override
        public override void Update(bool updateExterallyControlled)
        {
        }
        public override void AddNewDataItem()
        {
            if (DataItemCollection == null)
                DataItemCollection = new EmployeePaycodeCollection();
        }
        public override void ChangeModeEdit()
        {
        }
        #endregion
    }
}