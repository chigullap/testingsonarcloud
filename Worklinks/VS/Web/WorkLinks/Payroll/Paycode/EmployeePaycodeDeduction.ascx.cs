﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Payroll.Paycode
{
    public partial class EmployeePaycodeDeduction : WLP.Web.UI.WLPUserControl
    {
        public event EmployeeDetailToolbarButtonClickEventHandler EmployeeDetailToolbarButtonClick;

        #region fields
        private const String _employeePaycodeTypeCode = "3";
        private String _paycodeDataValidateMsg = "";
        private static bool _isMandatory = false;
        private static bool _isRemoveStubClicked = false;
        #endregion

        #region properties
        public EmployeePaycode EmployeePaycode
        {
            get
            {
                EmployeePaycode paycode = null;

                if (Data != null && Data.Count > 0)
                    paycode = (EmployeePaycode)Data[0];

                return paycode;
            }
        }
        public new IDataItemCollection<IDataItem> Data
        {
            get
            {
                return base.Data;
            }
            set
            {
                if (IsInsertMode)
                {
                    foreach (EmployeePaycode paycode in value)
                        paycode.PaycodeTypeCode = _employeePaycodeTypeCode;
                }

                base.Data = value;
            }
        }
        public PaycodeAttachedPaycodeProvisionCollection AttachedPaycodes
        {
            get
            {
                ComboBoxControl paycodeCodeControl = (ComboBoxControl)EmployeePaycodeDeductionView.FindControl("PaycodeCode");
                String paycodeCode = null;

                if (paycodeCodeControl != null)
                    paycodeCode = paycodeCodeControl.Value != null ? paycodeCodeControl.Value.ToString() : null;

                return Common.ServiceWrapper.CodeClient.GetPaycodeAttachedPaycodeProvision(paycodeCode);
            }
        }
        public String PaycodeDataValidateMsg
        {
            get { return _paycodeDataValidateMsg; }
            set { _paycodeDataValidateMsg = value; }
        }
        public static bool IsMandatory
        {
            get { return _isMandatory; }
            set { _isMandatory = value; }
        }
        public static bool IsRemoveStubClicked
        {
            get { return _isRemoveStubClicked; }
            set { _isRemoveStubClicked = value; }
        }
        private long EmployeeId { get { return Convert.ToInt64(Page.RouteData.Values["employeeId"]); } }
        public long EmployeePaycodeId { get { return EmployeePaycode.EmployeePaycodeId; } }
        public bool IsGarnishment { get { return EmployeePaycode.GarnishmentFlag; } }
        public bool IsGlobalEmployeePaycode { get { return EmployeePaycode.GlobalEmployeePaycodeFlag; } }
        public bool IsViewMode { get { return EmployeePaycodeDeductionView.CurrentMode == FormViewMode.ReadOnly; } }
        public bool IsInsertMode { get { return EmployeePaycodeDeductionView.CurrentMode == FormViewMode.Insert; } }
        public bool IsEditMode { get { return EmployeePaycodeDeductionView.CurrentMode == FormViewMode.Edit; } }
        public bool UpdateFlag { get { return Common.Security.RoleForm.EmployeePaycode.UpdateFlag; } }
        public String DropDownTreeDefaultMessage { get { return String.Format(GetGlobalResourceObject("PageContent", "ChooseADepartment").ToString()); } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        public void ChangeModeInsert()
        {
            EmployeePaycodeDeductionView.ChangeMode(FormViewMode.Insert);
            EmployeePaycodeDeductionView.DataBind();
        }
        #endregion

        #region event handlers
        protected void EmployeePaycodeDeductionView_DataBound(object sender, EventArgs e)
        {
            RadDropDownTree organizationUnitDropDownTree = (RadDropDownTree)EmployeePaycodeDeductionView.FindControl("OrganizationUnitDropDownTree");

            if (organizationUnitDropDownTree != null)
            {
                organizationUnitDropDownTree.DataSource = Common.ServiceWrapper.HumanResourcesClient.GetOrganizationUnitAssociationTree(LanguageCode, null);
                organizationUnitDropDownTree.DefaultMessage = DropDownTreeDefaultMessage;
                organizationUnitDropDownTree.DataBind();

                if (!IsInsertMode && !IsViewMode)
                {
                    EmployeePaycode employeePaycode = ((EmployeePaycodeCollection)Data)[0];

                    //asp hidden field to hold org hierarchy
                    HiddenField orgHierarchy = (HiddenField)EmployeePaycodeDeductionView.FindControl("OrganizationUnitDropDownValues");
                    //if we have values, remove the leading / so the javascript can split the array properly
                    if (orgHierarchy != null)
                        orgHierarchy.Value = employeePaycode.OrganizationUnit != null ? employeePaycode.OrganizationUnit.Substring(1) : null;
                }
            }
        }
        protected void EmployeePaycodeDeductionView_NeedDataSource(object sender, WLPNeedDataSourceEventArgs e)
        {
            OnNeedDataSource(e);
            EmployeePaycodeDeductionView.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void PaycodeCode_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulatePaycodes((ICodeControl)sender, LanguageCode);
        }
        protected void VendorCombo_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateVendorComboControl((ICodeControl)sender);
        }
        protected void PaycodeCode_DataBinding(object sender, EventArgs e)
        {
            if (sender is ComboBoxControl)
            {
                bool leaveSelected = false;
                EmployeePaycodeModule temp = (EmployeePaycodeModule)this.Parent.Parent.Parent.Parent;

                if (temp.IsDataExternallyLoaded)
                {
                    if (IsInsertMode)
                        Common.CodeHelper.PopulateComboBoxWithTypeForWizardRemovingTypesInUse((ICodeControl)sender, temp.DataItemCollection, LanguageCode, temp.WizardItemName, leaveSelected, _employeePaycodeTypeCode);
                    else
                    {
                        leaveSelected = true;
                        Common.CodeHelper.PopulateComboBoxWithTypeForWizardRemovingTypesInUse((ICodeControl)sender, temp.DataItemCollection, LanguageCode, temp.WizardItemName, leaveSelected, _employeePaycodeTypeCode);
                    }
                }
                else
                {
                    if (IsInsertMode)
                        Common.CodeHelper.PopulateComboBoxWithPaycodesByTypeRemovingPaycodesInUse((ComboBoxControl)sender, _employeePaycodeTypeCode, LanguageCode, EmployeeId, true);
                    else
                        Common.CodeHelper.PopulateComboBoxWithPaycodesByType((ComboBoxControl)sender, _employeePaycodeTypeCode);
                }

                BindCodePayCodeTypeCd(sender);
            }
        }
        private void BindCodePayCodeTypeCd(object sender)
        {
            if (sender is ComboBoxControl)
            {
                ComboBoxControl paycodeCombo = (ComboBoxControl)sender;
                WLPFormView formItem = paycodeCombo.BindingContainer as WLPFormView;
                ComboBoxControl paycodeTypeCombo = (ComboBoxControl)formItem.FindControl("CodePaycodeTypeCd");

                paycodeTypeCombo.DataSource = Common.ServiceWrapper.CodeClient.GetPaycodeTypeCode(paycodeCombo.SelectedValue);
                paycodeTypeCombo.DataBind();

                if (formItem.DataItem != null && formItem.DataItem is CodePaycode)
                    paycodeTypeCombo.Value = 3;
            }
        }
        protected void GarnishmentVendorId_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            IsRemoveStubClicked = true;
        }
        protected void GarnishmentVendorId_OnPreRender(object sender, EventArgs e)
        {
            ComboBoxControl garnishmentVendorId = (ComboBoxControl)sender;
            WLPFormView formItem = (WLPFormView)garnishmentVendorId.BindingContainer;
            AsyncUploadControl employeeRemittanceStub = (AsyncUploadControl)formItem.FindControl("EmployeeRemittanceStub");
            WLPButton removeStub = (WLPButton)formItem.FindControl("RemoveStub");

            ToggleEmployeeRemittanceStub(garnishmentVendorId, employeeRemittanceStub, removeStub);
        }
        protected void ToggleEmployeeRemittanceStub(ComboBoxControl garnishmentVendorId, AsyncUploadControl employeeRemittanceStub, WLPButton removeStub)
        {
            long? vendorId = garnishmentVendorId.Value != null ? Convert.ToInt64(garnishmentVendorId.Value) : (long?)null;
            employeeRemittanceStub.AllowedFileExtensions = Common.CodeHelper.PopulateAllowedFileExtensions(LanguageCode);

            if (vendorId != null)
            {
                if (IsEditMode && EmployeePaycode.RemittanceStubCollection.Count > 0 && EmployeePaycode.GarnishmentVendorId == vendorId)
                {
                    employeeRemittanceStub.Visible = IsRemoveStubClicked;
                    removeStub.Visible = !IsRemoveStubClicked;
                }
                else
                {
                    Vendor vendor = Common.ServiceWrapper.HumanResourcesClient.GetVendor(Convert.ToInt64(vendorId))[0];

                    //if the vendor garnishment type is OTHER, the option to include a garnishment stub will be presented but wont be mandatory
                    employeeRemittanceStub.Visible = vendor.GarnishmentTypeCode == "OTHER";
                    removeStub.Visible = false;
                }
            }
            else
            {
                employeeRemittanceStub.Visible = false;
                removeStub.Visible = false;
            }
        }
        protected void PaycodeCode_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ShowHideGarnishmentDetails((ComboBoxControl)sender, e); //show/hide the garnishment objects
        }
        protected void ShowHideGarnishmentDetails(object sender, EventArgs e)
        {
            //check the paycode object if garnishment_flag=1 and if so, update the object EmployeePaycode
            ComboBoxControl payCodeCd = (ComboBoxControl)sender;
            WLPFormView formItem = payCodeCd.BindingContainer as WLPFormView;
            bool isGarnishment = Common.ServiceWrapper.CodeClient.IsPaycodeGarnishment(payCodeCd.SelectedValue.ToString(), _employeePaycodeTypeCode);

            //find and show/hide the garnishment objects
            ComboBoxControl garnishmentVendorId = (ComboBoxControl)formItem.FindControl("GarnishmentVendorId");
            if (garnishmentVendorId != null)
                garnishmentVendorId.Visible = isGarnishment;

            TextBoxControl garnishmentOrderNumber = (TextBoxControl)formItem.FindControl("GarnishmentOrderNumber");
            if (garnishmentOrderNumber != null)
                garnishmentOrderNumber.Visible = isGarnishment;
        }
        protected virtual void OnEmployeeDetailToolbarButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (EmployeeDetailToolbarButtonClick != null)
                EmployeeDetailToolbarButtonClick(sender, e);
        }
        protected void EmployeeDetailToolBar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            OnEmployeeDetailToolbarButtonClick(sender, e);
        }
        protected void RemoveStub_Click(object sender, EventArgs e)
        {
            WLPButton removeStub = (WLPButton)sender;
            WLPFormView formItem = (WLPFormView)removeStub.BindingContainer;
            ComboBoxControl garnishmentVendorId = (ComboBoxControl)formItem.FindControl("GarnishmentVendorId");
            AsyncUploadControl employeeRemittanceStub = (AsyncUploadControl)formItem.FindControl("EmployeeRemittanceStub");

            IsRemoveStubClicked = true;
            ToggleEmployeeRemittanceStub(garnishmentVendorId, employeeRemittanceStub, removeStub);
        }
        #endregion

        #region handles updates
        protected void GetAdditionalEmployeePaycodeObjects(EmployeePaycode item)
        {
            if (!Common.ServiceWrapper.CodeClient.IsPaycodeGarnishment(item.PaycodeCode, _employeePaycodeTypeCode))
            {
                //null out the garnishment objects
                item.GarnishmentVendorId = null;
                item.GarnishmentOrderNumber = null;
            }

            AsyncUploadControl employeeRemittanceStub = (AsyncUploadControl)EmployeePaycodeDeductionView.FindControl("EmployeeRemittanceStub");
            if (employeeRemittanceStub.UploadedFiles.Count > 0)
            {
                //get the uploaded file from the EmployeeRemittanceStub control and add it to the EmployeeRemittanceStubCollection
                UploadedFile file = employeeRemittanceStub.UploadedFiles[0];
                byte[] bytes = new byte[file.ContentLength];
                file.InputStream.Read(bytes, 0, bytes.Length);

                item.RemittanceStubCollection = new EmployeeRemittanceStubCollection();
                item.RemittanceStubCollection.AddNew();
                item.RemittanceStubCollection[0].Data = bytes;
                item.RemittanceStubCollection[0].FileName = file.GetNameWithoutExtension();
                item.RemittanceStubCollection[0].FileTypeCode = file.GetExtension().Replace(".", "").ToUpper();
            }
            else if (IsRemoveStubClicked)
                item.RemittanceStubCollection = new EmployeeRemittanceStubCollection();

            //get the selected value from the OrganizationUnitDropDownTree control and assign it to the OrganizationUnit column
            HiddenField hiddenOrg = (HiddenField)EmployeePaycodeDeductionView.FindControl("OrganizationUnitDropDownValues");
            if (hiddenOrg != null)
            {
                if (hiddenOrg.Value.Length > 0 && !hiddenOrg.Value.StartsWith("/"))
                    hiddenOrg.Value = "/" + hiddenOrg.Value;

                item.OrganizationUnit = (hiddenOrg.Value == String.Empty) ? null : hiddenOrg.Value;
            }
        }
        protected void EmployeePaycodeDeductionView_Inserting(object sender, WLPItemInsertingEventArgs e)
        {
            if (Page.IsValid)
            {
                if (Common.ServiceWrapper.HumanResourcesClient.ValidatePaycodeEntryData((EmployeePaycode)e.DataItem))
                {
                    PaycodeDataValidateMsg = "";
                    GetAdditionalEmployeePaycodeObjects((EmployeePaycode)e.DataItem);
                    OnInserting(e);
                    IsRemoveStubClicked = false;
                }
                else
                {
                    PaycodeDataValidateMsg = String.Format("{0}", GetGlobalResourceObject("ErrorMessages", "PaycodeDataValidate"));
                    e.Cancel = true;
                }
            }
            else
                e.Cancel = true;
        }
        protected void EmployeePaycodeDeductionView_Updating(object sender, WLPItemUpdatingEventArgs e)
        {
            if (Page.IsValid)
            {
                if (Common.ServiceWrapper.HumanResourcesClient.ValidatePaycodeEntryData((EmployeePaycode)e.DataItem))
                {
                    PaycodeDataValidateMsg = "";
                    GetAdditionalEmployeePaycodeObjects((EmployeePaycode)e.DataItem);
                    OnUpdating(e);
                    IsRemoveStubClicked = false;
                }
                else
                {
                    PaycodeDataValidateMsg = String.Format("{0}", GetGlobalResourceObject("ErrorMessages", "PaycodeDataValidate"));
                    e.Cancel = true;
                }
            }
            else
                e.Cancel = true;
        }
        #endregion
    }
}