﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PayrollEmployeeSearchPage.aspx.cs" Inherits="WorkLinks.Payroll.WebForm2" %>
<%@ Register src="../../HumanResources/Employee/EmployeeSearchControl.ascx" tagname="EmployeeSearchControl" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ContentTemplate> 
        <uc1:EmployeeSearchControl ID="EmployeeSearchControl1" runat="server" SearchLocation="Payroll" />
    </ContentTemplate>
</asp:Content>
