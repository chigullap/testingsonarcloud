﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecurityUserGroupControl.ascx.cs" Inherits="WorkLinks.SecurityUserGroupControl" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<wasp:WLPComboBox ID="ProfileGroup" Text="Code Table:" AutoPostBack="true" runat="server" IncludeEmptyItem="false" OnSelectedIndexChanged="ProfileGroup_SelectedIndexChanged" OnDataBinding="ProfileGroup_DataBinding" />