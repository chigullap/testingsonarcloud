﻿using System;

namespace WorkLinks.Attachments
{
    public partial class AttachmentControl : WLP.Web.UI.WLPUserControl
    {
        #region properties
        private long AttachmentId { get { return Convert.ToInt64(Page.RouteData.Values["attachmentId"]); } }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BusinessLayer.BusinessObjects.AttachmentCollection attachmentCollection = Common.ServiceWrapper.HumanResourcesClient.GetAttachment(AttachmentId);

                if (attachmentCollection.Count > 0)
                {
                    byte[] attachment = attachmentCollection[0].Data;
                    String attachmentName = attachmentCollection[0].FileName;
                    String attachmentType = attachmentCollection[0].FileTypeCode;

                    if (attachmentType.Equals("JPG"))
                        Response.ContentType = "image/jpg";
                    else if (attachmentType.Equals("JPEG"))
                        Response.ContentType = "image/jpeg";
                    else if (attachmentType.Equals("GIF"))
                        Response.ContentType = "image/GIF";
                    else if (attachmentType.Equals("PNG"))
                        Response.ContentType = "image/png";
                    else if (attachmentType.Equals("PDF"))
                        Response.ContentType = "application/pdf";
                    else if (attachmentType.Equals("DOC"))
                        Response.ContentType = "application/msword";
                    else if (attachmentType.Equals("DOCX"))
                    {
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                        Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}.docx\"", attachmentName));
                    }
                    else if (attachmentType.Equals("XLS"))
                    {
                        Response.ContentType = "application/vnd.ms-excel";
                        Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}.xls\"", attachmentName));
                    }
                    else if (attachmentType.Equals("XLSX"))
                    {
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}.xlsx\"", attachmentName));
                    }
                    else
                    {
                        attachment = null;
                        Response.ContentType = "text/html";
                        Response.Write("The file " + attachmentName + "." + attachmentType.ToLower() + " could not be loaded.");
                    }

                    if (attachment != null) Response.OutputStream.Write(attachment, 0, attachment.Length);

                    Response.Flush();
                    Response.End();
                }
            }
        }
    }
}