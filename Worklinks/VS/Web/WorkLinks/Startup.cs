﻿using Microsoft.Owin;
using Microsoft.Owin.Extensions;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;
using System;
//using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq;
using IdentityModel.Client;
using System.Collections.Generic;
using Microsoft.Owin.Security;
using WorkLinks.Common;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;

//[assembly: OwinStartup(typeof(WorkLinks.Startup))]
namespace WorkLinks
{
    public class Startup
    {
        //        public void Configuration(IAppBuilder app)
        //        {

        //          //  app.UseKentorOwinCookieSaver(PipelineStage.Authenticate);

        //            // Use Cookies to Store JWT Token for Web Browsers
        //            app.UseCookieAuthentication(new CookieAuthenticationOptions
        //            {
        //                AuthenticationType = CookieAuthenticationDefaults.AuthenticationType,
        //                CookieName = "WLOldCookie3.0",
        //                //CookieHttpOnly = true,
        ////                ExpireTimeSpan = TimeSpan.FromMinutes(20),
        ////                SlidingExpiration = true,
        //            });

        //            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
        //            //            JwtSecurityTokenHandler.InboundClaimTypeMap.Clear();

        //            // Authenticate to Auth Server
        //            app.UseOpenIdConnectAuthentication(new OpenIdConnectAuthenticationOptions
        //            {
        //                AuthenticationType = "oidc",
        //                SignInAsAuthenticationType = "Cookies",
        //                Authority = ApplicationParameter.SecurityAuthority,
        //                ClientId = ApplicationParameter.SecurityClientId,
        //                RedirectUri = ApplicationParameter.SecurityRedirectUri,
        //                PostLogoutRedirectUri = ApplicationParameter.SecurityPostLogoutRedirectUri,
        ////                CallbackPath = new PathString("/"+ApplicationParameter.SecurityRedirectUri),

        //                ResponseType = "id_token token",
        //                Scope = ApplicationParameter.Scope,
        //                UseTokenLifetime = false,

        //                Notifications = GetOpenIdConnectAuthenticationNotifications()
        //            });

        //            app.UseStageMarker(PipelineStage.Authenticate);

        //        }

        //        private OpenIdConnectAuthenticationNotifications GetOpenIdConnectAuthenticationNotifications()
        //    {
        //        return new OpenIdConnectAuthenticationNotifications
        //        {
        //            SecurityTokenValidated = async notification =>
        //            {
        //                //     notification.OwinContext.Authentication.Challenge();

        //                List<Claim> claims = notification.AuthenticationTicket.Identity.Claims.ToList();
        //                claims.Add(new Claim("id_token", notification.ProtocolMessage.IdToken));

        //                if (notification.ProtocolMessage.AccessToken != null)
        //                {
        //                    // Add access_token so we don't need to request it when calling APIs
        //                    claims.Add(new Claim("access_token", notification.ProtocolMessage.AccessToken));

        //                    UserInfoClient userInfoClient = new UserInfoClient(new Uri(notification.Options.Authority + "connect/userinfo").ToString());
        //                    UserInfoResponse userInfoResponse = await userInfoClient.GetAsync(notification.ProtocolMessage.AccessToken);
        //                    claims.AddRange(userInfoResponse.Claims.Where(x => x.Type != "sub")); //combine claims skip sub as it's a dupe
        //                }

        //                ClaimsIdentity ci = new ClaimsIdentity(notification.AuthenticationTicket.Identity.AuthenticationType, "name", "role");
        //                ci.AddClaims(claims);

        //                notification.AuthenticationTicket = new AuthenticationTicket(ci, notification.AuthenticationTicket.Properties);
        //            },
        //            RedirectToIdentityProvider = notification =>
        //            {

        //                if (notification.ProtocolMessage.RequestType == OpenIdConnectRequestType.Logout)
        //                    notification.ProtocolMessage.IdTokenHint = notification.OwinContext.Authentication.User.FindFirst("id_token")?.Value;


        //                return Task.FromResult(0);
        //            }
        //        };


        //    }
    }
}
