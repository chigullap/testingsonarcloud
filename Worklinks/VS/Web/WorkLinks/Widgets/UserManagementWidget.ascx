﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserManagementWidget.ascx.cs" Inherits="WorkLinks.Widgets.UserManagementWidget" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="SearchPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SearchPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="SearchPanel" runat="server">
    <div class="widget">
        <wasp:WLPLabel ID="UserManagementLabel" runat="server" CssClass="widgetHeader" OnPreRender="UserManagementLabel_PreRender" />

        <div class="widgetContent">
            <wasp:WLPGrid
                ID="UserManagementGrid"
                runat="server"
                AllowPaging="true"
                PagerStyle-AlwaysVisible="true"
                PageSize="100"
                Height="300px"
                AllowSorting="true"
                GridLines="None"
                AutoAssignModifyProperties="true">

                <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                    <ClientEvents OnRowDblClick="OnUserManagementRowDblClick" OnRowClick="OnUserManagementRowClick" OnGridCreated="OnUserManagementGridCreated" />
                </ClientSettings>

                <MasterTableView ClientDataKeyNames="PersonId" AutoGenerateColumns="false" CommandItemDisplay="Top">
                    <CommandItemTemplate></CommandItemTemplate>
                    <Columns>
                        <wasp:GridBoundControl DataField="UserName" LabelText="**UserName**" SortExpression="UserName" UniqueName="UserName" ResourceName="UserName" />
                        <wasp:GridBoundControl DataField="LastName" LabelText="**LastName**" SortExpression="LastName" UniqueName="LastName" ResourceName="LastName" />
                        <wasp:GridBoundControl DataField="FirstName" LabelText="**FirstName**" SortExpression="FirstName" UniqueName="FirstName" ResourceName="FirstName" />
                        <wasp:GridDateTimeControl DataField="LastLoginDatetime" LabelText="**LastLogin**" SortExpression="LastLoginDatetime" UniqueName="LastLoginDatetime" ResourceName="LastLoginDatetime">
                            <HeaderStyle Width="120px" />
                        </wasp:GridDateTimeControl>
                        <wasp:GridDateTimeControl DataField="LoginExpiryDatetime" LabelText="**LoginExpiry**" SortExpression="LoginExpiryDatetime" UniqueName="LoginExpiryDatetime" ResourceName="LoginExpiryDatetime">
                            <HeaderStyle Width="120px" />
                        </wasp:GridDateTimeControl>
                        <wasp:GridCheckBoxControl DataField="IsLockedOutFlag" LabelText="**IsLockedOut**" SortExpression="IsLockedOutFlag" UniqueName="IsLockedOutFlag" ResourceName="IsLockedOutFlag" />
                        <wasp:GridDateTimeControl DataField="LastLockoutDatetime" LabelText="**LastLockout**" SortExpression="LastLockoutDatetime" UniqueName="LastLockoutDatetime" ResourceName="LastLockoutDatetime">
                            <HeaderStyle Width="120px" />
                        </wasp:GridDateTimeControl>
                        <wasp:GridDateTimeControl DataField="LastPasswordChangedDatetime" LabelText="**LastPasswordChanged**" SortExpression="LastPasswordChangedDatetime" UniqueName="LastPasswordChangedDatetime" ResourceName="LastPasswordChangedDatetime">
                            <HeaderStyle Width="120px" />
                        </wasp:GridDateTimeControl>
                        <wasp:GridDateTimeControl DataField="PasswordExpiryDatetime" LabelText="**PasswordExpiry**" SortExpression="PasswordExpiryDatetime" UniqueName="PasswordExpiryDatetime" ResourceName="PasswordExpiryDatetime">
                            <HeaderStyle Width="120px" />
                        </wasp:GridDateTimeControl>
                        <wasp:GridDateTimeControl DataField="LastLogoutDatetime" LabelText="**LastLogout**" SortExpression="LastLogoutDatetime" UniqueName="LastLogoutDatetime" ResourceName="LastLogoutDatetime">
                            <HeaderStyle Width="120px" />
                        </wasp:GridDateTimeControl>
                    </Columns>
                </MasterTableView>

                <HeaderContextMenu EnableAutoScroll="true"></HeaderContextMenu>
            </wasp:WLPGrid>  
        </div>
    </div>
</asp:Panel>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var personId;

        function GetSelectedRow() {
            var grid = $find('<%= UserManagementGrid.ClientID %>');

            if (grid != null) {
                var masterTable = grid.get_masterTableView();
                if (masterTable != null)
                    return masterTable.get_selectedItems()[0];
                else
                    return null;
            }

            return null;
         }

        function RowIsSelected() {
            var getSeletecteRow = GetSelectedRow();
            return (getSeletecteRow != null);
        }

        function OnUserManagementRowClick(sender, eventArgs) {
            personId = eventArgs.getDataKeyValue('PersonId');
        }

        function OnUserManagementRowDblClick(sender, eventArgs) {
            if (personId != null)
                openWindowWithManager('WidgetWindows', String.format('/Admin/UserAdmin/View/{0}', personId), false);
        }

        function OnUserManagementGridCreated(sender, eventArgs) {
            if (RowIsSelected()) {
                var selectedRow = GetSelectedRow();
                personId = selectedRow.getDataKeyValue('PersonId');
            }
            else
                personId = null;
        }
    </script>
</telerik:RadScriptBlock>