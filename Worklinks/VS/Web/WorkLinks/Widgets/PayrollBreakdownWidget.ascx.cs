﻿using System;
using Telerik.Web.UI;
using Telerik.Web.UI.HtmlChart;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.Widgets
{
    public partial class PayrollBreakdownWidget : WLP.Web.UI.WLPUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.PayrollBreakdownWidget.ViewFlag);

            Initialize();
        }
        private void Initialize()
        {
            if (!Page.IsPostBack)
            {
                CodeCollection collection = new CodeCollection
                {
                    new CodeObject() { Code = "CURR_GROUP", Description = "Current by Process Group" },
                    new CodeObject() { Code = "CURR_DEPART", Description = "Current by Department" },
                    new CodeObject() { Code = "YTD_GROUP", Description = "YTD by Process Group" },
                    new CodeObject() { Code = "YTD_DEPART", Description = "YTD by Department" }
                };

                BreakdownComboBox.DataSource = collection;
                BreakdownComboBox.DataBind();

                LoadPayrollBreakdown("CURR_GROUP");
            }
        }
        protected void Breakdowns_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (e.Value != null)
                LoadPayrollBreakdown(e.Value);
            else
                PayrollBreakdown.Visible = false;
        }
        protected void LoadPayrollBreakdown(String selectedValue)
        {
            String breakDownType = selectedValue.Split('_')[0];
            String filteredBy = selectedValue.Split('_')[1];

            PayrollBreakdown.Visible = true;
            PayrollBreakdown.Legend.Appearance.Position = ChartLegendPosition.Bottom;
            PayrollBreakdown.DataSource = Common.ServiceWrapper.HumanResourcesClient.GetPayrollBreakdown(null, breakDownType, filteredBy);
            PayrollBreakdown.DataBind();
        }
    }
}