﻿using System;

namespace WorkLinks.Widgets
{
    public partial class EmployeeTimeReviewWidget : WLP.Web.UI.WLPUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EmployeeTimeReviewWidget.ViewFlag);

            WidgetFrame.Src = System.Configuration.ConfigurationManager.AppSettings.Get("ANGULARURL") + "/widget/employee-time-review";
        }
    }
}