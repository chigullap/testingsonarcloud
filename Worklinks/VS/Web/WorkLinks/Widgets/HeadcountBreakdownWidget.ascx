﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeadcountBreakdownWidget.ascx.cs" Inherits="WorkLinks.Widgets.HeadcountBreakdownWidget" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<div class="widget">
    <wasp:WLPLabel ID="HeadcountBreakdownLabel" runat="server" CssClass="widgetHeader" OnPreRender="HeadcountBreakdownLabel_PreRender" />
    <div class="widgetContent">
        <div style="height: 300px; overflow-y: scroll; overflow-x: hidden;">
            <table width="100%" style="border-spacing: 0 !important;">
                <tr>
                    <td valign="top" width="33%">
                        <div id="HeadcountBreakdown1Content" runat="server" Visible="false">
                            <wasp:WLPLabel ID="HeadcountBreakdown1Label" runat="server" Visible="false" />
                            <telerik:RadHtmlChart runat="server" ID="HeadcountBreakdown1" Width="100%" Height="300px" Transitions="true" Visible="false">
                                <PlotArea>
                                    <Series>
                                        <telerik:PieSeries DataFieldY="Percentage" NameField="Description">
                                            <LabelsAppearance Visible="false" />
                                            <TooltipsAppearance>
                                                <ClientTemplate>#=dataItem.Percentage#% – #=dataItem.Description#</ClientTemplate>
                                            </TooltipsAppearance>
                                        </telerik:PieSeries>
                                    </Series>
                                </PlotArea>
                            </telerik:RadHtmlChart>
                        </div>
                    </td>
                    <td valign="top" width="33%">
                        <div id="HeadcountBreakdown2Content" runat="server" Visible="false">
                            <wasp:WLPLabel ID="HeadcountBreakdown2Label" runat="server" Visible="false" />
                            <telerik:RadHtmlChart runat="server" ID="HeadcountBreakdown2" Width="100%" Height="300px" Transitions="true" Visible="false">
                                <PlotArea>
                                    <Series>
                                        <telerik:DonutSeries HoleSize="60" DataFieldY="Percentage" NameField="Description">
                                            <LabelsAppearance Visible="false" />
                                            <TooltipsAppearance>
                                                <ClientTemplate>#=dataItem.Percentage#% – #=dataItem.Description#</ClientTemplate>
                                            </TooltipsAppearance>
                                        </telerik:DonutSeries>
                                    </Series>
                                </PlotArea>
                            </telerik:RadHtmlChart>
                        </div>
                    </td>
                    <td valign="top" width="33%">
                        <div id="HeadcountBreakdown3Content" runat="server" Visible="false">
                            <wasp:WLPLabel ID="HeadcountBreakdown3Label" runat="server" Visible="false" />
                            <telerik:RadHtmlChart runat="server" ID="HeadcountBreakdown3" Width="100%" Height="300px" Transitions="true" Visible="false">
                                <PlotArea>
                                    <Series>
                                        <telerik:PieSeries DataFieldY="Percentage" NameField="Description">
                                            <LabelsAppearance Visible="false" />
                                            <TooltipsAppearance>
                                                <ClientTemplate>#=dataItem.Percentage#% – #=dataItem.Description#</ClientTemplate>
                                            </TooltipsAppearance>
                                        </telerik:PieSeries>
                                    </Series>
                                </PlotArea>
                            </telerik:RadHtmlChart>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top" width="33%">
                        <div id="HeadcountBreakdown4Content" runat="server" Visible="false">
                            <wasp:WLPLabel ID="HeadcountBreakdown4Label" runat="server" Visible="false" />
                            <telerik:RadHtmlChart runat="server" ID="HeadcountBreakdown4" Width="100%" Height="300px" Transitions="true" Visible="false">
                                <PlotArea>
                                    <Series>
                                        <telerik:DonutSeries HoleSize="60" DataFieldY="Percentage" NameField="Description">
                                            <LabelsAppearance Visible="false" />
                                            <TooltipsAppearance>
                                                <ClientTemplate>#=dataItem.Percentage#% – #=dataItem.Description#</ClientTemplate>
                                            </TooltipsAppearance>
                                        </telerik:DonutSeries>
                                    </Series>
                                </PlotArea>
                            </telerik:RadHtmlChart>
                        </div>
                    </td>
                    <td valign="top" width="33%">
                        <div id="HeadcountBreakdown5Content" runat="server" Visible="false">
                            <wasp:WLPLabel ID="HeadcountBreakdown5Label" runat="server" Visible="false" />
                            <telerik:RadHtmlChart runat="server" ID="HeadcountBreakdown5" Width="100%" Height="300px" Transitions="true" Visible="false">
                                <PlotArea>
                                    <Series>
                                        <telerik:PieSeries DataFieldY="Percentage" NameField="Description">
                                            <LabelsAppearance Visible="false" />
                                            <TooltipsAppearance>
                                                <ClientTemplate>#=dataItem.Percentage#% – #=dataItem.Description#</ClientTemplate>
                                            </TooltipsAppearance>
                                        </telerik:PieSeries>
                                    </Series>
                                </PlotArea>
                            </telerik:RadHtmlChart>
                        </div>
                    </td>
                    <td valign="top" width="33%">
                        <div id="HeadcountBreakdown6Content" runat="server" Visible="false">
                            <wasp:WLPLabel ID="HeadcountBreakdown6Label" runat="server" Visible="false" />
                            <telerik:RadHtmlChart runat="server" ID="HeadcountBreakdown6" Width="100%" Height="300px" Transitions="true" Visible="false">
                                <PlotArea>
                                    <Series>
                                        <telerik:DonutSeries HoleSize="60" DataFieldY="Percentage" NameField="Description">
                                            <LabelsAppearance Visible="false" />
                                            <TooltipsAppearance>
                                                <ClientTemplate>#=dataItem.Percentage#% – #=dataItem.Description#</ClientTemplate>
                                            </TooltipsAppearance>
                                        </telerik:DonutSeries>
                                    </Series>
                                </PlotArea>
                            </telerik:RadHtmlChart>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>