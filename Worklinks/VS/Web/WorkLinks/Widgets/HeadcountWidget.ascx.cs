﻿using System;
using WLP.Web.UI.Controls;

namespace WorkLinks.Widgets
{
    public partial class HeadcountWidget : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected int ViewHireWizardTemplateSelection
        {
            get
            {
                if (Common.Security.RoleForm.WizardSelectTemplate.ViewFlag)
                    return 0;
                else
                    return 1;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.HeadcountWidget.ViewFlag);

            Initialize();
        }
        private void Initialize()
        {
            HeadcountChart.Legend.Appearance.Visible = false;
            HeadcountChart.DataSource = Common.ServiceWrapper.HumanResourcesClient.GetHeadcount();
            HeadcountChart.DataBind();
        }
        #endregion

        #region event handlers
        protected void HeadcountLabel_PreRender(object sender, EventArgs e)
        {
            WLPLabel label = (WLPLabel)sender;
            label.Text = LanguageCode == "FR" ? "Effectif" : "Headcount";
        }
        protected void NewHireButton_PreRender(object sender, EventArgs e)
        {
            WLPButton label = (WLPButton)sender;
            label.Text = LanguageCode == "FR" ? "Nouvelle location" : "New Hire";
        }
        #endregion
    }
}