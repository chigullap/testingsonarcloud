﻿using System;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Widgets
{
    public partial class WelcomeWidget : WLP.Web.UI.WLPUserControl
    {
        #region properties
        public EmployeeProfile EmployeeProfile
        {
            get
            {
                EmployeeProfile profile = new EmployeeProfile();

                if (Data != null && Data.Count > 0)
                    profile = (EmployeeProfile)Data[0];

                return profile;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.WelcomeWidget.ViewFlag);

            Initialize();
        }
        private void Initialize()
        {
            Data = Common.ServiceWrapper.HumanResourcesClient.GetEmployeeProfile();

            if (EmployeeProfile.EmployeePhoto == null)
            {
                String lFileName = Server.MapPath("~/App_Themes/Default/emp_default.png");
                EmployeeProfile.EmployeePhoto = System.IO.File.ReadAllBytes(lFileName);
            }

            EmployeePhoto.DataValue = EmployeeProfile.EmployeePhoto;
        }
        #endregion

        #region event handlers
        protected void EditButton_PreRender(object sender, EventArgs e)
        {
            ((WLPButton)sender).Visible = EmployeeProfile.EmployeeId != null && Common.Security.RoleForm.WelcomeWidgetEditProfile.ViewFlag;
        }
        protected void EditContactsButton_PreRender(object sender, EventArgs e)
        {
            ((WLPButton)sender).Visible = EmployeeProfile.EmployeeId != null && Common.Security.RoleForm.WelcomeWidgetEditContacts.ViewFlag;
        }
        #endregion
    }
}