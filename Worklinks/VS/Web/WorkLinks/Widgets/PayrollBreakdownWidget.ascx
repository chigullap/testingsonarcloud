﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayrollBreakdownWidget.ascx.cs" Inherits="WorkLinks.Widgets.PayrollBreakdownWidget" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="SearchPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SearchPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="SearchPanel" runat="server">
    <div class="widget">
        <div class="widgetHeader">
            <wasp:WLPLabel ID="PayrollBreakdownLabel" runat="server" Text="Payroll Breakdown" ResourceName="PayrollBreakdownLabel" />
            <div style="float: right;">
                <wasp:ComboBoxControl ID="BreakdownComboBox" runat="server" IncludeEmptyItem="false" HideLabel="true" AutoPostback="true" OnSelectedIndexChanged="Breakdowns_SelectedIndexChanged" ResourceName="BreakdownComboBox" />
            </div>
        </div>
        <div class="widgetContent">
            <telerik:RadHtmlChart runat="server" ID="PayrollBreakdown" Width="100%" Height="300px">
                <PlotArea>
                    <Series>
                        <telerik:BarSeries Name="Net Pay" DataFieldY="NetPay">
                            <LabelsAppearance Visible="false" />
                            <TooltipsAppearance DataFormatString="N" />
                        </telerik:BarSeries>
                        <telerik:BarSeries Name="Taxes" DataFieldY="Taxes">
                            <LabelsAppearance Visible="false" />
                            <TooltipsAppearance DataFormatString="N" />
                        </telerik:BarSeries>
                        <telerik:BarSeries Name="Deductions" DataFieldY="Deductions">
                            <LabelsAppearance Visible="false" />
                            <TooltipsAppearance DataFormatString="N" />
                        </telerik:BarSeries>
                    </Series>
                    <XAxis DataLabelsField="Description">
                        <MinorGridLines Visible="false" />
                        <MajorGridLines Visible="false" />
                    </XAxis>
                    <YAxis>
                        <LabelsAppearance DataFormatString="N" />
                        <MinorGridLines Visible="false" />
                        <MajorGridLines Visible="false" />
                    </YAxis>
                </PlotArea>
            </telerik:RadHtmlChart>
        </div>
    </div>
</asp:Panel>