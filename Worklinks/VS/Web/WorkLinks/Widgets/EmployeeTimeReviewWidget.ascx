﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeTimeReviewWidget.ascx.cs" Inherits="WorkLinks.Widgets.EmployeeTimeReviewWidget" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<div class="widget">
    <div class="widgetHeader">
        <wasp:WLPLabel ID="EmployeeTimeReviewLabel" runat="server" Text="Employee Time Review" ResourceName="EmployeeTimeReviewLabel" />
    </div>

    <div class="widgetContent">
        <iframe id="WidgetFrame" runat="server" class="w-100" style="border: none; height: 300px;" />
    </div>
</div>