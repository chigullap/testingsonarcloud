﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LabourCostWidget.ascx.cs" Inherits="WorkLinks.Widgets.LabourCostWidget" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<div class="widget">
    <wasp:WLPLabel ID="LabourCostLabel" runat="server" CssClass="widgetHeader" OnPreRender="LabourCostLabel_PreRender" />

    <div class="widgetContent">
        <telerik:RadHtmlChart runat="server" ID="LabourCostChart" Width="100%" Height="300px">
            <PlotArea>
                <Series>
                    <telerik:ColumnSeries Name="Amount" DataFieldY="Amount">
                        <TooltipsAppearance DataFormatString="N" />
                        <LabelsAppearance Visible="false" />
                    </telerik:ColumnSeries>
                </Series>
                <XAxis DataLabelsField="Description">
                    <MajorGridLines Visible="false" />
                    <MinorGridLines Visible="false" />
                </XAxis>
                <YAxis>
                    <LabelsAppearance DataFormatString="N" />
                    <MajorGridLines Visible="false" />
                    <MinorGridLines Visible="false" />
                </YAxis>
            </PlotArea>
        </telerik:RadHtmlChart>
    </div>
</div>