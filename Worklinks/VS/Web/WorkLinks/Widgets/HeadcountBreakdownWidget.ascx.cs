﻿using System;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Telerik.Web.UI.HtmlChart;
using Telerik.Web.UI.HtmlChart.Enums;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Widgets
{
    public partial class HeadcountBreakdownWidget : WLP.Web.UI.WLPUserControl
    {
        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.HeadcountWidget.ViewFlag);

            Initialize();
        }
        private void Initialize()
        {
            SetChartProperties(HeadcountBreakdown1Content, HeadcountBreakdown1Label, HeadcountBreakdown1, Common.ServiceWrapper.HumanResourcesClient.GetHeadcountByOrganizationUnitLevelId(1));
            SetChartProperties(HeadcountBreakdown2Content, HeadcountBreakdown2Label, HeadcountBreakdown2, Common.ServiceWrapper.HumanResourcesClient.GetHeadcountByOrganizationUnitLevelId(2));
            SetChartProperties(HeadcountBreakdown3Content, HeadcountBreakdown3Label, HeadcountBreakdown3, Common.ServiceWrapper.HumanResourcesClient.GetHeadcountByOrganizationUnitLevelId(3));
            SetChartProperties(HeadcountBreakdown4Content, HeadcountBreakdown4Label, HeadcountBreakdown4, Common.ServiceWrapper.HumanResourcesClient.GetHeadcountByOrganizationUnitLevelId(4));
            SetChartProperties(HeadcountBreakdown5Content, HeadcountBreakdown5Label, HeadcountBreakdown5, Common.ServiceWrapper.HumanResourcesClient.GetHeadcountByOrganizationUnitLevelId(5));
            SetChartProperties(HeadcountBreakdown6Content, HeadcountBreakdown6Label, HeadcountBreakdown6, Common.ServiceWrapper.HumanResourcesClient.GetHeadcountByOrganizationUnitLevelId(6));
        }
        protected void SetChartProperties(HtmlGenericControl div, WLPLabel label, RadHtmlChart chart, HeadcountCollection collection)
        {
            if (collection != null && collection.Count > 0)
            {
                div.Visible = true;

                label.Visible = true;
                label.Text = LanguageCode == "FR" ? "Effectif par " + collection[0].ChartTitle.ToLower() : "Headcount by " + collection[0].ChartTitle;

                chart.Visible = true;
                chart.Legend.Appearance.Visible = false;
                chart.PlotArea.Series[0].Appearance.Overlay.Gradient = Gradients.RoundedBevel;
                chart.DataSource = collection;
                chart.DataBind();
            }
        }
        #endregion

        #region event handlers
        protected void HeadcountBreakdownLabel_PreRender(object sender, EventArgs e)
        {
            WLPLabel label = (WLPLabel)sender;
            label.Text = LanguageCode == "FR" ? "Répartition de l'effectif" : "Headcount Breakdown";
        }
        #endregion
    }
}