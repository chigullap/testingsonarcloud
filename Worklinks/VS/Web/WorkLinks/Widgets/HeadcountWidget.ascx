﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeadcountWidget.ascx.cs" Inherits="WorkLinks.Widgets.HeadcountWidget" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<div class="widget">
    <div class="widgetHeader">
        <wasp:WLPLabel ID="HeadcountLabel" runat="server" OnPreRender="HeadcountLabel_PreRender" />
        <div style="float: right;">
            <wasp:WLPButton ID="WLPButton1" runat="server" Icon-PrimaryIconUrl="~/App_Themes/Default/AddWizard.gif" AutoPostBack="false" OnClientClicked="Add" CssClass="button" OnPreRender="NewHireButton_PreRender" />
        </div>
    </div>

    <div class="widgetContent">
        <telerik:RadHtmlChart runat="server" ID="HeadcountChart" Width="100%" Height="300px">
            <PlotArea>
                <Series>
                    <telerik:BarSeries Name="Count" DataFieldY="Count">
                        <LabelsAppearance Visible="false" />
                    </telerik:BarSeries>
                </Series>
                <XAxis DataLabelsField="Description">
                    <MinorGridLines Visible="false" />
                    <MajorGridLines Visible="false" />
                </XAxis>
                <YAxis>
                    <MinorGridLines Visible="false" />
                    <MajorGridLines Visible="false" />
                </YAxis>
            </PlotArea>
        </telerik:RadHtmlChart>
    </div>
</div>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function Add() {
            openWindowWithManager('WidgetWindows', String.format('/HumanResources/EmployeeWizard/Add/{0}/{1}/{2}/{3}/{4}', -1, <%= ViewHireWizardTemplateSelection %>, 'employee', 'false', 'x'), false);
        }
    </script>
</telerik:RadScriptBlock>