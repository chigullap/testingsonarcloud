﻿using System;
using System.Web;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Widgets
{
    public partial class UserManagementWidget : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected UserAdminSearchCriteria Criteria
        {
            get
            {
                UserAdminSearchCriteria criteria = new UserAdminSearchCriteria();
                criteria.LastName = "";
                criteria.UserName = "";
                criteria.WorklinksAdministrationFlag = true;//!Common.ServiceWrapper.SecurityClient.GetSecurityUser(HttpContext.Current.User.Identity.Name.ToString())[0].WorklinksAdministrationFlag;

                return criteria;
            }
        }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.UserManagementWidget.ViewFlag);

            WireEvents();
        }
        protected void WireEvents()
        {
            UserManagementGrid.NeedDataSource += new GridNeedDataSourceEventHandler(UserManagementGrid_NeedDataSource);

            //work around to remove empty rows caused by the OnCommand Telerik bug
            UserManagementGrid.ItemDataBound += new GridItemEventHandler(UserManagementGrid_ItemDataBound);
            UserManagementGrid.PreRender += new EventHandler(UserManagementGrid_PreRender);
            UserManagementGrid.AllowPaging = true;
        }
        #endregion

        #region event handlers
        protected void UserManagementLabel_PreRender(object sender, EventArgs e)
        {
            WLPLabel label = (WLPLabel)sender;
            label.Text = LanguageCode == "FR" ? "Gestion des utilisateurs" : "User Management";
        }
        protected void UserManagementGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            UserSummaryCollection collection = new UserSummaryCollection();
            collection.Load(Common.ServiceWrapper.SecurityClient.GetUserSummary(Criteria));

            Data = collection;
            UserManagementGrid.DataSource = Data;
        }
        protected void UserManagementGrid_PreRender(object sender, EventArgs e)
        {
            //work around to remove empty rows caused by the OnCommand Telerik bug
            if (Data == null)
                UserManagementGrid.AllowPaging = false;
            else
                UserManagementGrid.AllowPaging = true;
        }
        protected void UserManagementGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //work around to remove empty rows caused by the OnCommand Telerik bug
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                if (String.Equals((dataItem["UserName"].Text).ToUpper(), "&NBSP;") && String.Equals((dataItem["LastName"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["FirstName"].Text).ToUpper(), "&NBSP;") && String.Equals((dataItem["LastLoginDatetime"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["LoginExpiryDatetime"].Text).ToUpper(), "&NBSP;") && String.Equals((dataItem["IsLockedOutFlag"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["LastLockoutDatetime"].Text).ToUpper(), "&NBSP;") && String.Equals((dataItem["LastPasswordChangedDatetime"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["PasswordExpiryDatetime"].Text).ToUpper(), "&NBSP;") && String.Equals((dataItem["LastLogoutDatetime"].Text).ToUpper(), "&NBSP;"))
                {
                    e.Item.Display = false;
                }
            }
        }
        #endregion
    }
}