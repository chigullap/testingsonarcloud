﻿using System;
using WLP.Web.UI.Controls;

namespace WorkLinks.Widgets
{
    public partial class LabourCostWidget : WLP.Web.UI.WLPUserControl
    {
        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.LabourCostWidget.ViewFlag);

            Initialize();
        }
        private void Initialize()
        {
            LabourCostChart.Legend.Appearance.Visible = false;
            LabourCostChart.DataSource = Common.ServiceWrapper.HumanResourcesClient.GetLabourCost(DateTime.Now.Year);
            LabourCostChart.DataBind();
        }
        #endregion

        #region event handlers
        protected void LabourCostLabel_PreRender(object sender, EventArgs e)
        {
            WLPLabel label = (WLPLabel)sender;
            label.Text = LanguageCode == "FR" ? "Coût du travail" : "Labour Cost";
        }
        #endregion
    }
}