﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WelcomeWidget.ascx.cs" Inherits="WorkLinks.Widgets.WelcomeWidget" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<div class="widget">
    <div class="widgetHeader">
        <wasp:WLPLabel ID="MyProfileLabel" runat="server" Text="My Profile" ResourceName="MyProfileLabel" />
        <div style="float: right;">
            <wasp:WLPButton ID="EditButton" runat="server" Text="Edit Profile" Icon-PrimaryIconUrl="~/App_Themes/Default/edit.gif" AutoPostBack="false" OnClientClicked="EditProfile" CssClass="button" OnPreRender="EditButton_PreRender" ResourceName="EditButton" />
            <wasp:WLPButton ID="EditContacts" runat="server" Text="Edit Contacts" Icon-PrimaryIconUrl="~/App_Themes/Default/edit.gif" AutoPostBack="false" OnClientClicked="EditContacts" CssClass="button" OnPreRender="EditContactsButton_PreRender" ResourceName="EditContactsButton" />
        </div>
    </div>

    <div class="widgetContent">
         <table width="100%" style="border-spacing: 0px;">
            <tr>
                <td width="25%">
                    <telerik:RadBinaryImage ID="EmployeePhoto" runat="server" CssClass="img-circle" AutoAdjustImageControlSize="false" Width="130px" Height="130px" />
                </td>
                <td width="75%">
                    <table width="100%" style="border-spacing: 0px;">
                        <tr>
                            <td class="profileHeader" width="50%"><b><%= EmployeeProfile.FirstName + " " + EmployeeProfile.LastName %></b></td>
                            <td class="profileHeader" width="50%"><wasp:WLPLabel ID="Birthday" Text="Birth Date" runat="server" ResourceName="Birthday" />: <b><%= EmployeeProfile.BirthDateString %></b></td>
                        </tr>
                        <tr>
                            <td class="profileHeader" width="50%"><wasp:WLPLabel ID="EmployeeNumber" runat="server" Text="Employee #" ResourceName="EmployeeNumber" />: <b><%= EmployeeProfile.EmployeeNumber %></b></td>
                            <td class="profileHeader" width="50%"><wasp:WLPLabel ID="LastLogin" runat="server" Text="Last Login" ResourceName="LastLogin" />: <b><%= EmployeeProfile.LastLoginString %></b></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table width="100%" style="border-spacing: 0px;">
            <tr>
                <td class="profileHeader" width="50%"><wasp:WLPLabel ID="ProcessGroup" runat="server" Text="Process Group" ResourceName="ProcessGroup" />: <b><%= EmployeeProfile.PayrollProcessGroup %></b></td>
                <td class="profileHeader" width="50%"><wasp:WLPLabel ID="Status" runat="server" Text="Status" ResourceName="Status" />: <b><%= EmployeeProfile.Status %></b></td>
            </tr>
            <tr>
                <td class="profileHeader" width="50%"><wasp:WLPLabel ID="HireDate" runat="server" Text="Hire Date" ResourceName="HireDate" />: <b><%= EmployeeProfile.HireDateString %></b></td>
                <td class="profileHeader" width="50%"><wasp:WLPLabel ID="Job" Text="Job" runat="server" ResourceName="Job" />: <b><%= EmployeeProfile.Job %></b></td>
            </tr>
            <tr>
                <td class="profileHeader" width="50%"><wasp:WLPLabel ID="Email" runat="server" Text="Email" ResourceName="Email" />: <b><%= EmployeeProfile.EmailAddress %></b></td>
                <td class="profileHeader" width="50%"><wasp:WLPLabel ID="Phone" runat="server" Text="Phone" ResourceName="Phone" />: <b><%= EmployeeProfile.PhoneNumber %></b></td>
            </tr>
            <tr>
                <td class="profileHeader" width="100%" colspan="2"><wasp:WLPLabel ID="Address" runat="server" Text="Address" ResourceName="Address" />: <b><%= EmployeeProfile.Address %></b></td>
            </tr>
        </table>
    </div>
</div>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function EditProfile() {
            openWindowWithManager('WidgetWindows', String.format('/HumanResources/Employee/View/{0}/{1}/{2}', '<%= EmployeeProfile.EmployeeId %>', '<%= EmployeeProfile.LastName %>', '<%= EmployeeProfile.FirstName %>'), false);
        }

        function EditContacts() {
            openWindowWithManager('WidgetWindows', String.format('/HumanResources/Contact/{0}/{1}/{2}', '<%= EmployeeProfile.EmployeeId %>', '<%= EmployeeProfile.LastName %>', '<%= EmployeeProfile.FirstName %>'), false);
        }
    </script>
</telerik:RadScriptBlock>