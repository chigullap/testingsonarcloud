﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VacationSickTimeWidget.ascx.cs" Inherits="WorkLinks.Widgets.VacationSickTimeWidget" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<div class="widget">
    <div class="widgetHeader">
        <wasp:WLPLabel ID="VacationSickTimeLabel" runat="server" Text="Vacation Sick Time" ResourceName="VacationSickTimeLabel" />
    </div>

    <div class="widgetContent">
        <iframe id="WidgetFrame" runat="server" class="w-100" style="border: none; height: 300px;" />
    </div>
</div>