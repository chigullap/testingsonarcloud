﻿using System;
using Telerik.Web.UI;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks
{
    public partial class EmployeePayslipWidget : WLP.Web.UI.WLPUserControl
    {
        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EmployeePayslipWidget.ViewFlag);

            WireEvents();

            Search();
        }
        protected void WireEvents()
        {
            EmployeePayslipGrid.NeedDataSource += new GridNeedDataSourceEventHandler(EmployeePayslipGrid_NeedDataSource);
            EmployeePayslipGrid.PreRender += new EventHandler(EmployeePayslipGrid_PreRender);
            EmployeePayslipGrid.AllowPaging = true;
        }
        public void Search()
        {
            EmployeePayslipCollection collection = new EmployeePayslipCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetEmployeePayslip());
            Data = collection;

            EmployeePayslipGrid.Rebind();
        }
        #endregion

        #region event handlers
        protected void EmployeePayslipLabel_PreRender(object sender, EventArgs e)
        {
            WLPLabel label = (WLPLabel)sender;
            label.Text = LanguageCode == "FR" ? "Fiches de paie" : "Payslips";
        }
        void EmployeePayslipGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EmployeePayslipGrid.DataSource = Data;
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        void EmployeePayslipGrid_PreRender(object sender, EventArgs e)
        {
            //work around to remove empty rows caused by the OnCommand Telerik bug
            if (Data == null)
                EmployeePayslipGrid.AllowPaging = false;
            else
                EmployeePayslipGrid.AllowPaging = true;
        }
        #endregion
    }
}