﻿using System;

namespace WorkLinks.Widgets
{
    public partial class VacationSickTimeWidget : WLP.Web.UI.WLPUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.VacationSickTimeWidget.ViewFlag);

            WidgetFrame.Src = System.Configuration.ConfigurationManager.AppSettings.Get("ANGULARURL") + "/widget/vac-sick-time";
        }
    }
}