﻿using System;
using Telerik.Web.UI;
using WLP.BusinessLayer.BusinessObjects;
using WLP.Web.UI.Controls;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks
{
    public partial class EmployeeListWidget : WLP.Web.UI.WLPUserControl
    {
        #region properties
        protected EmployeeCriteria Criteria
        {
            get
            {
                EmployeeCriteria criteria = new EmployeeCriteria();
                criteria.EmployeeNumber = "";
                criteria.SocialInsuranceNumber = "";
                criteria.FirstName = "";
                criteria.LastName = "";
                criteria.OrganizationUnitName = "";
                criteria.PayrollProcessGroupCode = "";
                criteria.LanguageCode = LanguageCode;
                criteria.IsWizardSearch = false;
                criteria.UseLikeStatementInProcSelect = true;
                criteria.PageSize = CurrentPageSize;
                criteria.PageIndex = CurrentPageIndex;
                criteria.SortColumn = SortColumn;
                criteria.SortDirection = SortDirection;
                criteria.EmployeePositionStatusCode = new CodeCollection();

                return criteria;
            }
        }
        private String SortDirection
        {
            get
            {
                String sortDirection = String.Empty;

                if ((EmployeeListGrid != null && EmployeeListGrid.MasterTableView != null && EmployeeListGrid.MasterTableView.SortExpressions.Count > 0))
                {
                    switch (EmployeeListGrid.MasterTableView.SortExpressions[0].SortOrder)
                    {
                        case GridSortOrder.Ascending:
                            sortDirection = "asc";
                            break;
                        case GridSortOrder.Descending:
                            sortDirection = "desc";
                            break;
                    }
                }

                return sortDirection;
            }
        }
        private String SortColumn
        {
            get
            {
                if (EmployeeListGrid != null && EmployeeListGrid.MasterTableView != null && EmployeeListGrid.MasterTableView.SortExpressions.Count > 0)
                    return EmployeeListGrid.MasterTableView.SortExpressions[0].FieldName;
                else
                    return "EmployeeNumber"; //business class property name, sql layer will change it to employee_number
            }
        }
        private int CurrentPageIndex
        {
            get { return EmployeeListGrid.CurrentPageIndex; }
            set { EmployeeListGrid.CurrentPageIndex = value; }
        }
        private int CurrentPageSize { get { return EmployeeListGrid.PageSize; } }
        #endregion

        #region main
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetAuthorized(Common.Security.RoleForm.EmployeeListWidget.ViewFlag);

            WireEvents();
        }
        protected void WireEvents()
        {
            EmployeeListGrid.NeedDataSource += new GridNeedDataSourceEventHandler(EmployeeListGrid_NeedDataSource);

            //work around to remove empty rows caused by the OnCommand Telerik bug
            EmployeeListGrid.ItemDataBound += new GridItemEventHandler(EmployeeListGrid_ItemDataBound);
            EmployeeListGrid.AllowPaging = true;
        }
        void EmployeeListGrid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            EmployeeSummaryCollection collection = new EmployeeSummaryCollection();
            collection.Load(Common.ServiceWrapper.HumanResourcesClient.GetEmployeeSummary(Criteria));

            if (collection.Count > 0)
                EmployeeListGrid.VirtualItemCount = Convert.ToInt32(collection[0].NumberOfRows);
            else
                EmployeeListGrid.VirtualItemCount = 0;

            Data = collection;
            EmployeeListGrid.DataSource = Data;
        }
        #endregion

        #region event handlers
        protected void EmployeeListLabel_PreRender(object sender, EventArgs e)
        {
            WLPLabel label = (WLPLabel)sender;
            label.Text = LanguageCode == "FR" ? "Des employés" : "Employees";
        }
        protected void Code_NeedDataSource(object sender, EventArgs e)
        {
            Common.CodeHelper.PopulateControl((ICodeControl)sender, LanguageCode);
        }
        protected void EmployeeListGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //work around to remove empty rows caused by the OnCommand Telerik bug
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                if (String.Equals((dataItem["EmployeeNumber"].Text).ToUpper(), "&NBSP;") && String.Equals((dataItem["LastName"].Text).ToUpper(), "&NBSP;") &&
                    String.Equals((dataItem["FirstName"].Text).ToUpper(), "&NBSP;") && String.Equals((dataItem["OrganizationUnitDescription"].Text).ToUpper(), "&NBSP;"))
                {
                    e.Item.Display = false;
                }
            }
        }
        protected void EmployeeListGrid_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            int i = 0;
        }
        protected void EmployeeListGrid_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            int i = 0;
        }
        protected void EmployeeListGrid_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            int i = 0;
        }
        #endregion
    }
}