﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeePayslipWidget.ascx.cs" Inherits="WorkLinks.EmployeePayslipWidget" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="SearchPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SearchPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="SearchPanel" runat="server">
    <div class="widget">
        <wasp:WLPLabel ID="EmployeePayslipLabel" runat="server" CssClass="widgetHeader" OnPreRender="EmployeePayslipLabel_PreRender" />

        <div class="widgetContent">
            <wasp:WLPGrid
                ID="EmployeePayslipGrid"
                runat="server"
                AllowPaging="true"
                PagerStyle-AlwaysVisible="true"
                PageSize="100"
                Height="300px"
                AllowSorting="true"
                GridLines="None"
                AutoAssignModifyProperties="true">

                <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                    <ClientEvents OnRowDblClick="OnEmployeePayslipListRowDblClick" OnRowClick="OnEmployeePayslipListRowClick" OnGridCreated="OnEmployeePayslipListGridCreated" />
                </ClientSettings>

                <MasterTableView ClientDataKeyNames="PayrollProcessId, EmployeeNumber, EmployeeId" AutoGenerateColumns="false" CommandItemDisplay="Top">
                    <CommandItemTemplate></CommandItemTemplate>

                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="ChequeDate" SortOrder="Descending" />
                    </SortExpressions>

                    <Columns>
                        <wasp:GridDateTimeControl DataField="CutoffDate" SortExpression="CutoffDate" UniqueName="CutoffDate" ResourceName="CutoffDate" />
                        <wasp:GridDateTimeControl DataField="ChequeDate" SortExpression="ChequeDate" UniqueName="ChequeDate" ResourceName="ChequeDate" />
                        <wasp:GridNumericControl DataField="TotalGross" LabelText="**TotalGross**" DataFormatString="{0:$###,##0.00}" SortExpression="TotalGross" UniqueName="TotalGross" ResourceName="TotalGross" />
                        <wasp:GridNumericControl DataField="TotalDeductions" LabelText="**TotalDeductions**" DataFormatString="{0:$###,##0.00}" SortExpression="TotalDeductions" UniqueName="TotalDeductions" ResourceName="TotalDeductions" />
                        <wasp:GridNumericControl DataField="TotalTaxes" LabelText="**TotalTaxes**" DataFormatString="{0:$###,##0.00}" SortExpression="TotalTaxes" UniqueName="TotalTaxes" ResourceName="TotalTaxes" />
                        <wasp:GridNumericControl DataField="NetPay" LabelText="**NetPay**" DataFormatString="{0:$###,##0.00}" SortExpression="NetPay" UniqueName="NetPay" ResourceName="NetPay" />
                    </Columns>
                </MasterTableView>

                <HeaderContextMenu EnableAutoScroll="true" />
            </wasp:WLPGrid>
        </div>
    </div>
</asp:Panel>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var payrollProcessId;
        var employeeNumber;
        var employeeId;

        function GetSelectedRow() {
            var grid = $find('<%= EmployeePayslipGrid.ClientID %>');

            if (grid != null) {
                var masterTable = grid.get_masterTableView();
                if (masterTable != null)
                    return masterTable.get_selectedItems()[0];
                else
                    return null;
            }

            return null;
        }

        function RowIsSelected() {
            var getSeletecteRow = GetSelectedRow();
            return (getSeletecteRow != null);
        }

        function OnEmployeePayslipListRowClick(sender, eventArgs) {
            payrollProcessId = eventArgs.getDataKeyValue('PayrollProcessId');
            employeeNumber = eventArgs.getDataKeyValue('EmployeeNumber');
            employeeId = eventArgs.getDataKeyValue('EmployeeId');
        }

        function OnEmployeePayslipListGridCreated(sender, eventArgs) {
            if (RowIsSelected()) {
                var selectedRow = GetSelectedRow();

                payrollProcessId = selectedRow.getDataKeyValue('PayrollProcessId');
                employeeNumber = selectedRow.getDataKeyValue('EmployeeNumber');
                employeeId = selectedRow.getDataKeyValue('EmployeeId');
            }
            else {
                payrollProcessId = null;
                employeeNumber = null;
                employeeId = null;
            }
        }

        function OnEmployeePayslipListRowDblClick(sender, eventArgs) {
            if (payrollProcessId != null && employeeNumber != null)
                openWindowWithManager('WidgetWindows', String.format('/Report/EmployeePaySlip/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}', payrollProcessId, employeeNumber, "PDF", 'x', 'x', 'x', 'x', employeeId), true);
        }
    </script>
</telerik:RadScriptBlock>