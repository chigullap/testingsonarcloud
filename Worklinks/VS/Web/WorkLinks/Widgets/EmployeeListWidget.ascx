﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeListWidget.ascx.cs" Inherits="WorkLinks.EmployeeListWidget" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="WLP.Web.UI" Namespace="WLP.Web.UI.Controls" TagPrefix="wasp" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="SearchPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SearchPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:Panel ID="SearchPanel" runat="server">
    <div class="widget">
        <wasp:WLPLabel ID="EmployeeListLabel" runat="server" CssClass="widgetHeader" OnPreRender="EmployeeListLabel_PreRender" />

        <div class="widgetContent">
            <wasp:WLPGrid
                ID="EmployeeListGrid"
                runat="server"
                AllowPaging="true"
                PagerStyle-AlwaysVisible="true"
                PageSize="100"
                Height="300px"
                AllowSorting="true"
                GridLines="None"
                AutoAssignModifyProperties="true"
                OnPageIndexChanged="EmployeeListGrid_PageIndexChanged"
                OnPageSizeChanged="EmployeeListGrid_PageSizeChanged"
                OnSortCommand="EmployeeListGrid_SortCommand">

                <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                    <ClientEvents OnRowDblClick="OnEmployeeListRowDblClick" OnRowClick="OnEmployeeListRowClick" OnGridCreated="OnEmployeeListGridCreated" />
                </ClientSettings>

                <MasterTableView
                    AllowCustomSorting="true"
                    AllowCustomPaging="true"
                    ClientDataKeyNames="EmployeeId, LastName, FirstName"
                    AutoGenerateColumns="false"
                    DataKeyNames="EmployeeId"
                    CommandItemDisplay="Top">

                    <CommandItemTemplate></CommandItemTemplate>
                    <Columns>
                        <wasp:GridBoundControl DataField="EmployeeNumber" LabelText="**EmployeeNumber**" SortExpression="EmployeeNumber" UniqueName="EmployeeNumber" ResourceName="EmployeeNumber" />
                        <wasp:GridBoundControl DataField="LastName" LabelText="**LastName**" SortExpression="LastName" UniqueName="LastName" ResourceName="LastName" />
                        <wasp:GridBoundControl DataField="FirstName" LabelText="**FirstName**" SortExpression="FirstName" UniqueName="FirstName" ResourceName="FirstName" />
                        <wasp:GridBoundControl DataField="CodeEmployeePositionStatusDescription" LabelText="**CodeEmployeePositionStatusDescription**" SortExpression="CodeEmployeePositionStatusDescription" UniqueName="CodeEmployeePositionStatusDescription" ResourceName="CodeEmployeePositionStatusDescription" />
                        <wasp:GridKeyValueControl OnNeedDataSource="Code_NeedDataSource" LabelText="**PayrollProcessGroupCode**" SortExpression="PayrollProcessGroupCode" DataField="PayrollProcessGroupCode" Type="PayrollProcessGroupCode" ResourceName="PayrollProcessGroupCode" />
                        <wasp:GridBoundControl DataField="OrganizationUnitDescription" LabelText="**OrganizationUnitDescription**" SortExpression="OrganizationUnitDescription" UniqueName="OrganizationUnitDescription" ResourceName="OrganizationUnitDescription">
                            <HeaderStyle Width="36%" />
                        </wasp:GridBoundControl>
                    </Columns>

                </MasterTableView>

                <HeaderContextMenu EnableAutoScroll="true" />
            </wasp:WLPGrid>
        </div>
    </div>
</asp:Panel>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function GetSelectedRow() {
            var grid = $find('<%= EmployeeListGrid.ClientID %>');

            if (grid != null) {
                var masterTable = grid.get_masterTableView();
                if (masterTable != null)
                    return masterTable.get_selectedItems()[0];
                else
                    return null;
            }

            return null;
        }

        function RowIsSelected() {
            var getSeletecteRow = GetSelectedRow();
            return (getSeletecteRow != null);
        }

        function OnEmployeeListRowClick(sender, eventArgs) {
            employeeId = eventArgs.getDataKeyValue('EmployeeId');
            lastName = eventArgs.getDataKeyValue('LastName');
            firstName = eventArgs.getDataKeyValue('FirstName');
        }

        function OnEmployeeListRowDblClick(sender, eventArgs) {
            if (employeeId != null)
                openWindowWithManager('WidgetWindows', String.format('/HumanResources/Employee/View/{0}/{1}/{2}', employeeId, lastName, firstName), false);
        }

        function OnEmployeeListGridCreated(sender, eventArgs) {
            if (RowIsSelected()) {
                var selectedRow = GetSelectedRow();

                employeeId = selectedRow.getDataKeyValue('EmployeeId');
                lastName = selectedRow.getDataKeyValue('LastName');
                firstName = selectedRow.getDataKeyValue('FirstName');
            }
            else {
                employeeId = null;
                lastName = "";
                firstName = "";
            }
        }
    </script>
</telerik:RadScriptBlock>