﻿using System;
using System.Collections.Generic;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Web.BusinessService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "HumanResourcesReference" in code, svc and config file together.
    public class HumanResources : Contract.IHumanResources
    {
        #region fields
        private BusinessLayer.BusinessLogic.GrievanceManagement _grievanceManagement;
        private BusinessLayer.BusinessLogic.EmployeeManagement _employeeManagement;
        private BusinessLayer.BusinessLogic.PersonManagement _personManagement;
        private BusinessLayer.BusinessLogic.LabourUnionManagement _labourUnionManagement;
        private BusinessLayer.BusinessLogic.OrganizationUnitManagement _organzationUnitManagement;
        private BusinessLayer.BusinessLogic.WorkLinksDynamicsManagement _dynamicsManagement;
        private BusinessLayer.BusinessLogic.PayrollManagement _payrollManagement;
        private BusinessLayer.BusinessLogic.WizardManagement _wizardManagement;
        private BusinessLayer.BusinessLogic.ScreenControlManagement _screenControlManagement;
        private BusinessLayer.BusinessLogic.XmlManagement _xmlManagement;
        private BusinessLayer.BusinessLogic.ReportManagement _reportManagement;
        private BusinessLayer.BusinessLogic.YearEndManagement _yearEndManagement;
        private BusinessLayer.BusinessLogic.AccrualManagement _accrualManagement;
        private BusinessLayer.BusinessLogic.EmailManagement _emailManagement;
        private BusinessLayer.BusinessLogic.WidgetManagement _widgetManagement;
        private BusinessLayer.BusinessLogic.BenefitManagement _benefitManagement;
        #endregion

        #region properties
        private BusinessLayer.BusinessLogic.GrievanceManagement GrievanceManagement
        {
            get
            {
                if (_grievanceManagement == null)
                    _grievanceManagement = new BusinessLayer.BusinessLogic.GrievanceManagement();

                return _grievanceManagement;
            }
        }
        private BusinessLayer.BusinessLogic.EmployeeManagement EmployeeManagement
        {
            get
            {
                if (_employeeManagement == null)
                    _employeeManagement = new BusinessLayer.BusinessLogic.EmployeeManagement();

                return _employeeManagement;
            }
        }
        private BusinessLayer.BusinessLogic.PersonManagement PersonManagement
        {
            get
            {
                if (_personManagement == null)
                    _personManagement = new BusinessLayer.BusinessLogic.PersonManagement();

                return _personManagement;
            }
        }
        private BusinessLayer.BusinessLogic.LabourUnionManagement LabourUnionManagement
        {
            get
            {
                if (_labourUnionManagement == null)
                    _labourUnionManagement = new BusinessLayer.BusinessLogic.LabourUnionManagement();

                return _labourUnionManagement;
            }
        }
        private BusinessLayer.BusinessLogic.OrganizationUnitManagement OrganizationUnitManagement
        {
            get
            {
                if (_organzationUnitManagement == null)
                    _organzationUnitManagement = new BusinessLayer.BusinessLogic.OrganizationUnitManagement();

                return _organzationUnitManagement;
            }
        }
        private BusinessLayer.BusinessLogic.WorkLinksDynamicsManagement DynamicsManagement
        {
            get
            {
                if (_dynamicsManagement == null)
                    _dynamicsManagement = new BusinessLayer.BusinessLogic.WorkLinksDynamicsManagement();

                return _dynamicsManagement;
            }
        }
        private BusinessLayer.BusinessLogic.PayrollManagement PayrollManagement
        {
            get
            {
                if (_payrollManagement == null)
                    _payrollManagement = new BusinessLayer.BusinessLogic.PayrollManagement();

                return _payrollManagement;
            }
        }
        private BusinessLayer.BusinessLogic.WizardManagement WizardManagement
        {
            get
            {
                if (_wizardManagement == null)
                    _wizardManagement = new BusinessLayer.BusinessLogic.WizardManagement();

                return _wizardManagement;
            }
        }
        private BusinessLayer.BusinessLogic.ScreenControlManagement ScreenControlManagement
        {
            get
            {
                if (_screenControlManagement == null)
                    _screenControlManagement = new BusinessLayer.BusinessLogic.ScreenControlManagement();

                return _screenControlManagement;
            }
        }
        private BusinessLayer.BusinessLogic.XmlManagement XmlManagement
        {
            get
            {
                if (_xmlManagement == null)
                    _xmlManagement = new BusinessLayer.BusinessLogic.XmlManagement();

                return _xmlManagement;
            }
        }
        private BusinessLayer.BusinessLogic.ReportManagement ReportManagement
        {
            get
            {
                if (_reportManagement == null)
                    _reportManagement = new BusinessLayer.BusinessLogic.ReportManagement();

                return _reportManagement;
            }
        }
        private BusinessLayer.BusinessLogic.YearEndManagement YearEndManagement
        {
            get
            {
                if (_yearEndManagement == null)
                    _yearEndManagement = new BusinessLayer.BusinessLogic.YearEndManagement();

                return _yearEndManagement;
            }
        }
        private BusinessLayer.BusinessLogic.AccrualManagement AccrualManagement
        {
            get
            {
                if (_accrualManagement == null)
                    _accrualManagement = new BusinessLayer.BusinessLogic.AccrualManagement();

                return _accrualManagement;
            }
        }
        private BusinessLayer.BusinessLogic.EmailManagement EmailManagement
        {
            get
            {
                if (_emailManagement == null)
                    _emailManagement = new BusinessLayer.BusinessLogic.EmailManagement();

                return _emailManagement;
            }
        }
        private BusinessLayer.BusinessLogic.WidgetManagement WidgetManagement
        {
            get
            {
                if (_widgetManagement == null)
                    _widgetManagement = new BusinessLayer.BusinessLogic.WidgetManagement();

                return _widgetManagement;
            }
        }
        private BusinessLayer.BusinessLogic.BenefitManagement BenefitManagement
        {
            get
            {
                if (_benefitManagement == null)
                    _benefitManagement = new BusinessLayer.BusinessLogic.BenefitManagement();

                return _benefitManagement;
            }
        }
        #endregion

        #region employee biographical
        public String GetEmployeeNumber(DatabaseUser user, long employeeId)
        {
            return EmployeeManagement.GetEmployeeNumber(user, employeeId);
        }
        public EmployeeCollection GetEmployee(DatabaseUser user, EmployeeCriteria criteria)
        {
            return EmployeeManagement.GetEmployee(user, criteria);
        }
        public Employee UpdateEmployee(DatabaseUser user, Employee employee, List<ContactType> deletedItems)
        {
            EmployeeManagement.UpdateEmployee(user, employee, deletedItems);
            return employee;
        }
        public Employee InsertEmployee(DatabaseUser user, Employee employee)
        {
            EmployeeManagement.InsertEmployee(user, employee);
            return employee;
        }
        public void DeleteEmployee(DatabaseUser user, Employee employee)
        {
            EmployeeManagement.DeleteEmployee(user, employee);
        }
        //public String CheckSinDuplicateFlag(DatabaseUser user)
        //{
        //    return EmployeeManagement.CheckSinDuplicateFlag(DatabaseUser user);
        //}
        //public String SinRule(DatabaseUser user)
        //{
        //    return EmployeeManagement.SinRule(DatabaseUser user);
        //}
        public bool CheckDatabaseForDuplicateSin(DatabaseUser user, string sin, long employeeId, string employeeNumber)
        {
            return EmployeeManagement.CheckDatabaseForDuplicateSin(user, sin, employeeId, employeeNumber);
        }
        public bool ValidateSIN(DatabaseUser user, String SIN, String governmentIdentificationNumberTypeCode)
        {
            return EmployeeManagement.ValidateSIN(user, SIN, governmentIdentificationNumberTypeCode);
        }
        #endregion

        #region employee skill
        public EmployeeSkillCollection GetEmployeeSkill(DatabaseUser user, long employeeId)
        {
            return EmployeeManagement.GetEmployeeSkill(user, employeeId);
        }
        public EmployeeSkill UpdateEmployeeSkill(DatabaseUser user, EmployeeSkill skill)
        {
            EmployeeManagement.UpdateEmployeeSkill(user, skill);
            return skill;
        }
        public void DeleteEmployeeSkill(DatabaseUser user, EmployeeSkill skill)
        {
            EmployeeManagement.DeleteEmployeeSkill(user, skill);
        }
        public EmployeeSkill InsertEmployeeSkill(DatabaseUser user, EmployeeSkill skill)
        {
            EmployeeManagement.InsertEmployeeSkill(user, skill);
            return skill;
        }
        #endregion

        #region employee course
        public EmployeeCourseCollection GetEmployeeCourse(DatabaseUser user, long employeeId)
        {
            return EmployeeManagement.GetEmployeeCourse(user, employeeId);
        }
        public EmployeeCourse UpdateEmployeeCourse(DatabaseUser user, EmployeeCourse course)
        {
            EmployeeManagement.UpdateEmployeeCourse(user, course);
            return course;
        }
        public void DeleteEmployeeCourse(DatabaseUser user, EmployeeCourse course)
        {
            EmployeeManagement.DeleteEmployeeCourse(user, course);
        }
        public EmployeeCourse InsertEmployeeCourse(DatabaseUser user, EmployeeCourse course)
        {
            EmployeeManagement.InsertEmployeeCourse(user, course);
            return course;
        }
        #endregion

        #region employee membership
        public EmployeeMembershipCollection GetEmployeeMembership(DatabaseUser user, long employeeId)
        {
            return EmployeeManagement.GetEmployeeMembership(user, employeeId);
        }
        public EmployeeMembership InsertEmployeeMembership(DatabaseUser user, EmployeeMembership membership)
        {
            EmployeeManagement.InsertEmployeeMembership(user, membership);
            return membership;
        }
        public EmployeeMembership UpdateEmployeeMembership(DatabaseUser user, EmployeeMembership membership)
        {
            EmployeeManagement.UpdateEmployeeMembership(user, membership);
            return membership;
        }
        public void DeleteEmployeeMembership(DatabaseUser user, EmployeeMembership membership)
        {
            EmployeeManagement.DeleteEmployeeMembership(user, membership);
        }
        #endregion

        #region Pension Export
        public byte[] CreatePensionExportFile(DatabaseUser user, long payrollProcessId)
        {
            return DynamicsManagement.CreatePensionExportFile(user, payrollProcessId);
        }
        #endregion

        #region HSBC EFT
        public byte[] CreateHSBCEft(DatabaseUser user, long payrollProcessId, string employeeNumber, string eftType)
        {
            return DynamicsManagement.CreateHSBCEft(user, payrollProcessId, employeeNumber, eftType);
        }
        #endregion

        #region CITI EFT
        public byte[] CreateCITIeft(DatabaseUser user, long payrollProcessId, String eftClientNumber, String employeeNumber, string eftType)
        {
            return DynamicsManagement.CreateCITIeft(user, payrollProcessId, eftClientNumber, employeeNumber, eftType);
        }
        public byte[] CreateCitiChequeFile(DatabaseUser user, long payrollProcessId, String eftClientNumber, String companyName, String employeeNumber, string originatorId, string eftType)
        {
            return DynamicsManagement.CreateCitiChequeFile(user, payrollProcessId, eftClientNumber, companyName, employeeNumber, originatorId, eftType);
        }
        public byte[] CreateCitiAchFile(DatabaseUser user, long payrollProcessId, String eftClientNumber, String companyName, String companyShortName, String employeeNumber, string eftType)
        {
            return DynamicsManagement.CreateCitiAchFile(user, payrollProcessId, eftClientNumber, companyName, companyShortName, employeeNumber, eftType);
        }
        public byte[] CreateCitiEftAndChequeFile(DatabaseUser user, long payrollProcessId, String citiEftFileName, String citiChequeFileName, String eftClientNumber, String citiChequeAccountNumber, String companyName, String companyShortName, String employeeNumber, string originatorId, string eftType)
        {
            return DynamicsManagement.CreateCitiEftAndChequeFile(user, payrollProcessId, citiEftFileName, citiChequeFileName, eftClientNumber, citiChequeAccountNumber, companyName, companyShortName, employeeNumber, originatorId, eftType);
        }
        public String GetCitiNamingFormat(DatabaseUser user, String eftFileNameFormat, long payrollprocessid)
        {
            return DynamicsManagement.GetCitiNamingFormat(user, eftFileNameFormat, payrollprocessid);
        }
        #endregion

        #region RRSP Export
        public byte[] CreateRRSPExport(DatabaseUser user, long payrollProcessId, String rrspHeader, String rrspGroupNumber)
        {
            return DynamicsManagement.CreateRRSPExport(user, payrollProcessId, rrspHeader, rrspGroupNumber);
        }
        #endregion

        #region CIC Plus Weekly Export
        public byte[] CreateCicPlusWeeklyExportFile(DatabaseUser user, decimal periodYear)
        {
            return DynamicsManagement.CreateCicPlusWeeklyExportFile(user, periodYear);
        }
        #endregion

        #region Empower Remittance Export and Garnishment Export
        public byte[] CreateEmpowerRemittanceExport(DatabaseUser user, long payrollProcessId, String revenueQuebecTaxId, bool useQuebecTaxId)
        {
            return DynamicsManagement.CreateEmpowerRemittanceExport(user, payrollProcessId, revenueQuebecTaxId, useQuebecTaxId);
        }
        public byte[] CreateGarnishmentExportFile(DatabaseUser user, long payrollProcessId, String empowerFein)
        {
            return DynamicsManagement.CreateGarnishmentExportFile(user, payrollProcessId, empowerFein);
        }
        #endregion

        #region social costs export

        public byte[] CreateSocialCostsExport(DatabaseUser user, long payrollProcessId)
        {
            return DynamicsManagement.CreateSocialCostsExport(user, payrollProcessId);
        }

        #endregion

        #region REAL Custom Export Data

        public byte[] CreateRealCustomExport(DatabaseUser user, long payrollProcessId)
        {
            return DynamicsManagement.CreateRealCustomExport(user, payrollProcessId);
        }        

        #endregion

        #region RBC EDI EFT
        public byte[] GenerateRBCEftEdiFile(DatabaseUser user, long payrollProcessId, RbcEftEdiParameters ediParms, string employeeNumber)
        {
            return DynamicsManagement.GenerateRBCEftEdiFile(user, payrollProcessId, ediParms, employeeNumber);
        }
        #endregion

        #region SCOTIABANK EFT
        public byte[] GenerateScotiaBankEdiFile(DatabaseUser user, long payrollProcessId, ScotiaBankEdiParameters ediParms, string employeeNumber)
        {
            return DynamicsManagement.GenerateScotiaBankEdiFile(user, payrollProcessId, ediParms, employeeNumber);
        }
        #endregion
        

        #region CRA GARNISHMENT EDI
        public byte[] CreateCraEftGarnishmentFile(DatabaseUser user, long payrollProcessId, RbcEftSourceDeductionParameters garnishmentParms, string employeeNumber)
        {
            return DynamicsManagement.GenerateCraEftGarnishmentFile(user, payrollProcessId, garnishmentParms, employeeNumber);
        }
        #endregion

        #region RBC ACH EFT
        public byte[] GenerateEftFile(DatabaseUser user, long payrollProcessId, String eftClientNumber, bool eftTransmissionFlag, String eftFileIndicator, String companyName, String companyShortName, String employeeNumber, string eftType)
        {
            return DynamicsManagement.GenerateEftFile(user, payrollProcessId, eftClientNumber, eftTransmissionFlag, eftFileIndicator, companyName, companyShortName, employeeNumber, eftType);
        }
        #endregion

        #region employee banking
        public EmployeeBankingCollection GetEmployeeBanking(DatabaseUser user, long employeeId)
        {
            return EmployeeManagement.GetEmployeeBanking(user, employeeId);
        }
        public EmployeeBanking UpdateEmployeeBanking(DatabaseUser user, EmployeeBanking bank)
        {
            EmployeeManagement.UpdateEmployeeBanking(user, bank);
            return bank;
        }
        public void DeleteEmployeeBanking(DatabaseUser user, EmployeeBanking bank)
        {
            EmployeeManagement.DeleteEmployeeBanking(user, bank);
        }
        public EmployeeBanking InsertEmployeeBanking(DatabaseUser user, EmployeeBanking bank)
        {
            return EmployeeManagement.InsertEmployeeBanking(user, bank);
        }
        #endregion

        #region employee education
        public EmployeeEducationCollection GetEmployeeEducation(DatabaseUser user, long employeeId)
        {
            return EmployeeManagement.GetEmployeeEducation(user, employeeId);
        }
        public EmployeeEducation UpdateEmployeeEducation(DatabaseUser user, EmployeeEducation education)
        {
            EmployeeManagement.UpdateEmployeeEducation(user, education);
            return education;
        }
        public EmployeeEducation InsertEmployeeEducation(DatabaseUser user, EmployeeEducation education)
        {
            return EmployeeManagement.InsertEmployeeEducation(user, education);
        }
        public EmployeeEducation DeleteEmployeeEducation(DatabaseUser user, EmployeeEducation education)
        {
            EmployeeManagement.DeleteEmployeeEducation(user, education);
            return education;
        }
        #endregion

        #region employee companyproperty
        public EmployeeCompanyPropertyCollection GetEmployeeCompanyProperty(DatabaseUser user, long employeeId)
        {
            return EmployeeManagement.GetEmployeeCompanyProperty(user, employeeId);
        }
        public EmployeeCompanyProperty UpdateEmployeeCompanyProperty(DatabaseUser user, EmployeeCompanyProperty property)
        {
            EmployeeManagement.UpdateEmployeeCompanyProperty(user, property);
            return property;
        }
        public EmployeeCompanyProperty InsertEmployeeCompanyProperty(DatabaseUser user, EmployeeCompanyProperty property)
        {
            return EmployeeManagement.InsertEmployeeCompanyProperty(user, property);
        }
        public EmployeeCompanyProperty DeleteEmployeeCompanyProperty(DatabaseUser user, EmployeeCompanyProperty property)
        {
            EmployeeManagement.DeleteEmployeeCompanyProperty(user, property);
            return property;
        }
        #endregion

        #region employee contact
        public ContactCollection GetContact(DatabaseUser user, long employeeId, long? contactId)
        {
            return EmployeeManagement.GetContact(user, employeeId, contactId);
        }
        public Contact UpdateContact(DatabaseUser user, Contact contact)
        {
            EmployeeManagement.UpdateContact(user, contact);
            return contact;
        }
        public Contact InsertContact(DatabaseUser user, Contact contact)
        {
            EmployeeManagement.InsertContact(user, contact);
            return contact;
        }
        public void DeleteContact(DatabaseUser user, Contact contact)
        {
            EmployeeManagement.DeleteContact(user, contact);
        }
        #endregion

        #region employee contact type
        public ContactTypeCollection GetContactType(DatabaseUser user, long employeeId, String contactTypeCode)
        {
            return EmployeeManagement.GetContactType(user, employeeId, contactTypeCode);
        }
        public void UpdateContactType(DatabaseUser user, ContactTypeCollection contactTypes)
        {
            //EmployeeManagement.UpdateContactType(DatabaseUser user, contactTypes);
        }
        public ContactType InsertContactType(DatabaseUser user, ContactType contactType)
        {
            return EmployeeManagement.InsertContactType(user, contactType);
        }
        public void DeleteContactType(DatabaseUser user, ContactType contactType)
        {
            EmployeeManagement.DeleteContactType(user, contactType);
        }
        #endregion

        #region person
        public PersonCollection GetPerson(DatabaseUser user, long personId)
        {
            return PersonManagement.GetPerson(user, personId);
        }
        public void UpdatePerson(DatabaseUser user, Person person)
        {
            PersonManagement.UpdatePerson(user, person);
        }
        public Person UpdateUnionPerson(DatabaseUser user, Person person)
        {
            PersonManagement.UpdateUnionContactPerson(user, person);
            return person;
        }
        public void DeletePerson(DatabaseUser user, Person person)
        {
            PersonManagement.DeletePerson(user, person);
        }
        public Person InsertPerson(DatabaseUser user, Person person)
        {
            PersonManagement.InsertPerson(user, user.DatabaseName, person);
            return person;
        }
        #endregion

        #region person address
        public PersonAddressCollection GetPersonAddress(DatabaseUser user, long personId)
        {
            return PersonManagement.GetPersonAddress(user, personId);
        }
        public AddressCollection GetAddress(DatabaseUser user, long addressId)
        {
            return PersonManagement.GetAddress(user, addressId);
        }
        public PersonAddress UpdatePersonAddress(DatabaseUser user, PersonAddress address, bool handlePreviousAddress, List<PersonAddress> theData)
        {
            PersonManagement.UpdatePersonAddress(user, address, handlePreviousAddress, theData);
            return address;
        }
        public void DeletePersonAddress(DatabaseUser user, PersonAddress address)
        {
            PersonManagement.DeletePersonAddress(user, address);
        }
        public PersonAddress InsertPersonAddress(DatabaseUser user, PersonAddress address, List<PersonAddress> theData)
        {
            PersonManagement.InsertPersonAddress(user, address, theData);
            return address;
        }
        #endregion

        #region person ContactChannel
        public PersonContactChannelCollection GetPersonContactChannel(DatabaseUser user, long id, String contactChannelCategoryCode)
        {
            return PersonManagement.GetPersonContactChannel(user, id, contactChannelCategoryCode);
        }
        public PersonContactChannel UpdatePersonContactChannel(DatabaseUser user, PersonContactChannel contact, List<PersonContactChannel> theData)
        {
            PersonManagement.UpdatePersonContactChannel(user, contact, theData);
            return contact;
        }
        public void DeletePersonContactChannel(DatabaseUser user, PersonContactChannel contact)
        {
            PersonManagement.DeletePersonContactChannel(user, contact);
        }
        public PersonContactChannel InsertPersonContactChannel(DatabaseUser user, PersonContactChannel contact, List<PersonContactChannel> theData)
        {
            PersonManagement.InsertPersonContactChannel(user, contact, theData);
            return contact;
        }
        #endregion

        #region contact channel
        public ContactChannelCollection GetContactChannel(DatabaseUser user, long contactChannelId)
        {
            return PersonManagement.GetContactChannel(user, contactChannelId);
        }
        #endregion

        #region government identification number type template
        public GovernmentIdentificationNumberTypeTemplateCollection GetGovernmentIdentificationNumberTypeTemplate(DatabaseUser user, String numberTypeCode)
        {
            return PersonManagement.GetGovernmentIdentificationNumberTypeTemplate(user, numberTypeCode);
        }
        #endregion

        #region grievance
        public GrievanceCollection GetGrievance(DatabaseUser user, long id)
        {
            return GrievanceManagement.GetGrievance(user, id);
        }
        public Grievance UpdateGrievance(DatabaseUser user, Grievance data)
        {
            GrievanceManagement.UpdateGrievance(user, data);
            return data;
        }
        public Grievance InsertGrievance(DatabaseUser user, Grievance data)
        {
            GrievanceManagement.InsertGrievance(user, data);
            return data;
        }
        public void DeleteGrievance(DatabaseUser user, Grievance data)
        {
            GrievanceManagement.DeleteGrievance(user, data);
        }
        #endregion

        #region grievance action
        public GrievanceActionCollection GetGrievanceAction(DatabaseUser user, long id)
        {
            return GrievanceManagement.GetGrievanceAction(user, id);
        }
        public GrievanceAction UpdateGrievanceAction(DatabaseUser user, GrievanceAction data)
        {
            GrievanceManagement.UpdateGrievanceAction(user, data);
            return data;
        }
        public GrievanceAction InsertGrievanceAction(DatabaseUser user, GrievanceAction data)
        {
            GrievanceManagement.InsertGrievanceAction(user, data);
            return data;
        }
        public void DeleteGrievanceAction(DatabaseUser user, GrievanceAction data)
        {
            GrievanceManagement.DeleteGrievanceAction(user, data);
        }
        #endregion

        #region grievance reports
        public EmployeeSummaryCollection GetEmployeeSummary(DatabaseUser user, EmployeeCriteria criteria)
        {
            return EmployeeManagement.GetEmployeeSummary(user, criteria);
        }
        public GrievanceCollection GetGrievanceSummary(DatabaseUser user, GrievanceCriteria criteria)
        {
            return GrievanceManagement.GetGrievanceSummary(user, criteria);
        }
        //public Grievance UpdateGrievance(DatabaseUser user, Grievance data)
        //{
        //    GrievanceManagement.UpdateGrievance(DatabaseUser user, data);
        //    return data;
        //}
        //public Grievance InsertGrievance(DatabaseUser user, Grievance data)
        //{
        //    return GrievanceManagement.InsertGrievance(DatabaseUser user, data);
        //}
        //public void DeleteGrievance(DatabaseUser user, Grievance data)
        //{
        //    GrievanceManagement.DeleteGrievance(DatabaseUser user, data);
        //}
        #endregion

        #region labour union
        public LabourUnionSummaryCollection GetLabourUnion(DatabaseUser user, long? labourUnionId)
        {
            return LabourUnionManagement.GetLabourUnion(user, labourUnionId);
        }
        public LabourUnionSummary UpdateUnionContactData(DatabaseUser user, LabourUnionSummary union)
        {
            return LabourUnionManagement.UpdateUnionContactData(user, union);
        }
        public LabourUnion UpdateUnionContact(DatabaseUser user, LabourUnion union)
        {
            LabourUnionManagement.UpdateLabourUnion(user, union);
            return union;
        }
        public void DeleteUnionContact(DatabaseUser user, LabourUnion union)
        {
            LabourUnionManagement.DeleteLabourUnion(user, union);
        }
        public LabourUnionSummary InsertUnionContactInformationSummaryData(DatabaseUser user, LabourUnionSummary union)
        {
            return LabourUnionManagement.InsertUnionContactInformationSummaryData(user, union);
        }
        public LabourUnion InsertUnionContact(DatabaseUser user, LabourUnion union)
        {
            return LabourUnionManagement.InsertUnionContact(user, union);
        }
        #endregion

        #region union collective agreement
        public UnionCollectiveAgreementCollection GetUnionCollectiveAgreement(DatabaseUser user, long? labourUnionId)
        {
            return LabourUnionManagement.GetUnionCollectiveAgreement(user, labourUnionId);
        }
        public UnionCollectiveAgreement InsertUnionCollectiveAgreement(DatabaseUser user, UnionCollectiveAgreement collectiveAgreement)
        {
            return LabourUnionManagement.InsertCollectiveAgreement(user, collectiveAgreement);
        }
        public UnionCollectiveAgreement UpdateUnionCollectiveAgreement(DatabaseUser user, UnionCollectiveAgreement collectiveAgreement)
        {
            LabourUnionManagement.UpdateUnionCollectiveAgreement(user, collectiveAgreement);
            return collectiveAgreement;
        }
        public void DeleteUnionCollectiveAgreement(DatabaseUser user, UnionCollectiveAgreement collectiveAgreement)
        {
            LabourUnionManagement.DeleteCollectiveAgreement(user, collectiveAgreement);
        }
        #endregion

        #region employee license certificate
        public EmployeeLicenseCertificateCollection GetEmployeeLicenseCertificate(DatabaseUser user, long employeeId)
        {
            return EmployeeManagement.GetEmployeeLicenseCertificate(user, employeeId);
        }
        public EmployeeLicenseCertificate UpdateEmployeeLicenseCertificate(DatabaseUser user, EmployeeLicenseCertificate licenseCertificate)
        {
            EmployeeManagement.UpdateEmployeeLicenseCertificate(user, licenseCertificate);
            return licenseCertificate;
        }
        public void DeleteEmployeeLicenseCertificate(DatabaseUser user, EmployeeLicenseCertificate licenseCertificate)
        {
            EmployeeManagement.DeleteEmployeeLicenseCertificate(user, licenseCertificate);
        }
        public EmployeeLicenseCertificate InsertEmployeeLicenseCertificate(DatabaseUser user, EmployeeLicenseCertificate licenseCertificate)
        {
            return EmployeeManagement.InsertEmployeeLicenseCertificate(user, licenseCertificate);
        }
        #endregion

        #region employee paycode
        public EmployeePaycodeCollection GetEmployeePaycode(DatabaseUser user, long employeeId)
        {
            return EmployeeManagement.GetEmployeePaycode(user, employeeId);
        }
        public EmployeePaycode UpdateEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            EmployeeManagement.UpdateEmployeePaycode(user, employeePaycode);
            return employeePaycode;
        }
        public void DeleteEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            EmployeeManagement.DeleteEmployeePaycode(user, employeePaycode);
        }
        public EmployeePaycode InsertEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            return EmployeeManagement.InsertEmployeePaycode(user, employeePaycode);
        }
        public bool CheckRecurringIncomeCode(DatabaseUser user)
        {
            return EmployeeManagement.CheckRecurringIncomeCode(user);
        }
        public Decimal CalculateRate(DatabaseUser user, long employeeId, Decimal baseRate, Decimal rateFactor, bool vacationCalculationOverrideFlag)
        {
            return EmployeeManagement.CalculateRate(user, employeeId, baseRate, rateFactor, vacationCalculationOverrideFlag);
        }
        public bool ValidatePaycodeEntryData(DatabaseUser user, EmployeePaycode data)
        {
            return EmployeeManagement.ValidatePaycodeEntryData(user, data);
        }
        #endregion

        #region employee award honour
        public EmployeeAwardHonourCollection GetEmployeeAwardHonour(DatabaseUser user, long employeeId)
        {
            return EmployeeManagement.GetEmployeeAwardHonour(user, employeeId);
        }
        public EmployeeAwardHonour UpdateEmployeeAwardHonour(DatabaseUser user, EmployeeAwardHonour awardHonour)
        {
            EmployeeManagement.UpdateEmployeeAwardHonour(user, awardHonour);
            return awardHonour;
        }
        public void DeleteEmployeeAwardHonour(DatabaseUser user, EmployeeAwardHonour awardHonour)
        {
            EmployeeManagement.DeleteEmployeeAwardHonour(user, awardHonour);
        }
        public EmployeeAwardHonour InsertEmployeeAwardHonour(DatabaseUser user, EmployeeAwardHonour awardHonour)
        {
            return EmployeeManagement.InsertEmployeeAwardHonour(user, awardHonour);
        }
        #endregion

        #region employee discipline
        public EmployeeDisciplineCollection GetEmployeeDiscipline(DatabaseUser user, long employeeId)
        {
            return EmployeeManagement.GetEmployeeDiscipline(user, employeeId);
        }
        public EmployeeDiscipline UpdateEmployeeDiscipline(DatabaseUser user, EmployeeDiscipline discipline)
        {
            EmployeeManagement.UpdateEmployeeDiscipline(user, discipline);
            return discipline;
        }
        public void DeleteEmployeeDiscipline(DatabaseUser user, EmployeeDiscipline discipline)
        {
            EmployeeManagement.DeleteEmployeeDiscipline(user, discipline);
        }
        public EmployeeDiscipline InsertEmployeeDiscipline(DatabaseUser user, EmployeeDiscipline discipline)
        {
            return EmployeeManagement.InsertEmployeeDiscipline(user, discipline);
        }
        #endregion

        #region employee discipline action
        public EmployeeDisciplineActionCollection GetEmployeeDisciplineAction(DatabaseUser user, long employeeDisciplineId)
        {
            return EmployeeManagement.GetEmployeeDisciplineAction(user, employeeDisciplineId);
        }
        public EmployeeDisciplineAction UpdateEmployeeDisciplineAction(DatabaseUser user, EmployeeDisciplineAction disciplineAction)
        {
            EmployeeManagement.UpdateEmployeeDisciplineAction(user, disciplineAction);
            return disciplineAction;
        }
        public void DeleteEmployeeDisciplineAction(DatabaseUser user, EmployeeDisciplineAction disciplineAction)
        {
            EmployeeManagement.DeleteEmployeeDisciplineAction(user, disciplineAction);
        }
        public EmployeeDisciplineAction InsertEmployeeDisciplineAction(DatabaseUser user, EmployeeDisciplineAction disciplineAction)
        {
            return EmployeeManagement.InsertEmployeeDisciplineAction(user, disciplineAction);
        }
        #endregion

        #region Organization Unit Management
        #region OrganizationUnit
        public OrganizationUnitCollection GetOrganizationUnitByOrganizationUnitLevelId(DatabaseUser user, OrganizationUnitCriteria criteria)
        {
            return OrganizationUnitManagement.GetOrganizationUnitByOrganizationUnitLevelId(user, criteria);
        }
        public OrganizationUnitCollection GetOrganizationUnit(DatabaseUser user, long? organizationUnitId)
        {
            return OrganizationUnitManagement.GetOrganizationUnit(user, organizationUnitId);
        }
        public OrganizationUnitCollection GetOrganizationUnitByParentOrganizationUnitId(DatabaseUser user, long? parentOrganizationUnitId)
        {
            return OrganizationUnitManagement.GetOrganizationUnitByParentOrganizationUnitId(user, parentOrganizationUnitId);
        }
        public OrganizationUnitCollection GetOrganizationUnitWithLevelDescription(DatabaseUser user, long? organizationUnitId)
        {
            return OrganizationUnitManagement.GetOrganizationUnitWithLevelDescription(user, organizationUnitId);
        }
        public OrganizationUnit InsertOrganizationUnit(DatabaseUser user, OrganizationUnit item)
        {
            OrganizationUnit tmpOrganizationUnit = new OrganizationUnit();
            tmpOrganizationUnit = OrganizationUnitManagement.InsertOrganizationUnit(user, item);
            return tmpOrganizationUnit;
        }
        public OrganizationUnit UpdateOrganizationUnit(DatabaseUser user, OrganizationUnit item)
        {
            OrganizationUnit tmpOrganizationUnit = new OrganizationUnit();
            tmpOrganizationUnit = OrganizationUnitManagement.UpdateOrganizationUnit(user, item);
            return tmpOrganizationUnit;
        }
        public void DeleteOrganizationUnit(DatabaseUser user, OrganizationUnit item)
        {
            OrganizationUnitManagement.DeleteOrganizationUnit(user, item);
        }
        #endregion

        #region OrganizationUnitLevel

        public OrganizationUnitLevelDescriptionCollection GetOrganizationUnitLevelDescription(DatabaseUser user, long organizationUnitLevelId)
        {
            return OrganizationUnitManagement.GetOrganizationUnitLevelDescription(user, organizationUnitLevelId);
        }

        public OrganizationUnitLevelCollection GetOrganizationUnitLevel(DatabaseUser user)
        {
            return OrganizationUnitManagement.GetOrganizationUnitLevel(user);
        }
        public OrganizationUnitLevel InsertOrganizationUnitLevel(DatabaseUser user, OrganizationUnitLevel item)
        {
            OrganizationUnitLevel tmpOrganizationUnitLevel = new OrganizationUnitLevel();
            tmpOrganizationUnitLevel = OrganizationUnitManagement.InsertOrganizationUnitLevel(user, item);
            return tmpOrganizationUnitLevel;
        }
        public OrganizationUnitLevel UpdateOrganizationUnitLevel(DatabaseUser user, OrganizationUnitLevel item)
        {
            OrganizationUnitLevel tmpOrganizationUnitLevel = new OrganizationUnitLevel();
            tmpOrganizationUnitLevel = OrganizationUnitManagement.UpdateOrganizationUnitLevel(user, item);
            return tmpOrganizationUnitLevel;
        }
        public void DeleteOrganizationUnitLevel(DatabaseUser user, OrganizationUnitLevel item)
        {
            OrganizationUnitManagement.DeleteOrganizationUnitLevel(user, item);
        }
        #endregion

        #region OrganizationUnitAssociation
        public OrganizationUnitAssociationTreeCollection GetOrganizationUnitAssociationTree(DatabaseUser user, String codeLanguageCd, bool? checkActiveFlag)
        {
            return OrganizationUnitManagement.GetOrganizationUnitAssociationTree(user, codeLanguageCd, checkActiveFlag);
        }
        public OrganizationUnitAssociationCollection GetOrganizationUnitAssociation(DatabaseUser user, OrganizationUnitCriteria criteria)
        {
            return OrganizationUnitManagement.GetOrganizationUnitAssociation(user, criteria);
        }
        public OrganizationUnitAssociationCollection InsertOrganizationUnitAssociation(DatabaseUser user, OrganizationUnitAssociation item)
        {
            OrganizationUnitAssociationCollection tmp = new OrganizationUnitAssociationCollection();
            tmp = OrganizationUnitManagement.InsertOrganizationUnitAssociation(user, item);
            return tmp;
        }
        public void DeleteOrganizationUnitAssociation(DatabaseUser user, OrganizationUnitAssociation item)
        {
            OrganizationUnitManagement.DeleteOrganizationUnitAssociation(user, item);
        }
        #endregion

        public bool CheckEmployeeExist(DatabaseUser user)
        {
            return OrganizationUnitManagement.CheckEmployeeExist(user);
        }
        #endregion

        #region employee termination
        //termination other
        public EmployeeTerminationOtherCollection GetEmployeeTerminationOther(DatabaseUser user, long employeePositionId)
        {
            return EmployeeManagement.GetEmployeeTerminationOther(user, employeePositionId);
        }
        public EmployeeTerminationOther UpdateEmployeeTerminationOther(DatabaseUser user, EmployeeTerminationOther item)
        {
            EmployeeManagement.UpdateEmployeeTerminationOther(user, item);
            return item;
        }
        public void DeleteEmployeeTerminationOther(DatabaseUser user, EmployeeTerminationOther item)
        {
            EmployeeManagement.DeleteEmployeeTerminationOther(user, item);
        }
        public EmployeeTerminationOther InsertEmployeeTerminationOther(DatabaseUser user, EmployeeTerminationOther item)
        {
            EmployeeManagement.InsertEmployeeTerminationOther(user, item);
            return item;
        }

        //termination roe
        public EmployeeTerminationRoeCollection SelectRoe(DatabaseUser user, long employeePositionId)
        {
            return EmployeeManagement.GetEmployeeTerminationRoe(user, employeePositionId);
        }
        public EmployeeTerminationRoe UpdateRoe(DatabaseUser user, EmployeeTerminationRoe item)
        {
            EmployeeManagement.UpdateEmployeeTerminationRoe(user, item);
            return item;
        }
        public void DeleteRoe(DatabaseUser user, EmployeeTerminationRoe item)
        {
            EmployeeManagement.DeleteEmployeeTerminationRoe(user, item);
        }
        public EmployeeTerminationRoe InsertRoe(DatabaseUser user, EmployeeTerminationRoe item)
        {
            EmployeeManagement.InsertEmployeeTerminationRoe(user, item);
            return item;
        }
        #endregion

        #region employee position
        public EmployeePositionSummaryCollection GetEmployeePositionSummary(DatabaseUser user, EmployeePositionCriteria criteria)
        {
            return EmployeeManagement.GetEmployeePositionSummary(user, criteria);
        }
        public EmployeePositionCollection GetEmployeePosition(DatabaseUser user, EmployeePositionCriteria criteria)
        {
            return EmployeeManagement.GetEmployeePosition(user, criteria);
        }
        public EmployeePosition UpdateEmployeePosition(DatabaseUser user, EmployeePosition item, bool autoAddSecondaryPositions)
        {
            EmployeeManagement.UpdateEmployeePosition(user, item, autoAddSecondaryPositions);
            return item;
        }
        public bool IsProcessDateOpen(DatabaseUser user, long? payrollProcessId)
        {
            return EmployeeManagement.IsProcessDateOpen(user, payrollProcessId);
        }
        public void DeleteEmployeePosition(DatabaseUser user, EmployeePosition position, EmployeePosition formerPosition)
        {
            EmployeeManagement.DeleteEmployeePosition(user, position, formerPosition);
        }
        public void DeleteEmployeeTermination(DatabaseUser user, EmployeePosition employeePosition)
        {
            EmployeeManagement.DeleteEmployeeTermination(user, employeePosition);
        }
        public EmployeePosition InsertEmployeePosition(DatabaseUser user, EmployeePosition position, EmployeePosition formerPosition, DateTime? payrollPeriodCutoffDate, String payrollProcessGroupCode, String roeDefaultContactLastName, String roeDefaultContactFirstName, String roeDefaultContactTelephone, String roeDefaultContactTelephoneExt, bool autoAddSecondaryPositions)
        {
            EmployeeManagement.InsertEmployeePosition(user, position, formerPosition, payrollPeriodCutoffDate, payrollProcessGroupCode, roeDefaultContactLastName, roeDefaultContactFirstName, roeDefaultContactTelephone, roeDefaultContactTelephoneExt, autoAddSecondaryPositions);
            return position;
        }
        public EmployeePosition CreateEmployeePosition(DatabaseUser user)
        {
            return EmployeeManagement.CreateEmployeePosition(user);
        }
        #endregion

        #region employee position workday
        public EmployeePositionWorkdayCollection GetEmployeePositionWorkday(DatabaseUser user, long employeePositionId)
        {
            return EmployeeManagement.GetEmployeePositionWorkday(user, employeePositionId);
        }
        public void InsertEmployeePositionWorkday(DatabaseUser user, EmployeePositionWorkday workday)
        {
            EmployeeManagement.InsertEmployeePositionWorkday(user, workday);
        }
        public void UpdateEmployeePositionWorkday(DatabaseUser user, EmployeePositionWorkday workday)
        {
            EmployeeManagement.UpdateEmployeePositionWorkday(user, workday);
        }
        public void DeleteEmployeePositionWorkday(DatabaseUser user, EmployeePositionWorkday workday)
        {
            EmployeeManagement.DeleteEmployeePositionWorkday(user, workday);
        }
        #endregion

        #region employee position organization unit level report
        public EmployeePositionOrganizationUnitCollection GetEmployeePositionOrganizationUnitLevelSummary(DatabaseUser user, long employeePositionId)
        {
            return EmployeeManagement.GetEmployeePositionOrganizationUnitLevelSummary(user, employeePositionId);
        }
        #endregion

        #region employee wsib health and safety report
        public EmployeeWsibHealthAndSafetyReportCollection GetEmployeeWsibHealthAndSafetyReport(DatabaseUser user, long employeeId)
        {
            return EmployeeManagement.GetEmployeeWsibHealthAndSafetyReport(user, employeeId);
        }
        public EmployeeWsibHealthAndSafetyReportPersonCollection GetEmployeeWsibHealthAndSafetyReportPerson(DatabaseUser user, long employeeId)
        {
            return EmployeeManagement.GetEmployeeWsibHealthAndSafetyReportPerson(user, employeeId);
        }
        public EmployeeWsibHealthAndSafetyReport InsertEmployeeWsibHealthAndSafetyReport(DatabaseUser user, EmployeeWsibHealthAndSafetyReport item)
        {
            EmployeeManagement.InsertEmployeeWsibHealthAndSafetyReport(user, item);
            return item;
        }
        public EmployeeWsibHealthAndSafetyReport UpdateEmployeeWsibHealthAndSafetyReport(DatabaseUser user, EmployeeWsibHealthAndSafetyReport item)
        {
            EmployeeManagement.UpdateEmployeeWsibHealthAndSafetyReport(user, item);
            return item;
        }
        public void DeleteEmployeeWsibHealthAndSafetyReport(DatabaseUser user, EmployeeWsibHealthAndSafetyReport item)
        {
            EmployeeManagement.DeleteEmployeeWsibHealthAndSafetyReport(user, item);
        }
        #endregion

        #region statutory deduction
        public StatutoryDeductionCollection GetStatutoryDeduction(DatabaseUser user, long id)
        {
            return EmployeeManagement.GetStatutoryDeduction(user, id, false); //normal select from the control won't override the security settings
        }
        public StatutoryDeduction InsertStatutoryDeduction(DatabaseUser user, StatutoryDeduction item)
        {
            EmployeeManagement.InsertStatutoryDeduction(user, item);
            return item;
        }
        public StatutoryDeduction UpdateStatutoryDeduction(DatabaseUser user, StatutoryDeduction item)
        {
            EmployeeManagement.UpdateStatutoryDeduction(user, item);
            return item;
        }
        public void DeleteStatutoryDeduction(DatabaseUser user, StatutoryDeduction item)
        {
            EmployeeManagement.DeleteStatutoryDeduction(user, item);
        }
        #endregion

        #region statutory deduction barbados
        public StatutoryDeduction InsertStatutoryDeductionBarbados(DatabaseUser user, StatutoryDeduction item)
        {
            EmployeeManagement.InsertStatutoryDeductionBarbados(user, item);
            return item;
        }
        public StatutoryDeduction UpdateStatutoryDeductionBarbados(DatabaseUser user, StatutoryDeduction item)
        {
            EmployeeManagement.UpdateStatutoryDeductionBarbados(user, item);
            return item;
        }
        public void DeleteStatutoryDeductionBarbados(DatabaseUser user, StatutoryDeduction item)
        {
            EmployeeManagement.DeleteStatutoryDeductionBarbados(user, item);
        }
        #endregion

        #region statutory deduction saint lucia
        public StatutoryDeduction InsertStatutoryDeductionSaintLucia(DatabaseUser user, StatutoryDeduction item)
        {
            EmployeeManagement.InsertStatutoryDeductionSaintLucia(user, item);
            return item;
        }
        public StatutoryDeduction UpdateStatutoryDeductionSaintLucia(DatabaseUser user, StatutoryDeduction item)
        {
            EmployeeManagement.UpdateStatutoryDeductionSaintLucia(user, item);
            return item;
        }
        public void DeleteStatutoryDeductionSaintLucia(DatabaseUser user, StatutoryDeduction item)
        {
            EmployeeManagement.DeleteStatutoryDeductionSaintLucia(user, item);
        }
        #endregion

        #region statutory deduction trinidad
        public StatutoryDeduction InsertStatutoryDeductionTrinidad(DatabaseUser user, StatutoryDeduction item)
        {
            EmployeeManagement.InsertStatutoryDeductionTrinidad(user, item);
            return item;
        }
        public StatutoryDeduction UpdateStatutoryDeductionTrinidad(DatabaseUser user, StatutoryDeduction item)
        {
            EmployeeManagement.UpdateStatutoryDeductionTrinidad(user, item);
            return item;
        }
        public void DeleteStatutoryDeductionTrinidad(DatabaseUser user, StatutoryDeduction item)
        {
            EmployeeManagement.DeleteStatutoryDeductionTrinidad(user, item);
        }
        #endregion

        #region statutory deduction jamaica
        public StatutoryDeduction InsertStatutoryDeductionJamaica(DatabaseUser user, StatutoryDeduction item)
        {
            EmployeeManagement.InsertStatutoryDeductionJamaica(user, item);
            return item;
        }
        public StatutoryDeduction UpdateStatutoryDeductionJamaica(DatabaseUser user, StatutoryDeduction item)
        {
            EmployeeManagement.UpdateStatutoryDeductionJamaica(user, item);
            return item;
        }
        public void DeleteStatutoryDeductionJamaica(DatabaseUser user, StatutoryDeduction item)
        {
            EmployeeManagement.DeleteStatutoryDeductionJamaica(user, item);
        }
        #endregion

        #region statutory deduction defaults
        public StatutoryDeductionDefaultCollection GetStatutoryDeductionDefaults(DatabaseUser user, String codeProvinceStateCd)
        {
            return EmployeeManagement.GetStatutoryDeductionDefaults(user, codeProvinceStateCd);
        }
        #endregion

        #region EmployeeEmploymentInformation
        public EmployeeEmploymentInformationCollection GetEmployeeEmploymentInformation(DatabaseUser user, long id)
        {
            return EmployeeManagement.GetEmployeeEmploymentInformation(user, id);
        }
        public EmployeeEmploymentInformation UpdateEmployeeEmploymentInformation(DatabaseUser user, EmployeeEmploymentInformation item)
        {
            EmployeeManagement.UpdateEmployeeEmploymentInformation(user, item);
            return item;
        }
        public void DeleteEmployeeEmploymentInformation(DatabaseUser user, EmployeeEmploymentInformation item)
        {
            EmployeeManagement.DeleteEmployeeEmploymentInformation(user, item);
        }
        public EmployeeEmploymentInformation InsertEmployeeEmploymentInformation(DatabaseUser user, EmployeeEmploymentInformation item)
        {
            EmployeeManagement.InsertEmployeeEmploymentInformation(user, item);
            return item;
        }
        #endregion

        #region Payroll Period
        public PayrollPeriodCollection GetPayrollPeriodById(DatabaseUser user, long payrollPeriodId)
        {
            return PayrollManagement.GetPayrollPeriod(user, payrollPeriodId);
        }
        public PayrollPeriodCollection GetPayrollPeriod(DatabaseUser user, PayrollPeriodCriteria criteria)
        {
            return PayrollManagement.GetPayrollPeriod(user, criteria);
        }
        public PayrollPeriod UpdatePayrollPeriod(DatabaseUser user, PayrollPeriod item)
        {
            PayrollManagement.UpdatePayrollPeriod(user, item);
            return item;
        }
        public void DeletePayrollPeriod(DatabaseUser user, PayrollPeriod item)
        {
            PayrollManagement.DeletePayrollPeriod(user, item);
        }
        public PayrollPeriod InsertPayrollPeriod(DatabaseUser user, PayrollPeriod item)
        {
            PayrollManagement.InsertPayrollPeriod(user, item);
            return item;
        }
        #endregion

        #region Payroll Process
        public PayrollProcessSummaryCollection GetPayrollProcessSummary(DatabaseUser user, PayrollProcessCriteria criteria)
        {
            return PayrollManagement.GetPayrollProcessSummary(user, criteria);
        }
        public PayrollProcessCollection GetPayrollProcess(DatabaseUser user, PayrollProcessCriteria criteria)
        {
            return PayrollManagement.GetPayrollProcess(user, criteria);
        }
        public PayrollProcess UpdatePayrollProcess(DatabaseUser user, PayrollProcess item, PayrollProcess.CalculationProcess processType, String version, bool autoGenSalaryEmployee, bool calculateBmsPension, bool lockUnlockButtonPressed, bool prorationEnabledFlag, byte[] uploadedFile, String uploadedFileName, bool autoAddSecondaryPositions, bool newArrearsProcessingFlag)
        {
            PayrollManagement.UpdatePayrollProcess(user, item, processType, version, autoGenSalaryEmployee, calculateBmsPension, lockUnlockButtonPressed, prorationEnabledFlag, uploadedFile, uploadedFileName, autoAddSecondaryPositions, newArrearsProcessingFlag);
            return item;
        }
        #endregion

        #region Pending ROE
        public RoeCreationSearchResultsCollection GetPendingRoeEmployeeSummary(DatabaseUser user, String criteria)
        {
            return PayrollManagement.GetPendingRoeEmployeeSummary(user, criteria);
        }
        public void SetPendingRoeStatusForEmployeeId(DatabaseUser user, long selectedEmployeeId)
        {
            PayrollManagement.SetPendingRoeStatusForEmployeeId(user, selectedEmployeeId);
        }
        public EmployeeRoeAmountCollection GetRoeData(DatabaseUser user, long employeePositionId, long employeeId)
        {
            return PayrollManagement.GetRoeData(user, employeePositionId, employeeId);
        }
        public EmployeeRoeAmount UpdateEmployeeRoeAmount(DatabaseUser user, EmployeeRoeAmount roeObj)
        {
            PayrollManagement.UpdateEmployeeRoeAmount(user, roeObj);
            return roeObj;
        }
        public void UpdateEmployeeRoeDetail(DatabaseUser user, EmployeeRoeAmountDetail[] empRoeArray)
        {
            EmployeeRoeAmountDetailCollection coll = new EmployeeRoeAmountDetailCollection();
            coll.Load(empRoeArray);
            PayrollManagement.UpdateEmployeeRoeDetail(user, coll);
        }
        #endregion

        #region dynamics process batch
        public PayrollProcess PostTransaction(DatabaseUser user, PayrollProcess process, String status, RbcEftEdiParameters ediParms, ScotiaBankEdiParameters scotiaEdiParms, SendEmailParameters emailParms, RbcEftSourceDeductionParameters garnishmentParms, bool craRemitFlag, bool rqRemitFlag, bool sendInvoice, bool garnishmentRemitFlag, bool wcbChequeRemitFlag, bool healthTaxChequeRemitFlag, bool autoAddSecondaryPositions)
        {
            //this will post the transaction and close the period
            DynamicsManagement.PostTransaction(user, process, status, ediParms, scotiaEdiParms, emailParms, garnishmentParms, craRemitFlag, rqRemitFlag, sendInvoice, garnishmentRemitFlag, wcbChequeRemitFlag, healthTaxChequeRemitFlag, autoAddSecondaryPositions);
            return process;
        }
        #endregion

        #region Payroll Batch Report and Payroll Transaction Batch
        //batch
        public PayrollBatchReportCollection GetPayrollBatchReport(DatabaseUser user, PayrollBatchCriteria criteria)
        {
            return PayrollManagement.GetPayrollBatchReport(user, criteria);
        }
        public PayrollBatchCollection GetPayrollBatch(DatabaseUser user, long payrollBatchId)
        {
            return PayrollManagement.GetPayrollBatch(user, payrollBatchId);
        }
        public PayrollBatch InsertPayrollBatch(DatabaseUser user, PayrollBatch item)
        {
            return PayrollManagement.InsertPayrollBatch(user, item);
        }
        public void UpdatePayrollBatch(DatabaseUser user, PayrollBatch item)
        {
            PayrollManagement.UpdatePayrollBatch(user, item);
        }
        public void DeletePayrollBatch(DatabaseUser user, PayrollBatchReport item)
        {
            PayrollManagement.DeletePayrollBatch(user, item);
        }
        public PayrollPeriodCollection GetPayrollPeriodIdFromPayrollProcessGroupCode(DatabaseUser user, PayrollBatchCriteria payrollBatchCriteria)
        {
            return PayrollManagement.GetPayrollPeriodIdFromPayrollProcessGroupCode(user, payrollBatchCriteria);
        }

        //transaction
        public PayrollTransactionSummaryCollection GetPayrollTransactionSummary(DatabaseUser user, long payrollBatchId)
        {
            return PayrollManagement.GetPayrollTransactionSummary(user, payrollBatchId);
        }
        public PayrollTransaction UpdatePayrollTransaction(DatabaseUser user, PayrollTransaction payrollTransaction)
        {
            PayrollManagement.UpdatePayrollTransaction(user, payrollTransaction);
            return payrollTransaction;
        }
        public PayrollTransaction InsertPayrollTransaction(DatabaseUser user, PayrollTransaction payrollTransaction)
        {
            PayrollManagement.InsertPayrollTransaction(user, payrollTransaction);
            return payrollTransaction;
        }
        public void DeletePayrollTransaction(DatabaseUser user, PayrollTransaction payrollTransaction)
        {
            PayrollManagement.DeletePayrollTransaction(user, payrollTransaction);
        }
        public void DeletePayrollBatchCascadeTransactions(DatabaseUser user, PayrollBatch payrollBatch)
        {
            PayrollManagement.DeletePayrollBatchCascadeTransactions(user, payrollBatch);
        }
        #endregion

        //removed by McKayJ for now 
        //#region Payroll Batch Transaction Full Week
        //public PayrollTransactionWeekCollection GetPayrollTransactionWeek(DatabaseUser user, PayrollBatch batch)
        //{
        //    return PayrollManagement.GetPayrollTransactionWeek(DatabaseUser user, batch);
        //}

        //public PayrollTransactionWeek UpdatePayrollTransactionWeek(DatabaseUser user, PayrollTransactionWeek item, List<PayrollTransactionQuickEntry> deletedItems)
        //{
        //    PayrollManagement.UpdatePayrollTransactionWeek(DatabaseUser user, item, deletedItems);
        //    return item;
        //}

        //public void DeletePayrollTransactionWeek(DatabaseUser user, PayrollTransactionWeek item)
        //{
        //    PayrollManagement.DeletePayrollTransactionWeek(DatabaseUser user, item);
        //}

        //public PayrollTransactionWeek InsertPayrollTransactionWeek(DatabaseUser user, PayrollTransactionWeek item)
        //{
        //    throw new Exception(DatabaseUser user, "not implimented");
        //}
        //#endregion

        #region wizards
        public WizardCacheCollection GetWizardCache(DatabaseUser user, long wizardId, long? wizardCacheId, String itemName)
        {
            return WizardManagement.GetWizardCache(user, wizardId, wizardCacheId, itemName);
        }
        public WizardCache UpdateWizardCache(DatabaseUser user, WizardCache wizard)
        {
            WizardManagement.UpdateWizardCache(user, wizard);
            return wizard;
        }
        public WizardCacheItem UpdateWizardCacheItem(DatabaseUser user, WizardCacheItem item)
        {
            WizardManagement.UpdateWizardCacheItem(user, item);
            return item;
        }
        public Employee AddEmployee(DatabaseUser user, long wizardCacheId, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool autoAddSecondaryPositions)
        {
            return WizardManagement.AddEmployee(user, wizardCacheId, autoGenerateEmployeeNumber, employeeNumberFormat, autoAddSecondaryPositions);
        }
        public void DeleteWizardEmployee(DatabaseUser user, long wizardCacheId)
        {
            WizardManagement.DeleteWizardEmployee(user, wizardCacheId);
        }
        public bool ValidateWizardEmployee(DatabaseUser user, WizardCacheCollection wizardCacheCollection)
        {
            return WizardManagement.ValidateWizardEmployee(user, wizardCacheCollection);
        }
        #endregion

        #region ScreenControl
        public void MergeScreenControl(DatabaseUser user, ScreenControl[] screenControlArray)
        {
            ScreenControlManagement.MergeScreenControls(user, screenControlArray);
        }
        #endregion

        #region import/export
        public ImportExportSummaryCollection GetImportExportSummary(DatabaseUser user)
        {
            return XmlManagement.GetImportExportSummary(user);
        }
        #endregion

        #region vendor
        public VendorCollection GetVendor(DatabaseUser user, long VendorId)
        {
            return EmployeeManagement.GetVendor(user, VendorId);
        }
        public Vendor UpdateVendor(DatabaseUser user, Vendor vendor)
        {
            EmployeeManagement.UpdateVendor(user, vendor);
            return vendor;
        }
        public Vendor InsertVendor(DatabaseUser user, Vendor vendor)
        {
            Vendor tmpVendor = EmployeeManagement.InsertVendor(user, vendor);
            return tmpVendor;
        }
        public void DeleteVendor(DatabaseUser user, Vendor vendor)
        {
            EmployeeManagement.DeleteVendor(user, vendor);
        }
        #endregion

        #region temporary
        public KeyValuePair<String, String>[] GetDynamicsOutputFileList(DatabaseUser user, String dynamicsGPOutputFolder, long payrollProcessId)
        {
            return DynamicsManagement.GetDynamicsOutputFileList(user, dynamicsGPOutputFolder, payrollProcessId);
        }
        public String getFileText(DatabaseUser user, String filePath)
        {
            return DynamicsManagement.getFileText(user, filePath);
        }
        #endregion

        #region year end adjustments search
        public YearEndAdjustmentsSearchReportCollection GetYearEndAdjustmentsSearchReport(DatabaseUser user, YearEndAdjustmentsSearchCriteria criteria)
        {
            return EmployeeManagement.GetYearEndAdjustmentsSearchReport(user, criteria);
        }
        #endregion

        #region year end adjustment
        public YearEndAdjustmentCollection GetYearEndAdjustment(DatabaseUser user, long employeeId, Decimal year, long yearEndAdjustmentTableId, String employerNumber, String provinceStateCode, long revision, String columnImportIdentifier)
        {
            return YearEndManagement.GetYearEndAdjustment(user, employeeId, year, yearEndAdjustmentTableId, employerNumber, provinceStateCode, revision, columnImportIdentifier);
        }
        public YearEndAdjustmentCollection UpdateYearEndAdjustment(DatabaseUser user, YearEndAdjustment[] yearEndAdjustmentCollection)
        {
            YearEndAdjustmentCollection coll = new YearEndAdjustmentCollection();
            coll.Load(yearEndAdjustmentCollection);
            YearEndManagement.UpdateYearEndAdjustment(user, coll);
            return coll;
        }
        public YearEndAdjustmentsSearchReport InsertYearEndAdjustment(DatabaseUser user, YearEndAdjustmentsSearchReport report, long? previousKeyId = null)
        {
            return YearEndManagement.InsertYearEndAdjustment(user, report, previousKeyId);
        }
        public YearEndAdjustmentTableCollection GetYearEndAdjustmentTables(DatabaseUser user)
        {
            return YearEndManagement.GetYearEndAdjustmentTables(user);
        }
        public void DeleteYearEndReport(DatabaseUser user, YearEndAdjustmentsSearchReport reportObj)
        {
            YearEndManagement.DeleteYearEndReport(user, reportObj);
        }
        #endregion

        public YearEndT4Collection GetYearEndT4(DatabaseUser user, int year, long? businessNumberId)
        {
            return YearEndManagement.GetYearEndT4(user, year, businessNumberId);
        }
        public YearEndT4Collection GetBatchYearEndT4(DatabaseUser user, string[] yearEndT4Ids)
        {
            return YearEndManagement.GetBatchYearEndT4(user, yearEndT4Ids);
        }

        #region year end clone
        public YearEndT4 SelectSingleT4ByKey(DatabaseUser user, long key)
        {
            return YearEndManagement.SelectSingleT4ByKey(user, key);
        }
        public YearEndT4a SelectSingleT4aByKey(DatabaseUser user, long key)
        {
            return YearEndManagement.SelectSingleT4aByKey(user, key);
        }
        public YearEndR1 SelectSingleR1ByKey(DatabaseUser user, long key)
        {
            return YearEndManagement.SelectSingleR1ByKey(user, key);
        }
        public YearEndR2 SelectSingleR2ByKey(DatabaseUser user, long key)
        {
            return YearEndManagement.SelectSingleR2ByKey(user, key);
        }
        public YearEndT4rsp SelectSingleT4rspByKey(DatabaseUser user, long key)
        {
            return YearEndManagement.SelectSingleT4rspByKey(user, key);
        }
        public YearEndNR4rsp SelectSingleNR4rspByKey(DatabaseUser user, long key)
        {
            return YearEndManagement.SelectSingleNR4rspByKey(user, key);
        }
        public YearEndT4arca SelectSingleT4arcaByKey(DatabaseUser user, long key)
        {
            return YearEndManagement.SelectSingleT4arcaByKey(user, key);
        }
        #endregion

        #region year end
        public YearEndCollection GetYearEnd(DatabaseUser user, long? year)
        {
            return EmployeeManagement.GetYearEnd(user, year);
        }
        public void CreateCurrentYE(DatabaseUser user)
        {
            EmployeeManagement.CreateCurrentYE(user);
        }
        public void CloseVersionYE(DatabaseUser user, YearEnd yearEnd)
        {
            EmployeeManagement.CloseVersionYE(user, yearEnd);
        }
        public void CloseCurrentYE(DatabaseUser user, YearEnd yearEnd)
        {
            EmployeeManagement.CloseCurrentYE(user, yearEnd);
        }
        #endregion

        #region business number
        public BusinessNumberCollection GetBusinessNumber(DatabaseUser user, long? businessNumberId)
        {
            return EmployeeManagement.GetBusinessNumber(user, businessNumberId);
        }
        public BusinessNumber InsertBusinessNumber(DatabaseUser user, BusinessNumber businessNumber)
        {
            return EmployeeManagement.InsertBusinessNumber(user, businessNumber);
        }
        public void UpdateBusinessNumber(DatabaseUser user, BusinessNumber businessNumber)
        {
            EmployeeManagement.UpdateBusinessNumber(user, businessNumber);
        }
        public void DeleteBusinessNumber(DatabaseUser user, BusinessNumber businessNumber)
        {
            EmployeeManagement.DeleteBusinessNumber(user, businessNumber);
        }
        #endregion

        #region payroll master payment
        public PayrollMasterPaymentCollection GetPayrollMasterPaymentDirectDeposit(DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            return DynamicsManagement.GetEftData(user, payrollProcessId, employeeNumber);
        }
        #endregion

        #region export ftp
        public ExportFtpCollection GetExportFtp(DatabaseUser user, long? exportFtpId, string codeExportEftTypeCd, string ftpName)
        {
            return PayrollManagement.GetExportFtp(user, exportFtpId, codeExportEftTypeCd, ftpName);
        }
        public ExportFtp UpdateExportFtp(DatabaseUser user, ExportFtp export)
        {
            PayrollManagement.UpdateExportFtp(user, export);
            return export;
        }
        public ExportFtp InsertExportFtp(DatabaseUser user, ExportFtp export)
        {
            return PayrollManagement.InsertExportFtp(user, export);
        }
        public void DeleteExportFtp(DatabaseUser user, ExportFtp export)
        {
            PayrollManagement.DeleteExportFtp(user, export);
        }
        #endregion

        #region pay register export
        public PayRegisterExportCollection GetPayRegisterExport(DatabaseUser user, long? exportQueueId, string codeExportFtpTypeCd)
        {
            return PayrollManagement.GetPayRegisterExport(user, exportQueueId, codeExportFtpTypeCd);
        }
        public PayRegisterExport UpdatePayRegisterExport(DatabaseUser user, PayRegisterExport export)
        {
            PayrollManagement.UpdatePayRegisterExport(user, export);
            return export;
        }
        public PayRegisterExport InsertPayRegisterExport(DatabaseUser user, PayRegisterExport export)
        {
            return PayrollManagement.InsertPayRegisterExport(user, export);
        }
        public void DeletePayRegisterExport(DatabaseUser user, PayRegisterExport export)
        {
            PayrollManagement.DeletePayRegisterExport(user, export);
        }
        #endregion

        #region get year end negative amount employee
        public Employee GetYearEndNegativeAmountEmployee(DatabaseUser user, Decimal year, int revision)
        {
            return EmployeeManagement.GetYearEndNegativeAmountEmployee(user, year, revision);
        }
        #endregion

        #region year end zip file naming format
        public String GetYearEndZipFileNamingFormat(String zipFileNameFormat)
        {
            return ReportManagement.GetYearEndZipFileNameFormat(zipFileNameFormat);
        }
        #endregion

        public String GenerateSapFileName(DatabaseUser user, String zipFileNameFormat, long payrollProcessId)
        {
            return ReportManagement.GenerateSapFileName(user, zipFileNameFormat, payrollProcessId);
        }

        #region mass payslip file zip naming format
        public String GenerateFileName(DatabaseUser user, String zipFileNameFormat, long payrollProcessId)
        {
            return ReportManagement.GenerateFileName(user, zipFileNameFormat, payrollProcessId);
        }
        #endregion

        #region create citi cheque eft from payroll master payment records
        public byte[] CreateCitiChequeEftFromPayrollMasterPaymentRecords(DatabaseUser user, String[] payrollMasterPaymentIds, String eftClientNumber, String companyShortName, String employeeNumber, long payrollProccessId, string originatorId, long employeeId)
        {
            return DynamicsManagement.CreateCitiChequeEftFromPayrollMasterPaymentRecords(user, payrollMasterPaymentIds, eftClientNumber, companyShortName, employeeNumber, payrollProccessId, originatorId, employeeId);
        }
        #endregion

        #region employee position labour distribution
        public EmployeePositionLabourDistributionSummaryCollection GetEmployeePositionLabourDistribution(DatabaseUser user, long employeePositionId, String codeLanguageCd)
        {
            return OrganizationUnitManagement.GetEmployeePositionLabourDistribution(user, employeePositionId, codeLanguageCd);
        }
        public EmployeePositionLabourDistributionSummary UpdateEmployeePositionLabourDistribution(DatabaseUser user, EmployeePositionLabourDistributionSummary summary)
        {
            return OrganizationUnitManagement.UpdateEmployeePositionLabourDistribution(user, summary);
        }
        public EmployeePositionLabourDistributionSummary InsertEmployeePositionLabourDistribution(DatabaseUser user, EmployeePositionLabourDistributionSummary summary)
        {
            return OrganizationUnitManagement.InsertEmployeePositionLabourDistribution(user, summary);
        }
        public void DeleteEmployeePositionLabourDistribution(DatabaseUser user, EmployeePositionLabourDistributionSummary summary)
        {
            OrganizationUnitManagement.DeleteEmployeePositionLabourDistribution(user, summary);
        }
        #endregion

        #region employee position secondary
        public EmployeePositionSecondaryCollection GetEmployeePositionSecondary(DatabaseUser user, long? employeePositionSecondaryId, long? employeePositionId, String languageCode, string organizationUnit)
        {
            return EmployeeManagement.GetEmployeePositionSecondary(user, employeePositionSecondaryId, employeePositionId, languageCode, organizationUnit);
        }
        public EmployeePositionSecondary InsertEmployeePositionSecondary(DatabaseUser user, EmployeePositionSecondary item)
        {
            EmployeeManagement.InsertEmployeePositionSecondary(user, item);
            return item;
        }
        public EmployeePositionSecondary UpdateEmployeePositionSecondary(DatabaseUser user, EmployeePositionSecondary item)
        {
            return EmployeeManagement.UpdateEmployeePositionSecondary(user, item);
        }
        public void DeleteEmployeePositionSecondary(DatabaseUser user, EmployeePositionSecondary item)
        {
            EmployeeManagement.DeleteEmployeePositionSecondary(user, item);
        }
        #endregion

        #region employee position secondary organization unit
        public EmployeePositionSecondaryOrganizationUnitCollection GetEmployeePositionSecondaryOrganizationUnit(DatabaseUser user, long? employeePositionSecondaryId)
        {
            return EmployeeManagement.GetEmployeePositionSecondaryOrganizationUnit(user, employeePositionSecondaryId);
        }
        public void DeleteEmployeePositionSecondaryOrganizationUnit(DatabaseUser user, EmployeePositionSecondaryOrganizationUnit item)
        {
            EmployeeManagement.DeleteEmployeePositionSecondaryOrganizationUnit(user, item);
        }
        #endregion

        #region employee photo
        public EmployeePhotoCollection GetEmployeePhoto(DatabaseUser user, long employeeId)
        {
            return EmployeeManagement.GetEmployeePhoto(user, employeeId);
        }
        public void InsertEmployeePhoto(DatabaseUser user, EmployeePhoto photo)
        {
            EmployeeManagement.InsertEmployeePhoto(user, photo);
        }
        public void UpdateEmployeePhoto(DatabaseUser user, EmployeePhoto photo)
        {
            EmployeeManagement.UpdateEmployeePhoto(user, photo);
        }
        public void DeleteEmployeePhoto(DatabaseUser user, EmployeePhoto photo)
        {
            EmployeeManagement.DeleteEmployeePhoto(user, photo);
        }
        #endregion

        #region attachment
        public AttachmentCollection GetAttachment(DatabaseUser user, long attachmentId)
        {
            return EmployeeManagement.GetAttachment(user, attachmentId);
        }
        public void InsertAttachment(DatabaseUser user, Attachment attachment)
        {
            EmployeeManagement.InsertAttachment(user, attachment);
        }
        public void UpdateAttachment(DatabaseUser user, Attachment attachment)
        {
            EmployeeManagement.UpdateAttachment(user, attachment);
        }
        public void DeleteAttachment(DatabaseUser user, Attachment attachment)
        {
            EmployeeManagement.DeleteAttachment(user, attachment);
        }
        #endregion

        #region employee remittance stub
        public EmployeeRemittanceStubCollection GetEmployeeRemittanceStub(DatabaseUser user, long employeePaycodeId)
        {
            return EmployeeManagement.GetEmployeeRemittanceStub(user, employeePaycodeId);
        }
        public void InsertEmployeeRemittanceStub(DatabaseUser user, EmployeeRemittanceStub stub)
        {
            EmployeeManagement.InsertEmployeeRemittanceStub(user, stub);
        }
        public void UpdateEmployeeRemittanceStub(DatabaseUser user, EmployeeRemittanceStub stub)
        {
            EmployeeManagement.UpdateEmployeeRemittanceStub(user, stub);
        }
        public void DeleteEmployeeRemittanceStub(DatabaseUser user, EmployeeRemittanceStub stub)
        {
            EmployeeManagement.DeleteEmployeeRemittanceStub(user, stub);
        }
        #endregion

        #region paycode year end report map
        public PaycodeYearEndReportMapCollection GetPaycodeYearEndReportMap(DatabaseUser user, String paycodeCode)
        {
            return PayrollManagement.GetPaycodeYearEndReportMap(user, paycodeCode);
        }
        public PaycodeYearEndReportMap UpdatePaycodeYearEndReportMap(DatabaseUser user, PaycodeYearEndReportMap paycodeYearEndReportMap)
        {
            PayrollManagement.UpdatePaycodeYearEndReportMap(user, paycodeYearEndReportMap);
            return paycodeYearEndReportMap;
        }
        public PaycodeYearEndReportMap InsertPaycodeYearEndReportMap(DatabaseUser user, PaycodeYearEndReportMap paycodeYearEndReportMap)
        {
            return PayrollManagement.InsertPaycodeYearEndReportMap(user, paycodeYearEndReportMap);
        }
        public void DeletePaycodeYearEndReportMap(DatabaseUser user, PaycodeYearEndReportMap paycodeYearEndReportMap)
        {
            PayrollManagement.DeletePaycodeYearEndReportMap(user, paycodeYearEndReportMap);
        }
        #endregion

        #region paycode payroll transaction offset association
        public PaycodePayrollTransactionOffsetAssociationCollection GetPaycodePayrollTransactionOffsetAssociation(DatabaseUser user, String paycodeCode)
        {
            return PayrollManagement.GetPaycodePayrollTransactionOffsetAssociation(user, paycodeCode);
        }
        public PaycodePayrollTransactionOffsetAssociation UpdatePaycodePayrollTransactionOffsetAssociation(DatabaseUser user, PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation)
        {
            PayrollManagement.UpdatePaycodePayrollTransactionOffsetAssociation(user, paycodePayrollTransactionOffsetAssociation);
            return paycodePayrollTransactionOffsetAssociation;
        }
        public PaycodePayrollTransactionOffsetAssociation InsertPaycodePayrollTransactionOffsetAssociation(DatabaseUser user, PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation)
        {
            return PayrollManagement.InsertPaycodePayrollTransactionOffsetAssociation(user, paycodePayrollTransactionOffsetAssociation);
        }
        public void DeletePaycodePayrollTransactionOffsetAssociation(DatabaseUser user, PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation)
        {
            PayrollManagement.DeletePaycodePayrollTransactionOffsetAssociation(user, paycodePayrollTransactionOffsetAssociation);
        }
        #endregion

        #region accrual entitlement search
        public AccrualEntitlementSearchCollection GetAccrualEntitlementReport(DatabaseUser user, String criteria)
        {
            return AccrualManagement.GetAccrualEntitlementReport(user, criteria);
        }
        public void DeleteAccrualEntitlementReport(DatabaseUser user, long entitlementId)
        {
            AccrualManagement.DeleteAccrualEntitlementReport(user, entitlementId);
        }
        #endregion

        #region accrual entitlement
        public AccrualEntitlementCollection GetAccrualEntitlement(DatabaseUser user, long? entitlementId)
        {
            return AccrualManagement.GetAccrualEntitlement(user, entitlementId);
        }
        public AccrualEntitlement InsertAccrualEntitlement(DatabaseUser user, AccrualEntitlement entitlement)
        {
            return AccrualManagement.InsertAccrualEntitlement(user, entitlement);
        }
        public AccrualEntitlement UpdateAccrualEntitlement(DatabaseUser user, AccrualEntitlement entitlement)
        {
            AccrualManagement.UpdateAccrualEntitlement(user, entitlement);
            return entitlement;
        }
        #endregion

        #region accrual entitlement detail
        public AccrualEntitlementDetailCollection GetAccrualEntitlementDetail(DatabaseUser user, long entitlementId, long entitlementDetailId)
        {
            return AccrualManagement.GetAccrualEntitlementDetail(user, entitlementId, entitlementDetailId);
        }
        public AccrualEntitlementDetail InsertAccrualEntitlementDetail(DatabaseUser user, AccrualEntitlementDetail entitlementDetail)
        {
            return AccrualManagement.InsertAccrualEntitlementDetail(user, entitlementDetail);
        }
        public AccrualEntitlementDetail UpdateAccrualEntitlementDetail(DatabaseUser user, AccrualEntitlementDetail entitlementDetail, AccrualEntitlementDetailValueCollection entitlementDetailValueCollection, AccrualEntitlementDetailPaycodeCollection entitlementDetailPaycodeCollection)
        {
            AccrualManagement.UpdateAccrualEntitlementDetail(user, entitlementDetail, entitlementDetailValueCollection, entitlementDetailPaycodeCollection);
            return entitlementDetail;
        }
        public void DeleteAccrualEntitlementDetail(DatabaseUser user, AccrualEntitlementDetail entitlementDetail)
        {
            AccrualManagement.DeleteAccrualEntitlementDetail(user, entitlementDetail);
        }
        #endregion

        #region accrual entitlement detail paycode
        public AccrualEntitlementDetailPaycodeCollection GetAccrualEntitlementDetailPaycode(DatabaseUser user, long entitlementDetailId)
        {
            return AccrualManagement.GetAccrualEntitlementDetailPaycode(user, entitlementDetailId);
        }
        #endregion

        #region accrual entitlement detail value
        public AccrualEntitlementDetailValueCollection GetAccrualEntitlementDetailValue(DatabaseUser user, long entitlementDetailId)
        {
            return AccrualManagement.GetAccrualEntitlementDetailValue(user, entitlementDetailId);
        }
        #endregion

        #region accrual policy search
        public AccrualPolicySearchCollection GetAccrualPolicyReport(DatabaseUser user, String criteria)
        {
            return AccrualManagement.GetAccrualPolicyReport(user, criteria);
        }
        public void DeleteAccrualPolicyReport(DatabaseUser user, long policyId)
        {
            AccrualManagement.DeleteAccrualPolicyReport(user, policyId);
        }
        #endregion

        #region accrual policy
        public AccrualPolicyCollection GetAccrualPolicy(DatabaseUser user, long? policyId)
        {
            return AccrualManagement.GetAccrualPolicy(user, policyId);
        }
        public AccrualPolicy InsertAccrualPolicy(DatabaseUser user, AccrualPolicy policy, AccrualPolicyEntitlementCollection policyEntitlementCollection)
        {
            return AccrualManagement.InsertAccrualPolicy(user, policy, policyEntitlementCollection);
        }
        public AccrualPolicy UpdateAccrualPolicy(DatabaseUser user, AccrualPolicy policy, AccrualPolicyEntitlementCollection policyEntitlementCollection)
        {
            AccrualManagement.UpdateAccrualPolicy(user, policy, policyEntitlementCollection);
            return policy;
        }
        #endregion

        #region accrual policy entitlement
        public AccrualPolicyEntitlementCollection GetAccrualPolicyEntitlement(DatabaseUser user, long policyId)
        {
            return AccrualManagement.GetAccrualPolicyEntitlement(user, policyId);
        }
        #endregion

        #region global employee paycode
        public EmployeePaycodeCollection GetGlobalEmployeePaycode(DatabaseUser user, long? globalEmployeePaycodeId)
        {
            return EmployeeManagement.GetGlobalEmployeePaycode(user, globalEmployeePaycodeId);
        }
        public EmployeePaycode InsertGlobalEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            return EmployeeManagement.InsertGlobalEmployeePaycode(user, employeePaycode);
        }
        public EmployeePaycode UpdateGlobalEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            EmployeeManagement.UpdateGlobalEmployeePaycode(user, employeePaycode);
            return employeePaycode;
        }
        public void DeleteGlobalEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            EmployeeManagement.DeleteGlobalEmployeePaycode(user, employeePaycode);
        }
        #endregion

        #region statutory holiday date
        public StatutoryHolidayDateCollection GetStatutoryHolidayDateReport(DatabaseUser user, StatutoryDeductionCriteria criteria)
        {
            return EmployeeManagement.GetStatutoryHolidayDateReport(user, criteria);
        }
        public StatutoryHolidayDateCollection GetStatutoryHolidayDate(DatabaseUser user, long? statutoryHolidayDateId)
        {
            return EmployeeManagement.GetStatutoryHolidayDate(user, statutoryHolidayDateId);
        }
        public StatutoryHolidayDate InsertStatutoryHolidayDate(DatabaseUser user, StatutoryHolidayDate statutoryHolidayDate)
        {
            return EmployeeManagement.InsertStatutoryHolidayDate(user, statutoryHolidayDate);
        }
        public StatutoryHolidayDate UpdateStatutoryHolidayDate(DatabaseUser user, StatutoryHolidayDate statutoryHolidayDate)
        {
            EmployeeManagement.UpdateStatutoryHolidayDate(user, statutoryHolidayDate);
            return statutoryHolidayDate;
        }
        public void DeleteStatutoryHolidayDate(DatabaseUser user, StatutoryHolidayDate statutoryHolidayDate)
        {
            EmployeeManagement.DeleteStatutoryHolidayDate(user, statutoryHolidayDate);
        }
        #endregion

        #region remittance
        public RemittanceCollection GetRemittance(DatabaseUser user)
        {
            return PayrollManagement.GetRemittance(user);
        }
        #endregion

        #region remittance detail
        public RemittanceDetailCraSourceDeductionsCollection GetRemittanceDetailCraSourceDeductions(DatabaseUser user, long? craExportId, string remitCode, DateTime remitDate)
        {
            return PayrollManagement.GetRemittanceDetailCraSourceDeductions(user, craExportId, remitCode, remitDate);
        }
        public RemittanceDetailWcbDeductionsCollection GetRemittanceDetailWcbDeductions(DatabaseUser user, string detailType, long? wcbExportId, string remitCode, DateTime remitDate)
        {
            return PayrollManagement.GetRemittanceDetailWcbDeductions(user, detailType, wcbExportId, remitCode, remitDate);
        }
        public RemittanceDetailChequeHealthTaxDeductionsCollection GetRemittanceDetailChequeHealthTaxDeductions(DatabaseUser user, long? chequeHealthTaxExportId, string codeHealthTaxCode)
        {
            return PayrollManagement.GetRemittanceDetailChequeHealthTaxDeductions(user, chequeHealthTaxExportId, codeHealthTaxCode);
        }
        public RemittanceDetailGarnishmentDeductionsCollection GetRemittanceDetailGarnishmentDeductions(DatabaseUser user, string detailType, long? exportId)
        {
            return PayrollManagement.GetRemittanceDetailGarnishmentDeductions(user, detailType, exportId);
        }
        public RemittanceDetailRqSourceDeductionsCollection GetRemittanceDetailRqSourceDeductions(DatabaseUser user, long? rqExportId)
        {
            return PayrollManagement.GetRemittanceDetailRqSourceDeductions(user, rqExportId);
        }
        #endregion

        #region remittance import
        public RemittanceImportCollection GetRemittanceImport(DatabaseUser user, long? remittanceImportId)
        {
            return PayrollManagement.GetRemittanceImport(user, remittanceImportId);
        }
        //public RemittanceImportDetailCollection GetRemittanceImportDetail(DatabaseUser user, long remittanceImportId)
        //{
        //    return PayrollManagement.GetRemittanceImportDetail(user, remittanceImportId);
        //}
        //public void ProcessRemittanceImport(DatabaseUser user, long remittanceImportId, String remittanceImportStatusCode)
        //{
        //    PayrollManagement.ProcessRemittanceImport(user, remittanceImportId, remittanceImportStatusCode);
        //}
        #endregion

        #region email
        public EmailCollection GetEmail(DatabaseUser user, long? emailId)
        {
            return EmailManagement.GetEmail(user, emailId);
        }
        public Email InsertEmail(DatabaseUser user, Email item)
        {
            return EmailManagement.InsertEmail(user, item);
        }
        public Email UpdateEmail(DatabaseUser user, Email item)
        {
            EmailManagement.UpdateEmail(user, item);
            return item;
        }
        public void DeleteEmail(DatabaseUser user, Email item)
        {
            EmailManagement.DeleteEmail(user, item);
        }
        #endregion

        #region email template
        public EmailTemplateCollection GetEmailTemplate(DatabaseUser user, long emailId)
        {
            return EmailManagement.GetEmailTemplate(user, emailId);
        }
        public EmailTemplate InsertEmailTemplate(DatabaseUser user, EmailTemplate item)
        {
            EmailManagement.InsertEmailTemplate(user, item);
            return item;
        }
        public EmailTemplate UpdateEmailTemplate(DatabaseUser user, EmailTemplate item)
        {
            EmailManagement.UpdateEmailTemplate(user, item);
            return item;
        }
        public void DeleteEmailTemplate(DatabaseUser user, EmailTemplate item)
        {
            EmailManagement.DeleteEmailTemplate(user, item);
        }
        #endregion

        #region email rules
        public EmailRuleCollection GetEmailRule(DatabaseUser user, long emailId)
        {
            return EmailManagement.GetEmailRule(user, emailId);
        }
        public EmailRule InsertEmailRule(DatabaseUser user, EmailRule item)
        {
            EmailManagement.InsertEmailRule(user, item);
            return item;
        }
        public EmailRule UpdateEmailRule(DatabaseUser user, EmailRule item)
        {
            EmailManagement.UpdateEmailRule(user, item);
            return item;
        }
        public void DeleteEmailRule(DatabaseUser user, EmailRule item)
        {
            EmailManagement.DeleteEmailRule(user, item);
        }
        #endregion

        #region health tax
        public HealthTaxCollection GetHealthTax(DatabaseUser user, long? healthTaxId)
        {
            return PayrollManagement.GetHealthTax(user, healthTaxId);
        }
        public HealthTax InsertHealthTax(DatabaseUser user, HealthTax healthTax)
        {
            return PayrollManagement.InsertHealthTax(user, healthTax);
        }
        public HealthTax UpdateHealthTax(DatabaseUser user, HealthTax healthTax)
        {
            PayrollManagement.UpdateHealthTax(user, healthTax);
            return healthTax;
        }
        public void DeleteHealthTax(DatabaseUser user, HealthTax healthTax)
        {
            PayrollManagement.DeleteHealthTax(user, healthTax);
        }
        #endregion

        #region health tax detail
        public HealthTaxDetailCollection GetHealthTaxDetail(DatabaseUser user, long healthTaxId)
        {
            return PayrollManagement.GetHealthTaxDetail(user, healthTaxId);
        }
        public HealthTaxDetail InsertHealthTaxDetail(DatabaseUser user, HealthTaxDetail healthTaxDetail)
        {
            return PayrollManagement.InsertHealthTaxDetail(user, healthTaxDetail);
        }
        public HealthTaxDetail UpdateHealthTaxDetail(DatabaseUser user, HealthTaxDetail healthTaxDetail)
        {
            PayrollManagement.UpdateHealthTaxDetail(user, healthTaxDetail);
            return healthTaxDetail;
        }
        public void DeleteHealthTaxDetail(DatabaseUser user, HealthTaxDetail healthTaxDetail)
        {
            PayrollManagement.DeleteHealthTaxDetail(user, healthTaxDetail);
        }
        #endregion

        #region employee payslip
        public EmployeePayslipCollection GetEmployeePayslip(DatabaseUser user)
        {
            return WidgetManagement.GetEmployeePayslip(user);
        }
        #endregion

        #region labour cost
        public LabourCostCollection GetLabourCost(DatabaseUser user, Decimal year)
        {
            return WidgetManagement.GetLabourCost(user, year);
        }
        #endregion

        #region headcount
        public HeadcountCollection GetHeadcount(DatabaseUser user)
        {
            return WidgetManagement.GetHeadcount(user);
        }
        public HeadcountCollection GetHeadcountByOrganizationUnitLevelId(DatabaseUser user, long organizationUnitLevelId)
        {
            return WidgetManagement.GetHeadcountByOrganizationUnitLevelId(user, organizationUnitLevelId);
        }
        #endregion

        #region employee profile
        public EmployeeProfileCollection GetEmployeeProfile(DatabaseUser user)
        {
            return WidgetManagement.GetEmployeeProfile(user);
        }
        #endregion

        #region payroll breakdown
        public PayrollBreakdownCollection GetPayrollBreakdown(DatabaseUser user, String userName, String breakdownType, String filteredBy)
        {
            return WidgetManagement.GetPayrollBreakdown(user, userName, breakdownType, filteredBy);
        }
        #endregion

        #region employee custom field config
        public EmployeeCustomFieldConfigCollection GetEmployeeCustomFieldConfig(DatabaseUser user, long? employeeCustomFieldConfigId)
        {
            return EmployeeManagement.GetEmployeeCustomFieldConfig(user, employeeCustomFieldConfigId);
        }
        public EmployeeCustomFieldConfig InsertEmployeeCustomFieldConfig(DatabaseUser user, EmployeeCustomFieldConfig item)
        {
            return EmployeeManagement.InsertEmployeeCustomFieldConfig(user, item);
        }
        public EmployeeCustomFieldConfig UpdateEmployeeCustomFieldConfig(DatabaseUser user, EmployeeCustomFieldConfig item)
        {
            EmployeeManagement.UpdateEmployeeCustomFieldConfig(user, item);
            return item;
        }
        public void DeleteEmployeeCustomFieldConfig(DatabaseUser user, EmployeeCustomFieldConfig item)
        {
            EmployeeManagement.DeleteEmployeeCustomFieldConfig(user, item);
        }
        #endregion

        #region employee custom field
        public EmployeeCustomFieldCollection GetEmployeeCustomField(DatabaseUser user, long? employeeCustomFieldId)
        {
            return EmployeeManagement.GetEmployeeCustomField(user, employeeCustomFieldId);
        }
        public EmployeeCustomField InsertEmployeeCustomField(DatabaseUser user, EmployeeCustomField item)
        {
            return EmployeeManagement.InsertEmployeeCustomField(user, item);
        }
        public EmployeeCustomField UpdateEmployeeCustomField(DatabaseUser user, EmployeeCustomField item)
        {
            EmployeeManagement.UpdateEmployeeCustomField(user, item);
            return item;
        }
        public void DeleteEmployeeCustomField(DatabaseUser user, EmployeeCustomField item)
        {
            EmployeeManagement.DeleteEmployeeCustomField(user, item);
        }
        #endregion


        #region salary plan grade and effective detail
        public SalaryPlanGradeAndEffectiveDetailCollection GetSalaryPlanGradeAndEffectiveDetail(DatabaseUser user, long salaryPlanId)
        {
            return EmployeeManagement.GetSalaryPlanGradeAndEffectiveDetail(user, salaryPlanId);
        }
        public SalaryPlanGradeMinEffectiveDateCollection SelectMinEffectiveDate(DatabaseUser user, long organizationUnitId)
        {
            return EmployeeManagement.SelectMinEffectiveDate(user, organizationUnitId);
        }
        public void AddNewSalaryPlanGradeAndSteps(DatabaseUser user, SalaryPlanGradeAndEffectiveDetail item)
        {
            EmployeeManagement.AddNewSalaryPlanGradeAndSteps(user, item);
        }
        #endregion


        #region salary plan
        public SalaryPlanCollection GetSalaryPlan(DatabaseUser user, long? salaryPlanId)
        {
            return EmployeeManagement.GetSalaryPlan(user, salaryPlanId);
        }
        public SalaryPlanOrganizationUnitCollection GetSalaryPlanOrganizationUnit(DatabaseUser user, string organizationUnit)
        {
            return EmployeeManagement.GetSalaryPlanOrganizationUnit(user, organizationUnit);
        }
        #endregion

        #region salary plan grade
        public SalaryPlanGradeCollection GetSalaryPlanGrade(DatabaseUser user, long? salaryPlanGradeId, long? salaryPlanId, string languageCode)
        {
            return EmployeeManagement.GetSalaryPlanGrade(user, salaryPlanGradeId, salaryPlanId, languageCode);
        }

        public SalaryPlanGradeOrgUnitDescriptionComboCollection PopulateComboBoxWithSalaryPlanGrades(DatabaseUser user, long salaryPlanId, long organizationUnitLevelId)
        {
            return EmployeeManagement.PopulateComboBoxWithSalaryPlanGrades(user, salaryPlanId, organizationUnitLevelId);
        }

        #endregion

        #region salary plan grade step
        public SalaryPlanGradeStepCollection GetSalaryPlanGradeStep(DatabaseUser user, long? salaryPlanGradeStepId, long? salaryPlanGradeId)
        {
            return EmployeeManagement.GetSalaryPlanGradeStep(user, salaryPlanGradeStepId, salaryPlanGradeId);
        }
        public SalaryPlanGradeStepDetailCollection GetSalaryPlanGradeStepDetail(DatabaseUser user, long? salaryPlanGradeStepDetailId, long? salaryPlanGradeStepId)
        {
            return EmployeeManagement.GetSalaryPlanGradeStepDetail(user, salaryPlanGradeStepDetailId, salaryPlanGradeStepId);
        }
        #endregion


        public void CreateAndExportRealEmployeeFiles(DatabaseUser user, string realFtpName, string laborFileName, string employeeFileName, string adjustmentFileName)
        {
            DynamicsManagement.CreateAndExportRealEmployeeFiles(user, realFtpName, laborFileName, employeeFileName, adjustmentFileName);
        }

        #region labor level export
        public byte[] GenerateREALLaborLevelExportFile(DatabaseUser user)
        {
            return DynamicsManagement.GenerateREALLaborLevelExportFile(user);
        }
        #endregion

        #region adjustment rule export
        public byte[] GenerateREALAdjustmentRuleExportFile(DatabaseUser user)
        {
            return DynamicsManagement.GenerateREALAdjustmentRuleExportFile(user);
        }
        #endregion

        #region employee export
        public byte[] GenerateREALEmployeeExportFile(DatabaseUser user)
        {
            return DynamicsManagement.GenerateREALEmployeeExportFile(user);
        }

        public byte[] GenerateEmployeeBiographicsFile(DatabaseUser user, IEnumerable<long> employeeIds)
        {
            return EmployeeManagement.GetEmployeeBiographicsFile(user, employeeIds);
        }

        #endregion

        public TempBulkPayrollProcessCollection GetTempBulkPayrollProcess(DatabaseUser user)
        {
            return PayrollManagement.GetTempBulkPayrollProcess(user);
        }

        #region benefit policy search
        public BenefitPolicySearchCollection GetBenefitPolicyReport(DatabaseUser user, string criteria)
        {
            return BenefitManagement.GetBenefitPolicyReport(user, criteria);
        }

        public void DeleteBenefitPolicyReport(DatabaseUser user, long benefitPolicyId)
        {
            BenefitManagement.DeleteBenefitPolicyReport(user, benefitPolicyId);
        }
        #endregion

        #region benefit plan search
        public BenefitPlanSearchCollection GetBenefitPlanReport(DatabaseUser user, string criteria)
        {
            return BenefitManagement.GetBenefitPlanReport(user, criteria);
        }

        public void DeleteBenefitPlanReport(DatabaseUser user, long benefitPlanId)
        {
            BenefitManagement.DeleteBenefitPlanReport(user, benefitPlanId);
        }
        #endregion

        #region benefit policy
        public BenefitPolicyCollection GetBenefitPolicy(DatabaseUser user, long? benefitPolicyId)
        {
            return BenefitManagement.GetBenefitPolicy(user, benefitPolicyId);
        }

        public BenefitPolicy InsertBenefitPolicy(DatabaseUser user, BenefitPolicy benefitPolicy, BenefitPolicyPlanCollection benefitPolicyPlanCollection)
        {
            return BenefitManagement.InsertBenefitPolicy(user, benefitPolicy, benefitPolicyPlanCollection);
        }

        public BenefitPolicy UpdateBenefitPolicy(DatabaseUser user, BenefitPolicy benefitPolicy, BenefitPolicyPlanCollection benefitPolicyPlanCollection)
        {
            BenefitManagement.UpdateBenefitPolicy(user, benefitPolicy, benefitPolicyPlanCollection);
            return benefitPolicy;
        }
        #endregion

        #region benefit plan
        public BenefitPlanCollection GetBenefitPlan(DatabaseUser user, long? benefitPlanId)
        {
            return BenefitManagement.GetBenefitPlan(user, benefitPlanId);
        }

        public BenefitPlan InsertBenefitPlan(DatabaseUser user, BenefitPlan benefitPlan)
        {
            return BenefitManagement.InsertBenefitPlan(user, benefitPlan);
        }

        public BenefitPlan UpdateBenefitPlan(DatabaseUser user, BenefitPlan benefitPlan)
        {
            BenefitManagement.UpdateBenefitPlan(user, benefitPlan);
            return benefitPlan;
        }
        #endregion

        #region benefit plan detail
        public BenefitPlanDetailCollection GetBenefitPlanDetail(DatabaseUser user, long benefitPlanId, long benefitPlanDetailId)
        {
            return BenefitManagement.GetBenefitPlanDetail(user, benefitPlanId, benefitPlanDetailId);
        }

        public BenefitPlanDetail InsertBenefitPlanDetail(DatabaseUser user, BenefitPlanDetail benefitPlanDetail)
        {
            return BenefitManagement.InsertBenefitPlanDetail(user, benefitPlanDetail);
        }

        public BenefitPlanDetail UpdateBenefitPlanDetail(DatabaseUser user, BenefitPlanDetail benefitPlanDetail)
        {
            BenefitManagement.UpdateBenefitPlanDetail(user, benefitPlanDetail);
            return benefitPlanDetail;
        }

        public void DeleteBenefitPlanDetail(DatabaseUser user, BenefitPlanDetail benefitPlanDetail)
        {
            BenefitManagement.DeleteBenefitPlanDetail(user, benefitPlanDetail);
        }
        #endregion

        #region benefit policy plan
        public BenefitPolicyPlanCollection GetBenefitPolicyPlan(DatabaseUser user, long benefitPolicyId)
        {
            return BenefitManagement.GetBenefitPolicyPlan(user, benefitPolicyId);
        }
        #endregion

        #region employee benefit
        public EmployeeBenefitCollection GetEmployeeBenefit(DatabaseUser user, long employeeId)
        {
            return EmployeeManagement.GetEmployeeBenefit(user, employeeId);
        }

        public EmployeeBenefit UpdateEmployeeBenefit(DatabaseUser user, EmployeeBenefit employeeBenefit)
        {
            EmployeeManagement.UpdateEmployeeBenefit(user, employeeBenefit);
            return employeeBenefit;
        }

        public EmployeeBenefit InsertEmployeeBenefit(DatabaseUser user, EmployeeBenefit employeeBenefit)
        {
            return EmployeeManagement.InsertEmployeeBenefit(user, employeeBenefit);
        }

        public void DeleteEmployeeBenefit(DatabaseUser user, EmployeeBenefit employeeBenefit)
        {
            EmployeeManagement.DeleteEmployeeBenefit(user, employeeBenefit);
        }

        #endregion

        public SalaryPlanGradeRuleCollection GetSalaryPlanGradeRules(DatabaseUser user)
        {
            return EmployeeManagement.GetSalaryPlanGradeRules(user);
        }

        public SalaryPlanGradeRule InsertPlanGradeRule(DatabaseUser user, SalaryPlanGradeRule item)
        {
            return EmployeeManagement.InsertPlanGradeRule(user, item);
        }

        public SalaryPlanGradeRule UpdatePlanGradeRule(DatabaseUser user, SalaryPlanGradeRule item)
        {
            EmployeeManagement.UpdatePlanGradeRule(user, item);
            return item;
        }

        public SalaryPlanGradeRuleCodeCollection GetSalaryPlanGradeRuleCodes(DatabaseUser user)
        {
            return EmployeeManagement.GetSalaryPlanGradeRuleCodes(user);
        }

        public SalaryPlanGradeRuleCode InsertSalaryPlanGradeRuleCode(DatabaseUser user, SalaryPlanGradeRuleCode code)
        {
            return EmployeeManagement.InsertGradeRuleCode(user, code);
        }

        public SalaryPlanGradeRuleCode UpdateSalaryPlanGradeRuleCode(DatabaseUser user, SalaryPlanGradeRuleCode code)
        {
            return EmployeeManagement.UpdateGradeRuleCode(user, code);
        }

        public void DeleteSalaryPlanGradeRuleCode(DatabaseUser user, SalaryPlanGradeRuleCode code)
        {
            EmployeeManagement.DeleteGradeRuleCode(user, code);
        }
    }
}