﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Web.BusinessService
{
    public class Code : Contract.ICode
    {
        #region fields
        private BusinessLayer.BusinessLogic.CodeManagement _management;
        #endregion

        #region properties
        private BusinessLayer.BusinessLogic.CodeManagement Management
        {
            get
            {
                if (_management == null)
                    _management = new BusinessLayer.BusinessLogic.CodeManagement();

                return _management;
            }
        }
        #endregion

        #region field language editor
        public LanguageEntityCollection GetFieldLanguageInfo(DatabaseUser user, Decimal formId, String attributeIdentifier, String englishLanguageCode)
        {
            return Management.GetFieldLanguageInfo(user, formId, attributeIdentifier, englishLanguageCode);
        }
        public LanguageEntityCollection UpdateFieldLanguage(DatabaseUser user, LanguageEntity[] entityCollection, String englishLanguageCode, String frenchLanguageCode)
        {
            LanguageEntityCollection coll = new LanguageEntityCollection();
            coll.Load(entityCollection);
            Management.UpdateFieldLanguage(user, coll, englishLanguageCode, frenchLanguageCode);
            return coll;
        }
        #endregion

        #region field editor
        public FormDescriptionCollection GetFormInfo(DatabaseUser user, String criteria)
        {
            return Management.GetFormInfo(user, criteria);
        }
        public FieldEntityCollection GetFieldInfo(DatabaseUser user, Decimal formId, String attributeIdentifier, int roleId)
        {
            return Management.GetFieldInfo(user, formId, attributeIdentifier, roleId);
        }
        public FieldEntityCollection UpdateFieldEntity(DatabaseUser user, FieldEntity[] entityCollection)
        {
            FieldEntityCollection coll = new FieldEntityCollection();
            coll.Load(entityCollection);
            Management.UpdateFieldEntity(user, coll);
            return coll;
        }
        #endregion

        #region codeTableDescription calls
        public CodeTableDescriptionCollection GetCodeTableDescriptionRows(DatabaseUser user, String tableName, String code)
        {
            return Management.GetCodeTableDescriptionRows(user, tableName, code);
        }
        public CodeTable UpdateCodeTableDescRow(DatabaseUser user, CodeTable codeTableDesc, String codeTableName)
        {
            Management.UpdateCodeTableDescRow(user, codeTableDesc, codeTableName);
            return codeTableDesc;
        }
        public void DeleteCodeTableDescRow(DatabaseUser user, CodeTable codeTableDesc, String codeTableName)
        {
            Management.DeleteCodeTableDescRow(user, codeTableDesc, codeTableName);
        }
        public CodeTable InsertCodeTableDescRow(DatabaseUser user, CodeTable codeTableDesc, String codeTableName)
        {
            Management.InsertCodeTableDescRow(user, codeTableDesc, codeTableName);
            return codeTableDesc;
        }
        #endregion

        #region codeTable calls
        public CodeTableCollection GetCodeTableRows(DatabaseUser user, String tableName, String parentTableName)
        {
            return Management.GetCodeTableRows(user, tableName, parentTableName);
        }
        public CodeTable UpdateCodeTableData(DatabaseUser user, CodeTable codeTable, String codeTableName, String parentTableName)
        {
            CodeTable tmpCodeTable = Management.UpdateCodeTableData(user, codeTable, codeTableName, parentTableName);
            return tmpCodeTable;
        }
        public CodeTable UpdateCodeTableRow(DatabaseUser user, CodeTable codeTable, String codeTableName, String parentTableName)
        {
            Management.UpdateCodeTableRow(user, codeTable, codeTableName, parentTableName);
            return codeTable;
        }
        public void DeleteCodeTableData(DatabaseUser user, CodeTable codeTable, String codeTableName)
        {
            Management.DeleteCodeTableData(user, codeTable, codeTableName);
        }
        public void DeleteCodeTableRow(DatabaseUser user, CodeTable codeTable, String codeTableName)
        {
            Management.DeleteCodeTableRow(user, codeTable, codeTableName);
        }
        public CodeTable InsertCodeTableData(DatabaseUser user, CodeTable codeTable, String codeTableName, String parentTableName)
        {
            CodeTable tmpCodeTable = Management.InsertCodeTableData(user, codeTable, codeTableName, parentTableName);
            return tmpCodeTable;
        }
        public CodeTable InsertCodeTableRow(DatabaseUser user, CodeTable codeTable, String codeTableName, String parentTableName)
        {
            Management.InsertCodeTableRow(user, codeTable, codeTableName, parentTableName);
            return codeTable;
        }
        #endregion

        #region salary plan code table
        public SalaryPlanCodeTableCollection GetSalaryPlanCodeTableRows(DatabaseUser user, String tableName, String parentSalaryPlanCode)
        {
            return Management.GetSalaryPlanCodeTableRows(user, tableName, parentSalaryPlanCode);
        }
        public SalaryPlanCodeTable InsertSalaryPlanCodeTableData(DatabaseUser user, SalaryPlanCodeTable salaryPlanCodeTable)
        {
            SalaryPlanCodeTable tmpSalaryPlanCodeTable = Management.InsertSalaryPlanCodeTableData(user, salaryPlanCodeTable);
            return tmpSalaryPlanCodeTable;
        }
        public SalaryPlanCodeTable UpdateSalaryPlanCodeTableData(DatabaseUser user, SalaryPlanCodeTable salaryPlanCodeTable)
        {
            SalaryPlanCodeTable tmpSalaryPlanCodeTable = Management.UpdateSalaryPlanCodeTableData(user, salaryPlanCodeTable);
            return tmpSalaryPlanCodeTable;
        }
        public void DeleteSalaryPlanCodeTableData(DatabaseUser user, SalaryPlanCodeTable salaryPlanCodeTable)
        {
            Management.DeleteSalaryPlanCodeTableData(user, salaryPlanCodeTable);
        }
        #endregion

        #region personAddressType code table
        public PersonAddressTypeCodeTableCollection GetPersonAddressTypeCodeTableRows(DatabaseUser user)
        {
            return Management.GetPersonAddressTypeCodeTableRows(user);
        }
        public PersonAddressTypeCodeTable GetPersonAddressTypeCodeTableRow(DatabaseUser user, short priority)
        {
            return Management.GetPersonAddressTypeCodeTableRow(user, priority);
        }
        public PersonAddressTypeCodeTable InsertPersonAddressTypeCodeTableData(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTable)
        {
            PersonAddressTypeCodeTable tmpPersonAddressTypeCodeTable = Management.InsertPersonAddressTypeCodeTableData(user, personAddressTypeCodeTable);
            return tmpPersonAddressTypeCodeTable;
        }
        public PersonAddressTypeCodeTable UpdatePersonAddressTypeCodeTableData(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTable)
        {
            PersonAddressTypeCodeTable tmpPersonAddressTypeCodeTable = Management.UpdatePersonAddressTypeCodeTableData(user, personAddressTypeCodeTable);
            return tmpPersonAddressTypeCodeTable;
        }
        public void DeletePersonAddressTypeCodeTableData(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTable)
        {
            Management.DeletePersonAddressTypeCodeTableData(user, personAddressTypeCodeTable);
        }
        #endregion

        #region codeTableIndex calls
        public CodeTableIndexCollection GetCodeTableIndex(DatabaseUser user, String tableName)
        {
            return Management.GetCodeTableIndex(user, tableName);
        }
        #endregion

        #region codeCollection calls
        public CodeCollection GetCodeTable(String databaseName, String language, CodeTableType type, String code)
        {
            CodeCollection collection = null;

            if (code == null || code.Equals(String.Empty))
                collection = Management.GetCodeTable(databaseName, language, type);
            else
                collection = Management.GetCodeTable(databaseName, language, type, code);

            return collection;
        }
        /// <summary>
        /// Use this method when need to retrieve data by parent column name and value. 
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="language"></param>
        /// <param name="type"></param>
        /// <param name="code"></param>
        /// <param name="parentColumnName"></param>
        /// <param name="parentColumnValue"></param>
        /// <returns></returns>
        public CodeCollection GetCodeTableByParentColumn(String databaseName, String language, CodeTableType type, String code, String parentColumnName, String parentColumnValue)
        {
            CodeCollection collection = null;

            if (code == null || code.Equals(String.Empty))
                collection = Management.GetCodeTable(databaseName, language, type, parentColumnName, parentColumnValue);
            else
                collection = Management.GetCodeTable(databaseName, language, type, code, parentColumnName, parentColumnValue);

            return collection;
        }
        public CodeCollection GetDisciplineTypeCode(DatabaseUser user, string disciplineCode)
        {
            return Management.GetDisciplineTypeCode(user, disciplineCode);
        }
        public CodeCollection GetProvinceStateCode(DatabaseUser user, String country)
        {
            return Management.GetProvinceStateCode(user, country);
        }
        public CodeCollection GetDayCode(DatabaseUser user, String monthCode)
        {
            return Management.GetDayCode(user, monthCode);
        }
        public CodeCollection GetYearEndFormBoxCode(DatabaseUser user, String yearEndFormCode)
        {
            return Management.GetYearEndFormBoxCode(user, yearEndFormCode);
        }
        public CodeCollection GetPaycodeTypeCode(DatabaseUser user, String paycode)
        {
            return Management.GetPaycodeTypeCode(user, paycode);
        }
        public CodeCollection GetContactChannelTypeCode(DatabaseUser user, String contactChannelCategoryCode)
        {
            return Management.GetContactChannelTypeCode(user, contactChannelCategoryCode);
        }
        public CodeCollection GetContactChannelTypeCodeRemovingTypesInUse(DatabaseUser user, String contactChannelCategoryCode, long personId)
        {
            return Management.GetContactChannelTypeCodeRemovingTypesInUse(user, contactChannelCategoryCode, personId);
        }
        public CodeCollection GetContactChannelTypeCodeRemovingTypesInUseLeavingSelected(DatabaseUser user, String contactChannelCategoryCode, long personId, String selectedCode)
        {
            return Management.GetContactChannelTypeCodeRemovingTypesInUseLeavingSelected(user, contactChannelCategoryCode, personId, selectedCode);
        }
        public CodeCollection GetPaycodesByType(DatabaseUser user, String paycodeTypeCode)
        {
            return Management.GetPaycodesByType(user, paycodeTypeCode);
        }
        public CodeCollection GetPaycodeByCode(DatabaseUser user, String paycodeCode)
        {
            return Management.GetPaycodeByCode(user, paycodeCode);
        }
        public CodeCollection GetPaycodesByTypeRemovingPaycodesInUse(DatabaseUser user, String paycodeTypeCode, long? employeeId)
        {
            return Management.GetPaycodesByTypeRemovingPaycodesInUse(user, paycodeTypeCode, employeeId);
        }
        public CodeCollection GetPaycodesByTypeRemovingGlobalPaycodesInUse(DatabaseUser user, String paycodeTypeCode)
        {
            return Management.GetPaycodesByTypeRemovingGlobalPaycodesInUse(user, paycodeTypeCode);
        }
        public CodeCollection RemoveNonRecurringIncomeCodes(DatabaseUser user, CodeCollection collection)
        {
            return Management.RemoveNonRecurringIncomeCodes(collection);
        }
        public CodeCollection GetRecurringIncomePaycodesRemovingPaycodesInUse(DatabaseUser user, String paycodeTypeCode, long? employeeId)
        {
            return Management.GetRecurringIncomePaycodesRemovingPaycodesInUse(user, paycodeTypeCode, employeeId);
        }
        public CodeCollection GetPaycodesRemovingAssociationsInUse(DatabaseUser user, String paycodeAssociationTypeCode)
        {
            return Management.GetPaycodesRemovingAssociationsInUse(user, paycodeAssociationTypeCode);
        }
        public CodeCollection GetRecurringIncomePaycodesRemovingGlobalPaycodesInUse(DatabaseUser user, String paycodeTypeCode)
        {
            return Management.GetRecurringIncomePaycodesRemovingGlobalPaycodesInUse(user, paycodeTypeCode);
        }
        public CodeCollection GetEmployeeCustomFieldNameCodesRemovingInUse(DatabaseUser user)
        {
            return Management.GetEmployeeCustomFieldNameCodesRemovingInUse(user);
        }
        public CodeCollection GetBusinessNumber(DatabaseUser user, long? id)
        {
            return Management.GetBusinessNumber(user, id);
        }
        public bool IsPaycodeGarnishment(DatabaseUser user, String paycode, String paycodeType)
        {
            return Management.IsPaycodeGarnishment(user, paycode, paycodeType);
        }
        public CodeCollection GetUnionCollectiveAgreementCode(DatabaseUser user, String unionCode)
        {
            return Management.GetUnionCollectiveAgreementCode(user, unionCode);
        }
        public CodeCollection GetEmployeeBankCode(DatabaseUser user, String bankingCountryCode)
        {
            return Management.GetEmployeeBankCode(user, bankingCountryCode);
        }
        #endregion

        #region code paycode
        public CodePaycodeCollection GetCodePaycode(DatabaseUser user, String paycode, bool useExternalFlag)
        {
            return Management.GetCodePaycode(user, paycode, useExternalFlag);
        }
        public CodePaycode UpdateCodePaycode(DatabaseUser user, CodePaycode paycode, bool overrideConcurrencyCheckFlag)
        {
            Management.UpdateCodePaycode(user, paycode, overrideConcurrencyCheckFlag);
            return paycode;
        }
        public CodePaycode InsertCodePaycode(DatabaseUser user, CodePaycode paycode)
        {
            return Management.InsertCodePaycode(user, paycode);
        }
        public void DeleteCodePaycode(DatabaseUser user, CodePaycode paycode)
        {
            Management.DeleteCodePaycode(user, paycode);
        }
        #endregion

        #region code paycode benefit
        public CodePaycodeBenefitCollection GetCodePaycodeBenefit(DatabaseUser user, String paycodeCode)
        {
            return Management.GetCodePaycodeBenefit(user, paycodeCode);
        }
        public CodePaycodeBenefit UpdateCodePaycodeBenefit(DatabaseUser user, CodePaycodeBenefit paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false)
        {
            Management.UpdateCodePaycodeBenefit(user, paycode, overrideConcurrencyCheckFlag, paycodeImport);
            return paycode;
        }
        public CodePaycodeBenefit InsertCodePaycodeBenefit(DatabaseUser user, CodePaycodeBenefit paycode)
        {
            return Management.InsertCodePaycodeBenefit(user, paycode);
        }
        #endregion

        #region code paycode deduction
        public CodePaycodeDeductionCollection GetCodePaycodeDeduction(DatabaseUser user, String paycodeCode)
        {
            return Management.GetCodePaycodeDeduction(user, paycodeCode);
        }
        public CodePaycodeDeduction UpdateCodePaycodeDeduction(DatabaseUser user, CodePaycodeDeduction paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false)
        {
            Management.UpdateCodePaycodeDeduction(user, paycode, overrideConcurrencyCheckFlag, paycodeImport);
            return paycode;
        }
        public CodePaycodeDeduction InsertCodePaycodeDeduction(DatabaseUser user, CodePaycodeDeduction paycode)
        {
            return Management.InsertCodePaycodeDeduction(user, paycode);
        }
        #endregion

        #region code paycome income
        public CodePaycodeIncomeCollection GetCodePaycodeIncome(DatabaseUser user, String paycodeCode)
        {
            return Management.GetCodePaycodeIncome(user, paycodeCode);
        }
        public CodePaycodeIncome UpdateCodePaycodeIncome(DatabaseUser user, CodePaycodeIncome paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false)
        {
            Management.UpdateCodePaycodeIncome(user, paycode, overrideConcurrencyCheckFlag, paycodeImport);
            return paycode;
        }
        public CodePaycodeIncome InsertCodePaycodeIncome(DatabaseUser user, CodePaycodeIncome paycode)
        {
            return Management.InsertCodePaycodeIncome(user, paycode);
        }
        #endregion

        #region code paycome employer
        public CodePaycodeEmployerCollection GetCodePaycodeEmployer(DatabaseUser user, String paycodeCode)
        {
            return Management.GetCodePaycodeEmployer(user, paycodeCode);
        }
        public CodePaycodeEmployer UpdateCodePaycodeEmployer(DatabaseUser user, CodePaycodeEmployer paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false)
        {
            Management.UpdateCodePaycodeEmployer(user, paycode, overrideConcurrencyCheckFlag, paycodeImport);
            return paycode;
        }
        public CodePaycodeEmployer InsertCodePaycodeEmployer(DatabaseUser user, CodePaycodeEmployer paycode)
        {
            return Management.InsertCodePaycodeEmployer(user, paycode);
        }
        #endregion

        #region paycode attached paycode provision
        public PaycodeAttachedPaycodeProvisionCollection GetPaycodeAttachedPaycodeProvision(DatabaseUser user, String paycodeCode)
        {
            return Management.GetPaycodeAttachedPaycodeProvision(user, paycodeCode);
        }
        #endregion

        #region paycode export
        public byte[] ExportPaycodeFile(DatabaseUser user, String paycodeCode, String paycodeType)
        {
            return Management.ExportPaycodeFile(user, paycodeCode, paycodeType);
        }
        #endregion

        #region code payroll processing group
        public Decimal GetCurrentPayrollYear(DatabaseUser user)
        {
            return Management.GetCurrentPayrollYear(user);
        }
        public PayrollProcessingGroupCollection GetPayrollProcessingGroup(DatabaseUser user)
        {
            return Management.GetPayrollProcessingGroup(user);
        }
        public PayrollProcessingGroup UpdatePayrollProcessingGroup(DatabaseUser user, PayrollProcessingGroup ppG)
        {
            Management.UpdatePayrollProcessingGroup(user, ppG);
            return ppG;
        }
        public PayrollProcessingGroup InsertPayrollProcessingGroupCode(DatabaseUser user, PayrollProcessingGroup ppG)
        {
            PayrollProcessingGroup tmpPPG = Management.InsertPayrollProcessingGroup(user, ppG);
            return tmpPPG;
        }
        public void DeletePayrollProcessingGroup(DatabaseUser user, PayrollProcessingGroup ppG)
        {
            Management.DeletePayrollProcessingGroup(user, ppG);
        }
        #endregion

        #region payroll process group override period
        public PayrollProcessGroupOverridePeriodCollection GetPayrollProcessGroupOverridePeriod(DatabaseUser user, String payrollProcessGroupCode)
        {
            return Management.GetPayrollProcessGroupOverridePeriod(user, payrollProcessGroupCode);
        }
        public PayrollProcessGroupOverridePeriod InsertPayrollProcessGroupOverridePeriod(DatabaseUser user, PayrollProcessGroupOverridePeriod overridePeriod)
        {
            return Management.InsertPayrollProcessGroupOverridePeriod(user, overridePeriod);
        }
        public PayrollProcessGroupOverridePeriod UpdatePayrollProcessGroupOverridePeriod(DatabaseUser user, PayrollProcessGroupOverridePeriod overridePeriod)
        {
            Management.UpdatePayrollProcessGroupOverridePeriod(user, overridePeriod);
            return overridePeriod;
        }
        public void DeletePayrollProcessGroupOverridePeriod(DatabaseUser user, PayrollProcessGroupOverridePeriod overridePeriod)
        {
            Management.DeletePayrollProcessGroupOverridePeriod(user, overridePeriod);
        }
        #endregion

        #region code system
        public CodeSystemCollection GetCodeSystem(DatabaseUser user)
        {
            return Management.GetCodeSystem(user);
        }
        public CodeSystem UpdateCodeSystem(DatabaseUser user, CodeSystem code)
        {
            Management.UpdateCodeSystem(user, code);
            return code;
        }
        public CodeSystem InsertCodeSystem(DatabaseUser user, CodeSystem code)
        {
            return Management.InsertCodeSystem(user, code);
        }
        public void DeleteCodeSystem(DatabaseUser user, CodeSystem code)
        {
            Management.DeleteCodeSystem(user, code);
        }
        #endregion

        #region paycode association
        public PaycodeAssociationTypeCollection GetPaycodeAssociationType(DatabaseUser user, String paycodeAssociationTypeCode)
        {
            return Management.GetPaycodeAssociationType(user, paycodeAssociationTypeCode);
        }
        public PaycodeAssociationCollection GetPaycodeAssociation(DatabaseUser user, String paycodeAssociationTypeCode)
        {
            return Management.GetPaycodeAssociation(user, paycodeAssociationTypeCode);
        }
        public PaycodeAssociationType InsertPaycodeAssociationType(DatabaseUser user, PaycodeAssociationType paycode)
        {
            return Management.InsertPaycodeAssociationType(user, paycode);
        }
        public PaycodeAssociation InsertPaycodeAssociation(DatabaseUser user, PaycodeAssociation paycode)
        {
            return Management.InsertPaycodeAssociation(user, paycode);
        }
        public PaycodeAssociationType UpdatePaycodeAssociationType(DatabaseUser user, PaycodeAssociationType paycode)
        {
            Management.UpdatePaycodeAssociationType(user, paycode);
            return paycode;
        }
        public PaycodeAssociation UpdatePaycodeAssociation(DatabaseUser user, PaycodeAssociation paycode)
        {
            Management.UpdatePaycodeAssociation(user, paycode);
            return paycode;
        }
        public void DeletePaycodeAssociationType(DatabaseUser user, PaycodeAssociationType paycode)
        {
            Management.DeletePaycodeAssociationType(user, paycode);
        }
        public void DeletePaycodeAssociation(DatabaseUser user, PaycodeAssociation paycode)
        {
            Management.DeletePaycodeAssociation(user, paycode);
        }
        #endregion

        #region third party

        public CodeThirdParty InsertThirdPartyCode(DatabaseUser user, CodeThirdParty thirdParty)
        {
            return Management.InsertThirdPartyCode(user, thirdParty);
        }
        public CodeThirdPartyCollection GetCodeThirdParty(DatabaseUser user, string thirdPartyCode)
        {
            return Management.GetCodeThirdParty(user, thirdPartyCode);
        }
        public void DeleteThirdPartyCode(DatabaseUser user, CodeThirdParty thirdParty)
        {
            Management.DeleteThirdPartyCode(user, thirdParty);
        }
        public CodeThirdParty UpdateThirdPartyCode(DatabaseUser user, CodeThirdParty thirdParty)
        {
            Management.UpdateThirdPartyCode(user, thirdParty);
            return thirdParty;
        }
        #endregion

        #region code wsib
        public CodeWsibCollection GetCodeWsib(DatabaseUser user, String wsibCode)
        {
            return Management.GetCodeWsib(user, wsibCode);
        }
        public CodeWsib UpdateCodeWsib(DatabaseUser user, CodeWsib codeWsib)
        {
            Management.UpdateCodeWsib(user, codeWsib);
            return codeWsib;
        }
        public CodeWsib InsertCodeWsib(DatabaseUser user, CodeWsib codeWsib)
        {
            return Management.InsertCodeWsib(user, codeWsib);
        }
        public void DeleteCodeWsib(DatabaseUser user, CodeWsib codeWsib)
        {
            Management.DeleteCodeWsib(user, codeWsib);
        }
        public CodeWsibEffectiveCollection GetCodeWsibEffective(DatabaseUser user, String wsibCode)
        {
            return Management.GetCodeWsibEffective(user, wsibCode);
        }
        public CodeWsibEffective InsertCodeWsibEffective(DatabaseUser user, CodeWsibEffective codeWsibEffective)
        {
            return Management.InsertCodeWsibEffective(user, codeWsibEffective);
        }
        public CodeWsibEffective UpdateCodeWsibEffective(DatabaseUser user, CodeWsibEffective codeWsibEffective)
        {
            Management.UpdateCodeWsibEffective(user, codeWsibEffective);
            return codeWsibEffective;
        }
        public void DeleteCodeWsibEffective(DatabaseUser user, CodeWsibEffective codeWsibEffective)
        {
            Management.DeleteCodeWsibEffective(user, codeWsibEffective);
        }
        #endregion

        #region code wcb cheque remittance frequency
        public CodeWcbChequeRemittanceFrequencyCollection GetCodeWcbChequeRemittanceFrequency(DatabaseUser user, String wcbChequeRemittanceFrequencyCode)
        {
            return Management.GetCodeWcbChequeRemittanceFrequency(user, wcbChequeRemittanceFrequencyCode);
        }
        public CodeWcbChequeRemittanceFrequency InsertCodeWcbChequeRemittanceFrequency(DatabaseUser user, CodeWcbChequeRemittanceFrequency codeWcbChequeRemittanceFrequency)
        {
            return Management.InsertCodeWcbChequeRemittanceFrequency(user, codeWcbChequeRemittanceFrequency);
        }
        public CodeWcbChequeRemittanceFrequency UpdateCodeWcbChequeRemittanceFrequency(DatabaseUser user, CodeWcbChequeRemittanceFrequency codeWcbChequeRemittanceFrequency)
        {
            Management.UpdateCodeWcbChequeRemittanceFrequency(user, codeWcbChequeRemittanceFrequency);
            return codeWcbChequeRemittanceFrequency;
        }
        public void DeleteCodeWcbChequeRemittanceFrequency(DatabaseUser user, CodeWcbChequeRemittanceFrequency codeWcbChequeRemittanceFrequency)
        {
            Management.DeleteCodeWcbChequeRemittanceFrequency(user, codeWcbChequeRemittanceFrequency);
        }
        public WcbChequeRemittanceFrequencyDetailCollection GetWcbChequeRemittanceFrequencyDetail(DatabaseUser user, String wcbChequeRemittanceFrequencyCode)
        {
            return Management.GetWcbChequeRemittanceFrequencyDetail(user, wcbChequeRemittanceFrequencyCode);
        }
        public WcbChequeRemittanceFrequencyDetail InsertWcbChequeRemittanceFrequencyDetail(DatabaseUser user, WcbChequeRemittanceFrequencyDetail wcbChequeRemittanceFrequencyDetail)
        {
            return Management.InsertWcbChequeRemittanceFrequencyDetail(user, wcbChequeRemittanceFrequencyDetail);
        }
        public WcbChequeRemittanceFrequencyDetail UpdateWcbChequeRemittanceFrequencyDetail(DatabaseUser user, WcbChequeRemittanceFrequencyDetail wcbChequeRemittanceFrequencyDetail)
        {
            Management.UpdateWcbChequeRemittanceFrequencyDetail(user, wcbChequeRemittanceFrequencyDetail);
            return wcbChequeRemittanceFrequencyDetail;
        }
        public void DeleteWcbChequeRemittanceFrequencyDetail(DatabaseUser user, WcbChequeRemittanceFrequencyDetail wcbChequeRemittanceFrequencyDetail)
        {
            Management.DeleteWcbChequeRemittanceFrequencyDetail(user, wcbChequeRemittanceFrequencyDetail);
        }
        #endregion

        #region security
        public CodeCollection GetRoleDescription(DatabaseUser user, String securityRoleType, bool useSystemRoleId, bool? worklinksAdministrationFlag)
        {
            return Management.GetRoleDescription(user, securityRoleType, useSystemRoleId, worklinksAdministrationFlag);
        }
        public CodeCollection GetRoleDescriptionCombo(DatabaseUser user, String securityRoleType, String selectedValue, bool useSystemRoleId, bool? worklinksAdministrationFlag)
        {
            return Management.GetRoleDescriptionCombo(user, securityRoleType, selectedValue, useSystemRoleId, worklinksAdministrationFlag);
        }
        #endregion

        #region revenue quebec business 

        public RevenueQuebecBusiness GetRevenueQuebecBusiness(DatabaseUser user)
        {
            return Management.GetRevenueQuebecBusiness(user);
        }
        public bool ValidateModulus11CheckDigit(string businessId)
        {
            return Management.ValidateModulus11CheckDigit(businessId);
        }
        public RevenueQuebecBusiness UpdateRevenueQuebecBusiness(DatabaseUser user, RevenueQuebecBusiness revenueQuebecBusiness)
        {
            return Management.UpdateRevenueQuebecBusiness(user, revenueQuebecBusiness);
        }
        #endregion
    }
}