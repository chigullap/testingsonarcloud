﻿using System;
using System.ServiceModel;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Web.BusinessService.Contract
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICodeService" in both code and config file together.
    [ServiceContract]
    public interface ICode
    {
        #region field language editor
        [OperationContract]
        LanguageEntityCollection GetFieldLanguageInfo(DatabaseUser user, Decimal formId, String attributeIdentifier, String englishLanguageCode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        LanguageEntityCollection UpdateFieldLanguage(DatabaseUser user, LanguageEntity[] entityCollection, String englishLanguageCode, String frenchLanguageCode);
        #endregion

        #region field editor
        //get form names for field editor
        [OperationContract]
        FormDescriptionCollection GetFormInfo(DatabaseUser user, String criteria);

        [OperationContract]
        FieldEntityCollection GetFieldInfo(DatabaseUser user, Decimal formId, String attributeIdentifier, int roleId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        FieldEntityCollection UpdateFieldEntity(DatabaseUser user, FieldEntity[] entityCollection);
        #endregion

        #region code table description
        [OperationContract]
        CodeTableDescriptionCollection GetCodeTableDescriptionRows(DatabaseUser user, String tableName, String code);

        [OperationContract]
        CodeTable UpdateCodeTableDescRow(DatabaseUser user, CodeTable codeTableDesc, String codeTableName);

        [OperationContract]
        void DeleteCodeTableDescRow(DatabaseUser user, CodeTable codeTableDesc, String codeTableName);

        [OperationContract]
        CodeTable InsertCodeTableDescRow(DatabaseUser user, CodeTable codeTableDesc, String codeTableName);
        #endregion

        #region code table
        [OperationContract]
        CodeTableCollection GetCodeTableRows(DatabaseUser user, String tableName, String parentTableName);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        CodeTable UpdateCodeTableData(DatabaseUser user, CodeTable codeTable, String codeTableName, String parentTableName);

        [OperationContract]
        CodeTable UpdateCodeTableRow(DatabaseUser user, CodeTable codeTable, String codeTableName, String parentTableName);

        [OperationContract]
        CodeTable InsertCodeTableData(DatabaseUser user, CodeTable codeTable, String codeTableName, String parentTableName);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteCodeTableData(DatabaseUser user, CodeTable codeTable, String codeTableName);

        [OperationContract]
        void DeleteCodeTableRow(DatabaseUser user, CodeTable codeTable, String codeTableName);

        [OperationContract]
        CodeTable InsertCodeTableRow(DatabaseUser user, CodeTable codeTable, String codeTableName, String parentTableName);

        [OperationContract]
        CodeTableIndexCollection GetCodeTableIndex(DatabaseUser user, String tableName);

        [OperationContract]
        CodeCollection GetCodeTable(String databaseName, String language, CodeTableType type, String code);

        [OperationContract]
        CodeCollection GetCodeTableByParentColumn(String databaseName, String language, CodeTableType type, String code, String parentColumnName, String parentColumnValue);
        #endregion

        #region salary plan code table
        [OperationContract]
        SalaryPlanCodeTableCollection GetSalaryPlanCodeTableRows(DatabaseUser user, String tableName, String parentSalaryPlanCode);

        [OperationContract]
        SalaryPlanCodeTable InsertSalaryPlanCodeTableData(DatabaseUser user, SalaryPlanCodeTable salaryPlanCodeTable);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        SalaryPlanCodeTable UpdateSalaryPlanCodeTableData(DatabaseUser user, SalaryPlanCodeTable salaryPlanCodeTable);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteSalaryPlanCodeTableData(DatabaseUser user, SalaryPlanCodeTable salaryPlanCodeTable);
        #endregion

        #region personAddressType code table
        [OperationContract]
        PersonAddressTypeCodeTableCollection GetPersonAddressTypeCodeTableRows(DatabaseUser user);

        [OperationContract]
        PersonAddressTypeCodeTable GetPersonAddressTypeCodeTableRow(DatabaseUser user, short priority);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        PersonAddressTypeCodeTable UpdatePersonAddressTypeCodeTableData(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTable);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeletePersonAddressTypeCodeTableData(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTable);

        [OperationContract]
        PersonAddressTypeCodeTable InsertPersonAddressTypeCodeTableData(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTable);
        #endregion

        #region contact channel
        [OperationContract]
        CodeCollection GetContactChannelTypeCode(DatabaseUser user, String contactChannelCategoryCode);

        [OperationContract]
        CodeCollection GetContactChannelTypeCodeRemovingTypesInUse(DatabaseUser user, String contactChannelCategoryCode, long personId);

        [OperationContract]
        CodeCollection GetContactChannelTypeCodeRemovingTypesInUseLeavingSelected(DatabaseUser user, String contactChannelCategoryCode, long personId, String selectedCode);
        #endregion

        #region paycodes
        [OperationContract]
        CodeCollection GetPaycodeTypeCode(DatabaseUser user, String paycode);

        [OperationContract]
        CodeCollection GetPaycodesByType(DatabaseUser user, String paycodeTypeCode);

        [OperationContract]
        CodeCollection GetPaycodeByCode(DatabaseUser user, String paycodeCode);

        [OperationContract]
        CodeCollection GetPaycodesByTypeRemovingPaycodesInUse(DatabaseUser user, String paycodeTypeCode, long? employeeId);

        [OperationContract]
        CodeCollection GetPaycodesByTypeRemovingGlobalPaycodesInUse(DatabaseUser user, String paycodeTypeCode);

        [OperationContract]
        CodeCollection RemoveNonRecurringIncomeCodes(DatabaseUser user, CodeCollection collection);

        [OperationContract]
        CodeCollection GetRecurringIncomePaycodesRemovingPaycodesInUse(DatabaseUser user, String paycodeTypeCode, long? employeeId);

        [OperationContract]
        CodeCollection GetPaycodesRemovingAssociationsInUse(DatabaseUser user, String paycodeAssociationTypeCode);

        [OperationContract]
        CodeCollection GetRecurringIncomePaycodesRemovingGlobalPaycodesInUse(DatabaseUser user, String paycodeTypeCode);

        [OperationContract]
        CodeCollection GetEmployeeCustomFieldNameCodesRemovingInUse(DatabaseUser user);

        [OperationContract]
        bool IsPaycodeGarnishment(DatabaseUser user, String paycode, String paycodeType);
        #endregion

        #region code paycode
        [OperationContract]
        CodePaycodeCollection GetCodePaycode(DatabaseUser user, String paycode, bool useExternalFlag);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        CodePaycode UpdateCodePaycode(DatabaseUser user, CodePaycode paycode, bool overrideConcurrencyCheckFlag);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        CodePaycode InsertCodePaycode(DatabaseUser user, CodePaycode paycode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteCodePaycode(DatabaseUser user, CodePaycode paycode);
        #endregion

        #region code paycode benefit
        [OperationContract]
        CodePaycodeBenefitCollection GetCodePaycodeBenefit(DatabaseUser user, String paycodeCode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        CodePaycodeBenefit UpdateCodePaycodeBenefit(DatabaseUser user, CodePaycodeBenefit paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        CodePaycodeBenefit InsertCodePaycodeBenefit(DatabaseUser user, CodePaycodeBenefit paycode);
        #endregion

        #region code paycode deuction
        [OperationContract]
        CodePaycodeDeductionCollection GetCodePaycodeDeduction(DatabaseUser user, String paycodeCode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        CodePaycodeDeduction UpdateCodePaycodeDeduction(DatabaseUser user, CodePaycodeDeduction paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        CodePaycodeDeduction InsertCodePaycodeDeduction(DatabaseUser user, CodePaycodeDeduction paycode);
        #endregion

        #region code paycode income
        [OperationContract]
        CodePaycodeIncomeCollection GetCodePaycodeIncome(DatabaseUser user, String paycodeCode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        CodePaycodeIncome UpdateCodePaycodeIncome(DatabaseUser user, CodePaycodeIncome paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        CodePaycodeIncome InsertCodePaycodeIncome(DatabaseUser user, CodePaycodeIncome paycode);
        #endregion

        #region code paycode employer
        [OperationContract]
        CodePaycodeEmployerCollection GetCodePaycodeEmployer(DatabaseUser user, String paycodeCode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        CodePaycodeEmployer UpdateCodePaycodeEmployer(DatabaseUser user, CodePaycodeEmployer paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        CodePaycodeEmployer InsertCodePaycodeEmployer(DatabaseUser user, CodePaycodeEmployer paycode);
        #endregion

        #region paycode attached paycode provision
        [OperationContract]
        PaycodeAttachedPaycodeProvisionCollection GetPaycodeAttachedPaycodeProvision(DatabaseUser user, String paycodeCode);
        #endregion

        #region paycode export
        [OperationContract]
        byte[] ExportPaycodeFile(DatabaseUser user, String paycodeCode, String paycodeType);
        #endregion

        #region code payroll processing group
        [OperationContract]
        Decimal GetCurrentPayrollYear(DatabaseUser user);

        [OperationContract]
        PayrollProcessingGroupCollection GetPayrollProcessingGroup(DatabaseUser user);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        PayrollProcessingGroup UpdatePayrollProcessingGroup(DatabaseUser user, PayrollProcessingGroup ppG);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeletePayrollProcessingGroup(DatabaseUser user, PayrollProcessingGroup ppG);

        [OperationContract]
        PayrollProcessingGroup InsertPayrollProcessingGroupCode(DatabaseUser user, PayrollProcessingGroup ppG);
        #endregion

        #region payroll process group override period
        [OperationContract]
        PayrollProcessGroupOverridePeriodCollection GetPayrollProcessGroupOverridePeriod(DatabaseUser user, String payrollProcessGroupCode);

        [OperationContract]
        PayrollProcessGroupOverridePeriod InsertPayrollProcessGroupOverridePeriod(DatabaseUser user, PayrollProcessGroupOverridePeriod overridePeriod);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        PayrollProcessGroupOverridePeriod UpdatePayrollProcessGroupOverridePeriod(DatabaseUser user, PayrollProcessGroupOverridePeriod overridePeriod);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeletePayrollProcessGroupOverridePeriod(DatabaseUser user, PayrollProcessGroupOverridePeriod overridePeriod);
        #endregion

        #region code system
        [OperationContract]
        CodeSystemCollection GetCodeSystem(DatabaseUser user);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        CodeSystem UpdateCodeSystem(DatabaseUser user, CodeSystem code);

        [OperationContract]
        CodeSystem InsertCodeSystem(DatabaseUser user, CodeSystem code);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteCodeSystem(DatabaseUser user, CodeSystem code);
        #endregion

        #region paycode association
        [OperationContract]
        PaycodeAssociationTypeCollection GetPaycodeAssociationType(DatabaseUser user, String paycodeAssociationTypeCode);

        [OperationContract]
        PaycodeAssociationCollection GetPaycodeAssociation(DatabaseUser user, String paycodeAssociationTypeCode);

        [OperationContract]
        PaycodeAssociationType InsertPaycodeAssociationType(DatabaseUser user, PaycodeAssociationType paycode);

        [OperationContract]
        PaycodeAssociation InsertPaycodeAssociation(DatabaseUser user, PaycodeAssociation paycode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        PaycodeAssociationType UpdatePaycodeAssociationType(DatabaseUser user, PaycodeAssociationType paycode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        PaycodeAssociation UpdatePaycodeAssociation(DatabaseUser user, PaycodeAssociation paycode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeletePaycodeAssociationType(DatabaseUser user, PaycodeAssociationType paycode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeletePaycodeAssociation(DatabaseUser user, PaycodeAssociation paycode);
        #endregion

        #region third party
        [OperationContract]
        CodeThirdParty InsertThirdPartyCode(DatabaseUser user, CodeThirdParty thirdParty);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        CodeThirdParty UpdateThirdPartyCode(DatabaseUser user, CodeThirdParty thirdParty);

        [OperationContract]
        CodeThirdPartyCollection GetCodeThirdParty(DatabaseUser user, string thirdPartyCode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteThirdPartyCode(DatabaseUser user, CodeThirdParty thirdParty);
        #endregion

        #region code wsib
        [OperationContract]
        CodeWsibCollection GetCodeWsib(DatabaseUser user, String wsibCode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        CodeWsib UpdateCodeWsib(DatabaseUser user, CodeWsib codeWsib);

        [OperationContract]
        CodeWsib InsertCodeWsib(DatabaseUser user, CodeWsib codeWsib);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteCodeWsib(DatabaseUser user, CodeWsib codeWsib);

        [OperationContract]
        CodeWsibEffectiveCollection GetCodeWsibEffective(DatabaseUser user, String wsibCode);

        [OperationContract]
        CodeWsibEffective InsertCodeWsibEffective(DatabaseUser user, CodeWsibEffective codeWsibEffective);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        CodeWsibEffective UpdateCodeWsibEffective(DatabaseUser user, CodeWsibEffective codeWsibEffective);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteCodeWsibEffective(DatabaseUser user, CodeWsibEffective codeWsibEffective);
        #endregion

        #region code wcb cheque remittance frequency
        [OperationContract]
        CodeWcbChequeRemittanceFrequencyCollection GetCodeWcbChequeRemittanceFrequency(DatabaseUser user, String wcbChequeRemittanceFrequencyCode);

        [OperationContract]
        CodeWcbChequeRemittanceFrequency InsertCodeWcbChequeRemittanceFrequency(DatabaseUser user, CodeWcbChequeRemittanceFrequency codeWcbChequeRemittanceFrequency);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        CodeWcbChequeRemittanceFrequency UpdateCodeWcbChequeRemittanceFrequency(DatabaseUser user, CodeWcbChequeRemittanceFrequency codeWcbChequeRemittanceFrequency);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteCodeWcbChequeRemittanceFrequency(DatabaseUser user, CodeWcbChequeRemittanceFrequency codeWcbChequeRemittanceFrequency);

        [OperationContract]
        WcbChequeRemittanceFrequencyDetailCollection GetWcbChequeRemittanceFrequencyDetail(DatabaseUser user, String wcbChequeRemittanceFrequencyCode);

        [OperationContract]
        WcbChequeRemittanceFrequencyDetail InsertWcbChequeRemittanceFrequencyDetail(DatabaseUser user, WcbChequeRemittanceFrequencyDetail wcbChequeRemittanceFrequencyDetail);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        WcbChequeRemittanceFrequencyDetail UpdateWcbChequeRemittanceFrequencyDetail(DatabaseUser user, WcbChequeRemittanceFrequencyDetail wcbChequeRemittanceFrequencyDetail);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteWcbChequeRemittanceFrequencyDetail(DatabaseUser user, WcbChequeRemittanceFrequencyDetail wcbChequeRemittanceFrequencyDetail);
        #endregion

        #region security
        [OperationContract]
        CodeCollection GetRoleDescription(DatabaseUser user, String securityRoleType, bool useSystemRoleId, bool? worklinksAdministrationFlag);

        [OperationContract]
        CodeCollection GetRoleDescriptionCombo(DatabaseUser user, String securityRoleType, String selectedValue, bool useSystemRoleId, bool? worklinksAdministrationFlag);
        #endregion

        [OperationContract]
        CodeCollection GetBusinessNumber(DatabaseUser user, long? id);

        [OperationContract]
        CodeCollection GetDisciplineTypeCode(DatabaseUser user, string disciplineCode);

        [OperationContract]
        CodeCollection GetProvinceStateCode(DatabaseUser user, String country);

        [OperationContract]
        CodeCollection GetDayCode(DatabaseUser user, String monthCode);

        [OperationContract]
        CodeCollection GetYearEndFormBoxCode(DatabaseUser user, String yearEndFormCode);

        [OperationContract]
        CodeCollection GetUnionCollectiveAgreementCode(DatabaseUser user, String unionCode);

        [OperationContract]
        CodeCollection GetEmployeeBankCode(DatabaseUser user, String bankingCountryCode);

        #region revenue quebec business 
        [OperationContract]
        RevenueQuebecBusiness GetRevenueQuebecBusiness(DatabaseUser user);

        [OperationContract]
        bool ValidateModulus11CheckDigit(string businessId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        RevenueQuebecBusiness UpdateRevenueQuebecBusiness(DatabaseUser user, RevenueQuebecBusiness revenueQuebecBusiness);
        #endregion
    }
}