﻿using System;
using System.ServiceModel;

namespace WorkLinks.Web.BusinessService.Contract
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IApplicationResourceService" in both code and config file together.
    [ServiceContract]
    public interface IApplicationResource
    {
        [OperationContract]
        String GetResourceByLanguageAndKey(String databaseName, long securityRoleId, String languageCode, String virtualPath, String resourceKey);
    }
}