﻿using System;
using System.ServiceModel;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Web.BusinessService.Contract
{
    [ServiceContract]
    public interface ISecurity
    {
        [OperationContract]
        bool ValidateUser(string databaseName, string userName, string password);

        [OperationContract]
        bool DoesSecurityUserAlreadyExist(DatabaseUser user, long employeeId);

        [OperationContract]
        bool ValidateUserName(DatabaseUser user, string username);

        [OperationContract]
        bool ValidateUserWithHistory(string databaseName, SecurityUserLoginHistory history, string password, int passwordAttemptWindow, int maxInvalidPasswordAttempts, string loginExpiryDays);

        [OperationContract]
        bool ValidatePasswordHistory(DatabaseUser user, string username, string password);

        //[OperationContract]
        //[FaultContract(typeof(EmployeeServiceException))]
        //void UpdateLogoutUser(DatabaseUser user, string userName);

        [OperationContract]
        SecurityUserCollection GetSecurityUser(string databaseName, string userName);

        [OperationContract]
        SecurityUser InsertUser(DatabaseUser user, SecurityUser securityUser, string authDatabaseName, string ngSecurityClientId, string loginExpiryHours, string ngSecurity, string _ngDbSecurity, string _ngApiSecuritye);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void UpdateSecurityUser(DatabaseUser user, string username, string newPassword, SecurityUser securityUser, string passwordExpiryDays, string loginExpiryHours, string adminDatabaseName, bool onlyUpdateLocalDbForLockedOutFlag);

        [OperationContract]
        void UpdateSecurityUserPassword(DatabaseUser user, string username, string newPassword, SecurityUser securityUser, string passwordExpiryDays, string loginExpiryHours, string adminDatabaseName);

        #region Security Categories
        [OperationContract]
        SecurityCategoryCollection GetSecurityCategories(DatabaseUser user, bool isViewMode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void UpdateCategories(DatabaseUser user, SecurityCategoryCollection securityCategoryColl);

        [OperationContract]
        void RebuildSecurity(DatabaseUser user);
        #endregion

        #region user admin
        [OperationContract]
        UserSummaryCollection GetUserSummary(DatabaseUser user, UserAdminSearchCriteria criteria);

        [OperationContract]
        int SelectSecurityClientUser(DatabaseUser user, string authDatabaseName, long securityUserId);

        [OperationContract]
        SecurityUserCollection GetPersonUserDetails(DatabaseUser user, long personId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteUser(DatabaseUser user, long personId);
        #endregion

        #region security marking
        [OperationContract]
        SecurityMarkingCollection GetSecurityMarking(DatabaseUser user, int hierarchicalSortOrder);
        #endregion 

        #region security label role
        [OperationContract]
        SecurityLabelRoleCollection GetSecurityLabelRole(DatabaseUser user, long securityRoleId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        SecurityLabelRoleCollection UpdateSecurityLabelRole(DatabaseUser user, long roleId, SecurityLabelRoleCollection labels);
        #endregion

        #region Role Description
        [OperationContract]
        RoleDescriptionCollection GetRoleDescriptions(DatabaseUser user, string criteria, long? roleId, bool? worklinksAdministrationFlag);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        RoleDescription UpdateRoleDesc(DatabaseUser user, string authDatabaseName, string ngSecurityClientId, RoleDescription roleDesc);

        [OperationContract]
        RoleDescription InsertRoleDescription(DatabaseUser user, string authDatabaseName, string ngSecurityClientId, RoleDescription roleDesc);
        #endregion

        #region Group Description
        [OperationContract]
        GroupDescriptionCollection GetUserGroups(DatabaseUser user, long securityUserId, bool isViewMode, bool? worklinksAdministrationFlag); //used in UserAdmin to show a person's groups

        [OperationContract]
        GroupDescriptionCollection GetGroupDescriptions(DatabaseUser user, string criteria, long? groupId, bool? worklinksAdministrationFlag);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        GroupDescription UpdateGroupDesc(DatabaseUser user, string authDatabaseName, string ngSecurityClientId, GroupDescription grpDesc);

        [OperationContract]
        GroupDescription InsertGroupDescription(DatabaseUser user, string authDatabaseName, string ngSecurityClientId, GroupDescription grpDesc);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void UpdateUserGroups(DatabaseUser user, string authDatabaseName, string ngSecurityClientId, string userName, long securityUserId, GroupDescription[] groupArray);
        #endregion

        #region Form Security
        [OperationContract]
        FormSecurityCollection GetFormSecurityInfo(DatabaseUser user, long roleId, bool? worklinksAdministrationFlag);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void UpdateFormSecurity(DatabaseUser user, string authDatabaseName, string ngSecurityClientId, FormSecurity[] formsArray, long roleId);
        #endregion

        [OperationContract]
        string GetEmailByEmployeeId(DatabaseUser user, long employeeId);

        #region SecurityUserProfile
        [OperationContract]
        SecurityUserProfileCollection GetSecurityUserProfile(string databaseName, string userName);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        SecurityUserProfile UpdateSecurityUserProfile(DatabaseUser user, SecurityUserProfile item);
        #endregion
    }
}