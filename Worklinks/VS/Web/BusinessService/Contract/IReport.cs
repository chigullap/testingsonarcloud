﻿using System;
using System.ServiceModel;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Web.BusinessService.Contract
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IReport" in both code and config file together.
    [ServiceContract]
    public interface IReport
    {
        [OperationContract]
        byte[] GetReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String reportName, long payrollProcessId, String employeeNumber, String reportType, String year, String employerNumber, String provinceCode, String reportShowDescriptionField, String revisionNumber, String reportPayRegisterSortOrder, long employeeId);

        [OperationContract]
        byte[] GetRemittanceReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, string reportName, string detailType, string remitCode, string exportId, string reportType, string remitDate);

        [OperationContract]
        byte[] GetYearEndReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, YearEndReportsCriteria criteria);

        [OperationContract]
        byte[] GetHRMenuReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, HrReportsCriteria criteria);

        [OperationContract]
        byte[] GetReportForAllEmployeesOfThisPayProcess(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String massPayslipInternalFileFormat, long payrollProcessId, String reportShowDescriptionField, String reportPayRegisterSortOrder, int maxDegreeOfParallelism);

        [OperationContract]
        byte[] CreateGLExportFile(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, String glType, String reportShowDescriptionField, String reportPayRegisterSortOrder);

        [OperationContract]
        byte[] CreateMercerPensionExportFile(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, String reportShowDescriptionField, String reportPayRegisterSortOrder);

        [OperationContract]
        byte[] CreateStockExportFile(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId);

        [OperationContract]
        byte[] ExportCeridianPayProcess(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, long onEFTAmount, String companyNumber, String companyName, BusinessLayer.BusinessObjects.Export.CeridianBilling.ProcessType ceridianBillingProcessType, DateTime wcbStartDate);

        [OperationContract]
        byte[] GetReportForAllEmployeesOfThisYear(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String massPayslipInternalFileFormat, long year, String reportType, String reportShowDescriptionField, String reportPayRegisterSortOrder);

        [OperationContract]
        byte[] GetMassFileForAllReportsForYear(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String[] massPayslipInternalFileFormats, long year, String[] reportTypes, String reportShowDescriptionField, String reportPayRegisterSortOrder, String revision);

        #region CSB Export
        [OperationContract]
        CanadaRevenueAgencyBondExportCollection GetCanadaRevenueAgencyBondExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId);

        [OperationContract]
        CanadaRevenueAgencyBondExport InsertCanadaRevenueAgencySavingsBondExportData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CanadaRevenueAgencyBondExport export);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        CanadaRevenueAgencyBondExport UpdateCanadaRevenueAgencySavingsBondExportData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CanadaRevenueAgencyBondExport export);

        [OperationContract]
        byte[] CreateCSBExportFile(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId);
        #endregion

        #region sftp report and export section
        [OperationContract]
        void CreateReportsAndExports(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, String periodYear, PayRegisterExportCollection collection, string pexFtp, String reportShowDescriptionField, String reportPayRegisterSortOrder, String revenueQuebecTaxId, bool useQuebecTaxId, String citiEftFileName, String citiChequeFileName, String eftClientNumber, String citiChequeAccountNumber, String companyName, String companyShortName, String massPayslipInternalFileFormat, string originatorId, string eftType, int maxDegreeOfParallelism);
        #endregion

        #region rbc edi eft export section
        [OperationContract]
        void FtpRbcEdiExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, RbcEftEdiParameters ediParms, RbcEftSourceDeductionParameters garnishmentParms, string rbcFtpName);
        #endregion

        //#region report testing
        //[OperationContract]
        //BusinessLayer.BusinessObjects.Test.TestReportCollection GetTestReports(WLP.BusinessLayer.BusinessObjects.DatabaseUser user);

        //[OperationContract]
        //BusinessLayer.BusinessObjects.Test.TestReportParameterCollection GetTestReportParameters(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long testReportId);

        //[OperationContract]
        //BusinessLayer.BusinessObjects.Test.TestReportExpectedValueCollection GetTestReportExpectedValues(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long testReportId);
        //#endregion

        [OperationContract]
        ReportCollection GetReportRecord(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String reportName);

        #region report export
        [OperationContract]
        ReportExportCollection GetReportExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? exportId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        ReportExport UpdateReportExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ReportExport export);

        [OperationContract]
        ReportExport InsertReportExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ReportExport export);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteReportExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ReportExport export);
        #endregion
    }
}