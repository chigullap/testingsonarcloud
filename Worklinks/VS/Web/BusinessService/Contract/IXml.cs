﻿using System;
using System.ServiceModel;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Web.BusinessService.Contract
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IXmlService" in both code and config file together.
    [ServiceContract]
    public interface IXml
    {
        #region import/export
        [OperationContract]
        void ImportBatchesViaFtp(DatabaseUser user, string importName, string importExportProcessingDirectory, bool importDirtySave, bool autoAddSecondaryPositions, string realFtpName, string filePatternToMatch, string payrollProcessRunTypeCode, string payrollProcessGroupCode);

        [OperationContract]
        String ImportFile(DatabaseUser user, long importExportId, byte[] data, String importFileName, String importExportProcessingDirectory, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions);

        [OperationContract]
        byte[] ExportRoe(DatabaseUser user, long[] employeePositionIds, String importExportProcessingDirectory, String roeIssueCode, out bool missingRoeReason);
        #endregion

        #region t4 export
        [OperationContract]
        CanadaRevenueAgencyT4ExportCollection ExportT4Details(DatabaseUser user, String year, String exportType);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        CanadaRevenueAgencyT4Export UpdateExportT4Details(DatabaseUser user, CanadaRevenueAgencyT4Export export);

        [OperationContract]
        CanadaRevenueAgencyT4Export InsertExportT4Details(DatabaseUser user, CanadaRevenueAgencyT4Export export, String year);

        [OperationContract]
        byte[] ExportT4XmlFile(DatabaseUser user, int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string exportType);
        #endregion

        #region CIC T4/T4A/R1 export
        [OperationContract]
        byte[] ExportT4CIC(DatabaseUser user, int year, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2);

        [OperationContract]
        byte[] ExportT4aCIC(DatabaseUser user, int year, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2);

        [OperationContract]
        byte[] ExportR1CIC(DatabaseUser user, int year, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2, String revenueQuebecTaxId);
        #endregion

        #region t4a export
        [OperationContract]
        byte[] ExportT4aXmlFile(DatabaseUser user, int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string exportType);
        #endregion

        #region NR4 export
        [OperationContract]
        byte[] ExportNR4XmlFile(DatabaseUser user, int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string exportType);
        #endregion

        #region R1 export
        [OperationContract]
        byte[] ExportR1XmlFile(DatabaseUser user, int year, String importExportProcessingDirectory, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2, String revenueQuebecTaxId);

        [OperationContract]
        RevenueQuebecR1ExportCollection ExportR1Details(DatabaseUser user, String year, String exportType);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        RevenueQuebecR1Export UpdateExportR1Details(DatabaseUser user, RevenueQuebecR1Export export);

        [OperationContract]
        RevenueQuebecR1Export InsertExportR1Details(DatabaseUser user, RevenueQuebecR1Export export);
        #endregion

        #region R2 export
        [OperationContract]
        byte[] ExportR2XmlFile(DatabaseUser user, int year, String importExportProcessingDirectory, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2, String revenueQuebecTaxId);
        #endregion
    }
}