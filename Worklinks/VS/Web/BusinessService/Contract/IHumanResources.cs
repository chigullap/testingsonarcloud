﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Web.BusinessService.Contract
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IHumanResourcesReference" in both code and config file together.
    [ServiceContract]
    [ServiceKnownType(typeof(PayrollBatchReport))]
    [ServiceKnownType(typeof(EmployeePositionSummary))]
    [ServiceKnownType(typeof(PayrollTransactionSummary))]
    [ServiceKnownType(typeof(EmployeeRoeAmountDetailSummary))]
    public interface IHumanResources
    {
        #region employee biographical
        [OperationContract]
        String GetEmployeeNumber(DatabaseUser user, long employeeId);

        [OperationContract]
        EmployeeCollection GetEmployee(DatabaseUser user, EmployeeCriteria criteria);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        Employee UpdateEmployee(DatabaseUser user, Employee employee, List<ContactType> deletedItems);

        [OperationContract]
        Employee InsertEmployee(DatabaseUser user, Employee employee);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployee(DatabaseUser user, Employee employee);

        [OperationContract]
        bool CheckDatabaseForDuplicateSin(DatabaseUser user, string sin, long employeeId, string employeeNumber);

        [OperationContract]
        bool ValidateSIN(DatabaseUser user, String SIN, String governmentIdentificationNumberTypeCode);
        #endregion

        #region employee education
        [OperationContract]
        EmployeeEducationCollection GetEmployeeEducation(DatabaseUser user, long employeeId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeEducation UpdateEmployeeEducation(DatabaseUser user, EmployeeEducation education);

        [OperationContract]
        EmployeeEducation InsertEmployeeEducation(DatabaseUser user, EmployeeEducation education);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeEducation DeleteEmployeeEducation(DatabaseUser user, EmployeeEducation education);
        #endregion

        #region employee company property
        [OperationContract]
        EmployeeCompanyPropertyCollection GetEmployeeCompanyProperty(DatabaseUser user, long employeeId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeCompanyProperty UpdateEmployeeCompanyProperty(DatabaseUser user, EmployeeCompanyProperty property);

        [OperationContract]
        EmployeeCompanyProperty InsertEmployeeCompanyProperty(DatabaseUser user, EmployeeCompanyProperty property);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeCompanyProperty DeleteEmployeeCompanyProperty(DatabaseUser user, EmployeeCompanyProperty property);
        #endregion

        #region employee contact
        [OperationContract]
        ContactCollection GetContact(DatabaseUser user, long employeeId, long? contactId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        Contact UpdateContact(DatabaseUser user, Contact contact);

        [OperationContract]
        Contact InsertContact(DatabaseUser user, Contact contact);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteContact(DatabaseUser user, Contact contact);
        #endregion

        #region employee contact type
        [OperationContract]
        ContactTypeCollection GetContactType(DatabaseUser user, long employeeId, String contactTypeCode);

        [OperationContract]
        void UpdateContactType(DatabaseUser user, ContactTypeCollection contactTypes);

        [OperationContract]
        ContactType InsertContactType(DatabaseUser user, ContactType contactType);

        [OperationContract]
        void DeleteContactType(DatabaseUser user, ContactType contactType);
        #endregion

        #region employee skill
        [OperationContract]
        EmployeeSkillCollection GetEmployeeSkill(DatabaseUser user, long employeeId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeSkill UpdateEmployeeSkill(DatabaseUser user, EmployeeSkill skill);

        [OperationContract]
        EmployeeSkill InsertEmployeeSkill(DatabaseUser user, EmployeeSkill skill);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeeSkill(DatabaseUser user, EmployeeSkill skill);
        #endregion

        #region employee course
        [OperationContract]
        EmployeeCourseCollection GetEmployeeCourse(DatabaseUser user, long employeeId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeCourse UpdateEmployeeCourse(DatabaseUser user, EmployeeCourse course);

        [OperationContract]
        EmployeeCourse InsertEmployeeCourse(DatabaseUser user, EmployeeCourse course);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeeCourse(DatabaseUser user, EmployeeCourse course);
        #endregion

        #region employee membership
        [OperationContract]
        EmployeeMembershipCollection GetEmployeeMembership(DatabaseUser user, long employeeId);

        [OperationContract]
        EmployeeMembership InsertEmployeeMembership(DatabaseUser user, EmployeeMembership membership);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeMembership UpdateEmployeeMembership(DatabaseUser user, EmployeeMembership membership);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeeMembership(DatabaseUser user, EmployeeMembership membership);
        #endregion

        #region Pension Export
        [OperationContract]
        byte[] CreatePensionExportFile(DatabaseUser user, long payrollProcessId);
        #endregion

        #region HSBC EFT
        [OperationContract]
        byte[] CreateHSBCEft(DatabaseUser user, long payrollProcessId, string employeeNumber, string eftType);
        #endregion

        #region CITI EFT
        [OperationContract]
        byte[] CreateCITIeft(DatabaseUser user, long payrollProcessId, String eftClientNumber, String employeeNumber, string eftType);

        [OperationContract]
        byte[] CreateCitiChequeFile(DatabaseUser user, long payrollProcessId, String eftClientNumber, String companyName, String employeeNumber, string originatorId, string eftType);

        [OperationContract]
        byte[] CreateCitiAchFile(DatabaseUser user, long payrollProcessId, String eftClientNumber, String companyName, String companyShortName, String employeeNumber, string eftType);

        [OperationContract]
        byte[] CreateCitiEftAndChequeFile(DatabaseUser user, long payrollProcessId, String citiEftFileName, String citiChequeFileName, String eftClientNumber, String citiChequeAccountNumber, String companyName, String companyShortName, String employeeNumber, string originatorId, string eftType);

        [OperationContract]
        String GetCitiNamingFormat(DatabaseUser user, String eftFileNameFormat, long payrollprocessid);
        #endregion

        #region RRSP Export
        [OperationContract]
        byte[] CreateRRSPExport(DatabaseUser user, long payrollProcessId, String rrspHeader, String rrspGroupNumber);
        #endregion

        #region CIC Plus Weekly Export
        [OperationContract]
        byte[] CreateCicPlusWeeklyExportFile(DatabaseUser user, decimal periodYear);
        #endregion

        #region Empower Remittance Export and Garnishment Export
        [OperationContract]
        byte[] CreateEmpowerRemittanceExport(DatabaseUser user, long payrollProcessId, String revenueQuebecTaxId, bool useQuebecTaxId);

        [OperationContract]
        byte[] CreateGarnishmentExportFile(DatabaseUser user, long payrollProcessId, String empowerFein);
        #endregion

        #region REAL Custom Export Data
        [OperationContract]
        byte[] CreateRealCustomExport(DatabaseUser user, long payrollProcessId);

        #endregion

        #region social costs export

        [OperationContract]
        byte[] CreateSocialCostsExport(DatabaseUser user, long payrollProcessId);

        #endregion

        #region RBC EFT EDI
        [OperationContract]
        byte[] GenerateRBCEftEdiFile(DatabaseUser user, long payrollProcessId, RbcEftEdiParameters ediParms, string employeeNumber);
        #endregion

        #region SCOTIABANK EDI
        [OperationContract]
        byte[] GenerateScotiaBankEdiFile(DatabaseUser user, long payrollProcessId, ScotiaBankEdiParameters ediParms, string employeeNumber);
        #endregion

        #region CRA GARNISHMENT EDI
        [OperationContract]
        byte[] CreateCraEftGarnishmentFile(DatabaseUser user, long payrollProcessId, RbcEftSourceDeductionParameters ediParms, string employeeNumber);
        #endregion

        #region DynamicsXmlToWorklinks
        [OperationContract]
        byte[] GenerateEftFile(DatabaseUser user, long payrollProcessId, String eftClientNumber, bool eftTransmissionFlag, String eftFileIndicator, String companyName, String companyShortName, String employeeNumber, string eftType);
        #endregion

        #region employee banking
        [OperationContract]
        EmployeeBankingCollection GetEmployeeBanking(DatabaseUser user, long employeeId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeBanking UpdateEmployeeBanking(DatabaseUser user, EmployeeBanking bank);

        [OperationContract]
        EmployeeBanking InsertEmployeeBanking(DatabaseUser user, EmployeeBanking bank);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeeBanking(DatabaseUser user, EmployeeBanking bank);
        #endregion

        #region person
        [OperationContract]
        PersonCollection GetPerson(DatabaseUser user, long personId);

        [OperationContract]
        void UpdatePerson(DatabaseUser user, Person person);

        [OperationContract]
        Person UpdateUnionPerson(DatabaseUser user, Person person);

        [OperationContract]
        Person InsertPerson(DatabaseUser user, Person person);

        [OperationContract]
        void DeletePerson(DatabaseUser user, Person person);
        #endregion

        #region person address
        [OperationContract]
        PersonAddressCollection GetPersonAddress(DatabaseUser user, long personId);

        [OperationContract]
        AddressCollection GetAddress(DatabaseUser user, long addressId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        PersonAddress UpdatePersonAddress(DatabaseUser user, PersonAddress address, bool handlePreviousAddress, List<PersonAddress> theData);

        [OperationContract]
        PersonAddress InsertPersonAddress(DatabaseUser user, PersonAddress address, List<PersonAddress> theData);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeletePersonAddress(DatabaseUser user, PersonAddress address);
        #endregion

        #region person ContactChannel
        [OperationContract]
        PersonContactChannelCollection GetPersonContactChannel(DatabaseUser user, long personId, String contactChannelCategoryCode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        PersonContactChannel UpdatePersonContactChannel(DatabaseUser user, PersonContactChannel contact, List<PersonContactChannel> theData);

        [OperationContract]
        PersonContactChannel InsertPersonContactChannel(DatabaseUser user, PersonContactChannel contact, List<PersonContactChannel> theData);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeletePersonContactChannel(DatabaseUser user, PersonContactChannel contact);
        #endregion

        #region ContactChannel
        [OperationContract]
        ContactChannelCollection GetContactChannel(DatabaseUser user, long contactChannelId);
        #endregion

        #region government identification number type template
        [OperationContract]
        GovernmentIdentificationNumberTypeTemplateCollection GetGovernmentIdentificationNumberTypeTemplate(DatabaseUser user, String numberTypeCode);
        #endregion

        #region grievance
        [OperationContract]
        GrievanceCollection GetGrievance(DatabaseUser user, long id);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        Grievance UpdateGrievance(DatabaseUser user, Grievance data);

        [OperationContract]
        Grievance InsertGrievance(DatabaseUser user, Grievance data);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteGrievance(DatabaseUser user, Grievance data);
        #endregion

        #region grievance action
        [OperationContract]
        GrievanceActionCollection GetGrievanceAction(DatabaseUser user, long id);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        GrievanceAction UpdateGrievanceAction(DatabaseUser user, GrievanceAction data);

        [OperationContract]
        GrievanceAction InsertGrievanceAction(DatabaseUser user, GrievanceAction data);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteGrievanceAction(DatabaseUser user, GrievanceAction data);
        #endregion

        #region Grievance report
        [OperationContract]
        GrievanceCollection GetGrievanceSummary(DatabaseUser user, GrievanceCriteria criteria);

        //[OperationContract]
        //Grievance UpdateGrievance(DatabaseUser user, Grievance data);

        //[OperationContract]
        //Grievance InsertGrievance(DatabaseUser user, Grievance data);

        //[OperationContract]
        //void DeleteGrievance(DatabaseUser user, Grievance data);

        [OperationContract]
        EmployeeSummaryCollection GetEmployeeSummary(DatabaseUser user, EmployeeCriteria criteria);
        #endregion

        #region labour union
        [OperationContract]
        LabourUnionSummaryCollection GetLabourUnion(DatabaseUser user, long? labourUnionId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        LabourUnionSummary UpdateUnionContactData(DatabaseUser user, LabourUnionSummary union);

        [OperationContract]
        LabourUnion UpdateUnionContact(DatabaseUser user, LabourUnion union);

        [OperationContract]
        LabourUnionSummary InsertUnionContactInformationSummaryData(DatabaseUser user, LabourUnionSummary union);

        [OperationContract]
        LabourUnion InsertUnionContact(DatabaseUser user, LabourUnion union);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteUnionContact(DatabaseUser user, LabourUnion union);
        #endregion

        #region Union Collective Agreement
        [OperationContract]
        UnionCollectiveAgreementCollection GetUnionCollectiveAgreement(DatabaseUser user, long? labourUnionId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        UnionCollectiveAgreement UpdateUnionCollectiveAgreement(DatabaseUser user, UnionCollectiveAgreement collectiveAgreement);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        UnionCollectiveAgreement InsertUnionCollectiveAgreement(DatabaseUser user, UnionCollectiveAgreement collectiveAgreement);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteUnionCollectiveAgreement(DatabaseUser user, UnionCollectiveAgreement collectiveAgreement);
        #endregion

        #region employee license certificate
        [OperationContract]
        EmployeeLicenseCertificateCollection GetEmployeeLicenseCertificate(DatabaseUser user, long employeeId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeLicenseCertificate UpdateEmployeeLicenseCertificate(DatabaseUser user, EmployeeLicenseCertificate licenseCertificate);

        [OperationContract]
        EmployeeLicenseCertificate InsertEmployeeLicenseCertificate(DatabaseUser user, EmployeeLicenseCertificate licenseCertificate);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeeLicenseCertificate(DatabaseUser user, EmployeeLicenseCertificate licenseCertificate);
        #endregion

        #region employee paycode
        [OperationContract]
        bool CheckRecurringIncomeCode(DatabaseUser user);

        [OperationContract]
        EmployeePaycodeCollection GetEmployeePaycode(DatabaseUser user, long employeeId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeePaycode UpdateEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode);

        [OperationContract]
        EmployeePaycode InsertEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode);

        [OperationContract]
        Decimal CalculateRate(DatabaseUser user, long employeeId, Decimal baseRate, Decimal rateFactor, bool vacationCalculationOverrideFlag);

        [OperationContract]
        bool ValidatePaycodeEntryData(DatabaseUser user, EmployeePaycode employeePaycode);
        #endregion

        #region employee award honour
        [OperationContract]
        EmployeeAwardHonourCollection GetEmployeeAwardHonour(DatabaseUser user, long employeeId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeAwardHonour UpdateEmployeeAwardHonour(DatabaseUser user, EmployeeAwardHonour awardHonour);

        [OperationContract]
        EmployeeAwardHonour InsertEmployeeAwardHonour(DatabaseUser user, EmployeeAwardHonour awardHonour);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeeAwardHonour(DatabaseUser user, EmployeeAwardHonour awardHonour);
        #endregion

        #region employee discipline
        [OperationContract]
        EmployeeDisciplineCollection GetEmployeeDiscipline(DatabaseUser user, long employeeId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeDiscipline UpdateEmployeeDiscipline(DatabaseUser user, EmployeeDiscipline discipline);

        [OperationContract]
        EmployeeDiscipline InsertEmployeeDiscipline(DatabaseUser user, EmployeeDiscipline discipline);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeeDiscipline(DatabaseUser user, EmployeeDiscipline discipline);
        #endregion

        #region employee discipline action
        [OperationContract]
        EmployeeDisciplineActionCollection GetEmployeeDisciplineAction(DatabaseUser user, long employeeDisciplineId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeDisciplineAction UpdateEmployeeDisciplineAction(DatabaseUser user, EmployeeDisciplineAction disciplineAction);

        [OperationContract]
        EmployeeDisciplineAction InsertEmployeeDisciplineAction(DatabaseUser user, EmployeeDisciplineAction disciplineAction);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeeDisciplineAction(DatabaseUser user, EmployeeDisciplineAction disciplineAction);
        #endregion

        #region Organization Unit Management

        #region OrganizationUnit
        [OperationContract]
        OrganizationUnitCollection GetOrganizationUnitByOrganizationUnitLevelId(DatabaseUser user, OrganizationUnitCriteria criteria);

        [OperationContract]
        OrganizationUnitCollection GetOrganizationUnitWithLevelDescription(DatabaseUser user, long? organizationUnitId);

        [OperationContract]
        OrganizationUnitAssociationCollection GetOrganizationUnitAssociation(DatabaseUser user, OrganizationUnitCriteria criteria);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        OrganizationUnitAssociationCollection InsertOrganizationUnitAssociation(DatabaseUser user, OrganizationUnitAssociation item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteOrganizationUnitAssociation(DatabaseUser user, OrganizationUnitAssociation item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        OrganizationUnit UpdateOrganizationUnit(DatabaseUser user, OrganizationUnit item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteOrganizationUnit(DatabaseUser user, OrganizationUnit item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        OrganizationUnit InsertOrganizationUnit(DatabaseUser user, OrganizationUnit item);
        #endregion

        #region OrganizationUnitAssocaition
        [OperationContract]
        OrganizationUnitAssociationTreeCollection GetOrganizationUnitAssociationTree(DatabaseUser user, String codeLanguageCd, bool? checkActiveFlag);

        [OperationContract]
        OrganizationUnitCollection GetOrganizationUnit(DatabaseUser user, long? organizationUnitId);

        [OperationContract]
        OrganizationUnitCollection GetOrganizationUnitByParentOrganizationUnitId(DatabaseUser user, long? parentOrganizationUnitId);
        #endregion

        #region OrganizationUnitLevel
        [OperationContract]
        OrganizationUnitLevelDescriptionCollection GetOrganizationUnitLevelDescription(DatabaseUser user, long organizationUnitLevelId);

        [OperationContract]
        OrganizationUnitLevelCollection GetOrganizationUnitLevel(DatabaseUser user);

        [OperationContract]
        OrganizationUnitLevel InsertOrganizationUnitLevel(DatabaseUser user, OrganizationUnitLevel item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        OrganizationUnitLevel UpdateOrganizationUnitLevel(DatabaseUser user, OrganizationUnitLevel item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteOrganizationUnitLevel(DatabaseUser user, OrganizationUnitLevel item);
        #endregion

        [OperationContract]
        bool CheckEmployeeExist(DatabaseUser user);
        #endregion

        #region employee termination

        #region termination other
        [OperationContract]
        EmployeeTerminationOtherCollection GetEmployeeTerminationOther(DatabaseUser user, long employeePositionId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeTerminationOther UpdateEmployeeTerminationOther(DatabaseUser user, EmployeeTerminationOther item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeeTerminationOther(DatabaseUser user, EmployeeTerminationOther item);

        [OperationContract]
        EmployeeTerminationOther InsertEmployeeTerminationOther(DatabaseUser user, EmployeeTerminationOther item);
        #endregion

        #region termination roe
        [OperationContract]
        EmployeeTerminationRoeCollection SelectRoe(DatabaseUser user, long employeePositionId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeTerminationRoe UpdateRoe(DatabaseUser user, EmployeeTerminationRoe item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteRoe(DatabaseUser user, EmployeeTerminationRoe item);

        [OperationContract]
        EmployeeTerminationRoe InsertRoe(DatabaseUser user, EmployeeTerminationRoe item);
        #endregion

        #endregion

        #region employee position
        [OperationContract]
        EmployeePositionSummaryCollection GetEmployeePositionSummary(DatabaseUser user, EmployeePositionCriteria criteria);

        [OperationContract]
        EmployeePositionCollection GetEmployeePosition(DatabaseUser user, EmployeePositionCriteria criteria);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeePosition UpdateEmployeePosition(DatabaseUser user, EmployeePosition item, bool autoAddSecondaryPositions);

        [OperationContract]
        bool IsProcessDateOpen(DatabaseUser user, long? payrollProcessId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeePosition(DatabaseUser user, EmployeePosition position, EmployeePosition formerPosition);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeeTermination(DatabaseUser user, EmployeePosition employeePosition);

        [OperationContract]
        EmployeePosition InsertEmployeePosition(DatabaseUser user, EmployeePosition position, EmployeePosition formerPosition, DateTime? payrollPeriodCutoffDate, String payrollProcessGroupCode, String roeDefaultContactLastName, String roeDefaultContactFirstName, String roeDefaultContactTelephone, String roeDefaultContactTelephoneExt, bool autoAddSecondaryPositions);

        [OperationContract]
        EmployeePosition CreateEmployeePosition(DatabaseUser user);
        #endregion

        #region employee position workday
        [OperationContract]
        EmployeePositionWorkdayCollection GetEmployeePositionWorkday(DatabaseUser user, long employeePositionId);

        [OperationContract]
        void InsertEmployeePositionWorkday(DatabaseUser user, EmployeePositionWorkday workday);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void UpdateEmployeePositionWorkday(DatabaseUser user, EmployeePositionWorkday workday);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeePositionWorkday(DatabaseUser user, EmployeePositionWorkday workday);
        #endregion

        #region employee position labour distribution
        [OperationContract]
        EmployeePositionLabourDistributionSummaryCollection GetEmployeePositionLabourDistribution(DatabaseUser user, long employeePositionId, String codeLanguageCd);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeePositionLabourDistributionSummary UpdateEmployeePositionLabourDistribution(DatabaseUser user, EmployeePositionLabourDistributionSummary summary);

        [OperationContract]
        EmployeePositionLabourDistributionSummary InsertEmployeePositionLabourDistribution(DatabaseUser user, EmployeePositionLabourDistributionSummary summary);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeePositionLabourDistribution(DatabaseUser user, EmployeePositionLabourDistributionSummary summary);
        #endregion

        #region employee position organization unit level report
        [OperationContract]
        EmployeePositionOrganizationUnitCollection GetEmployeePositionOrganizationUnitLevelSummary(DatabaseUser user, long employeePositionId);
        #endregion

        #region employee position secondary
        [OperationContract]
        EmployeePositionSecondaryCollection GetEmployeePositionSecondary(DatabaseUser user, long? employeePositionSecondaryId, long? employeePositionId, String languageCode, string organizationUnit);

        [OperationContract]
        EmployeePositionSecondary InsertEmployeePositionSecondary(DatabaseUser user, EmployeePositionSecondary item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeePositionSecondary UpdateEmployeePositionSecondary(DatabaseUser user, EmployeePositionSecondary item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeePositionSecondary(DatabaseUser user, EmployeePositionSecondary item);
        #endregion

        #region employee position secondary organization unit
        [OperationContract]
        EmployeePositionSecondaryOrganizationUnitCollection GetEmployeePositionSecondaryOrganizationUnit(DatabaseUser user, long? employeePositionSecondaryId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeePositionSecondaryOrganizationUnit(DatabaseUser user, EmployeePositionSecondaryOrganizationUnit item);
        #endregion

        #region employee wsib health and safety report
        [OperationContract]
        EmployeeWsibHealthAndSafetyReportCollection GetEmployeeWsibHealthAndSafetyReport(DatabaseUser user, long employeeId);

        [OperationContract]
        EmployeeWsibHealthAndSafetyReportPersonCollection GetEmployeeWsibHealthAndSafetyReportPerson(DatabaseUser user, long employeeId);

        [OperationContract]
        EmployeeWsibHealthAndSafetyReport InsertEmployeeWsibHealthAndSafetyReport(DatabaseUser user, EmployeeWsibHealthAndSafetyReport item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeWsibHealthAndSafetyReport UpdateEmployeeWsibHealthAndSafetyReport(DatabaseUser user, EmployeeWsibHealthAndSafetyReport item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeeWsibHealthAndSafetyReport(DatabaseUser user, EmployeeWsibHealthAndSafetyReport item);
        #endregion

        #region statutory deduction
        [OperationContract]
        StatutoryDeductionCollection GetStatutoryDeduction(DatabaseUser user, long id);

        [OperationContract]
        StatutoryDeduction InsertStatutoryDeduction(DatabaseUser user, StatutoryDeduction item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        StatutoryDeduction UpdateStatutoryDeduction(DatabaseUser user, StatutoryDeduction item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteStatutoryDeduction(DatabaseUser user, StatutoryDeduction item);
        #endregion

        #region statutory deduction barbados
        [OperationContract]
        StatutoryDeduction InsertStatutoryDeductionBarbados(DatabaseUser user, StatutoryDeduction item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        StatutoryDeduction UpdateStatutoryDeductionBarbados(DatabaseUser user, StatutoryDeduction item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteStatutoryDeductionBarbados(DatabaseUser user, StatutoryDeduction item);
        #endregion

        #region statutory deduction saint lucia
        [OperationContract]
        StatutoryDeduction InsertStatutoryDeductionSaintLucia(DatabaseUser user, StatutoryDeduction item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        StatutoryDeduction UpdateStatutoryDeductionSaintLucia(DatabaseUser user, StatutoryDeduction item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteStatutoryDeductionSaintLucia(DatabaseUser user, StatutoryDeduction item);
        #endregion

        #region statutory deduction trinidad
        [OperationContract]
        StatutoryDeduction InsertStatutoryDeductionTrinidad(DatabaseUser user, StatutoryDeduction item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        StatutoryDeduction UpdateStatutoryDeductionTrinidad(DatabaseUser user, StatutoryDeduction item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteStatutoryDeductionTrinidad(DatabaseUser user, StatutoryDeduction item);
        #endregion

        #region statutory deduction jamaica
        [OperationContract]
        StatutoryDeduction InsertStatutoryDeductionJamaica(DatabaseUser user, StatutoryDeduction item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        StatutoryDeduction UpdateStatutoryDeductionJamaica(DatabaseUser user, StatutoryDeduction item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteStatutoryDeductionJamaica(DatabaseUser user, StatutoryDeduction item);
        #endregion

        #region statutory deduction default
        [OperationContract]
        StatutoryDeductionDefaultCollection GetStatutoryDeductionDefaults(DatabaseUser user, String codeProvinceStateCd);
        #endregion

        #region EmployeeEmploymentInformation
        [OperationContract]
        EmployeeEmploymentInformationCollection GetEmployeeEmploymentInformation(DatabaseUser user, long id);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeEmploymentInformation UpdateEmployeeEmploymentInformation(DatabaseUser user, EmployeeEmploymentInformation item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeeEmploymentInformation(DatabaseUser user, EmployeeEmploymentInformation item);

        [OperationContract]
        EmployeeEmploymentInformation InsertEmployeeEmploymentInformation(DatabaseUser user, EmployeeEmploymentInformation item);
        #endregion

        #region Payroll Period
        [OperationContract]
        PayrollPeriodCollection GetPayrollPeriodById(DatabaseUser user, long payrollPeriodId);

        [OperationContract]
        PayrollPeriodCollection GetPayrollPeriod(DatabaseUser user, PayrollPeriodCriteria criteria);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        PayrollPeriod UpdatePayrollPeriod(DatabaseUser user, PayrollPeriod item);

        [OperationContract]
        void DeletePayrollPeriod(DatabaseUser user, PayrollPeriod item);

        [OperationContract]
        PayrollPeriod InsertPayrollPeriod(DatabaseUser user, PayrollPeriod item);
        #endregion

        #region Payroll Process
        [OperationContract]
        PayrollProcessSummaryCollection GetPayrollProcessSummary(DatabaseUser user, PayrollProcessCriteria criteria);

        [OperationContract]
        PayrollProcessCollection GetPayrollProcess(DatabaseUser user, PayrollProcessCriteria criteria);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        [FaultContract(typeof(PayrollValidationFault))]
        PayrollProcess UpdatePayrollProcess(DatabaseUser user, PayrollProcess item, PayrollProcess.CalculationProcess processType, String version, bool autoGenSalaryEmployee, bool calculateBmsPension, bool lockUnlockButtonPressed, bool prorationEnabledFlag, byte[] uploadedFile, String uploadedFileName, bool autoAddSecondaryPositions, bool newArrearsProcessingFlag);
        #endregion

        #region Pending ROE
        [OperationContract]
        RoeCreationSearchResultsCollection GetPendingRoeEmployeeSummary(DatabaseUser user, String criteria);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void SetPendingRoeStatusForEmployeeId(DatabaseUser user, long selectedEmployeeId);

        [OperationContract]
        EmployeeRoeAmountCollection GetRoeData(DatabaseUser user, long employeePositionId, long employeeId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeRoeAmount UpdateEmployeeRoeAmount(DatabaseUser user, EmployeeRoeAmount roeObj);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void UpdateEmployeeRoeDetail(DatabaseUser user, EmployeeRoeAmountDetail[] empRoeArray);
        #endregion

        #region dynamics process batch
        [OperationContract]
        PayrollProcess PostTransaction(DatabaseUser user, PayrollProcess process, String status, RbcEftEdiParameters ediParms, ScotiaBankEdiParameters scotiaEdiParms, SendEmailParameters emailParms, RbcEftSourceDeductionParameters garnishmentParms, bool craRemitFlag, bool rqRemitFlag, bool sendInvoice, bool garnishmentRemitFlag, bool wcbChequeRemitFlag, bool healthTaxChequeRemitFlag, bool autoAddSecondaryPositions);
        #endregion

        #region Payroll Batch Report and Payroll Batch Transaction

        #region batch
        [OperationContract]
        PayrollBatchReportCollection GetPayrollBatchReport(DatabaseUser user, PayrollBatchCriteria criteria);

        [OperationContract]
        PayrollBatchCollection GetPayrollBatch(DatabaseUser user, long payrollBatchId);

        [OperationContract]
        PayrollBatch InsertPayrollBatch(DatabaseUser user, PayrollBatch PayrollBatch);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void UpdatePayrollBatch(DatabaseUser user, PayrollBatch item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeletePayrollBatch(DatabaseUser user, PayrollBatchReport PayrollBatch);

        [OperationContract]
        PayrollPeriodCollection GetPayrollPeriodIdFromPayrollProcessGroupCode(DatabaseUser user, PayrollBatchCriteria payrollBatchCriteria);
        #endregion

        #region transaction
        [OperationContract]
        PayrollTransactionSummaryCollection GetPayrollTransactionSummary(DatabaseUser user, long payrollBatchAdjustmentOtherAmountId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        PayrollTransaction UpdatePayrollTransaction(DatabaseUser user, PayrollTransaction payrollTransaction);

        [OperationContract]
        PayrollTransaction InsertPayrollTransaction(DatabaseUser user, PayrollTransaction payrollTransaction);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeletePayrollTransaction(DatabaseUser user, PayrollTransaction payrollTransaction);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeletePayrollBatchCascadeTransactions(DatabaseUser user, PayrollBatch payrollBatch);
        #endregion

        #endregion

        //removed by McKayJ for now 
        //#region Payroll Batch Transaction Full Week
        //[OperationContract]
        //PayrollTransactionWeekCollection GetPayrollTransactionWeek(DatabaseUser user, PayrollBatch batch);

        //[OperationContract]
        //PayrollTransactionWeek UpdatePayrollTransactionWeek(DatabaseUser user, PayrollTransactionWeek item, List<PayrollTransactionQuickEntry> deletedItems);

        //[OperationContract]
        //void DeletePayrollTransactionWeek(DatabaseUser user, PayrollTransactionWeek item);

        //[OperationContract]
        //PayrollTransactionWeek InsertPayrollTransactionWeek(DatabaseUser user, PayrollTransactionWeek item);
        //#endregion

        #region wizards
        [OperationContract]
        WizardCacheCollection GetWizardCache(DatabaseUser user, long wizardId, long? wizardCacheId, String itemName);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        WizardCache UpdateWizardCache(DatabaseUser user, WizardCache wizard);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        WizardCacheItem UpdateWizardCacheItem(DatabaseUser user, WizardCacheItem item);

        [OperationContract]
        Employee AddEmployee(DatabaseUser user, long wizardCacheId, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool autoAddSecondaryPositions);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteWizardEmployee(DatabaseUser user, long wizardCacheId);

        [OperationContract]
        bool ValidateWizardEmployee(DatabaseUser user, WizardCacheCollection wizardCacheCollection);
        #endregion

        #region ScreenControl
        [OperationContract]
        void MergeScreenControl(DatabaseUser user, ScreenControl[] screenControlArray);
        #endregion

        #region import/export
        [OperationContract]
        ImportExportSummaryCollection GetImportExportSummary(DatabaseUser user);

        [OperationContract]
        byte[] GenerateEmployeeBiographicsFile(DatabaseUser user, IEnumerable<long> employeeIds);
        #endregion

        #region vendor
        [OperationContract]
        VendorCollection GetVendor(DatabaseUser user, long VendorId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        Vendor UpdateVendor(DatabaseUser user, Vendor vendor);

        [OperationContract]
        Vendor InsertVendor(DatabaseUser user, Vendor vendor);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteVendor(DatabaseUser user, Vendor vendor);
        #endregion

        #region temporary
        [OperationContract]
        KeyValuePair<String, String>[] GetDynamicsOutputFileList(DatabaseUser user, String dynamicsGPOutputFolder, long payrollProcessId);

        [OperationContract]
        String getFileText(DatabaseUser user, String filePath);
        #endregion

        #region year end adjustments search
        [OperationContract]
        YearEndAdjustmentsSearchReportCollection GetYearEndAdjustmentsSearchReport(DatabaseUser user, YearEndAdjustmentsSearchCriteria criteria);
        #endregion

        #region year end adjustment
        [OperationContract]
        YearEndAdjustmentCollection GetYearEndAdjustment(DatabaseUser user, long employeeId, Decimal year, long yearEndAdjustmentTableId, String employerNumber, String provinceStateCode, long revision, String columnImportIdentifier);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        YearEndAdjustmentCollection UpdateYearEndAdjustment(DatabaseUser user, YearEndAdjustment[] yearEndAdjustmentCollection);

        [OperationContract]
        YearEndAdjustmentsSearchReport InsertYearEndAdjustment(DatabaseUser user, YearEndAdjustmentsSearchReport report, long? previousKeyId = null);

        [OperationContract]
        YearEndAdjustmentTableCollection GetYearEndAdjustmentTables(DatabaseUser user);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteYearEndReport(DatabaseUser user, YearEndAdjustmentsSearchReport reportObj);
        #endregion

        [OperationContract]
        YearEndT4Collection GetYearEndT4(DatabaseUser user, int year, long? businessNumberId);

        [OperationContract]
        YearEndT4Collection GetBatchYearEndT4(DatabaseUser user, string[] yearEndT4Ids);

        #region year end clone
        [OperationContract]
        YearEndT4 SelectSingleT4ByKey(DatabaseUser user, long key);

        [OperationContract]
        YearEndT4a SelectSingleT4aByKey(DatabaseUser user, long key);

        [OperationContract]
        YearEndR1 SelectSingleR1ByKey(DatabaseUser user, long key);

        [OperationContract]
        YearEndR2 SelectSingleR2ByKey(DatabaseUser user, long key);

        [OperationContract]
        YearEndT4rsp SelectSingleT4rspByKey(DatabaseUser user, long key);

        [OperationContract]
        YearEndNR4rsp SelectSingleNR4rspByKey(DatabaseUser user, long key);

        [OperationContract]
        YearEndT4arca SelectSingleT4arcaByKey(DatabaseUser user, long key);
        #endregion

        #region year end
        [OperationContract]
        YearEndCollection GetYearEnd(DatabaseUser user, long? year);

        [OperationContract]
        void CreateCurrentYE(DatabaseUser user);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void CloseVersionYE(DatabaseUser user, YearEnd yearEnd);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void CloseCurrentYE(DatabaseUser user, YearEnd yearEnd);
        #endregion

        #region business number
        [OperationContract]
        BusinessNumberCollection GetBusinessNumber(DatabaseUser user, long? businessNumberId);

        [OperationContract]
        BusinessNumber InsertBusinessNumber(DatabaseUser user, BusinessNumber businessNumber);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void UpdateBusinessNumber(DatabaseUser user, BusinessNumber businessNumber);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteBusinessNumber(DatabaseUser user, BusinessNumber businessNumber);
        #endregion

        #region year end zip file naming format
        [OperationContract]
        String GetYearEndZipFileNamingFormat(String zipFileNameFormat);
        #endregion

        #region sap gl naming format
        [OperationContract]
        String GenerateSapFileName(DatabaseUser user, String fileNameFormat, long payrollProcessId);
        #endregion

        #region mass payslip file zip naming format
        [OperationContract]
        String GenerateFileName(DatabaseUser user, String zipFileNameFormat, long payrollProcessId);
        #endregion

        [OperationContract]
        PayrollMasterPaymentCollection GetPayrollMasterPaymentDirectDeposit(DatabaseUser user, long payrollProcessId, String employeeNumber);

        #region export ftp
        [OperationContract]
        ExportFtpCollection GetExportFtp(DatabaseUser user, long? exportFtpId, string codeExportEftTypeCd, string ftpName);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        ExportFtp UpdateExportFtp(DatabaseUser user, ExportFtp export);

        [OperationContract]
        ExportFtp InsertExportFtp(DatabaseUser user, ExportFtp export);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteExportFtp(DatabaseUser user, ExportFtp export);
        #endregion

        #region pay register export
        [OperationContract]
        PayRegisterExportCollection GetPayRegisterExport(DatabaseUser user, long? exportQueueId, string codeExportFtpTypeCd);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        PayRegisterExport UpdatePayRegisterExport(DatabaseUser user, PayRegisterExport export);

        [OperationContract]
        PayRegisterExport InsertPayRegisterExport(DatabaseUser user, PayRegisterExport export);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeletePayRegisterExport(DatabaseUser user, PayRegisterExport export);
        #endregion

        #region get year end negative amount employee
        [OperationContract]
        Employee GetYearEndNegativeAmountEmployee(DatabaseUser user, Decimal year, int revision);
        #endregion

        #region create citi cheque eft from payroll master payment records
        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        byte[] CreateCitiChequeEftFromPayrollMasterPaymentRecords(DatabaseUser user, String[] payrollMasterPaymentIds, String eftClientNumber, String companyShortName, String employeeNumber, long payrollProccessId, string originatorId, long employeeId);
        #endregion

        #region employee photo
        [OperationContract]
        EmployeePhotoCollection GetEmployeePhoto(DatabaseUser user, long employeeId);

        [OperationContract]
        void InsertEmployeePhoto(DatabaseUser user, EmployeePhoto photo);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void UpdateEmployeePhoto(DatabaseUser user, EmployeePhoto photo);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeePhoto(DatabaseUser user, EmployeePhoto photo);
        #endregion

        #region attachment
        [OperationContract]
        AttachmentCollection GetAttachment(DatabaseUser user, long attachmentId);

        [OperationContract]
        void InsertAttachment(DatabaseUser user, Attachment attachment);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void UpdateAttachment(DatabaseUser user, Attachment attachment);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteAttachment(DatabaseUser user, Attachment attachment);
        #endregion

        #region employee remittance stub
        [OperationContract]
        EmployeeRemittanceStubCollection GetEmployeeRemittanceStub(DatabaseUser user, long employeePaycodeId);

        [OperationContract]
        void InsertEmployeeRemittanceStub(DatabaseUser user, EmployeeRemittanceStub stub);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void UpdateEmployeeRemittanceStub(DatabaseUser user, EmployeeRemittanceStub stub);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeeRemittanceStub(DatabaseUser user, EmployeeRemittanceStub stub);
        #endregion

        #region paycode year end report map
        [OperationContract]
        PaycodeYearEndReportMapCollection GetPaycodeYearEndReportMap(DatabaseUser user, String paycodeCode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        PaycodeYearEndReportMap UpdatePaycodeYearEndReportMap(DatabaseUser user, PaycodeYearEndReportMap paycodeYearEndReportMap);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        PaycodeYearEndReportMap InsertPaycodeYearEndReportMap(DatabaseUser user, PaycodeYearEndReportMap paycodeYearEndReportMap);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeletePaycodeYearEndReportMap(DatabaseUser user, PaycodeYearEndReportMap paycodeYearEndReportMap);
        #endregion

        #region paycode payroll transaction offset association
        [OperationContract]
        PaycodePayrollTransactionOffsetAssociationCollection GetPaycodePayrollTransactionOffsetAssociation(DatabaseUser user, String paycodeCode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        PaycodePayrollTransactionOffsetAssociation UpdatePaycodePayrollTransactionOffsetAssociation(DatabaseUser user, PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        PaycodePayrollTransactionOffsetAssociation InsertPaycodePayrollTransactionOffsetAssociation(DatabaseUser user, PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeletePaycodePayrollTransactionOffsetAssociation(DatabaseUser user, PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation);
        #endregion

        #region accrual entitlement search
        [OperationContract]
        AccrualEntitlementSearchCollection GetAccrualEntitlementReport(DatabaseUser user, String criteria);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteAccrualEntitlementReport(DatabaseUser user, long entitlementId);
        #endregion

        #region accrual entitlement
        [OperationContract]
        AccrualEntitlementCollection GetAccrualEntitlement(DatabaseUser user, long? entitlementId);

        [OperationContract]
        AccrualEntitlement InsertAccrualEntitlement(DatabaseUser user, AccrualEntitlement entitlement);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        AccrualEntitlement UpdateAccrualEntitlement(DatabaseUser user, AccrualEntitlement entitlement);
        #endregion

        #region accrual entitlement detail
        [OperationContract]
        AccrualEntitlementDetailCollection GetAccrualEntitlementDetail(DatabaseUser user, long entitlementId, long entitlementDetailId);

        [OperationContract]
        AccrualEntitlementDetail InsertAccrualEntitlementDetail(DatabaseUser user, AccrualEntitlementDetail entitlementDetail);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        AccrualEntitlementDetail UpdateAccrualEntitlementDetail(DatabaseUser user, AccrualEntitlementDetail entitlementDetail, AccrualEntitlementDetailValueCollection entitlementDetailValueCollection, AccrualEntitlementDetailPaycodeCollection entitlementDetailPaycodeCollection);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteAccrualEntitlementDetail(DatabaseUser user, AccrualEntitlementDetail entitlementDetail);
        #endregion

        #region accrual entitlement detail paycode
        [OperationContract]
        AccrualEntitlementDetailPaycodeCollection GetAccrualEntitlementDetailPaycode(DatabaseUser user, long entitlementDetailId);
        #endregion

        #region accrual entitlement detail value
        [OperationContract]
        AccrualEntitlementDetailValueCollection GetAccrualEntitlementDetailValue(DatabaseUser user, long entitlementDetailId);
        #endregion

        #region accrual policy search
        [OperationContract]
        AccrualPolicySearchCollection GetAccrualPolicyReport(DatabaseUser user, String criteria);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteAccrualPolicyReport(DatabaseUser user, long policyId);
        #endregion

        #region accrual policy
        [OperationContract]
        AccrualPolicyCollection GetAccrualPolicy(DatabaseUser user, long? policyId);

        [OperationContract]
        AccrualPolicy InsertAccrualPolicy(DatabaseUser user, AccrualPolicy policy, AccrualPolicyEntitlementCollection policyEntitlementCollection);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        AccrualPolicy UpdateAccrualPolicy(DatabaseUser user, AccrualPolicy policy, AccrualPolicyEntitlementCollection policyEntitlementCollection);
        #endregion

        #region accrual policy entitlement
        [OperationContract]
        AccrualPolicyEntitlementCollection GetAccrualPolicyEntitlement(DatabaseUser user, long policyId);
        #endregion

        #region global employee paycode
        [OperationContract]
        EmployeePaycodeCollection GetGlobalEmployeePaycode(DatabaseUser user, long? globalEmployeePaycodeId);

        [OperationContract]
        EmployeePaycode InsertGlobalEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeePaycode UpdateGlobalEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteGlobalEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode);
        #endregion

        #region statutory holiday date
        [OperationContract]
        StatutoryHolidayDateCollection GetStatutoryHolidayDateReport(DatabaseUser user, StatutoryDeductionCriteria criteria);

        [OperationContract]
        StatutoryHolidayDateCollection GetStatutoryHolidayDate(DatabaseUser user, long? statutoryHolidayDateId);

        [OperationContract]
        StatutoryHolidayDate InsertStatutoryHolidayDate(DatabaseUser user, StatutoryHolidayDate statutoryHolidayDate);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        StatutoryHolidayDate UpdateStatutoryHolidayDate(DatabaseUser user, StatutoryHolidayDate statutoryHolidayDate);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteStatutoryHolidayDate(DatabaseUser user, StatutoryHolidayDate statutoryHolidayDate);
        #endregion

        #region remittance
        [OperationContract]
        RemittanceCollection GetRemittance(DatabaseUser user);
        #endregion

        #region remittance detail
        [OperationContract]
        RemittanceDetailCraSourceDeductionsCollection GetRemittanceDetailCraSourceDeductions(DatabaseUser user, long? craExportId, string remitCode, DateTime remitDate);
        [OperationContract]
        RemittanceDetailWcbDeductionsCollection GetRemittanceDetailWcbDeductions(DatabaseUser user, string detailType, long? wcbExportId, string remitCode, DateTime remitDate);
        [OperationContract]
        RemittanceDetailChequeHealthTaxDeductionsCollection GetRemittanceDetailChequeHealthTaxDeductions(DatabaseUser user, long? chequeHealthTaxExportId, string codeHealthTaxCode);
        [OperationContract]
        RemittanceDetailGarnishmentDeductionsCollection GetRemittanceDetailGarnishmentDeductions(DatabaseUser user, string detailType, long? exportId);
        [OperationContract]
        RemittanceDetailRqSourceDeductionsCollection GetRemittanceDetailRqSourceDeductions(DatabaseUser user, long? rqExportId);
        #endregion

        #region remittance import
        [OperationContract]
        RemittanceImportCollection GetRemittanceImport(DatabaseUser user, long? remittanceImportId);

        //[OperationContract]
        //RemittanceImportDetailCollection GetRemittanceImportDetail(DatabaseUser user, long remittanceImportId);

        //[OperationContract]
        //void ProcessRemittanceImport(DatabaseUser user, long remittanceImportId, String remittanceImportStatusCode);
        #endregion

        #region email
        [OperationContract]
        EmailCollection GetEmail(DatabaseUser user, long? emailId);

        [OperationContract]
        Email InsertEmail(DatabaseUser user, Email item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        Email UpdateEmail(DatabaseUser user, Email item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmail(DatabaseUser user, Email item);
        #endregion

        #region email template
        [OperationContract]
        EmailTemplateCollection GetEmailTemplate(DatabaseUser user, long emailId);

        [OperationContract]
        EmailTemplate InsertEmailTemplate(DatabaseUser user, EmailTemplate item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmailTemplate UpdateEmailTemplate(DatabaseUser user, EmailTemplate item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmailTemplate(DatabaseUser user, EmailTemplate item);
        #endregion

        #region email rules
        [OperationContract]
        EmailRuleCollection GetEmailRule(DatabaseUser user, long emailId);

        [OperationContract]
        EmailRule InsertEmailRule(DatabaseUser user, EmailRule item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmailRule UpdateEmailRule(DatabaseUser user, EmailRule item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmailRule(DatabaseUser user, EmailRule item);
        #endregion

        #region health tax
        [OperationContract]
        HealthTaxCollection GetHealthTax(DatabaseUser user, long? healthTaxId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        HealthTax InsertHealthTax(DatabaseUser user, HealthTax healthTax);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        HealthTax UpdateHealthTax(DatabaseUser user, HealthTax healthTax);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteHealthTax(DatabaseUser user, HealthTax healthTax);
        #endregion

        #region health tax detail
        [OperationContract]
        HealthTaxDetailCollection GetHealthTaxDetail(DatabaseUser user, long healthTaxId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        HealthTaxDetail InsertHealthTaxDetail(DatabaseUser user, HealthTaxDetail healthTaxDetail);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        HealthTaxDetail UpdateHealthTaxDetail(DatabaseUser user, HealthTaxDetail healthTaxDetail);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteHealthTaxDetail(DatabaseUser user, HealthTaxDetail healthTaxDetail);
        #endregion

        #region employee payslip
        [OperationContract]
        EmployeePayslipCollection GetEmployeePayslip(DatabaseUser user);
        #endregion

        #region labour cost
        [OperationContract]
        LabourCostCollection GetLabourCost(DatabaseUser user, Decimal year);
        #endregion

        #region headcount
        [OperationContract]
        HeadcountCollection GetHeadcount(DatabaseUser user);

        [OperationContract]
        HeadcountCollection GetHeadcountByOrganizationUnitLevelId(DatabaseUser user, long organizationUnitLevelId);
        #endregion

        #region employee profile
        [OperationContract]
        EmployeeProfileCollection GetEmployeeProfile(DatabaseUser user);
        #endregion

        #region payroll breakdown
        [OperationContract]
        PayrollBreakdownCollection GetPayrollBreakdown(DatabaseUser user, String userName, String breakdownType, String filteredBy);
        #endregion

        #region employee custom field config
        [OperationContract]
        EmployeeCustomFieldConfigCollection GetEmployeeCustomFieldConfig(DatabaseUser user, long? employeeCustomFieldConfigId);

        [OperationContract]
        EmployeeCustomFieldConfig InsertEmployeeCustomFieldConfig(DatabaseUser user, EmployeeCustomFieldConfig item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeCustomFieldConfig UpdateEmployeeCustomFieldConfig(DatabaseUser user, EmployeeCustomFieldConfig item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeeCustomFieldConfig(DatabaseUser user, EmployeeCustomFieldConfig item);
        #endregion

        #region employee custom field
        [OperationContract]
        EmployeeCustomFieldCollection GetEmployeeCustomField(DatabaseUser user, long? employeeCustomFieldId);

        [OperationContract]
        EmployeeCustomField InsertEmployeeCustomField(DatabaseUser user, EmployeeCustomField item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeCustomField UpdateEmployeeCustomField(DatabaseUser user, EmployeeCustomField item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeeCustomField(DatabaseUser user, EmployeeCustomField item);
        #endregion


        #region salary plan grade and effective detail
        [OperationContract]
        SalaryPlanGradeAndEffectiveDetailCollection GetSalaryPlanGradeAndEffectiveDetail(DatabaseUser user, long salaryPlanId);

        [OperationContract]
        SalaryPlanGradeMinEffectiveDateCollection SelectMinEffectiveDate(DatabaseUser user, long organizationUnitId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void AddNewSalaryPlanGradeAndSteps(DatabaseUser user, SalaryPlanGradeAndEffectiveDetail item);
        #endregion

        #region salary plan
        [OperationContract]
        SalaryPlanCollection GetSalaryPlan(DatabaseUser user, long? salaryPlanId);

        [OperationContract]
        SalaryPlanOrganizationUnitCollection GetSalaryPlanOrganizationUnit(DatabaseUser user, string organizationUnit);
        #endregion

        #region salary plan grade
        [OperationContract]
        SalaryPlanGradeCollection GetSalaryPlanGrade(DatabaseUser user, long? salaryPlanGradeId, long? salaryPlanId, string languageCode);

        [OperationContract]
        SalaryPlanGradeOrgUnitDescriptionComboCollection PopulateComboBoxWithSalaryPlanGrades(DatabaseUser user, long salaryPlanId, long organizationUnitLevelId);
        #endregion

        #region salary plan grade step
        [OperationContract]
        SalaryPlanGradeStepCollection GetSalaryPlanGradeStep(DatabaseUser user, long? salaryPlanGradeStepId, long? salaryPlanGradeId);

        [OperationContract]
        SalaryPlanGradeStepDetailCollection GetSalaryPlanGradeStepDetail(DatabaseUser user, long? salaryPlanGradeStepDetailId, long? salaryPlanGradeStepId);
        #endregion

        #region real exportexport to ftp
        [OperationContract]
        void CreateAndExportRealEmployeeFiles(DatabaseUser user, string realFtpName, string laborFileName, string employeeFileName, string adjustmentFileName);
        #endregion

        #region labor level export
        [OperationContract]
        byte[] GenerateREALLaborLevelExportFile(DatabaseUser user);
        #endregion

        #region adjustment rule export
        [OperationContract]
        byte[] GenerateREALAdjustmentRuleExportFile(DatabaseUser user);
        #endregion

        #region employee export
        [OperationContract]
        byte[] GenerateREALEmployeeExportFile(DatabaseUser user);
        #endregion

        [OperationContract]
        TempBulkPayrollProcessCollection GetTempBulkPayrollProcess(DatabaseUser user);

        #region benefit policy search
        [OperationContract]
        BenefitPolicySearchCollection GetBenefitPolicyReport(DatabaseUser user, String criteria);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteBenefitPolicyReport(DatabaseUser user, long benefitPolicyId);
        #endregion

        #region benefit plan search
        [OperationContract]
        BenefitPlanSearchCollection GetBenefitPlanReport(DatabaseUser user, String criteria);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteBenefitPlanReport(DatabaseUser user, long benefitPlanId);
        #endregion

        #region benefit policy
        [OperationContract]
        BenefitPolicyCollection GetBenefitPolicy(DatabaseUser user, long? benefitPolicyId);

        [OperationContract]
        BenefitPolicy InsertBenefitPolicy(DatabaseUser user, BenefitPolicy benefitPolicy, BenefitPolicyPlanCollection benefitPolicyPlanCollection);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        BenefitPolicy UpdateBenefitPolicy(DatabaseUser user, BenefitPolicy benefitPolicy, BenefitPolicyPlanCollection benefitPolicyPlanCollection);
        #endregion

        #region benefit plan
        [OperationContract]
        BenefitPlanCollection GetBenefitPlan(DatabaseUser user, long? benefitPlanId);

        [OperationContract]
        BenefitPlan InsertBenefitPlan(DatabaseUser user, BenefitPlan benefitPlan);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        BenefitPlan UpdateBenefitPlan(DatabaseUser user, BenefitPlan benefitPlan);
        #endregion

        #region benefit plan detail
        [OperationContract]
        BenefitPlanDetailCollection GetBenefitPlanDetail(DatabaseUser user, long benefitPlanId, long benefitPlanDetailId);

        [OperationContract]
        BenefitPlanDetail InsertBenefitPlanDetail(DatabaseUser user, BenefitPlanDetail benefitPlanDetail);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        BenefitPlanDetail UpdateBenefitPlanDetail(DatabaseUser user, BenefitPlanDetail benefitPlanDetail);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteBenefitPlanDetail(DatabaseUser user, BenefitPlanDetail benefitPlanDetail);
        #endregion

        #region benefit policy plan
        [OperationContract]
        BenefitPolicyPlanCollection GetBenefitPolicyPlan(DatabaseUser user, long benefitPolicyId);
        #endregion

        #region employee benefit
        [OperationContract]
        EmployeeBenefitCollection GetEmployeeBenefit(DatabaseUser user, long employeeId);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        EmployeeBenefit UpdateEmployeeBenefit(DatabaseUser user, EmployeeBenefit employeeBenefit);

        [OperationContract]
        EmployeeBenefit InsertEmployeeBenefit(DatabaseUser user, EmployeeBenefit employeeBenefit);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteEmployeeBenefit(DatabaseUser user, EmployeeBenefit employeeBenefit);
        #endregion

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        SalaryPlanGradeRuleCollection GetSalaryPlanGradeRules(DatabaseUser user);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        SalaryPlanGradeRule InsertPlanGradeRule(DatabaseUser user, SalaryPlanGradeRule item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        SalaryPlanGradeRule UpdatePlanGradeRule(DatabaseUser user, SalaryPlanGradeRule item);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        SalaryPlanGradeRuleCodeCollection GetSalaryPlanGradeRuleCodes(DatabaseUser user);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        SalaryPlanGradeRuleCode InsertSalaryPlanGradeRuleCode(DatabaseUser user, SalaryPlanGradeRuleCode code);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        SalaryPlanGradeRuleCode UpdateSalaryPlanGradeRuleCode(DatabaseUser user, SalaryPlanGradeRuleCode code);

        [OperationContract]
        [FaultContract(typeof(EmployeeServiceException))]
        void DeleteSalaryPlanGradeRuleCode(DatabaseUser user, SalaryPlanGradeRuleCode code);
    }
}