﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Web.BusinessService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "XmlService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select XmlService.svc or XmlService.svc.cs at the Solution Explorer and start debugging.
    public class Xml : Contract.IXml
    {
        #region fields
        private BusinessLayer.BusinessLogic.XmlManagement _xmlManagement = null;
        #endregion

        #region properties
        private BusinessLayer.BusinessLogic.XmlManagement XmlManagement
        {
            get
            {
                if (_xmlManagement == null)
                    _xmlManagement = new BusinessLayer.BusinessLogic.XmlManagement();

                return _xmlManagement;
            }
        }
        #endregion

        #region import/export

        public void ImportBatchesViaFtp(DatabaseUser user, string importName, string importExportProcessingDirectory, bool importDirtySave, bool autoAddSecondaryPositions, string realFtpName, string filePatternToMatch, string payrollProcessRunTypeCode, string payrollProcessGroupCode)
        {
            XmlManagement.ImportBatchesViaFtp(user, importName, importExportProcessingDirectory, importDirtySave, autoAddSecondaryPositions, realFtpName, filePatternToMatch, payrollProcessRunTypeCode, payrollProcessGroupCode);
        }
        public String ImportFile(DatabaseUser user, long importExportId, byte[] data, String importFileName, String importExportProcessingDirectory, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions)
        {
            return (XmlManagement.ImportFile(user, importExportId, data, importFileName, importExportProcessingDirectory, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions)).ProcessingOutput;
        }
        public byte[] ExportRoe(DatabaseUser user, long[] employeePositionIds, String importExportProcessingDirectory, String roeIssueCode, out bool missingRoeReason)
        {
            return XmlManagement.ExportRoe(user, employeePositionIds, importExportProcessingDirectory, roeIssueCode, out missingRoeReason);
        }
        #endregion

        #region t4 export
        public CanadaRevenueAgencyT4ExportCollection ExportT4Details(DatabaseUser user, String year, String exportType)
        {
            return XmlManagement.ExportT4Details(user, year, exportType);
        }
        public CanadaRevenueAgencyT4Export UpdateExportT4Details(DatabaseUser user, CanadaRevenueAgencyT4Export export)
        {
            return XmlManagement.UpdateExportT4Details(user, export);
        }
        public CanadaRevenueAgencyT4Export InsertExportT4Details(DatabaseUser user, CanadaRevenueAgencyT4Export export, String year)
        {
            return XmlManagement.InsertExportT4Details(user, export, year);
        }
        public byte[] ExportT4XmlFile(DatabaseUser user, int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string exportType)
        {
            return XmlManagement.ExportT4XmlFile(user, year, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, exportType);
        }
        #endregion

        #region CIC T4/T4A/R1 export
        public byte[] ExportT4CIC(DatabaseUser user, int year, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2)
        {
            return XmlManagement.ExportCICT4File(user, year, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2);
        }
        public byte[] ExportT4aCIC(DatabaseUser user, int year, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2)
        {
            return XmlManagement.ExportCICT4aFile(user, year, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2);
        }
        public byte[] ExportR1CIC(DatabaseUser user, int year, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2, String revenueQuebecTaxId)
        {
            return XmlManagement.ExportCICR1File(user, year, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, revenueQuebecTaxId);
        }
        #endregion

        #region t4a export
        public byte[] ExportT4aXmlFile(DatabaseUser user, int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string exportType)
        {
            return XmlManagement.ExportT4aXmlFile(user, year, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, exportType);
        }
        #endregion

        #region NR4 export
        public byte[] ExportNR4XmlFile(DatabaseUser user, int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string exportType)
        {
            return XmlManagement.ExportNR4XmlFile(user, year, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, exportType);
        }
        #endregion

        #region R1 export
        public byte[] ExportR1XmlFile(DatabaseUser user, int year, String importExportProcessingDirectory, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2, String revenueQuebecTaxId)
        {
            return XmlManagement.ExportR1XmlFile(user, year, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, revenueQuebecTaxId);
        }
        public RevenueQuebecR1ExportCollection ExportR1Details(DatabaseUser user, String year, String exportType)
        {
            return XmlManagement.ExportR1Details(user, year, exportType);
        }
        public RevenueQuebecR1Export UpdateExportR1Details(DatabaseUser user, RevenueQuebecR1Export export)
        {
            return XmlManagement.UpdateExportR1Details(user, export);
        }
        public RevenueQuebecR1Export InsertExportR1Details(DatabaseUser user, RevenueQuebecR1Export export)
        {
            return XmlManagement.InsertExportR1Details(user, export);
        }
        #endregion

        #region R2 export
        public byte[] ExportR2XmlFile(DatabaseUser user, int year, String importExportProcessingDirectory, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2, String revenueQuebecTaxId)
        {
            return XmlManagement.ExportR2XmlFile(user, year, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, revenueQuebecTaxId);
        }
        #endregion
    }
}