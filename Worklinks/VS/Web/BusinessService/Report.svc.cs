﻿using System;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Web.BusinessService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Report" in code, svc and config file together.
    public class Report : Contract.IReport
    {
        #region fields
        BusinessLayer.BusinessLogic.ReportManagement _management = null;
        BusinessLayer.BusinessLogic.DynamicsManagement _dynamicsManagement = null;
        #endregion

        #region properties
        public BusinessLayer.BusinessLogic.ReportManagement Management
        {
            get
            {
                if (_management == null)
                    _management = new BusinessLayer.BusinessLogic.ReportManagement();

                return _management;
            }
        }
        public BusinessLayer.BusinessLogic.DynamicsManagement DynamicsManagement
        {
            get
            {
                if (_dynamicsManagement == null)
                    _dynamicsManagement = new BusinessLayer.BusinessLogic.DynamicsManagement();

                return _dynamicsManagement;
            }
        }
        #endregion

        public byte[] GetReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String reportName, long payrollProcessId, String employeeNumber, String reportType, String year, String employerNumber, String provinceCode, String reportShowDescriptionField, String revisionNumber, String reportPayRegisterSortOrder, long employeeId)
        {
            return Management.GetReport(user, reportName, payrollProcessId, employeeNumber, reportType, year, employerNumber, provinceCode, reportShowDescriptionField, revisionNumber, reportPayRegisterSortOrder, null, null, null, null, employeeId);
        }

        public byte[] GetRemittanceReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, string reportName, string detailType, string remitCode, string exportId, string reportType, string remitDate)
        {
            return Management.GetRemittanceReport(user, reportName, detailType, remitCode, exportId, reportType, remitDate);
        }
        public byte[] GetYearEndReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, YearEndReportsCriteria criteria)
        {
            return Management.GetYearEndReport(user, criteria);
        }
        public byte[] GetHRMenuReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, HrReportsCriteria criteria)
        {
            return Management.GetHRMenuReport(user, criteria);
        }
        public byte[] GetPayrollProcessReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String reportName, String reportType, long payrollProcessId)
        {
            return null;
        }
        public byte[] GetReportForAllEmployeesOfThisPayProcess(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String massPayslipInternalFileFormat, long payrollProcessId, String reportShowDescriptionField, String reportPayRegisterSortOrder, int maxDegreeOfParallelism)
        {
            return Management.GetReportForAllEmployeesOfThisPayProcess(user, massPayslipInternalFileFormat, payrollProcessId, reportShowDescriptionField, reportPayRegisterSortOrder, maxDegreeOfParallelism);
        }
        public byte[] CreateGLExportFile(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, String glType, String reportShowDescriptionField, String reportPayRegisterSortOrder)
        {
            return Management.CreateGLExportFile(user, payrollProcessId, glType, reportShowDescriptionField, reportPayRegisterSortOrder);
        }
        public byte[] CreateMercerPensionExportFile(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, String reportShowDescriptionField, String reportPayRegisterSortOrder)
        {
            return Management.CreateMercerPensionExportFile(user, payrollProcessId, reportShowDescriptionField, reportPayRegisterSortOrder);
        }
        public byte[] CreateStockExportFile(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            return Management.CreateStockExportFile(user, payrollProcessId);
        }
        public byte[] ExportCeridianPayProcess(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, long onEFTAmount, String companyNumber, String companyName, BusinessLayer.BusinessObjects.Export.CeridianBilling.ProcessType ceridianBillingProcessType, DateTime wcbStartDate)
        {
            return DynamicsManagement.ExportCeridianPayProcess(user, payrollProcessId, onEFTAmount, companyNumber, companyName, ceridianBillingProcessType, wcbStartDate);
        }
        public byte[] GetReportForAllEmployeesOfThisYear(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String massPayslipInternalFileFormat, long year, String reportType, String reportShowDescriptionField, String reportPayRegisterSortOrder)
        {
            return Management.GetReportForAllEmployeesOfThisYear(user, massPayslipInternalFileFormat, year, reportType, reportShowDescriptionField, reportPayRegisterSortOrder);
        }
        public byte[] GetMassFileForAllReportsForYear(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String[] massPayslipInternalFileFormats, long year, String[] reportTypes, String reportShowDescriptionField, String reportPayRegisterSortOrder, String revision)
        {
            return Management.GetMassFileForAllReportsForYear(user, massPayslipInternalFileFormats, year, reportTypes, reportShowDescriptionField, reportPayRegisterSortOrder, revision);
        }

        //#region report testing
        //public BusinessLayer.BusinessObjects.Test.TestReportCollection GetTestReports(WLP.BusinessLayer.BusinessObjects.DatabaseUser user)
        //{
        //    return Management.GetTestReports(user);
        //}
        //public BusinessLayer.BusinessObjects.Test.TestReportParameterCollection GetTestReportParameters(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long testReportId)
        //{
        //    return Management.GetTestReportParameters(user, testReportId);
        //}
        //public BusinessLayer.BusinessObjects.Test.TestReportExpectedValueCollection GetTestReportExpectedValues(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long testReportId)
        //{
        //    return Management.GetTestReportExpectedValues(user, testReportId);
        //}
        //#endregion

        #region CSB Export
        public CanadaRevenueAgencyBondExportCollection GetCanadaRevenueAgencyBondExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            return Management.GetCanadaRevenueAgencyBondExport(user, payrollProcessId);
        }
        public CanadaRevenueAgencyBondExport InsertCanadaRevenueAgencySavingsBondExportData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CanadaRevenueAgencyBondExport export)
        {
            return Management.InsertCanadaRevenueAgencySavingsBondExportData(user, export);
        }
        public CanadaRevenueAgencyBondExport UpdateCanadaRevenueAgencySavingsBondExportData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CanadaRevenueAgencyBondExport export)
        {
            Management.UpdateCanadaRevenueAgencySavingsBondExportData(user, export);
            return export;
        }
        public byte[] CreateCSBExportFile(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            return Management.CreateCSBExportFile(user, payrollProcessId);
        }
        #endregion

        public ReportCollection GetReportRecord(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String reportName)
        {
            return Management.GetReportRecord(user, reportName);
        }

        #region report export
        public ReportExportCollection GetReportExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? exportId)
        {
            return Management.GetReportExport(user, exportId);
        }
        public ReportExport UpdateReportExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ReportExport export)
        {
            Management.UpdateReportExport(user, export);
            return export;
        }
        public ReportExport InsertReportExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ReportExport export)
        {
            return Management.InsertReportExport(user, export);
        }
        public void DeleteReportExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ReportExport export)
        {
            Management.DeleteReportExport(user, export);
        }
        #endregion

        #region sftp report and export section
        public void CreateReportsAndExports(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, String periodYear, PayRegisterExportCollection collection, string pexFtp, String reportShowDescriptionField, String reportPayRegisterSortOrder, String revenueQuebecTaxId, bool useQuebecTaxId, String citiEftFileName, String citiChequeFileName, String eftClientNumber, String citiChequeAccountNumber, String companyName, String companyShortName, String massPayslipInternalFileFormat, string originatorId, string eftType, int maxDegreeOfParallelism)
        {
            Management.CreateReportsAndExports(user, payrollProcessId, periodYear, collection, pexFtp, reportShowDescriptionField, reportPayRegisterSortOrder, revenueQuebecTaxId, useQuebecTaxId, citiEftFileName, citiChequeFileName, eftClientNumber, citiChequeAccountNumber, companyName, companyShortName, massPayslipInternalFileFormat, originatorId, eftType, maxDegreeOfParallelism);
        }
        #endregion

        #region rbc edi eft export section
        public void FtpRbcEdiExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, RbcEftEdiParameters ediParms, RbcEftSourceDeductionParameters garnishmentParms, string rbcFtpName)
        {
            Management.FtpRbcEdiExport(user, payrollProcessId, ediParms, garnishmentParms, rbcFtpName);
        }
        #endregion
    }
}