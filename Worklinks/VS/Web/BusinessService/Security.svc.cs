﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.Web.BusinessService
{
    public class Security : Contract.ISecurity
    {
        #region fields
        private BusinessLayer.BusinessLogic.SecurityManagement _securityManagement;
        #endregion

        #region properties
        private BusinessLayer.BusinessLogic.SecurityManagement SecurityManagement
        {
            get
            {
                if (_securityManagement == null)
                    _securityManagement = new BusinessLayer.BusinessLogic.SecurityManagement();

                return _securityManagement;
            }
        }
        #endregion

        #region security user
        public bool ValidateUserWithHistory(string databaseName, SecurityUserLoginHistory history, string password, int passwordAttemptWindow, int maxInvalidPasswordAttempts, string loginExpiryDays)
        {
            return SecurityManagement.ValidateUserWithHistory(databaseName, history, password, passwordAttemptWindow, maxInvalidPasswordAttempts, loginExpiryDays);
        }
        public bool ValidateUser(string databaseName, string userName, string password)
        {
            return SecurityManagement.ValidateUser(databaseName, userName, password);
        }
        public bool DoesSecurityUserAlreadyExist(DatabaseUser user, long employeeId)
        {
            return SecurityManagement.DoesSecurityUserAlreadyExist(user, employeeId);
        }
        public bool ValidateUserName(DatabaseUser user, string username)
        {
            return SecurityManagement.ValidateUserName(user, username);
        }
        public bool ValidatePasswordHistory(DatabaseUser user, string username, string password)
        {
            return SecurityManagement.ValidatePasswordHistory(user, username, password);
        }
        //public void UpdateLogoutUser(DatabaseUser user, string userName)
        //{
        //    SecurityManagement.UpdateLogoutUser(user, userName);
        //}
        public SecurityUserCollection GetSecurityUser(string databaseName, string userName)
        {
            return SecurityManagement.GetSecurityUser(databaseName, userName);
        }
        public SecurityUser InsertUser(DatabaseUser user, SecurityUser securityUser, string authDatabaseName, string ngSecurityClientId, string loginExpiryHours, string ngSecurity, string _ngDbSecurity, string _ngApiSecurity)
        {
            return SecurityManagement.InsertUser(user, securityUser, authDatabaseName, ngSecurityClientId, loginExpiryHours, ngSecurity, _ngDbSecurity, _ngApiSecurity);
        }
        public void UpdateSecurityUser(DatabaseUser user, string username, string newPassword, SecurityUser securityUser, string passwordExpiryDays, string loginExpiryHours, string adminDatabaseName, bool onlyUpdateLocalDbForLockedOutFlag)
        {
            SecurityManagement.UpdateSecurityUser(user, username, newPassword, securityUser, passwordExpiryDays, loginExpiryHours, adminDatabaseName, onlyUpdateLocalDbForLockedOutFlag);
        }
        public void UpdateSecurityUserPassword(DatabaseUser user, string username, string newPassword, SecurityUser securityUser, string passwordExpiryDays, string loginExpiryHours, string adminDatabaseName)
        {
            SecurityManagement.UpdateSecurityUserPassword(user, username, newPassword, securityUser, false, passwordExpiryDays, loginExpiryHours,adminDatabaseName);
        }
        #endregion

        #region Security Categories
        public SecurityCategoryCollection GetSecurityCategories(DatabaseUser user, bool isViewMode)
        {
            return SecurityManagement.GetSecurityCategories(user, isViewMode);
        }
        public void UpdateCategories(DatabaseUser user, SecurityCategoryCollection securityCategoryColl)
        {
            SecurityManagement.UpdateCategories(user, securityCategoryColl);
        }
        public void RebuildSecurity(DatabaseUser user)
        {
            SecurityManagement.RebuildSecurity(user);
        }
        #endregion

        #region user admin
        public UserSummaryCollection GetUserSummary(DatabaseUser user, UserAdminSearchCriteria criteria)
        {
            return SecurityManagement.GetUserSummary(user, criteria);
        }
        public SecurityUserCollection GetPersonUserDetails(DatabaseUser user, long personId)
        {
            return SecurityManagement.GetPersonUserDetails(user, personId);
        }
        public void DeleteUser(DatabaseUser user, long personId)
        {
            SecurityManagement.DeleteUser(user, personId);
        }
        public int SelectSecurityClientUser(DatabaseUser user, string authDatabaseName, long securityUserId)
        {
            return SecurityManagement.SelectSecurityClientUser(user, authDatabaseName, securityUserId);
        }

        #endregion

        #region security marking
        public SecurityMarkingCollection GetSecurityMarking(DatabaseUser user, int hierarchicalSortOrder)
        {
            return SecurityManagement.GetSecurityMarking(user, hierarchicalSortOrder);
        }
        #endregion

        #region security label role
        public SecurityLabelRoleCollection GetSecurityLabelRole(DatabaseUser user, long securityRoleId)
        {
            return SecurityManagement.GetSecurityLabelRole(user, securityRoleId);
        }
        public SecurityLabelRoleCollection UpdateSecurityLabelRole(DatabaseUser user, long roleId, SecurityLabelRoleCollection labels)
        {
            SecurityManagement.UpdateSecurityLabelRole(user, roleId, labels);
            return labels;
        }
        #endregion

        #region Role Description
        public RoleDescriptionCollection GetRoleDescriptions(DatabaseUser user, string criteria, long? roleId, bool? worklinksAdministrationFlag)
        {
            return SecurityManagement.GetRoleDescriptions(user, criteria, roleId, worklinksAdministrationFlag);
        }
        public RoleDescription UpdateRoleDesc(DatabaseUser user, string authDatabaseName, string ngSecurityClientId, RoleDescription roleDesc)
        {
            SecurityManagement.UpdateRoleDesc(user, authDatabaseName, ngSecurityClientId, roleDesc);
            return roleDesc;
        }
        public RoleDescription InsertRoleDescription(DatabaseUser user, string authDatabaseName, string ngSecurityClientId, RoleDescription roleDesc)
        {
            return SecurityManagement.InsertRoleDescription(user, authDatabaseName, ngSecurityClientId, roleDesc);
        }
        #endregion

        #region Group Description
        //used in UserAdmin to show a person's groups
        public GroupDescriptionCollection GetUserGroups(DatabaseUser user, long securityUserId, bool isViewMode, bool? worklinksAdministrationFlag)
        {
            return SecurityManagement.GetUserGroups(user, securityUserId, isViewMode, worklinksAdministrationFlag);
        }
        public GroupDescriptionCollection GetGroupDescriptions(DatabaseUser user, string criteria, long? groupId, bool? worklinksAdministrationFlag)
        {
            return SecurityManagement.GetGroupDescriptions(user, criteria, groupId, worklinksAdministrationFlag);
        }
        public GroupDescription UpdateGroupDesc(DatabaseUser user, string authDatabaseName, string ngSecurityClientId, GroupDescription grpDesc)
        {
            SecurityManagement.UpdateGroupDesc(user, authDatabaseName, ngSecurityClientId, grpDesc);
            return grpDesc;
        }
        public GroupDescription InsertGroupDescription(DatabaseUser user, string authDatabaseName, string ngSecurityClientId, GroupDescription grpDesc)
        {
            return SecurityManagement.InsertGroupDescription(user, authDatabaseName, ngSecurityClientId, grpDesc);
        }
        public void UpdateUserGroups(DatabaseUser user, string authDatabaseName, string ngSecurityClientId, string userName, long securityUserId, GroupDescription[] groupArray)
        {
            GroupDescriptionCollection collection = new GroupDescriptionCollection();
            collection.Load(groupArray);
            SecurityManagement.UpdateUserGroups(user, authDatabaseName, ngSecurityClientId, userName, securityUserId, collection);
        }
        #endregion

        #region Form Security
        public FormSecurityCollection GetFormSecurityInfo(DatabaseUser user, long roleId, bool? worklinksAdministrationFlag)
        {
            return SecurityManagement.GetFormSecurityInfo(user, roleId, worklinksAdministrationFlag);
        }
        public void UpdateFormSecurity(DatabaseUser user, string authDatabaseName, string ngSecurityClientId, FormSecurity[] formsArray, long roleId)
        {
            FormSecurityCollection collection = new FormSecurityCollection();
            collection.Load(formsArray);
            SecurityManagement.UpdateFormSecurity(user, authDatabaseName, ngSecurityClientId, collection, roleId);
        }
        #endregion

        public string GetEmailByEmployeeId(DatabaseUser user, long employeeId)
        {
            return SecurityManagement.GetEmailByEmployeeId(user, employeeId);
        }

        #region SecurityUserProfile
        public SecurityUserProfileCollection GetSecurityUserProfile(string databaseName, string userName)
        {
            return SecurityManagement.GetSecurityUserProfile(databaseName, userName);
        }
        public SecurityUserProfile UpdateSecurityUserProfile(DatabaseUser user, SecurityUserProfile item)
        {
            SecurityManagement.UpdateSecurityUserProfile(user, item);
            return item;
        }
        #endregion
    }
}