﻿//using System.Runtime.Serialization;
//using System;
//using System.ServiceModel;
//using WorkLinks.BusinessLayer.BusinessLogic;


//namespace WorkLinks.Web.BusinessService.Exceptions
//{
//    [DataContract]
//    public class EmployeeServiceException
//    {
//        public enum ExceptionCodes
//        {
//            DataConcurrency,
//            ForeignKeyConstraint,
//            Other,
//        }

//        public EmployeeServiceException(String message, ExceptionCodes code, Exception exc)
//        {
//            ExceptionCode = (ExceptionCodes)code;
//        }

//        #region fields
//        private ExceptionCodes _exceptionCode = 0;
//        #endregion

//        #region properties
//        [DataMember]
//        public ExceptionCodes ExceptionCode
//        {
//            get { return _exceptionCode; }
//            set { _exceptionCode = value; }
//        }
//        #endregion

//        public static FaultException<EmployeeServiceException> CreateFromAccessException(EmployeeManagementException exc)
//        {
//            EmployeeServiceException serviceExc = null;
//            serviceExc = new EmployeeServiceException(exc.Message, (ExceptionCodes)exc.ExceptionCode, exc);
//            return new FaultException<EmployeeServiceException>(serviceExc, exc.Message);
//        }

//    }
//}