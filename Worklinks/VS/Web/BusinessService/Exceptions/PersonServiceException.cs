﻿//using System.Runtime.Serialization;
//using System;
//using System.ServiceModel;
//using WorkLinks.BusinessLayer.BusinessLogic;

//namespace WorkLinks.Web.BusinessService.Exceptions
//{
//    [DataContract]
//    public class PersonServiceException
//    {
//        public enum ExceptionCodes
//        {
//            DataConcurrency,
//            ForeignKeyConstraint,
//            Other,
//        }

//        #region fields
//        private ExceptionCodes _exceptionCode = 0;
//        private String _message = "";
//        private Exception _exc = null;
//        #endregion

//        #region properties
//        [DataMember]
//        public ExceptionCodes ExceptionCode
//        {
//            get { return _exceptionCode; }
//            set { _exceptionCode = value; }
//        }
//        [DataMember]
//        public String ExceptionMessage
//        {
//            get { return _message; }
//            set { _message = value; }
//        }
//        [DataMember]
//        public Exception Exc
//        {
//            get { return _exc; }
//            set { _exc = value; }
//        }
//        #endregion

//        public PersonServiceException(String message, ExceptionCodes code, Exception exc)
//        {
//            ExceptionCode = code;
//            ExceptionMessage = message;
//            Exc = exc;
//        }

//        public static FaultException<PersonServiceException> CreateFromPersonManagementException(PersonManagementException exc)
//        {
//            PersonServiceException serviceExc = null;
//            PersonManagementException personManagementException = new PersonManagementException(exc.Message, (PersonManagementException.ExceptionCodes)exc.ExceptionCode, exc);
//            serviceExc = new PersonServiceException(exc.Message, (ExceptionCodes)exc.ExceptionCode, personManagementException);
//            return new FaultException<PersonServiceException>(serviceExc, exc.Message);
//        }
//    }
//}