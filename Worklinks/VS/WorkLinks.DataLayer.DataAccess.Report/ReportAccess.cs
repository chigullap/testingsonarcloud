﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Reporting.WebForms;

namespace WorkLinks.DataLayer.DataAccess.Report
{
    public class ReportAccess
    {
        private string _reportServerURL = null;

        public ReportAccess(String reportServerURL)
        {
            _reportServerURL = reportServerURL;
        }

        public byte[] GetReport(String databaseName, String reportPath, long payrollProcessId, String employeeNumber, String payrollProcessGroupCode, String languageCode, String sortOrder, String showDescription)
        {
            ReportViewer viewer = new ReportViewer();
            viewer.ServerReport.ReportServerUrl = new Uri(_reportServerURL);
            System.Collections.Generic.List<Microsoft.Reporting.WebForms.ReportParameter> parms
                = new System.Collections.Generic.List<Microsoft.Reporting.WebForms.ReportParameter>();

            parms.Add(new Microsoft.Reporting.WebForms.ReportParameter("databaseName", databaseName));
            parms.Add(new Microsoft.Reporting.WebForms.ReportParameter("payrollProcessId", payrollProcessId.ToString()));


            if (employeeNumber.Equals("a") && payrollProcessGroupCode.Equals("b"))
            {
                parms.Add(new Microsoft.Reporting.WebForms.ReportParameter("languageCode", languageCode));
                parms.Add(new Microsoft.Reporting.WebForms.ReportParameter("order_by", sortOrder));
            }
            else if (employeeNumber.Equals("a") && payrollProcessGroupCode.Equals("c"))
            {
                parms.Add(new Microsoft.Reporting.WebForms.ReportParameter("languageCode", languageCode));
            }
            else if (employeeNumber.Equals("mass") && payrollProcessGroupCode.Equals("mass"))
            {
                parms.Add(new Microsoft.Reporting.WebForms.ReportParameter("showDescription", showDescription));
            }
            else
            {
                parms.Add(new Microsoft.Reporting.WebForms.ReportParameter("employeeNumber", employeeNumber));
                parms.Add(new Microsoft.Reporting.WebForms.ReportParameter("showDescription", showDescription));
            }

            viewer.ServerReport.ReportPath = reportPath;

            viewer.ServerReport.SetParameters(parms);

            return viewer.ServerReport.Render("PDF");
        }
    }
}
