﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.Web.BusinessService.Contract;

namespace ConsoleApplication2
{
    class Program
    {
        private static ChannelFactory<ICode> _codeFactory = null;
        private static ICode _codeProxy = null;


        private static CodeService _service = new CodeService("http://localhost:50010", "Code.svc");

        private static ChannelFactory<ICode> CodeFactory
        {
            get
            {
                if (_codeFactory == null)
                {
                    _codeFactory = new ChannelFactory<ICode>(new BasicHttpBinding(), new EndpointAddress("http://localhost:50010/Code.svc"));
                }

                return _codeFactory;
            }
        }

        private static ICode CodeProxy
        {
            get
            {
                if (_codeProxy == null || 1==1)
                {
                    _codeProxy = CodeFactory.CreateChannel();
                }
                return _codeProxy;
            }
        }

        //public static ICode GetCodeProxy()
        //{
        //    return new ChannelFactory<ICode>(new BasicHttpBinding(), new EndpointAddress("http://localhost:50010/Code.svc")).CreateChannel();
        //}
        


        static void Main(string[] args)
        {


            //29.15
            //28.13

            //25.66
            //25.82

            old();
            DateTime now = DateTime.Now;
            old();
            Console.Out.WriteLine((DateTime.Now - now).TotalSeconds);


            newer();
            now = DateTime.Now;
            newer();
            Console.Out.WriteLine((DateTime.Now - now).TotalSeconds);
            String blah = Console.In.ReadLine();

        }


        public static void old()
        {
            ServiceReference1.CodeClient client = new ServiceReference1.CodeClient();
            for (int i = 0; i < 1000; i++)
            {
                ServiceReference1.CodeCollection collection = client.GetCodeTable((new WLP.BusinessLayer.BusinessObjects.Language("EN")), WorkLinks.BusinessLayer.BusinessObjects.CodeTableType.CountryCode, "CA");
                if (i % 100 == 0) Console.Out.WriteLine(i);
            }

            client.Close();
        }

        //public static void newer()
        //{
        //    ICode proxy = null;
        //        try
        //        {
        //             proxy= CodeProxy;
        //             ((IClientChannel)proxy).Open();

        //            for (int i = 0; i < 1000; i++)
        //            {
        //                CodeCollection collection = proxy.GetCodeTable((new WLP.BusinessLayer.BusinessObjects.Language("EN")), WorkLinks.BusinessLayer.BusinessObjects.CodeTableType.CountryCode, "CA");
        //                if (i % 100 == 0) Console.Out.WriteLine(i);
        //            }
        //        }
        //        catch
        //        {
        //            ((IClientChannel)proxy).Abort();
        //            throw;
        //        }
        //        finally
        //        {
        //            ((IClientChannel)proxy).Close();
        //        }

        //    }
        public static void newer()
        {

            using (CodeChannelWrapper codeService = _service.CreateChannel())
            {

                for (int i = 0; i < 1000; i++)
                {
                    CodeCollection collection = codeService.Channel.GetCodeTable((new WLP.BusinessLayer.BusinessObjects.Language("EN")), WorkLinks.BusinessLayer.BusinessObjects.CodeTableType.CountryCode, "CA");
                    if (i % 100 == 0) Console.Out.WriteLine(i);
                }
            }

        }
        
    }
   

}
