# 0 - user id = sa
# 1 - password = Niatross8888
# 2 - company name = "Pendylum PEI / Worklinks"
# 3 - batch number = BATCH_02B
# 4 - batch comment = Batch_02
# 5 - job code of batch = Combined
# 6 - pay period = 26/27
# 7 - period start date = 14112010
# 8 - period end date = 1122010
# 9 - batch file name = :C:data/dynamics_import/transaction/All_Employees_1_BCH.CSV
# 10 - templateFile = Z:\work\1_Pendylum\adjustments_batch_calculation_template.txt
# 11 - workingDirectory = Z:\work\1_Pendylum\

##############################
#     login dynamics         #
##############################
CheckActiveWin dictionary 'default' form Login window Login
MoveTo field 'User ID'
TypeTo field 'User ID' , '{0}'
MoveTo field Password
TypeTo field Password , '{1}'
MoveTo field 'OK Button'
ClickHit field 'OK Button'
NewActiveWin dictionary 'default' form 'Switch Company' window 'Switch Company'
ClickHit field '(L) Company Names' item 1 # '{2}'
MoveTo field 'OK Button'
ClickHit field 'OK Button'
CommandExec dictionary 'default' form 'Command_System' command CloseAllWindows
ActivateWindow dictionary 'default' form Toolbar window 'Main_Menu_1'
NewActiveWin dictionary 'default'  form sheLL window sheLL 


##############################
#     create batch           #
##############################
  CommandExec dictionary 'Canadian Payroll'  form 'Command_CPR' command 'P_CPY_SETP_Batch' 
NewActiveWin dictionary 'Canadian Payroll'  form 'P_CPY_SETP_Batch' window 'P_CPY_SETP_Batch' 
  TypeTo field 'Batch Number' , {3}
  CommandExec dictionary 'Canadian Payroll'  form 'P_CPY_SETP_Batch' command 'Delete Button_w_P_CPY_SETP_Batch_f_P_CPY_SETP_Batch' 
# Are you sure you want to delete this record?
NewActiveWin dictionary 'Canadian Payroll'  form DiaLog window DiaLog 
  ClickHit field OK 
NewActiveWin dictionary 'Canadian Payroll'  form 'P_CPY_SETP_Batch' window 'P_CPY_SETP_Batch' 
CloseWindow dictionary 'Canadian Payroll'  form 'P_CPY_SETP_Batch' window 'P_CPY_SETP_Batch' 
NewActiveWin dictionary 'default'  form sheLL window sheLL 
  CommandExec dictionary 'Canadian Payroll'  form 'Command_CPR' command 'P_CPY_SETP_Batch' 
NewActiveWin dictionary 'Canadian Payroll'  form 'P_CPY_SETP_Batch' window 'P_CPY_SETP_Batch' 


  TypeTo field 'Batch Number' , '{12}'
  MoveTo field 'Batch Comment' 
  TypeTo field 'Batch Comment' , '{4}'
  MoveTo field 'P_Batch_Type' item 0 
  ClickHit field 'P_Batch_Type' item 8  # 'Other' #### switch from 'Manual' to 'Other' so dynamics will import the batch
  MoveTo field 'P_Pay_Periods' item 0 
  ClickHit field 'P_Pay_Periods' item 3  # '{6}' 
  MoveTo field 'P_Start_Date' 
  TypeTo field 'P_Start_Date' , '{7}'
  MoveTo field 'P_Cutoff_Date' 
  MoveTo field 'P_Cheque_Date' 
  TypeTo field 'P_Cheque_Date' , '{8}'
  MoveTo field 'P_Frequency_Batch' litem 0 
  ClickHit field 'P_Frequency_Batch' litem {14}  
  MoveTo field 'Save Button' 
  ClickHit field 'Save Button' 
  CommandExec form BuiLtin command cmdCloseWindow 
NewActiveWin dictionary 'default'  form sheLL window sheLL 


##############################
#     import transactions    #
##############################
  CommandExec dictionary 'Canadian Payroll'  form 'Command_CPR' command 'P_CPY_Data_Import_Export_Functions' 
NewActiveWin dictionary 'Canadian Payroll'  form 'P_CPY_Data_Import_Export_Functions' window 'P_CPY_Data_Import_Export_Functions' 
  MoveTo field 'Batch Number' 
  TypeTo field 'Batch Number' , '{9}'
  MoveTo field '(L) Import Batch Data' 
  ClickHit field '(L) Import Batch Data' 
  FileOpen file '{10}' type 0 
  
  
# This function will import batch records to batch BATCH_02B. Do you wish to continue?
NewActiveWin dictionary 'Canadian Payroll'  form DiaLog window DiaLog 
  ClickHit field OK 
NewActiveWin dictionary 'Canadian Payroll'  form 'P_CPY_Data_Import_Export_Functions' window 'P_CPY_Data_Import_Export_Functions' 
#******** Batch Import Successful. *****
NewActiveWin dictionary 'Canadian Payroll'  form DiaLog window DiaLog 
  ClickHit field OK 


NewActiveWin dictionary 'Canadian Payroll'  form 'P_Progress_Control' window 'P_Progress_Control' 
NewActiveWin dictionary 'Canadian Payroll'  form 'P_CPY_Data_Import_Export_Functions' window 'P_CPY_Data_Import_Export_Functions' 
NewActiveWin dictionary 'DEX.DIC'  form 'Report Ask' window 'Report Type' 
  MoveTo field '(L) Cancel' 
  ClickHit field '(L) Cancel' 
NewActiveWin dictionary 'Canadian Payroll'  form 'P_CPY_Data_Import_Export_Functions' window 'P_CPY_Data_Import_Export_Functions' 
  MoveTo field 'P_Close_Form_Button' 
  ClickHit field 'P_Close_Form_Button' 
NewActiveWin dictionary 'default'  form sheLL window sheLL 

##############################
#     close dynamics         #
##############################

  CommandExec dictionary 'default'  form 'Command_System' command CloseAllWindows 
  ActivateWindow dictionary 'default'  form Toolbar window 'Main_Menu_1' 
  CommandExec form BuiLtin command cmdQuitApplication 


### The rest of the logic will be executed in adjustments_batch_calculation_template2.txt after a database update is performed.
