set login=%1
set password=%2
set company=%3
set macroPath=%4
set batchNumber=%5
set outputPath1=%6
set outputPath2=%7

java -jar dynamicMacro.jar post %login% %password% %company% %batchNumber% %macroPath% %outputPath1% %outputPath2%

