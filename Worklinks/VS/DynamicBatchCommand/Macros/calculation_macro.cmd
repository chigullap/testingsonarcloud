set login=%1
set password=%2
set company=%3
set macroPath=%4
set inputFile=%5
set batchNumber=%6
set outputPath1=%7
set outputPath2=%8
set batchType=%9
set frequency=%10
set startDate=%11
set cutoffDate=%12

java.exe -jar dynamicMacro.jar calc %login% %password% %company% %batchNumber% %batchNumber% %batchType% %frequency% %startDate% %cutoffDate% %inputFile% %macroPath% %outputPath1% %outputPath2%
