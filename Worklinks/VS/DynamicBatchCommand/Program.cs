﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace DynamicBatchCommand
{
    class Program
    {
        #region fields
        //management class
        private static WorkLinks.BusinessLayer.BusinessLogic.MicrosoftDynamicsManagement _dynamicsMgmt = null;

        //paths
        private static String _inBoxProcessedPath = System.Configuration.ConfigurationManager.AppSettings["InBoxProcessedPath"];



        #endregion

        #region properties
        private static WorkLinks.BusinessLayer.BusinessLogic.MicrosoftDynamicsManagement DynamicsMgmt
        {
            get
            {
                if (_dynamicsMgmt == null)
                    _dynamicsMgmt = new WorkLinks.BusinessLayer.BusinessLogic.MicrosoftDynamicsManagement();

                return _dynamicsMgmt;
            }
        }
        #endregion

        #region main
        static int Main(String[] args)
        {
            //any files in inbox?
            String fileSuffix = "xml";
            String[] fileNames = Directory.GetFiles(_inBoxPath, String.Format("*.{0}", fileSuffix));
            bool reRunImport = false;
            bool skipProcessing = false;

            //lock files
            if (fileNames.Length > 0)
            {
                List<FileStream> files = null;

                try
                {
                    files = LockFiles(fileNames);
                    var orderedFiles = files.OrderBy(f => f.Name);

                    foreach (FileStream file in orderedFiles)
                    {
                        System.Xml.Serialization.XmlSerializer xml = new System.Xml.Serialization.XmlSerializer(typeof(DynamicsRequest));
                        DynamicsRequest request = (DynamicsRequest)xml.Deserialize(file);


                        switch (request.Type)
                        {
                            case DynamicsRequest.RequestType.ImportAndPostTransaction:
                                if (args.Length == 0 || args[0].ToLower().Equals("calc"))
                                {
                                    //regular and quick batches
                                    if (request.BatchType.ToLower() != "manual")
                                    {
                                        //validate the incoming wsib and paycodes from worklinks.  Proceed if they exist in GP, otherwise setup a message for the service to return
                                        if (CheckWsibCodes(request, request.DynamicsCompanyCode) && CheckPaycodes(request, request.DynamicsCompanyCode))
                                        {
                                            ImportEmployee(request);
                                            ProcessTransactionImport(request);

                                            foreach (Employee emp in request.Employees) //for each employee...
                                            {
                                                foreach (EmployeePaycode empPayCode in emp.Paycodes) //check each paycode to see if GarnishmentFlag is true...
                                                {
                                                    if (empPayCode.GarnishmentAlternateNetCalculationFlag) //perform garnishment calculations
                                                    {
                                                        //perform calcs
                                                        CalculateGarnishmentAmount(empPayCode, emp.EmployeeNumber, request.DynamicsCompanyCode);

                                                        if (empPayCode.AmountRate != null)
                                                        {
                                                            //update worklinks database with new paycode information using SqlServerEmployeePaycode->update
                                                            DynamicsMgmt.UpdateEmployeePaycode(emp.EmployeeNumber, empPayCode, request.DynamicsCompanyCode);

                                                            //update our flag to re-run the import
                                                            reRunImport = true;
                                                        }
                                                    }
                                                }
                                            }

                                            if (reRunImport)
                                            {
                                                reRunImport = false;
                                                ProcessTransactionImport(request);
                                            }
                                        }
                                        else
                                        {
                                            //either a wsib or paycode from worklinks doesnt exist in GP.  Create a message to be processed by the DynamicsImportService
                                            Console.WriteLine("invalid wsib or paycode, asking import service to restore data");
                                            GetServiceToResetPayroll(_wsibOrPaycodeError, "", request.RequestName, request.DynamicsCompanyName, request.DynamicsCompanyCode, request.RequesterEmailAddress, request.DynamicsCompanyCode);
                                            skipProcessing = true;
                                        }
                                    }
                                    else //adjustments
                                    {
                                        ProcessTransactionImport(request);

                                        //update dynamics db table
                                        UpdateDynamicsAdjustmentRecord(request.RequestName, request.DynamicsCompanyCode, request.Adjustments);

                                        //run part 2 of adjustments batch macro
                                        ProcessAdjustmentsTransactionImportPart2(request);
                                    }

                                    //master pay detail insert
                                    Console.WriteLine("Performing tax override if needed");
                                    String arrearsMsg = PerformTaxOverride(request);

                                    //we have an arrears issue with an employee
                                    if (arrearsMsg.Length > 0)
                                    {
                                        GetServiceToResetPayroll(_ArrrearsError, arrearsMsg, request.RequestName, request.DynamicsCompanyName, request.DynamicsCompanyCode, request.RequesterEmailAddress, request.DynamicsCompanyCode);
                                        skipProcessing = true;
                                    }
                                }

                                if (!skipProcessing)
                                {
                                    if (args.Length == 0 || args[0].ToLower().Equals("post"))
                                    {
                                        String glEmployeeIds = "";
                                        glEmployeeIds = CheckGlAccounts(request.DynamicsCompanyCode);

                                        if (glEmployeeIds == "") //make sure there are no missing GL accounts
                                        {
                                            //backup the calcuation data
                                            Console.WriteLine("performing GP database backup");
                                            PerformDatabaseBackup(_databaseBackupPath, request.DynamicsCompanyCode);

                                            //perform the post
                                            Console.WriteLine("performing post");
                                            PostTransaction(request);

                                            //grab info from dynamics db and write to an xml file to be imported later
                                            Console.WriteLine("performing data capture");
                                            DynamicsEFTCollection dynamicsEftColl = new DynamicsEFTCollection();
                                            GetDynamicsResponse(dynamicsEftColl, request);
                                        }
                                        else
                                        {
                                            Console.WriteLine("missing GL Account");
                                            GetServiceToResetPayroll(_GLError, glEmployeeIds, request.RequestName, request.DynamicsCompanyName, request.DynamicsCompanyCode, request.RequesterEmailAddress, request.DynamicsCompanyCode);
                                        }
                                    }
                                }

                                //reset var
                                skipProcessing = false;

                                break;

                            case DynamicsRequest.RequestType.RestoreGPDatabase:

                                //restore GP database from backup
                                Console.WriteLine("restoring GP database backup");
                                RestoreGPDatabase(_databaseBackupPath, request.DynamicsCompanyCode);

                                break;

                            case DynamicsRequest.RequestType.ProcessYearEnd:

                                //backup the year end data
                                Console.WriteLine("performing GP year end database backup");
                                PerformYearEndDatabaseBackup(_yearEndDatabaseBackupPath, request.DynamicsCompanyCode, request.RequestName);

                                Console.WriteLine("performing year end in GP");
                                PerformYearEnd(request);

                                //grab year end t4, t4a, r1 data and write to an xml file to be imported later
                                Console.WriteLine("creating year end file for worklinks service");
                                CreateYearEndXml(request.RequestName, request.DynamicsCompanyCode, _DynamicsServicePath); //requestName holds the Year value

                                break;
                        }

                        UnlockFile(file);

                        if (args.Length == 0 || args[0].ToLower().Equals("post"))
                            RenameFile(file);
                    }
                }
                finally
                {
                    UnlockFiles(files);
                }
            }

            return 0;
        }
        #endregion

        #region methods
  
        public static bool CheckApplicationDatabaseVersions(String databaseVersion)
        {
            String version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            return (version.Equals(databaseVersion));
        }


      
        /// <summary>
        /// locks all files for processing
        /// </summary>
        /// <param name="fileNames"></param>
        /// <returns></returns>
        private static List<FileStream> LockFiles(String[] fileNames)
        {
            List<FileStream> files = new List<FileStream>();

            foreach (String fileName in fileNames)
            {
                //LogComment(String.Format("Locking File {0}", fileName), control);
                FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.None);
                files.Add(stream);
            }

            return files;
        }
        private static void UnlockFiles(List<FileStream> files)
        {
            foreach (FileStream file in files)
            {
                //LogComment(String.Format("Unlocking File {0}", file.Name), control);
                UnlockFile(file);
            }
        }
        private static void UnlockFile(FileStream file)
        {
            file.Close();
        }
        /// <summary>
        /// moves files after processing
        /// </summary>
        /// <param name="files"></param>
        /// <param name="control"></param>
        private static void RenameFiles(List<FileStream> files)
        {
            foreach (FileStream file in files)
            {
                //get processed directory
                RenameFile(file);
            }
        }
        private static void RenameFile(FileStream file)
        {
            File.Move(file.Name, _inBoxProcessedPath + Path.DirectorySeparatorChar + Path.GetFileName(file.Name));
        }
        #endregion
    }
}