﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;


namespace DynamicsBatchCommand
{

public class BuildMacro {

//	# {0} - user id = 'sa'
//		# {1} - password = 'Niatross8888'
//		# {2} - company name = 'Pendylum PEI / Worklinks'
//		# {3} - batch number = 'BATCH_02B'
//		# {12} - batch number = 'BATCH_02B'
//		# {4} - batch comment = 'Batch 02'
//		# {5} - job code of batch = 'Combined'
//		# {6} - pay period = '26/27'
//		# {7} - period start date = '14112010'
//		# {8} - period end date = '1122010'
//		# {9} - batch to run = 'BATCH_02B'
//		# {10} - batch file name = ':C:data/dynamics_import/transaction/All_Employees_1_BCH.CSV'
//		# {11} - batch number = 'BATCH_02B'

    private static String batchType = null;
    private static String userId = null;
    private static String password = null;
    private static String companyName = null;
    private static String batchNumber = null;
    private static String batchComment = null;
    private static String jobCode = null;
    private static String payPeriod = null;
    private static String periodStartDate = null;
    private static String periodEndDate = null;
    private static String batchFileName = null;
    private static String templateFileDirectory = null;
    private static String workingDirectory = null;
    private static String workingDirectoryDynmaicFormat = null;
    private static String employeeCsv                   = null;
    private static String employeeImportRpt             = null;
    private static String employeePayCodeCsv            = null;

    private static String _dynamicsExecutable = " \"C:\\Program Files (x86)\\Microsoft Dynamics\\GP2010\\DYNAMICS.EXE\" \"C:\\Program Files (x86)\\Microsoft Dynamics\\GP2010\\DYNAMICS.SET\" ";

    private String createCalcMacro(String filename)  
    {
        String lineSep = System.Environment.NewLine;
        StringBuilder sb = new StringBuilder();

        using (System.IO.StreamReader stream = new System.IO.StreamReader(filename))
        {

       String nextLine = "";
       while ((nextLine = stream.ReadLine()) != null) 
       {
           if (nextLine.Contains("{0}"))
               nextLine = nextLine.Replace("{0}", userId);

           if (nextLine.Contains("{1}"))
               nextLine = nextLine.Replace("{1}", password);

           if (nextLine.Contains("{2}"))
               nextLine = nextLine.Replace("{2}", companyName);

           if (nextLine.Contains("{3}"))
               nextLine = nextLine.Replace("{3}", batchNumber);

           if (nextLine.Contains("{4}"))
               nextLine = nextLine.Replace("{4}", batchComment);

           if (nextLine.Contains("{5}"))
               nextLine = nextLine.Replace("{5}", jobCode);

           if (nextLine.Contains("{6}"))
               nextLine =  nextLine.Replace("{6}", payPeriod);

           if (nextLine.Contains("{7}"))
               nextLine = nextLine.Replace("{7}", periodStartDate);

           if (nextLine.Contains("{8}"))
               nextLine = nextLine.Replace("{8}", periodEndDate);

           if (nextLine.Contains("{9}"))
               nextLine = nextLine.Replace("{9}", batchNumber);

           if (nextLine.Contains("{10}"))
               nextLine = nextLine.Replace("{10}", batchFileName);

           if (nextLine.Contains("{11}"))
               nextLine = nextLine.Replace("{11}", batchNumber);

           if (nextLine.Contains("{12}"))
               nextLine = nextLine.Replace("{12}", batchNumber);

           if (nextLine.Contains("{13}"))
               nextLine = nextLine.Replace("{13}", workingDirectoryDynmaicFormat  + batchNumber + "\\");

           sb.Append(nextLine);
         //
         // note:
         //   BufferedReader strips the EOL character
         //   so we add a new one!
         //
         sb.Append(lineSep);
       }
        }
       return sb.ToString();
    }


    private String createPostMacro(String filename) 
    {
        String lineSep = System.Environment.NewLine;
       StringBuilder sb = new StringBuilder();

        using (System.IO.StreamReader stream = new System.IO.StreamReader(filename))
        {

       String nextLine = "";
       while ((nextLine = stream.ReadLine()) != null) 
       {
           if (nextLine.Contains("{0}"))
               nextLine = nextLine.Replace("{0}", userId);

           if (nextLine.Contains("{1}"))
               nextLine = nextLine.Replace("{1}", password);

           if (nextLine.Contains("{2}"))
               nextLine = nextLine.Replace("{2}", companyName);

           if (nextLine.Contains("{3}"))
               nextLine = nextLine.Replace("{3}", workingDirectoryDynmaicFormat + batchNumber + "\\cheque_transaction.txt");

           if (nextLine.Contains("{4}"))
               nextLine = nextLine.Replace("{4}", workingDirectoryDynmaicFormat + batchNumber +  "\\cheque_print.txt");

           if (nextLine.Contains("{5}"))
               nextLine = nextLine.Replace("{5}", workingDirectoryDynmaicFormat + batchNumber +  "\\");


           sb.Append(nextLine);
         //
         // note:
         //   BufferedReader strips the EOL character
         //   so we add a new one!
         //
         sb.Append(lineSep);
       }
        }
       return sb.ToString();
    }

    private String createEmployeeMacro(String filename)
    {
       String lineSep = System.Environment.NewLine;
       StringBuilder sb = new StringBuilder();

        using (System.IO.StreamReader stream = new System.IO.StreamReader(filename))
        {

               String nextLine = "";
               while ((nextLine = stream.ReadLine()) != null) 
               {
                   if (nextLine.Contains("{0}"))
                       nextLine = nextLine.Replace("{0}", userId);

                   if (nextLine.Contains("{1}"))
                       nextLine = nextLine.Replace("{1}", password);

                   if (nextLine.Contains("{2}"))
                       nextLine = nextLine.Replace("{2}", companyName);

                   if (nextLine.Contains("{3}"))
                       nextLine = nextLine.Replace("{3}", employeeCsv);

                   if (nextLine.Contains("{4}"))
                       nextLine = nextLine.Replace("{4}", employeeImportRpt);

                   if (nextLine.Contains("{5}"))
                       nextLine = nextLine.Replace("{5}", employeePayCodeCsv);

                   sb.Append(nextLine);

                   sb.Append(lineSep);
       
               }

        }

        return sb.ToString();

    }

    private void DeleteDirectory(String path)
    {

        if (Directory.Exists(path))
        {
            Directory.Delete(path, true);
        }

    }

    private void saveCalcMacroFile(String macroContent) 
    {
        String fileName = null;
        String fullDirectoryName = workingDirectory  + batchNumber + Path.DirectorySeparatorChar;

        DeleteDirectory(fullDirectoryName);
        Directory.CreateDirectory(fullDirectoryName);

        fileName = fullDirectoryName + batchNumber +".mac";
        
        using (System.IO.StreamWriter stream = new System.IO.StreamWriter(fileName))
        {
            stream.Write(macroContent);
        }
    }


    private void savePostMacroFile(String macroContent)
    {
        String fileName = null;
        String fullDirectoryName = workingDirectory  + batchNumber + "\\"; 

        // delete the file first
        fileName = fullDirectoryName + "post_" + batchNumber +".mac";


        using (System.IO.StreamWriter stream = new System.IO.StreamWriter(fileName))
        {
            stream.Write(macroContent);
        }


    }


    private void saveEmployeeMacroFile(String macroContent)
    {
        String fileName = null;
        String fullDirectoryName = workingDirectory + batchNumber + "\\";  

        DeleteDirectory(fullDirectoryName);
        Directory.CreateDirectory(fullDirectoryName);

        using (System.IO.StreamWriter stream = new System.IO.StreamWriter(fileName))
        {
            stream.Write(macroContent);
        }
    }


    private void runBatch(String batchFileName)
    {
        Process process = new Process();

        try
        {
            process.StartInfo.UseShellExecute = true;
            process.StartInfo.FileName = batchFileName;

            process.Start();
            process.WaitForExit();
        }
        catch
        {
            throw;
        }

    }

    private String createCalcWindowsCommandFile() 
    {
        String windowCmd = _dynamicsExecutable + batchNumber + "\\" + batchNumber +".mac";     

        String fileName = workingDirectory  + batchNumber + "\\run.cmd";

        using (System.IO.StreamWriter stream = new System.IO.StreamWriter(fileName))
        {
            stream.Write(windowCmd);
        }

        return fileName;
    }


    private String createPostWindowsCommandFile() 
    {
        String windowCmd = _dynamicsExecutable +
        workingDirectory  + batchNumber + "\\post_" + batchNumber +".mac";     

        String fileName = workingDirectory  + batchNumber + "\\run.cmd";

        using (System.IO.StreamWriter stream = new System.IO.StreamWriter(fileName))
        {
            stream.Write(windowCmd);
        }

        return fileName;
    }


    private String createEmployeeWindowsCommandFile() 
    {
        String windowCmd = _dynamicsExecutable +
        workingDirectory   + batchNumber +  "\\" + batchNumber + ".mac";     

        String fileName = workingDirectory  + batchNumber + "\\run.cmd";

        using (System.IO.StreamWriter stream = new System.IO.StreamWriter(fileName))
        {
            stream.Write(windowCmd);
        }

        return fileName;
    }

    /**
     * build dynamics macro for batch payroll calculation
     */
    private void runCalculationMacro()
    {
        try
        {
            String str = this.createCalcMacro(templateFileDirectory); //"Z:\\work\\1_Pendylum\\batch_calculation_template.txt");
            this.saveCalcMacroFile(str);
            String runCommand = this.createCalcWindowsCommandFile();
            this.runBatch(runCommand);
            System.Console.Out.WriteLine("Done");
        }
        catch(System.IO.IOException ioe)
        {
            System.Console.Out.WriteLine(ioe.StackTrace);
            System.Console.Out.WriteLine("IOE -> " + ioe);
        }
        catch(Exception e)
        {
            System.Console.Out.WriteLine(e.StackTrace);
            System.Console.Out.WriteLine("E -> " + e);
        }
    }


    /**
     * build dynamics macro for batch payroll calculation
     */
    private void runPostMacro()
    {
        try
        {
            String str = this.createPostMacro(templateFileDirectory ); //"Z:\\work\\1_Pendylum\\batch_calculation_template.txt");
            this.savePostMacroFile(str);
            String runCommand = this.createPostWindowsCommandFile();
            this.runBatch(runCommand);
            System.Console.Out.WriteLine("Done");
        }
        catch(System.IO.IOException ioe)
        {
            System.Console.Out.WriteLine(ioe.StackTrace);
            System.Console.Out.WriteLine("IOE -> " + ioe);
        }
        catch(Exception e)
        {
            System.Console.Out.WriteLine(e.StackTrace);
            System.Console.Out.WriteLine("E -> " + e);
        }
    }


    /**
     * build dynamics macro for batch payroll calculation
     */
    private void runEmployeeMacro()
    {
        try
        {
            String str = this.createEmployeeMacro(templateFileDirectory ); //"Z:\\work\\1_Pendylum\\batch_calculation_template.txt");
            this.saveEmployeeMacroFile(str);
            String runCommand = this.createEmployeeWindowsCommandFile();
            this.runBatch(runCommand);
            System.Console.Out.WriteLine("Done");
        }
        catch(System.IO.IOException ioe)
        {
            System.Console.Out.WriteLine(ioe.StackTrace);
            System.Console.Out.WriteLine("IOE -> " + ioe);
        }
        catch(Exception e)
        {
            System.Console.Out.WriteLine(e.StackTrace);
            System.Console.Out.WriteLine("E -> " + e);
        }
    }

    static int Main(String[] args)
    {
        System.Console.Out.WriteLine("BuildMacro V 0.1 begins...");


        BuildMacro macGen = new BuildMacro();
        batchType    = args[0];


        if (batchType.Equals("calc"))
        {
            if (args.Length != 14)
            {
                System.Console.Out.WriteLine("invalid usage, must have 14 parameters");
                Environment.Exit(-1);
            } 
            userId                        = args[1];
            password                      = args[2];
            companyName                   = args[3];
            batchNumber                   = args[4];
            batchComment                  = args[5];
            jobCode                       = args[6];
            payPeriod                     = args[7];
            periodStartDate               = args[8];
            periodEndDate                 = args[9];
            batchFileName                 = args[10];
            templateFileDirectory         = args[11];
            workingDirectory              = args[12];
            workingDirectoryDynmaicFormat = args[13];


            templateFileDirectory = templateFileDirectory + "\\batch_calculation_template.txt";
            macGen.runCalculationMacro();
        }

        if (batchType.Equals("post"))
        {
            if (args.Length != 8)
            {
                System.Console.Out.WriteLine("invalid usage, must have 8 parameters");
                Environment.Exit(-1);
            }

            userId                        = args[1];
            password                      = args[2];
            companyName                   = args[3];
            batchNumber                   = args[4];
            templateFileDirectory         = args[5];
            workingDirectory              = args[6];
            workingDirectoryDynmaicFormat = args[7];

            templateFileDirectory = templateFileDirectory + "\\posting_template.txt";
            macGen.runPostMacro();
        }

        if (batchType.Equals("employee"))
        {
            if (args.Length != 10)
            {
                System.Console.Out.WriteLine("invalid usage, must have 8 parameters");
                Environment.Exit(-1);
            }

            userId                        = args[1];
            password                      = args[2];
            companyName                   = args[3];
            batchNumber                   = args[4];
            templateFileDirectory         = args[5];
            employeeCsv                   = args[6];
            employeeImportRpt             = args[7];
            employeePayCodeCsv            = args[8]; 
            workingDirectory              = args[9];

            employeeImportRpt = employeeImportRpt + "\\" + batchNumber + "\\Employee_import_rpt.txt";
            templateFileDirectory = templateFileDirectory + "\\employee_template.txt";
            macGen.runEmployeeMacro();
        }

        return 0;

    }
}

}
