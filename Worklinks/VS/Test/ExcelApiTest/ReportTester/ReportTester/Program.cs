﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Xml.Serialization;
using System.Threading;

using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Test;
using WorkLinks.BusinessLayer.BusinessLogic;
using WorkLinks.BusinessLayer.BusinessObjects.Dynamics;

using WLP.BusinessLayer.BusinessObjects;

using NPOI.HSSF.UserModel;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;


namespace ReportTester
{
    class Program
    {
        //args for the report to generate via webservice
        private const String createUser = "reportTestingTool";

        static void Main(string[] args)
        {
            //this runs behind firewall so dont need to go thru web services, go directly to mgmt layer
            ReportManagement _reportMgmt = new ReportManagement();

            //create database user object
            DatabaseUser user = new DatabaseUser()
            {
                DatabaseName = Common.ApplicationParameter.DatabaseName,
                UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name,
                LanguageCode = "EN"
            };

            //collections for reporting testing
            TestReportCollection testReportCollection = new TestReportCollection();
            TestReportParameterCollection testReportParameterCollection = null;
            TestReportExpectedValueCollection testReportExpectedValueCollection = null;

            byte[] file = null;

            //declare report parameters
            String reportName = null;
            String companyCode = null;
            String year = null;
            String reportType = null;
            String employeeNumber = null;
            String languageCode = null;
            String employerNumber = null;
            String provinceCode = null;
            String reportShowDescriptionField = null;
            String reportPayRegisterSortOrder = null;
            String revisionNumber = "0";
            long payrollProcessId = -1;

            //get reports from db
            testReportCollection = _reportMgmt.GetTestReports(user);

            if (testReportCollection != null && testReportCollection.Count > 0)
            {
                //for each report we have, run a test
                foreach (TestReport report in testReportCollection)
                {
                    //get report parameters
                    testReportParameterCollection = _reportMgmt.GetTestReportParameters(user, report.TestReportId);

                    //get report expected values
                    testReportExpectedValueCollection = _reportMgmt.GetTestReportExpectedValues(user, report.TestReportId);

                    //assign variables to call the report
                    AssignReportParameters(report, testReportParameterCollection, ref reportName, ref reportType, ref companyCode, ref year, ref payrollProcessId, ref employeeNumber, ref languageCode, ref employerNumber, ref provinceCode, ref reportShowDescriptionField, ref reportPayRegisterSortOrder);

                    Console.WriteLine("Grabbing the " + report.TestName.ToUpper() + " report...");

                    //generate report
                    file = _reportMgmt.GetReport(user, reportName, payrollProcessId, employeeNumber, reportType, year, employerNumber, provinceCode, reportShowDescriptionField, revisionNumber, reportPayRegisterSortOrder);
                    Console.WriteLine("Complete, now testing report values...");

                    //store report in memory
                    MemoryStream stream = new MemoryStream(file);

                    //get the excel sheet from memory
                    HSSFWorkbook wb = new HSSFWorkbook(new POIFSFileSystem(stream), true);
                    ISheet sheet = wb.GetSheetAt(0);

                    //check against expected values
                    if (CheckExpectedValues(testReportExpectedValueCollection, reportName, sheet))
                        Console.WriteLine("Tests have passed for " + report.TestName.ToUpper() + "\n");
                    else
                        Console.WriteLine("Tests have failed " + report.TestName.ToUpper() + "\n");
                }
            }

            //sleep 10 seconds so we can see the output
            Thread.Sleep(10000);
        }

        public static void AssignReportParameters(TestReport testReport, TestReportParameterCollection testReportParameterCollection, ref String reportName, ref String reportType, ref String companyCode,
            ref String year, ref long payrollProcessId, ref String employeeNumber, ref String languageCode, ref String employerNumber, ref String provinceCode, ref String reportShowDescriptionField, ref String reportPayRegisterSortOrder)
        {
            //assign values to the variables needed to generate the report
            reportName = testReport.ReportName;
            reportType = testReport.ReportType;

            foreach (TestReportParameter reportParm in testReportParameterCollection)
            {
                switch (reportParm.ParameterName.ToLower())
                {
                    case "companycode":
                        companyCode = reportParm.Value;
                        break;
                    case "year":
                        year = reportParm.Value;
                        break;
                    case "payrollprocessid":
                        payrollProcessId = Convert.ToInt64(reportParm.Value);
                        break;
                    case "employeenumber":
                        employeeNumber = reportParm.Value;
                        break;
                    case "languagecode":
                        languageCode = reportParm.Value;
                        break;
                    case "provincecode":
                        provinceCode = reportParm.Value;
                        break;
                    case "reportshowdescriptionfield":
                        reportShowDescriptionField = reportParm.Value;
                        break;
                    case "reportpayregistersortorder":
                        reportPayRegisterSortOrder = reportParm.Value;
                        break;
                    default:
                        break;
                }
            }
        }

        private static bool CheckExpectedValues(TestReportExpectedValueCollection testReportExpectedValues, String reportName, ISheet report)
        {
            bool valid = true;

            foreach (TestReportExpectedValue testReportExpectedValue in testReportExpectedValues)
            {
                try
                {
                    if (!valid) break;

                    switch (testReportExpectedValue.Datatype.ToLower())
                    {
                        case "decimal":
                            if (Convert.ToDecimal(testReportExpectedValue.ExpectedValue) != Convert.ToDecimal(GetCellValue(report.GetRow(testReportExpectedValue.RowNumber).GetCell(testReportExpectedValue.ColumnNumber))))
                            {
                                valid = false;
                                Console.WriteLine(String.Format("Issue with report {0}, block {1}", reportName, convertToExcelBlock(testReportExpectedValue.RowNumber, testReportExpectedValue.ColumnNumber)));
                            }
                            break;
                        case "string":
                            if (testReportExpectedValue.ExpectedValue != Convert.ToString(GetCellValue(report.GetRow(testReportExpectedValue.RowNumber).GetCell(testReportExpectedValue.ColumnNumber))).Trim())
                            {
                                valid = false;
                                Console.WriteLine(String.Format("Issue with report {0}, block {1}", reportName, convertToExcelBlock(testReportExpectedValue.RowNumber, testReportExpectedValue.ColumnNumber)));
                            }
                            break;
                        case "bool":
                            if (Convert.ToBoolean(testReportExpectedValue.ExpectedValue) != Convert.ToBoolean(GetCellValue(report.GetRow(testReportExpectedValue.RowNumber).GetCell(testReportExpectedValue.ColumnNumber))))
                            {
                                valid = false;
                                Console.WriteLine(String.Format("Issue with report {0}, block {1}", reportName, convertToExcelBlock(testReportExpectedValue.RowNumber, testReportExpectedValue.ColumnNumber)));
                            }
                            break;
                        default:
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(String.Format("An exception occured trying to grab data from the excel report\n*****Check cell {0} for bad data", convertToExcelBlock(testReportExpectedValue.RowNumber, testReportExpectedValue.ColumnNumber)));
                    return false;
                }
            }

            return valid;
        }

        private static String convertToExcelBlock(int row, int col)
        {
            //rows and columns are 0 indexed.  Increase the row by 1 for reference on the report.  Leave column alone as it's used in the charcter position calcuation
            row++;

            //get ansi character based on number
            return Convert.ToString((Char)(65 + col)) + row.ToString();
        }

        private static Object GetCellValue(ICell cell)
        {
            if (cell == null)
                return null;
            else if (cell.CellType.Equals(CellType.BOOLEAN))
                return cell.BooleanCellValue;
            else if (cell.CellType.Equals(CellType.FORMULA))
                return cell.CellFormula;
            else if (cell.CellType.Equals(CellType.NUMERIC))
            {
                if (DateUtil.IsCellDateFormatted(cell))
                    return cell.DateCellValue.ToShortDateString().ToString();
                else
                    return cell.NumericCellValue;
            }
            else if (cell.CellType.Equals(CellType.STRING))
                return cell.StringCellValue;
            else
                return null;
        }
    }
}
