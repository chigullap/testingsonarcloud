﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace ReportTester.Common
{
    public static class ApplicationParameter
    {
        public static String DatabaseName
        {
            get { return ConfigurationManager.AppSettings["DatabaseName"]; }
        }
    }
}
