﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WorkLinks.BusinessLayer.BusinessObjects;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Web;
using Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using Saxon.Api;
using WorkLinks.DataLayer.DataAccess;
using WorkLinks.BusinessLayer.BusinessObjects.Import;

namespace PaySlipReportBatchCommand
{
    class Program
    {

        //static void Main(String[] args)
        //{

        //    Processor processor = new Processor();
        //    XsltCompiler compiler = processor.NewXsltCompiler();

        //    //convert import file data to stream
        //    FileStream xslStream = new FileStream(@"C:\dev\src\WorkLinks\temp\MappingMapToBulkRoeHeader_Basic.txt", FileMode.Open);
        //    //build transformer
        //    XsltExecutable xslExec = compiler.Compile(xslStream);


        //}


        

        static void Main(string[] args)
        {
            const int employeeColumnIndex = 8;
            const int employeeTerminatedColumnIndex = 11;
            const int employeeStartRowIndex = 10;

            const int paycodeIdentifier1StartColumnIndex = 12;
            const int paycodeIdentifier1RowIndex = 7;
            const int paycodeIdentifier2RowIndex = 9;
            const int paycodeIdentifier2NumberOfColumn = 7;

            Application application = new Application();
            Workbook wb = null;
            try
            {
                XmlAccess _xmlAccess = new XmlAccess();
                ExternalPaycodeImportMapCollection externalPaycodeMapCollection = _xmlAccess.GetExternalPaycodeImportMap("SunLifeBenefit");

                wb = application.Workbooks.Open(@"c:\temp\Dynamic_Payroll_report_sample.xls", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                Worksheet sheet = wb.Worksheets[1];

                int maxRowCount = sheet.Rows.Count;
                int maxColumnCount = sheet.Columns.Count;

                int importIndex = 0;

                PaycodeImportCollection importedPaycodes = new PaycodeImportCollection();


                //loop to get rows (assume starts at row 10)
                for (long rowIndex = employeeStartRowIndex; rowIndex < maxRowCount; rowIndex++)
                {
                    //get employee number
                    String employeeIdentifier = Convert.ToString(sheet.Cells[rowIndex, employeeColumnIndex].value);
                    if (employeeIdentifier == null || employeeIdentifier.Trim().Equals(String.Empty))
                        break;

                    //get employee terminated
                    String employeeTerminatedValue = Convert.ToString(sheet.Cells[rowIndex, employeeTerminatedColumnIndex].value);
                    bool employeeIsTerminated = !(employeeTerminatedValue == null || employeeTerminatedValue.Trim().Equals(String.Empty));


                    String paycodeIdentifier1 = null;
                    String paycodeIdentifier2 = null;
                    Decimal paycodeValue = 0;
                    //get paycode info 
                    for (int columnIndex = paycodeIdentifier1StartColumnIndex; columnIndex < maxColumnCount; columnIndex++)
                    {
                        //get paycode identifier #1
                        if ((columnIndex - paycodeIdentifier1StartColumnIndex) % paycodeIdentifier2NumberOfColumn == 0)
                        {
                            paycodeIdentifier1 = sheet.Cells[paycodeIdentifier1RowIndex, columnIndex].value;
                            //if no paycode identifier, we are done, exit loop
                            if (paycodeIdentifier1 == null || paycodeIdentifier1.Trim().Equals(String.Empty))
                                break;
                        }
                        //get paycode identifer #2
                        paycodeIdentifier2 = sheet.Cells[paycodeIdentifier2RowIndex, columnIndex].value;
                        paycodeValue = Convert.ToDecimal(sheet.Cells[employeeStartRowIndex, columnIndex].value);

                        PaycodeImport import = new PaycodeImport();
                        import.PaycodeImportId = importIndex--;
                        import.EmployeeIdentifier = employeeIdentifier;
                        import.AmountRate = paycodeValue;
                        import.Units = 1;
                        import.ImportType = employeeIsTerminated ? PaycodeImport.PaycodeImportType.Batch : PaycodeImport.PaycodeImportType.EmployeePaycode;
                        import.ExternalPaycodeImportMap = externalPaycodeMapCollection.GetExternalPaycodeImportMap(paycodeIdentifier1, paycodeIdentifier2);

                        if (import.ExternalPaycodeImportMap == null)
                        {
                            throw new Exception(String.Format("Missing paycode mapping for {0} - {1}", paycodeIdentifier1,paycodeIdentifier2));
                        }

                        importedPaycodes.Add(import);
                    }

                }

                String employeePaycodes = importedPaycodes.EmployeePaycodeCSV;
                String payrollBatch = importedPaycodes.PayrollBatchCSV;


                //generate 

            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {

                wb.Close();
                Marshal.ReleaseComObject(wb);
            }




        }

    //    //vars
    //    private static WorkLinks.BusinessLayer.BusinessLogic.ReportManagement _reportMgmt = null;

    //    //properties
    //    private static WorkLinks.BusinessLayer.BusinessLogic.ReportManagement ReportMgmt
    //    {
    //        get
    //        {
    //            if (_reportMgmt == null)
    //                _reportMgmt = new WorkLinks.BusinessLayer.BusinessLogic.ReportManagement();
    //            return _reportMgmt;
    //        }
    //    }

    //    static void Main(string[] args)
    //    {
    //        //if (args.Length == 1) //stacy uncomment IF structure when done testing
    //        //{
    //        //long payrollProcessId = Convert.ToInt64(args[0].ToString());
    //        long payrollProcessId = 10040; //stacy remove this when done testing and uncomment line above

    //        Dictionary<String, byte[]> reports = null;

    //        //get reports for the employees in this payroll process
    //        reports = null;// ReportMgmt.GetReportForAllEmployeesOfThisPayProcess(payrollProcessId);

    //        ZipArchive zipFile = CreateZip(reports);
    //        PushToBrowser(zipFile);
    //        //}
    //        //else if (args.Length == 0)
    //        //{
    //        //    Console.WriteLine("PaySlipReportBatchCommand requires 1 argument [payroll process id]");
    //        //}
    //        //else
    //        //{
    //        //    Console.WriteLine("Too many arguments supplied.  PaySlipReportBatchCommand requires 1 argument [payroll process id]");
    //        //}
    //    }

    //    private static void PushToBrowser(ZipArchive zip)
    //    {
           
    //        //attempt 4 --no good cause it uses a String for path
    //        //HttpResponse response = HttpContext.Current.Response; //"Current" is null so we blow up
    //        //response.ClearContent();
    //        //response.Clear();
    //        //response.ContentType = "application/octet-stream";
    //        //response.AddHeader("Content-Disposition", "attachment; filename=" + "blah blah" + ";");
    //        ////response.TransmitFile(FilePath);


    //        ////attempt 5
    //        //byte[] fin = new byte[5];
            
            
    //        //response.OutputStream.Write(fin, 0, fin.Length);
    //        //response.Flush();
    //        //response.End();

           
           

    //        //byte[] eftTextFile = File.ReadAllBytes(fileNameAndPath);

    //        //if (eftTextFile != null)
    //        //{
    //        //    //this will prompt the user to open or download the file (which is also written to the EFT directory)
    //        //    Response.ContentType = "application/octet-stream";
    //        //    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", fileNameAndPath));
    //        //    Response.AddHeader("Content-Length", eftTextFile.Length.ToString());

    //        //    Response.OutputStream.Write(zip, 0, eftTextFile.Length);
    //        //}
            
    //    }

    //    private static ZipArchive CreateZip(Dictionary<String, byte[]> reports)
    //    {
    //        MemoryStream stream = new MemoryStream();
    //        //FileStream stream = File.Create("C:\\dev\\temp\\stacy.zip"); //this would write to a file on disk rather than a memory stream.  then we would use VOID for the function
    //        ZipArchive archive = new ZipArchive(stream, ZipArchiveMode.Create);

    //        using (archive)
    //        {
    //            int i = 1;
    //            ZipArchiveEntry entry = null;

    //            foreach (KeyValuePair<String,byte[]> report in reports)
    //            {
    //                entry = archive.CreateEntry("report" + i.ToString() + ".pdf");
    //                i++;

    //                using (BinaryWriter bWriter = new BinaryWriter(entry.Open()))
    //                {
    //                    bWriter.Write(report.Value);
    //                }
    //            }
    //        }
    //        stream.Close();
    //        stream.Dispose();
            
    //        return archive;
    //    }
    }
}
