﻿
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;
namespace WorkLinks.DataLayer.DataAccess.Central
{
    public class ImportExportAccess
    {
        #region fields
        private SqlServer.SqlServerHrxmlDatabaseMap _clientHrxmlDatabaseMap;
        private SqlServer.SqlServerRemoteImportImportExportLog _clientRemoteImportImportExportLog;
        #endregion 

        #region properties
        private SqlServer.SqlServerHrxmlDatabaseMap ClientHrxmlDatabaseMap
        {
            get
            {
                if (_clientHrxmlDatabaseMap == null)
                    _clientHrxmlDatabaseMap = new SqlServer.SqlServerHrxmlDatabaseMap();
                return _clientHrxmlDatabaseMap;
            }
        }
        private SqlServer.SqlServerRemoteImportImportExportLog ClientRemoteImportImportExportLog
        {
            get
            {
                if (_clientRemoteImportImportExportLog == null)
                    _clientRemoteImportImportExportLog = new SqlServer.SqlServerRemoteImportImportExportLog();
                return _clientRemoteImportImportExportLog;
            }
        }
        #endregion

        #region hrxml

        public HrxmlDatabaseMapCollection GetHrxmlDatabaseMap(DatabaseUser user)
        {
            return ClientHrxmlDatabaseMap.Select(user);
        }
        #endregion

        public void InsertRemoteImportExportLog(RemoteImportExportLog item)
        {
            ClientRemoteImportImportExportLog.Insert(item);
        }

    }
}
