﻿using System;
using System.Data;

using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;


namespace WorkLinks.DataLayer.DataAccess.Central.SqlServer
{
    internal class SqlServerAutoCalculatePayrollDatabaseMap : SqlServerBase
    {
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String AutoCalculatePayrollDatabaseMapId = "auto_calculate_payroll_database_map_id";
            public static String LogicalIdPrefix = "logical_id_prefix";
            public static String DatabaseName = "database_name";
        }

        #region main

        internal AutoCalculatePayrollDatabaseMapCollection Select(DatabaseUser user)
        {
            using (DataBaseCommand command = GetStoredProcCommand("AutoCalculatePayrollDatabaseMap_select", user.DatabaseName))
            {
                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToAutoCalculatePayrollDatabaseMapCollection(reader);
                }
            }
        }

        #endregion
        
        #region data mapping
        protected AutoCalculatePayrollDatabaseMapCollection MapToAutoCalculatePayrollDatabaseMapCollection(IDataReader reader)
        {
            AutoCalculatePayrollDatabaseMapCollection collection = new AutoCalculatePayrollDatabaseMapCollection();
            while (reader.Read())
            {
                collection.Add(MapToAutoCalculatePayrollDatabaseMap(reader));
            }

            return collection;
        }
        protected AutoCalculatePayrollDatabaseMap MapToAutoCalculatePayrollDatabaseMap(IDataReader reader)
        {
            AutoCalculatePayrollDatabaseMap item = new AutoCalculatePayrollDatabaseMap();
            base.MapToBase(item, reader);

            item.AutoCalculatePayrollDatabaseMapId = (long)CleanDataValue(reader[ColumnNames.AutoCalculatePayrollDatabaseMapId]);
            item.LogicalIdPrefix = (String)CleanDataValue(reader[ColumnNames.LogicalIdPrefix]);
            item.DatabaseName = (String)CleanDataValue(reader[ColumnNames.DatabaseName]);

            return item;
        }
        #endregion
    }
}
