﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects.Central;

namespace WorkLinks.DataLayer.DataAccess.Central.SqlServer
{
    internal class SqlServerRemoteImportImportExportLog : SqlServerBase
    {
        #region main

        public void Insert(RemoteImportExportLog item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("RemoteImportExportLog_insert"))
            {
                command.BeginTransaction("RemoteImportExportLog_insert");

                SqlParameter PayrollPeriodIdParm = command.AddParameterWithValue("@remoteImportExportLogId", item.RemoteImportExportLogId, ParameterDirection.Output);

                command.AddParameterWithValue("@importFlag", item.ImportFlag);
                command.AddParameterWithValue("@importExportType", item.ImportExportType);
                command.AddParameterWithValue("@identifier", item.Identifier);
                command.AddParameterWithValue("@filePath", item.FilePath);
                command.AddParameterWithValue("@remoteServer", item.RemoteServer);
                command.AddParameterWithValue("@successFlag", item.SuccessFlag);
                command.AddParameterWithValue("@processingOutput", item.ProcessingOutput);


                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", item.UpdateUser);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                item.RemoteImportExportLogId = Convert.ToInt64(PayrollPeriodIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();
            }
        }

        #endregion
    }
}

