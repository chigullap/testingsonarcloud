﻿using System;
using System.Data;

using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;


namespace WorkLinks.DataLayer.DataAccess.Central.SqlServer
{
    internal class SqlServerCraRemittanceDatabaseMap : SqlServerBase
    {
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String CraRemittanceDatabaseMapId = "cra_remittance_database_map_id";
            public static String LogicalIdPrefix = "logical_id_prefix";
            public static String DatabaseName = "database_name";
        }

        #region main

        internal CraRemittanceDatabaseMapCollection Select(DatabaseUser user)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CraRemittanceDatabaseMap_select", user.DatabaseName))
            {
                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToCraRemittanceDatabaseMapCollection(reader);
                }
            }
        }

        #endregion


        #region data mapping
        protected CraRemittanceDatabaseMapCollection MapToCraRemittanceDatabaseMapCollection(IDataReader reader)
        {
            CraRemittanceDatabaseMapCollection collection = new CraRemittanceDatabaseMapCollection();
            while (reader.Read())
            {
                collection.Add(MapToCraRemittanceDatabaseMap(reader));
            }

            return collection;
        }
        protected CraRemittanceDatabaseMap MapToCraRemittanceDatabaseMap(IDataReader reader)
        {
            CraRemittanceDatabaseMap item = new CraRemittanceDatabaseMap();
            base.MapToBase(item, reader);

            item.CraRemittanceDatabaseMapId = (long)CleanDataValue(reader[ColumnNames.CraRemittanceDatabaseMapId]);
            item.LogicalIdPrefix = (String)CleanDataValue(reader[ColumnNames.LogicalIdPrefix]);
            item.DatabaseName = (String)CleanDataValue(reader[ColumnNames.DatabaseName]);

            return item;
        }
        #endregion
    }
}
