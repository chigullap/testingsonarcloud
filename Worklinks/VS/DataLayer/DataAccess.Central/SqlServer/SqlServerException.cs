﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.DataLayer.DataAccess.Central.SqlServer
{
    /// <summary>
    /// class used for WorkLinks sql exception handling
    /// </summary>
    [Serializable]
    [KnownType(typeof(DataAccessException))]
    public class SqlServerException : Exception
    {
        public enum ExceptionCodes
        {
            DataConcurrency,
            ForeignKeyConstraint,
            Other,
        }

        #region fields
        ExceptionCodes _exceptionCode = 0;
        #endregion

        #region properties
        public ExceptionCodes ExceptionCode
        {
            get { return _exceptionCode; }
            set { _exceptionCode = value; }
        }
        #endregion

        public SqlServerException(String message, ExceptionCodes code, Exception exc)
            : base(message, exc)
        {
            ExceptionCode = code;
        }
    }

}
