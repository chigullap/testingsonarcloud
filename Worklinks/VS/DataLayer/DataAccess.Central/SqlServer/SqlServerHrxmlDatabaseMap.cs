﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;


namespace WorkLinks.DataLayer.DataAccess.Central.SqlServer
{
    internal class SqlServerHrxmlDatabaseMap : SqlServerBase
    {
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String Code = "cd";


            public static String HrxmlDatabaseMapId = "hrxml_database_map_id";
            public static String LogicalIdPrefix = "logical_id_prefix";
            public static String ReferenceIdNumber = "reference_id_number";
            public static String DatabaseName = "database_name";

        }


        #region main

        internal HrxmlDatabaseMapCollection Select(DatabaseUser user)
        {
            using (DataBaseCommand command = GetStoredProcCommand("HrxmlDatabaseMap_select",user.DatabaseName))
            {
                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToHrxmlDatabaseMapCollection(reader);
                }
            }
        }


        #endregion


        #region data mapping
        protected HrxmlDatabaseMapCollection MapToHrxmlDatabaseMapCollection(IDataReader reader)
        {
            HrxmlDatabaseMapCollection collection = new HrxmlDatabaseMapCollection();
            while (reader.Read())
            {
                collection.Add(MapToHrxmlDatabaseMap(reader));
            }

            return collection;
        }
        protected HrxmlDatabaseMap MapToHrxmlDatabaseMap(IDataReader reader)
        {
            HrxmlDatabaseMap item = new HrxmlDatabaseMap();
            base.MapToBase(item, reader);

            item.HrxmlDatabaseMapId = (long)CleanDataValue(reader[ColumnNames.HrxmlDatabaseMapId]);
            item.LogicalIdPrefix = (String)CleanDataValue(reader[ColumnNames.LogicalIdPrefix]);
            item.ReferenceIdNumber = (String)CleanDataValue(reader[ColumnNames.ReferenceIdNumber]);
            item.DatabaseName = (String)CleanDataValue(reader[ColumnNames.DatabaseName]);

            return item;
        }
        #endregion
    }
}
