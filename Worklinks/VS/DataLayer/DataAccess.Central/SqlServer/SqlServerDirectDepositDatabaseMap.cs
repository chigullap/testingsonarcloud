﻿using System;
using System.Data;

using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;


namespace WorkLinks.DataLayer.DataAccess.Central.SqlServer
{
    internal class SqlServerDirectDepositDatabaseMap : SqlServerBase
    {
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String DirectDepositDatabaseMapId = "direct_deposit_database_map_id";
            public static String LogicalIdPrefix = "logical_id_prefix";
            public static String DatabaseName = "database_name";
        }

        #region main

        internal DirectDepositDatabaseMapCollection Select(DatabaseUser user)
        {
            using (DataBaseCommand command = GetStoredProcCommand("DirectDepositDatabaseMap_select", user.DatabaseName))
            {
                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToDirectDepositDatabaseMapCollection(reader);
                }
            }
        }

        #endregion


        #region data mapping
        protected DirectDepositDatabaseMapCollection MapToDirectDepositDatabaseMapCollection(IDataReader reader)
        {
            DirectDepositDatabaseMapCollection collection = new DirectDepositDatabaseMapCollection();
            while (reader.Read())
            {
                collection.Add(MapToDirectDepositDatabaseMap(reader));
            }

            return collection;
        }
        protected DirectDepositDatabaseMap MapToDirectDepositDatabaseMap(IDataReader reader)
        {
            DirectDepositDatabaseMap item = new DirectDepositDatabaseMap();
            base.MapToBase(item, reader);

            item.DirectDepositDatabaseMapId = (long)CleanDataValue(reader[ColumnNames.DirectDepositDatabaseMapId]);
            item.LogicalIdPrefix = (String)CleanDataValue(reader[ColumnNames.LogicalIdPrefix]);
            item.DatabaseName = (String)CleanDataValue(reader[ColumnNames.DatabaseName]);

            return item;
        }
        #endregion
    }
}
