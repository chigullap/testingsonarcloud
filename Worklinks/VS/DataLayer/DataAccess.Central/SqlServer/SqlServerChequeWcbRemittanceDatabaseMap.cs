﻿using System;
using System.Data;

using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;


namespace WorkLinks.DataLayer.DataAccess.Central.SqlServer
{
    internal class SqlServerChequeWcbRemittanceDatabaseMap : SqlServerBase
    {
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ChequeWcbRemittanceDatabaseMapId = "cheque_wcb_remittance_database_map_id";
            public static String LogicalIdPrefix = "logical_id_prefix";
            public static String DatabaseName = "database_name";
        }

        #region main

        internal ChequeWcbRemittanceDatabaseMapCollection Select(DatabaseUser user)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ChequeWcbRemittanceDatabaseMap_select", user.DatabaseName))
            {
                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToChequeWcbRemittanceDatabaseMapCollection(reader);
                }
            }
        }
        #endregion

        #region data mapping
        protected ChequeWcbRemittanceDatabaseMapCollection MapToChequeWcbRemittanceDatabaseMapCollection(IDataReader reader)
        {
            ChequeWcbRemittanceDatabaseMapCollection collection = new ChequeWcbRemittanceDatabaseMapCollection();
            while (reader.Read())
            {
                collection.Add(MapToChequeWcbRemittanceDatabaseMap(reader));
            }

            return collection;
        }
        protected ChequeWcbRemittanceDatabaseMap MapToChequeWcbRemittanceDatabaseMap(IDataReader reader)
        {
            ChequeWcbRemittanceDatabaseMap item = new ChequeWcbRemittanceDatabaseMap();
            base.MapToBase(item, reader);

            item.ChequeWcbRemittanceDatabaseMapId = (long)CleanDataValue(reader[ColumnNames.ChequeWcbRemittanceDatabaseMapId]);
            item.LogicalIdPrefix = (String)CleanDataValue(reader[ColumnNames.LogicalIdPrefix]);
            item.DatabaseName = (String)CleanDataValue(reader[ColumnNames.DatabaseName]);

            return item;
        }
        #endregion
    }
}
