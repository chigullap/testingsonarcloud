﻿using System;
using System.Data;

using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;


namespace WorkLinks.DataLayer.DataAccess.Central.SqlServer
{
    internal class SqlServerChequeHealthTaxRemittanceDatabaseMap : SqlServerBase
    {
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ChequeHealthTaxRemittanceDatabaseMapId = "cheque_health_tax_remittance_database_map_id";
            public static String LogicalIdPrefix = "logical_id_prefix";
            public static String DatabaseName = "database_name";
        }

        #region main

        internal ChequeHealthTaxRemittanceDatabaseMapCollection Select(DatabaseUser user)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ChequeHealthTaxRemittanceDatabaseMap_select", user.DatabaseName))
            {
                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToChequeHealthTaxRemittanceDatabaseMapCollection(reader);
                }
            }
        }
        #endregion

        #region data mapping
        protected ChequeHealthTaxRemittanceDatabaseMapCollection MapToChequeHealthTaxRemittanceDatabaseMapCollection(IDataReader reader)
        {
            ChequeHealthTaxRemittanceDatabaseMapCollection collection = new ChequeHealthTaxRemittanceDatabaseMapCollection();
            while (reader.Read())
            {
                collection.Add(MapToChequeHealthTaxRemittanceDatabaseMap(reader));
            }

            return collection;
        }
        protected ChequeHealthTaxRemittanceDatabaseMap MapToChequeHealthTaxRemittanceDatabaseMap(IDataReader reader)
        {
            ChequeHealthTaxRemittanceDatabaseMap item = new ChequeHealthTaxRemittanceDatabaseMap();
            base.MapToBase(item, reader);

            item.ChequeHealthTaxRemittanceDatabaseMapId = (long)CleanDataValue(reader[ColumnNames.ChequeHealthTaxRemittanceDatabaseMapId]);
            item.LogicalIdPrefix = (String)CleanDataValue(reader[ColumnNames.LogicalIdPrefix]);
            item.DatabaseName = (String)CleanDataValue(reader[ColumnNames.DatabaseName]);

            return item;
        }
        #endregion
    }
}
