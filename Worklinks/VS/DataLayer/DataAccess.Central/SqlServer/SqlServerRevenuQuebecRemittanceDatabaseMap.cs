﻿using System;
using System.Data;

using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;


namespace WorkLinks.DataLayer.DataAccess.Central.SqlServer
{
    internal class SqlServerRevenuQuebecRemittanceDatabaseMap : SqlServerBase
    {
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String RevenuQuebecRemittanceDatabaseMapId = "revenu_quebec_remittance_database_map_id";
            public static String LogicalIdPrefix = "logical_id_prefix";
            public static String DatabaseName = "database_name";
        }

        #region main

        internal RevenuQuebecRemittanceDatabaseMapCollection Select(DatabaseUser user)
        {
            using (DataBaseCommand command = GetStoredProcCommand("RevenuQuebecRemittanceDatabaseMap_select", user.DatabaseName))
            {
                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToRevenuQuebecRemittanceDatabaseMapCollection(reader);
                }
            }
        }

        #endregion


        #region data mapping
        protected RevenuQuebecRemittanceDatabaseMapCollection MapToRevenuQuebecRemittanceDatabaseMapCollection(IDataReader reader)
        {
            RevenuQuebecRemittanceDatabaseMapCollection collection = new RevenuQuebecRemittanceDatabaseMapCollection();
            while (reader.Read())
            {
                collection.Add(MapToRevenuQuebecRemittanceDatabaseMap(reader));
            }

            return collection;
        }
        protected RevenuQuebecRemittanceDatabaseMap MapToRevenuQuebecRemittanceDatabaseMap(IDataReader reader)
        {
            RevenuQuebecRemittanceDatabaseMap item = new RevenuQuebecRemittanceDatabaseMap();
            base.MapToBase(item, reader);

            item.RevenuQuebecRemittanceDatabaseMapId = (long)CleanDataValue(reader[ColumnNames.RevenuQuebecRemittanceDatabaseMapId]);
            item.LogicalIdPrefix = (String)CleanDataValue(reader[ColumnNames.LogicalIdPrefix]);
            item.DatabaseName = (String)CleanDataValue(reader[ColumnNames.DatabaseName]);

            return item;
        }
        #endregion
    }
}
