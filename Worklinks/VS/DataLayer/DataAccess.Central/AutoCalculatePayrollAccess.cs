﻿using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;

namespace WorkLinks.DataLayer.DataAccess.Central
{
    public class AutoCalculatePayrollAccess
    {
        #region fields

        private SqlServer.SqlServerAutoCalculatePayrollDatabaseMap _sqlServerAutoCalculatePayrollDatabaseMap;

        #endregion 

        #region properties
        private SqlServer.SqlServerAutoCalculatePayrollDatabaseMap SqlServerAutoCalculatePayrollDatabaseMap
        {
            get
            {
                if (_sqlServerAutoCalculatePayrollDatabaseMap == null)
                    _sqlServerAutoCalculatePayrollDatabaseMap = new SqlServer.SqlServerAutoCalculatePayrollDatabaseMap();
                return _sqlServerAutoCalculatePayrollDatabaseMap;
            }
        }

        #endregion

        #region methods

        public AutoCalculatePayrollDatabaseMapCollection GetAutoCalculatePayrollDatabaseMapCollection(DatabaseUser user)
        {
            return SqlServerAutoCalculatePayrollDatabaseMap.Select(user);
        }
        #endregion


    }
}
