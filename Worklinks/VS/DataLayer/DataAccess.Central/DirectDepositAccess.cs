﻿using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;

namespace WorkLinks.DataLayer.DataAccess.Central
{
    public class DirectDepositAccess
    {
        #region fields

        private SqlServer.SqlServerDirectDepositDatabaseMap _sqlServerDirectDepositDatabaseMap;

        #endregion 

        #region properties
        private SqlServer.SqlServerDirectDepositDatabaseMap SqlServerDirectDepositDatabaseMap
        {
            get
            {
                if (_sqlServerDirectDepositDatabaseMap == null)
                    _sqlServerDirectDepositDatabaseMap = new SqlServer.SqlServerDirectDepositDatabaseMap();
                return _sqlServerDirectDepositDatabaseMap;
            }
        }

        #endregion

        #region methods

        public DirectDepositDatabaseMapCollection GetDirectDepositDatabaseMapCollection(DatabaseUser user)
        {
            return SqlServerDirectDepositDatabaseMap.Select(user);
        }
        #endregion


    }
}
