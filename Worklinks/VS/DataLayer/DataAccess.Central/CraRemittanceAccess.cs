﻿using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;
namespace WorkLinks.DataLayer.DataAccess.Central
{
    public class CraRemittanceAccess
    {
        #region fields

        private SqlServer.SqlServerCraRemittanceDatabaseMap _sqlServerCraRemittanceDatabaseMap;

        #endregion 

        #region properties
        private SqlServer.SqlServerCraRemittanceDatabaseMap SqlServerCraRemittanceDatabaseMap
        {
            get
            {
                if (_sqlServerCraRemittanceDatabaseMap == null)
                    _sqlServerCraRemittanceDatabaseMap = new SqlServer.SqlServerCraRemittanceDatabaseMap();
                return _sqlServerCraRemittanceDatabaseMap;
            }
        }
      
        #endregion

        #region methods

        public CraRemittanceDatabaseMapCollection GetCraRemittanceDatabaseMap(DatabaseUser user)
        {
            return SqlServerCraRemittanceDatabaseMap.Select(user);
        }
        #endregion


    }
}
