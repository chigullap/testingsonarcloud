﻿using System;
using System.Runtime.Serialization;
using WorkLinks.DataLayer.DataAccess.Central.SqlServer;

namespace WorkLinks.DataLayer.DataAccess.Central
{
    [Serializable]
     public class DataAccessException : SqlServerException
    {
        /// <summary>
        /// class used for WorkLinks DataAccess exception handling
        /// </summary>

        new public enum ExceptionCodes
        {
            DataConcurrency,
            ForeignKeyConstraint,
            Other,
        }

        #region fields
        ExceptionCodes _exceptionCode = 0;
        #endregion

        #region properties
        new public ExceptionCodes ExceptionCode
        {
            get { return _exceptionCode; }
            set { _exceptionCode = value; }
        }
        #endregion

        public DataAccessException(String message, ExceptionCodes code, Exception exc)
            : base(message, (SqlServerException.ExceptionCodes)code, exc)
        {
            ExceptionCode = code;
        }

    }
}
