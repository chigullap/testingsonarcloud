﻿using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;

namespace WorkLinks.DataLayer.DataAccess.Central
{
    public class RevenuQuebecRemittanceAccess
    {
        #region fields

        private SqlServer.SqlServerRevenuQuebecRemittanceDatabaseMap _sqlServerRevenuQuebecRemittanceDatabaseMap;

        #endregion 

        #region properties
        private SqlServer.SqlServerRevenuQuebecRemittanceDatabaseMap SqlServerRevenuQuebecRemittanceDatabaseMap
        {
            get
            {
                if (_sqlServerRevenuQuebecRemittanceDatabaseMap == null)
                    _sqlServerRevenuQuebecRemittanceDatabaseMap = new SqlServer.SqlServerRevenuQuebecRemittanceDatabaseMap();
                return _sqlServerRevenuQuebecRemittanceDatabaseMap;
            }
        }

        #endregion

        #region methods

        public RevenuQuebecRemittanceDatabaseMapCollection GetRevenuQuebecRemittanceDatabaseMap(DatabaseUser user)
        {
            return SqlServerRevenuQuebecRemittanceDatabaseMap.Select(user);
        }
        #endregion


    }
}
