﻿using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;

namespace WorkLinks.DataLayer.DataAccess.Central
{
    public class ChequeHealthTaxRemittanceAccess
    {
        #region fields

        private SqlServer.SqlServerChequeHealthTaxRemittanceDatabaseMap _sqlServerChequeHealthTaxRemittanceDatabaseMap;

        #endregion 

        #region properties
        private SqlServer.SqlServerChequeHealthTaxRemittanceDatabaseMap SqlServerChequeHealthTaxRemittanceDatabaseMap
        {
            get
            {
                if (_sqlServerChequeHealthTaxRemittanceDatabaseMap == null)
                    _sqlServerChequeHealthTaxRemittanceDatabaseMap = new SqlServer.SqlServerChequeHealthTaxRemittanceDatabaseMap();
                return _sqlServerChequeHealthTaxRemittanceDatabaseMap;
            }
        }

        #endregion

        #region methods

        public ChequeHealthTaxRemittanceDatabaseMapCollection GetChequeHealthTaxRemittanceDatabaseMap(DatabaseUser user)
        {
            return SqlServerChequeHealthTaxRemittanceDatabaseMap.Select(user);
        }
        #endregion


    }
}
