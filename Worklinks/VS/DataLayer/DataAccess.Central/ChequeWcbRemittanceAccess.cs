﻿using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;

namespace WorkLinks.DataLayer.DataAccess.Central
{
    public class ChequeWcbRemittanceAccess
    {
        #region fields

        private SqlServer.SqlServerChequeWcbRemittanceDatabaseMap _sqlServerChequeWcbRemittanceDatabaseMap;

        #endregion 

        #region properties
        private SqlServer.SqlServerChequeWcbRemittanceDatabaseMap SqlServerChequeWcbRemittanceDatabaseMap
        {
            get
            {
                if (_sqlServerChequeWcbRemittanceDatabaseMap == null)
                    _sqlServerChequeWcbRemittanceDatabaseMap = new SqlServer.SqlServerChequeWcbRemittanceDatabaseMap();
                return _sqlServerChequeWcbRemittanceDatabaseMap;
            }
        }

        #endregion

        #region methods

        public ChequeWcbRemittanceDatabaseMapCollection GetChequeWcbRemittanceDatabaseMap(DatabaseUser user)
        {
            return SqlServerChequeWcbRemittanceDatabaseMap.Select(user);
        }
        #endregion


    }
}
