﻿using System;
using System.Collections.Generic;
using System.Linq;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.ChequeHealthTaxRemittance;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.ChequeWcbRemittance;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CIC;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CICWeeklyExport;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CitiAch;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CitiCheque;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CraEftGarnishment;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.PreAuthorizedDebit;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.RbcEftEdi;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.RbcEftEdiSourceDed;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.RbcRevenuQuebecEdiSourceDed;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.REALAdjustmentRule;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.REALEftBanking;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.REALExport;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.REALLaborLevel;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.SageGlExport;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.PexGlExport;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.VertivSunLifePension;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.SocialCosts;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.ScotiaBankEdi;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.REALCustomExport;

namespace WorkLinks.DataLayer.DataAccess.Dynamics
{
    public class DynamicsAccess
    {
        #region fields
        //used to store xml file data into worklinks database
        SqlServer.SqlServerPerson _sqlServerPerson = new SqlServer.SqlServerPerson();
        SqlServer.SqlServerBusinessNumber _sqlServerBusinessNumber = new SqlServer.SqlServerBusinessNumber();
        SqlServer.SqlServerStatutoryDeduction _sqlServerStatutoryDeduction = new SqlServer.SqlServerStatutoryDeduction();
        SqlServer.SqlServerPayrollMaster _sqlServerPayrollMaster = new SqlServer.SqlServerPayrollMaster();
        SqlServer.SqlServerPayrollMasterPaycode _sqlServerPayrollMasterPaycode = new SqlServer.SqlServerPayrollMasterPaycode();

        //used to generate export files
        SqlServer.SqlServerCitiEftPayrollMasterPayment _sqlServerCitiEftPayrollMasterPayment = new SqlServer.SqlServerCitiEftPayrollMasterPayment();
        SqlServer.SqlServerPensionExport _sqlServerPensionExport = new SqlServer.SqlServerPensionExport();
        SqlServer.SqlServerVertivPensionExport _sqlServerVertivPensionExport = new SqlServer.SqlServerVertivPensionExport();

        SqlServer.SqlServerCitiChequeDetails _sqlServerCitiChequeDetails = new SqlServer.SqlServerCitiChequeDetails();
        SqlServer.SqlServerTextExport _sqlServerTextExport = new SqlServer.SqlServerTextExport();
        SqlServer.SqlServerRbcEftEdiExport _sqlServerRbcEftEdiExport = new SqlServer.SqlServerRbcEftEdiExport();
        SqlServer.SqlServerCICWeeklyExport _sqlServerCICWeeklyExport = new SqlServer.SqlServerCICWeeklyExport();
        SqlServer.SqlServerHSBCEftPayrollMasterPayment _sqlServerHSBCEftPayrollMasterPayment = new SqlServer.SqlServerHSBCEftPayrollMasterPayment();
        SqlServer.SqlServerScotiaBankEdiExport _sqlServerScotiaBankEdiExport = new SqlServer.SqlServerScotiaBankEdiExport();

        //cra remittance
        SqlServer.SqlServerCraRemittance _sqlServerCraRemittance = new SqlServer.SqlServerCraRemittance();
        SqlServer.SqlServerCraExport _sqlServerCraExport = new SqlServer.SqlServerCraExport();
        SqlServer.SqlServerCraGarnishment _sqlServerCraGarnishment = new SqlServer.SqlServerCraGarnishment();
        SqlServer.SqlServerCraWcbRemittance _sqlServerCraWcbRemittance = new SqlServer.SqlServerCraWcbRemittance();
        SqlServer.SqlServerCraWcbExport _sqlServerCraWcbExport = new SqlServer.SqlServerCraWcbExport();

        //cheque remittances
        SqlServer.SqlServerChequeWcbRemittance _sqlServerChequeWcbRemittance = new SqlServer.SqlServerChequeWcbRemittance();
        SqlServer.SqlServerChequeWcbExport _sqlServerChequeWcbExport = new SqlServer.SqlServerChequeWcbExport();
        SqlServer.SqlServerChequeHealthTaxRemittance _sqlServerChequeHealthTaxRemittance = new SqlServer.SqlServerChequeHealthTaxRemittance();
        SqlServer.SqlServerChequeHealthTaxExport _sqlServerChequeHealthTaxExport = new SqlServer.SqlServerChequeHealthTaxExport();

        //store cheque invoice
        SqlServer.SqlServerEmailSentBySystem _sqlServerEmailSentBySystem = new SqlServer.SqlServerEmailSentBySystem();
        SqlServer.SqlServerChequeWcbInvoiceExport _sqlServerChequeWcbInvoiceExport = new SqlServer.SqlServerChequeWcbInvoiceExport();

        //rq remittance
        SqlServer.SqlServerRqRemittance _sqlServerRqRemittance = new SqlServer.SqlServerRqRemittance();
        SqlServer.SqlServerRevenuQuebecExport _sqlServerRqExport = new SqlServer.SqlServerRevenuQuebecExport();

        //used to export cheques
        SqlServer.SqlServerPayrollMasterPayment _sqlServerPayrollMasterPayment = new SqlServer.SqlServerPayrollMasterPayment();

        //direct deposit and cra garnishment
        SqlServer.SqlServerPayrollExport _sqlServerPayrollExport = new SqlServer.SqlServerPayrollExport();

        //social costs export
        SqlServer.SqlServerSocialCosts _sqlServerSocialCostsData = new SqlServer.SqlServerSocialCosts();

        //real custom export
        SqlServer.SqlServerREALCustomExport _sqlServerREALCustomExport = new SqlServer.SqlServerREALCustomExport();

        //pre-authorized debit
        SqlServer.SqlServerScotiaPreAuthorizedDebitTotals _sqlServerScotiaPreAuthorizedDebitTotals = new SqlServer.SqlServerScotiaPreAuthorizedDebitTotals();
        SqlServer.SqlServerPreAuthorizedDebitTotals _sqlServerPreAuthorizedDebitTotals = new SqlServer.SqlServerPreAuthorizedDebitTotals();
        SqlServer.SqlServerPreAuthorizedDebitExport _sqlServerPreAuthorizedDebitExport = new SqlServer.SqlServerPreAuthorizedDebitExport();
        SqlServer.SqlServerWcbEstimatePreAuthorizedDebitExport _sqlServerWcbEstimatePreAuthorizedDebitExport = new SqlServer.SqlServerWcbEstimatePreAuthorizedDebitExport();

        //REAL export
        SqlServer.SqlServerREALExport _sqlServerREALExport = new SqlServer.SqlServerREALExport();
        SqlServer.SqlServerREALLaborLevel _sqlServerREALLaborLevel = new SqlServer.SqlServerREALLaborLevel();
        SqlServer.SqlServerREALAdjustmentRule _sqlServerREALAdjustmentRule = new SqlServer.SqlServerREALAdjustmentRule();

        //access 
        EmployeeAccess _employeeAccess = new EmployeeAccess();
        YearEndAccess _yearEndAccess = new YearEndAccess();
        PayrollAccess _payrollAccess = new PayrollAccess(); //used in "DeserializeDynamicsXmlFile" to update "payroll_process.code_payroll_process_status_cd" column
        CodeAccess _codeAccess = new CodeAccess();
        #endregion

        #region payroll master payment
        public PayrollMasterPaymentCollection GetPayrollMasterPaymentCheque(DatabaseUser user, long payrollProcessId)
        {
            return _sqlServerPayrollMasterPayment.GetCheque(user, payrollProcessId);
        }
        public void InsertPayrollMasterPayment(DatabaseUser user, PayrollMasterPayment payment)
        {
            _sqlServerPayrollMasterPayment.Insert(user, payment);
        }
        public void DeletePayrollMasterPaymentById(DatabaseUser user, long payrollMasterPaymentId)
        {
            _sqlServerPayrollMasterPayment.DeleteById(user, payrollMasterPaymentId);
        }
        #endregion

        public byte[] GetGlFlatFileData(DatabaseUser user, String procName, String[] parmNames, Object[] parmValues)
        {
            byte[] fileContents = null;

            //call sql
            String[] fileData = _sqlServerTextExport.Select(user, procName, parmNames, parmValues);

            if (fileData != null && fileData.Length > 0)
            {
                //convert array to single string
                String fileString = String.Join(Environment.NewLine, fileData);

                //create the byte array
                fileContents = convertStringToByteArray(fileString);
            }

            return fileContents;
        }
        public byte[] CreateGlSapExport(GlSapCollection collection)
        {
            byte[] fileContents = null;

            if (collection != null && collection.Count > 0)
            {
                //declare the classes, create arrays, then instantiate the object
                GLSapExportHeader[] glSapExportHeaderFile = new GLSapExportHeader[1] { new GLSapExportHeader() { PY_IN_HEADER_PR_DT = DateTime.Now } };
                GLSapExport[] glSapExportDetails = PopulateGlSapExportDetails(collection, glSapExportHeaderFile[0].PY_IN_HEADER_PR_DT);
                GLSapExportTrailer[] glSapExportTrailerFile = GetTrailerTotals(collection);

                FileHelpers.MultiRecordEngine engine = new FileHelpers.MultiRecordEngine(typeof(GLSapExportHeader), typeof(GLSapExport), typeof(GLSapExportTrailer));
                String glSapExportFileString = WriteGLSapFile(engine, glSapExportHeaderFile, glSapExportDetails, glSapExportTrailerFile);

                //create the csv file
                fileContents = convertStringToByteArray(glSapExportFileString);
            }

            return fileContents;
        }
        public GLSapExport[] PopulateGlSapExportDetails(GlSapCollection collection, DateTime fileCreateDate)
        {
            GLSapExport[] detailsRecord = new GLSapExport[collection.Count];

            //loop to instantiate class entries
            for (int i = 0; i < collection.Count; i++)
            {
                detailsRecord[i] = new GLSapExport()
                {
                    PY_IN_LINE_NO = (i + 1).ToString(),
                    PY_IN_BKPF_BUKRS = collection[i].LegalEntity,
                    PY_IN_BUDAT = fileCreateDate,
                    PY_IN_RF05A_NEWKO = collection[i].Account,
                    PY_IN_RF05A_NEWBS = collection[i].DebitCreditIndicator,
                    PY_IN_BSEG_WRBTR = (collection[i].DebitAmount > 0 ? Convert.ToString(Math.Round(collection[i].DebitAmount, 2, MidpointRounding.AwayFromZero)) : Convert.ToString(Math.Round(collection[i].CreditAmount, 2, MidpointRounding.AwayFromZero))),
                    PY_IN_COBL_KOSTL = collection[i].CostCentre
                };
            }

            return detailsRecord;
        }
        public GLSapExportTrailer[] GetTrailerTotals(GlSapCollection collection)
        {
            GLSapExportTrailer[] trailerRecord = new GLSapExportTrailer[1] { new GLSapExportTrailer() };

            //total number of records
            trailerRecord[0].PY_IN_FOOTER_RCD_CNT = collection.Count.ToString();

            //get debit/credit amount totals from collection and round
            trailerRecord[0].PY_IN_FOOTER_DR_AMT = Convert.ToString(Math.Round(collection.TotalDebitAmount, 2, MidpointRounding.AwayFromZero));
            trailerRecord[0].PY_IN_FOOTER_CR_AMT = Convert.ToString(Math.Round(collection.TotalCreditAmount, 2, MidpointRounding.AwayFromZero));

            return trailerRecord;
        }
        private String WriteGLSapFile(FileHelpers.MultiRecordEngine eng, GLSapExportHeader[] glSapHeader, GLSapExport[] glSapDetails, GLSapExportTrailer[] glSapTrailer)
        {
            String fileData = eng.WriteString(glSapHeader);
            fileData += eng.WriteString(glSapDetails);
            fileData += eng.WriteString(glSapTrailer);

            return fileData;
        }

        public byte[] CreateGlPexCsvExport(GlPexCollection collection)
        {
            byte[] fileContents = null;

            if (collection != null && collection.Count > 0)
            {
                //declare the classes for the csv file, create arrays, then instantiate the object
                PexGlExportHeader[] header = new PexGlExportHeader[1] { new PexGlExportHeader() };
                PexGlExportDetail[] detail = new PexGlExportDetail[collection.Count];
                PexGlExportTrailer[] trailer = new PexGlExportTrailer[1] { new PexGlExportTrailer() };

                //loop to instantiate class entries
                for (int i = 0; i < collection.Count; i++)
                    detail[i] = new PexGlExportDetail();

                //poplulate arrays
                PopulateGlPexHeader(header, collection);
                PopulateGlPexDetails(detail, collection);
                PopulateGlPexTrailer(trailer, collection);

                FileHelpers.MultiRecordEngine engine = new FileHelpers.MultiRecordEngine(typeof(PexGlExportHeader), typeof(PexGlExportDetail), typeof(PexGlExportTrailer));
                String glCsvExportFileString = WritePexCsvFile(engine, header, detail, trailer);

                //create the csv file
                fileContents = convertStringToByteArray(glCsvExportFileString);
            }

            return fileContents;
        }

        private void PopulateGlPexHeader(PexGlExportHeader[] header, GlPexCollection collection)
        {
            header[0].PayrollPeriod = collection[0].PeriodYear.ToString() + collection[0].Period.ToString().PadLeft(2, '0') + collection[0].Sequence.ToString().PadLeft(2, '0');
        }

        private void PopulateGlPexDetails(PexGlExportDetail[] detail, GlPexCollection collection)
        {
            for (int i = 0; i < collection.Count; i++)
            {
                //populate
                detail[i].RecordNo = (i + 2).ToString().PadLeft(10, '0'); //will start at 2 since header starts at 1
                detail[i].GLAccount = collection[i].GeneralLedgerAccount;
                detail[i].GLAccountDesc = collection[i].GlAccountDescription;
                detail[i].CostCenter = collection[i].CostCenter;
                detail[i].PayElement = collection[i].PayElement;
                detail[i].PayServID = collection[i].PayServId;
                detail[i].HRISID = collection[i].HRISID;
                detail[i].DebitAmount = Convert.ToString(Math.Round(collection[i].DebitAmount, 2, MidpointRounding.AwayFromZero));
                detail[i].CreditAmount = Convert.ToString(Math.Round(collection[i].CreditAmount, 2, MidpointRounding.AwayFromZero));
                detail[i].Hours = collection[i].Hours;
                detail[i].PostingDate = collection[i].ChequeDate.ToString("yyyyMMdd");
            }
        }

        private void PopulateGlPexTrailer(PexGlExportTrailer[] trailer, GlPexCollection collection)
        {
            trailer[0].RecordNo = (collection.Count + 2).ToString().PadLeft(10, '0'); //count of collection + header + trailer
            trailer[0].RecordCount = (collection.Count + 2).ToString();

            //get debit/credit amount totals from collection and round
            trailer[0].DebitTotal = Convert.ToString(Math.Round(collection.TotalDebitAmount, 2, MidpointRounding.AwayFromZero));
            trailer[0].CreditTotal = Convert.ToString(Math.Round(collection.TotalCreditAmount, 2, MidpointRounding.AwayFromZero));
        }

        private String WritePexCsvFile(FileHelpers.MultiRecordEngine eng, PexGlExportHeader[] header, PexGlExportDetail[] detail, PexGlExportTrailer[] trailer)
        {
            System.Text.StringBuilder fileData = new System.Text.StringBuilder();

            fileData.Append(eng.WriteString(header));
            fileData.Append(eng.WriteString(detail));
            fileData.Append(eng.WriteString(trailer));

            return fileData.ToString();
        }

        public byte[] CreateGlSageCsvExport(GlSageCollection collection)
        {
            byte[] fileContents = null;

            if (collection != null && collection.Count > 0)
            {
                //declare the glcsv classes for the csv file, create arrays, then instantiate the object
                SageGlExportHeader1[] header1 = new SageGlExportHeader1[1] { new SageGlExportHeader1() };
                SageGlExportHeader2[] header2 = new SageGlExportHeader2[1] { new SageGlExportHeader2() };

                //first header data
                SageGlExportHeader1Data[] header1Data = new SageGlExportHeader1Data[1];
                header1Data[0] = new SageGlExportHeader1Data();

                header1Data[0].RecType = collection[0].Header1RecType; //padded in file
                header1Data[0].BatchId = collection[0].Header1BatchId;
                header1Data[0].BatchEntry = collection[0].Header1BatchEntry;
                header1Data[0].SourceLedger = collection[0].Header1SourceLedger;
                header1Data[0].SourceType = collection[0].Header1SourceType;
                header1Data[0].JournalDescription = collection[0].Header1JournalDescription;
                header1Data[0].FiscalYear = collection[0].Header1FiscalYear;
                header1Data[0].FiscalPeriod = collection[0].Header1FiscalMonth.PadLeft(2, '0');
                header1Data[0].DateEntry = collection[0].Header1DateEntry;

                //second header data
                SageGlExportHeader2Data[] header2Data = new SageGlExportHeader2Data[collection.Count];

                //loop to instantiate class entries
                for (int i = 0; i < collection.Count; i++)
                    header2Data[i] = new SageGlExportHeader2Data();

                header2Data = PopulateGlSageHeader2Details(header2Data, collection);

                FileHelpers.MultiRecordEngine engine = new FileHelpers.MultiRecordEngine(typeof(SageGlExportHeader1), typeof(SageGlExportHeader2), typeof(SageGlExportHeader1Data), typeof(SageGlExportHeader2Data));
                String glCsvExportFileString = WriteSageCsvFile(engine, header1, header2, header1Data, header2Data);

                //create the csv file
                fileContents = convertStringToByteArray(glCsvExportFileString);
            }

            return fileContents;
        }

        private SageGlExportHeader2Data[] PopulateGlSageHeader2Details(SageGlExportHeader2Data[] glSageArray, GlSageCollection collection)
        {
            for (int i = 0; i < collection.Count; i++)
            {
                glSageArray[i].RecType = collection[i].Header2RecType;
                glSageArray[i].BatchNumber = collection[i].Header2BatchId;
                glSageArray[i].JournalId = collection[i].Header2JournalEntry;
                glSageArray[i].TransactionNumber = collection[i].Header2TransactionNumber.PadLeft(9, '0');
                glSageArray[i].AccountId = collection[i].Header2AccountNumber;
                glSageArray[i].TransactionAmount = Math.Round(collection[i].Header2TransactionAmount, 2, MidpointRounding.AwayFromZero);
                glSageArray[i].TransactionDescription = collection[i].Header2TransactionDesc;
                glSageArray[i].TransactionReference = collection[i].Header2TransactionRef;
                glSageArray[i].TransactionDate = collection[i].Header2TransactionDate;
            }

            return glSageArray;
        }

        private String WriteSageCsvFile(FileHelpers.MultiRecordEngine eng, SageGlExportHeader1[] header1, SageGlExportHeader2[] header2, SageGlExportHeader1Data[] header1Data, SageGlExportHeader2Data[] header2Data)
        {
            System.Text.StringBuilder fileData = new System.Text.StringBuilder();

            fileData.Append(eng.WriteString(header1)); //file header1 always present
            fileData.Append(eng.WriteString(header2)); //file header2 always present

            if (header1Data != null && header1Data.Length > 0)
            {
                for (int i = 0; i < header1Data.Length; i++)
                {
                    fileData.Append(eng.WriteString(new SageGlExportHeader1Data[] { header1Data[i] }));
                }
            }

            if (header2Data != null && header2Data.Length > 0)
            {
                for (int i = 0; i < header2Data.Length; i++)
                {
                    fileData.Append(eng.WriteString(new SageGlExportHeader2Data[] { header2Data[i] }));
                }
            }

            return fileData.ToString();
        }

        public byte[] CreateGlCsvExport(GlCsvCollection collection, bool includeHeader)
        {
            byte[] fileContents = null;

            if (collection != null && collection.Count > 0)
            {
                //declare the glcsv classes for the csv file, create arrays, then instantiate the object
                GLCsvExportHeader[] glCsvExportHeaderFile = new GLCsvExportHeader[1];
                GLCsvExport[] glCsvExportFile = new GLCsvExport[collection.Count];

                glCsvExportHeaderFile[0] = new GLCsvExportHeader();

                //loop to instantiate class entries
                for (int i = 0; i < collection.Count; i++)
                    glCsvExportFile[i] = new GLCsvExport();

                //populate fields in the export classes
                glCsvExportFile = populateGlCsvExportDetails(glCsvExportFile, collection);

                FileHelpers.MultiRecordEngine engine = new FileHelpers.MultiRecordEngine(typeof(GLCsvExportHeader), typeof(GLCsvExport));
                String glCsvExportFileString = WriteCsvFile(engine, glCsvExportHeaderFile, glCsvExportFile, includeHeader);

                //create the csv file
                fileContents = convertStringToByteArray(glCsvExportFileString);
            }

            return fileContents;
        }
        private GLCsvExport[] populateGlCsvExportDetails(GLCsvExport[] glCsvArray, GlCsvCollection collection)
        {
            for (int i = 0; i < collection.Count; i++)
            {
                glCsvArray[i].Amount = collection[i].Amount.ToString();
                glCsvArray[i].GL_Account = collection[i].GlAccount;
                glCsvArray[i].Company = collection[i].Company;
                glCsvArray[i].Cost_Ctr = collection[i].CostCenter;
                glCsvArray[i].Debit_Credit = collection[i].CreditDebit;
            }

            return glCsvArray;
        }
        private String WriteCsvFile(FileHelpers.MultiRecordEngine eng, GLCsvExportHeader[] header, GLCsvExport[] details, bool includeHeader)
        {
            System.Text.StringBuilder rtn = new System.Text.StringBuilder();

            if (includeHeader)
                rtn.Append(eng.WriteString(header));

            rtn.Append(eng.WriteString(details));

            return rtn.ToString();
        }

        #region real custom export

        public REALCustomExportCollection GetRealCustomExportData(DatabaseUser user, long payrollProcessId)
        {
            return _sqlServerREALCustomExport.Select(user, payrollProcessId);
        }

        public byte[] CreateRealCustomExportDataFile(DatabaseUser user, REALCustomExportCollection customExportColl)
        {
            byte[] fileContents = null;

            if (customExportColl != null && customExportColl.Count > 0)
            {
                //declare the social costs class for the export file, create arrays, then instantiate the object
                RealCustomExportHeader[] header = new RealCustomExportHeader[1] { new RealCustomExportHeader() };
                RealCustomExportDetail[] details = CreateRealCustomExportDataFileDetail(customExportColl);

                //write file to string
                FileHelpers.MultiRecordEngine engine = new FileHelpers.MultiRecordEngine(typeof(RealCustomExportHeader), typeof(RealCustomExportDetail));
                string realCustomExportDataFileString = WriteRealCustomExportDataCsvFile(engine, header, details);

                //create the file
                fileContents = convertStringToByteArray(realCustomExportDataFileString);
            }

            return fileContents;
        }

        private RealCustomExportDetail[] CreateRealCustomExportDataFileDetail(REALCustomExportCollection export)
        {
            RealCustomExportDetail[] realCustomExportDataDetails = new RealCustomExportDetail[export.Count];

            for (int i = 0; i < export.Count; i++)
            {
                realCustomExportDataDetails[i] = new RealCustomExportDetail();

                //here we map and restrict the data to the pre-defined lengths in the spec document (if applicable)
                realCustomExportDataDetails[i].EmployeeNumber = export[i].EmployeeNumber;
                realCustomExportDataDetails[i].EmployeeFirstName = export[i].EmployeeFirstName;
                realCustomExportDataDetails[i].EmployeeLastName = export[i].EmployeeLastName;
                realCustomExportDataDetails[i].TransactionDate = export[i].TransactionDate;
                realCustomExportDataDetails[i].PayCodeType = export[i].PayCodeType;
                realCustomExportDataDetails[i].PayCode = export[i].PayCode;
                realCustomExportDataDetails[i].PayCodeDescription = export[i].PayCodeDescription;
                realCustomExportDataDetails[i].CurrentHours = export[i].CurrentHours;
                realCustomExportDataDetails[i].CurrentRate = export[i].CurrentRate;
                realCustomExportDataDetails[i].CurrentAmount = export[i].CurrentAmount;
                realCustomExportDataDetails[i].CostCentreNumber = export[i].CostCentreNumber;
                realCustomExportDataDetails[i].CostCentreDescription = export[i].CostCentreDescription;
                realCustomExportDataDetails[i].PayPeriod = export[i].PayPeriod;
            }

            return realCustomExportDataDetails;
        }
        
        private String WriteRealCustomExportDataCsvFile(FileHelpers.MultiRecordEngine eng, RealCustomExportHeader[] header, RealCustomExportDetail[] details)
        {
            System.Text.StringBuilder fileData = new System.Text.StringBuilder();

            fileData.Append(eng.WriteString(header));
            fileData.Append(eng.WriteString(details));

            return fileData.ToString();
        }
        #endregion

        #region social costs export
        public SocialCostsCollection GetSocialCostsData(DatabaseUser user, long payrollProcessId)
        {
            return _sqlServerSocialCostsData.Select(user, payrollProcessId);
        }
        public byte[] CreateSocialCostsExportFile(DatabaseUser user, SocialCostsCollection socialCostsColl)
        {
            byte[] fileContents = null;

            if (socialCostsColl != null && socialCostsColl.Count > 0)
            {
                //declare the social costs class for the export file, create arrays, then instantiate the object
                SocialCostsExportFileHeader[] header = new SocialCostsExportFileHeader[1] { new SocialCostsExportFileHeader() };
                SocialCostsExportFileDetails[] details = CreateSocialCostsExportFileDetail(socialCostsColl);

                //write file to string
                FileHelpers.MultiRecordEngine engine = new FileHelpers.MultiRecordEngine(typeof(SocialCostsExportFileHeader), typeof(SocialCostsExportFileDetails));
                String socialCostsExportFileString = WriteSocialCostsCsvFile(engine, header, details);

                //create the file
                fileContents = convertStringToByteArray(socialCostsExportFileString);
            }

            return fileContents;
        }

        private SocialCostsExportFileDetails[] CreateSocialCostsExportFileDetail(SocialCostsCollection export)
        {
            SocialCostsExportFileDetails[] socialCostsDetails = new SocialCostsExportFileDetails[export.Count];

            for (int i = 0; i < export.Count; i++)
            {
                socialCostsDetails[i] = new SocialCostsExportFileDetails();

                //here we map and restrict the data to the pre-defined lengths in the spec document (if applicable)
                socialCostsDetails[i].EmployeeNumber = export[i].EmployeeNumber;
                socialCostsDetails[i].BenefitName = export[i].BenefitName;
                socialCostsDetails[i].BenefitValue = Convert.ToString(Math.Round(export[i].BenefitValue, 2, MidpointRounding.AwayFromZero));
                socialCostsDetails[i].StartDate = export[i].StartDate;
                socialCostsDetails[i].EndDate = export[i].CutoffDate;
            }

            return socialCostsDetails;
        }

        private String WriteSocialCostsCsvFile(FileHelpers.MultiRecordEngine eng, SocialCostsExportFileHeader[] header, SocialCostsExportFileDetails[] details)
        {
            System.Text.StringBuilder fileData = new System.Text.StringBuilder();

            fileData.Append(eng.WriteString(header));
            fileData.Append(eng.WriteString(details));

            return fileData.ToString();
        }
        #endregion

        #region vertiv pension export

        public byte[] CreateVertivPensionExportFile(DatabaseUser user, long payrollProcessId)
        {
            byte[] fileContents = null;
            VertivSunLifePensionExportCollection coll = _sqlServerVertivPensionExport.Select(user, payrollProcessId);

            if (coll != null && coll.Count > 0)
            {
                //declare the pension class for the pension file, create arrays, then instantiate the object
                VertivSunLifePensionRecord[] pensionFile = new VertivSunLifePensionRecord[coll.Count];

                //loop to instantiate class entries
                for (int i = 0; i < coll.Count; i++)
                    pensionFile[i] = new VertivSunLifePensionRecord();

                //populate fields in the export classes
                PopulateVertivPensionDetails(pensionFile, coll);

                FileHelpers.FileHelperEngine engine = new FileHelpers.FileHelperEngine(typeof(VertivSunLifePensionRecord));
                string pensionFileString = engine.WriteString(pensionFile);

                //create the EFT file
                fileContents = convertStringToByteArray(pensionFileString);
            }

            return fileContents;
        }
        private void PopulateVertivPensionDetails(VertivSunLifePensionRecord[] pensionArray, VertivSunLifePensionExportCollection collection)
        {
            for (int i = 0; i < collection.Count; i++)
            {
                pensionArray[i].EmployeeNumber = collection[i].EmployeeNumber.PadLeft(9, '0');//9 Digit Numeric Employee ID with leading zeros 

                //if negative amount, remove decimal, pad left so field is 9 chars.  concat "-" symbol to the value and remove the first 0
                if (Convert.ToDecimal(collection[i].EmployerRequiredContributionAmount1) < 0)
                    pensionArray[i].EmployerRequiredContributionAmount1 = "-" + collection[i].EmployerRequiredContributionAmount1.Replace(".", "").PadLeft(9).Substring(1, collection[i].EmployerRequiredContributionAmount1.Length - 1);
                else
                    pensionArray[i].EmployerRequiredContributionAmount1 = collection[i].EmployerRequiredContributionAmount1;

                if (Convert.ToDecimal(collection[i].MemberVoluntaryMatchedContributionAmount1) < 0)
                    pensionArray[i].MemberVoluntaryMatchedContributionAmount1 = "-" + collection[i].MemberVoluntaryMatchedContributionAmount1.Replace(".", "").PadLeft(9).Substring(1, collection[i].MemberVoluntaryMatchedContributionAmount1.Length - 1);
                else
                    pensionArray[i].MemberVoluntaryMatchedContributionAmount1 = collection[i].MemberVoluntaryMatchedContributionAmount1;

                if (Convert.ToDecimal(collection[i].MemberVoluntaryUnmatchedContributionAmount1) < 0)
                    pensionArray[i].MemberVoluntaryUnmatchedContributionAmount1 = "-" + collection[i].MemberVoluntaryUnmatchedContributionAmount1.Replace(".", "").PadLeft(9).Substring(1, collection[i].MemberVoluntaryUnmatchedContributionAmount1.Length - 1);
                else
                    pensionArray[i].MemberVoluntaryUnmatchedContributionAmount1 = collection[i].MemberVoluntaryUnmatchedContributionAmount1;

                if (Convert.ToDecimal(collection[i].EmployerVoluntaryMatchingContributionAmount1) < 0)
                    pensionArray[i].EmployerVoluntaryMatchingContributionAmount1 = "-" + collection[i].EmployerVoluntaryMatchingContributionAmount1.Replace(".", "").PadLeft(9).Substring(1, collection[i].EmployerVoluntaryMatchingContributionAmount1.Length - 1);
                else
                    pensionArray[i].EmployerVoluntaryMatchingContributionAmount1 = collection[i].EmployerVoluntaryMatchingContributionAmount1;

                if (Convert.ToDecimal(collection[i].MemberVoluntaryMatchedContributionAmount2) < 0)
                    pensionArray[i].MemberVoluntaryMatchedContributionAmount2 = "-" + collection[i].MemberVoluntaryMatchedContributionAmount2.Replace(".", "").PadLeft(9).Substring(1, collection[i].MemberVoluntaryMatchedContributionAmount2.Length - 1);
                else
                    pensionArray[i].MemberVoluntaryMatchedContributionAmount2 = collection[i].MemberVoluntaryMatchedContributionAmount2;

                if (Convert.ToDecimal(collection[i].MemberVoluntaryUnmatchedContributionAmount2) < 0)
                    pensionArray[i].MemberVoluntaryUnmatchedContributionAmount2 = "-" + collection[i].MemberVoluntaryUnmatchedContributionAmount2.Replace(".", "").PadLeft(9).Substring(1, collection[i].MemberVoluntaryUnmatchedContributionAmount2.Length - 1);
                else
                    pensionArray[i].MemberVoluntaryUnmatchedContributionAmount2 = collection[i].MemberVoluntaryUnmatchedContributionAmount2;

                if (Convert.ToDecimal(collection[i].EmployerRequiredContributionAmount2) < 0)
                    pensionArray[i].EmployerRequiredContributionAmount2 = "-" + collection[i].EmployerRequiredContributionAmount2.Replace(".", "").PadLeft(9).Substring(1, collection[i].EmployerRequiredContributionAmount2.Length - 1);
                else
                    pensionArray[i].EmployerRequiredContributionAmount2 = collection[i].EmployerRequiredContributionAmount2;

                if (Convert.ToDecimal(collection[i].MemberVoluntaryMatchedContributionAmount3) < 0)
                    pensionArray[i].MemberVoluntaryMatchedContributionAmount3 = "-" + collection[i].MemberVoluntaryMatchedContributionAmount3.Replace(".", "").PadLeft(9).Substring(1, collection[i].MemberVoluntaryMatchedContributionAmount3.Length - 1);
                else
                    pensionArray[i].MemberVoluntaryMatchedContributionAmount3 = collection[i].MemberVoluntaryMatchedContributionAmount3;

                if (Convert.ToDecimal(collection[i].MemberVoluntaryUnmatchedContributionAmount3) < 0)
                    pensionArray[i].MemberVoluntaryUnmatchedContributionAmount3 = "-" + collection[i].MemberVoluntaryUnmatchedContributionAmount3.Replace(".", "").PadLeft(9).Substring(1, collection[i].MemberVoluntaryUnmatchedContributionAmount3.Length - 1);
                else
                    pensionArray[i].MemberVoluntaryUnmatchedContributionAmount3 = collection[i].MemberVoluntaryUnmatchedContributionAmount3;

                if (Convert.ToDecimal(collection[i].EmployerVoluntaryMatchingContributionAmount2) < 0)
                    pensionArray[i].EmployerVoluntaryMatchingContributionAmount2 = "-" + collection[i].EmployerVoluntaryMatchingContributionAmount2.Replace(".", "").PadLeft(9).Substring(1, collection[i].EmployerVoluntaryMatchingContributionAmount2.Length - 1);
                else
                    pensionArray[i].EmployerVoluntaryMatchingContributionAmount2 = collection[i].EmployerVoluntaryMatchingContributionAmount2;

                if (Convert.ToDecimal(collection[i].EmployerVoluntaryMatchingContributionAmount3) < 0)
                    pensionArray[i].EmployerVoluntaryMatchingContributionAmount3 = "-" + collection[i].EmployerVoluntaryMatchingContributionAmount3.Replace(".", "").PadLeft(9).Substring(1, collection[i].EmployerVoluntaryMatchingContributionAmount3.Length - 1);
                else
                    pensionArray[i].EmployerVoluntaryMatchingContributionAmount3 = collection[i].EmployerVoluntaryMatchingContributionAmount3;
                //end of financial section

                pensionArray[i].PayStartDate = collection[i].PayStartDate;
                pensionArray[i].PayEndDate = collection[i].PayEndDate;
            }
        }

        #endregion

        public byte[] CreatePensionExportFile(DatabaseUser user, long payrollProcessId)
        {
            byte[] fileContents = null;
            SunLifePensionExportCollection coll = _sqlServerPensionExport.Select(user, payrollProcessId);

            if (coll != null && coll.Count > 0)
            {
                //declare the pension class for the pension file, create arrays, then instantiate the object
                SunLifePensionRecord[] sunLifePensionFile = new SunLifePensionRecord[coll.Count];

                //loop to instantiate class entries
                for (int i = 0; i < coll.Count; i++)
                    sunLifePensionFile[i] = new SunLifePensionRecord();

                //populate fields in the export classes
                sunLifePensionFile = populatePensionDetails(sunLifePensionFile, coll);

                FileHelpers.FileHelperEngine engine = new FileHelpers.FileHelperEngine(typeof(SunLifePensionRecord));
                String pensionFileString = engine.WriteString(sunLifePensionFile);

                //create the EFT file
                fileContents = convertStringToByteArray(pensionFileString);
            }

            return fileContents;
        }
        private SunLifePensionRecord[] populatePensionDetails(SunLifePensionRecord[] pensionArray, SunLifePensionExportCollection collection)
        {
            for (int i = 0; i < collection.Count; i++)
            {
                pensionArray[i].EmployeeNumber = collection[i].EmployeeNumber.PadRight(9);                                                      //left justified, pad to 9 chars if len < 9
                pensionArray[i].SocialInsuranceNumber = collection[i].SocialInsuranceNumber.Trim().Replace("-", "");                            //no hypens
                pensionArray[i].MemberLastName = collection[i].MemberLastName.PadRight(32);                                                     //left justified, padded to fit 32 chars
                pensionArray[i].MemberFirstName = collection[i].MemberFirstName.PadRight(32);                                                   //left justified, padded to fit 32 chars

                //NOTE:  For addresses outside of Canada, please also include the Country name in one of the address fields.
                pensionArray[i].AddressLine1 = collection[i].AddressLine1 != null ? collection[i].AddressLine1.PadRight(32) : pensionArray[i].AddressLine1.PadLeft(32); //left justified, padded to fit 32 chars
                pensionArray[i].AddressLine2 = collection[i].AddressLine2 != null ? collection[i].AddressLine2.PadRight(32) : pensionArray[i].AddressLine2.PadLeft(32); //left justified, padded to fit 32 chars
                pensionArray[i].AddressLine3 = collection[i].AddressLine3 != null ? collection[i].AddressLine3.PadRight(32) : pensionArray[i].AddressLine3.PadLeft(32); //left justified, padded to fit 32 chars
                pensionArray[i].AddressLine4 = collection[i].AddressLine4 != null ? collection[i].AddressLine4.PadRight(32) : pensionArray[i].AddressLine4.PadLeft(32); //left justified, padded to fit 32 chars
                pensionArray[i].MemberResidenceCode = collection[i].MemberResidenceCode;
                pensionArray[i].UserField1 = collection[i].UserField1;

                //NOTE:  For US or Foreign addresses, please leave this field blank and insert ZIP or country code in one of the Address fields above
                if (collection[i].MemberResidenceCode.StartsWith("1")) //all CA codes start with "1"
                    pensionArray[i].MemberPostalCode = collection[i].MemberPostalCode.Trim().Replace(" ", "").Replace("-", "").PadRight(6);     //padding wont be needed if data is correct.
                else
                    pensionArray[i].MemberPostalCode = "      ";

                pensionArray[i].UserField2 = collection[i].UserField2.PadRight(10);                                                             //date is 10 chars YYYYMMDD plus 2 spaces
                pensionArray[i].UserField5 = collection[i].UserField5.PadRight(15);                                                             //Length is 16 chars

                //numeric with 2 decimal places (ie.Insert 100 hours as 100.00)
                pensionArray[i].UserField9 = String.Format("{0:0.00}", Convert.ToDecimal(collection[i].UserField9)).PadLeft(9, ' ');            //Length is 9 chars, no leading 0's

                /*  Financial section
                    Below 7 fields:  all numeric fields.  Right justify, no decimals.  
                    For a positive amount of ie. $123.45 should be entered as +00012345 or 000012345 and a negative amount ie. $-123.45 should be entered as -00012345.  
                    For 0 amounts, please use 000000000 or filler (spaces)."
                    sql procedure will return correct format for 0's and positive values.
                */

                //if negative amount, remove decimal, pad left so field is 9 chars.  concat "-" symbol to the value and remove the first 0
                if (Convert.ToDecimal(collection[i].EmployersRequired) < 0)
                    pensionArray[i].EmployersRequired = "-" + collection[i].EmployersRequired.Replace(".", "").PadLeft(9).Substring(1, collection[i].EmployersRequired.Length - 1);
                else
                    pensionArray[i].EmployersRequired = collection[i].EmployersRequired;

                if (Convert.ToDecimal(collection[i].MemberVoluntaryMatched) < 0)
                    pensionArray[i].MemberVoluntaryMatched = "-" + collection[i].MemberVoluntaryMatched.Replace(".", "").PadLeft(9).Substring(1, collection[i].MemberVoluntaryMatched.Length - 1);
                else
                    pensionArray[i].MemberVoluntaryMatched = collection[i].MemberVoluntaryMatched;

                if (Convert.ToDecimal(collection[i].EmployerVoluntaryMatched) < 0)
                    pensionArray[i].EmployerVoluntaryMatched = "-" + collection[i].EmployerVoluntaryMatched.Replace(".", "").PadLeft(9).Substring(1, collection[i].EmployerVoluntaryMatched.Length - 1);
                else
                    pensionArray[i].EmployerVoluntaryMatched = collection[i].EmployerVoluntaryMatched;

                if (Convert.ToDecimal(collection[i].MemberVoluntaryUnmatched1) < 0)
                    pensionArray[i].MemberVoluntaryUnmatched1 = "-" + collection[i].MemberVoluntaryUnmatched1.Replace(".", "").PadLeft(9).Substring(1, collection[i].MemberVoluntaryUnmatched1.Length - 1);
                else
                    pensionArray[i].MemberVoluntaryUnmatched1 = collection[i].MemberVoluntaryUnmatched1;

                if (Convert.ToDecimal(collection[i].MemberVoluntaryUnmatched2) < 0)
                    pensionArray[i].MemberVoluntaryUnmatched2 = "-" + collection[i].MemberVoluntaryUnmatched2.Replace(".", "").PadLeft(9).Substring(1, collection[i].MemberVoluntaryUnmatched2.Length - 1);
                else
                    pensionArray[i].MemberVoluntaryUnmatched2 = collection[i].MemberVoluntaryUnmatched2;

                if (Convert.ToDecimal(collection[i].MemberVoluntarySpousalUnmatched) < 0)
                    pensionArray[i].MemberVoluntarySpousalUnmatched = "-" + collection[i].MemberVoluntarySpousalUnmatched.Replace(".", "").PadLeft(9).Substring(1, collection[i].MemberVoluntarySpousalUnmatched.Length - 1);
                else
                    pensionArray[i].MemberVoluntarySpousalUnmatched = collection[i].MemberVoluntarySpousalUnmatched;

                if (Convert.ToDecimal(collection[i].MemberVoluntaryUnmatched3) < 0)
                    pensionArray[i].MemberVoluntaryUnmatched3 = "-" + collection[i].MemberVoluntaryUnmatched3.Replace(".", "").PadLeft(9).Substring(1, collection[i].MemberVoluntaryUnmatched3.Length - 1);
                else
                    pensionArray[i].MemberVoluntaryUnmatched3 = collection[i].MemberVoluntaryUnmatched3;
                //end of financial section

                pensionArray[i].MemberProvinceOfEmployment1 = collection[i].MemberProvinceOfEmployment1;
                pensionArray[i].MemberDateOfBirth1 = collection[i].MemberDateOfBirth1;
                pensionArray[i].MemberDateOfHire = collection[i].MemberDateOfHire;
                pensionArray[i].LanguageCode = collection[i].LanguageCode;
                pensionArray[i].CurrentYearCompensation = collection[i].CurrentYearCompensation;
                pensionArray[i].CurrentYearSalaryWages = collection[i].CurrentYearSalaryWages;
                pensionArray[i].EmployeeStatusCode = collection[i].EmployeeStatusCode;
                pensionArray[i].EmployeeStatusEffectiveDate = collection[i].EmployeeStatusEffectiveDate;
                pensionArray[i].MemberGender = collection[i].MemberGender;
                pensionArray[i].PayStartDate = collection[i].PayStartDate;
                pensionArray[i].PayEndDate = collection[i].PayEndDate;

                pensionArray[i].MemberProvinceOfEmployment2 = collection[i].MemberProvinceOfEmployment2;
                pensionArray[i].DateOfHire = collection[i].DateOfHire;
                pensionArray[i].MemberDateOfBirth2 = collection[i].MemberDateOfBirth2;
                pensionArray[i].EmailFlag = collection[i].EmailFlag;
                pensionArray[i].EmailAddress = collection[i].EmailAddress.PadRight(64); //left justified
                pensionArray[i].PhoneFlag = collection[i].PhoneFlag;
            }

            return pensionArray;
        }

        public PayrollMasterPaycodeCollection GetPayrollMasterPaycode(DatabaseUser user, long payrollProcessId, bool? includeGarnishments, String paycodeAssociationCode, bool? hasCurrent)
        {
            return _sqlServerPayrollMasterPaycode.Select(user, payrollProcessId, includeGarnishments, paycodeAssociationCode, hasCurrent);
        }
        public PayrollMasterCollection GetPayrollMaster(DatabaseUser user, long payrollProcessId)
        {
            return _sqlServerPayrollMaster.Select(user, payrollProcessId);
        }
        public byte[] CreateEmpowerRemittanceExport(DatabaseUser user, PayrollMasterCollection calcCollection, String revenueQuebecTaxId, PayDetailCollection payDetails, bool useQuebecTaxId)
        {
            byte[] fileContents = null;

            if (payDetails != null && payDetails.Count > 0) //only create export if we have rows to process
            {
                PayDetailCollection nonQuebecPayDetails = payDetails.GetPayDetailForFederal();
                PayDetailCollection quebecPayDetails = payDetails.GetPayDetailForQuebec();

                //populate records
                EmpowerRemittanceHeaderRecord[] exportHeader = PopulateHeader(user, calcCollection[0].PayrollProcessId, nonQuebecPayDetails, quebecPayDetails, revenueQuebecTaxId, payDetails[0].ChequeDate, useQuebecTaxId);

                //instantiate the engine
                FileHelpers.MultiRecordEngine engine = new FileHelpers.MultiRecordEngine(typeof(EmpowerRemittanceHeaderRecord), typeof(EmpowerRemittanceWageRecord), typeof(EmpowerRemittanceTaxRecord));

                //write records to a string
                String empowerFileString = WriteEmpowerFile(exportHeader, engine);

                //create the rrsp file
                fileContents = convertStringToByteArray(empowerFileString);
            }

            return fileContents;
        }

        #region CIC Plus Weekly Export

        //get data
        public CICWeeklyExportCollection GetCicPlusWeeklyExportData(DatabaseUser user, decimal periodYear)
        {
            return _sqlServerCICWeeklyExport.Select(user, periodYear);
        }

        //this is the CICPlus export they can run anytime to see who was active in the selected year and who had positive pay
        public byte[] CreateCICPlusWeeklyExportFile(CICWeeklyExportCollection export)
        {
            byte[] fileContents = null;

            if (export != null && export.Count > 0)
            {
                CICWeeklyExportDetailRecord[] cicWeeklyExport = CreateCicWeeklyExportDetail(export);

                //write file to string
                FileHelpers.FileHelperEngine engine = new FileHelpers.FileHelperEngine(typeof(CICWeeklyExportDetailRecord));
                String cicWeeklyFileString = engine.WriteString(cicWeeklyExport);

                //create the file
                fileContents = convertStringToByteArray(cicWeeklyFileString);
            }

            return fileContents;
        }

        private CICWeeklyExportDetailRecord[] CreateCicWeeklyExportDetail(CICWeeklyExportCollection export)
        {
            CICWeeklyExportDetailRecord[] cicWeeklyDetails = new CICWeeklyExportDetailRecord[export.Count];

            for (int i = 0; i < export.Count; i++)
            {
                cicWeeklyDetails[i] = new CICWeeklyExportDetailRecord();

                //if Unique Identifier is less than 8 characters, it would need to have the preceding zeroes
                if (export[i].EmployeeNumber.Length < 8)
                    cicWeeklyDetails[i].UniqueEmployeeIdentifier = export[i].EmployeeNumber.PadLeft(8, '0');
                //max length = 36 chars.  Our system currently (Feb 2017) allows a max of 32 so this should not be an issue unless it changes
                else if (export[i].EmployeeNumber.Length > 36)
                    cicWeeklyDetails[i].UniqueEmployeeIdentifier = export[i].EmployeeNumber.Substring(0, 36);
                else
                    cicWeeklyDetails[i].UniqueEmployeeIdentifier = export[i].EmployeeNumber;

                //max length = 15 chars.  Our system currently (Feb 2017) allows a max of 32 but this should not be an issue for CA/US.
                cicWeeklyDetails[i].AuthenticationValuePrimary = (export[i].GovernmentIdentificationNumber1.Length > 15) ? export[i].GovernmentIdentificationNumber1.Substring(0, 15) : export[i].GovernmentIdentificationNumber1;

                cicWeeklyDetails[i].AuthenticationValueSecondary = Convert.ToDateTime(export[i].BirthDate);
                cicWeeklyDetails[i].ActiveTerminatedEmployee = export[i].Status;
            }

            return cicWeeklyDetails;
        }

        #endregion

        #region CIC T4/T4A/R1 export

        public byte[] CreateT4CICExportFile(CanadaRevenueAgencyT4Export export)
        {
            byte[] fileContents = null;

            if (export != null && export.T4s.Count > 0)
            {
                CICT4DetailRecord[] cicT4Detail = CreateCicT4Detail(export);

                //write file to string
                FileHelpers.FileHelperEngine engine = new FileHelpers.FileHelperEngine(typeof(CICT4DetailRecord));
                String cicT4FileString = engine.WriteString(cicT4Detail);

                //create the file
                fileContents = convertStringToByteArray(cicT4FileString);
            }

            return fileContents;
        }
        private CICT4DetailRecord[] CreateCicT4Detail(CanadaRevenueAgencyT4Export export)
        {
            CICT4DetailRecord[] t4Detail = new CICT4DetailRecord[export.T4s.Count];

            for (int i = 0; i < export.T4s.Count; i++)
            {
                t4Detail[i] = new CICT4DetailRecord()
                {
                    TaxYear = export.T4s[i].Year.ToString(),
                    UniqueEmployeeIdentifier = export.T4s[i].EmployeeNumber,
                    AuthenticationValuePrimary = export.T4s[i].Employee.GovernmentIdentificationNumber1,
                    AuthenticationValueSecondary = Convert.ToDateTime(export.T4s[i].Employee.BirthDate),
                    EmployeeSocialInsuranceNumber = export.T4s[i].Employee.GovernmentIdentificationNumber1,
                    EmployeeFirstName = export.T4s[i].Employee.FirstName,
                    EmployeeMiddleInitial = export.T4s[i].Employee.MiddleName,
                    EmployeeLastName = export.T4s[i].Employee.LastName,
                    EmployeeSuffix = export.T4s[i].Employee.TitleCode,

                    EmployeeLocationAddress = export.T4s[i].Address.AddressLine1,
                    EmployeeDeliveryAddress = export.T4s[i].Address.AddressLine2,
                    EmployeeCity = export.T4s[i].Address.City,
                    EmployeeProvinceState = export.T4s[i].Address.ProvinceStateCode,
                    EmployeePostalCode = export.T4s[i].Address.PostalZipCode,
                    EmployeeCountry = export.T4s[i].Address.CountryCode,

                    EmployerNameLine1 = export.Employer.Name,
                    EmployerLocationAddress = export.Employer.Address.AddressLine1,
                    EmployerDeliveryAddress = export.Employer.Address.AddressLine2,
                    EmployerCity = export.Employer.Address.City,
                    EmployerProvinceState = export.Employer.Address.ProvinceStateCode,
                    EmployerPostalCode = export.Employer.Address.PostalZipCode,
                    EmployerCountry = export.Employer.Address.CountryCode,

                    Box28CppQppExemptFlag = (export.T4s[i].CanadaPensionPlanExempt28Flag) ? 1 : 0,
                    Box28EiExemptFlag = (export.T4s[i].EmploymentInsuranceExemption28Flag) ? 1 : 0,
                    Box28PpipExemptFlag = (export.T4s[i].ProvincialParentalInsurancePlanExemption28Flag) ? 1 : 0,

                    Box14EmploymentIncome = ((int)(export.T4s[i].EmploymentIncome14 * 100)).ToString(),
                    Box22IncomeTaxDeducted = ((int)(export.T4s[i].IncomeTaxDeducted22 * 100)).ToString(),
                    Box54PayrollAccountNumber = export.T4s[i].BusinessNumber.BusinessTaxNumber,
                    Box10ProvinceOfEmploymentCode = export.T4s[i].TaxableCodeProvinceStateCd,
                    Box16EmployeesCppContributions = (export.T4s[i].TaxableCodeProvinceStateCd != "QC") ? ((int)(export.T4s[i].CanadaPensionPlanDeducted16 * 100)).ToString() : null,
                    Box17EmployeesQppContributions = (export.T4s[i].TaxableCodeProvinceStateCd == "QC") ? ((int)(export.T4s[i].QuebecPensionPlanDeducted17 * 100)).ToString() : null,
                    Box24EiInsurableEarnings = ((int)(export.T4s[i].EmploymentInsuranceEarnings24 * 100)).ToString(),
                    Box26CppQppPensionableEarnings = ((int)(export.T4s[i].CanadaPensionPlanEarn26 * 100)).ToString(),
                    Box18EmployeesEiPremium = ((int)(export.T4s[i].EmploymentInsuranceDeducted18 * 100)).ToString(),
                    Box44UnionDues = ((int)(export.T4s[i].UnionDues44 * 100)).ToString(),
                    Box20RppContributions = ((int)(export.T4s[i].RegisteredPension20 * 100)).ToString(),
                    Box46CharitableDonations = ((int)(export.T4s[i].CharitableDonate46 * 100)).ToString(),
                    Box52PensionAdjustment = ((int)(export.T4s[i].PensionAdjustment52 * 100)).ToString(),
                    Box50RppOrDpspRegistrationNumber = export.T4s[i].RegistrationNumber50,
                    Box55EmployeesPpipPremiums = ((int)(export.T4s[i].ProvincialParentalInsurancePlanPremiums * 100)).ToString(),
                    Box56PpipInsurableEarnings = ((int)(export.T4s[i].ProvincialParentalInsurancePlanEarnings * 100)).ToString(),

                    ActiveTerminatedEmployee = (export.T4s[i].Employee.CurrentEmployeePosition.EmployeePositionStatusCode.Substring(0, 1) == "T") ? "T" : "A"
                };

                if (export.T4s[i].T4BoxNumber1 != null)
                {
                    t4Detail[i].OtherBox1Code = ((int)export.T4s[i].T4BoxNumber1).ToString();
                    t4Detail[i].OtherBox1Amount = ((int)(export.T4s[i].T4BoxAmount1 * 100)).ToString();
                }

                if (export.T4s[i].T4BoxNumber2 != null)
                {
                    t4Detail[i].OtherBox2Code = ((int)export.T4s[i].T4BoxNumber2).ToString();
                    t4Detail[i].OtherBox2Amount = ((int)(export.T4s[i].T4BoxAmount2 * 100)).ToString();
                }

                if (export.T4s[i].T4BoxNumber3 != null)
                {
                    t4Detail[i].OtherBox3Code = ((int)export.T4s[i].T4BoxNumber3).ToString();
                    t4Detail[i].OtherBox3Amount = ((int)(export.T4s[i].T4BoxAmount3 * 100)).ToString();
                }

                if (export.T4s[i].T4BoxNumber4 != null)
                {
                    t4Detail[i].OtherBox4Code = ((int)export.T4s[i].T4BoxNumber4).ToString();
                    t4Detail[i].OtherBox4Amount = ((int)(export.T4s[i].T4BoxAmount4 * 100)).ToString();
                }

                if (export.T4s[i].T4BoxNumber5 != null)
                {
                    t4Detail[i].OtherBox5Code = ((int)export.T4s[i].T4BoxNumber5).ToString();
                    t4Detail[i].OtherBox5Amount = ((int)(export.T4s[i].T4BoxAmount5 * 100)).ToString();
                }

                if (export.T4s[i].T4BoxNumber6 != null)
                {
                    t4Detail[i].OtherBox6Code = ((int)export.T4s[i].T4BoxNumber6).ToString();
                    t4Detail[i].OtherBox6Amount = ((int)(export.T4s[i].T4BoxAmount6 * 100)).ToString();
                }
            }

            return t4Detail;
        }
        public byte[] CreateT4aCICExportFile(CanadaRevenueAgencyT4Export export)
        {
            byte[] fileContents = null;

            if (export != null && export.T4as.Count > 0)
            {
                CICT4aDetailRecord[] cicT4aDetail = CreateCicT4aDetail(export);

                //write file to string
                FileHelpers.FileHelperEngine engine = new FileHelpers.FileHelperEngine(typeof(CICT4aDetailRecord));
                String cicT4aFileString = engine.WriteString(cicT4aDetail);

                //create the file
                fileContents = convertStringToByteArray(cicT4aFileString);
            }

            return fileContents;
        }
        private CICT4aDetailRecord[] CreateCicT4aDetail(CanadaRevenueAgencyT4Export export)
        {
            CICT4aDetailRecord[] t4aDetail = new CICT4aDetailRecord[export.T4as.Count];

            for (int i = 0; i < export.T4as.Count; i++)
            {
                t4aDetail[i] = new CICT4aDetailRecord()
                {
                    TaxYear = export.T4as[i].Year.ToString(),
                    UniqueEmployeeIdentifier = export.T4as[i].EmployeeNumber,
                    AuthenticationValuePrimary = export.T4as[i].Employee.GovernmentIdentificationNumber1,
                    AuthenticationValueSecondary = Convert.ToDateTime(export.T4as[i].Employee.BirthDate),
                    RecipientSocialInsuranceNumber = export.T4as[i].Employee.GovernmentIdentificationNumber1,

                    RecipientFirstName = export.T4as[i].Employee.FirstName,
                    RecipientMiddleInitial = export.T4as[i].Employee.MiddleName,
                    RecipientLastName = export.T4as[i].Employee.LastName,

                    RecipientLocationAddresss = export.T4as[i].Address.AddressLine1,
                    RecipientDeliveryAddress = export.T4as[i].Address.AddressLine2,
                    RecipientCity = export.T4as[i].Address.City,
                    RecipientProvinceState = export.T4as[i].Address.ProvinceStateCode,
                    RecipientPostalCode = export.T4as[i].Address.PostalZipCode,
                    RecipientCountry = export.T4as[i].Address.CountryCode,

                    PayerNameLine1 = export.Employer.Name,
                    PayerLocationAddress = export.Employer.Address.AddressLine1,
                    PayerDeliveryAddress = export.Employer.Address.AddressLine2,
                    PayerCity = export.Employer.Address.City,
                    PayerProvinceState = export.Employer.Address.ProvinceStateCode,
                    PayerPostalCode = export.Employer.Address.PostalZipCode,
                    PayerCountry = export.Employer.Address.CountryCode,

                    Box061PayerAccountNumber = export.T4as[i].BusinessNumber.BusinessTaxNumber,
                    Box016PensionOrSuperannuation = ((int)(export.T4as[i].PensionSuperannuation * 100)).ToString(),
                    Box018LumpSumPayments = ((int)(export.T4as[i].LumpSumPay18 * 100)).ToString(),
                    Box020SelfEmployedCommisions = ((int)(export.T4as[i].SelfEmployedCommission * 100)).ToString(),
                    Box022IncomeTaxDeducted = ((int)(export.T4as[i].IncomeTaxDeduction22 * 100)).ToString(),
                    Box024Annuities = ((int)(export.T4as[i].Annuities * 100)).ToString(),

                    //not in 2017 spec.
                    //OtherBox026EligibleRetiringAllowances = ((int)(export.T4as[i].EligibleRetiringAllow * 100)).ToString(),
                    //OtherBox027NonEligibleRetiringAllowances = ((int)(export.T4as[i].NonEligibleRetiringAllow * 100)).ToString(),

                    OtherBox028OtherIncome = ((int)(export.T4as[i].OtherIncome * 100)).ToString(),
                    OtherBox030PatronageAllocations = ((int)(export.T4as[i].PatronageAllocation * 100)).ToString(),
                    OtherBox032RegisteredPensionPlanContributions = ((int)(export.T4as[i].RegisteredPensionPlanContributions * 100)).ToString(),
                    OtherBox034PensionAdjustment = ((int)(export.T4as[i].PensionAdjustment * 100)).ToString(),
                    OtherBox040RESPAccumulatedIncomePayments = ((int)(export.T4as[i].RespIncome * 100)).ToString(),
                    OtherBox042RESPEducationalAssistancePayments = ((int)(export.T4as[i].RespAssistance * 100)).ToString(),
                    OtherBox046CharitableDonations = ((int)(export.T4as[i].CharitableDonations46 * 100)).ToString(),
                    OtherBox048FeesForServices = ((int)(export.T4as[i].FeesForServices * 100)).ToString(),
                    OtherBox109PeriodicPaymentsFromUnregisteredPlan = ((int)(export.T4as[i].UnregisteredPlanAmount109 * 100)).ToString(),
                    OtherBox118MedicalPremiumBenefits = ((int)(export.T4as[i].MedicalPremiumBenefitsAmount118 * 100)).ToString(),
                    OtherBox119PremiumsPaidToGroupTermLifeInsurancePlan = ((int)(export.T4as[i].GroupInsurancePlanAmount119 * 100)).ToString(),
                    OtherBox128VeteransBenefitsEligibleForPensionSplitting = ((int)(export.T4as[i].VeteransBenefitsEligibleForPensionSplitting128 * 100)).ToString(),
                    OtherBox190LumpSumPaymentsFromUnregisteredPlan = ((int)(export.T4as[i].LumpSumPaymentsFromUnregisteredPlan190 * 100)).ToString(),

                    ActiveTerminatedEmployee = (export.T4as[i].Employee.CurrentEmployeePosition.EmployeePositionStatusCode.Substring(0, 1) == "T") ? "T" : "A"
                };
            }

            return t4aDetail;
        }
        public byte[] CreateR1CICExportFile(RevenueQuebecR1Export export)
        {
            byte[] fileContents = null;

            if (export != null && export.R1s.Count > 0)
            {
                CICR1DetailRecord[] cicR1Detail = CreateCicR1Detail(export);

                //write file to string
                FileHelpers.FileHelperEngine engine = new FileHelpers.FileHelperEngine(typeof(CICR1DetailRecord));
                String cicR1FileString = engine.WriteString(cicR1Detail);

                //create the file
                fileContents = convertStringToByteArray(cicR1FileString);
            }

            return fileContents;
        }
        private CICR1DetailRecord[] CreateCicR1Detail(RevenueQuebecR1Export export)
        {
            CICR1DetailRecord[] r1Detail = new CICR1DetailRecord[export.R1s.Count];

            for (int i = 0; i < export.R1s.Count; i++)
            {
                r1Detail[i] = new CICR1DetailRecord()
                {
                    TaxYear = export.R1s[i].Year.ToString(),
                    UniqueEmployeeIdentifier = export.R1s[i].EmployeeNumber,
                    AuthenticationValuePrimary = export.R1s[i].Employee.GovernmentIdentificationNumber1,
                    AuthenticationValueSecondary = Convert.ToDateTime(export.R1s[i].Employee.BirthDate),
                    EmployeeSocialInsuranceNumber = export.R1s[i].Employee.GovernmentIdentificationNumber1,

                    EmployeeFirstName = export.R1s[i].Employee.FirstName,
                    EmployeeMiddleInitial = export.R1s[i].Employee.MiddleName,
                    EmployeeLastName = export.R1s[i].Employee.LastName,
                    EmployeeSuffix = export.R1s[i].Employee.TitleCode,

                    EmployeeLocationAddress = export.R1s[i].Address.AddressLine1,
                    EmployeeDeliveryAddress = export.R1s[i].Address.AddressLine2,
                    EmployeeCity = export.R1s[i].Address.City,
                    EmployeeProvinceState = export.R1s[i].Address.ProvinceStateCode,
                    EmployeePostalCode = export.R1s[i].Address.PostalZipCode,
                    EmployeeCountry = export.R1s[i].Address.CountryCode,

                    EmployerNameLine1 = export.Employer.Name,
                    EmployerLocationAddress = export.Employer.Address.AddressLine1,
                    EmployerDeliveryAddress = export.Employer.Address.AddressLine2,
                    EmployerCity = export.Employer.Address.City,
                    EmployerProvinceState = export.Employer.Address.ProvinceStateCode,
                    EmployerPostalCode = export.Employer.Address.PostalZipCode,
                    EmployerCountry = export.Employer.Address.CountryCode,

                    BoxAEmploymentIncomeBeforeSourceDeductions = ((int)(export.R1s[i].BoxAAmount * 100)).ToString(),
                    BoxBContributionsToQpp = ((int)(export.R1s[i].BoxBAmount * 100)).ToString(),
                    BoxCEmploymentInsurancePremiums = ((int)(export.R1s[i].BoxCAmount * 100)).ToString(),
                    BoxDContributionsToRpp = ((int)(export.R1s[i].BoxDAmount * 100)).ToString(),
                    BoxEQuebecIncomeTaxWithheldAtSource = ((int)(export.R1s[i].BoxEAmount * 100)).ToString(),
                    BoxFUnionAndProfessionalDues = ((int)(export.R1s[i].BoxFAmount * 100)).ToString(),
                    BoxGQppPensionEarnings = ((int)(export.R1s[i].BoxGAmount * 100)).ToString(),
                    BoxHQpip = ((int)(export.R1s[i].BoxHAmount * 100)).ToString(),
                    BoxIInsuranceSalaryAndWagesUnderQpip = ((int)(export.R1s[i].BoxIAmount * 100)).ToString(),
                    BoxJContributionsPaidByEmployerUnderPrivateHealthServicesPlan = ((int)(export.R1s[i].BoxJAmount * 100)).ToString(),
                    BoxKTripsMadeByAResidentOfADesignatedRemoteArea = ((int)(export.R1s[i].BoxKAmount * 100)).ToString(),
                    BoxLOtherBenefits = ((int)(export.R1s[i].BoxLAmount * 100)).ToString(),
                    BoxMCommissions = ((int)(export.R1s[i].BoxMAmount * 100)).ToString(),
                    BoxNCharitableDonations = ((int)(export.R1s[i].BoxNAmount * 100)).ToString(),
                    BoxPContributionsToAMultiEmployerInsurancePlan = ((int)(export.R1s[i].BoxPAmount * 100)).ToString(),
                    BoxQDeferredSalaryOrWages = ((int)(export.R1s[i].BoxQAmount * 100)).ToString(),
                    BoxRTaxExemptIncomeEarnedOnAReserve = ((int)(export.R1s[i].BoxRAmount * 100)).ToString(),
                    BoxSTipsReceivedNotIncludedInBoxT = ((int)(export.R1s[i].BoxSAmount * 100)).ToString(),
                    BoxTTipsAllocatedByTheEmployer = ((int)(export.R1s[i].BoxTAmount * 100)).ToString(),
                    BoxUPhasedRetirement = ((int)(export.R1s[i].BoxUAmount * 100)).ToString(),
                    BoxVMealsAndAccomodation = ((int)(export.R1s[i].BoxVAmount * 100)).ToString(),
                    BoxWUseOfMotorVehicleForPersonalPurposes = ((int)(export.R1s[i].BoxWAmount * 100)).ToString(),

                    A12DeductionForForeignExperts = ((int)(export.R1s[i].BoxA12Amount * 100)).ToString(),
                    L8SecurityOptionsElection = ((int)(export.R1s[i].BoxL8Amount * 100)).ToString(),
                    L9DeductionsForStockOptionUnderSection725_2OfTheIncomeTaxAct = ((int)(export.R1s[i].BoxL9Amount * 100)).ToString(),
                    RetiringAllowance = (export.R1s[i].BoxORJAmount != null) ? ((int)(export.R1s[i].BoxORJAmount * 100)).ToString() : null,
                    AmountsAllocatedUnderARetirementCompensationAgreement = (export.R1s[i].BoxORQAmount != null) ? ((int)(export.R1s[i].BoxORQAmount * 100)).ToString() : null,
                    x235ContributionPaidByTheEmployeeToPrivateHealthInsurancePlan = (export.R1s[i].Box235Amount != null) ? ((int)(export.R1s[i].Box235Amount * 100)).ToString() : null,
                    x201ChildCareExpensesFromTheMinistereDeLEmploiDeLaSolidarite = (export.R1s[i].Box201Amount != null) ? ((int)(export.R1s[i].Box201Amount * 100)).ToString() : null,
                    x211BenefitRelatedToPreviousEmployment = (export.R1s[i].Box211Amount != null) ? ((int)(export.R1s[i].Box211Amount * 100)).ToString() : null,
                    SequenceNumber = export.R1s[i].SlipNumber.ToString(), //xml sequence number
                    QcBusinessNumber = export.Employer.RevenueQuebecTaxId,
                    EmployeeLanguagePreference = (export.R1s[i].Employee.LanguageCode != null) ? export.R1s[i].Employee.LanguageCode.Substring(0, 1) : " ",
                    ActiveTerminatedEmployee = (export.R1s[i].Employee.CurrentEmployeePosition.EmployeePositionStatusCode.Substring(0, 1) == "T") ? "T" : "A"
                };
            }

            return r1Detail;
        }
        #endregion

        private String WriteEmpowerFile(EmpowerRemittanceHeaderRecord[] headers, FileHelpers.MultiRecordEngine eng)
        {
            System.Text.StringBuilder fileData = new System.Text.StringBuilder();

            foreach (EmpowerRemittanceHeaderRecord header in headers)
            {
                fileData.Append(eng.WriteString(new EmpowerRemittanceHeaderRecord[] { header }));

                foreach (EmpowerRemittanceWageRecord wage in header.WageRecords)
                {
                    fileData.Append(eng.WriteString(new EmpowerRemittanceWageRecord[] { wage }));
                    fileData.Append(eng.WriteString(wage.TaxRecords));
                }
            }

            return fileData.ToString();
        }
        private EmpowerRemittanceTaxRecord[] PopulateTax(PayDetailCollection details)
        {
            List<EmpowerRemittanceTaxRecord> taxOutputs = new List<EmpowerRemittanceTaxRecord>();

            foreach (String externalCode in details.UniqueExternalCodes)
            {
                PayDetailCollection paycodeCollection = details.GetPayDetailByExternalCode(externalCode);
                EmpowerRemittanceTaxRecord record = new EmpowerRemittanceTaxRecord();
                Decimal total = paycodeCollection.TotalCurrentAmount;

                if (total != 0)
                {
                    if (total == 0)
                        record.TaxAmount = "".PadLeft(14);
                    else
                        record.TaxAmount = String.Format("{0:0.00}", total).PadLeft(14);

                    record.TranslationCode = externalCode.PadRight(40);

                    taxOutputs.Add(record);
                }
            }

            return taxOutputs.ToArray();
        }
        private EmpowerRemittanceWageRecord[] PopulateWage(PayDetailCollection details)
        {
            List<EmpowerRemittanceWageRecord> wageOutputs = new List<EmpowerRemittanceWageRecord>();

            foreach (String externalCodeFirst7Chars in details.UniqueExternalCodeFirst7Chars)
            {
                //extract wage subset
                PayDetailCollection wages = details.GetPayDetailByExternalCodeFirst7Chars(externalCodeFirst7Chars);

                EmpowerRemittanceWageRecord record = new EmpowerRemittanceWageRecord();
                record.TaxType = "".PadRight(10); //externalCode.PadRight(10); commented out as per wlinks904
                Decimal grossWages = wages.TotalEmployeeGrossAmount;
                record.GrossWages = grossWages == 0 ? "".PadLeft(14) : String.Format("{0:0.00}", grossWages).PadLeft(14); //this has 5 digits after the decimal so remove those, pad with spaces
                record.SubjectWages = record.GrossWages;
                record.TaxableWages = record.GrossWages;
                record.TaxRecords = PopulateTax(wages);

                wageOutputs.Add(record);
            }

            return wageOutputs.ToArray();
        }
        private EmpowerRemittanceHeaderRecord[] PopulateHeader(DatabaseUser user, long payrollProcessId, PayDetailCollection nonQuebecPayDetails, PayDetailCollection quebecPayDetails, String quebecTaxId, DateTime chequeDate, bool useQuebecTaxId)
        {
            List<EmpowerRemittanceHeaderRecord> rtn = new List<EmpowerRemittanceHeaderRecord>();

            //create quebec record, if exists
            if (quebecPayDetails.Count > 0)
            {
                foreach (String employerNumber in (useQuebecTaxId ? new List<String>() { quebecTaxId } : quebecPayDetails.UniqueEmployerNumbers))
                {
                    foreach (String externalCodeFirst3Chars in (useQuebecTaxId ? quebecPayDetails : quebecPayDetails.GetPayDetailByEmployerNumber(employerNumber)).UniqueExternalCodeFirst3Chars)
                    {
                        PayDetailCollection details = (useQuebecTaxId ? quebecPayDetails : quebecPayDetails.GetPayDetailByEmployerNumber(employerNumber)).GetPayDetailByExternalCodeFirst3Chars(externalCodeFirst3Chars);
                        EmpowerRemittanceHeaderRecord record = new EmpowerRemittanceHeaderRecord();

                        record.PayrollId = /*payrollProcessId.ToString()*/String.Empty.PadRight(15);
                        record.UnitId = "               "; //"2".PadRight(15); commented out as per wlinks904
                        record.PayDate = chequeDate;
                        record.EmployeeCount = details.UniqueEmployeeCount.ToString().PadLeft(8);
                        record.CompanyId = useQuebecTaxId ? quebecTaxId.PadRight(29) : _sqlServerBusinessNumber.SelectByEmployerNumber(user, employerNumber).PadRight(29);
                        record.WageRecords = PopulateWage(details);

                        rtn.Add(record);
                    }
                }
            }

            //create federal records, if exists
            foreach (String employerNumber in nonQuebecPayDetails.UniqueEmployerNumbers)
            {
                foreach (String externalCodeFirst3Chars in nonQuebecPayDetails.GetPayDetailByEmployerNumber(employerNumber).UniqueExternalCodeFirst3Chars)
                {
                    PayDetailCollection details = nonQuebecPayDetails.GetPayDetailByEmployerNumber(employerNumber).GetPayDetailByExternalCodeFirst3Chars(externalCodeFirst3Chars);
                    EmpowerRemittanceHeaderRecord record = new EmpowerRemittanceHeaderRecord();

                    record.PayrollId = /*payrollProcessId.ToString()*/String.Empty.PadRight(15);
                    record.UnitId = "".PadRight(15); //"1".PadRight(15); commented out as per wlinks904
                    record.PayDate = chequeDate;
                    record.EmployeeCount = details.UniqueEmployeeCount.ToString().PadLeft(8);
                    record.CompanyId = _sqlServerBusinessNumber.SelectByEmployerNumber(user, employerNumber).PadRight(29);
                    record.WageRecords = PopulateWage(details);

                    rtn.Add(record);
                }
            }

            return rtn.ToArray();
        }
        public byte[] CreateGarnishmentExport(DatabaseUser user, PayrollMasterPaycodeCollection payrollMasterPaycodeColl, PayrollMasterCollection calcMasterColl, String empowerFein)
        {
            byte[] fileContents = null;

            if (payrollMasterPaycodeColl != null && payrollMasterPaycodeColl.Count > 0) //only create garnishment export if we have rows to process
            {
                //match garnishment records (mpd) to calc master records (dcm) and create a new object
                var garnishmentRecordsInPayrollMaster =
                    from payrollMasterPaycodeRec in payrollMasterPaycodeColl
                    join calcMasterRecord in calcMasterColl on payrollMasterPaycodeRec.EmployeeId equals calcMasterRecord.EmployeeId
                    orderby payrollMasterPaycodeRec.EmployeeId
                    select new
                    {
                        EmployeeNumber = payrollMasterPaycodeRec.EmployeeNumber.PadRight(15),
                        SIN = "",
                        PayDate = calcMasterRecord.ChequeDate,
                        PayrollId = Convert.ToString(calcMasterRecord.PayrollProcessId).PadRight(35),
                        AttachmentCodeX = payrollMasterPaycodeRec.CodePaycodeCd.PadRight(6),
                        //if negative, add leading "-".  In both cases only use 2 decimal places, not 4, and PadLeft to 11 spots
                        AttachmentAmountX = (payrollMasterPaycodeRec.Amount < 0)
                            ? ("-" + payrollMasterPaycodeRec.Amount.ToString().Substring(0, payrollMasterPaycodeRec.Amount.ToString().Length - 2)).PadLeft(11)
                            : payrollMasterPaycodeRec.Amount.ToString().Substring(0, payrollMasterPaycodeRec.Amount.ToString().Length - 2).PadLeft(11)
                    };

                //get distinct number of employee numbers in payrollMasterPaycodeColl
                int numberOfDistinctEmployees = garnishmentRecordsInPayrollMaster.Select(x => x.EmployeeNumber).Distinct().Count();

                //declare the garnishment class for the export, create array, then instantiate the object
                EmpowerGarnishmentRecord[] garnRecord = new EmpowerGarnishmentRecord[numberOfDistinctEmployees];
                int i = -1;
                String SIN = "";
                String prevEmployeeNumber = "";

                foreach (var record in garnishmentRecordsInPayrollMaster)
                {
                    if (prevEmployeeNumber != record.EmployeeNumber)
                    {
                        i++;

                        //get social insurance number from person based on employee number
                        SIN = _sqlServerPerson.SelectByEmployeeNumber(user, record.EmployeeNumber)[0].GovernmentIdentificationNumber1;

                        garnRecord[i] = new EmpowerGarnishmentRecord()
                        {
                            FEIN = empowerFein.PadRight(9),
                            EmployeeID = record.EmployeeNumber,
                            SSN = SIN,
                            PayDate = record.PayDate,
                            PayrollID = record.PayrollId,
                            AttachmentCode1 = record.AttachmentCodeX,
                            AttachmentAmount1 = record.AttachmentAmountX
                        };
                    }
                    else //same employee, add garnishment to current record
                    {
                        if (garnRecord[i].AttachmentCode2.Trim().Length == 0)
                        {
                            garnRecord[i].AttachmentCode2 = record.AttachmentCodeX;
                            garnRecord[i].AttachmentAmount2 = record.AttachmentAmountX;
                        }
                        else if (garnRecord[i].AttachmentCode3.Trim().Length == 0)
                        {
                            garnRecord[i].AttachmentCode3 = record.AttachmentCodeX;
                            garnRecord[i].AttachmentAmount3 = record.AttachmentAmountX;
                        }
                        else if (garnRecord[i].AttachmentCode4.Trim().Length == 0)
                        {
                            garnRecord[i].AttachmentCode4 = record.AttachmentCodeX;
                            garnRecord[i].AttachmentAmount4 = record.AttachmentAmountX;
                        }
                        else if (garnRecord[i].AttachmentCode5.Trim().Length == 0)
                        {
                            garnRecord[i].AttachmentCode5 = record.AttachmentCodeX;
                            garnRecord[i].AttachmentAmount5 = record.AttachmentAmountX;
                        }
                        else if (garnRecord[i].AttachmentCode6.Trim().Length == 0)
                        {
                            garnRecord[i].AttachmentCode6 = record.AttachmentCodeX;
                            garnRecord[i].AttachmentAmount6 = record.AttachmentAmountX;
                        }
                        else if (garnRecord[i].AttachmentCode7.Trim().Length == 0)
                        {
                            garnRecord[i].AttachmentCode7 = record.AttachmentCodeX;
                            garnRecord[i].AttachmentAmount7 = record.AttachmentAmountX;
                        }
                        else if (garnRecord[i].AttachmentCode8.Trim().Length == 0)
                        {
                            garnRecord[i].AttachmentCode8 = record.AttachmentCodeX;
                            garnRecord[i].AttachmentAmount8 = record.AttachmentAmountX;
                        }
                        else if (garnRecord[i].AttachmentCode9.Trim().Length == 0)
                        {
                            garnRecord[i].AttachmentCode9 = record.AttachmentCodeX;
                            garnRecord[i].AttachmentAmount9 = record.AttachmentAmountX;
                        }
                        else if (garnRecord[i].AttachmentCode10.Trim().Length == 0)
                        {
                            garnRecord[i].AttachmentCode10 = record.AttachmentCodeX;
                            garnRecord[i].AttachmentAmount10 = record.AttachmentAmountX;
                        }
                        else if (garnRecord[i].AttachmentCode20.Trim().Length == 0)
                        {
                            garnRecord[i].AttachmentCode20 = record.AttachmentCodeX;
                            garnRecord[i].AttachmentAmount20 = record.AttachmentAmountX;
                        }
                    }

                    prevEmployeeNumber = record.EmployeeNumber;
                }

                FileHelpers.FileHelperEngine engine = new FileHelpers.FileHelperEngine(typeof(EmpowerGarnishmentRecord));
                String garnishmentFileString = engine.WriteString(garnRecord);

                //create the garnishment file as a byte array
                fileContents = convertStringToByteArray(garnishmentFileString);
            }

            return fileContents;
        }
        public byte[] CreateRRSPExport(DatabaseUser user, PayrollMasterPaycodeCollection collection, String rrspHeaderText, String rrspGroupNumber)
        {
            byte[] fileContents = null;

            if (collection != null && collection.Count > 0) //only create rrsp export if we have rows to process
            {
                //declare the RRSP classes (sections) for the RRSP export, create arrays, then instantiate the objects
                RRSPHeaderRecord[] rrspHeader = new RRSPHeaderRecord[1];
                RRSPDetailRecord[] rrspDetail = new RRSPDetailRecord[collection.Count]; //create array for multiple entry records
                RRSPTrailerRecord[] rrspTrailer = new RRSPTrailerRecord[1];

                //instantiate the RRSP classes (sections) for the RRSP export
                rrspHeader[0] = new RRSPHeaderRecord() { HeaderType = String.Format(rrspHeaderText, rrspGroupNumber) };

                //loop for multiple entry records
                Decimal totalAmount = 0;

                for (int i = 0; i < collection.Count; i++)
                {
                    rrspDetail[i] = new RRSPDetailRecord() { GroupNumber = rrspGroupNumber };
                    totalAmount += collection[i].Amount;
                }

                rrspTrailer[0] = new RRSPTrailerRecord() { GroupNumber = rrspGroupNumber };

                //populate fields in the RRSP detail class
                rrspDetail = populateRRSPDetail(user, rrspDetail, collection);

                //populate the total deposit amount, remove decimal, pad left with 0's
                rrspTrailer[0].DepositAmount = Convert.ToString(Math.Round(totalAmount, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(10, '0');

                FileHelpers.MultiRecordEngine engine = null;
                String rrspFileString = null;

                engine = new FileHelpers.MultiRecordEngine(typeof(RRSPHeaderRecord), typeof(RRSPDetailRecord), typeof(RRSPTrailerRecord));
                rrspFileString = writeRRSPFile(engine, rrspHeader, rrspDetail, rrspTrailer);

                //create the rrsp file
                fileContents = convertStringToByteArray(rrspFileString);
            }

            return fileContents;
        }
        private String writeRRSPFile(FileHelpers.MultiRecordEngine eng, RRSPHeaderRecord[] rrspHead, RRSPDetailRecord[] rrspDetail, RRSPTrailerRecord[] rrspTrailer)
        {
            String fileData = eng.WriteString(rrspHead);
            fileData += eng.WriteString(rrspDetail);
            fileData += eng.WriteString(rrspTrailer);

            return fileData;
        }
        private RRSPDetailRecord[] populateRRSPDetail(DatabaseUser user, RRSPDetailRecord[] rrspDetail, PayrollMasterPaycodeCollection collection)
        {
            for (int i = 0; i < collection.Count; i++)
            {
                PersonCollection temp = _sqlServerPerson.SelectByEmployeeNumber(user, collection[i].EmployeeNumber);

                rrspDetail[i].EmployeeNumber = temp[0].GovernmentIdentificationNumber1.Replace("-", ""); //use SIN, remove any dashes that may exist
                rrspDetail[i].DepositAmount = Convert.ToString(Math.Round(collection[i].Amount, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(10, '0'); //10 chars, remove decimal, pad left with 0's

                String employeeName = temp[0].LastName.ToUpper() + ", " + temp[0].FirstName.ToUpper();
                rrspDetail[i].EmployeeName = (employeeName).Substring(0, (employeeName).Length <= 20 ? (employeeName).Length : 20).PadRight(20); //max 20 chars, lastname + comma + space + firstname
            }

            return rrspDetail;
        }

        #region HSBC eft export

        public byte[] CreateHSBCEft(DatabaseUser user, long payrollProcessId, string employeeNumber)
        {
            byte[] fileContents = null;
            HSBCEftPayrollMasterPaymentCollection collection = _sqlServerHSBCEftPayrollMasterPayment.Select(user, payrollProcessId, employeeNumber);

            if (collection != null && collection.Count > 0)
            {
                //get sequence
                long sequenceNumber = GetSequenceNumber(user, "HSBC_HEADER_SEQUENCE");

                //declare the EFT class for the EFT file, create arrays, then instantiate the object
                REALEftBankingHeader[] hsbcEftHeader = new REALEftBankingHeader[1];
                hsbcEftHeader[0] = new REALEftBankingHeader() { EFTOriginatorID = collection[0].EftOriginatorId, FileCreationNumber = Convert.ToInt32(sequenceNumber) };

                REALEftBankingDetailDebitCreditRecord[] hsbcEftDetails = createHsbcDetailRecord(collection, sequenceNumber);

                REALEftBankingTrailer[] hsbcEftTrailer = new REALEftBankingTrailer[1];
                hsbcEftTrailer[0] = new REALEftBankingTrailer()
                {
                    RecordSequenceCount = hsbcEftDetails.Length + 2,
                    EFTOriginatorID = collection[0].EftOriginatorId,
                    FileCreationNumber = hsbcEftHeader[0].FileCreationNumber,
                    ValueOfCredits = Convert.ToString(Math.Round(collection.SumPaymentAmount, 2, MidpointRounding.AwayFromZero)).Replace(".", ""),
                    NumberOfCredits = collection.Count.ToString()
                };

                //write the file
                FileHelpers.MultiRecordEngine engine = new FileHelpers.MultiRecordEngine(typeof(REALEftBankingHeader), typeof(REALEftBankingDetailDebitCreditRecord), typeof(REALEftBankingTrailer));
                String eftFileString = WriteHsbcFile(engine, hsbcEftHeader, hsbcEftDetails, hsbcEftTrailer);

                //convert to byte array
                fileContents = convertStringToByteArray(eftFileString);
            }

            return fileContents;
        }

        private String WriteHsbcFile(FileHelpers.MultiRecordEngine eng, REALEftBankingHeader[] hsbcEftHeader, REALEftBankingDetailDebitCreditRecord[] hsbcEftDetails, REALEftBankingTrailer[] hsbcEftTrailer)
        {
            System.Text.StringBuilder fileData = new System.Text.StringBuilder();
            fileData.Append(eng.WriteString(hsbcEftHeader));
            fileData.Append(eng.WriteString(hsbcEftDetails));
            fileData.Append(eng.WriteString(hsbcEftTrailer));

            return fileData.ToString();
        }

        public REALEftBankingDetailDebitCreditRecord[] createHsbcDetailRecord(HSBCEftPayrollMasterPaymentCollection coll, long sequenceNumber)
        {
            /* each detail class can hold up to 6 transactions so determine how many classes to create */
            //number of classes with 6 segments populated + determine if we need to create a class to hold partial segments
            int numOfClasses = (coll.Count / 6) + (coll.Count % 6 != 0 ? 1 : 0);

            REALEftBankingDetailDebitCreditRecord[] hsbcEftDetailFile = new REALEftBankingDetailDebitCreditRecord[numOfClasses];
            int arrayIndex = 0;
            int RecordSequenceCount = 2; //header record starts at 1, so this starts at 2

            for (int i = 0; i < coll.Count; i += 6)
            {
                //instantiate object
                hsbcEftDetailFile[arrayIndex] = new REALEftBankingDetailDebitCreditRecord();

                //assign values to the 3 static fields in the record
                hsbcEftDetailFile[arrayIndex].RecordSequenceCount = RecordSequenceCount++;                   //Increment by 1 from field 02 in the Header record
                hsbcEftDetailFile[arrayIndex].EFTOriginatorID = coll[i].EftOriginatorId;
                hsbcEftDetailFile[arrayIndex].FileCreationNumber = Convert.ToInt32(sequenceNumber);          //Must be the same as field 04 in the Header record

                /* loop thru the collection 6 records at a time to poplulate the 6 segments */
                //first segment will exist
                hsbcEftDetailFile[arrayIndex].CPACode1 = "200";
                hsbcEftDetailFile[arrayIndex].Amount1 = Convert.ToString(Math.Round(coll[i].ChequeAmount, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(10, '0');
                hsbcEftDetailFile[arrayIndex].DueDateCentury1 = "0";
                hsbcEftDetailFile[arrayIndex].DueDateYear1 = coll[i].ChequeDate;
                hsbcEftDetailFile[arrayIndex].DueDateDay1 = coll[i].ChequeDate.DayOfYear.ToString().PadLeft(3, '0');
                hsbcEftDetailFile[arrayIndex].FinancialInstitutionNumber1 = String.Format("0{0}", coll[i].BankCode.PadLeft(3, '0'));
                hsbcEftDetailFile[arrayIndex].FinancialInstitutionBranchNumber1 = coll[i].BankTransitNumber;
                hsbcEftDetailFile[arrayIndex].AccountNumber1 = coll[i].BankAccountNumber;
                hsbcEftDetailFile[arrayIndex].ItemTraceNumber1 = "".PadLeft(22, '0');
                hsbcEftDetailFile[arrayIndex].StoredTransType1 = "".PadLeft(3, '0');
                hsbcEftDetailFile[arrayIndex].ShortName1 = coll[i].CompanyShortName;
                hsbcEftDetailFile[arrayIndex].TransactionPayeePayorName1 = coll[i].EmployeeName;
                hsbcEftDetailFile[arrayIndex].LongName1 = coll[i].CompanyLongName;
                hsbcEftDetailFile[arrayIndex].EFTOriginatorIDCopy1 = coll[i].EftOriginatorId;
                hsbcEftDetailFile[arrayIndex].InvalidDataElement1 = "".PadLeft(11, '0');

                //create segment2 if there's data
                if (coll.Count > i + 1)
                {
                    hsbcEftDetailFile[arrayIndex].CPACode2 = "200";
                    hsbcEftDetailFile[arrayIndex].Amount2 = Convert.ToString(Math.Round(coll[i + 1].ChequeAmount, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(10, '0');
                    hsbcEftDetailFile[arrayIndex].DueDateCentury2 = "0";
                    hsbcEftDetailFile[arrayIndex].DueDateYear2 = coll[i + 1].ChequeDate;
                    hsbcEftDetailFile[arrayIndex].DueDateDay2 = coll[i + 1].ChequeDate.DayOfYear.ToString().PadLeft(3, '0');
                    hsbcEftDetailFile[arrayIndex].FinancialInstitutionNumber2 = String.Format("0{0}", coll[i + 1].BankCode.PadLeft(3, '0'));
                    hsbcEftDetailFile[arrayIndex].FinancialInstitutionBranchNumber2 = coll[i + 1].BankTransitNumber;
                    hsbcEftDetailFile[arrayIndex].AccountNumber2 = coll[i + 1].BankAccountNumber;
                    hsbcEftDetailFile[arrayIndex].ItemTraceNumber2 = "".PadLeft(22, '0');
                    hsbcEftDetailFile[arrayIndex].StoredTransType2 = "".PadLeft(3, '0');
                    hsbcEftDetailFile[arrayIndex].ShortName2 = coll[i + 1].CompanyShortName;
                    hsbcEftDetailFile[arrayIndex].TransactionPayeePayorName2 = coll[i + 1].EmployeeName;
                    hsbcEftDetailFile[arrayIndex].LongName2 = coll[i + 1].CompanyLongName;
                    hsbcEftDetailFile[arrayIndex].EFTOriginatorIDCopy2 = coll[i + 1].EftOriginatorId;
                    hsbcEftDetailFile[arrayIndex].InvalidDataElement2 = "".PadLeft(11, '0');
                }
                else
                    break;      //done processing, so skip the other IFs

                //create segment3 if there's data
                if (coll.Count > i + 2)
                {
                    hsbcEftDetailFile[arrayIndex].CPACode3 = "200";
                    hsbcEftDetailFile[arrayIndex].Amount3 = Convert.ToString(Math.Round(coll[i + 2].ChequeAmount, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(10, '0');
                    hsbcEftDetailFile[arrayIndex].DueDateCentury3 = "0";
                    hsbcEftDetailFile[arrayIndex].DueDateYear3 = coll[i + 2].ChequeDate;
                    hsbcEftDetailFile[arrayIndex].DueDateDay3 = coll[i + 2].ChequeDate.DayOfYear.ToString().PadLeft(3, '0');
                    hsbcEftDetailFile[arrayIndex].FinancialInstitutionNumber3 = String.Format("0{0}", coll[i + 2].BankCode.PadLeft(3, '0'));
                    hsbcEftDetailFile[arrayIndex].FinancialInstitutionBranchNumber3 = coll[i + 2].BankTransitNumber;
                    hsbcEftDetailFile[arrayIndex].AccountNumber3 = coll[i + 2].BankAccountNumber;
                    hsbcEftDetailFile[arrayIndex].ItemTraceNumber3 = "".PadLeft(22, '0');
                    hsbcEftDetailFile[arrayIndex].StoredTransType3 = "".PadLeft(3, '0');
                    hsbcEftDetailFile[arrayIndex].ShortName3 = coll[i + 2].CompanyShortName;
                    hsbcEftDetailFile[arrayIndex].TransactionPayeePayorName3 = coll[i + 2].EmployeeName;
                    hsbcEftDetailFile[arrayIndex].LongName3 = coll[i + 2].CompanyLongName;
                    hsbcEftDetailFile[arrayIndex].EFTOriginatorIDCopy3 = coll[i + 2].EftOriginatorId;
                    hsbcEftDetailFile[arrayIndex].InvalidDataElement3 = "".PadLeft(11, '0');
                }
                else
                    break;      //done processing, so skip the other IFs

                //create segment4 if there's data
                if (coll.Count > i + 3)
                {
                    hsbcEftDetailFile[arrayIndex].CPACode4 = "200";
                    hsbcEftDetailFile[arrayIndex].Amount4 = Convert.ToString(Math.Round(coll[i + 3].ChequeAmount, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(10, '0');
                    hsbcEftDetailFile[arrayIndex].DueDateCentury4 = "0";
                    hsbcEftDetailFile[arrayIndex].DueDateYear4 = coll[i + 3].ChequeDate;
                    hsbcEftDetailFile[arrayIndex].DueDateDay4 = coll[i + 3].ChequeDate.DayOfYear.ToString().PadLeft(3, '0');
                    hsbcEftDetailFile[arrayIndex].FinancialInstitutionNumber4 = String.Format("0{0}", coll[i + 3].BankCode.PadLeft(3, '0'));
                    hsbcEftDetailFile[arrayIndex].FinancialInstitutionBranchNumber4 = coll[i + 3].BankTransitNumber;
                    hsbcEftDetailFile[arrayIndex].AccountNumber4 = coll[i + 3].BankAccountNumber;
                    hsbcEftDetailFile[arrayIndex].ItemTraceNumber4 = "".PadLeft(22, '0');
                    hsbcEftDetailFile[arrayIndex].StoredTransType4 = "".PadLeft(3, '0');
                    hsbcEftDetailFile[arrayIndex].ShortName4 = coll[i + 3].CompanyShortName;
                    hsbcEftDetailFile[arrayIndex].TransactionPayeePayorName4 = coll[i + 3].EmployeeName;
                    hsbcEftDetailFile[arrayIndex].LongName4 = coll[i + 3].CompanyLongName;
                    hsbcEftDetailFile[arrayIndex].EFTOriginatorIDCopy4 = coll[i + 3].EftOriginatorId;
                    hsbcEftDetailFile[arrayIndex].InvalidDataElement4 = "".PadLeft(11, '0');
                }
                else
                    break;      //done processing, so skip the other IFs

                //create segment5 if there's data
                if (coll.Count > i + 4)
                {
                    hsbcEftDetailFile[arrayIndex].CPACode5 = "200";
                    hsbcEftDetailFile[arrayIndex].Amount5 = Convert.ToString(Math.Round(coll[i + 4].ChequeAmount, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(10, '0');
                    hsbcEftDetailFile[arrayIndex].DueDateCentury5 = "0";
                    hsbcEftDetailFile[arrayIndex].DueDateYear5 = coll[i + 4].ChequeDate;
                    hsbcEftDetailFile[arrayIndex].DueDateDay5 = coll[i + 4].ChequeDate.DayOfYear.ToString().PadLeft(3, '0');
                    hsbcEftDetailFile[arrayIndex].FinancialInstitutionNumber5 = String.Format("0{0}", coll[i + 4].BankCode.PadLeft(3, '0'));
                    hsbcEftDetailFile[arrayIndex].FinancialInstitutionBranchNumber5 = coll[i + 4].BankTransitNumber;
                    hsbcEftDetailFile[arrayIndex].AccountNumber5 = coll[i + 4].BankAccountNumber;
                    hsbcEftDetailFile[arrayIndex].ItemTraceNumber5 = "".PadLeft(22, '0');
                    hsbcEftDetailFile[arrayIndex].StoredTransType5 = "".PadLeft(3, '0');
                    hsbcEftDetailFile[arrayIndex].ShortName5 = coll[i + 4].CompanyShortName;
                    hsbcEftDetailFile[arrayIndex].TransactionPayeePayorName5 = coll[i + 4].EmployeeName;
                    hsbcEftDetailFile[arrayIndex].LongName5 = coll[i + 4].CompanyLongName;
                    hsbcEftDetailFile[arrayIndex].EFTOriginatorIDCopy5 = coll[i + 4].EftOriginatorId;
                    hsbcEftDetailFile[arrayIndex].InvalidDataElement5 = "".PadLeft(11, '0');
                }
                else
                    break;      //done processing, so skip the other IFs

                //create segment6 if there's data
                if (coll.Count > i + 5)
                {
                    hsbcEftDetailFile[arrayIndex].CPACode6 = "200";
                    hsbcEftDetailFile[arrayIndex].Amount6 = Convert.ToString(Math.Round(coll[i + 5].ChequeAmount, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(10, '0');
                    hsbcEftDetailFile[arrayIndex].DueDateCentury6 = "0";
                    hsbcEftDetailFile[arrayIndex].DueDateYear6 = coll[i + 5].ChequeDate;
                    hsbcEftDetailFile[arrayIndex].DueDateDay6 = coll[i + 5].ChequeDate.DayOfYear.ToString().PadLeft(3, '0');
                    hsbcEftDetailFile[arrayIndex].FinancialInstitutionNumber6 = String.Format("0{0}", coll[i + 5].BankCode.PadLeft(3, '0'));
                    hsbcEftDetailFile[arrayIndex].FinancialInstitutionBranchNumber6 = coll[i + 5].BankTransitNumber;
                    hsbcEftDetailFile[arrayIndex].AccountNumber6 = coll[i + 5].BankAccountNumber;
                    hsbcEftDetailFile[arrayIndex].ItemTraceNumber6 = "".PadLeft(22, '0');
                    hsbcEftDetailFile[arrayIndex].StoredTransType6 = "".PadLeft(3, '0');
                    hsbcEftDetailFile[arrayIndex].ShortName6 = coll[i + 5].CompanyShortName;
                    hsbcEftDetailFile[arrayIndex].TransactionPayeePayorName6 = coll[i + 5].EmployeeName;
                    hsbcEftDetailFile[arrayIndex].LongName6 = coll[i + 5].CompanyLongName;
                    hsbcEftDetailFile[arrayIndex].EFTOriginatorIDCopy6 = coll[i + 5].EftOriginatorId;
                    hsbcEftDetailFile[arrayIndex].InvalidDataElement6 = "".PadLeft(11, '0');
                }
                else
                    break;      //done processing

                arrayIndex++;
            }

            return hsbcEftDetailFile;
        }

        #endregion

        public byte[] CreateCITIeft(DatabaseUser user, long payrollProcessId, String eftAccount, String employeeNumber)
        {
            byte[] fileContents = null;
            CitiEftPayrollMasterPaymentCollection coll = _sqlServerCitiEftPayrollMasterPayment.Select(user, payrollProcessId, employeeNumber);

            if (coll != null && coll.Count > 0)
            {
                //declare the EFT class for the EFT file, create arrays, then instantiate the object
                CitiEftRecord[] citiEftFile = new CitiEftRecord[coll.Count];

                //instantiate the EFT class for the EFT file
                //loop for multiple entry records
                for (int i = 0; i < coll.Count; i++)
                    citiEftFile[i] = new CitiEftRecord();

                //populate fields in the EFT classes
                citiEftFile = populateCitiEftDetail(citiEftFile, coll, eftAccount);

                FileHelpers.FileHelperEngine engine = new FileHelpers.FileHelperEngine(typeof(CitiEftRecord));
                String eftFileString = engine.WriteString(citiEftFile);

                //create the EFT file
                fileContents = convertStringToByteArray(eftFileString);
            }

            return fileContents;
        }
        public CitiChequeDetailsCollection GetPayrollMasterPaymentData(DatabaseUser user, long payrollMasterPaymentId)
        {
            return _sqlServerCitiChequeDetails.GetPayrollMasterPaymentData(user, payrollMasterPaymentId);
        }
        public CitiChequeDetailsCollection GetCitiChequeData(DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            return _sqlServerCitiChequeDetails.Select(user, payrollProcessId, employeeNumber);
        }
        public byte[] CreateCitiChequeFile(CitiChequeDetailsCollection collection, String citiChequeAccountNumber, String companyShortName, long payrollProcessId, string originatorId)
        {
            byte[] fileContents = null;

            if (collection != null && collection.Count > 0) //only create if we have rows to process
            {
                CitiChequeFileHeaderRecord[] chequeFileHeader = createFileHeader(payrollProcessId, originatorId);                   //100
                CitiChequeBatchHeaderRecord[] chequeBatchHeader = createBatchHeader(payrollProcessId, companyShortName);            //200
                CitiChequePaymentDetailRecord[] chequePaymentDetail = createPaymentDetail(collection, citiChequeAccountNumber);     //300
                CitiChequePayeeNameAddressRecord[] chequePayeeNameAddress = createPayeeNameAddress(collection);                     //301
                CitiChequeDetailRecord[] chequeDetail = createDetail(collection);                                                   //303
                CitiChequeTotalAndDescriptionRecord[] chequeTotalAndDescription = createTotalAndDescription(collection);            //305
                CitiChequeBatchTrailerRecord[] chequeBatchTrailer = null;                                                           //400
                CitiChequeFileTrailerRecord[] chequeFileTrailer = createFileTrailer(payrollProcessId);                              //500

                //batch trailer
                chequeBatchTrailer = createBatchTrailer(collection);

                //file trailer
                chequeFileTrailer = getTrailerTotals(chequeFileTrailer, collection);

                //write file to string
                FileHelpers.MultiRecordEngine engine = new FileHelpers.MultiRecordEngine(typeof(CitiChequeFileHeaderRecord), typeof(CitiChequeBatchHeaderRecord), typeof(CitiChequePaymentDetailRecord), typeof(CitiChequePayeeNameAddressRecord), typeof(CitiChequeDetailRecord), typeof(CitiChequeTotalAndDescriptionRecord), typeof(CitiChequeBatchTrailerRecord), typeof(CitiChequeFileTrailerRecord));
                String chequeFileString = writeChequeFile(engine, chequeFileHeader, chequeBatchHeader, chequePaymentDetail, chequePayeeNameAddress, chequeDetail, chequeTotalAndDescription, chequeBatchTrailer, chequeFileTrailer);

                //create the file
                fileContents = convertStringToByteArray(chequeFileString);
            }

            return fileContents;
        }
        private String writeChequeFile(FileHelpers.MultiRecordEngine eng, CitiChequeFileHeaderRecord[] chequeFileHeader, CitiChequeBatchHeaderRecord[] chequeBatchHeader, CitiChequePaymentDetailRecord[] chequePaymentDetail, CitiChequePayeeNameAddressRecord[] chequePayeeNameAddress, CitiChequeDetailRecord[] chequeDetail, CitiChequeTotalAndDescriptionRecord[] chequeTotalAndDescription, CitiChequeBatchTrailerRecord[] chequeBatchTrailer, CitiChequeFileTrailerRecord[] chequeFileTrailer)
        {
            System.Text.StringBuilder fileData = new System.Text.StringBuilder();

            fileData.Append(eng.WriteString(chequeFileHeader)); //file header always present

            if (chequeBatchHeader != null)
                fileData.Append(eng.WriteString(chequeBatchHeader));

            if (chequePaymentDetail != null && chequePayeeNameAddress != null && chequeDetail != null && chequeTotalAndDescription != null)
            {
                for (int i = 0; i < chequePaymentDetail.Length; i++)
                {
                    fileData.Append(eng.WriteString(new CitiChequePaymentDetailRecord[] { chequePaymentDetail[i] }));
                    fileData.Append(eng.WriteString(new CitiChequePayeeNameAddressRecord[] { chequePayeeNameAddress[i] }));
                    fileData.Append(eng.WriteString(new CitiChequeDetailRecord[] { chequeDetail[i] }));
                    fileData.Append(eng.WriteString(new CitiChequeTotalAndDescriptionRecord[] { chequeTotalAndDescription[i] }));
                }
            }

            if (chequeBatchTrailer != null)
                fileData.Append(eng.WriteString(chequeBatchTrailer));

            fileData.Append(eng.WriteString(chequeFileTrailer)); //file trailer always present

            return fileData.ToString();
        }
        public CitiChequeFileHeaderRecord[] createFileHeader(long payrollProcessId, string originatorId)
        {
            CitiChequeFileHeaderRecord[] chequeFileHeader = new CitiChequeFileHeaderRecord[1];
            chequeFileHeader[0] = new CitiChequeFileHeaderRecord() { FileHeaderControlNumber = Convert.ToString(payrollProcessId).PadLeft(9, '0'), OriginatorId = originatorId.PadRight(15) };

            return chequeFileHeader;
        }
        public CitiChequeFileTrailerRecord[] createFileTrailer(long payrollProcessId)
        {
            CitiChequeFileTrailerRecord[] chequeFileTrailer = new CitiChequeFileTrailerRecord[1];
            chequeFileTrailer[0] = new CitiChequeFileTrailerRecord() { FileTrailerControlNumber = Convert.ToString(payrollProcessId).PadLeft(9, '0') };

            return chequeFileTrailer;
        }
        public CitiChequeBatchHeaderRecord[] createBatchHeader(long payrollProcessId, String shortCompanyName)
        {
            CitiChequeBatchHeaderRecord[] chequeBatchHeader = new CitiChequeBatchHeaderRecord[1];
            chequeBatchHeader[0] = new CitiChequeBatchHeaderRecord()
            {
                BatchControlNumber = Convert.ToString(payrollProcessId + 100000000), //BatchControlNumber = processID plus 100000000 for cheques, our unique identifier
                CompanyName = shortCompanyName.PadRight(16)
            };

            return chequeBatchHeader;
        }
        public CitiChequePaymentDetailRecord[] createPaymentDetail(CitiChequeDetailsCollection collection, String citiChequeAccountNumber)
        {
            Decimal amount = 0;
            CitiChequePaymentDetailRecord[] chequePaymentDetail = new CitiChequePaymentDetailRecord[collection.Count];

            for (int i = 0; i < collection.Count; i++)
            {
                if (collection[i].ChequeAmount > 0 && collection[i].ChequeAmount.ToString().Length < 11)
                    amount = collection[i].ChequeAmount;

                chequePaymentDetail[i] = new CitiChequePaymentDetailRecord()
                {
                    PaymentAmount = Convert.ToString(Math.Round(amount, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(13, '0'),
                    PaymentEffectiveDate = collection[i].ProcessChequeDate,
                    CheckTransactionNumber = (collection[i].PayrollMasterPaymentId + 1000000000).ToString(),
                    OriginatorPayerAccountNumber = citiChequeAccountNumber.PadRight(17),
                    ReceiverPayeeName = collection[i].EmployeeName.PadRight(35),
                    FormCode = collection[i].CodeLanguage == "EN" ? "BMS01" : "BMS02"
                };
            }

            return chequePaymentDetail;
        }
        private String[] AddressSplit(String[] addressArray, int characterCount)
        {
            String overFlowText = "";
            int indexOfSpace = 0;

            for (int i = 0; i < addressArray.Length; i++)
            {
                //concat the overflow address text, might not be any
                addressArray[i] = overFlowText.TrimStart() + addressArray[i];
                overFlowText = "";

                if (addressArray[i].Length > characterCount)
                {
                    //check if character limit is hit on a space (ie.  "main<space>street")           NOTE: substring is a 0 based
                    if (addressArray[i].Substring(characterCount - 1, 1) == " ")
                    {
                        overFlowText = addressArray[i].Substring(characterCount - 1) + " ";
                        indexOfSpace = characterCount - 1;
                    }
                    //check if character limit is hit on the start of a word (ie.  "main Street", where "s" is limit)
                    else if (addressArray[i].Substring(characterCount - 2, 1) == " ")
                    {
                        overFlowText = addressArray[i].Substring(characterCount - 2) + " ";
                        indexOfSpace = characterCount - 2;
                    }
                    //check if character limit is hit on the end of a word (ie.  "main sT blvd", where "t" is limit)
                    else if (addressArray[i].Substring(characterCount, 1) == " ")
                    {
                        overFlowText = addressArray[i].Substring(characterCount) + " ";
                        indexOfSpace = characterCount;
                    }
                    else
                    {
                        //reverse search the address to find the last occurrence of a space
                        indexOfSpace = addressArray[i].LastIndexOf(" ", characterCount - 1);
                        overFlowText = addressArray[i].Substring(indexOfSpace + 1) + " ";
                    }

                    addressArray[i] = addressArray[i].Substring(0, indexOfSpace).PadRight(characterCount);
                }
                else
                    //pad to 40 chars
                    addressArray[i] = addressArray[i].PadRight(characterCount);
            }

            return addressArray;
        }
        public CitiChequePayeeNameAddressRecord[] createPayeeNameAddress(CitiChequeDetailsCollection collection)
        {
            int characterLimit = 40;
            CitiChequePayeeNameAddressRecord[] chequePayeeNameAddress = new CitiChequePayeeNameAddressRecord[collection.Count];

            for (int i = 0; i < collection.Count; i++)
            {
                chequePayeeNameAddress[i] = new CitiChequePayeeNameAddressRecord() { PayeeName = collection[i].EmployeeName.PadRight(40) };

                //create an array to send to the AddressSplit method.  The chequePayeeNameAddress uses 4 address properties
                String[] addressArray = new String[4];

                if (collection[i].AddressLine1 != null)
                    addressArray[0] = collection[i].AddressLine1;
                else
                    addressArray[0] = "";

                if (collection[i].AddressLine2 != null)
                    addressArray[1] = collection[i].AddressLine2;
                else
                    addressArray[1] = "";

                // WD-856: Remove Address Line 3
                //if (collection[i].AddressLine3 != null)
                //    addressArray[2] = collection[i].AddressLine3;
                //else
                addressArray[2] = "";

                //worklinks doesn't have a fourth address line but the citi cheque file does so create an empty item
                addressArray[3] = "";

                //break the address up into chunks defined by characterLimit (avoiding truncating words)
                addressArray = AddressSplit(addressArray, characterLimit);

                //assign results to our chequePayeeNameAddress object
                chequePayeeNameAddress[i].PayeeAddress1 = addressArray[0];
                chequePayeeNameAddress[i].PayeeAddress2 = addressArray[1];
                chequePayeeNameAddress[i].PayeeAddress3 = addressArray[2];
                chequePayeeNameAddress[i].PayeeAddress4 = addressArray[3];

                if (collection[i].City != null)
                    chequePayeeNameAddress[i].PayeeCity = collection[i].City.PadRight(19);

                if (collection[i].CodeProvinceStateCd != null)
                    chequePayeeNameAddress[i].PayeeProvStateCode = collection[i].CodeProvinceStateCd.PadRight(2);

                if (collection[i].PostalZipCode != null)
                    chequePayeeNameAddress[i].PayeeZipCode = collection[i].PostalZipCode.PadRight(9);
            }

            return chequePayeeNameAddress;
        }
        public CitiChequeDetailRecord[] createDetail(CitiChequeDetailsCollection collection)
        {
            Decimal amount = 0;
            CitiChequeDetailRecord[] chequeDetail = new CitiChequeDetailRecord[collection.Count];

            for (int i = 0; i < collection.Count; i++)
            {
                if (collection[i].ChequeAmount > 0 && collection[i].ChequeAmount.ToString().Length < 11)
                    amount = collection[i].ChequeAmount;

                chequeDetail[i] = new CitiChequeDetailRecord()
                {
                    GrossAmount = Convert.ToString(Math.Round(amount, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(10, '0'),
                    NetAmount = Convert.ToString(Math.Round(amount, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(10, '0'),
                    InvoiceDate = collection[i].CutoffDate
                };
            }

            return chequeDetail;
        }
        public CitiChequeTotalAndDescriptionRecord[] createTotalAndDescription(CitiChequeDetailsCollection collection)
        {
            Decimal amount = 0;
            CitiChequeTotalAndDescriptionRecord[] chequeTotalAndDescription = new CitiChequeTotalAndDescriptionRecord[collection.Count];

            for (int i = 0; i < collection.Count; i++)
            {
                if (collection[i].ChequeAmount > 0 && collection[i].ChequeAmount.ToString().Length < 11)
                    amount = collection[i].ChequeAmount;

                chequeTotalAndDescription[i] = new CitiChequeTotalAndDescriptionRecord()
                {
                    TotalGrossAmount = Convert.ToString(Math.Round(amount, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(10, '0'),
                    TotalNetAmount = Convert.ToString(Math.Round(amount, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(10, '0')
                };
            }

            return chequeTotalAndDescription;
        }
        public CitiChequeBatchTrailerRecord[] createBatchTrailer(CitiChequeDetailsCollection collection)
        {
            Decimal amount = 0;
            CitiChequeBatchTrailerRecord[] batchTrailer = new CitiChequeBatchTrailerRecord[1];

            batchTrailer[0] = new CitiChequeBatchTrailerRecord()
            {
                BatchControlNumber = Convert.ToString(collection[0].PayrollProcessId + 100000000), //BatchControlNumber = processID plus 100000000 for cheques, our unique identifier
                NumberOfPaymentTransInBatch = collection.Count.ToString().PadLeft(9, '0')
            };

            //get amount totals
            for (int i = 0; i < collection.Count; i++)
            {
                if (collection[i].ChequeAmount > 0 && collection[i].ChequeAmount.ToString().Length < 11)
                    amount += collection[i].ChequeAmount;
            }

            batchTrailer[0].DollarValueOfPaymentsInBatch = Convert.ToString(Math.Round(amount, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(13, '0');
            batchTrailer[0].NumberOfRecordsInBatch = (2 + 4 * collection.Count).ToString().PadLeft(9, '0');

            return batchTrailer;
        }
        public CitiChequeFileTrailerRecord[] getTrailerTotals(CitiChequeFileTrailerRecord[] trailerRecords, CitiChequeDetailsCollection collection)
        {
            Decimal amount = 0;

            trailerRecords[0].NumberOfRecordsInFile = (4 + 4 * collection.Count).ToString().PadLeft(9, '0');

            //get amount totals
            for (int i = 0; i < collection.Count; i++)
            {
                if (collection[i].ChequeAmount > 0 && collection[i].ChequeAmount.ToString().Length < 11)
                    amount += collection[i].ChequeAmount;
            }

            trailerRecords[0].DollarValueOfPaymentsInFile = Convert.ToString(Math.Round(amount, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(15, '0');
            trailerRecords[0].NumberOfBatchesInFile = "1".PadLeft(9, '0');
            trailerRecords[0].NumberOfPaymentsInFile = collection.Count.ToString().PadLeft(9, '0');

            return trailerRecords;
        }
        private CitiEftRecord[] populateCitiEftDetail(CitiEftRecord[] eftArray, CitiEftPayrollMasterPaymentCollection collection, String eftAccount)
        {
            for (int i = 0; i < collection.Count; i++)
            {
                eftArray[i].CurrentDate1 = collection[i].CurrentDate1;
                eftArray[i].PaymentAmount = collection[i].ChequeAmount;
                eftArray[i].CurrentDate2 = collection[i].CurrentDate2;
                eftArray[i].TransRefNumber = collection[i].PayrollMasterPaymentId.ToString();
                eftArray[i].CompanyShortName = collection[i].CompanyShortName;
                eftArray[i].CompanyLongName = collection[i].CompanyLongName;
                eftArray[i].DebitAccountNumber = eftAccount;
                eftArray[i].EmployeeEftAccount = collection[i].BankAccountNumber;

                //name can only contain A-Za-z and space
                char[] arr = collection[i].EmployeeName.ToCharArray();
                arr = Array.FindAll<char>(arr, (c => (char.IsLetter(c) || char.IsWhiteSpace(c))));

                eftArray[i].EmployeeName = new String(arr);
                eftArray[i].EmployeeTransitIntNumber = collection[i].BankTransInstCode;
            }

            return eftArray;
        }
        private byte[] convertStringToByteArray(String input)
        {
            //****there must be a better way to do this****
            byte[] bytes = new byte[input.Length];
            long i = 0;

            foreach (char val in input.ToCharArray())
                bytes[i++] = (byte)val;

            return bytes;
        }
        public PayrollExportCollection CheckForEftOnDatabase(DatabaseUser user, long payrollProcessId, string eftType)
        {
            return _sqlServerPayrollExport.Select(user, payrollProcessId, eftType);
        }
        public void InsertEftIntoDatabase(DatabaseUser user, PayrollExport payExport)
        {
            _sqlServerPayrollExport.Insert(user, payExport);
        }
        public void InsertCraRemittanceData(DatabaseUser user, long payrollProcessId)
        {
            _sqlServerCraRemittance.Insert(user, payrollProcessId);
        }
        public void InsertCraWcbRemittanceData(DatabaseUser user, long payrollProcessId)
        {
            _sqlServerCraWcbRemittance.Insert(user, payrollProcessId);
        }

        #region direct deposit / cra garnishment remittance
        public PayrollExportCollection GetDirectDepositCraGarnishmentExportData(DatabaseUser user, DateTime date, bool usingRbcEdiForDirectDepositsFlag, bool usingCraGarnishmentFlag, bool usingScotiaBankEdiForDirectDepositsFlag)
        {
            return _sqlServerPayrollExport.GetDirectDepositCraGarnishmentExportData(user, date, usingRbcEdiForDirectDepositsFlag, usingCraGarnishmentFlag, usingScotiaBankEdiForDirectDepositsFlag);
        }
        public void UpdatePayrollExportFileSentFlag(DatabaseUser user, PayrollExport payExport)
        {
            _sqlServerPayrollExport.UpdateFileSentFlag(user, payExport);
        }
        #endregion

        #region cheque health tax remittance
        public void InsertChequeHealthTaxRemittanceData(DatabaseUser user, long payrollProcessId, decimal periodYear)
        {
            _sqlServerChequeHealthTaxRemittance.Insert(user, payrollProcessId, periodYear);
        }
        public ChequeHealthTaxRemittanceScheduleCollection GetChequeHealthTaxRemittanceSchedule(DatabaseUser user, DateTime date)
        {
            return _sqlServerChequeHealthTaxRemittance.GetChequeHealthTaxRemittanceSchedule(user, date);
        }
        public ChequeHealthTaxExportCollection CheckForChequeHealthTaxRemittanceOnDatabase(DatabaseUser user, long? chequeHealthTaxExportId)
        {
            return _sqlServerChequeHealthTaxExport.Select(user, chequeHealthTaxExportId);
        }
        public void InsertChequeHealthTaxExportToDatabase(DatabaseUser user, ChequeHealthTaxExport chequeHealthTaxExport)
        {
            _sqlServerChequeHealthTaxExport.Insert(user, chequeHealthTaxExport);
        }
        public void UpdateChequeHealthTaxRemittanceChequeHealthTaxExportIdColumn(DatabaseUser user, DateTime remitDate, long chequeHealthTaxExportId, long healthTaxId)
        {
            _sqlServerChequeHealthTaxRemittance.UpdateChequeHealthTaxRemittanceChequeHealthTaxExportIdColumn(user, remitDate, chequeHealthTaxExportId, healthTaxId);
        }
        public ChequeHealthTaxRemittanceSummaryCollection GetRbcChequeHealthTaxRemittanceFileData(DatabaseUser user, ChequeHealthTaxRemittanceSchedule remit)
        {
            return _sqlServerChequeHealthTaxRemittance.GetRbcChequeHealthTaxRemittanceFileData(user, remit);
        }
        public void UpdateChequeHealthTaxExportFileTransmissionDate(DatabaseUser user, long chequeHealthTaxExportId, DateTime exportDate)
        {
            _sqlServerChequeHealthTaxExport.UpdateChequeHealthTaxExportFileTransmissionDate(user, chequeHealthTaxExportId, exportDate);
        }
        public ChequeHealthTaxExportCollection CheckForUnprocessedChequeHealthTaxExports(DatabaseUser user)
        {
            return _sqlServerChequeHealthTaxExport.CheckForUnprocessedHealthTaxRemittanceOnDatabase(user);
        }
        #endregion

        #region email sent by system
        public EmailSentBySystemCollection GetInvoiceByEmailSentBySystemId(DatabaseUser user, long emailSentBySystemId)
        {
            return _sqlServerEmailSentBySystem.GetInvoiceByEmailSentBySystemId(user, emailSentBySystemId);
        }
        public void InsertEmailSentBySystem(DatabaseUser user, EmailSentBySystem email)
        {
            _sqlServerEmailSentBySystem.Insert(user, email);
        }
        #endregion

        #region cheque wcb remittance
        public void InsertChequeWcbRemittanceData(DatabaseUser user, long payrollProcessId)
        {
            _sqlServerChequeWcbRemittance.Insert(user, payrollProcessId);
        }
        public ChequeWcbRemittanceScheduleCollection GetChequeWcbRemittanceSchedule(DatabaseUser user, DateTime date, bool queryForInvoice)
        {
            return _sqlServerChequeWcbRemittance.GetChequeWcbRemittanceSchedule(user, date, queryForInvoice);
        }
        public ChequeWcbRemittanceSummaryCollection GetRbcChequeWcbRemittanceFileData(DatabaseUser user, ChequeWcbRemittanceSchedule remit, bool estimatesOnly)
        {
            return _sqlServerChequeWcbRemittance.GetRbcChequeWcbRemittanceFileData(user, remit, estimatesOnly);
        }
        public void InsertChequeWcbExportToDatabase(DatabaseUser user, ChequeWcbExport chequeWcbExport)
        {
            _sqlServerChequeWcbExport.Insert(user, chequeWcbExport);
        }
        public void UpdateChequeWcbRemittanceChequeWcbExportIdColumn(DatabaseUser user, DateTime date, long chequeWcbExportId, string codeWcbCd)
        {
            _sqlServerChequeWcbRemittance.UpdateChequeWcbRemittanceChequeWcbExportIdColumn(user, date, chequeWcbExportId, codeWcbCd);
        }
        public ChequeWcbExportCollection CheckForUnprocessedChequeWcbExports(DatabaseUser user)
        {
            return _sqlServerChequeWcbExport.CheckForUnprocessedWcbRemittanceOnDatabase(user);
        }
        public void UpdateChequeWcbExportFileTransmissionDate(DatabaseUser user, long chequeWcbExportId, DateTime exportDate)
        {
            _sqlServerChequeWcbExport.UpdateChequeWcbExportFileTransmissionDate(user, chequeWcbExportId, exportDate);
        }
        public ChequeWcbExportCollection CheckForChequeWcbRemittanceOnDatabase(DatabaseUser user, long? chequeWcbExportId)
        {
            return _sqlServerChequeWcbExport.Select(user, chequeWcbExportId);
        }

        public void InsertChequeWcbInvoiceExport(DatabaseUser user, ChequeWcbInvoiceExport invoice)
        {
            _sqlServerChequeWcbInvoiceExport.Insert(user, invoice);
        }
        #endregion

        #region revenu quebec remittance
        public void InsertRqRemittanceData(DatabaseUser user, long payrollProcessId, decimal periodYear)
        {
            _sqlServerRqRemittance.Insert(user, payrollProcessId, periodYear);
        }
        public RevenuQuebecRemittanceSummaryCollection GetRevenueQuebecExportData(DatabaseUser user, DateTime date, string rqRemittancePeriodCode)
        {
            return _sqlServerRqRemittance.GetRevenueQuebecExportData(user, date, rqRemittancePeriodCode);
        }
        public void UpdateRevenuQuebecRemittanceRevenuQuebecExportIdColumn(DatabaseUser user, long rqExportId, DateTime date, string rqRemittancePeriodCode)
        {
            _sqlServerRqRemittance.UpdateRevenuQuebecRemittanceRevenuQuebecExportIdColumn(user, rqExportId, date, rqRemittancePeriodCode);
        }
        #endregion

        public long GetSequenceNumber(DatabaseUser user, string sequenceName)
        {
            return _sqlServerPayrollMasterPayment.GetSequenceNumber(user, sequenceName);
        }
        public CraRemittanceSummaryCollection GetRbcEftSourceDeductionData(DatabaseUser user, DateTime date, String craRemittancePeriodCode)
        {
            return _sqlServerCraRemittance.GetRbcEftSourceDeductionData(user, date, craRemittancePeriodCode);
        }
        public CraWcbRemittanceSummaryCollection GetRbcEftWcbData(DatabaseUser user, DateTime date, String craRemittancePeriodCode)
        {
            return _sqlServerCraWcbRemittance.GetRbcEftWcbData(user, date, craRemittancePeriodCode);
        }
        public CraExportCollection CheckForUnprocessedCraRemittanceOnDatabase(DatabaseUser user)
        {
            return _sqlServerCraExport.CheckForUnprocessedCraRemittanceOnDatabase(user);
        }
        public CraWcbExportCollection CheckForUnprocessedCraWcbRemittanceOnDatabase(DatabaseUser user)
        {
            return _sqlServerCraWcbExport.CheckForUnprocessedCraWcbRemittanceOnDatabase(user);
        }
        public CraExportCollection CheckForCraRemittanceOnDatabase(DatabaseUser user, long? craExportId)
        {
            return _sqlServerCraExport.Select(user, craExportId);
        }
        public CraWcbExportCollection CheckForCraWcbRemittanceOnDatabase(DatabaseUser user, long? craWcbExportId)
        {
            return _sqlServerCraWcbExport.Select(user, craWcbExportId);
        }
        public RevenuQuebecExportCollection CheckForRqRemittanceOnDatabase(DatabaseUser user, long? rqExportId)
        {
            return _sqlServerRqExport.Select(user, rqExportId);
        }
        public void UpdateRevenuQuebecExportFileTransmissionDate(DatabaseUser user, long rqExportId, DateTime exportDate)
        {
            _sqlServerRqExport.UpdateRevenuQuebecExportFileTransmissionDate(user, rqExportId, exportDate);
        }
        public RevenuQuebecExportCollection CheckForUnprocessedRevenuQuebecRemittanceOnDatabase(DatabaseUser user)
        {
            return _sqlServerRqExport.CheckForUnprocessedRevenuQuebecRemittanceOnDatabase(user);
        }
        public void InsertCraExportToDatabase(DatabaseUser user, CraExport craExport)
        {
            _sqlServerCraExport.Insert(user, craExport);
        }
        public void InsertCraWcbExportToDatabase(DatabaseUser user, CraWcbExport craWcbExport)
        {
            _sqlServerCraWcbExport.Insert(user, craWcbExport);
        }
        public void UpdateCraExportFileTransmissionDate(DatabaseUser user, long craExportId, DateTime exportDate)
        {
            _sqlServerCraExport.UpdateCraExportFileTransmissionDate(user, craExportId, exportDate);
        }
        public void UpdateCraWcbExportFileTransmissionDate(DatabaseUser user, long craWcbExportId, DateTime exportDate)
        {
            _sqlServerCraWcbExport.UpdateCraWcbExportFileTransmissionDate(user, craWcbExportId, exportDate);
        }
        public void UpdateCraRemittanceCraExportIdColumn(DatabaseUser user, DateTime date, long craExportId, String craRemittancePeriodCode)
        {
            _sqlServerCraRemittance.UpdateCraRemittanceCraExportIdColumn(user, date, craExportId, craRemittancePeriodCode);
        }
        public void UpdateCraWcbRemittanceCraWcbExportIdColumn(DatabaseUser user, DateTime date, long? craWcbExportId, String craRemittancePeriodCode)
        {
            _sqlServerCraWcbRemittance.UpdateCraWcbRemittanceCraWcbExportIdColumn(user, date, craWcbExportId, craRemittancePeriodCode);
        }
        public void InsertRevenuQuebecExportToDatabase(DatabaseUser user, RevenuQuebecExport rqExport)
        {
            _sqlServerRqExport.Insert(user, rqExport);
        }
        public CraGarnishmentCollection GetCraEftGarnishmentDeductionData(DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            return _sqlServerCraGarnishment.GetCraEftGarnishmentDeductionData(user, payrollProcessId, employeeNumber);
        }
        public RbcEftEdiCollection GetRbcEftEdiData(DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            return _sqlServerRbcEftEdiExport.Select(user, payrollProcessId, employeeNumber);
        }

        public ScotiaBankEdiCollection GetScotiaBankEdiData(DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            return _sqlServerScotiaBankEdiExport.Select(user, payrollProcessId, employeeNumber);
        }

        public PayrollMasterPaymentCollection GetEftData(DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            return _sqlServerPayrollMasterPayment.GetDirectDeposit(user, payrollProcessId, employeeNumber);
        }
        public byte[] CreateCitiAch(DatabaseUser user, PayrollMasterPaymentCollection collection, String status, String eftClientNumber, String companyName, String companyShortName)
        {
            byte[] fileContents = null;

            if (collection != null && collection.Count > 0) //only create if we have rows to process
            {
                CitiAchFileHeaderRecord[] citiAchFileHeader = createCitiAchFileHeader(user);
                CitiAchBatchHeaderRecord[] citiAchBatchHeader = createCitiAchBatchHeader(eftClientNumber);
                CitiAchEftDetailRecord[] citiAchDetailRecord = createCitiAchDetailRecord(collection, companyName, companyShortName);
                CitiAchBatchControlRecord[] citiAchBatchControlRecord = createCitiBatchControlRecord(collection);
                CitiAchFileControlRecord[] citiAchFileControlRecord = new CitiAchFileControlRecord[1] { new CitiAchFileControlRecord() };

                FileHelpers.MultiRecordEngine engine = new FileHelpers.MultiRecordEngine(typeof(CitiAchFileHeaderRecord), typeof(CitiAchBatchHeaderRecord), typeof(CitiAchEftDetailRecord), typeof(CitiAchBatchControlRecord), typeof(CitiAchFileControlRecord));
                String eftFileString = writeCitiAchFile(engine, citiAchFileHeader, citiAchBatchHeader, citiAchDetailRecord, citiAchBatchControlRecord, citiAchFileControlRecord);

                fileContents = convertStringToByteArray(eftFileString);

                //update db records from NEW to RP ("Newly Added" to "Record Processed")
                UpdateStatusOfEFTRecords(user, collection, status);
            }

            return fileContents;
        }
        public CitiAchFileHeaderRecord[] createCitiAchFileHeader(DatabaseUser user)
        {
            long sequenceNumber = GetSequenceNumber(user, "CITI_BANK_CHEQUE");
            CitiAchFileHeaderRecord[] citiFileHeader = new CitiAchFileHeaderRecord[1];
            citiFileHeader[0] = new CitiAchFileHeaderRecord() { FileCreationNumber = ((sequenceNumber % 9999) + 1).ToString().PadLeft(4, '0') };

            return citiFileHeader;
        }
        public CitiAchBatchHeaderRecord[] createCitiAchBatchHeader(String eftClientNumber)
        {
            CitiAchBatchHeaderRecord[] citiBatchHeader = new CitiAchBatchHeaderRecord[1];
            citiBatchHeader[0] = new CitiAchBatchHeaderRecord() { CustomerId = eftClientNumber.PadLeft(4, '0') };

            return citiBatchHeader;
        }
        public CitiAchEftDetailRecord[] createCitiAchDetailRecord(PayrollMasterPaymentCollection collection, String companyName, String companyShortName)
        {
            CitiAchEftDetailRecord[] citiAchDetail = new CitiAchEftDetailRecord[collection.Count];

            for (int i = 0; i < collection.Count; i++)
            {
                citiAchDetail[i] = new CitiAchEftDetailRecord();
                citiAchDetail[i].Amount = Convert.ToString(Math.Round(collection[i].Amount, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(10, '0');
                citiAchDetail[i].ValueDate = collection[i].ChequeDate;
                //format: "0BBBTTTTT", where: 0 = Constant, BBB = Bank Number, TTTTT = Branch Tr. Number
                citiAchDetail[i].PayeePayorInstitutionalID = String.Format("0{0}{1}", collection[i].CodeEmployeeBankCd.PadLeft(3, '0'), collection[i].TransitNumber.PadLeft(5, '0'));
                citiAchDetail[i].PayeePayorAccountNumber = collection[i].AccountNumber.PadRight(12);
                citiAchDetail[i].PayeePayorName = collection[i].EmployeeName.PadRight(30);
                citiAchDetail[i].OriginatorsLongName = companyName.PadRight(30);
                citiAchDetail[i].PaymentID = collection[i].PayrollMasterPaymentId.ToString().PadRight(15);
                citiAchDetail[i].OriginatorsShortName = companyShortName.PadRight(15);
            }

            return citiAchDetail;
        }
        public CitiAchBatchControlRecord[] createCitiBatchControlRecord(PayrollMasterPaymentCollection collection)
        {
            CitiAchBatchControlRecord[] citiAchControl = new CitiAchBatchControlRecord[1] { new CitiAchBatchControlRecord() };

            //TotalValueOfDebit
            Decimal moneyTotal = 0;

            foreach (PayrollMasterPayment rec in collection)
                moneyTotal += Convert.ToDecimal(rec.Amount);

            citiAchControl[0].TotalValueOfCredit = Convert.ToString(Math.Round(moneyTotal, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(14, '0');
            citiAchControl[0].TotalCreditTransactions = collection.Count.ToString().PadLeft(8, '0');

            return citiAchControl;
        }
        private String writeCitiAchFile(FileHelpers.MultiRecordEngine eng, CitiAchFileHeaderRecord[] citiAchFileHeader, CitiAchBatchHeaderRecord[] citiAchBatchHeader, CitiAchEftDetailRecord[] citiAchDetailRecord, CitiAchBatchControlRecord[] citiAchBatchControlRecord, CitiAchFileControlRecord[] citiAchFileControlRecord)
        {
            String fileData = eng.WriteString(citiAchFileHeader);
            fileData += eng.WriteString(citiAchBatchHeader);
            fileData += eng.WriteString(citiAchDetailRecord);
            fileData += eng.WriteString(citiAchBatchControlRecord);
            fileData += eng.WriteString(citiAchFileControlRecord);

            return fileData;
        }

        #region cheque health tax remittance
        public byte[] CreateRbcChequeHealthTaxRemittanceFile(DatabaseUser user, ChequeHealthTaxRemittanceSummaryCollection collection, RbcEftEdiParameters parms, ChequeHealthTaxRemittanceSchedule remit, long headerSequenceNumber, long transactionSequenceNumber)
        {
            byte[] fileContents = null;

            if (collection != null && collection.Count > 0) //only create eft if we have rows to process
            {
                //create our object and populate
                ChequeHealthTaxRemittanceDetail chequeFile = new ChequeHealthTaxRemittanceDetail();

                //isa
                chequeFile.isaHeader[0].SenderInterchangeID = parms.IsaSenderInterchangeID.PadRight(15, ' ');       //must be 15 chars
                chequeFile.isaHeader[0].ReceiverInterchangeID = parms.IsaReceiverInterchangeID.PadRight(15, ' ');   //must be 15 chars
                chequeFile.isaHeader[0].InterchangeControlNumber = headerSequenceNumber.ToString().PadLeft(9, '0');       //must be 9 chars
                chequeFile.isaHeader[0].TestIndicator = parms.IsaTestIndicator;

                //gs
                chequeFile.gsHeader[0].SenderInterchangeID = parms.IsaSenderInterchangeID;
                chequeFile.gsHeader[0].ApplicationReceiversCode = parms.IsaReceiverInterchangeID;
                chequeFile.gsHeader[0].GroupControlNumber = (headerSequenceNumber.ToString().Length) > 4 ? headerSequenceNumber.ToString().Substring(headerSequenceNumber.ToString().Length - 4, 4) : headerSequenceNumber.ToString();         //max 4 chars, no leading 0s required

                //st
                chequeFile.st[0].TransactionSetControlNumber = "1".ToString().PadLeft(9, '0');      //this process will only have one cheque so just use a "1".

                //bpr
                chequeFile.bpr[0].MonetaryAmount = Convert.ToString(Math.Round(collection[0].Premium, 2, MidpointRounding.AwayFromZero));
                chequeFile.bpr[0].OriginatorDepositoryFinancialInstitutionIdentificationNumber = parms.DirectDepositBankCodeTransitNumber;
                chequeFile.bpr[0].OriginatorAccountNumber = parms.DirectDepositAccountNumber;
                chequeFile.bpr[0].PaymentDate = collection[0].RemittanceDate;

                //trn
                chequeFile.trn[0].ReferenceIdentification = transactionSequenceNumber.ToString();

                //cur
                //nothing to populate outside of constructor

                //ref1
                chequeFile.ref1[0].ReferenceId = parms.IsaSenderInterchangeID.Substring(0, 9);  //substring of IsaSenderInterchangeID.  First 9 chars

                //ref2
                chequeFile.ref2[0].ReferenceId = parms.ChequesChequeIssuance;

                //ref3
                chequeFile.ref3[0].ReferenceId = transactionSequenceNumber.ToString();

                //ref4
                //for worlinks, no padding will happen as the value will be 0004.  This is only required if the value is empty string, for a company using our product for file generation but not submission, they may not require 0004 but just 4 spaces.
                chequeFile.ref4[0].DescriptionPayeeMatch = parms.DescriptionPayeeMatch.PadLeft(4);

                //Remit Period: YYYY/MM/DD - YYYY/MM/DD
                string line1 = "Remit Period: " + remit.ReportingPeriodStartDate.ToString("yyyy/MM/dd") + "-" + remit.ReportingPeriodEndDate.ToString("yyyy/MM/dd");
                chequeFile.customLine1[0].Description = line1.Length > 35 ? line1.Substring(0, 35) : line1;

                //Remuneration Amount: $xx.xx
                string line2 = "Remuneration Amount: " + collection[0].Earnings.ToString("c");
                chequeFile.customLine2[0].Description = line2.Length > 35 ? line2.Substring(0, 35) : line2;

                //Remittance Amount: $xx.xx
                string line3 = "Remit Amount: " + collection[0].Premium.ToString("c");
                chequeFile.customLine3[0].Description = line3.Length > 35 ? line3.Substring(0, 35) : line3;

                //3 extra segments being included
                chequeFile.se[0].NumberOfIncludedSegments = (Convert.ToInt16(chequeFile.se[0].NumberOfIncludedSegments) + 3).ToString();

                //ref6 (max 24 chars)
                chequeFile.ref6[0].Description = (collection[0].HealthRemittanceAccountNumber.Length > 24) ? collection[0].HealthRemittanceAccountNumber.Substring(0, 24) : collection[0].HealthRemittanceAccountNumber;

                //dtm
                //nothing to populate outside of constructor

                //n1pr
                chequeFile.n1pr[0].OriginatorPayorName = (parms.CompanyShortName.Length > 23) ? parms.CompanyShortName.Substring(0, 23) : parms.CompanyShortName;

                //n1pe
                chequeFile.n1pe[0].PayeeName = (collection[0].VendorName.Length > 60) ? collection[0].VendorName.Substring(0, 60) : collection[0].VendorName; //max 60 chars
                chequeFile.n1pe[0].IdentificationCode = (collection[0].HealthRemittanceAccountNumber.Length > 20) ? collection[0].HealthRemittanceAccountNumber.Substring(0, 20) : collection[0].HealthRemittanceAccountNumber; //max 20 chars

                //n3
                chequeFile.n3[0].AddressInformationLine1 = (collection[0].VendorAddressLine1.Length > 35) ? collection[0].VendorAddressLine1.Substring(0, 35) : collection[0].VendorAddressLine1; //max 35 chars

                //n3 Address additional lines
                if (collection[0].VendorAddressLine2 != null && collection[0].VendorAddressLine2.Length > 0)
                    chequeFile.n3[0].AddressInformationLine1 += "*" + ((collection[0].VendorAddressLine2.Length > 35) ? collection[0].VendorAddressLine2.Substring(0, 35) : collection[0].VendorAddressLine2); //max 35 chars

                // WD-856: Remove Address Line 3
                //if (collection[0].VendorAddressLine3 != null && collection[0].VendorAddressLine3.Length > 0)
                //{
                //    chequeFile.n3Part2[0] = new ChequePayeeReceiverAddressInformation2() { AddressInformationLine3 = (collection[0].VendorAddressLine3.Length > 35) ? collection[0].VendorAddressLine3.Substring(0, 35) : collection[0].VendorAddressLine3 }; //max 35 chars
                //    chequeFile.se[0].NumberOfIncludedSegments = (Convert.ToInt16(chequeFile.se[0].NumberOfIncludedSegments) + 1).ToString(); //one extra segment if this is included
                //}

                //n4
                chequeFile.n4[0].ReceiverCityName = (collection[0].VendorCity.Length > 35) ? collection[0].VendorCity.Substring(0, 30) : collection[0].VendorCity; //max 30 chars
                chequeFile.n4[0].ReceiverStateProvinceCode = collection[0].VendorCodeProvinceStateCd;
                chequeFile.n4[0].ReceiverStatePostalCode = collection[0].VendorPostalZipCode;
                chequeFile.n4[0].ReceiverCountryCode = collection[0].VendorCodeCountryCd;

                //ent
                String seqNum = transactionSequenceNumber.ToString();
                chequeFile.ent[0].AssignedNumber = (seqNum.Length > 6) ? seqNum.Substring(seqNum.Length - 6, 6) : seqNum; //max 6 chars

                //rmr
                chequeFile.rmr[0].MonetaryAmount = Convert.ToString(Math.Round(collection[0].Premium, 2, MidpointRounding.AwayFromZero));
                chequeFile.rmr[0].AssessableAmount = Convert.ToString(Math.Round(collection[0].Earnings, 2, MidpointRounding.AwayFromZero));

                //ref5
                //this section below is for the start/end date to appear on the cheque stub without the year showing.
                String description = remit.ReportingPeriodStartDate.ToString("MM/dd") + " - " + remit.ReportingPeriodEndDate.ToString("MM/dd");
                chequeFile.ref5[0].Description = (description.Length > 30) ? description.Substring(0, 30) : description;

                //se
                chequeFile.se[0].TransactionSetControlNumber = chequeFile.st[0].TransactionSetControlNumber;

                //ge
                chequeFile.geTrailer[0].NumberOfTransactionSetsIncluded = collection.Count.ToString();
                chequeFile.geTrailer[0].GroupControlNumber = chequeFile.gsHeader[0].GroupControlNumber;

                //iea
                chequeFile.ieaTrailer[0].InterchangeControlNumber = chequeFile.isaHeader[0].InterchangeControlNumber;

                //add to array to make writing easier
                ChequeHealthTaxRemittanceDetail[] chequeFileArray = new ChequeHealthTaxRemittanceDetail[1];
                chequeFileArray[0] = chequeFile;

                //create the EFT file
                fileContents = convertStringToByteArray(WriteRbcChequeHealthTaxRemittanceFile(chequeFileArray));
            }

            return fileContents;
        }
        private String WriteRbcChequeHealthTaxRemittanceFile(ChequeHealthTaxRemittanceDetail[] chequeFile)
        {
            //declare the types of records to be written by the engine
            FileHelpers.MultiRecordEngine eng = new FileHelpers.MultiRecordEngine(typeof(IsaInterchangeControlHeader), typeof(GsFunctionalGroupHeader),
                                                typeof(ChequeStTransactionSetHeader), typeof(ChequeBeginningSegmentForPaymentOrderRemittanceAdvice),
                                                typeof(ChequeTrace), typeof(ChequeCurrency), typeof(ChequeReferenceIdentification), typeof(ChequeReferenceIdentification2),
                                                typeof(ChequeReferenceIdentification3), typeof(ChequeReferenceIdentification4), typeof(ChequeReferenceIdentification6),
                                                typeof(ChequeDateTimeReference), typeof(ChequeOriginatorNamePayor), typeof(ChequePayeeReceiverName),
                                                typeof(ChequePayeeReceiverAddressInformation), typeof(PayeeReceiverGeographicLocation), typeof(ChequeEntity),
                                                typeof(HealthTaxChequeRemittanceAdviceAccountsReceivableOpenItemReference), typeof(ChequeReferenceIdentification5),
                                                typeof(ChequeSeTransactionSetTrailer), typeof(GsFunctionalGroupTrailer), typeof(IsaInterchangeControlTrailer), typeof(ChequePayeeReceiverAddressInformation2),
                                                typeof(ChequeReferenceIdentificationCustom)
                                                );

            System.Text.StringBuilder fileData = new System.Text.StringBuilder();

            fileData.Append(eng.WriteString(chequeFile[0].isaHeader));
            fileData.Append(eng.WriteString(chequeFile[0].gsHeader));
            //no direct deposit info in this class
            //write out cheque
            fileData.Append(eng.WriteString(chequeFile[0].st));
            fileData.Append(eng.WriteString(chequeFile[0].bpr));
            fileData.Append(eng.WriteString(chequeFile[0].trn));
            fileData.Append(eng.WriteString(chequeFile[0].cur));
            fileData.Append(eng.WriteString(chequeFile[0].ref1));
            fileData.Append(eng.WriteString(chequeFile[0].ref2));
            fileData.Append(eng.WriteString(chequeFile[0].ref3));
            fileData.Append(eng.WriteString(chequeFile[0].ref4));
            fileData.Append(eng.WriteString(chequeFile[0].customLine1));
            fileData.Append(eng.WriteString(chequeFile[0].customLine2));
            fileData.Append(eng.WriteString(chequeFile[0].customLine3));
            fileData.Append(eng.WriteString(chequeFile[0].ref6));
            fileData.Append(eng.WriteString(chequeFile[0].dtm));
            fileData.Append(eng.WriteString(chequeFile[0].n1pr));
            fileData.Append(eng.WriteString(chequeFile[0].n1pe));
            fileData.Append(eng.WriteString(chequeFile[0].n3));
            //if address line 3 was present
            if (chequeFile[0].n3Part2[0] != null)
                fileData.Append(eng.WriteString(chequeFile[0].n3Part2));
            fileData.Append(eng.WriteString(chequeFile[0].n4));
            fileData.Append(eng.WriteString(chequeFile[0].ent));
            fileData.Append(eng.WriteString(chequeFile[0].rmr));
            fileData.Append(eng.WriteString(chequeFile[0].ref5));
            fileData.Append(eng.WriteString(chequeFile[0].se));
            fileData.Append(eng.WriteString(chequeFile[0].geTrailer));
            fileData.Append(eng.WriteString(chequeFile[0].ieaTrailer));

            return fileData.ToString();
        }
        #endregion

        #region cheque wcb remittance
        public byte[] CreateRbcChequeWcbRemittanceFile(DatabaseUser user, ChequeWcbRemittanceSummaryCollection collection, RbcEftEdiParameters parms, ChequeWcbRemittanceSchedule remit, long headerSequenceNumber, long transactionSequenceNumber)
        {
            byte[] fileContents = null;

            if (collection != null && collection.Count > 0) //only create eft if we have rows to process
            {
                //create our object and populate
                ChequeWcbRemittanceDetail chequeFile = new ChequeWcbRemittanceDetail();

                //isa
                chequeFile.isaHeader[0].SenderInterchangeID = parms.IsaSenderInterchangeID.PadRight(15, ' ');       //must be 15 chars
                chequeFile.isaHeader[0].ReceiverInterchangeID = parms.IsaReceiverInterchangeID.PadRight(15, ' ');   //must be 15 chars
                chequeFile.isaHeader[0].InterchangeControlNumber = headerSequenceNumber.ToString().PadLeft(9, '0');       //must be 9 chars
                chequeFile.isaHeader[0].TestIndicator = parms.IsaTestIndicator;

                //gs
                chequeFile.gsHeader[0].SenderInterchangeID = parms.IsaSenderInterchangeID;
                chequeFile.gsHeader[0].ApplicationReceiversCode = parms.IsaReceiverInterchangeID;
                chequeFile.gsHeader[0].GroupControlNumber = (headerSequenceNumber.ToString().Length) > 4 ? headerSequenceNumber.ToString().Substring(headerSequenceNumber.ToString().Length - 4, 4) : headerSequenceNumber.ToString();         //max 4 chars, no leading 0s required

                //st
                chequeFile.st[0].TransactionSetControlNumber = "1".ToString().PadLeft(9, '0');      //this process will only have one cheque so just use a "1".

                //bpr
                chequeFile.bpr[0].MonetaryAmount = Convert.ToString(Math.Round(collection[0].PaymentAmount, 2, MidpointRounding.AwayFromZero));
                chequeFile.bpr[0].OriginatorDepositoryFinancialInstitutionIdentificationNumber = parms.DirectDepositBankCodeTransitNumber;
                chequeFile.bpr[0].OriginatorAccountNumber = parms.DirectDepositAccountNumber;
                chequeFile.bpr[0].PaymentDate = collection[0].RemittanceDate;

                //trn
                chequeFile.trn[0].ReferenceIdentification = transactionSequenceNumber.ToString();

                //cur
                //nothing to populate outside of constructor

                //ref1
                chequeFile.ref1[0].ReferenceId = parms.IsaSenderInterchangeID.Substring(0, 9);  //substring of IsaSenderInterchangeID.  First 9 chars

                //ref2
                chequeFile.ref2[0].ReferenceId = parms.ChequesChequeIssuance;

                //ref3
                chequeFile.ref3[0].ReferenceId = transactionSequenceNumber.ToString();

                //ref4
                //for worlinks, no padding will happen as the value will be 0004.  This is only required if the value is empty string, for a company using our product for file generation but not submission, they may not require 0004 but just 4 spaces.
                chequeFile.ref4[0].DescriptionPayeeMatch = parms.DescriptionPayeeMatch.PadLeft(4);

                //Remit Period: YYYY/MM/DD - YYYY/MM/DD
                string line1 = "Remit Period: " + remit.ReportingPeriodStartDate.ToString("yyyy/MM/dd") + "-" + remit.ReportingPeriodEndDate.ToString("yyyy/MM/dd");
                chequeFile.customLine1[0].Description = line1.Length > 35 ? line1.Substring(0, 35) : line1;

                //Remuneration Amount: $xx.xx
                string line2 = "Assessable Amount: " + collection[0].AssessableEarnings.ToString("c");
                chequeFile.customLine2[0].Description = line2.Length > 35 ? line2.Substring(0, 35) : line2;

                //Remittance Amount: $xx.xx
                string line3 = "Remit Amount: " + collection[0].PaymentAmount.ToString("c");
                chequeFile.customLine3[0].Description = line3.Length > 35 ? line3.Substring(0, 35) : line3;

                chequeFile.se[0].NumberOfIncludedSegments = (Convert.ToInt16(chequeFile.se[0].NumberOfIncludedSegments) + 3).ToString(); //3 extra segments being included

                //ref6 (max 24 chars)
                chequeFile.ref6[0].Description = (collection[0].WorkersCompensationAccountNumber.Length > 24) ? collection[0].WorkersCompensationAccountNumber.Substring(0, 24) : collection[0].WorkersCompensationAccountNumber;

                //dtm
                //nothing to populate outside of constructor

                //n1pr
                chequeFile.n1pr[0].OriginatorPayorName = (parms.CompanyShortName.Length > 23) ? parms.CompanyShortName.Substring(0, 23) : parms.CompanyShortName;

                //n1pe
                chequeFile.n1pe[0].PayeeName = (collection[0].VendorName.Length > 60) ? collection[0].VendorName.Substring(0, 60) : collection[0].VendorName; //max 60 chars
                chequeFile.n1pe[0].IdentificationCode = (collection[0].WorkersCompensationAccountNumber.Length > 20) ? collection[0].WorkersCompensationAccountNumber.Substring(0, 20) : collection[0].WorkersCompensationAccountNumber; //max 20 chars

                //n3
                chequeFile.n3[0].AddressInformationLine1 = (collection[0].AddressLine1.Length > 35) ? collection[0].AddressLine1.Substring(0, 35) : collection[0].AddressLine1; //max 35 chars

                //n3 Address additional lines
                if (collection[0].AddressLine2 != null && collection[0].AddressLine2.Length > 0)
                    chequeFile.n3[0].AddressInformationLine1 += "*" + ((collection[0].AddressLine2.Length > 35) ? collection[0].AddressLine2.Substring(0, 35) : collection[0].AddressLine2); //max 35 chars

                // WD-856: Remove Address Line 3
                //if (collection[0].AddressLine3 != null && collection[0].AddressLine3.Length > 0)
                //{
                //    chequeFile.n3Part2[0] = new ChequePayeeReceiverAddressInformation2() { AddressInformationLine3 = (collection[0].AddressLine3.Length > 35) ? collection[0].AddressLine3.Substring(0, 35) : collection[0].AddressLine3 }; //max 35 chars
                //    chequeFile.se[0].NumberOfIncludedSegments = (Convert.ToInt16(chequeFile.se[0].NumberOfIncludedSegments) + 1).ToString(); //one extra segment if this is included
                //}

                //n4
                chequeFile.n4[0].ReceiverCityName = (collection[0].City.Length > 35) ? collection[0].City.Substring(0, 30) : collection[0].City; //max 30 chars
                chequeFile.n4[0].ReceiverStateProvinceCode = collection[0].CodeProvinceStateCd;
                chequeFile.n4[0].ReceiverStatePostalCode = collection[0].PostalZipCode;
                chequeFile.n4[0].ReceiverCountryCode = collection[0].CodeCountryCd;

                //ent
                String seqNum = transactionSequenceNumber.ToString();
                chequeFile.ent[0].AssignedNumber = (seqNum.Length > 6) ? seqNum.Substring(seqNum.Length - 6, 6) : seqNum; //max 6 chars

                //rmr
                chequeFile.rmr[0].MonetaryAmount = Convert.ToString(Math.Round(collection[0].PaymentAmount, 2, MidpointRounding.AwayFromZero));
                chequeFile.rmr[0].AssessableAmount = Convert.ToString(Math.Round(collection[0].AssessableEarnings, 2, MidpointRounding.AwayFromZero));

                //ref5
                //this section below is for the start/end date to appear on the cheque stub without the year showing.
                String description = remit.ReportingPeriodStartDate.ToString("MM/dd") + " - " + remit.ReportingPeriodEndDate.ToString("MM/dd");
                chequeFile.ref5[0].Description = (description.Length > 30) ? description.Substring(0, 30) : description;

                //se
                chequeFile.se[0].TransactionSetControlNumber = chequeFile.st[0].TransactionSetControlNumber;

                //ge
                chequeFile.geTrailer[0].NumberOfTransactionSetsIncluded = collection.Count.ToString();
                chequeFile.geTrailer[0].GroupControlNumber = chequeFile.gsHeader[0].GroupControlNumber;

                //iea
                chequeFile.ieaTrailer[0].InterchangeControlNumber = chequeFile.isaHeader[0].InterchangeControlNumber;

                //add to array to make writing easier
                ChequeWcbRemittanceDetail[] chequeFileArray = new ChequeWcbRemittanceDetail[1];
                chequeFileArray[0] = chequeFile;

                //create the EFT file
                fileContents = convertStringToByteArray(WriteRbcChequeWcbRemittanceFile(chequeFileArray));
            }

            return fileContents;
        }

        private String WriteRbcChequeWcbRemittanceFile(ChequeWcbRemittanceDetail[] chequeFile)
        {
            //declare the types of records to be written by the engine
            FileHelpers.MultiRecordEngine eng = new FileHelpers.MultiRecordEngine(typeof(IsaInterchangeControlHeader), typeof(GsFunctionalGroupHeader),
                                                typeof(ChequeStTransactionSetHeader), typeof(ChequeBeginningSegmentForPaymentOrderRemittanceAdvice),
                                                typeof(ChequeTrace), typeof(ChequeCurrency), typeof(ChequeReferenceIdentification), typeof(ChequeReferenceIdentification2),
                                                typeof(ChequeReferenceIdentification3), typeof(ChequeReferenceIdentification4), typeof(ChequeReferenceIdentification6),
                                                typeof(ChequeDateTimeReference), typeof(ChequeOriginatorNamePayor), typeof(ChequePayeeReceiverName),
                                                typeof(ChequePayeeReceiverAddressInformation), typeof(PayeeReceiverGeographicLocation), typeof(ChequeEntity),
                                                typeof(WcbChequeRemittanceAdviceAccountsReceivableOpenItemReference), typeof(ChequeReferenceIdentification5),
                                                typeof(ChequeSeTransactionSetTrailer), typeof(GsFunctionalGroupTrailer), typeof(IsaInterchangeControlTrailer), typeof(ChequePayeeReceiverAddressInformation2),
                                                typeof(ChequeReferenceIdentificationCustom)
                                            );

            System.Text.StringBuilder fileData = new System.Text.StringBuilder();
            fileData.Append(eng.WriteString(chequeFile[0].isaHeader));
            fileData.Append(eng.WriteString(chequeFile[0].gsHeader));
            //no direct deposit info in this class
            //write out cheque
            fileData.Append(eng.WriteString(chequeFile[0].st));
            fileData.Append(eng.WriteString(chequeFile[0].bpr));
            fileData.Append(eng.WriteString(chequeFile[0].trn));
            fileData.Append(eng.WriteString(chequeFile[0].cur));
            fileData.Append(eng.WriteString(chequeFile[0].ref1));
            fileData.Append(eng.WriteString(chequeFile[0].ref2));
            fileData.Append(eng.WriteString(chequeFile[0].ref3));
            fileData.Append(eng.WriteString(chequeFile[0].ref4));
            fileData.Append(eng.WriteString(chequeFile[0].customLine1));
            fileData.Append(eng.WriteString(chequeFile[0].customLine2));
            fileData.Append(eng.WriteString(chequeFile[0].customLine3));
            fileData.Append(eng.WriteString(chequeFile[0].ref6));
            fileData.Append(eng.WriteString(chequeFile[0].dtm));
            fileData.Append(eng.WriteString(chequeFile[0].n1pr));
            fileData.Append(eng.WriteString(chequeFile[0].n1pe));
            fileData.Append(eng.WriteString(chequeFile[0].n3));
            //if address line 3 was present
            if (chequeFile[0].n3Part2[0] != null)
                fileData.Append(eng.WriteString(chequeFile[0].n3Part2));
            fileData.Append(eng.WriteString(chequeFile[0].n4));
            fileData.Append(eng.WriteString(chequeFile[0].ent));
            fileData.Append(eng.WriteString(chequeFile[0].rmr));
            fileData.Append(eng.WriteString(chequeFile[0].ref5));
            fileData.Append(eng.WriteString(chequeFile[0].se));
            fileData.Append(eng.WriteString(chequeFile[0].geTrailer));
            fileData.Append(eng.WriteString(chequeFile[0].ieaTrailer));

            return fileData.ToString();
        }

        #endregion

        public void UpdateSequenceNumber(DatabaseUser user, PayrollProcess process)
        {
            _sqlServerPayrollMasterPayment.UpdateSequenceNumber(user, process);
        }

        private DirectDepositUS PopulateDirectDepositDetailsUS(RbcEftEdi record, int ddSequenceNumber, RbcEftEdiParameters ediParms)
        {
            //create new object
            DirectDepositUS directDepositUS = new DirectDepositUS();

            //st
            directDepositUS.st[0].TransactionSetControlNumber = (ddSequenceNumber).ToString().PadLeft(9, '0');

            //bpr
            directDepositUS.bpr[0].MonetaryAmount = Convert.ToString(Math.Round(record.ChequeAmount, 2, MidpointRounding.AwayFromZero));
            directDepositUS.bpr[0].PaymentMethodCode = (record.CodeBankAccountTypeCd == "CHQ") ? "DCC" : "PDC"; //Indicate if employee's USD bank account is chequing (DCC) or savings (PDC)
            directDepositUS.bpr[0].OriginatorDepositoryFinancialInstitutionIdentificationNumber = ediParms.DirectDepositBankCodeTransitNumber;
            directDepositUS.bpr[0].OriginatorAccountNumber = ediParms.DirectDepositAccountNumber;
            directDepositUS.bpr[0].ReceiverDepositoryFinancialInstitutionIdentificationNumber = record.BankTransactionNumber; //US has routing# stored here.
            directDepositUS.bpr[0].ReceiverAccountNumber = record.EmployeeBankAccountNumber;
            directDepositUS.bpr[0].PaymentDate = record.ChequeDate;

            //trn
            directDepositUS.trnUs[0].ReferenceIdentification = record.RbcSequenceNumber.ToString();
            directDepositUS.trnUs[0].CrossBorderPaymentDescription = ediParms.UsTrn;

            //cur
            //nothing to populate outside of constructor

            //ref
            directDepositUS.ref1[0].ReferenceId = ediParms.DirectDepositGsan;

            //dtm
            //nothing to populate outside of constructor

            //n1 payor
            directDepositUS.n1pr[0].OriginatorPayorName = (ediParms.CompanyShortName.Length > 23) ? ediParms.CompanyShortName.Substring(0, 23) : ediParms.CompanyShortName;

            //n2 payor
            directDepositUS.n2[0].Name = (ediParms.CompanyShortName.Length > 15) ? ediParms.CompanyShortName.Substring(0, 15) : ediParms.CompanyShortName;

            //n3
            directDepositUS.n3Employer[0].AddressInformationLine1 = (record.EmployerAddressLine1.Length > 35) ? record.EmployerAddressLine1.Substring(0, 35) : record.EmployerAddressLine1; //max 35 chars

            //Address additional lines
            if (record.EmployerAddressLine2 != null && record.EmployerAddressLine2.Length > 0)
                directDepositUS.n3Employer[0].AddressInformationLine1 += "*" + ((record.EmployerAddressLine2.Length > 35) ? record.EmployerAddressLine2.Substring(0, 35) : record.EmployerAddressLine2); //max 35 chars

            //n4
            directDepositUS.n4Employer[0].ReceiverCityName = (record.EmployerCity.Length > 35) ? record.EmployerCity.Substring(0, 30) : record.EmployerCity; //max 30 chars
            directDepositUS.n4Employer[0].ReceiverStateProvinceCode = record.EmployerProvinceStateCode;
            directDepositUS.n4Employer[0].ReceiverStatePostalCode = record.EmployerPostalZipCode;
            directDepositUS.n4Employer[0].ReceiverCountryCode = record.EmployerCountryCode;

            //n1 payee
            directDepositUS.n1pe[0].PayeeName = (record.EmployeeName.Length > 60) ? record.EmployeeName.Substring(0, 60) : record.EmployeeName; //max 60 chars

            //n3
            directDepositUS.n3[0].AddressInformationLine1 = (record.EmployeeAddressLine1.Length > 35) ? record.EmployeeAddressLine1.Substring(0, 35) : record.EmployeeAddressLine1; //max 35 chars

            //Address additional lines
            if (record.EmployeeAddressLine2 != null && record.EmployeeAddressLine2.Length > 0)
                directDepositUS.n3[0].AddressInformationLine1 += "*" + ((record.EmployeeAddressLine2.Length > 35) ? record.EmployeeAddressLine2.Substring(0, 35) : record.EmployeeAddressLine2); //max 35 chars

            //n4
            directDepositUS.n4[0].ReceiverCityName = (record.EmployeeCity.Length > 35) ? record.EmployeeCity.Substring(0, 30) : record.EmployeeCity; //max 30 chars
            directDepositUS.n4[0].ReceiverStateProvinceCode = record.EmployeeProvinceStateCode;
            directDepositUS.n4[0].ReceiverStatePostalCode = record.EmployeePostalZipCode;
            directDepositUS.n4[0].ReceiverCountryCode = record.EmployeeCountryCode;

            //se
            directDepositUS.se[0].TransactionSetControlNumber = directDepositUS.st[0].TransactionSetControlNumber;

            return directDepositUS;
        }

        private DirectDeposit PopulateDirectDepositDetails(RbcEftEdi record, int ddSequenceNumber, RbcEftEdiParameters ediParms)
        {
            //create new object
            DirectDeposit directDeposit = new DirectDeposit();

            //st
            directDeposit.st[0].TransactionSetControlNumber = (ddSequenceNumber).ToString().PadLeft(9, '0');

            //bpr
            directDeposit.bpr[0].MonetaryAmount = Convert.ToString(Math.Round(record.ChequeAmount, 2, MidpointRounding.AwayFromZero));
            directDeposit.bpr[0].OriginatorDepositoryFinancialInstitutionIdentificationNumber = ediParms.DirectDepositBankCodeTransitNumber;
            directDeposit.bpr[0].OriginatorAccountNumber = ediParms.DirectDepositAccountNumber;
            directDeposit.bpr[0].ReceiverDepositoryFinancialInstitutionIdentificationNumber = record.BankInstCode.PadLeft(4, '0') + record.BankTransactionNumber;
            directDeposit.bpr[0].ReceiverAccountNumber = record.EmployeeBankAccountNumber;
            directDeposit.bpr[0].PaymentDate = record.ChequeDate;

            //trn
            directDeposit.trn[0].ReferenceIdentification = record.RbcSequenceNumber.ToString();

            //cur
            //nothing to populate outside of constructor

            //ref
            directDeposit.ref1[0].ReferenceId = ediParms.DirectDepositGsan;

            //dtm
            //nothing to populate outside of constructor

            //n1 payor
            directDeposit.n1pr[0].OriginatorPayorName = (ediParms.CompanyShortName.Length > 23) ? ediParms.CompanyShortName.Substring(0, 23) : ediParms.CompanyShortName;

            //n2 payor
            directDeposit.n2[0].Name = (ediParms.CompanyShortName.Length > 15) ? ediParms.CompanyShortName.Substring(0, 15) : ediParms.CompanyShortName;

            //n1 payee
            directDeposit.n1pe[0].PayeeName = (record.EmployeeName.Length > 60) ? record.EmployeeName.Substring(0, 60) : record.EmployeeName; //max 60 chars

            //se
            directDeposit.se[0].TransactionSetControlNumber = directDeposit.st[0].TransactionSetControlNumber;

            return directDeposit;
        }
        private Cheque PopulateChequeDetails(RbcEftEdi record, int chSequenceNumber, RbcEftEdiParameters ediParms)
        {
            //create new object
            Cheque cheque = new Cheque();

            //st
            cheque.st[0].TransactionSetControlNumber = (chSequenceNumber).ToString().PadLeft(9, '0');

            //bpr
            cheque.bpr[0].MonetaryAmount = Convert.ToString(Math.Round(record.ChequeAmount, 2, MidpointRounding.AwayFromZero));
            cheque.bpr[0].OriginatorDepositoryFinancialInstitutionIdentificationNumber = ediParms.DirectDepositBankCodeTransitNumber;
            cheque.bpr[0].OriginatorAccountNumber = ediParms.DirectDepositAccountNumber;
            cheque.bpr[0].PaymentDate = record.ChequeDate;

            //trn
            cheque.trn[0].ReferenceIdentification = record.RbcSequenceNumber.ToString();

            //cur
            //nothing to populate outside of constructor

            //ref1
            cheque.ref1[0].ReferenceId = ediParms.IsaSenderInterchangeID.Substring(0, 9);  //substring of IsaSenderInterchangeID.  First 9 chars

            //ref2
            cheque.ref2[0].ReferenceId = ediParms.ChequesChequeIssuance;

            //ref3
            cheque.ref3[0].ReferenceId = record.RbcSequenceNumber.ToString();

            //ref4
            //for worlinks, no padding will happen as the value will be 0004.  This is only required if the value is empty string, for a company using our product for file generation but not submission, they may not require 0004 but just 4 spaces.
            cheque.ref4[0].DescriptionPayeeMatch = ediParms.DescriptionPayeeMatch.PadLeft(4);

            //custom line 1 in use
            if (ediParms.CustomLine1 != null && ediParms.CustomLine1.Length > 0)
            {
                cheque.customLine1[0] = new ChequeReferenceIdentificationCustom() { Description = (ediParms.CustomLine1.Length > 35) ? ediParms.CustomLine1.Substring(0, 35) : ediParms.CustomLine1 };
                cheque.se[0].NumberOfIncludedSegments = (Convert.ToInt16(cheque.se[0].NumberOfIncludedSegments) + 1).ToString(); //one extra segment if this is included
            }
            //custom line 2 in use
            if (ediParms.CustomLine2 != null && ediParms.CustomLine2.Length > 0)
            {
                cheque.customLine2[0] = new ChequeReferenceIdentificationCustom() { Description = (ediParms.CustomLine2.Length > 35) ? ediParms.CustomLine2.Substring(0, 35) : ediParms.CustomLine2 };
                cheque.se[0].NumberOfIncludedSegments = (Convert.ToInt16(cheque.se[0].NumberOfIncludedSegments) + 1).ToString(); //one extra segment if this is included
            }
            //custom line 3 in use
            if (ediParms.CustomLine3 != null && ediParms.CustomLine3.Length > 0)
            {
                cheque.customLine3[0] = new ChequeReferenceIdentificationCustom() { Description = (ediParms.CustomLine3.Length > 35) ? ediParms.CustomLine3.Substring(0, 35) : ediParms.CustomLine3 };
                cheque.se[0].NumberOfIncludedSegments = (Convert.ToInt16(cheque.se[0].NumberOfIncludedSegments) + 1).ToString(); //one extra segment if this is included
            }

            //ref6
            cheque.ref6[0].Description = (ediParms.CompanyShortName.Length > 24) ? ediParms.CompanyShortName.Substring(0, 24) : ediParms.CompanyShortName;

            //dtm
            //nothing to populate outside of constructor

            //n1pr
            cheque.n1pr[0].OriginatorPayorName = (ediParms.CompanyShortName.Length > 23) ? ediParms.CompanyShortName.Substring(0, 23) : ediParms.CompanyShortName;

            //n1pe
            cheque.n1pe[0].PayeeName = (record.EmployeeName.Length > 60) ? record.EmployeeName.Substring(0, 60) : record.EmployeeName; //max 60 chars
            cheque.n1pe[0].IdentificationCode = (record.EmployeeNumber.Length > 20) ? record.EmployeeNumber.Substring(0, 20) : record.EmployeeNumber; //max 20 chars

            //n3
            cheque.n3[0].AddressInformationLine1 = (record.EmployeeAddressLine1.Length > 35) ? record.EmployeeAddressLine1.Substring(0, 35) : record.EmployeeAddressLine1; //max 35 chars

            //Address additional lines
            if (record.EmployeeAddressLine2 != null && record.EmployeeAddressLine2.Length > 0)
                cheque.n3[0].AddressInformationLine1 += "*" + ((record.EmployeeAddressLine2.Length > 35) ? record.EmployeeAddressLine2.Substring(0, 35) : record.EmployeeAddressLine2); //max 35 chars

            //n4
            cheque.n4[0].ReceiverCityName = (record.EmployeeCity.Length > 35) ? record.EmployeeCity.Substring(0, 30) : record.EmployeeCity; //max 30 chars
            cheque.n4[0].ReceiverStateProvinceCode = record.EmployeeProvinceStateCode;
            cheque.n4[0].ReceiverStatePostalCode = record.EmployeePostalZipCode;
            cheque.n4[0].ReceiverCountryCode = record.EmployeeCountryCode;

            //ent
            cheque.ent[0].AssignedNumber = (record.RbcSequenceNumber.ToString().Length > 6) ? record.RbcSequenceNumber.ToString().Substring(record.RbcSequenceNumber.ToString().Length - 6, 6) : record.RbcSequenceNumber.ToString(); //max 6 chars

            //rmr
            cheque.rmr[0].MonetaryAmount = Convert.ToString(Math.Round(record.ChequeAmount, 2, MidpointRounding.AwayFromZero));

            //ref5
            cheque.ref5[0].Description = (ediParms.CompanyName.Length > 30) ? ediParms.CompanyName.Substring(0, 30) : ediParms.CompanyName;

            //dtm2
            //nothing to populate outside of constructor

            //se
            cheque.se[0].TransactionSetControlNumber = cheque.st[0].TransactionSetControlNumber;

            return cheque;
        }

        #region scotiabank edi file
        public byte[] CreateScotiaBankEdiFile(DatabaseUser user, ScotiaBankEdiCollection collection, string status, ScotiaBankEdiParameters ediParms, long headerSequenceNumber)
        {
            byte[] fileContents = null;

            if (collection != null && collection.Count > 0) //only create eft if we have rows to process
            {
                //isa
                ISA[] isaHeader = new ISA[1] { new ISA()
                    {
                        ISA06 = ediParms.ScotiaBankIsa06.PadRight(15),
                        ISA08 = ediParms.ScotiaBankIsa08.PadRight(15),
                        ISA13 = headerSequenceNumber.ToString().PadLeft(9, '0'),
                        ISA15 = ediParms.ScotiaBankIsa15
                    }
                };
                //gs
                GS[] gsHeader = new GS[1] { new GS() { GS02 = isaHeader[0].ISA06.Replace(" ", ""), GS03 = isaHeader[0].ISA08.Replace(" ", ""), GS06 = isaHeader[0].ISA13 } };

                //number of direct deposit records
                ScotiaDirectDeposit[] directDeposit = new ScotiaDirectDeposit[collection.CountOfRecordsInFile];

                //array indexer
                int directDepositCounter = 0;

                //loop thru collection here and create the ST/SE pairs along with the corresponding DirectDeposit/Cheque classes
                foreach (ScotiaBankEdi record in collection)
                {
                    directDeposit[directDepositCounter] = PopulateScotiaBankDirectDepositDetails(record, ++directDepositCounter, ediParms);
                }

                //ge
                GE[] geTrailer = new GE[1] { new GE() { GE01 = collection.CountOfRecordsInFile.ToString(), GE02 = gsHeader[0].GS06 } };

                //iea
                IEA[] ieaTrailer = new IEA[1] { new IEA() { IEA02 = isaHeader[0].ISA13 } };

                //create the EFT file
                fileContents = convertStringToByteArray(WriteScotiaBankEdiFile(isaHeader, gsHeader, geTrailer, ieaTrailer, directDeposit));

                //update db records from NEW to RP ("Newly Added" to "Record Processed")
                UpdateStatusOfPayrollMasterPaymentRecords(user, null, collection, status);
            }
            return fileContents;
        }

        private ScotiaDirectDeposit PopulateScotiaBankDirectDepositDetails(ScotiaBankEdi record, int ddSequenceNumber, ScotiaBankEdiParameters ediParms)
        {
            //create new object
            ScotiaDirectDeposit directDeposit = new ScotiaDirectDeposit();

            //st
            directDeposit.st[0].ST02 = (ddSequenceNumber).ToString().PadLeft(9, '0');

            //bpr
            directDeposit.bpr[0].BPR02 = Convert.ToString(Math.Round(record.ChequeAmount, 2, MidpointRounding.AwayFromZero));
            directDeposit.bpr[0].BPR07 = ediParms.ScotiaBankBpr07;
            directDeposit.bpr[0].BPR09 = ediParms.ScotiaBankBpr09;
            directDeposit.bpr[0].BPR13 = record.AbaNumber;
            directDeposit.bpr[0].BPR15 = record.EmployeeBankAccountNumber;
            directDeposit.bpr[0].BPR16 = record.ChequeDate;

            //nte
            //nothing to populate outside of constructor

            //trn
            directDeposit.trn[0].TRN02 = record.SequenceNumber.ToString();

            //cur
            //nothing to populate outside of constructor

            //ref
            //nothing to populate outside of constructor

            //dtm
            //nothing to populate outside of constructor

            //n1 payor
            directDeposit.n1pr[0].N1PR02 = (ediParms.CompanyShortName.Length > 35) ? ediParms.CompanyShortName.Substring(0, 35) : ediParms.CompanyShortName; //max 35 chars

            //n2 payor
            directDeposit.n2[0].N201 = (ediParms.CompanyShortName.Length > 15) ? ediParms.CompanyShortName.Substring(0, 15) : ediParms.CompanyShortName; //max 15 chars

            //n1 payee
            directDeposit.n1pe[0].N1PE02 = (record.EmployeeName.Length > 35) ? record.EmployeeName.Substring(0, 35) : record.EmployeeName; //max 35 chars

            //n1Orig
            //nothing to populate outside of constructor

            //n4Orig
            //nothing to populate outside of constructor

            //n1rb
            directDeposit.n1rb[0].N1RB02 = (record.EmployeeName.Length > 35) ? record.EmployeeName.Substring(0, 35) : record.EmployeeName; //max 35 chars

            //n4rb
            //nothing to populate outside of constructor

            //se
            directDeposit.se[0].SE02 = directDeposit.st[0].ST02;

            return directDeposit;
        }

        private String WriteScotiaBankEdiFile(ISA[] isaHeader, GS[] gsHeader, GE[] geTrailer, IEA[] ieaTrailer, ScotiaDirectDeposit[] dd)
        {
            //declare the types of records to be written by the engine
            FileHelpers.MultiRecordEngine eng = new FileHelpers.MultiRecordEngine(typeof(ISA), typeof(GS), typeof(ST), typeof(BPR), typeof(NTE), typeof(TRN), typeof(CUR), typeof(REF),
                typeof(DTM), typeof(N1PR), typeof(N2), typeof(N1PE), typeof(N1Orig), typeof(N4Orig), typeof(N1RB), typeof(N4RB), typeof(SE), typeof(GE), typeof(IEA));

            System.Text.StringBuilder fileData = new System.Text.StringBuilder();

            fileData.Append(eng.WriteString(isaHeader));
            fileData.Append(eng.WriteString(gsHeader));

            if (dd.Length > 0)
            {
                for (int i = 0; i < dd.Length; i++)
                {
                    fileData.Append(eng.WriteString(dd[i].st));
                    fileData.Append(eng.WriteString(dd[i].bpr));
                    fileData.Append(eng.WriteString(dd[i].nte));
                    fileData.Append(eng.WriteString(dd[i].trn));
                    fileData.Append(eng.WriteString(dd[i].cur));
                    fileData.Append(eng.WriteString(dd[i].ref1));
                    fileData.Append(eng.WriteString(dd[i].dtm));
                    fileData.Append(eng.WriteString(dd[i].n1pr));
                    fileData.Append(eng.WriteString(dd[i].n2));
                    fileData.Append(eng.WriteString(dd[i].n1pe));
                    fileData.Append(eng.WriteString(dd[i].n1Orig));
                    fileData.Append(eng.WriteString(dd[i].n4Orig));
                    fileData.Append(eng.WriteString(dd[i].n1rb));
                    fileData.Append(eng.WriteString(dd[i].n4rb));
                    fileData.Append(eng.WriteString(dd[i].se));
                }
            }

            fileData.Append(eng.WriteString(geTrailer));
            fileData.Append(eng.WriteString(ieaTrailer));

            return fileData.ToString();
        }

        #endregion
        public byte[] CreateRbcEftEdiFile(DatabaseUser user, RbcEftEdiCollection collection, String status, RbcEftEdiParameters ediParms, long headerSequenceNumber)
        {
            byte[] fileContents = null;

            if (collection != null && collection.Count > 0) //only create eft if we have rows to process
            {
                //isa
                IsaInterchangeControlHeader[] isaHeader = new IsaInterchangeControlHeader[1]
                    {
                        new IsaInterchangeControlHeader()
                        {
                            SenderInterchangeID = ediParms.IsaSenderInterchangeID.PadRight(15, ' '),                 //must be 15 chars
                            ReceiverInterchangeID = ediParms.IsaReceiverInterchangeID.PadRight(15, ' '),             //must be 15 chars
                            InterchangeControlNumber = headerSequenceNumber.ToString().PadLeft(9, '0'),           //must be 9 chars
                            TestIndicator = ediParms.IsaTestIndicator
                        }
                    };

                //gs
                GsFunctionalGroupHeader[] gsHeader = new GsFunctionalGroupHeader[1]
                    {
                        new GsFunctionalGroupHeader()
                        {
                            SenderInterchangeID = ediParms.IsaSenderInterchangeID,
                            ApplicationReceiversCode = ediParms.IsaReceiverInterchangeID,
                            GroupControlNumber = (headerSequenceNumber.ToString().Length) > 4 ? headerSequenceNumber.ToString().Substring(headerSequenceNumber.ToString().Length-4, 4) : headerSequenceNumber.ToString()         //max 4 chars, no leading 0s required
                        }
                    };

                //number of direct deposit records
                int numDirectDeposits = collection.Count(n => n.DirectDepositFlag == true && n.CodeBankingCountryCd == "CA");
                int numDirectDepositsUS = collection.Count(n => n.DirectDepositFlag == true && n.CodeBankingCountryCd == "US");
                int numCheques = collection.Count(n => n.DirectDepositFlag == false && n.ManualChequeFlag == false);

                //create direct deposit/cheque objects
                DirectDeposit[] directDeposit = new DirectDeposit[numDirectDeposits];
                DirectDepositUS[] directDepositUS = new DirectDepositUS[numDirectDepositsUS];
                Cheque[] cheques = new Cheque[numCheques];

                //array indexer
                int directDepositCounter = 0;
                int directDepositUSCounter = 0;
                int chequeCounter = 0;

                //sequence used in directdeposits/cheque segments
                int ddSequenceNumber = 1;
                int chSequenceNumber = numDirectDeposits + numDirectDepositsUS + 1;   //this will start after the direct deposits sequence

                //loop thru collection here and create the ST/SE pairs along with the corresponding DirectDeposit/Cheque classes
                foreach (RbcEftEdi record in collection)
                {
                    if (record.DirectDepositFlag && record.CodeBankingCountryCd == "CA") //direct deposit
                    {
                        directDeposit[directDepositCounter] = PopulateDirectDepositDetails(record, ddSequenceNumber, ediParms);

                        //increment counters
                        directDepositCounter++;
                        ddSequenceNumber++;
                    }
                    else if (record.DirectDepositFlag && record.CodeBankingCountryCd == "US")
                    {
                        directDepositUS[directDepositUSCounter] = PopulateDirectDepositDetailsUS(record, ddSequenceNumber, ediParms);

                        //increment counters
                        directDepositUSCounter++;
                        ddSequenceNumber++;
                    }
                    else if (!record.DirectDepositFlag && !record.ManualChequeFlag)           //cheque
                    {
                        cheques[chequeCounter] = PopulateChequeDetails(record, chSequenceNumber, ediParms);

                        //increment counters
                        chequeCounter++;
                        chSequenceNumber++;
                    }
                }

                //ge (one transaction set per record in the collection)
                GsFunctionalGroupTrailer[] geTrailer = new GsFunctionalGroupTrailer[1] { new GsFunctionalGroupTrailer() { NumberOfTransactionSetsIncluded = collection.CountOfRecordsInFile.ToString(), GroupControlNumber = gsHeader[0].GroupControlNumber } };

                //iea
                IsaInterchangeControlTrailer[] ieaTrailer = new IsaInterchangeControlTrailer[1] { new IsaInterchangeControlTrailer() { InterchangeControlNumber = isaHeader[0].InterchangeControlNumber } };

                //create the EFT file
                fileContents = convertStringToByteArray(WriteRbcEftEdiFile(isaHeader, gsHeader, geTrailer, ieaTrailer, directDeposit, directDepositUS, cheques));

                //update db records from NEW to RP ("Newly Added" to "Record Processed")
                UpdateStatusOfPayrollMasterPaymentRecords(user, collection, null, status);
            }

            return fileContents;
        }

        private void UpdateStatusOfPayrollMasterPaymentRecords(DatabaseUser user, RbcEftEdiCollection collection, ScotiaBankEdiCollection scotia, string status)
        {
            _payrollAccess.UpdateStatusOfPayrollMasterPaymentRecords(user, collection, scotia, status);
        }

        private String WriteRbcEftEdiFile(IsaInterchangeControlHeader[] isaHeader, GsFunctionalGroupHeader[] gsHeader, GsFunctionalGroupTrailer[] geTrailer, IsaInterchangeControlTrailer[] ieaTrailer, DirectDeposit[] dd, DirectDepositUS[] ddUS, Cheque[] cheques)
        {
            //declare the types of records to be written by the engine
            FileHelpers.MultiRecordEngine eng = new FileHelpers.MultiRecordEngine(typeof(IsaInterchangeControlHeader), typeof(GsFunctionalGroupHeader), typeof(GsFunctionalGroupTrailer), typeof(IsaInterchangeControlTrailer),
                typeof(DirectDepositStTransactionSetHeader), typeof(DirectDepositBeginningSegmentForPaymentOrderRemittanceAdvice), typeof(DirectDepositTrace), typeof(DirectDepositCurrency), typeof(DirectDepositReferenceIdentification), typeof(DirectDepositDateTimeReference),
                typeof(DirectDepositOriginatorNamePayor), typeof(DirectDepositPayeeReceiverName), typeof(DirectDepositSeTransactionSetTrailer), typeof(ChequeStTransactionSetHeader), typeof(ChequeBeginningSegmentForPaymentOrderRemittanceAdvice), typeof(ChequeTrace),
                typeof(ChequeCurrency), typeof(ChequeReferenceIdentification), typeof(ChequeReferenceIdentification2), typeof(ChequeReferenceIdentification3), typeof(ChequeReferenceIdentification4), typeof(ChequeDateTimeReference), typeof(ChequeOriginatorNamePayor),
                typeof(ChequePayeeReceiverName), typeof(ChequePayeeReceiverAddressInformation), typeof(PayeeReceiverGeographicLocation), typeof(ChequeEntity), typeof(ChequeRemittanceAdviceAccountsReceivableOpenItemReference), typeof(ChequeReferenceIdentification5), typeof(ChequeSeTransactionSetTrailer),
                typeof(ChequeDateTimeReference2), typeof(DirectDepositAdditionalNameInformationPayor), typeof(ChequePayeeReceiverAddressInformation2), typeof(ChequeReferenceIdentification6), typeof(ChequeReferenceIdentificationCustom), typeof(UsDirectDepositTrace), typeof(UsDirectDepositCurrency)
                );

            System.Text.StringBuilder fileData = new System.Text.StringBuilder();

            fileData.Append(eng.WriteString(isaHeader));
            fileData.Append(eng.WriteString(gsHeader));

            //write out direct deposits if they exist
            if (dd.Length > 0)
            {
                for (int i = 0; i < dd.Length; i++)
                {
                    fileData.Append(eng.WriteString(dd[i].st));
                    fileData.Append(eng.WriteString(dd[i].bpr));
                    fileData.Append(eng.WriteString(dd[i].trn));
                    fileData.Append(eng.WriteString(dd[i].cur));
                    fileData.Append(eng.WriteString(dd[i].ref1));
                    fileData.Append(eng.WriteString(dd[i].dtm));
                    fileData.Append(eng.WriteString(dd[i].n1pr));
                    fileData.Append(eng.WriteString(dd[i].n2));
                    fileData.Append(eng.WriteString(dd[i].n1pe));
                    fileData.Append(eng.WriteString(dd[i].se));
                }
            }
            //write out US direct deposits if they exist
            if (ddUS.Length > 0)
            {
                for (int i = 0; i < ddUS.Length; i++)
                {
                    fileData.Append(eng.WriteString(ddUS[i].st));
                    fileData.Append(eng.WriteString(ddUS[i].bpr));
                    fileData.Append(eng.WriteString(ddUS[i].trnUs));
                    fileData.Append(eng.WriteString(ddUS[i].curUs));
                    fileData.Append(eng.WriteString(ddUS[i].ref1));
                    fileData.Append(eng.WriteString(ddUS[i].dtm));
                    fileData.Append(eng.WriteString(ddUS[i].n1pr));
                    fileData.Append(eng.WriteString(ddUS[i].n2));
                    fileData.Append(eng.WriteString(ddUS[i].n3Employer));
                    fileData.Append(eng.WriteString(ddUS[i].n4Employer));
                    fileData.Append(eng.WriteString(ddUS[i].n1pe));
                    fileData.Append(eng.WriteString(ddUS[i].n3));
                    fileData.Append(eng.WriteString(ddUS[i].n4));
                    fileData.Append(eng.WriteString(ddUS[i].se));
                }
            }
            //write out cheques if they exist
            if (cheques.Length > 0)
            {
                for (int i = 0; i < cheques.Length; i++)
                {
                    fileData.Append(eng.WriteString(cheques[i].st));
                    fileData.Append(eng.WriteString(cheques[i].bpr));
                    fileData.Append(eng.WriteString(cheques[i].trn));
                    fileData.Append(eng.WriteString(cheques[i].cur));
                    fileData.Append(eng.WriteString(cheques[i].ref1));
                    fileData.Append(eng.WriteString(cheques[i].ref2));
                    fileData.Append(eng.WriteString(cheques[i].ref3));
                    fileData.Append(eng.WriteString(cheques[i].ref4));
                    //if custom line 1 was present
                    if (cheques[i].customLine1[0] != null)
                        fileData.Append(eng.WriteString(cheques[i].customLine1));
                    //if custom line 2 was present
                    if (cheques[i].customLine2[0] != null)
                        fileData.Append(eng.WriteString(cheques[i].customLine2));
                    //if custom line 3 was present
                    if (cheques[i].customLine3[0] != null)
                        fileData.Append(eng.WriteString(cheques[i].customLine3));
                    fileData.Append(eng.WriteString(cheques[i].ref6));
                    fileData.Append(eng.WriteString(cheques[i].dtm));
                    fileData.Append(eng.WriteString(cheques[i].n1pr));
                    fileData.Append(eng.WriteString(cheques[i].n1pe));
                    fileData.Append(eng.WriteString(cheques[i].n3));
                    //if address line 3 was present
                    if (cheques[i].n3Part2[0] != null)
                        fileData.Append(eng.WriteString(cheques[i].n3Part2));
                    fileData.Append(eng.WriteString(cheques[i].n4));
                    fileData.Append(eng.WriteString(cheques[i].ent));
                    fileData.Append(eng.WriteString(cheques[i].rmr));
                    fileData.Append(eng.WriteString(cheques[i].ref5));
                    fileData.Append(eng.WriteString(cheques[i].dtm2));
                    fileData.Append(eng.WriteString(cheques[i].se));
                }
            }

            fileData.Append(eng.WriteString(geTrailer));
            fileData.Append(eng.WriteString(ieaTrailer));

            return fileData.ToString();
        }

        #region cra garnishment
        public byte[] CreateCraEftGarnishmentDeductionFile(DatabaseUser user, CraGarnishmentCollection collection, RbcEftSourceDeductionParameters garnishmentParms, long headerSequenceNumber, long transactionSequenceNumber)
        {
            byte[] fileContents = null;

            if (collection != null && collection.Count > 0) //only create eft if we have rows to process
            {
                int numOfLoopingSegments = collection.Count; //compute this for number of repeating segments within the ls/le segment

                GarnishmentDetails[] garnishment = new GarnishmentDetails[1] { new GarnishmentDetails(numOfLoopingSegments) };
                garnishment[0] = PopulateGarnishmentDetails(garnishment[0], collection, headerSequenceNumber, transactionSequenceNumber, garnishmentParms);

                //create the EFT file
                fileContents = convertStringToByteArray(WriteCraEftGarnishmentFile(garnishment));
            }

            return fileContents;
        }
        private GarnishmentDetails PopulateGarnishmentDetails(GarnishmentDetails garnishment, CraGarnishmentCollection coll, long headerSequenceNumber, long transactionSequenceNumber, RbcEftSourceDeductionParameters garnishmentParms)
        {
            //isa
            garnishment.isaHeader[0].SenderInterchangeID = garnishmentParms.IsaSenderInterchangeIDSourceDed.PadRight(15, ' ');      //must be 15 chars
            garnishment.isaHeader[0].ReceiverInterchangeID = garnishmentParms.IsaReceiverInterchangeIDSourceDed.PadRight(15, ' ');  //must be 15 chars
            garnishment.isaHeader[0].InterchangeControlNumber = headerSequenceNumber.ToString().PadLeft(9, '0');                          //must be 9 chars
            garnishment.isaHeader[0].TestIndicator = garnishmentParms.IsaTestIndicatorSourceDed;

            //gs
            garnishment.gsHeader[0].SenderInterchangeID = garnishmentParms.GsSenderInterchangeIdSourceDed;
            garnishment.gsHeader[0].ApplicationReceiversCode = garnishmentParms.GsApplicationReceiversCodeSourceDed;
            garnishment.gsHeader[0].GroupControlNumber = (headerSequenceNumber.ToString().Length) > 4 ? headerSequenceNumber.ToString().Substring(headerSequenceNumber.ToString().Length - 4, 4) : headerSequenceNumber.ToString();//max 4 chars, no leading 0s required

            //st
            //nothing to populate outside of constructor

            /*  
             *  If this is a test file, the BPS.PaymentAmount must be < $5 and so does the RMT.RemittanceAmount, therefore i'm assigning $0.01 to employee
             *  in the ls/le loop and the total is in the BPS ($0.01 x number of employees)
            */

            //bps
            if (garnishmentParms.IsaTestIndicatorSourceDed == "T") //if this is a test file, we must send a PaymentAmount less than $5
                garnishment.bps[0].PaymentAmount = (garnishment.repeatingElements.Length * 0.01M).ToString();
            else
                garnishment.bps[0].PaymentAmount = Convert.ToString(Math.Round(coll.SumGarnishmentAmount, 2, MidpointRounding.AwayFromZero));  //property in coll to total employee amounts

            garnishment.bps[0].OriginatingCompanySupplementalCode = "GARNISHEE";
            garnishment.bps[0].OriginatorDepositoryFinancialInstitutionIdentificationNumber = garnishmentParms.DirectDepositBankCodeTransitNumber;
            garnishment.bps[0].OriginatorAccountNumber = garnishmentParms.DirectDepositAccountNumber;
            garnishment.bps[0].ReceiverDepositoryFinancialInstitutionIdentificationNumber = garnishmentParms.CraBankCodeTransitNumberSourceDed;
            garnishment.bps[0].ReceiverAccountNumber = garnishmentParms.CraAccountNumberSourceDed;
            garnishment.bps[0].EffectiveEntryDate = coll[0].ChequeDate;

            //ref1
            garnishment.ref1[0].ReferenceNumber = transactionSequenceNumber.ToString();

            //ref2
            garnishment.ref2[0].ReferenceNumber = garnishmentParms.RefItReferenceNumberSourceDed;

            //n1pr
            garnishment.n1pr[0].Name = garnishmentParms.N1PrNameSourceDed;

            //dtm, n1pe, per
            //nothing to populate outside of constructor

            //ls
            garnishment.ls[0].LoopIdentifierCode = "1";


            //loop thru repeating elements
            for (int i = 0; i < garnishment.repeatingElements.Length; i++)
            {
                //n1pr2
                garnishment.repeatingElements[i].n1pr2[0].Name = (coll[i].EmployeeName.Length > 35) ? coll[i].EmployeeName.Substring(0, 35) : coll[i].EmployeeName;

                //rmt
                garnishment.repeatingElements[i].rmt[0].SIN = coll[i].SocialInsuranceNumber;

                if (garnishmentParms.IsaTestIndicatorSourceDed == "T") //if this is a test file, we must send a PaymentAmount less than $5
                    garnishment.repeatingElements[i].rmt[0].GarnishmentAmount = "0.01";
                else
                    garnishment.repeatingElements[i].rmt[0].GarnishmentAmount = Convert.ToString(Math.Round(coll[i].GarnishmentAmount, 2, MidpointRounding.AwayFromZero));

                //ref3
                garnishment.repeatingElements[i].ref3[0].ReferenceNumber = "1013";

                //dtm
                garnishment.repeatingElements[i].dtm2[0].Date = coll[i].ChequeDate;
            }

            //le
            garnishment.le[0].LoopIdentifierCode = garnishment.ls[0].LoopIdentifierCode;

            //se
            if (garnishment.repeatingElements.Length > 1)//if repeating segments in ls/le exist, add each extra set (4 nodes per set) to the constructor default of 15 (constructor default includes 1 set already as at least one employee has to be in the file)
                garnishment.se[0].NumberOfIncludedSegments = (Convert.ToInt16(garnishment.se[0].NumberOfIncludedSegments) + (4 * (garnishment.repeatingElements.Length - 1))).ToString();

            //ge
            garnishment.geTrailer[0].GroupControlNumber = garnishment.gsHeader[0].GroupControlNumber;

            //iea
            garnishment.ieaTrailer[0].InterchangeControlNumber = garnishment.isaHeader[0].InterchangeControlNumber;

            return garnishment;
        }
        private String WriteCraEftGarnishmentFile(GarnishmentDetails[] garnishmentDetails)
        {
            FileHelpers.MultiRecordEngine eng = new FileHelpers.MultiRecordEngine(typeof(IsaInterchangeControlHeaderSourceDed), typeof(GsFunctionalGroupHeaderSourceDed), typeof(GsFunctionalGroupTrailerSourceDed), typeof(IsaInterchangeControlTrailerSourceDed), typeof(SourceDeductionsStTransactionSetHeader),
                    typeof(SourceDeductionsBeginningSegment), typeof(SourceDeductionsReferenceIdentification), typeof(SourceDeductionsReferenceIdentification2), typeof(SourceDeductionsDateTimeReference), typeof(SourceDeductionsPayeeReceiverName), typeof(SourceDeductionsPayorReceiverName),
                    typeof(SourceDeductionsAdministrativeCommunicationsContact), typeof(SourceDeductionsLoopHeader), typeof(SourceDeductionsPayorReceiverName2), typeof(GarnishmentSourceDeductionsRemittanceAdvice), typeof(SourceDeductionsReferenceIdentification3), typeof(SourceDeductionsDateTimeReference2),
                    typeof(SourceDeductionsLoopTrailer), typeof(SourceDeductionsSeTransactionSetTrailer));

            System.Text.StringBuilder fileData = new System.Text.StringBuilder();

            fileData.Append(eng.WriteString(garnishmentDetails[0].isaHeader));
            fileData.Append(eng.WriteString(garnishmentDetails[0].gsHeader));
            fileData.Append(eng.WriteString(garnishmentDetails[0].st));
            fileData.Append(eng.WriteString(garnishmentDetails[0].bps));
            fileData.Append(eng.WriteString(garnishmentDetails[0].ref1));
            fileData.Append(eng.WriteString(garnishmentDetails[0].ref2));
            fileData.Append(eng.WriteString(garnishmentDetails[0].dtm));
            fileData.Append(eng.WriteString(garnishmentDetails[0].n1pe));
            fileData.Append(eng.WriteString(garnishmentDetails[0].n1pr));
            fileData.Append(eng.WriteString(garnishmentDetails[0].per));
            fileData.Append(eng.WriteString(garnishmentDetails[0].ls));

            //lopping ls/le segments
            for (int i = 0; i < garnishmentDetails[0].repeatingElements.Length; i++)
            {
                fileData.Append(eng.WriteString(garnishmentDetails[0].repeatingElements[i].n1pr2));
                fileData.Append(eng.WriteString(garnishmentDetails[0].repeatingElements[i].rmt));
                fileData.Append(eng.WriteString(garnishmentDetails[0].repeatingElements[i].ref3));
                fileData.Append(eng.WriteString(garnishmentDetails[0].repeatingElements[i].dtm2));
            }

            fileData.Append(eng.WriteString(garnishmentDetails[0].le));
            fileData.Append(eng.WriteString(garnishmentDetails[0].se));
            fileData.Append(eng.WriteString(garnishmentDetails[0].geTrailer));
            fileData.Append(eng.WriteString(garnishmentDetails[0].ieaTrailer));

            return fileData.ToString();
        }
        #endregion

        #region cra nova scotia wcb remittances
        public byte[] CreateCraNsWcbSourceDeductionFile(DatabaseUser user, CraWcbRemittanceSummaryCollection collection, RbcEftSourceDeductionParameters sourceParms, String craRemittancePeriodCode, long headerSequenceNumber, long transactionSequenceNumber)
        {
            //this method reuses the classes from RbcEftEdiSourceDed since they have the exact same structure but some default values will be overwritten
            byte[] fileContents = null;

            if (collection != null && collection.Count > 0) //only create eft if we have rows to process
            {
                int numOfLoopingSegments = collection.Count; //compute this for number of repeating segments within the ls/le segment

                //isa
                IsaInterchangeControlHeaderSourceDed[] isaHeader = new IsaInterchangeControlHeaderSourceDed[1]
                    {
                        new IsaInterchangeControlHeaderSourceDed()
                        {
                            SenderInterchangeID = sourceParms.IsaSenderInterchangeIDSourceDed.PadRight(15, ' '),                 //must be 15 chars
                            ReceiverInterchangeID = sourceParms.IsaReceiverInterchangeIDSourceDed.PadRight(15, ' '),             //must be 15 chars
                            InterchangeControlNumber = headerSequenceNumber.ToString().PadLeft(9, '0'),           //must be 9 chars
                            TestIndicator = sourceParms.IsaTestIndicatorSourceDed
                        }
                    };

                //gs
                GsFunctionalGroupHeaderSourceDed[] gsHeader = new GsFunctionalGroupHeaderSourceDed[1]
                    {
                        new GsFunctionalGroupHeaderSourceDed()
                        {
                            SenderInterchangeID = sourceParms.GsSenderInterchangeIdSourceDed,
                            ApplicationReceiversCode = sourceParms.GsApplicationReceiversCodeSourceDed,
                            GroupControlNumber = (headerSequenceNumber.ToString().Length) > 4 ? headerSequenceNumber.ToString().Substring(headerSequenceNumber.ToString().Length-4, 4) : headerSequenceNumber.ToString()         //max 4 chars, no leading 0s required
                        }
                    };

                //create details segments, constructor takes an int for number of repeating LS/LE segments
                SourceDeductionDetails[] sourceDetails = new SourceDeductionDetails[1] { new SourceDeductionDetails(numOfLoopingSegments) };
                sourceDetails[0] = PopulateNsWcbSourceDeductionDetails(sourceDetails[0], collection, transactionSequenceNumber, sourceParms, craRemittancePeriodCode);

                //ge
                GsFunctionalGroupTrailerSourceDed[] geTrailer = new GsFunctionalGroupTrailerSourceDed[1] { new GsFunctionalGroupTrailerSourceDed() { GroupControlNumber = gsHeader[0].GroupControlNumber } };

                //iea
                IsaInterchangeControlTrailerSourceDed[] ieaTrailer = new IsaInterchangeControlTrailerSourceDed[1] { new IsaInterchangeControlTrailerSourceDed() { InterchangeControlNumber = isaHeader[0].InterchangeControlNumber } };

                //create the EFT file
                fileContents = convertStringToByteArray(WriteRbcEftSourceDeductionsFile(isaHeader, gsHeader, geTrailer, ieaTrailer, sourceDetails));
            }

            return fileContents;
        }

        private SourceDeductionDetails PopulateNsWcbSourceDeductionDetails(SourceDeductionDetails sourceDeductions, CraWcbRemittanceSummaryCollection coll, long transactionSequenceNumber, RbcEftSourceDeductionParameters sourceParms, String craRemittancePeriodCode)
        {
            /*  
             *  If this is a test file, the BPS.PaymentAmount must be < $5 and so does the RMT.RemittanceAmount, therefore i'm assigning $0.01 to each biz number 
             *  in the ls/le loop and the total is in the BPS ($0.01 x number of biz numbers)
            */

            //st
            //nothing to populate outside of constructor

            //bps
            if (sourceParms.IsaTestIndicatorSourceDed == "T") //if this is a test file, we must send a PaymentAmount less than $5
                sourceDeductions.bps[0].PaymentAmount = (sourceDeductions.repeatingElements.Length * 0.01M).ToString();
            else
                sourceDeductions.bps[0].PaymentAmount = Convert.ToString(Math.Round(coll.SumPaymentAmount, 2, MidpointRounding.AwayFromZero));  //property in coll to total biz # amounts

            sourceDeductions.bps[0].OriginatorDepositoryFinancialInstitutionIdentificationNumber = sourceParms.DirectDepositBankCodeTransitNumber;
            sourceDeductions.bps[0].OriginatorAccountNumber = sourceParms.DirectDepositAccountNumber;
            sourceDeductions.bps[0].ReceiverDepositoryFinancialInstitutionIdentificationNumber = sourceParms.CraBankCodeTransitNumberSourceDed;
            sourceDeductions.bps[0].ReceiverAccountNumber = sourceParms.CraAccountNumberSourceDed;
            sourceDeductions.bps[0].EffectiveEntryDate = Convert.ToDateTime(coll[0].EffectiveEntryDate);

            //ref1
            sourceDeductions.ref1[0].ReferenceNumber = transactionSequenceNumber.ToString();

            //ref2
            sourceDeductions.ref2[0].ReferenceNumber = sourceParms.RefItReferenceNumberSourceDed;

            //n1pr
            sourceDeductions.n1pr[0].Name = sourceParms.N1PrNameSourceDed;

            //dtm, n1pe, per
            //nothing to populate outside of constructor

            //ls
            sourceDeductions.ls[0].LoopIdentifierCode = "1";

            //loop thru repeating elements
            for (int i = 0; i < sourceDeductions.repeatingElements.Length; i++)
            {
                //n1pr2
                sourceDeductions.repeatingElements[i].n1pr2[0].Name = (sourceParms.CompanyShortName.Length > 35) ? sourceParms.CompanyShortName.Substring(0, 35) : sourceParms.CompanyShortName;

                //rmt
                sourceDeductions.repeatingElements[i].rmt[0].BusinessNumber = coll[i].WorkersCompensationAccountNumber;

                if (sourceParms.IsaTestIndicatorSourceDed == "T") //if this is a test file, we must send a PaymentAmount less than $5
                    sourceDeductions.repeatingElements[i].rmt[0].RemittanceAmount = "0.01";
                else
                    sourceDeductions.repeatingElements[i].rmt[0].RemittanceAmount = Convert.ToString(Math.Round(coll[i].CurrentPremium, 2, MidpointRounding.AwayFromZero));

                sourceDeductions.repeatingElements[i].rmt[0].NumberOfEmployeesConcatGrossPayrollAmount = coll[i].EmployeeCount.ToString().PadLeft(6, '0').Substring(0, 6) + Math.Round(coll[i].CurrentAssessableEarnings, 0, MidpointRounding.AwayFromZero).ToString().PadLeft(9, '0');

                //ref3
                sourceDeductions.repeatingElements[i].ref3[0].ReferenceNumber = "1105"; //overwrite default with code for WCB

                //dtm
                sourceDeductions.repeatingElements[i].dtm2[0].Date = Convert.ToDateTime(coll[0].RemittancePeriodEndDate);
            }

            //le
            sourceDeductions.le[0].LoopIdentifierCode = sourceDeductions.ls[0].LoopIdentifierCode;

            //se
            if (sourceDeductions.repeatingElements.Length > 1)//if repeating segments in ls/le exist, add each extra set (4 nodes per set) to the constructor default of 15 (constructor default includes 1 set already as at least one business number has to be in the file)
                sourceDeductions.se[0].NumberOfIncludedSegments = (Convert.ToInt16(sourceDeductions.se[0].NumberOfIncludedSegments) + (4 * (sourceDeductions.repeatingElements.Length - 1))).ToString();

            return sourceDeductions;
        }
        #endregion

        #region revenu quebec remittances
        public byte[] CreateRevenuQuebecExportFile(DatabaseUser user, RevenuQuebecRemittanceSummaryCollection collection, RbcRqSourceDeductionParameters sourceParms, String rqRemittancePeriodCode, long headerSequenceNumber, long transactionSequenceNumber)
        {
            byte[] fileContents = null;

            if (collection != null && collection.Count > 0) //only create eft if we have rows to process
            {

                int numOfLoopingSegments = collection.Count; //compute this for number of repeating segments within the ls/le segment

                //create our object and populate
                RbcRqRemittanceDetails rqFile = new RbcRqRemittanceDetails(numOfLoopingSegments);

                //isa
                rqFile.isaHeader[0].SenderInterchangeID = sourceParms.IsaSenderInterchangeIDSourceDed.PadRight(15, ' ');       //must be 15 chars
                rqFile.isaHeader[0].ReceiverInterchangeID = sourceParms.IsaReceiverInterchangeIDSourceDed.PadRight(15, ' ');   //must be 15 chars
                rqFile.isaHeader[0].InterchangeControlNumber = headerSequenceNumber.ToString().PadLeft(9, '0');       //must be 9 chars
                rqFile.isaHeader[0].TestIndicator = sourceParms.IsaTestIndicatorSourceDed;

                //gs
                rqFile.gsHeader[0].SenderInterchangeID = sourceParms.IsaSenderInterchangeIDSourceDed;
                rqFile.gsHeader[0].ApplicationReceiversCode = sourceParms.GsApplicationReceiversCodeSourceDed;
                rqFile.gsHeader[0].GroupControlNumber = (headerSequenceNumber.ToString().Length) > 4 ? headerSequenceNumber.ToString().Substring(headerSequenceNumber.ToString().Length - 4, 4) : headerSequenceNumber.ToString();         //max 4 chars, no leading 0s required

                //st
                //nothing to populate outside of constructor

                //bpr
                rqFile.bpr[0].MonetaryAmount = Convert.ToString(Math.Round(collection.SumTotalRemits, 2, MidpointRounding.AwayFromZero));
                rqFile.bpr[0].OriginatorDepositoryFinancialInstitutionIdentificationNumber = sourceParms.DirectDepositBankCodeTransitNumber;
                rqFile.bpr[0].OriginatorAccountNumber = sourceParms.DirectDepositAccountNumber;
                rqFile.bpr[0].RqFiAndTransitNumber = sourceParms.RqFiTransitNumber;
                rqFile.bpr[0].RqBankAccountNumber = sourceParms.RqBankAccount;
                rqFile.bpr[0].DepositDate = Convert.ToDateTime(collection[0].EffectiveEntryDate);

                //trn
                rqFile.trn[0].ReferenceIdentification = transactionSequenceNumber.ToString().PadLeft(4, '0');

                //ref1
                //YYYYMM9999 = YYYYMM = payment period, 9999 = # of remittance advices in txn
                //YYYY = year for remittance period, MM = month for remittance period
                //000X = number of ENT segments padded with leading 0s so it is 4 chars in length
                rqFile.ref1[0].DatePlusTransNums = Convert.ToDateTime(collection[0].RemittancePeriodEndDate).Year.ToString() + Convert.ToDateTime(collection[0].RemittancePeriodEndDate).ToString("MM") + collection.Count.ToString().PadLeft(4, '0');

                //ref2, ref3, dtm
                //nothing to populate outside of constructor

                //n1pr                
                rqFile.n1pr[0].PartnerName = sourceParms.N1PrNameSourceDed;
                rqFile.n1pr[0].PartnerId = sourceParms.RqPartnerId;

                //n1pe, n1de
                //nothing to populate outside of constructor

                //loop thru repeating elements
                for (int i = 0; i < rqFile.repeatingElements.Length; i++)
                {
                    //ent
                    //MMMMMMMMMMCC9999SSFFF, MMMMMMMMMMCC9999 = quebec tax number from code_system, SS = RqSubdivisionCode from collection, FFF = RqFormCode from collection
                    rqFile.repeatingElements[i].ent[0].EmployersIdNumber = collection[i].RevenuQuebecTaxId + collection[i].RqSubdivisionCode + collection[i].RqFormCode;

                    //rmrImpo
                    rqFile.repeatingElements[i].rmrImpo[0].MonetaryAmount = Convert.ToString(Math.Round(collection[i].QuebecTax, 2, MidpointRounding.AwayFromZero));

                    //rmrRrq
                    rqFile.repeatingElements[i].rmrRrq[0].MonetaryAmount = Convert.ToString(Math.Round(collection[i].TotalQuebecPensionPlanContribution, 2, MidpointRounding.AwayFromZero));

                    //rmrRamq
                    rqFile.repeatingElements[i].rmrRamq[0].MonetaryAmount = Convert.ToString(Math.Round(collection[i].TotalHealthTaxLevy, 2, MidpointRounding.AwayFromZero));

                    //rmrRqap
                    rqFile.repeatingElements[i].rmrRqap[0].MonetaryAmount = Convert.ToString(Math.Round(collection[i].TotalQuebecParentalInsurancePlanContribution, 2, MidpointRounding.AwayFromZero));

                    //rmrCsst
                    rqFile.repeatingElements[i].rmrCsst[0].MonetaryAmount = Convert.ToString(Math.Round(collection[i].EmployerWorkersCompensationPremium, 2, MidpointRounding.AwayFromZero));
                }

                //se
                rqFile.se[0].TransactionSetControlNumber = rqFile.st[0].TransactionSetControlNumber;
                if (rqFile.repeatingElements.Length > 1)
                    rqFile.se[0].NumberOfIncludedSegments = (Convert.ToInt16(rqFile.se[0].NumberOfIncludedSegments) + (6 * (rqFile.repeatingElements.Length - 1))).ToString();

                //ge
                rqFile.geTrailer[0].GroupControlNumber = rqFile.gsHeader[0].GroupControlNumber;

                //iea
                rqFile.ieaTrailer[0].InterchangeControlNumber = rqFile.isaHeader[0].InterchangeControlNumber;


                //add to array to make writing easier
                RbcRqRemittanceDetails[] rqFileArray = new RbcRqRemittanceDetails[1];
                rqFileArray[0] = rqFile;

                //create the EFT file
                String fileData = WriteRbcRqRemittanceFile(rqFileArray);
                //End of line (EOL) characters for this file are line feed (LF) and carriage return (CR).  
                //revenu quebec requires that there only be a LF character at the end of the line, not a LF + CR, so remove the CR.
                fileData = fileData.Replace("\r", "");

                fileContents = convertStringToByteArray(fileData);
            }

            return fileContents;
        }

        private String WriteRbcRqRemittanceFile(RbcRqRemittanceDetails[] rqFile)
        {
            //declare the types of records to be written by the engine
            FileHelpers.MultiRecordEngine eng = new FileHelpers.MultiRecordEngine(typeof(IsaInterchangeControlHeaderSourceDed), typeof(RqGsFunctionalGroupHeaderSourceDed), typeof(SourceDeductionsStTransactionSetHeader), typeof(RqChequeBeginningSegmentForPaymentOrderRemittanceAdvice), typeof(ChequeTrace), typeof(RqSourceDeductionsReferenceIdentification),
                typeof(SourceDeductionsReferenceIdentification2), typeof(RqRefRe), typeof(ChequeDateTimeReference), typeof(RqOriginatorNamePayor), typeof(SourceDeductionsPayeeReceiverName), typeof(RqN1De), typeof(RqEntity), typeof(RqRemittanceSegment), typeof(SourceDeductionsSeTransactionSetTrailer), typeof(GsFunctionalGroupTrailerSourceDed), typeof(IsaInterchangeControlTrailerSourceDed));

            System.Text.StringBuilder fileData = new System.Text.StringBuilder();
            fileData.Append(eng.WriteString(rqFile[0].isaHeader));
            fileData.Append(eng.WriteString(rqFile[0].gsHeader));
            fileData.Append(eng.WriteString(rqFile[0].st));

            fileData.Append(eng.WriteString(rqFile[0].bpr));
            fileData.Append(eng.WriteString(rqFile[0].trn));
            fileData.Append(eng.WriteString(rqFile[0].ref1));
            fileData.Append(eng.WriteString(rqFile[0].ref2));
            fileData.Append(eng.WriteString(rqFile[0].ref3));
            fileData.Append(eng.WriteString(rqFile[0].dtm));
            fileData.Append(eng.WriteString(rqFile[0].n1pr));
            fileData.Append(eng.WriteString(rqFile[0].n1pe));
            fileData.Append(eng.WriteString(rqFile[0].n1de));

            //lopping segments
            for (int i = 0; i < rqFile[0].repeatingElements.Length; i++)
            {
                fileData.Append(eng.WriteString(rqFile[0].repeatingElements[i].ent));
                fileData.Append(eng.WriteString(rqFile[0].repeatingElements[i].rmrImpo));
                fileData.Append(eng.WriteString(rqFile[0].repeatingElements[i].rmrRrq));
                fileData.Append(eng.WriteString(rqFile[0].repeatingElements[i].rmrRamq));
                fileData.Append(eng.WriteString(rqFile[0].repeatingElements[i].rmrRqap));
                fileData.Append(eng.WriteString(rqFile[0].repeatingElements[i].rmrCsst));
            }

            fileData.Append(eng.WriteString(rqFile[0].se));
            fileData.Append(eng.WriteString(rqFile[0].geTrailer));
            fileData.Append(eng.WriteString(rqFile[0].ieaTrailer));

            return fileData.ToString();
        }

        #endregion

        #region cra remittances
        public byte[] CreateRbcEftSourceDeductionFile(DatabaseUser user, CraRemittanceSummaryCollection collection, RbcEftSourceDeductionParameters sourceParms, String craRemittancePeriodCode, long headerSequenceNumber, long transactionSequenceNumber)
        {
            byte[] fileContents = null;

            if (collection != null && collection.Count > 0) //only create eft if we have rows to process
            {
                int numOfLoopingSegments = collection.Count; //compute this for number of repeating segments within the ls/le segment

                //isa
                IsaInterchangeControlHeaderSourceDed[] isaHeader = new IsaInterchangeControlHeaderSourceDed[1]
                    {
                        new IsaInterchangeControlHeaderSourceDed()
                        {
                            SenderInterchangeID = sourceParms.IsaSenderInterchangeIDSourceDed.PadRight(15, ' '),                 //must be 15 chars
                            ReceiverInterchangeID = sourceParms.IsaReceiverInterchangeIDSourceDed.PadRight(15, ' '),             //must be 15 chars
                            InterchangeControlNumber = headerSequenceNumber.ToString().PadLeft(9, '0'),           //must be 9 chars
                            TestIndicator = sourceParms.IsaTestIndicatorSourceDed
                        }
                    };

                //gs
                GsFunctionalGroupHeaderSourceDed[] gsHeader = new GsFunctionalGroupHeaderSourceDed[1]
                    {
                        new GsFunctionalGroupHeaderSourceDed()
                        {
                            SenderInterchangeID = sourceParms.GsSenderInterchangeIdSourceDed,
                            ApplicationReceiversCode = sourceParms.GsApplicationReceiversCodeSourceDed,
                            GroupControlNumber = (headerSequenceNumber.ToString().Length) > 4 ? headerSequenceNumber.ToString().Substring(headerSequenceNumber.ToString().Length-4, 4) : headerSequenceNumber.ToString()         //max 4 chars, no leading 0s required
                        }
                    };

                //create details segments, constructor takes an int for number of repeating LS/LE segments
                SourceDeductionDetails[] sourceDetails = new SourceDeductionDetails[1] { new SourceDeductionDetails(numOfLoopingSegments) };
                sourceDetails[0] = PopulateSourceDeductionDetails(sourceDetails[0], collection, transactionSequenceNumber, sourceParms, craRemittancePeriodCode);

                //ge
                GsFunctionalGroupTrailerSourceDed[] geTrailer = new GsFunctionalGroupTrailerSourceDed[1] { new GsFunctionalGroupTrailerSourceDed() { GroupControlNumber = gsHeader[0].GroupControlNumber } };

                //iea
                IsaInterchangeControlTrailerSourceDed[] ieaTrailer = new IsaInterchangeControlTrailerSourceDed[1] { new IsaInterchangeControlTrailerSourceDed() { InterchangeControlNumber = isaHeader[0].InterchangeControlNumber } };

                //create the EFT file
                fileContents = convertStringToByteArray(WriteRbcEftSourceDeductionsFile(isaHeader, gsHeader, geTrailer, ieaTrailer, sourceDetails));
            }

            return fileContents;
        }

        private SourceDeductionDetails PopulateSourceDeductionDetails(SourceDeductionDetails sourceDeductions, CraRemittanceSummaryCollection coll, long transactionSequenceNumber, RbcEftSourceDeductionParameters sourceParms, String craRemittancePeriodCode)
        {
            /*  
             *  If this is a test file, the BPS.PaymentAmount must be < $5 and so does the RMT.RemittanceAmount, therefore i'm assigning $1 to each biz number 
             *  in the ls/le loop and the total is in the BPS ($1 x number of biz numbers)
            */

            //st
            //nothing to populate outside of constructor

            //bps
            if (sourceParms.IsaTestIndicatorSourceDed == "T") //if this is a test file, we must send a PaymentAmount less than $5
                sourceDeductions.bps[0].PaymentAmount = (sourceDeductions.repeatingElements.Length * 1.00M).ToString();
            else
                sourceDeductions.bps[0].PaymentAmount = Convert.ToString(Math.Round(coll.SumPaymentAmount, 2, MidpointRounding.AwayFromZero));  //property in coll to total biz # amounts

            sourceDeductions.bps[0].OriginatorDepositoryFinancialInstitutionIdentificationNumber = sourceParms.DirectDepositBankCodeTransitNumber;
            sourceDeductions.bps[0].OriginatorAccountNumber = sourceParms.DirectDepositAccountNumber;
            sourceDeductions.bps[0].ReceiverDepositoryFinancialInstitutionIdentificationNumber = sourceParms.CraBankCodeTransitNumberSourceDed;
            sourceDeductions.bps[0].ReceiverAccountNumber = sourceParms.CraAccountNumberSourceDed;
            sourceDeductions.bps[0].EffectiveEntryDate = Convert.ToDateTime(coll[0].EffectiveEntryDate);

            //ref1
            sourceDeductions.ref1[0].ReferenceNumber = transactionSequenceNumber.ToString();

            //ref2
            sourceDeductions.ref2[0].ReferenceNumber = sourceParms.RefItReferenceNumberSourceDed;

            //n1pr
            sourceDeductions.n1pr[0].Name = sourceParms.N1PrNameSourceDed;

            //dtm, n1pe, per
            //nothing to populate outside of constructor

            //ls
            sourceDeductions.ls[0].LoopIdentifierCode = "1";

            //loop thru repeating elements
            for (int i = 0; i < sourceDeductions.repeatingElements.Length; i++)
            {
                //n1pr2
                sourceDeductions.repeatingElements[i].n1pr2[0].Name = (sourceParms.CompanyShortName.Length > 35) ? sourceParms.CompanyShortName.Substring(0, 35) : sourceParms.CompanyShortName;

                //rmt
                sourceDeductions.repeatingElements[i].rmt[0].BusinessNumber = coll[i].BusinessTaxNumber;

                if (sourceParms.IsaTestIndicatorSourceDed == "T") //if this is a test file, we must send a PaymentAmount less than $5
                    sourceDeductions.repeatingElements[i].rmt[0].RemittanceAmount = "1.00";
                else
                    sourceDeductions.repeatingElements[i].rmt[0].RemittanceAmount = Convert.ToString(Math.Round(coll[i].PaymentAmount, 2, MidpointRounding.AwayFromZero));

                sourceDeductions.repeatingElements[i].rmt[0].NumberOfEmployeesConcatGrossPayrollAmount = coll[i].EmployeeCount.ToString().PadLeft(6, '0').Substring(0, 6) + Math.Round(coll[i].TotalTaxableIncome, 0, MidpointRounding.AwayFromZero).ToString().PadLeft(9, '0');

                //ref3
                //nothing to populate outside of constructor

                //dtm
                sourceDeductions.repeatingElements[i].dtm2[0].Date = Convert.ToDateTime(coll[0].RemittancePeriodEndDate);
            }

            //le
            sourceDeductions.le[0].LoopIdentifierCode = sourceDeductions.ls[0].LoopIdentifierCode;

            //se
            if (sourceDeductions.repeatingElements.Length > 1)//if repeating segments in ls/le exist, add each extra set (4 nodes per set) to the constructor default of 15 (constructor default includes 1 set already as at least one business number has to be in the file)
                sourceDeductions.se[0].NumberOfIncludedSegments = (Convert.ToInt16(sourceDeductions.se[0].NumberOfIncludedSegments) + (4 * (sourceDeductions.repeatingElements.Length - 1))).ToString();

            return sourceDeductions;
        }

        private String WriteRbcEftSourceDeductionsFile(IsaInterchangeControlHeaderSourceDed[] isaHeader, GsFunctionalGroupHeaderSourceDed[] gsHeader, GsFunctionalGroupTrailerSourceDed[] geTrailer, IsaInterchangeControlTrailerSourceDed[] ieaTrailer, SourceDeductionDetails[] sourceDetails)
        {
            FileHelpers.MultiRecordEngine eng = new FileHelpers.MultiRecordEngine(typeof(IsaInterchangeControlHeaderSourceDed), typeof(GsFunctionalGroupHeaderSourceDed), typeof(GsFunctionalGroupTrailerSourceDed), typeof(IsaInterchangeControlTrailerSourceDed), typeof(SourceDeductionsStTransactionSetHeader),
                    typeof(SourceDeductionsBeginningSegment), typeof(SourceDeductionsReferenceIdentification), typeof(SourceDeductionsReferenceIdentification2), typeof(SourceDeductionsDateTimeReference), typeof(SourceDeductionsPayeeReceiverName), typeof(SourceDeductionsPayorReceiverName),
                    typeof(SourceDeductionsAdministrativeCommunicationsContact), typeof(SourceDeductionsLoopHeader), typeof(SourceDeductionsPayorReceiverName2), typeof(SourceDeductionsRemittanceAdvice), typeof(SourceDeductionsReferenceIdentification3), typeof(SourceDeductionsDateTimeReference2),
                    typeof(SourceDeductionsLoopTrailer), typeof(SourceDeductionsSeTransactionSetTrailer));

            System.Text.StringBuilder fileData = new System.Text.StringBuilder();

            fileData.Append(eng.WriteString(isaHeader));
            fileData.Append(eng.WriteString(gsHeader));
            fileData.Append(eng.WriteString(sourceDetails[0].st));
            fileData.Append(eng.WriteString(sourceDetails[0].bps));
            fileData.Append(eng.WriteString(sourceDetails[0].ref1));
            fileData.Append(eng.WriteString(sourceDetails[0].ref2));
            fileData.Append(eng.WriteString(sourceDetails[0].dtm));
            fileData.Append(eng.WriteString(sourceDetails[0].n1pe));
            fileData.Append(eng.WriteString(sourceDetails[0].n1pr));
            fileData.Append(eng.WriteString(sourceDetails[0].per));
            fileData.Append(eng.WriteString(sourceDetails[0].ls));

            //lopping ls/le segments
            for (int i = 0; i < sourceDetails[0].repeatingElements.Length; i++)
            {
                fileData.Append(eng.WriteString(sourceDetails[0].repeatingElements[i].n1pr2));
                fileData.Append(eng.WriteString(sourceDetails[0].repeatingElements[i].rmt));
                fileData.Append(eng.WriteString(sourceDetails[0].repeatingElements[i].ref3));
                fileData.Append(eng.WriteString(sourceDetails[0].repeatingElements[i].dtm2));
            }

            fileData.Append(eng.WriteString(sourceDetails[0].le));
            fileData.Append(eng.WriteString(sourceDetails[0].se));
            fileData.Append(eng.WriteString(geTrailer));
            fileData.Append(eng.WriteString(ieaTrailer));

            return fileData.ToString();
        }
        #endregion

        #region pre authorized debit

        public ScotiaPreAuthorizedDebitTotalsCollection GetScotiaPreAuthorizedDebitData(DatabaseUser user, long payrollProcessId)
        {
            return _sqlServerScotiaPreAuthorizedDebitTotals.Select(user, payrollProcessId);
        }
        public byte[] CreateScotiaPreAuthorizedDebitFile(DatabaseUser user, ScotiaPreAuthorizedDebitTotalsCollection collection, ScotiaBankEdiParameters ediParms, long headerSequenceNumber)
        {
            byte[] fileContents = null;

            if (collection != null && collection.Count > 0) //only create eft if we have rows to process
            {
                //group the accounts in the collection
                ScotiaPreAuthorizedDebitTotalsCollection groupedAccounts = collection.GroupedAccounts;

                //instantiate the PreAuthorizedDebitDetails class
                ScotiaBankPreAuthorizedDebit preAuthDetails = new ScotiaBankPreAuthorizedDebit(groupedAccounts.Count);

                //ISA
                preAuthDetails.isa[0].ISA06 = ediParms.ScotiaBankIsa06.PadRight(15, ' ');             //must be 15 chars
                preAuthDetails.isa[0].ISA08 = ediParms.ScotiaBankIsa08.PadRight(15, ' ');             //must be 15 chars
                preAuthDetails.isa[0].ISA13 = headerSequenceNumber.ToString().PadLeft(9, '0');        //must be 9 chars
                preAuthDetails.isa[0].ISA15 = ediParms.ScotiaBankIsa15;

                //GS
                preAuthDetails.gs[0].GS02 = ediParms.ScotiaBankIsa06; //no padding
                preAuthDetails.gs[0].GS03 = ediParms.ScotiaBankIsa08; //no padding
                preAuthDetails.gs[0].GS06 = (headerSequenceNumber.ToString().Length) > 9 ? headerSequenceNumber.ToString().Substring(headerSequenceNumber.ToString().Length - 9, 9) : headerSequenceNumber.ToString();         //max 9 chars, no leading 0s required

                int i = 0; //array counter
                foreach (ScotiaPreAuthorizedDebitTotals account in groupedAccounts)
                {
                    //st
                    preAuthDetails.padDetails[i].st[0].ST02 = (i + 1).ToString().PadLeft(9, '0'); //just counter variable for ST/SE pairs

                    //bpr
                    //CreditDebitFlagCode - override handled in construtor
                    preAuthDetails.padDetails[i].bpr[0].BPR02 = Convert.ToString(Math.Round(account.Amount, 2, MidpointRounding.AwayFromZero));
                    preAuthDetails.padDetails[i].bpr[0].BPR07 = ediParms.ScotiaBankBpr07;
                    preAuthDetails.padDetails[i].bpr[0].BPR09 = ediParms.ScotiaBankBpr09;
                    preAuthDetails.padDetails[i].bpr[0].BPR13 = account.AbaNumber;
                    preAuthDetails.padDetails[i].bpr[0].BPR15 = account.PreAuthorizedAccountNumber;
                    preAuthDetails.padDetails[i].bpr[0].BPR16 = account.PaymentDueDate;

                    //nte
                    //nothing to populate outside of constructor

                    //trn
                    preAuthDetails.padDetails[i].trn[0].TRN02 = GetSequenceNumber(user, "GLOBAL_RBC_TRANSACTION_SEQUENCE").ToString();

                    //cur
                    //nothing to populate outside of constructor

                    //ref
                    //nothing to populate outside of constructor

                    //dtm
                    //nothing to populate outside of constructor

                    //n1 payor
                    //OriginatorPayorName - override handled in constructor

                    //n1 payee
                    //stacy test --change this to suit Jamaica when we get more information.
                    //if more than one bank account in the file, show 19 chars of company short name, a space, and the 15 chars of the busines tax number (9 digits+RP+4 digits)
                    string shortCompanyName = ediParms.CompanyShortName.Length > 19 ? ediParms.CompanyShortName.Substring(0, 19) : ediParms.CompanyShortName;
                    string businessTaxNumber = account.BusinessTaxNumber.Length > 15 ? account.BusinessTaxNumber.Substring(0, 15) : account.BusinessTaxNumber;

                    if (groupedAccounts.Count > 1)
                        preAuthDetails.padDetails[i].n1pe[0].N1PE02 = shortCompanyName + " " + businessTaxNumber; //max 35 chars
                    else
                        preAuthDetails.padDetails[i].n1pe[0].N1PE02 = shortCompanyName;   //just the shortname here

                    //N1Orig
                    //nothing to populate outside of constructor

                    //N4Orig
                    //nothing to populate outside of constructor

                    //N1RB
                    preAuthDetails.padDetails[i].n1rb[0].N1RB02 = preAuthDetails.padDetails[i].n1pe[0].N1PE02;

                    //N4RB
                    //nothing to populate outside of constructor

                    //se
                    preAuthDetails.padDetails[i].se[0].SE02 = preAuthDetails.padDetails[i].st[0].ST02;

                    i++;
                }

                //GE (one transaction set per record in the collection)
                preAuthDetails.ge[0].GE01 = groupedAccounts.Count.ToString();
                preAuthDetails.ge[0].GE02 = preAuthDetails.gs[0].GS06;

                //IEA
                preAuthDetails.iea[0].IEA02 = preAuthDetails.isa[0].ISA13;

                //create the EFT file
                fileContents = convertStringToByteArray(WriteScotiaPreAuthorizedDebitFile(preAuthDetails));
            }

            return fileContents;
        }
        private String WriteScotiaPreAuthorizedDebitFile(ScotiaBankPreAuthorizedDebit preAuthDetails)
        {
            //declare the types of records to be written by the engine
            FileHelpers.MultiRecordEngine eng = new FileHelpers.MultiRecordEngine(typeof(ISA), typeof(GS), typeof(ST), typeof(BPR), typeof(NTE), typeof(TRN), typeof(CUR), typeof(DTM), typeof(REF),
            typeof(N1PR), typeof(N1PE), typeof(N1Orig), typeof(N4Orig), typeof(N1RB), typeof(N4RB), typeof(SE), typeof(GE), typeof(IEA));

            System.Text.StringBuilder fileData = new System.Text.StringBuilder();
            fileData.Append(eng.WriteString(preAuthDetails.isa));
            fileData.Append(eng.WriteString(preAuthDetails.gs));

            //write out details, could be multiple accounts
            if (preAuthDetails.padDetails.Length > 0)
            {
                for (int i = 0; i < preAuthDetails.padDetails.Length; i++)
                {
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].st));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].bpr));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].nte));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].trn));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].cur));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].ref1));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].dtm));                    
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].n1pr));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].n1pe));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].n1Orig));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].n4Orig));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].n1rb));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].n4rb));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].se));
                }
            }

            fileData.Append(eng.WriteString(preAuthDetails.ge));
            fileData.Append(eng.WriteString(preAuthDetails.iea));

            return fileData.ToString();
        }

        public PreAuthorizedDebitTotalsCollection GetPreAuthorizedDebitData(DatabaseUser user, long payrollProcessId)
        {
            return _sqlServerPreAuthorizedDebitTotals.Select(user, payrollProcessId);
        }
        public void InsertPreAuthorizedDebitExportToDatabase(DatabaseUser user, PreAuthorizedDebitExport preAuthorizedDebitExport)
        {
            _sqlServerPreAuthorizedDebitExport.Insert(user, preAuthorizedDebitExport);
        }
        public void UpdatePreAuthorizedDebitExportFileTransmissionDate(DatabaseUser user, long preAuthorizedDebitExportId)
        {
            _sqlServerPreAuthorizedDebitExport.UpdatePreAuthorizedDebitExportFileTransmissionDate(user, preAuthorizedDebitExportId);
        }

        public void InsertWcbEstimatePreAuthorizedDebitExportToDatabase(DatabaseUser user, ChequeWcbPreAuthorizedDebitExport padExport)
        {
            _sqlServerWcbEstimatePreAuthorizedDebitExport.Insert(user, padExport);
        }
        public void UpdateChequeWcbEstimatePreAuthorizedDebitExportFileTransmissionDate(DatabaseUser user, long chequeWcbPreAuthorizedDebitExportId)
        {
            _sqlServerWcbEstimatePreAuthorizedDebitExport.UpdateChequeWcbEstimatePreAuthorizedDebitExportFileTransmissionDate(user, chequeWcbPreAuthorizedDebitExportId);
        }

        public byte[] CreatePreAuthorizedDebitFile(DatabaseUser user, PreAuthorizedDebitTotalsCollection collection, RbcEftEdiParameters ediParms, long headerSequenceNumber)
        {
            byte[] fileContents = null;

            if (collection != null && collection.Count > 0) //only create eft if we have rows to process
            {
                //group the accounts in the collection
                PreAuthorizedDebitTotalsCollection groupedAccounts = collection.GroupedAccounts;

                //instantiate the PreAuthorizedDebitDetails class
                PreAuthorizedDebitDetails preAuthDetails = new PreAuthorizedDebitDetails(groupedAccounts.Count);

                //ISA
                preAuthDetails.isaHeader[0].SenderInterchangeID = ediParms.IsaSenderInterchangeID.PadRight(15, ' ');            //must be 15 chars
                preAuthDetails.isaHeader[0].ReceiverInterchangeID = ediParms.IsaReceiverInterchangeID.PadRight(15, ' ');        //must be 15 chars
                preAuthDetails.isaHeader[0].InterchangeControlNumber = headerSequenceNumber.ToString().PadLeft(9, '0');         //must be 9 chars
                preAuthDetails.isaHeader[0].TestIndicator = ediParms.IsaTestIndicator;

                //GS
                preAuthDetails.gsHeader[0].SenderInterchangeID = ediParms.IsaSenderInterchangeID;
                preAuthDetails.gsHeader[0].ApplicationReceiversCode = ediParms.IsaReceiverInterchangeID;
                preAuthDetails.gsHeader[0].GroupControlNumber = (headerSequenceNumber.ToString().Length) > 4 ? headerSequenceNumber.ToString().Substring(headerSequenceNumber.ToString().Length - 4, 4) : headerSequenceNumber.ToString();         //max 4 chars, no leading 0s required

                int i = 0; //array counter
                foreach (PreAuthorizedDebitTotals account in groupedAccounts)
                {
                    //st
                    preAuthDetails.padDetails[i].st[0].TransactionSetControlNumber = (i + 1).ToString().PadLeft(9, '0'); //just counter variable for ST/SE pairs

                    //bpr
                    //CreditDebitFlagCode - override handled in construtor
                    preAuthDetails.padDetails[i].bpr[0].MonetaryAmount = Convert.ToString(Math.Round(account.Amount, 2, MidpointRounding.AwayFromZero));
                    preAuthDetails.padDetails[i].bpr[0].OriginatorDepositoryFinancialInstitutionIdentificationNumber = ediParms.DirectDepositBankCodeTransitNumber;
                    preAuthDetails.padDetails[i].bpr[0].OriginatorAccountNumber = ediParms.DirectDepositAccountNumber;
                    preAuthDetails.padDetails[i].bpr[0].ReceiverDepositoryFinancialInstitutionIdentificationNumber = account.CodePreAuthorizedBankCd.PadLeft(4, '0') + account.PreAuthorizedTransitNumber;
                    preAuthDetails.padDetails[i].bpr[0].ReceiverAccountNumber = account.PreAuthorizedAccountNumber;
                    preAuthDetails.padDetails[i].bpr[0].PaymentDate = account.PaymentDueDate;

                    //trn
                    preAuthDetails.padDetails[i].trn[0].ReferenceIdentification = GetSequenceNumber(user, "GLOBAL_RBC_TRANSACTION_SEQUENCE").ToString();

                    //cur
                    //nothing to populate outside of constructor

                    //ref
                    preAuthDetails.padDetails[i].ref1[0].ReferenceId = ediParms.PreAuthorizedDebitClientNumber;

                    //dtm
                    //nothing to populate outside of constructor

                    //n1 payor
                    //OriginatorPayorName - override handled in constructor

                    //n1 payee
                    //if more than one bank account in the file, show 19 chars of company short name, a space, and the 15 chars of the busines tax number (9 digits+RP+4 digits)
                    string shortCompanyName = ediParms.CompanyShortName.Length > 19 ? ediParms.CompanyShortName.Substring(0, 19) : ediParms.CompanyShortName;
                    string businessTaxNumber = account.BusinessTaxNumber.Length > 15 ? account.BusinessTaxNumber.Substring(0, 15) : account.BusinessTaxNumber;

                    if (groupedAccounts.Count > 1)
                        preAuthDetails.padDetails[i].n1pe[0].PayeeName = shortCompanyName + " " + businessTaxNumber; //max 35 chars
                    else
                        preAuthDetails.padDetails[i].n1pe[0].PayeeName = shortCompanyName;   //just the shortname here

                    //se
                    preAuthDetails.padDetails[i].se[0].TransactionSetControlNumber = preAuthDetails.padDetails[i].st[0].TransactionSetControlNumber;

                    i++;
                }

                //GE (one transaction set per record in the collection)
                preAuthDetails.geTrailer[0].NumberOfTransactionSetsIncluded = groupedAccounts.Count.ToString();
                preAuthDetails.geTrailer[0].GroupControlNumber = preAuthDetails.gsHeader[0].GroupControlNumber;

                //IEA
                preAuthDetails.ieaTrailer[0].InterchangeControlNumber = preAuthDetails.isaHeader[0].InterchangeControlNumber;

                //create the EFT file
                fileContents = convertStringToByteArray(WritePreAuthorizedDebitFile(preAuthDetails));
            }

            return fileContents;
        }
        private String WritePreAuthorizedDebitFile(PreAuthorizedDebitDetails preAuthDetails)
        {
            //declare the types of records to be written by the engine
            FileHelpers.MultiRecordEngine eng = new FileHelpers.MultiRecordEngine(typeof(IsaInterchangeControlHeader), typeof(GsFunctionalGroupHeader), typeof(DirectDepositStTransactionSetHeader), typeof(DirectDepositBeginningSegmentForPaymentOrderRemittanceAdvice),
            typeof(DirectDepositTrace), typeof(DirectDepositCurrency), typeof(DirectDepositReferenceIdentification), typeof(DirectDepositDateTimeReference), typeof(DirectDepositOriginatorNamePayor), typeof(DirectDepositPayeeReceiverName),
            typeof(DirectDepositSeTransactionSetTrailer), typeof(GsFunctionalGroupTrailer), typeof(IsaInterchangeControlTrailer));

            System.Text.StringBuilder fileData = new System.Text.StringBuilder();
            fileData.Append(eng.WriteString(preAuthDetails.isaHeader));
            fileData.Append(eng.WriteString(preAuthDetails.gsHeader));

            //write out details, could be multiple accounts
            if (preAuthDetails.padDetails.Length > 0)
            {
                for (int i = 0; i < preAuthDetails.padDetails.Length; i++)
                {
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].st));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].bpr));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].trn));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].cur));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].ref1));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].dtm));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].n1pr));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].n1pe));
                    fileData.Append(eng.WriteString(preAuthDetails.padDetails[i].se));
                }
            }

            fileData.Append(eng.WriteString(preAuthDetails.geTrailer));
            fileData.Append(eng.WriteString(preAuthDetails.ieaTrailer));

            return fileData.ToString();
        }

        #endregion

        public byte[] CreateEftFile(DatabaseUser user, PayrollMasterPaymentCollection collection, String status, String eftClientNumber, String companyName, String companyShortName, bool eftTransmissionFlag, String eftFileIndicator)
        {
            byte[] fileContents = null;
            EFTTransmissionHeader[] eftTransmissionHeader = null;

            if (collection != null && collection.Count > 0) //only create eft if we have rows to process
            {
                if (eftTransmissionFlag)
                {
                    //create transmission header
                    eftTransmissionHeader = new EFTTransmissionHeader[1];
                    eftTransmissionHeader[0] = new EFTTransmissionHeader();
                    eftTransmissionHeader[0].FileType = eftFileIndicator;
                }

                //declare the EFT classes (sections) for the EFT file, create arrays, then instantiate the objects
                EftHeader[] eftHeader = new EftHeader[1];
                EftCompanyBatchHeaderRecord[] eftCompanyBatchHeader = new EftCompanyBatchHeaderRecord[1];
                EftEntryDetailRecord[] eftEntryDetails = new EftEntryDetailRecord[collection.Count]; //create array for multiple entry records
                CompanyBatchControlRecord[] companyBatch = new CompanyBatchControlRecord[1];
                FileControlRecord[] fileControl = new FileControlRecord[1];

                //instantiate the EFT classes (sections) for the EFT file
                eftHeader[0] = new EftHeader() { ClientNumber = eftClientNumber, CompanyName = companyName, FileCreationNumber = Convert.ToString(collection[0].PayrollProcessId).PadLeft(4, '0').Substring(Convert.ToString(collection[0].PayrollProcessId).PadLeft(4, '0').Length - 4, 4) };
                eftCompanyBatchHeader[0] = new EftCompanyBatchHeaderRecord() { DueDate = collection[0].ChequeDate, ClientNumber = eftClientNumber, ClientShortName = companyShortName };

                //loop for multiple entry records
                for (int i = 0; i < collection.Count; i++)
                    eftEntryDetails[i] = new EftEntryDetailRecord();

                companyBatch[0] = new CompanyBatchControlRecord() { ClientNumber = eftClientNumber };
                fileControl[0] = new FileControlRecord();

                //populate fields in the EFT classes
                eftEntryDetails = populateEntryDetail(eftEntryDetails, collection);

                //populate control records
                populateData(eftCompanyBatchHeader, companyBatch, fileControl, collection);

                FileHelpers.MultiRecordEngine engine = null;
                String eftFileString = null;

                if (Convert.ToBoolean(eftTransmissionFlag))
                {
                    engine = new FileHelpers.MultiRecordEngine(typeof(EFTTransmissionHeader), typeof(EftHeader), typeof(EftCompanyBatchHeaderRecord), typeof(EftEntryDetailRecord), typeof(CompanyBatchControlRecord), typeof(FileControlRecord));
                    eftFileString = writeEftFileWithTransmissionHeader(engine, eftTransmissionHeader, eftHeader, eftCompanyBatchHeader, eftEntryDetails, companyBatch, fileControl);
                }
                else
                {
                    engine = new FileHelpers.MultiRecordEngine(typeof(EftHeader), typeof(EftCompanyBatchHeaderRecord), typeof(EftEntryDetailRecord), typeof(CompanyBatchControlRecord), typeof(FileControlRecord));
                    eftFileString = writeEftFile(engine, eftHeader, eftCompanyBatchHeader, eftEntryDetails, companyBatch, fileControl);
                }

                //create the EFT file
                fileContents = convertStringToByteArray(eftFileString);

                //update db records from NEW to RP ("Newly Added" to "Record Processed")
                UpdateStatusOfEFTRecords(user, collection, status);
            }

            return fileContents;
        }
        private void UpdateStatusOfEFTRecords(DatabaseUser user, PayrollMasterPaymentCollection collection, String status)
        {
            foreach (PayrollMasterPayment rec in collection)
                _sqlServerPayrollMasterPayment.UpdateStatus(user, rec, status);
        }
        private EftEntryDetailRecord[] populateEntryDetail(EftEntryDetailRecord[] eftArray, PayrollMasterPaymentCollection collection)
        {
            for (int i = 0; i < collection.Count; i++)
            {
                eftArray[i].AccountNumber = collection[i].AccountNumber;
                eftArray[i].CustomerName = collection[i].EmployeeName;
                eftArray[i].CustomerNumber = collection[i].PayrollMasterPaymentId.ToString(); //unique identifier
                eftArray[i].Amount = Convert.ToString(Math.Round(collection[i].Amount, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(10, '0'); //round to two decimal places, convert value to string, replace the decimal character, and pad the left with zeroes
                eftArray[i].CANBankNumber = eftArray[i].CANBankNumber + collection[i].CodeEmployeeBankCd;
                eftArray[i].CANBranchTransitNumber = collection[i].TransitNumber;
            }

            return eftArray;
        }
        private void populateData(EftCompanyBatchHeaderRecord[] EftCompBatch, CompanyBatchControlRecord[] EftCompControl, FileControlRecord[] eftFileControl, PayrollMasterPaymentCollection eftCollection)
        {
            //comp batch
            EftCompBatch[0].BatchNumber = Convert.ToString(eftCollection[0].PayrollProcessId).PadLeft(7, '0');

            //comp control
            EftCompControl[0].BatchNumber = EftCompBatch[0].BatchNumber;
            EftCompControl[0].EntryAddendaCount = Convert.ToString(eftCollection.Count).PadLeft(6, '0');

            //file control
            eftFileControl[0].BatchHeaderBlockCount = "000001"; //6 chars
            eftFileControl[0].PhysicalBlockCount = Convert.ToString(Convert.ToInt16(EftCompControl[0].EntryAddendaCount) + 5).PadLeft(6, '0'); //5 basic records plus repeating Entry Addenda Records
            eftFileControl[0].EntryAddendaCount = Convert.ToString(eftCollection.Count).PadLeft(8, '0');

            //TotalCreditDollarAmount
            Decimal moneyTotal = 0;

            foreach (PayrollMasterPayment rec in eftCollection)
                moneyTotal += Convert.ToDecimal(rec.Amount);

            eftFileControl[0].TotalCreditDollarAmount = Convert.ToString(Math.Round(moneyTotal, 2, MidpointRounding.AwayFromZero)).Replace(".", "").PadLeft(12, '0');

            //same value as eftFileControl[0].TotalCreditDollarAmount
            EftCompControl[0].TotalCreditEntryDollarAmount = eftFileControl[0].TotalCreditDollarAmount;
        }
        private String writeEftFileWithTransmissionHeader(FileHelpers.MultiRecordEngine eng, EFTTransmissionHeader[] eftTransHeader, EftHeader[] eftHead, EftCompanyBatchHeaderRecord[] eftCompBatchHead, EftEntryDetailRecord[] eftEntryDet, CompanyBatchControlRecord[] eftCompBatchControl, FileControlRecord[] eftFileControl)
        {
            System.Text.StringBuilder fileData = new System.Text.StringBuilder();
            fileData.Append(eng.WriteString(eftTransHeader));
            fileData.Append(eng.WriteString(eftHead));
            fileData.Append(eng.WriteString(eftCompBatchHead));
            fileData.Append(eng.WriteString(eftEntryDet));
            fileData.Append(eng.WriteString(eftCompBatchControl));
            fileData.Append(eng.WriteString(eftFileControl));

            return fileData.ToString();
        }
        private String writeEftFile(FileHelpers.MultiRecordEngine eng, EftHeader[] eftHead, EftCompanyBatchHeaderRecord[] eftCompBatchHead, EftEntryDetailRecord[] eftEntryDet, CompanyBatchControlRecord[] eftCompBatchControl, FileControlRecord[] eftFileControl)
        {
            System.Text.StringBuilder fileData = new System.Text.StringBuilder();
            fileData.Append(eng.WriteString(eftHead));
            fileData.Append(eng.WriteString(eftCompBatchHead));
            fileData.Append(eng.WriteString(eftEntryDet));
            fileData.Append(eng.WriteString(eftCompBatchControl));
            fileData.Append(eng.WriteString(eftFileControl));

            return fileData.ToString();
        }

        #region CSB Export
        public byte[] CreateCSBExport(CanadaRevenueAgencyBondExportFileCollection collection)
        {
            byte[] fileContents = null;

            if (collection != null && collection.Count > 0) //only create export if we have rows to process
            {
                //declare the CSB classes (sections) for the CSB export file, create arrays, then instantiate the object
                CSBTransmissionHeaderRecord[] csbTransHeader = new CSBTransmissionHeaderRecord[1];
                CSBBatchHeaderRecord[] csbBatchHeader = new CSBBatchHeaderRecord[1];
                CSBBatchDetailRecord[] csbBatchDetails = new CSBBatchDetailRecord[collection.Count]; //create array for multiple entry records
                CSBBatchTrailerRecord[] csbBatchTrailer = new CSBBatchTrailerRecord[1];
                CSBTransmissionTrailerRecord[] csbTransmissionTrailer = new CSBTransmissionTrailerRecord[1];

                //instantiate the CSB classes (sections) for the CSB file
                //loop for multiple detail records
                for (int i = 0; i < collection.Count; i++)
                    csbBatchDetails[i] = new CSBBatchDetailRecord();

                //populate fields in the CSB detail classes
                csbBatchDetails = populateCSBDetails(csbBatchDetails, collection);

                //Populate control records
                populateHeadersAndTrailers(csbTransHeader, csbBatchHeader, csbBatchTrailer, csbTransmissionTrailer, collection);

                FileHelpers.MultiRecordEngine engine = new FileHelpers.MultiRecordEngine(typeof(CSBTransmissionHeaderRecord), typeof(CSBBatchHeaderRecord), typeof(CSBBatchDetailRecord), typeof(CSBBatchTrailerRecord), typeof(CSBTransmissionTrailerRecord));
                String csbFileString = writeCsbFile(engine, csbTransHeader, csbBatchHeader, csbBatchDetails, csbBatchTrailer, csbTransmissionTrailer);

                //create the CSB Export file
                fileContents = convertStringToByteArray(csbFileString);
            }

            return fileContents;
        }
        private CSBBatchDetailRecord[] populateCSBDetails(CSBBatchDetailRecord[] csbArray, CanadaRevenueAgencyBondExportFileCollection collection)
        {
            for (int i = 0; i < collection.Count; i++)
            {
                //deduction amount cannot be 0
                if (collection[i].PPaycodeCurrentDollars != 0)
                {
                    csbArray[i].OrganizationId = collection[i].OrganizationNumber.ToString();
                    csbArray[i].EffectiveDate = collection[i].ChequeDate;
                    csbArray[i].SequenceNumber = collection[i].SequenceNumber.ToString().PadLeft(5, '0'); //00001...99999, incremented
                    csbArray[i].EmployeeReferenceNumber = collection[i].SocialInsuranceNumber.Replace("-", "");
                    csbArray[i].EmployeeName = collection[i].EmployeeName.PadRight(50, ' ');

                    //check for negative amount, replace the neg sign in the amount, flip the "sign" property...
                    if (collection[i].PPaycodeCurrentDollars < 0)
                        csbArray[i].Sign = "-";

                    //8 digits.  Last 2 are implied decimal (ie. $10 is 00001000).  So round to get 2 decimal places, replace the decimal and negative sign (if present), and pad left with 0's if needed
                    csbArray[i].DeductionAmount = Convert.ToString(Math.Round(collection[i].PPaycodeCurrentDollars, 2, MidpointRounding.AwayFromZero)).Replace(".", "").Replace("-", "").PadLeft(8, '0');
                    csbArray[i].EmployeeBirthDate = collection[i].BirthDate;
                }
            }

            return csbArray;
        }
        public void populateHeadersAndTrailers(CSBTransmissionHeaderRecord[] transHeader, CSBBatchHeaderRecord[] batchHeader, CSBBatchTrailerRecord[] batchTrailer, CSBTransmissionTrailerRecord[] transTrailer, CanadaRevenueAgencyBondExportFileCollection coll)
        {
            transHeader[0] = new CSBTransmissionHeaderRecord()
            {
                TransmitterOrganizationId = coll[0].TransmitterOrganizationNumber.ToString(),
                TransmissionDate = (DateTime)coll[0].TransmissionDate,
                TransmissionId = coll[0].CanadaRevenueAgencyBondExportId.ToString().PadLeft(8, '0').Substring(coll[0].CanadaRevenueAgencyBondExportId.ToString().PadLeft(8, '0').Length - 8, 8),
                PaymentTypeIndicator = coll[0].CodeCanadaRevenueAgencyBondPaymentTypeCd,
                EmailFaxIndicator = coll[0].ContactEmail != null ? "01" : "02", //01 for Email, 02 for Fax
                EmailFaxAddress = coll[0].ContactEmail != null ? coll[0].ContactEmail.PadRight(40, ' ') : coll[0].ContactFax.ToString().PadRight(40, ' ')
            };

            batchHeader[0] = new CSBBatchHeaderRecord()
            {
                OrganizationId = coll[0].OrganizationNumber.ToString(),
                EffectiveDate = coll[0].ChequeDate,
                EmailFaxIndicator = coll[0].ContactEmail != null ? "01" : "02", //01 for Email, 02 for Fax
                EmailFaxAddress = coll[0].ContactEmail != null ? coll[0].ContactEmail.PadRight(40, ' ') : coll[0].ContactFax.ToString().PadRight(40, ' ')
            };

            batchTrailer[0] = new CSBBatchTrailerRecord()
            {
                OrganizationId = coll[0].OrganizationNumber.ToString(),
                EffectiveDate = coll[0].ChequeDate,
                BatchTotalNumberOfRecords = coll.Count.ToString().PadLeft(6, '0')
            };

            //calculate BatchTotalDeductionAmount
            Decimal moneyTotal = 0;
            int numberOfNonZeroAmounts = 0;

            foreach (CanadaRevenueAgencyBondExportFile rec in coll)
            {
                if (rec.PPaycodeCurrentDollars != 0)
                {
                    numberOfNonZeroAmounts++;
                    moneyTotal += rec.PPaycodeCurrentDollars;
                }
            }

            if (moneyTotal < 0)
                batchTrailer[0].Sign = "-";

            //15 digits.  Last 2 are implied decimal (ie. $10 is 00001000).  So round to get 2 decimal places, replace the decimal and negative sign (if present), and pad left with 0's if needed
            batchTrailer[0].BatchTotalDeductionAmount = Convert.ToString(Math.Round(moneyTotal, 2, MidpointRounding.AwayFromZero)).Replace(".", "").Replace("-", "").PadLeft(15, '0');

            transTrailer[0] = new CSBTransmissionTrailerRecord()
            {
                TransmissionOrganizationId = coll[0].TransmitterOrganizationNumber.ToString(),
                TransmisionDate = coll[0].TransmissionDate,
                TransmissionId = coll[0].CanadaRevenueAgencyBondExportId.ToString().PadLeft(8, '0').Substring(coll[0].CanadaRevenueAgencyBondExportId.ToString().PadLeft(8, '0').Length - 8, 8),
                TotalRecordsTransmitted = (4 + numberOfNonZeroAmounts).ToString().PadLeft(9, '0')
            };
        }
        private String writeCsbFile(FileHelpers.MultiRecordEngine eng, CSBTransmissionHeaderRecord[] csbTransHeader, CSBBatchHeaderRecord[] csbBatchHeader, CSBBatchDetailRecord[] csbBatchDetails, CSBBatchTrailerRecord[] csbBatchTrailer, CSBTransmissionTrailerRecord[] csbTransmissionTrailer)
        {
            System.Text.StringBuilder fileData = new System.Text.StringBuilder();
            fileData.Append(eng.WriteString(csbBatchHeader));
            fileData.Append(eng.WriteString(csbBatchDetails));
            fileData.Append(eng.WriteString(csbBatchTrailer));
            fileData.Append(eng.WriteString(csbTransmissionTrailer));

            return fileData.ToString();
        }
        #endregion

        #region adjustment rule export
        public AdjustmentRuleExportCollection GetREALAdjustmentRuleExportData(DatabaseUser user)
        {
            return _sqlServerREALAdjustmentRule.Select(user);
        }
        public byte[] CreateREALAdjustmentRuleExportFile(AdjustmentRuleExportCollection exportCollection)
        {
            byte[] fileContents = null;

            if (exportCollection != null && exportCollection.Count > 0)
            {
                //declare the classes for the csv file, create arrays, then instantiate the object
                AdjustmentRuleGlExportHeader[] header = new AdjustmentRuleGlExportHeader[1] { new AdjustmentRuleGlExportHeader() };
                AdjustmentRuleGlExportData[] details = CreateREALAdjustmentRuleExportDetail(exportCollection);

                //write file to string
                FileHelpers.MultiRecordEngine engine = new FileHelpers.MultiRecordEngine(typeof(AdjustmentRuleGlExportHeader), typeof(AdjustmentRuleGlExportData));
                String realExportFileString = WriteRealAdjustmentRuleCsvFile(engine, header, details);

                //create the file
                fileContents = convertStringToByteArray(realExportFileString);
            }

            return fileContents;
        }

        private AdjustmentRuleGlExportData[] CreateREALAdjustmentRuleExportDetail(AdjustmentRuleExportCollection export)
        {
            AdjustmentRuleGlExportData[] adjustmentRules = new AdjustmentRuleGlExportData[export.Count];

            for (int i = 0; i < export.Count; i++)
            {
                adjustmentRules[i] = new AdjustmentRuleGlExportData();

                //here we map and restrict the data to the pre-defined lengths in the spec document
                //max 50
                adjustmentRules[i].PersonNumber = (export[i].PersonNumber != null && export[i].PersonNumber.Length > 50) ? export[i].PersonNumber.Substring(0, 50) : export[i].PersonNumber;
                //max 100
                adjustmentRules[i].PersonName = (export[i].PersonName != null && export[i].PersonName.Length > 50) ? export[i].PersonName.Substring(0, 50) : export[i].PersonName;
                //date
                adjustmentRules[i].EffectiveDate = export[i].EffectiveDate;
                //max 50
                adjustmentRules[i].Job = (export[i].Job != null && export[i].Job.Length > 10) ? export[i].Job.Substring(0, 10) : export[i].Job;
                //rate
                adjustmentRules[i].FlatRate = export[i].Rate;
            }

            return adjustmentRules;
        }

        private String WriteRealAdjustmentRuleCsvFile(FileHelpers.MultiRecordEngine eng, AdjustmentRuleGlExportHeader[] header, AdjustmentRuleGlExportData[] details)
        {
            System.Text.StringBuilder fileData = new System.Text.StringBuilder();

            fileData.Append(eng.WriteString(header));
            fileData.Append(eng.WriteString(details));

            return fileData.ToString();
        }
        #endregion

        #region REAL labor level export
        public LaborLevelExportCollection GetREALLaborLevelExportData(DatabaseUser user)
        {
            return _sqlServerREALLaborLevel.Select(user);
        }

        public byte[] CreateREALLaborLevelExportFile(LaborLevelExportCollection exportCollection)
        {
            byte[] fileContents = null;

            if (exportCollection != null && exportCollection.Count > 0)
            {
                //declare the classes for the csv file, create arrays, then instantiate the object
                LaborLevelHeader[] realHeader = new LaborLevelHeader[1] { new LaborLevelHeader() };
                LaborLevelDetail[] realDetails = CreateREALLaborLevelExportDetail(exportCollection);

                //write file to string
                FileHelpers.MultiRecordEngine engine = new FileHelpers.MultiRecordEngine(typeof(LaborLevelHeader), typeof(LaborLevelDetail));
                String realExportFileString = WriteRealLaborLevelCsvFile(engine, realHeader, realDetails);

                //create the file
                fileContents = convertStringToByteArray(realExportFileString);
            }

            return fileContents;
        }
        private String WriteRealLaborLevelCsvFile(FileHelpers.MultiRecordEngine eng, LaborLevelHeader[] header, LaborLevelDetail[] details)
        {
            System.Text.StringBuilder fileData = new System.Text.StringBuilder();

            fileData.Append(eng.WriteString(header));
            fileData.Append(eng.WriteString(details));

            return fileData.ToString();
        }
        private LaborLevelDetail[] CreateREALLaborLevelExportDetail(LaborLevelExportCollection export)
        {
            LaborLevelDetail[] realDetails = new LaborLevelDetail[export.Count];

            for (int i = 0; i < export.Count; i++)
            {
                realDetails[i] = new LaborLevelDetail();

                //here we map and restrict the data to the pre-defined lengths in the spec document
                //max 50
                realDetails[i].Level = (export[i].Level != null && export[i].Level.Length > 50) ? export[i].Level.Substring(0, 50) : export[i].Level;
                //max 50
                realDetails[i].EntryName = (export[i].EntryName != null && export[i].EntryName.Length > 50) ? export[i].EntryName.Substring(0, 50) : export[i].EntryName;
                //max 250
                realDetails[i].Description = (export[i].Description != null && export[i].Description.Length > 250) ? export[i].Description.Substring(0, 250) : export[i].Description;
                //max 10
                realDetails[i].Status = (export[i].Status != null && export[i].Status.Length > 10) ? export[i].Status.Substring(0, 10) : export[i].Status;
            }

            return realDetails;
        }
        #endregion

        #region REAL export

        public REALExportCollection GetREALExportData(DatabaseUser user)
        {
            return _sqlServerREALExport.Select(user);
        }

        //this is the REAL employee export
        public byte[] CreateREALEmployeeExportFile(REALExportCollection export)
        {
            byte[] fileContents = null;

            if (export != null && export.Count > 0)
            {
                //declare the classes for the csv file, create arrays, then instantiate the object
                REALExportHeader[] realExportHeader = new REALExportHeader[1] { new REALExportHeader() };
                REALExportDetail[] realExportDetail = CreateREALEmployeeExportFileDetail(export);

                //write file to string
                FileHelpers.MultiRecordEngine engine = new FileHelpers.MultiRecordEngine(typeof(REALExportHeader), typeof(REALExportDetail));
                String realExportFileString = WriteRealCsvFile(engine, realExportHeader, realExportDetail);

                //create the file
                fileContents = convertStringToByteArray(realExportFileString);
            }

            return fileContents;
        }

        private String WriteRealCsvFile(FileHelpers.MultiRecordEngine eng, REALExportHeader[] header, REALExportDetail[] details)
        {
            System.Text.StringBuilder fileData = new System.Text.StringBuilder();

            fileData.Append(eng.WriteString(header));
            fileData.Append(eng.WriteString(details));

            return fileData.ToString();
        }

        private REALExportDetail[] CreateREALEmployeeExportFileDetail(REALExportCollection export)
        {
            REALExportDetail[] realDetails = new REALExportDetail[export.Count];

            for (int i = 0; i < export.Count; i++)
            {
                realDetails[i] = new REALExportDetail();

                //here we map and restrict the data to the pre-defined lengths in the spec document

                //max 15
                realDetails[i].EmployeeNumber = (export[i].EmployeeNumber != null && export[i].EmployeeNumber.Length > 15) ? export[i].EmployeeNumber.Substring(0, 15) : export[i].EmployeeNumber;
                //max 30
                realDetails[i].FirstName = (export[i].FirstName != null && export[i].FirstName.Length > 30) ? export[i].FirstName.Substring(0, 30) : export[i].FirstName;
                //max 30
                realDetails[i].LastName = (export[i].LastName != null && export[i].LastName.Length > 30) ? export[i].LastName.Substring(0, 30) : export[i].LastName;
                //formatting is already part of the realDetails.HireDate field
                realDetails[i].HireDate = export[i].HireDate;
                //max 70
                realDetails[i].Username = (export[i].Username != null && export[i].Username.Length > 70) ? export[i].Username.Substring(0, 70) : export[i].Username;
                //max 30
                realDetails[i].EmployeeStatus = (export[i].EmployeeStatus != null && export[i].EmployeeStatus.Length > 30) ? export[i].EmployeeStatus.Substring(0, 30) : export[i].EmployeeStatus;
                //formatting is already part of the realDetails.EmployeeStatusDate field
                realDetails[i].EmployeeStatusDate = export[i].EmployeeStatusDate;
                //formatting is already part of the realDetails.BirthDate field
                realDetails[i].BirthDate = export[i].BirthDate;
                //max 10
                realDetails[i].StandardHoursPerDay = (export[i].StandardHoursPerDay.ToString().Length > 10) ? export[i].StandardHoursPerDay.ToString().Substring(0, 10) : export[i].StandardHoursPerDay.ToString();
                //max 100
                realDetails[i].Address = (export[i].Address != null && export[i].Address.Length > 100) ? export[i].Address.Substring(0, 100) : export[i].Address;
                //max 25
                realDetails[i].City = (export[i].City != null && export[i].City.Length > 25) ? export[i].City.Substring(0, 25) : export[i].City;
                //max 25
                realDetails[i].Province = (export[i].Province != null && export[i].Province.Length > 25) ? export[i].Province.Substring(0, 25) : export[i].Province;
                //max 25
                realDetails[i].Zip = (export[i].Zip != null && export[i].Zip.Length > 25) ? export[i].Zip.Substring(0, 25) : export[i].Zip;
                //max 25
                realDetails[i].Country = (export[i].Country != null && export[i].Country.Length > 25) ? export[i].Country.Substring(0, 25) : export[i].Country;
                //max 35
                realDetails[i].Phone1 = (export[i].Phone1 != null && export[i].Phone1.Length > 35) ? export[i].Phone1.Substring(0, 35) : export[i].Phone1;
                //max 35
                realDetails[i].Phone2 = (export[i].Phone2 != null && export[i].Phone2.Length > 35) ? export[i].Phone2.Substring(0, 35) : export[i].Phone2;
                //max 35
                realDetails[i].Phone3 = (export[i].Phone3 != null && export[i].Phone3.Length > 35) ? export[i].Phone3.Substring(0, 35) : export[i].Phone3;
                //max 50
                realDetails[i].Email = (export[i].Email != null && export[i].Email.Length > 50) ? export[i].Email.Substring(0, 50) : export[i].Email;
                //max 70
                realDetails[i].UnionCode = (export[i].UnionCode != null && export[i].UnionCode.Length > 70) ? export[i].UnionCode.Substring(0, 70) : export[i].UnionCode;
                //max 70
                realDetails[i].PayCycle = (export[i].UnionCode != null && export[i].PayCycle.ToString().Length > 70) ? export[i].PayCycle.ToString().Substring(0, 70) : export[i].PayCycle.ToString();
                //max 70
                realDetails[i].HourlySalary = (export[i].HourlySalary != null && export[i].HourlySalary.Length > 70) ? export[i].HourlySalary.Substring(0, 70) : export[i].HourlySalary;
                //if populated, this will be a decimal
                realDetails[i].BaseRateOfPay = (export[i].BaseRateOfPay == null) ? null : export[i].BaseRateOfPay.ToString();
                //formatting is already part of the realDetails.BaseWage field
                realDetails[i].BaseWage = export[i].BaseWage;
                //formatting is already part of the realDetails.PositionChangeDate field
                realDetails[i].PositionChangeDate = export[i].PositionChangeDate;
                //max 50
                realDetails[i].LL1 = (export[i].LL1 != null && export[i].LL1.Length > 50) ? export[i].LL1.Substring(0, 50) : export[i].LL1;
                //max 50
                realDetails[i].LL2 = (export[i].LL2 != null && export[i].LL2.Length > 50) ? export[i].LL2.Substring(0, 50) : export[i].LL2;
                //max 50
                realDetails[i].LL3 = (export[i].LL3 != null && export[i].LL3.Length > 50) ? export[i].LL3.Substring(0, 50) : export[i].LL3;
                //max 50
                realDetails[i].LL4 = (export[i].LL4 != null && export[i].LL4.Length > 50) ? export[i].LL4.Substring(0, 50) : export[i].LL4;
                //max 50
                realDetails[i].LL5 = (export[i].LL5 != null && export[i].LL5.Length > 50) ? export[i].LL5.Substring(0, 50) : export[i].LL5;
                //max 50
                realDetails[i].LL6 = (export[i].LL6 != null && export[i].LL6.Length > 50) ? export[i].LL6.Substring(0, 50) : export[i].LL6;
                //max 50
                realDetails[i].LL7 = (export[i].LL7 != null && export[i].LL7.Length > 50) ? export[i].LL7.Substring(0, 50) : export[i].LL7;
                //max 250
                realDetails[i].LL1Desc = (export[i].LL1Desc != null && export[i].LL1Desc.Length > 250) ? export[i].LL1Desc.Substring(0, 250) : export[i].LL1Desc;
                //max 250
                realDetails[i].LL2Desc = (export[i].LL2Desc != null && export[i].LL2Desc.Length > 250) ? export[i].LL2Desc.Substring(0, 250) : export[i].LL2Desc;
                //max 250
                realDetails[i].LL3Desc = (export[i].LL3Desc != null && export[i].LL3Desc.Length > 250) ? export[i].LL3Desc.Substring(0, 250) : export[i].LL3Desc;
                //max 250
                realDetails[i].LL4Desc = (export[i].LL4Desc != null && export[i].LL4Desc.Length > 250) ? export[i].LL4Desc.Substring(0, 250) : export[i].LL4Desc;
                //max 250
                realDetails[i].LL5Desc = (export[i].LL5Desc != null && export[i].LL5Desc.Length > 250) ? export[i].LL5Desc.Substring(0, 250) : export[i].LL5Desc;
                //max 250
                realDetails[i].LL6Desc = (export[i].LL6Desc != null && export[i].LL6Desc.Length > 250) ? export[i].LL6Desc.Substring(0, 250) : export[i].LL6Desc;
                //max 250
                realDetails[i].LL7Desc = (export[i].LL7Desc != null && export[i].LL7Desc.Length > 250) ? export[i].LL7Desc.Substring(0, 250) : export[i].LL7Desc;
                //max 15
                realDetails[i].ReportsTo = (export[i].ReportsTo != null && export[i].ReportsTo.Length > 15) ? export[i].ReportsTo.Substring(0, 15) : export[i].ReportsTo;
                //max 15
                realDetails[i].WorkerType = (export[i].WorkerType != null && export[i].WorkerType.Length > 15) ? export[i].WorkerType.Substring(0, 15) : export[i].WorkerType;
                //max 50
                realDetails[i].BadgeNumber = (export[i].BadgeNumber != null && export[i].BadgeNumber.Length > 50) ? export[i].BadgeNumber.Substring(0, 50) : export[i].BadgeNumber;

                //Y or N values, these contain nulls at the moment so account for that
                realDetails[i].MobileLicense = (export[i].MobileLicense == null || export[i].MobileLicense == "0") ? "N" : "Y";
                realDetails[i].ManagerLicense = (export[i].ManagerLicense == null || export[i].ManagerLicense == "0") ? "N" : "Y";
                realDetails[i].ScheduleLicense = (export[i].ScheduleLicense == null || export[i].ScheduleLicense == "0") ? "N" : "Y";

                //max 50
                realDetails[i].Timezone = (export[i].Timezone != null && export[i].Timezone.Length > 50) ? export[i].Timezone.Substring(0, 50) : export[i].Timezone;

                //formatting is already part of the realDetails.SeniorityDate field
                realDetails[i].SeniorityDate = export[i].SeniorityDate;
                //max 50

                //if len is 1, then pad with a leading 0
                if (export[i].DrawNumber != null && export[i].DrawNumber.Length == 1)
                    realDetails[i].DrawNumber = export[i].DrawNumber.PadLeft(2, '0');
                else
                    realDetails[i].DrawNumber = (export[i].DrawNumber != null && export[i].DrawNumber.Length > 50) ? export[i].DrawNumber.Substring(0, 50) : export[i].DrawNumber;

                //max 250
                realDetails[i].EligibleJobs = (export[i].EligibleJobs != null && export[i].EligibleJobs.Length > 250) ? export[i].EligibleJobs.Substring(0, 250) : export[i].EligibleJobs;
                //Y or N values, these contain nulls at the moment so account for that
                realDetails[i].Posting = (export[i].Posting == null || export[i].Posting == "0") ? "N" : "Y";

                //formatting is already part of the realDetails.PostingEndDate field
                realDetails[i].PostingEndDate = export[i].PostingEndDate;

                //Y or N values, these contain nulls at the moment so account for that
                realDetails[i].BankVacation = (export[i].BankVacation == null || export[i].BankVacation == "0") ? "N" : "Y";

                realDetails[i].SecondaryLabour = export[i].SecondaryLabour;
            }

            return realDetails;
        }

        #endregion

        public void ImportStatDeduction(String database, String username, List<StatDeductionImport> array)
        {
            _sqlServerStatutoryDeduction.ImportStatDeductionViaService(database, username, array);
        }
    }
}
