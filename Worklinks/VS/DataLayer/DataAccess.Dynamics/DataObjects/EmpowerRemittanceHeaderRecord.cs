﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [FixedLengthRecord()]
    public class EmpowerRemittanceHeaderRecord
    {
        [FieldFixedLength(1)]
        public string RecordType;

        [FieldFixedLength(9)]
        public string FEIN;

        [FieldFixedLength(26)]
        public string Filler1;

        [FieldFixedLength(15)]
        public string PayrollId;  //treated as alpha

        [FieldFixedLength(15)]
        public string UnitId;  //treated as alpha

        [FieldFixedLength(10)]
        [FieldConverter(ConverterKind.Date, "MM/dd/yyyy")]
        public DateTime PayDate;

        [FieldFixedLength(17)]
        public string LongAgencyCode;

        [FieldFixedLength(1)]
        public string Filler2;

        [FieldFixedLength(8)]
        public string EmployeeCount;

        [FieldFixedLength(8)]
        public string Filler3;

        [FieldFixedLength(35)]
        public string ExtendedPayrollId;

        [FieldFixedLength(35)]
        public string ExtendedUnitID;

        [FieldFixedLength(1)]
        public string CreditRollForwardIndicator;

        [FieldFixedLength(1)]
        public string CreditRollForwardAdjustmentRecord;

        [FieldFixedLength(1)]
        public string CreditRollForwardQuarter;

        [FieldFixedLength(35)]
        public string CreditRollForwardReference;
        
        [FieldFixedLength(29)]
        public string CompanyId; //(foreign taxes only) "CAN" applies here

        [FieldFixedLength(2)]
        public string CountryCode;

        [FieldFixedLength(10)]
        public string Version;

        [FieldFixedLength(50)]
        public string UserDefinedField;

        [FieldFixedLength(6)]
        public string WithheldPSDCode;

        [FieldFixedLength(1)]
        public string AdjustmentRecordIndicator;

        [FieldIgnored]
        public EmpowerRemittanceWageRecord[] WageRecords;  //stacy note:  I read [FieldIgnored] is replaced with [FieldHidden] in newer filehelpers

        public EmpowerRemittanceHeaderRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = "H";
            FEIN = "000000000"; //If the record is for US taxes, this field will contain the FEIN. 
                                //If the record is for foreign taxes, the Company ID and country code in fields 17 and 18 should be populated and the FEIN must be zero filled.
            Filler1 = "                          ";
            PayrollId = "";
            UnitId = "";
            PayDate = DateTime.Now;
            LongAgencyCode = "                 ";
            Filler2 = " ";
            EmployeeCount = "";
            Filler3 = "        ";
            ExtendedPayrollId = "                                   ";
            ExtendedUnitID = "                                   ";
            CreditRollForwardIndicator = " ";
            CreditRollForwardAdjustmentRecord = " ";
            CreditRollForwardQuarter = " ";
            CreditRollForwardReference = "                                   ";
            CompanyId = "";
            CountryCode = "CA";
            Version = "PT12.3.00 ";
            UserDefinedField = "                                                  ";
            WithheldPSDCode = "      ";
            AdjustmentRecordIndicator = " ";
            WageRecords = null;
        }
    }
}
