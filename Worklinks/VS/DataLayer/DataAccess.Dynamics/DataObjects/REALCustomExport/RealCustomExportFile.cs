﻿using System;
using FileHelpers;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.REALCustomExport
{
    [DelimitedRecord(",")]
    public class RealCustomExportHeader
    {
        public string EmployeeNumber;
        public string EmployeeFirstName;
        public string EmployeeLastName;
        public string TransactionDate;
        public string PayCodeType;
        public string PayCode;
        public string PayCodeDescription;
        public string CurrentHours;
        public string CurrentRate;
        public string CurrentAmount;
        public string CostCentreNumber;
        public string CostCentreDescription;
        public string PayPeriod;

        public RealCustomExportHeader()
        {
            Clear();
        }

        protected void Clear()
        {
            EmployeeNumber = "EmployeeId";
            EmployeeFirstName = "EmployeeFirstName";
            EmployeeLastName = "EmployeeLastName";
            TransactionDate = "TransactionDate";
            PayCodeType = "PayCodeType";
            PayCode = "PayCode";
            PayCodeDescription = "PayCodeDescription";
            CurrentHours = "CurrentHours";
            CurrentRate = "CurrentRate";
            CurrentAmount = "CurrentAmount";
            CostCentreNumber = "CostCentreNumber";
            CostCentreDescription = "CostCentreDescription";
            PayPeriod = "PayPeriod";
        }
    }

    [DelimitedRecord(",")]
    public class RealCustomExportDetail
    {
        public string EmployeeNumber;
        public string EmployeeFirstName;
        public string EmployeeLastName;
        [FieldConverter(ConverterKind.Date, "MM/dd/yyyy")]
        public DateTime TransactionDate;
        public string PayCodeType;
        public string PayCode;
        public string PayCodeDescription;
        public string CurrentHours;
        public string CurrentRate;
        public string CurrentAmount;
        public string CostCentreNumber;
        public string CostCentreDescription;
        public string PayPeriod;

        public RealCustomExportDetail()
        {
            Clear();
        }

        protected void Clear()
        {
            EmployeeNumber = "";
            EmployeeFirstName = "";
            EmployeeLastName = "";
            TransactionDate = DateTime.MinValue;
            PayCodeType = "";
            PayCode = "";
            PayCodeDescription = "";
            CurrentHours = "";
            CurrentRate = "";
            CurrentAmount = "";
            CostCentreNumber = "";
            CostCentreDescription = "";
            PayPeriod = "";
        }
    }
}