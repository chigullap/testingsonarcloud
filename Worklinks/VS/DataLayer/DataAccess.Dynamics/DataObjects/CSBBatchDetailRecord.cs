﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [FixedLengthRecord()]
    public class CSBBatchDetailRecord
    {
        [FieldFixedLength(2)]
        public int RecordType;

        [FieldFixedLength(5)]
        public string OrganizationId;

        [FieldFixedLength(10)]
        [FieldConverter(ConverterKind.Date, "yyyy-MM-dd")]
        public DateTime EffectiveDate;

        [FieldFixedLength(5)]
        public string SequenceNumber;

        [FieldFixedLength(9)]
        public string EmployeeReferenceNumber;

        [FieldFixedLength(50)]
        public string EmployeeName;

        [FieldFixedLength(1)]
        public string Sign;

        [FieldFixedLength(8)]
        public string DeductionAmount;

        [FieldFixedLength(10)]
        [FieldConverter(ConverterKind.Date, "yyyy-MM-dd")]
        public DateTime? EmployeeBirthDate;

        [FieldFixedLength(20)]
        public string Filler;

        public CSBBatchDetailRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = 50;
            OrganizationId = null;
            EffectiveDate = DateTime.Now;
            SequenceNumber = null;              //00001...99999, incremented
            EmployeeReferenceNumber = null;     //contains SIN
            EmployeeName = null;
            Sign = " ";
            DeductionAmount = null;             //8 digits.  Last 2 are implied decimal (ie. $10 is 00001000)
            EmployeeBirthDate = DateTime.Now;
            Filler = "                   x";    //doc states "after the last field a filler is needed.  Put a char in the 120th position to ensure no compression occurs during movement between platforms"
        }
    }
}
