﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [FixedLengthRecord()]
    public class RRSPDetailRecord
    {
        [FieldFixedLength(6)]
        public string GroupNumber;

        [FieldFixedLength(9)]
        public string EmployeeNumber;

        [FieldFixedLength(10)]
        public string DepositAmount;

        [FieldFixedLength(1)]
        public string DepositIndicator;

        [FieldFixedLength(20)]
        public string EmployeeName;

        public RRSPDetailRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            GroupNumber = "";                   //spec says this will be 4 digits
            EmployeeNumber = "";                //spec says this will be 9 digits
            DepositAmount = "";                 //spec says this will be 10 digits  (0000001000 for $10.00)
            DepositIndicator = "1";             //spec says this i will be:  1 for employee contribution, 2 for employer contribution, 3 for employee/other contribution (normally an additional employee contribution)
            EmployeeName = "";                  //spec says this will be MAX 20 chars
        }
    }
}
