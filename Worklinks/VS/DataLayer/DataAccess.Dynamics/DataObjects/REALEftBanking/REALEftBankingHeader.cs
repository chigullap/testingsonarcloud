﻿using System;
using FileHelpers;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.REALEftBanking
{
    [FixedLengthRecord()]
    public class REALEftBankingHeader
    {
        [FieldFixedLength(1)]
        public string RecordType;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public int RecordSequenceCount;

        [FieldFixedLength(10)]
        public string EFTOriginatorID;

        [FieldFixedLength(4)]
        [FieldAlign(AlignMode.Right, '0')]
        public int FileCreationNumber;

        [FieldFixedLength(6)]
        [FieldConverter(typeof(CYYDDDConverter))] //specific date format for HSBC
        public DateTime FileCreationDate;

        [FieldFixedLength(5)]
        public string HSBCInstitutionID;

        [FieldFixedLength(20)]
        public string Reserved;

        [FieldFixedLength(3)]
        public string CurrencyCodeIdentifier;

        [FieldFixedLength(1406)]
        public string Filler;

        public REALEftBankingHeader()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = "A";
            RecordSequenceCount = 1;
            EFTOriginatorID = "";                           //supplied by HSBC (code system value)
            FileCreationNumber = 0;                         //use sequence:  HSBC_HEADER_SEQUENCE.  Identifier for this EFT file. Each file must contain a file creation number that is greater than that of previously transmitted files.
            FileCreationDate = DateTime.Now;                //Format CYYDDD: C = 0 (no change in turn of century) YY = 2 digit year DDD = day of the year
            HSBCInstitutionID = "01600";                    //always 01600
            Reserved = null;
            CurrencyCodeIdentifier = "CAD";
            Filler = null;
        }
    }

    public class CYYDDDConverter : ConverterBase
    {
        public override object StringToField(string from)
        {
            return Convert.ToDateTime(from); //not used since we are not reading from an input file
        }
        public override string FieldToString(object fieldValue) //used when writing to a file.  This will format the date as per HSBC spec CYYDDD
        {
            DateTime date = Convert.ToDateTime(fieldValue);
            return string.Format("0{0}{1:000}", date.ToString("yy"), date.DayOfYear);
        }
    }
}
