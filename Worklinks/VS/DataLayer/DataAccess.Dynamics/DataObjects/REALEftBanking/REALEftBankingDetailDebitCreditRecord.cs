﻿using System;
using FileHelpers;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.REALEftBanking
{
    [FixedLengthRecord()]
    public class REALEftBankingDetailDebitCreditRecord
    {
        [FieldFixedLength(1)]
        public string RecordType;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public int RecordSequenceCount;

        [FieldFixedLength(10)]
        public string EFTOriginatorID;

        [FieldFixedLength(4)]
        [FieldAlign(AlignMode.Right, '0')]
        public int FileCreationNumber;

        #region segment 1

        //SEGMENT 1 FIELDS
        [FieldFixedLength(3)]
        public string CPACode1;

        [FieldFixedLength(10)]
        public string Amount1;

        [FieldFixedLength(1)]
        public string DueDateCentury1;

        [FieldFixedLength(2)]
        [FieldConverter(ConverterKind.Date, "yy")]
        public DateTime? DueDateYear1;

        [FieldFixedLength(3)]
        public string DueDateDay1;

        [FieldFixedLength(4)]
        public string FinancialInstitutionNumber1;

        [FieldFixedLength(5)]
        public string FinancialInstitutionBranchNumber1;

        [FieldFixedLength(12)]
        public string AccountNumber1;

        [FieldFixedLength(22)]
        public string ItemTraceNumber1;

        [FieldFixedLength(3)]
        public string StoredTransType1;

        [FieldFixedLength(15)]
        public string ShortName1;

        [FieldFixedLength(30)]
        public string TransactionPayeePayorName1;

        [FieldFixedLength(30)]
        public string LongName1;

        [FieldFixedLength(10)]
        public string EFTOriginatorIDCopy1;

        [FieldFixedLength(19)]
        public string TransactionOriginationXref1;

        [FieldFixedLength(9)]
        public string TransactionReturnFinancialInstitution1;

        [FieldFixedLength(12)]
        public string TransactionReturnAccount1;

        [FieldFixedLength(15)]
        public string OriginatorsSundryInfo1;

        [FieldFixedLength(22)]
        public string StoredTraceNumber1;

        [FieldFixedLength(2)]
        public string SettlementCode1;

        [FieldFixedLength(11)]
        public string InvalidDataElement1;

        #endregion

        #region segment 2

        //SEGMENT 2 FIELDS
        [FieldFixedLength(3)]
        public string CPACode2;

        [FieldFixedLength(10)]
        public string Amount2;

        [FieldFixedLength(1)]
        public string DueDateCentury2;

        [FieldFixedLength(2)]
        [FieldConverter(ConverterKind.Date, "yy")]
        public DateTime? DueDateYear2;

        [FieldFixedLength(3)]
        public string DueDateDay2;

        [FieldFixedLength(4)]
        public string FinancialInstitutionNumber2;

        [FieldFixedLength(5)]
        public string FinancialInstitutionBranchNumber2;

        [FieldFixedLength(12)]
        public string AccountNumber2;

        [FieldFixedLength(22)]
        public string ItemTraceNumber2;

        [FieldFixedLength(3)]
        public string StoredTransType2;

        [FieldFixedLength(15)]
        public string ShortName2;

        [FieldFixedLength(30)]
        public string TransactionPayeePayorName2;

        [FieldFixedLength(30)]
        public string LongName2;

        [FieldFixedLength(10)]
        public string EFTOriginatorIDCopy2;

        [FieldFixedLength(19)]
        public string TransactionOriginationXref2;

        [FieldFixedLength(9)]
        public string TransactionReturnFinancialInstitution2;

        [FieldFixedLength(12)]
        public string TransactionReturnAccount2;

        [FieldFixedLength(15)]
        public string OriginatorsSundryInfo2;

        [FieldFixedLength(22)]
        public string StoredTraceNumber2;

        [FieldFixedLength(2)]
        public string SettlementCode2;

        [FieldFixedLength(11)]
        public string InvalidDataElement2;

        #endregion

        #region segment 3

        //SEGMENT 3 FIELDS
        [FieldFixedLength(3)]
        public string CPACode3;

        [FieldFixedLength(10)]
        public string Amount3;

        [FieldFixedLength(1)]
        public string DueDateCentury3;

        [FieldFixedLength(2)]
        [FieldConverter(ConverterKind.Date, "yy")]
        public DateTime? DueDateYear3;

        [FieldFixedLength(3)]
        public string DueDateDay3;

        [FieldFixedLength(4)]
        public string FinancialInstitutionNumber3;

        [FieldFixedLength(5)]
        public string FinancialInstitutionBranchNumber3;

        [FieldFixedLength(12)]
        public string AccountNumber3;

        [FieldFixedLength(22)]
        public string ItemTraceNumber3;

        [FieldFixedLength(3)]
        public string StoredTransType3;

        [FieldFixedLength(15)]
        public string ShortName3;

        [FieldFixedLength(30)]
        public string TransactionPayeePayorName3;

        [FieldFixedLength(30)]
        public string LongName3;

        [FieldFixedLength(10)]
        public string EFTOriginatorIDCopy3;

        [FieldFixedLength(19)]
        public string TransactionOriginationXref3;

        [FieldFixedLength(9)]
        public string TransactionReturnFinancialInstitution3;

        [FieldFixedLength(12)]
        public string TransactionReturnAccount3;

        [FieldFixedLength(15)]
        public string OriginatorsSundryInfo3;

        [FieldFixedLength(22)]
        public string StoredTraceNumber3;

        [FieldFixedLength(2)]
        public string SettlementCode3;

        [FieldFixedLength(11)]
        public string InvalidDataElement3;

        #endregion

        #region segment 4

        //SEGMENT 4 FIELDS
        [FieldFixedLength(3)]
        public string CPACode4;

        [FieldFixedLength(10)]
        public string Amount4;

        [FieldFixedLength(1)]
        public string DueDateCentury4;

        [FieldFixedLength(2)]
        [FieldConverter(ConverterKind.Date, "yy")]
        public DateTime? DueDateYear4;

        [FieldFixedLength(3)]
        public string DueDateDay4;

        [FieldFixedLength(4)]
        public string FinancialInstitutionNumber4;

        [FieldFixedLength(5)]
        public string FinancialInstitutionBranchNumber4;

        [FieldFixedLength(12)]
        public string AccountNumber4;

        [FieldFixedLength(22)]
        public string ItemTraceNumber4;

        [FieldFixedLength(3)]
        public string StoredTransType4;

        [FieldFixedLength(15)]
        public string ShortName4;

        [FieldFixedLength(30)]
        public string TransactionPayeePayorName4;

        [FieldFixedLength(30)]
        public string LongName4;

        [FieldFixedLength(10)]
        public string EFTOriginatorIDCopy4;

        [FieldFixedLength(19)]
        public string TransactionOriginationXref4;

        [FieldFixedLength(9)]
        public string TransactionReturnFinancialInstitution4;

        [FieldFixedLength(12)]
        public string TransactionReturnAccount4;

        [FieldFixedLength(15)]
        public string OriginatorsSundryInfo4;

        [FieldFixedLength(22)]
        public string StoredTraceNumber4;

        [FieldFixedLength(2)]
        public string SettlementCode4;

        [FieldFixedLength(11)]
        public string InvalidDataElement4;

        #endregion

        #region segment 5

        //SEGMENT 5 FIELDS
        [FieldFixedLength(3)]
        public string CPACode5;

        [FieldFixedLength(10)]
        public string Amount5;

        [FieldFixedLength(1)]
        public string DueDateCentury5;

        [FieldFixedLength(2)]
        [FieldConverter(ConverterKind.Date, "yy")]
        public DateTime? DueDateYear5;

        [FieldFixedLength(3)]
        public string DueDateDay5;

        [FieldFixedLength(4)]
        public string FinancialInstitutionNumber5;

        [FieldFixedLength(5)]
        public string FinancialInstitutionBranchNumber5;

        [FieldFixedLength(12)]
        public string AccountNumber5;

        [FieldFixedLength(22)]
        public string ItemTraceNumber5;

        [FieldFixedLength(3)]
        public string StoredTransType5;

        [FieldFixedLength(15)]
        public string ShortName5;

        [FieldFixedLength(30)]
        public string TransactionPayeePayorName5;

        [FieldFixedLength(30)]
        public string LongName5;

        [FieldFixedLength(10)]
        public string EFTOriginatorIDCopy5;

        [FieldFixedLength(19)]
        public string TransactionOriginationXref5;

        [FieldFixedLength(9)]
        public string TransactionReturnFinancialInstitution5;

        [FieldFixedLength(12)]
        public string TransactionReturnAccount5;

        [FieldFixedLength(15)]
        public string OriginatorsSundryInfo5;

        [FieldFixedLength(22)]
        public string StoredTraceNumber5;

        [FieldFixedLength(2)]
        public string SettlementCode5;

        [FieldFixedLength(11)]
        public string InvalidDataElement5;

        #endregion

        #region segment 6

        //SEGMENT 6 FIELDS
        [FieldFixedLength(3)]
        public string CPACode6;

        [FieldFixedLength(10)]
        public string Amount6;

        [FieldFixedLength(1)]
        public string DueDateCentury6;

        [FieldFixedLength(2)]
        [FieldConverter(ConverterKind.Date, "yy")]
        public DateTime? DueDateYear6;

        [FieldFixedLength(3)]
        public string DueDateDay6;

        [FieldFixedLength(4)]
        public string FinancialInstitutionNumber6;

        [FieldFixedLength(5)]
        public string FinancialInstitutionBranchNumber6;

        [FieldFixedLength(12)]
        public string AccountNumber6;

        [FieldFixedLength(22)]
        public string ItemTraceNumber6;

        [FieldFixedLength(3)]
        public string StoredTransType6;

        [FieldFixedLength(15)]
        public string ShortName6;

        [FieldFixedLength(30)]
        public string TransactionPayeePayorName6;

        [FieldFixedLength(30)]
        public string LongName6;

        [FieldFixedLength(10)]
        public string EFTOriginatorIDCopy6;

        [FieldFixedLength(19)]
        public string TransactionOriginationXref6;

        [FieldFixedLength(9)]
        public string TransactionReturnFinancialInstitution6;

        [FieldFixedLength(12)]
        public string TransactionReturnAccount6;

        [FieldFixedLength(15)]
        public string OriginatorsSundryInfo6;

        [FieldFixedLength(22)]
        public string StoredTraceNumber6;

        [FieldFixedLength(2)]
        public string SettlementCode6;

        [FieldFixedLength(11)]
        public string InvalidDataElement6;

        #endregion

        public REALEftBankingDetailDebitCreditRecord()
        {
            Clear();
        }

        //Up to 6 segments can be populated and because unused segments must be blank, we do not default any segment fields, and do not put formatting on the fields until we create the segment.
        protected void Clear()
        {
            RecordType = "C";                           //C = Credit Payment, D = Debit Payment
            RecordSequenceCount = 1;                    //Increment by 1 from field 02 in the Header record
            EFTOriginatorID = null;                     //Must be the same as field 03 in the Header record
            FileCreationNumber = 0;                     //Must be the same as field 04 in the Header record

            #region segment 1

            //SEGMENT 1 FIELDS
            CPACode1 = null;                            //200 = Payroll Deposit
            Amount1 = null;                             //Amount of EFT transaction, format $$$$$$$$¢¢ (leading 0's in the file example)
            DueDateCentury1 = null;                     //spec says use 0
            DueDateYear1 = null;
            DueDateDay1 = null;                         //Julian date (day of year)
            FinancialInstitutionNumber1 = null;         //Format 0999 (bank number)
            FinancialInstitutionBranchNumber1 = null;   //Format 99999 (branch / transit number)
            AccountNumber1 = null;                      //Participant account number; field must be left justified, do not zero fill.Enter significant digits only
            ItemTraceNumber1 = null;
            StoredTransType1 = null;
            ShortName1 = null;                          //15 chars
            TransactionPayeePayorName1 = null;          //30 chars
            LongName1 = null;                           //30 chars
            EFTOriginatorIDCopy1 = null;                //Must be the same as field 03 in the Header record
            TransactionOriginationXref1 = null;         //Optional reference field
            TransactionReturnFinancialInstitution1 = null;
            TransactionReturnAccount1 = null;
            OriginatorsSundryInfo1 = null;              //may be empty
            StoredTraceNumber1 = null;                  //blank
            SettlementCode1 = null;                     //blank
            InvalidDataElement1 = null;                 //all 0's

            #endregion

            #region segment 2

            //SEGMENT 2 FIELDS
            CPACode2 = null;
            Amount2 = null;
            DueDateCentury2 = null;
            DueDateYear2 = null;
            DueDateDay2 = null;
            FinancialInstitutionNumber2 = null;
            FinancialInstitutionBranchNumber2 = null;
            AccountNumber2 = null;
            ItemTraceNumber2 = null;
            StoredTransType2 = null;
            ShortName2 = null;
            TransactionPayeePayorName2 = null;
            LongName2 = null;
            EFTOriginatorIDCopy2 = null;
            TransactionOriginationXref2 = null;
            TransactionReturnFinancialInstitution2 = null;
            TransactionReturnAccount2 = null;
            OriginatorsSundryInfo2 = null;
            StoredTraceNumber2 = null;
            SettlementCode2 = null;
            InvalidDataElement2 = null;

            #endregion

            #region segment 3

            //SEGMENT 3 FIELDS
            CPACode3 = null;
            Amount3 = null;
            DueDateCentury3 = null;
            DueDateYear3 = null;
            DueDateDay3 = null;
            FinancialInstitutionNumber3 = null;
            FinancialInstitutionBranchNumber3 = null;
            AccountNumber3 = null;
            ItemTraceNumber3 = null;
            StoredTransType3 = null;
            ShortName3 = null;
            TransactionPayeePayorName3 = null;
            LongName3 = null;
            EFTOriginatorIDCopy3 = null;
            TransactionOriginationXref3 = null;
            TransactionReturnFinancialInstitution3 = null;
            TransactionReturnAccount3 = null;
            OriginatorsSundryInfo3 = null;
            StoredTraceNumber3 = null;
            SettlementCode3 = null;
            InvalidDataElement3 = null;

            #endregion

            #region segment 4

            //SEGMENT 4 FIELDS
            CPACode4 = null;
            Amount4 = null;
            DueDateCentury4 = null;
            DueDateYear4 = null;
            DueDateDay4 = null;
            FinancialInstitutionNumber4 = null;
            FinancialInstitutionBranchNumber4 = null;
            AccountNumber4 = null;
            ItemTraceNumber4 = null;
            StoredTransType4 = null;
            ShortName4 = null;
            TransactionPayeePayorName4 = null;
            LongName4 = null;
            EFTOriginatorIDCopy4 = null;
            TransactionOriginationXref4 = null;
            TransactionReturnFinancialInstitution4 = null;
            TransactionReturnAccount4 = null;
            OriginatorsSundryInfo4 = null;
            StoredTraceNumber4 = null;
            SettlementCode4 = null;
            InvalidDataElement4 = null;

            #endregion

            #region segment 5

            //SEGMENT 5 FIELDS
            CPACode5 = null;
            Amount5 = null;
            DueDateCentury5 = null;
            DueDateYear5 = null;
            DueDateDay5 = null;
            FinancialInstitutionNumber5 = null;
            FinancialInstitutionBranchNumber5 = null;
            AccountNumber5 = null;
            ItemTraceNumber5 = null;
            StoredTransType5 = null;
            ShortName5 = null;
            TransactionPayeePayorName5 = null;
            LongName5 = null;
            EFTOriginatorIDCopy5 = null;
            TransactionOriginationXref5 = null;
            TransactionReturnFinancialInstitution5 = null;
            TransactionReturnAccount5 = null;
            OriginatorsSundryInfo5 = null;
            StoredTraceNumber5 = null;
            SettlementCode5 = null;
            InvalidDataElement5 = null;

            #endregion

            #region segment 6

            //SEGMENT 6 FIELDS
            CPACode6 = null;
            Amount6 = null;
            DueDateCentury6 = null;
            DueDateYear6 = null;
            DueDateDay6 = null;
            FinancialInstitutionNumber6 = null;
            FinancialInstitutionBranchNumber6 = null;
            AccountNumber6 = null;
            ItemTraceNumber6 = null;
            StoredTransType6 = null;
            ShortName6 = null;
            TransactionPayeePayorName6 = null;
            LongName6 = null;
            EFTOriginatorIDCopy6 = null;
            TransactionOriginationXref6 = null;
            TransactionReturnFinancialInstitution6 = null;
            TransactionReturnAccount6 = null;
            OriginatorsSundryInfo6 = null;
            StoredTraceNumber6 = null;
            SettlementCode6 = null;
            InvalidDataElement6 = null;

            #endregion
        }
    }
}
