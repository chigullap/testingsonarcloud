﻿using System;
using FileHelpers;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.REALEftBanking
{
    [FixedLengthRecord()]
    public class REALEftBankingTrailer
    {
        [FieldFixedLength(1)]
        public string RecordType;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public int RecordSequenceCount;

        [FieldFixedLength(10)]
        public string EFTOriginatorID;

        [FieldFixedLength(4)]
        [FieldAlign(AlignMode.Right, '0')]
        public int FileCreationNumber;

        [FieldFixedLength(14)]
        [FieldAlign(AlignMode.Right, '0')]
        public string ValueOfDebits;

        [FieldFixedLength(8)]
        [FieldAlign(AlignMode.Right, '0')]
        public string NumberOfDebits;

        [FieldFixedLength(14)]
        [FieldAlign(AlignMode.Right, '0')]
        public string ValueOfCredits;

        [FieldFixedLength(8)]
        [FieldAlign(AlignMode.Right, '0')]
        public string NumberOfCredits;

        [FieldFixedLength(14)]
        [FieldAlign(AlignMode.Right, '0')]
        public string ValueOfErrCorrE;

        [FieldFixedLength(8)]
        [FieldAlign(AlignMode.Right, '0')]
        public string NumberOfErrCorrE;

        [FieldFixedLength(14)]
        [FieldAlign(AlignMode.Right, '0')]
        public string ValueOfErrCorrF;

        [FieldFixedLength(8)]
        [FieldAlign(AlignMode.Right, '0')]
        public string NumberOfErrCorrF;

        [FieldFixedLength(1352)]
        public string Filler;

        public REALEftBankingTrailer()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = "Z";
            RecordSequenceCount = 0;                        //will be overwritten.  Increment by 1 from previous record sequence number
            EFTOriginatorID = null;                         //Must be the same as field 03 in the Header record
            FileCreationNumber = 0;                         //Must be the same as field 04 in the Header record
            ValueOfDebits = "0";                            //(will be 0).  Amount of debit payment transactions (type D). Right justify with leading zeros. Format $$$$$$$$$$$$¢¢
            NumberOfDebits = "0";                           //(will be 0).  Total number of debit payment transactions (type D). Right justify with leading zeros
            ValueOfCredits = "0";                           //Amount of credit payment transactions (type C). Right justify with leading zeros. Format $$$$$$$$$$$$¢¢
            NumberOfCredits = "0";                          //Total number of credit payment transactions (type C). Right justify with leading zeros.
            ValueOfErrCorrE = "0";                          //all 0
            NumberOfErrCorrE = "0";                         //all 0
            ValueOfErrCorrF = "0";                          //all 0
            NumberOfErrCorrF = "0";                         //all 0
            Filler = null;                                  //blank
        }
    }
}
