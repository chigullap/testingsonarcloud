﻿using System;
using FileHelpers;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.SocialCosts
{
    [DelimitedRecord(",")]
    public class SocialCostsExportFileHeader
    {
        public string EmployeeNumber;
        public string BenefitName;
        public string BenefitValue;
        public string StartDate;
        public string EndDate;

        public SocialCostsExportFileHeader()
        {
            Clear();
        }
        protected void Clear()
        {
            EmployeeNumber = "EmployeeNumber";
            BenefitName = "BenefitName";
            BenefitValue = "BenefitValue";
            StartDate = "StartDate";
            EndDate = "EndDate";
        }
    }

    [DelimitedRecord(",")]
    public class SocialCostsExportFileDetails
    {
        public string EmployeeNumber;
        public string BenefitName;
        public string BenefitValue;

        [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
        public DateTime StartDate;

        [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
        public DateTime EndDate;

        public SocialCostsExportFileDetails()
        {
            Clear();
        }
        protected void Clear()
        {
            EmployeeNumber = "";
            BenefitName = "";
            BenefitValue = "";
            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
        }
    }
}


