﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.RbcEftEdi;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.ChequeWcbRemittance
{
    public class ChequeWcbRemittanceDetail
    {
        //these classes are from the RbcEftEdi folder but not all classes were to be used so i created this class to only create what i needed
        public IsaInterchangeControlHeader[] isaHeader = new IsaInterchangeControlHeader[1];
        public GsFunctionalGroupHeader[] gsHeader = new GsFunctionalGroupHeader[1];
        public ChequeStTransactionSetHeader[] st = new ChequeStTransactionSetHeader[1];
        public ChequeBeginningSegmentForPaymentOrderRemittanceAdvice[] bpr = new ChequeBeginningSegmentForPaymentOrderRemittanceAdvice[1];
        public ChequeTrace[] trn = new ChequeTrace[1];
        public ChequeCurrency[] cur = new ChequeCurrency[1];
        public ChequeReferenceIdentification[] ref1 = new ChequeReferenceIdentification[1];
        public ChequeReferenceIdentification2[] ref2 = new ChequeReferenceIdentification2[1];
        public ChequeReferenceIdentification3[] ref3 = new ChequeReferenceIdentification3[1];
        public ChequeReferenceIdentification4[] ref4 = new ChequeReferenceIdentification4[1];
        public ChequeReferenceIdentificationCustom[] customLine1 = new ChequeReferenceIdentificationCustom[1]; //this is being used to display "Remit Period: YYYY/MM/DD - YYYY/MM/DD"
        public ChequeReferenceIdentificationCustom[] customLine2 = new ChequeReferenceIdentificationCustom[1]; //this is being used to display "Remuneration Amount: $xx.xx"
        public ChequeReferenceIdentificationCustom[] customLine3 = new ChequeReferenceIdentificationCustom[1]; //this is being used to display "Remittance Amount: $xx.xx"
        public ChequeReferenceIdentification6[] ref6 = new ChequeReferenceIdentification6[1];
        public ChequeDateTimeReference[] dtm = new ChequeDateTimeReference[1];
        public ChequeOriginatorNamePayor[] n1pr = new ChequeOriginatorNamePayor[1];
        public ChequePayeeReceiverName[] n1pe = new ChequePayeeReceiverName[1];
        public ChequePayeeReceiverAddressInformation[] n3 = new ChequePayeeReceiverAddressInformation[1];
        public PayeeReceiverGeographicLocation[] n4 = new PayeeReceiverGeographicLocation[1];
        public ChequeEntity[] ent = new ChequeEntity[1];
        public WcbChequeRemittanceAdviceAccountsReceivableOpenItemReference[] rmr = new WcbChequeRemittanceAdviceAccountsReceivableOpenItemReference[1];
        public ChequeReferenceIdentification5[] ref5 = new ChequeReferenceIdentification5[1];
        public ChequeSeTransactionSetTrailer[] se = new ChequeSeTransactionSetTrailer[1];
        public GsFunctionalGroupTrailer[] geTrailer = new GsFunctionalGroupTrailer[1];
        public IsaInterchangeControlTrailer[] ieaTrailer = new IsaInterchangeControlTrailer[1];

        //optional so not instantiated in constructor
        public ChequePayeeReceiverAddressInformation2[] n3Part2 = new ChequePayeeReceiverAddressInformation2[1];

        public ChequeWcbRemittanceDetail()
        {
            //instantiate and overwrite any defaults that need to be changed for wcb cheque remittances
            isaHeader[0] = new IsaInterchangeControlHeader();
            gsHeader[0] = new GsFunctionalGroupHeader();
            st[0] = new ChequeStTransactionSetHeader();
            bpr[0] = new ChequeBeginningSegmentForPaymentOrderRemittanceAdvice();
            trn[0] = new ChequeTrace();
            cur[0] = new ChequeCurrency();
            ref1[0] = new ChequeReferenceIdentification();
            ref2[0] = new ChequeReferenceIdentification2();
            ref3[0] = new ChequeReferenceIdentification3();
            ref4[0] = new ChequeReferenceIdentification4();
            customLine1[0] = new ChequeReferenceIdentificationCustom();
            customLine2[0] = new ChequeReferenceIdentificationCustom();
            customLine3[0] = new ChequeReferenceIdentificationCustom();
            ref6[0] = new ChequeReferenceIdentification6();
            dtm[0] = new ChequeDateTimeReference();
            n1pr[0] = new ChequeOriginatorNamePayor();
            n1pe[0] = new ChequePayeeReceiverName();
            n3[0] = new ChequePayeeReceiverAddressInformation();
            n4[0] = new PayeeReceiverGeographicLocation();
            ent[0] = new ChequeEntity();
            rmr[0] = new WcbChequeRemittanceAdviceAccountsReceivableOpenItemReference();
            ref5[0] = new ChequeReferenceIdentification5();
            se[0] = new ChequeSeTransactionSetTrailer() { NumberOfIncludedSegments = "18" };
            geTrailer[0] = new GsFunctionalGroupTrailer();
            ieaTrailer[0] = new IsaInterchangeControlTrailer();
        }
    }

    [DelimitedRecord("*")]
    public class WcbChequeRemittanceAdviceAccountsReceivableOpenItemReference
    {
        public string IdElement;
        public string EmptySpot1;
        public string EmptySpot2;
        public string EmptySpot3;
        public string MonetaryAmount;
        public string AssessableAmount;

        public WcbChequeRemittanceAdviceAccountsReceivableOpenItemReference()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "RMR";
            EmptySpot1 = "";
            EmptySpot2 = "";
            EmptySpot3 = "";
            MonetaryAmount = "";            //from payroll_master_payment.amount  Max 10 including 2 decimal places (99999999.99)
            AssessableAmount = "";          //for WCB and HEALTH TAX cheques, we will store the assessable earnings here
        }
    }
}
