﻿using System;
using FileHelpers;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.RbcEftEdiSourceDed;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.RbcEftEdi;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.RbcRevenuQuebecEdiSourceDed
{
    public class RbcRqRemittanceDetails
    {
        //some of these classes are existing classes used in the CRA remits or Direct Deposit/ Cheque remits to RBC
        public IsaInterchangeControlHeaderSourceDed[] isaHeader = new IsaInterchangeControlHeaderSourceDed[1];
        public RqGsFunctionalGroupHeaderSourceDed[] gsHeader = new RqGsFunctionalGroupHeaderSourceDed[1];
        public SourceDeductionsStTransactionSetHeader[] st = new SourceDeductionsStTransactionSetHeader[1];
        //details
        public RqChequeBeginningSegmentForPaymentOrderRemittanceAdvice[] bpr = new RqChequeBeginningSegmentForPaymentOrderRemittanceAdvice[1];
        public ChequeTrace[] trn = new ChequeTrace[1];
        public RqSourceDeductionsReferenceIdentification[] ref1 = new RqSourceDeductionsReferenceIdentification[1];
        public SourceDeductionsReferenceIdentification2[] ref2 = new SourceDeductionsReferenceIdentification2[1];
        public RqRefRe[] ref3 = new RqRefRe[1];
        public ChequeDateTimeReference[] dtm = new ChequeDateTimeReference[1];
        public RqOriginatorNamePayor[] n1pr = new RqOriginatorNamePayor[1];
        public SourceDeductionsPayeeReceiverName[] n1pe = new SourceDeductionsPayeeReceiverName[1];
        public RqN1De[] n1de = new RqN1De[1];

        //these are part of a loopable item.....make construtor take int and declare then
        public RepeatingElements[] repeatingElements;

        public SourceDeductionsSeTransactionSetTrailer[] se = new SourceDeductionsSeTransactionSetTrailer[1];
        public GsFunctionalGroupTrailerSourceDed[] geTrailer = new GsFunctionalGroupTrailerSourceDed[1];
        public IsaInterchangeControlTrailerSourceDed[] ieaTrailer = new IsaInterchangeControlTrailerSourceDed[1];

        public RbcRqRemittanceDetails(int numLoops)
        {
            //override default values with RQ specific values when instantiating
            isaHeader[0] = new IsaInterchangeControlHeaderSourceDed() { InterchangeControlVersionNumber = "00400" };
            gsHeader[0] = new RqGsFunctionalGroupHeaderSourceDed();
            st[0] = new SourceDeductionsStTransactionSetHeader() { TransactionSetControlNumber = "0001" };

            bpr[0] = new RqChequeBeginningSegmentForPaymentOrderRemittanceAdvice();
            trn[0] = new ChequeTrace();
            ref1[0] = new RqSourceDeductionsReferenceIdentification();
            ref2[0] = new SourceDeductionsReferenceIdentification2() { ReferenceNumber = "0440000A" };
            ref3[0] = new RqRefRe();
            dtm[0] = new ChequeDateTimeReference();
            n1pr[0] = new RqOriginatorNamePayor();
            n1pe[0] = new SourceDeductionsPayeeReceiverName() { Name = "MINISTERE DES FINANCES" };
            n1de[0] = new RqN1De();

            //create repeating elements
            repeatingElements = new RepeatingElements[numLoops];
            for (int i = 0; i < numLoops; i++)
                repeatingElements[i] = new RepeatingElements();

            se[0] = new SourceDeductionsSeTransactionSetTrailer() { NumberOfIncludedSegments = "17", TransactionSetControlNumber = "0001" };
            geTrailer[0] = new GsFunctionalGroupTrailerSourceDed();
            ieaTrailer[0] = new IsaInterchangeControlTrailerSourceDed();
        }
    }
    public class RepeatingElements
    {
        public RqEntity[] ent = new RqEntity[1];
        public RqRemittanceSegment[] rmrImpo = new RqRemittanceSegment[1];
        public RqRemittanceSegment[] rmrRrq = new RqRemittanceSegment[1];
        public RqRemittanceSegment[] rmrRamq = new RqRemittanceSegment[1];
        public RqRemittanceSegment[] rmrRqap = new RqRemittanceSegment[1];
        public RqRemittanceSegment[] rmrCsst = new RqRemittanceSegment[1];

        public RepeatingElements()
        {
            ent[0] = new RqEntity();
            rmrImpo[0] = new RqRemittanceSegment() { RemitType = "IMPO" };
            rmrRrq[0] = new RqRemittanceSegment() { RemitType = "RRQ" };
            rmrRamq[0] = new RqRemittanceSegment() { RemitType = "RAMQ" };
            rmrRqap[0] = new RqRemittanceSegment() { RemitType = "RQAP" };
            rmrCsst[0] = new RqRemittanceSegment() { RemitType = "CSST" };
        }
    }
    #region RQ only classes

    [DelimitedRecord("*")]
    public class RqGsFunctionalGroupHeaderSourceDed //had to make seperate new class because this uses a yyyyMMdd format and HHmm.  The other similar do one or the other.
    {
        public string IdElement;
        public string FunctionalIdentifierCode;
        public string SenderInterchangeID;
        public string ApplicationReceiversCode;
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime Date;
        [FieldConverter(ConverterKind.Date, "HHmm")]
        public DateTime Time;
        public string GroupControlNumber;
        public string ResponsibleAgencyCode;
        public string VersionReleaseIndustryIdentifierCode;

        public RqGsFunctionalGroupHeaderSourceDed()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "GS";
            FunctionalIdentifierCode = "RA";
            SenderInterchangeID = "";                   //populated from code system 
            ApplicationReceiversCode = "";              //populated from code system 
            Date = DateTime.Now;
            Time = DateTime.Now;
            GroupControlNumber = "";                    //sequence from db, It must be unique per file.  Does NOT need to be sequential.  4 digits max, no leading 0.
            ResponsibleAgencyCode = "X";
            VersionReleaseIndustryIdentifierCode = "004010";
        }
    }
    [DelimitedRecord("*")]
    public class RqChequeBeginningSegmentForPaymentOrderRemittanceAdvice
    {
        public string IdElement;
        public string TransactionHandlingCode;
        public string MonetaryAmount;
        public string CreditDebitFlagCode;
        public string PaymentMethodCode;
        public string EmptySpot1;
        public string OriginatorIdNumberQualifier;
        public string OriginatorDepositoryFinancialInstitutionIdentificationNumber;
        public string EmptySpot2;
        public string OriginatorAccountNumber;
        public string EmptySpot3;
        public string EmptySpot4;
        public string FixedValueCdnBank;
        public string RqFiAndTransitNumber;
        public string EmptySpot5;
        public string RqBankAccountNumber;
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime DepositDate;

        public RqChequeBeginningSegmentForPaymentOrderRemittanceAdvice()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "BPR";
            TransactionHandlingCode = "C";
            MonetaryAmount = "";                                                //Total value of all remittances (RMR) in this file
            CreditDebitFlagCode = "C";
            PaymentMethodCode = "ZZZ";
            EmptySpot1 = "";
            OriginatorIdNumberQualifier = "04";
            OriginatorDepositoryFinancialInstitutionIdentificationNumber = "";  //populated from code system (bank code (4 digit) + Branch #.  Our trust account FI + transit Originator)
            EmptySpot2 = "";
            OriginatorAccountNumber = "";                                       //populated from code system (Our trust account number)
            EmptySpot3 = "";
            EmptySpot4 = "";
            FixedValueCdnBank = "04";
            RqFiAndTransitNumber = "";                                          //populated from code system (revenu quebec fi and transit number)
            EmptySpot5 = "";
            RqBankAccountNumber = "";                                           //populated from code system (revenu quebec bank account number)
            DepositDate = DateTime.Now;
        }
    }

    [DelimitedRecord("*")]
    public class RqSourceDeductionsReferenceIdentification
    {
        public string IdElement;
        public string ReferenceNumberQualifier;
        public string ReferenceNumber;
        public string DatePlusTransNums;

        public RqSourceDeductionsReferenceIdentification()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "REF";
            ReferenceNumberQualifier = "TN";
            ReferenceNumber = "1";
            DatePlusTransNums = "";                         //YYYYMM9999 = YYYYMM = payment period, 9999 = # of remittance advices in txn
                                                            //YYYY = year for remittance, MM = month for remittance, 0001 = fixed value, always sending 1 remittance advice (one ENT)
        }
    }

    [DelimitedRecord("*")]
    public class RqRefRe
    {
        public string IdElement;
        public string ReferenceNumberQualifier;
        public string ReferenceNumber;

        public RqRefRe()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "REF";
            ReferenceNumberQualifier = "RE";
            ReferenceNumber = "00604";
        }
    }

    [DelimitedRecord("*")]
    public class RqOriginatorNamePayor
    {
        public string IdElement;
        public string EntityIdentifierCode;
        public string PartnerName;
        public string FixedValue;
        public string PartnerId;

        public RqOriginatorNamePayor()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "N1";
            EntityIdentifierCode = "PR";
            PartnerName = "";               //populated from code system
            FixedValue = "46";
            PartnerId = "";                 //populated from code system
        }
    }

    [DelimitedRecord("*")]
    public class RqN1De
    {
        public string FixedValue1;
        public string FixedValue2;
        public string FixedValue3;
        public string FixedValue4;
        public string FixedValue5;

        public RqN1De()
        {
            Clear();
        }

        protected void Clear()
        {
            FixedValue1 = "N1";
            FixedValue2 = "DE";
            FixedValue3 = "MRQ-RAS";
            FixedValue4 = "ZZ";
            FixedValue5 = "0440000A";
        }
    }

    [DelimitedRecord("*")]
    public class RqEntity
    {
        public string IdElement;
        public string EmptySpot1;
        public string FixedValue1;
        public string FixedValue2;
        public string EmployersIdNumber;
        public string EmptySpot2;
        public string EmptySpot3;
        public string EmptySpot4;
        public string EmptySpot5;
        public RqEntity()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "ENT";
            EmptySpot1 = "";
            FixedValue1 = "PR";
            FixedValue2 = "ZZ";
            EmployersIdNumber = ""; //MMMMMMMMMMCC9999SSFFF
                                    //MMMMMMMMMMCC9999 = quebec tax number from code_system
                                    //SS => 01 = week 1 remit, 02 = week 2 remit, 03 = week 3 remit, 04 = week 4 remit, FFF = form code = 232"
            EmptySpot2 = "";
            EmptySpot3 = "";
            EmptySpot4 = "";
            EmptySpot5 = "";
        }
    }

    [DelimitedRecord("*")]
    public class RqRemittanceSegment
    {
        public string IdElement;
        public string FixedValue1;
        public string RemitType;
        public string EmptySpot1;
        public string MonetaryAmount;

        public RqRemittanceSegment()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "RMR";
            FixedValue1 = "ZZ";
            RemitType = "";
            EmptySpot1 = "";
            MonetaryAmount = "";
        }
    }

    #endregion
}
