﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [DelimitedRecord("#")]
    public class CitiEftRecord
    {
        public string Filler1;
        public string CountryCode;
        public string PayMethod;
        public string CurrentDate1;
        public string Filler2;
        public string PaymentCurrency;
        public string PaymentAmount;
        public string Filler3;
        public string DebitAccountNumber;
        public string Filler4;
        public string CurrentDate2;
        public string Filler5;
        public string TransRefNumber;
        public string CrossRefNum;
        public string Filler6;
        public string TransactionType;
        public string Filler7;
        public string CompanyShortName;
        public string CompanyLongName;
        public string Filler8;
        public string EmployeeEftAccount;
        public string EmployeeName;
        public string Filler9;
        public string EmployeeTransitIntNumber;
        public string Filler10;

        public CitiEftRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            Filler1 = "";
            CountryCode = "CA";
            PayMethod = "ACH";
            CurrentDate1 = "";
            Filler2 = "###";
            PaymentCurrency = "CAD";
            PaymentAmount = "";
            Filler3 = "";
            DebitAccountNumber = "";
            Filler4 = "#########";
            CurrentDate2 = "";
            Filler5 = "";
            TransRefNumber = "";
            CrossRefNum = "CROSSREF";
            Filler6 = "##";
            TransactionType = "200";
            Filler7 = "#####";
            CompanyShortName = "";
            CompanyLongName = "";
            Filler8 = "####";
            EmployeeEftAccount = "";
            EmployeeName = "";
            Filler9 = "####";
            EmployeeTransitIntNumber = "";
            Filler10 = "##################################";
        }
    }
}
