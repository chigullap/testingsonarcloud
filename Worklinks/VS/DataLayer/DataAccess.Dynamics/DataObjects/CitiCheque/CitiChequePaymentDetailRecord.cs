﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CitiCheque
{
    [FixedLengthRecord()]
    public class CitiChequePaymentDetailRecord
    {
        [FieldFixedLength(3)]
        public string PaymentDetailIndicator;

        [FieldFixedLength(5)]
        public string FormCode;

        [FieldFixedLength(5)]
        public string SpecialHandlingCode;

        [FieldFixedLength(2)]
        public string PaymentType;

        [FieldFixedLength(3)]
        public string PaymentMethodCode;

        [FieldFixedLength(3)]
        public string PaymentFormat;

        [FieldFixedLength(10)]
        public string ReturnAddressLogoSignatureCodes;

        [FieldFixedLength(13)]
        public string PaymentAmount;

        [FieldFixedLength(8)]
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime PaymentEffectiveDate;

        [FieldFixedLength(10)]
        public string CheckTransactionNumber;

        [FieldFixedLength(9)]
        public string ODFIABANumber;

        [FieldFixedLength(17)]
        public string OriginatorPayerAccountNumber;

        [FieldFixedLength(35)]
        public string ReceiverPayeeName;

        [FieldFixedLength(2)]
        public string ReceivingBankIdNumberQualifier;

        [FieldFixedLength(9)]
        public string RDFITransitRoutingNumber;

        [FieldFixedLength(2)]
        public string AccountNumberQualifier;

        [FieldFixedLength(17)]
        public string ReceiverPayeeAccountNumber;

        [FieldFixedLength(16)]
        public string VendorCode;

        [FieldFixedLength(12)]
        public string OptionalField1;

        [FieldFixedLength(18)]
        public string OptionalField2;

        [FieldFixedLength(3)]
        public string Currency;

        [FieldFixedLength(30)]
        public string AdditionalDataForIssuanceOnly;

        [FieldFixedLength(18)]
        public string Filler1;

        public CitiChequePaymentDetailRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            PaymentDetailIndicator = "300";
            FormCode = "BMS01";
            SpecialHandlingCode = "".PadRight(5);
            PaymentType = "01";                                     //01 - Checks, 02 - ACH
            PaymentMethodCode = "CHK";                              //CHK - Checks, ACH - EFT
            PaymentFormat = "".PadRight(3);                         //ACH ONLY:  values are CTX, PPD, CCD
            ReturnAddressLogoSignatureCodes = "".PadRight(10);
            PaymentAmount = "".PadLeft(13, '0');
            PaymentEffectiveDate = DateTime.Now;
            CheckTransactionNumber = "".PadRight(10);               //uniqueid for each transaction
            ODFIABANumber = "020012328";                            //'031100209' for DE, '020012328' for CAD
            OriginatorPayerAccountNumber = "".PadRight(17);
            ReceiverPayeeName = "".PadRight(35);
            ReceivingBankIdNumberQualifier = "".PadRight(2);        //ACH ONLY:  01 for ABA number
            RDFITransitRoutingNumber = "".PadRight(9);              //ACH ONLY
            AccountNumberQualifier = "".PadRight(2);                //ACH ONLY:  SG for savings, DA for demand deposit (checking)
            ReceiverPayeeAccountNumber = "".PadRight(17);           //ACH ONLY
            VendorCode = "".PadRight(16);
            OptionalField1 = "".PadRight(12);
            OptionalField2 = "".PadRight(18);
            Currency = "CAD";
            AdditionalDataForIssuanceOnly = "".PadRight(30);
            Filler1 = "".PadRight(18);
        }
    }
}
