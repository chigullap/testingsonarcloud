﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CitiCheque
{
    [FixedLengthRecord()]
    public class CitiChequeBatchTrailerRecord
    {
        [FieldFixedLength(3)]
        public string BatchTrailerIndicator;

        [FieldFixedLength(9)]
        public string BatchControlNumber;

        [FieldFixedLength(9)]
        public string NumberOfPaymentTransInBatch;

        [FieldFixedLength(13)]
        public string DollarValueOfPaymentsInBatch;

        [FieldFixedLength(9)]
        public string NumberOfRecordsInBatch;

        [FieldFixedLength(207)]
        public string Filler;

        public CitiChequeBatchTrailerRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            BatchTrailerIndicator = "400";
            BatchControlNumber = "".PadRight(9);
            NumberOfPaymentTransInBatch = "".PadLeft(9, '0');
            DollarValueOfPaymentsInBatch = "".PadLeft(13, '0');
            NumberOfRecordsInBatch = "".PadLeft(9, '0');
            Filler = "".PadRight(207);
        }
    }
}
