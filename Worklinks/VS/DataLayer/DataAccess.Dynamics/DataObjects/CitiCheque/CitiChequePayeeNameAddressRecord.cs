﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CitiCheque
{
    [FixedLengthRecord()]
    public class CitiChequePayeeNameAddressRecord
    {
        [FieldFixedLength(3)]
        public string AddressTypeIndicator;

        [FieldFixedLength(2)]
        public string NameAddressQualifier;

        [FieldFixedLength(40)]
        public string PayeeName;

        [FieldFixedLength(40)]
        public string PayeeAddress1;

        [FieldFixedLength(40)]
        public string PayeeAddress2;

        [FieldFixedLength(40)]
        public string PayeeAddress3;

        [FieldFixedLength(40)]
        public string PayeeAddress4;

        [FieldFixedLength(1)]
        public string GeographicQualifier;

        [FieldFixedLength(19)]
        public string PayeeCity;

        [FieldFixedLength(2)]
        public string PayeeProvStateCode;

        [FieldFixedLength(9)]
        public string PayeeZipCode;

        [FieldFixedLength(2)]
        public string CountryCode;

        [FieldFixedLength(12)]
        public string Filler2;

        public CitiChequePayeeNameAddressRecord()              //can have up to 3 of these records
        {
            Clear();
        }

        protected void Clear()
        {
            AddressTypeIndicator = "301";
            NameAddressQualifier = "PE";                //PE, RL, BH  (Payee, Mail-To, On Behalf Of)
            PayeeName = "".PadRight(40);
            PayeeAddress1 = "".PadRight(40);
            PayeeAddress2 = "".PadRight(40);
            PayeeAddress3 = "".PadRight(40);
            PayeeAddress4 = "".PadRight(40);
            GeographicQualifier = "A";
            PayeeCity = "".PadRight(19);                //if field8 = A, populate this field
            PayeeProvStateCode = "".PadRight(2);        //if field8 = A, populate this field
            PayeeZipCode = "".PadRight(9);              //if field8 = A, populate this field
            CountryCode = "CA";                         //use ISO country code table
            Filler2 = "".PadRight(12);
        }
    }
}
