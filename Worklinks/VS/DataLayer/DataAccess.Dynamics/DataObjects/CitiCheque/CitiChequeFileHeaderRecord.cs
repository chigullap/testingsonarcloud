﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CitiCheque
{
    [FixedLengthRecord()]
    public class CitiChequeFileHeaderRecord
    {
        [FieldFixedLength(3)]
        public string FileHeaderIndicator;

        [FieldFixedLength(15)]
        public string OriginatorId;

        [FieldFixedLength(15)]
        public string ReceiverId;

        [FieldFixedLength(9)]
        public string FileHeaderControlNumber;

        [FieldFixedLength(8)]
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime FileCreationDate;

        [FieldFixedLength(200)]
        public string Filler1;

        public CitiChequeFileHeaderRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            FileHeaderIndicator = "100";
            OriginatorId = "";
            ReceiverId = "CITIBANKDEL-EDI";
            FileHeaderControlNumber = "".PadLeft(9, '0');    //NOTE:  assigned number originated and maintained by sender to keep track of the number of interchanges transmitted to Citibank.  Default is zeros.
            FileCreationDate = DateTime.Now;
            Filler1 = "".PadRight(200);
        }
    }
}
