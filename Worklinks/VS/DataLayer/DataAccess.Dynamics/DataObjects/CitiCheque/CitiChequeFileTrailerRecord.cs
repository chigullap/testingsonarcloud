﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CitiCheque
{
    [FixedLengthRecord()]
    public class CitiChequeFileTrailerRecord
    {
        [FieldFixedLength(3)]
        public string FileTrailerIndicator;

        [FieldFixedLength(9)]
        public string FileTrailerControlNumber;

        [FieldFixedLength(9)]
        public string NumberOfRecordsInFile;

        [FieldFixedLength(15)]
        public string DollarValueOfPaymentsInFile;

        [FieldFixedLength(9)]
        public string NumberOfBatchesInFile;

        [FieldFixedLength(9)]
        public string NumberOfPaymentsInFile;

        [FieldFixedLength(196)]
        public string Filler;

        public CitiChequeFileTrailerRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            FileTrailerIndicator = "500";
            FileTrailerControlNumber = "".PadRight(9);              //same value as CitiAchFileHeaderRecord.FileHeaderControlNumber
            NumberOfRecordsInFile = "2".PadLeft(9, '0');            //file header thru file trailer
            DollarValueOfPaymentsInFile = "".PadLeft(15, '0');
            NumberOfBatchesInFile = "".PadLeft(9, '0');
            NumberOfPaymentsInFile = "0".PadLeft(9, '0');
            Filler = "".PadRight(196);
        }
    }
}
