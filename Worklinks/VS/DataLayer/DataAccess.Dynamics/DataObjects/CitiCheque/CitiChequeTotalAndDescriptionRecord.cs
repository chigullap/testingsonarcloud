﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CitiCheque
{
    [FixedLengthRecord()]
    public class CitiChequeTotalAndDescriptionRecord
    {
        [FieldFixedLength(3)]
        public string TotalIndicator;

        [FieldFixedLength(10)]
        public string TotalGrossAmount;

        [FieldFixedLength(10)]
        public string TotalDiscountAmount;

        [FieldFixedLength(10)]
        public string TotalNetAmount;

        [FieldFixedLength(80)]
        public string MessageLine1;

        [FieldFixedLength(80)]
        public string MessageLine2;

        [FieldFixedLength(57)]
        public string Filler;

        public CitiChequeTotalAndDescriptionRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            TotalIndicator = "305";
            TotalGrossAmount = "".PadLeft(10, '0');
            TotalDiscountAmount = "".PadLeft(10, '0');
            TotalNetAmount = "".PadLeft(10, '0');
            MessageLine1 = "".PadRight(80);
            MessageLine2 = "".PadRight(80);
            Filler = "".PadRight(57);
        }
    }
}
