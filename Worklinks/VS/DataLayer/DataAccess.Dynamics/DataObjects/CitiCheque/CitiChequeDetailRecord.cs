﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CitiCheque
{
    [FixedLengthRecord()]
    public class CitiChequeDetailRecord
    {
        [FieldFixedLength(3)]
        public string DetailIndicator;

        [FieldFixedLength(20)]
        public string InvoiceNumber;

        [FieldFixedLength(8)]
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime InvoiceDate;

        [FieldFixedLength(35)]
        public string Description;

        [FieldFixedLength(10)]
        public string GrossAmount;

        [FieldFixedLength(1)]
        public string GrossDebitCreditFlag;

        [FieldFixedLength(10)]
        public string DiscountAmount;

        [FieldFixedLength(1)]
        public string DiscountDebitCreditFlag;

        [FieldFixedLength(10)]
        public string NetAmount;

        [FieldFixedLength(1)]
        public string NetDebitCreditFlag;

        [FieldFixedLength(15)]
        public string PoNumber;

        [FieldFixedLength(15)]
        public string VoucherNumber;

        [FieldFixedLength(12)]
        public string Reference1;

        [FieldFixedLength(18)]
        public string Reference2;

        [FieldFixedLength(91)]
        public string Filler1;

        public CitiChequeDetailRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            DetailIndicator = "303";
            InvoiceNumber = "".PadRight(20);                //optional
            InvoiceDate = DateTime.Now;
            Description = "".PadRight(35);                  //optional
            GrossAmount = "0000000000";                     //optional
            GrossDebitCreditFlag = " ";
            DiscountAmount = "0000000000";                  //optional
            DiscountDebitCreditFlag = " ";
            NetAmount = "0000000000";
            NetDebitCreditFlag = " ";
            PoNumber = "".PadRight(15);                     //optional
            VoucherNumber = "".PadRight(15);                //optional
            Reference1 = "".PadRight(12);                   //optional
            Reference2 = "".PadRight(18);                   //optional
            Filler1 = "".PadRight(91);
        }
    }
}
