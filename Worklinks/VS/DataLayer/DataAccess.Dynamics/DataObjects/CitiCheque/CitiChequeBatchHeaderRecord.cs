﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CitiCheque
{
    [FixedLengthRecord()]
    public class CitiChequeBatchHeaderRecord
    {
        [FieldFixedLength(3)]
        public string BatchHeaderIndicator;

        [FieldFixedLength(9)]
        public string BatchControlNumber;

        [FieldFixedLength(15)]
        public string BatchSenderId;

        [FieldFixedLength(15)]
        public string BatchReceiverId;

        [FieldFixedLength(8)]
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime BatchCreationDate;

        [FieldFixedLength(4)]
        public string BatchCreationTime;

        [FieldFixedLength(16)]
        public string CompanyName;

        [FieldFixedLength(10)]
        public string CompanyId;

        [FieldFixedLength(170)]
        public string Filler1;

        public CitiChequeBatchHeaderRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            BatchHeaderIndicator = "200";
            BatchControlNumber = "".PadLeft(9, '0');//NOTE:  assigned number originated and maintained by sender to keep track of the number of interchanges transmitted to Citibank.  Default is zeros.
            BatchSenderId = "BMSCAD-CPC".PadRight(15);
            BatchReceiverId = "BMSCAD-CHK".PadRight(15);
            BatchCreationDate = DateTime.Now;
            BatchCreationTime = DateTime.Now.ToString("HHmm");   //assuming 24 hour
            CompanyName = "".PadRight(16);
            CompanyId = "".PadRight(10);                        //blank as per email
            Filler1 = "".PadRight(170);
        }
    }
}
