﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [FixedLengthRecord()]
    public class FileControlRecord
    {
        [FieldFixedLength(1)]
        public int RecordType;

        [FieldFixedLength(6)]
        public string BatchHeaderBlockCount;

        [FieldFixedLength(6)]
        public string PhysicalBlockCount;

        [FieldFixedLength(8)]
        public string EntryAddendaCount;

        [FieldFixedLength(10)]
        public string Filler8;

        [FieldFixedLength(12)]
        public string TotalDebitDollarAmount;

        [FieldFixedLength(12)]
        public string TotalCreditDollarAmount;                  //in eft file, this is a numeric...string here so we can pad with "0's"

        [FieldFixedLength(39)]
        public string Filler9;

        public FileControlRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = 9;
            BatchHeaderBlockCount = "000000";                   //total number of batch header records (type 5) in file
            PhysicalBlockCount = "000000";                      //total number of all physical blocks in the file
            EntryAddendaCount="00000000";                       //total number of detail (type 6) PLUS addenda record (type 7) in the file
            Filler8 = "0000000000";                             //zero fill
            TotalDebitDollarAmount = "000000000000";            //zero fill
            TotalCreditDollarAmount = "000000000000";           //total number of detail credit records.  Format $$$$$$$$$$cc
            Filler9 = "                                       ";//blanks
        }
    }
}
