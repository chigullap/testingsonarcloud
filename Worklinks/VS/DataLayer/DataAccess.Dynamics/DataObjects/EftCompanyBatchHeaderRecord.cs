﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [FixedLengthRecord()]
    public class EftCompanyBatchHeaderRecord
    {

        [FieldFixedLength(1)]
        public int RecordType;

        [FieldFixedLength(3)]
        public int ServiceIdentifier;

        [FieldFixedLength(16)]
        public string ClientShortName;

        [FieldFixedLength(1)]
        public string Filler3;

        [FieldFixedLength(2)]
        public string ForeignExchangeInd;

        [FieldFixedLength(3)]
        public string OriginationCurrency;

        [FieldFixedLength(2)]
        public string OriginationCountry;

        [FieldFixedLength(3)]
        public string DestinationCurrency;

        [FieldFixedLength(9)]
        public string ForeignExchangeRate; //in eft file, this is a numeric...string here so we can keep the "0's" if we pad it

        [FieldFixedLength(10)]
        public string ClientNumber;

        [FieldFixedLength(3)]
        public string StandardEntryClassCode;

        [FieldFixedLength(10)]
        public string CompanyEntryDescription;

        [FieldFixedLength(6)]
        public string Filler4;

        [FieldFixedLength(6)]
        [FieldConverter(ConverterKind.Date, "yyMMdd")]
        public DateTime DueDate;

        [FieldFixedLength(3)]
        public string Filler5;

        [FieldFixedLength(1)]
        public string OriginatorStatusCode;

        [FieldFixedLength(8)]
        public string FinancialInstitution;

        [FieldFixedLength(7)]
        public string BatchNumber;//in eft file, this is a numeric...string here so we can keep the "0's" if we pad it

        public EftCompanyBatchHeaderRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = 5;
            ServiceIdentifier = 220;
            ClientShortName = "";                   //retrieved from code_system
            Filler3 = " ";
            ForeignExchangeInd = "  ";              //(FV=fixed to variable, VF=variable to fixed, FF=fixed to fixed, or leave blank)
            OriginationCurrency = "CAD";            //(USD or CAD or leave blank)
            OriginationCountry ="CA";               //(CA or leave blank)
            DestinationCurrency = "CAD";            //(USD or CAD or leave blank)
            ForeignExchangeRate = "         ";      //(max 9 digits including decimal or leave blank)
            ClientNumber = "";                      //retrieved from code_system ...6 rbc digits, 4 client for subsid (0's for no subsid)
            StandardEntryClassCode = "PPD";         //(PPD Personal payments, CCD corporate payments)
            CompanyEntryDescription = "Payroll   "; //(for cdn banks, this will be in the "electronic message" field. If left blank, default will be ACH DR or ACH CR (10 chars)
            Filler4 = "      ";
            DueDate = DateTime.Now;                 //for CDN credits, can be 30 days in past or 173 days in future.  Default to today as this cannot be null to use the converter above.
            Filler5 = "   ";
            OriginatorStatusCode = "1";
            FinancialInstitution = "        ";      //(blanks or ignored)
            BatchNumber = "";                       //(must be sequential within file, 7 chars)
        }

    }
}