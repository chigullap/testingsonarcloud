﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [FixedLengthRecord()]
    public class EftHeader
    {
        [FieldFixedLength(1)]
        public int RecordType;

        [FieldFixedLength(2)]
        public string PriorityCode; //in eft file, this is a numeric of "01"...string here so we can keep the "0"

        [FieldFixedLength(10)]
        public string Filler1;

        [FieldFixedLength(10)]
        public string ClientNumber;

        [FieldFixedLength(6)]
        [FieldConverter(ConverterKind.Date, "yyMMdd")]   
        public DateTime FileCreationDate;

        [FieldFixedLength(4)]
        public string FileCreationNumber;

        [FieldFixedLength(1)]
        public string FileIdModifier;

        [FieldFixedLength(3)]
        public string RecordSize; //in eft file, this is a numeric of "01"...string here so we can keep the "0"

        [FieldFixedLength(2)]
        public int BlockingFactor;

        [FieldFixedLength(1)]
        public int FormatCode;

        [FieldFixedLength(23)]
        public string DestinationBank;

        [FieldFixedLength(23)]
        public string CompanyName;

        [FieldFixedLength(8)]
        public string Filler2;
        
        
        public EftHeader()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = 1;
            PriorityCode = "01";
            Filler1 = "          ";
            ClientNumber = "";                  //retrieved from code_system table...6 rbc digits, 4 client for subsid (0's for no subsid)
            FileCreationDate = DateTime.Now;
            FileCreationNumber = "TEST";        //failsafe - consider this a test unless a numeric is supplied.  Value will be 4 digits of payroll process id which is passed in
            FileIdModifier = "A";               //default - will be a process later to fill this
            RecordSize = "094";
            BlockingFactor = 10;
            FormatCode = 1;
            DestinationBank = "RBC Royal Bank";
            CompanyName = "Worklinks Inc          ";
            Filler1 = "        ";
        }
    }
}
