﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [FixedLengthRecord()]
    public class CSBBatchHeaderRecord
    {
        [FieldFixedLength(2)]
        public int RecordType;

        [FieldFixedLength(5)]
        public string OrganizationId;

        [FieldFixedLength(10)]
        [FieldConverter(ConverterKind.Date, "yyyy-MM-dd")]
        public DateTime EffectiveDate;

        [FieldFixedLength(30)]
        public string ReferenceId;

        [FieldFixedLength(2)]
        public string EmailFaxIndicator;

        [FieldFixedLength(40)]
        public string EmailFaxAddress;

        [FieldFixedLength(31)]
        public string Filler;

        public CSBBatchHeaderRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = 20;
            OrganizationId = null;
            EffectiveDate = DateTime.Now;
            ReferenceId = null;
            EmailFaxIndicator = null;
            EmailFaxAddress = null;                 /* doc states "Please enter the fax number as required to send a fax FROM the Ottawa (613) area to the employer's location.
                                                        for example, if it is long distance from Ottawa to an employer's location in the 519 area, the number to enter is 15195551212; for local 6135551212 */
            Filler = "                              x"; //doc states "after the last field a filler is needed.  Put a char in the 120th position to ensure no compression occurs during movement between platforms"
        }
    }
}