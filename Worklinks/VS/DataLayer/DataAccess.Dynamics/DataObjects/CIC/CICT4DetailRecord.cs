﻿using System;
using FileHelpers;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CIC
{
    [FixedLengthRecord()]
    public class CICT4DetailRecord
    {
        [FieldFixedLength(4)]
        public string TaxYear;

        [FieldFixedLength(36)]
        public string UniqueEmployeeIdentifier;

        [FieldFixedLength(15)]
        public string AuthenticationValuePrimary;                       //typically SIN or Employee #.  USING SIN

        [FieldFixedLength(15)]
        [FieldConverter(ConverterKind.Date, "MM-dd-yyyy")]
        public DateTime AuthenticationValueSecondary;                   //Typically birthdate in MM-dd-yyyy format, Employee #, or first three letters of last name.  USING BIRTHDATE.

        [FieldFixedLength(9)]
        public string EmployeeSocialInsuranceNumber;                    //do not include dashes

        [FieldFixedLength(20)]
        public string EmployeeFirstName;

        [FieldFixedLength(1)]
        public string EmployeeMiddleInitial;

        [FieldFixedLength(25)]
        public string EmployeeLastName;

        [FieldFixedLength(4)]
        public string EmployeeSuffix;

        [FieldFixedLength(30)]
        public string EmployeeLocationAddress; //Line 1

        [FieldFixedLength(30)]
        public string EmployeeDeliveryAddress; //Line 2

        [FieldFixedLength(25)]
        public string EmployeeCity;

        [FieldFixedLength(2)]
        public string EmployeeProvinceState;

        [FieldFixedLength(10)]
        public string EmployeePostalCode;

        [FieldFixedLength(40)]
        public string EmployeeCountry;

        [FieldFixedLength(40)]
        public string EmployerNameLine1;

        [FieldFixedLength(40)]
        public string EmployerNameLine2;

        [FieldFixedLength(40)]
        public string AdditionalLineOfEmployerNameInformation;

        [FieldFixedLength(40)]
        public string EmployerLocationAddress; //Line 1

        [FieldFixedLength(40)]
        public string EmployerDeliveryAddress; //Line 2

        [FieldFixedLength(25)]
        public string EmployerCity;

        [FieldFixedLength(2)]
        public string EmployerProvinceState;

        [FieldFixedLength(10)]
        public string EmployerPostalCode;

        [FieldFixedLength(40)]
        public string EmployerCountry;

        [FieldFixedLength(1)]
        public int Box28CppQppExemptFlag;

        [FieldFixedLength(1)]
        public int Box28EiExemptFlag;

        [FieldFixedLength(1)]
        public int Box28PpipExemptFlag;

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public string Box14EmploymentIncome;                            //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public string Box22IncomeTaxDeducted;                           //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(15)]
        public string Box54PayrollAccountNumber;

        [FieldFixedLength(2)]
        public string Box10ProvinceOfEmploymentCode;
        
        [FieldFixedLength(2)]
        public string Box29EmploymentCode;

        [FieldFixedLength(6)]
        [FieldAlign(AlignMode.Right, '0')]
        public string Box16EmployeesCppContributions;                   //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(7)]
        [FieldAlign(AlignMode.Right, '0')]
        public string Box24EiInsurableEarnings;                         //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(6)]
        [FieldAlign(AlignMode.Right, '0')]
        public string Box17EmployeesQppContributions;                   //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string Box26CppQppPensionableEarnings;                   //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(6)]
        [FieldAlign(AlignMode.Right, '0')]
        public string Box18EmployeesEiPremium;                          //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string Box44UnionDues;                                   //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(7)]
        [FieldAlign(AlignMode.Right, '0')]
        public string Box20RppContributions;                            //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string Box46CharitableDonations;                         //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(7)]
        [FieldAlign(AlignMode.Right, '0')]
        public string Box52PensionAdjustment;                           //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(7)]
        public string Box50RppOrDpspRegistrationNumber;

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public string Box55EmployeesPpipPremiums;                       //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public string Box56PpipInsurableEarnings;                       //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(2)]
        public string OtherBox1Code;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox1Amount;                                  //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(2)]
        public string OtherBox2Code;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox2Amount;                                  //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(2)]
        public string OtherBox3Code;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox3Amount;                                  //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(2)]
        public string OtherBox4Code;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox4Amount;                                  //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(2)]
        public string OtherBox5Code;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox5Amount;                                  //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(2)]
        public string OtherBox6Code;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox6Amount;                                  //right justified, 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(1)]
        public string ActiveTerminatedEmployee;                         //A = Active, T = Terminated (Must be "A" or "T")
        
        //other fields optional - not including here
        public CICT4DetailRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            TaxYear = "";
            UniqueEmployeeIdentifier = "";
            AuthenticationValuePrimary = "";
            AuthenticationValueSecondary = DateTime.Now;
            EmployeeSocialInsuranceNumber = "";
            EmployeeFirstName = "";
            EmployeeMiddleInitial = " ";
            EmployeeLastName = "";
            EmployeeSuffix = "";
            EmployeeLocationAddress = "";
            EmployeeDeliveryAddress = "";
            EmployeeCity = "";
            EmployeeProvinceState = "";
            EmployeePostalCode = "";
            EmployeeCountry = ""; //if blank, Canada is assumed
            EmployerNameLine1 = "";
            EmployerNameLine2 = "";
            AdditionalLineOfEmployerNameInformation = "";
            EmployerLocationAddress = "";
            EmployerDeliveryAddress = "";
            EmployerCity = "";
            EmployerProvinceState = "";
            EmployerPostalCode = "";
            EmployerCountry = "";  //if blank, Canada is assumed
            Box28CppQppExemptFlag = 0;
            Box28EiExemptFlag = 0;
            Box28PpipExemptFlag = 0;
            Box14EmploymentIncome = "";
            Box22IncomeTaxDeducted = "";
            Box54PayrollAccountNumber = "";
            Box10ProvinceOfEmploymentCode = "";
            Box29EmploymentCode = "";
            Box16EmployeesCppContributions = "";
            Box24EiInsurableEarnings = "";
            Box17EmployeesQppContributions = "";
            Box26CppQppPensionableEarnings = "";
            Box18EmployeesEiPremium = "";
            Box44UnionDues = "";
            Box20RppContributions = "";
            Box46CharitableDonations = "";
            Box52PensionAdjustment = "";
            Box50RppOrDpspRegistrationNumber = "";
            Box55EmployeesPpipPremiums = "";
            Box56PpipInsurableEarnings = "";
            OtherBox1Code = "";
            OtherBox1Amount = "";
            OtherBox2Code = "";
            OtherBox2Amount = "";
            OtherBox3Code = "";
            OtherBox3Amount = "";
            OtherBox4Code = "";
            OtherBox4Amount = "";
            OtherBox5Code = "";
            OtherBox5Amount = "";
            OtherBox6Code = "";
            OtherBox6Amount = "";
            ActiveTerminatedEmployee = "A";
            //other fields optional - not including here
        }
    }
}