﻿using System;
using FileHelpers;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CIC
{
    [FixedLengthRecord()]
    public class CICR1DetailRecord
    {
        [FieldFixedLength(4)]
        public string TaxYear;

        [FieldFixedLength(36)]
        public string UniqueEmployeeIdentifier;

        [FieldFixedLength(15)]
        public string AuthenticationValuePrimary;                       //typically SIN or Employee #.  USING SIN

        [FieldFixedLength(15)]
        [FieldConverter(ConverterKind.Date, "MM-dd-yyyy")]
        public DateTime AuthenticationValueSecondary;                   //Typically birthdate in MM-dd-yyyy format, Employee #, or first three letters of last name.  USING BIRTHDATE.

        [FieldFixedLength(9)]
        public string EmployeeSocialInsuranceNumber;

        [FieldFixedLength(20)]
        public string EmployeeFirstName;

        [FieldFixedLength(1)]
        public string EmployeeMiddleInitial;

        [FieldFixedLength(25)]
        public string EmployeeLastName;

        [FieldFixedLength(4)]
        public string EmployeeSuffix;

        [FieldFixedLength(30)]
        public string EmployeeLocationAddress; //Line 1

        [FieldFixedLength(30)]
        public string EmployeeDeliveryAddress; //Line 2

        [FieldFixedLength(25)]
        public string EmployeeCity;

        [FieldFixedLength(2)]
        public string EmployeeProvinceState;

        [FieldFixedLength(10)]
        public string EmployeePostalCode;

        [FieldFixedLength(40)]
        public string EmployeeCountry;

        [FieldFixedLength(40)]
        public string EmployerNameLine1;

        [FieldFixedLength(40)]
        public string EmployerNameLine2;

        [FieldFixedLength(40)]
        public string AdditionalLineOfEmployerNameInformation;

        [FieldFixedLength(40)]
        public string EmployerLocationAddress; //Line 1

        [FieldFixedLength(40)]
        public string EmployerDeliveryAddress; //Line 2

        [FieldFixedLength(25)]
        public string EmployerCity;

        [FieldFixedLength(2)]
        public string EmployerProvinceState;

        [FieldFixedLength(10)]
        public string EmployerPostalCode;

        [FieldFixedLength(40)]
        public string EmployerCountry;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxAEmploymentIncomeBeforeSourceDeductions;       //all fields below are right justified (unless commented), 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxBContributionsToQpp;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxCEmploymentInsurancePremiums;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxDContributionsToRpp;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxEQuebecIncomeTaxWithheldAtSource;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxFUnionAndProfessionalDues;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxGQppPensionEarnings;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxHQpip;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxIInsuranceSalaryAndWagesUnderQpip;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxJContributionsPaidByEmployerUnderPrivateHealthServicesPlan;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxKTripsMadeByAResidentOfADesignatedRemoteArea;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxLOtherBenefits;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxMCommissions;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxNCharitableDonations;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxPContributionsToAMultiEmployerInsurancePlan;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxQDeferredSalaryOrWages;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxRTaxExemptIncomeEarnedOnAReserve;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxSTipsReceivedNotIncludedInBoxT;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxTTipsAllocatedByTheEmployer;   //Amount should be included in Boxes A or B, or R and T for a status indian.

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxUPhasedRetirement;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxVMealsAndAccomodation;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BoxWUseOfMotorVehicleForPersonalPurposes;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string WageEarnerProtectionProgramPayments;                                  //code: CA

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string TaxFreeSavingsAccount;                                                //code: CB

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string PaymentsToRegisteredDisabilitySavingsPlan;                            //code: CC

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BenefitsPaidToParentsOfMissingOrMurderedChildren;                     //code: CD

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string PaymentsMadeUnderSupplementaryUnemploymentBenefitPlan;                //code: RA

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string ScholarshipsBursariesFellowshipsAndPrizes;                            //code: RB

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string ResearchGrantPaidToAnIndividual;                                      //code: RC

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string FeesForServicesRendered;                                              //code: RD

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string IncentiveSubsidyToApprentices;                                        //code: RX

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string LabourAdjustmentBenefits;                                             //code: RG

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string LabourAdjustmentBenefitsForOlderWorkersAndIncomeAssistancePayments;   //code: RH

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string PaymentsMadeUnderPlantWorkersAdjustmentProgramAndNorthernCodAdjustmentAndRecoveryProgram; //code: RI

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string RetiringAllowance;                                                    //code: RJ

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string DeathBenefit;                                                         //code: RK

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string PatronageDividends;                                                   //code: RL

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string CommissionsPaidToASelfEmployedPerson;                                 //code: RM

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BenefitsPaidUnderAWageLossReplacementPlan;                            //code: RN

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BenefitsReceivedByAShareholder;                                       //code: RO

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string BenefitsReceivedByAPartner;                                           //code: RP

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string AmountsAllocatedUnderARetirementCompensationAgreement;                //code: RQ

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string PaymentsForServiceRenderedinQuebecByAPersonNotResidentInCanada;       //code: RR

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string FinancialAssistance;                                                  //code: RS

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherIndemnitiesPaidByEmployerAsResultOfIndustrialAccident;           //code: RT

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string AssistancePaymentsToSchoolUnderRESP;                                  //code: RU

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string AccumulatedIncomePaymentsFromRESP;                                    //code: RV

        [FieldFixedLength(9)]
        public string FacsimileNumber; //Space filled, provided by CICPlus. Sequence number that appears on the printed copy

        [FieldFixedLength(9)]
        public string SequenceNumber; //Sequence number that is used when filing employee data electronically

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string A1EmployeeBenefitPlan;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string A2EmployeeTrust;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string A3RepaymentOfSalaryOrWages;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string B1CppContributions;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string D2ContributionsForServiceBefore1990IncludedInBoxDEmployeeContributor;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string D3ContributionsForServiceBefore1990IncludedInBoxDEmployeeNonContributor;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string D1RetirementCompensationArrangment;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string G1QppTaxableBenefitInKind;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string K1TripsForMedicalCare;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string O2DeductionForPatronageDividends;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string O3RedemptionOfAPreferredShare;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string O4RepaymentOfWageLossReplacementBenefits;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string R1EmploymentIncome;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string A4ChainsawExpenses;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string A5BrushCutterExpenses;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string A7DeductionForCanadianForcesPersonnel;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string L3NonTaxableAllocationForExpensesIncurredAsPartOfJob;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string V1NonTaxableBenefitForBoardAndLodging;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string L2VolunteerCompensationNotIncludedInBoxesAAndL;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string L4BenefitResultingFromADebtContractedToAquireInvestments;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string L5DedutionForAHomeRelocationLoan;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string L7BenefitForPurchaseOptionUponDeath;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string A6RenumerationReceivedByAQuebecSailor;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string A9DeductionForForeignSpecialists;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string A10DedutionForForeignResearchers;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string A11DeductionForForeignResearchersOnPostDoctoralInternship;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string A12DeductionForForeignExperts;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string A13DeductionForForeignProfessors;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string x201ChildCareExpensesFromTheMinistereDeLEmploiDeLaSolidarite;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string A8DeductionsForPoliceOfficers;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string A14ExemptionRate;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string L8SecurityOptionsElection;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string x235ContributionPaidByTheEmployeeToPrivateHealthInsurancePlan;

        [FieldFixedLength(15)]
        public string x200NameOfCurrencyUsed; //left justified, space filled

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string G2ContributorySalarayOrWagesUnderTheCpp;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string x211BenefitRelatedToPreviousEmployment;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string L9DeductionsForStockOptionUnderSection725_2OfTheIncomeTaxAct;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string L10DeductionsForStockOptionUnderSection725_3OfTheIncomeTaxAct;

        [FieldFixedLength(16)]
        public string QcBusinessNumber;

        [FieldFixedLength(10)]
        public string ReferenceNumber;

        [FieldFixedLength(1)]
        public string EmployeeLanguagePreference; //E=English, F=French

        [FieldFixedLength(1)]
        public string ActiveTerminatedEmployee;                         //A = Active, T = Terminated (Must be "A" or "T")

        //other fields optional - not including here

        public CICR1DetailRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            TaxYear = "";
            UniqueEmployeeIdentifier = "";
            AuthenticationValuePrimary = "";
            AuthenticationValueSecondary = DateTime.Now;
            EmployeeSocialInsuranceNumber = "";
            EmployeeFirstName = "";
            EmployeeMiddleInitial = " ";
            EmployeeLastName = "";
            EmployeeSuffix = "";
            EmployeeLocationAddress = "";
            EmployeeDeliveryAddress = "";
            EmployeeCity = "";
            EmployeeProvinceState = "";
            EmployeePostalCode = "";
            EmployeeCountry = ""; //if blank, Canada is assumed
            EmployerNameLine1 = "";
            EmployerNameLine2 = "";
            AdditionalLineOfEmployerNameInformation = "";
            EmployerLocationAddress = "";
            EmployerDeliveryAddress = "";
            EmployerCity = "";
            EmployerProvinceState = "";
            EmployerPostalCode = "";
            EmployerCountry = ""; //if blank, Canada is assumed
            BoxAEmploymentIncomeBeforeSourceDeductions = "";
            BoxBContributionsToQpp = "";
            BoxCEmploymentInsurancePremiums = "";
            BoxDContributionsToRpp = "";
            BoxEQuebecIncomeTaxWithheldAtSource = "";
            BoxFUnionAndProfessionalDues = "";
            BoxGQppPensionEarnings = "";
            BoxHQpip = "";
            BoxIInsuranceSalaryAndWagesUnderQpip = "";
            BoxJContributionsPaidByEmployerUnderPrivateHealthServicesPlan = "";
            BoxKTripsMadeByAResidentOfADesignatedRemoteArea = "";
            BoxLOtherBenefits = "";
            BoxMCommissions = "";
            BoxNCharitableDonations = "";
            BoxPContributionsToAMultiEmployerInsurancePlan = "";
            BoxQDeferredSalaryOrWages = "";
            BoxRTaxExemptIncomeEarnedOnAReserve = "";
            BoxSTipsReceivedNotIncludedInBoxT = "";
            BoxTTipsAllocatedByTheEmployer = "";
            BoxUPhasedRetirement = "";
            BoxVMealsAndAccomodation = "";
            BoxWUseOfMotorVehicleForPersonalPurposes = "";
            WageEarnerProtectionProgramPayments = "";
            TaxFreeSavingsAccount = "";
            PaymentsToRegisteredDisabilitySavingsPlan = "";
            PaymentsMadeUnderSupplementaryUnemploymentBenefitPlan = "";
            ScholarshipsBursariesFellowshipsAndPrizes = "";
            ResearchGrantPaidToAnIndividual = "";
            FeesForServicesRendered = "";
            IncentiveSubsidyToApprentices = "";
            LabourAdjustmentBenefits = "";
            LabourAdjustmentBenefitsForOlderWorkersAndIncomeAssistancePayments = "";
            PaymentsMadeUnderPlantWorkersAdjustmentProgramAndNorthernCodAdjustmentAndRecoveryProgram = "";
            RetiringAllowance = "";
            DeathBenefit = "";
            PatronageDividends = "";
            CommissionsPaidToASelfEmployedPerson = "";
            BenefitsPaidUnderAWageLossReplacementPlan = "";
            BenefitsReceivedByAShareholder = "";
            BenefitsReceivedByAPartner = "";
            AmountsAllocatedUnderARetirementCompensationAgreement = "";
            PaymentsForServiceRenderedinQuebecByAPersonNotResidentInCanada = "";
            FinancialAssistance = "";
            OtherIndemnitiesPaidByEmployerAsResultOfIndustrialAccident = "";
            AssistancePaymentsToSchoolUnderRESP = "";
            AccumulatedIncomePaymentsFromRESP = "";
            FacsimileNumber = "";
            SequenceNumber = "";
            A1EmployeeBenefitPlan = "";
            A2EmployeeTrust = "";
            A3RepaymentOfSalaryOrWages = "";
            B1CppContributions = "";
            D2ContributionsForServiceBefore1990IncludedInBoxDEmployeeContributor = "";
            D3ContributionsForServiceBefore1990IncludedInBoxDEmployeeNonContributor = "";
            D1RetirementCompensationArrangment = "";
            G1QppTaxableBenefitInKind = "";
            K1TripsForMedicalCare = "";
            O2DeductionForPatronageDividends = "";
            O3RedemptionOfAPreferredShare = "";
            O4RepaymentOfWageLossReplacementBenefits = "";
            R1EmploymentIncome = "";
            A4ChainsawExpenses = "";
            A5BrushCutterExpenses = "";
            A7DeductionForCanadianForcesPersonnel = "";
            L3NonTaxableAllocationForExpensesIncurredAsPartOfJob = "";
            V1NonTaxableBenefitForBoardAndLodging = "";
            L2VolunteerCompensationNotIncludedInBoxesAAndL = "";
            L4BenefitResultingFromADebtContractedToAquireInvestments = "";
            L5DedutionForAHomeRelocationLoan = "";
            L7BenefitForPurchaseOptionUponDeath = "";
            A6RenumerationReceivedByAQuebecSailor = "";
            A9DeductionForForeignSpecialists = "";
            A10DedutionForForeignResearchers = "";
            A11DeductionForForeignResearchersOnPostDoctoralInternship = "";
            A12DeductionForForeignExperts = "";
            A13DeductionForForeignProfessors = "";
            x201ChildCareExpensesFromTheMinistereDeLEmploiDeLaSolidarite = "";
            A8DeductionsForPoliceOfficers = "";
            A14ExemptionRate = "";
            L8SecurityOptionsElection = "";
            x235ContributionPaidByTheEmployeeToPrivateHealthInsurancePlan = "";
            x200NameOfCurrencyUsed = "";
            G2ContributorySalarayOrWagesUnderTheCpp = "";
            x211BenefitRelatedToPreviousEmployment = "";
            L9DeductionsForStockOptionUnderSection725_2OfTheIncomeTaxAct = "";
            L10DeductionsForStockOptionUnderSection725_3OfTheIncomeTaxAct = "";
            QcBusinessNumber = "";
            ReferenceNumber = "";
            EmployeeLanguagePreference = "F";
            ActiveTerminatedEmployee = "A";
            //other fields optional - not including here
        }
    }
}
