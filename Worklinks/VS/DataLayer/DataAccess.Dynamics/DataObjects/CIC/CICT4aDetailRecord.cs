﻿using System;
using FileHelpers;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CIC
{
    [FixedLengthRecord()]
    public class CICT4aDetailRecord
    {
        [FieldFixedLength(4)]
        public string TaxYear;

        [FieldFixedLength(36)]
        public string UniqueEmployeeIdentifier;

        [FieldFixedLength(15)]
        public string AuthenticationValuePrimary;                       //typically SIN or Employee #.  USING SIN

        [FieldFixedLength(15)]
        [FieldConverter(ConverterKind.Date, "MM-dd-yyyy")]
        public DateTime AuthenticationValueSecondary;                   //Typically birthdate in MM-dd-yyyy format, Employee #, or first three letters of last name.  USING BIRTHDATE.

        [FieldFixedLength(9)]
        public string RecipientSocialInsuranceNumber;                   //do not include dashes

        [FieldFixedLength(20)]
        public string RecipientFirstName;                               //specify either a Corporation/Partnership name and business number, or an individual's name and SIN.

        [FieldFixedLength(1)]
        public string RecipientMiddleInitial;                           

        [FieldFixedLength(25)]
        public string RecipientLastName;

        [FieldFixedLength(30)]
        public string RecipientCorporationNameLine1;

        [FieldFixedLength(30)]
        public string RecipientCorporationNameLine2;

        [FieldFixedLength(30)]
        public string RecipientLocationAddresss; //Line 1

        [FieldFixedLength(30)]
        public string RecipientDeliveryAddress; //Line 2

        [FieldFixedLength(25)]
        public string RecipientCity;

        [FieldFixedLength(2)]
        public string RecipientProvinceState;

        [FieldFixedLength(10)]
        public string RecipientPostalCode;

        [FieldFixedLength(40)]
        public string RecipientCountry;

        [FieldFixedLength(30)]
        public string PayerNameLine1;

        [FieldFixedLength(30)]
        public string PayerNameLine2;

        [FieldFixedLength(40)]
        public string PayerLocationAddress; //Line 1

        [FieldFixedLength(40)]
        public string PayerDeliveryAddress; //Line 2

        [FieldFixedLength(25)]
        public string PayerCity;

        [FieldFixedLength(2)]
        public string PayerProvinceState;

        [FieldFixedLength(10)]
        public string PayerPostalCode;

        [FieldFixedLength(40)]
        public string PayerCountry;

        [FieldFixedLength(15)]
        public string Box061PayerAccountNumber;

        [FieldFixedLength(15)]
        public string Box013RecipientAccountNumber;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string Box016PensionOrSuperannuation;            //all fields below are right justified (unless commented), 0 filled, last 2 digits are assumed decimal digits

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string Box018LumpSumPayments;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox102;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox108;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox110;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox158;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox180;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox190LumpSumPaymentsFromUnregisteredPlan;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string Box020SelfEmployedCommisions;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string Box022IncomeTaxDeducted;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string Box024Annuities;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox111IncomeAveragingAnnuityContracts;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox115DeferredProfitSharingPlan;

        //[FieldFixedLength(9)]
        //[FieldAlign(AlignMode.Right, '0')]
        //public string OtherBox026EligibleRetiringAllowances; //not in 2017 spec, replaced with OtherBox129TaxDeferredCooperativeShare

        //[FieldFixedLength(9)]
        //[FieldAlign(AlignMode.Right, '0')]
        //public string OtherBox129TaxDeferredCooperativeShare; //2018 spec changes to map to Other Box 128

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox128VeteransBenefitsEligibleForPensionSplitting;

        //[FieldFixedLength(9)]
        //[FieldAlign(AlignMode.Right, '0')]
        //public string OtherBox027NonEligibleRetiringAllowances;//not in 2017 spec, replaced with OtherBox196TuitionAssistanceForAdultBasicEducation

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox196TuitionAssistanceForAdultBasicEducation;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox028OtherIncome;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox030PatronageAllocations;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox032RegisteredPensionPlanContributions;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox126Pre1990PastServiceContributionsWhileContributor;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox034PensionAdjustment;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox040RESPAccumulatedIncomePayments;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox122RESPAccumulatedIncomePaymentPaidToOthers;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox042RESPEducationalAssistancePayments;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox046CharitableDonations;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox048FeesForServices;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox133VariablePensionBenefits;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox135RecipientPaidPremiumsForPrivateHealthServices;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox104ResearchGrants;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox107PaymentForAWageLossReplacementPlan;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox118MedicalPremiumBenefits;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox119PremiumsPaidToGroupTermLifeInsurancePlan;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox127VeteransBenefits;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox132WageEarnerProtectionPlan;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox131RegisteredDisabilitySavingsPlan;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox105ScholarshipsBursariesFellowshipsArtistsProjectGrantsAndPrizes;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox106DeathBenefits;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox109PeriodicPaymentsFromUnregisteredPlan;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox116MedicalTravelAssistance;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox117LoanBenefits;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox123PaymentsFromARevokedDPSP;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox124BoardAndLodgingAtSpecialWorkSites;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox125DisabilityBenefitsPaidOutOfASuperannuationOrPensionPlan;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox129TaxDeferredCooperativeShare;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox130ApprenticeshipIncentiveGrantOrApprenticeshipCompletionGrant;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox134TaxFreeSavingsAccountTaxableAmount;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox136FederalIncomeSupportForPMMCGrant;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox150LabourAdjustmentBenefitsActAndAppropriationActs;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox152SUBPQualifiedUnderTheIncomeTaxAct;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox154CashAwardOrPrizeFromPayer;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox156BankruptcySettlement;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox162Pre1990PastServiceContributionsWhileNotAContributor;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox194PRPPPayments;

        [FieldFixedLength(20)]
        public string OtherBox014RecipientNumber;                   //left justified, space filled

        [FieldFixedLength(7)]
        public string OtherBox036PlanRegistrationNumber;            //left justified, space filled

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox142StatusIndianExemptIncomeEligibleRetiringAllowance;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox143StatusIndianExemptIncomeNonEligibleRetiringAllowance;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox144StatusIndianExemptIncomeOtherIncome;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox146StatusIndianExemptIncomePensionOrSuperannuation;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox148StatusIndianExemptIncomeLumpSumPayments;

        [FieldFixedLength(9)]
        [FieldAlign(AlignMode.Right, '0')]
        public string OtherBox195IndianExemptIncomePRPPPayments;

        [FieldFixedLength(1)]
        public string ActiveTerminatedEmployee;                         //A = Active, T = Terminated (Must be "A" or "T")

        //other fields optional - not including here
        public CICT4aDetailRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            TaxYear = "";
            UniqueEmployeeIdentifier = "";
            AuthenticationValuePrimary = "";
            AuthenticationValueSecondary = DateTime.Now;
            RecipientSocialInsuranceNumber = "";
            RecipientFirstName = "";
            RecipientMiddleInitial = " ";
            RecipientLastName = "";
            RecipientCorporationNameLine1 = "";
            RecipientCorporationNameLine2 = "";
            RecipientLocationAddresss = "";
            RecipientDeliveryAddress = "";
            RecipientCity = "";
            RecipientProvinceState = "";
            RecipientPostalCode = "";
            RecipientCountry = ""; //if blank, Canada is assumed
            PayerNameLine1 = "";
            PayerNameLine2 = "";
            PayerLocationAddress = "";
            PayerDeliveryAddress = "";
            PayerCity = "";
            PayerProvinceState = "";
            PayerPostalCode = "";
            PayerCountry = ""; //if blank, Canada is assumed
            Box061PayerAccountNumber = "";
            Box013RecipientAccountNumber = "000000000RP0000";
            Box016PensionOrSuperannuation = "";
            Box018LumpSumPayments = "";
            OtherBox102 = "";
            OtherBox108 = "";
            OtherBox110 = "";
            OtherBox158 = "";
            OtherBox180 = "";
            OtherBox190LumpSumPaymentsFromUnregisteredPlan = "";
            Box020SelfEmployedCommisions = "";
            Box022IncomeTaxDeducted = "";
            Box024Annuities = "";
            OtherBox111IncomeAveragingAnnuityContracts = "";
            OtherBox115DeferredProfitSharingPlan = "";

            //stacy - box 26/27 replaced with box 129/196 in their position
            //OtherBox026EligibleRetiringAllowances = "";
            //OtherBox027NonEligibleRetiringAllowances = "";
            //OtherBox129TaxDeferredCooperativeShare = ""; //replaced with Box 128 in it's position in 2018 spec
            OtherBox128VeteransBenefitsEligibleForPensionSplitting = "";
            OtherBox196TuitionAssistanceForAdultBasicEducation = "";

            OtherBox028OtherIncome = "";
            OtherBox030PatronageAllocations = "";
            OtherBox032RegisteredPensionPlanContributions = "";
            OtherBox126Pre1990PastServiceContributionsWhileContributor = "";
            OtherBox034PensionAdjustment = "";
            OtherBox040RESPAccumulatedIncomePayments = "";
            OtherBox122RESPAccumulatedIncomePaymentPaidToOthers = "";
            OtherBox042RESPEducationalAssistancePayments = "";
            OtherBox046CharitableDonations = "";
            OtherBox048FeesForServices = "";
            OtherBox133VariablePensionBenefits = "";
            OtherBox135RecipientPaidPremiumsForPrivateHealthServices = "";
            OtherBox104ResearchGrants = "";
            OtherBox107PaymentForAWageLossReplacementPlan = "";
            OtherBox118MedicalPremiumBenefits = "";
            OtherBox119PremiumsPaidToGroupTermLifeInsurancePlan = "";
            OtherBox127VeteransBenefits = "";
            OtherBox132WageEarnerProtectionPlan = "";
            OtherBox131RegisteredDisabilitySavingsPlan = "";
            OtherBox105ScholarshipsBursariesFellowshipsArtistsProjectGrantsAndPrizes = "";
            OtherBox106DeathBenefits = "";
            OtherBox109PeriodicPaymentsFromUnregisteredPlan = "";
            OtherBox116MedicalTravelAssistance = "";
            OtherBox117LoanBenefits = "";
            OtherBox123PaymentsFromARevokedDPSP = "";
            OtherBox124BoardAndLodgingAtSpecialWorkSites = "";
            OtherBox125DisabilityBenefitsPaidOutOfASuperannuationOrPensionPlan = "";
            OtherBox129TaxDeferredCooperativeShare = "";
            OtherBox130ApprenticeshipIncentiveGrantOrApprenticeshipCompletionGrant = "";
            OtherBox134TaxFreeSavingsAccountTaxableAmount = "";
            OtherBox136FederalIncomeSupportForPMMCGrant = "";
            OtherBox150LabourAdjustmentBenefitsActAndAppropriationActs = "";
            OtherBox152SUBPQualifiedUnderTheIncomeTaxAct = "";
            OtherBox154CashAwardOrPrizeFromPayer = "";
            OtherBox156BankruptcySettlement = "";
            OtherBox162Pre1990PastServiceContributionsWhileNotAContributor = "";
            OtherBox194PRPPPayments = "";
            OtherBox014RecipientNumber = "";
            OtherBox036PlanRegistrationNumber = "";
            OtherBox142StatusIndianExemptIncomeEligibleRetiringAllowance = "";
            OtherBox143StatusIndianExemptIncomeNonEligibleRetiringAllowance = "";
            OtherBox144StatusIndianExemptIncomeOtherIncome = "";
            OtherBox146StatusIndianExemptIncomePensionOrSuperannuation = "";
            OtherBox148StatusIndianExemptIncomeLumpSumPayments = "";
            OtherBox195IndianExemptIncomePRPPPayments = "";
            ActiveTerminatedEmployee = "A";
            //other fields optional - not including here
        }
    }
}
