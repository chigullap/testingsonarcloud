﻿using System;
using FileHelpers;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.REALExport
{
    [DelimitedRecord(",")]
    public class REALExportHeader
    {
        public string EmployeeNumber;
        public string FirstName;
        public string LastName;
        public string HireDate;
        public string Username;
        public string EmployeeStatus;
        public string EmployeeStatusDate;
        public string BirthDate;
        public string StandardHoursPerDay;
        public string Address;
        public string City;
        public string Province;
        public string Zip;
        public string Country;
        public string Phone1;
        public string Phone2;
        public string Phone3;
        public string Email;
        public string UnionCode;
        public string PayCycle;
        public string HourlySalary;
        public string BaseRateOfPay;
        public string BaseWage;
        public string PositionChangeDate;
        public string LL1;
        public string LL2;
        public string LL3;
        public string LL4;
        public string LL5;
        public string LL6;
        public string LL7;
        public string LL1Desc;
        public string LL2Desc;
        public string LL3Desc;
        public string LL4Desc;
        public string LL5Desc;
        public string LL6Desc;
        public string LL7Desc;
        public string ReportsTo;
        public string WorkerType;
        public string BadgeNumber;
        public string MobileLicense;
        public string ManagerLicense;
        public string ScheduleLicense;
        public string Timezone;
        public string SeniorityDate;
        public string DrawNumber;
        public string EligibleJobs;
        public string Posting;
        public string PostingEndDate;
        public string BankVacation;
        public string BankOvertime;
        public string SecondaryLabour;

        public REALExportHeader()
        {
            Clear();
        }
        protected void Clear()
        {
            EmployeeNumber = "Employee Number";
            FirstName = "First Name";
            LastName = "Last Name";
            HireDate = "Hire Date";
            Username = "Username";
            EmployeeStatus = "Employee Status";
            EmployeeStatusDate = "Employee Status Date";
            BirthDate = "Birth Date";
            StandardHoursPerDay = "Standard Hours Per Day";
            Address = "Address";
            City = "City";
            Province = "Province";
            Zip = "Zip";
            Country = "Country";
            Phone1 = "Phone 1";
            Phone2 = "Phone 2";
            Phone3 = "Phone 3";
            Email = "Email";
            UnionCode = "Union Code";
            PayCycle = "Pay Cycle";
            HourlySalary = "Hourly/Salary";
            BaseRateOfPay = "Base Rate of Pay";
            BaseWage = "Base Wage";
            PositionChangeDate = "Position Change Date";
            LL1 = "LL1";
            LL2 = "LL2";
            LL3 = "LL3";
            LL4 = "LL4";
            LL5 = "LL5";
            LL6 = "LL6";
            LL7 = "LL7";
            LL1Desc = "LL1 Desc";
            LL2Desc = "LL2 Desc";
            LL3Desc = "LL3 Desc";
            LL4Desc = "LL4 Desc";
            LL5Desc = "LL5 Desc";
            LL6Desc = "LL6 Desc";
            LL7Desc = "LL7 Desc";
            ReportsTo = "Reports To";
            WorkerType = "Worker Type";
            BadgeNumber = "Badge Number";
            MobileLicense = "Mobile License";
            ManagerLicense = "Manager License";
            ScheduleLicense = "Schedule License";
            Timezone = "Timezone";
            SeniorityDate = "SeniorityDate";
            DrawNumber = "DrawNumber";
            EligibleJobs = "EligibleJobs";
            Posting = "Posting";
            PostingEndDate = "PostingEndDate";
            BankVacation = "BankVacation";
            BankOvertime = "BankOvertime";
            SecondaryLabour = "SecondaryLabour";
        }
    }

    [DelimitedRecord(",")]
    public class REALExportDetail
    {
        public string EmployeeNumber;
        public string FirstName;
        public string LastName;

        [FieldConverter(ConverterKind.Date, "MM/dd/yyyy")]
        public DateTime HireDate;

        public string Username;
        public string EmployeeStatus;

        [FieldConverter(ConverterKind.Date, "MM/dd/yyyy")]
        public DateTime EmployeeStatusDate;

        [FieldConverter(ConverterKind.Date, "MM/dd/yyyy")]
        public DateTime? BirthDate;

        public string StandardHoursPerDay;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string City;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Province;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Zip;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Country;

        public string Phone1;
        public string Phone2;
        public string Phone3;
        public string Email;
        public string UnionCode;
        public string PayCycle;
        public string HourlySalary;
        public string BaseRateOfPay;

        [FieldConverter(ConverterKind.Date, "MM/dd/yyyy")]
        public DateTime BaseWage;

        [FieldConverter(ConverterKind.Date, "MM/dd/yyyy")]
        public DateTime PositionChangeDate;

        public string LL1;
        public string LL2;
        public string LL3;
        public string LL4;
        public string LL5;
        public string LL6;
        public string LL7;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LL1Desc;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LL2Desc;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LL3Desc;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LL4Desc;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LL5Desc;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LL6Desc;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LL7Desc;

        public string ReportsTo;
        public string WorkerType;
        public string BadgeNumber;
        public string MobileLicense;
        public string ManagerLicense;
        public string ScheduleLicense;
        public string Timezone;

        [FieldConverter(ConverterKind.Date, "yyyy/MM/dd")]
        public DateTime? SeniorityDate;

        public string DrawNumber;
        public string EligibleJobs;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Posting;

        [FieldConverter(ConverterKind.Date, "MM/dd/yyyy")]
        public DateTime? PostingEndDate;

        public string BankVacation;
        public string BankOvertime;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SecondaryLabour;

        public REALExportDetail()
        {
            Clear();
        }

        protected void Clear()
        {
            EmployeeNumber = null;
            EmployeeNumber = null;
            FirstName = null;
            LastName = null;
            HireDate = DateTime.MinValue;
            Username = null;
            EmployeeStatus = null;
            EmployeeStatusDate = DateTime.MinValue;
            BirthDate = null;
            StandardHoursPerDay = null;
            Address = null;
            City = null;
            Province = null;
            Zip = null;
            Country = null;
            Phone1 = null;
            Phone2 = null;
            Phone3 = null;
            Email = null;
            UnionCode = null;
            PayCycle = null;
            HourlySalary = null;
            BaseRateOfPay = null;
            BaseWage = DateTime.MinValue;
            PositionChangeDate = DateTime.MinValue;
            LL1 = null;
            LL2 = null;
            LL3 = null;
            LL4 = null;
            LL5 = null;
            LL6 = null;
            LL7 = null;
            LL1Desc = null;
            LL2Desc = null;
            LL3Desc = null;
            LL4Desc = null;
            LL5Desc = null;
            LL6Desc = null;
            LL7Desc = null;
            ReportsTo = null;
            WorkerType = null;
            BadgeNumber = null;
            MobileLicense = null;
            ManagerLicense = null;
            ScheduleLicense = null;
            Timezone = null;
            SeniorityDate = null;
            DrawNumber = null;
            EligibleJobs = null;
            Posting = null;
            PostingEndDate = null;
            BankVacation = null;
            BankOvertime = null;
            SecondaryLabour = null;
        }
    }
}