﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [FixedLengthRecord()]
    public class CSBTransmissionHeaderRecord
    {
        [FieldFixedLength(2)]
        public int RecordType;

        [FieldFixedLength(5)]
        public string TransmitterOrganizationId;

        [FieldFixedLength(10)]
        [FieldConverter(ConverterKind.Date, "yyyy-MM-dd" )]
        public DateTime TransmissionDate; //must be YYYY-MM-DD

        [FieldFixedLength(8)]
        public string TransmissionId;

        [FieldFixedLength(30)]
        public string TransmittersReferenceId;

        [FieldFixedLength(1)]
        public string PaymentTypeIndicator;

        [FieldFixedLength(2)]
        public string EmailFaxIndicator;

        [FieldFixedLength(40)]
        public string EmailFaxAddress;

        [FieldFixedLength(22)]
        public string Filler;

        public CSBTransmissionHeaderRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = 10;
            TransmitterOrganizationId = null;
            TransmissionDate = DateTime.Now;
            TransmissionId = null;
            TransmittersReferenceId = "                              ";
            PaymentTypeIndicator = null;
            EmailFaxIndicator = null;
            EmailFaxAddress = null;     /* doc states "Please enter the fax number as required to send a fax FROM the Ottawa (613) area to the employer's location.
                                           for example, if it is long distance from Ottawa to an employer's location in the 519 area, the number to enter is 15195551212; for local 6135551212 */
            Filler = "                     x"; //doc states "after the last field a filler is needed.  Put a char in the 120th position to ensure no compression occurs during movement between platforms"
        }
    }
}
