﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [FixedLengthRecord()]
    public class CSBBatchTrailerRecord
    {
        [FieldFixedLength(2)]
        public int RecordType;

        [FieldFixedLength(5)]
        public string OrganizationId;

        [FieldFixedLength(10)]
        [FieldConverter(ConverterKind.Date, "yyyy-MM-dd")]
        public DateTime EffectiveDate;

        [FieldFixedLength(6)]
        public string BatchTotalNumberOfRecords;

        [FieldFixedLength(1)]
        public string Sign;

        [FieldFixedLength(15)]
        public string BatchTotalDeductionAmount;

        [FieldFixedLength(81)]
        public string Filler;

        public CSBBatchTrailerRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = 80;
            OrganizationId = null;
            EffectiveDate = DateTime.Now;
            BatchTotalNumberOfRecords = null;
            Sign = " ";
            BatchTotalDeductionAmount = null;             //15 digits.  Last 2 are implied decimal (ie. $10 is 000000000001000)
            Filler = "                                                                                x";  //doc states "after the last field a filler is needed.  Put a char in the 120th position to ensure no compression occurs during movement between platforms"
        }
    }
}
