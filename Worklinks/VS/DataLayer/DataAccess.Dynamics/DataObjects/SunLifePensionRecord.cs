﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [FixedLengthRecord()]
    public class SunLifePensionRecord
    {
        //key data
        [FieldFixedLength(1)]
        public string RecordType;
        [FieldFixedLength(3)]
        public string TapeId;
        [FieldFixedLength(5)]
        public string ClientId;
        [FieldFixedLength(2)]
        public string PlanId;
        [FieldFixedLength(9)]
        public string EmployeeNumber;

        //non-financial update data
        [FieldFixedLength(9)]
        public string SocialInsuranceNumber;
        [FieldFixedLength(32)]
        public string MemberLastName;
        [FieldFixedLength(32)]
        public string MemberFirstName;
        [FieldFixedLength(32)]
        public string AddressLine1;
        [FieldFixedLength(32)]
        public string AddressLine2;
        [FieldFixedLength(32)]
        public string AddressLine3;
        [FieldFixedLength(32)]
        public string AddressLine4;
        [FieldFixedLength(6)]
        public string MemberPostalCode;
        [FieldFixedLength(3)]
        public string MemberResidenceCode;
        [FieldFixedLength(3)]
        public string PayrollDivision;
        [FieldFixedLength(210)]
        public string Filler1;
        [FieldFixedLength(1)]
        public string UserField1;
        [FieldFixedLength(9)]
        public string Filler2;
        [FieldFixedLength(10)]
        public string UserField2;
        [FieldFixedLength(10)]
        public string Filler3;
        [FieldFixedLength(15)]
        public string UserField5;
        [FieldFixedLength(39)]
        public string Filler4;
        [FieldFixedLength(9)]
        public string UserField9;
        [FieldFixedLength(189)]
        public string Filler5;

        //financial update data
        [FieldFixedLength(4)]
        public string ProductId1;
        [FieldFixedLength(15)]
        public string Filler6;
        [FieldFixedLength(9)]
        public string EmployersRequired;
        [FieldFixedLength(9)]
        public string MemberVoluntaryMatched;
        [FieldFixedLength(45)]
        public string Filler7;
        [FieldFixedLength(9)]
        public string EmployerVoluntaryMatched;
        [FieldFixedLength(4)]
        public string ProductId2;
        [FieldFixedLength(33)]
        public string Filler8;
        [FieldFixedLength(9)]
        public string MemberVoluntaryUnmatched1;
        [FieldFixedLength(45)]
        public string Filler9;
        [FieldFixedLength(5)]
        public string ProductId3;
        [FieldFixedLength(32)]
        public string Filler10;
        [FieldFixedLength(9)]
        public string MemberVoluntaryUnmatched2;
        [FieldFixedLength(45)]
        public string Filler11;
        [FieldFixedLength(5)]
        public string ProductId4;
        [FieldFixedLength(32)]
        public string Filler12;
        [FieldFixedLength(9)]
        public string MemberVoluntarySpousalUnmatched;
        [FieldFixedLength(45)]
        public string Filler13;
        [FieldFixedLength(5)]
        public string ProductId5;
        [FieldFixedLength(32)]
        public string Filler14;
        [FieldFixedLength(9)]
        public string MemberVoluntaryUnmatched3;
        [FieldFixedLength(72)]
        public string Filler15;

        //other non-financial update data
        [FieldFixedLength(3)]
        public string MemberProvinceOfEmployment1;
        [FieldFixedLength(8)]
        public string Filler16;
        [FieldFixedLength(8)]
        public string MemberDateOfBirth1;
        [FieldFixedLength(8)]
        public string MemberDateOfHire;
        [FieldFixedLength(26)]
        public string Filler26;
        [FieldFixedLength(1)]
        public string LanguageCode;
        [FieldFixedLength(9)]
        public string CurrentYearCompensation;
        [FieldFixedLength(9)]
        public string CurrentYearSalaryWages;
        [FieldFixedLength(34)]
        public string Filler27;
        [FieldFixedLength(1)]
        public string EmployeeStatusCode;
        [FieldFixedLength(8)]
        public string EmployeeStatusEffectiveDate;
        [FieldFixedLength(5)]
        public string Filler28;
        [FieldFixedLength(1)]
        public string MemberGender;
        [FieldFixedLength(50)]
        public string Filler29;
        [FieldFixedLength(8)]
        public string PayStartDate;
        [FieldFixedLength(8)]
        public string PayEndDate;
        [FieldFixedLength(32)]
        public string Filler30;

        //enrollment data
        [FieldFixedLength(3)]
        public string MemberProvinceOfEmployment2;
        [FieldFixedLength(16)]
        public string Filler31;
        [FieldFixedLength(8)]
        public string DateOfHire;
        [FieldFixedLength(1)]
        public string MemberLanguageCode;
        [FieldFixedLength(1)]
        public string Filler32;
        [FieldFixedLength(8)]
        public string MemberDateOfBirth2;
        [FieldFixedLength(8)]
        public string DateOfCoverage;
        [FieldFixedLength(10)]
        public string Filler33;
        [FieldFixedLength(2)]
        public string SubDivision;
        [FieldFixedLength(15)]
        public string Filler34;
        [FieldFixedLength(1)]
        public string EnrollmentFlag;

        //email and phone data
        [FieldFixedLength(1)]
        public string EmailFlag;
        [FieldFixedLength(64)]
        public string EmailAddress;
        [FieldFixedLength(1)]
        public string PhoneFlag;
        [FieldFixedLength(5)]
        public string PhoneCountryCode;
        [FieldFixedLength(5)]
        public string PhoneCityCode;
        [FieldFixedLength(12)]
        public string PhoneNumber;
        [FieldFixedLength(5)]
        public string PhoneExt;
        [FieldFixedLength(408)]
        public string Filler35;

        public SunLifePensionRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = "D";
            TapeId = "ZOE";
            ClientId = "C0QWO";
            PlanId = "01";
            EmployeeNumber = "";
            SocialInsuranceNumber = "";
            MemberLastName = "";
            MemberFirstName = "";
            AddressLine1 = "";
            AddressLine2 = "";
            AddressLine3 = "";
            AddressLine4 = "";
            MemberPostalCode = "";
            MemberResidenceCode = "";
            PayrollDivision = "419";
            Filler1 = "".PadRight(210);
            UserField1 = "";
            Filler2 = "".PadRight(9);
            UserField2 = "";
            Filler3 = "".PadRight(10);
            UserField5 = "";
            Filler4 = "".PadRight(39);
            UserField9 = "";
            Filler5 = "".PadRight(189);
            ProductId1 = "DCPP";
            Filler6 = "";
            EmployersRequired = "";
            MemberVoluntaryMatched = "";
            Filler7 = "".PadRight(45);
            EmployerVoluntaryMatched = "";
            ProductId2 = "NREG";
            Filler8 = "".PadRight(33);
            MemberVoluntaryUnmatched1 = "";
            Filler9 = "".PadRight(45);
            ProductId3 = "RRSP ";               //5 length but value RRSP only 4 chars...pad with space for now???  //stacy test
            Filler10 = "".PadRight(32);
            MemberVoluntaryUnmatched2 = "";
            Filler11 = "".PadRight(45);
            ProductId4 = "RRSPS";
            Filler12 = "".PadRight(32);
            MemberVoluntarySpousalUnmatched = "";
            Filler13 = "".PadRight(45);
            ProductId5 = "NREG9";
            Filler14 = "".PadRight(32);
            MemberVoluntaryUnmatched3 = "";
            Filler15 = "".PadRight(72);

            MemberProvinceOfEmployment1 = "";
            Filler16 = "".PadRight(8);
            MemberDateOfBirth1 = "";
            MemberDateOfHire = "";
            Filler26 = "".PadRight(26);
            LanguageCode = "";
            CurrentYearCompensation = "";
            CurrentYearSalaryWages = "";
            Filler27 = "".PadRight(34);
            EmployeeStatusCode = "";
            EmployeeStatusEffectiveDate = "";
            Filler28 = "".PadRight(5);
            MemberGender = "";
            Filler29 = "".PadRight(50);
            PayStartDate = "";
            PayEndDate = "";
            Filler30 = "".PadRight(32);

            MemberProvinceOfEmployment2 = "";
            Filler31 = "".PadRight(16);
            DateOfHire = "";
            MemberLanguageCode = "E";
            Filler32 = "".PadRight(1);
            MemberDateOfBirth2 = "";
            DateOfCoverage = "20790101";
            Filler33 = "".PadRight(10);
            SubDivision = "01";
            Filler34 = "".PadRight(15);
            EnrollmentFlag = "Y";

            EmailFlag = "";
            EmailAddress = "";
            PhoneFlag = "";
            PhoneCountryCode = "".PadRight(5);
            PhoneCityCode = "".PadRight(5);
            PhoneNumber = "".PadRight(12);
            PhoneExt = "".PadRight(5);
            Filler35 = "".PadRight(408);
        }
    }
}
