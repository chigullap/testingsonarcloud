﻿using System;
using FileHelpers;


namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.PexGlExport
{
    [DelimitedRecord(",")]
    public class PexGlExportHeader
    {
        public string RecordNo;
        public string RecordType;
        public string GCC;
        public string LCC;
        public string PayGroupID;
        public string PayrollPeriod;
        public string CurrencyCode;

        public PexGlExportHeader()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordNo = "0000000001";    //“0000000001”
            RecordType = "H";           //"H"
            GCC = "VER";                //Payroll Exchange Global Customer Code
            LCC = "CA001";              //Payroll Exchange Local Customer Code
            PayGroupID = "C1";          //Payroll Exchange Payroll Group ID
            PayrollPeriod = "";         //Payroll Period YYYYPPRR:  - YYYY - Payroll Year
                                        //                          - PP - Payroll Period
                                        //                          - RR - Off Cycle / Supplementary Run.  Will be 01 for main / regular run.
           CurrencyCode = "CAD";       //ISO Currency Code
        }
    }

    [DelimitedRecord(",")]
    public class PexGlExportDetail
    {
        public string RecordNo;
        public string RecordType;
        public string GLAccount;
        public string GLAccountDesc;
        public string CostCenter;
        public string PayElement;
        public string PayServID;
        public string HRISID;
        public string DebitAmount;
        public string CreditAmount;
        public string Hours;
        public string PostingDate;
        public string Filler1;
        public string Filler2;
        public string Filler3;
        public string Filler4;
        public string Filler5;


        public PexGlExportDetail()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordNo = "";      //Record Sequence number, zero filled, starting with 0000000002
            RecordType = "D";   //"D"
            GLAccount = "";     //General Ledger Account Number NOTE: The PEX standard is to summarize at the GLAccount / CostCenter field level.
            GLAccountDesc = ""; //General Ledger Account Description / Text if available in the payroll system, if not left blank.
                                //For field below...NOTE: The PEX standard is to summarize at the GLAccount / CostCenter field level.
            CostCenter = "";    //Cost Center Code, left blank if not available.Availability will depend on the payroll system capabilities
            PayElement = "";    //Payroll Exchange Wage Type / Pay Element Code, will be blank when detail is summarized by Account code
            PayServID = "";     //Payroll Service ID, will be blank when detail is summarized by Account code
            HRISID = "";        //HRIS ID (The ID based in the PersonID field of the New Hire Events and stored as an informational value in the Payroll Service) 
                                    //If not held then blank, will be blank when detail is summarized by Account code

            DebitAmount = "";   //Debit Amount 
                                    //Includes decimals and decimal point "."
                                    //No sign
                                    //No thousands separator
                                    //No Currency Symbol
                                    //e.g. 12299.45, 4335.30, 100.00
                                    //Leave blank if not held
            CreditAmount = "";  //Credit Amount, format as per Debit Amount
            Hours = "";         //Payroll Hours if applicable
                                    //Includes significant decimals (no need to pad with zero as per currency values) and decimal point "."
                                    //Leading minus sign if applicable
                                    //No thousands separator
                                    //e.g. 99.45, -35.3, 1000
                                    //This is only required in countries and payroll services that require this field.
            PostingDate = "";   //Posting date in format YYYYMMDD
            Filler1 = "";
            Filler2 = "";
            Filler3 = "";
            Filler4 = "";
            Filler5 = "";
        }
    }
    
    [DelimitedRecord(",")]
    public class PexGlExportTrailer
    {
        public string RecordNo;
        public string RecordType;
        public string RecordCount;
        public string DebitTotal;
        public string CreditTotal;

        public PexGlExportTrailer()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordNo = "";      //Record Sequence number, zero filled
            RecordType = "T";   //"T"
            RecordCount = "";   //Count of all records including header and trailer
            DebitTotal = "";    //Total of all Detail Record Debit Amounts in same format as Debit Amount
            CreditTotal = "";   //Total of all Detail Record Credit Amounts in same format as Debit Amount
        }
    }
}
