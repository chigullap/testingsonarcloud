﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [FixedLengthRecord()]
    public class RRSPTrailerRecord
    {
        [FieldFixedLength(6)]
        public string GroupNumber;

        [FieldFixedLength(9)]
        public string EmployeeNumber;

        [FieldFixedLength(10)]
        public string DepositAmount;

        [FieldFixedLength(1)]
        public string DepositIndicator;

        [FieldFixedLength(20)]
        public string NameFiller;

        public RRSPTrailerRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            GroupNumber = "";                       //value will come from code_system db table
            EmployeeNumber = "999999999";           //spec says this will always be nine 9's.
            DepositAmount = "";                     //total amount deposited, zero filled, right justififed.  0001234500 (for $123.50)
            DepositIndicator = "1";                 //spec says this will always be "1"
            NameFiller = "                    ";    //spec says this will always be spaces
        }
    }
}
