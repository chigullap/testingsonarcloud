﻿using System;
using FileHelpers;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.RbcEftEdiSourceDed;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CraEftGarnishment
{
    public class GarnishmentDetails
    {
        //all but one of these classes are from the "RbcEftGarnishment" folder, used for CRA remittances
        public IsaInterchangeControlHeaderSourceDed[] isaHeader = new IsaInterchangeControlHeaderSourceDed[1];
        public GsFunctionalGroupHeaderSourceDed[] gsHeader = new GsFunctionalGroupHeaderSourceDed[1];
        public SourceDeductionsStTransactionSetHeader[] st = new SourceDeductionsStTransactionSetHeader[1];
        public SourceDeductionsBeginningSegment[] bps = new SourceDeductionsBeginningSegment[1];
        public SourceDeductionsReferenceIdentification[] ref1 = new SourceDeductionsReferenceIdentification[1];
        public SourceDeductionsReferenceIdentification2[] ref2 = new SourceDeductionsReferenceIdentification2[1];
        public SourceDeductionsDateTimeReference[] dtm = new SourceDeductionsDateTimeReference[1];
        public SourceDeductionsPayeeReceiverName[] n1pe = new SourceDeductionsPayeeReceiverName[1];
        public SourceDeductionsPayorReceiverName[] n1pr = new SourceDeductionsPayorReceiverName[1];
        public SourceDeductionsAdministrativeCommunicationsContact[] per = new SourceDeductionsAdministrativeCommunicationsContact[1];
        public SourceDeductionsLoopHeader[] ls = new SourceDeductionsLoopHeader[1];

        //these are part of a loopable item.....make construtor take int and declare then
        public GarnishmentRepeatingElements[] repeatingElements;

        public SourceDeductionsLoopTrailer[] le = new SourceDeductionsLoopTrailer[1];
        public SourceDeductionsSeTransactionSetTrailer[] se = new SourceDeductionsSeTransactionSetTrailer[1];
        public GsFunctionalGroupTrailerSourceDed[] geTrailer = new GsFunctionalGroupTrailerSourceDed[1];
        public IsaInterchangeControlTrailerSourceDed[] ieaTrailer = new IsaInterchangeControlTrailerSourceDed[1];

        public GarnishmentDetails(int numLoops)
        {
            isaHeader[0] = new IsaInterchangeControlHeaderSourceDed();
            gsHeader[0] = new GsFunctionalGroupHeaderSourceDed();
            st[0] = new SourceDeductionsStTransactionSetHeader();
            bps[0] = new SourceDeductionsBeginningSegment();
            ref1[0] = new SourceDeductionsReferenceIdentification();
            ref2[0] = new SourceDeductionsReferenceIdentification2();
            dtm[0] = new SourceDeductionsDateTimeReference();
            n1pe[0] = new SourceDeductionsPayeeReceiverName();
            n1pr[0] = new SourceDeductionsPayorReceiverName();
            per[0] = new SourceDeductionsAdministrativeCommunicationsContact();
            ls[0] = new SourceDeductionsLoopHeader();

            //create repeating elements
            repeatingElements = new GarnishmentRepeatingElements[numLoops];
            for (int i = 0; i < numLoops; i++)
                repeatingElements[i] = new GarnishmentRepeatingElements();

            le[0] = new SourceDeductionsLoopTrailer();
            se[0] = new SourceDeductionsSeTransactionSetTrailer();
            geTrailer[0] = new GsFunctionalGroupTrailerSourceDed();
            ieaTrailer[0] = new IsaInterchangeControlTrailerSourceDed();
        }
    }

    //repeating elements class
    public class GarnishmentRepeatingElements
    {
        public SourceDeductionsPayorReceiverName2[] n1pr2 = new SourceDeductionsPayorReceiverName2[1];
        public GarnishmentSourceDeductionsRemittanceAdvice[] rmt = new GarnishmentSourceDeductionsRemittanceAdvice[1];
        public SourceDeductionsReferenceIdentification3[] ref3 = new SourceDeductionsReferenceIdentification3[1];
        public SourceDeductionsDateTimeReference2[] dtm2 = new SourceDeductionsDateTimeReference2[1];

        public GarnishmentRepeatingElements()
        {
            n1pr2[0] = new SourceDeductionsPayorReceiverName2();
            rmt[0] = new GarnishmentSourceDeductionsRemittanceAdvice();
            ref3[0] = new SourceDeductionsReferenceIdentification3();
            dtm2[0] = new SourceDeductionsDateTimeReference2();
        }
    }

    [DelimitedRecord("*")]
    public class GarnishmentSourceDeductionsRemittanceAdvice
    {
        public string IdElement;
        public string ReferenceNumberQualifier;
        public string SIN;
        public string GarnishmentAmount;

        public GarnishmentSourceDeductionsRemittanceAdvice()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "RMT";
            ReferenceNumberQualifier = "TJ";
            SIN = "";
            GarnishmentAmount = "";
        }
    }
}
