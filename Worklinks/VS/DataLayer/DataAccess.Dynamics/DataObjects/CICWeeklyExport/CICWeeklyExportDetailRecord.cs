﻿using System;
using FileHelpers;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CICWeeklyExport
{
    [DelimitedRecord(",")]
    public class CICWeeklyExportDetailRecord
    {
        public string UniqueEmployeeIdentifier;             //EMPLOYEE NUMBER

        public string AuthenticationValuePrimary;           //SIN

        [FieldConverter(ConverterKind.Date, "MM-dd-yyyy")]  //BIRTH DATE
        public DateTime AuthenticationValueSecondary;

        public string ActiveTerminatedEmployee;             //Active or Terminated

        public CICWeeklyExportDetailRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            UniqueEmployeeIdentifier = null;
            AuthenticationValuePrimary = null;
            AuthenticationValueSecondary = DateTime.Now;
            ActiveTerminatedEmployee = null;
        }
    }
}
