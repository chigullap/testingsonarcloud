﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CitiAch
{
    [FixedLengthRecord()]
    public class CitiAchFileHeaderRecord
    {
        [FieldFixedLength(1)]
        public string RecordType;

        [FieldFixedLength(4)]
        public string FileCreationNumber;

        [FieldFixedLength(8)]
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime FileCreationDate;

        [FieldFixedLength(136)]
        public string Filler1;

        public CitiAchFileHeaderRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = "A";
            FileCreationNumber = "";
            FileCreationDate = DateTime.Now;
            Filler1 = "".PadRight(136);           
        }
    }
}
