﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CitiAch
{
    [FixedLengthRecord()]
    public class CitiAchEftDetailRecord
    {
        [FieldFixedLength(1)]
        public string RecordType;

        [FieldFixedLength(3)]
        public string TransactionTypeCode;

        [FieldFixedLength(10)]
        public string Amount;

        [FieldFixedLength(8)]
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime ValueDate;

        [FieldFixedLength(9)]
        public string PayeePayorInstitutionalID;

        [FieldFixedLength(12)]
        public string PayeePayorAccountNumber;

        [FieldFixedLength(30)]
        public string PayeePayorName;

        [FieldFixedLength(30)]
        public string OriginatorsLongName;

        [FieldFixedLength(15)]
        public string PaymentID;

        [FieldFixedLength(15)]
        public string OriginatorsShortName;

        [FieldFixedLength(16)]
        public string CustomerReferenceGrouping;

        public CitiAchEftDetailRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = "C";
            TransactionTypeCode = "200";
            Amount = "".PadLeft(10, '0');
            ValueDate = DateTime.Now;                       /* The value date must not be a Canadian Holiday or Saturday or Sunday. Credits must be future dated by at least one day, up to a maximum of 90 days. 
                                                               Debits may be current dated, or future dated up to a maximum of 90 days. No back dates are permitted. 
                                                               If your file is received after the daily cut-off time, your file will be processed on the next business day and all value dates will be 
                                                               advanced one business day */
            PayeePayorInstitutionalID = "0BBBTTTTT";        //The receiving bank (not Citibank Canada).Format: “0BBBTTTTT”, where:0 = ConstantBBB = Bank #TTTTT = Branch Tr. #
                                                            //Transactions with invalid institution ID’s will be rejected. 
            PayeePayorAccountNumber = "";                   //The account number at the receiving bank
            PayeePayorName = "".PadRight(30);               //The name of the account holder at the receiving bank
            OriginatorsLongName = "".PadRight(30);          //Name of the Citibank Canada customer who forwarded the transaction for processing.
            PaymentID = "".PadRight(15);
            OriginatorsShortName = "".PadRight(15);
            CustomerReferenceGrouping = "".PadRight(16);
        }
    }
}

