﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CitiAch
{
    [FixedLengthRecord()]
    public class CitiAchFileControlRecord
    {
        [FieldFixedLength(1)]
        public string RecordType;

        [FieldFixedLength(6)]
        public string BatchCount;

        [FieldFixedLength(142)]
        public string Filler1;

        public CitiAchFileControlRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = "Z";
            BatchCount = "1".PadLeft(6, '0');
            Filler1 = "".PadRight(142);           
        }
    }
}

