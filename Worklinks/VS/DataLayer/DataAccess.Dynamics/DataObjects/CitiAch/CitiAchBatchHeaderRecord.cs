﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CitiAch
{
    [FixedLengthRecord()]
    public class CitiAchBatchHeaderRecord
    {
        [FieldFixedLength(1)]
        public string RecordType;

        [FieldFixedLength(4)]
        public string CustomerId;

        [FieldFixedLength(144)]
        public string Filler1;

        public CitiAchBatchHeaderRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = "B";
            CustomerId = "";                    //unique identification number assigned to the customer by Citibank Canada.
            Filler1 = "".PadRight(144);           
        }
    }
}

