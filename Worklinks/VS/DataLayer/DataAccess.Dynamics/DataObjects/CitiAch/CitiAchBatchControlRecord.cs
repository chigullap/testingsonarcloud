﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.CitiAch
{
    [FixedLengthRecord()]
    public class CitiAchBatchControlRecord
    {
        [FieldFixedLength(1)]
        public string RecordType;

        [FieldFixedLength(14)]
        public string TotalValueOfDebit;

        [FieldFixedLength(8)]
        public string TotalDebitTransactions;

        [FieldFixedLength(14)]
        public string TotalValueOfCredit;

        [FieldFixedLength(8)]
        public string TotalCreditTransactions;

        [FieldFixedLength(104)]
        public string Filler1;

        public CitiAchBatchControlRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = "X";
            TotalValueOfDebit = "".PadLeft(14, '0');
            TotalDebitTransactions = "".PadLeft(8, '0');
            TotalValueOfCredit = "".PadLeft(14, '0');
            TotalCreditTransactions = "".PadLeft(8, '0');
            Filler1 = "".PadRight(104);           
        }
    }
}

