﻿using FileHelpers;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.REALLaborLevel
{
    [DelimitedRecord(",")]
    public class LaborLevelHeader
    {
        public string LevelHeader;
        public string EntryNameHeader;
        public string DescriptionHeader;
        public string StatusHeader;

        public LaborLevelHeader()
        {
            Clear();
        }
        protected void Clear()
        {
            LevelHeader = "Level";
            EntryNameHeader = "EntryName";
            DescriptionHeader = "Description";
            StatusHeader = "Status";
        }
    }

    [DelimitedRecord(",")]
    public class LaborLevelDetail
    {
        public string Level;
        public string EntryName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Description;
        public string Status;

        public LaborLevelDetail()
        {
            Clear();
        }

        protected void Clear()
        {
            Level = null;
            EntryName = null;
            Description = null;
            Status = null;
        }
    }
}
