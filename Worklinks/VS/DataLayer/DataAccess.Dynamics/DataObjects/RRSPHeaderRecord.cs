﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [FixedLengthRecord()]
    public class RRSPHeaderRecord
    {
        [FieldFixedLength(24)]
        public string HeaderType;

        public RRSPHeaderRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            HeaderType = "";           //value will come from code_system db table
        }
    }
}