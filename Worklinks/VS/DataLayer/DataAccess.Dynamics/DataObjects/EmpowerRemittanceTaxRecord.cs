﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [FixedLengthRecord()]
    public class EmpowerRemittanceTaxRecord
    {
        [FieldFixedLength(1)]
        public string RecordType;

        [FieldFixedLength(10)]
        public string PaymentResponsibility;

        [FieldFixedLength(10)]
        public string Residency;

        [FieldFixedLength(14)]
        public string TaxAmount;

        [FieldFixedLength(10)]
        public string CreditRollForwardDueDate;

        [FieldFixedLength(10)]
        public string Filler;

        [FieldFixedLength(40)]
        public string TranslationCode;

        [FieldFixedLength(10)]
        public String DueDate; //(Adjustment Record Only) will be spaces for our use

        public EmpowerRemittanceTaxRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = "T";
            PaymentResponsibility = "          ";
            Residency = "          ";
            TaxAmount = "";
            CreditRollForwardDueDate = "          ";
            Filler = "          ";
            TranslationCode = "";
            DueDate = "          ";
        }
    }
}
