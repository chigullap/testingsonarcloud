﻿using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.RbcEftEdi;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.PreAuthorizedDebit
{
    //this is the main class
    public class PreAuthorizedDebitDetails
    {
        //these classes are from the RbcEftEdi folder but not all classes were to be used so i created this class to only create what i needed and to override any defaults
        public IsaInterchangeControlHeader[] isaHeader = new IsaInterchangeControlHeader[1];
        public GsFunctionalGroupHeader[] gsHeader = new GsFunctionalGroupHeader[1];
        
        //these can appear multiple times.....make construtor take int and declare then
        public PreAuthorizedDebitAccountDetails[] padDetails;

        public GsFunctionalGroupTrailer[] geTrailer = new GsFunctionalGroupTrailer[1];
        public IsaInterchangeControlTrailer[] ieaTrailer = new IsaInterchangeControlTrailer[1];

        public PreAuthorizedDebitDetails(int numberOfAccounts)
        {
            //instantiate and overwrite any defaults that need to be changed for pre authorized debit process
            isaHeader[0] = new IsaInterchangeControlHeader();
            gsHeader[0] = new GsFunctionalGroupHeader();

            //create repeating elements
            padDetails = new PreAuthorizedDebitAccountDetails[numberOfAccounts];
            for (int i = 0; i < numberOfAccounts; i++)
                padDetails[i] = new PreAuthorizedDebitAccountDetails();

            geTrailer[0] = new GsFunctionalGroupTrailer() { NumberOfTransactionSetsIncluded = numberOfAccounts.ToString() };
            ieaTrailer[0] = new IsaInterchangeControlTrailer();
        }
    }

    //this class holds the repeating ST/SE and child segments
    public class PreAuthorizedDebitAccountDetails
    {
        public DirectDepositStTransactionSetHeader[] st = new DirectDepositStTransactionSetHeader[1];
        public DirectDepositBeginningSegmentForPaymentOrderRemittanceAdvice[] bpr = new DirectDepositBeginningSegmentForPaymentOrderRemittanceAdvice[1];
        public DirectDepositTrace[] trn = new DirectDepositTrace[1];
        public DirectDepositCurrency[] cur = new DirectDepositCurrency[1];
        public DirectDepositReferenceIdentification[] ref1 = new DirectDepositReferenceIdentification[1];
        public DirectDepositDateTimeReference[] dtm = new DirectDepositDateTimeReference[1];
        public DirectDepositOriginatorNamePayor[] n1pr = new DirectDepositOriginatorNamePayor[1];
        public DirectDepositPayeeReceiverName[] n1pe = new DirectDepositPayeeReceiverName[1];
        public DirectDepositSeTransactionSetTrailer[] se = new DirectDepositSeTransactionSetTrailer[1];

        public PreAuthorizedDebitAccountDetails()
        {
            st[0] = new DirectDepositStTransactionSetHeader();
            bpr[0] = new DirectDepositBeginningSegmentForPaymentOrderRemittanceAdvice() { CreditDebitFlagCode = "D" };
            trn[0] = new DirectDepositTrace();
            cur[0] = new DirectDepositCurrency();
            ref1[0] = new DirectDepositReferenceIdentification();
            dtm[0] = new DirectDepositDateTimeReference();
            n1pr[0] = new DirectDepositOriginatorNamePayor() { OriginatorPayorName = "Worklinks" };
            n1pe[0] = new DirectDepositPayeeReceiverName();
            se[0] = new DirectDepositSeTransactionSetTrailer() { NumberOfIncludedSegments = "9" };  //Pre authorized debit contains 1 less segment than direct deposit
        }
    }
}
