﻿using System;
using FileHelpers;
using WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.RbcEftEdi;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.RbcEftEdi
{
    public class DirectDepositUS //re-uses classes from DirectDeposit but has a few unique classes (diff field sizes) of its own
    {
        //declarations
        public DirectDepositStTransactionSetHeader[] st = new DirectDepositStTransactionSetHeader[1];
        public DirectDepositBeginningSegmentForPaymentOrderRemittanceAdvice[] bpr = new DirectDepositBeginningSegmentForPaymentOrderRemittanceAdvice[1];
        public UsDirectDepositTrace[] trnUs = new UsDirectDepositTrace[1];
        public UsDirectDepositCurrency[] curUs = new UsDirectDepositCurrency[1];
        public DirectDepositReferenceIdentification[] ref1 = new DirectDepositReferenceIdentification[1];
        public DirectDepositDateTimeReference[] dtm = new DirectDepositDateTimeReference[1];
        public DirectDepositOriginatorNamePayor[] n1pr = new DirectDepositOriginatorNamePayor[1];
        public DirectDepositAdditionalNameInformationPayor[] n2 = new DirectDepositAdditionalNameInformationPayor[1];
        public ChequePayeeReceiverAddressInformation[] n3 = new ChequePayeeReceiverAddressInformation[1];
        public PayeeReceiverGeographicLocation[] n4 = new PayeeReceiverGeographicLocation[1];
        public DirectDepositPayeeReceiverName[] n1pe = new DirectDepositPayeeReceiverName[1];
        public ChequePayeeReceiverAddressInformation[] n3Employer = new ChequePayeeReceiverAddressInformation[1];
        public PayeeReceiverGeographicLocation[] n4Employer = new PayeeReceiverGeographicLocation[1];
        public DirectDepositSeTransactionSetTrailer[] se = new DirectDepositSeTransactionSetTrailer[1];

        //constructor
        public DirectDepositUS()
        {
            st[0] = new DirectDepositStTransactionSetHeader();
            bpr[0] = new DirectDepositBeginningSegmentForPaymentOrderRemittanceAdvice() { PaymentMethodCode = "", ReceiverIdNumberQualifier = "01" };
            trnUs[0] = new UsDirectDepositTrace();
            curUs[0] = new UsDirectDepositCurrency();
            ref1[0] = new DirectDepositReferenceIdentification();
            dtm[0] = new DirectDepositDateTimeReference();
            n1pr[0] = new DirectDepositOriginatorNamePayor();
            n2[0] = new DirectDepositAdditionalNameInformationPayor();
            n3Employer[0] = new ChequePayeeReceiverAddressInformation();
            n4Employer[0] = new PayeeReceiverGeographicLocation();
            n1pe[0] = new DirectDepositPayeeReceiverName();
            n3[0] = new ChequePayeeReceiverAddressInformation();
            n4[0] = new PayeeReceiverGeographicLocation();
            se[0] = new DirectDepositSeTransactionSetTrailer() { NumberOfIncludedSegments = "14" };
        }
    }

    //classes that are unique in field size for US bank accounts
    [DelimitedRecord("*")]
    public class UsDirectDepositTrace
    {
        public string IdElement;
        public string TraceTypeCode;
        public string ReferenceIdentification;
        public string EmptySpot1;
        public string CrossBorderPaymentDescription;

        public UsDirectDepositTrace()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "TRN";
            TraceTypeCode = "1";
            ReferenceIdentification = "";       //Sequence number from database.  Unique number that ties back to the WL database at txn level.  Should be unique across files.  Does NOT need to be padded.
            EmptySpot1 = "";
            CrossBorderPaymentDescription = "";
        }
    }

    [DelimitedRecord("*")]
    public class UsDirectDepositCurrency
    {
        public string IdElement;
        public string EntityIdentifierCode;
        public string CurrencyCode;
        public string EmptySpot1;
        public string FixedValue1;
        public string FixedValue2;

        public UsDirectDepositCurrency()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "CUR";
            EntityIdentifierCode = "PR";
            CurrencyCode = "CAD";
            EmptySpot1 = "";
            FixedValue1 = "PE";
            FixedValue2 = "USD";
        }
    }
}
