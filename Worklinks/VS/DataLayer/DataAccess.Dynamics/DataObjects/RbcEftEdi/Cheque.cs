﻿using System;
using FileHelpers;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.RbcEftEdi
{
    public class Cheque //this class encapsulates everything below needed for cheques
    {
        public ChequeStTransactionSetHeader[] st = new ChequeStTransactionSetHeader[1];
        public ChequeBeginningSegmentForPaymentOrderRemittanceAdvice[] bpr = new ChequeBeginningSegmentForPaymentOrderRemittanceAdvice[1];
        public ChequeTrace[] trn = new ChequeTrace[1];
        public ChequeCurrency[] cur = new ChequeCurrency[1];
        public ChequeReferenceIdentification[] ref1 = new ChequeReferenceIdentification[1];
        public ChequeReferenceIdentification2[] ref2 = new ChequeReferenceIdentification2[1];
        public ChequeReferenceIdentification3[] ref3 = new ChequeReferenceIdentification3[1];
        public ChequeReferenceIdentification4[] ref4 = new ChequeReferenceIdentification4[1];
        public ChequeDateTimeReference[] dtm = new ChequeDateTimeReference[1];
        public ChequeOriginatorNamePayor[] n1pr = new ChequeOriginatorNamePayor[1];
        public ChequePayeeReceiverName[] n1pe = new ChequePayeeReceiverName[1];
        public ChequePayeeReceiverAddressInformation[] n3 = new ChequePayeeReceiverAddressInformation[1];
        public PayeeReceiverGeographicLocation[] n4 = new PayeeReceiverGeographicLocation[1];
        public ChequeEntity[] ent = new ChequeEntity[1];
        public ChequeRemittanceAdviceAccountsReceivableOpenItemReference[] rmr = new ChequeRemittanceAdviceAccountsReceivableOpenItemReference[1];
        public ChequeReferenceIdentification5[] ref5 = new ChequeReferenceIdentification5[1];
        public ChequeReferenceIdentification6[] ref6 = new ChequeReferenceIdentification6[1];
        public ChequeDateTimeReference2[] dtm2 = new ChequeDateTimeReference2[1];
        public ChequeSeTransactionSetTrailer[] se = new ChequeSeTransactionSetTrailer[1];        

        //optional so not instantiated in constructor
        public ChequePayeeReceiverAddressInformation2[] n3Part2 = new ChequePayeeReceiverAddressInformation2[1];
        public ChequeReferenceIdentificationCustom[] customLine1 = new ChequeReferenceIdentificationCustom[1];
        public ChequeReferenceIdentificationCustom[] customLine2 = new ChequeReferenceIdentificationCustom[1];
        public ChequeReferenceIdentificationCustom[] customLine3 = new ChequeReferenceIdentificationCustom[1];

        public Cheque()
        {
            st[0] = new ChequeStTransactionSetHeader();
            bpr[0] = new ChequeBeginningSegmentForPaymentOrderRemittanceAdvice();
            trn[0] = new ChequeTrace();
            cur[0] = new ChequeCurrency();
            ref1[0] = new ChequeReferenceIdentification();
            ref2[0] = new ChequeReferenceIdentification2();
            ref3[0] = new ChequeReferenceIdentification3();
            ref4[0] = new ChequeReferenceIdentification4();
            ref6[0] = new ChequeReferenceIdentification6();
            dtm[0] = new ChequeDateTimeReference();
            n1pr[0] = new ChequeOriginatorNamePayor();
            n1pe[0] = new ChequePayeeReceiverName();
            n3[0] = new ChequePayeeReceiverAddressInformation();
            n4[0] = new PayeeReceiverGeographicLocation();
            ent[0] = new ChequeEntity();
            rmr[0] = new ChequeRemittanceAdviceAccountsReceivableOpenItemReference();
            ref5[0] = new ChequeReferenceIdentification5();
            dtm2[0] = new ChequeDateTimeReference2();
            se[0] = new ChequeSeTransactionSetTrailer();            
        }
    }

    [DelimitedRecord("*")]
    public class ChequeStTransactionSetHeader
    {
        public string IdElement;
        public string TransactionSetIdentifierCode;
        public string TransactionSetControlNumber;

        public ChequeStTransactionSetHeader()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "ST";
            TransactionSetIdentifierCode = "820";
            TransactionSetControlNumber = "";       //We generate this number, unique per txn, use counter.  Padded with leading zeros to 9 chars
        }
    }

    [DelimitedRecord("*")]
    public class ChequeSeTransactionSetTrailer
    {
        public string IdElement;
        public string NumberOfIncludedSegments;
        public string TransactionSetControlNumber;

        public ChequeSeTransactionSetTrailer()
        {
            Clear();
        }
        protected void Clear()
        {
            IdElement = "SE";
            NumberOfIncludedSegments = "19";        //Total number of segments (row) included in a transaction set including ST and SE segments.
            TransactionSetControlNumber = "";       //same as StTransactionSetHeader.TransactionSetControlNumber
        }
    }

    [DelimitedRecord("*")]
    public class ChequeBeginningSegmentForPaymentOrderRemittanceAdvice
    {
        public string IdElement;
        public string TransactionHandlingCode;
        public string MonetaryAmount;
        public string CreditDebitFlagCode;
        public string PaymentMethodCode;
        public string EmptySpot1;
        public string OriginatorIdNumberQualifier;
        public string OriginatorDepositoryFinancialInstitutionIdentificationNumber;
        public string EmptySpot2;
        public string OriginatorAccountNumber;
        public string EmptySpot3;
        public string EmptySpot4;
        public string EmptySpot5;
        public string EmptySpot6;
        public string EmptySpot7;
        public string EmptySpot8;
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime PaymentDate;

        public ChequeBeginningSegmentForPaymentOrderRemittanceAdvice()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "BPR";
            TransactionHandlingCode = "N";
            MonetaryAmount = "";                                                //from payroll_master_payment.amount  Max 10 including 2 decimal places (99999999.99)
            CreditDebitFlagCode = "C";
            PaymentMethodCode = "CHK";
            EmptySpot1 = "";
            OriginatorIdNumberQualifier = "04";
            OriginatorDepositoryFinancialInstitutionIdentificationNumber = "";  //populated from code system (bank code (4 digit) + Branch #.  Our trust account FI + transit Originator)
            EmptySpot2 = "";
            OriginatorAccountNumber = "";                                       //populated from code system (Our trust account number)
            EmptySpot3 = "";
            EmptySpot4 = "";
            EmptySpot5 = "";
            EmptySpot6 = "";
            EmptySpot7 = "";
            EmptySpot8 = "";
            PaymentDate = DateTime.Now;                                         //payroll_master.cheque_date
        }
    }

    [DelimitedRecord("*")]
    public class ChequeTrace
    {
        public string IdElement;
        public string TraceTypeCode;
        public string ReferenceIdentification;

        public ChequeTrace()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "TRN";
            TraceTypeCode = "1";
            ReferenceIdentification = "";       //Sequence number from database.  Unique number that ties back to the WL database at txn level.  Should be unique across files.  Does NOT need to be padded.
        }
    }

    [DelimitedRecord("*")]
    public class ChequeCurrency
    {
        public string IdElement;
        public string EntityIdentifierCode;
        public string CurrencyCode;

        public ChequeCurrency()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "CUR";
            EntityIdentifierCode = "PR";
            CurrencyCode = "CAD";
        }
    }

    [DelimitedRecord("*")]
    public class ChequeReferenceIdentification
    {
        public string IdElement;
        public string ReferenceIdentificationQualifier;
        public string ReferenceId;

        public ChequeReferenceIdentification()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "REF";
            ReferenceIdentificationQualifier = "23";
            ReferenceId = "";                           //substring of IsaInterchangeControlHeader.SenderInterchangeID.  First 9 chars
        }
    }

    [DelimitedRecord("*")]
    public class ChequeReferenceIdentification2
    {
        public string IdElement;
        public string ReferenceIdentificationQualifier;
        public string ReferenceId;

        public ChequeReferenceIdentification2()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "REF";
            ReferenceIdentificationQualifier = "S5";
            ReferenceId = "";                           //populated from code system.  Either 000 = MAIL CHEQUE, 999 = COURIER BACK TO OFFICE.  Assume 000.
        }
    }

    [DelimitedRecord("*")]
    public class ChequeReferenceIdentification3
    {
        public string IdElement;
        public string ReferenceIdentificationQualifier;
        public string ReferenceId;

        public ChequeReferenceIdentification3()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "REF";
            ReferenceIdentificationQualifier = "CK";
            ReferenceId = "";                           //payroll_master_payment.cheque_number
        }
    }

    [DelimitedRecord("*")]
    public class ChequeReferenceIdentification4
    {
        public string IdElement;
        public string ReferenceIdentificationQualifier;
        public string ReferenceId;
        public string DescriptionPayeeMatch;

        public ChequeReferenceIdentification4()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "REF";
            ReferenceIdentificationQualifier = "ACE";
            ReferenceId = "300";
            DescriptionPayeeMatch = "";
        }
    }

    //optional
    [DelimitedRecord("*")]
    public class ChequeReferenceIdentificationCustom
    {
        public string IdElement;
        public string ReferenceIdentificationQualifier;
        public string ReferenceIdentification;
        public string Description;

        public ChequeReferenceIdentificationCustom()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "REF";
            ReferenceIdentificationQualifier = "EM";
            ReferenceIdentification = "CHQ";
            Description = "";         //populated from code system.
        }
    }

    [DelimitedRecord("*")]
    public class ChequeReferenceIdentification6
    {
        public string IdElement;
        public string ReferenceIdentificationQualifier;
        public string Description;

        public ChequeReferenceIdentification6()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "REF";
            ReferenceIdentificationQualifier = "XM";
            Description = "";         //populated from code system.
        }
    }

    [DelimitedRecord("*")]
    public class ChequeDateTimeReference
    {
        public string IdElement;
        public string DateTimeQualifier;
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime Date;

        public ChequeDateTimeReference()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "DTM";
            DateTimeQualifier = "097";
            Date = DateTime.Now;
        }
    }

    [DelimitedRecord("*")]
    public class ChequeOriginatorNamePayor
    {
        public string IdElement;
        public string EntityIdentifierCode;
        public string OriginatorPayorName;

        public ChequeOriginatorNamePayor()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "N1";
            EntityIdentifierCode = "PR";
            OriginatorPayorName = "";               //populated from code system
        }
    }

    [DelimitedRecord("*")]
    public class ChequePayeeReceiverName
    {
        public string IdElement;
        public string EntityIdentifierCode;
        public string PayeeName;
        public string IdentificationCodeQualifier;
        public string IdentificationCode;

        public ChequePayeeReceiverName()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "N1";
            EntityIdentifierCode = "PE";
            PayeeName = "";                     //from person table
            IdentificationCodeQualifier = "8";
            IdentificationCode = "";
        }
    }

    [DelimitedRecord("*")]
    public class ChequePayeeReceiverAddressInformation
    {
        public string IdElement;
        public string AddressInformationLine1;

        public ChequePayeeReceiverAddressInformation()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "N3";
            AddressInformationLine1 = "";                //max 35 chars.  address line 1
        }
    }

    //optional
    [DelimitedRecord("*")]
    public class ChequePayeeReceiverAddressInformation2
    {
        public string IdElement;
        public string AddressInformationLine3;

        public ChequePayeeReceiverAddressInformation2()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "N3";
            AddressInformationLine3 = "";                //max 35 chars.  address line 3
        }
    }

    [DelimitedRecord("*")]
    public class PayeeReceiverGeographicLocation
    {
        public string IdElement;
        public string ReceiverCityName;
        public string ReceiverStateProvinceCode;
        public string ReceiverStatePostalCode;
        public string ReceiverCountryCode;

        public PayeeReceiverGeographicLocation()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "N4";
            ReceiverCityName = "";              //address.city
            ReceiverStateProvinceCode = "";     //address.code_province_state_cd
            ReceiverStatePostalCode = "";       //address.postal_zip_code
            ReceiverCountryCode = "";           //address.code_country_cd
        }
    }

    [DelimitedRecord("*")]
    public class ChequeEntity
    {
        public string IdElement;
        public string AssignedNumber;
        public ChequeEntity()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "ENT";
            AssignedNumber = "";            //sequence number from db
        }
    }

    [DelimitedRecord("*")]
    public class ChequeRemittanceAdviceAccountsReceivableOpenItemReference
    {
        public string IdElement;
        public string EmptySpot1;
        public string EmptySpot2;
        public string EmptySpot3;
        public string MonetaryAmount;

        public ChequeRemittanceAdviceAccountsReceivableOpenItemReference()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "RMR";
            EmptySpot1 = "";
            EmptySpot2 = "";
            EmptySpot3 = "";
            MonetaryAmount = "";            //from payroll_master_payment.amount  Max 10 including 2 decimal places (99999999.99)
        }
    }

    [DelimitedRecord("*")]
    public class ChequeReferenceIdentification5
    {
        public string IdElement;
        public string ReferenceIdentificationQualifier;
        public string ReferenceIdentification;
        public string Description;

        public ChequeReferenceIdentification5()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "REF";
            ReferenceIdentificationQualifier = "EM";
            ReferenceIdentification = "CHQ";
            Description = "";
        }
    }

    [DelimitedRecord("*")]
    public class ChequeDateTimeReference2
    {
        public string IdElement;
        public string DateTimeQualifier;
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime Date;

        public ChequeDateTimeReference2()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "DTM";
            DateTimeQualifier = "003";
            Date = DateTime.Now;
        }
    }
}
