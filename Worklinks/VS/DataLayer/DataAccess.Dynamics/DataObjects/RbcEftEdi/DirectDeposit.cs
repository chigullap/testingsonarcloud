﻿using System;
using FileHelpers;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.RbcEftEdi
{
    public class DirectDeposit //this class encapsulates everything below needed for direct deposit
    {
        public DirectDepositStTransactionSetHeader[] st = new DirectDepositStTransactionSetHeader[1];
        public DirectDepositBeginningSegmentForPaymentOrderRemittanceAdvice[] bpr = new DirectDepositBeginningSegmentForPaymentOrderRemittanceAdvice[1];
        public DirectDepositTrace[] trn = new DirectDepositTrace[1];
        public DirectDepositCurrency[] cur = new DirectDepositCurrency[1];
        public DirectDepositReferenceIdentification[] ref1 = new DirectDepositReferenceIdentification[1];
        public DirectDepositDateTimeReference[] dtm = new DirectDepositDateTimeReference[1];
        public DirectDepositOriginatorNamePayor[] n1pr = new DirectDepositOriginatorNamePayor[1];
        public DirectDepositPayeeReceiverName[] n1pe = new DirectDepositPayeeReceiverName[1];
        public DirectDepositAdditionalNameInformationPayor[] n2 = new DirectDepositAdditionalNameInformationPayor[1];
        public DirectDepositSeTransactionSetTrailer[] se = new DirectDepositSeTransactionSetTrailer[1];

        public DirectDeposit()
        {
            st[0] = new DirectDepositStTransactionSetHeader();
            bpr[0] = new DirectDepositBeginningSegmentForPaymentOrderRemittanceAdvice();
            trn[0] = new DirectDepositTrace();
            cur[0] = new DirectDepositCurrency();
            ref1[0] = new DirectDepositReferenceIdentification();
            dtm[0] = new DirectDepositDateTimeReference();
            n1pr[0] = new DirectDepositOriginatorNamePayor();
            n2[0] = new DirectDepositAdditionalNameInformationPayor();
            n1pe[0] = new DirectDepositPayeeReceiverName();
            se[0] = new DirectDepositSeTransactionSetTrailer();
        }
    }

    [DelimitedRecord("*")]
    public class DirectDepositStTransactionSetHeader
    {
        public string IdElement;
        public string TransactionSetIdentifierCode;
        public string TransactionSetControlNumber;

        public DirectDepositStTransactionSetHeader()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "ST";
            TransactionSetIdentifierCode = "820";
            TransactionSetControlNumber = "";       //We generate this number, unique per txn, use counter.  Padded with leading zeros to 9 chars
        }
    }

    [DelimitedRecord("*")]
    public class DirectDepositSeTransactionSetTrailer
    {
        public string IdElement;
        public string NumberOfIncludedSegments;
        public string TransactionSetControlNumber;

        public DirectDepositSeTransactionSetTrailer()
        {
            Clear();
        }
        protected void Clear()
        {
            IdElement = "SE";
            NumberOfIncludedSegments = "10";         //Total number of segments (row) included in a transaction set including ST and SE segments
            TransactionSetControlNumber = "";       //same as StTransactionSetHeader.TransactionSetControlNumber
        }
    }

    [DelimitedRecord("*")]
    public class DirectDepositBeginningSegmentForPaymentOrderRemittanceAdvice
    {
        public string IdElement;
        public string TransactionHandlingCode;
        public string MonetaryAmount;
        public string CreditDebitFlagCode;
        public string PaymentMethodCode;
        public string EmptySpot1;
        public string OriginatorIdNumberQualifier;
        public string OriginatorDepositoryFinancialInstitutionIdentificationNumber;
        public string EmptySpot2;
        public string OriginatorAccountNumber;
        public string EmptySpot3;
        public string EmptySpot4;
        public string ReceiverIdNumberQualifier;
        public string ReceiverDepositoryFinancialInstitutionIdentificationNumber;
        public string EmptySpot5;
        public string ReceiverAccountNumber;
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime PaymentDate;

        public DirectDepositBeginningSegmentForPaymentOrderRemittanceAdvice()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "BPR";
            TransactionHandlingCode = "D";
            MonetaryAmount = "";                                                //from payroll_master_payment.amount  Max 10 including 2 decimal places (99999999.99)
            CreditDebitFlagCode = "C";
            PaymentMethodCode = "CDA";
            EmptySpot1 = "";
            OriginatorIdNumberQualifier = "04";
            OriginatorDepositoryFinancialInstitutionIdentificationNumber = "";  //populated from code system (bank code (4 digit) + Branch #.  Our trust account FI + transit Originator)
            EmptySpot2 = "";
            OriginatorAccountNumber = "";                                       //populated from code system (Our trust account number)
            EmptySpot3 = "";
            EmptySpot4 = "";
            ReceiverIdNumberQualifier = "04";
            ReceiverDepositoryFinancialInstitutionIdentificationNumber = "";    //from payroll_master_payment.  employee bank code (4 chars leading 0s) + transit # (5 chars)
            EmptySpot5 = "";
            ReceiverAccountNumber = "";                                         //from payroll_master_payment.  employee account number
            PaymentDate = DateTime.Now;                                         //payroll_master.cheque_date
        }
    }

    [DelimitedRecord("*")]
    public class DirectDepositTrace
    {
        public string IdElement;
        public string TraceTypeCode;
        public string ReferenceIdentification;

        public DirectDepositTrace()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "TRN";
            TraceTypeCode = "1";
            ReferenceIdentification = "";       //Sequence number from database.  Unique number that ties back to the WL database at txn level.  Should be unique across files.  Does NOT need to be padded.
        }
    }

    [DelimitedRecord("*")]
    public class DirectDepositCurrency
    {
        public string IdElement;
        public string EntityIdentifierCode;
        public string CurrencyCode;

        public DirectDepositCurrency()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "CUR";
            EntityIdentifierCode = "PR";
            CurrencyCode = "CAD";
        }
    }

    [DelimitedRecord("*")]
    public class DirectDepositReferenceIdentification
    {
        public string IdElement;
        public string ReferenceIdentificationQualifier;
        public string ReferenceId;

        public DirectDepositReferenceIdentification()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "REF";
            ReferenceIdentificationQualifier = "8M";
            ReferenceId = "";                           //populated from code system
        }
    }

    [DelimitedRecord("*")]
    public class DirectDepositDateTimeReference
    {
        public string IdElement;
        public string DateTimeQualifier;
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime Date;

        public DirectDepositDateTimeReference()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "DTM";
            DateTimeQualifier = "097";
            Date = DateTime.Now;
        }
    }

    [DelimitedRecord("*")]
    public class DirectDepositOriginatorNamePayor
    {
        public string IdElement;
        public string EntityIdentifierCode;
        public string OriginatorPayorName;

        public DirectDepositOriginatorNamePayor()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "N1";
            EntityIdentifierCode = "PR";
            OriginatorPayorName = "";               //populated from code system
        }
    }

    [DelimitedRecord("*")]
    public class DirectDepositAdditionalNameInformationPayor
    {
        public string IdElement;
        public string Name;

        public DirectDepositAdditionalNameInformationPayor()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "N2";
            Name = "";
        }
    }

    [DelimitedRecord("*")]
    public class DirectDepositPayeeReceiverName
    {
        public string IdElement;
        public string EntityIdentifierCode;
        public string PayeeName;

        public DirectDepositPayeeReceiverName()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "N1";
            EntityIdentifierCode = "PE";
            PayeeName = "";                     //from person table
        }
    }
}
