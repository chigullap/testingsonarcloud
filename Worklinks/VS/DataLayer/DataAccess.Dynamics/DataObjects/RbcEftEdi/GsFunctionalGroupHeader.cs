﻿using System;
using FileHelpers;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.RbcEftEdi
{
    [DelimitedRecord("*")]
    public class GsFunctionalGroupHeader
    {
        public string IdElement;
        public string FunctionalIdentifierCode;
        public string SenderInterchangeID;
        public string ApplicationReceiversCode;
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime Date;
        [FieldConverter(ConverterKind.Date, "HHmmss")]
        public DateTime Time;
        public string GroupControlNumber;
        public string ResponsibleAgencyCode;
        public string VersionReleaseIndustryIdentifierCode;

        public GsFunctionalGroupHeader()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "GS";
            FunctionalIdentifierCode = "RA";
            SenderInterchangeID = "";                   //populated from code system, same as IsaInterchangeControlHeader.SenderInterchangeID
            ApplicationReceiversCode = "";              //populated from code system, same as IsaInterchangeControlHeader.ReceiverInterchangeID
            Date = DateTime.Now;
            Time = DateTime.Now;
            GroupControlNumber = "";                    //sequence from db, It must be unique per file.  Does NOT need to be sequential.  4 digits max, no leading 0.
            ResponsibleAgencyCode = "X";
            VersionReleaseIndustryIdentifierCode = "004010";
        }
    }

    [DelimitedRecord("*")]
    public class GsFunctionalGroupTrailer
    {
        public string IdElement;
        public string NumberOfTransactionSetsIncluded;
        public string GroupControlNumber;

        public GsFunctionalGroupTrailer()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "GE";
            NumberOfTransactionSetsIncluded = "";       //Number of ST/SE pairs
            GroupControlNumber = "";                    //sequence from db, same as GsFunctionalGroupHeader.GroupControlNumber
        }
    }
}
