﻿using System;
using FileHelpers;


namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.ScotiaBankEdi
{
    [DelimitedRecord("*")]
    public class GS
    {
        public string SegmentId;
        public string GS01;
        public string GS02;
        public string GS03;
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime GS04;
        [FieldConverter(ConverterKind.Date, "HHmm")]
        public DateTime GS05;
        public string GS06;
        public string GS07;
        public string GS08;

        public GS()
        {
            Clear();
        }

        protected void Clear()
        {
            SegmentId = "GS";
            GS01 = "RA";
            GS02 = "";              //populated from code system, same as ISA.ISA06
            GS03 = "";              //populated from code system, same as ISA.ISA08
            GS04 = DateTime.Now;
            GS05 = DateTime.Now;
            GS06 = "";              //sequence from db, can be same as ISA13 if just one GS/GE pair.
            GS07 = "X";
            GS08 = "004010";
        }
    }

    [DelimitedRecord("*")]
    public class GE
    {
        public string SegmentId;
        public string GE01;
        public string GE02;

        public GE()
        {
            Clear();
        }

        protected void Clear()
        {
            SegmentId = "GE";
            GE01 = "";       //Number of ST/SE pairs
            GE02 = "";       //sequence from db, same as GS.GS06
        }
    }
}
