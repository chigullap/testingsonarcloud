﻿using System;
using FileHelpers;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.ScotiaBankEdi
{
    //main class to hold all the direct deposit classes
    public class ScotiaDirectDeposit
    {
        public ST[] st = new ST[1];
        public BPR[] bpr = new BPR[1];
        public NTE[] nte= new NTE[1];
        public TRN[] trn = new TRN[1];
        public CUR[] cur = new CUR[1];
        public DTM[] dtm = new DTM[1];
        public REF[] ref1 = new REF[1];
        public N1PR[] n1pr = new N1PR[1];
        public N2[] n2 = new N2[1];
        public N1PE[] n1pe = new N1PE[1];
        public N1Orig[] n1Orig = new N1Orig[1];
        public N4Orig[] n4Orig = new N4Orig[1];
        public N1RB[] n1rb = new N1RB[1];
        public N4RB[] n4rb = new N4RB[1];
        public SE[] se = new SE[1];

        public ScotiaDirectDeposit()
        {
            st[0] = new ST();
            bpr[0] = new BPR();
            nte[0] = new NTE();
            trn[0] = new TRN();
            cur[0] = new CUR();
            dtm[0] = new DTM();
            ref1[0] = new REF();
            n1pr[0] = new N1PR();
            n2[0] = new N2();
            n1pe[0] = new N1PE();
            n1Orig[0] = new N1Orig();
            n4Orig[0] = new N4Orig();
            n1rb[0] = new N1RB();
            n4rb[0] = new N4RB();
            se[0] = new SE();
        }
    }

    #region single classes
    [DelimitedRecord("*")]
    public class ST
    {
        public string SegmentId;
        public string ST01;
        public string ST02;

        public ST()
        {
            Clear();
        }

        protected void Clear()
        {
            SegmentId = "ST";
            ST01 = "820";
            ST02 = "";       //We generate this number, unique per txn, use counter.  Padded with leading zeros to 9 chars
        }
    }

    [DelimitedRecord("*")]
    public class BPR
    {
        public string SegmentId;
        public string BPR01;
        public string BPR02;
        public string BPR03;
        public string BPR04;
        public string BPR05;
        public string BPR06;
        public string BPR07;
        public string BPR08;
        public string BPR09;
        public string BPR10;
        public string BPR11;
        public string BPR12;
        public string BPR13;
        public string BPR14;
        public string BPR15;
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime BPR16;
        public string BPR17;

        public BPR()
        {
            Clear();
        }

        protected void Clear()
        {
            SegmentId = "BPR";
            BPR01 = "D";
            BPR02 = "";            //from payroll_master_payment.amount  Max 10 including 2 decimal places (99999999.99)
            BPR03 = "C";
            BPR04 = "ACH";
            BPR05 = "PPD";
            BPR06 = "01";
            BPR07 = "";            //populated from code system. For this GFT payment going to Jamaica, it will be a 9 digits ABA number
            BPR08 = "DA";
            BPR09 = "";            //populated from code system (Our trust account number)????
            BPR10 = "";
            BPR11 = "";
            BPR12 = "01";
            BPR13 = "";            //it will be a 9 digits ABA number.  **For canada, it was from payroll_master_payment.  employee bank code (4 chars leading 0s) + transit # (5 chars)
            BPR14 = "";
            BPR15 = "";            //from payroll_master_payment.  employee account number
            BPR16 = DateTime.Now;  //payroll_master.cheque_date
            BPR17 = "VEN";
        }
    }

    [DelimitedRecord("*")]
    public class NTE
    {
        public string SegmentId;
        public string NTE01;
        public string NTE02;

        public NTE()
        {
            Clear();
        }

        protected void Clear()
        {
            SegmentId = "NTE";
            NTE01 = "OBI";
            NTE02 = "PAYMENT";
        }
    }

    [DelimitedRecord("*")]
    public class TRN
    {
        public string SegmentId;
        public string TRN01;
        public string TRN02;

        public TRN()
        {
            Clear();
        }

        protected void Clear()
        {
            SegmentId = "TRN";
            TRN01 = "1";
            TRN02 = "";                 //Sequence number from database.  Unique number that ties back to the WL database at txn level.  Should be unique across files.  Does NOT need to be padded.
        }
    }

    [DelimitedRecord("*")]
    public class CUR
    {
        public string SegmentId;
        public string CUR01;
        public string CUR02;

        public CUR()
        {
            Clear();
        }

        protected void Clear()
        {
            SegmentId = "CUR";
            CUR01 = "BK";
            CUR02 = "JMD";
        }
    }

    [DelimitedRecord("*")]
    public class REF
    {
        public string SegmentId;
        public string REF01;
        public string REF02;

        public REF()
        {
            Clear();
        }

        protected void Clear()
        {
            SegmentId = "REF";
            REF01 = "4N";
            REF02 = "JM";
        }
    }

    [DelimitedRecord("*")]
    public class DTM
    {
        public string IdElement;
        public string DateTimeQualifier;
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime Date;

        public DTM()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "DTM";
            DateTimeQualifier = "097";
            Date = DateTime.Now;
        }
    }

    [DelimitedRecord("*")]
    public class N1PR
    {
        public string SegmentId;
        public string N1PR01;
        public string N1PR02;

        public N1PR()
        {
            Clear();
        }

        protected void Clear()
        {
            SegmentId = "N1";
            N1PR01 = "PR";
            N1PR02 = "";               //populated from code system
        }
    }

    [DelimitedRecord("*")]
    public class N2
    {
        public string SegmentId;
        public string N201;

        public N2()
        {
            Clear();
        }

        protected void Clear()
        {
            SegmentId = "N2";
            N201 = "";              //populated from code system
        }
    }

    [DelimitedRecord("*")]
    public class N1PE
    {
        public string SegmentId;
        public string N1PE01;
        public string N1PE02;

        public N1PE()
        {
            Clear();
        }

        protected void Clear()
        {
            SegmentId = "N1";
            N1PE01 = "PE";
            N1PE02 = "";                     //from person table
        }
    }

    [DelimitedRecord("*")]
    public class N1Orig
    {
        public string SegmentId;
        public string N1Orig01;
        public string N1Orig02;

        public N1Orig()
        {
            Clear();
        }

        protected void Clear()
        {
            SegmentId = "N1";
            N1Orig01 = "O1";
            N1Orig02 = "WORKLINKS";
        }
    }

    [DelimitedRecord("*")]
    public class N4Orig
    {
        public string SegmentId;
        public string N4Orig01;
        public string N4Orig02;
        public string N4Orig03;
        public string N4Orig04;

        public N4Orig()
        {
            Clear();
        }

        protected void Clear()
        {
            SegmentId = "N4";
            N4Orig01 = "";
            N4Orig02 = "";
            N4Orig03 = "";
            N4Orig04 = "JM";
        }
    }

    [DelimitedRecord("*")]
    public class N1RB
    {
        public string SegmentId;
        public string N1RB01;
        public string N1RB02;

        public N1RB()
        {
            Clear();
        }

        protected void Clear()
        {
            SegmentId = "N1";
            N1RB01 = "RB";
            N1RB02 = "";
        }
    }

    [DelimitedRecord("*")]
    public class N4RB
    {
        public string SegmentId;
        public string N4RB01;
        public string N4RB02;
        public string N4RB03;
        public string N4RB04;

        public N4RB()
        {
            Clear();
        }

        protected void Clear()
        {
            SegmentId = "N4";
            N4RB01 = "";
            N4RB02 = "";
            N4RB03 = "";
            N4RB04 = "JM";
        }
    }

    [DelimitedRecord("*")]
    public class SE
    {
        public string SegmentId;
        public string SE01;
        public string SE02;

        public SE()
        {
            Clear();
        }
        protected void Clear()
        {
            SegmentId = "SE";
            SE01 = "15";         //Total number of segments (row) included in a transaction set including ST and SE segments
            SE02 = "";          //same as StTransactionSetHeader.TransactionSetControlNumber
        }
    }
    #endregion
}
