﻿
namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.ScotiaBankEdi
{
    //this is the main class
    public class ScotiaBankPreAuthorizedDebit
    {
        //these classes are from the ScotiaBankEdi folder but not all classes were to be used so i created this class to only create what i needed and to override any defaults
        public ISA[] isa = new ISA[1];
        public GS[] gs = new GS[1];

        //these can appear multiple times.....make construtor take int and declare then
        public ScotiaBankPreAuthorizedDebitAccountDetails[] padDetails;

        public GE[] ge = new GE[1];
        public IEA[] iea = new IEA[1];

        public ScotiaBankPreAuthorizedDebit(int numberOfAccounts)
        {
            //instantiate and overwrite any defaults that need to be changed for pre authorized debit process
            isa[0] = new ISA();
            gs[0] = new GS();

            //create repeating elements
            padDetails = new ScotiaBankPreAuthorizedDebitAccountDetails[numberOfAccounts];
            for (int i = 0; i < numberOfAccounts; i++)
                padDetails[i] = new ScotiaBankPreAuthorizedDebitAccountDetails();

            ge[0] = new GE() { GE01 = numberOfAccounts.ToString() };
            iea[0] = new IEA();
        }
    }

    //this class holds the repeating ST/SE and child segments
    public class ScotiaBankPreAuthorizedDebitAccountDetails
    {
        public ST[] st = new ST[1];
        public BPR[] bpr = new BPR[1];
        public NTE[] nte = new NTE[1];
        public TRN[] trn = new TRN[1];
        public CUR[] cur = new CUR[1];
        public DTM[] dtm = new DTM[1];
        public REF[] ref1 = new REF[1];
        public N1PR[] n1pr = new N1PR[1];
        public N1PE[] n1pe = new N1PE[1];
        public N1Orig[] n1Orig = new N1Orig[1];
        public N4Orig[] n4Orig = new N4Orig[1];
        public N1RB[] n1rb = new N1RB[1];
        public N4RB[] n4rb = new N4RB[1];
        public SE[] se = new SE[1];

        public ScotiaBankPreAuthorizedDebitAccountDetails()
        {
            st[0] = new ST();
            bpr[0] = new BPR() { BPR03 = "D", BPR17 = "DBT" };
            nte[0] = new NTE() { NTE02 = "PREAUTHORIZED DEBIT" };
            trn[0] = new TRN();
            cur[0] = new CUR();
            dtm[0] = new DTM();
            ref1[0] = new REF();
            n1pr[0] = new N1PR() { N1PR02 = "Worklinks" };
            n1pe[0] = new N1PE();               //company short name
            n1Orig[0] = new N1Orig();
            n4Orig[0] = new N4Orig();
            n1rb[0] = new N1RB();               //company short name
            n4rb[0] = new N4RB();
            se[0] = new SE() { SE01 = "14" };   //Pre authorized debit contains 1 less segment than direct deposit
        }
    }
}
