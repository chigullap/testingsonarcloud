﻿using System;
using FileHelpers;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.ScotiaBankEdi
{
    //header
    [DelimitedRecord("*")]
    public class ISA
    {
        public string SegmentId;
        public string ISA01;
        public string ISA02;
        public string ISA03;
        public string ISA04;
        public string ISA05;
        public string ISA06;
        public string ISA07;
        public string ISA08;
        [FieldConverter(ConverterKind.Date, "yyMMdd")]
        public DateTime ISA09;
        [FieldConverter(ConverterKind.Date, "HHmm")]
        public DateTime ISA10;
        public string ISA11;
        public string ISA12;
        public string ISA13;
        public string ISA14;
        public string ISA15;
        public string ISA16;

        public ISA()
        {
            Clear();
        }

        protected void Clear()
        {
            SegmentId = "ISA";
            ISA01 = "00";
            ISA02 = "".PadRight(10);
            ISA03 = "00";
            ISA04 = "".PadRight(10);
            ISA05 = "ZZ";
            ISA06 = "";                     //populated from code system?????
            ISA07 = "ZZ";
            ISA08 = "";                     //populated from code system?????
            ISA09 = DateTime.Now;
            ISA10 = DateTime.Now;
            ISA11 = "U";
            ISA12 = "00401";
            ISA13 = "";                     //sequence from db
            ISA14 = "0";
            ISA15 = "";                     //populated from code system
            ISA16 = ">";
        }
    }

    //trailer
    [DelimitedRecord("*")]
    public class IEA
    {
        public string SegmentId;
        public string IEA01;
        public string IEA02;

        public IEA()
        {
            Clear();
        }

        protected void Clear()
        {
            SegmentId = "IEA";
            IEA01 = "1";
            IEA02 = "";                  //sequence from db, same as ISA.ISA13
        }
    }

}

