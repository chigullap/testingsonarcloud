﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [FixedLengthRecord()]
    public class EftEntryDetailRecord
    {
        [FieldFixedLength(1)]
        public int RecordType;

        [FieldFixedLength(2)]
        public string TransactionCode;

        [FieldFixedLength(4)]
        public string CANBankNumber;            //in eft file, this is a numeric...string here so we can keep the leading "0"

        [FieldFixedLength(5)]
        public string CANBranchTransitNumber;   //in eft file,  this is alphanumeric but RBC only accepts numeric.  It is a string here incase there are leading 0's.

        [FieldFixedLength(17)]
        public string AccountNumber;

        [FieldFixedLength(10)]
        public string Amount;

        [FieldFixedLength(15)]
        public string CustomerNumber;

        [FieldFixedLength(22)]
        public string CustomerName;

        [FieldFixedLength(2)]
        public string DestinationCountry;

        [FieldFixedLength(1)]
        public string AddendaRecordIndicator;

        [FieldFixedLength(15)]
        public string USDTraceNumber;       //in eft file, this is a numeric...string here so we can leave a blank in its place
        
        public EftEntryDetailRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = 6;
            TransactionCode = "  ";                     //(2 chars, 22-demandCredit, 23 Demand pre-note, 32 savings credit, 33 savings pre-note).  
                                                        //For payments destined for canada, the CPA transaction code from your profile will default on all CA destined payments
            CANBankNumber = "0";                        //0999 - financial institution number
            CANBranchTransitNumber = "";                //99999 - financial branch number institution
            AccountNumber = "0";                        //Account number.  (17 chars)customer acct num, left justified, DO NOT ZERO FILL, enter sig digits only
            Amount = "0";                               //format $$$$$$$$cc (10 chars)
            CustomerNumber = "               ";         //client assigned, left justify
            CustomerName = "                      ";    //mandatory
            DestinationCountry = "CA";                  //MANDATORY - CA or US
            AddendaRecordIndicator = "0";               //IGNORED FOR PAYS TO CDN BANK but still needs to have a value when uploading
            USDTraceNumber = "               ";         //If filled, stored on GRADS system in the Electronic Message field.  Will be part of returns data file.
                                                        //This information is passed to FI's in Canada
        }
    }
}
