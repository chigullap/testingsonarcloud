﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [FixedLengthRecord()]
    public class FirstIatAddendaRecord
    {
        [FieldFixedLength(1)]
        public int RecordType;

        [FieldFixedLength(2)]
        public int AddendumTypeCode;

        [FieldFixedLength(3)]
        public string TransactionTypeCode;

        [FieldFixedLength(18)]
        public string ForeignPaymentAmount;     //in eft file, this is a numeric...string here so we can pad with "0's"

        [FieldFixedLength(22)]
        public string Filler6;

        [FieldFixedLength(35)]
        public string CustomerReceiverName;

        [FieldFixedLength(6)]
        public string Filler7;

        [FieldFixedLength(7)]
        public string EntryDetailSequenceNumber;

        public FirstIatAddendaRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = 7;
            AddendumTypeCode = 10;
            TransactionTypeCode = "200";                // MANDATORY - set to 200 (= payroll)
            ForeignPaymentAmount = "000000000000000000";//Zero fill or enter the Canadian $$ amount if converted. $$$$$$$$$$$$$$$$cc
            Filler6 = "                      ";         //leave blank
            CustomerReceiverName = "Worklinks Inc                      ";//(35 chars, but last 5 truncd by RBC) Name of customer or company.  No special chars.
            Filler7 = "      ";
            EntryDetailSequenceNumber = "       ";      //(7 chars, same as the last 7 digits of the trace number of the related Entry Detail Record, or leave blank)			
        }
    }
}
