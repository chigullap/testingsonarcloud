﻿
using FileHelpers;
using System;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [DelimitedRecord("\t")]
    public class GLSapExportHeader
    {
        public string PY_IND_HEADER_IND;

        [FieldConverter(ConverterKind.Date, "MM/dd/yyyy")]
        public DateTime PY_IN_HEADER_PR_DT;

        public string PY_IN_HEADER_TEXT;

        public GLSapExportHeader()
        {
            Clear();
        }

        protected void Clear()
        {
            PY_IND_HEADER_IND = "HEADER";
            PY_IN_HEADER_PR_DT = DateTime.Now;
            PY_IN_HEADER_TEXT = "FILE CONTAINS DATA FOR CA PAYROLL PROCESSING";
        }
    }

    [DelimitedRecord("\t")]
    public class GLSapExport
    {
        public string PY_IN_LINE_NO;
        public string PY_IN_BKPF_BUKRS;

        [FieldConverter(ConverterKind.Date, "MM/dd/yyyy")]
        public DateTime PY_IN_BUDAT;
        
        public string PY_IN_BKPF_BKTXT;
        public string PY_IN_RF05A_NEWKO;
        public string PY_IN_RF05A_NEWBS;
        public string PY_IN_BSEG_WRBTR;
        public string PY_IN_BKPF_WAERS;
        public string PY_IN_COBL_KOSTL;
        public string PY_IN_BSEG_SGTXT;

        public GLSapExport()
        {
            Clear();
        }

        protected void Clear()
        {
            PY_IN_LINE_NO = "";
            PY_IN_BKPF_BUKRS = "";
            PY_IN_BUDAT = DateTime.Now;
            PY_IN_BKPF_BKTXT = "Payroll Processing - CA ";
            PY_IN_RF05A_NEWKO = "";
            PY_IN_RF05A_NEWBS = "";
            PY_IN_BSEG_WRBTR = "";
            PY_IN_BKPF_WAERS = "CAD";
            PY_IN_COBL_KOSTL = "";
            PY_IN_BSEG_SGTXT = "";
        }
    }

    [DelimitedRecord("\t")]
    public class GLSapExportTrailer
    {
        public string PY_IN_FOOTER_CNTS_TXT;
        public string PY_IN_FOOTER_RCD_CNT;
        public string PY_IN_FOOTER_DR_TXT;
        public string PY_IN_FOOTER_DR_AMT;
        public string PY_IN_FOOTER_CR_TXT;
        public string PY_IN_FOOTER_CR_AMT;

        public GLSapExportTrailer()
        {
            Clear();
        }

        protected void Clear()
        {
            PY_IN_FOOTER_CNTS_TXT = "TRAILER COUNT";
            PY_IN_FOOTER_RCD_CNT = "0";
            PY_IN_FOOTER_DR_TXT = "DEBIT TOTAL";
            PY_IN_FOOTER_DR_AMT = "0";              //include decimal, 2 decimal places
            PY_IN_FOOTER_CR_TXT = "TOTAL CREDIT";
            PY_IN_FOOTER_CR_AMT = "0";              //include "-", include decimal, 2 decimal places
        }
    }
}
