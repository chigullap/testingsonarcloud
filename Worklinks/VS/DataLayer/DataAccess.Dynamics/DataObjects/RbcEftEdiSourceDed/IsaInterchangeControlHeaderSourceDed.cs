﻿using System;
using FileHelpers;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.RbcEftEdiSourceDed
{
    //header
    [DelimitedRecord("*")]
    public class IsaInterchangeControlHeaderSourceDed
    {
        public string IdElement;
        public string AuthorizationInformationQualifier;
        public string AuthorizationInformation;
        public string SecurityInformationQualifier;
        public string SecurityInformation;
        public string InterchangeIDQualifier;
        public string SenderInterchangeID;
        public string InterchangeIDQualifier2;
        public string ReceiverInterchangeID;
        [FieldConverter(ConverterKind.Date, "yyMMdd")]
        public DateTime InterchangeDate;
        [FieldConverter(ConverterKind.Date, "HHmm")]
        public DateTime InterchangeTime;
        public string InterchangeControlStandardsIdentifier;
        public string InterchangeControlVersionNumber;
        public string InterchangeControlNumber;
        public string AcknowledgmentRequested;
        public string TestIndicator;
        public string ComponentElementSeparator;

        public IsaInterchangeControlHeaderSourceDed()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "ISA";
            AuthorizationInformationQualifier = "00";
            AuthorizationInformation = "".PadRight(10);
            SecurityInformationQualifier = "00";
            SecurityInformation = "".PadRight(10);
            InterchangeIDQualifier = "ZZ";
            SenderInterchangeID = "";                       //populated from code system
            InterchangeIDQualifier2 = "01";
            ReceiverInterchangeID = "";                     //populated from code system
            InterchangeDate = DateTime.Now;
            InterchangeTime = DateTime.Now;
            InterchangeControlStandardsIdentifier = "U";
            InterchangeControlVersionNumber = "00300";
            InterchangeControlNumber = "";                  //sequence from db
            AcknowledgmentRequested = "0";
            TestIndicator = "";                             //populated from code system
            ComponentElementSeparator = ">";
        }
    }

    //trailer
    [DelimitedRecord("*")]
    public class IsaInterchangeControlTrailerSourceDed
    {
        public string IdElement;
        public string NumberOfIncludedFunctionalGroups;
        public string InterchangeControlNumber;

        public IsaInterchangeControlTrailerSourceDed()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "IEA";
            NumberOfIncludedFunctionalGroups = "1";
            InterchangeControlNumber = "";                  //sequence from db, same as IsaInterchangeControlHeaderSourceDed.InterchangeControlNumber
        }
    }
}
