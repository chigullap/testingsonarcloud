﻿using System;
using FileHelpers;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.RbcEftEdiSourceDed
{
    public class SourceDeductionDetails //this class encapsulates everything below needed for source deductions
    {
        public SourceDeductionsStTransactionSetHeader[] st = new SourceDeductionsStTransactionSetHeader[1];
        public SourceDeductionsBeginningSegment[] bps = new SourceDeductionsBeginningSegment[1];
        public SourceDeductionsReferenceIdentification[] ref1 = new SourceDeductionsReferenceIdentification[1];
        public SourceDeductionsReferenceIdentification2[] ref2 = new SourceDeductionsReferenceIdentification2[1];
        public SourceDeductionsDateTimeReference[] dtm = new SourceDeductionsDateTimeReference[1];
        public SourceDeductionsPayeeReceiverName[] n1pe = new SourceDeductionsPayeeReceiverName[1];
        public SourceDeductionsPayorReceiverName[] n1pr = new SourceDeductionsPayorReceiverName[1];
        public SourceDeductionsAdministrativeCommunicationsContact[] per = new SourceDeductionsAdministrativeCommunicationsContact[1];
        public SourceDeductionsLoopHeader[] ls = new SourceDeductionsLoopHeader[1];
        public SourceDeductionsLoopTrailer[] le = new SourceDeductionsLoopTrailer[1];

        //these are part of a loopable item.....make construtor take int and declare then
        public RepeatingElements[] repeatingElements;

        public SourceDeductionsSeTransactionSetTrailer[] se = new SourceDeductionsSeTransactionSetTrailer[1];

        public SourceDeductionDetails(int numLoops)
        {
            st[0] = new SourceDeductionsStTransactionSetHeader();
            bps[0] = new SourceDeductionsBeginningSegment();
            ref1[0] = new SourceDeductionsReferenceIdentification();
            ref2[0] = new SourceDeductionsReferenceIdentification2();
            dtm[0] = new SourceDeductionsDateTimeReference();
            n1pe[0] = new SourceDeductionsPayeeReceiverName();
            n1pr[0] = new SourceDeductionsPayorReceiverName();
            per[0] = new SourceDeductionsAdministrativeCommunicationsContact();
            ls[0] = new SourceDeductionsLoopHeader();
            
            //create repeating elements
            repeatingElements = new RepeatingElements[numLoops];
            for (int i = 0; i < numLoops; i++)
                repeatingElements[i] = new RepeatingElements();

            le[0] = new SourceDeductionsLoopTrailer();
            se[0] = new SourceDeductionsSeTransactionSetTrailer();
        }
    }

    public class RepeatingElements
    {
        public SourceDeductionsPayorReceiverName2[] n1pr2 = new SourceDeductionsPayorReceiverName2[1];
        public SourceDeductionsRemittanceAdvice[] rmt = new SourceDeductionsRemittanceAdvice[1];
        public SourceDeductionsReferenceIdentification3[] ref3 = new SourceDeductionsReferenceIdentification3[1];
        public SourceDeductionsDateTimeReference2[] dtm2 = new SourceDeductionsDateTimeReference2[1];

        public RepeatingElements()
        {
            n1pr2[0] = new SourceDeductionsPayorReceiverName2();
            rmt[0] = new SourceDeductionsRemittanceAdvice();
            ref3[0] = new SourceDeductionsReferenceIdentification3();
            dtm2[0] = new SourceDeductionsDateTimeReference2();
        }
    }

    [DelimitedRecord("*")]
    public class SourceDeductionsStTransactionSetHeader
    {
        public string IdElement;
        public string TransactionSetIdentifierCode;
        public string TransactionSetControlNumber;

        public SourceDeductionsStTransactionSetHeader()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "ST";
            TransactionSetIdentifierCode = "820";
            TransactionSetControlNumber = "0001";
        }
    }

    [DelimitedRecord("*")]
    public class SourceDeductionsSeTransactionSetTrailer
    {
        public string IdElement;
        public string NumberOfIncludedSegments;
        public string TransactionSetControlNumber;

        public SourceDeductionsSeTransactionSetTrailer()
        {
            Clear();
        }
        protected void Clear()
        {
            IdElement = "SE";
            NumberOfIncludedSegments = "15";        //Total number of rows in the ST/SE pair, including ST and SE.  This will change if we have a loop of ls/le segments
            TransactionSetControlNumber = "0001";
        }
    }

    [DelimitedRecord("*")]
    public class SourceDeductionsBeginningSegment
    {
        public string IdElement;
        public string PaymentMethodCode;
        public string PaymentAmount;
        public string TransactionHandlingCode;
        public string OriginatorIdNumberQualifier;
        public string OriginatorDepositoryFinancialInstitutionIdentificationNumber;
        public string OriginatorAccountNumber;
        public string EmptySpot1;
        public string OriginatingCompanySupplementalCode;
        public string OriginatorIdNumberQualifier2;
        public string ReceiverDepositoryFinancialInstitutionIdentificationNumber;
        public string ReceiverAccountNumber;
        [FieldConverter(ConverterKind.Date, "yyMMdd")]
        public DateTime EffectiveEntryDate;

        public SourceDeductionsBeginningSegment()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "BPS";
            PaymentMethodCode = "ZZZ";
            PaymentAmount = "";                                                         //Payment amount (Fed tax + EI + CPP + additional federal tax)
            TransactionHandlingCode = "X";
            OriginatorIdNumberQualifier = "04";
            OriginatorDepositoryFinancialInstitutionIdentificationNumber = "";          //populated from code system (WL trust account bank / transit)
            OriginatorAccountNumber = "";                                               //populated from code system (WL trust account #)
            EmptySpot1 = "";
            OriginatingCompanySupplementalCode = "PAYROLIST";
            OriginatorIdNumberQualifier2 = "04";
            ReceiverDepositoryFinancialInstitutionIdentificationNumber = "";            //populated from code system (CRA bank / transit)
            ReceiverAccountNumber = "";                                                 //populated from code system (CRA account #)
            EffectiveEntryDate = DateTime.Now;                                          //Effective entry date.  WL expects txn to be settled on this date.  3 working days after reporting end date.  Not province specific, federal only.
        }
    }

    [DelimitedRecord("*")]
    public class SourceDeductionsReferenceIdentification
    {
        public string IdElement;
        public string ReferenceNumberQualifier;
        public string ReferenceNumber;

        public SourceDeductionsReferenceIdentification()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "REF";
            ReferenceNumberQualifier = "TN";
            ReferenceNumber = "";                           //populated from sequence
        }
    }

    [DelimitedRecord("*")]
    public class SourceDeductionsReferenceIdentification2
    {
        public string IdElement;
        public string ReferenceNumberQualifier;
        public string ReferenceNumber;

        public SourceDeductionsReferenceIdentification2()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "REF";
            ReferenceNumberQualifier = "IT";
            ReferenceNumber = "";                       //populated from code system 
        }
    }

    [DelimitedRecord("*")]
    public class SourceDeductionsDateTimeReference
    {
        public string IdElement;
        public string DateTimeQualifier;
        [FieldConverter(ConverterKind.Date, "yyMMdd")]
        public DateTime Date;

        public SourceDeductionsDateTimeReference()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "DTM";
            DateTimeQualifier = "097";
            Date = DateTime.Now;
        }
    }

    [DelimitedRecord("*")]
    public class SourceDeductionsPayeeReceiverName
    {
        public string IdElement;
        public string EntityIdentifierCode;
        public string Name;

        public SourceDeductionsPayeeReceiverName()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "N1";
            EntityIdentifierCode = "PE";
            Name = "REC GEN";
        }
    }

    [DelimitedRecord("*")]
    public class SourceDeductionsPayorReceiverName
    {
        public string IdElement;
        public string EntityIdentifierCode;
        public string Name;

        public SourceDeductionsPayorReceiverName()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "N1";
            EntityIdentifierCode = "PR";
            Name = "";                              //populated from code system 
        }
    }

    [DelimitedRecord("*")]
    public class SourceDeductionsAdministrativeCommunicationsContact
    {
        public string IdElement;
        public string ContactFunctionCode;

        public SourceDeductionsAdministrativeCommunicationsContact()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "PER";
            ContactFunctionCode = "ZZ";
        }
    }

    [DelimitedRecord("*")]
    public class SourceDeductionsLoopHeader
    {
        public string IdElement;
        public string LoopIdentifierCode;

        public SourceDeductionsLoopHeader()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "LS";
            LoopIdentifierCode = "";                    //Code identifying a loop within the transaction set which is bounded by the related LS and LE segments (counter)
        }
    }

    [DelimitedRecord("*")]
    public class SourceDeductionsPayorReceiverName2
    {
        public string IdElement;
        public string EntityIdentifierCode;
        public string Name;

        public SourceDeductionsPayorReceiverName2()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "N1";
            EntityIdentifierCode = "PR";
            Name = "";                      //populated from code system (company short name)
        }
    }

    [DelimitedRecord("*")]
    public class SourceDeductionsRemittanceAdvice
    {
        public string IdElement;
        public string ReferenceNumberQualifier;
        public string BusinessNumber;
        public string RemittanceAmount;
        public string EmptySpot1;
        public string EmptySpot2;
        public string EmptySpot3;
        public string EmptySpot4;
        public string EmptySpot5;
        public string EmptySpot6;
        public string NumberOfEmployeesConcatGrossPayrollAmount;

        public SourceDeductionsRemittanceAdvice()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "RMT";
            ReferenceNumberQualifier = "TJ";
            BusinessNumber = "";                        //business number table
            RemittanceAmount = "";                      //payroll master detail
            EmptySpot1 = "";
            EmptySpot2 = "";
            EmptySpot3 = "";
            EmptySpot4 = "";
            EmptySpot5 = "";
            EmptySpot6 = "";
            NumberOfEmployeesConcatGrossPayrollAmount = ""; //computed.  15 chars length.  6 chars # employee padded left, 9 chars gross pay amount padded left, concat together (ie. 000006000001254)
        }

    }

    [DelimitedRecord("*")]
    public class SourceDeductionsReferenceIdentification3
    {
        public string IdElement;
        public string ReferenceNumberQualifier;
        public string ReferenceNumber;

        public SourceDeductionsReferenceIdentification3()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "REF";
            ReferenceNumberQualifier = "T2";
            ReferenceNumber = "0160";
        }
    }

    [DelimitedRecord("*")]
    public class SourceDeductionsDateTimeReference2
    {
        public string IdElement;
        public string DateTimeQualifier;
        [FieldConverter(ConverterKind.Date, "yyMMdd")]
        public DateTime Date;

        public SourceDeductionsDateTimeReference2()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "DTM";
            DateTimeQualifier = "091";
            Date = DateTime.Now;                    //computed
        }
    }

    [DelimitedRecord("*")]
    public class SourceDeductionsLoopTrailer
    {
        public string IdElement;
        public string LoopIdentifierCode;

        public SourceDeductionsLoopTrailer()
        {
            Clear();
        }

        protected void Clear()
        {
            IdElement = "LE";
            LoopIdentifierCode = "";                    //Code identifying a loop within the transaction set which is bounded by the related LS and LE segments (counter)
        }
    }


}
