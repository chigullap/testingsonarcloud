﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [FixedLengthRecord()]
    public class EFTTransmissionHeader
    {
        [FieldFixedLength(14)]
        public string StartingString;

        [FieldFixedLength(4)]
        public string FileType;

        [FieldFixedLength(5)]
        public string EndingString;

        public EFTTransmissionHeader()
        {
            Clear();
        }

        protected void Clear()
        {
            StartingString = "$$AA01ACH0094[";
            FileType = "PROD";
            EndingString = "[NL$$";
        }
    }
}
