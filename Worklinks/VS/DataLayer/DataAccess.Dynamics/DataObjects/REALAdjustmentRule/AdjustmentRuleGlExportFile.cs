﻿using System;
using FileHelpers;


namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.REALAdjustmentRule
{
    [DelimitedRecord(",")]
    public class AdjustmentRuleGlExportHeader
    {
        public string PersonNumberHeader;
        public string PersonNameHeader;
        public string EffectiveDateHeader;
        public string JobHeader;
        public string FlatRateHeader;

        public AdjustmentRuleGlExportHeader()
        {
            Clear();
        }

        protected void Clear()
        {
            PersonNumberHeader = "ID";
            PersonNameHeader = "PersonName";
            EffectiveDateHeader = "EffectiveDate";
            JobHeader = "Job";
            FlatRateHeader = "FlatRate";
        }
    }

    [DelimitedRecord(",")]
    public class AdjustmentRuleGlExportData
    {
        public string PersonNumber;
        public string PersonName;
        [FieldConverter(ConverterKind.Date, "MM/dd/yyyy")]
        public DateTime EffectiveDate;
        public string Job;
        public Decimal FlatRate;

        public AdjustmentRuleGlExportData()
        {
            Clear();
        }

        protected void Clear()
        {
            PersonNumber = "";
            PersonName = "";
            EffectiveDate = DateTime.Now;
            Job = "";
            FlatRate = 0;
        }
    }

}
