﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [DelimitedRecord(",")]
    public class GLCsvExportHeader
    {
        public string AmountTitle;
        public string GL_AccountTitle;
        public string CompanyTitle;
        public string Cost_CtrTitle;
        public string Debit_CreditTitle;

        public GLCsvExportHeader()
        {
            Clear();
        }

        protected void Clear()
        {
            AmountTitle = "Amount";
            GL_AccountTitle = "GL Account";
            CompanyTitle = "Company";
            Cost_CtrTitle = "Cost Ctr";
            Debit_CreditTitle = "Debit / Credit";
        }
    }

    [DelimitedRecord(",")]
    public class GLCsvExport
    {
        public string Amount;
        public string GL_Account;
        public string Company;
        public string Cost_Ctr;
        public string Debit_Credit;

        public GLCsvExport()
        {
            Clear();
        }

        protected void Clear()
        {
            Amount = "";
            GL_Account = "";
            Company = "";
            Cost_Ctr = "";
            Debit_Credit = "";
        }
    }
}
