﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    //This record is not used by RBC but is in the ACH standard

    [FixedLengthRecord()]
    public class CompanyBatchControlRecord
    {
        [FieldFixedLength(1)]
        public int RecordType;

        [FieldFixedLength(3)]
        public int ServiceIdentifier;

        [FieldFixedLength(6)]
        public string EntryAddendaCount;                //in eft file, this is a numeric...string here so we can pad with "0's"

        [FieldFixedLength(10)]
        public string EntryHash;                        //in eft file, this is a numeric...string here so we can pad with "0's" or blanks

        [FieldFixedLength(12)]
        public string TotalDebitEntryDollarAmount;      //in eft file, this is a numeric...string here so we can pad with "0's"

        [FieldFixedLength(12)]
        public string TotalCreditEntryDollarAmount;     //in eft file, this is a numeric...string here so we can keep leading "0's", if any

        [FieldFixedLength(10)]
        public string ClientNumber;

        [FieldFixedLength(19)]
        public string MessageAuthCode1;

        [FieldFixedLength(6)]
        public string Reserved;

        [FieldFixedLength(8)]
        public string MessageAuthCode2;

        [FieldFixedLength(7)]
        public string BatchNumber;//in eft file, this is a numeric...string here so we can keep the "0's" if we pad it

        public CompanyBatchControlRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = 8;
            ServiceIdentifier = 220;
            EntryAddendaCount = "000000";
            EntryHash = "          ";                           //not sure if we should 0 fill or blank fill
            TotalDebitEntryDollarAmount = "000000000000";       //0 fill
            TotalCreditEntryDollarAmount = "000000000000";      //Total dollar amount for all detail credit records.  $$$$$$$$$$cc
            ClientNumber = "";                                  //retrieved from code_system ...6 rbc digits, 4 client for subsid (0's for no subsid)
            MessageAuthCode1 = "                   ";           //blanks
            Reserved = "      ";                                //blanks
            MessageAuthCode2 = "        ";                      //blanks
            BatchNumber = "";                                   //(must be sequential within file, 7 chars)
        }
    }
}
