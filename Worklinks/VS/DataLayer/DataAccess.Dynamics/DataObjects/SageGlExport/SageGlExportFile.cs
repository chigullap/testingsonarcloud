﻿using System;
using FileHelpers;


namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects.SageGlExport
{
    [DelimitedRecord(",")]
    public class SageGlExportHeader1
    {
        public string RecType;
        public string BatchId;
        public string BatchEntry;
        public string SourceLedger;
        public string SourceType;
        public string JournalDescription;
        public string FiscalYear;
        public string FiscalPeriod;
        public string DateEntry;

        public SageGlExportHeader1()
        {
            Clear();
        }
        protected void Clear()
        {
            RecType = "RECTYPE";
            BatchId = "BATCHID";
            BatchEntry = "BTCHENTRY";
            SourceLedger = "SRCELEDGER";
            SourceType = "SRCETYPE";
            JournalDescription = "JRNLDESC";
            FiscalYear = "FSCSYR";
            FiscalPeriod = "FSCSPERD";
            DateEntry = "DATEENTRY";
        }        
    }

    [DelimitedRecord(",")]
    public class SageGlExportHeader2
    {
        public string RecType;
        public string BatchNumber;
        public string JournalId;
        public string TransactionNumber;
        public string AccountId;
        public string TransactionAmount;
        public string TransactionDescription;
        public string TransactionReference;
        public string TransactionDate;

        public SageGlExportHeader2()
        {
            Clear();
        }
        protected void Clear()
        {
            RecType = "RECTYPE";
            BatchNumber = "BATCHNBR";
            JournalId = "JOURNALID";
            TransactionNumber = "TRANSNBR";
            AccountId = "ACCTID";
            TransactionAmount = "TRANSAMT";
            TransactionDescription = "TRANSDESC";
            TransactionReference = "TRANSREF";
            TransactionDate = "TRANSDATE";
        }
    }

    [DelimitedRecord(",")]
    public class SageGlExportHeader1Data
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string RecType;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string BatchId;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string BatchEntry;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SourceLedger;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SourceType;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string JournalDescription;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FiscalYear;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FiscalPeriod;

        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime DateEntry;

        public SageGlExportHeader1Data()
        {
            Clear();
        }
        protected void Clear()
        {
            RecType = "1";                  //JE Batch Header
            BatchId = "000000";             //Static, Required field replaced by Sage with JE Batch number
            BatchEntry = "";                //Example file has this padded with 0s for total of 5 spaces.  Numeric reference to Batch Entry number. Each subsequent record type associated with this header will refer to RECTYPE 1 with the same number
            SourceLedger = "GL";            //Reference to Source Ledger. This would remain "GL" for payroll journal entry
            SourceType = "JE";              //Reference to Source Type. This would remain "JE" for payroll journal entry
            JournalDescription = "";        //Description for Journal Entry. This should be unique reference to the Payroll being transferred
            FiscalYear = "";                //Fiscal year to post to
            FiscalPeriod = "";              //Fiscal month to post to
            DateEntry = DateTime.Now;       //Date of Entry YYYYMMDD. Should be end date of payroll work week
        }
    }

    [DelimitedRecord(",")]
    public class SageGlExportHeader2Data
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string RecType;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string BatchNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string JournalId;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string TransactionNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string AccountId;

        public Decimal TransactionAmount;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string TransactionDescription;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string TransactionReference;

        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime TransactionDate;

        public SageGlExportHeader2Data()
        {
            Clear();
        }
        protected void Clear()
        {
            RecType = "2";                      //JE Line entry
            BatchNumber = "000000";             //Static, Required field replaced by Sage with JE Batch number
            JournalId = "";                     //Will be the same as it's associated header entry for RECTYPE = "1" (class SageGlExportHeader1Data.BatchEntry)
            TransactionNumber = "0";            //Example file has this padded with 0s for total of 9 spaces.  Order number, normally incremented by 20 of the line entry within the Journal Entry
            AccountId = "";                     //Full Accounting number
            TransactionAmount = 0;             //Entry value. Debit = + Credit = -
            TransactionDescription = "";        //Line entry description
            TransactionReference = "";          //Transaction reference. Same for all line entries within JE. Detail as required
            TransactionDate = DateTime.Now;               //Date of line Entry in format YYYYMMDD. Should be end date of payroll work week
        }
    }    
}
