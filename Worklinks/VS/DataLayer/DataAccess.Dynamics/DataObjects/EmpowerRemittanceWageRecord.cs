﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [FixedLengthRecord()]
    public class EmpowerRemittanceWageRecord
    {
        [FieldFixedLength(1)]
        public string RecordType;

        [FieldFixedLength(10)]
        public string TaxType;

        [FieldFixedLength(35)]
        public string Filler1;

        [FieldFixedLength(14)]
        public string GrossWages;

        [FieldFixedLength(14)]
        public string SubjectWages;

        [FieldFixedLength(14)]
        public string TaxableWages;

        [FieldFixedLength(14)]
        public string TipsAmount;

        [FieldFixedLength(14)]
        public string Hours;

        [FieldIgnored]
        public EmpowerRemittanceTaxRecord[] TaxRecords;  //stacy note:  I read [FieldIgnored] is replaced with [FieldHidden] in newer filehelpers

        public EmpowerRemittanceWageRecord()
        {
            Clear();
        }



        protected void Clear()
        {
            RecordType = "W";
            TaxType = "";
            Filler1 = "                                   ";
            GrossWages = "";
            SubjectWages = "";
            TaxableWages = "";
            TipsAmount = "              ";
            Hours = "              ";
            TaxRecords = null;
        }
    }
}
