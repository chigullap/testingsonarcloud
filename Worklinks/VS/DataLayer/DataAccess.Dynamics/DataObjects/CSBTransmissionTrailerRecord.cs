﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Dynamics.DataObjects
{
    [FixedLengthRecord()]
    public class CSBTransmissionTrailerRecord
    {
        [FieldFixedLength(2)]
        public int RecordType;

        [FieldFixedLength(5)]
        public string TransmissionOrganizationId;

        [FieldFixedLength(10)]
        [FieldConverter(ConverterKind.Date, "yyyy-MM-dd")]
        public DateTime? TransmisionDate;

        [FieldFixedLength(8)]
        public string TransmissionId;

        [FieldFixedLength(9)]
        public string TotalRecordsTransmitted;

        [FieldFixedLength(86)]
        public string Filler;

        public CSBTransmissionTrailerRecord()
        {
            Clear();
        }

        protected void Clear()
        {
            RecordType = 90;
            TransmissionOrganizationId = null;
            TransmisionDate = DateTime.Now;
            TransmissionId = null;
            TotalRecordsTransmitted = null;  //must equal total nulmber of records included in this transmission, regardless of record type, and including this record
            Filler = "                                                                                     x";  //doc states "after the last field a filler is needed.  Put a char in the 120th position to ensure no compression occurs during movement between platforms"
        }
    }
}
