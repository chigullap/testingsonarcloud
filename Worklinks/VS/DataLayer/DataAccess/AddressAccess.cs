﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess
{
    public class AddressAccess
    {

        SqlServer.SqlServerAddress _sqlServerAddress = new SqlServer.SqlServerAddress();

        #region person address
        public AddressCollection GetAddress(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long addressId)
        {
            return _sqlServerAddress.Select(user,null, addressId);
        }
        public void UpdateAddress(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Address address)
        {
            _sqlServerAddress.Update(user,address);
        }
        public void InsertAddress(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Address address)
        {
            _sqlServerAddress.Insert(user,address);
        }
        public void DeleteAddress(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Address address)
        {
            _sqlServerAddress.Delete(user,address);
        }
        #endregion

    }
}
