﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess
{
    public class OrganizationUnitAccess
    {
        #region properties
        SqlServer.SqlServerOrganizationUnit _sqlOrganizationUnit = new SqlServer.SqlServerOrganizationUnit();
        SqlServer.SqlServerOrganizationUnitLevel _sqlOrganizationUnitLevel = new SqlServer.SqlServerOrganizationUnitLevel();
        SqlServer.SqlServerOrganizationUnitAssociation _sqlOrganizationUnitAssociation = new SqlServer.SqlServerOrganizationUnitAssociation();
        SqlServer.SqlServerOrganizationUnitLevelDescription _sqlOrganizationUnitLevelDesciption = new SqlServer.SqlServerOrganizationUnitLevelDescription();
        SqlServer.SqlServerOrganizationUnitDescription _sqlOrganizationUnitDesciption = new SqlServer.SqlServerOrganizationUnitDescription();
        SqlServer.SqlServerEmployeePositionLabourDistribution _sqlServerEmployeePositionLabourDistribution = new SqlServer.SqlServerEmployeePositionLabourDistribution();
        #endregion

        #region organization unit
        public OrganizationUnitCollection GetOrganizationUnitByOrganizationUnitLevelId(DatabaseUser user, OrganizationUnitCriteria criteria)
        {
            OrganizationUnitCollection unitCollection = _sqlOrganizationUnit.SelectByOrganizationUnitLevelId(user, criteria);

            foreach (OrganizationUnit organizationUnit in unitCollection)
                organizationUnit.Descriptions = GetOrganizationUnitDescription(user, new OrganizationUnitCriteria() { OrganizationUnitId = organizationUnit.OrganizationUnitId });

            return unitCollection;
        }
        public OrganizationUnitCollection GetOrganizationUnitWithLevelDescription(DatabaseUser user, long? organizationUnitId)
        {
            return _sqlOrganizationUnit.SelectWithLevelDescription(user, organizationUnitId);
        }
        public OrganizationUnit InsertOrganizationUnitRow(DatabaseUser user, OrganizationUnit data)
        {
            OrganizationUnit organizationUnit = new OrganizationUnit();
            organizationUnit = _sqlOrganizationUnit.Insert(user, data);

            return organizationUnit;
        }
        public void UpdateOrganizationUnitRow(DatabaseUser user, OrganizationUnit data)
        {
            _sqlOrganizationUnit.Update(user, data);
        }
        public void DeleteOrganizationUnitRow(DatabaseUser user, OrganizationUnit data)
        {
            _sqlOrganizationUnit.Delete(user, data);
        }
        #endregion

        #region organization unit description
        public OrganizationUnitDescriptionCollection GetOrganizationUnitDescription(DatabaseUser user, OrganizationUnitCriteria criteria)
        {
            return _sqlOrganizationUnitDesciption.SelectByOrganizationUnitId(user, criteria);
        }
        public OrganizationUnitDescription InsertOrganizationUnitDescriptionRow(DatabaseUser user, OrganizationUnitDescription data)
        {
            OrganizationUnitDescription organizationUnitDescription = new OrganizationUnitDescription();
            organizationUnitDescription = _sqlOrganizationUnitDesciption.Insert(user, data);

            return organizationUnitDescription;
        }
        public void UpdateOrganizationUnitDescriptionRow(DatabaseUser user, OrganizationUnitDescription data)
        {
            _sqlOrganizationUnitDesciption.Update(user, data);
        }
        public void DeleteOrganizationUnitDescriptionRow(DatabaseUser user, OrganizationUnitDescription data)
        {
            _sqlOrganizationUnitDesciption.Delete(user, data);
        }
        #endregion

        #region organization unit level
        public OrganizationUnitCollection GetOrganizationUnit(DatabaseUser user, long? organizationUnitId)
        {
            return _sqlOrganizationUnit.Select(user, organizationUnitId);
        }
        public OrganizationUnitCollection GetOrganizationUnitByParentOrganizationUnitId(DatabaseUser user, long? parentOrganizationUnitId)
        {
            return _sqlOrganizationUnit.SelectByParentOrganizationUnitId(user, parentOrganizationUnitId);
        }
        public OrganizationUnitLevelCollection GetOrganizationUnitLevel(DatabaseUser user)
        {
            OrganizationUnitLevelCollection collection = _sqlOrganizationUnitLevel.Select(user);
            OrganizationUnitCriteria criteria = new OrganizationUnitCriteria();

            foreach (OrganizationUnitLevel organizationUnitLevel in collection)
            {
                criteria.OrganizationUnitLevelId = organizationUnitLevel.OrganizationUnitLevelId;
                organizationUnitLevel.Descriptions = GetOrganizationUnitLevelDescription(user, criteria);
            }

            return collection;
        }

        public OrganizationUnitLevelDescriptionCollection GetOrganizationUnitLevelDescription(DatabaseUser user, long organizationUnitLevelId)
        {
            OrganizationUnitCriteria criteria = new OrganizationUnitCriteria() { OrganizationUnitLevelId = organizationUnitLevelId };
            return GetOrganizationUnitLevelDescription(user, criteria);
        }

        public OrganizationUnitLevel InsertOrganizationUnitLevelRow(DatabaseUser user, OrganizationUnitLevel data)
        {
            OrganizationUnitLevel organizationUnitLevel = new OrganizationUnitLevel();
            organizationUnitLevel = _sqlOrganizationUnitLevel.Insert(user, data);

            return organizationUnitLevel;
        }
        public void UpdateOrganizationUnitLevelRow(DatabaseUser user, OrganizationUnitLevel data)
        {
            _sqlOrganizationUnitLevel.Update(user, data);
        }
        public void DeleteOrganizationUnitLevelRow(DatabaseUser user, OrganizationUnitLevel data)
        {
            _sqlOrganizationUnitLevel.Delete(user, data);
        }
        #endregion

        #region organization unit level description
        public OrganizationUnitLevelDescriptionCollection GetOrganizationUnitLevelDescription(DatabaseUser user, OrganizationUnitCriteria criteria)
        {
            return _sqlOrganizationUnitLevelDesciption.Select(user, criteria);
        }
        public OrganizationUnitLevelDescription InsertOrganizationUnitLevelDescriptionRow(DatabaseUser user, OrganizationUnitLevelDescription data)
        {
            OrganizationUnitLevelDescription organizationUnitLevelDescription = new OrganizationUnitLevelDescription();
            organizationUnitLevelDescription = _sqlOrganizationUnitLevelDesciption.Insert(user, data);

            return organizationUnitLevelDescription;
        }
        public void UpdateOrganizationUnitLevelDescriptionRow(DatabaseUser user, OrganizationUnitLevelDescription data)
        {
            _sqlOrganizationUnitLevelDesciption.Update(user, data);
        }
        public void DeleteOrganizationUnitLevelDescriptionRow(DatabaseUser user, OrganizationUnitLevelDescription data)
        {
            _sqlOrganizationUnitLevelDesciption.Delete(user, data);
        }
        #endregion

        #region organization unit association
        public OrganizationUnitAssociationTreeCollection GetOrganizationUnitAssociationTree(DatabaseUser user, String codeLanguageCd, bool? checkActiveFlag)
        {
            return _sqlOrganizationUnitAssociation.GetOrganizationUnitAssociationTree(user, codeLanguageCd, checkActiveFlag);
        }
        public OrganizationUnitAssociationCollection GetOrganizationUnitAssociation(DatabaseUser user, OrganizationUnitCriteria criteria)
        {
            OrganizationUnitAssociationCollection organizationUnitAssociation = _sqlOrganizationUnitAssociation.Select(user, criteria);

            foreach (OrganizationUnitAssociation organizationUnit in organizationUnitAssociation)
                organizationUnit.Descriptions = GetOrganizationUnitDescription(user, new OrganizationUnitCriteria() { OrganizationUnitId = organizationUnit.OrganizationUnitId });

            return organizationUnitAssociation;
        }
        public void InsertOrganizationUnitAssociation(DatabaseUser user, OrganizationUnitAssociation data)
        {
            _sqlOrganizationUnitAssociation.Insert(user, data);
        }
        public void DeleteOrganizationUnitAssociation(DatabaseUser user, OrganizationUnitAssociation data)
        {
            _sqlOrganizationUnitAssociation.Delete(user, data);
        }
        #endregion

        #region employee position labour distribution
        public EmployeePositionLabourDistributionSummaryCollection GetEmployeePositionLabourDistribution(DatabaseUser user, long employeePositionId, String codeLanguageCd)
        {
            return _sqlServerEmployeePositionLabourDistribution.Select(user, employeePositionId, codeLanguageCd);
        }
        public void UpdateEmployeePositionLabourDistribution(DatabaseUser user, EmployeePositionLabourDistributionSummary summary)
        {
            _sqlServerEmployeePositionLabourDistribution.Update(user, summary);
        }
        public EmployeePositionLabourDistributionSummary InsertEmployeePositionLabourDistribution(DatabaseUser user, EmployeePositionLabourDistributionSummary summary)
        {
            return _sqlServerEmployeePositionLabourDistribution.Insert(user, summary);
        }
        public void DeleteEmployeePositionLabourDistribution(DatabaseUser user, EmployeePositionLabourDistributionSummary summary)
        {
            _sqlServerEmployeePositionLabourDistribution.Delete(user, summary);
        }
        #endregion
    }
}