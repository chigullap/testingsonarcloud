﻿using System;
using System.Text;
using System.Collections;
using System.Configuration;
using System.Collections.Generic;

using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.Dynamics
{
    public static class DatabaseInfo
    {
        private static Database _mainDatabase;

        static DatabaseInfo()
        {
            System.Configuration.ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings["WorkLinksDynamics"];
            _mainDatabase = new Database(settings.ConnectionString, settings.ProviderName);
        }
        
        public static Database MainDatabase
        {
            get { return _mainDatabase; }
        }

    }

}
