﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Data;
using System.Data.Common;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;


namespace WorkLinks.DataLayer.DataAccess.Dynamics
{
    public class SqlServerBase : IDisposable
    {

        /// <summary>
        /// returns a stored proc command
        /// </summary>
        /// <param name="storedProcedureName"></param>
        /// <returns></returns>
        protected StoredProcedureCommand GetStoredProcCommand(String storedProcedureName)
        {
            return MainDatabase.GetStoredProcCommand(storedProcedureName);
        }
        
        protected class ColumnNames
        {
            public static String RowVersion = "row_version";
            public static String CreateUser = "create_user";
            public static String CreateDatetime = "create_datetime";
            public static String UpdateUser = "update_user";
            public static String UpdateDatetime = "update_datetime";
        }

        //protected String UserName
        //{
        //    get { return ((System.Threading.Thread.CurrentPrincipal).Identity).Name; }
        //}
        protected DateTime Time
        {
            get { return DateTime.Now; }
        }

        private Database MainDatabase
        {
            get { return DatabaseInfo.MainDatabase; }
        }
        
        protected Object CleanDataValue(Object dataValue)
        {
            return dataValue == DBNull.Value ? null : dataValue;
        }
        protected Object ValueOrDBNull(Object dataValue)
        {
            return dataValue == null ? DBNull.Value : dataValue;
        }
        protected int GetReaderColumnIndex(System.Data.SqlClient.SqlDataReader reader, String columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).Equals(columnName))
                {
                    return i;
                }
            }
            return -1;
        }
        protected bool ReaderColumnExists(System.Data.SqlClient.SqlDataReader reader, String columnName)
        {
            if (GetReaderColumnIndex(reader, columnName) < 0)
                return false;
            else
                return true;
        }

        protected bool HandleException(Exception exc, int SqlErrorNumber)
        {
            switch (SqlErrorNumber)
            {
                case 50010:
                    throw new SqlServerException(exc.Message, SqlServerException.ExceptionCodes.DataConcurrency, exc);
            }
            return false;
        }

        #region data mapping
        internal void MapToBase(WLP.BusinessLayer.BusinessObjects.BusinessObject obj, IDataReader reader)
        {
            MapToBase(obj, reader, true);
        }
        internal void MapToBase(WLP.BusinessLayer.BusinessObjects.BusinessObject obj, IDataReader reader, bool mapRowVersion)
        {
            if (mapRowVersion)
                obj.RowVersion = (byte[])CleanDataValue(reader[ColumnNames.RowVersion]);
            obj.CreateUser = (String)CleanDataValue(reader[ColumnNames.CreateUser]);
            obj.CreateDatetime = (DateTime)CleanDataValue(reader[ColumnNames.CreateDatetime]);
            obj.UpdateUser = (String)CleanDataValue(reader[ColumnNames.UpdateUser]);
            obj.UpdateDatetime = (DateTime)CleanDataValue(reader[ColumnNames.UpdateDatetime]);
        }
        #endregion


        #region IDisposable Members

        public void Dispose()
        {
        }

        #endregion
    }
}
