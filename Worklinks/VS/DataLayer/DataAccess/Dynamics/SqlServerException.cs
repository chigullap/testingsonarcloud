﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkLinks.DataLayer.DataAccess.Dynamics
{
    /// <summary>
    /// class used for WorkLinks sql exception handling
    /// </summary>
    public class SqlServerException : Exception
    {
        public enum ExceptionCodes
        {
            DataConcurrency,
            Other,
        }

        #region fields
        ExceptionCodes _exceptionCode = 0;
        #endregion

        #region properties
        public ExceptionCodes ExceptionCode
        {
            get { return _exceptionCode; }
            set { _exceptionCode = value; }
        }
        #endregion

        public SqlServerException(String message, ExceptionCodes code, Exception exc)
            : base(message, exc)
        {
            _exceptionCode = code;
        }
    }

}
