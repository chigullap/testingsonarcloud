﻿using System;
using System.Collections.Generic;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.CalcModel;
using WorkLinks.BusinessLayer.BusinessObjects.Import;
using WorkLinks.BusinessLayer.BusinessObjects.Payroll;

namespace WorkLinks.DataLayer.DataAccess
{
    public class PayrollAccess
    {
        #region fields
        SqlServer.SqlServerPayrollPeriod _sqlServerPayrollPeriod = new SqlServer.SqlServerPayrollPeriod();
        SqlServer.SqlServerPayrollPeriodOverride _sqlServerPayrollPeriodOverride = new SqlServer.SqlServerPayrollPeriodOverride();
        SqlServer.SqlServerPayrollProcess _sqlServerPayrollProcess = new SqlServer.SqlServerPayrollProcess();
        SqlServer.SqlServerPayrollBatch _sqlServerPayrollBatch = new SqlServer.SqlServerPayrollBatch();
        SqlServer.SqlServerPayrollTransaction _sqlServerPayrollTransaction = new SqlServer.SqlServerPayrollTransaction();
        SqlServer.SqlServerSalaryEmployee _sqlServerSalaryEmployee = new SqlServer.SqlServerSalaryEmployee();
        SqlServer.sqlServerRoeCreationSearch _sqlServerRoeCreationSearch = new SqlServer.sqlServerRoeCreationSearch();
        SqlServer.SqlServerEmployeeRoeAmount _sqlServerEmployeeRoeAmount = new SqlServer.SqlServerEmployeeRoeAmount();
        SqlServer.SqlServerEmployeeRoeAmountDetail _sqlServerEmployeeRoeAmountDetail = new SqlServer.SqlServerEmployeeRoeAmountDetail();
        SqlServer.SqlServerRoe _sqlServerRoe = new SqlServer.SqlServerRoe();
        SqlServer.SqlServerEmployeePaycodeArrears _sqlServerEmployeePaycodeArrears = new SqlServer.SqlServerEmployeePaycodeArrears();
        SqlServer.SqlServerPayDetail _sqlServerPayDetail = new SqlServer.SqlServerPayDetail();
        SqlServer.SqlServerExportFtp _sqlServerExportFtp = new SqlServer.SqlServerExportFtp();
        SqlServer.SqlServerPayRegisterExport _sqlServerPayRegisterExport = new SqlServer.SqlServerPayRegisterExport();
        SqlServer.SqlServerCitiEftPayrollMasterPayment _sqlCitiEftPayrollMasterPayment = new SqlServer.SqlServerCitiEftPayrollMasterPayment();
        SqlServer.SqlServerPaycodeYearEndReportMap _sqlServerPaycodeYearEndReportMap = new SqlServer.SqlServerPaycodeYearEndReportMap();
        SqlServer.SqlServerPaycodePayrollTransactionOffsetAssociation _sqlServerPaycodePayrollTransactionOffsetAssociation = new SqlServer.SqlServerPaycodePayrollTransactionOffsetAssociation();
        SqlServer.SqlServerPayrollMaster _sqlServerPayrollMaster = new SqlServer.SqlServerPayrollMaster();
        SqlServer.SqlServerPayrollMasterPayment _sqlServerPayrollMasterPayment = new SqlServer.SqlServerPayrollMasterPayment();
        SqlServer.SqlServerPayrollMasterPaycode _sqlServerPayrollMasterPaycode = new SqlServer.SqlServerPayrollMasterPaycode();
        SqlServer.SqlServerTempBulkPayrollProcess _sqlServerTempBulkPayrollProcess = new SqlServer.SqlServerTempBulkPayrollProcess();
        SqlServer.SqlServerPayrollMasterDeduction _sqlServerPayrollMasterDeduction = new SqlServer.SqlServerPayrollMasterDeduction();
        SqlServer.SqlServerRemittance _sqlServerRemittance = new SqlServer.SqlServerRemittance();
        SqlServer.SqlServerRemittanceImport _sqlServerRemittanceImport = new SqlServer.SqlServerRemittanceImport();
        SqlServer.SqlServerHealthTax _sqlServerHealthTax = new SqlServer.SqlServerHealthTax();
        SqlServer.SqlServerHealthTaxDetail _sqlServerHealthTaxDetail = new SqlServer.SqlServerHealthTaxDetail();
        SqlServer.SqlServerRemittanceDetailCraSourceDeductions _sqlServerRemittanceDetailCraSourceDeductions = new SqlServer.SqlServerRemittanceDetailCraSourceDeductions();
        SqlServer.SqlServerRemittanceDetailWcbDeductions _sqlServerRemittanceDetailWcbDeductions = new SqlServer.SqlServerRemittanceDetailWcbDeductions();
        SqlServer.SqlServerRemittanceDetailChequeHealthTaxDeductions _sqlServerRemittanceDetailChequeHealthTaxDeductions = new SqlServer.SqlServerRemittanceDetailChequeHealthTaxDeductions();
        SqlServer.SqlServerRemittanceDetailGarnishmentDeductions _sqlServerRemittanceDetailGarnishmentDeductions = new SqlServer.SqlServerRemittanceDetailGarnishmentDeductions();
        SqlServer.SqlServerRemittanceDetailRqSourceDeductions _sqlServerRemittanceDetailRqSourceDeductions = new SqlServer.SqlServerRemittanceDetailRqSourceDeductions();
        SqlServer.SqlServerPayroll _sqlServerPayroll = new SqlServer.SqlServerPayroll();
        SqlServer.SqlServerAutoPayroll _sqlServerAutoPayroll = new SqlServer.SqlServerAutoPayroll();
        SqlServer.SqlServerSalaryPlanGradeAccumulation _sqlServerSalaryPlanGradeAccumulation = new SqlServer.SqlServerSalaryPlanGradeAccumulation();
        SqlServer.SqlServerAdvantageTimeRecord _sqlServerAdvantageTimeRecord = new SqlServer.SqlServerAdvantageTimeRecord();
        #endregion

        #region payroll batch
        public PayrollBatchReportCollection GetPayrollBatchReport(DatabaseUser user, PayrollBatchCriteria criteria)
        {
            return _sqlServerPayrollBatch.SelectPayrollBatchSummary(user, criteria);
        }
        public PayrollBatch InsertPayrollBatch(DatabaseUser user, PayrollBatch item)
        {
            return _sqlServerPayrollBatch.Insert(user, item);
        }
        public void BatchUpdatePayrollBatch(DatabaseUser user, PayrollBatchCollection items)
        {
            _sqlServerPayrollBatch.BatchUpdate(user, items);
        }
        public void UpdatePayrollBatch(DatabaseUser user, PayrollBatch item)
        {
            _sqlServerPayrollBatch.Update(user, item);
        }
        public void DeletePayrollBatch(DatabaseUser user, PayrollBatch item)
        {
            _sqlServerPayrollBatch.Delete(user, item);
        }
        public PayrollBatchCollection GetPayrollBatch(DatabaseUser user, PayrollBatchCriteria criteria)
        {
            return _sqlServerPayrollBatch.Select(user, criteria);
        }
        public SalaryEmployeeCollection GetSalaryEmployees(DatabaseUser user, String payrollProcessGroupCode, bool prorationEnabledFlag)
        {
            return _sqlServerSalaryEmployee.Select(user, payrollProcessGroupCode, prorationEnabledFlag);
        }
        public PayrollPeriodCollection GetPayrollPeriodIdFromPayrollProcessGroupCode(DatabaseUser user, PayrollBatchCriteria payrollBatchCriteria)
        {
            return _sqlServerPayrollBatch.GetPayrollPeriodIdFromPayrollProcessGroupCode(user, payrollBatchCriteria);
        }
        #endregion

        #region payroll period
        public Decimal GetCurrentPayrollYear(DatabaseUser user)
        {
            return _sqlServerPayrollPeriod.SelectCurrentPayrollYear(user);
        }
        public PayrollPeriodCollection GetPayrollPeriod(DatabaseUser user, PayrollPeriodCriteria criteria)
        {
            return _sqlServerPayrollPeriod.Select(user, criteria);
        }
        public PayrollPeriodOverrideCollection CheckForPayrollPeriodOverride(DatabaseUser user, String payrollProcessGroupCode, decimal periodYear, decimal period)
        {
            return _sqlServerPayrollPeriodOverride.Select(user, payrollProcessGroupCode, periodYear, period);
        }
        public void ClosePeriod(DatabaseUser user, PayrollPeriod item, long payrollProcessId)
        {
            _sqlServerPayrollPeriod.ClosePeriod(user, item, payrollProcessId);
        }
        public void UpdatePayrollPeriod(DatabaseUser user, PayrollPeriod item)
        {
            _sqlServerPayrollPeriod.Update(user, item);
        }
        public void InsertPayrollPeriod(DatabaseUser user, PayrollPeriod item)
        {
            _sqlServerPayrollPeriod.Insert(user, item);
        }
        public void DeletePayrollPeriod(DatabaseUser user, PayrollPeriod item)
        {
            _sqlServerPayrollPeriod.Delete(user, item);
        }
        #endregion

        #region pending roe
        public RoeCreationSearchResultsCollection GetPendingRoeEmployeeSummary(DatabaseUser user, String criteria)
        {
            return _sqlServerRoeCreationSearch.Select(user, criteria);
        }
        public void SetPendingRoeStatusForEmployeeId(DatabaseUser user, EmployeePosition coll)
        {
            _sqlServerRoeCreationSearch.SetPendingRoeStatusForEmployeeId(user, coll);
        }
        public EmployeeRoeAmountCollection GetRoeData(DatabaseUser user, long employeePositionId, long employeeId)
        {
            return _sqlServerEmployeeRoeAmount.Select(user, employeePositionId, employeeId, false);
        }
        public EmployeeRoeAmountSummaryCollection GetEmployeeRoeAmountSummary(DatabaseUser user, long employeePositionId)
        {
            return _sqlServerEmployeeRoeAmount.SelectEmployeeRoeAmountSummary(user, employeePositionId);
        }
        public void UpdateEmployeeRoeAmount(DatabaseUser user, EmployeeRoeAmount roeObj)
        {
            _sqlServerEmployeeRoeAmount.Update(user, roeObj);
        }
        public EmployeeRoeAmountDetail UpdateEmployeeRoeDetail(DatabaseUser user, EmployeeRoeAmountDetail collection)
        {
            return _sqlServerEmployeeRoeAmountDetail.Update(user, collection);
        }
        public EmployeeRoeAmountDetailSummaryCollection GetRoeDataDetailsSummary(DatabaseUser user, long employeePositionId, long employeeId)
        {
            return _sqlServerEmployeeRoeAmountDetail.SelectSummary(user, employeePositionId, employeeId, false);
        }
        #endregion

        #region employee paycode arrears
        public EmployeePaycodeArrearsCollection GetEmployeePaycodeArrears(DatabaseUser user, long employeeId, long? employeePaycodeArrearsId)
        {
            return _sqlServerEmployeePaycodeArrears.Select(user, employeeId, employeePaycodeArrearsId);
        }
        public EmployeePaycodeArrearsCollection GetEmployeePaycodeArrearsBatch(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            return _sqlServerEmployeePaycodeArrears.SelectBatch(user, importExternalIdentifiers);
        }
        public void UpdateEmployeeArrears(DatabaseUser user, EmployeePaycodeArrears item)
        {
            _sqlServerEmployeePaycodeArrears.Update(user, item);
        }
        #endregion

        #region payroll process
        public PayrollMasterCollection GetPayrollMaster(DatabaseUser user, long payrollProcessId)
        {
            return _sqlServerPayrollMaster.Select(user, payrollProcessId);
        }
        public PayrollMasterCollection LoadPayrollMaster(DatabaseUser user, PayrollProcess payrollProcess)
        {
            return _sqlServerPayrollMaster.LoadPayrollMaster(user, payrollProcess);
        }
        public PayrollMasterPaycodeCollection GetPayrollMasterPaycode(DatabaseUser user, long payrollProcessId, bool? includeGarnishments)
        {
            return _sqlServerPayrollMasterPaycode.Select(user, payrollProcessId, includeGarnishments, null, null);
        }
        public void InsertPayrollMasterAndTax(DatabaseUser user, PayrollMasterCollection payrollMaster, List<PayrollMasterTax> payrollOutput, string payrollProcessRunTypeCode)
        {
            _sqlServerPayrollMaster.InsertPayrollMasterAndTax(user, payrollMaster, payrollOutput, payrollProcessRunTypeCode);
        }
        public void PerformTableCleanups(DatabaseUser user, long payrollProcessId)
        {
            _sqlServerPayrollMasterDeduction.DeleteByProcessId(user, payrollProcessId);
            _sqlServerPayrollMasterPayment.DeleteByProcessId(user, payrollProcessId);
            _sqlServerPayrollMasterPaycode.DeleteByProcessId(user, payrollProcessId);
            _sqlServerPayrollMaster.DeletePayrollMasterAndDetail(user, payrollProcessId);
            _sqlServerRemittanceImport.DeleteByProcessId(user, payrollProcessId);
            _sqlServerSalaryPlanGradeAccumulation.Delete(user, payrollProcessId);
        }
        public void DeletePayrollMasterAndDetail(DatabaseUser user, long payrollProcessId)
        {
            _sqlServerPayrollMaster.DeletePayrollMasterAndDetail(user, payrollProcessId);
        }
        public PayrollProcessSummaryCollection GetPayrollProcessSummary(DatabaseUser user, PayrollProcessCriteria criteria)
        {
            return _sqlServerPayrollProcess.SelectPayrollProcessSummary(user, criteria);
        }
        public PayrollProcessCollection GetPayrollProcess(DatabaseUser user, PayrollProcessCriteria criteria)
        {
            return _sqlServerPayrollProcess.Select(user, criteria);
        }
        public DateTime UpdatePayrollProcessStatusCode(DatabaseUser user, long payrollProcessId, string codeStatus, String processException)
        {
            return _sqlServerPayrollProcess.UpdatePayrollProcessStatusCode(user, payrollProcessId, codeStatus, processException);
        }
        public void UpdatePayrollProcess(DatabaseUser user, PayrollProcess item)
        {
            _sqlServerPayrollProcess.Update(user, item);
        }
        public PayrollMasterPaymentErrorCollection GeneratePayrollMasterData(DatabaseUser user, long payrollProcessId)
        {
            _sqlServerPayrollMasterDeduction.GenerateData(user, payrollProcessId);
            _sqlServerPayrollMasterPaycode.GenerateData(user, payrollProcessId);

            return _sqlServerPayrollMasterPayment.GenerateData(user, payrollProcessId);
        }
        public void BatchUpdateEmployeePositionPayrollProcessField(DatabaseUser user, long payrollProcessId)
        {
            _sqlServerPayrollProcess.BatchUpdateEmployeePositionPayrollProcessField(user, payrollProcessId);
        }
        public void RevertUpdateTerminatedEmployees(DatabaseUser user, long payrollProcessId)
        { 
            _sqlServerPayrollProcess.RevertUpdateTerminatedEmployees(user, payrollProcessId);
        }
        public void InsertPayrollProcess(DatabaseUser user, PayrollProcess item)
        {
            _sqlServerPayrollProcess.Insert(user, item);
        }
        public PayDetailCollection GetPayDetail(DatabaseUser user, long payrollProcessId, String paycodeAssociationTypeCode)
        {
            return _sqlServerPayDetail.Select(user, payrollProcessId, paycodeAssociationTypeCode);
        }
        public void UpdateStatusOfPayrollMasterPaymentRecords(DatabaseUser user, RbcEftEdiCollection items, ScotiaBankEdiCollection scotia, string status)
        {
            _sqlServerPayrollMasterPayment.BatchUpdate(user, items, scotia, status);
        }
        #endregion

        #region payroll transaction
        public void ImportPayrollTransaction(String database, String username, System.Collections.Generic.List<PayrollTransactionImportUsedInService> array)
        {
            _sqlServerPayrollTransaction.ImportPayrollTransaction(database, username, array);
        }
        public PayrollTransactionRateProgressionBonusCollection GetPayrollTransactionRateProgressionBonus(DatabaseUser user, long payrollProcessId, BonusRuleCollection rules)
        {
            return _sqlServerPayrollTransaction.SelectPayrollTransactionRateProgressionBonus(user, payrollProcessId, rules);
        }
        public PayrollTransactionRateProgressionCollection GetPayrollTransactionRateProgression(DatabaseUser user, long payrollProcessId)
        {
            return _sqlServerPayrollTransaction.SelectPayrollTransactionRateProgression(user, payrollProcessId);
        }
        public PayrollTransactionSummaryCollection GetPayrollTransactionSummary(DatabaseUser user, long payrollBatchId)
        {
            return _sqlServerPayrollTransaction.SelectPayrollTransactionSummary(user, payrollBatchId);
        }
        public PayrollTransactionPayrollMasterCollection GetPayrollTransactionPayrollMaster(DatabaseUser user, long[] batchIds)
        {
            return _sqlServerPayrollTransaction.GetPayrollTransactionPayrollMaster(user, batchIds);
        }
        public void UpdatePayrollTransaction(DatabaseUser user, PayrollTransaction item)
        {
            _sqlServerPayrollTransaction.Update(user, item);
        }
        public void InsertPayrollTransaction(DatabaseUser user, PayrollTransaction item)
        {
            _sqlServerPayrollTransaction.Insert(user, item);
        }
        public PayrollTransactionErrorCollection InsertPayrollTransactionBatch(DatabaseUser user, PayrollTransactionCollection items)
        {
            return _sqlServerPayrollTransaction.InsertBatch(user, items);
        }
        public PayrollTransactionEmployeePaycodeCollection GetPayrollTransactionEmployeePaycode(DatabaseUser user, long payrollProcessId)
        {
            return _sqlServerPayrollTransaction.GetPayrollTransactionEmployeePaycode(user, payrollProcessId);
        }
        public EmployeePaycodeCalculationCollection GetEmployeePaycodeAmount(DatabaseUser user, long payrollProcessId, bool newArrearsProcessingFlag)
        {
            return _sqlServerPayrollTransaction.GetEmployeePaycodeAmount(user, payrollProcessId, newArrearsProcessingFlag);
        }
        public void DeletePayrollTransaction(DatabaseUser user, PayrollTransaction item)
        {
            _sqlServerPayrollTransaction.Delete(user, item);
        }
        public void DeletePayrollTransactionByBatchId(DatabaseUser user, long payrollBatchId)
        {
            _sqlServerPayrollTransaction.DeleteByBatchId(user, payrollBatchId);
        }
        public void DeleteFtpImportBatchCascade(DatabaseUser user, string codePayrollProcessRunTypeCode, string codePayrollProcessGroupCd)
        {
            _sqlServerPayrollTransaction.DeleteImportBatchCascade(user, codePayrollProcessRunTypeCode, codePayrollProcessGroupCd);
        }
        public PayrollTransactionCollection CheckForBonusWithDeduction(DatabaseUser user, long[] batchIds)
        {
            return _sqlServerPayrollTransaction.CheckForBonusWithDeduction(user, batchIds);
        }
        public BonusRuleCollection GetBonusRule(DatabaseUser user, DateTime cutoffDate)
        {
            return _sqlServerPayrollTransaction.GetBonusRule(user, cutoffDate);
        }


        #endregion

        #region SalaryPlanGradeAccumulation
        public void InsertPayrollTransaction(DatabaseUser user, SalaryPlanGradeAccumulationCollection items)
        {
            _sqlServerSalaryPlanGradeAccumulation.Insert(user, items);
        }
        #endregion

        #region roe
        public RoeCollection GetRoe(DatabaseUser user, long roeId)
        {
            return _sqlServerRoe.Select(user, roeId);
        }
        public void UpdateRoe(DatabaseUser user, Roe item)
        {
            _sqlServerRoe.Update(user, item);
        }
        public void InsertRoe(DatabaseUser user, Roe item)
        {
            _sqlServerRoe.Insert(user, item);
        }
        public void DeleteRoe(DatabaseUser user, Roe item)
        {
            _sqlServerRoe.Delete(user, item);
        }
        #endregion

        public CitiEftPayrollMasterPaymentCollection GetCitiEftPayrollMasterPayment(DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            return _sqlCitiEftPayrollMasterPayment.Select(user, payrollProcessId, employeeNumber);
        }

        #region export ftp
        public ExportFtpCollection GetExportFtp(DatabaseUser user, long? exportFtpId, string codeExportEftTypeCd, string ftpName)
        {
            return _sqlServerExportFtp.Select(user, exportFtpId, codeExportEftTypeCd, ftpName);
        }
        public void UpdateExportFtp(DatabaseUser user, ExportFtp export)
        {
            _sqlServerExportFtp.Update(user, export);
        }
        public ExportFtp InsertExportFtp(DatabaseUser user, ExportFtp export)
        {
            return _sqlServerExportFtp.Insert(user, export);
        }
        public void DeleteExportFtp(DatabaseUser user, ExportFtp export)
        {
            _sqlServerExportFtp.Delete(user, export);
        }
        #endregion

        #region pay register export
        public PayRegisterExportCollection GetPayRegisterExport(DatabaseUser user, long? exportQueueId, string codeExportFtpTypeCd)
        {
            return _sqlServerPayRegisterExport.Select(user, exportQueueId, codeExportFtpTypeCd);
        }
        public void UpdatePayRegisterExport(DatabaseUser user, PayRegisterExport export)
        {
            _sqlServerPayRegisterExport.Update(user, export);
        }
        public PayRegisterExport InsertPayRegisterExport(DatabaseUser user, PayRegisterExport export)
        {
            return _sqlServerPayRegisterExport.Insert(user, export);
        }
        public void DeletePayRegisterExport(DatabaseUser user, PayRegisterExport export)
        {
            _sqlServerPayRegisterExport.Delete(user, export);
        }
        #endregion

        #region paycode year end report map
        public PaycodeYearEndReportMapCollection GetPaycodeYearEndReportMap(DatabaseUser user, String paycodeCode)
        {
            return _sqlServerPaycodeYearEndReportMap.Select(user, paycodeCode);
        }
        public void UpdatePaycodeYearEndReportMap(DatabaseUser user, PaycodeYearEndReportMap paycodeYearEndReportMap)
        {
            _sqlServerPaycodeYearEndReportMap.Update(user, paycodeYearEndReportMap);
        }
        public PaycodeYearEndReportMap InsertPaycodeYearEndReportMap(DatabaseUser user, PaycodeYearEndReportMap paycodeYearEndReportMap)
        {
            return _sqlServerPaycodeYearEndReportMap.Insert(user, paycodeYearEndReportMap);
        }
        public void DeletePaycodeYearEndReportMap(DatabaseUser user, PaycodeYearEndReportMap paycodeYearEndReportMap, bool deleteByPaycode = false)
        {
            _sqlServerPaycodeYearEndReportMap.Delete(user, paycodeYearEndReportMap, deleteByPaycode);
        }
        #endregion

        #region paycode payroll transaction offset association
        public PaycodePayrollTransactionOffsetAssociationCollection GetPaycodePayrollTransactionOffsetAssociation(DatabaseUser user, String paycodeCode)
        {
            return _sqlServerPaycodePayrollTransactionOffsetAssociation.Select(user, paycodeCode);
        }
        public void UpdatePaycodePayrollTransactionOffsetAssociation(DatabaseUser user, PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation)
        {
            _sqlServerPaycodePayrollTransactionOffsetAssociation.Update(user, paycodePayrollTransactionOffsetAssociation);
        }
        public PaycodePayrollTransactionOffsetAssociation InsertPaycodePayrollTransactionOffsetAssociation(DatabaseUser user, PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation)
        {
            return _sqlServerPaycodePayrollTransactionOffsetAssociation.Insert(user, paycodePayrollTransactionOffsetAssociation);
        }
        public void DeletePaycodePayrollTransactionOffsetAssociation(DatabaseUser user, PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation, bool deleteByPaycode = false)
        {
            _sqlServerPaycodePayrollTransactionOffsetAssociation.Delete(user, paycodePayrollTransactionOffsetAssociation, deleteByPaycode);
        }
        #endregion

        public TempBulkPayrollProcessCollection GetTempBulkPayrollProcess(DatabaseUser user)
        {
            return _sqlServerTempBulkPayrollProcess.Select(user);
        }

        #region remittance
        public RemittanceCollection GetRemittance(DatabaseUser user)
        {
            return _sqlServerRemittance.Select(user);
        }
        #endregion

        #region remittance detail
        public RemittanceDetailCraSourceDeductionsCollection GetRemittanceDetailCraSourceDeductions(DatabaseUser user, long? craExportId, string remitCode, DateTime remitDate)
        {
            return _sqlServerRemittanceDetailCraSourceDeductions.Select(user, craExportId, remitCode, remitDate);
        }
        public RemittanceDetailWcbDeductionsCollection GetRemittanceDetailWcbDeductions(DatabaseUser user, string detailType, long? wcbExportId, string remitCode, DateTime remitDate)
        {
            return _sqlServerRemittanceDetailWcbDeductions.Select(user, detailType, wcbExportId, remitCode, remitDate);
        }
        public RemittanceDetailChequeHealthTaxDeductionsCollection GetRemittanceDetailChequeHealthTaxDeductions(DatabaseUser user, long? chequeHealthTaxExportId, string codeHealthTaxCode)
        {
            return _sqlServerRemittanceDetailChequeHealthTaxDeductions.Select(user, chequeHealthTaxExportId, codeHealthTaxCode);
        }
        public RemittanceDetailGarnishmentDeductionsCollection GetRemittanceDetailGarnishmentDeductions(DatabaseUser user, string detailType, long? exportId)
        {
            return _sqlServerRemittanceDetailGarnishmentDeductions.Select(user, detailType, exportId);
        }
        public RemittanceDetailRqSourceDeductionsCollection GetRemittanceDetailRqSourceDeductions(DatabaseUser user, long? rqExportId)
        {
            return _sqlServerRemittanceDetailRqSourceDeductions.Select(user, rqExportId);
        }
        #endregion

        #region remittance import
        public RemittanceImportCollection GetRemittanceImport(DatabaseUser user, long? remittanceImportId)
        {
            return _sqlServerRemittanceImport.Select(user, remittanceImportId);
        }
        //public RemittanceImportDetailCollection GetRemittanceImportDetail(DatabaseUser user, long remittanceImportId)
        //{
        //    return _sqlServerRemittanceImport.SelectDetail(user, remittanceImportId);
        //}
        //public void ProcessRemittanceImport(DatabaseUser user, long remittanceImportId, String remittanceImportStatusCode)
        //{
        //    _sqlServerRemittanceImport.Process(user, remittanceImportId, remittanceImportStatusCode);
        //}

        public void DeleteByProcessId(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            _sqlServerRemittanceImport.DeleteByProcessId(user, payrollProcessId);
        }

        public void InsertRemittanceImportBatch(DatabaseUser user, RemittanceImport remittanceImport)
        {
            _sqlServerRemittanceImport.InsertBatch(user, remittanceImport);
        }
        public String ProcessRemittanceImport(DatabaseUser user, long payrollProcessId)
        {
            return _sqlServerRemittanceImport.ProcessRemittanceImport(user, payrollProcessId);
        }
        #endregion

        #region health tax
        public HealthTaxCollection GetHealthTax(DatabaseUser user, long? healthTaxId, bool isCeridianBilling = false)
        {
            return _sqlServerHealthTax.Select(user, healthTaxId, isCeridianBilling);
        }
        public HealthTax InsertHealthTax(DatabaseUser user, HealthTax healthTax)
        {
            return _sqlServerHealthTax.Insert(user, healthTax);
        }
        public void UpdateHealthTax(DatabaseUser user, HealthTax healthTax)
        {
            _sqlServerHealthTax.Update(user, healthTax);
        }
        public void DeleteHealthTax(DatabaseUser user, HealthTax healthTax)
        {
            _sqlServerHealthTax.Delete(user, healthTax);
        }
        #endregion

        #region health tax detail
        public HealthTaxDetailCollection GetHealthTaxDetail(DatabaseUser user, long healthTaxId)
        {
            return _sqlServerHealthTaxDetail.Select(user, healthTaxId);
        }
        public HealthTaxDetail InsertHealthTaxDetail(DatabaseUser user, HealthTaxDetail healthTaxDetail)
        {
            return _sqlServerHealthTaxDetail.Insert(user, healthTaxDetail);
        }
        public void UpdateHealthTaxDetail(DatabaseUser user, HealthTaxDetail healthTaxDetail)
        {
            _sqlServerHealthTaxDetail.Update(user, healthTaxDetail);
        }
        public void DeleteHealthTaxDetail(DatabaseUser user, HealthTaxDetail healthTaxDetail)
        {
            _sqlServerHealthTaxDetail.Delete(user, healthTaxDetail);
        }
        #endregion

        #region payroll process
        public PayrollEmployeePaycodeCollection SelectPayrollEmployeePaycode(DatabaseUser user, long payrollProcessId)
        {
            return _sqlServerPayroll.SelectPayrollEmployeePaycode(user, payrollProcessId);
        }
        public PayrollTransactionIncomeCollection SelectPayrollTransactionIncome(DatabaseUser user, long payrollProcessId, String payrollProcessGroupCode, String payrollProcessRunTypeCode)
        {
            return _sqlServerPayroll.SelectPayrollTransactionIncome(user, payrollProcessId, payrollProcessGroupCode, payrollProcessRunTypeCode);
        }
        public PayrollEmployeeYearToDatePaycodeCollection SelectPayrollEmployeeYearToDatePaycode(DatabaseUser user, long payrollProcessId, int periodYear)
        {
            return _sqlServerPayroll.SelectPayrollEmployeeYearToDatePaycode(user, payrollProcessId, periodYear);
        }

        //auto payroll method
        public AutoPayrollScheduleCollection GetAutoPayrollSchedule(DatabaseUser user, DateTime todaysDate)
        {
            return _sqlServerAutoPayroll.Select(user, todaysDate);
        }
        public void UpdateAutoPayrollScheduleProcessed(DatabaseUser user, AutoPayrollSchedule schedule)
        {
            _sqlServerAutoPayroll.UpdateProcessed(user, schedule);
        }

        #endregion

        #region Advantage Time Record

        public void ImportAdvantageTimeRecord(DatabaseUser user, AdvantageTimeRecord advantageTimeRecord)
        {
            _sqlServerAdvantageTimeRecord.ImportAdvantageTimeRecord(user, advantageTimeRecord);
        }

        #endregion
    }
}