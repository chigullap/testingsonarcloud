﻿using System;

using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.DataLayer.DataAccess.SqlServer;

namespace WorkLinks.DataLayer.DataAccess
{
    public class ScreenControlAccess
    {

        SqlServer.SqlServerScreenControl _sqlServerScreenControl = new SqlServer.SqlServerScreenControl();
 
        #region ScreenControl 
        public void Merge(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ScreenControl[] screenControlArray)
        {
                _sqlServerScreenControl.Merge(user, screenControlArray);
        }
        #endregion

    }
    
[Serializable]
    public class ScreenControlAccessException : DataAccessException
    {
        new public enum ExceptionCodes
        {
            DataConcurrency,
            ForeignKeyConstraint,
            Other,
        }

        #region fields
        ExceptionCodes _exceptionCode = 0;
        #endregion

        #region properties
        new public ExceptionCodes ExceptionCode
        {
            get { return _exceptionCode; }
            set { _exceptionCode = value; }
        }
        #endregion

        public ScreenControlAccessException(String message, SqlServerException.ExceptionCodes code, Exception exc)
            : base(message, (DataAccessException.ExceptionCodes)code, exc)
        {
            ExceptionCode = (ExceptionCodes)code;
        }
    }
}
