﻿using System;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess
{
    public class AccrualAccess
    {
        #region properties
        SqlServer.SqlServerAccrualEntitlement _sqlServerAccrualEntitlement = new SqlServer.SqlServerAccrualEntitlement();
        SqlServer.SqlServerAccrualEntitlementDetail _sqlServerAccrualEntitlementDetail = new SqlServer.SqlServerAccrualEntitlementDetail();
        SqlServer.SqlServerAccrualEntitlementDetailPaycode _sqlServerAccrualEntitlementDetailPaycode = new SqlServer.SqlServerAccrualEntitlementDetailPaycode();
        SqlServer.SqlServerAccrualEntitlementDetailValue _sqlServerAccrualEntitlementDetailValue = new SqlServer.SqlServerAccrualEntitlementDetailValue();
        SqlServer.SqlServerAccrualEntitlementSearch _sqlServerAccrualEntitlementSearch = new SqlServer.SqlServerAccrualEntitlementSearch();
        SqlServer.SqlServerAccrualPolicy _sqlServerAccrualPolicy = new SqlServer.SqlServerAccrualPolicy();
        SqlServer.SqlServerAccrualPolicyEntitlement _sqlServerAccrualPolicyEntitlement = new SqlServer.SqlServerAccrualPolicyEntitlement();
        SqlServer.SqlServerAccrualPolicySearch _sqlServerAccrualPolicySearch = new SqlServer.SqlServerAccrualPolicySearch();
        #endregion

        #region accrual entitlement search
        public AccrualEntitlementSearchCollection GetAccrualEntitlementReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String criteria)
        {
            return _sqlServerAccrualEntitlementSearch.Select(user, criteria);
        }
        #endregion

        #region accrual entitlement
        public AccrualEntitlementCollection GetAccrualEntitlement(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? entitlementId)
        {
            return _sqlServerAccrualEntitlement.Select(user, entitlementId);
        }
        public AccrualEntitlement InsertAccrualEntitlement(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlement entitlement)
        {
            return _sqlServerAccrualEntitlement.Insert(user, entitlement);
        }
        public void UpdateAccrualEntitlement(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlement entitlement)
        {
            _sqlServerAccrualEntitlement.Update(user, entitlement);
        }
        public void DeleteAccrualEntitlement(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlement entitlement)
        {
            _sqlServerAccrualEntitlement.Delete(user, entitlement);
        }
        #endregion

        #region accrual entitlement detail
        public AccrualEntitlementDetailCollection GetAccrualEntitlementDetail(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long entitlementId, long entitlementDetailId)
        {
            return _sqlServerAccrualEntitlementDetail.Select(user, entitlementId, entitlementDetailId);
        }
        public AccrualEntitlementDetail InsertAccrualEntitlementDetail(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetail entitlementDetail)
        {
            return _sqlServerAccrualEntitlementDetail.Insert(user, entitlementDetail);
        }
        public void UpdateAccrualEntitlementDetail(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetail entitlementDetail)
        {
            _sqlServerAccrualEntitlementDetail.Update(user, entitlementDetail);
        }
        public void DeleteAccrualEntitlementDetail(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetail entitlementDetail)
        {
            _sqlServerAccrualEntitlementDetail.Delete(user, entitlementDetail);
        }
        #endregion

        #region accrual entitlement detail paycode
        public AccrualEntitlementDetailPaycodeCollection GetAccrualEntitlementDetailPaycode(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long entitlementDetailId)
        {
            return _sqlServerAccrualEntitlementDetailPaycode.Select(user, entitlementDetailId);
        }
        public AccrualEntitlementDetailPaycode InsertAccrualEntitlementDetailPaycode(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetailPaycode detailPaycode)
        {
            return _sqlServerAccrualEntitlementDetailPaycode.Insert(user, detailPaycode);
        }
        public void DeleteAccrualEntitlementDetailPaycode(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long entitlementDetailId)
        {
            _sqlServerAccrualEntitlementDetailPaycode.Delete(user, entitlementDetailId);
        }
        #endregion

        #region accrual entitlement detail value
        public AccrualEntitlementDetailValueCollection GetAccrualEntitlementDetailValue(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long entitlementDetailId)
        {
            return _sqlServerAccrualEntitlementDetailValue.Select(user, entitlementDetailId);
        }
        public AccrualEntitlementDetailValue InsertAccrualEntitlementDetailValue(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetailValue detailValue)
        {
            return _sqlServerAccrualEntitlementDetailValue.Insert(user, detailValue);
        }
        public void UpdateAccrualEntitlementDetailValue(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetailValue detailValue)
        {
            _sqlServerAccrualEntitlementDetailValue.Update(user, detailValue);
        }
        public void DeleteAccrualEntitlementDetailValue(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetailValue detailValue)
        {
            _sqlServerAccrualEntitlementDetailValue.Delete(user, detailValue);
        }
        #endregion

        #region accrual policy search
        public AccrualPolicySearchCollection GetAccrualPolicyReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String criteria)
        {
            return _sqlServerAccrualPolicySearch.Select(user, criteria);
        }
        #endregion

        #region accrual policy
        public AccrualPolicyCollection GetAccrualPolicy(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? policyId)
        {
            return _sqlServerAccrualPolicy.Select(user, policyId);
        }
        public AccrualPolicy InsertAccrualPolicy(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualPolicy policy)
        {
            return _sqlServerAccrualPolicy.Insert(user, policy);
        }
        public void UpdateAccrualPolicy(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualPolicy policy)
        {
            _sqlServerAccrualPolicy.Update(user, policy);
        }
        public void DeleteAccrualPolicy(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualPolicy policy)
        {
            _sqlServerAccrualPolicy.Delete(user, policy);
        }
        #endregion

        #region accrual policy entitlement
        public AccrualPolicyEntitlementCollection GetAccrualPolicyEntitlement(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long policyId)
        {
            return _sqlServerAccrualPolicyEntitlement.Select(user, policyId);
        }
        public AccrualPolicyEntitlement InsertAccrualPolicyEntitlement(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualPolicyEntitlement policyEntitlement)
        {
            return _sqlServerAccrualPolicyEntitlement.Insert(user, policyEntitlement);
        }
        public void DeleteAccrualPolicyEntitlement(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long policyId)
        {
            _sqlServerAccrualPolicyEntitlement.Delete(user, policyId);
        }
        #endregion
    }
}