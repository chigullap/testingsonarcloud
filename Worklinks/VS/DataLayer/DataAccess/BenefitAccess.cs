﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess
{
    public class BenefitAccess
    {
        #region properties
        SqlServer.SqlServerBenefitPolicySearch _sqlServerBenefitPolicySearch = new SqlServer.SqlServerBenefitPolicySearch();
        SqlServer.SqlServerBenefitPlanSearch _sqlServerBenefitPlanSearch = new SqlServer.SqlServerBenefitPlanSearch();
        SqlServer.SqlServerBenefitPolicy _sqlServerBenefitPolicy = new SqlServer.SqlServerBenefitPolicy();
        SqlServer.SqlServerBenefitPlan _sqlServerBenefitPlan = new SqlServer.SqlServerBenefitPlan();
        SqlServer.SqlServerBenefitPlanDetail _sqlServerBenefitPlanDetail = new SqlServer.SqlServerBenefitPlanDetail();
        SqlServer.SqlServerBenefitPolicyPlan _sqlServerBenefitPolicyPlan = new SqlServer.SqlServerBenefitPolicyPlan();
        #endregion

        #region benefit policy search
        public BenefitPolicySearchCollection GetBenefitPolicyReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String criteria)
        {
            return _sqlServerBenefitPolicySearch.Select(user, criteria);
        }
        #endregion

        #region benefit plan search
        public BenefitPlanSearchCollection GetBenefitPlanReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String criteria)
        {
            return _sqlServerBenefitPlanSearch.Select(user, criteria);
        }
        #endregion

        #region benefit policy
        public BenefitPolicyCollection GetBenefitPolicy(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? benefitPolicyId)
        {
            return _sqlServerBenefitPolicy.Select(user, benefitPolicyId);
        }

        public BenefitPolicy InsertBenefitPolicy(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPolicy benefitPolicy)
        {
            return _sqlServerBenefitPolicy.Insert(user, benefitPolicy);
        }

        public void UpdateBenefitPolicy(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPolicy benefitPolicy)
        {
            _sqlServerBenefitPolicy.Update(user, benefitPolicy);
        }

        public void DeleteBenefitPolicy(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPolicy benefitPolicy)
        {
            _sqlServerBenefitPolicy.Delete(user, benefitPolicy);
        }
        #endregion

        #region benefit plan
        public BenefitPlanCollection GetBenefitPlan(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? benefitPlanId)
        {
            return _sqlServerBenefitPlan.Select(user, benefitPlanId);
        }

        public BenefitPlan InsertBenefitPlan(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPlan benefitPlan)
        {
            return _sqlServerBenefitPlan.Insert(user, benefitPlan);
        }

        public void UpdateBenefitPlan(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPlan benefitPlan)
        {
            _sqlServerBenefitPlan.Update(user, benefitPlan);
        }

        public void DeleteBenefitPlan(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPlan benefitPlan)
        {
            _sqlServerBenefitPlan.Delete(user, benefitPlan);
        }
        #endregion

        #region benefit plan detail
        public BenefitPlanDetailCollection GetBenefitPlanDetail(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long benefitPlanId, long benefitPlanDetailId)
        {
            return _sqlServerBenefitPlanDetail.Select(user, benefitPlanId, benefitPlanDetailId);
        }

        public BenefitPlanDetail InsertBenefitPlanDetail(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPlanDetail benefitPlanDetail)
        {
            return _sqlServerBenefitPlanDetail.Insert(user, benefitPlanDetail);
        }

        public void UpdateBenefitPlanDetail(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPlanDetail benefitPlanDetail)
        {
            _sqlServerBenefitPlanDetail.Update(user, benefitPlanDetail);
        }

        public void DeleteBenefitPlanDetail(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPlanDetail benefitPlanDetail)
        {
            _sqlServerBenefitPlanDetail.Delete(user, benefitPlanDetail);
        }
        #endregion

        #region benefit policy plan
        public BenefitPolicyPlanCollection GetBenefitPolicyPlan(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long benefitPolicyId)
        {
            return _sqlServerBenefitPolicyPlan.Select(user, benefitPolicyId);
        }

        public BenefitPolicyPlan InsertBenefitPolicyPlan(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPolicyPlan benefitPolicyPlan)
        {
            return _sqlServerBenefitPolicyPlan.Insert(user, benefitPolicyPlan);
        }

        public void DeleteBenefitPolicyPlan(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long benefitPolicyId)
        {
            _sqlServerBenefitPolicyPlan.Delete(user, benefitPolicyId);
        }
        #endregion
    }
}
