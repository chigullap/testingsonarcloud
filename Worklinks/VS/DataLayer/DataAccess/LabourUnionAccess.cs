﻿using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess
{
    public class LabourUnionAccess
    {
        #region properties
        SqlServer.SqlServerLabourUnion _sqlServerLabourUnion = new SqlServer.SqlServerLabourUnion();
        SqlServer.SqlServerUnionCollectiveAgreement _sqlServerUnionCollectiveAgreement = new SqlServer.SqlServerUnionCollectiveAgreement();
        #endregion

        #region labour union
        public LabourUnionSummaryCollection GetLabourUnion(DatabaseUser user, long? labourUnionId)
        {
            return _sqlServerLabourUnion.Select(user, labourUnionId);
        }
        public LabourUnion InsertLabourUnion(DatabaseUser user, LabourUnion union)
        {
            return _sqlServerLabourUnion.Insert(user, union);
        }
        public LabourUnion UpdateLabourUnion(DatabaseUser user, LabourUnion union)
        {
            return _sqlServerLabourUnion.Update(user, union);
        }
        public void DeleteLabourUnion(DatabaseUser user, LabourUnion union)
        {
            _sqlServerLabourUnion.Delete(user, union);
        }
        #endregion

        #region union collective agreement
        public UnionCollectiveAgreementCollection GetCollectiveAgreement(DatabaseUser user, long? labourUnionId)
        {
            return _sqlServerUnionCollectiveAgreement.Select(user, labourUnionId);
        }
        public void UpdateUnionCollectiveAgreement(DatabaseUser user, UnionCollectiveAgreement collectiveAgreement)
        {
            _sqlServerUnionCollectiveAgreement.Update(user, collectiveAgreement);
        }
        public void DeleteUnionCollectiveAgreement(DatabaseUser user, UnionCollectiveAgreement collectiveAgreement)
        {
            _sqlServerUnionCollectiveAgreement.Delete(user, collectiveAgreement);
        }
        public UnionCollectiveAgreement InsertUnionCollectiveAgreement(DatabaseUser user, UnionCollectiveAgreement collectiveAgreement)
        {
            return _sqlServerUnionCollectiveAgreement.Insert(user, collectiveAgreement);
        }
        #endregion
    }
}