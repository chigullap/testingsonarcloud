﻿using System;
using System.Collections.Generic;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;

namespace WorkLinks.DataLayer.DataAccess
{
    public class PersonAccess
    {
        #region properties
        SqlServer.SqlServerPersonAddress _sqlServerPersonalAddress = new SqlServer.SqlServerPersonAddress();
        SqlServer.SqlServerPersonContactChannel _sqlServerPersonContactChannel = new SqlServer.SqlServerPersonContactChannel();
        SqlServer.SqlServerContactChannel _sqlServerContactChannel = new SqlServer.SqlServerContactChannel();
        SqlServer.SqlServerPerson _sqlServerPerson = new SqlServer.SqlServerPerson();
        SqlServer.SqlServerGovernmentIdentificationNumberTypeTemplate _sqlGovernmentIdentificationNumberTypeTemplate = new SqlServer.SqlServerGovernmentIdentificationNumberTypeTemplate();
        #endregion

        #region person
        public PersonCollection GetPerson(DatabaseUser user, long personId, bool removeFrenchAccents = false)
        {
            return _sqlServerPerson.Select(user, personId, removeFrenchAccents);
        }
        public PersonCollection GetPersonByEmployeeNumber(DatabaseUser user, String employeeNumber)
        {
            return _sqlServerPerson.SelectByEmployeeNumber(user, employeeNumber);
        }
        public void UpdateAuthPerson(DatabaseUser user, string adminDatabaseName, string userName, Person person)
        {
            _sqlServerPerson.UpdateAuthPerson(user, adminDatabaseName, userName, person);
        }
        public void UpdatePerson(DatabaseUser user, Person person)
        {
            _sqlServerPerson.Update(user, person);
        }
        public Person UpdateUnionContactPerson(DatabaseUser user, Person person)
        {
            return _sqlServerPerson.UpdateUnionPerson(user, person);
        }
        public Person InsertPerson(DatabaseUser user, string overrideDatabaseName, Person person)
        {
            return _sqlServerPerson.Insert(user, overrideDatabaseName, person);
        }
        public void DeletePerson(DatabaseUser user, Person person)
        {
            _sqlServerPerson.Delete(user, person);
        }
        public bool CheckDatabaseForDuplicateSin(DatabaseUser user, string sin, long employeeId, string employeeNumber)
        {
            return _sqlServerPerson.CheckDatabaseForDuplicateSin(user, sin, employeeId, employeeNumber);
        }
        #endregion

        #region person address
        public void ImportAddressUsingService(String database, String username, List<AddressImport> array)
        {
            _sqlServerPersonalAddress.ImportAddressUsingService(database, username, array);
        }
        public void ImportAddress(DatabaseUser user, WorkLinksPersonAddressCollection existingAddresses)
        {
            _sqlServerPersonalAddress.ImportAddress(user, existingAddresses);
        }
        public PersonAddressCollection GetPersonAddress(DatabaseUser user, long? personId, bool removeFrenchAccents = false)
        {
            return GetPersonAddress(user, personId, null, null, removeFrenchAccents);
        }
        public PersonAddress GetPrimaryPersonAddress(DatabaseUser user, long? personId)
        {
            foreach (PersonAddress address in GetPersonAddress(user, personId, null, null))
            {
                if (address.PrimaryFlag)
                    return address;
            }

            return null;
        }
        public PersonAddressCollection GetPersonAddress(DatabaseUser user, long? personId, long? personAddressId, long? addressId, bool removeFrenchAccents = false)
        {
            return _sqlServerPersonalAddress.Select(user, personId, personAddressId, addressId, removeFrenchAccents);
        }
        public void UpdatePersonAddress(DatabaseUser user, PersonAddress address)
        {
            _sqlServerPersonalAddress.Update(user, address);
        }
        public void InsertPersonAddress(DatabaseUser user, PersonAddress address)
        {
            _sqlServerPersonalAddress.Insert(user, address);
        }
        public void DeletePersonAddress(DatabaseUser user, PersonAddress address)
        {
            _sqlServerPersonalAddress.Delete(user, address);
        }
        public void DeletePersonAddressByPersonId(DatabaseUser user, long personId)
        {
            _sqlServerPersonalAddress.DeleteByPersonId(user, personId);
        }
        #endregion

        #region person ContactChannel
        public void ImportPhone(String database, String username, List<PhoneImport> array)
        {
            _sqlServerContactChannel.ImportPhone(database, username, array);
        }
        public PersonContactChannelCollection GetPersonContactChannel(DatabaseUser user, long? personId, String contactChannelCategoryCode, long? contactChannelId)
        {
            return _sqlServerPersonContactChannel.Select(user, personId, contactChannelCategoryCode, contactChannelId);
        }
        public void UpdatePersonContactChannel(DatabaseUser user, PersonContactChannel contact)
        {
            _sqlServerPersonContactChannel.Update(user, contact);
        }
        public void InsertPersonContactChannel(DatabaseUser user, PersonContactChannel contact)
        {
            _sqlServerPersonContactChannel.Insert(user, contact);
        }
        public void DeletePersonContactChannel(DatabaseUser user, PersonContactChannel contact)
        {
            _sqlServerPersonContactChannel.Delete(user, contact);
        }
        public void DeletePersonContactChannelByPersonId(DatabaseUser user, long personId)
        {
            _sqlServerPersonContactChannel.DeleteByPersonId(user, personId);
        }
        #endregion

        #region ContactChannel
        public ContactChannelCollection GetContactChannel(DatabaseUser user, long? contactChannelId)
        {
            return _sqlServerContactChannel.Select(user, contactChannelId);
        }
        public String GetContactChannelCategory(DatabaseUser user, PersonContactChannel channel)
        {
            return _sqlServerPersonContactChannel.GetContactChannelCategory(user, channel);
        }
        public void UpdateContactChannel(DatabaseUser user, ContactChannel contact)
        {
            _sqlServerContactChannel.Update(user, contact);
        }
        public void InsertContactChannel(DatabaseUser user, ContactChannel contact)
        {
            _sqlServerContactChannel.Insert(user, contact);
        }
        public void DeleteContactChannel(DatabaseUser user, ContactChannel contact)
        {
            _sqlServerContactChannel.Delete(user, contact);
        }
        #endregion

        #region government identification number type template
        public GovernmentIdentificationNumberTypeTemplateCollection GetGovernmentIdentificationNumberTypeTemplate(DatabaseUser user, String numberTypeCode)
        {
            return _sqlGovernmentIdentificationNumberTypeTemplate.Select(user, numberTypeCode);
        }
        #endregion
    }
}