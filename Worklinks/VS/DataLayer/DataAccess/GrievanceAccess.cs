﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess
{
    public class GrievanceAccess
    {
        SqlServer.SqlServerGrievance _sqlGrievance = new SqlServer.SqlServerGrievance();
        SqlServer.SqlServerGrievanceAction _sqlGrievanceAction = new SqlServer.SqlServerGrievanceAction();

        #region grievance
        public GrievanceCollection GetGrievance(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long id)
        {
            GrievanceCollection collection  =  _sqlGrievance.Select(user,id);
            foreach (Grievance grievance in collection)
            {
                grievance.Actions = _sqlGrievanceAction.Select(user, grievance.GrievanceId);
            }

            return collection;
        }
        public void UpdateGrievance(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Grievance data)
        {
            _sqlGrievance.Update(user, data);
        }
        public void InsertGrievance(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Grievance data)
        {
            _sqlGrievance.Insert(user, data);
        }
        public void DeleteGrievance(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Grievance data)
        {
            _sqlGrievance.Delete(user, data);
        }
        #endregion

        #region grievance action
        public GrievanceActionCollection GetGrievanceAction(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long grievanceId)
        {
            return _sqlGrievanceAction.Select(user, grievanceId);
        }
        public void UpdateGrievanceAction(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, GrievanceAction data)
        {
            _sqlGrievanceAction.Update(user, data);
        }
        public void InsertGrievanceAction(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, GrievanceAction data)
        {
            _sqlGrievanceAction.Insert(user, data);
        }
        public void DeleteGrievanceAction(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, GrievanceAction data)
        {
            _sqlGrievanceAction.Delete(user, data);
        }
        #endregion

        #region grievance reports
        public GrievanceCollection GetGrievanceSummary(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, GrievanceCriteria criteria)
        {
            return _sqlGrievance.SelectGrievanceSummary(user, criteria);
        }
        #endregion
    }
}
