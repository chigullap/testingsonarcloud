﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess
{
    public class ErrorLogAccess
    {
        SqlServer.SqlServerErrorLog _sqlServerErrorLog = new SqlServer.SqlServerErrorLog();

        public void InsertErrorLog(DatabaseUser user, String methodName, Exception exc)
        {
            _sqlServerErrorLog.InsertErrorLog(user, methodName, exc);
        }
    }
}
