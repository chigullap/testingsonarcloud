﻿using System;
using System.Collections.Generic;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess
{
    public class WizardAccess
    {
        #region fields
        SqlServer.SqlServerWizardCache _sqlServerWizardCache = new SqlServer.SqlServerWizardCache();
        SqlServer.SqlServerWizardCacheItem _sqlServerWizardCacheItem = new SqlServer.SqlServerWizardCacheItem();
        #endregion

        #region wizard
        public WizardCacheCollection GetWizardCache(DatabaseUser user, long? wizardCacheId)
        {
            return _sqlServerWizardCache.Select(user, wizardCacheId);
        }
        public WizardCacheCollection GetWizardCacheBatch(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            return _sqlServerWizardCache.SelectBatch(user, importExternalIdentifiers);
        }
        public WizardCacheItemCollection GetWizardCacheItem(DatabaseUser user, long wizardId, long? wizardCacheId, String itemName)
        {
            return _sqlServerWizardCacheItem.Select(user, wizardId, wizardCacheId, itemName);
        }
        public WizardCacheItemCollection GetWizardCacheItemBatch(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            return _sqlServerWizardCacheItem.SelectBatch(user, importExternalIdentifiers);
        }
        public WizardCacheItemCollection GetWizardCacheItemSummary(DatabaseUser user, long wizardId, long? wizardItemId, long? wizardCacheId, bool templateFlag, String description)
        {
            return _sqlServerWizardCacheItem.SelectWizardCacheItemSummary(user, wizardId, wizardItemId, wizardCacheId, templateFlag, description);
        }
        public void UpdateWizardCache(DatabaseUser user, WizardCache wizard)
        {
            _sqlServerWizardCache.Update(user, wizard);
        }
        public void UpdateWizardCacheItem(DatabaseUser user, WizardCacheItem item)
        {
            _sqlServerWizardCacheItem.Update(user, item);
        }
        public void InsertWizardCache(DatabaseUser user, WizardCache wizard)
        {
            _sqlServerWizardCache.Insert(user, wizard);
        }
        public void ImportWizardCache(DatabaseUser user, WizardCacheImportCollection importCollection)
        {
            _sqlServerWizardCache.Import(user, importCollection);
        }
        public void InsertWizardCacheItem(DatabaseUser user, WizardCacheItem item)
        {
            _sqlServerWizardCacheItem.Insert(user, item);
        }
        public void DeleteWizardEmployee(DatabaseUser user, long wizardCacheId)
        {
            _sqlServerWizardCache.Delete(user, wizardCacheId);
        }
        #endregion
    }
}