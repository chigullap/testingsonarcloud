﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess
{
    public class WidgetAccess
    {
        #region fields
        SqlServer.SqlServerEmployeePayslip _sqlServerEmployeePayslip = new SqlServer.SqlServerEmployeePayslip();
        SqlServer.SqlServerLabourCost _sqlServerLabourCost = new SqlServer.SqlServerLabourCost();
        SqlServer.SqlServerHeadcount _sqlServerHeadcount = new SqlServer.SqlServerHeadcount();
        SqlServer.SqlServerEmployeeProfile _sqlServerEmployeeProfile = new SqlServer.SqlServerEmployeeProfile();
        SqlServer.SqlServerPayrollBreakdown _sqlServerPayrollBreakdown = new SqlServer.SqlServerPayrollBreakdown();
        #endregion

        #region employee payslip
        public EmployeePayslipCollection GetEmployeePayslip(DatabaseUser user)
        {
            return _sqlServerEmployeePayslip.Select(user);
        }
        #endregion

        #region labour cost
        public LabourCostCollection GetLabourCost(DatabaseUser user, Decimal year)
        {
            return _sqlServerLabourCost.Select(user, year);
        }
        #endregion

        #region headcount
        public HeadcountCollection GetHeadcount(DatabaseUser user)
        {
            return _sqlServerHeadcount.Select(user);
        }
        public HeadcountCollection GetHeadcountByOrganizationUnitLevelId(DatabaseUser user, long organizationUnitLevelId)
        {
            return _sqlServerHeadcount.SelectByOrganizationUnitLevelId(user, organizationUnitLevelId);
        }
        #endregion

        #region employee profile
        public EmployeeProfileCollection GetEmployeeProfile(DatabaseUser user)
        {
            return _sqlServerEmployeeProfile.SelectEmployeeProfile(user);
        }
        #endregion

        #region payroll breakdown
        public PayrollBreakdownCollection GetPayrollBreakdown(DatabaseUser user, String userName, String breakdownType, String filteredBy)
        {
            return _sqlServerPayrollBreakdown.Select(user, userName, breakdownType, filteredBy);
        }
        #endregion
    }
}