﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerFieldLanguage : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ControlFieldValueId = "control_field_value_id";
            public static String ControlFieldId = "control_field_id";
            public static String FieldIdentifier = "field_identifier";
            public static String CodeLanguageCd = "code_language_cd";
            public static String SecurityRoleId = "security_role_id";
            public static String AttributeIdentifier = "attribute_identifier";
            public static String FieldValue = "value";
        }
        #endregion

        #region main
        internal LanguageEntityCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, decimal formId, String attributeIdentifier, String englishLanguageCode)
        {
            DataBaseCommand command = GetStoredProcCommand("FormFieldLanguage_select", user.DatabaseName);
            command.AddParameterWithValue("@formId", formId);
            command.AddParameterWithValue("@attributeIdentifier", attributeIdentifier);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToFieldLanguageCollection(reader, englishLanguageCode);
            }
        }
        public void UpdateLanguage(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, LanguageEntity entity, String englishLanguageCode, String frenchLanguageCode)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("FormFieldLanguage_update", user.DatabaseName))
                {
                    command.BeginTransaction("FormFieldLanguage_update");

                    command.AddParameterWithValue("@controlFieldValueId", entity.ControlFieldValueId);
                    command.AddParameterWithValue("@ControlFieldId", entity.SecurityRoleId);
                    command.AddParameterWithValue("@codeLanguageCd", entity.CodeLanguageCd);
                    command.AddParameterWithValue("@securityRoleId", entity.SecurityRoleId);
                    command.AddParameterWithValue("@englishFieldValue", entity.EnglishFieldValue);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);
                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", entity.RowVersion, ParameterDirection.InputOutput);

                    command.AddParameterWithValue("@frenchControlFieldValueId", entity.FrenchControlFieldValueId);
                    command.AddParameterWithValue("@frenchFieldValue", entity.FrenchFieldValue);
                    SqlParameter frenchRowVersionParm = command.AddParameterWithValue("@frenchRowVersion", entity.FrenchRowVersion, ParameterDirection.InputOutput);

                    command.AddParameterWithValue("@englishLanguageCode", englishLanguageCode);
                    command.AddParameterWithValue("@frenchLanguageCode", frenchLanguageCode);

                    command.ExecuteNonQuery();

                    entity.RowVersion = (byte[])rowVersionParm.Value;
                    entity.FrenchRowVersion = (byte[])frenchRowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public LanguageEntity InsertLanguage(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, LanguageEntity entity, String englishLanguageCode, String frenchLanguageCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("FormFieldLanguage_insert", user.DatabaseName))
            {
                command.BeginTransaction("FormFieldLanguage_insert");

                SqlParameter controlFieldValueIdParm = command.AddParameterWithValue("@controlFieldValueId", entity.ControlFieldValueId, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@controlFieldId", entity.ControlFieldId);
                command.AddParameterWithValue("@codeLanguageCd", entity.CodeLanguageCd);
                command.AddParameterWithValue("@securityRoleId", entity.SecurityRoleId);
                command.AddParameterWithValue("@englishFieldValue", entity.EnglishFieldValue);
                command.AddParameterWithValue("@attributeIdentifier", entity.AttributeIdentifier);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);
                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", entity.RowVersion, ParameterDirection.InputOutput);

                SqlParameter frenchControlFieldValueIdParm = command.AddParameterWithValue("@frenchControlFieldValueId", entity.FrenchControlFieldValueId, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@frenchFieldValue", entity.FrenchFieldValue);
                SqlParameter frenchRowVersionParm = command.AddParameterWithValue("@frenchRowVersion", entity.FrenchRowVersion, ParameterDirection.InputOutput);

                command.AddParameterWithValue("@englishLanguageCode", englishLanguageCode);
                command.AddParameterWithValue("@frenchLanguageCode", frenchLanguageCode);

                command.ExecuteNonQuery();

                if (entity.ControlFieldValueId < 0) //an English object was being inserted
                {
                    entity.ControlFieldValueId = Convert.ToInt64(controlFieldValueIdParm.Value);
                    entity.RowVersion = (byte[])rowVersionParm.Value;
                }
                if (entity.FrenchControlFieldValueId < 0)//a French object was being inserted
                {
                    entity.FrenchControlFieldValueId = Convert.ToInt64(frenchControlFieldValueIdParm.Value);
                    entity.FrenchRowVersion = (byte[])frenchRowVersionParm.Value;
                }

                command.CommitTransaction();

                return entity;
            }
        }
        #endregion

        #region data mapping
        protected LanguageEntityCollection MapToFieldLanguageCollection(IDataReader reader, String englishLanguageCode)
        {
            LanguageEntityCollection collection = new LanguageEntityCollection();

            while (reader.Read())
            {
                LanguageEntity langField = new LanguageEntity();

                MapToFieldLanguageEntity(reader, langField, englishLanguageCode);

                if (collection.Count == 0)
                    collection.Add(langField);
                else
                {
                    if (collection[collection.Count - 1].ControlFieldId == langField.ControlFieldId)
                        MapToFieldLanguageEntity(reader, collection[collection.Count - 1], englishLanguageCode);
                    else
                        collection.Add(langField);
                }
            }

            return collection;
        }

        protected LanguageEntity MapToFieldLanguageEntity(IDataReader reader, LanguageEntity langField, String englishLanguageCode)
        {
            if ((String)CleanDataValue(reader[ColumnNames.CodeLanguageCd]) == englishLanguageCode)
            {
                base.MapToBase(langField, reader);

                langField.ControlFieldValueId = (long)CleanDataValue(reader[ColumnNames.ControlFieldValueId]);
                langField.ControlFieldId = (long)CleanDataValue(reader[ColumnNames.ControlFieldId]);
                langField.FieldIdentifier = (String)CleanDataValue(reader[ColumnNames.FieldIdentifier]);
                langField.CodeLanguageCd = (String)CleanDataValue(reader[ColumnNames.CodeLanguageCd]);
                langField.SecurityRoleId = (long)CleanDataValue(reader[ColumnNames.SecurityRoleId]);
                langField.AttributeIdentifier = (String)CleanDataValue(reader[ColumnNames.AttributeIdentifier]);
                langField.EnglishFieldValue = (String)CleanDataValue(reader[ColumnNames.FieldValue]);
            }
            else //language is French
            {
                langField.FrenchControlFieldValueId = Convert.ToInt64(CleanDataValue(reader[ColumnNames.ControlFieldValueId]));
                langField.FrenchFieldValue = (String)CleanDataValue(reader[ColumnNames.FieldValue]);
                langField.ControlFieldId = (long)CleanDataValue(reader[ColumnNames.ControlFieldId]);
                langField.FieldIdentifier = (String)CleanDataValue(reader[ColumnNames.FieldIdentifier]);
                langField.SecurityRoleId = (long)CleanDataValue(reader[ColumnNames.SecurityRoleId]);
                langField.AttributeIdentifier = (String)CleanDataValue(reader[ColumnNames.AttributeIdentifier]);

                //base object properties
                if (CleanDataValue(reader[ColumnNames.RowVersion]) == null || ((byte[])CleanDataValue(reader[ColumnNames.RowVersion])).Length < 1)
                    langField.FrenchRowVersion = new byte[8];
                else
                    langField.FrenchRowVersion = (byte[])CleanDataValue(reader[ColumnNames.RowVersion]);
            }

            return langField;
        }
        #endregion
    }
}