﻿using System;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerReport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ReportId = "report_id";
            public static String Name = "name";
            public static String ReportServerUrl = "report_server_url";
            public static String ReportPath = "report_path";
        }
        #endregion

        #region main
        /// <summary>
        /// returns a Report
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        internal ReportCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String reportName)
        {
            using (WLP.DataLayer.SqlClient.DataBaseCommand command = GetStoredProcCommand("Report_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@name", reportName);

                ReportCollection items = null;

                using (System.Data.IDataReader reader = command.ExecuteReader())
                    items = MapToReportCollection(reader);

                return items;
            }
        }
        internal ReportCollection SelectById(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long reportId)
        {
            using (WLP.DataLayer.SqlClient.DataBaseCommand command = GetStoredProcCommand("ReportByReportId_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@reportId", reportId);

                ReportCollection items = null;

                using (System.Data.IDataReader reader = command.ExecuteReader())
                    items = MapToReportCollection(reader);

                return items;
            }
        }
        #endregion

        #region data mapping
        /// <summary>
        /// maps result set to Report collection
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected ReportCollection MapToReportCollection(System.Data.IDataReader reader)
        {
            ReportCollection collection = new ReportCollection();

            while (reader.Read())
                collection.Add(MapToReport(reader));

            return collection;
        }
        /// <summary>
        /// maps columns to Report properties
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected Report MapToReport(System.Data.IDataReader reader)
        {
            Report item = new Report();
            base.MapToBase(item, reader);

            item.ReportId = (long)CleanDataValue(reader[ColumnNames.ReportId]);
            item.Name = (String)CleanDataValue(reader[ColumnNames.Name]);
            item.ReportServerUrl = (String)CleanDataValue(reader[ColumnNames.ReportServerUrl]);
            item.ReportPath = (String)CleanDataValue(reader[ColumnNames.ReportPath]);

            return item;
        }
        #endregion
    }
}