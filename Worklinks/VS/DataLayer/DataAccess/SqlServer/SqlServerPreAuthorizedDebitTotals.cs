﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerPreAuthorizedDebitTotals : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String BusinessTaxNumber = "business_tax_number";
            public static String Amount = "amount";
            public static String CodePreAuthorizedBankCd = "code_pre_authorized_bank_cd";
            public static String PreAuthorizedTransitNumber = "pre_authorized_transit_number";
            public static String PreAuthorizedAccountNumber = "pre_authorized_account_number";
            public static String PaymentDueDate = "payment_due_date";
            public static String ProcessGroupDescription = "process_group_description";
        }
        #endregion

        #region main
        public PreAuthorizedDebitTotalsCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            DataBaseCommand command = GetStoredProcCommand("PreAuthorizedDebitTotals_select", user.DatabaseName);
            command.AddParameterWithValue("@databaseName", user.DatabaseName);
            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);            

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToPreAuthorizedDebitTotalsCollection(reader);
            }
        }
        #endregion

        #region data mapping
        protected PreAuthorizedDebitTotalsCollection MapToPreAuthorizedDebitTotalsCollection(IDataReader reader)
        {
            PreAuthorizedDebitTotalsCollection collection = new PreAuthorizedDebitTotalsCollection();
            while (reader.Read())
            {
                collection.Add(MapToPreAuthorizedDebitTotals(reader));
            }

            return collection;
        }
        protected PreAuthorizedDebitTotals MapToPreAuthorizedDebitTotals(IDataReader reader)
        {
            PreAuthorizedDebitTotals item = new PreAuthorizedDebitTotals();

            item.BusinessTaxNumber = (string)CleanDataValue(reader[ColumnNames.BusinessTaxNumber]);
            item.Amount = (Decimal)CleanDataValue(reader[ColumnNames.Amount]);
            item.CodePreAuthorizedBankCd = (string)CleanDataValue(reader[ColumnNames.CodePreAuthorizedBankCd]);
            item.PreAuthorizedTransitNumber = (string)(CleanDataValue(reader[ColumnNames.PreAuthorizedTransitNumber]));
            item.PreAuthorizedAccountNumber = (string)CleanDataValue(reader[ColumnNames.PreAuthorizedAccountNumber]);
            item.PaymentDueDate = (DateTime)CleanDataValue(reader[ColumnNames.PaymentDueDate]);
            item.ProcessGroupDescription = (string)CleanDataValue(reader[ColumnNames.ProcessGroupDescription]);

            return item;
        }
        #endregion
    }
}
