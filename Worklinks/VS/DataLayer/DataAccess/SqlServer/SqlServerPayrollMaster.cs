﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerPayrollMaster : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayrollMasterId = "payroll_master_id";
            public static String HasCurrentFlag = "has_current_flag";
            public static String PayrollProcessId = "payroll_process_id";
            public static String BusinessNumberId = "business_number_id";
            public static String CodeWsibCd = "code_wsib_cd";
            public static String EmployeeId = "employee_id";
            public static String EmployeePositionId = "employee_position_id";
            public static String CodeProvinceStateCd = "code_province_state_cd";
            public static String PeriodsPerYear = "periods_per_year";
            public static String BirthDate = "birth_date";
            public static String FederalTaxClaim = "federal_tax_claim";
            public static String FederalTaxExemptFlag = "federal_tax_exempt_flag";
            public static String FederalDesignatedAreaDeduction = "federal_designated_area_deduction";
            public static String FederalAdditionalTax = "federal_additional_tax";
            public static String ProvincialAdditionalTax = "provincial_additional_tax";
            public static String FederalAuthorizedAnnualDeduction = "federal_authorized_annual_deduction";
            public static String FederalAuthorizedAnnualTaxCredit = "federal_authorized_annual_tax_credit";
            public static String EstimatedAnnualIncome = "estimated_annual_income";
            public static String EstimatedAnnualExpense = "estimated_annual_expense";
            public static String ProvincialTaxClaim = "provincial_tax_claim";
            public static String ProvincialTaxExemptFlag = "provincial_tax_exempt_flag";
            public static String ProvincialAuthorizedAnnualDeduction = "provincial_authorized_annual_deduction";
            public static String ProvincialAuthorizedAnnualTaxCredit = "provincial_authorized_annual_tax_credit";
            public static String CanadaPensionPlanExemptFlag = "canada_pension_plan_exempt_flag";
            public static String EmploymentInsuranceExemptFlag = "employment_insurance_exempt_flag";
            public static String ParentalInsurancePlanExemptFlag = "parental_insurance_plan_exempt_flag";
            public static String EmployerEmploymentInsuranceRate = "employer_employment_insurance_rate";
            public static String YtdPreviousCanadaPensionPlan = "ytd_previous_canada_pension_plan";
            public static String YtdPreviousCanadaPensionPlanEarnings = "ytd_previous_canada_pension_plan_earnings";
            public static String YtdPreviousEmploymentInsurance = "ytd_previous_employment_insurance";
            public static String YtdPreviousEmployerEmploymentInsurance = "ytd_previous_employer_employment_insurance";
            public static String YtdPreviousQuebecParentalInsurancePlan = "ytd_previous_quebec_parental_insurance_plan";
            public static String YtdPreviousEmployerQuebecParentalInsurancePlan = "ytd_previous_employer_quebec_parental_insurance_plan";
            public static String YtdPreviousBonusTaxableIncome = "ytd_previous_bonus_taxable_income";
            public static String YtdPreviousQuebecBonusTaxableIncome = "ytd_previous_quebec_bonus_taxable_income";
            public static String YtdPreviousLumpsumTaxableIncome = "ytd_previous_lumpsum_taxable_income";
            public static String YtdPreviousQuebecLumpsumTaxableIncome = "ytd_previous_quebec_lumpsum_taxable_income";
            public static String YtdPreviousWorkersCompensationAssessable = "ytd_previous_employer_workers_compensation_assessable";
            public static String YtdPreviousQuebecHealthContribution = "ytd_previous_quebec_health_contribution";
            public static String Period = "period";
            public static String NumberOfPayPeriodsCovered = "number_of_pay_periods_covered";
            public static String ChequeDate = "cheque_date";
            public static String TaxableIncome = "taxable_income";
            public static String QuebecTaxableIncome = "quebec_taxable_income";
            public static String OverrideTaxIncome = "override_tax_income";
            public static String BonusTaxableIncome = "bonus_taxable_income";
            public static String QuebecBonusTaxableIncome = "quebec_bonus_taxable_income";
            public static String LumpsumTaxableIncome = "lumpsum_taxable_income";
            public static String QuebecLumpsumTaxableIncome = "quebec_lumpsum_taxable_income";
            public static String PensionTaxableIncome = "pension_taxable_income";
            public static String QuebecPensionTaxableIncome = "quebec_pension_taxable_income";
            public static String TaxableBenefit = "taxable_benefit";
            public static String QuebecTaxableBenefit = "quebec_taxable_benefit";
            public static String TaxableCashBenefit = "taxable_cash_benefit";
            public static String QuebecTaxableCashBenefit = "quebec_taxable_cash_benefit";
            public static String TaxableNonCashBenefit = "taxable_non_cash_benefit";
            public static String QuebecTaxableNonCashBenefit = "quebec_taxable_non_cash_benefit";
            public static String SeveranceTaxableIncome = "severance_taxable_income";
            public static String QuebecSeveranceTaxableIncome = "quebec_severance_taxable_income";
            public static String CanadaPensionPlanEarnings = "canada_pension_plan_earnings";
            public static String QuebecPensionPlanEarnings = "quebec_pension_plan_earnings";
            public static String QuebecParentalInsurancePlanEarnings = "quebec_parental_insurance_plan_earnings";
            public static String EmploymentInsuranceEarnings = "employment_insurance_earnings";
            public static String CommissionTaxableIncome = "commission_taxable_income";
            public static String QuebecCommissionTaxableIncome = "quebec_commission_taxable_income";
            public static String DaysSinceLastCommissionPayment = "days_since_last_commission_payment";
            public static String BeforeTaxDeduction = "before_tax_deduction";
            public static String BeforeIncomeTaxDeduction = "before_income_tax_deduction";
            public static String BeforeBonusTaxDeduction = "before_bonus_tax_deduction";
            public static String BonusLumpSumTaxableIncome = "bonus_lumpsum_taxable_income";
            public static String WorkersCompensationAssessable = "workers_compensation_assessable";
            public static String WorkersCompensationExcessEarning = "workers_compensation_excess_earning";
            //public static String WorkersCompensationPremium = "workers_compensation_premium";
            public static String EmployerHealthTaxAssessable = "employer_health_tax_assessable";
            public static String MaxQuebecPensionPlanEarnings = "max_quebec_pension_plan_earnings";
            public static String MaxCanadaPensionPlanEarnings = "max_canada_pension_plan_earnings";
            public static String MaxQuebecParentalInsuranceEarnings = "max_quebec_parental_insurance_earnings";
            public static String CompensationAmount = "compensation_amount";
            public static String WorkersCompensationMaximumAssessableEarnings = "workers_compensation_maximum_assessable_earnings";
            public static String FederalPayTaxFlag = "federal_pay_tax_flag";
            public static String ProvincialPayTaxFlag = "provincial_pay_tax_flag";
            public static String PayCanadaPensionPlanFlag = "pay_canada_pension_plan_flag";
            public static String PayEmploymentInsuranceFlag = "pay_employment_insurance_flag";
            public static String ParentalInsurancePlanFlag = "parental_insurance_plan_flag";
            public static String HealthTaxId = "health_tax_id";
            public static String YtdPreviousCommissionPayDate = "ytd_previous_commission_pay_date";
            //for exports
            public static String EmployeeNumber = "employee_number";
            public static String StartDate = "start_date";
            public static String CutoffDate = "cutoff_date";
            public static String ChequeAmount = "cheque_amount";
            public static String EmploymentIncome14 = "employment_income_14";
            public static String CanadaPensionPlan = "canada_pension_plan";
            public static String EmploymentInsurance = "employment_insurance";
            public static String FederalTax = "federal_tax";
            public static String QuebecTax = "quebec_tax";
            public static String QuebecParentalInsurancePlan = "quebec_parental_insurance_plan";
            public static String EmployerWorkersCompensationPremium = "employer_workers_compensation_premium";
            public static String HowPaid = "how_paid";
            public static String CodeCompanyCd = "code_company_cd";
            public static String EmployerHealthTaxLevy = "employer_health_tax_levy";
            public static String EmployerEmploymentInsurance = "employer_employment_insurance";
            public static String EmployerCanadaPensionPlan = "employer_canada_pension_plan";
            public static String EmployerNumber = "employer_number";
            public static String WorkersCompensationEarnings = "workers_compensation_earnings";
        }
        #endregion

        #region select
        public PayrollMasterCollection Select(DatabaseUser user, long payrollProcessId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollMaster_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToPayrollMasterCollection(reader);
            }
        }
        public PayrollMasterCollection LoadPayrollMaster(DatabaseUser user, PayrollProcess payrollProcess)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollMaster_generate", user.DatabaseName))
            {
                command.AddParameterWithValue("@payrollProcessId", payrollProcess.PayrollProcessId);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToParameterReportPayrollMasterCollection(reader, payrollProcess);
            }
        }
        #endregion

        #region insert
        public void InsertPayrollMasterAndTax(DatabaseUser user, PayrollMasterCollection payrollMaster, System.Collections.Generic.List<PayrollMasterTax> payrollOutput, string payrollProcessRunTypeCode)
        {
            DataBaseCommand command = GetStoredProcCommand("PayrollMaster_insert", user.DatabaseName);

            using (command)
            {
                command.BeginTransaction("PayrollMaster_insert");

                //first table parameter
                SqlParameter parmPayrollMaster = new SqlParameter();
                parmPayrollMaster.ParameterName = "@parmPayrollMaster";
                parmPayrollMaster.SqlDbType = SqlDbType.Structured;

                DataTable tablePayrollMaster = LoadPayrollMasterDataTable(payrollMaster);
                parmPayrollMaster.Value = tablePayrollMaster;
                command.AddParameter(parmPayrollMaster);

                //first table parameter
                SqlParameter parmPayrollMasterDetail = new SqlParameter();
                parmPayrollMasterDetail.ParameterName = "@parmPayrollMasterDetail";
                parmPayrollMasterDetail.SqlDbType = SqlDbType.Structured;

                DataTable tablePayrollMasterDetail = LoadPayrollMasterDetailDataTable(payrollMaster.PayrollMasterDetails);
                parmPayrollMasterDetail.Value = tablePayrollMasterDetail;
                command.AddParameter(parmPayrollMasterDetail);

                //third table parameter
                SqlParameter parmPayrollMasterTax = new SqlParameter();
                parmPayrollMasterTax.ParameterName = "@parmPayrollMasterTax";
                parmPayrollMasterTax.SqlDbType = SqlDbType.Structured;

                DataTable tablePayrollMasterTax = LoadPayrollMasterTaxDataTable(payrollOutput, user);
                parmPayrollMasterTax.Value = tablePayrollMasterTax;
                command.AddParameter(parmPayrollMasterTax);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);
                command.AddParameterWithValue("@payrollProcessRunTypeCode", payrollProcessRunTypeCode);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        private DataTable LoadPayrollMasterDataTable(PayrollMasterCollection collection)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("payroll_master_id", typeof(long));
            table.Columns.Add("has_current_flag", typeof(bool));
            table.Columns.Add("payroll_process_id", typeof(long));
            table.Columns.Add("employee_id", typeof(long));
            table.Columns.Add("employee_position_id", typeof(long));
            table.Columns.Add("birth_date", typeof(DateTime));
            table.Columns.Add("business_number_id", typeof(long));
            table.Columns.Add("code_wsib_cd", typeof(String));
            table.Columns.Add("period_per_year", typeof(int));
            table.Columns.Add("taxable_code_province_state_cd", typeof(String));
            table.Columns.Add("federal_tax_claim", typeof(Decimal));
            table.Columns.Add("federal_pay_tax_flag", typeof(bool));
            table.Columns.Add("federal_designated_area_deduction", typeof(Decimal));
            table.Columns.Add("federal_additional_tax", typeof(Decimal));
            table.Columns.Add("federal_authorized_annual_deduction", typeof(Decimal));
            table.Columns.Add("federal_authorized_annual_tax_credit", typeof(Decimal));
            table.Columns.Add("estimated_annual_income", typeof(Decimal));
            table.Columns.Add("estimated_annual_expense", typeof(Decimal));
            table.Columns.Add("provincial_tax_claim", typeof(Decimal));
            table.Columns.Add("provincial_pay_tax_flag", typeof(bool));
            table.Columns.Add("provincial_authorized_annual_deduction", typeof(Decimal));
            table.Columns.Add("provincial_authorized_annual_tax_credit", typeof(Decimal));
            table.Columns.Add("provincial_additional_tax", typeof(Decimal));
            table.Columns.Add("pay_canada_pension_plan_flag", typeof(bool));
            table.Columns.Add("pay_employment_insurance_flag", typeof(bool));
            table.Columns.Add("parental_insurance_plan_flag", typeof(bool));
            table.Columns.Add("health_tax_id", typeof(long));
            table.Columns.Add("employer_employment_insurance_rate", typeof(Decimal));
            table.Columns.Add("ytd_previous_canada_pension_plan", typeof(Decimal));
            table.Columns.Add("ytd_previous_canada_pension_plan_earnings", typeof(Decimal));
            table.Columns.Add("ytd_previous_employment_insurance", typeof(Decimal));
            table.Columns.Add("ytd_previous_employer_employment_insurance", typeof(Decimal));
            table.Columns.Add("ytd_previous_quebec_parental_insurance_plan", typeof(Decimal));
            table.Columns.Add("ytd_previous_employer_quebec_parental_insurance_plan", typeof(Decimal));
            table.Columns.Add("ytd_previous_bonus_taxable_income", typeof(Decimal));
            table.Columns.Add("ytd_previous_quebec_bonus_taxable_income", typeof(Decimal));
            table.Columns.Add("ytd_previous_lumpsum_taxable_income", typeof(Decimal));
            table.Columns.Add("ytd_previous_quebec_lumpsum_taxable_income", typeof(Decimal));
            table.Columns.Add("ytd_previous_quebec_health_contribution", typeof(Decimal));
            table.Columns.Add("ytd_previous_employer_workers_compensation_assessable", typeof(Decimal));
            table.Columns.Add("period", typeof(int));
            table.Columns.Add("number_of_pay_periods_covered", typeof(int));
            table.Columns.Add("compensation_amount", typeof(Decimal));
            table.Columns.Add("ytd_previous_commission_pay_date", typeof(DateTime));
            table.Columns.Add("max_canada_pension_plan_earnings", typeof(Decimal));
            table.Columns.Add("max_quebec_pension_plan_earnings", typeof(Decimal));
            table.Columns.Add("max_quebec_parental_insurance_earnings", typeof(Decimal));
            table.Columns.Add("workers_compensation_maximum_assessable_earnings", typeof(Decimal));
            table.Columns.Add("canada_pension_plan_earnings_assessable", typeof(Decimal));
            table.Columns.Add("quebec_pension_plan_earnings_assessable", typeof(Decimal));
            table.Columns.Add("quebec_parental_insurance_plan_earnings_assessable", typeof(Decimal));
            table.Columns.Add("employment_insurance_earnings_assessable", typeof(Decimal));
            table.Columns.Add("workers_compensation_earnings_assessable", typeof(Decimal));
            table.Columns.Add("workers_compensation_excess_earnings", typeof(Decimal));
            table.Columns.Add("employer_health_tax_earnings_assessable", typeof(Decimal));



            //loop thru and add values
            for (int i = 0; i < collection.Count; i++)
            {
                table.Rows.Add(new Object[]
                {
                    collection[i].PayrollMasterId,
                    collection[i].HasCurrentFlag,
                    collection[i].PayrollProcessId,
                    collection[i].EmployeeId,
                    collection[i].EmployeePositionId,
                    collection[i].BirthDate,
                    collection[i].BusinessNumberId,
                    collection[i].CodeWsibCd,
                    collection[i].PeriodsPerYear,
                    collection[i].CodeProvinceStateCd,
                    collection[i].FederalTaxClaim,
                    collection[i].FederalPayTaxFlag,
                    collection[i].FederalDesignatedAreaDeduction,
                    collection[i].FederalAdditionalTax,
                    collection[i].FederalAuthorizedAnnualDeduction,
                    collection[i].FederalAuthorizedAnnualTaxCredit,
                    collection[i].EstimatedAnnualIncome,
                    collection[i].EstimatedAnnualExpense,
                    collection[i].ProvincialTaxClaim,
                    collection[i].ProvincialPayTaxFlag,
                    collection[i].ProvincialAuthorizedAnnualDeduction,
                    collection[i].ProvincialAuthorizedAnnualTaxCredit,
                    collection[i].ProvincialAdditionalTax,
                    collection[i].PayCanadaPensionPlanFlag,
                    collection[i].PayEmploymentInsuranceFlag,
                    collection[i].ParentalInsurancePlanFlag,
                    collection[i].HealthTaxId,
                    collection[i].EmployerEmploymentInsuranceRate,
                    collection[i].YtdPreviousCanadaPensionPlan,
                    collection[i].YtdPreviousCanadaPensionPlanEarnings,
                    collection[i].YtdPreviousEmploymentInsurance,
                    collection[i].YtdPreviousEmployerEmploymentInsurance,
                    collection[i].YtdPreviousQuebecParentalInsurancePlan,
                    collection[i].YtdPreviousEmployerQuebecParentalInsurancePlan,
                    collection[i].YtdPreviousBonusTaxableIncome,
                    collection[i].YtdPreviousQuebecBonusTaxableIncome,
                    collection[i].YtdPreviousLumpsumTaxableIncome,
                    collection[i].YtdPreviousQuebecLumpsumTaxableIncome,
                    collection[i].YtdPreviousQuebecHealthContribution,
                    collection[i].YtdPreviousWorkersCompensationAssessable,
                    collection[i].Period,
                    collection[i].NumberOfPayPeriodsCovered,
                    collection[i].CompensationAmount,
                    collection[i].YtdPreviousCommissionPayDate,
                    collection[i].MaxCanadaPensionPlanEarnings,
                    collection[i].MaxQuebecPensionPlanEarnings,
                    collection[i].MaxQuebecParentalInsuranceEarnings,
                    collection[i].WorkersCompensationMaximumAssessableEarnings,
                    collection[i].CanadaPensionPlanEarningsAssessable,
                    collection[i].QuebecPensionPlanEarningsAssessable,
                    collection[i].QuebecParentalInsurancePlanEarningsAssessable,
                    collection[i].EmploymentInsuranceEarningsAssessable,
                    collection[i].WorkersCompensationEarningsAssessable,
                    collection[i].WorkersCompensationExcessEarnings,
                    collection[i].EmployerHealthTaxEarningsAssessable,

            });
            }

            return table;
        }

        private DataTable LoadPayrollMasterDetailDataTable(PayrollMasterDetailCollection collection)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("payroll_master_id", typeof(long));
            table.Columns.Add("organization_unit", typeof(String));
            table.Columns.Add("taxable_income", typeof(Decimal));
            table.Columns.Add("override_tax_income", typeof(Decimal));
            table.Columns.Add("quebec_taxable_income", typeof(Decimal));
            table.Columns.Add("bonus_taxable_income", typeof(Decimal));
            table.Columns.Add("quebec_bonus_taxable_income", typeof(Decimal));
            table.Columns.Add("lumpsum_taxable_income", typeof(Decimal));
            table.Columns.Add("quebec_lumpsum_taxable_income", typeof(Decimal));
            table.Columns.Add("pension_taxable_income", typeof(Decimal));
            table.Columns.Add("quebec_pension_taxable_income", typeof(Decimal));
            table.Columns.Add("taxable_benefit", typeof(Decimal));
            table.Columns.Add("quebec_taxable_benefit", typeof(Decimal));
            table.Columns.Add("taxable_cash_benefit", typeof(Decimal));
            table.Columns.Add("quebec_taxable_cash_benefit", typeof(Decimal));
            table.Columns.Add("taxable_non_cash_benefit", typeof(Decimal));
            table.Columns.Add("quebec_taxable_non_cash_benefit", typeof(Decimal));
            table.Columns.Add("severance_taxable_income", typeof(Decimal));
            table.Columns.Add("quebec_severance_taxable_income", typeof(Decimal));
            table.Columns.Add("canada_pension_plan_earnings", typeof(Decimal));
            table.Columns.Add("quebec_pension_plan_earnings", typeof(Decimal));
            table.Columns.Add("quebec_parental_insurance_plan_earnings", typeof(Decimal));
            table.Columns.Add("employment_insurance_earnings", typeof(Decimal));
            table.Columns.Add("commission_taxable_income", typeof(Decimal));
            table.Columns.Add("quebec_commission_taxable_income", typeof(Decimal));
            table.Columns.Add("before_tax_deduction", typeof(Decimal));
            table.Columns.Add("before_income_tax_deduction", typeof(Decimal));
            table.Columns.Add("before_bonus_tax_deduction", typeof(Decimal));
            table.Columns.Add("workers_compensation_earnings", typeof(Decimal));
            table.Columns.Add("employer_health_tax_earnings", typeof(Decimal));


            //loop thru and add values
            for (int i = 0; i < collection.Count; i++)
            {
                table.Rows.Add(new Object[]
                {
                    collection[i].PayrollMasterId,
                    collection[i].OrganizationUnit,
                    collection[i].TaxableIncome,
                    collection[i].OverrideTaxIncome,
                    collection[i].QuebecTaxableIncome,
                    collection[i].BonusTaxableIncome,
                    collection[i].QuebecBonusTaxableIncome,
                    collection[i].LumpsumTaxableIncome,
                    collection[i].QuebecLumpsumTaxableIncome,
                    collection[i].PensionTaxableIncome,
                    collection[i].QuebecPensionTaxableIncome,
                    collection[i].TaxableBenefit,
                    collection[i].QuebecTaxableBenefit,
                    collection[i].TaxableCashBenefit,
                    collection[i].QuebecTaxableCashBenefit,
                    collection[i].TaxableNonCashBenefit,
                    collection[i].QuebecTaxableNonCashBenefit,
                    collection[i].SeveranceTaxableIncome,
                    collection[i].QuebecSeveranceTaxableIncome,
                    collection[i].CanadaPensionPlanEarnings,
                    collection[i].QuebecPensionPlanEarnings,
                    collection[i].QuebecParentalInsurancePlanEarnings,
                    collection[i].EmploymentInsuranceEarnings,
                    collection[i].CommissionTaxableIncome,
                    collection[i].QuebecCommissionTaxableIncome,
                    collection[i].BonusIncomeBeforeTaxDeduction,
                    collection[i].BeforeIncomeTaxDeduction,
                    collection[i].BeforeBonusTaxDeduction,
                    collection[i].WorkersCompensationEarnings,
                    collection[i].EmployerHealthTaxEarnings,
            });
            }

            return table;
        }

        private DataTable LoadPayrollMasterTaxDataTable(System.Collections.Generic.List<PayrollMasterTax> list, DatabaseUser user)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("PayrollMasterTaxId", typeof(long));
            table.Columns.Add("PayrollMasterId", typeof(long));
            table.Columns.Add("QuebecCalculationFlag", typeof(bool));
            table.Columns.Add("CalculationType", typeof(int));
            table.Columns.Add("FederalTax", typeof(Decimal));
            table.Columns.Add("ProvincialTax", typeof(Decimal));
            table.Columns.Add("QuebecProvincialTax", typeof(Decimal));
            table.Columns.Add("CombinedTaxes", typeof(Decimal));
            table.Columns.Add("QuebecCombinedTaxes", typeof(Decimal));
            table.Columns.Add("EmployeeCpp", typeof(Decimal));
            table.Columns.Add("EmployerCpp", typeof(Decimal));
            table.Columns.Add("CppEarnings", typeof(Decimal));
            table.Columns.Add("EmployeeQpp", typeof(Decimal));
            table.Columns.Add("EmployerQpp", typeof(Decimal));
            table.Columns.Add("QppEarnings", typeof(Decimal));
            table.Columns.Add("EmployeeEi", typeof(Decimal));
            table.Columns.Add("EmployerEi", typeof(Decimal));
            table.Columns.Add("EiEarnings", typeof(Decimal));
            table.Columns.Add("EmployeeEiReturnable", typeof(Decimal));
            table.Columns.Add("EmployeeQpip", typeof(Decimal));
            table.Columns.Add("EmployerQpip", typeof(Decimal));
            table.Columns.Add("QpipEarnings", typeof(Decimal));
            table.Columns.Add("QuebecHeathContribution", typeof(Decimal));
            table.Columns.Add("LumpSumTax", typeof(Decimal));
            table.Columns.Add("PayrollProcessId", typeof(long));
            table.Columns.Add("EmployeeId", typeof(long));

            //loop thru and add values
            for (int i = 0; i < list.Count; i++)
            {
                table.Rows.Add(new Object[]
                {
                    list[i].PayrollMasterTaxId,
                    list[i].PayrollMasterId,
                    list[i].QuebecCalculationFlag,
                    list[i].CalculationType,
                    list[i].FederalTax,
                    list[i].ProvincialTax,
                    list[i].QuebecProvincialTax,
                    list[i].CombinedTaxes,
                    list[i].QuebecCombinedTaxes,
                    list[i].EmployeeCpp,
                    list[i].EmployerCpp,
                    list[i].CppEarnings,
                    list[i].EmployeeQpp,
                    list[i].EmployerQpp,
                    list[i].QppEarnings,
                    list[i].EmployeeEi,
                    list[i].EmployerEi,
                    list[i].EiEarnings,
                    list[i].EmployeeEiReturnable,
                    list[i].EmployeeQpip,
                    list[i].EmployerQpip,
                    list[i].QpipEarnings,
                    list[i].QuebecHeathContribution,
                    list[i].LumpSumTax,
                    list[i].PayrollProcessId,
                    list[i].EmployeeId
                });
            }

            return table;
        }
        #endregion

        #region delete
        public void DeletePayrollMasterAndDetail(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollMaster_delete", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollMaster_delete");

                    command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected PayrollMasterCollection MapToPayrollMasterCollection(IDataReader reader)
        {
            PayrollMasterCollection collection = new PayrollMasterCollection();

            while (reader.Read())
                collection.Add(MapToPayrollMaster(reader));

            return collection;
        }
        protected PayrollMaster MapToPayrollMaster(IDataReader reader)
        {
            PayrollMaster item = new PayrollMaster();

            item.PayrollMasterId = (long)CleanDataValue(reader[ColumnNames.PayrollMasterId]);
            item.EmployeeNumber = (String)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.CodeProvinceStateCd = (String)CleanDataValue(reader[ColumnNames.CodeProvinceStateCd]);
            item.CodeWsibCd = (String)CleanDataValue(reader[ColumnNames.CodeWsibCd]);
            //item.WorkersCompensationAssessable = (Decimal)CleanDataValue(reader[ColumnNames.WorkersCompensationAssessable]);
            item.EmployerWorkersCompensationPremium = (Decimal)CleanDataValue(reader[ColumnNames.EmployerWorkersCompensationPremium]);
            //item.EmployerHealthTaxAssessable = ((Decimal)CleanDataValue(reader[ColumnNames.EmployerHealthTaxAssessable]));
            item.EmployerHealthTaxLevy = ((Decimal)CleanDataValue(reader[ColumnNames.EmployerHealthTaxLevy]));
            item.EmployerEmploymentInsurance = ((Decimal)CleanDataValue(reader[ColumnNames.EmployerEmploymentInsurance]));
            item.EmployerCanadaPensionPlan = ((Decimal)CleanDataValue(reader[ColumnNames.EmployerCanadaPensionPlan]));
            item.EmployerNumber = ((String)CleanDataValue(reader[ColumnNames.EmployerNumber]));
            item.EmploymentIncome14 = (Decimal)CleanDataValue(reader[ColumnNames.EmploymentIncome14]);
            item.FederalTax = (Decimal)CleanDataValue(reader[ColumnNames.FederalTax]);
            item.CanadaPensionPlan = (Decimal)CleanDataValue(reader[ColumnNames.CanadaPensionPlan]);
            item.EmploymentInsurance = (Decimal)CleanDataValue(reader[ColumnNames.EmploymentInsurance]);
            //item.OverrideTaxIncome = (Decimal)CleanDataValue(reader[ColumnNames.OverrideTaxIncome]);
            item.QuebecTax = (Decimal)CleanDataValue(reader[ColumnNames.QuebecTax]);
            item.ChequeAmount = (Decimal)CleanDataValue(reader[ColumnNames.ChequeAmount]);
            item.PayrollProcessId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessId]);
            item.StartDate = (DateTime)CleanDataValue(reader[ColumnNames.StartDate]);
            item.CutoffDate = (DateTime)CleanDataValue(reader[ColumnNames.CutoffDate]);
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.HowPaid = (Int16)CleanDataValue(reader[ColumnNames.HowPaid]);
            item.QuebecParentalInsurancePlan = (Decimal)CleanDataValue(reader[ColumnNames.QuebecParentalInsurancePlan]);
            item.CodeCompanyCd = (String)CleanDataValue(reader[ColumnNames.CodeCompanyCd]);

            item.MaxQuebecPensionPlanEarnings = (Decimal)CleanDataValue(reader[ColumnNames.MaxQuebecPensionPlanEarnings]);
            item.MaxCanadaPensionPlanEarnings = (Decimal)CleanDataValue(reader[ColumnNames.MaxCanadaPensionPlanEarnings]);
            item.MaxQuebecParentalInsuranceEarnings = (Decimal)CleanDataValue(reader[ColumnNames.MaxQuebecParentalInsuranceEarnings]);
            item.CompensationAmount = (Decimal)CleanDataValue(reader[ColumnNames.CompensationAmount]);
            item.WorkersCompensationEarnings = (Decimal)CleanDataValue(reader[ColumnNames.WorkersCompensationEarnings]);

            return item;
        }
        protected PayrollMasterCollection MapToParameterReportPayrollMasterCollection(IDataReader reader, PayrollProcess process)
        {
            PayrollMasterCollection collection = new PayrollMasterCollection();

            while (reader.Read())
                collection.Add(MapToParameterReportPayrollMaster(reader, process));

            return collection;
        }
        protected PayrollMaster MapToParameterReportPayrollMaster(IDataReader reader, PayrollProcess process)
        {
            PayrollMaster item = new PayrollMaster(process);

            item.PayrollMasterId = (long)CleanDataValue(reader[ColumnNames.PayrollMasterId]);
            item.HasCurrentFlag = (bool)CleanDataValue(reader[ColumnNames.HasCurrentFlag]);
            item.PayrollProcessId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessId]);
            item.BusinessNumberId = (long)CleanDataValue(reader[ColumnNames.BusinessNumberId]);
            item.CodeWsibCd = (String)CleanDataValue(reader[ColumnNames.CodeWsibCd]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EmployeePositionId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionId]);
            item.CodeProvinceStateCd = (String)CleanDataValue(reader[ColumnNames.CodeProvinceStateCd]);
            item.PeriodsPerYear = (Int16)CleanDataValue(reader[ColumnNames.PeriodsPerYear]);
            item.BirthDate = (DateTime?)CleanDataValue(reader[ColumnNames.BirthDate]);
            item.FederalTaxClaim = (Decimal?)(CleanDataValue(reader[ColumnNames.FederalTaxClaim]));
            item.FederalDesignatedAreaDeduction = (Decimal?)(CleanDataValue(reader[ColumnNames.FederalDesignatedAreaDeduction]));
            item.FederalAdditionalTax = (Decimal?)CleanDataValue(reader[ColumnNames.FederalAdditionalTax]);
            item.ProvincialAdditionalTax = (Decimal?)CleanDataValue(reader[ColumnNames.ProvincialAdditionalTax]);
            item.FederalAuthorizedAnnualDeduction = (Decimal?)CleanDataValue(reader[ColumnNames.FederalAuthorizedAnnualDeduction]);
            item.FederalAuthorizedAnnualTaxCredit = (Decimal?)(CleanDataValue(reader[ColumnNames.FederalAuthorizedAnnualTaxCredit]));
            item.EstimatedAnnualIncome = (Decimal?)(CleanDataValue(reader[ColumnNames.EstimatedAnnualIncome]));
            item.EstimatedAnnualExpense = (Decimal?)(CleanDataValue(reader[ColumnNames.EstimatedAnnualExpense]));
            item.ProvincialTaxClaim = (Decimal?)(CleanDataValue(reader[ColumnNames.ProvincialTaxClaim]));
            item.ProvincialAuthorizedAnnualDeduction = (Decimal?)(CleanDataValue(reader[ColumnNames.ProvincialAuthorizedAnnualDeduction]));
            item.ProvincialAuthorizedAnnualTaxCredit = (Decimal?)(CleanDataValue(reader[ColumnNames.ProvincialAuthorizedAnnualTaxCredit]));
            item.EmployerEmploymentInsuranceRate = (Decimal?)CleanDataValue(reader[ColumnNames.EmployerEmploymentInsuranceRate]);
            item.YtdPreviousCanadaPensionPlan = (Decimal?)CleanDataValue(reader[ColumnNames.YtdPreviousCanadaPensionPlan]);
            item.YtdPreviousCanadaPensionPlanEarnings = (Decimal?)CleanDataValue(reader[ColumnNames.YtdPreviousCanadaPensionPlanEarnings]);
            item.YtdPreviousEmploymentInsurance = (Decimal?)CleanDataValue(reader[ColumnNames.YtdPreviousEmploymentInsurance]);
            item.YtdPreviousEmployerEmploymentInsurance = (Decimal?)CleanDataValue(reader[ColumnNames.YtdPreviousEmployerEmploymentInsurance]);
            item.YtdPreviousQuebecParentalInsurancePlan = (Decimal?)CleanDataValue(reader[ColumnNames.YtdPreviousQuebecParentalInsurancePlan]);
            item.YtdPreviousEmployerQuebecParentalInsurancePlan = (Decimal?)CleanDataValue(reader[ColumnNames.YtdPreviousEmployerQuebecParentalInsurancePlan]);
            item.YtdPreviousBonusTaxableIncome = (Decimal?)CleanDataValue(reader[ColumnNames.YtdPreviousBonusTaxableIncome]);
            item.YtdPreviousQuebecBonusTaxableIncome = (Decimal?)CleanDataValue(reader[ColumnNames.YtdPreviousQuebecBonusTaxableIncome]);
            item.YtdPreviousLumpsumTaxableIncome = (Decimal?)CleanDataValue(reader[ColumnNames.YtdPreviousLumpsumTaxableIncome]);
            item.YtdPreviousQuebecLumpsumTaxableIncome = (Decimal?)CleanDataValue(reader[ColumnNames.YtdPreviousQuebecLumpsumTaxableIncome]);
            item.YtdPreviousWorkersCompensationAssessable = (Decimal?)CleanDataValue(reader[ColumnNames.YtdPreviousWorkersCompensationAssessable]);
            item.YtdPreviousQuebecHealthContribution = (Decimal?)CleanDataValue(reader[ColumnNames.YtdPreviousQuebecHealthContribution]);
            item.Period = Convert.ToInt16(CleanDataValue(reader[ColumnNames.Period]));
            item.NumberOfPayPeriodsCovered = Convert.ToInt16(CleanDataValue(reader[ColumnNames.NumberOfPayPeriodsCovered]));

            item.FederalPayTaxFlag = (bool)CleanDataValue(reader[ColumnNames.FederalPayTaxFlag]);
            item.ProvincialPayTaxFlag = (bool)CleanDataValue(reader[ColumnNames.ProvincialPayTaxFlag]);
            item.PayCanadaPensionPlanFlag = (bool)CleanDataValue(reader[ColumnNames.PayCanadaPensionPlanFlag]);
            item.PayEmploymentInsuranceFlag = (bool)CleanDataValue(reader[ColumnNames.PayEmploymentInsuranceFlag]);
            item.ParentalInsurancePlanFlag = (bool)CleanDataValue(reader[ColumnNames.ParentalInsurancePlanFlag]);
            item.HealthTaxId = (long?)CleanDataValue(reader[ColumnNames.HealthTaxId]);
            item.MaxQuebecPensionPlanEarnings = (Decimal)CleanDataValue(reader[ColumnNames.MaxQuebecPensionPlanEarnings]);
            item.MaxCanadaPensionPlanEarnings = (Decimal)CleanDataValue(reader[ColumnNames.MaxCanadaPensionPlanEarnings]);
            item.MaxQuebecParentalInsuranceEarnings = (Decimal)CleanDataValue(reader[ColumnNames.MaxQuebecParentalInsuranceEarnings]);
            item.YtdPreviousCommissionPayDate = (DateTime?)CleanDataValue(reader[ColumnNames.YtdPreviousCommissionPayDate]);
            item.CompensationAmount = (Decimal)CleanDataValue(reader[ColumnNames.CompensationAmount]);
            item.WorkersCompensationMaximumAssessableEarnings=(Decimal?)CleanDataValue(reader[ColumnNames.WorkersCompensationMaximumAssessableEarnings]);
            item.ChequeDate=(DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);

            return item;
        }
        #endregion
    }
}