﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerPensionExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeId = "employee_id";
            public static String EmployeeNumber = "employee_number";
            public static String SocialInsuranceNumber = "social_insurance_number";
            public static String MemberLastName = "member_last_name";
            public static String MemberFirstName = "member_first_name";
            public static String AddressLine1 = "address_line_1";
            public static String AddressLine2 = "address_line_2";
            public static String AddressLine3 = "address_line_3";
            public static String AddressLine4 = "address_line_4";
            public static String MemberPostalCode = "member_postal_code";
            public static String MemberResidenceCode = "member_residence_code";
            public static String UserField1 = "user_field_1";
            public static String UserField2 = "user_field_2";
            public static String UserField5 = "user_field_5";
            public static String UserField9 = "user_field_9";
            public static String EmployersRequired = "employers_required";
            public static String MemberVoluntaryMatched = "member_voluntary_matched";
            public static String EmployerVoluntaryMatched = "employer_voluntary_matched";
            public static String MemberVoluntaryUnmatched1 = "member_voluntary_unmatched_1";
            public static String MemberVoluntaryUnmatched2 = "member_voluntary_unmatched_2";
            public static String MemberVoluntarySpousalUnmatched = "member_voluntary_spousal_unmatched";
            public static String MemberVoluntaryUnmatched3 = "member_voluntary_unmatched_3";
            public static String MemberProvinceOfEmployment1 = "member_province_of_employment_1";
            public static String MemberDateOfBirth1 = "member_date_of_birth_1";
            public static String MemberDateOfHire = "member_date_of_hire";
            public static String LanguageCode = "language_code";
            public static String CurrentYearCompensation = "current_year_compensation";
            public static String CurrentYearSalaryWages = "current_year_salary_wages";
            public static String EmployeeStatusCode = "employee_status_code";
            public static String EmployeeStatusEffectiveDate = "employee_status_effective_date";
            public static String MemberGender = "member_gender";
            public static String PayStartDate = "pay_start_date";
            public static String PayEndDate = "pay_end_date";
            public static String MemberProvinceOfEmployment2 = "member_province_of_employment_2";
            public static String DateOfHire = "date_of_hire";
            public static String MemberLanguageCode = "member_language_code";
            public static String MemberDateOfBirth2 = "member_date_of_birth_2";
            public static String EmailFlag = "email_flag";
            public static String EmailAddress = "email_address";
            public static String PhoneFlag = "phone_flag";
        }
        #endregion

        #region main

        public SunLifePensionExportCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            DataBaseCommand command = GetStoredProcCommand("PensionExport_select", user.DatabaseName);
            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToSunLifePensionExportCollection(reader);
            }
        }
        #endregion

        #region data mapping
        protected SunLifePensionExportCollection MapToSunLifePensionExportCollection(IDataReader reader)
        {
            SunLifePensionExportCollection collection = new SunLifePensionExportCollection();
            while (reader.Read())
            {
                collection.Add(MapToSunLifePensionExport(reader));
            }

            return collection;
        }
        protected SunLifePensionExport MapToSunLifePensionExport(IDataReader reader)
        {
            SunLifePensionExport item = new SunLifePensionExport();

            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EmployeeNumber = (String)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.SocialInsuranceNumber = (String)CleanDataValue(reader[ColumnNames.SocialInsuranceNumber]);
            item.MemberLastName = (String)CleanDataValue(reader[ColumnNames.MemberLastName]);
            item.MemberFirstName = (String)CleanDataValue(reader[ColumnNames.MemberFirstName]);
            item.AddressLine1 = (String)CleanDataValue(reader[ColumnNames.AddressLine1]);
            item.AddressLine2 = (String)CleanDataValue(reader[ColumnNames.AddressLine2]);
            item.AddressLine3 = (String)CleanDataValue(reader[ColumnNames.AddressLine3]);
            item.AddressLine4 = (String)CleanDataValue(reader[ColumnNames.AddressLine4]);
            item.MemberPostalCode = (String)CleanDataValue(reader[ColumnNames.MemberPostalCode]);
            item.MemberResidenceCode = (String)CleanDataValue(reader[ColumnNames.MemberResidenceCode]);
            item.UserField1 = (String)CleanDataValue(reader[ColumnNames.UserField1]);
            item.UserField2 = (String)CleanDataValue(reader[ColumnNames.UserField2]);
            item.UserField5 = (String)CleanDataValue(reader[ColumnNames.UserField5]);
            item.UserField9 = (String)CleanDataValue(reader[ColumnNames.UserField9]);
            item.EmployersRequired = (String)CleanDataValue(reader[ColumnNames.EmployersRequired]);
            item.MemberVoluntaryMatched = (String)CleanDataValue(reader[ColumnNames.MemberVoluntaryMatched]);
            item.EmployerVoluntaryMatched = (String)CleanDataValue(reader[ColumnNames.EmployerVoluntaryMatched]);
            item.MemberVoluntaryUnmatched1 = (String)CleanDataValue(reader[ColumnNames.MemberVoluntaryUnmatched1]);
            item.MemberVoluntaryUnmatched2 = (String)CleanDataValue(reader[ColumnNames.MemberVoluntaryUnmatched2]);
            item.MemberVoluntarySpousalUnmatched = (String)CleanDataValue(reader[ColumnNames.MemberVoluntarySpousalUnmatched]);
            item.MemberVoluntaryUnmatched3 = (String)CleanDataValue(reader[ColumnNames.MemberVoluntaryUnmatched3]);
            item.MemberProvinceOfEmployment1 = (String)CleanDataValue(reader[ColumnNames.MemberProvinceOfEmployment1]);
            item.MemberDateOfBirth1 = (String)CleanDataValue(reader[ColumnNames.MemberDateOfBirth1]);
            item.MemberDateOfHire = (String)CleanDataValue(reader[ColumnNames.MemberDateOfHire]);
            item.LanguageCode = (String)CleanDataValue(reader[ColumnNames.LanguageCode]);
            item.CurrentYearCompensation = (String)CleanDataValue(reader[ColumnNames.CurrentYearCompensation]);
            item.CurrentYearSalaryWages = (String)CleanDataValue(reader[ColumnNames.CurrentYearSalaryWages]);
            item.EmployeeStatusCode = (String)CleanDataValue(reader[ColumnNames.EmployeeStatusCode]);
            item.EmployeeStatusEffectiveDate = (String)CleanDataValue(reader[ColumnNames.EmployeeStatusEffectiveDate]);
            item.MemberGender = (String)CleanDataValue(reader[ColumnNames.MemberGender]);
            item.PayStartDate = (String)CleanDataValue(reader[ColumnNames.PayStartDate]);
            item.PayEndDate = (String)CleanDataValue(reader[ColumnNames.PayEndDate]);
            item.MemberProvinceOfEmployment2 = (String)CleanDataValue(reader[ColumnNames.MemberProvinceOfEmployment2]);
            item.DateOfHire = (String)CleanDataValue(reader[ColumnNames.DateOfHire]);
            item.MemberLanguageCode = (String)CleanDataValue(reader[ColumnNames.MemberLanguageCode]);
            item.MemberDateOfBirth2 = (String)CleanDataValue(reader[ColumnNames.MemberDateOfBirth2]);
            item.EmailFlag = (String)CleanDataValue(reader[ColumnNames.EmailFlag]);
            item.EmailAddress = (String)CleanDataValue(reader[ColumnNames.EmailAddress]);
            item.PhoneFlag = (String)CleanDataValue(reader[ColumnNames.PhoneFlag]);

            return item;
        }
        #endregion
    }
}
