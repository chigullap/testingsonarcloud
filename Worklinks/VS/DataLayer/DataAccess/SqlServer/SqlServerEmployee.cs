﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployee : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string EmployeeId = "employee_id";
            public static string PersonId = "person_id";
            public static string EmployeeNumber = "employee_number";
            public static string TitleCode = "code_title_cd";
            public static string GenderCode = "code_gender_cd";
            public static string LastName = "last_name";
            public static string MiddleName = "middle_name";
            public static string FirstName = "first_name";
            public static string SocialInsuranceNumber = "social_insurance_number";
            public static string BirthDate = "birth_date";
            public static string KnownAsName = "known_as_name";
            public static string MaritalStatusCode = "code_marital_status_cd";
            public static string MaritalStatusEffectiveDate = "marital_status_effective_date";
            public static string LanguageCode = "code_language_cd";
            public static string BilingualismCode = "code_bilingualism_cd";
            public static string HealthCareNumber = "health_care_number";
            public static string HealthCareProvinceStateCode = "health_care_code_province_state_cd";
            public static string CitizenshipCode = "code_citizenship_cd";
            public static string BadgeNumber = "badge_number";
            public static string OrganizationUnitDescription = "organization_unit_description";
            public static string PayrollProcessGroupCode = "code_payroll_process_group_cd";
            public static string ImportExternalIdentifier = "import_external_identifier";
            public static string CodeEmployeePositionStatusDescription = "code_employee_position_status_description";
            public static string ExcludeMassPayslipFileFlag = "exclude_mass_payslip_file_flag";
            public static string NumberOfRows = "number_of_rows";
            public static string Email = "email";
            public static string GovernmentIdentificationNumberType = "code_government_identification_number_type_cd";
            public static string GovernmentIdentificationNumber1 = "government_identification_number_1";
            public static string GovernmentIdentificationNumber2 = "government_identification_number_2";
            public static string GovernmentIdentificationNumber3 = "government_identification_number_3";
            public static string AlternateEmployeeNumber = "alternate_employee_number";

            public static string GetColumName(string propertyName)
            {
                if (propertyName != null)
                    return (string)typeof(ColumnNames).GetField(propertyName).GetValue(new ColumnNames());
                else
                    return null;
            }
        }
        #endregion

        #region select

        internal IEnumerable<EmployeeBiographic> SelectEmployeeBiographics(DatabaseUser user, IEnumerable<long> employeeIds)
        {
            if (employeeIds == null)
            {
                return Enumerable.Empty<EmployeeBiographic>();
            }

            #region Build employee data table

            DataTable table = new DataTable();
            table.Columns.Add("employee_id", typeof(long));
            foreach (long employeeId in employeeIds)
            {
                table.Rows.Add(employeeId);
            }

            #endregion

            using (DataBaseCommand command = GetStoredProcCommand("EmployeeBiographicsExport_select", user.DatabaseName))
            {
                SqlParameter parm = new SqlParameter("@employeeIds", SqlDbType.Structured) {Value = table};
                command.Parameters.Add(parm);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToEmployeeBiographicList(reader);
                }
            }
        }

        internal EmployeeCollection Select(DatabaseUser user, long employeeId, bool securityOverrideFlag)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Employee_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@securityOverride", securityOverrideFlag);
                command.AddParameterWithValue("@employeeId", employeeId);
                command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
                command.AddParameterWithValue("@securityUserId", user.SecurityUserId);

                EmployeeCollection employees = null;

                using (IDataReader reader = command.ExecuteReader())
                    employees = MapToEmployeeCollection(reader);

                return employees;
            }
        }
        internal EmployeeCollection SelectEmployeeByPersonId(DatabaseUser user, long personId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeByPersonId_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@personId", personId);

                EmployeeCollection employees = null;

                using (IDataReader reader = command.ExecuteReader())
                    employees = MapToEmployeeCollection(reader);

                return employees;
            }
        }
        internal EmployeeSummaryCollection SelectEmployeeSummary(DatabaseUser user, EmployeeCriteria criteria)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Employee_report", user.DatabaseName))
            {
                command.AddParameterWithValue("@employeeId", criteria.EmployeeId);
                command.AddParameterWithValue("@securityOverride", criteria.SecurityOverrideFlag);
                command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
                command.AddParameterWithValue("@securityUserId", user.SecurityUserId);
                command.AddParameterWithValue("@employeeNumber", criteria.EmployeeNumber);
                command.AddParameterWithValue("@importExternalIdentifier", criteria.ImportExternalIdentifierNotUsed);
                command.AddParameterWithValue("@languageCode", criteria.LanguageCode);
                command.AddParameterWithValue("@useLikeCondition", criteria.UseLikeStatementInProcSelect);
                command.AddParameterWithValue("@socialInsuranceNumber", criteria.SocialInsuranceNumber);
                command.AddParameterWithValue("@firstName", criteria.FirstName);
                command.AddParameterWithValue("@lastName", criteria.LastName);
                command.AddParameterWithValue("@organizationUnitDescription", criteria.OrganizationUnitName);
                command.AddParameterWithValue("@payrollProcessId", criteria.PayrollProcessId);
                command.AddParameterWithValue("@payrollProcessGroupCode", criteria.PayrollProcessGroupCode);
                command.AddParameterWithValue("@pageIndex", criteria.PageIndex);
                command.AddParameterWithValue("@pageSize", criteria.PageSize);
                command.AddParameterWithValue("@sortColumn", ColumnNames.GetColumName(criteria.SortColumn));
                command.AddParameterWithValue("@sortDirection", criteria.SortDirection);
                command.AddParameterWithValue("@positionStatusCodeWhereClause", criteria.EmployeePositionStatusCodeWhereClause);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToEmployeeSummaryCollection(reader);
            }
        }
        public EmployeeCollection SelectBatch(DatabaseUser user, string[] employeeIdentifiers)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Employee_batchSelect", user.DatabaseName))
            {
                SqlParameter parmEmployee = new SqlParameter();
                parmEmployee.ParameterName = "@parmEmployee";
                parmEmployee.SqlDbType = SqlDbType.Structured;

                DataTable tableEmployee = LoadEmployeeDataTable(employeeIdentifiers);
                parmEmployee.Value = tableEmployee;
                command.AddParameter(parmEmployee);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToEmployeeCollection(reader);
            }
        }
        private DataTable LoadEmployeeDataTable(string[] employeeIdentifiers)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("import_external_identifier", typeof(string));

            for (int i = 0; i < employeeIdentifiers.Length; i++)
            {
                table.Rows.Add(new Object[]
                {
                    employeeIdentifiers[i]
                });
            }

            return table;
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, Employee employee)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("Employee_update", user.DatabaseName))
                {
                    command.BeginTransaction("Employee_update");

                    command.AddParameterWithValue("@employeeId", employee.EmployeeId);
                    command.AddParameterWithValue("@personId", employee.PersonId);
                    command.AddParameterWithValue("@employeeNumber", employee.EmployeeNumber);
                    command.AddParameterWithValue("@maritalStatusCode", employee.MaritalStatusCode);
                    command.AddParameterWithValue("@maritalStatusEffectiveDate", employee.MaritalStatusEffectiveDate);
                    command.AddParameterWithValue("@bilingualismCode", employee.BilingualismCode);
                    command.AddParameterWithValue("@healthCareNumber", employee.HealthCareNumber);
                    command.AddParameterWithValue("@healthCareProvinceStateCode", employee.HealthCareProvinceStateCode);
                    command.AddParameterWithValue("@citizenshipCode", employee.CitizenshipCode);
                    command.AddParameterWithValue("@badgeNumber", employee.BadgeNumber);
                    command.AddParameterWithValue("@importExternalIdentifier", employee.ImportExternalIdentifier);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", employee.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    //employee.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void UpdateBatch(DatabaseUser user, WorkLinksEmployeeCollection items)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Employee_batchUpdate", user.DatabaseName))
            {
                command.BeginTransaction("Employee_batchUpdate");

                //table parameter
                SqlParameter parmEmployee = new SqlParameter();
                parmEmployee.ParameterName = "@parmEmployee";
                parmEmployee.SqlDbType = SqlDbType.Structured;

                DataTable tableEmployee = LoadEmployeeDataTable(items);
                parmEmployee.Value = tableEmployee;
                command.AddParameter(parmEmployee);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        private DataTable LoadEmployeeDataTable(WorkLinksEmployeeCollection items)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("person_id", typeof(long));
            table.Columns.Add("code_title_cd", typeof(string));
            table.Columns.Add("first_name", typeof(string));
            table.Columns.Add("middle_name", typeof(string));
            table.Columns.Add("last_name", typeof(string));
            table.Columns.Add("known_as_name", typeof(string));
            table.Columns.Add("code_gender_cd", typeof(string));
            table.Columns.Add("government_identification_number_1", typeof(string));
            table.Columns.Add("government_identification_number_2", typeof(string));
            table.Columns.Add("government_identification_number_3", typeof(string));
            table.Columns.Add("birth_date", typeof(DateTime));
            table.Columns.Add("code_language_cd", typeof(string));
            table.Columns.Add("employee_id", typeof(long));
            table.Columns.Add("employee_number", typeof(string));
            table.Columns.Add("code_marital_status_cd", typeof(string));
            table.Columns.Add("marital_status_effective_date", typeof(DateTime));
            table.Columns.Add("code_bilingualism_cd", typeof(string));
            table.Columns.Add("health_care_number", typeof(string));
            table.Columns.Add("health_care_code_province_state_cd", typeof(string));
            table.Columns.Add("code_citizenship_cd", typeof(string));
            table.Columns.Add("badge_number", typeof(string));
            table.Columns.Add("import_external_identifier", typeof(string));

            foreach (Employee item in items)
            {
                table.Rows.Add(new Object[]
                {
                    item.PersonId,
                    item.TitleCode == string.Empty ? null : item.TitleCode,
                    item.FirstName,
                    item.MiddleName,
                    item.LastName,
                    item.KnownAsName,
                    item.GenderCode,
                    item.GovernmentIdentificationNumber1,
                    item.GovernmentIdentificationNumber2,
                    item.GovernmentIdentificationNumber3,
                    item.BirthDate,
                    item.LanguageCode,
                    item.EmployeeId,
                    item.EmployeeNumber,
                    item.MaritalStatusCode == string.Empty ? null : item.MaritalStatusCode,
                    item.MaritalStatusEffectiveDate,
                    item.BilingualismCode == string.Empty ? null: item.BilingualismCode,
                    item.HealthCareNumber,
                    item.HealthCareProvinceStateCode,
                    item.CitizenshipCode,
                    item.BadgeNumber,
                    item.ImportExternalIdentifier
                });
            }

            return table;
        }
        #endregion

        #region insert
        public void Insert(DatabaseUser user, Employee employee)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Employee_insert", user.DatabaseName))
            {
                command.BeginTransaction("Employee_insert");

                SqlParameter employeeIdParm = command.AddParameterWithValue("@employeeId", employee.EmployeeId, ParameterDirection.Output);
                command.AddParameterWithValue("@personId", employee.PersonId);
                command.AddParameterWithValue("@employeeNumber", employee.EmployeeNumber);
                command.AddParameterWithValue("@maritalStatusCode", employee.MaritalStatusCode == string.Empty ? null : employee.MaritalStatusCode);
                command.AddParameterWithValue("@maritalStatusEffectiveDate", employee.MaritalStatusEffectiveDate);
                command.AddParameterWithValue("@bilingualismCode", employee.BilingualismCode == string.Empty ? null : employee.BilingualismCode);
                command.AddParameterWithValue("@healthCareNumber", employee.HealthCareNumber);
                command.AddParameterWithValue("@healthCareProvinceStateCode", employee.HealthCareProvinceStateCode);
                command.AddParameterWithValue("@citizenshipCode", employee.CitizenshipCode == string.Empty ? null : employee.CitizenshipCode);
                command.AddParameterWithValue("@badgeNumber", employee.BadgeNumber);
                command.AddParameterWithValue("@importExternalIdentifier", employee.ImportExternalIdentifier);

                //command.AddParameterWithValue("@rowVersion", employee.RowVersion);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                employee.EmployeeId = Convert.ToInt64(employeeIdParm.Value);

                command.CommitTransaction();
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, Employee employee)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("Employee_delete", user.DatabaseName))
                {
                    command.BeginTransaction("Employee_delete");

                    command.AddParameterWithValue("@employeeId", employee.EmployeeId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeCollection MapToEmployeeCollection(IDataReader reader)
        {
            EmployeeCollection collection = new EmployeeCollection();

            while (reader.Read())
                collection.Add(MapToEmployee(reader));

            return collection;
        }
        protected Employee MapToEmployee(IDataReader reader)
        {
            Employee employee = new Employee();
            base.MapToBase(employee, reader);

            employee.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            employee.PersonId = (long)CleanDataValue(reader[ColumnNames.PersonId]);
            employee.EmployeeNumber = (string)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            employee.MaritalStatusCode = (string)CleanDataValue(reader[ColumnNames.MaritalStatusCode]);
            employee.MaritalStatusEffectiveDate = (DateTime?)CleanDataValue(reader[ColumnNames.MaritalStatusEffectiveDate]);
            employee.BilingualismCode = (string)CleanDataValue(reader[ColumnNames.BilingualismCode]);
            employee.HealthCareNumber = (string)CleanDataValue(reader[ColumnNames.HealthCareNumber]);
            employee.HealthCareProvinceStateCode = (string)CleanDataValue(reader[ColumnNames.HealthCareProvinceStateCode]);
            employee.CitizenshipCode = (string)CleanDataValue(reader[ColumnNames.CitizenshipCode]);
            employee.BadgeNumber = (string)CleanDataValue(reader[ColumnNames.BadgeNumber]);
            employee.ImportExternalIdentifier = (string)CleanDataValue(reader[ColumnNames.ImportExternalIdentifier]);

            return employee;
        }
        protected EmployeeSummaryCollection MapToEmployeeSummaryCollection(IDataReader reader)
        {
            EmployeeSummaryCollection collection = new EmployeeSummaryCollection();

            while (reader.Read())
                collection.Add(MapToEmployeeSummary(reader));

            return collection;
        }
        protected EmployeeSummary MapToEmployeeSummary(IDataReader reader)
        {
            EmployeeSummary employee = new EmployeeSummary();

            employee.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            employee.PersonId = (long)CleanDataValue(reader[ColumnNames.PersonId]);
            employee.EmployeeNumber = (string)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            employee.LastName = (string)CleanDataValue(reader[ColumnNames.LastName]);
            employee.FirstName = (string)CleanDataValue(reader[ColumnNames.FirstName]);
            employee.BirthDate = (DateTime?)CleanDataValue(reader[ColumnNames.BirthDate]);
            employee.PayrollProcessGroupCode = (string)CleanDataValue(reader[ColumnNames.PayrollProcessGroupCode]);
            employee.OrganizationUnitDescription = (string)CleanDataValue(reader[ColumnNames.OrganizationUnitDescription]);
            employee.ImportExternalIdentifier = (string)CleanDataValue(reader[ColumnNames.ImportExternalIdentifier]);
            employee.CodeEmployeePositionStatusDescription = (string)CleanDataValue(reader[ColumnNames.CodeEmployeePositionStatusDescription]);
            employee.ExcludeMassPayslipFileFlag = (bool)CleanDataValue(reader[ColumnNames.ExcludeMassPayslipFileFlag]);
            employee.NumberOfRows = (Int32?)CleanDataValue(reader[ColumnNames.NumberOfRows]);
            employee.Email = (string)CleanDataValue(reader[ColumnNames.Email]);

            return employee;
        }

        protected IEnumerable<EmployeeBiographic> MapToEmployeeBiographicList(IDataReader reader)
        {
            List<EmployeeBiographic> bios = new List<EmployeeBiographic>();

            while (reader.Read())
            {
                bios.Add(MapToEmployeeBiographic(reader));
            }

            return bios;
        }

        protected EmployeeBiographic MapToEmployeeBiographic(IDataReader reader)
        {
            EmployeeBiographic bio = new EmployeeBiographic();

            bio.EmployeeNumber = (string)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            bio.TitleCode = (string) CleanDataValue(reader[ColumnNames.TitleCode]);
            bio.LastName = (string)CleanDataValue(reader[ColumnNames.LastName]);
            bio.MiddleName = (string)CleanDataValue(reader[ColumnNames.MiddleName]);
            bio.FirstName = (string)CleanDataValue(reader[ColumnNames.FirstName]);
            bio.KnownAsName = (string)CleanDataValue(reader[ColumnNames.KnownAsName]);
            bio.BirthDate = (DateTime?)CleanDataValue(reader[ColumnNames.BirthDate]);
            bio.LanguageCode = (string)CleanDataValue(reader[ColumnNames.LanguageCode]);
            bio.GenderCode = (string)CleanDataValue(reader[ColumnNames.GenderCode]);
            bio.SocialInsuranceNumber = (string)CleanDataValue(reader[ColumnNames.SocialInsuranceNumber]);
            bio.MaritalStatusCode = (string)CleanDataValue(reader[ColumnNames.MaritalStatusCode]);
            bio.BilingualismCode = (string)CleanDataValue(reader[ColumnNames.BilingualismCode]);
            bio.HealthCareNumber = (string)CleanDataValue(reader[ColumnNames.HealthCareNumber]);
            bio.HealthCareProvinceStateCode = (string)CleanDataValue(reader[ColumnNames.HealthCareProvinceStateCode]);
            bio.CitizenshipCode = (string)CleanDataValue(reader[ColumnNames.CitizenshipCode]);
            bio.GovernmentIdentificationNumberTypeCode = (string) CleanDataValue(reader[ColumnNames.GovernmentIdentificationNumberType]);
            bio.GovernmentIdentificationNumber1 = (string)CleanDataValue(reader[ColumnNames.GovernmentIdentificationNumber1]);
            bio.GovernmentIdentificationNumber2 = (string)CleanDataValue(reader[ColumnNames.GovernmentIdentificationNumber2]);
            bio.GovernmentIdentificationNumber3 = (string)CleanDataValue(reader[ColumnNames.GovernmentIdentificationNumber3]);
            bio.BadgeNumber = (string)CleanDataValue(reader[ColumnNames.BadgeNumber]);
            bio.AlternateEmployeeNumber = (string) CleanDataValue(reader[ColumnNames.AlternateEmployeeNumber]);

            return bio;
        }

        #endregion
    }
}