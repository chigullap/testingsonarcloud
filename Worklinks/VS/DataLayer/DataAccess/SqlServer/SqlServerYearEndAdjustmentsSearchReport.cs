﻿using System;
using System.Data;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerYearEndAdjustmentsSearchReport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String KeyId = "key";
            public static String YearEndAdjustmentTableId = "year_end_adjustment_table_id";
            public static String EmployeeNumber = "employee_number";
            public static String EmployeeId = "employee_id";
            public static String LastName = "last_name";
            public static String FirstName = "first_name";
            public static String Year = "year";
            public static String YearEndTypeForm = "year_end_type_form";
            public static String CodePayrollProcessGroupCd = "code_payroll_process_group_cd";
            public static String EmployerNumber = "employer_number";
            public static String TaxableCodeProvinceStateCd = "taxable_code_province_state_cd";
            public static String Revision = "revision";
            public static String CurrentRevision = "current_revision";
            public static String MaxRevision = "max_revision";
        }
        #endregion

        #region main
        internal YearEndAdjustmentsSearchReportCollection GetYearEndAdjustmentsSearchReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, YearEndAdjustmentsSearchCriteria criteria)
        {
            using (DataBaseCommand command = GetStoredProcCommand("YearEndFormAdjustment_report", user.DatabaseName))
            {
                command.AddParameterWithValue("@year", criteria.Year);
                command.AddParameterWithValue("@employeeNumber", criteria.EmployeeNumber);
                command.AddParameterWithValue("@lastName", criteria.LastName);
                command.AddParameterWithValue("@sin", criteria.SIN);
                command.AddParameterWithValue("@codePayrollProcessGroupCd", criteria.PayrollProcessGroupCode);
                command.AddParameterWithValue("@formType", criteria.FormType);
                command.AddParameterWithValue("@getMaxRevision", criteria.GetMaxRevision);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToYearEndAdjustmentsSearchReportCollection(reader);
            }
        }
        #endregion

        #region data mapping
        protected YearEndAdjustmentsSearchReportCollection MapToYearEndAdjustmentsSearchReportCollection(IDataReader reader)
        {
            YearEndAdjustmentsSearchReportCollection collection = new YearEndAdjustmentsSearchReportCollection();

            while (reader.Read())
                collection.Add(MapToYearEndAdjustmentsSearchReport(reader));

            return collection;
        }
        protected YearEndAdjustmentsSearchReport MapToYearEndAdjustmentsSearchReport(IDataReader reader)
        {
            YearEndAdjustmentsSearchReport yearEndAdjustment = new YearEndAdjustmentsSearchReport();
            yearEndAdjustment.KeyId = Convert.ToInt64(CleanDataValue(reader[ColumnNames.KeyId]));
            yearEndAdjustment.YearEndAdjustmentTableId = Convert.ToInt64(CleanDataValue(reader[ColumnNames.YearEndAdjustmentTableId]));
            yearEndAdjustment.EmployeeNumber = (String)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            yearEndAdjustment.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            yearEndAdjustment.LastName = (String)CleanDataValue(reader[ColumnNames.LastName]);
            yearEndAdjustment.FirstName = (String)CleanDataValue(reader[ColumnNames.FirstName]);
            yearEndAdjustment.Year = (Decimal)CleanDataValue(reader[ColumnNames.Year]);
            yearEndAdjustment.YearEndTypeForm = (String)CleanDataValue(reader[ColumnNames.YearEndTypeForm]);
            yearEndAdjustment.CodePayrollProcessGroupCd = (String)CleanDataValue(reader[ColumnNames.CodePayrollProcessGroupCd]);
            yearEndAdjustment.EmployerNumber = (String)CleanDataValue(reader[ColumnNames.EmployerNumber]);
            yearEndAdjustment.TaxableCodeProvinceStateCd = (String)CleanDataValue(reader[ColumnNames.TaxableCodeProvinceStateCd]);
            yearEndAdjustment.Revision = (int)CleanDataValue(reader[ColumnNames.Revision]);
            yearEndAdjustment.CurrentRevision = (int)CleanDataValue(reader[ColumnNames.CurrentRevision]);
            yearEndAdjustment.MaxRevision = (int)CleanDataValue(reader[ColumnNames.MaxRevision]);

            return yearEndAdjustment;
        }
        #endregion
    }
}