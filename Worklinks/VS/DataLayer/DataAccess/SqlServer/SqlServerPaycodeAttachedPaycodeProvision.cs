﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerPaycodeAttachedPaycodeProvision : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PaycodeAttachedPaycodeProvisionId = "paycode_attached_paycode_provision_id";
            public static String PaycodeCode = "code_paycode_cd";
            public static String ProvisionPaycodeCode = "provision_code_paycode_cd";
        }
        #endregion

        #region main
        internal PaycodeAttachedPaycodeProvisionCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String paycodeCode)
        {
            DataBaseCommand command = GetStoredProcCommand("PaycodeAttachedPaycodeProvision_select", user.DatabaseName);
            command.AddParameterWithValue("@paycodeCode", paycodeCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCodeCollection(reader);
        }
        public PaycodeAttachedPaycodeProvision Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, PaycodeAttachedPaycodeProvision attachedPaycode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PaycodeAttachedPaycodeProvision_insert", user.DatabaseName))
            {
                command.BeginTransaction("PaycodeAttachedPaycodeProvision_insert");

                SqlParameter attachedPaycodeIdParm = command.AddParameterWithValue("@paycodeAttachedPaycodeProvisionId", attachedPaycode.PaycodeAttachedPaycodeProvisionId, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@paycodeCode", attachedPaycode.PaycodeCode);
                command.AddParameterWithValue("@provisionPaycodeCode", attachedPaycode.ProvisionPaycodeCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", attachedPaycode.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                attachedPaycode.PaycodeAttachedPaycodeProvisionId = (long)attachedPaycodeIdParm.Value;
                attachedPaycode.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();

                return attachedPaycode;
            }
        }
        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String paycodeCode)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PaycodeAttachedPaycodeProvision_delete", user.DatabaseName))
                {
                    command.BeginTransaction("PaycodeAttachedPaycodeProvision_delete");
                    command.AddParameterWithValue("@paycodeCode", paycodeCode);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected PaycodeAttachedPaycodeProvisionCollection MapToCodeCollection(IDataReader reader)
        {
            PaycodeAttachedPaycodeProvisionCollection collection = new PaycodeAttachedPaycodeProvisionCollection();

            while (reader.Read())
                collection.Add(MapToCode(reader));

            return collection;
        }
        protected PaycodeAttachedPaycodeProvision MapToCode(IDataReader reader)
        {
            PaycodeAttachedPaycodeProvision code = new PaycodeAttachedPaycodeProvision();
            base.MapToBase(code, reader);

            code.PaycodeAttachedPaycodeProvisionId = (long)CleanDataValue(reader[ColumnNames.PaycodeAttachedPaycodeProvisionId]);
            code.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            code.ProvisionPaycodeCode = (String)CleanDataValue(reader[ColumnNames.ProvisionPaycodeCode]);

            return code;
        }
        #endregion 
    }
}