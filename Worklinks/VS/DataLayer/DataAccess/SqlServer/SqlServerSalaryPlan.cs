﻿using System;
using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerSalaryPlan : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string SalaryPlanId = "salary_plan_id";
            public static string EffectiveDate = "effective_date";
            public static string EndDate = "end_date";
            public static string Name = "name";
            public static string ActiveFlag = "active_flag";
            public static string EnglishDescription = "english_description";
            public static string FrenchDescription = "french_description";

            //organization unit
            public static string SalaryPlanOrganizationUnitId = "salary_plan_organization_unit_id";
            public static string SalaryPlanGradeId = "salary_plan_grade_id";
            public static string OrganizationUnit = "organization_unit";
        }
        #endregion

        #region select
        internal SalaryPlanCollection Select(DatabaseUser user, long? salaryPlanId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SalaryPlan_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@salaryPlanId", salaryPlanId);

                SalaryPlanCollection collection = null;

                using (IDataReader reader = command.ExecuteReader())
                    collection = MapToSalaryPlanCollection(reader);

                return collection;
            }
        }
        internal SalaryPlanOrganizationUnitCollection SelectOrganizationUnit(DatabaseUser user, string organizationUnit)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SalaryPlanOrganizationUnit_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@organizationUnit", organizationUnit);

                SalaryPlanOrganizationUnitCollection collection = null;

                using (IDataReader reader = command.ExecuteReader())
                    collection = MapToOrganizationUnitCollection(reader);

                return collection;
            }
        }
        #endregion

        #region data mapping
        protected SalaryPlanCollection MapToSalaryPlanCollection(IDataReader reader)
        {
            SalaryPlanCollection collection = new SalaryPlanCollection();

            while (reader.Read())
                collection.Add(MapToSalaryPlan(reader));

            return collection;
        }
        protected SalaryPlanOrganizationUnitCollection MapToOrganizationUnitCollection(IDataReader reader)
        {
            SalaryPlanOrganizationUnitCollection collection = new SalaryPlanOrganizationUnitCollection();

            while (reader.Read())
                collection.Add(MapToOrganizationUnit(reader));

            return collection;
        }
        protected SalaryPlan MapToSalaryPlan(IDataReader reader)
        {
            SalaryPlan item = new SalaryPlan();
            base.MapToBase(item, reader);

            item.SalaryPlanId = (long)CleanDataValue(reader[ColumnNames.SalaryPlanId]);
            item.EffectiveDate = (DateTime)CleanDataValue(reader[ColumnNames.EffectiveDate]);
            item.EndDate = (DateTime)CleanDataValue(reader[ColumnNames.EndDate]);
            item.Name = (string)CleanDataValue(reader[ColumnNames.Name]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.EnglishDescription = (string)CleanDataValue(reader[ColumnNames.EnglishDescription]);
            item.FrenchDescription = (string)CleanDataValue(reader[ColumnNames.FrenchDescription]);

            return item;
        }
        protected SalaryPlanOrganizationUnit MapToOrganizationUnit(IDataReader reader)
        {
            SalaryPlanOrganizationUnit item = new SalaryPlanOrganizationUnit();
            base.MapToBase(item, reader);

            item.SalaryPlanOrganizationUnitId = (long)CleanDataValue(reader[ColumnNames.SalaryPlanOrganizationUnitId]);
            item.SalaryPlanId = (long)CleanDataValue(reader[ColumnNames.SalaryPlanId]);
            item.SalaryPlanGradeId = (long)CleanDataValue(reader[ColumnNames.SalaryPlanGradeId]);
            item.OrganizationUnit = (string)CleanDataValue(reader[ColumnNames.OrganizationUnit]);

            return item;
        }
        #endregion
    }
}