﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerYearEndT4XmlFileToWorklinks : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string YearEndT4Id = "year_end_t4_id";
            public static string EmployeeId = "employee_id";
            public static string LastName = "last_name";
            public static string FirstName = "first_name";
            public static string AddressLine1 = "address_line_1";
            public static string AddressLine2 = "address_line_2";
            public static string City = "city";
            public static string PostalZipCode = "postal_zip_code";
            public static string ProvinceStateCode = "code_province_state_cd";
            public static string CountryCode = "code_country_cd";
            public static string SocialInsuranceNumberSegment1 = "social_insurance_number_segment_1";
            public static string SocialInsuranceNumberSegment2 = "social_insurance_number_segment_2";
            public static string SocialInsuranceNumberSegment3 = "social_insurance_number_segment_3";
            public static string EmployeeNumber = "employee_number";
            public static string EmployerNumber = "employer_number";
            public static string TaxableCodeProvinceStateCd = "taxable_code_province_state_cd";
            public static string T4Sequence = "t4_sequence";
            public static string Year = "year";
            public static string EmploymentIncome14 = "employment_income_14";
            public static string CanadaPensionPlanDeducted16 = "canada_pension_plan_deducted_16";
            public static string QuebecPensionPlanDeducted17 = "quebec_pension_plan_deducted_17";
            public static string EmploymentInsuranceDeducted18 = "employment_insurance_deducted_18";
            public static string RegisteredPension20 = "registered_pension_20";
            public static string IncomeTaxDeducted22 = "income_tax_deducted_22";
            public static string EmploymentInsuranceEarnings24 = "employment_insurance_earnings_24";
            public static string CanadaPensionPlanEarn26 = "canada_pension_plan_earn_26";
            public static string UnionDues44 = "union_dues_44";
            public static string CharitableDonate46 = "charitable_donate_46";
            public static string OriginalRegistrationNumber50 = "original_registration_number_50";
            public static string PensionAdjustment52 = "pension_adjustment_52";
            public static string EmployerCanadaPensionPlanAmount = "employer_canada_pension_plan_amount";
            public static string EmployerEmploymentInsuranceAmount = "employer_employment_insurance_amount";
            public static string EmploymentInsuranceExemption28Flag = "employment_insurance_exemption_28_flag";
            public static string CanadaPensionPlanExempt28Flag = "canada_pension_plan_exempt_28_flag";
            public static string EmploymentInsuranceEarnings24Report = "employment_insurance_earnings_24_report";
            public static string CanadaPensionPlanEarn26Report = "canada_pension_plan_earn_26_report";
            public static string ProvincialParentalInsurancePlanPremiums = "provincial_parental_insurance_plan_premiums";
            public static string ProvincialParentalInsurancePlanEarnings = "provincial_parental_insurance_plan_earnings";
            public static string ProvincialParentalInsurancePlanExemption28Flag = "provincial_parental_insurance_plan_exemption_28_flag";
            public static string RecordStatus = "record_status";
            public static string GenerateXml = "generate_xml";
            public static string XmlCreated = "xml_created";
            public static string HousingBoardLodging30 = "housing_board_lodging_30";
            public static string SpecialWorksite31 = "special_worksite_31";
            public static string DesignatedAreaTravel32 = "designated_area_travel_32";
            public static string MedicalTravel33 = "medical_travel_33";
            public static string PersonalAutoUse34 = "personal_auto_use_34";
            public static string InterestFreeLoan36 = "interest_free_loan_36";
            public static string StockOptionBenefit38 = "stock_option_benefit_38";
            public static string OtherTaxAll40 = "other_tax_all_40";
            public static string CommissionIncome42 = "commission_income_42";
            public static string DeferredSecurity53 = "deferred_security_53";
            public static string EligibleRetiringAllow = "eligible_retiring_allow";
            public static string NonEligibleRetiringAllow = "non_eligible_retiring_allow";
            public static string IndianNonEligibleRetiring = "indian_non_eligible_retiring";
            public static string StatusIndian71 = "status_indian_71";
            public static string WorkersCompensationBoardRepaid77 = "workers_compensation_board_repaid_77";
            public static string PrivateHealth85 = "private_health_85";
            public static string SupplementaryTopupAmountWhileOnLeave89 = "supplementary_topup_amount_while_on_leave_89";
            public static string EmployeeHomeRelocationLoanDeduction37 = "employee_home_relocation_loan_deduction_37";
            public static string SecurityOptionsDeduction110_1_d_39 = "security_options_deduction_110_1_d_39";
            public static string SecurityOptionsDeduction_110_1_41 = "security_options_deduction_110_1_41";
            public static string T4BoxNumber1 = "T4BoxNumber1";
            public static string T4BoxAmount1 = "T4BoxAmount1";
            public static string T4BoxNumber2 = "T4BoxNumber2";
            public static string T4BoxAmount2 = "T4BoxAmount2";
            public static string T4BoxNumber3 = "T4BoxNumber3";
            public static string T4BoxAmount3 = "T4BoxAmount3";
            public static string T4BoxNumber4 = "T4BoxNumber4";
            public static string T4BoxAmount4 = "T4BoxAmount4";
            public static string T4BoxNumber5 = "T4BoxNumber5";
            public static string T4BoxAmount5 = "T4BoxAmount5";
            public static string T4BoxNumber6 = "T4BoxNumber6";
            public static string T4BoxAmount6 = "T4BoxAmount6";
            public static string ActiveFlag = "active_flag";
            public static string Revision = "revision";
            public static string PreviousRevisionYearEndT4Id = "previous_revision_year_end_t4_id";
            public static string RegistrationNumber50 = "registration_number_50";
        }
        #endregion

        #region main
        public YearEndT4Collection Select(DatabaseUser user, int year, long? businessNumberId, bool getOrginals = false, bool getAmended = false)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndT4_select", user.DatabaseName);

            command.AddParameterWithValue("@databaseName", user.DatabaseName);
            command.AddParameterWithValue("@year", year);
            command.AddParameterWithValue("@businessNumberId", businessNumberId);
            command.AddParameterWithValue("@getOriginals", getOrginals);
            command.AddParameterWithValue("@getAmended", getAmended);

            using (IDataReader reader = command.ExecuteReader())
                return MapToYearEndT4Collection(reader);
        }
        public YearEndT4Collection SelectBatch(DatabaseUser user, string[] yearEndT4Ids)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndT4_batchSelect", user.DatabaseName);

            command.AddParameterWithValue("@yearEndT4Ids", yearEndT4Ids);

            using (IDataReader reader = command.ExecuteReader())
                return MapToYearEndT4Collection(reader);
        }
        public YearEndT4Collection SelectSingleT4ByKey(DatabaseUser user, long yearEndT4Id)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndT4ByKeyId_select", user.DatabaseName);

            command.AddParameterWithValue("@yearEndT4Id", yearEndT4Id);

            using (IDataReader reader = command.ExecuteReader())
                return MapToYearEndT4Collection(reader);
        }
        public YearEndT4 Insert(DatabaseUser user, YearEndT4 item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("YearEndT4_insert", user.DatabaseName))
            {
                command.BeginTransaction("YearEndT4_insert");

                SqlParameter yearEndT4IdParm = command.AddParameterWithValue("@yearEndT4Id", item.YearEndT4Id, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", item.EmployeeId);
                command.AddParameterWithValue("@employerNumber", item.EmployerNumber);
                command.AddParameterWithValue("@taxableCodeProvinceStateCd", item.TaxableCodeProvinceStateCd);
                command.AddParameterWithValue("@t4Sequence", item.T4Sequence);
                command.AddParameterWithValue("@year", item.Year);
                command.AddParameterWithValue("@employmentIncome14", item.EmploymentIncome14);
                command.AddParameterWithValue("@canadaPensionPlanDeducted16", item.CanadaPensionPlanDeducted16);
                command.AddParameterWithValue("@quebecPensionPlanDeducted17", item.QuebecPensionPlanDeducted17);
                command.AddParameterWithValue("@employmentInsuranceDeducted18", item.EmploymentInsuranceDeducted18);
                command.AddParameterWithValue("@registeredPension20", item.RegisteredPension20);
                command.AddParameterWithValue("@incomeTaxDeducted22", item.IncomeTaxDeducted22);
                command.AddParameterWithValue("@employmentInsuranceEarnings24", item.EmploymentInsuranceEarnings24);
                command.AddParameterWithValue("@canadaPensionPlanEarn26", item.CanadaPensionPlanEarn26);
                command.AddParameterWithValue("@unionDues44", item.UnionDues44);
                command.AddParameterWithValue("@charitableDonate46", item.CharitableDonate46);
                command.AddParameterWithValue("@originalRegistrationNumber50", item.OriginalRegistrationNumber50);
                command.AddParameterWithValue("@pensionAdjustment52", item.PensionAdjustment52);
                command.AddParameterWithValue("@employerCanadaPensionPlanAmount", item.EmployerCanadaPensionPlanAmount);
                command.AddParameterWithValue("@employerEmploymentInsuranceAmount", item.EmployerEmploymentInsuranceAmount);
                command.AddParameterWithValue("@employmentInsuranceExemption28Flag", item.EmploymentInsuranceExemption28Flag);
                command.AddParameterWithValue("@canadaPensionPlanExempt28Flag", item.CanadaPensionPlanExempt28Flag);
                command.AddParameterWithValue("@employmentInsuranceEarnings24Report", item.EmploymentInsuranceEarnings24Report);
                command.AddParameterWithValue("@canadaPensionPlanEarn26Report", item.CanadaPensionPlanEarn26Report);
                command.AddParameterWithValue("@provincialParentalInsurancePlanPremiums", item.ProvincialParentalInsurancePlanPremiums);
                command.AddParameterWithValue("@provincialParentalInsurancePlanEarnings", item.ProvincialParentalInsurancePlanEarnings);
                command.AddParameterWithValue("@provincialParentalInsurancePlanExemption28Flag", item.ProvincialParentalInsurancePlanExemption28Flag);
                command.AddParameterWithValue("@recordStatus", item.RecordStatus);
                command.AddParameterWithValue("@generateXml", item.GenerateXml);
                command.AddParameterWithValue("@xmlCreated", item.XmlCreated);
                command.AddParameterWithValue("@housingBoardLodging30", item.HousingBoardLodging30);
                command.AddParameterWithValue("@specialWorksite31", item.SpecialWorksite31);
                command.AddParameterWithValue("@designatedAreaTravel32", item.DesignatedAreaTravel32);
                command.AddParameterWithValue("@medicalTravel33", item.MedicalTravel33);
                command.AddParameterWithValue("@personalAutoUse34", item.PersonalAutoUse34);
                command.AddParameterWithValue("@interestFreeLoan36", item.InterestFreeLoan36);
                command.AddParameterWithValue("@stockOptionBenefit38", item.StockOptionBenefit38);
                command.AddParameterWithValue("@otherTaxAll40", item.OtherTaxAll40);
                command.AddParameterWithValue("@commissionIncome42", item.CommissionIncome42);
                command.AddParameterWithValue("@deferredSecurity53", item.DeferredSecurity53);
                command.AddParameterWithValue("@eligibleRetiringAllow", item.EligibleRetiringAllow);
                command.AddParameterWithValue("@nonEligibleRetiringAllow", item.NonEligibleRetiringAllow);
                command.AddParameterWithValue("@indianNonEligibleRetiring", item.IndianNonEligibleRetiring);
                command.AddParameterWithValue("@statusIndian71", item.StatusIndian71);
                command.AddParameterWithValue("@workersCompensationBoardRepaid77", item.WorkersCompensationBoardRepaid77);
                command.AddParameterWithValue("@privateHealth85", item.PrivateHealth85);
                command.AddParameterWithValue("@supplementaryTopupAmountWhileOnLeave89", item.SupplementaryTopupAmountWhileOnLeave89);
                command.AddParameterWithValue("@activeFlag", item.ActiveFlag);
                command.AddParameterWithValue("@revision", item.Revision);
                command.AddParameterWithValue("@previousRevisionYearEndT4Id", item.PreviousRevisionYearEndT4Id);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.YearEndT4Id = Convert.ToInt64(yearEndT4IdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        public void Update(DatabaseUser user, long keyId, bool activeFlag)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("YearEndT4_update", user.DatabaseName))
                {
                    command.BeginTransaction("YearEndT4_update");

                    command.AddParameterWithValue("@keyId", keyId);
                    command.AddParameterWithValue("@activeFlag", activeFlag);

                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected YearEndT4Collection MapToYearEndT4Collection(IDataReader reader)
        {
            YearEndT4Collection collection = new YearEndT4Collection();

            while (reader.Read())
                collection.Add(MapToYearEndT4(reader));

            return collection;
        }
        protected YearEndT4 MapToYearEndT4(IDataReader reader)
        {
            YearEndT4 item = new YearEndT4();
            MapToBase(item, reader);

            item.YearEndT4Id = (long)CleanDataValue(reader[ColumnNames.YearEndT4Id]);
            item.Year = (decimal)CleanDataValue(reader[ColumnNames.Year]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.LastName = (string)CleanDataValue(reader[ColumnNames.LastName]);
            item.FirstName = (string)CleanDataValue(reader[ColumnNames.FirstName]);
            item.AddressLine1 = (string)CleanDataValue(reader[ColumnNames.AddressLine1]);
            item.AddressLine2 = (string)CleanDataValue(reader[ColumnNames.AddressLine2]);
            item.City = (string)CleanDataValue(reader[ColumnNames.City]);
            item.PostalZipCode = (string)CleanDataValue(reader[ColumnNames.PostalZipCode]);
            item.ProvinceStateCode = (string)CleanDataValue(reader[ColumnNames.ProvinceStateCode]);
            item.CountryCode = (string)CleanDataValue(reader[ColumnNames.CountryCode]);
            item.SocialInsuranceNumberSegment1 = (string)CleanDataValue(reader[ColumnNames.SocialInsuranceNumberSegment1]);
            item.SocialInsuranceNumberSegment2 = (string)CleanDataValue(reader[ColumnNames.SocialInsuranceNumberSegment2]);
            item.SocialInsuranceNumberSegment3 = (string)CleanDataValue(reader[ColumnNames.SocialInsuranceNumberSegment3]);
            item.EmployeeNumber = (string)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.EmployerNumber = (string)CleanDataValue(reader[ColumnNames.EmployerNumber]);
            item.TaxableCodeProvinceStateCd = (string)CleanDataValue(reader[ColumnNames.TaxableCodeProvinceStateCd]);
            item.T4Sequence = (short?)CleanDataValue(reader[ColumnNames.T4Sequence]);
            item.EmploymentIncome14 = (decimal)CleanDataValue(reader[ColumnNames.EmploymentIncome14]);
            item.CanadaPensionPlanDeducted16 = (decimal)CleanDataValue(reader[ColumnNames.CanadaPensionPlanDeducted16]);
            item.QuebecPensionPlanDeducted17 = (decimal)CleanDataValue(reader[ColumnNames.QuebecPensionPlanDeducted17]);
            item.EmploymentInsuranceDeducted18 = (decimal)CleanDataValue(reader[ColumnNames.EmploymentInsuranceDeducted18]);
            item.RegisteredPension20 = (decimal)CleanDataValue(reader[ColumnNames.RegisteredPension20]);
            item.IncomeTaxDeducted22 = (decimal)CleanDataValue(reader[ColumnNames.IncomeTaxDeducted22]);
            item.EmploymentInsuranceEarnings24 = (decimal)CleanDataValue(reader[ColumnNames.EmploymentInsuranceEarnings24]);
            item.CanadaPensionPlanEarn26 = (decimal)CleanDataValue(reader[ColumnNames.CanadaPensionPlanEarn26]);
            item.UnionDues44 = (decimal)CleanDataValue(reader[ColumnNames.UnionDues44]);
            item.CharitableDonate46 = (decimal)CleanDataValue(reader[ColumnNames.CharitableDonate46]);
            item.OriginalRegistrationNumber50 = (string)CleanDataValue(reader[ColumnNames.OriginalRegistrationNumber50]);
            item.PensionAdjustment52 = (decimal)CleanDataValue(reader[ColumnNames.PensionAdjustment52]);
            item.EmployerCanadaPensionPlanAmount = (decimal)CleanDataValue(reader[ColumnNames.EmployerCanadaPensionPlanAmount]);
            item.EmployerEmploymentInsuranceAmount = (decimal)CleanDataValue(reader[ColumnNames.EmployerEmploymentInsuranceAmount]);
            item.EmploymentInsuranceExemption28Flag = (bool)CleanDataValue(reader[ColumnNames.EmploymentInsuranceExemption28Flag]);
            item.CanadaPensionPlanExempt28Flag = (bool)CleanDataValue(reader[ColumnNames.CanadaPensionPlanExempt28Flag]);
            item.EmploymentInsuranceEarnings24Report = (decimal)CleanDataValue(reader[ColumnNames.EmploymentInsuranceEarnings24Report]);
            item.CanadaPensionPlanEarn26Report = (decimal)CleanDataValue(reader[ColumnNames.CanadaPensionPlanEarn26Report]);
            item.ProvincialParentalInsurancePlanPremiums = (decimal)CleanDataValue(reader[ColumnNames.ProvincialParentalInsurancePlanPremiums]);
            item.ProvincialParentalInsurancePlanEarnings = (decimal)CleanDataValue(reader[ColumnNames.ProvincialParentalInsurancePlanEarnings]);
            item.ProvincialParentalInsurancePlanExemption28Flag = (bool)CleanDataValue(reader[ColumnNames.ProvincialParentalInsurancePlanExemption28Flag]);
            item.RecordStatus = (short?)CleanDataValue(reader[ColumnNames.RecordStatus]);
            item.GenerateXml = (byte?)CleanDataValue(reader[ColumnNames.GenerateXml]);
            item.XmlCreated = (byte?)CleanDataValue(reader[ColumnNames.XmlCreated]);
            item.HousingBoardLodging30 = (decimal)CleanDataValue(reader[ColumnNames.HousingBoardLodging30]);
            item.SpecialWorksite31 = (decimal)CleanDataValue(reader[ColumnNames.SpecialWorksite31]);
            item.DesignatedAreaTravel32 = (decimal)CleanDataValue(reader[ColumnNames.DesignatedAreaTravel32]);
            item.MedicalTravel33 = (decimal)CleanDataValue(reader[ColumnNames.MedicalTravel33]);
            item.PersonalAutoUse34 = (decimal)CleanDataValue(reader[ColumnNames.PersonalAutoUse34]);
            item.InterestFreeLoan36 = (decimal)CleanDataValue(reader[ColumnNames.InterestFreeLoan36]);
            item.StockOptionBenefit38 = (decimal)CleanDataValue(reader[ColumnNames.StockOptionBenefit38]);
            item.OtherTaxAll40 = (decimal)CleanDataValue(reader[ColumnNames.OtherTaxAll40]);
            item.CommissionIncome42 = (decimal)CleanDataValue(reader[ColumnNames.CommissionIncome42]);
            item.DeferredSecurity53 = (decimal)CleanDataValue(reader[ColumnNames.DeferredSecurity53]);
            item.EligibleRetiringAllow = (decimal)CleanDataValue(reader[ColumnNames.EligibleRetiringAllow]);
            item.NonEligibleRetiringAllow = (decimal)CleanDataValue(reader[ColumnNames.NonEligibleRetiringAllow]);
            item.IndianNonEligibleRetiring = (decimal)CleanDataValue(reader[ColumnNames.IndianNonEligibleRetiring]);
            item.StatusIndian71 = (decimal)CleanDataValue(reader[ColumnNames.StatusIndian71]);
            item.WorkersCompensationBoardRepaid77 = (decimal)CleanDataValue(reader[ColumnNames.WorkersCompensationBoardRepaid77]);
            item.PrivateHealth85 = (decimal)CleanDataValue(reader[ColumnNames.PrivateHealth85]);
            item.SupplementaryTopupAmountWhileOnLeave89 = (decimal)CleanDataValue(reader[ColumnNames.SupplementaryTopupAmountWhileOnLeave89]);
            item.EmployeeHomeRelocationLoanDeduction37 = (decimal?)CleanDataValue(reader[ColumnNames.EmployeeHomeRelocationLoanDeduction37]);
            item.SecurityOptionsDeduction110_1_d_39 = (decimal?)CleanDataValue(reader[ColumnNames.SecurityOptionsDeduction110_1_d_39]);
            item.SecurityOptionsDeduction_110_1_41 = (decimal?)CleanDataValue(reader[ColumnNames.SecurityOptionsDeduction_110_1_41]);
            item.T4BoxNumber1 = (int?)CleanDataValue(reader[ColumnNames.T4BoxNumber1]);
            item.T4BoxAmount1 = (decimal?)CleanDataValue(reader[ColumnNames.T4BoxAmount1]);
            item.T4BoxNumber2 = (int?)CleanDataValue(reader[ColumnNames.T4BoxNumber2]);
            item.T4BoxAmount2 = (decimal?)CleanDataValue(reader[ColumnNames.T4BoxAmount2]);
            item.T4BoxNumber3 = (int?)CleanDataValue(reader[ColumnNames.T4BoxNumber3]);
            item.T4BoxAmount3 = (decimal?)CleanDataValue(reader[ColumnNames.T4BoxAmount3]);
            item.T4BoxNumber4 = (int?)CleanDataValue(reader[ColumnNames.T4BoxNumber4]);
            item.T4BoxAmount4 = (decimal?)CleanDataValue(reader[ColumnNames.T4BoxAmount4]);
            item.T4BoxNumber5 = (int?)CleanDataValue(reader[ColumnNames.T4BoxNumber5]);
            item.T4BoxAmount5 = (decimal?)CleanDataValue(reader[ColumnNames.T4BoxAmount5]);
            item.T4BoxNumber6 = (int?)CleanDataValue(reader[ColumnNames.T4BoxNumber6]);
            item.T4BoxAmount6 = (decimal?)CleanDataValue(reader[ColumnNames.T4BoxAmount6]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.Revision = (int)CleanDataValue(reader[ColumnNames.Revision]);
            item.PreviousRevisionYearEndT4Id = (long?)CleanDataValue(reader[ColumnNames.PreviousRevisionYearEndT4Id]);
            item.RegistrationNumber50 = (string)CleanDataValue(reader[ColumnNames.RegistrationNumber50]);

            return item;
        }
        #endregion
    }
}