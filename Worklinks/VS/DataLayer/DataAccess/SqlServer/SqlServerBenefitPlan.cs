﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerBenefitPlan : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String BenefitPlanId = "benefit_plan_id";
            public static String EnglishDescription = "english_description";
            public static String FrenchDescription = "french_description";
        }
        #endregion

        #region main
        internal BenefitPlanCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? benefitPlanId)
        {
            DataBaseCommand command = GetStoredProcCommand("BenefitPlan_select", user.DatabaseName);

            command.AddParameterWithValue("@benefitPlanId", benefitPlanId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToBenefitPlanCollection(reader);
        }

        public BenefitPlan Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPlan benefitPlan)
        {
            using (DataBaseCommand command = GetStoredProcCommand("BenefitPlan_insert", user.DatabaseName))
            {
                command.BeginTransaction("BenefitPlan_insert");

                SqlParameter entitlementIdParm = command.AddParameterWithValue("@benefitPlanId", benefitPlan.BenefitPlanId, ParameterDirection.Output);
                command.AddParameterWithValue("@englishDescription", benefitPlan.EnglishDescription);
                command.AddParameterWithValue("@frenchDescription", benefitPlan.FrenchDescription);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", benefitPlan.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                benefitPlan.BenefitPlanId = Convert.ToInt64(entitlementIdParm.Value);
                benefitPlan.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return benefitPlan;
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPlan benefitPlan)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("BenefitPlan_update", user.DatabaseName))
                {
                    command.BeginTransaction("BenefitPlan_update");

                    command.AddParameterWithValue("@benefitPlanId", benefitPlan.BenefitPlanId);
                    command.AddParameterWithValue("@englishDescription", benefitPlan.EnglishDescription);
                    command.AddParameterWithValue("@frenchDescription", benefitPlan.FrenchDescription);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", benefitPlan.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    benefitPlan.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPlan benefitPlan)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("BenefitPlan_delete", user.DatabaseName))
                {
                    command.BeginTransaction("BenefitPlan_delete");

                    command.AddParameterWithValue("@benefitPlanId", benefitPlan.BenefitPlanId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected BenefitPlanCollection MapToBenefitPlanCollection(IDataReader reader)
        {
            BenefitPlanCollection collection = new BenefitPlanCollection();

            while (reader.Read())
                collection.Add(MapToBenefitPlan(reader));

            return collection;
        }

        protected BenefitPlan MapToBenefitPlan(IDataReader reader)
        {
            BenefitPlan item = new BenefitPlan();
            base.MapToBase(item, reader);

            item.BenefitPlanId = (long)CleanDataValue(reader[ColumnNames.BenefitPlanId]);
            item.EnglishDescription = (String)CleanDataValue(reader[ColumnNames.EnglishDescription]);
            item.FrenchDescription = (String)CleanDataValue(reader[ColumnNames.FrenchDescription]);

            return item;
        }
        #endregion
    }
}
