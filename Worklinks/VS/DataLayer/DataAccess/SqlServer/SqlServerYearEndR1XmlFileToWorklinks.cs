﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerYearEndR1XmlFileToWorklinks : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String YearEndR1Id = "year_end_r1_id";
            public static String EmployeeId = "employee_id";
            public static String EmployeeNumber = "employee_number";
            public static String Year = "year";
            public static String BoxAAmount = "box_a_amount";
            public static String BoxA1Amount = "box_a_1_amount";
            public static String BoxA2Amount = "box_a_2_amount";
            public static String BoxA3Amount = "box_a_3_amount";
            public static String BoxA4Amount = "box_a_4_amount";
            public static String BoxA5Amount = "box_a_5_amount";
            public static String BoxA6Amount = "box_a_6_amount";
            public static String BoxA7Amount = "box_a_7_amount";
            public static String BoxA8Amount = "box_a_8_amount";
            public static String BoxA9Amount = "box_a_9_amount";
            public static String BoxA10Amount = "box_a_10_amount";
            public static String BoxA11Amount = "box_a_11_amount";
            public static String BoxA12Amount = "box_a_12_amount";
            public static String BoxA13Amount = "box_a_13_amount";
            public static String BoxA14Amount = "box_a_14_amount";
            public static String BoxBAmount = "box_b_amount";
            public static String BoxB1Amount = "box_b_1_amount";
            public static String BoxCAmount = "box_c_amount";
            public static String BoxDAmount = "box_d_amount";
            public static String BoxD1Amount = "box_d_1_amount";
            public static String BoxD2Amount = "box_d_2_amount";
            public static String BoxD3Amount = "box_d_3_amount";
            public static String BoxEAmount = "box_e_amount";
            public static String BoxFAmount = "box_f_amount";
            public static String BoxGAmount = "box_g_amount";
            public static String BoxG1Amount = "box_g_1_amount";
            public static String BoxG2Amount = "box_g_2_amount";
            public static String BoxHAmount = "box_h_amount";
            public static String BoxIAmount = "box_i_amount";
            public static String BoxJAmount = "box_j_amount";
            public static String BoxKAmount = "box_k_amount";
            public static String BoxK1Amount = "box_k_1_amount";
            public static String BoxLAmount = "box_l_amount";
            public static String BoxL2Amount = "box_l_2_amount";
            public static String BoxL3Amount = "box_l_3_amount";
            public static String BoxL4Amount = "box_l_4_amount";
            public static String BoxL5Amount = "box_l_5_amount";
            public static String BoxL7Amount = "box_l_7_amount";
            public static String BoxL8Amount = "box_l_8_amount";
            public static String BoxL9Amount = "box_l_9_amount";
            public static String BoxL10Amount = "box_l_10_amount";
            public static String BoxMAmount = "box_m_amount";
            public static String BoxNAmount = "box_n_amount";
            public static String BoxO2Amount = "box_o_2_amount";
            public static String BoxO3Amount = "box_o_3_amount";
            public static String BoxO4Amount = "box_o_4_amount";
            public static String BoxORJAmount = "box_o_rj_amount";
            public static String BoxORQAmount = "box_o_rq_amount";
            public static String BoxPAmount = "box_p_amount";
            public static String BoxQAmount = "box_q_amount";
            public static String BoxRAmount = "box_r_amount";
            public static String BoxR1Amount = "box_r_1_amount";
            public static String BoxSAmount = "box_s_amount";
            public static String BoxTAmount = "box_t_amount";
            public static String BoxUAmount = "box_u_amount";
            public static String BoxVAmount = "box_v_amount";
            public static String BoxV1Amount = "box_v_1_amount";
            public static String BoxWAmount = "box_w_amount";
            public static String Box200Amount = "box_200_amount";
            public static String Box201Amount = "box_201_amount";
            public static String Box211Amount = "box_211_amount";
            public static String Box235Amount = "box_235_amount";
            public static String GenerateXml = "generate_xml";
            public static String RecordStatus = "record_status";
            public static String SlipNumber = "xml_slip_number";
            public static String EmployerProvincialPensionPlanAmount = "employer_provincial_pension_plan_amount";
            public static String EmployerProvincialParentalInsurancePlanAmount = "employer_provincial_parental_insurance_plan_amount";
            public static String ActiveFlag = "active_flag";
            public static String Revision = "revision";
            public static String PreviousRevisionYearEndR1Id = "previous_revision_year_end_r1_id";
            public static String PreviousSlipNumber = "previous_xml_slip_number";
            public static String R1BoxNumberO = "R1BoxNumberO";
            public static String R1BoxAmountO = "R1BoxAmountO";
            public static String R1BoxNumber1 = "R1BoxNumber1";
            public static String R1BoxAmount1 = "R1BoxAmount1";
            public static String R1BoxNumber2 = "R1BoxNumber2";
            public static String R1BoxAmount2 = "R1BoxAmount2";
            public static String R1BoxNumber3 = "R1BoxNumber3";
            public static String R1BoxAmount3 = "R1BoxAmount3";
            public static String R1BoxNumber4 = "R1BoxNumber4";
            public static String R1BoxAmount4 = "R1BoxAmount4";
        }
        #endregion

        #region select
        public YearEndR1Collection Select(DatabaseUser user, int year, bool getOrginals = false, bool getAmended = false)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndR1_select", user.DatabaseName);

            command.AddParameterWithValue("@year", year);
            command.AddParameterWithValue("@getOriginals", getOrginals);
            command.AddParameterWithValue("@getAmended", getAmended);

            using (IDataReader reader = command.ExecuteReader())
                return MapToYearEndR1Collection(reader);
        }
        public YearEndR1Collection SelectSingleR1ByKey(DatabaseUser user, long key)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndR1ByKeyId_select", user.DatabaseName);

            command.AddParameterWithValue("@yearEndR1Id", key);

            using (IDataReader reader = command.ExecuteReader())
                return MapToYearEndR1Collection(reader);
        }
        #endregion

        #region insert
        public YearEndR1 Insert(DatabaseUser user, YearEndR1 item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("YearEndR1_insert", user.DatabaseName))
            {
                command.BeginTransaction("YearEndR1_insert");

                SqlParameter yearEndR1IdParm = command.AddParameterWithValue("@yearEndR1Id", item.YearEndR1Id, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", item.EmployeeId);
                command.AddParameterWithValue("@year", item.Year);
                command.AddParameterWithValue("@boxAAmount", item.BoxAAmount);
                command.AddParameterWithValue("@boxA1Amount", item.BoxA1Amount);
                command.AddParameterWithValue("@boxA2Amount", item.BoxA2Amount);
                command.AddParameterWithValue("@boxA3Amount", item.BoxA3Amount);
                command.AddParameterWithValue("@boxA4Amount", item.BoxA4Amount);
                command.AddParameterWithValue("@boxA5Amount", item.BoxA5Amount);
                command.AddParameterWithValue("@boxA6Amount", item.BoxA6Amount);
                command.AddParameterWithValue("@boxA7Amount", item.BoxA7Amount);
                command.AddParameterWithValue("@boxA8Amount", item.BoxA8Amount);
                command.AddParameterWithValue("@boxA9Amount", item.BoxA9Amount);
                command.AddParameterWithValue("@boxA10Amount", item.BoxA10Amount);
                command.AddParameterWithValue("@boxA11Amount", item.BoxA11Amount);
                command.AddParameterWithValue("@boxA12Amount", item.BoxA12Amount);
                command.AddParameterWithValue("@boxA13Amount", item.BoxA13Amount);
                command.AddParameterWithValue("@boxA14Amount", item.BoxA14Amount);
                command.AddParameterWithValue("@boxBAmount", item.BoxBAmount);
                command.AddParameterWithValue("@boxB1Amount", item.BoxB1Amount);
                command.AddParameterWithValue("@boxCAmount", item.BoxCAmount);
                command.AddParameterWithValue("@boxDAmount", item.BoxDAmount);
                command.AddParameterWithValue("@boxD1Amount", item.BoxD1Amount);
                command.AddParameterWithValue("@boxD2Amount", item.BoxD2Amount);
                command.AddParameterWithValue("@boxD3Amount", item.BoxD3Amount);
                command.AddParameterWithValue("@boxEAmount", item.BoxEAmount);
                command.AddParameterWithValue("@boxFAmount", item.BoxFAmount);
                command.AddParameterWithValue("@boxGAmount", item.BoxGAmount);
                command.AddParameterWithValue("@boxG1Amount", item.BoxG1Amount);
                command.AddParameterWithValue("@boxG2Amount", item.BoxG2Amount);
                command.AddParameterWithValue("@boxHAmount", item.BoxHAmount);
                command.AddParameterWithValue("@boxIAmount", item.BoxIAmount);
                command.AddParameterWithValue("@boxJAmount", item.BoxJAmount);
                command.AddParameterWithValue("@boxKAmount", item.BoxKAmount);
                command.AddParameterWithValue("@boxK1Amount", item.BoxK1Amount);
                command.AddParameterWithValue("@boxLAmount", item.BoxLAmount);
                command.AddParameterWithValue("@boxL2Amount", item.BoxL2Amount);
                command.AddParameterWithValue("@boxL3Amount", item.BoxL3Amount);
                command.AddParameterWithValue("@boxL4Amount", item.BoxL4Amount);
                command.AddParameterWithValue("@boxL5Amount", item.BoxL5Amount);
                command.AddParameterWithValue("@boxL7Amount", item.BoxL7Amount);
                command.AddParameterWithValue("@boxL8Amount", item.BoxL8Amount);
                command.AddParameterWithValue("@boxL9Amount", item.BoxL9Amount);
                command.AddParameterWithValue("@boxL10Amount", item.BoxL10Amount);
                command.AddParameterWithValue("@boxMAmount", item.BoxMAmount);
                command.AddParameterWithValue("@boxNAmount", item.BoxNAmount);
                command.AddParameterWithValue("@boxO2Amount", item.BoxO2Amount);
                command.AddParameterWithValue("@boxO3Amount", item.BoxO3Amount);
                command.AddParameterWithValue("@boxO4Amount", item.BoxO4Amount);
                command.AddParameterWithValue("@boxORJAmount", item.BoxORJAmount);
                command.AddParameterWithValue("@boxORQAmount", item.BoxORQAmount);
                command.AddParameterWithValue("@boxPAmount", item.BoxPAmount);
                command.AddParameterWithValue("@boxQAmount", item.BoxQAmount);
                command.AddParameterWithValue("@boxRAmount", item.BoxRAmount);
                command.AddParameterWithValue("@boxR1Amount", item.BoxR1Amount);
                command.AddParameterWithValue("@boxSAmount", item.BoxSAmount);
                command.AddParameterWithValue("@boxTAmount", item.BoxTAmount);
                command.AddParameterWithValue("@boxUAmount", item.BoxUAmount);
                command.AddParameterWithValue("@boxVAmount", item.BoxVAmount);
                command.AddParameterWithValue("@boxV1Amount", item.BoxV1Amount);
                command.AddParameterWithValue("@boxWAmount", item.BoxWAmount);
                command.AddParameterWithValue("@box200Amount", item.Box200Amount);
                command.AddParameterWithValue("@box201Amount", item.Box201Amount);
                command.AddParameterWithValue("@box211Amount", item.Box211Amount);
                command.AddParameterWithValue("@box235Amount", item.Box235Amount);
                command.AddParameterWithValue("@generateXml", item.GenerateXml);
                command.AddParameterWithValue("@recordStatus", item.RecordStatus);
                command.AddParameterWithValue("@slipNumber", item.SlipNumber);
                command.AddParameterWithValue("@employerProvincialPensionPlanAmount", item.EmployerProvincialPensionPlanAmount);
                command.AddParameterWithValue("@employerProvincialParentalInsurancePlanAmount", item.EmployerProvincialParentalInsurancePlanAmount);
                command.AddParameterWithValue("@activeFlag", item.ActiveFlag);
                command.AddParameterWithValue("@revision", item.Revision);
                command.AddParameterWithValue("@previousRevisionYearEndR1Id", item.PreviousRevisionYearEndR1Id);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.YearEndR1Id = Convert.ToInt64(yearEndR1IdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, long keyId, bool activeFlag)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("YearEndR1_update", user.DatabaseName))
                {
                    command.BeginTransaction("YearEndR1_update");

                    command.AddParameterWithValue("@keyId", keyId);
                    command.AddParameterWithValue("@activeFlag", activeFlag);

                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected YearEndR1Collection MapToYearEndR1Collection(IDataReader reader)
        {
            YearEndR1Collection collection = new YearEndR1Collection();

            while (reader.Read())
                collection.Add(MapToYearEndR1(reader));

            return collection;
        }
        protected YearEndR1 MapToYearEndR1(IDataReader reader)
        {
            YearEndR1 item = new YearEndR1();
            base.MapToBase(item, reader);

            item.YearEndR1Id = (long)CleanDataValue(reader[ColumnNames.YearEndR1Id]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EmployeeNumber = (String)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.Year = (Decimal)CleanDataValue(reader[ColumnNames.Year]);
            item.BoxAAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxAAmount]);
            item.BoxA1Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxA1Amount]);
            item.BoxA2Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxA2Amount]);
            item.BoxA3Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxA3Amount]);
            item.BoxA4Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxA4Amount]);
            item.BoxA5Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxA5Amount]);
            item.BoxA6Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxA6Amount]);
            item.BoxA7Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxA7Amount]);
            item.BoxA8Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxA8Amount]);
            item.BoxA9Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxA9Amount]);
            item.BoxA10Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxA10Amount]);
            item.BoxA11Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxA11Amount]);
            item.BoxA12Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxA12Amount]);
            item.BoxA13Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxA13Amount]);
            item.BoxA14Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxA14Amount]);
            item.BoxBAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxBAmount]);
            item.BoxB1Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxB1Amount]);
            item.BoxCAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxCAmount]);
            item.BoxDAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxDAmount]);
            item.BoxD1Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxD1Amount]);
            item.BoxD2Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxD2Amount]);
            item.BoxD3Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxD3Amount]);
            item.BoxEAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxEAmount]);
            item.BoxFAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxFAmount]);
            item.BoxGAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxGAmount]);
            item.BoxG1Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxG1Amount]);
            item.BoxG2Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxG2Amount]);
            item.BoxHAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxHAmount]);
            item.BoxIAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxIAmount]);
            item.BoxJAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxJAmount]);
            item.BoxKAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxKAmount]);
            item.BoxK1Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxK1Amount]);
            item.BoxLAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxLAmount]);
            item.BoxL2Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxL2Amount]);
            item.BoxL3Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxL3Amount]);
            item.BoxL4Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxL4Amount]);
            item.BoxL5Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxL5Amount]);
            item.BoxL7Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxL7Amount]);
            item.BoxL8Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxL8Amount]);
            item.BoxL9Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxL9Amount]);
            item.BoxL10Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxL10Amount]);
            item.BoxMAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxMAmount]);
            item.BoxNAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxNAmount]);
            item.BoxO2Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxO2Amount]);
            item.BoxO3Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxO3Amount]);
            item.BoxO4Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxO4Amount]);
            item.BoxORJAmount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxORJAmount]);
            item.BoxORQAmount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxORQAmount]);
            item.BoxPAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxPAmount]);
            item.BoxQAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxQAmount]);
            item.BoxRAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxRAmount]);
            item.BoxR1Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxR1Amount]);
            item.BoxSAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxSAmount]);
            item.BoxTAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxTAmount]);
            item.BoxUAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxUAmount]);
            item.BoxVAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxVAmount]);
            item.BoxV1Amount = (Decimal?)CleanDataValue(reader[ColumnNames.BoxV1Amount]);
            item.BoxWAmount = (Decimal)CleanDataValue(reader[ColumnNames.BoxWAmount]);
            item.Box200Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box200Amount]);
            item.Box201Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box201Amount]);
            item.Box211Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box211Amount]);
            item.Box235Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box235Amount]);
            item.GenerateXml = (byte?)CleanDataValue(reader[ColumnNames.GenerateXml]);
            item.RecordStatus = (short?)CleanDataValue(reader[ColumnNames.RecordStatus]);
            item.SlipNumber = (int)(long)CleanDataValue(reader[ColumnNames.SlipNumber]);
            item.EmployerProvincialPensionPlanAmount = (Decimal)CleanDataValue(reader[ColumnNames.EmployerProvincialPensionPlanAmount]);
            item.EmployerProvincialParentalInsurancePlanAmount = (Decimal)CleanDataValue(reader[ColumnNames.EmployerProvincialParentalInsurancePlanAmount]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.Revision = (int)CleanDataValue(reader[ColumnNames.Revision]);
            item.PreviousRevisionYearEndR1Id = (long?)CleanDataValue(reader[ColumnNames.PreviousRevisionYearEndR1Id]);
            item.PreviousSlipNumber = (int?)(long?)CleanDataValue(reader[ColumnNames.PreviousSlipNumber]);
            item.R1BoxNumberO = (String)CleanDataValue(reader[ColumnNames.R1BoxNumberO]);
            item.R1BoxAmountO = (Decimal?)CleanDataValue(reader[ColumnNames.R1BoxAmountO]);
            item.R1BoxNumber1 = (String)CleanDataValue(reader[ColumnNames.R1BoxNumber1]);
            item.R1BoxAmount1 = (Decimal?)CleanDataValue(reader[ColumnNames.R1BoxAmount1]);
            item.R1BoxNumber2 = (String)CleanDataValue(reader[ColumnNames.R1BoxNumber2]);
            item.R1BoxAmount2 = (Decimal?)CleanDataValue(reader[ColumnNames.R1BoxAmount2]);
            item.R1BoxNumber3 = (String)CleanDataValue(reader[ColumnNames.R1BoxNumber3]);
            item.R1BoxAmount3 = (Decimal?)CleanDataValue(reader[ColumnNames.R1BoxAmount3]);
            item.R1BoxNumber4 = (String)CleanDataValue(reader[ColumnNames.R1BoxNumber4]);
            item.R1BoxAmount4 = (Decimal?)CleanDataValue(reader[ColumnNames.R1BoxAmount4]);

            return item;
        }
        #endregion
    }
}