﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerAutoPayroll : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String AutoPayrollScheduleId = "auto_payroll_schedule_id";
            public static String CodePayrollProcessGroupCd = "code_payroll_process_group_cd";
            public static String DateToExecute = "date_to_execute";
            public static String ChequeDate = "cheque_date";
            public static String ProcessedFlag = "processed_flag";
            public static String PayrollProcessId = "payroll_process_id";
        }
        #endregion

        #region main
        internal AutoPayrollScheduleCollection Select(DatabaseUser user, DateTime todaysDate)
        {
            DataBaseCommand command = GetStoredProcCommand("AutoPayrollSchedule_select", user.DatabaseName);

            command.AddParameterWithValue("@dateToday", todaysDate);

            using (IDataReader reader = command.ExecuteReader())
                return MapToAutoPayrollScheduleCollection(reader);
        }

        public void UpdateProcessed(DatabaseUser user, AutoPayrollSchedule schedule)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("AutoPayrollSchedule_updateProcessed", user.DatabaseName))
                {
                    command.BeginTransaction("AutoPayrollSchedule_updateProcessed");

                    command.AddParameterWithValue("@autoPayrollScheduleId", schedule.AutoPayrollScheduleId);
                    command.AddParameterWithValue("@processedFlag", schedule.ProcessedFlag);
                    command.AddParameterWithValue("@payrollProcessId", schedule.PayrollProcessId);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", schedule.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    schedule.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        #endregion

        #region data mapping
        protected AutoPayrollScheduleCollection MapToAutoPayrollScheduleCollection(IDataReader reader)
        {
            AutoPayrollScheduleCollection collection = new AutoPayrollScheduleCollection();

            while (reader.Read())
                collection.Add(MapToAutoPayrollSchedule(reader));

            return collection;
        }
        protected AutoPayrollSchedule MapToAutoPayrollSchedule(IDataReader reader)
        {
            AutoPayrollSchedule item = new AutoPayrollSchedule();
            base.MapToBase(item, reader);

            item.AutoPayrollScheduleId = (long)CleanDataValue(reader[ColumnNames.AutoPayrollScheduleId]);
            item.CodePayrollProcessGroupCd = (String)CleanDataValue(reader[ColumnNames.CodePayrollProcessGroupCd]);
            item.DateToExecute = (DateTime)CleanDataValue(reader[ColumnNames.DateToExecute]);
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.ProcessedFlag = Convert.ToBoolean(CleanDataValue(reader[ColumnNames.ProcessedFlag]));
            item.PayrollProcessId = (long?)CleanDataValue(reader[ColumnNames.PayrollProcessId]);

            return item;
        }

        #endregion
    }
}