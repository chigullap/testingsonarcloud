﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerCanadaRevenueAgencyBondExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String CanadaRevenueAgencyBondExportId = "canada_revenue_agency_bond_export_id";
            public static String PayrollProcessId = "payroll_process_id";
            public static String TransmitterOrganizationNumber = "transmitter_organization_number";
            public static String OrganizationNumber = "organization_number";
            public static String TransmissionDate = "transmission_date";
            public static String CodeCanadaRevenueAgencyBondPaymentTypeCd = "code_canada_revenue_agency_bond_payment_type_cd";
            public static String ContactFax = "contact_fax";
            public static String ContactEmail = "contact_email";
        }
        #endregion

        #region main
        internal CanadaRevenueAgencyBondExportCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            DataBaseCommand command = GetStoredProcCommand("CanadaRevenueAgencyBondExport_select", user.DatabaseName);
            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToCanadaRevenueAgencyBondExportCollection(reader);
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CanadaRevenueAgencyBondExport export)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CanadaRevenueAgencyBondExport_update", user.DatabaseName))
            {
                command.BeginTransaction("CanadaRevenueAgencyBondExport_update");

                command.AddParameterWithValue("@canadaRevenueAgencyBondExportId", export.CanadaRevenueAgencyBondExportId);
                command.AddParameterWithValue("@payrollProcessId", export.PayrollProcessId);
                command.AddParameterWithValue("@transmitterOrganizationNumber", export.TransmitterOrganizationNumber);
                command.AddParameterWithValue("@organizationNumber", export.OrganizationNumber);
                command.AddParameterWithValue("@transmissionDate", export.TransmissionDate);
                command.AddParameterWithValue("@codeCanadaRevenueAgencyBondPaymentTypeCd", export.CodeCanadaRevenueAgencyBondPaymentTypeCd);
                command.AddParameterWithValue("@contactFax", export.ContactFax);
                command.AddParameterWithValue("@contactEmail", export.ContactEmail);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", export.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                export.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();
            }
        }

        public CanadaRevenueAgencyBondExport Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CanadaRevenueAgencyBondExport export)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CanadaRevenueAgencyBondExport_insert", user.DatabaseName))
            {
                command.BeginTransaction("CanadaRevenueAgencyBondExport_insert");

                SqlParameter canadaRevenueAgencyBondExportIdParm = command.AddParameterWithValue("@canadaRevenueAgencyBondExportId", export.CanadaRevenueAgencyBondExportId, ParameterDirection.Output);
                command.AddParameterWithValue("@payrollProcessId", export.PayrollProcessId);
                command.AddParameterWithValue("@transmitterOrganizationNumber", export.TransmitterOrganizationNumber);
                command.AddParameterWithValue("@organizationNumber", export.OrganizationNumber);
                command.AddParameterWithValue("@transmissionDate", export.TransmissionDate);
                command.AddParameterWithValue("@codeCanadaRevenueAgencyBondPaymentTypeCd", export.CodeCanadaRevenueAgencyBondPaymentTypeCd);
                command.AddParameterWithValue("@contactFax", export.ContactFax);
                command.AddParameterWithValue("@contactEmail", export.ContactEmail);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", export.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                export.CanadaRevenueAgencyBondExportId = Convert.ToInt64(canadaRevenueAgencyBondExportIdParm.Value);
                export.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return export;
            }
        }

        #endregion

        #region data mapping
        protected CanadaRevenueAgencyBondExportCollection MapToCanadaRevenueAgencyBondExportCollection(IDataReader reader)
        {
            CanadaRevenueAgencyBondExportCollection collection = new CanadaRevenueAgencyBondExportCollection();
            while (reader.Read())
            {
                collection.Add(MapToCanadaRevenueAgencyBondExport(reader));
            }

            return collection;
        }
        protected CanadaRevenueAgencyBondExport MapToCanadaRevenueAgencyBondExport(IDataReader reader)
        {
            CanadaRevenueAgencyBondExport item = new CanadaRevenueAgencyBondExport();
            base.MapToBase(item, reader);

            item.CanadaRevenueAgencyBondExportId = (long)CleanDataValue(reader[ColumnNames.CanadaRevenueAgencyBondExportId]);
            item.PayrollProcessId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessId]);
            item.TransmitterOrganizationNumber = (Decimal)CleanDataValue(reader[ColumnNames.TransmitterOrganizationNumber]);
            item.OrganizationNumber = (Decimal)CleanDataValue(reader[ColumnNames.OrganizationNumber]);
            item.TransmissionDate = (DateTime?)CleanDataValue(reader[ColumnNames.TransmissionDate]);
            item.CodeCanadaRevenueAgencyBondPaymentTypeCd = (string)CleanDataValue(reader[ColumnNames.CodeCanadaRevenueAgencyBondPaymentTypeCd]);
            item.ContactFax = (Int64?)CleanDataValue(reader[ColumnNames.ContactFax]);
            item.ContactEmail = (string)CleanDataValue(reader[ColumnNames.ContactEmail]);

            return item;
        }
        #endregion
    }
}