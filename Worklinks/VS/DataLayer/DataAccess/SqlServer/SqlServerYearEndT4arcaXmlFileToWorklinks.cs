﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerYearEndT4arcaXmlFileToWorklinks : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String YearEndT4arcaId = "year_end_t4arca_id";
            public static String Revision = "revision";
            public static String PreviousRevisionYearEndT4arcaId = "previous_revision_year_end_t4arca_id";
            public static String EmployerNumber = "employer_number";
            public static String EmployeeId = "employee_id";
            public static String Year = "year";
            public static String Box12Amount = "box_12_amount";
            public static String Box14Amount = "box_14_amount";
            public static String Box16Amount = "box_16_amount";
            public static String Box17Amount = "box_17_amount";
            public static String Box18Amount = "box_18_amount";
            public static String Box20Amount = "box_20_amount";
            public static String Box22Amount = "box_22_amount";
            public static String PersonCustodianName = "person_custodian_name";
            public static String ActiveFlag = "active_flag";
        }
        #endregion

        #region main
        public YearEndT4arcaCollection Select(DatabaseUser user, int year, bool getOrginals = false, bool getAmended = false)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndT4arca_select", user.DatabaseName);

            command.AddParameterWithValue("@year", year);
            command.AddParameterWithValue("@getOriginals", getOrginals);
            command.AddParameterWithValue("@getAmended", getAmended);

            using (IDataReader reader = command.ExecuteReader())
                return MapToYearEndT4arcaCollection(reader);
        }
        public YearEndT4arcaCollection SelectSingleT4arcaByKey(DatabaseUser user, long key)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndT4arcaByKeyId_select", user.DatabaseName);

            command.AddParameterWithValue("@yearEndT4arcaId", key);

            using (IDataReader reader = command.ExecuteReader())
                return MapToYearEndT4arcaCollection(reader);
        }
        public YearEndT4arca Insert(DatabaseUser user, YearEndT4arca yearEndT4arcaObj)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndT4arca_insert", user.DatabaseName);

            using (command)
            {
                command.BeginTransaction("YearEndT4arca_insert");

                SqlParameter yearEndT4arcaIdParm = command.AddParameterWithValue("@yearEndT4arcaId", yearEndT4arcaObj.YearEndT4arcaId, ParameterDirection.Output);
                command.AddParameterWithValue("@revision", yearEndT4arcaObj.Revision);
                command.AddParameterWithValue("@previousRevisionYearEndT4arcaId", yearEndT4arcaObj.PreviousRevisionYearEndT4arcaId);
                command.AddParameterWithValue("@employerNumber", yearEndT4arcaObj.EmployerNumber);
                command.AddParameterWithValue("@employeeId", yearEndT4arcaObj.EmployeeId);
                command.AddParameterWithValue("@year", yearEndT4arcaObj.Year);
                command.AddParameterWithValue("@box12Amount", yearEndT4arcaObj.Box12Amount);
                command.AddParameterWithValue("@box14Amount", yearEndT4arcaObj.Box14Amount);
                command.AddParameterWithValue("@box16Amount", yearEndT4arcaObj.Box16Amount);
                command.AddParameterWithValue("@box17Amount", yearEndT4arcaObj.Box17Amount);
                command.AddParameterWithValue("@box18Amount", yearEndT4arcaObj.Box18Amount);
                command.AddParameterWithValue("@box20Amount", yearEndT4arcaObj.Box20Amount);
                command.AddParameterWithValue("@box22Amount", yearEndT4arcaObj.Box22Amount);
                command.AddParameterWithValue("@personCustodianName", yearEndT4arcaObj.PersonCustodianName);
                command.AddParameterWithValue("@activeFlag", yearEndT4arcaObj.ActiveFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", yearEndT4arcaObj.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                yearEndT4arcaObj.YearEndT4arcaId = Convert.ToInt64(yearEndT4arcaIdParm.Value);
                yearEndT4arcaObj.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return yearEndT4arcaObj;
            }
        }
        public void Update(DatabaseUser user, long keyId, bool activeFlag)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("YearEndT4arca_update", user.DatabaseName))
                {
                    command.BeginTransaction("YearEndT4arca_update");

                    command.AddParameterWithValue("@keyId", keyId);
                    command.AddParameterWithValue("@activeFlag", activeFlag);

                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, String year)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("YearEndT4arca_delete", user.DatabaseName))
                {
                    command.BeginTransaction("YearEndT4arca_delete");

                    command.AddParameterWithValue("@year", Convert.ToDecimal(year));

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected YearEndT4arcaCollection MapToYearEndT4arcaCollection(IDataReader reader)
        {
            YearEndT4arcaCollection collection = new YearEndT4arcaCollection();

            while (reader.Read())
                collection.Add(MapToYearEndT4arca(reader));

            return collection;
        }
        protected YearEndT4arca MapToYearEndT4arca(IDataReader reader)
        {
            YearEndT4arca item = new YearEndT4arca();
            base.MapToBase(item, reader);

            item.YearEndT4arcaId = (long)CleanDataValue(reader[ColumnNames.YearEndT4arcaId]);
            item.Revision = (int)CleanDataValue(reader[ColumnNames.Revision]);
            item.PreviousRevisionYearEndT4arcaId = (long?)CleanDataValue(reader[ColumnNames.PreviousRevisionYearEndT4arcaId]);
            item.EmployerNumber = (String)CleanDataValue(reader[ColumnNames.EmployerNumber]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.Year = (Decimal)CleanDataValue(reader[ColumnNames.Year]);
            item.Box12Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box12Amount]);
            item.Box14Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box14Amount]);
            item.Box16Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box16Amount]);
            item.Box17Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box17Amount]);
            item.Box18Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box18Amount]);
            item.Box20Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box20Amount]);
            item.Box22Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box22Amount]);
            item.PersonCustodianName = (String)CleanDataValue(reader[ColumnNames.PersonCustodianName]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);

            return item;
        }
        #endregion       
    }
}