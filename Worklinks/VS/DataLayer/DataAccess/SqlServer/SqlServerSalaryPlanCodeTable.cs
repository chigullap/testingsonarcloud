﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerSalaryPlanCodeTable : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String CodeTableId = "cd";
            public static String ParentCodeTableId = "parent_cd";
            public static String ActiveFlag = "active_flag";
            public static String SystemFlag = "system_flag";
            public static String SortOrder = "sort_order";
            public static String StartDate = "start_date";
            public static String EndDate = "end_date";
            public static String ImportExternalIdentifier = "import_external_identifier";
            public static String HasChildrenFlag = "has_children_flag";
        }
        #endregion

        #region main
        internal SalaryPlanCodeTableCollection Select(DatabaseUser user, String parentSalaryPlanCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CodeSalaryPlan_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@parentSalaryPlanCode", parentSalaryPlanCode);

                SalaryPlanCodeTableCollection codeTableRows = null;

                using (IDataReader reader = command.ExecuteReader())
                    codeTableRows = MapToCollection(reader);

                return codeTableRows;
            }
        }
        internal SalaryPlanCodeTable Insert(DatabaseUser user, SalaryPlanCodeTable item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CodeSalaryPlan_insert", user.DatabaseName))
            {
                command.BeginTransaction("CodeSalaryPlan_insert");

                command.AddParameterWithValue("@code", item.CodeTableId);
                command.AddParameterWithValue("@parentCode", item.ParentCodeTableId == "" ? null : item.ParentCodeTableId);
                command.AddParameterWithValue("@activeFlag", item.ActiveFlag);
                command.AddParameterWithValue("@systemFlag", item.SystemFlag);
                command.AddParameterWithValue("@sortOrder", item.SortOrder);
                command.AddParameterWithValue("@startDate", item.StartDate);
                command.AddParameterWithValue("@endDate", item.EndDate);
                command.AddParameterWithValue("@importExternalIdentifier", item.ImportExternalIdentifier);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        internal SalaryPlanCodeTable Update(DatabaseUser user, SalaryPlanCodeTable item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CodeSalaryPlan_update", user.DatabaseName))
                {
                    command.BeginTransaction("CodeSalaryPlan_update");

                    command.AddParameterWithValue("@code", item.CodeTableId);
                    command.AddParameterWithValue("@parentCode", item.ParentCodeTableId == "" ? null : item.ParentCodeTableId);
                    command.AddParameterWithValue("@activeFlag", item.ActiveFlag);
                    command.AddParameterWithValue("@systemFlag", item.SystemFlag);
                    command.AddParameterWithValue("@sortOrder", item.SortOrder);
                    command.AddParameterWithValue("@startDate", item.StartDate);
                    command.AddParameterWithValue("@endDate", item.EndDate);
                    command.AddParameterWithValue("@importExternalIdentifier", item.ImportExternalIdentifier);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();

                    return item;
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
                return null;
            }
        }
        internal void Delete(DatabaseUser user, SalaryPlanCodeTable item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CodeSalaryPlan_delete", user.DatabaseName))
                {
                    command.BeginTransaction("CodeSalaryPlan_delete");

                    command.AddParameterWithValue("@code", item.CodeTableId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected SalaryPlanCodeTableCollection MapToCollection(IDataReader reader)
        {
            SalaryPlanCodeTableCollection collection = new SalaryPlanCodeTableCollection();

            while (reader.Read())
                collection.Add(MapToItem(reader));

            return collection;
        }
        protected SalaryPlanCodeTable MapToItem(IDataReader reader)
        {
            SalaryPlanCodeTable item = new SalaryPlanCodeTable();
            base.MapToBase(item, reader);

            item.CodeTableId = (string)CleanDataValue(reader[ColumnNames.CodeTableId]);
            item.ParentCodeTableId = (string)CleanDataValue(reader[ColumnNames.ParentCodeTableId]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.SystemFlag = (bool)CleanDataValue(reader[ColumnNames.SystemFlag]);
            item.SortOrder = Convert.ToInt64(CleanDataValue(reader[ColumnNames.SortOrder]));
            item.StartDate = (DateTime?)CleanDataValue(reader[ColumnNames.StartDate]);
            item.EndDate = (DateTime?)CleanDataValue(reader[ColumnNames.EndDate]);
            item.ImportExternalIdentifier = (String)CleanDataValue(reader[ColumnNames.ImportExternalIdentifier]);
            item.HasChildrenFlag = (bool)CleanDataValue(reader[ColumnNames.HasChildrenFlag]);

            return item;
        }
        #endregion
    }
}