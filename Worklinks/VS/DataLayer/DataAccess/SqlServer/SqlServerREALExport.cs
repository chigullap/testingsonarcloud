﻿using System;
using System.Data;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerREALExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string EmployeeNumber = "EmployeeNumber";
            public static string FirstName = "FirstName";
            public static string LastName = "LastName";
            public static string HireDate = "HireDate";
            public static string Username = "Username";
            public static string EmployeeStatus = "EmployeeStatus";
            public static string EmployeeStatusDate = "EmployeeStatusDate";
            public static string BirthDate = "BirthDate";
            public static string StandardHoursPerDay = "StandardHoursPerDay";
            public static string Address = "Address";
            public static string City = "City";
            public static string Province = "Province";
            public static string Zip = "Zip";
            public static string Country = "Country";
            public static string Phone1 = "Phone1";
            public static string Phone2 = "Phone2";
            public static string Phone3 = "Phone3";
            public static string Email = "Email";
            public static string UnionCode = "UnionCode";
            public static string PayCycle = "PayCycle";
            public static string HourlySalary = "HourlySalary";
            public static string BaseRateOfPay = "BaseRateOfPay";
            public static string BaseWage = "BaseWage";
            public static string PositionChangeDate = "PositionChangeDate";
            public static string LL1 = "LL1";
            public static string LL2 = "LL2";
            public static string LL3 = "LL3";
            public static string LL4 = "LL4";
            public static string LL5 = "LL5";
            public static string LL6 = "LL6";
            public static string LL7 = "LL7";
            public static string LL1Desc = "LL1Desc";
            public static string LL2Desc = "LL2Desc";
            public static string LL3Desc = "LL3Desc";
            public static string LL4Desc = "LL4Desc";
            public static string LL5Desc = "LL5Desc";
            public static string LL6Desc = "LL6Desc";
            public static string LL7Desc = "LL7Desc";
            public static string ReportsTo = "ReportsTo";
            public static string WorkerType = "WorkerType";
            public static string BadgeNumber = "BadgeNumber";
            public static string MobileLicense = "MobileLicense";
            public static string ManagerLicense = "ManagerLicense";
            public static string ScheduleLicense = "ScheduleLicense";
            public static string Timezone = "Timezone";
            public static string SeniorityDate = "SeniorityDate";
            public static string DrawNumber = "DrawNumber";
            public static string EligibleJobs = "EligibleJobs";
            public static string Posting = "Posting";
            public static string PostingEndDate = "PostingEndDate";
            public static string BankVacation = "BankVacation";
            public static string BankOvertime = "BankOvertime";
            public static string SecondaryLabour = "SecondaryLabour";
        }
        #endregion

        #region main
        public REALExportCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user)
        {
            DataBaseCommand command = GetStoredProcCommand("REALexport_report", user.DatabaseName);

            using (IDataReader reader = command.ExecuteReader())
                return MapToREALExportCollection(reader);
        }
        #endregion

        #region data mapping
        protected REALExportCollection MapToREALExportCollection(IDataReader reader)
        {
            REALExportCollection collection = new REALExportCollection();

            while (reader.Read())
                collection.Add(MapToREALExport(reader));

            return collection;
        }
        protected REALExport MapToREALExport(IDataReader reader)
        {
            REALExport item = new REALExport();

            item.EmployeeNumber = (string)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.FirstName = (string)CleanDataValue(reader[ColumnNames.FirstName]);
            item.LastName = (string)CleanDataValue(reader[ColumnNames.LastName]);
            item.HireDate = (DateTime)CleanDataValue(reader[ColumnNames.HireDate]);
            item.Username = (string)CleanDataValue(reader[ColumnNames.Username]);
            item.EmployeeStatus = (string)CleanDataValue(reader[ColumnNames.EmployeeStatus]);
            item.EmployeeStatusDate = (DateTime)CleanDataValue(reader[ColumnNames.EmployeeStatusDate]);
            item.BirthDate = (DateTime?)CleanDataValue(reader[ColumnNames.BirthDate]);
            item.StandardHoursPerDay = (decimal)CleanDataValue(reader[ColumnNames.StandardHoursPerDay]);
            item.Address = (string)CleanDataValue(reader[ColumnNames.Address]);
            item.City = (string)CleanDataValue(reader[ColumnNames.City]);
            item.Province = (string)CleanDataValue(reader[ColumnNames.Province]);
            item.Zip = (string)CleanDataValue(reader[ColumnNames.Zip]);
            item.Country = (string)CleanDataValue(reader[ColumnNames.Country]);
            item.Phone1 = (string)CleanDataValue(reader[ColumnNames.Phone1]);
            item.Phone2 = (string)CleanDataValue(reader[ColumnNames.Phone2]);
            item.Phone3 = (string)CleanDataValue(reader[ColumnNames.Phone3]);
            item.Email = (string)CleanDataValue(reader[ColumnNames.Email]);

            if (CleanDataValue(reader[ColumnNames.UnionCode]) != null)
                item.UnionCode = CleanDataValue(reader[ColumnNames.UnionCode]).ToString();
            else
                item.UnionCode = (string)CleanDataValue(reader[ColumnNames.UnionCode]);

            item.PayCycle = (decimal)CleanDataValue(reader[ColumnNames.PayCycle]);
            item.HourlySalary = (string)CleanDataValue(reader[ColumnNames.HourlySalary]);
            item.BaseRateOfPay = (decimal?)CleanDataValue(reader[ColumnNames.BaseRateOfPay]);
            item.BaseWage = (DateTime)CleanDataValue(reader[ColumnNames.BaseWage]);
            item.PositionChangeDate = (DateTime)CleanDataValue(reader[ColumnNames.PositionChangeDate]);
            item.LL1 = (string)CleanDataValue(reader[ColumnNames.LL1]);
            item.LL2 = (string)CleanDataValue(reader[ColumnNames.LL2]);
            item.LL3 = (string)CleanDataValue(reader[ColumnNames.LL3]);
            item.LL4 = (string)CleanDataValue(reader[ColumnNames.LL4]);
            item.LL5 = (string)CleanDataValue(reader[ColumnNames.LL5]);
            item.LL6 = (string)CleanDataValue(reader[ColumnNames.LL6]);
            item.LL7 = (string)CleanDataValue(reader[ColumnNames.LL7]);
            item.LL1Desc = (string)CleanDataValue(reader[ColumnNames.LL1Desc]);
            item.LL2Desc = (string)CleanDataValue(reader[ColumnNames.LL2Desc]);
            item.LL3Desc = (string)CleanDataValue(reader[ColumnNames.LL3Desc]);
            item.LL4Desc = (string)CleanDataValue(reader[ColumnNames.LL4Desc]);
            item.LL5Desc = (string)CleanDataValue(reader[ColumnNames.LL5Desc]);
            item.LL6Desc = (string)CleanDataValue(reader[ColumnNames.LL6Desc]);
            item.LL7Desc = (string)CleanDataValue(reader[ColumnNames.LL7Desc]);
            item.ReportsTo = (string)CleanDataValue(reader[ColumnNames.ReportsTo]);
            item.WorkerType = (string)CleanDataValue(reader[ColumnNames.WorkerType]);
            item.BadgeNumber = (string)CleanDataValue(reader[ColumnNames.BadgeNumber]);
            item.MobileLicense = (string)CleanDataValue(reader[ColumnNames.MobileLicense]);
            item.ManagerLicense = (string)CleanDataValue(reader[ColumnNames.ManagerLicense]);
            item.ScheduleLicense = (string)CleanDataValue(reader[ColumnNames.ScheduleLicense]);
            item.Timezone = (string)CleanDataValue(reader[ColumnNames.Timezone]);
            item.SeniorityDate = (DateTime?)CleanDataValue(reader[ColumnNames.SeniorityDate]);
            item.DrawNumber = (string)CleanDataValue(reader[ColumnNames.DrawNumber]);
            item.EligibleJobs = (string)CleanDataValue(reader[ColumnNames.EligibleJobs]);
            item.Posting = (string)CleanDataValue(reader[ColumnNames.Posting]);

            if (CleanDataValue(reader[ColumnNames.PostingEndDate]) != null)
                item.PostingEndDate = Convert.ToDateTime(CleanDataValue(reader[ColumnNames.PostingEndDate]));
            else
                item.PostingEndDate = null;

            item.BankVacation = (string)CleanDataValue(reader[ColumnNames.BankVacation]);
            //item.BankOvertime = (string)CleanDataValue(reader[ColumnNames.BankOvertime]);
            item.SecondaryLabour = (string)CleanDataValue(reader[ColumnNames.SecondaryLabour]);

            return item;
        }
        #endregion
    }
}