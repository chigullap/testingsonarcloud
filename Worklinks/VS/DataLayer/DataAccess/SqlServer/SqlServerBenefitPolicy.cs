﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerBenefitPolicy : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String BenefitPolicyId = "benefit_policy_id";
            public static String EnglishDescription = "english_description";
            public static String FrenchDescription = "french_description";
        }
        #endregion

        #region main
        internal BenefitPolicyCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? policyId)
        {
            DataBaseCommand command = GetStoredProcCommand("BenefitPolicy_select", user.DatabaseName);

            command.AddParameterWithValue("@benefitPolicyId", policyId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToBenefitPolicyCollection(reader);
        }

        public BenefitPolicy Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPolicy benefitPolicy)
        {
            using (DataBaseCommand command = GetStoredProcCommand("BenefitPolicy_insert", user.DatabaseName))
            {
                command.BeginTransaction("BenefitPolicy_insert");

                SqlParameter policyIdParm = command.AddParameterWithValue("@benefitPolicyId", benefitPolicy.BenefitPolicyId, ParameterDirection.Output);
                command.AddParameterWithValue("@englishDescription", benefitPolicy.EnglishDescription);
                command.AddParameterWithValue("@frenchDescription", benefitPolicy.FrenchDescription);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", benefitPolicy.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                benefitPolicy.BenefitPolicyId = Convert.ToInt64(policyIdParm.Value);
                benefitPolicy.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return benefitPolicy;
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPolicy benefitPolicy)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("BenefitPolicy_update", user.DatabaseName))
                {
                    command.BeginTransaction("BenefitPolicy_update");

                    command.AddParameterWithValue("@benefitPolicyId", benefitPolicy.BenefitPolicyId);
                    command.AddParameterWithValue("@englishDescription", benefitPolicy.EnglishDescription);
                    command.AddParameterWithValue("@frenchDescription", benefitPolicy.FrenchDescription);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", benefitPolicy.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    benefitPolicy.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPolicy benefitPolicy)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("BenefitPolicy_delete", user.DatabaseName))
                {
                    command.BeginTransaction("BenefitPolicy_delete");

                    command.AddParameterWithValue("@benefitPolicyId", benefitPolicy.BenefitPolicyId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected BenefitPolicyCollection MapToBenefitPolicyCollection(IDataReader reader)
        {
            BenefitPolicyCollection collection = new BenefitPolicyCollection();

            while (reader.Read())
                collection.Add(MapToBenefitPolicy(reader));

            return collection;
        }

        protected BenefitPolicy MapToBenefitPolicy(IDataReader reader)
        {
            BenefitPolicy item = new BenefitPolicy();
            base.MapToBase(item, reader);

            item.BenefitPolicyId = (long)CleanDataValue(reader[ColumnNames.BenefitPolicyId]);
            item.EnglishDescription = (String)CleanDataValue(reader[ColumnNames.EnglishDescription]);
            item.FrenchDescription = (String)CleanDataValue(reader[ColumnNames.FrenchDescription]);

            return item;
        }
        #endregion
    }
}
