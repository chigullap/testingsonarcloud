﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerAccrualPolicyEntitlement : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String AccrualPolicyEntitlementId = "accrual_policy_entitlement_id";
            public static String AccrualPolicyId = "accrual_policy_id";
            public static String AccrualEntitlementId = "accrual_entitlement_id";
        }
        #endregion

        #region main
        internal AccrualPolicyEntitlementCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long policyId)
        {
            DataBaseCommand command = GetStoredProcCommand("AccrualPolicyEntitlement_select", user.DatabaseName);

            command.AddParameterWithValue("@accrualPolicyId", policyId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToAccrualPolicyEntitlementCollection(reader);
        }
        public AccrualPolicyEntitlement Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualPolicyEntitlement policyEntitlement)
        {
            using (DataBaseCommand command = GetStoredProcCommand("AccrualPolicyEntitlement_insert", user.DatabaseName))
            {
                command.BeginTransaction("AccrualPolicyEntitlement_insert");

                SqlParameter policyEntitlementIdParam = command.AddParameterWithValue("@accrualPolicyEntitlementId", policyEntitlement.AccrualPolicyEntitlementId, ParameterDirection.Output);
                command.AddParameterWithValue("@accrualPolicyId", policyEntitlement.AccrualPolicyId);
                command.AddParameterWithValue("@accrualEntitlementId", policyEntitlement.AccrualEntitlementId);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", policyEntitlement.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                policyEntitlement.AccrualPolicyEntitlementId = Convert.ToInt64(policyEntitlementIdParam.Value);
                policyEntitlement.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return policyEntitlement;
            }
        }
        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualPolicyEntitlement policyEntitlement)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("AccrualPolicyEntitlement_update", user.DatabaseName))
                {
                    command.BeginTransaction("AccrualPolicyEntitlement_update");

                    command.AddParameterWithValue("@accrualPolicyEntitlementId", policyEntitlement.AccrualPolicyEntitlementId);
                    command.AddParameterWithValue("@accrualPolicyId", policyEntitlement.AccrualPolicyId);
                    command.AddParameterWithValue("@accrualEntitlementId", policyEntitlement.AccrualEntitlementId);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", policyEntitlement.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    policyEntitlement.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long policyId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("AccrualPolicyEntitlement_delete", user.DatabaseName))
                {
                    command.BeginTransaction("AccrualPolicyEntitlement_delete");

                    command.AddParameterWithValue("@accrualPolicyId", policyId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected AccrualPolicyEntitlementCollection MapToAccrualPolicyEntitlementCollection(IDataReader reader)
        {
            AccrualPolicyEntitlementCollection collection = new AccrualPolicyEntitlementCollection();

            while (reader.Read())
                collection.Add(MapToAccrualEntitlementDetail(reader));

            return collection;
        }
        protected AccrualPolicyEntitlement MapToAccrualEntitlementDetail(IDataReader reader)
        {
            AccrualPolicyEntitlement item = new AccrualPolicyEntitlement();
            base.MapToBase(item, reader);

            item.AccrualPolicyEntitlementId = (long)CleanDataValue(reader[ColumnNames.AccrualPolicyEntitlementId]);
            item.AccrualPolicyId = (long)CleanDataValue(reader[ColumnNames.AccrualPolicyId]);
            item.AccrualEntitlementId = (long)CleanDataValue(reader[ColumnNames.AccrualEntitlementId]);

            return item;
        }
        #endregion
    }
}