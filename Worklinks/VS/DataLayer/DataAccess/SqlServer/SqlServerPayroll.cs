﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Payroll;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerPayroll : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            //common
            public static String EmployeeId = "employee_id";
            public static String PaycodeCode = "code_paycode_cd";

            //payroll employee position 
            public static String EmployeePaycodeId = "employee_paycode_id";
            public static String AmountRate = "amount_rate";
            public static String YearlyMaximum = "yearly_maximum";
            public static String YearlyMinimumCalculationOffset = "yearly_minimum_calculation_offset";
            public static String AmountUnits = "amount_units";
            public static String AmountPercentage = "amount_percentage";

            //payroll income
            public static String PayrollTransactionId = "payroll_transaction_id";
            public static String Units = "units";
            public static String Rate = "rate";
            public static String FederalTaxAmount = "federal_tax_amount";
            public static String ProvincialTaxAmount = "provincial_tax_amount";

            //ytd 
            public static String Amount = "amount";

        }
        #endregion

        #region main

        internal PayrollEmployeePaycodeCollection SelectPayrollEmployeePaycode (DatabaseUser user, long payrollProcessId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Payroll_PayrollTransaction_getEmployeePaycode", user.DatabaseName))
            {
                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToPayrollEmployeePaycodeCollection(reader);
            }
        }
        internal PayrollTransactionIncomeCollection SelectPayrollTransactionIncome(DatabaseUser user, long payrollProcessId, String payrollProcessGroupCode, String payrollProcessRunTypeCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Payroll_PayrollTransaction_getPayrollTransactionIncome", user.DatabaseName))
            {
                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                command.AddParameterWithValue("@payrollProcessGroupCode", payrollProcessGroupCode);
                command.AddParameterWithValue("@payrollProcessRunTypeCode", payrollProcessRunTypeCode);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToPayrollTransactionIncomeCollection(reader);
            }
        }
        internal PayrollEmployeeYearToDatePaycodeCollection SelectPayrollEmployeeYearToDatePaycode(DatabaseUser user, long payrollProcessId, int periodYear)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Payroll_PayrollTransaction_getPayrollEmployeeYearToDatePaycode", user.DatabaseName))
            {
                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                command.AddParameterWithValue("@periodYear", periodYear);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToPayrollEmployeeYearToDatePaycodeCollection(reader);
            }
        }
        

        #endregion

        #region data mapping
        protected PayrollEmployeePaycodeCollection MapToPayrollEmployeePaycodeCollection(IDataReader reader)
        {
            PayrollEmployeePaycodeCollection collection = new PayrollEmployeePaycodeCollection();

            while (reader.Read())
                collection.Add(MapToPayrollEmployeePaycode(reader));

            return collection;
        }

        protected PayrollTransactionIncomeCollection MapToPayrollTransactionIncomeCollection(IDataReader reader)
        {
            PayrollTransactionIncomeCollection collection = new PayrollTransactionIncomeCollection();

            while (reader.Read())
                collection.Add(MapToPayrollTransactionIncome(reader));

            return collection;
        }

        protected PayrollEmployeeYearToDatePaycodeCollection MapToPayrollEmployeeYearToDatePaycodeCollection(IDataReader reader)
        {
            PayrollEmployeeYearToDatePaycodeCollection collection = new PayrollEmployeeYearToDatePaycodeCollection();

            while (reader.Read())
                collection.Add(MapToPayrollEmployeeYearToDatePaycode(reader));

            return collection;
        }

        protected PayrollEmployeePaycode MapToPayrollEmployeePaycode(IDataReader reader)
        {
            PayrollEmployeePaycode item = new PayrollEmployeePaycode();
            //base.MapToBase(item, reader);

            item.EmployeePaycodeId = (long)CleanDataValue(reader[ColumnNames.EmployeePaycodeId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.AmountRate = (Decimal?)CleanDataValue(reader[ColumnNames.AmountRate]);
            item.YearlyMaximum = (Decimal?)CleanDataValue(reader[ColumnNames.YearlyMaximum]);
            item.YearlyMinimumCalculationOffset = (Decimal?)CleanDataValue(reader[ColumnNames.YearlyMinimumCalculationOffset]);
            item.AmountUnits = (Decimal?)CleanDataValue(reader[ColumnNames.AmountUnits]);
            item.AmountPercentage = (Decimal?)CleanDataValue(reader[ColumnNames.AmountPercentage]);

            return item;
        }

        protected PayrollTransactionIncome MapToPayrollTransactionIncome(IDataReader reader)
        {
            PayrollTransactionIncome item = new PayrollTransactionIncome();
            //base.MapToBase(item, reader);

            item.PayrollTransactionId = (long)CleanDataValue(reader[ColumnNames.PayrollTransactionId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.Units = (Decimal?)CleanDataValue(reader[ColumnNames.Units]);
            item.Rate = (Decimal?)CleanDataValue(reader[ColumnNames.Rate]);
            item.FederalTaxAmount = (Decimal?)CleanDataValue(reader[ColumnNames.FederalTaxAmount]);
            item.ProvincialTaxAmount = (Decimal?)CleanDataValue(reader[ColumnNames.ProvincialTaxAmount]);

            return item;
        }

        protected PayrollEmployeeYearToDatePaycode MapToPayrollEmployeeYearToDatePaycode(IDataReader reader)
        {
            PayrollEmployeeYearToDatePaycode item = new PayrollEmployeeYearToDatePaycode();
            //base.MapToBase(item, reader);

            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.Amount = (Decimal)CleanDataValue(reader[ColumnNames.Amount]);

            return item;
        }
        #endregion
    }
}