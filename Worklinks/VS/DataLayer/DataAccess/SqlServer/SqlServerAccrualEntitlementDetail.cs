﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerAccrualEntitlementDetail : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String AccrualEntitlementDetailId = "accrual_entitlement_detail_id";
            public static String AccrualEntitlementId = "accrual_entitlement_id";
            public static String EffectiveDate = "effective_date";
            public static String PayrollProcessId = "payroll_process_id";
            public static String EmploymentDateCode = "code_employment_date_cd";
            public static String EntitlementRecurrenceCode = "code_entitlement_recurrence_cd";
        }
        #endregion

        #region main
        internal AccrualEntitlementDetailCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long entitlementId, long entitlementDetailId)
        {
            DataBaseCommand command = GetStoredProcCommand("AccrualEntitlementDetail_select", user.DatabaseName);

            command.AddParameterWithValue("@accrualEntitlementId", entitlementId);
            command.AddParameterWithValue("@accrualEntitlementDetailId", entitlementDetailId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToAccrualEntitlementDetailCollection(reader);
        }
        public AccrualEntitlementDetail Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetail entitlementDetail)
        {
            using (DataBaseCommand command = GetStoredProcCommand("AccrualEntitlementDetail_insert", user.DatabaseName))
            {
                command.BeginTransaction("AccrualEntitlementDetail_insert");

                SqlParameter entitlementDetailIdParam = command.AddParameterWithValue("@accrualEntitlementDetailId", entitlementDetail.AccrualEntitlementDetailId, ParameterDirection.Output);
                command.AddParameterWithValue("@accrualEntitlementId", entitlementDetail.AccrualEntitlementId);
                command.AddParameterWithValue("@effectiveDate", entitlementDetail.EffectiveDate);
                command.AddParameterWithValue("@payrollProcessId", entitlementDetail.PayrollProcessId);
                command.AddParameterWithValue("@employmentDateCode", entitlementDetail.EmploymentDateCode);
                command.AddParameterWithValue("@entitlementRecurrenceCode", entitlementDetail.EntitlementRecurrenceCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", entitlementDetail.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                entitlementDetail.AccrualEntitlementDetailId = Convert.ToInt64(entitlementDetailIdParam.Value);
                entitlementDetail.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return entitlementDetail;
            }
        }
        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetail entitlementDetail)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("AccrualEntitlementDetail_update", user.DatabaseName))
                {
                    command.BeginTransaction("AccrualEntitlementDetail_update");

                    command.AddParameterWithValue("@accrualEntitlementDetailId", entitlementDetail.AccrualEntitlementDetailId);
                    command.AddParameterWithValue("@accrualEntitlementId", entitlementDetail.AccrualEntitlementId);
                    command.AddParameterWithValue("@effectiveDate", entitlementDetail.EffectiveDate);
                    command.AddParameterWithValue("@payrollProcessId", entitlementDetail.PayrollProcessId);
                    command.AddParameterWithValue("@employmentDateCode", entitlementDetail.EmploymentDateCode);
                    command.AddParameterWithValue("@entitlementRecurrenceCode", entitlementDetail.EntitlementRecurrenceCode);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", entitlementDetail.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    entitlementDetail.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetail entitlementDetail)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("AccrualEntitlementDetail_delete", user.DatabaseName))
                {
                    command.BeginTransaction("AccrualEntitlementDetail_delete");

                    command.AddParameterWithValue("@accrualEntitlementDetailId", entitlementDetail.AccrualEntitlementDetailId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected AccrualEntitlementDetailCollection MapToAccrualEntitlementDetailCollection(IDataReader reader)
        {
            AccrualEntitlementDetailCollection collection = new AccrualEntitlementDetailCollection();

            while (reader.Read())
                collection.Add(MapToAccrualEntitlementDetail(reader));

            return collection;
        }
        protected AccrualEntitlementDetail MapToAccrualEntitlementDetail(IDataReader reader)
        {
            AccrualEntitlementDetail item = new AccrualEntitlementDetail();
            base.MapToBase(item, reader);

            item.AccrualEntitlementDetailId = (long)CleanDataValue(reader[ColumnNames.AccrualEntitlementDetailId]);
            item.AccrualEntitlementId = (long)CleanDataValue(reader[ColumnNames.AccrualEntitlementId]);
            item.EffectiveDate = (DateTime?)CleanDataValue(reader[ColumnNames.EffectiveDate]);
            item.PayrollProcessId = (long?)CleanDataValue(reader[ColumnNames.PayrollProcessId]);
            item.EmploymentDateCode = (String)CleanDataValue(reader[ColumnNames.EmploymentDateCode]);
            item.EntitlementRecurrenceCode = (String)CleanDataValue(reader[ColumnNames.EntitlementRecurrenceCode]);

            return item;
        }
        #endregion
    }
}