﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerRoleDescription : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String RoleId = "role_id";
            public static String SystemRoleId = "system_role_id";
            public static String EnglishDesc = "english_description";
            public static String FrenchDescription = "french_description";
            public static String RoleTypeSecurityRoleId = "role_type_security_role_id";
            public static String WorklinksAdministrationFlag = "worklinks_administration_flag";
        }
        #endregion

        #region select
        internal RoleDescriptionCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String criteria, long? roleId, bool? worklinksAdministrationFlag)
        {
            DataBaseCommand command = GetStoredProcCommand("RoleDescription_select", user.DatabaseName);

            command.AddParameterWithValue("@roleDescription", criteria = criteria == "" ? null : criteria); //if criteria is "", then make it null for the proc, otherwise leave it alone
            command.AddParameterWithValue("@roleId", roleId);
            command.AddParameterWithValue("@worklinksAdministrationFlag", worklinksAdministrationFlag);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToRoleDescriptionCollection(reader);
            }
        }

        internal RoleDescriptionCollection SelectType(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String criteria, String securityRoleType, long? securityRoleDescriptionId, bool? worklinksAdministrationFlag)
        {
            DataBaseCommand command = GetStoredProcCommand("RoleDescriptionType_select", user.DatabaseName);

            command.AddParameterWithValue("@RoleDescription", criteria = criteria == "" ? null : criteria); //if criteria is "", then make it null for the proc, otherwise leave it alone
            command.AddParameterWithValue("@CodeLanguageCd", user.LanguageCode);
            command.AddParameterWithValue("@CodeSecurityRoleTypeCd", securityRoleType);
            command.AddParameterWithValue("@SecurityRoleDescriptionId", securityRoleDescriptionId);
            command.AddParameterWithValue("@worklinksAdministrationFlag", worklinksAdministrationFlag);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToRoleDescriptionTypeCollection(reader);
            }
        }
        #endregion

        #region update
        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, RoleDescription roleDesc)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("RoleDescription_update", user.DatabaseName))
                {
                    command.BeginTransaction("RoleDescription_update");

                    command.AddParameterWithValue("@roleId", roleDesc.RoleId);
                    command.AddParameterWithValue("@englishDescription", roleDesc.EnglishDesc);
                    command.AddParameterWithValue("@frenchDescription", roleDesc.FrenchDesc);
                    command.AddParameterWithValue("@roleTypeSecurityRoleId", roleDesc.RoleTypeSecurityRoleId);
                    command.AddParameterWithValue("@worklinksAdministrationFlag", roleDesc.WorklinksAdministrationFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", roleDesc.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    roleDesc.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region insert
        public RoleDescription InsertRoleDescription(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, RoleDescription Role)
        {
            using (DataBaseCommand command = GetStoredProcCommand("RoleDescription_insert", user.DatabaseName))
            {
                command.BeginTransaction("RoleDescription_insert");

                SqlParameter RoleIdParm = command.AddParameterWithValue("@roleId", Role.RoleId, ParameterDirection.Output);
                command.AddParameterWithValue("@englishDescription", Role.EnglishDesc);
                command.AddParameterWithValue("@frenchDescription", Role.FrenchDesc);
                command.AddParameterWithValue("@roleTypeSecurityRoleId", Role.RoleTypeSecurityRoleId);
                command.AddParameterWithValue("@worklinksAdministrationFlag", Role.WorklinksAdministrationFlag);

                //NOTE:  view will automatically insert the "CodeSecurityRoleTypeCd" value of "ROL" (for "Role").

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", Role.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                //retrieve PK
                Role.RoleId = Convert.ToInt64(RoleIdParm.Value);
                Role.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return Role;
            }
        }

        public void UpdateSecurityClientRoleDescription(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String authDatabaseName, String ngSecurityClientId, ISecurityRoleDescription roleDesc)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SecurityClientRole_update", authDatabaseName))
            {
                command.BeginTransaction("SecurityClientRole_update");

                command.AddParameterWithValue("@ngSecurityClientId", ngSecurityClientId);
                command.AddParameterWithValue("@securityClientRoleIdentifier", roleDesc.RoleId);
                command.AddParameterWithValue("@englishDesc", roleDesc.EnglishDesc);
                command.AddParameterWithValue("@frenchDesc", roleDesc.FrenchDesc);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                command.CommitTransaction();
            }
        }
        public void InsertSecurityClientRoleDescription(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String authDatabaseName, String ngSecurityClientId, ISecurityRoleDescription roleDesc)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SecurityClientRole_insert", authDatabaseName))
            {
                command.BeginTransaction("SecurityClientRole_insert");

                command.AddParameterWithValue("@ngSecurityClientId", ngSecurityClientId);
                command.AddParameterWithValue("@securityClientRoleIdentifier", roleDesc.RoleId);
                command.AddParameterWithValue("@englishDesc", roleDesc.EnglishDesc);
                command.AddParameterWithValue("@frenchDesc", roleDesc.FrenchDesc);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                command.CommitTransaction();
            }
        }

        public void UpdateSecurityClientRoleHierarchy(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String authDatabaseName, String ngSecurityClientId, GroupDescription group)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SecurityClientRoleHierarchy_update", authDatabaseName))
            {
                command.BeginTransaction("SecurityClientRoleHierarchy_update");

                command.AddParameterWithValue("@ngSecurityClientId", ngSecurityClientId);
                command.AddParameterWithValue("@rootSecurityClientRoleIdentifier", group.RoleId);

                SqlParameter parmClaims = new SqlParameter();
                parmClaims.ParameterName = "@securityClientRoleHierarchy";
                parmClaims.SqlDbType = SqlDbType.Structured;

                DataTable claims = LoadHierarchy(group);
                parmClaims.Value = claims;
                command.AddParameter(parmClaims);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                command.CommitTransaction();
            }
        }

        private DataTable LoadHierarchy(GroupDescription group)
        {
            DataTable table = new DataTable();
            table.Columns.Add("security_client_role_identifier", typeof(string));
            table.Columns.Add("parent_security_client_role_identifier", typeof(string));


            table.Rows.Add(new Object[] { group.GroupId, null });
            table.Rows.Add(new Object[] { group.GuiSecurityRoleId, group.GroupId });
            table.Rows.Add(new Object[] { group.DatabaseSecurityRoleId, group.GroupId });

            return table;
        }

        #endregion

        #region data mapping
        protected RoleDescriptionCollection MapToRoleDescriptionCollection(IDataReader reader)
        {
            RoleDescriptionCollection collection = new RoleDescriptionCollection();
            while (reader.Read())
            {
                collection.Add(MapToRoleDescription(reader));
            }

            return collection;
        }

        protected RoleDescription MapToRoleDescription(IDataReader reader)
        {
            RoleDescription role = new RoleDescription();
            base.MapToBase(role, reader);

            role.RoleId = (long)CleanDataValue(reader[ColumnNames.RoleId]);
            role.EnglishDesc = (String)CleanDataValue(reader[ColumnNames.EnglishDesc]);
            role.FrenchDesc = (String)CleanDataValue(reader[ColumnNames.FrenchDescription]);
            role.RoleTypeSecurityRoleId = (long?)CleanDataValue(reader[ColumnNames.RoleTypeSecurityRoleId]);
            role.WorklinksAdministrationFlag = (bool)CleanDataValue(reader[ColumnNames.WorklinksAdministrationFlag]);

            return role;
        }

        protected RoleDescriptionCollection MapToRoleDescriptionTypeCollection(IDataReader reader)
        {
            RoleDescriptionCollection collection = new RoleDescriptionCollection();
            while (reader.Read())
            {
                collection.Add(MapToRoleDescriptionType(reader));
            }

            return collection;
        }

        protected RoleDescription MapToRoleDescriptionType(IDataReader reader)
        {
            RoleDescription role = new RoleDescription();
            base.MapToBase(role, reader);

            role.RoleId = (long)CleanDataValue(reader[ColumnNames.RoleId]);
            role.SystemRoleId = (long?)CleanDataValue(reader[ColumnNames.SystemRoleId]);
            role.EnglishDesc = (String)CleanDataValue(reader[ColumnNames.EnglishDesc]);

            return role;
        }
        #endregion
    }
}