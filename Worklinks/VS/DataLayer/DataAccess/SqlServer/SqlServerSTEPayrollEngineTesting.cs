﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerSTEPayrollEngineTesting : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayrollEngineOutputId = "payroll_engine_output_id";
            public static String EmployeeId = "employee_id";
            public static String EmployeeCPP = "employee_cpp";
            public static String EmployeeEI = "GL employee_ei";
            public static String EmployerEI = "employer_ei";
            public static String FederalTax = "federal_tax";
            public static String ProvincialTax = "provincial_tax";
            public static String LumpSumTax = "lump_sum_tax";
            public static String CombinedTaxes = "combined_taxes";
            public static String BonusTaxFederal = "bonus_tax_federal";
            public static String BonusTaxProvincial = "bonus_tax_provincial";
            public static String BonusEI = "bonus_ei";
            public static String BonusCPP = "bonus_cpp";
            public static String ElapsedTimeInMilliSeconds = "elapsed_time_in_milliseconds";
            public static String QuebecProvincialTax = "quebec_provincial_tax";
            public static String QuebecHeathContribution = "quebec_heath_contribution";
        }
        #endregion

        #region main
        public void InsertPayrollEngineOutput(String databaseName, PayrollEngineOutput payEngineOutput)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollEngineOutput_insert", databaseName))
            {
                command.BeginTransaction("PayrollEngineOutput_insert");

                SqlParameter payrollEngineOutputIdParm = command.AddParameterWithValue("@payrollEngineOutputId", payEngineOutput.PayrollEngineOutputId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", payEngineOutput.EmployeeId);
                command.AddParameterWithValue("@employeeCpp", payEngineOutput.EmployeeCPP);
                command.AddParameterWithValue("@employeeQpp", payEngineOutput.EmployeeQPP);
                command.AddParameterWithValue("@employerCppQpp", payEngineOutput.EmployerCppQpp);
                command.AddParameterWithValue("@employeeEi", payEngineOutput.EmployeeEI);
                command.AddParameterWithValue("@employerEi", payEngineOutput.EmployerEI);
                command.AddParameterWithValue("@employeeQpip", payEngineOutput.EmployeeQpip);
                command.AddParameterWithValue("@employerQpip", payEngineOutput.EmployerQpip);
                command.AddParameterWithValue("@federalTax", payEngineOutput.FederalTax);
                command.AddParameterWithValue("@provincialTax", payEngineOutput.ProvincialTax);
                command.AddParameterWithValue("@quebecProvincialTax", payEngineOutput.QuebecProvincialTax);
                command.AddParameterWithValue("@quebecHeathContribution", payEngineOutput.QuebecHeathContribution);
                command.AddParameterWithValue("@lumpSumTax", payEngineOutput.LumpSumTax);
                command.AddParameterWithValue("@combinedTaxes", payEngineOutput.CombinedTaxes);
                command.AddParameterWithValue("@bonusTaxFederal", payEngineOutput.BonusTaxFederal);
                command.AddParameterWithValue("@bonusTaxProvincial", payEngineOutput.BonusTaxProvincial);
                command.AddParameterWithValue("@bonusEi", payEngineOutput.BonusEI);
                command.AddParameterWithValue("@bonusCpp", payEngineOutput.BonusCPP);
                command.AddParameterWithValue("@elapsedTimeInMilliseconds", payEngineOutput.ElapsedTimeInMilliSeconds);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", payEngineOutput.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", payEngineOutput.UpdateUser);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                payEngineOutput.PayrollEngineOutputId = Convert.ToInt64(payrollEngineOutputIdParm.Value);
                payEngineOutput.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();
            }
        }
        #endregion
    }
}
