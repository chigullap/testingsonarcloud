﻿using System;
using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerImportExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ImportExportId = "import_export_id";
            public static String XsdBusinessObject = "xsd_business_object";
            public static String LanguageCode = "code_language_cd";
            public static String Description = "description";
            public static String Label = "label";
            public static String PrependHeaderValue = "prepend_header_value";
            public static String ActiveFlag = "active_flag";
            public static string CanConvertXlsxToCsv = "can_convert_xlsx_to_csv_flag";
            public static string RemoveByteOrderMark = "remove_byte_order_mark";
        }
        #endregion

        #region main
        internal ImportExportSummaryCollection SelectImportExportSummary(DatabaseUser user)
        {
            DataBaseCommand command = GetStoredProcCommand("ImportExport_report", user.DatabaseName);

            command.AddParameterWithValue("@languageCode", user.LanguageCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToImportExportSummaryCollection(reader);
        }
        internal ImportExportCollection Select(DatabaseUser user, long? importExportId, String label)
        {
            DataBaseCommand command = GetStoredProcCommand("ImportExport_select", user.DatabaseName);

            command.AddParameterWithValue("@importExportId", importExportId);
            command.AddParameterWithValue("@importExportLabel", label);
            command.AddParameterWithValue("@languageCode", user.LanguageCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToImportExportCollection(reader);
        }
        #endregion

        #region data mapping
        protected ImportExportCollection MapToImportExportCollection(IDataReader reader)
        {
            ImportExportCollection collection = new ImportExportCollection();

            while (reader.Read())
                collection.Add(MapToImportExport(reader));

            return collection;
        }
        protected ImportExportSummaryCollection MapToImportExportSummaryCollection(IDataReader reader)
        {
            ImportExportSummaryCollection collection = new ImportExportSummaryCollection();

            while (reader.Read())
                collection.Add(MapToImportExportSummary(reader));

            return collection;
        }
        protected ImportExport MapToImportExport(IDataReader reader)
        {
            ImportExport item = new ImportExport();
            base.MapToBase(item, reader);

            item.ImportExportId = (long)CleanDataValue(reader[ColumnNames.ImportExportId]);
            item.XsdBusinessObject = (String)CleanDataValue(reader[ColumnNames.XsdBusinessObject]);
            item.Label = (String)CleanDataValue(reader[ColumnNames.Label]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            item.PrependHeaderValue = (String)CleanDataValue(reader[ColumnNames.PrependHeaderValue]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.CanConvertXlsxToCsv = (bool) CleanDataValue(reader[ColumnNames.CanConvertXlsxToCsv]);
            item.RemoveByteOrderMark = (bool) CleanDataValue(reader[ColumnNames.RemoveByteOrderMark]);

            return item;
        }
        protected ImportExportSummary MapToImportExportSummary(IDataReader reader)
        {
            ImportExportSummary item = new ImportExportSummary();
            base.MapToBase(item, reader);

            item.ImportExportId = (long)CleanDataValue(reader[ColumnNames.ImportExportId]);
            item.LanguageCode = (String)CleanDataValue(reader[ColumnNames.LanguageCode]);
            item.Label = (String)CleanDataValue(reader[ColumnNames.Label]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.CanConvertXlsxToCsv = (bool)CleanDataValue(reader[ColumnNames.CanConvertXlsxToCsv]);
            item.RemoveByteOrderMark = (bool) CleanDataValue(reader[ColumnNames.RemoveByteOrderMark]);

            return item;
        }
        #endregion
    }
}