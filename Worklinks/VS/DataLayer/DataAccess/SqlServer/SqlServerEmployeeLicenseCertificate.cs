﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeLicenseCertificate : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeLicenseCertificateId = "employee_license_certificate_id";
            public static String EmployeeId = "employee_id";
            public static String LicenseCertificateCode = "code_employee_license_certificate_cd";
            public static String EmployeeLicenseCertificateNumber = "employee_license_certificate_number";
            public static String DateIssued = "date_issued";
            public static String ExpiryDate = "expiry_date";
            public static String CostEmployee = "cost_employee";
            public static String CostEmployer = "cost_employer";
            public static String JobRelated = "job_related";
            public static String JobRequired = "job_required";
            public static String AttachmentId = "attachment_id";
        }
        #endregion

        #region main
        internal EmployeeLicenseCertificateCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeeId)
        {
            return Select(user, employeeId, null);
        }

        internal EmployeeLicenseCertificateCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeeId, long? employeeLicenseCertificateId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeLicenseCertificate_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@employeeId", employeeId);
                command.AddParameterWithValue("@employeeLicenseCertificateId", employeeLicenseCertificateId);
                command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
                command.AddParameterWithValue("@securityUserId", user.SecurityUserId);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToEmployeeLicenseCertificateCollection(reader);
                }
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeLicenseCertificate licenseCertificate)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeLicenseCertificate_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeLicenseCertificate_update");

                    command.AddParameterWithValue("@employeeLicenseCertificateId", licenseCertificate.EmployeeLicenseCertificateId);
                    command.AddParameterWithValue("@employeeId", licenseCertificate.EmployeeId);
                    command.AddParameterWithValue("@codeEmployeeLicenseCertificateCd", licenseCertificate.EmployeeLicenseCertificateCode);
                    command.AddParameterWithValue("@employeeLicenseCertificateNumber", licenseCertificate.EmployeeLicenseCertificateNumber);
                    command.AddParameterWithValue("@dateIssued", licenseCertificate.DateIssued);
                    command.AddParameterWithValue("@expiryDate", licenseCertificate.ExpiryDate);
                    command.AddParameterWithValue("@jobRelated", licenseCertificate.JobRelated);
                    command.AddParameterWithValue("@jobRequired", licenseCertificate.JobRequired);
                    command.AddParameterWithValue("@costEmployee", licenseCertificate.CostEmployee);
                    command.AddParameterWithValue("@costEmployer", licenseCertificate.CostEmployer);
                    command.AddParameterWithValue("@attachmentId", licenseCertificate.AttachmentId);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", licenseCertificate.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    licenseCertificate.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public EmployeeLicenseCertificate Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeLicenseCertificate licenseCertificate)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeLicenseCertificate_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeLicenseCertificate_insert");

                SqlParameter employeeLicenseCertificateIdParm = command.AddParameterWithValue("@employeeLicenseCertificateId", licenseCertificate.EmployeeLicenseCertificateId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", licenseCertificate.EmployeeId);
                command.AddParameterWithValue("@codeEmployeeLicenseCertificateCd", licenseCertificate.EmployeeLicenseCertificateCode);
                command.AddParameterWithValue("@employeeLicenseCertificateNumber", licenseCertificate.EmployeeLicenseCertificateNumber);
                command.AddParameterWithValue("@dateIssued", licenseCertificate.DateIssued);
                command.AddParameterWithValue("@expiryDate", licenseCertificate.ExpiryDate);
                command.AddParameterWithValue("@jobRelated", licenseCertificate.JobRelated);
                command.AddParameterWithValue("@jobRequired", licenseCertificate.JobRequired);
                command.AddParameterWithValue("@costEmployee", licenseCertificate.CostEmployee);
                command.AddParameterWithValue("@costEmployer", licenseCertificate.CostEmployer);
                command.AddParameterWithValue("@attachmentId", licenseCertificate.AttachmentId);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", licenseCertificate.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                licenseCertificate.EmployeeLicenseCertificateId = Convert.ToInt64(employeeLicenseCertificateIdParm.Value);
                licenseCertificate.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();

                return licenseCertificate;
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeLicenseCertificate licenseCertificate)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeLicenseCertificate_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeLicenseCertificate_delete");
                    command.AddParameterWithValue("@employeeLicenseCertificateId", licenseCertificate.EmployeeLicenseCertificateId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeLicenseCertificateCollection MapToEmployeeLicenseCertificateCollection(IDataReader reader)
        {
            EmployeeLicenseCertificateCollection collection = new EmployeeLicenseCertificateCollection();
            while (reader.Read())
            {
                collection.Add(MapToEmployeeLicenseCertificate(reader));
            }

            return collection;
        }

        protected EmployeeLicenseCertificate MapToEmployeeLicenseCertificate(IDataReader reader)
        {
            EmployeeLicenseCertificate licenseCertificate = new EmployeeLicenseCertificate();
            base.MapToBase(licenseCertificate, reader);

            licenseCertificate.EmployeeLicenseCertificateId = (long)CleanDataValue(reader[ColumnNames.EmployeeLicenseCertificateId]);
            licenseCertificate.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            licenseCertificate.EmployeeLicenseCertificateCode = (String)CleanDataValue(reader[ColumnNames.LicenseCertificateCode]);
            licenseCertificate.EmployeeLicenseCertificateNumber = (String)CleanDataValue(reader[ColumnNames.EmployeeLicenseCertificateNumber]);
            licenseCertificate.DateIssued = (DateTime?)CleanDataValue(reader[ColumnNames.DateIssued]);
            licenseCertificate.ExpiryDate = (DateTime?)CleanDataValue(reader[ColumnNames.ExpiryDate]);
            licenseCertificate.JobRelated = (Boolean?)CleanDataValue(reader[ColumnNames.JobRelated]);
            licenseCertificate.JobRequired = (Boolean?)CleanDataValue(reader[ColumnNames.JobRequired]);
            licenseCertificate.CostEmployee = (Decimal?)CleanDataValue(reader[ColumnNames.CostEmployee]);
            licenseCertificate.CostEmployer = (Decimal?)CleanDataValue(reader[ColumnNames.CostEmployer]);
            licenseCertificate.AttachmentId = (long?)CleanDataValue(reader[ColumnNames.AttachmentId]);

            return licenseCertificate;
        }
        #endregion
    }
}