﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerCanadaRevenueAgencyT619 : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String CanadaRevenueAgencyT619Id = "canada_revenue_agency_t619_id";
            public static String TransmitterNumber = "transmitter_number";
            public static String TransmitterName = "transmitter_name";
            public static String CanadaRevenueAgencyT619TransmitterTypeCode = "code_canada_revenue_agency_t619_transmitter_type_cd";
            public static String LanguageCode = "code_language_cd";
            public static String TransmitterPersonId = "transmitter_person_id";
        }
        #endregion

        #region main
        internal CanadaRevenueAgencyT619Collection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long canadaRevenueAgencyT619Id)
        {
            DataBaseCommand command = GetStoredProcCommand("CanadaRevenueAgencyT619_select", user.DatabaseName);
            command.AddParameterWithValue("@canadaRevenueAgencyT619Id", canadaRevenueAgencyT619Id);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToCanadaRevenueAgencyT619Collection(reader);
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CanadaRevenueAgencyT619 t619)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CanadaRevenueAgencyT619_update", user.DatabaseName))
                {
                    command.BeginTransaction("CanadaRevenueAgencyT619_upate");

                    command.AddParameterWithValue("@canadaRevenueAgencyT619Id", t619.CanadaRevenueAgencyT619Id);
                    command.AddParameterWithValue("@transmitterNumber", t619.TransmitterNumber);
                    command.AddParameterWithValue("@transmitterName", t619.TransmitterName);
                    command.AddParameterWithValue("@canadaRevenueAgencyT619TransmitterTypeCode", t619.CanadaRevenueAgencyT619TransmitterTypeCode);
                    command.AddParameterWithValue("@languageCode", t619.LanguageCode);
                    command.AddParameterWithValue("@transmitterPersonId", t619.TransmitterPersonId);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", t619.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    t619.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CanadaRevenueAgencyT619 t619)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CanadaRevenueAgencyT619_insert", user.DatabaseName))
            {
                command.BeginTransaction("CanadaRevenueAgencyT619_insert");

                SqlParameter idParm = command.AddParameterWithValue("@canadaRevenueAgencyT619Id", t619.CanadaRevenueAgencyT619Id, ParameterDirection.Output);

                command.AddParameterWithValue("@transmitterNumber", t619.TransmitterNumber);
                command.AddParameterWithValue("@transmitterName", t619.TransmitterName);
                command.AddParameterWithValue("@canadaRevenueAgencyT619TransmitterTypeCode", t619.CanadaRevenueAgencyT619TransmitterTypeCode);
                command.AddParameterWithValue("@languageCode", t619.LanguageCode);
                command.AddParameterWithValue("@transmitterPersonId", t619.TransmitterPersonId);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", t619.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                t619.CanadaRevenueAgencyT619Id = Convert.ToInt64(idParm.Value);
                t619.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }

        //public void Delete(YearEnd yearEnd)
        //{
        //    using (DataBaseCommand command = GetStoredProcCommand("YearEnd_delete"))
        //    {
        //        command.BeginTransaction("YearEnd_delete");
        //        command.AddParameterWithValue("@yearEndId", yearEnd.YearEndId);

        //        command.ExecuteNonQuery();
        //        command.CommitTransaction();
        //    }
        //}
        #endregion

        #region data mapping
        protected CanadaRevenueAgencyT619Collection MapToCanadaRevenueAgencyT619Collection(IDataReader reader)
        {
            CanadaRevenueAgencyT619Collection collection = new CanadaRevenueAgencyT619Collection();
            while (reader.Read())
            {
                collection.Add(MapToCanadaRevenueAgencyT619(reader));
            }

            return collection;
        }
        protected CanadaRevenueAgencyT619 MapToCanadaRevenueAgencyT619(IDataReader reader)
        {
            CanadaRevenueAgencyT619 item = new CanadaRevenueAgencyT619();
            base.MapToBase(item, reader);

            item.CanadaRevenueAgencyT619Id = (long)CleanDataValue(reader[ColumnNames.CanadaRevenueAgencyT619Id]);
            item.TransmitterNumber = (String)CleanDataValue(reader[ColumnNames.TransmitterNumber]);
            item.TransmitterName = (String)CleanDataValue(reader[ColumnNames.TransmitterName]);
            item.CanadaRevenueAgencyT619TransmitterTypeCode = (String)CleanDataValue(reader[ColumnNames.CanadaRevenueAgencyT619TransmitterTypeCode]);
            item.LanguageCode = (String)CleanDataValue(reader[ColumnNames.LanguageCode]);
            item.TransmitterPersonId = (long)CleanDataValue(reader[ColumnNames.TransmitterPersonId]);

            return item;
        }
        #endregion
    }
}