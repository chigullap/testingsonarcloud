﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerCodePaycodeIncome : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PaycodeIncomeId = "paycode_income_id";
            public static String PaycodeCode = "code_paycode_cd";
            public static String PaycodeIncomePayTypeCode = "code_paycode_income_pay_type_cd";
            public static String TaxOverrideFlag = "tax_override_flag";
            public static String IncomeTaxFlag = "income_tax_flag";
            public static String CanadaQuebecPensionPlanFlag = "canada_quebec_pension_plan_flag";
            public static String EmploymentInsuranceFlag = "employment_insurance_flag";
            public static String ProvincialParentalInsurancePlanFlag = "provincial_parental_insurance_plan_flag";
            public static String QuebecTaxFlag = "quebec_tax_flag";
            public static String ProvincialHealthTaxFlag = "provincial_health_tax_flag";
            public static String WorkersCompensationBoardFlag = "workers_compensation_board_flag";
            public static String FederalTaxPaycodeCode = "federal_tax_code_paycode_cd";
            public static String QuebecTaxPaycodeCode = "quebec_tax_code_paycode_cd";
            public static String BarbadosNationalInsuranceSchemeFlag = "barbados_national_insurance_scheme_flag";
            public static String SaintLuciaNationalInsuranceCorporationFlag = "saint_lucia_national_insurance_corporation_flag";
            public static String TrinidadNationalInsuranceBoardFlag = "trinidad_national_insurance_board_flag";
            public static String BarbadosIncomeTaxFlag = "barbados_income_tax_flag";
            public static String SaintLuciaIncomeTaxFlag = "saint_lucia_income_tax_flag";
            public static String TrinidadIncomeTaxFlag = "trinidad_income_tax_flag";
            public static String JamaicaNationalInsuranceSchemeFlag = "jamaica_national_insurance_scheme_flag";
            public static String JamaicaNationalHealthTaxFlag = "jamaica_national_health_tax_flag";
            public static String JamaicaIncomeTaxFlag = "jamaica_income_tax_flag";
        }
        #endregion

        #region main
        internal CodePaycodeIncomeCollection Select(DatabaseUser user, String paycodeCode)
        {
            DataBaseCommand command = GetStoredProcCommand("CodePaycodeIncome_select", user.DatabaseName);

            command.AddParameterWithValue("@paycodeCode", paycodeCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCodeCollection(reader);
        }
        public CodePaycodeIncome Insert(DatabaseUser user, CodePaycodeIncome item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CodePaycodeIncome_insert", user.DatabaseName))
            {
                command.BeginTransaction("CodePaycodeIncome_insert");

                SqlParameter paycodeIncomeIdParm = command.AddParameterWithValue("@paycodeIncomeId", item.PaycodeIncomeId, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@paycodeCode", item.PaycodeCode);
                command.AddParameterWithValue("@paycodeIncomePayTypeCode", item.PaycodeIncomePayTypeCode);
                command.AddParameterWithValue("@taxOverrideFlag", item.TaxOverrideFlag);
                command.AddParameterWithValue("@incomeTaxFlag", item.IncomeTaxFlag);
                command.AddParameterWithValue("@canadaQuebecPensionPlanFlag", item.CanadaQuebecPensionPlanFlag);
                command.AddParameterWithValue("@employmentInsuranceFlag", item.EmploymentInsuranceFlag);
                command.AddParameterWithValue("@provincialParentalInsurancePlanFlag", item.ProvincialParentalInsurancePlanFlag);
                command.AddParameterWithValue("@quebecTaxFlag", item.QuebecTaxFlag);
                command.AddParameterWithValue("@provincialHealthTaxFlag", item.ProvincialHealthTaxFlag);
                command.AddParameterWithValue("@workersCompensationBoardFlag", item.WorkersCompensationBoardFlag);
                command.AddParameterWithValue("@federalTaxPaycodeCode", item.FederalTaxPaycodeCode);
                command.AddParameterWithValue("@quebecTaxPaycodeCode", item.QuebecTaxPaycodeCode);
                command.AddParameterWithValue("@barbadosNationalInsuranceSchemeFlag", item.BarbadosNationalInsuranceSchemeFlag);
                command.AddParameterWithValue("@saintLuciaNationalInsuranceCorporationFlag", item.SaintLuciaNationalInsuranceCorporationFlag);
                command.AddParameterWithValue("@trinidadNationalInsuranceBoardFlag", item.TrinidadNationalInsuranceBoardFlag);
                command.AddParameterWithValue("@barbadosIncomeTaxFlag", item.BarbadosIncomeTaxFlag);
                command.AddParameterWithValue("@saintLuciaIncomeTaxFlag", item.SaintLuciaIncomeTaxFlag);
                command.AddParameterWithValue("@trinidadIncomeTaxFlag", item.TrinidadIncomeTaxFlag);
                command.AddParameterWithValue("@jamaicaNationalInsuranceSchemeFlag", item.JamaicaNationalInsuranceSchemeFlag);
                command.AddParameterWithValue("@jamaicaNationalHealthTaxFlag", item.JamaicaNationalHealthTaxFlag);
                command.AddParameterWithValue("@jamaicaIncomeTaxFlag", item.JamaicaIncomeTaxFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.PaycodeIncomeId = (long)paycodeIncomeIdParm.Value;
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        public void Update(DatabaseUser user, CodePaycodeIncome item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CodePaycodeIncome_update", user.DatabaseName))
                {
                    command.BeginTransaction("CodePaycodeIncome_update");

                    command.AddParameterWithValue("@paycodeCode", item.PaycodeCode);
                    command.AddParameterWithValue("@paycodeIncomePayTypeCode", item.PaycodeIncomePayTypeCode);
                    command.AddParameterWithValue("@taxOverrideFlag", item.TaxOverrideFlag);
                    command.AddParameterWithValue("@incomeTaxFlag", item.IncomeTaxFlag);
                    command.AddParameterWithValue("@canadaQuebecPensionPlanFlag", item.CanadaQuebecPensionPlanFlag);
                    command.AddParameterWithValue("@employmentInsuranceFlag", item.EmploymentInsuranceFlag);
                    command.AddParameterWithValue("@provincialParentalInsurancePlanFlag", item.ProvincialParentalInsurancePlanFlag);
                    command.AddParameterWithValue("@quebecTaxFlag", item.QuebecTaxFlag);
                    command.AddParameterWithValue("@provincialHealthTaxFlag", item.ProvincialHealthTaxFlag);
                    command.AddParameterWithValue("@workersCompensationBoardFlag", item.WorkersCompensationBoardFlag);
                    command.AddParameterWithValue("@federalTaxPaycodeCode", item.FederalTaxPaycodeCode);
                    command.AddParameterWithValue("@quebecTaxPaycodeCode", item.QuebecTaxPaycodeCode);
                    command.AddParameterWithValue("@barbadosNationalInsuranceSchemeFlag", item.BarbadosNationalInsuranceSchemeFlag);
                    command.AddParameterWithValue("@saintLuciaNationalInsuranceCorporationFlag", item.SaintLuciaNationalInsuranceCorporationFlag);
                    command.AddParameterWithValue("@trinidadNationalInsuranceBoardFlag", item.TrinidadNationalInsuranceBoardFlag);
                    command.AddParameterWithValue("@barbadosIncomeTaxFlag", item.BarbadosIncomeTaxFlag);
                    command.AddParameterWithValue("@saintLuciaIncomeTaxFlag", item.SaintLuciaIncomeTaxFlag);
                    command.AddParameterWithValue("@trinidadIncomeTaxFlag", item.TrinidadIncomeTaxFlag);
                    command.AddParameterWithValue("@jamaicaNationalInsuranceSchemeFlag", item.JamaicaNationalInsuranceSchemeFlag);
                    command.AddParameterWithValue("@jamaicaNationalHealthTaxFlag", item.JamaicaNationalHealthTaxFlag);
                    command.AddParameterWithValue("@jamaicaIncomeTaxFlag", item.JamaicaIncomeTaxFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, CodePaycode item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CodePaycodeIncome_delete", user.DatabaseName))
                {
                    command.BeginTransaction("CodePaycodeIncome_delete");

                    command.AddParameterWithValue("@paycodeCode", item.PaycodeCode);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected CodePaycodeIncomeCollection MapToCodeCollection(IDataReader reader)
        {
            CodePaycodeIncomeCollection collection = new CodePaycodeIncomeCollection();

            while (reader.Read())
                collection.Add(MapToCode(reader));

            return collection;
        }
        protected CodePaycodeIncome MapToCode(IDataReader reader)
        {
            CodePaycodeIncome item = new CodePaycodeIncome();
            base.MapToBase(item, reader);

            item.PaycodeIncomeId = (long)CleanDataValue(reader[ColumnNames.PaycodeIncomeId]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.PaycodeIncomePayTypeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeIncomePayTypeCode]);
            item.TaxOverrideFlag = (bool)CleanDataValue(reader[ColumnNames.TaxOverrideFlag]);
            item.IncomeTaxFlag = (bool)CleanDataValue(reader[ColumnNames.IncomeTaxFlag]);
            item.CanadaQuebecPensionPlanFlag = (bool)CleanDataValue(reader[ColumnNames.CanadaQuebecPensionPlanFlag]);
            item.EmploymentInsuranceFlag = (bool)CleanDataValue(reader[ColumnNames.EmploymentInsuranceFlag]);
            item.ProvincialParentalInsurancePlanFlag = (bool)CleanDataValue(reader[ColumnNames.ProvincialParentalInsurancePlanFlag]);
            item.QuebecTaxFlag = (bool)CleanDataValue(reader[ColumnNames.QuebecTaxFlag]);
            item.ProvincialHealthTaxFlag = (bool)CleanDataValue(reader[ColumnNames.ProvincialHealthTaxFlag]);
            item.WorkersCompensationBoardFlag = (bool)CleanDataValue(reader[ColumnNames.WorkersCompensationBoardFlag]);
            item.FederalTaxPaycodeCode = (String)CleanDataValue(reader[ColumnNames.FederalTaxPaycodeCode]);
            item.QuebecTaxPaycodeCode = (String)CleanDataValue(reader[ColumnNames.QuebecTaxPaycodeCode]);
            item.BarbadosNationalInsuranceSchemeFlag = (bool)CleanDataValue(reader[ColumnNames.BarbadosNationalInsuranceSchemeFlag]);
            item.SaintLuciaNationalInsuranceCorporationFlag = (bool)CleanDataValue(reader[ColumnNames.SaintLuciaNationalInsuranceCorporationFlag]);
            item.TrinidadNationalInsuranceBoardFlag = (bool)CleanDataValue(reader[ColumnNames.TrinidadNationalInsuranceBoardFlag]);
            item.BarbadosIncomeTaxFlag = (bool)CleanDataValue(reader[ColumnNames.BarbadosIncomeTaxFlag]);
            item.SaintLuciaIncomeTaxFlag = (bool)CleanDataValue(reader[ColumnNames.SaintLuciaIncomeTaxFlag]);
            item.TrinidadIncomeTaxFlag = (bool)CleanDataValue(reader[ColumnNames.TrinidadIncomeTaxFlag]);
            item.JamaicaNationalInsuranceSchemeFlag = (bool)CleanDataValue(reader[ColumnNames.JamaicaNationalInsuranceSchemeFlag]);
            item.JamaicaNationalHealthTaxFlag = (bool)CleanDataValue(reader[ColumnNames.JamaicaNationalHealthTaxFlag]);
            item.JamaicaIncomeTaxFlag = (bool)CleanDataValue(reader[ColumnNames.JamaicaIncomeTaxFlag]);

            return item;
        }
        #endregion 
    }
}