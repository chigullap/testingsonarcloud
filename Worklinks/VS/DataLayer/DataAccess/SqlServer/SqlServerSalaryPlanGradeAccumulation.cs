﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerSalaryPlanGradeAccumulation : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string DummyKey = "row_number";
            public static string EmployeeId = "employee_id";
            public static string SalaryPlanGradeId = "salary_plan_grade_id";
            public static string TotalAmount = "total_amount";
            public static string TotalHours = "total_hours";
        }
        #endregion

        #region main

        public SalaryPlanGradeAccumulationTotalsCollection SelectTotals(DatabaseUser user, long employeeId)
        {
            DataBaseCommand command = GetStoredProcCommand("SalaryPlanGradeTotals_select", user.DatabaseName);

            command.AddParameterWithValue("@employeeId", employeeId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToSalaryPlanGradeAccumulationTotalsCollection(reader);
            }
        }

        #endregion

        #region data mapping
        protected SalaryPlanGradeAccumulationTotalsCollection MapToSalaryPlanGradeAccumulationTotalsCollection(IDataReader reader)
        {
            SalaryPlanGradeAccumulationTotalsCollection collection = new SalaryPlanGradeAccumulationTotalsCollection();

            while (reader.Read())
            {
                collection.Add(MapToSalaryPlanGradeAccumulationTotals(reader));
            }

            return collection;
        }

        protected SalaryPlanGradeAccumulationTotals MapToSalaryPlanGradeAccumulationTotals(IDataReader reader)
        {
            SalaryPlanGradeAccumulationTotals item = new SalaryPlanGradeAccumulationTotals();

            item.DummyKey = (long)CleanDataValue(reader[ColumnNames.DummyKey]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.SalaryPlanGradeId = (long)CleanDataValue(reader[ColumnNames.SalaryPlanGradeId]);
            item.TotalAmount = (decimal?)CleanDataValue(reader[ColumnNames.TotalAmount]);
            item.TotalHours = (decimal?)CleanDataValue(reader[ColumnNames.TotalHours]);

            return item;
        }

        #endregion

        #region insert
        public void Insert(DatabaseUser user, SalaryPlanGradeAccumulationCollection items)
        {
            DataBaseCommand command = GetStoredProcCommand("SalaryPlanGradeAccumulation_insert", user.DatabaseName);

            using (command)
            {
                command.BeginTransaction("SalaryPlanGradeAccumulation_insert");

                //first table parameter
                SqlParameter parmPayrollMaster = new SqlParameter();
                parmPayrollMaster.ParameterName = "@parmSalaryPlanGradeAccumulation";
                parmPayrollMaster.SqlDbType = SqlDbType.Structured;

                DataTable tablePayrollMaster = LoadSalaryPlanGradeAccumulationDataTable(items);
                parmPayrollMaster.Value = tablePayrollMaster;
                command.AddParameter(parmPayrollMaster);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        public DataTable LoadSalaryPlanGradeAccumulationDataTable(SalaryPlanGradeAccumulationCollection collection)
        {

            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("payroll_process_id", typeof(long));
            table.Columns.Add("employee_id", typeof(long));
            table.Columns.Add("salary_plan_grade_id", typeof(long));
            table.Columns.Add("hours", typeof(decimal));
            table.Columns.Add("amount", typeof(decimal));
            table.Columns.Add("create_employee_position_id", typeof(long));

            //loop thru and add values
            for (int i = 0; i < collection.Count; i++)
            {
                table.Rows.Add(new Object[]
                {
                    collection[i].PayrollProcessId,
                    collection[i].EmployeeId,
                    collection[i].SalaryPlanGradeId,
                    collection[i].Hours,
                    collection[i].Amount,
                    collection[i].CreateEmployeePositionId
                });
            }

            return table;

        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SalaryPlanGradeAccumulation_delete", user.DatabaseName))
                {
                    command.BeginTransaction("SalaryPlanGradeAccumulation_delete");
                    command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void DeleteByCreateEmployeePositionId(DatabaseUser user, long createEmployeePositionId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SalaryPlanGradeAccumulation_deleteByCreateEmployeePositionId", user.DatabaseName))
                {
                    command.BeginTransaction("SalaryPlanGradeAccumulation_deleteByCreateEmployeePositionId");
                    command.AddParameterWithValue("@createEmployeePositionId", createEmployeePositionId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion
    }
}