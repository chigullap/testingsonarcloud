﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeePositionSecondary : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeePositionSecondaryId = "employee_position_secondary_id";
            public static String EmployeePositionId = "employee_position_id";
            public static String CompensationAmount = "compensation_amount";
            public static String SalaryPlanId = "salary_plan_id";
            public static String SalaryPlanGradeId = "salary_plan_grade_id";
            public static String SalaryPlanGradeStepId = "salary_plan_grade_step_id";
            public static String OrganizationUnitDescription = "organization_unit_description";
            public static String OrganizationUnit = "organization_unit";
            public static string AccumulatedHours = "accumulated_hours";
        }
        #endregion

        #region main
        internal EmployeePositionSecondaryCollection Select(DatabaseUser user, long? employeePositionSecondaryId, long? employeePositionId, string languageCode, string organizationUnit)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeePositionSecondary_report", user.DatabaseName);

            command.AddParameterWithValue("@employeePositionSecondaryId", employeePositionSecondaryId);
            command.AddParameterWithValue("@employeePositionId", employeePositionId);
            command.AddParameterWithValue("@languageCode", languageCode);
            command.AddParameterWithValue("@organizationUnit", organizationUnit);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCollection(reader);
        }

        public EmployeePositionSecondaryCollection SelectBatchEmployeePositionSecondaryPosition(DatabaseUser user, EmployeePositionCollection positionIds, string languageCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionSecondary_batchSelectByPositionId", user.DatabaseName))
            {
                SqlParameter parmPositionids = new SqlParameter();
                parmPositionids.ParameterName = "@parmPositionIds";
                parmPositionids.SqlDbType = SqlDbType.Structured;

                DataTable tablePositionids = LoadEmployeePositionIdDataTable(positionIds);
                parmPositionids.Value = tablePositionids;
                command.AddParameter(parmPositionids);

                command.AddParameterWithValue("@languageCode", languageCode);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToCollection(reader);
            }
        }
        private DataTable LoadEmployeePositionIdDataTable(EmployeePositionCollection positionIds)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_position_id", typeof(long));

            foreach (EmployeePosition position in positionIds)
            {
                table.Rows.Add(new Object[]
                {
                    position.EmployeePositionId
                });
            }

            return table;
        }

        public EmployeePositionSecondaryCollection SelectBatch(DatabaseUser user, List<string> importExternalIdentifiers)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionSecondary_batchSelect", user.DatabaseName))
            {
                SqlParameter parmSecondary = new SqlParameter();
                parmSecondary.ParameterName = "@parmSecondary";
                parmSecondary.SqlDbType = SqlDbType.Structured;

                DataTable tableSecondary = LoadEmployeePositionSecondaryDataTable(importExternalIdentifiers);
                parmSecondary.Value = tableSecondary;
                command.AddParameter(parmSecondary);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToSecondaryBatchCollection(reader);
            }
        }
        private DataTable LoadEmployeePositionSecondaryDataTable(List<string> importExternalIdentifiers)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("import_external_identifier", typeof(string));

            for (int i = 0; i < importExternalIdentifiers.Count; i++)
            {
                table.Rows.Add(new Object[]
                {
                    importExternalIdentifiers[i]
                });
            }

            return table;
        }

        public EmployeePositionSecondary Insert(DatabaseUser user, EmployeePositionSecondary item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionSecondary_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeePositionSecondary_insert");

                SqlParameter idParm = command.AddParameterWithValue("@employeePositionSecondaryId", item.EmployeePositionSecondaryId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeePositionId", item.EmployeePositionId);
                command.AddParameterWithValue("@compensationAmount", item.CompensationAmount);
                command.AddParameterWithValue("@salaryPlanId", item.SalaryPlanId);
                command.AddParameterWithValue("@salaryPlanGradeId", item.SalaryPlanGradeId);
                command.AddParameterWithValue("@salaryPlanGradeStepId", item.SalaryPlanGradeStepId);
                command.AddParameterWithValue("@organizationUnit", item.OrganizationUnit);
                command.AddParameterWithValue("@organizationalUnitDates", item.OrganizationUnitDates);


                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.EmployeePositionSecondaryId = Convert.ToInt64(idParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        public EmployeePositionSecondary Update(DatabaseUser user, EmployeePositionSecondary item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionSecondary_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePositionSecondary_update");

                    command.AddParameterWithValue("@employeePositionSecondaryId", item.EmployeePositionSecondaryId);
                    command.AddParameterWithValue("@employeePositionId", item.EmployeePositionId);
                    command.AddParameterWithValue("@compensationAmount", item.CompensationAmount);
                    command.AddParameterWithValue("@salaryPlanId", item.SalaryPlanId);
                    command.AddParameterWithValue("@salaryPlanGradeId", item.SalaryPlanGradeId);
                    command.AddParameterWithValue("@salaryPlanGradeStepId", item.SalaryPlanGradeStepId);
                    command.AddParameterWithValue("@organizationUnit", item.OrganizationUnit);
                    command.AddParameterWithValue("@organizationalUnitDates", item.OrganizationUnitDates);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }

            return item;
        }
        public void Delete(DatabaseUser user, EmployeePositionSecondary item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionSecondary_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePositionSecondary_delete");

                    command.AddParameterWithValue("@employeePositionSecondaryId", item.EmployeePositionSecondaryId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeePositionSecondaryCollection MapToCollection(IDataReader reader)
        {
            EmployeePositionSecondaryCollection collection = new EmployeePositionSecondaryCollection();

            while (reader.Read())
                collection.Add(MapToItem(reader));

            return collection;
        }
        protected EmployeePositionSecondary MapToItem(IDataReader reader)
        {
            EmployeePositionSecondary item = new EmployeePositionSecondary();
            base.MapToBase(item, reader);

            item.EmployeePositionSecondaryId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionSecondaryId]);
            item.EmployeePositionId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionId]);
            item.CompensationAmount = (decimal)CleanDataValue(reader[ColumnNames.CompensationAmount]);
            item.SalaryPlanId = (long?)CleanDataValue(reader[ColumnNames.SalaryPlanId]);
            item.SalaryPlanGradeId = (long?)CleanDataValue(reader[ColumnNames.SalaryPlanGradeId]);
            item.SalaryPlanGradeStepId = (long?)CleanDataValue(reader[ColumnNames.SalaryPlanGradeStepId]);
            item.OrganizationUnitDescription = (string)CleanDataValue(reader[ColumnNames.OrganizationUnitDescription]);
            item.OrganizationUnit = (string)CleanDataValue(reader[ColumnNames.OrganizationUnit]);
            item.AccumulatedHours = (decimal)CleanDataValue(reader[ColumnNames.AccumulatedHours]);

            return item;
        }

        protected EmployeePositionSecondaryCollection MapToSecondaryBatchCollection(IDataReader reader)
        {
            EmployeePositionSecondaryCollection collection = new EmployeePositionSecondaryCollection();

            while (reader.Read())
                collection.Add(MapToSecondaryBatch(reader));

            return collection;
        }
        protected EmployeePositionSecondary MapToSecondaryBatch(IDataReader reader)
        {
            EmployeePositionSecondary item = new EmployeePositionSecondary();
            MapToBase(item, reader);

            item.EmployeePositionSecondaryId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionSecondaryId]);
            item.EmployeePositionId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionId]);
            item.CompensationAmount = (decimal)CleanDataValue(reader[ColumnNames.CompensationAmount]);
            item.SalaryPlanId = (long?)CleanDataValue(reader[ColumnNames.SalaryPlanId]);
            item.SalaryPlanGradeId = (long?)CleanDataValue(reader[ColumnNames.SalaryPlanGradeId]);
            item.SalaryPlanGradeStepId = (long?)CleanDataValue(reader[ColumnNames.SalaryPlanGradeStepId]);
            item.OrganizationUnit = (string)CleanDataValue(reader[ColumnNames.OrganizationUnit]);

            return item;
        }

        #endregion
    }
}