﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerEmployerContributionAmount : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployerContributionAmountId = "employer_contribution_amount_id";
            public static String BusinessNumberId = "business_number_id";
            public static String Year = "year";
            public static String EmploymentInsuranceRate = "employment_insurance_rate";
        }
        #endregion


        #region main
        internal EmployerContributionAmountCollection Select(DatabaseUser user, long? employerContributionAmountId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployerContributionAmount_select", user.DatabaseName);

            command.AddParameterWithValue("@employerContributionAmountId", employerContributionAmountId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToEmployerContributionAmountCollection(reader);
        }

        internal EmployerContributionAmount Insert(DatabaseUser user, EmployerContributionAmount employerContributionAmount)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployerContributionAmount_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployerContributionAmount_insert");

                SqlParameter employerContributionAmountIdParm = command.AddParameterWithValue("@employerContributionAmountId", employerContributionAmount.EmployerContributionAmountId, ParameterDirection.Output);
                command.AddParameterWithValue("@businessNumberId", employerContributionAmount.BusinessNumberId);
                command.AddParameterWithValue("@year", employerContributionAmount.Year);
                command.AddParameterWithValue("@employmentInsuranceRate", employerContributionAmount.EmploymentInsuranceRate);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", employerContributionAmount.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                employerContributionAmount.EmployerContributionAmountId = Convert.ToInt64(employerContributionAmountIdParm.Value);
                employerContributionAmount.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return employerContributionAmount;
            }
        }

        internal void Update(DatabaseUser user, EmployerContributionAmount employerContributionAmount)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployerContributionAmount_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployerContributionAmount_update");

                    command.AddParameterWithValue("@employerContributionAmountId", employerContributionAmount.EmployerContributionAmountId);
                    command.AddParameterWithValue("@businessNumberId", employerContributionAmount.BusinessNumberId);
                    command.AddParameterWithValue("@year", employerContributionAmount.Year);
                    command.AddParameterWithValue("@employmentInsuranceRate", employerContributionAmount.EmploymentInsuranceRate);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", employerContributionAmount.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    employerContributionAmount.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        internal void Delete(DatabaseUser user, EmployerContributionAmount employerContributionAmount)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployerContributionAmount_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployerContributionAmount_delete");

                    command.AddParameterWithValue("@employerContributionAmountId", employerContributionAmount.EmployerContributionAmountId);
                    command.AddParameterWithValue("@rowVersion", employerContributionAmount.RowVersion);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion


        #region data mapping
        protected EmployerContributionAmountCollection MapToEmployerContributionAmountCollection(IDataReader reader)
        {
            EmployerContributionAmountCollection collection = new EmployerContributionAmountCollection();

            while (reader.Read())
                collection.Add(MapToEmployerContributionAmount(reader));

            return collection;
        }

        protected EmployerContributionAmount MapToEmployerContributionAmount(IDataReader reader)
        {
            EmployerContributionAmount item = new EmployerContributionAmount();
            base.MapToBase(item, reader);

            item.EmployerContributionAmountId = (long)CleanDataValue(reader[ColumnNames.EmployerContributionAmountId]);
            item.BusinessNumberId = (long)CleanDataValue(reader[ColumnNames.BusinessNumberId]);
            item.Year = (int)CleanDataValue(reader[ColumnNames.Year]);
            item.EmploymentInsuranceRate = (decimal)CleanDataValue(reader[ColumnNames.EmploymentInsuranceRate]);

            return item;
        }
        #endregion
    }
}
