﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerYearEndR2XmlFileToWorklinks : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string YearEndR2Id = "year_end_r2_id";
            public static string XmlSlipNumber = "xml_slip_number";
            public static string PrintedSlipNumber = "printed_slip_number";
            public static string Revision = "revision";
            public static string PreviousRevisionYearEndR2Id = "previous_revision_year_end_r2_id";
            public static string EmployeeId = "employee_id";
            public static string Year = "year";
            public static string R2SourceOfIncomeCode = "code_r2_source_of_income_cd";
            public static string BoxAAmount = "box_a_amount";
            public static string BoxBAmount = "box_b_amount";
            public static string BoxCAmount = "box_c_amount";
            public static string BoxDAmount = "box_d_amount";
            public static string BoxEAmount = "box_e_amount";
            public static string BoxFAmount = "box_f_amount";
            public static string BoxGAmount = "box_g_amount";
            public static string BoxHAmount = "box_h_amount";
            public static string BoxIAmount = "box_i_amount";
            public static string BoxJAmount = "box_j_amount";
            public static string BoxKAmount = "box_k_amount";
            public static string BoxLAmount = "box_l_amount";
            public static string BoxMAmount = "box_m_amount";
            public static string BoxNBeneficiarySocialInsuranceNumber = "box_n_beneficiary_social_insurance_number";
            public static string BoxOAmount = "box_o_amount";
            public static string BoxB1Amount = "box_b_1_amount";
            public static string BoxB2Amount = "box_b_2_amount";
            public static string BoxB3Amount = "box_b_3_amount";
            public static string BoxB4Amount = "box_b_4_amount";
            public static string BoxC1Amount = "box_c_1_amount";
            public static string BoxC2Amount = "box_c_2_amount";
            public static string BoxC3Amount = "box_c_3_amount";
            public static string BoxC4aAmount = "box_c_4a_amount";
            public static string BoxC4bAmount = "box_c_4b_amount";
            public static string BoxC4cAmount = "box_c_4c_amount";
            public static string BoxC4dAmount = "box_c_4d_amount";
            public static string BoxC4eAmount = "box_c_4e_amount";
            public static string BoxC4fAmount = "box_c_4f_amount";
            public static string BoxC6Amount = "box_c_6_amount";
            public static string BoxC7Date = "box_c_7_date";
            public static string BoxC8YearMonth = "box_c_8_year_month";
            public static string BoxC9Amount = "box_c_9_amount";
            public static string BoxC10Date = "box_c_10_date";
            public static string Box201Code= "box_201_code";
            public static string Box210Amount = "box_210_amount";
            public static string Box235Amount = "box_235_amount";
            public static string ActiveFlag = "active_flag";
            public static string R2BoxNumber1 = "R2BoxNumber1";
            public static string R2BoxAmount1 = "R2BoxAmount1";
            public static string R2BoxNumber2 = "R2BoxNumber2";
            public static string R2BoxAmount2 = "R2BoxAmount2";
            public static string R2BoxNumber3 = "R2BoxNumber3";
            public static string R2BoxAmount3 = "R2BoxAmount3";
            public static string R2BoxNumber4 = "R2BoxNumber4";
            public static string R2BoxAmount4 = "R2BoxAmount4";
            public static string PreviousXmlSlipNumber = "previous_xml_slip_number";
            public static string PreviousPrintedSlipNumber = "previous_printed_slip_number";
        }
        #endregion

        #region main
        public YearEndR2Collection Select(DatabaseUser user, int year, bool getOrginals = false, bool getAmended = false)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndR2_select", user.DatabaseName);

            command.AddParameterWithValue("@year", year);
            command.AddParameterWithValue("@getOriginals", getOrginals);
            command.AddParameterWithValue("@getAmended", getAmended);

            using (IDataReader reader = command.ExecuteReader())
                return MapToYearEndR2Collection(reader);
        }
        public YearEndR2Collection SelectSingleR2ByKey(DatabaseUser user, long key)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndR2ByKeyId_select", user.DatabaseName);

            command.AddParameterWithValue("@yearEndR2Id", key);

            using (IDataReader reader = command.ExecuteReader())
                return MapToYearEndR2Collection(reader);
        }
        public YearEndR2 Insert(DatabaseUser user, YearEndR2 yearEndR2Obj)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndR2_Insert", user.DatabaseName);

            using (command)
            {
                command.BeginTransaction("YearEndR2_insert");

                SqlParameter yearEndR2IdParm = command.AddParameterWithValue("@yearEndR2Id", yearEndR2Obj.YearEndR2Id, ParameterDirection.Output);
                command.AddParameterWithValue("@revision", yearEndR2Obj.Revision);
                command.AddParameterWithValue("@previousRevisionYearEndR2Id", yearEndR2Obj.PreviousRevisionYearEndR2Id);
                command.AddParameterWithValue("@employeeId", yearEndR2Obj.EmployeeId);
                command.AddParameterWithValue("@year", yearEndR2Obj.Year);
                command.AddParameterWithValue("@r2SourceOfIncomeCode", yearEndR2Obj.R2SourceOfIncomeCode);
                command.AddParameterWithValue("@boxAAmount", yearEndR2Obj.BoxAAmount);
                command.AddParameterWithValue("@boxBAmount", yearEndR2Obj.BoxBAmount);
                command.AddParameterWithValue("@boxCAmount", yearEndR2Obj.BoxCAmount);
                command.AddParameterWithValue("@boxDAmount", yearEndR2Obj.BoxDAmount);
                command.AddParameterWithValue("@boxEAmount", yearEndR2Obj.BoxEAmount);
                command.AddParameterWithValue("@boxFAmount", yearEndR2Obj.BoxFAmount);
                command.AddParameterWithValue("@boxGAmount", yearEndR2Obj.BoxGAmount);
                command.AddParameterWithValue("@boxHAmount", yearEndR2Obj.BoxHAmount);
                command.AddParameterWithValue("@boxIAmount", yearEndR2Obj.BoxIAmount);
                command.AddParameterWithValue("@boxJAmount", yearEndR2Obj.BoxJAmount);
                command.AddParameterWithValue("@boxKAmount", yearEndR2Obj.BoxKAmount);
                command.AddParameterWithValue("@boxLAmount", yearEndR2Obj.BoxLAmount);
                command.AddParameterWithValue("@boxMAmount", yearEndR2Obj.BoxMAmount);
                command.AddParameterWithValue("@boxNBeneficiarySocialInsuranceNumber", yearEndR2Obj.BoxNBeneficiarySocialInsuranceNumber);
                command.AddParameterWithValue("@boxOAmount", yearEndR2Obj.BoxOAmount);
                command.AddParameterWithValue("@boxB1Amount", yearEndR2Obj.BoxB1Amount);
                command.AddParameterWithValue("@boxB2Amount", yearEndR2Obj.BoxB2Amount);
                command.AddParameterWithValue("@boxB3Amount", yearEndR2Obj.BoxB3Amount);
                command.AddParameterWithValue("@boxB4Amount", yearEndR2Obj.BoxB4Amount);
                command.AddParameterWithValue("@boxC1Amount", yearEndR2Obj.BoxC1Amount);
                command.AddParameterWithValue("@boxC2Amount", yearEndR2Obj.BoxC2Amount);
                command.AddParameterWithValue("@boxC3Amount", yearEndR2Obj.BoxC3Amount);
                command.AddParameterWithValue("@boxC4aAmount", yearEndR2Obj.BoxC4aAmount);
                command.AddParameterWithValue("@boxC4bAmount", yearEndR2Obj.BoxC4bAmount);
                command.AddParameterWithValue("@boxC4cAmount", yearEndR2Obj.BoxC4cAmount);
                command.AddParameterWithValue("@boxC4dAmount", yearEndR2Obj.BoxC4dAmount);
                command.AddParameterWithValue("@boxC4eAmount", yearEndR2Obj.BoxC4eAmount);
                command.AddParameterWithValue("@boxC4fAmount", yearEndR2Obj.BoxC4fAmount);
                command.AddParameterWithValue("@boxC6Amount", yearEndR2Obj.BoxC6Amount);
                command.AddParameterWithValue("@boxC7Date", yearEndR2Obj.BoxC7Date);
                command.AddParameterWithValue("@boxC8YearMonth", yearEndR2Obj.BoxC8YearMonth);
                command.AddParameterWithValue("@boxC9Amount", yearEndR2Obj.BoxC9Amount);
                command.AddParameterWithValue("@boxC10Date", yearEndR2Obj.BoxC10Date);
                command.AddParameterWithValue("@box201Code", yearEndR2Obj.Box201Code);
                command.AddParameterWithValue("@box210Amount", yearEndR2Obj.Box210Amount);
                command.AddParameterWithValue("@box235Amount", yearEndR2Obj.Box235Amount);
                command.AddParameterWithValue("@activeFlag", yearEndR2Obj.ActiveFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", yearEndR2Obj.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                yearEndR2Obj.YearEndR2Id = Convert.ToInt64(yearEndR2IdParm.Value);
                yearEndR2Obj.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return yearEndR2Obj;
            }
        }
        public void Update(DatabaseUser user, long keyId, bool activeFlag)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("YearEndR2_update", user.DatabaseName))
                {
                    command.BeginTransaction("YearEndR2_update");

                    command.AddParameterWithValue("@keyId", keyId);
                    command.AddParameterWithValue("@activeFlag", activeFlag);

                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        
        #endregion

        #region data mapping
        protected YearEndR2Collection MapToYearEndR2Collection(IDataReader reader)
        {
            YearEndR2Collection collection = new YearEndR2Collection();

            while (reader.Read())
                collection.Add(MapToYearEndR2(reader));

            return collection;
        }
        protected YearEndR2 MapToYearEndR2(IDataReader reader)
        {
            YearEndR2 item = new YearEndR2();
            base.MapToBase(item, reader);

            item.YearEndR2Id = (long)CleanDataValue(reader[ColumnNames.YearEndR2Id]);
            item.XmlSlipNumber = (int)(long)CleanDataValue(reader[ColumnNames.XmlSlipNumber]);
            item.PrintedSlipNumber = (int)(long)CleanDataValue(reader[ColumnNames.PrintedSlipNumber]);
            item.Revision = (int)CleanDataValue(reader[ColumnNames.Revision]);
            item.PreviousRevisionYearEndR2Id = (long?)CleanDataValue(reader[ColumnNames.PreviousRevisionYearEndR2Id]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.Year = (decimal)CleanDataValue(reader[ColumnNames.Year]);
            item.R2SourceOfIncomeCode = (string)CleanDataValue(reader[ColumnNames.R2SourceOfIncomeCode]);
            item.BoxAAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxAAmount]);
            item.BoxBAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxBAmount]);
            item.BoxCAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxCAmount]);
            item.BoxDAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxDAmount]);
            item.BoxEAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxEAmount]);
            item.BoxFAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxFAmount]);
            item.BoxGAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxGAmount]);
            item.BoxHAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxHAmount]);
            item.BoxIAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxIAmount]);
            item.BoxJAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxJAmount]);
            item.BoxKAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxKAmount]);
            item.BoxLAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxLAmount]);
            item.BoxMAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxMAmount]);
            item.BoxNBeneficiarySocialInsuranceNumber = (decimal?)CleanDataValue(reader[ColumnNames.BoxNBeneficiarySocialInsuranceNumber]);
            item.BoxOAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxOAmount]);
            item.BoxB1Amount = (decimal)CleanDataValue(reader[ColumnNames.BoxB1Amount]);
            item.BoxB2Amount = (decimal)CleanDataValue(reader[ColumnNames.BoxB2Amount]);
            item.BoxB3Amount = (decimal)CleanDataValue(reader[ColumnNames.BoxB3Amount]);
            item.BoxB4Amount = (decimal)CleanDataValue(reader[ColumnNames.BoxB4Amount]);
            item.BoxC1Amount = (decimal)CleanDataValue(reader[ColumnNames.BoxC1Amount]);
            item.BoxC2Amount = (decimal)CleanDataValue(reader[ColumnNames.BoxC2Amount]);
            item.BoxC3Amount = (decimal)CleanDataValue(reader[ColumnNames.BoxC3Amount]);
            item.BoxC4aAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxC4aAmount]);
            item.BoxC4bAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxC4bAmount]);
            item.BoxC4cAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxC4cAmount]);
            item.BoxC4dAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxC4dAmount]);
            item.BoxC4eAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxC4eAmount]);
            item.BoxC4fAmount = (decimal)CleanDataValue(reader[ColumnNames.BoxC4fAmount]);
            item.BoxC6Amount = (string)CleanDataValue(reader[ColumnNames.BoxC6Amount]);
            item.BoxC7Date = (string)CleanDataValue(reader[ColumnNames.BoxC7Date]);
            item.BoxC8YearMonth = (string)CleanDataValue(reader[ColumnNames.BoxC8YearMonth]);
            item.BoxC9Amount = (decimal)CleanDataValue(reader[ColumnNames.BoxC9Amount]);
            item.BoxC10Date = (string)CleanDataValue(reader[ColumnNames.BoxC10Date]);
            item.Box201Code = (string)CleanDataValue(reader[ColumnNames.Box201Code]);
            item.Box210Amount = (decimal)CleanDataValue(reader[ColumnNames.Box210Amount]);
            item.Box235Amount = (decimal)CleanDataValue(reader[ColumnNames.Box235Amount]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.R2BoxNumber1 = (string)CleanDataValue(reader[ColumnNames.R2BoxNumber1]);
            item.R2BoxAmount1 = (string)CleanDataValue(reader[ColumnNames.R2BoxAmount1]);
            item.R2BoxNumber2 = (string)CleanDataValue(reader[ColumnNames.R2BoxNumber2]);
            item.R2BoxAmount2 = (string)CleanDataValue(reader[ColumnNames.R2BoxAmount2]);
            item.R2BoxNumber3 = (string)CleanDataValue(reader[ColumnNames.R2BoxNumber3]);
            item.R2BoxAmount3 = (string)CleanDataValue(reader[ColumnNames.R2BoxAmount3]);
            item.R2BoxNumber4 = (string)CleanDataValue(reader[ColumnNames.R2BoxNumber4]);
            item.R2BoxAmount4 = (string)CleanDataValue(reader[ColumnNames.R2BoxAmount4]);
            item.PreviousXmlSlipNumber = (int?)(long?)CleanDataValue(reader[ColumnNames.PreviousXmlSlipNumber]);
            item.PreviousPrintedSlipNumber = (int?)(long?)CleanDataValue(reader[ColumnNames.PreviousPrintedSlipNumber]);

            return item;
        }
        #endregion
    }
}