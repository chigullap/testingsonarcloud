﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerOrganizationUnitAssociation : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public const String OrganizationUnitAssociationId = "organization_unit_association_id";
            public const String ParentOrganizationUnitId = "parent_organization_unit_id";
            public const String OrganizationUnitId = "organization_unit_id";
            public const String Description = "description";

            public static String LevelId = "level_id";
            public static String ParentDescription = "parent_description";
            public static String Assigned = "assigned";
            public static String LanguageCode = "code_language_cd";
        }
        #endregion

        #region main
        internal OrganizationUnitAssociationTreeCollection GetOrganizationUnitAssociationTree(DatabaseUser user, String codeLanguageCd, bool? checkActiveFlag)
        {
            using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnitAssociationTree_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@codeLanguageCd", codeLanguageCd);
                command.AddParameterWithValue("@activeFlag", checkActiveFlag);

                OrganizationUnitAssociationTreeCollection collection = null;

                using (IDataReader reader = command.ExecuteReader())
                    collection = MapToOrganizationUnitAssociationTreeCollection(user, reader);

                return collection;
            }
        }
        internal OrganizationUnitAssociationCollection Select(DatabaseUser user, OrganizationUnitCriteria criteria)
        {
            using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnitAssociation_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@parentId", criteria.OrganizationUnitLevelId);
                command.AddParameterWithValue("@languageCode", user.LanguageCode);

                OrganizationUnitAssociationCollection collection = null;

                using (IDataReader reader = command.ExecuteReader())
                    collection = MapToOrganizationUnitAssociationCollection(reader);

                return collection;
            }
        }
        public void Insert(DatabaseUser user, OrganizationUnitAssociation data)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnitAssociation_insert", user.DatabaseName))
                {
                    command.BeginTransaction("OrganizationUnitAssociation_insert");

                    SqlParameter organizationUnitAssociationIdParm = command.AddParameterWithValue("@organizationUnitAssociationId", data.OrganizationUnitAssociationId, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@organizationUnitParentId", data.LevelId);
                    command.AddParameterWithValue("@organizationUnitId", data.OrganizationUnitId);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", data.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    data.OrganizationUnitAssociationId = (long)organizationUnitAssociationIdParm.Value;
                    data.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, OrganizationUnitAssociation data)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnitAssociation_delete", user.DatabaseName))
                {
                    command.BeginTransaction("OrganizationUnitAssociation_delete");

                    command.AddParameterWithValue("@parentOrganizationUnitId", data.LevelId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected OrganizationUnitAssociationTreeCollection MapToOrganizationUnitAssociationTreeCollection(DatabaseUser user, IDataReader reader)
        {
            OrganizationUnitAssociationTreeCollection collection = new OrganizationUnitAssociationTreeCollection();

            while (reader.Read())
                collection.Add(MapToOrganizationUnitAssociationTree(user, reader));

            return collection;
        }
        protected OrganizationUnitAssociationTree MapToOrganizationUnitAssociationTree(DatabaseUser user, IDataReader reader)
        {
            OrganizationUnitAssociationTree item = new OrganizationUnitAssociationTree();
            base.MapToBase(item, reader);

            item.OrganizationUnitAssociationId = (long)CleanDataValue(reader[ColumnNames.OrganizationUnitAssociationId]);
            item.ParentOrganizationUnitId = (long?)CleanDataValue(reader[ColumnNames.ParentOrganizationUnitId]);
            item.OrganizationUnitId = (long)CleanDataValue(reader[ColumnNames.OrganizationUnitId]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);

            return item;
        }
        protected OrganizationUnitAssociationCollection MapToOrganizationUnitAssociationCollection(IDataReader reader)
        {
            OrganizationUnitAssociationCollection collection = new OrganizationUnitAssociationCollection();

            while (reader.Read())
                collection.Add(OrganizationUnitAssociation(reader));

            return collection;
        }
        protected OrganizationUnitAssociation OrganizationUnitAssociation(IDataReader reader)
        {
            OrganizationUnitAssociation item = new OrganizationUnitAssociation();
            base.MapToBase(item, reader);

            if (CleanDataValue(reader[ColumnNames.OrganizationUnitAssociationId]) != null)
                item.OrganizationUnitAssociationId = (long)CleanDataValue(reader[ColumnNames.OrganizationUnitAssociationId]);

            item.OrganizationUnitId = (long)CleanDataValue(reader[ColumnNames.OrganizationUnitId]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            item.LevelId = Convert.ToInt64(CleanDataValue(reader[ColumnNames.LevelId]));
            item.ParentDescription = (String)CleanDataValue(reader[ColumnNames.ParentDescription]);
            item.Assigned = Convert.ToBoolean(CleanDataValue(reader[ColumnNames.Assigned]));
            item.LanguageCode = (String)CleanDataValue(reader[ColumnNames.LanguageCode]);

            return item;
        }
        #endregion
    }
}