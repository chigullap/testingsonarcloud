﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeTerminationOther : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeTerminationOtherId = "employee_termination_other_id";
            public static String EmployeePositionId = "employee_position_id";
            public static String RecommendForRehire = "recommend_for_rehire";
            public static String VoluntaryTermination = "voluntary_termination";
            public static String TerminationLetterReceived = "termination_letter_received";
            public static String WeeksOfTerminationPay = "weeks_of_termination_pay";
            public static String SalaryContinuanceFrom = "salary_continuance_from";
            public static String WeeksOfSeverancePay = "weeks_of_severance_pay";
            public static String SalaryContinuanceTo = "salary_continuance_to";
            public static String VacationPay = "vacation_pay";
            public static String VacationDays = "vacation_days";
            public static String AnticipatedCommission = "anticipated_commission";
            public static String VacationPercent = "vacation_percent";
            public static String AnticipatedBonus = "anticipated_bonus";
            public static String MedicalFlag = "medical_flag";
            public static String DentalFlag = "dental_flag";
            public static String LifeFlag = "life_flag";
            public static String OtherFlag = "other_flag";
            public static String OtherFlagText = "other_flag_text";
            public static String Comments = "comments";
        }
        #endregion

        #region main
        internal EmployeeTerminationOtherCollection Select(DatabaseUser user, long employeePositionId)
        {
            return Select(user, employeePositionId, null);
        }

        internal EmployeeTerminationOtherCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeePositionId, long? employeeTerminationOtherId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeeTerminationOther_select", user.DatabaseName);
            command.AddParameterWithValue("@employeePositionId", employeePositionId);
            command.AddParameterWithValue("@employeeTerminationOtherId", employeeTerminationOtherId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToEmployeeTerminationOtherCollection(reader);
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeTerminationOther other)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeTerminationOther_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeTerminationOther_update");

                    command.AddParameterWithValue("@employeeTerminationOtherId", other.EmployeeTerminationOtherId);
                    command.AddParameterWithValue("@employeePositionId", other.EmployeePositionId);
                    command.AddParameterWithValue("@recommendForRehire", other.RecommendForRehire);
                    command.AddParameterWithValue("@voluntaryTermination", other.VoluntaryTermination);
                    command.AddParameterWithValue("@terminationLetterReceived", other.TerminationLetterReceived);
                    command.AddParameterWithValue("@weeksOfTerminationPay", other.WeeksOfTerminationPay);
                    command.AddParameterWithValue("@salaryContinuanceFrom", other.SalaryContinuanceFrom);

                    command.AddParameterWithValue("@weeksOfSeverancePay", other.WeeksOfSeverancePay);
                    command.AddParameterWithValue("@salaryContinuanceTo", other.SalaryContinuanceTo);
                    command.AddParameterWithValue("@vacationPay", other.VacationPay);
                    command.AddParameterWithValue("@vacationDays", other.VacationDays);
                    command.AddParameterWithValue("@anticipatedCommission", other.AnticipatedCommission);
                    command.AddParameterWithValue("@vacationPercent", other.VacationPercent);
                    command.AddParameterWithValue("@anticipatedBonus", other.AnticipatedBonus);
                    command.AddParameterWithValue("@medicalFlag", other.MedicalFlag);
                    command.AddParameterWithValue("@dentalFlag", other.DentalFlag);
                    command.AddParameterWithValue("@lifeFlag", other.LifeFlag);
                    command.AddParameterWithValue("@otherFlag", other.OtherFlag);
                    command.AddParameterWithValue("@otherFlagText", other.OtherFlagText);
                    command.AddParameterWithValue("@comments", other.Comments);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", other.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    other.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public EmployeeTerminationOther Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeTerminationOther other)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeTerminationOther_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeTerminationOther_insert");

                SqlParameter employeeTerminationOtherIdParm = command.AddParameterWithValue("@employeeTerminationOtherId", other.EmployeeTerminationOtherId, ParameterDirection.Output);

                command.AddParameterWithValue("@employeePositionId", other.EmployeePositionId);
                command.AddParameterWithValue("@recommendForRehire", other.RecommendForRehire);
                command.AddParameterWithValue("@voluntaryTermination", other.VoluntaryTermination);
                command.AddParameterWithValue("@terminationLetterReceived", other.TerminationLetterReceived);
                command.AddParameterWithValue("@weeksOfTerminationPay", other.WeeksOfTerminationPay);
                command.AddParameterWithValue("@salaryContinuanceFrom", other.SalaryContinuanceFrom);

                command.AddParameterWithValue("@weeksOfSeverancePay", other.WeeksOfSeverancePay);
                command.AddParameterWithValue("@salaryContinuanceTo", other.SalaryContinuanceTo);
                command.AddParameterWithValue("@vacationPay", other.VacationPay);
                command.AddParameterWithValue("@vacationDays", other.VacationDays);
                command.AddParameterWithValue("@anticipatedCommission", other.AnticipatedCommission);
                command.AddParameterWithValue("@vacationPercent", other.VacationPercent);
                command.AddParameterWithValue("@anticipatedBonus", other.AnticipatedBonus);
                command.AddParameterWithValue("@medicalFlag", other.MedicalFlag);
                command.AddParameterWithValue("@dentalFlag", other.DentalFlag);
                command.AddParameterWithValue("@lifeFlag", other.LifeFlag);
                command.AddParameterWithValue("@otherFlag", other.OtherFlag);
                command.AddParameterWithValue("@otherFlagText", other.OtherFlagText);
                command.AddParameterWithValue("@comments", other.Comments);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", other.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                other.EmployeeTerminationOtherId = Convert.ToInt64(employeeTerminationOtherIdParm.Value);
                other.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();
                return other;
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeTerminationOther other)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeTerminationOther_delete", user.DatabaseName))
                {

                    command.BeginTransaction("EmployeeTerminationOther_delete");
                    command.AddParameterWithValue("@employeeTerminationOtherId", other.EmployeeTerminationOtherId);
                    command.AddParameterWithValue("@employeePositionId", other.EmployeePositionId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeTerminationOtherCollection MapToEmployeeTerminationOtherCollection(IDataReader reader)
        {
            EmployeeTerminationOtherCollection collection = new EmployeeTerminationOtherCollection();
            while (reader.Read())
            {
                collection.Add(MapToEmployeeTerminationOther(reader));
            }

            return collection;
        }
        protected EmployeeTerminationOther MapToEmployeeTerminationOther(IDataReader reader)
        {
            EmployeeTerminationOther other = new EmployeeTerminationOther();
            base.MapToBase(other, reader);

            other.EmployeeTerminationOtherId = (long)CleanDataValue(reader[ColumnNames.EmployeeTerminationOtherId]);
            other.EmployeePositionId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionId]);
            other.RecommendForRehire = (bool)CleanDataValue(reader[ColumnNames.RecommendForRehire]);
            other.VoluntaryTermination = (bool?)CleanDataValue(reader[ColumnNames.VoluntaryTermination]);
            other.TerminationLetterReceived = (bool?)CleanDataValue(reader[ColumnNames.TerminationLetterReceived]);
            other.WeeksOfTerminationPay = (short?)CleanDataValue(reader[ColumnNames.WeeksOfTerminationPay]);
            other.SalaryContinuanceFrom = (DateTime?)CleanDataValue(reader[ColumnNames.SalaryContinuanceFrom]);
            other.WeeksOfSeverancePay = (short?)CleanDataValue(reader[ColumnNames.WeeksOfSeverancePay]);
            other.SalaryContinuanceTo = (DateTime?)CleanDataValue(reader[ColumnNames.SalaryContinuanceTo]);
            other.VacationPay = (decimal?)CleanDataValue(reader[ColumnNames.VacationPay]);
            other.VacationDays = (short?)CleanDataValue(reader[ColumnNames.VacationDays]);
            other.AnticipatedCommission = (decimal?)CleanDataValue(reader[ColumnNames.AnticipatedCommission]);
            other.VacationPercent = (decimal?)CleanDataValue(reader[ColumnNames.VacationPercent]);
            other.AnticipatedBonus = (decimal?)CleanDataValue(reader[ColumnNames.AnticipatedBonus]);
            other.MedicalFlag = (bool?)CleanDataValue(reader[ColumnNames.MedicalFlag]);
            other.DentalFlag = (bool?)CleanDataValue(reader[ColumnNames.DentalFlag]);
            other.LifeFlag = (bool?)CleanDataValue(reader[ColumnNames.LifeFlag]);
            other.OtherFlag = (bool?)CleanDataValue(reader[ColumnNames.OtherFlag]);
            other.OtherFlagText = (String)CleanDataValue(reader[ColumnNames.OtherFlagText]);
            other.Comments = (String)CleanDataValue(reader[ColumnNames.Comments]);

            return other;
        }
        #endregion
    }
}