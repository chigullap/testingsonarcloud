﻿using System;
using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerLabourCost : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String LabourCostId = "labour_cost_id";
            public static String Month = "month";
            public static String Description = "description";
            public static String Amount = "amount";
        }
        #endregion

        #region main
        internal LabourCostCollection Select(DatabaseUser user, Decimal year)
        {
            DataBaseCommand command = GetStoredProcCommand("LabourCost_select", user.DatabaseName);

            command.AddParameterWithValue("@year", year);
            command.AddParameterWithValue("@languageCode", user.LanguageCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCollection(reader);
        }
        #endregion

        #region data mapping
        protected LabourCostCollection MapToCollection(IDataReader reader)
        {
            LabourCostCollection collection = new LabourCostCollection();

            while (reader.Read())
                collection.Add(MapToItem(reader));

            return collection;
        }
        protected LabourCost MapToItem(IDataReader reader)
        {
            LabourCost item = new LabourCost();

            item.LabourCostId = (long)CleanDataValue(reader[ColumnNames.LabourCostId]);
            item.Month = (Int16)CleanDataValue(reader[ColumnNames.Month]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            item.Amount = (Decimal)CleanDataValue(reader[ColumnNames.Amount]);

            return item;
        }
        #endregion
    }
}