﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using System.Data;
using System.Data.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeVacationHourlyRate : SqlServerBase
    {
        #region column names

        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeVacationHourlyRateId = "employee_vacation_hourly_rate_id";
            public static String AccrualCalculationPeriodStartDate = "accrual_calculation_period_start_date";
            public static String AccrualCalculationPeriodEndDate = "accrual_calculation_period_end_date";
            public static String EmployeeId = "employee_id";
            public static String Rate = "rate";
        }

        #endregion

        #region main

        internal EmployeeVacationHourlyRateCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeeId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeVacationHourlyRate_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@employeeId", employeeId);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToEmployeeVacationHourlyRateCollection(reader);
                }
            }
        }
       
        #endregion

        #region data mapping

        protected EmployeeVacationHourlyRateCollection MapToEmployeeVacationHourlyRateCollection(IDataReader reader)
        {
            EmployeeVacationHourlyRateCollection collection = new EmployeeVacationHourlyRateCollection();
            while (reader.Read())
            {
                collection.Add(MapToEmployeeVacationHourlyRate(reader));
            }

            return collection;
        }

        protected EmployeeVacationHourlyRate MapToEmployeeVacationHourlyRate(IDataReader reader)
        {
            EmployeeVacationHourlyRate employeeVacationHourlyRate = new EmployeeVacationHourlyRate();
            base.MapToBase(employeeVacationHourlyRate, reader);

            employeeVacationHourlyRate.EmployeeVacationHourlyRateId = (long)CleanDataValue(reader[ColumnNames.EmployeeVacationHourlyRateId]);
            employeeVacationHourlyRate.AccrualCalculationPeriodStartDate = (DateTime)CleanDataValue(reader[ColumnNames.AccrualCalculationPeriodStartDate]);
            employeeVacationHourlyRate.AccrualCalculationPeriodEndDate = (DateTime)CleanDataValue(reader[ColumnNames.AccrualCalculationPeriodEndDate]);
            employeeVacationHourlyRate.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            employeeVacationHourlyRate.Rate = (decimal)CleanDataValue(reader[ColumnNames.Rate]);

            return employeeVacationHourlyRate;
        }

        #endregion
    }
}
