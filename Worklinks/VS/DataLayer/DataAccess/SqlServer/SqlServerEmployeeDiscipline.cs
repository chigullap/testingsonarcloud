﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeDiscipline : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {

            public static string EmployeeDisciplineId = "employee_discipline_id";
            public static string EmployeeId = "employee_id";
            public static string ReportedByEmployeeId = "reported_by_employee_id";
            public static string DisciplineCode = "code_discipline_cd";
            public static string DisciplineTypeCode = "code_discipline_type_cd";
            public static string DisciplineDate = "discipline_date";
            public static string ReportedDate = "reported_date";
            public static string ExpiryDate = "expiry_date";
            public static string Note = "note";
            public static string DisciplineResolutionCode = "code_discipline_resolution_cd";
            public static string ResolutionDate = "resolution_date";
            public static string ResolutionNote = "resolution_note";
        }
        #endregion

        #region main
        internal EmployeeDisciplineCollection Select(DatabaseUser user, long employeeId)
        {
            return Select(user, employeeId, null);
        }
        internal EmployeeDisciplineCollection Select(DatabaseUser user, long employeeId, long? employeeDisciplineId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeDiscipline_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@employeeId", employeeId);
                command.AddParameterWithValue("@employeeDisciplineId", employeeDisciplineId);
                command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
                command.AddParameterWithValue("@securityUserId", user.SecurityUserId);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToEmployeeDisciplineCollection(reader);
            }
        }
        public EmployeeDiscipline Insert(DatabaseUser user, EmployeeDiscipline discipline)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeDiscipline_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeDiscipline_insert");

                SqlParameter employeeDisciplineIdParm = command.AddParameterWithValue("@employeeDisciplineId", discipline.EmployeeDisciplineId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", discipline.EmployeeId);
                command.AddParameterWithValue("@reportedByEmployeeId", discipline.ReportedByEmployeeId);
                command.AddParameterWithValue("@disciplineCode", discipline.DisciplineCode);
                command.AddParameterWithValue("@codeDisciplineTypeCd", discipline.DisciplineTypeCode);
                command.AddParameterWithValue("@disciplineDate", discipline.DisciplineDate);
                command.AddParameterWithValue("@reportedDate", discipline.ReportedDate);
                command.AddParameterWithValue("@expiryDate", discipline.ExpiryDate);
                command.AddParameterWithValue("@note", discipline.Note);
                command.AddParameterWithValue("@codeDisciplineResolutionCd", discipline.DisciplineResolutionCode);
                command.AddParameterWithValue("@resolutionDate", discipline.ResolutionDate);
                command.AddParameterWithValue("@resolutionNote", discipline.ResolutionNote);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", discipline.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@createUser", user.UserName);
                command.AddParameterWithValue("@createDatetime", Time);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                discipline.EmployeeDisciplineId = Convert.ToInt64(employeeDisciplineIdParm.Value);
                discipline.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return discipline;
            }
        }
        public void Update(DatabaseUser user, EmployeeDiscipline discipline)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeDiscipline_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeDiscipline_update");

                    command.AddParameterWithValue("@employeeDisciplineId", discipline.EmployeeDisciplineId);
                    command.AddParameterWithValue("@employeeId", discipline.EmployeeId);
                    command.AddParameterWithValue("@reportedByEmployeeId", discipline.ReportedByEmployeeId);
                    command.AddParameterWithValue("@disciplineCode", discipline.DisciplineCode);
                    command.AddParameterWithValue("@codeDisciplineTypeCd", discipline.DisciplineTypeCode);
                    command.AddParameterWithValue("@disciplineDate", discipline.DisciplineDate);
                    command.AddParameterWithValue("@reporteddate", discipline.ReportedDate);
                    command.AddParameterWithValue("@expiryDate", discipline.ExpiryDate);
                    command.AddParameterWithValue("@note", discipline.Note);
                    command.AddParameterWithValue("@codeDisciplineResolutionCd", discipline.DisciplineResolutionCode);
                    command.AddParameterWithValue("@resolutionDate", discipline.ResolutionDate);
                    command.AddParameterWithValue("@resolutionNote", discipline.ResolutionNote);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", discipline.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    discipline.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, EmployeeDiscipline discipline)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeDiscipline_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeDiscipline_delete");

                    command.AddParameterWithValue("@employeeDisciplineId", discipline.EmployeeDisciplineId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeDisciplineCollection MapToEmployeeDisciplineCollection(IDataReader reader)
        {
            EmployeeDisciplineCollection collection = new EmployeeDisciplineCollection();

            while (reader.Read())
                collection.Add(MapToEmployeeDiscipline(reader));

            return collection;
        }
        protected EmployeeDiscipline MapToEmployeeDiscipline(IDataReader reader)
        {
            EmployeeDiscipline discipline = new EmployeeDiscipline();
            base.MapToBase(discipline, reader);

            discipline.EmployeeDisciplineId = (long)CleanDataValue(reader[ColumnNames.EmployeeDisciplineId]);
            discipline.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            discipline.ReportedByEmployeeId = (long?)CleanDataValue(reader[ColumnNames.ReportedByEmployeeId]);
            discipline.DisciplineCode = (string)CleanDataValue(reader[ColumnNames.DisciplineCode]);
            discipline.DisciplineTypeCode = (string)CleanDataValue(reader[ColumnNames.DisciplineTypeCode]);
            discipline.DisciplineDate = (DateTime?)CleanDataValue(reader[ColumnNames.DisciplineDate]);
            discipline.ReportedDate = (DateTime?)CleanDataValue(reader[ColumnNames.ReportedDate]);
            discipline.ExpiryDate = (DateTime?)CleanDataValue(reader[ColumnNames.ExpiryDate]);
            discipline.Note = (string)CleanDataValue(reader[ColumnNames.Note]);
            discipline.DisciplineResolutionCode = (string)CleanDataValue(reader[ColumnNames.DisciplineResolutionCode]);
            discipline.ResolutionDate = (DateTime?)CleanDataValue(reader[ColumnNames.ResolutionDate]);
            discipline.ResolutionNote = (string)CleanDataValue(reader[ColumnNames.ResolutionNote]);

            return discipline;
        }
        #endregion
    }
}