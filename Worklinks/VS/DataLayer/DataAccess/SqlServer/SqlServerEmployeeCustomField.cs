﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeCustomField : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeCustomFieldId = "employee_custom_field_id";
            public static String EmployeeId = "employee_id";
            public static String CustomField01 = "custom_field_01";
            public static String CustomField01DatatypeCode = "custom_field_01_code_datatype_cd";
            public static String CustomField02 = "custom_field_02";
            public static String CustomField02DatatypeCode = "custom_field_02_code_datatype_cd";
            public static String CustomField03 = "custom_field_03";
            public static String CustomField03DatatypeCode = "custom_field_03_code_datatype_cd";
            public static String CustomField04 = "custom_field_04";
            public static String CustomField04DatatypeCode = "custom_field_04_code_datatype_cd";
            public static String CustomField05 = "custom_field_05";
            public static String CustomField05DatatypeCode = "custom_field_05_code_datatype_cd";
            public static String CustomField06 = "custom_field_06";
            public static String CustomField06DatatypeCode = "custom_field_06_code_datatype_cd";
            public static String CustomField07 = "custom_field_07";
            public static String CustomField07DatatypeCode = "custom_field_07_code_datatype_cd";
            public static String CustomField08 = "custom_field_08";
            public static String CustomField08DatatypeCode = "custom_field_08_code_datatype_cd";
            public static String CustomField09 = "custom_field_09";
            public static String CustomField09DatatypeCode = "custom_field_09_code_datatype_cd";
            public static String CustomField10 = "custom_field_10";
            public static String CustomField10DatatypeCode = "custom_field_10_code_datatype_cd";
            public static String CustomField11 = "custom_field_11";
            public static String CustomField11DatatypeCode = "custom_field_11_code_datatype_cd";
            public static String CustomField12 = "custom_field_12";
            public static String CustomField12DatatypeCode = "custom_field_12_code_datatype_cd";
            public static String CustomField13 = "custom_field_13";
            public static String CustomField13DatatypeCode = "custom_field_13_code_datatype_cd";
            public static String CustomField14 = "custom_field_14";
            public static String CustomField14DatatypeCode = "custom_field_14_code_datatype_cd";
            public static String CustomField15 = "custom_field_15";
            public static String CustomField15DatatypeCode = "custom_field_15_code_datatype_cd";
            public static String CustomField16 = "custom_field_16";
            public static String CustomField16DatatypeCode = "custom_field_16_code_datatype_cd";
            public static String CustomField17 = "custom_field_17";
            public static String CustomField17DatatypeCode = "custom_field_17_code_datatype_cd";
            public static String CustomField18 = "custom_field_18";
            public static String CustomField18DatatypeCode = "custom_field_18_code_datatype_cd";
            public static String CustomField19 = "custom_field_19";
            public static String CustomField19DatatypeCode = "custom_field_19_code_datatype_cd";
            public static String CustomField20 = "custom_field_20";
            public static String CustomField20DatatypeCode = "custom_field_20_code_datatype_cd";
            public static String CustomField21 = "custom_field_21";
            public static String CustomField21DatatypeCode = "custom_field_21_code_datatype_cd";
            public static String CustomField22 = "custom_field_22";
            public static String CustomField22DatatypeCode = "custom_field_22_code_datatype_cd";
            public static String CustomField23 = "custom_field_23";
            public static String CustomField23DatatypeCode = "custom_field_23_code_datatype_cd";
            public static String CustomField24 = "custom_field_24";
            public static String CustomField24DatatypeCode = "custom_field_24_code_datatype_cd";
            public static String CustomField25 = "custom_field_25";
            public static String CustomField25DatatypeCode = "custom_field_25_code_datatype_cd";
            public static String CustomField26 = "custom_field_26";
            public static String CustomField26DatatypeCode = "custom_field_26_code_datatype_cd";
            public static String CustomField27 = "custom_field_27";
            public static String CustomField27DatatypeCode = "custom_field_27_code_datatype_cd";
            public static String CustomField28 = "custom_field_28";
            public static String CustomField28DatatypeCode = "custom_field_28_code_datatype_cd";
            public static String CustomField29 = "custom_field_29";
            public static String CustomField29DatatypeCode = "custom_field_29_code_datatype_cd";
            public static String CustomField30 = "custom_field_30";
            public static String CustomField30DatatypeCode = "custom_field_30_code_datatype_cd";
            public static String CustomField31 = "custom_field_31";
            public static String CustomField31DatatypeCode = "custom_field_31_code_datatype_cd";
            public static String CustomField32 = "custom_field_32";
            public static String CustomField32DatatypeCode = "custom_field_32_code_datatype_cd";
        }
        #endregion

        #region main
        internal EmployeeCustomFieldCollection Select(DatabaseUser user, long? employeeId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeeCustomField_select", user.DatabaseName);

            command.AddParameterWithValue("@employeeId", employeeId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToEmployeeCustomFieldCollection(reader);
        }
        public EmployeeCustomField Insert(DatabaseUser user, EmployeeCustomField item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeCustomField_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeCustomField_insert");

                SqlParameter idParm = command.AddParameterWithValue("@employeeCustomFieldId", item.EmployeeCustomFieldId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", item.EmployeeId);
                command.AddParameterWithValue("@customField01", item.CheckCustomFieldDatatype("CustomField01", "BOOL") ? (item.CustomField01 ?? "0") : item.CustomField01);
                command.AddParameterWithValue("@customField02", item.CheckCustomFieldDatatype("CustomField02", "BOOL") ? (item.CustomField02 ?? "0") : item.CustomField02);
                command.AddParameterWithValue("@customField03", item.CheckCustomFieldDatatype("CustomField03", "BOOL") ? (item.CustomField03 ?? "0") : item.CustomField03);
                command.AddParameterWithValue("@customField04", item.CheckCustomFieldDatatype("CustomField04", "BOOL") ? (item.CustomField04 ?? "0") : item.CustomField04);
                command.AddParameterWithValue("@customField05", item.CheckCustomFieldDatatype("CustomField05", "BOOL") ? (item.CustomField05 ?? "0") : item.CustomField05);
                command.AddParameterWithValue("@customField06", item.CheckCustomFieldDatatype("CustomField06", "BOOL") ? (item.CustomField06 ?? "0") : item.CustomField06);
                command.AddParameterWithValue("@customField07", item.CheckCustomFieldDatatype("CustomField07", "BOOL") ? (item.CustomField07 ?? "0") : item.CustomField07);
                command.AddParameterWithValue("@customField08", item.CheckCustomFieldDatatype("CustomField08", "BOOL") ? (item.CustomField08 ?? "0") : item.CustomField08);
                command.AddParameterWithValue("@customField09", item.CheckCustomFieldDatatype("CustomField09", "BOOL") ? (item.CustomField09 ?? "0") : item.CustomField09);
                command.AddParameterWithValue("@customField10", item.CheckCustomFieldDatatype("CustomField10", "BOOL") ? (item.CustomField10 ?? "0") : item.CustomField10);
                command.AddParameterWithValue("@customField11", item.CheckCustomFieldDatatype("CustomField11", "BOOL") ? (item.CustomField11 ?? "0") : item.CustomField11);
                command.AddParameterWithValue("@customField12", item.CheckCustomFieldDatatype("CustomField12", "BOOL") ? (item.CustomField12 ?? "0") : item.CustomField12);
                command.AddParameterWithValue("@customField13", item.CheckCustomFieldDatatype("CustomField13", "BOOL") ? (item.CustomField13 ?? "0") : item.CustomField13);
                command.AddParameterWithValue("@customField14", item.CheckCustomFieldDatatype("CustomField14", "BOOL") ? (item.CustomField14 ?? "0") : item.CustomField14);
                command.AddParameterWithValue("@customField15", item.CheckCustomFieldDatatype("CustomField15", "BOOL") ? (item.CustomField15 ?? "0") : item.CustomField15);
                command.AddParameterWithValue("@customField16", item.CheckCustomFieldDatatype("CustomField16", "BOOL") ? (item.CustomField16 ?? "0") : item.CustomField16);
                command.AddParameterWithValue("@customField17", item.CheckCustomFieldDatatype("CustomField17", "BOOL") ? (item.CustomField17 ?? "0") : item.CustomField17);
                command.AddParameterWithValue("@customField18", item.CheckCustomFieldDatatype("CustomField18", "BOOL") ? (item.CustomField18 ?? "0") : item.CustomField18);
                command.AddParameterWithValue("@customField19", item.CheckCustomFieldDatatype("CustomField19", "BOOL") ? (item.CustomField19 ?? "0") : item.CustomField19);
                command.AddParameterWithValue("@customField20", item.CheckCustomFieldDatatype("CustomField20", "BOOL") ? (item.CustomField20 ?? "0") : item.CustomField20);
                command.AddParameterWithValue("@customField21", item.CheckCustomFieldDatatype("CustomField21", "BOOL") ? (item.CustomField21 ?? "0") : item.CustomField21);
                command.AddParameterWithValue("@customField22", item.CheckCustomFieldDatatype("CustomField22", "BOOL") ? (item.CustomField22 ?? "0") : item.CustomField22);
                command.AddParameterWithValue("@customField23", item.CheckCustomFieldDatatype("CustomField23", "BOOL") ? (item.CustomField23 ?? "0") : item.CustomField23);
                command.AddParameterWithValue("@customField24", item.CheckCustomFieldDatatype("CustomField24", "BOOL") ? (item.CustomField24 ?? "0") : item.CustomField24);
                command.AddParameterWithValue("@customField25", item.CheckCustomFieldDatatype("CustomField25", "BOOL") ? (item.CustomField25 ?? "0") : item.CustomField25);
                command.AddParameterWithValue("@customField26", item.CheckCustomFieldDatatype("CustomField26", "BOOL") ? (item.CustomField26 ?? "0") : item.CustomField26);
                command.AddParameterWithValue("@customField27", item.CheckCustomFieldDatatype("CustomField27", "BOOL") ? (item.CustomField27 ?? "0") : item.CustomField27);
                command.AddParameterWithValue("@customField28", item.CheckCustomFieldDatatype("CustomField28", "BOOL") ? (item.CustomField28 ?? "0") : item.CustomField28);
                command.AddParameterWithValue("@customField29", item.CheckCustomFieldDatatype("CustomField29", "BOOL") ? (item.CustomField29 ?? "0") : item.CustomField29);
                command.AddParameterWithValue("@customField30", item.CheckCustomFieldDatatype("CustomField30", "BOOL") ? (item.CustomField30 ?? "0") : item.CustomField30);
                command.AddParameterWithValue("@customField31", item.CheckCustomFieldDatatype("CustomField31", "BOOL") ? (item.CustomField31 ?? "0") : item.CustomField31);
                command.AddParameterWithValue("@customField32", item.CheckCustomFieldDatatype("CustomField32", "BOOL") ? (item.CustomField32 ?? "0") : item.CustomField32);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.EmployeeCustomFieldId = Convert.ToInt64(idParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        public void Update(DatabaseUser user, EmployeeCustomField item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeCustomField_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeCustomField_update");

                    command.AddParameterWithValue("@employeeCustomFieldId", item.EmployeeCustomFieldId);
                    command.AddParameterWithValue("@employeeId", item.EmployeeId);
                    command.AddParameterWithValue("@customField01", item.CheckCustomFieldDatatype("CustomField01", "BOOL") ? (item.CustomField01 ?? "0") : item.CustomField01);
                    command.AddParameterWithValue("@customField02", item.CheckCustomFieldDatatype("CustomField02", "BOOL") ? (item.CustomField02 ?? "0") : item.CustomField02);
                    command.AddParameterWithValue("@customField03", item.CheckCustomFieldDatatype("CustomField03", "BOOL") ? (item.CustomField03 ?? "0") : item.CustomField03);
                    command.AddParameterWithValue("@customField04", item.CheckCustomFieldDatatype("CustomField04", "BOOL") ? (item.CustomField04 ?? "0") : item.CustomField04);
                    command.AddParameterWithValue("@customField05", item.CheckCustomFieldDatatype("CustomField05", "BOOL") ? (item.CustomField05 ?? "0") : item.CustomField05);
                    command.AddParameterWithValue("@customField06", item.CheckCustomFieldDatatype("CustomField06", "BOOL") ? (item.CustomField06 ?? "0") : item.CustomField06);
                    command.AddParameterWithValue("@customField07", item.CheckCustomFieldDatatype("CustomField07", "BOOL") ? (item.CustomField07 ?? "0") : item.CustomField07);
                    command.AddParameterWithValue("@customField08", item.CheckCustomFieldDatatype("CustomField08", "BOOL") ? (item.CustomField08 ?? "0") : item.CustomField08);
                    command.AddParameterWithValue("@customField09", item.CheckCustomFieldDatatype("CustomField09", "BOOL") ? (item.CustomField09 ?? "0") : item.CustomField09);
                    command.AddParameterWithValue("@customField10", item.CheckCustomFieldDatatype("CustomField10", "BOOL") ? (item.CustomField10 ?? "0") : item.CustomField10);
                    command.AddParameterWithValue("@customField11", item.CheckCustomFieldDatatype("CustomField11", "BOOL") ? (item.CustomField11 ?? "0") : item.CustomField11);
                    command.AddParameterWithValue("@customField12", item.CheckCustomFieldDatatype("CustomField12", "BOOL") ? (item.CustomField12 ?? "0") : item.CustomField12);
                    command.AddParameterWithValue("@customField13", item.CheckCustomFieldDatatype("CustomField13", "BOOL") ? (item.CustomField13 ?? "0") : item.CustomField13);
                    command.AddParameterWithValue("@customField14", item.CheckCustomFieldDatatype("CustomField14", "BOOL") ? (item.CustomField14 ?? "0") : item.CustomField14);
                    command.AddParameterWithValue("@customField15", item.CheckCustomFieldDatatype("CustomField15", "BOOL") ? (item.CustomField15 ?? "0") : item.CustomField15);
                    command.AddParameterWithValue("@customField16", item.CheckCustomFieldDatatype("CustomField16", "BOOL") ? (item.CustomField16 ?? "0") : item.CustomField16);
                    command.AddParameterWithValue("@customField17", item.CheckCustomFieldDatatype("CustomField17", "BOOL") ? (item.CustomField17 ?? "0") : item.CustomField17);
                    command.AddParameterWithValue("@customField18", item.CheckCustomFieldDatatype("CustomField18", "BOOL") ? (item.CustomField18 ?? "0") : item.CustomField18);
                    command.AddParameterWithValue("@customField19", item.CheckCustomFieldDatatype("CustomField19", "BOOL") ? (item.CustomField19 ?? "0") : item.CustomField19);
                    command.AddParameterWithValue("@customField20", item.CheckCustomFieldDatatype("CustomField20", "BOOL") ? (item.CustomField20 ?? "0") : item.CustomField20);
                    command.AddParameterWithValue("@customField21", item.CheckCustomFieldDatatype("CustomField21", "BOOL") ? (item.CustomField21 ?? "0") : item.CustomField21);
                    command.AddParameterWithValue("@customField22", item.CheckCustomFieldDatatype("CustomField22", "BOOL") ? (item.CustomField22 ?? "0") : item.CustomField22);
                    command.AddParameterWithValue("@customField23", item.CheckCustomFieldDatatype("CustomField23", "BOOL") ? (item.CustomField23 ?? "0") : item.CustomField23);
                    command.AddParameterWithValue("@customField24", item.CheckCustomFieldDatatype("CustomField24", "BOOL") ? (item.CustomField24 ?? "0") : item.CustomField24);
                    command.AddParameterWithValue("@customField25", item.CheckCustomFieldDatatype("CustomField25", "BOOL") ? (item.CustomField25 ?? "0") : item.CustomField25);
                    command.AddParameterWithValue("@customField26", item.CheckCustomFieldDatatype("CustomField26", "BOOL") ? (item.CustomField26 ?? "0") : item.CustomField26);
                    command.AddParameterWithValue("@customField27", item.CheckCustomFieldDatatype("CustomField27", "BOOL") ? (item.CustomField27 ?? "0") : item.CustomField27);
                    command.AddParameterWithValue("@customField28", item.CheckCustomFieldDatatype("CustomField28", "BOOL") ? (item.CustomField28 ?? "0") : item.CustomField28);
                    command.AddParameterWithValue("@customField29", item.CheckCustomFieldDatatype("CustomField29", "BOOL") ? (item.CustomField29 ?? "0") : item.CustomField29);
                    command.AddParameterWithValue("@customField30", item.CheckCustomFieldDatatype("CustomField30", "BOOL") ? (item.CustomField30 ?? "0") : item.CustomField30);
                    command.AddParameterWithValue("@customField31", item.CheckCustomFieldDatatype("CustomField31", "BOOL") ? (item.CustomField31 ?? "0") : item.CustomField31);
                    command.AddParameterWithValue("@customField32", item.CheckCustomFieldDatatype("CustomField32", "BOOL") ? (item.CustomField32 ?? "0") : item.CustomField32);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, EmployeeCustomField item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeCustomField_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeCustomField_delete");

                    command.AddParameterWithValue("@employeeCustomFieldId", item.EmployeeCustomFieldId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeCustomFieldCollection MapToEmployeeCustomFieldCollection(IDataReader reader)
        {
            EmployeeCustomFieldCollection collection = new EmployeeCustomFieldCollection();

            while (reader.Read())
                collection.Add(MapToEmployeeCustomField(reader));

            return collection;
        }
        protected EmployeeCustomField MapToEmployeeCustomField(IDataReader reader)
        {
            EmployeeCustomField item = new EmployeeCustomField();
            base.MapToBase(item, reader);

            item.EmployeeCustomFieldId = (long)CleanDataValue(reader[ColumnNames.EmployeeCustomFieldId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.CustomField01 = (String)CleanDataValue(reader[ColumnNames.CustomField01]);
            item.CustomField01DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField01DatatypeCode]);
            item.CustomField02 = (String)CleanDataValue(reader[ColumnNames.CustomField02]);
            item.CustomField02DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField02DatatypeCode]);
            item.CustomField03 = (String)CleanDataValue(reader[ColumnNames.CustomField03]);
            item.CustomField03DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField03DatatypeCode]);
            item.CustomField04 = (String)CleanDataValue(reader[ColumnNames.CustomField04]);
            item.CustomField04DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField04DatatypeCode]);
            item.CustomField05 = (String)CleanDataValue(reader[ColumnNames.CustomField05]);
            item.CustomField05DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField05DatatypeCode]);
            item.CustomField06 = (String)CleanDataValue(reader[ColumnNames.CustomField06]);
            item.CustomField06DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField06DatatypeCode]);
            item.CustomField07 = (String)CleanDataValue(reader[ColumnNames.CustomField07]);
            item.CustomField07DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField07DatatypeCode]);
            item.CustomField08 = (String)CleanDataValue(reader[ColumnNames.CustomField08]);
            item.CustomField08DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField08DatatypeCode]);
            item.CustomField09 = (String)CleanDataValue(reader[ColumnNames.CustomField09]);
            item.CustomField09DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField09DatatypeCode]);
            item.CustomField10 = (String)CleanDataValue(reader[ColumnNames.CustomField10]);
            item.CustomField10DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField10DatatypeCode]);
            item.CustomField11 = (String)CleanDataValue(reader[ColumnNames.CustomField11]);
            item.CustomField11DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField11DatatypeCode]);
            item.CustomField12 = (String)CleanDataValue(reader[ColumnNames.CustomField12]);
            item.CustomField12DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField12DatatypeCode]);
            item.CustomField13 = (String)CleanDataValue(reader[ColumnNames.CustomField13]);
            item.CustomField13DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField13DatatypeCode]);
            item.CustomField14 = (String)CleanDataValue(reader[ColumnNames.CustomField14]);
            item.CustomField14DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField14DatatypeCode]);
            item.CustomField15 = (String)CleanDataValue(reader[ColumnNames.CustomField15]);
            item.CustomField15DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField15DatatypeCode]);
            item.CustomField16 = (String)CleanDataValue(reader[ColumnNames.CustomField16]);
            item.CustomField16DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField16DatatypeCode]);
            item.CustomField17 = (String)CleanDataValue(reader[ColumnNames.CustomField17]);
            item.CustomField17DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField17DatatypeCode]);
            item.CustomField18 = (String)CleanDataValue(reader[ColumnNames.CustomField18]);
            item.CustomField18DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField18DatatypeCode]);
            item.CustomField19 = (String)CleanDataValue(reader[ColumnNames.CustomField19]);
            item.CustomField19DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField19DatatypeCode]);
            item.CustomField20 = (String)CleanDataValue(reader[ColumnNames.CustomField20]);
            item.CustomField20DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField20DatatypeCode]);
            item.CustomField21 = (String)CleanDataValue(reader[ColumnNames.CustomField21]);
            item.CustomField21DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField21DatatypeCode]);
            item.CustomField22 = (String)CleanDataValue(reader[ColumnNames.CustomField22]);
            item.CustomField22DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField22DatatypeCode]);
            item.CustomField23 = (String)CleanDataValue(reader[ColumnNames.CustomField23]);
            item.CustomField23DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField23DatatypeCode]);
            item.CustomField24 = (String)CleanDataValue(reader[ColumnNames.CustomField24]);
            item.CustomField24DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField24DatatypeCode]);
            item.CustomField25 = (String)CleanDataValue(reader[ColumnNames.CustomField25]);
            item.CustomField25DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField25DatatypeCode]);
            item.CustomField26 = (String)CleanDataValue(reader[ColumnNames.CustomField26]);
            item.CustomField26DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField26DatatypeCode]);
            item.CustomField27 = (String)CleanDataValue(reader[ColumnNames.CustomField27]);
            item.CustomField27DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField27DatatypeCode]);
            item.CustomField28 = (String)CleanDataValue(reader[ColumnNames.CustomField28]);
            item.CustomField28DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField28DatatypeCode]);
            item.CustomField29 = (String)CleanDataValue(reader[ColumnNames.CustomField29]);
            item.CustomField29DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField29DatatypeCode]);
            item.CustomField30 = (String)CleanDataValue(reader[ColumnNames.CustomField30]);
            item.CustomField30DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField30DatatypeCode]);
            item.CustomField31 = (String)CleanDataValue(reader[ColumnNames.CustomField31]);
            item.CustomField31DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField31DatatypeCode]);
            item.CustomField32 = (String)CleanDataValue(reader[ColumnNames.CustomField32]);
            item.CustomField32DatatypeCode = (String)CleanDataValue(reader[ColumnNames.CustomField32DatatypeCode]);

            return item;
        }
        #endregion
    }
}