﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerRevenueQuebecBusiness : SqlServerBase
    {
        #region column names

        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String BusinessId = "business_id";
            public static String FileSequenceNumber = "file_sequence_number";
        }

        #endregion


        #region select

        internal RevenueQuebecBusiness Select(DatabaseUser user)
        {
            RevenueQuebecBusiness revenueQuebecBusiness = null;


            using (DataBaseCommand command = GetStoredProcCommand("RevenueQuebecBusiness_select", user.DatabaseName))
            {
                using (IDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        revenueQuebecBusiness = MapToRevenueQuebecBusiness(reader);
                    }
                }
            }

            return revenueQuebecBusiness;
        }

        internal RevenueQuebecBusiness Update(DatabaseUser user, RevenueQuebecBusiness revenueQuebecBusiness)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("RevenueQuebecBusiness_update", user.DatabaseName))
                {
                    command.BeginTransaction("RevenueQuebecBusiness_update");

                    command.AddParameterWithValue("@businessId", revenueQuebecBusiness.BusinessId);
                    command.AddParameterWithValue("@fileSequenceNumber", revenueQuebecBusiness.FileSequenceNumber);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", revenueQuebecBusiness.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    revenueQuebecBusiness.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();

                    return revenueQuebecBusiness;
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
                return null;
            }
        }

        #endregion


        #region data mapping

        protected RevenueQuebecBusiness MapToRevenueQuebecBusiness(IDataReader reader)
        {
            RevenueQuebecBusiness item = new RevenueQuebecBusiness();

            base.MapToBase(item, reader);

            item.BusinessId = (String)CleanDataValue(reader[ColumnNames.BusinessId]);
            item.FileSequenceNumber = (String)CleanDataValue(reader[ColumnNames.FileSequenceNumber]);

            return item;
        }

        #endregion
    }
}
