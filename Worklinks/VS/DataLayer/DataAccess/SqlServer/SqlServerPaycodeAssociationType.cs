﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerPaycodeAssociationType : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PaycodeAssociationTypeCode = "code_paycode_association_type_cd";
            public static String Description = "description";
            public static String HasChildrenFlag = "has_children_flag";
        }
        #endregion

        #region select
        internal PaycodeAssociationTypeCollection Select(DatabaseUser user, String paycodeAssociationTypeCode)
        {
            DataBaseCommand command = GetStoredProcCommand("PaycodeAssociationType_select", user.DatabaseName);
            command.AddParameterWithValue("@paycodeAssociationTypeCode", paycodeAssociationTypeCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToPaycodeAssociationTypeCollection(reader);
        }
        #endregion

        #region insert
        public PaycodeAssociationType Insert(DatabaseUser user, PaycodeAssociationType paycode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PaycodeAssociationType_insert", user.DatabaseName))
            {
                command.BeginTransaction("PaycodeAssociationType_insert");

                command.AddParameterWithValue("@paycodeAssociationTypeCode", paycode.PaycodeAssociationTypeCode);
                command.AddParameterWithValue("@description", paycode.Description);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", paycode.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                paycode.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return paycode;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, PaycodeAssociationType paycode)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PaycodeAssociationType_update", user.DatabaseName))
                {
                    command.BeginTransaction("PaycodeAssociationType_update");

                    command.AddParameterWithValue("@paycodeAssociationTypeCode", paycode.PaycodeAssociationTypeCode);
                    command.AddParameterWithValue("@description", paycode.Description);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", paycode.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    paycode.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, PaycodeAssociationType paycode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PaycodeAssociationType_delete", user.DatabaseName))
            {
                command.BeginTransaction("PaycodeAssociationType_delete");
                command.AddParameterWithValue("@paycodeAssociationTypeCode", paycode.PaycodeAssociationTypeCode);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        #endregion

        #region data mapping
        protected PaycodeAssociationTypeCollection MapToPaycodeAssociationTypeCollection(IDataReader reader)
        {
            PaycodeAssociationTypeCollection collection = new PaycodeAssociationTypeCollection();

            while (reader.Read())
                collection.Add(MapToPaycodeAssociationType(reader));

            return collection;
        }

        protected PaycodeAssociationType MapToPaycodeAssociationType(IDataReader reader)
        {
            PaycodeAssociationType item = new PaycodeAssociationType();
            base.MapToBase(item, reader);

            item.PaycodeAssociationTypeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeAssociationTypeCode]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            item.HasChildrenFlag = (bool)CleanDataValue(reader[ColumnNames.HasChildrenFlag]);

            return item;
        }
        #endregion
    }
}