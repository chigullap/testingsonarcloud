﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects.Import;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerAdvantageTimeRecord : SqlServerBase
    {
        #region import

        public void ImportAdvantageTimeRecord(DatabaseUser user, AdvantageTimeRecord advantageTimeRecord)
        {
            using (DataBaseCommand command = GetStoredProcCommand("custom.AdvantageTimeRecord_import", user.DatabaseName))
            {
                command.BeginTransaction("AdvantageTimeRecord_import");

                SqlParameter advantageTimeRecordIdParm = command.AddParameterWithValue("@advantageTimeRecordId", advantageTimeRecord.AdvantageTimeRecordId, ParameterDirection.Output);

                // first table parameter
                SqlParameter parmAdvantageTimeRecordImport = new SqlParameter();
                parmAdvantageTimeRecordImport.ParameterName = "@parmAdvantageTimeRecordImport";
                parmAdvantageTimeRecordImport.SqlDbType = SqlDbType.Structured;

                DataTable tableAdvantageTimeRecordImport = LoadAdvantageTimeRecordImportDataTable(advantageTimeRecord);
                parmAdvantageTimeRecordImport.Value = tableAdvantageTimeRecordImport;
                command.AddParameter(parmAdvantageTimeRecordImport);

                // second table parameter
                SqlParameter parmAdvantageTimeRecordDetailImport = new SqlParameter();
                parmAdvantageTimeRecordDetailImport.ParameterName = "@parmAdvantageTimeRecordDetailImport";
                parmAdvantageTimeRecordDetailImport.SqlDbType = SqlDbType.Structured;

                DataTable tableAdvantageTimeRecordDetailImport = LoadAdvantageTimeRecordDetailImportDataTable(advantageTimeRecord);
                parmAdvantageTimeRecordDetailImport.Value = tableAdvantageTimeRecordDetailImport;
                command.AddParameter(parmAdvantageTimeRecordDetailImport);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", advantageTimeRecord.RowVersion, ParameterDirection.InputOutput);

                command.AddParameterWithValue("@updateUser", user.UserName);

                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                advantageTimeRecord.AdvantageTimeRecordId = Convert.ToInt64(advantageTimeRecordIdParm.Value);
                advantageTimeRecord.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }

        private DataTable LoadAdvantageTimeRecordImportDataTable(AdvantageTimeRecord advantageTimeRecord)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("advantage_time_record_id", typeof(long));
            table.Columns.Add("import_export_log_id", typeof(long));

            table.Rows.Add(new Object[]
            {
                advantageTimeRecord.AdvantageTimeRecordId,
                advantageTimeRecord.ImportExportLogId
            });

            return table;
        }

        private DataTable LoadAdvantageTimeRecordDetailImportDataTable(AdvantageTimeRecord advantageTimeRecord)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("advantage_time_record_detail_id", typeof(long));
            table.Columns.Add("advantage_time_record_id", typeof(long));
            table.Columns.Add("transaction_date", typeof(DateTime));
            table.Columns.Add("full_name", typeof(String));
            table.Columns.Add("unique_identifier", typeof(String));
            table.Columns.Add("employee_import_external_identifier", typeof(String));
            table.Columns.Add("payroll_pin", typeof(String));
            table.Columns.Add("logged_in", typeof(decimal));
            table.Columns.Add("total_payable", typeof(decimal));
            table.Columns.Add("total_non_payable", typeof(decimal));
            table.Columns.Add("total_training", typeof(decimal));
            table.Columns.Add("total_non_billable", typeof(decimal));
            table.Columns.Add("total_billable", typeof(decimal));
            table.Columns.Add("timecode_00", typeof(decimal));
            table.Columns.Add("timecode_01", typeof(decimal));
            table.Columns.Add("timecode_02", typeof(decimal));
            table.Columns.Add("timecode_03", typeof(decimal));
            table.Columns.Add("timecode_04", typeof(decimal));
            table.Columns.Add("timecode_05", typeof(decimal));
            table.Columns.Add("timecode_06", typeof(decimal));
            table.Columns.Add("timecode_07", typeof(decimal));
            table.Columns.Add("timecode_08", typeof(decimal));
            table.Columns.Add("timecode_09", typeof(decimal));
            table.Columns.Add("timecode_10", typeof(decimal));
            table.Columns.Add("timecode_11", typeof(decimal));
            table.Columns.Add("timecode_12", typeof(decimal));
            table.Columns.Add("timecode_13", typeof(decimal));
            table.Columns.Add("timecode_14", typeof(decimal));
            table.Columns.Add("timecode_15", typeof(decimal));
            table.Columns.Add("timecode_16", typeof(decimal));
            table.Columns.Add("timecode_17", typeof(decimal));
            table.Columns.Add("timecode_18", typeof(decimal));
            table.Columns.Add("timecode_19", typeof(decimal));
            table.Columns.Add("timecode_20", typeof(decimal));
            table.Columns.Add("timecode_21", typeof(decimal));
            table.Columns.Add("timecode_22", typeof(decimal));
            table.Columns.Add("timecode_23", typeof(decimal));
            table.Columns.Add("timecode_24", typeof(decimal));
            table.Columns.Add("timecode_25", typeof(decimal));
            table.Columns.Add("timecode_26", typeof(decimal));
            table.Columns.Add("timecode_27", typeof(decimal));
            table.Columns.Add("timecode_28", typeof(decimal));
            table.Columns.Add("timecode_29", typeof(decimal));
            table.Columns.Add("timecode_30", typeof(decimal));
            table.Columns.Add("timecode_31", typeof(decimal));
            table.Columns.Add("timecode_32", typeof(decimal));
            table.Columns.Add("timecode_33", typeof(decimal));
            table.Columns.Add("timecode_34", typeof(decimal));
            table.Columns.Add("timecode_35", typeof(decimal));
            table.Columns.Add("timecode_36", typeof(decimal));
            table.Columns.Add("timecode_37", typeof(decimal));
            table.Columns.Add("timecode_38", typeof(decimal));
            table.Columns.Add("timecode_39", typeof(decimal));
            table.Columns.Add("timecode_40", typeof(decimal));
            table.Columns.Add("timecode_41", typeof(decimal));
            table.Columns.Add("timecode_42", typeof(decimal));
            table.Columns.Add("timecode_43", typeof(decimal));
            table.Columns.Add("timecode_44", typeof(decimal));
            table.Columns.Add("timecode_45", typeof(decimal));
            table.Columns.Add("timecode_46", typeof(decimal));
            table.Columns.Add("timecode_47", typeof(decimal));
            table.Columns.Add("timecode_48", typeof(decimal));
            table.Columns.Add("timecode_49", typeof(decimal));
            table.Columns.Add("timecode_50", typeof(decimal));

            foreach (AdvantageTimeRecordDetail item in advantageTimeRecord.AdvantageTimeRecordDetails)
            {
                table.Rows.Add(new Object[]
                {
                    item.AdvantageTimeRecordDetailId,
                    item.AdvantageTimeRecordId,
                    item.TransactionDate,
                    item.FullName,
                    item.UniqueIdentifier,
                    item.EmployeeImportExternalIdentifier,
                    item.PayrollPin,
                    item.LoggedIn,
                    item.TotalPayable,
                    item.TotalNonPayable,
                    item.TotalTraining,
                    item.TotalNonBillable,
                    item.TotalBillable,
                    item.TimeCode00,
                    item.TimeCode01,
                    item.TimeCode02,
                    item.TimeCode03,
                    item.TimeCode04,
                    item.TimeCode05,
                    item.TimeCode06,
                    item.TimeCode07,
                    item.TimeCode08,
                    item.TimeCode09,
                    item.TimeCode10,
                    item.TimeCode11,
                    item.TimeCode12,
                    item.TimeCode13,
                    item.TimeCode14,
                    item.TimeCode15,
                    item.TimeCode16,
                    item.TimeCode17,
                    item.TimeCode18,
                    item.TimeCode19,
                    item.TimeCode20,
                    item.TimeCode21,
                    item.TimeCode22,
                    item.TimeCode23,
                    item.TimeCode24,
                    item.TimeCode25,
                    item.TimeCode26,
                    item.TimeCode27,
                    item.TimeCode28,
                    item.TimeCode29,
                    item.TimeCode30,
                    item.TimeCode31,
                    item.TimeCode32,
                    item.TimeCode33,
                    item.TimeCode34,
                    item.TimeCode35,
                    item.TimeCode36,
                    item.TimeCode37,
                    item.TimeCode38,
                    item.TimeCode39,
                    item.TimeCode40,
                    item.TimeCode41,
                    item.TimeCode42,
                    item.TimeCode43,
                    item.TimeCode44,
                    item.TimeCode45,
                    item.TimeCode46,
                    item.TimeCode47,
                    item.TimeCode48,
                    item.TimeCode49,
                    item.TimeCode50
                });
            }

            return table;
        }

        #endregion
    }
}
