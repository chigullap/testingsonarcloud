﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerEmployeeRemittanceStub : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeRemittanceStubId = "employee_remittance_stub_id";
            public static String EmployeePaycodeId = "employee_paycode_id";
            public static String FileName = "file_name";
            public static String FileTypeCode = "code_file_type_cd";
            public static String Data = "data";
        }
        #endregion

        #region main
        public EmployeeRemittanceStubCollection Select(DatabaseUser user, long employeePaycodeId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeeRemittanceStub_select", user.DatabaseName);

            command.AddParameterWithValue("@employeePaycodeId", employeePaycodeId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToEmployeeRemittanceStubCollection(reader);
        }
        public EmployeeRemittanceStub Insert(DatabaseUser user, EmployeeRemittanceStub stub)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeRemittanceStub_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeRemittanceStub_insert");

                SqlParameter stubIdParm = command.AddParameterWithValue("@employeeRemittanceStubId", stub.EmployeeRemittanceStubId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeePaycodeId", stub.EmployeePaycodeId);
                command.AddParameterWithValue("@fileName", stub.FileName);
                command.AddParameterWithValue("@fileTypeCode", stub.FileTypeCode);
                command.AddParameterWithValue("@data", stub.Data);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", stub.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                stub.EmployeeRemittanceStubId = Convert.ToInt64(stubIdParm.Value);
                stub.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return stub;
            }
        }
        public void Update(DatabaseUser user, EmployeeRemittanceStub stub)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeRemittanceStub_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeRemittanceStub_update");

                    command.AddParameterWithValue("@employeeRemittanceStubId", stub.EmployeeRemittanceStubId);
                    command.AddParameterWithValue("@employeePaycodeId", stub.EmployeePaycodeId);
                    command.AddParameterWithValue("@fileName", stub.FileName);
                    command.AddParameterWithValue("@fileTypeCode", stub.FileTypeCode);

                    if (stub.Data == null)
                        command.AddParameterWithValue("@data", stub.Data, DbType.Byte);
                    else
                        command.AddParameterWithValue("@data", stub.Data);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", stub.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    stub.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, EmployeeRemittanceStub stub)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeRemittanceStub_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeRemittanceStub_delete");

                    command.AddParameterWithValue("@employeePaycodeId", stub.EmployeePaycodeId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeRemittanceStubCollection MapToEmployeeRemittanceStubCollection(IDataReader reader)
        {
            EmployeeRemittanceStubCollection collection = new EmployeeRemittanceStubCollection();

            while (reader.Read())
                collection.Add(MapToEmployeeRemittanceStub(reader));

            return collection;
        }
        protected EmployeeRemittanceStub MapToEmployeeRemittanceStub(IDataReader reader)
        {
            EmployeeRemittanceStub item = new EmployeeRemittanceStub();
            base.MapToBase(item, reader);

            item.EmployeeRemittanceStubId = (long)CleanDataValue(reader[ColumnNames.EmployeeRemittanceStubId]);
            item.EmployeePaycodeId = (long)CleanDataValue(reader[ColumnNames.EmployeePaycodeId]);
            item.FileName = (String)CleanDataValue(reader[ColumnNames.FileName]);
            item.FileTypeCode = (String)CleanDataValue(reader[ColumnNames.FileTypeCode]);
            item.Data = (byte[])CleanDataValue(reader[ColumnNames.Data]);

            return item;
        }
        #endregion
    }
}