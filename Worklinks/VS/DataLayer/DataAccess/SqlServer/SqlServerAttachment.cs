﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerAttachment : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String AttachmentId = "attachment_id";
            public static String FileName = "file_name";
            public static String FileTypeCode = "code_file_type_cd";
            public static String Description = "description";
            public static String Data = "data";
        }
        #endregion

        #region main
        public AttachmentCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long attachmentId)
        {
            DataBaseCommand command = GetStoredProcCommand("Attachment_select", user.DatabaseName);

            command.AddParameterWithValue("@attachmentId", attachmentId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToAttachmentCollection(reader);
            }
        }

        public Attachment Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Attachment attachment)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Attachment_insert", user.DatabaseName))
            {
                command.BeginTransaction("Attachment_insert");

                SqlParameter attachmentIdParm = command.AddParameterWithValue("@attachmentId", attachment.AttachmentId, ParameterDirection.Output);
                command.AddParameterWithValue("@fileName", attachment.FileName);
                command.AddParameterWithValue("@codeFileTypeCd", attachment.FileTypeCode);
                command.AddParameterWithValue("@description", attachment.Description);
                command.AddParameterWithValue("@data", attachment.Data);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", attachment.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                attachment.AttachmentId = Convert.ToInt64(attachmentIdParm.Value);
                attachment.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return attachment;
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Attachment attachment)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("Attachment_update", user.DatabaseName))
                {
                    command.BeginTransaction("Attachment_update");

                    command.AddParameterWithValue("@attachmentId", attachment.AttachmentId);
                    command.AddParameterWithValue("@fileName", attachment.FileName);
                    command.AddParameterWithValue("@codeFileTypeCd", attachment.FileTypeCode);
                    command.AddParameterWithValue("@description", attachment.Description);

                    if (attachment.Data == null)
                        command.AddParameterWithValue("@data", attachment.Data, DbType.Byte);
                    else
                        command.AddParameterWithValue("@data", attachment.Data);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", attachment.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    attachment.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Attachment attachment)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("Attachment_delete", user.DatabaseName))
                {
                    command.BeginTransaction("Attachment_delete");

                    command.AddParameterWithValue("@attachmentId", attachment.AttachmentId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected AttachmentCollection MapToAttachmentCollection(IDataReader reader)
        {
            AttachmentCollection collection = new AttachmentCollection();
            while (reader.Read())
            {
                collection.Add(MapToAttachment(reader));
            }

            return collection;
        }

        protected Attachment MapToAttachment(IDataReader reader)
        {
            Attachment item = new Attachment();
            base.MapToBase(item, reader);

            item.AttachmentId = (long)CleanDataValue(reader[ColumnNames.AttachmentId]);
            item.FileName = (String)CleanDataValue(reader[ColumnNames.FileName]);
            item.FileTypeCode = (String)CleanDataValue(reader[ColumnNames.FileTypeCode]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            item.Data = (byte[])CleanDataValue(reader[ColumnNames.Data]);

            return item;
        }
        #endregion
    }
}