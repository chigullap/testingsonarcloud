﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerCodeTableIndex : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String CodeTableIndexId = "code_table_index_id";
            public static String TableName = "table_name";
            public static String ParentTableName = "parent_table_name";
            public static String TableDescription = "table_description";
            public static String VisibleFlag = "visible_flag";
        }
        #endregion

        #region main
        /// <summary>
        /// returns a code table
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        //internal CodeTableIndexCollection Select(long CodeTableIndexId) no params, selecting all that are visible via sql proc
        internal CodeTableIndexCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, string tableName)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CodeTableIndex_select", user.DatabaseName))
            {
                //the SELECT proc will get all rows that are visible, does not contain a "WHERE key = KeyVar" clause
                command.AddParameterWithValue("@codeTableName", tableName);

                CodeTableIndexCollection codeTables = null;

                using (IDataReader reader = command.ExecuteReader())
                {
                    codeTables = MapToCodeTableIndexCollection(reader);
                }

                return codeTables;
            }
        }
        #endregion
        
        #region data mapping
        /// <summary>
        /// maps result set to code table index collection
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected CodeTableIndexCollection MapToCodeTableIndexCollection(IDataReader reader)
        {
            CodeTableIndexCollection collection = new CodeTableIndexCollection();
            while (reader.Read())
            {
                collection.Add(MapToCodeTableIndex(reader));
            }

            return collection;
        }

        /// <summary>
        /// maps columns to code table index properties
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected CodeTableIndex MapToCodeTableIndex(IDataReader reader)
        {
            CodeTableIndex codeTable = new CodeTableIndex();
            base.MapToBase(codeTable, reader);
            codeTable.CodeTableIndexId = (long)CleanDataValue(reader[ColumnNames.CodeTableIndexId]);
            codeTable.TableName = (String)CleanDataValue(reader[ColumnNames.TableName]);
            codeTable.ParentTableName = (String)CleanDataValue(reader[ColumnNames.ParentTableName]);
            codeTable.TableDescription = (String)CleanDataValue(reader[ColumnNames.TableDescription]);
            codeTable.VisibleFlag = (bool)CleanDataValue(reader[ColumnNames.VisibleFlag]);

            return codeTable;
        }
        #endregion
    }
}
