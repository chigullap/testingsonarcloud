﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerImportExportLog : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ImportExportLogId = "import_export_log_id";
            public static String ImportExportId = "import_export_id";
            public static String ImportExportDate = "import_export_date";
            public static String FileProcessingDirectory = "file_processing_directory";
            public static String ProcessingOutput = "processing_output";
            public static String SuccessFlag = "success_flag";
            public static String WarningFlag = "warning_flag";
            public static String UniqueIdentifier = "unique_identifier";
            public static String AlternateExternalIdentifier1 = "alternate_external_identifier_1";
            public static String AlternateExternalIdentifier2 = "alternate_external_identifier_2";
            public static String AlternateExternalIdentifier3 = "alternate_external_identifier_3";
            public static String AlternateExternalIdentifier4 = "alternate_external_identifier_4";
            public static String RelatedImportExportLogId = "related_import_export_log_id";
            public static String RemoteImportExportLogId = "remote_import_export_log_id";

        }
        #endregion

        #region main

        internal ImportExportLogCollection Select(DatabaseUser user, Guid uniqueIdentifier)
        {
            DataBaseCommand command = GetStoredProcCommand("ImportExportLog_select", user.DatabaseName);
            command.AddParameterWithValue("@uniqueIdentifier", uniqueIdentifier);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToImportExportLogCollection(reader);
            }
        }

        public void Insert(DatabaseUser user, ImportExportLog item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ImportExportLog_insert", user.DatabaseName))
            {
                command.BeginTransaction("ImportExportLog_insert");
             
                SqlParameter importExportLogIdParm = command.AddParameterWithValue("@importExportLogId", item.ImportExportLogId, ParameterDirection.Output);
                command.AddParameterWithValue("@importExportId", item.ImportExportId);
                command.AddParameterWithValue("@importExportDate", item.ImportExportDate);
                command.AddParameterWithValue("@fileProcessingDirectory", item.FileProcessingDirectory);
                command.AddParameterWithValue("@processingOutput",item.ProcessingOutput);
                command.AddParameterWithValue("@successFlag", item.SuccessFlag);
                command.AddParameterWithValue("@warningFlag", item.WarningFlag);
                command.AddParameterWithValue("@uniqueIdentifier", item.UniqueIdentifier);
                command.AddParameterWithValue("@alternateExternalIdentifier1", item.AlternateExternalIdentifier1);
                command.AddParameterWithValue("@alternateExternalIdentifier2", item.AlternateExternalIdentifier2);
                command.AddParameterWithValue("@alternateExternalIdentifier3", item.AlternateExternalIdentifier3);
                command.AddParameterWithValue("@alternateExternalIdentifier4", item.AlternateExternalIdentifier4);
                command.AddParameterWithValue("@relatedImportExportLogId", item.RelatedImportExportLogId);
                command.AddParameterWithValue("@remoteImportExportLogId", item.RemoteImportExportLogId);
                


                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                item.ImportExportLogId= Convert.ToInt64(importExportLogIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();
            }
        }

        public void Update(DatabaseUser user, ImportExportLog item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ImportExportLog_update", user.DatabaseName))
            {
                command.BeginTransaction("ImportExportLog_update");

                command.AddParameterWithValue("@importExportLogId", item.ImportExportLogId);
                command.AddParameterWithValue("@importExportId", item.ImportExportId);
                command.AddParameterWithValue("@importExportDate", item.ImportExportDate);
                command.AddParameterWithValue("@fileProcessingDirectory", item.FileProcessingDirectory);
                command.AddParameterWithValue("@processingOutput", item.ProcessingOutput);
                command.AddParameterWithValue("@successFlag", item.SuccessFlag);
                command.AddParameterWithValue("@warningFlag", item.WarningFlag);
                command.AddParameterWithValue("@uniqueIdentifier", item.UniqueIdentifier);
                command.AddParameterWithValue("@alternateExternalIdentifier1", item.AlternateExternalIdentifier1);
                command.AddParameterWithValue("@alternateExternalIdentifier2", item.AlternateExternalIdentifier2);
                command.AddParameterWithValue("@alternateExternalIdentifier3", item.AlternateExternalIdentifier3);
                command.AddParameterWithValue("@alternateExternalIdentifier4", item.AlternateExternalIdentifier4);
                command.AddParameterWithValue("@relatedImportExportLogId", item.RelatedImportExportLogId);
                command.AddParameterWithValue("@remoteImportExportLogId", item.RemoteImportExportLogId);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                
                command.CommitTransaction();
            }
        }

        #endregion


        #region data mapping
        protected ImportExportLogCollection MapToImportExportLogCollection(IDataReader reader)
        {
            ImportExportLogCollection collection = new ImportExportLogCollection();
            while (reader.Read())
            {
                collection.Add(MapToImportExportLog(reader));
            }

            return collection;
        }
        protected ImportExportLog MapToImportExportLog(IDataReader reader)
        {
            ImportExportLog item = new ImportExportLog();
            base.MapToBase(item, reader);

            item.ImportExportLogId = (long)CleanDataValue(reader[ColumnNames.ImportExportLogId]);
            item.ImportExportId = (long?)CleanDataValue(reader[ColumnNames.ImportExportId]);
            item.ImportExportDate = (DateTime?)CleanDataValue(reader[ColumnNames.ImportExportDate]);
            item.FileProcessingDirectory = (String)CleanDataValue(reader[ColumnNames.FileProcessingDirectory]);
            item.ProcessingOutput = (String)CleanDataValue(reader[ColumnNames.ProcessingOutput]);
            item.SuccessFlag = (bool)CleanDataValue(reader[ColumnNames.SuccessFlag]);
            item.WarningFlag = (bool)CleanDataValue(reader[ColumnNames.WarningFlag]);
            item.UniqueIdentifier = (Guid)CleanDataValue(reader[ColumnNames.UniqueIdentifier]);
            item.AlternateExternalIdentifier1 = (String)CleanDataValue(reader[ColumnNames.AlternateExternalIdentifier1]);
            item.AlternateExternalIdentifier2 = (String)CleanDataValue(reader[ColumnNames.AlternateExternalIdentifier2]);
            item.AlternateExternalIdentifier3 = (String)CleanDataValue(reader[ColumnNames.AlternateExternalIdentifier3]);
            item.AlternateExternalIdentifier4 = (String)CleanDataValue(reader[ColumnNames.AlternateExternalIdentifier4]);
            item.RelatedImportExportLogId = (long?)CleanDataValue(reader[ColumnNames.RelatedImportExportLogId]);
            item.RemoteImportExportLogId = (long?)CleanDataValue(reader[ColumnNames.RemoteImportExportLogId]);


            return item;
        }
        #endregion

    }
}
