﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerCraWcbRemittance : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String BusinessNumberId = "business_number_id";
            public static String WorkersCompensationAccountNumber = "workers_compensation_account_number";
            public static String ChequeDate = "cheque_date";
            public static String EmployeeCount = "employee_count";
            public static String CurrentPremium = "current_premium";
            public static String CurrentAssessableEarnings = "current_assessable_earnings";
            public static String CodeCraRemittancePeriodCd = "code_cra_remittance_period_cd";
            public static String RemittancePeriodStartDate = "remittance_period_start_date";
            public static String RemittancePeriodEndDate = "remittance_period_end_date";
            public static String EffectiveEntryDate = "effective_entry_date";
        }
        #endregion

        #region main

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CraWcbRemittance_Insert", user.DatabaseName))
            {
                command.BeginTransaction("CraWcbRemittance_Insert");

                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        public void UpdateCraWcbRemittanceCraWcbExportIdColumn(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, DateTime date, long? craWcbExportId, String craRemittancePeriodCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CraWcbRemittance_updateCraWcbExportId", user.DatabaseName))
            {
                command.BeginTransaction("CraWcbRemittance_updateCraWcbExportId");

                command.AddParameterWithValue("@date", date);
                command.AddParameterWithValue("@craWcbExportId", craWcbExportId);
                command.AddParameterWithValue("@craRemitCode", craRemittancePeriodCode);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        public CraWcbRemittanceSummaryCollection GetRbcEftWcbData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, DateTime date, String craRemittancePeriodCode)
        {
            DataBaseCommand command = GetStoredProcCommand("CraWcbRemittance_selectSourceData", user.DatabaseName);

            command.AddParameterWithValue("@date", date);
            command.AddParameterWithValue("@craRemitCode", craRemittancePeriodCode);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToCraWcbRemittanceSummaryCollection(reader);
            }
        }

        #endregion

        #region data mapping
        protected CraWcbRemittanceSummaryCollection MapToCraWcbRemittanceSummaryCollection(IDataReader reader)
        {
            CraWcbRemittanceSummaryCollection collection = new CraWcbRemittanceSummaryCollection();
            while (reader.Read())
            {
                collection.Add(MapToCraWcbRemittanceSummary(reader));
            }

            return collection;
        }
        protected CraWcbRemittanceSummary MapToCraWcbRemittanceSummary(IDataReader reader)
        {
            CraWcbRemittanceSummary item = new CraWcbRemittanceSummary();

            item.BusinessNumberId = (long)CleanDataValue(reader[ColumnNames.BusinessNumberId]);
            item.WorkersCompensationAccountNumber = (String)CleanDataValue(reader[ColumnNames.WorkersCompensationAccountNumber]);
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.EmployeeCount = (Int64)CleanDataValue(reader[ColumnNames.EmployeeCount]);
            item.CurrentPremium = (decimal)CleanDataValue(reader[ColumnNames.CurrentPremium]);
            item.CurrentAssessableEarnings = (decimal)CleanDataValue(reader[ColumnNames.CurrentAssessableEarnings]);
            item.CodeCraRemittancePeriodCd = (String)CleanDataValue(reader[ColumnNames.CodeCraRemittancePeriodCd]);
            item.RemittancePeriodStartDate = (DateTime?)CleanDataValue(reader[ColumnNames.RemittancePeriodStartDate]);
            item.RemittancePeriodEndDate = (DateTime?)CleanDataValue(reader[ColumnNames.RemittancePeriodEndDate]);
            item.EffectiveEntryDate = (DateTime?)CleanDataValue(reader[ColumnNames.EffectiveEntryDate]);

            return item;
        }
        #endregion
    }
}
