﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerOrganizationUnitLevelDescription : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string OrganizationUnitLevelId = "organization_unit_level_id";
            public static String LanguageCode = "code_language_cd";
            public static String OrganizationUnitLevelDescription = "description";
        }
        #endregion

        #region main
        internal OrganizationUnitLevelDescriptionCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, OrganizationUnitCriteria criteria)
        {
            using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnitLevelDescription_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@organizationUnitLevelId", criteria.OrganizationUnitLevelId);

                OrganizationUnitLevelDescriptionCollection organizationUnitLevelDescRows = null;

                using (IDataReader reader = command.ExecuteReader())
                {
                    organizationUnitLevelDescRows = MapToCodeTableCollection(reader);
                }
                return organizationUnitLevelDescRows;
            }
        }

        public OrganizationUnitLevelDescription Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, OrganizationUnitLevelDescription data)
        {
            using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnitLevelDescription_insert", user.DatabaseName))
            {
                command.BeginTransaction("OrganizationUnitLevelDescription_insert");

                command.AddParameterWithValue("@organizationUnitLevelId", data.OrganizationUnitLevelId);
                command.AddParameterWithValue("@langaugeCode", data.LanguageCode);
                command.AddParameterWithValue("@description", data.LevelDescription);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", data.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                data.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();
                return data;
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, OrganizationUnitLevelDescription data)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnitLevelDescription_update", user.DatabaseName))
                {
                    command.BeginTransaction("OrganizationUnitLevelDescription_update");

                    command.AddParameterWithValue("@organizationUnitLevelId", data.OrganizationUnitLevelId);
                    command.AddParameterWithValue("@languageCode", data.LanguageCode);
                    command.AddParameterWithValue("@description", data.LevelDescription);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", data.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    data.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, OrganizationUnitLevelDescription data)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnitLevelDescription_delete", user.DatabaseName))
                {
                    command.BeginTransaction("OrganizationUnitLevelDescription_delete");
                    command.AddParameterWithValue("@organizationUnitLevelId", data.OrganizationUnitLevelId);
                    command.AddParameterWithValue("@rowVersion", data.RowVersion);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected OrganizationUnitLevelDescriptionCollection MapToCodeTableCollection(IDataReader reader)
        {
            OrganizationUnitLevelDescriptionCollection collection = new OrganizationUnitLevelDescriptionCollection();
            while (reader.Read())
            {
                collection.Add(MapToOrganizationUnitLevelDescRows(reader));
            }
            return collection;
        }
        protected OrganizationUnitLevelDescription MapToOrganizationUnitLevelDescRows(IDataReader reader)
        {
            OrganizationUnitLevelDescription organizationUnitLevelDescription = new OrganizationUnitLevelDescription();
            base.MapToBase(organizationUnitLevelDescription, reader);

            organizationUnitLevelDescription.OrganizationUnitLevelId = (long?)CleanDataValue(reader[ColumnNames.OrganizationUnitLevelId]);
            organizationUnitLevelDescription.LanguageCode = (string)CleanDataValue(reader[ColumnNames.LanguageCode]);
            organizationUnitLevelDescription.LevelDescription = (string)CleanDataValue(reader[ColumnNames.OrganizationUnitLevelDescription]);

            return organizationUnitLevelDescription;
        }
        #endregion
    }
}