﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerHealthTaxDetail : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String HealthTaxDetailId = "health_tax_detail_id";
            public static String HealthTaxId = "health_tax_id";
            public static String EffectiveYear = "effective_year";
            public static String ExemptionAmount = "exemption_amount";
            public static String RatePercentage = "rate_percentage";
            public static String WcbChequeRemittanceFrequencyCode = "code_wcb_cheque_remittance_frequency_cd";
        }
        #endregion

        #region select
        internal HealthTaxDetailCollection Select(DatabaseUser user, long healthTaxId)
        {
            DataBaseCommand command = GetStoredProcCommand("HealthTaxDetail_select", user.DatabaseName);

            command.AddParameterWithValue("@healthTaxId", healthTaxId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToHealthTaxDetailCollection(reader);
        }
        #endregion

        #region insert
        public HealthTaxDetail Insert(DatabaseUser user, HealthTaxDetail item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("HealthTaxDetail_insert", user.DatabaseName))
                {
                    command.BeginTransaction("HealthTaxDetail_insert");

                    SqlParameter idParm = command.AddParameterWithValue("@healthTaxDetailId", item.HealthTaxDetailId, ParameterDirection.Output);
                    command.AddParameterWithValue("@healthTaxId", item.HealthTaxId);
                    command.AddParameterWithValue("@effectiveYear", item.EffectiveYear);
                    command.AddParameterWithValue("@exemptionAmount", item.ExemptionAmount);
                    command.AddParameterWithValue("@ratePercentage", item.RatePercentage);
                    command.AddParameterWithValue("@wcbChequeRemittanceFrequencyCode", item.WcbChequeRemittanceFrequencyCode);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.HealthTaxDetailId = Convert.ToInt64(idParm.Value);
                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();

                    return item;
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
                return null;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, HealthTaxDetail item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("HealthTaxDetail_update", user.DatabaseName))
                {
                    command.BeginTransaction("HealthTaxDetail_update");

                    command.AddParameterWithValue("@healthTaxDetailId", item.HealthTaxDetailId);
                    command.AddParameterWithValue("@healthTaxId", item.HealthTaxId);
                    command.AddParameterWithValue("@effectiveYear", item.EffectiveYear);
                    command.AddParameterWithValue("@exemptionAmount", item.ExemptionAmount);
                    command.AddParameterWithValue("@ratePercentage", item.RatePercentage);
                    command.AddParameterWithValue("@wcbChequeRemittanceFrequencyCode", item.WcbChequeRemittanceFrequencyCode);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, HealthTaxDetail item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("HealthTaxDetail_delete", user.DatabaseName))
                {
                    command.BeginTransaction("HealthTaxDetail_delete");

                    command.AddParameterWithValue("@healthTaxDetailId", item.HealthTaxDetailId);
                    command.AddParameterWithValue("@healthTaxId", item.HealthTaxId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected HealthTaxDetailCollection MapToHealthTaxDetailCollection(IDataReader reader)
        {
            HealthTaxDetailCollection collection = new HealthTaxDetailCollection();

            while (reader.Read())
                collection.Add(MapToHealthTaxDetail(reader));

            return collection;
        }
        protected HealthTaxDetail MapToHealthTaxDetail(IDataReader reader)
        {
            HealthTaxDetail item = new HealthTaxDetail();
            base.MapToBase(item, reader);

            item.HealthTaxDetailId = (long)CleanDataValue(reader[ColumnNames.HealthTaxDetailId]);
            item.HealthTaxId = (long)CleanDataValue(reader[ColumnNames.HealthTaxId]);
            item.EffectiveYear = (Decimal)CleanDataValue(reader[ColumnNames.EffectiveYear]);
            item.ExemptionAmount = (Decimal)CleanDataValue(reader[ColumnNames.ExemptionAmount]);
            item.RatePercentage = (Decimal)CleanDataValue(reader[ColumnNames.RatePercentage]);
            item.WcbChequeRemittanceFrequencyCode = (String)CleanDataValue(reader[ColumnNames.WcbChequeRemittanceFrequencyCode]);

            return item;
        }
        #endregion
    }
}