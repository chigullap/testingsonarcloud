﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerPerson : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PersonId = "person_id";
            public static String TitleCode = "code_title_cd";
            public static String GenderCode = "code_gender_cd";
            public static String LastName = "last_name";
            public static String MiddleName = "middle_name";
            public static String FirstName = "first_name";
            public static String GovernmentIdentificationNumberTypeCode = "code_government_identification_number_type_cd";
            public static String GovernmentIdentificationNumber1 = "government_identification_number_1";
            public static String GovernmentIdentificationNumber2 = "government_identification_number_2";
            public static String GovernmentIdentificationNumber3 = "government_identification_number_3";
            public static String BirthDate = "birth_date";
            public static String KnownAsName = "known_as_name";
            public static String LanguageCode = "code_language_cd";
            public static String DisabledFlag = "disabled_flag";
            public static String StudentFlag = "student_flag";
            public static String SmokerCode = "code_smoker_cd";
        }
        #endregion

        #region select
        internal PersonCollection Select(DatabaseUser user, long personId, bool removeFrenchAccents = false)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Person_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@personId", personId);
                command.AddParameterWithValue("@removeFrenchAccents", removeFrenchAccents);

                PersonCollection persons = null;

                using (IDataReader reader = command.ExecuteReader())
                    persons = MapToPersonCollection(reader);

                return persons;
            }
        }
        public PersonCollection SelectByEmployeeNumber(DatabaseUser user, String employeeNumber)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PersonByEmployeeeNumber_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@employeeNumber", employeeNumber);

                PersonCollection persons = null;

                using (IDataReader reader = command.ExecuteReader())
                    persons = MapToPersonCollection(reader);

                return persons;
            }
        }
        public bool CheckDatabaseForDuplicateSin(DatabaseUser user, string sin, long employeeId, string employeeNumber)
        {
            using (StoredProcedureCommand command = GetStoredProcCommand("DuplicateSIN_select", user.DatabaseName))
            {
                bool temp = false;

                try
                {
                    command.BeginTransaction("DuplicateSIN_select");

                    command.AddParameterWithValue("@socialInsuranceNumber", sin);
                    command.AddParameterWithValue("@employeeId", employeeId);
                    command.AddParameterWithValue("@employeeNumber", employeeNumber);
                    SqlParameter returnValueParm = command.AddParameterWithValue("@returnValue", false, ParameterDirection.Output);

                    command.ExecuteNonQuery();

                    temp = (bool)returnValueParm.Value;

                    command.CommitTransaction();
                }
                catch (Exception exc)
                {
                    command.RollbackTransaction();
                    HandleException(exc, command.ReturnCode);
                }

                return temp;
            }
        }
        #endregion

        #region insert
        public Person Insert(DatabaseUser user, string overrideDatabaseName,  Person person)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Person_insert", overrideDatabaseName))
            {
                command.BeginTransaction("Person_insert");

                SqlParameter personIdParm = command.AddParameterWithValue("@personId", person.PersonId, ParameterDirection.Output);
                command.AddParameterWithValue("@titleCode", person.TitleCode);
                command.AddParameterWithValue("@firstName", person.FirstName);
                command.AddParameterWithValue("@middleName", person.MiddleName);
                command.AddParameterWithValue("@lastName", person.LastName);
                command.AddParameterWithValue("@knownAsName", person.KnownAsName);
                command.AddParameterWithValue("@genderCode", person.GenderCode);
                command.AddParameterWithValue("@governmentIdentificationNumber1", person.GovernmentIdentificationNumber1);
                command.AddParameterWithValue("@governmentIdentificationNumber2", person.GovernmentIdentificationNumber2);
                command.AddParameterWithValue("@governmentIdentificationNumber3", person.GovernmentIdentificationNumber3);
                command.AddParameterWithValue("@birthDate", person.BirthDate);
                command.AddParameterWithValue("@languageCode", person.LanguageCode);
                command.AddParameterWithValue("@disabledFlag", person.DisabledFlag);
                command.AddParameterWithValue("@studentFlag", person.StudentFlag);
                command.AddParameterWithValue("@smokerCode", person.SmokerCode);
                command.AddParameterWithValue("@codeGovernmentIdentificationNumberTypeCd", person.GovernmentIdentificationNumberTypeCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", person.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                person.PersonId = Convert.ToInt64(personIdParm.Value);
                person.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return person;
            }
        }
        #endregion

        #region update

        public void UpdateAuthPerson(DatabaseUser user, string adminDatabaseName, string userName, Person person)
        {
            try
            {
                using (StoredProcedureCommand command = GetStoredProcCommand("Person_update", adminDatabaseName))
                {
                    command.BeginTransaction("Person_update");

                    command.AddParameterWithValue("@userName", userName);
                    command.AddParameterWithValue("@firstName", person.FirstName);
                    command.AddParameterWithValue("@middleName", person.MiddleName);
                    command.AddParameterWithValue("@lastName", person.LastName);
                    
                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", person.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    person.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Update(DatabaseUser user, Person person)
        {
            try
            {
                using (StoredProcedureCommand command = GetStoredProcCommand("Person_update", user.DatabaseName))
                {
                    command.BeginTransaction("Person_update");

                    command.AddParameterWithValue("@personId", person.PersonId);
                    command.AddParameterWithValue("@titleCode", person.TitleCode);
                    command.AddParameterWithValue("@firstName", person.FirstName);
                    command.AddParameterWithValue("@middleName", person.MiddleName);
                    command.AddParameterWithValue("@lastName", person.LastName);
                    command.AddParameterWithValue("@knownAsName", person.KnownAsName);
                    command.AddParameterWithValue("@genderCode", person.GenderCode);
                    command.AddParameterWithValue("@governmentIdentificationNumber1", person.GovernmentIdentificationNumber1);
                    command.AddParameterWithValue("@governmentIdentificationNumber2", person.GovernmentIdentificationNumber2);
                    command.AddParameterWithValue("@governmentIdentificationNumber3", person.GovernmentIdentificationNumber3);
                    command.AddParameterWithValue("@birthDate", person.BirthDate);
                    command.AddParameterWithValue("@languageCode", person.LanguageCode);
                    command.AddParameterWithValue("@disabledFlag", person.DisabledFlag);
                    command.AddParameterWithValue("@studentFlag", person.StudentFlag);
                    command.AddParameterWithValue("@smokerCode", person.SmokerCode);
                    command.AddParameterWithValue("@codeGovernmentIdentificationNumberTypeCd", person.GovernmentIdentificationNumberTypeCode);
                    command.AddParameterWithValue("@overrideConcurrencyCheckFlag", person.OverrideConcurrencyCheckFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", person.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    person.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        //used when updating the first/last name of a person on the Union Contacts screen
        public Person UpdateUnionPerson(DatabaseUser user, Person person)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("UnionContactPerson_update", user.DatabaseName))
                {
                    command.BeginTransaction("UnionContactPerson_update");

                    command.AddParameterWithValue("@personId", person.PersonId);
                    command.AddParameterWithValue("@firstName", person.FirstName);
                    command.AddParameterWithValue("@lastName", person.LastName);
                    command.AddParameterWithValue("@titleCode", person.TitleCode);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", person.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    person.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();

                    return person;
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
                return null;
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, Person person)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("Person_delete", user.DatabaseName))
                {
                    command.BeginTransaction("Person_delete");

                    command.AddParameterWithValue("@personId", person.PersonId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected PersonCollection MapToPersonCollection(IDataReader reader)
        {
            PersonCollection collection = new PersonCollection();

            while (reader.Read())
                collection.Add(MapToPerson(reader));

            return collection;
        }
        protected Person MapToPerson(IDataReader reader)
        {
            Person item = new Person();
            base.MapToBase(item, reader);

            item.PersonId = (long)CleanDataValue(reader[ColumnNames.PersonId]);
            item.TitleCode = (String)CleanDataValue(reader[ColumnNames.TitleCode]);
            item.GenderCode = (String)CleanDataValue(reader[ColumnNames.GenderCode]);
            item.LastName = (String)CleanDataValue(reader[ColumnNames.LastName]);
            item.MiddleName = (String)CleanDataValue(reader[ColumnNames.MiddleName]);
            item.FirstName = (String)CleanDataValue(reader[ColumnNames.FirstName]);
            item.GovernmentIdentificationNumberTypeCode = (String)CleanDataValue(reader[ColumnNames.GovernmentIdentificationNumberTypeCode]);
            item.GovernmentIdentificationNumber1 = (String)CleanDataValue(reader[ColumnNames.GovernmentIdentificationNumber1]);
            item.GovernmentIdentificationNumber2 = (String)CleanDataValue(reader[ColumnNames.GovernmentIdentificationNumber2]);
            item.GovernmentIdentificationNumber3 = (String)CleanDataValue(reader[ColumnNames.GovernmentIdentificationNumber3]);
            item.BirthDate = (DateTime?)CleanDataValue(reader[ColumnNames.BirthDate]);
            item.KnownAsName = (String)CleanDataValue(reader[ColumnNames.KnownAsName]);
            item.LanguageCode = (String)CleanDataValue(reader[ColumnNames.LanguageCode]);
            item.DisabledFlag = (bool)CleanDataValue(reader[ColumnNames.DisabledFlag]);
            item.StudentFlag = (bool)CleanDataValue(reader[ColumnNames.StudentFlag]);
            item.SmokerCode = (String)CleanDataValue(reader[ColumnNames.SmokerCode]);

            return item;
        }
        #endregion
    }
}