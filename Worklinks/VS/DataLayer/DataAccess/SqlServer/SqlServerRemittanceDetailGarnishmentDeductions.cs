﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerRemittanceDetailGarnishmentDeductions : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String DummyKey = "row_number";
            public static String GarnishmentOrderNumber = "garnishment_order_number";
            public static String EmployeeName = "employee_name";
            public static String ChequeDate = "cheque_date";
            public static String CodePaycodeCd = "code_paycode_cd";
            public static String Amount = "amount";
            public static String Period = "period";
            public static String StartDate = "start_date";
            public static String CutoffDate = "cutoff_date";
            public static String PayrollProcessId = "payroll_process_id";
        }
        #endregion

        #region main
        internal RemittanceDetailGarnishmentDeductionsCollection Select(DatabaseUser user, string detailType, long? exportId)
        {
            DataBaseCommand command = GetStoredProcCommand("RemittanceDetailGarnishment_report", user.DatabaseName);

            command.AddParameterWithValue("@detailType", detailType);
            command.AddParameterWithValue("@exportId", exportId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToRemittanceDetailGarnishmentDeductionsCollection(reader);
        }
        #endregion

        #region data mapping
        protected RemittanceDetailGarnishmentDeductionsCollection MapToRemittanceDetailGarnishmentDeductionsCollection(IDataReader reader)
        {
            RemittanceDetailGarnishmentDeductionsCollection collection = new RemittanceDetailGarnishmentDeductionsCollection();

            while (reader.Read())
                collection.Add(MapToRemittanceDetailGarnishmentDeductions(reader));

            return collection;
        }
        protected RemittanceDetailGarnishmentDeductions MapToRemittanceDetailGarnishmentDeductions(IDataReader reader)
        {
            RemittanceDetailGarnishmentDeductions item = new RemittanceDetailGarnishmentDeductions();

            item.DummyKey = (long)CleanDataValue(reader[ColumnNames.DummyKey]);
            item.GarnishmentOrderNumber = (String)CleanDataValue(reader[ColumnNames.GarnishmentOrderNumber]);
            item.EmployeeName = (String)CleanDataValue(reader[ColumnNames.EmployeeName]);            
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.CodePaycodeCd = (String)CleanDataValue(reader[ColumnNames.CodePaycodeCd]);            
            item.Amount = (Decimal)CleanDataValue(reader[ColumnNames.Amount]);
            item.Period = (Decimal)CleanDataValue(reader[ColumnNames.Period]);
            item.StartDate = (DateTime)CleanDataValue(reader[ColumnNames.StartDate]);
            item.CutoffDate = (DateTime)CleanDataValue(reader[ColumnNames.CutoffDate]);
            item.PayrollProcessId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessId]);

            return item;
        }
        #endregion
    }
}