﻿using System;
using System.Data;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerChequeHealthTaxRemittance : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string CodeProvinceStateCd = "code_province_state_cd";
            public static string HealthRemittanceAccountNumber = "health_remittance_account_number";
            public static string Earnings = "earnings";
            public static string Premium = "premium";
            public static string RemittanceDate = "remittance_date";
            public static string VendorName = "vendor_name";
            public static string VendorAddressLine1 = "vendor_address_line_1";
            public static string VendorAddressLine2 = "vendor_address_line_2";
            public static string VendorCity = "vendor_city";
            public static string VendorCodeProvinceStateCd = "vendor_code_province_state_cd";
            public static string VendorPostalZipCode = "vendor_postal_zip_code";
            public static string VendorCodeCountryCd = "vendor_code_country_cd";

            //used with GetChequeHealthTaxRemittanceSchedule method
            public static string ReportingPeriodStartDate = "reporting_period_start_date";
            public static string ReportingPeriodEndDate = "reporting_period_end_date";
            public static string CodeWcbChequeRemittanceFrequencyCd = "code_wcb_cheque_remittance_frequency_cd";
            public static string HealthTaxId = "health_tax_id";
            public static string CodeHealthTaxCd = "code_health_tax_cd";
        }
        #endregion

        #region main

        public void UpdateChequeHealthTaxRemittanceChequeHealthTaxExportIdColumn(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, DateTime remitDate, long chequeHealthTaxExportId, long healthTaxId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ChequeHealthTaxRemittance_updateChequeHealthTaxExportId", user.DatabaseName))
            {
                command.BeginTransaction("ChequeHealthTaxRemittance_updateChequeHealthTaxExportId");

                command.AddParameterWithValue("@healthTaxId", healthTaxId);
                command.AddParameterWithValue("@chequeHealthTaxExportId", chequeHealthTaxExportId);
                command.AddParameterWithValue("@remittanceDate", remitDate);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        public ChequeHealthTaxRemittanceSummaryCollection GetRbcChequeHealthTaxRemittanceFileData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ChequeHealthTaxRemittanceSchedule remit)
        {
            DataBaseCommand command = GetStoredProcCommand("ChequeHealthTaxGetRemittanceSummary_select", user.DatabaseName);

            command.AddParameterWithValue("@remittanceDate", remit.RemittanceDate);
            command.AddParameterWithValue("@healthTaxId", remit.HealthTaxId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToChequeHealthTaxRemittanceSummaryCollection(reader);
            }
        }

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, decimal periodYear)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ChequeHealthTaxRemittance_Insert", user.DatabaseName))
            {
                command.BeginTransaction("ChequeHealthTaxRemittance_Insert");

                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                command.AddParameterWithValue("@periodYear", periodYear);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        public ChequeHealthTaxRemittanceScheduleCollection GetChequeHealthTaxRemittanceSchedule(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, DateTime date)
        {
            DataBaseCommand command = GetStoredProcCommand("ChequeHealthTaxGetSchedule_select", user.DatabaseName);

            command.AddParameterWithValue("@date", date);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToChequeHealthTaxRemittanceScheduleCollection(reader);
            }
        }
        #endregion


        #region data mapping

        protected ChequeHealthTaxRemittanceScheduleCollection MapToChequeHealthTaxRemittanceScheduleCollection(IDataReader reader)
        {
            ChequeHealthTaxRemittanceScheduleCollection collection = new ChequeHealthTaxRemittanceScheduleCollection();
            while (reader.Read())
            {
                collection.Add(MapToChequeHealthTaxRemittanceSchedule(reader));
            }

            return collection;
        }

        protected ChequeHealthTaxRemittanceSchedule MapToChequeHealthTaxRemittanceSchedule(IDataReader reader)
        {
            ChequeHealthTaxRemittanceSchedule item = new ChequeHealthTaxRemittanceSchedule();

            item.HealthTaxId = (long)CleanDataValue(reader[ColumnNames.HealthTaxId]);
            item.CodeProvinceStateCd = (string)CleanDataValue(reader[ColumnNames.CodeProvinceStateCd]);
            item.RemittanceDate = (DateTime)CleanDataValue(reader[ColumnNames.RemittanceDate]);
            item.ReportingPeriodStartDate = (DateTime)CleanDataValue(reader[ColumnNames.ReportingPeriodStartDate]);
            item.ReportingPeriodEndDate = (DateTime)CleanDataValue(reader[ColumnNames.ReportingPeriodEndDate]);
            item.CodeWcbChequeRemittanceFrequencyCd = (string)CleanDataValue(reader[ColumnNames.CodeWcbChequeRemittanceFrequencyCd]);
            item.CodeHealthTaxCd = (string)CleanDataValue(reader[ColumnNames.CodeHealthTaxCd]);

            return item;
        }

        protected ChequeHealthTaxRemittanceSummaryCollection MapToChequeHealthTaxRemittanceSummaryCollection(IDataReader reader)
        {
            ChequeHealthTaxRemittanceSummaryCollection collection = new ChequeHealthTaxRemittanceSummaryCollection();
            while (reader.Read())
            {
                collection.Add(ChequeHealthTaxRemittanceSummary(reader));
            }

            return collection;
        }

        protected ChequeHealthTaxRemittanceSummary ChequeHealthTaxRemittanceSummary(IDataReader reader)
        {
            ChequeHealthTaxRemittanceSummary item = new ChequeHealthTaxRemittanceSummary();

            item.HealthTaxId = (long)CleanDataValue(reader[ColumnNames.HealthTaxId]);
            item.CodeProvinceStateCd = (string)CleanDataValue(reader[ColumnNames.CodeProvinceStateCd]);
            item.HealthRemittanceAccountNumber = (string)CleanDataValue(reader[ColumnNames.HealthRemittanceAccountNumber]);
            item.Earnings = (decimal)CleanDataValue(reader[ColumnNames.Earnings]);
            item.Premium = (decimal)CleanDataValue(reader[ColumnNames.Premium]);
            item.RemittanceDate = (DateTime)CleanDataValue(reader[ColumnNames.RemittanceDate]);
            item.VendorName = (string)CleanDataValue(reader[ColumnNames.VendorName]);
            item.VendorAddressLine1 = (string)CleanDataValue(reader[ColumnNames.VendorAddressLine1]);
            item.VendorAddressLine2 = (string)CleanDataValue(reader[ColumnNames.VendorAddressLine2]);
            item.VendorCity = (string)CleanDataValue(reader[ColumnNames.VendorCity]);
            item.VendorCodeProvinceStateCd = (string)CleanDataValue(reader[ColumnNames.VendorCodeProvinceStateCd]);
            item.VendorPostalZipCode = (string)CleanDataValue(reader[ColumnNames.VendorPostalZipCode]);
            item.VendorCodeCountryCd = (string)CleanDataValue(reader[ColumnNames.VendorCodeCountryCd]);

            return item;
        }
        #endregion
    }
}
