﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerPayrollPeriod : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayrollPeriodId = "payroll_period_id";
            public static String PeriodYear = "period_year";
            public static String Period = "period";
            public static String StartDate = "start_date";
            public static String CutoffDate = "cutoff_date";
            public static String PayrollProcessGroupCode = "code_payroll_process_group_cd";
            public static String PPCurrentPayrollYear = "current_payroll_year";
            public static String CodePayrollProcessGroupCountryCd = "code_payroll_process_group_country_cd";
            public static String CodeCountryCd = "code_country_cd";
        }
        #endregion

        #region main
        internal PayrollPeriodCollection Select(DatabaseUser user, PayrollPeriodCriteria criteria)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollPeriod_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@payrollPeriodId", criteria.PayrollPeriodId);
                command.AddParameterWithValue("@payrollProcessGroupCode", criteria.PayrollProcessGroupCode);
                command.AddParameterWithValue("@getMinOpenPeriodFlag", criteria.GetMinOpenPeriodFlag);
                command.AddParameterWithValue("@getMaxClosedPeriodFlag", criteria.GetMaxClosedPeriodFlag);

                PayrollPeriodCollection payrollPeriods = null;

                using (IDataReader reader = command.ExecuteReader())
                    payrollPeriods = MapToPayrollPeriodCollection(reader);

                return payrollPeriods;
            }
        }
        internal Decimal SelectCurrentPayrollYear(DatabaseUser user)
        {
            using (DataBaseCommand command = GetStoredProcCommand("GetCurrentPayrollYear_select", user.DatabaseName))
            {
                using (IDataReader reader = command.ExecuteReader())
                    return MapToPayrollYear(reader);
            }
        }
        public void Insert(DatabaseUser user, PayrollPeriod item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollPeriod_insert", user.DatabaseName))
            {
                command.BeginTransaction("PayrollPeriod_insert");

                SqlParameter PayrollPeriodIdParm = command.AddParameterWithValue("@PayrollPeriodId", item.PayrollPeriodId, ParameterDirection.Output);
                command.AddParameterWithValue("@periodYear", item.PeriodYear);
                command.AddParameterWithValue("@period", item.Period);
                command.AddParameterWithValue("@startDate", item.StartDate);
                command.AddParameterWithValue("@cutoffdate", item.CutoffDate);
                command.AddParameterWithValue("@payrollProcessGroupCode", item.PayrollProcessGroupCode);
                command.AddParameterWithValue("@processedDate", item.ProcessedDate);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", item.UpdateUser);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.PayrollPeriodId = Convert.ToInt64(PayrollPeriodIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }
        public void Update(DatabaseUser user, PayrollPeriod item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollPeriod_update", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollPeriod_update");

                    command.AddParameterWithValue("@payrollPeriodId", item.PayrollPeriodId);
                    command.AddParameterWithValue("@periodYear", item.PeriodYear);
                    command.AddParameterWithValue("@period", item.Period);
                    command.AddParameterWithValue("@startDate", item.StartDate);
                    command.AddParameterWithValue("@cutoffdate", item.CutoffDate);
                    command.AddParameterWithValue("@payrollProcessGroupCode", item.PayrollProcessGroupCode);
                    command.AddParameterWithValue("@processedDate", item.ProcessedDate);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void ClosePeriod(DatabaseUser user, PayrollPeriod item, long payrollProcessId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("ClosePeriod_update", user.DatabaseName))
                {
                    command.BeginTransaction("ClosePeriod_update");

                    command.AddParameterWithValue("@payrollPeriodId", item.PayrollPeriodId);
                    command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                    command.AddParameterWithValue("@periodYear", item.PeriodYear);
                    command.AddParameterWithValue("@period", item.Period);
                    command.AddParameterWithValue("@startDate", item.StartDate);
                    command.AddParameterWithValue("@cutoffdate", item.CutoffDate);
                    command.AddParameterWithValue("@payrollProcessGroupCode", item.PayrollProcessGroupCode);
                    command.AddParameterWithValue("@processedDate", item.ProcessedDate);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, PayrollPeriod item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollPeriod_delete", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollPeriod_delete");

                    command.AddParameterWithValue("@payrollPeriodId", item.PayrollPeriodId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected PayrollPeriodCollection MapToPayrollPeriodCollection(IDataReader reader)
        {
            PayrollPeriodCollection collection = new PayrollPeriodCollection();

            while (reader.Read())
                collection.Add(MapToPayrollPeriod(reader));

            return collection;
        }
        protected PayrollPeriod MapToPayrollPeriod(IDataReader reader)
        {
            PayrollPeriod item = new PayrollPeriod();
            base.MapToBase(item, reader);

            item.PayrollPeriodId = (long)CleanDataValue(reader[ColumnNames.PayrollPeriodId]);
            item.PeriodYear = (Decimal)CleanDataValue(reader[ColumnNames.PeriodYear]);
            item.Period = (Decimal)CleanDataValue(reader[ColumnNames.Period]);
            item.StartDate = (DateTime)CleanDataValue(reader[ColumnNames.StartDate]);
            item.CutoffDate = (DateTime)CleanDataValue(reader[ColumnNames.CutoffDate]);
            item.PPCurrentPayrollYear = (Decimal)CleanDataValue(reader[ColumnNames.PPCurrentPayrollYear]);
            item.PayrollProcessGroupCode = (String)CleanDataValue(reader[ColumnNames.PayrollProcessGroupCode]);
            item.CodePayrollProcessGroupCountryCd = (String)CleanDataValue(reader[ColumnNames.CodePayrollProcessGroupCountryCd]);
            item.CodeCountryCd = (String)CleanDataValue(reader[ColumnNames.CodeCountryCd]);

            return item;
        }
        protected Decimal MapToPayrollYear(IDataReader reader)
        {
            Decimal year = 0;

            while (reader.Read())
                year = (Decimal)CleanDataValue(reader[ColumnNames.PPCurrentPayrollYear]);

            return year;
        }
        #endregion
    }
}