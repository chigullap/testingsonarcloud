﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerBenefitPolicyPlan : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String BenefitPolicyPlanId = "benefit_policy_plan_id";
            public static String BenefitPolicyId = "benefit_policy_id";
            public static String BenefitPlanId = "benefit_plan_id";
        }
        #endregion

        #region main
        internal BenefitPolicyPlanCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long benefitPolicyId)
        {
            DataBaseCommand command = GetStoredProcCommand("BenefitPolicyPlan_select", user.DatabaseName);

            command.AddParameterWithValue("@benefitPolicyId", benefitPolicyId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToBenefitPolicyPlanCollection(reader);
        }

        public BenefitPolicyPlan Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPolicyPlan benefitPolicyPlan)
        {
            using (DataBaseCommand command = GetStoredProcCommand("BenefitPolicyPlan_insert", user.DatabaseName))
            {
                command.BeginTransaction("BenefitPolicyPlan_insert");

                SqlParameter benefitPolicyPlanIdParam = command.AddParameterWithValue("@benefitPolicyPlanId", benefitPolicyPlan.BenefitPolicyPlanId, ParameterDirection.Output);
                command.AddParameterWithValue("@benefitPolicyId", benefitPolicyPlan.BenefitPolicyId);
                command.AddParameterWithValue("@benefitPlanId", benefitPolicyPlan.BenefitPlanId);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", benefitPolicyPlan.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                benefitPolicyPlan.BenefitPolicyPlanId = Convert.ToInt64(benefitPolicyPlanIdParam.Value);
                benefitPolicyPlan.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return benefitPolicyPlan;
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPolicyPlan benefitPolicyPlan)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("BenefitPolicyPlan_update", user.DatabaseName))
                {
                    command.BeginTransaction("BenefitPolicyPlan_update");

                    command.AddParameterWithValue("@benefitPolicyPlanId", benefitPolicyPlan.BenefitPolicyPlanId);
                    command.AddParameterWithValue("@benefitPolicyId", benefitPolicyPlan.BenefitPolicyId);
                    command.AddParameterWithValue("@benefitPlanId", benefitPolicyPlan.BenefitPlanId);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", benefitPolicyPlan.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    benefitPolicyPlan.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long benefitPolicyId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("BenefitPolicyPlan_delete", user.DatabaseName))
                {
                    command.BeginTransaction("BenefitPolicyPlan_delete");

                    command.AddParameterWithValue("@benefitPolicyId", benefitPolicyId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected BenefitPolicyPlanCollection MapToBenefitPolicyPlanCollection(IDataReader reader)
        {
            BenefitPolicyPlanCollection collection = new BenefitPolicyPlanCollection();

            while (reader.Read())
                collection.Add(MapToBenefitPlanDetail(reader));

            return collection;
        }

        protected BenefitPolicyPlan MapToBenefitPlanDetail(IDataReader reader)
        {
            BenefitPolicyPlan item = new BenefitPolicyPlan();
            base.MapToBase(item, reader);

            item.BenefitPolicyPlanId = (long)CleanDataValue(reader[ColumnNames.BenefitPolicyPlanId]);
            item.BenefitPolicyId = (long)CleanDataValue(reader[ColumnNames.BenefitPolicyId]);
            item.BenefitPlanId = (long)CleanDataValue(reader[ColumnNames.BenefitPlanId]);

            return item;
        }
        #endregion
    }
}
