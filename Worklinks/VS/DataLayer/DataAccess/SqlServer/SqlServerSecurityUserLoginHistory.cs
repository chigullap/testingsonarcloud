﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerSecurityUserLoginHistory : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String SecurityUserLoginHistoryid = "security_user_login_history_id";
            public static String UserName = "user_name";
            public static String LoginAttemptDatetime = "login_attempt_datetime";
            public static String Browser = "browser";
            public static String BrowserVersion = "browser_version";
            public static String IpAddress = "ip_address";
            public static String AuthenticatedFlag = "authenticated_flag";
            public static String Platform = "platform";
            public static String Version = "version";
            public static String Resolution = "resolution";
        }
        #endregion

        #region main

        internal SecurityUserLoginHistoryCollection Select(String databaseName, String userName, int passwordAttemptWindow)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SecurityUserLoginHistory_select", databaseName))
            {
                command.AddParameterWithValue("@userName", userName);
                command.AddParameterWithValue("@passwordAttemptWindow", passwordAttemptWindow);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToSecurityUserLoginHistoryCollection(reader);
                }
            }
        }

        public void Insert(String databaseName, SecurityUserLoginHistory item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SecurityUserLoginHistory_insert", databaseName))
            {
                command.BeginTransaction("SecurityUserLoginHistory_insert");

                SqlParameter contactIdParm = command.AddParameterWithValue("@securityUserLoginHistoryId", item.SecurityUserLoginHistoryid, ParameterDirection.Output);

                command.AddParameterWithValue("@userName", item.UserName);
                command.AddParameterWithValue("@loginAttemptDatetime", item.LoginAttemptDatetime);
                command.AddParameterWithValue("@browser", item.Browser);
                command.AddParameterWithValue("@browserVersion", item.BrowserVersion);
                command.AddParameterWithValue("@ip_address", item.IpAddress);
                command.AddParameterWithValue("@authenticatedFlag", item.AuthenticatedFlag);
                command.AddParameterWithValue("@platform", item.Platform);
                command.AddParameterWithValue("@version", item.Version);
                command.AddParameterWithValue("@resolution", item.Resolution);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", item.UpdateUser);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                item.SecurityUserLoginHistoryid = Convert.ToInt64(contactIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();
            }
        }

        #endregion


        #region data mapping
        protected SecurityUserLoginHistoryCollection MapToSecurityUserLoginHistoryCollection(IDataReader reader)
        {
            SecurityUserLoginHistoryCollection collection = new SecurityUserLoginHistoryCollection();
            while (reader.Read())
            {
                collection.Add(MapToSecurityUserLoginHistory(reader));
            }

            return collection;
        }
        protected SecurityUserLoginHistory MapToSecurityUserLoginHistory(IDataReader reader)
        {
            SecurityUserLoginHistory item = new SecurityUserLoginHistory();
            base.MapToBase(item, reader,false);
            item.SecurityUserLoginHistoryid = (long)CleanDataValue(reader[ColumnNames.SecurityUserLoginHistoryid]);
            item.UserName = (String)CleanDataValue(reader[ColumnNames.UserName]);
            item.LoginAttemptDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.LoginAttemptDatetime]);
            item.Browser = (String)CleanDataValue(reader[ColumnNames.Browser]);
            item.BrowserVersion = (String)CleanDataValue(reader[ColumnNames.BrowserVersion]);
            item.IpAddress = (String)CleanDataValue(reader[ColumnNames.IpAddress]);
            item.AuthenticatedFlag = (bool)CleanDataValue(reader[ColumnNames.AuthenticatedFlag]);
            item.Platform = (String)CleanDataValue(reader[ColumnNames.Platform]);
            item.Version = (String)CleanDataValue(reader[ColumnNames.Version]);
            item.Resolution = (String)CleanDataValue(reader[ColumnNames.Resolution]);

            return item;
        }

        #endregion
    }
}
