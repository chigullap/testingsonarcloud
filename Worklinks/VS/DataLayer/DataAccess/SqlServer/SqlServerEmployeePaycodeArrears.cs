﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeePaycodeArrears : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeePaycodeArrearsId = "employee_paycode_Arrears_id";
            public static String EmployeeId = "employee_id";
            public static String TransactionDate = "transaction_date";
            public static String CodePaycodeCd = "code_paycode_cd";
            public static String CodeEntryTypeCd = "code_entry_type_cd";
            public static String Units = "units";
            public static String Rate = "rate";
            public static String EmploymentInsuranceHour = "employment_insurance_hour";
            public static String PayrollTransactionId = "payroll_transaction_id";
            public static String CodeEmployeePaycodeArrearsStatusCd = "code_employee_paycode_arrears_status_cd";
            public static String OriginatingPayrollProcessId = "originating_payroll_process_id";
            public static String ProcessingEmployeePositionId = "processing_employee_position_id";
        }
        #endregion

        #region select
        internal EmployeePaycodeArrearsCollection Select(DatabaseUser user, long employeeId, long? employeePaycodeArrearsId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeePaycodeArrears_select", user.DatabaseName);

            command.AddParameterWithValue("@employeeId", employeeId);
            command.AddParameterWithValue("@employeePaycodeArrearsId", employeePaycodeArrearsId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToEmployeePaycodeArrearsCollection(reader);
        }
        public EmployeePaycodeArrearsCollection SelectBatch(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePaycodeArrears_batchSelect", user.DatabaseName))
            {
                SqlParameter parmEmployeePaycodeArrears = new SqlParameter();
                parmEmployeePaycodeArrears.ParameterName = "@parmEmployeePaycodeArrears";
                parmEmployeePaycodeArrears.SqlDbType = SqlDbType.Structured;

                DataTable tableEmployeePaycodeArrears = LoadEmployeePaycodeArrearsDataTable(importExternalIdentifiers);
                parmEmployeePaycodeArrears.Value = tableEmployeePaycodeArrears;
                command.AddParameter(parmEmployeePaycodeArrears);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToEmployeePaycodeArrearsCollection(reader);
            }
        }
        private DataTable LoadEmployeePaycodeArrearsDataTable(List<String> importExternalIdentifiers)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("import_external_identifier", typeof(String));

            for (int i = 0; i < importExternalIdentifiers.Count; i++)
            {
                table.Rows.Add(new Object[]
                {
                    importExternalIdentifiers[i]
                });
            }

            return table;
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, EmployeePaycodeArrears item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePaycodeArrears_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePaycodeArrears_update");

                    command.AddParameterWithValue("@employeePaycodeArrearsId", item.EmployeePaycodeArrearsId);
                    command.AddParameterWithValue("@employeeId", item.EmployeeId);
                    command.AddParameterWithValue("@transactionDate", item.TransactionDate);
                    command.AddParameterWithValue("@codePaycodeCd", item.PaycodeCode);
                    command.AddParameterWithValue("@codeEntryTypeCd", item.EntryTypeCode);
                    command.AddParameterWithValue("@units", item.Units);
                    command.AddParameterWithValue("@rate", item.Rate);
                    command.AddParameterWithValue("@employmentInsuranceHour", item.EmploymentInsuranceHour);
                    command.AddParameterWithValue("@payrollTransactionId", item.PayrollTransactionId);
                    command.AddParameterWithValue("@codeEmployeePaycodeArrearsStatusCd", item.CodeEmployeePaycodeArrearsStatusCd);
                    command.AddParameterWithValue("@originatingPayrollProcessId", item.OriginatingPayrollProcessId);
                    command.AddParameterWithValue("@processingEmployeePositionId", item.ProcessingEmployeePositionId);

                    //these are passed to the procs, but not used
                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region insert

        //commented out 2017-09-11 by ChaissonS because this is not used in c# and parms are not correct.

        //public void Insert(DatabaseUser user, EmployeePaycodeArrears item)
        //{
        //    using (DataBaseCommand command = GetStoredProcCommand("EmployeePaycodeArrears_insert", user.DatabaseName))
        //    {
        //        command.BeginTransaction("EmployeePaycodeArrears_insert");

        //        SqlParameter payrollTransactionIdParam = command.AddParameterWithValue("@employeePaycodeArrearsId", item.EmployeePaycodeArrearsId, ParameterDirection.Output);
        //        command.AddParameterWithValue("@employeeId", item.EmployeeId);
        //        command.AddParameterWithValue("@codePaycodeCd", item.PaycodeCode);
        //        command.AddParameterWithValue("@rate", item.Rate);
        //        command.AddParameterWithValue("@units", item.Units);
        //        command.AddParameterWithValue("@codeEntryTypeCd", item.EntryTypeCode);
        //        command.AddParameterWithValue("@transactionDate", item.TransactionDate);
        //        command.AddParameterWithValue("@employmentInsuranceHour", item.EmploymentInsuranceHour);
        //        command.AddParameterWithValue("@payrollTransactionId", item.PayrollTransactionId);
        //        command.AddParameterWithValue("@codeEmployeePaycodeArrearsStatusCd", item.CodeEmployeePaycodeArrearsStatusCd);
        //        command.AddParameterWithValue("@payrollProcessId", item.PayrollProcessId);
        //        command.AddParameterWithValue("@originatingPayrollProcessId", item.OriginatingPayrollProcessId);
        //        command.AddParameterWithValue("@processingEmployeePositionId", item.ProcessingEmployeePositionId);

        //        //these are passed to the procs, but not used
        //        SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
        //        command.AddParameterWithValue("@updateUser", user.UserName);
        //        command.AddParameterWithValue("@updateDatetime", Time);

        //        command.ExecuteNonQuery();

        //        item.PayrollTransactionId = Convert.ToInt64(payrollTransactionIdParam.Value);
        //        item.RowVersion = (byte[])rowVersionParm.Value;

        //        command.CommitTransaction();
        //    }
        //}
        #endregion

        #region data mapping
        protected EmployeePaycodeArrearsCollection MapToEmployeePaycodeArrearsCollection(IDataReader reader)
        {
            EmployeePaycodeArrearsCollection collection = new EmployeePaycodeArrearsCollection();

            while (reader.Read())
                collection.Add(MapToEmployeePaycodeArrears(reader));

            return collection;
        }
        protected EmployeePaycodeArrears MapToEmployeePaycodeArrears(IDataReader reader)
        {
            EmployeePaycodeArrears item = new EmployeePaycodeArrears();
            base.MapToBase(item, reader);

            item.EmployeePaycodeArrearsId = (long)CleanDataValue(reader[ColumnNames.EmployeePaycodeArrearsId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.TransactionDate = (DateTime)CleanDataValue(reader[ColumnNames.TransactionDate]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.CodePaycodeCd]);
            item.EntryTypeCode = (String)CleanDataValue(reader[ColumnNames.CodeEntryTypeCd]);
            item.Units = (decimal)CleanDataValue(reader[ColumnNames.Units]);
            item.Rate = (decimal)CleanDataValue(reader[ColumnNames.Rate]);
            item.EmploymentInsuranceHour = (decimal)CleanDataValue(reader[ColumnNames.EmploymentInsuranceHour]);
            item.PayrollTransactionId = (long?)CleanDataValue(reader[ColumnNames.PayrollTransactionId]);
            item.CodeEmployeePaycodeArrearsStatusCd = (String)CleanDataValue(reader[ColumnNames.CodeEmployeePaycodeArrearsStatusCd]);
            item.OriginatingPayrollProcessId = (long)CleanDataValue(reader[ColumnNames.OriginatingPayrollProcessId]);
            item.ProcessingEmployeePositionId = (long?)CleanDataValue(reader[ColumnNames.ProcessingEmployeePositionId]);

            return item;
        }
        #endregion
    }
}