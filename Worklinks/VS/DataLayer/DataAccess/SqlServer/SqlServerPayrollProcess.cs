﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerPayrollProcess : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayrollProcessId = "payroll_process_id";
            public static String PayrollPeriodId = "payroll_period_id";
            public static String ProcessedDate = "processed_date";
            public static String ChequeDate = "cheque_date";
            public static String PayrollProcessRunTypeCode = "code_payroll_process_run_type_cd";
            public static String PayrollProcessAutoSelectPaycodeModeCode = "code_payroll_process_auto_select_paycode_mode_cd";
            public static String PreventReclaimsFlag = "prevent_reclaims_flag";
            public static String AutoPayVacationPayThisPeriodFlag = "auto_pay_vacation_pay_this_period_flag";
            public static String IgnoreCppQppExemptionFlag = "ignore_cpp_qpp_exemption_flag";
            public static String PayrollProcessStatusCode = "code_payroll_process_status_cd";
            public static String PaymentFrequencyCode = "code_payment_frequency_cd";
            public static String PayrollProcessGroupCode = "code_payroll_process_group_cd";
            public static String PeriodYear = "period_year";
            public static String Period = "period";
            public static String StartDate = "start_date";
            public static String CutoffDate = "cutoff_date";
            public static String ExportSequenceNumber = "export_sequence_number";
            public static String ProcessException = "process_exception";
            public static String CurrentPayrollYear = "current_payroll_year";
            public static String LockedFlag = "locked_flag";
            public static String FileTransmissionDate = "file_transmission_date";
            public static String EftFileTransmissionDate = "eft_file_transmission_date";
            public static String PayslipNote = "payslip_note";
            public static String UpdatedUser = "update_user";
            public static String LastCalcDate = "last_calc_date";
            public static String LastCalcUser = "last_calc_user";
            public static string PostedUser = "posted_user";

        }
        #endregion

        #region select
        internal PayrollProcessSummaryCollection SelectPayrollProcessSummary(DatabaseUser user, PayrollProcessCriteria criteria)
        {
            PayrollProcessSummaryCollection payrollProcesss = null;

            using (DataBaseCommand command = GetStoredProcCommand("PayrollProcess_report", user.DatabaseName))
            {
                command.AddParameterWithValue("@payrollProcessId", criteria.PayrollProcessId);
                command.AddParameterWithValue("@payrollProcessGroup", criteria.PayrollProcessGroupCode);
                command.AddParameterWithValue("@payrollProcessRunTypeCode", criteria.PayrollProcessRunTypeCode);
                command.AddParameterWithValue("@employeeId", criteria.EmployeeId);
                command.AddParameterWithValue("@payrollProcessStatusCode", criteria.PayrollProcessStatusCode);
                command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
                command.AddParameterWithValue("@securityUserId", user.SecurityUserId);
                command.AddParameterWithValue("@securityOverride", criteria.SecurityOverrideFlag);
                command.AddParameterWithValue("@limit", criteria.Limit);

                using (IDataReader reader = command.ExecuteReader())
                    payrollProcesss = MapToPayrollProcessSummaryCollection(reader);
            }

            return payrollProcesss;
        }
        internal PayrollProcessCollection Select(DatabaseUser user, PayrollProcessCriteria criteria)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollProcess_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@payrollProcessId", criteria.PayrollProcessId);
                command.AddParameterWithValue("@payrollPeriodId", criteria.PayrollPeriodId);
                command.AddParameterWithValue("@getLastOpenPayrollProcessFlag", criteria.GetLastOpenPayrollProcessFlag);

                PayrollProcessCollection PayrollProcesss = null;

                using (IDataReader reader = command.ExecuteReader())
                    PayrollProcesss = MapToPayrollProcessCollection(reader);

                return PayrollProcesss;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, PayrollProcess item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollProcess_update", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollProcess_update");

                    command.AddParameterWithValue("@payrollProcessId", item.PayrollProcessId);
                    command.AddParameterWithValue("@payrollPeriodId", item.PayrollPeriodId);
                    command.AddParameterWithValue("@processedDate", item.ProcessedDate);
                    command.AddParameterWithValue("@chequeDate", item.ChequeDate);
                    command.AddParameterWithValue("@payrollProcessRunTypeCode", item.PayrollProcessRunTypeCode);
                    command.AddParameterWithValue("@payrollProcessAutoSelectPaycodeModeCode", item.PayrollProcessAutoSelectPaycodeModeCode);
                    command.AddParameterWithValue("@preventReclaimsFlag", item.PreventReclaimsFlag);
                    command.AddParameterWithValue("@autoPayVacationPayThisPeriodFlag", item.AutoPayVacationPayThisPeriodFlag);
                    command.AddParameterWithValue("@ignoreCppQppExemptionFlag", item.IgnoreCppQppExemptionFlag);
                    command.AddParameterWithValue("@payrollProcessStatusCode", item.PayrollProcessStatusCode);
                    command.AddParameterWithValue("@processException", item.ProcessException);
                    command.AddParameterWithValue("@lockedFlag", item.LockedFlag);
                    command.AddParameterWithValue("@fileTransmissionDate", item.FileTransmissionDate);
                    command.AddParameterWithValue("@eftFileTransmissionDate", item.EftFileTransmissionDate);
                    command.AddParameterWithValue("@payslipNote", item.PayslipNote);
                    command.AddParameterWithValue("@postedUser", item.PostedUser);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public DateTime UpdatePayrollProcessStatusCode(DatabaseUser user, long payrollProcessId, string codePayrollProcessStatus, String payProcessException)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollProcess_updateStatus", user.DatabaseName))
            {
                command.BeginTransaction("PayrollProcess_updateStatus");

                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                command.AddParameterWithValue("@codePayrollProcessStatusCd", codePayrollProcessStatus);
                command.AddParameterWithValue("@processException", payProcessException);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();

                return Time;
            }
        }
        public void BatchUpdateEmployeePositionPayrollProcessField(DatabaseUser user, long payrollProcessId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePosition_batchUpdatePayrollProcess", user.DatabaseName))
            {
                command.BeginTransaction("EmployeePosition_batchUpdatePayrollProcess");

                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        public void RevertUpdateTerminatedEmployees(DatabaseUser user, long payrollProcessId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePosition_revertPayrollProcess", user.DatabaseName))
            {
                command.BeginTransaction("EmployeePosition_revertPayrollProcess");

                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        #endregion

        #region insert
        public void Insert(DatabaseUser user, PayrollProcess item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollProcess_insert", user.DatabaseName))
            {
                command.BeginTransaction("PayrollProcess_insert");

                SqlParameter PayrollProcessIdParm = command.AddParameterWithValue("@payrollProcessId", item.PayrollProcessId, ParameterDirection.Output);
                command.AddParameterWithValue("@payrollPeriodId", item.PayrollPeriodId);
                command.AddParameterWithValue("@processedDate", item.ProcessedDate);
                command.AddParameterWithValue("@chequeDate", item.ChequeDate);
                command.AddParameterWithValue("@payrollProcessRunTypeCode", item.PayrollProcessRunTypeCode);
                command.AddParameterWithValue("@payrollProcessAutoSelectPaycodeModeCode", item.PayrollProcessAutoSelectPaycodeModeCode);
                command.AddParameterWithValue("@preventReclaimsFlag", item.PreventReclaimsFlag);
                command.AddParameterWithValue("@autoPayVacationPayThisPeriodFlag", item.AutoPayVacationPayThisPeriodFlag);
                command.AddParameterWithValue("@ignoreCppQppExemptionFlag", item.IgnoreCppQppExemptionFlag);
                command.AddParameterWithValue("@payrollProcessStatusCode", item.PayrollProcessStatusCode);
                command.AddParameterWithValue("@lockedFlag", item.LockedFlag);
                command.AddParameterWithValue("@payslipNote", item.PayslipNote);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.PayrollProcessId = Convert.ToInt64(PayrollProcessIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }
        #endregion

        #region data mapping
        protected PayrollProcessCollection MapToPayrollProcessCollection(IDataReader reader)
        {
            PayrollProcessCollection collection = new PayrollProcessCollection();

            while (reader.Read())
                collection.Add(MapToPayrollProcess(reader));

            return collection;
        }
        protected PayrollProcessSummaryCollection MapToPayrollProcessSummaryCollection(IDataReader reader)
        {
            PayrollProcessSummaryCollection collection = new PayrollProcessSummaryCollection();

            while (reader.Read())
                collection.Add(MapToPayrollProcessSummary(reader));

            return collection;
        }
        protected PayrollProcess MapToPayrollProcess(IDataReader reader)
        {
            PayrollProcess item = new PayrollProcess();
            MapToPayrollProcess(item, reader);

            return item;
        }
        protected PayrollProcess MapToPayrollProcess(PayrollProcess item, IDataReader reader)
        {
            base.MapToBase(item, reader, true);

            item.PayrollProcessId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessId]);
            item.PayrollPeriodId = (long)CleanDataValue(reader[ColumnNames.PayrollPeriodId]);
            item.ProcessedDate = (DateTime?)CleanDataValue(reader[ColumnNames.ProcessedDate]);
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.PayrollProcessRunTypeCode = (String)CleanDataValue(reader[ColumnNames.PayrollProcessRunTypeCode]);
            item.PayrollProcessAutoSelectPaycodeModeCode = (String)CleanDataValue(reader[ColumnNames.PayrollProcessAutoSelectPaycodeModeCode]);
            item.PreventReclaimsFlag = (bool)CleanDataValue(reader[ColumnNames.PreventReclaimsFlag]);
            item.AutoPayVacationPayThisPeriodFlag = (bool)CleanDataValue(reader[ColumnNames.AutoPayVacationPayThisPeriodFlag]);
            item.IgnoreCppQppExemptionFlag = (bool)CleanDataValue(reader[ColumnNames.IgnoreCppQppExemptionFlag]);
            item.PayrollProcessStatusCode = (String)CleanDataValue(reader[ColumnNames.PayrollProcessStatusCode]);
            item.ProcessException = (String)CleanDataValue(reader[ColumnNames.ProcessException]);
            item.CurrentPayrollYear = (Decimal)CleanDataValue(reader[ColumnNames.CurrentPayrollYear]);
            item.RowVersion = (byte[])CleanDataValue(reader[ColumnNames.RowVersion]);
            item.LockedFlag = (bool)CleanDataValue(reader[ColumnNames.LockedFlag]);
            item.FileTransmissionDate = (DateTime?)CleanDataValue(reader[ColumnNames.FileTransmissionDate]);
            item.EftFileTransmissionDate = (DateTime?)CleanDataValue(reader[ColumnNames.EftFileTransmissionDate]);
            item.PayslipNote = (String)CleanDataValue(reader[ColumnNames.PayslipNote]);
            item.PostedUser = (String)CleanDataValue(reader[ColumnNames.PostedUser]);
            item.LastCalcDate = (DateTime?)CleanDataValue(reader[ColumnNames.LastCalcDate]);
            item.LastCalcUser = (String)CleanDataValue(reader[ColumnNames.LastCalcUser]);

            return item;
        }
        protected PayrollProcessSummary MapToPayrollProcessSummary(IDataReader reader)
        {
            PayrollProcessSummary item = new PayrollProcessSummary();
            MapToPayrollProcess(item, reader);

            item.PaymentFrequencyCode = (String)CleanDataValue(reader[ColumnNames.PaymentFrequencyCode]);
            item.ExportSequenceNumber = (int?)CleanDataValue(reader[ColumnNames.ExportSequenceNumber]);
            item.PayrollProcessGroupCode = (String)CleanDataValue(reader[ColumnNames.PayrollProcessGroupCode]);
            item.Period = (Decimal)CleanDataValue(reader[ColumnNames.Period]);
            item.PeriodYear = (Decimal)CleanDataValue(reader[ColumnNames.PeriodYear]);
            item.StartDate = (DateTime)CleanDataValue(reader[ColumnNames.StartDate]);
            item.CutoffDate = (DateTime)CleanDataValue(reader[ColumnNames.CutoffDate]);

            return item;
        }
        #endregion
    }
}