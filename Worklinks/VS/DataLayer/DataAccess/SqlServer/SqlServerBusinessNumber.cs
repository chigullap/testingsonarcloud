﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerBusinessNumber : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string BusinessNumberId = "business_number_id";
            public static string Description = "description";
            public static string EmployerNumber = "employer_number";
            public static string EmployerName = "employer_name";
            public static string EmployerAddressId = "employer_address_id";
            public static string BusinessTaxNumber = "business_tax_number";
            public static string RevenuQuebecBusinessTaxNumber = "revenu_quebec_business_tax_number";
            public static string CodeCraRemittancePeriodCd = "code_cra_remittance_period_cd";
            public static string Rate = "employment_insurance_rate";
            public static string CodePreAuthorizedBankCd = "code_pre_authorized_bank_cd";
            public static string PreAuthorizedTransitNumber = "pre_authorized_transit_number";
            public static string PreAuthorizedAccountNumber = "pre_authorized_account_number";
            public static string AbaNumber = "aba_number";
            public static string CodeRevenuQuebecRemittancePeriodCd = "code_revenu_quebec_remittance_period_cd";
        }
        #endregion

        #region main
        internal BusinessNumberCollection Select(DatabaseUser user, long? businessNumberId)
        {
            DataBaseCommand command = GetStoredProcCommand("BusinessNumber_select", user.DatabaseName);

            command.AddParameterWithValue("@businessNumberId", businessNumberId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToBusinessNumberCollection(reader);
        }

        public String SelectByEmployerNumber(DatabaseUser user, String employerNumber)
        {
            DataBaseCommand command = GetStoredProcCommand("T4BusinessTaxNumber_select", user.DatabaseName);

            command.AddParameterWithValue("@databaseName", null);
            command.AddParameterWithValue("@employeeNumber", null);
            command.AddParameterWithValue("@year", null);
            command.AddParameterWithValue("@employerNumber", employerNumber);
            command.AddParameterWithValue("@provinceCode", null);

            using (IDataReader reader = command.ExecuteReader())
                return MapTheBusinessNumber(reader);
        }

        public BusinessNumber Insert(DatabaseUser user, BusinessNumber businessNumber)
        {
            using (DataBaseCommand command = GetStoredProcCommand("BusinessNumber_insert", user.DatabaseName))
            {
                command.BeginTransaction("BusinessNumber_insert");

                SqlParameter businessNumberIdParm = command.AddParameterWithValue("@businessNumberId", businessNumber.BusinessNumberId, ParameterDirection.Output);
                command.AddParameterWithValue("@description", businessNumber.Description);
                command.AddParameterWithValue("@employerNumber", businessNumber.EmployerNumber);
                command.AddParameterWithValue("@employerName", businessNumber.EmployerName);
                command.AddParameterWithValue("@employerAddressId", businessNumber.EmployerAddressId);
                command.AddParameterWithValue("@businessTaxNumber", businessNumber.BusinessTaxNumber);
                command.AddParameterWithValue("@codePreAuthorizedBankCd", businessNumber.CodePreAuthorizedBankCd);
                command.AddParameterWithValue("@preAuthorizedTransitNumber", businessNumber.PreAuthorizedTransitNumber);
                command.AddParameterWithValue("@preAuthorizedAccountNumber", businessNumber.PreAuthorizedAccountNumber);
                command.AddParameterWithValue("@abaNumber", businessNumber.AbaNumber);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", businessNumber.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                businessNumber.BusinessNumberId = Convert.ToInt64(businessNumberIdParm.Value);
                businessNumber.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return businessNumber;
            }
        }

        public void Update(DatabaseUser user, BusinessNumber businessNumber)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("BusinessNumber_update", user.DatabaseName))
                {
                    command.BeginTransaction("BusinessNumber_update");

                    command.AddParameterWithValue("@businessNumberId", businessNumber.BusinessNumberId);
                    command.AddParameterWithValue("@description", businessNumber.Description);
                    command.AddParameterWithValue("@employerNumber", businessNumber.EmployerNumber);
                    command.AddParameterWithValue("@employerName", businessNumber.EmployerName);
                    command.AddParameterWithValue("@employerAddressId", businessNumber.EmployerAddressId);
                    command.AddParameterWithValue("@businessTaxNumber", businessNumber.BusinessTaxNumber);
                    command.AddParameterWithValue("@codePreAuthorizedBankCd", businessNumber.CodePreAuthorizedBankCd);
                    command.AddParameterWithValue("@preAuthorizedTransitNumber", businessNumber.PreAuthorizedTransitNumber);
                    command.AddParameterWithValue("@preAuthorizedAccountNumber", businessNumber.PreAuthorizedAccountNumber);
                    command.AddParameterWithValue("@abaNumber", businessNumber.AbaNumber);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", businessNumber.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    businessNumber.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Delete(DatabaseUser user, BusinessNumber businessNumber)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("BusinessNumber_delete", user.DatabaseName))
                {
                    command.BeginTransaction("BusinessNumber_delete");

                    command.AddParameterWithValue("@businessNumberId", businessNumber.BusinessNumberId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected BusinessNumberCollection MapToBusinessNumberCollection(IDataReader reader)
        {
            BusinessNumberCollection collection = new BusinessNumberCollection();

            while (reader.Read())
                collection.Add(MapToBusinessNumber(reader));

            return collection;
        }
        protected BusinessNumber MapToBusinessNumber(IDataReader reader)
        {
            BusinessNumber item = new BusinessNumber();
            base.MapToBase(item, reader);

            item.BusinessNumberId = (long)CleanDataValue(reader[ColumnNames.BusinessNumberId]);
            item.Description = (string)CleanDataValue(reader[ColumnNames.Description]);
            item.EmployerNumber = (string)CleanDataValue(reader[ColumnNames.EmployerNumber]);
            item.EmployerName = (string)CleanDataValue(reader[ColumnNames.EmployerName]);
            item.EmployerAddressId = (long)CleanDataValue(reader[ColumnNames.EmployerAddressId]);
            item.BusinessTaxNumber = (string)CleanDataValue(reader[ColumnNames.BusinessTaxNumber]);
            item.RevenuQuebecBusinessTaxNumber = (string)CleanDataValue(reader[ColumnNames.RevenuQuebecBusinessTaxNumber]);
            item.CodeCraRemittancePeriodCd = (string)CleanDataValue(reader[ColumnNames.CodeCraRemittancePeriodCd]);
            item.Rate = (Decimal)CleanDataValue(reader[ColumnNames.Rate]);
            item.CodePreAuthorizedBankCd = (string)CleanDataValue(reader[ColumnNames.CodePreAuthorizedBankCd]);
            item.PreAuthorizedTransitNumber = (string)CleanDataValue(reader[ColumnNames.PreAuthorizedTransitNumber]);
            item.PreAuthorizedAccountNumber = (string)CleanDataValue(reader[ColumnNames.PreAuthorizedAccountNumber]);
            item.AbaNumber = (string)CleanDataValue(reader[ColumnNames.AbaNumber]);
            item.CodeRevenuQuebecRemittancePeriodCd = (string)CleanDataValue(reader[ColumnNames.CodeRevenuQuebecRemittancePeriodCd]);

            return item;
        }
        protected String MapTheBusinessNumber(IDataReader reader)
        {
            String businessNumber = "";

            while (reader.Read())
                businessNumber = Map(reader);

            return businessNumber;
        }
        protected String Map(IDataReader reader)
        {
            String item = (String)CleanDataValue(reader["business_tax_number"]);
            return item;
        }
        #endregion
    }
}