﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerRemittanceDetailWcbDeductions : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String DummyKey = "row_number";
            public static String WorkersCompensationAccountNumber = "workers_compensation_account_number";
            public static String ChequeDate = "cheque_date";
            public static String CurrentPremium = "current_premium";
            public static String Period = "period";
            public static String StartDate = "start_date";
            public static String CutoffDate = "cutoff_date";
            public static String PayrollProcessId = "payroll_process_id";
        }
        #endregion

        #region main
        internal RemittanceDetailWcbDeductionsCollection Select(DatabaseUser user, string detailType, long? wcbExportId, string remitCode, DateTime remitDate)
        {
            DataBaseCommand command = GetStoredProcCommand("RemittanceDetailWcb_report", user.DatabaseName);

            command.AddParameterWithValue("@detailType", detailType);
            command.AddParameterWithValue("@wcbExportId", wcbExportId);
            command.AddParameterWithValue("@remitCode", remitCode);
            command.AddParameterWithValue("@remitDate", remitDate);
            
            using (IDataReader reader = command.ExecuteReader())
                return MapToRemittanceDetailWcbDeductionsCollection(reader);
        }
        #endregion

        #region data mapping
        protected RemittanceDetailWcbDeductionsCollection MapToRemittanceDetailWcbDeductionsCollection(IDataReader reader)
        {
            RemittanceDetailWcbDeductionsCollection collection = new RemittanceDetailWcbDeductionsCollection();

            while (reader.Read())
                collection.Add(MapToRemittanceDetailWcbDeductions(reader));

            return collection;
        }
        protected RemittanceDetailWcbDeductions MapToRemittanceDetailWcbDeductions(IDataReader reader)
        {
            RemittanceDetailWcbDeductions item = new RemittanceDetailWcbDeductions();

            item.DummyKey = (long)CleanDataValue(reader[ColumnNames.DummyKey]);
            item.WorkersCompensationAccountNumber = (String)CleanDataValue(reader[ColumnNames.WorkersCompensationAccountNumber]);
            item.ChequeDate = (DateTime?)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.CurrentPremium = (Decimal)CleanDataValue(reader[ColumnNames.CurrentPremium]);
            item.Period = (Decimal?)CleanDataValue(reader[ColumnNames.Period]);
            item.StartDate = (DateTime?)CleanDataValue(reader[ColumnNames.StartDate]);
            item.CutoffDate = (DateTime?)CleanDataValue(reader[ColumnNames.CutoffDate]);
            item.PayrollProcessId = (long?)CleanDataValue(reader[ColumnNames.PayrollProcessId]);

            return item;
        }
        #endregion
    }
}