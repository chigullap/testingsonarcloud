﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerCraRemittance : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String DummyKey = "row_number";
            public static String BusinessTaxNumber = "business_tax_number";
            public static String ChequeDate = "cheque_date";
            public static String EmployeeCount = "employee_count";
            public static String PaymentAmount = "payment_amount";
            public static String TotalTaxableIncome = "total_taxable_income";
            public static String CodeCraRemittancePeriodCd = "code_cra_remittance_period_cd";
            public static String RemittancePeriodStartDate = "remittance_period_start_date";
            public static String RemittancePeriodEndDate = "remittance_period_end_date";
            public static String EffectiveEntryDate = "effective_entry_date";
        }
        #endregion

        #region main

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CraRemittance_Insert", user.DatabaseName))
            {
                command.BeginTransaction("CraRemittance_Insert");

                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        public void UpdateCraRemittanceCraExportIdColumn(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, DateTime date, long craExportId, String craRemittancePeriodCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CraRemittance_updateCraExportId", user.DatabaseName))
            {
                command.BeginTransaction("CraRemittance_updateCraExportId");

                command.AddParameterWithValue("@date", date);
                command.AddParameterWithValue("@craExportId", craExportId);
                command.AddParameterWithValue("@craRemitCode", craRemittancePeriodCode);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        public CraRemittanceSummaryCollection GetRbcEftSourceDeductionData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, DateTime date, String craRemittancePeriodCode)
        {
            DataBaseCommand command = GetStoredProcCommand("CraRemittance_selectSourceData", user.DatabaseName);
            command.AddParameterWithValue("@date", date);
            command.AddParameterWithValue("@craRemitCode", craRemittancePeriodCode);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToCraRemittanceSummaryCollection(reader);
            }
        }

        #endregion

        #region data mapping
        protected CraRemittanceSummaryCollection MapToCraRemittanceSummaryCollection(IDataReader reader)
        {
            CraRemittanceSummaryCollection collection = new CraRemittanceSummaryCollection();
            while (reader.Read())
            {
                collection.Add(MapToCraRemittanceSummary(reader));
            }

            return collection;
        }
        protected CraRemittanceSummary MapToCraRemittanceSummary(IDataReader reader)
        {
            CraRemittanceSummary item = new CraRemittanceSummary();

            item.DummyKey = (long)CleanDataValue(reader[ColumnNames.DummyKey]);
            item.BusinessTaxNumber = (String)CleanDataValue(reader[ColumnNames.BusinessTaxNumber]);
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.EmployeeCount = (Int64)CleanDataValue(reader[ColumnNames.EmployeeCount]);
            item.PaymentAmount = (decimal)CleanDataValue(reader[ColumnNames.PaymentAmount]);
            item.TotalTaxableIncome = (decimal)CleanDataValue(reader[ColumnNames.TotalTaxableIncome]);
            item.CodeCraRemittancePeriodCd = (String)CleanDataValue(reader[ColumnNames.CodeCraRemittancePeriodCd]);
            item.RemittancePeriodStartDate = (DateTime?)CleanDataValue(reader[ColumnNames.RemittancePeriodStartDate]);
            item.RemittancePeriodEndDate = (DateTime?)CleanDataValue(reader[ColumnNames.RemittancePeriodEndDate]);
            item.EffectiveEntryDate = (DateTime?)CleanDataValue(reader[ColumnNames.EffectiveEntryDate]);

            return item;
        }
        #endregion
    }
}
