﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerChequeWcbExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ChequeWcbExportId = "cheque_wcb_export_id";
            public static String RbcSequenceNumber = "rbc_sequence_number";
            public static String RbcChequeNumber = "rbc_cheque_number";
            public static String Data = "data";
            public static String CodeWsibCd = "code_wsib_cd";
            public static String RemittanceDate = "remittance_date";
            public static String CodeWcbChequeRemittanceFrequencyCd = "code_wcb_cheque_remittance_frequency_cd";
            public static String ChequeFileTransmissionDate = "cheque_file_transmission_date";
            public static String ChequeWcbPreAuthorizedDebitExportId = "cheque_wcb_pre_authorized_debit_export_id";
            public static String ReportingPeriodStartDate = "reporting_period_start_date";
            public static String ReportingPeriodEndDate = "reporting_period_end_date";
        }
        #endregion

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ChequeWcbExport item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ChequeWcbExport_insert", user.DatabaseName))
            {
                command.BeginTransaction("ChequeWcbExport_insert");

                SqlParameter chequeWcbExportIdParm = command.AddParameterWithValue("@chequeWcbExportId", item.ChequeWcbExportId, ParameterDirection.Output);
                command.AddParameterWithValue("@rbcSequenceNumber", item.RbcSequenceNumber);
                command.AddParameterWithValue("@rbcChequeNumber", item.RbcChequeNumber);
                command.AddParameterWithValue("@data", item.Data);
                command.AddParameterWithValue("@codeWsibCd", item.CodeWsibCd);
                command.AddParameterWithValue("@remittanceDate", item.RemittanceDate);
                command.AddParameterWithValue("@codeWcbChequeRemittanceFrequencyCd", item.CodeWcbChequeRemittanceFrequencyCd);
                command.AddParameterWithValue("@chequeWcbPreAuthorizedDebitExportId", item.ChequeWcbPreAuthorizedDebitExportId);
                command.AddParameterWithValue("@reportingPeriodStartDate", item.ReportingPeriodStartDate);
                command.AddParameterWithValue("@reportingPeriodEndDate", item.ReportingPeriodEndDate);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.ChequeWcbExportId = Convert.ToInt64(chequeWcbExportIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }

        public void UpdateChequeWcbExportFileTransmissionDate(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long chequeWcbExportId, DateTime exportDate)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ChequeWcbExport_UpdateChequeWcbFileTransmissionDate", user.DatabaseName))
            {
                command.BeginTransaction("ChequeWcbExport_UpdateChequeWcbFileTransmissionDate");

                command.AddParameterWithValue("@chequeWcbExportId", chequeWcbExportId);
                command.AddParameterWithValue("@exportDate", exportDate);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        public ChequeWcbExportCollection CheckForUnprocessedWcbRemittanceOnDatabase(WLP.BusinessLayer.BusinessObjects.DatabaseUser user)
        {
            DataBaseCommand command = GetStoredProcCommand("ChequeWcbExport_selectUnprocessed", user.DatabaseName);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToChequeWcbExportCollection(reader);
            }
        }

        public ChequeWcbExportCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? chequeWcbExportId)
        {
            DataBaseCommand command = GetStoredProcCommand("ChequeWcbExport_select", user.DatabaseName);
            command.AddParameterWithValue("@chequeWcbExportId", chequeWcbExportId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToChequeWcbExportCollection(reader);
            }
        }

        #region data mapping
        protected ChequeWcbExportCollection MapToChequeWcbExportCollection(IDataReader reader)
        {
            ChequeWcbExportCollection collection = new ChequeWcbExportCollection();
            while (reader.Read())
            {
                collection.Add(MapToChequeWcbExport(reader));
            }

            return collection;
        }
        protected ChequeWcbExport MapToChequeWcbExport(IDataReader reader)
        {
            ChequeWcbExport item = new ChequeWcbExport();
            base.MapToBase(item, reader);

            item.ChequeWcbExportId = (long)CleanDataValue(reader[ColumnNames.ChequeWcbExportId]);
            item.RbcSequenceNumber = (long)CleanDataValue(reader[ColumnNames.RbcSequenceNumber]);
            item.RbcChequeNumber = (long)CleanDataValue(reader[ColumnNames.RbcChequeNumber]);
            item.Data = (byte[])CleanDataValue(reader[ColumnNames.Data]);
            item.CodeWsibCd = (String)CleanDataValue(reader[ColumnNames.CodeWsibCd]);
            item.RemittanceDate = (DateTime)CleanDataValue(reader[ColumnNames.RemittanceDate]);
            item.CodeWcbChequeRemittanceFrequencyCd = (String)CleanDataValue(reader[ColumnNames.CodeWcbChequeRemittanceFrequencyCd]);
            item.ChequeFileTransmissionDate = (DateTime?)CleanDataValue(reader[ColumnNames.ChequeFileTransmissionDate]);
            item.ChequeWcbPreAuthorizedDebitExportId = (long?)CleanDataValue(reader[ColumnNames.ChequeWcbPreAuthorizedDebitExportId]);
            item.ReportingPeriodStartDate = (DateTime)CleanDataValue(reader[ColumnNames.ReportingPeriodStartDate]);
            item.ReportingPeriodEndDate = (DateTime)CleanDataValue(reader[ColumnNames.ReportingPeriodEndDate]);


            return item;
        }

        #endregion

    }
}
