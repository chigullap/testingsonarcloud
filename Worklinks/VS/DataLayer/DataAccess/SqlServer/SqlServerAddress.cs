﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerAddress : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String AddressId = "address_id";
            public static String AddressLine1 = "address_line_1";
            public static String AddressLine2 = "address_line_2";
            public static String City = "city";
            public static String PostalZipCode = "postal_zip_code";
            public static String ProvinceStateCode = "code_province_state_cd";
            public static String CountryCode = "code_country_cd";
            public static String ThreeCharacterCountryCode = "three_character_country_code";
        }
        #endregion

        #region main
        internal AddressCollection Select(DatabaseUser user, long? personId, long? addressId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Address_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@addressId", addressId);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToAddressCollection(reader);
            }
        }

        public void Update(DatabaseUser user, Address address)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("Address_update", user.DatabaseName))
                {
                    command.BeginTransaction("Address_update");

                    command.AddParameterWithValue("@addressId", address.AddressId);
                    command.AddParameterWithValue("@addressLine1", address.AddressLine1);
                    command.AddParameterWithValue("@addressLine2", address.AddressLine2);
                    command.AddParameterWithValue("@city", address.City);
                    command.AddParameterWithValue("@postalZipCode", address.PostalZipCode);
                    command.AddParameterWithValue("@provinceStateCd", address.ProvinceStateCode == "" ? null : address.ProvinceStateCode);
                    command.AddParameterWithValue("@countryCd", address.CountryCode);
                    command.AddParameterWithValue("@overrideConcurrencyCheckFlag", address.OverrideConcurrencyCheckFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", address.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();

                    address.RowVersion = (byte[])rowVersionParm.Value;
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Insert(DatabaseUser user, Address address)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Address_insert", user.DatabaseName))
            {
                command.BeginTransaction("PersonAddress_insert");

                SqlParameter AddressIdParm = command.AddParameterWithValue("@addressId", address.AddressId, ParameterDirection.Output);
                command.AddParameterWithValue("@addressLine1", address.AddressLine1);
                command.AddParameterWithValue("@addressLine2", address.AddressLine2);
                command.AddParameterWithValue("@city", address.City);
                command.AddParameterWithValue("@postalZipCode", address.PostalZipCode);
                command.AddParameterWithValue("@provinceStateCd", address.ProvinceStateCode == "" ? null : address.ProvinceStateCode);
                command.AddParameterWithValue("@countryCd", address.CountryCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", address.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                address.AddressId = Convert.ToInt64(AddressIdParm.Value);
                address.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }

        public void Delete(DatabaseUser user, Address address)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("Address_delete", user.DatabaseName))
                {
                    command.BeginTransaction("Address_delete");

                    command.AddParameterWithValue("@addressId", address.AddressId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected AddressCollection MapToAddressCollection(IDataReader reader)
        {
            AddressCollection collection = new AddressCollection();

            while (reader.Read())
                collection.Add(MapToAddress(reader));

            return collection;
        }

        protected Address MapToAddress(IDataReader reader)
        {
            Address address = new Address();
            base.MapToBase(address, reader);

            address.AddressId = (long)CleanDataValue(reader[ColumnNames.AddressId]);
            address.AddressLine1 = (String)CleanDataValue(reader[ColumnNames.AddressLine1]);
            address.AddressLine2 = (String)CleanDataValue(reader[ColumnNames.AddressLine2]);
            address.City = (String)CleanDataValue(reader[ColumnNames.City]);
            address.PostalZipCode = (String)CleanDataValue(reader[ColumnNames.PostalZipCode]);
            address.ProvinceStateCode = (String)CleanDataValue(reader[ColumnNames.ProvinceStateCode]);
            address.CountryCode = (String)CleanDataValue(reader[ColumnNames.CountryCode]);
            address.ThreeCharacterCountryCode = (String)CleanDataValue(reader[ColumnNames.ThreeCharacterCountryCode]);

            return address;
        }
        #endregion
    }
}