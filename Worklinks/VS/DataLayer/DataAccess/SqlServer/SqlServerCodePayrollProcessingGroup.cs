﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerCodePayrollProcessingGroup : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayrollProcessGroupCode = "code_payroll_process_group_cd";
            public static String PayrollProcessGroupCountryCode = "code_payroll_process_group_country_cd";
            public static String SortOrder = "sort_order";
            public static String StartDate = "start_date";
            public static String EndDate = "end_date";
            public static String ActiveFlag = "active_flag";
            public static String SystemFlag = "system_flag";
            public static String PayrollProcessGroupDesc = "description";
            public static String PaymentFrequencyCode = "code_payment_frequency_cd";
            public static String PayrollProcessGroupCodeDescId = "code_payroll_process_group_description_id";
            public static String DescRowVersion = "desc_rowversion";
            public static String ManualChequeFlag = "manual_cheque_flag";
        }
        #endregion

        #region select
        internal PayrollProcessingGroupCollection Select(DatabaseUser user, String payrollProcessGroupCode)
        {
            DataBaseCommand command = GetStoredProcCommand("PayrollProcessGroup_select", user.DatabaseName);

            command.AddParameterWithValue("@payrollProcessGroupCode", payrollProcessGroupCode);
            command.AddParameterWithValue("@languageCode", "EN"); /* HARDCODE */

            using (IDataReader reader = command.ExecuteReader())
                return MapToPayrollProcessingGroupCollection(reader);
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, PayrollProcessingGroup item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollProcessGroup_update", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollProcessGroup_update");

                    command.AddParameterWithValue("@payrollProcessGroupCode", item.PayrollProcessGroupCode);
                    command.AddParameterWithValue("@payrollProcessGroupCountryCode", item.PayrollProcessGroupCountryCode);
                    command.AddParameterWithValue("@payrollProcessGroupCodeEnglishDesc", item.PayrollProcessGroupEnglishDesc);
                    command.AddParameterWithValue("@payrollProcessGroupCodeFrenchDesc", item.PayrollProcessGroupFrenchDesc);
                    command.AddParameterWithValue("@paymentFrequencyCode", item.PaymentFrequencyCode);
                    command.AddParameterWithValue("@payrollProcessGroupCodeEngDescId", item.PayrollProcessGroupCodeEnglishDescId);
                    command.AddParameterWithValue("@payrollProcessGroupCodeFrDescId", item.PayrollProcessGroupCodeFrenchDescId);

                    SqlParameter enDescRowVersionParm = command.AddParameterWithValue("@EngDescRowVersion", item.EngDescRowVersion, ParameterDirection.InputOutput);
                    SqlParameter frDescRowVersionParm = command.AddParameterWithValue("@FrDescRowVersion", item.FrDescRowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@englishLanguageCode", item.EnglishLanguageCode);
                    command.AddParameterWithValue("@frenchLanguageCode", item.FrenchLanguageCode);
                    command.AddParameterWithValue("@manualChequeFlag", item.ManualChequeFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;
                    item.EngDescRowVersion = (byte[])enDescRowVersionParm.Value;
                    item.FrDescRowVersion = (byte[])frDescRowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region insert
        public PayrollProcessingGroup InsertCode(DatabaseUser user, PayrollProcessingGroup item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollProcessGroupCode_insert", user.DatabaseName))
            {
                command.BeginTransaction("PayrollProcessGroupCode_insert");

                command.AddParameterWithValue("@code", item.PayrollProcessGroupCode);
                command.AddParameterWithValue("@payrollProcessGroupCountryCode", item.PayrollProcessGroupCountryCode);
                command.AddParameterWithValue("@activeFlag", item.ActiveFlag);
                command.AddParameterWithValue("@sortOrder", item.SortOrder);
                command.AddParameterWithValue("@startDate", item.StartDate);
                command.AddParameterWithValue("@endDate", item.EndDate);
                command.AddParameterWithValue("@paymentFrequencyCode", item.PaymentFrequencyCode);
                command.AddParameterWithValue("@manualChequeFlag", item.ManualChequeFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        public PayrollProcessingGroup InsertCodeDesc(DatabaseUser user, PayrollProcessingGroup item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollProcessGroupCodeDescription_insert", user.DatabaseName))
            {
                command.BeginTransaction("PayrollProcessGroupCodeDescription_insert");

                SqlParameter englishDescriptionIdParm = command.AddParameterWithValue("@codeEnglishDescriptionId", item.PayrollProcessGroupCodeEnglishDescId, ParameterDirection.Output);
                SqlParameter frenchDescriptionIdParm = command.AddParameterWithValue("@codeFrenchDescriptionId", item.PayrollProcessGroupCodeFrenchDescId, ParameterDirection.Output);
                command.AddParameterWithValue("@code", item.PayrollProcessGroupCode);
                command.AddParameterWithValue("@englishLanguageCode", item.EnglishLanguageCode);
                command.AddParameterWithValue("@englishDescription", item.PayrollProcessGroupEnglishDesc);
                command.AddParameterWithValue("@frenchLanguageCode", item.FrenchLanguageCode);
                command.AddParameterWithValue("@frenchDescription", item.PayrollProcessGroupFrenchDesc);

                SqlParameter enDescRowVersionParm = command.AddParameterWithValue("@EngDescRowVersion", item.EngDescRowVersion, ParameterDirection.InputOutput);
                SqlParameter frDescRowVersionParm = command.AddParameterWithValue("@FrDescRowVersion", item.FrDescRowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.PayrollProcessGroupCodeEnglishDescId = Convert.ToInt64(englishDescriptionIdParm.Value);
                item.PayrollProcessGroupCodeFrenchDescId = Convert.ToInt64(frenchDescriptionIdParm.Value);

                command.CommitTransaction();

                return item;
            }
        }
        #endregion

        /* NOTE:  
         * The delete to remove descriptions and the code are taken care of
         * in SqlServerCodeTable.cs and SqlServerCodeTableDesription.cs  
        */

        #region data mapping
        protected PayrollProcessingGroupCollection MapToPayrollProcessingGroupCollection(IDataReader reader)
        {
            PayrollProcessingGroupCollection collection = new PayrollProcessingGroupCollection();
            int i = 0;

            while (reader.Read())
            {
                if (i > 0 && ((String)reader[ColumnNames.PayrollProcessGroupCode] == collection[i - 1].PayrollProcessGroupCode))
                {
                    //already have the code in the PayrollProcessingGrpCollection, now adding french desc and it's id
                    collection[i - 1].PayrollProcessGroupFrenchDesc = (String)CleanDataValue(reader[ColumnNames.PayrollProcessGroupDesc]);
                    collection[i - 1].PayrollProcessGroupCodeFrenchDescId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessGroupCodeDescId]);
                    collection[i - 1].FrDescRowVersion = (byte[])CleanDataValue(reader[ColumnNames.DescRowVersion]);
                }
                else
                {
                    //first record or next code in the list
                    collection.Add(MapToPayrollProcessingGroup(reader));
                    i++;
                }
            }

            return collection;
        }
        protected PayrollProcessingGroup MapToPayrollProcessingGroup(IDataReader reader)
        {
            PayrollProcessingGroup item = new PayrollProcessingGroup();
            base.MapToBase(item, reader);

            item.PayrollProcessGroupCode = (String)CleanDataValue(reader[ColumnNames.PayrollProcessGroupCode]);
            item.PayrollProcessGroupCountryCode = (String)CleanDataValue(reader[ColumnNames.PayrollProcessGroupCountryCode]);
            item.SortOrder = Convert.ToInt64(CleanDataValue(reader[ColumnNames.SortOrder]));
            item.StartDate = (DateTime?)CleanDataValue(reader[ColumnNames.StartDate]);
            item.EndDate = (DateTime?)CleanDataValue(reader[ColumnNames.EndDate]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.SystemFlag = (bool)CleanDataValue(reader[ColumnNames.SystemFlag]);
            item.PayrollProcessGroupEnglishDesc = (String)CleanDataValue(reader[ColumnNames.PayrollProcessGroupDesc]);
            item.PaymentFrequencyCode = (String)CleanDataValue(reader[ColumnNames.PaymentFrequencyCode]);
            item.PayrollProcessGroupCodeEnglishDescId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessGroupCodeDescId]);
            item.EngDescRowVersion = (byte[])CleanDataValue(reader[ColumnNames.DescRowVersion]);
            item.ManualChequeFlag = (bool)CleanDataValue(reader[ColumnNames.ManualChequeFlag]);

            return item;
        }
        #endregion
    }
}