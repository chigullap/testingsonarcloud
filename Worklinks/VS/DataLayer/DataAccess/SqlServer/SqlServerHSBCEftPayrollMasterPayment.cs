﻿using System;
using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerHSBCEftPayrollMasterPayment : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayrollMasterPaymentId = "payroll_master_payment_id";
            public static String EftOriginatorId = "eft_originator_id";
            public static String ChequeAmount = "cheque_amount";
            public static String ChequeDate = "cheque_date";
            public static String BankCode = "bank_code";
            public static String BankTransitNumber = "bank_transit_number";
            public static String BankAccountNumber = "bank_account_number";
            public static String CompanyShortName = "company_short_name";
            public static String EmployeeName = "employee_name";
            public static String CompanyLongName = "company_long_name";
        }
        #endregion

        #region main
        public HSBCEftPayrollMasterPaymentCollection Select(DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            DataBaseCommand command = GetStoredProcCommand("HsbcEftPayrollMasterPayment_select", user.DatabaseName);

            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
            command.AddParameterWithValue("@employeeNumber", employeeNumber);

            using (IDataReader reader = command.ExecuteReader())
                return MapToHSBCEftPayrollMasterPaymentCollection(reader);
        }
        #endregion

        #region data mapping
        protected HSBCEftPayrollMasterPaymentCollection MapToHSBCEftPayrollMasterPaymentCollection(IDataReader reader)
        {
            HSBCEftPayrollMasterPaymentCollection collection = new HSBCEftPayrollMasterPaymentCollection();

            while (reader.Read())
                collection.Add(MapToHSBCEftPayrollMasterPayment(reader));

            return collection;
        }
        protected HSBCEftPayrollMasterPayment MapToHSBCEftPayrollMasterPayment(IDataReader reader)
        {
            HSBCEftPayrollMasterPayment item = new HSBCEftPayrollMasterPayment();

            item.PayrollMasterPaymentId = (long)CleanDataValue(reader[ColumnNames.PayrollMasterPaymentId]);
            item.EftOriginatorId = (string)CleanDataValue(reader[ColumnNames.EftOriginatorId]);
            item.ChequeAmount = (decimal)CleanDataValue(reader[ColumnNames.ChequeAmount]);
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.BankCode = (string)CleanDataValue(reader[ColumnNames.BankCode]);
            item.BankTransitNumber = (string)CleanDataValue(reader[ColumnNames.BankTransitNumber]);
            item.BankAccountNumber = (string)CleanDataValue(reader[ColumnNames.BankAccountNumber]);
            item.CompanyShortName = (string)CleanDataValue(reader[ColumnNames.CompanyShortName]);
            item.EmployeeName = (string)CleanDataValue(reader[ColumnNames.EmployeeName]);
            item.CompanyLongName = (string)CleanDataValue(reader[ColumnNames.CompanyLongName]);

            return item;
        }
        #endregion
    }
}