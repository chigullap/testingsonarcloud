﻿using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerSalaryPlanGradeStep : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string SalaryPlanGradeStepId = "salary_plan_grade_step_id";
            public static string SalaryPlanGradeId = "salary_plan_grade_id";
            public static string Name = "name";
            public static string ActiveFlag = "active_flag";
            public static string EnglishDescription = "english_description";
            public static string FrenchDescription = "french_description";

            //detail
            public static string SalaryPlanGradeStepDetailId = "salary_plan_grade_step_detail_id";
            public static string SalaryPlanGradeEffectiveId = "salary_plan_grade_effective_id";
            public static string Amount = "amount";
        }
        #endregion

        #region select
        internal SalaryPlanGradeStepCollection Select(DatabaseUser user, long? salaryPlanGradeStepId, long? salaryPlanGradeId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SalaryPlanGradeStep_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@salaryPlanGradeStepId", salaryPlanGradeStepId);
                command.AddParameterWithValue("@salaryPlanGradeId", salaryPlanGradeId);

                SalaryPlanGradeStepCollection collection = null;

                using (IDataReader reader = command.ExecuteReader())
                    collection = MapToStepCollection(reader);

                return collection;
            }
        }
        internal SalaryPlanGradeStepDetailCollection SelectDetail(DatabaseUser user, long? salaryPlanGradeStepDetailId, long? salaryPlanGradeStepId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SalaryPlanGradeStepDetail_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@salaryPlanGradeStepDetailId", salaryPlanGradeStepDetailId);
                command.AddParameterWithValue("@salaryPlanGradeStepId", salaryPlanGradeStepId);

                SalaryPlanGradeStepDetailCollection collection = null;

                using (IDataReader reader = command.ExecuteReader())
                    collection = MapToStepDetailCollection(reader);

                return collection;
            }
        }
        #endregion

        #region data mapping
        protected SalaryPlanGradeStepCollection MapToStepCollection(IDataReader reader)
        {
            SalaryPlanGradeStepCollection collection = new SalaryPlanGradeStepCollection();

            while (reader.Read())
                collection.Add(MapToStep(reader));

            return collection;
        }
        protected SalaryPlanGradeStepDetailCollection MapToStepDetailCollection(IDataReader reader)
        {
            SalaryPlanGradeStepDetailCollection collection = new SalaryPlanGradeStepDetailCollection();

            while (reader.Read())
                collection.Add(MapToStepDetail(reader));

            return collection;
        }
        protected SalaryPlanGradeStep MapToStep(IDataReader reader)
        {
            SalaryPlanGradeStep item = new SalaryPlanGradeStep();
            base.MapToBase(item, reader);

            item.SalaryPlanGradeStepId = (long)CleanDataValue(reader[ColumnNames.SalaryPlanGradeStepId]);
            item.SalaryPlanGradeId = (long)CleanDataValue(reader[ColumnNames.SalaryPlanGradeId]);
            item.Name = (string)CleanDataValue(reader[ColumnNames.Name]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.EnglishDescription = (string)CleanDataValue(reader[ColumnNames.EnglishDescription]);
            item.FrenchDescription = (string)CleanDataValue(reader[ColumnNames.FrenchDescription]);

            return item;
        }
        protected SalaryPlanGradeStepDetail MapToStepDetail(IDataReader reader)
        {
            SalaryPlanGradeStepDetail item = new SalaryPlanGradeStepDetail();
            base.MapToBase(item, reader);

            item.SalaryPlanGradeStepDetailId = (long)CleanDataValue(reader[ColumnNames.SalaryPlanGradeStepDetailId]);
            item.SalaryPlanGradeStepId = (long)CleanDataValue(reader[ColumnNames.SalaryPlanGradeStepId]);
            item.SalaryPlanGradeEffectiveId = (long)CleanDataValue(reader[ColumnNames.SalaryPlanGradeStepDetailId]);
            item.Amount = (decimal)CleanDataValue(reader[ColumnNames.Amount]);

            return item;
        }
        #endregion
    }
}