﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmailTemplate : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmailTemplateId = "email_template_id";
            public static String EmailId = "email_id";

            //primary
            public static String PrimaryLanguageCode = "primary_code_language_cd";
            public static String PrimarySubject = "primary_subject";
            public static String PrimaryBodyHeader = "primary_body_header";
            public static String PrimaryBodyDetail = "primary_body_detail";
            public static String PrimaryBodyFooter = "primary_body_footer";

            //secondary
            public static String SecondaryLanguageCode = "secondary_code_language_cd";
            public static String SecondarySubject = "secondary_subject";
            public static String SecondaryBodyHeader = "secondary_body_header";
            public static String SecondaryBodyDetail = "secondary_body_detail";
            public static String SecondaryBodyFooter = "secondary_body_footer";
        }
        #endregion

        #region main
        internal EmailTemplateCollection Select(DatabaseUser user, long emailId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmailTemplate_select", user.DatabaseName);

            command.AddParameterWithValue("@emailId", emailId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToEmailTemplateCollection(reader);
        }
        public EmailTemplate Insert(DatabaseUser user, EmailTemplate item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmailTemplate_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmailTemplate_insert");

                SqlParameter idParm = command.AddParameterWithValue("@emailTemplateId", item.EmailTemplateId, ParameterDirection.Output);
                command.AddParameterWithValue("@emailId", item.EmailId);

                //primary
                command.AddParameterWithValue("@primaryLanguageCode", item.PrimaryLanguageCode);
                command.AddParameterWithValue("@primarySubject", item.PrimarySubject);
                command.AddParameterWithValue("@primaryBodyHeader", item.PrimaryBodyHeader);
                command.AddParameterWithValue("@primaryBodyDetail", item.PrimaryBodyDetail);
                command.AddParameterWithValue("@primaryBodyFooter", item.PrimaryBodyFooter);

                //secondary
                command.AddParameterWithValue("@secondaryLanguageCode", item.SecondaryLanguageCode);
                command.AddParameterWithValue("@secondarySubject", item.SecondarySubject);
                command.AddParameterWithValue("@secondaryBodyHeader", item.SecondaryBodyHeader);
                command.AddParameterWithValue("@secondaryBodyDetail", item.SecondaryBodyDetail);
                command.AddParameterWithValue("@secondaryBodyFooter", item.SecondaryBodyFooter);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        public void Update(DatabaseUser user, EmailTemplate item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmailTemplate_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmailTemplate_update");

                    command.AddParameterWithValue("@emailTemplateId", item.EmailTemplateId);
                    command.AddParameterWithValue("@emailId", item.EmailId);

                    //primary
                    command.AddParameterWithValue("@primaryLanguageCode", item.PrimaryLanguageCode);
                    command.AddParameterWithValue("@primarySubject", item.PrimarySubject);
                    command.AddParameterWithValue("@primaryBodyHeader", item.PrimaryBodyHeader);
                    command.AddParameterWithValue("@primaryBodyDetail", item.PrimaryBodyDetail);
                    command.AddParameterWithValue("@primaryBodyFooter", item.PrimaryBodyFooter);

                    //secondary
                    command.AddParameterWithValue("@secondaryLanguageCode", item.SecondaryLanguageCode);
                    command.AddParameterWithValue("@secondarySubject", item.SecondarySubject);
                    command.AddParameterWithValue("@secondaryBodyHeader", item.SecondaryBodyHeader);
                    command.AddParameterWithValue("@secondaryBodyDetail", item.SecondaryBodyDetail);
                    command.AddParameterWithValue("@secondaryBodyFooter", item.SecondaryBodyFooter);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, EmailTemplate item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmailTemplate_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmailTemplate_delete");

                    command.AddParameterWithValue("@emailId", item.EmailId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmailTemplateCollection MapToEmailTemplateCollection(IDataReader reader)
        {
            EmailTemplateCollection collection = new EmailTemplateCollection();

            while (reader.Read())
                collection.Add(MapToEmailTemplate(reader));

            return collection;
        }
        protected EmailTemplate MapToEmailTemplate(IDataReader reader)
        {
            EmailTemplate item = new EmailTemplate();
            base.MapToBase(item, reader);

            item.EmailTemplateId = (long)CleanDataValue(reader[ColumnNames.EmailTemplateId]);
            item.EmailId = (long)CleanDataValue(reader[ColumnNames.EmailId]);

            //primary
            item.PrimaryLanguageCode = (String)CleanDataValue(reader[ColumnNames.PrimaryLanguageCode]);
            item.PrimarySubject = (String)CleanDataValue(reader[ColumnNames.PrimarySubject]);
            item.PrimaryBodyHeader = (String)CleanDataValue(reader[ColumnNames.PrimaryBodyHeader]);
            item.PrimaryBodyDetail = (String)CleanDataValue(reader[ColumnNames.PrimaryBodyDetail]);
            item.PrimaryBodyFooter = (String)CleanDataValue(reader[ColumnNames.PrimaryBodyFooter]);

            //secondary
            item.SecondaryLanguageCode = (String)CleanDataValue(reader[ColumnNames.SecondaryLanguageCode]);
            item.SecondarySubject = (String)CleanDataValue(reader[ColumnNames.SecondarySubject]);
            item.SecondaryBodyHeader = (String)CleanDataValue(reader[ColumnNames.SecondaryBodyHeader]);
            item.SecondaryBodyDetail = (String)CleanDataValue(reader[ColumnNames.SecondaryBodyDetail]);
            item.SecondaryBodyFooter = (String)CleanDataValue(reader[ColumnNames.SecondaryBodyFooter]);

            return item;
        }
        #endregion
    }
}