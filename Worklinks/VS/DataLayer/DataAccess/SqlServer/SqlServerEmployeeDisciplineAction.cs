﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using System.Data;
using System.Data.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeDisciplineAction : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {

            public static String EmployeeDisciplineActionId = "employee_discipline_action_id";
            public static String EmployeeDisciplineId = "employee_discipline_id";
            public static String DisciplineActionStepCode = "code_discipline_action_step_cd";
            public static String ActionDate = "action_date";
            public static String DiscussedWith = "discussed_with";
            public static String Note = "note";

        }
        #endregion

        #region main
        internal EmployeeDisciplineActionCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeeDisciplineActionId)
        {
            return Select(user, employeeDisciplineActionId, null);
        }

        internal EmployeeDisciplineActionCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeeDisciplineId, long? employeeDisciplineActionId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeDisciplineAction_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@employeeDisciplineId", employeeDisciplineId);
                command.AddParameterWithValue("@employeeDisciplineActionId", employeeDisciplineActionId);


                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToEmployeeDisciplineActionCollection(reader);
                }
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeDisciplineAction disciplineAction)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeDisciplineAction_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeDisciplineAction_update");

                    command.AddParameterWithValue("@employeeDisciplineActionId", disciplineAction.EmployeeDisciplineActionId);
                    command.AddParameterWithValue("@employeeDisciplineId", disciplineAction.EmployeeDisciplineId);
                    command.AddParameterWithValue("@codeDisciplineActionStepCd", disciplineAction.DisciplineActionStepCode);
                    command.AddParameterWithValue("@actionDate", disciplineAction.ActionDate);
                    command.AddParameterWithValue("@discussedWith", disciplineAction.DiscussedWith);
                    command.AddParameterWithValue("@note", disciplineAction.Note);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", disciplineAction.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    disciplineAction.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public EmployeeDisciplineAction Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeDisciplineAction disciplineAction)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeDisciplineAction_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeDisciplineAction_insert");

                SqlParameter employeeDisciplineActionIdParm = command.AddParameterWithValue("@employeeDisciplineActionId", disciplineAction.EmployeeDisciplineActionId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeDisciplineId", disciplineAction.EmployeeDisciplineId);
                command.AddParameterWithValue("@codeDisciplineActionStepCd", disciplineAction.DisciplineActionStepCode);
                command.AddParameterWithValue("@actionDate", disciplineAction.ActionDate);
                command.AddParameterWithValue("@discussedWith", disciplineAction.DiscussedWith);
                command.AddParameterWithValue("@note", disciplineAction.Note);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", disciplineAction.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@createUser", user.UserName);
                command.AddParameterWithValue("@createDatetime", Time);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                disciplineAction.EmployeeDisciplineActionId = Convert.ToInt64(employeeDisciplineActionIdParm.Value);
                disciplineAction.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();

                return disciplineAction;
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeDisciplineAction disciplineAction)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeDisciplineAction_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeDisciplineAction_delete");
                    command.AddParameterWithValue("@employeeDisciplineActionId", disciplineAction.EmployeeDisciplineActionId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeDisciplineActionCollection MapToEmployeeDisciplineActionCollection(IDataReader reader)
        {
            EmployeeDisciplineActionCollection collection = new EmployeeDisciplineActionCollection();
            while (reader.Read())
            {
                collection.Add(MapToEmployeeDisciplineAction(reader));
            }

            return collection;
        }
        protected EmployeeDisciplineAction MapToEmployeeDisciplineAction(IDataReader reader)
        {
            EmployeeDisciplineAction disciplineAction = new EmployeeDisciplineAction();
            base.MapToBase(disciplineAction, reader);

            disciplineAction.EmployeeDisciplineActionId = (long)CleanDataValue(reader[ColumnNames.EmployeeDisciplineActionId]);
            disciplineAction.EmployeeDisciplineId = (long)CleanDataValue(reader[ColumnNames.EmployeeDisciplineId]);
            disciplineAction.DisciplineActionStepCode = (String)CleanDataValue(reader[ColumnNames.DisciplineActionStepCode]);
            disciplineAction.ActionDate = (DateTime?)CleanDataValue(reader[ColumnNames.ActionDate]);
            disciplineAction.DiscussedWith = (String)CleanDataValue(reader[ColumnNames.DiscussedWith]);
            disciplineAction.Note = (String)CleanDataValue(reader[ColumnNames.Note]);

            return disciplineAction;
        }
        #endregion
    }
}