﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeEducation : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeEducationId = "employee_education_id";
            public static String EmployeeId = "employee_id";
            public static String EmployeeEducationDegreeCode = "code_employee_education_degree_cd";
            public static String EmployeeEducationMajorCode = "code_employee_education_major_cd";
            public static String School = "school";
            public static String YearEarned = "year_earned";
            public static String CountryCode = "code_country_cd";
            public static String CreditHour = "credit_hour";
            public static String EmployeeEducationCreditHourCode = "code_employee_education_credit_hour_cd";
            public static String AttachmentId = "attachment_id";
            public static String JobRelatedFlag = "job_related_flag";
            public static String JobRequiredFlag = "job_required_flag"; 
        }
        #endregion

        #region main
        internal EmployeeEducationCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeeId)
        {
            return Select(user, employeeId, null);
        }

        internal EmployeeEducationCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeeId, long? employeeEducationId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeEducation_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@employeeId", employeeId);
                command.AddParameterWithValue("@employeeEducationId", employeeEducationId);
                command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
                command.AddParameterWithValue("@securityUserId", user.SecurityUserId);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToEmployeeEducationCollection(reader);
                }
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeEducation education)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeEducation_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeEducation_update");

                    command.AddParameterWithValue("@employeeEducationId", education.EmployeeEducationId);
                    command.AddParameterWithValue("@employeeEducationDegreeCode", education.EmployeeEducationDegreeCode);
                    command.AddParameterWithValue("@employeeEducationMajorCode", education.EmployeeEducationMajorCode);
                    command.AddParameterWithValue("@school", education.School);
                    command.AddParameterWithValue("@yearEarned", education.YearEarned);
                    command.AddParameterWithValue("@CountryCode", education.CountryCode);
                    command.AddParameterWithValue("@creditHour", education.CreditHour);
                    command.AddParameterWithValue("@employeeEducationCreditHourCode", education.EmployeeEducationCreditHourCode);
                    command.AddParameterWithValue("@attachmentId", education.AttachmentId);
                    command.AddParameterWithValue("@jobRelatedFlag", education.JobRelatedFlag);
                    command.AddParameterWithValue("@jobRequiredFlag", education.JobRequiredFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", education.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    education.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public EmployeeEducation Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeEducation education)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeEducation_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeEducation_insert");

                SqlParameter employeeEducationIdParm = command.AddParameterWithValue("@employeeEducationId", education.EmployeeEducationId, ParameterDirection.Output);
                command.AddParameterWithValue("EmployeeId", education.EmployeeId);
                command.AddParameterWithValue("@employeeEducationDegreeCode", education.EmployeeEducationDegreeCode);
                command.AddParameterWithValue("@employeeEducationMajorCode", education.EmployeeEducationMajorCode);
                command.AddParameterWithValue("@school", education.School);
                command.AddParameterWithValue("@yearEarned", education.YearEarned);
                command.AddParameterWithValue("@CountryCode", education.CountryCode);
                command.AddParameterWithValue("@creditHour", education.CreditHour);
                command.AddParameterWithValue("@employeeEducationCreditHourCode", education.EmployeeEducationCreditHourCode);

                if (education.AttachmentId != -1)
                    command.AddParameterWithValue("@attachmentId", education.AttachmentId);
                else
                    command.AddParameterWithValue("@attachmentId", null);

                command.AddParameterWithValue("@jobRelatedFlag", education.JobRelatedFlag);
                command.AddParameterWithValue("@jobRequiredFlag", education.JobRequiredFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", education.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                education.EmployeeEducationId = Convert.ToInt64(employeeEducationIdParm.Value);
                education.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();

                return education;
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeEducation education)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeEducation_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeEducation_delete");
                    command.AddParameterWithValue("@employeeEducationId", education.EmployeeEducationId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeEducationCollection MapToEmployeeEducationCollection(IDataReader reader)
        {
            EmployeeEducationCollection collection = new EmployeeEducationCollection();
            while (reader.Read())
            {
                collection.Add(MapToEmployeeEducation(reader));
            }

            return collection;
        }

        protected EmployeeEducation MapToEmployeeEducation(IDataReader reader)
        {
            EmployeeEducation education = new EmployeeEducation();
            base.MapToBase(education, reader);

            education.EmployeeEducationId = (long)CleanDataValue(reader[ColumnNames.EmployeeEducationId]);
            education.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            education.EmployeeEducationDegreeCode = (String)CleanDataValue(reader[ColumnNames.EmployeeEducationDegreeCode]);
            education.EmployeeEducationMajorCode = (String)CleanDataValue(reader[ColumnNames.EmployeeEducationMajorCode]);
            education.School = (String)CleanDataValue(reader[ColumnNames.School]);
            education.YearEarned = (Decimal?)CleanDataValue(reader[ColumnNames.YearEarned]);
            education.CountryCode = (String)CleanDataValue(reader[ColumnNames.CountryCode]);
            education.CreditHour = (short?)CleanDataValue(reader[ColumnNames.CreditHour]);
            education.EmployeeEducationCreditHourCode = (String)CleanDataValue(reader[ColumnNames.EmployeeEducationCreditHourCode]);
            education.AttachmentId = (long?)CleanDataValue(reader[ColumnNames.AttachmentId]);
            education.JobRelatedFlag = (bool)CleanDataValue(reader[ColumnNames.JobRelatedFlag]);
            education.JobRequiredFlag = (bool)CleanDataValue(reader[ColumnNames.JobRequiredFlag]);

            return education;
        }
        #endregion
    }
}