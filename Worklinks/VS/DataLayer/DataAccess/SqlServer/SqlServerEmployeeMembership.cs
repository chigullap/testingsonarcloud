﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeMembership : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeMembershipId = "employee_membership_id";
            public static String EmployeeId = "employee_id";
            public static String EmployeeMembershipCode = "code_employee_membership_cd";
            public static String EmployeeAssociationCode = "code_employee_association_cd";
            public static String StartDate = "start_date";
            public static String ExpiryDate = "expiry_date";
            public static String EmployerCost = "employer_cost";
            public static String EmployeeCost = "employee_cost";
            public static String JobRelatedFlag = "job_related_flag";
            public static String JobRequiredFlag = "job_required_flag";
        }
        #endregion

        #region select
        internal EmployeeMembershipCollection Select(DatabaseUser user, long employeeId)
        {
            return Select(user, employeeId, null);
        }
        internal EmployeeMembershipCollection Select(DatabaseUser user, long employeeId, long? employeeMembershipId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeeMembership_select", user.DatabaseName);

            command.AddParameterWithValue("@employeeId", employeeId);
            command.AddParameterWithValue("@EmployeeMembershipId", employeeMembershipId);
            command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
            command.AddParameterWithValue("@securityUserId", user.SecurityUserId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCollection(reader);
        }
        #endregion

        #region insert
        public EmployeeMembership Insert(DatabaseUser user, EmployeeMembership course)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeMembership_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeMembership_insert");

                SqlParameter idParm = command.AddParameterWithValue("@employeeMembershipId", course.EmployeeMembershipId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", course.EmployeeId);
                command.AddParameterWithValue("@employeeMembershipCode", course.EmployeeMembershipCode);
                command.AddParameterWithValue("@employeeAssociationCode", course.EmployeeAssociationCode);
                command.AddParameterWithValue("@startDate", course.StartDate);
                command.AddParameterWithValue("@expiryDate", course.ExpiryDate);
                command.AddParameterWithValue("@employerCost", course.EmployerCost);
                command.AddParameterWithValue("@employeeCost", course.EmployeeCost);
                command.AddParameterWithValue("@jobRelatedFlag", course.JobRelatedFlag);
                command.AddParameterWithValue("@jobRequiredFlag", course.JobRequiredFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", course.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                course.EmployeeMembershipId = Convert.ToInt64(idParm.Value);
                course.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return course;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, EmployeeMembership course)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeMembership_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeMembership_update");

                    command.AddParameterWithValue("@employeeMembershipId", course.EmployeeMembershipId);
                    command.AddParameterWithValue("@employeeId", course.EmployeeId);
                    command.AddParameterWithValue("@employeeMembershipCode", course.EmployeeMembershipCode);
                    command.AddParameterWithValue("@employeeAssociationCode", course.EmployeeAssociationCode);
                    command.AddParameterWithValue("@startDate", course.StartDate);
                    command.AddParameterWithValue("@expiryDate", course.ExpiryDate);
                    command.AddParameterWithValue("@employerCost", course.EmployerCost);
                    command.AddParameterWithValue("@employeeCost", course.EmployeeCost);
                    command.AddParameterWithValue("@jobRelatedFlag", course.JobRelatedFlag);
                    command.AddParameterWithValue("@jobRequiredFlag", course.JobRequiredFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", course.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    course.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, EmployeeMembership course)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeMembership_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeMembership_delete");

                    command.AddParameterWithValue("@employeeMembershipId", course.EmployeeMembershipId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeMembershipCollection MapToCollection(IDataReader reader)
        {
            EmployeeMembershipCollection collection = new EmployeeMembershipCollection();

            while (reader.Read())
                collection.Add(MapToItem(reader));

            return collection;
        }
        protected EmployeeMembership MapToItem(IDataReader reader)
        {
            EmployeeMembership item = new EmployeeMembership();
            base.MapToBase(item, reader);

            item.EmployeeMembershipId = (long)CleanDataValue(reader[ColumnNames.EmployeeMembershipId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EmployeeMembershipCode = (String)CleanDataValue(reader[ColumnNames.EmployeeMembershipCode]);
            item.EmployeeAssociationCode = (String)CleanDataValue(reader[ColumnNames.EmployeeAssociationCode]);
            item.StartDate = (DateTime?)CleanDataValue(reader[ColumnNames.StartDate]);
            item.ExpiryDate = (DateTime?)CleanDataValue(reader[ColumnNames.ExpiryDate]);
            item.EmployerCost = (Decimal?)CleanDataValue(reader[ColumnNames.EmployerCost]);
            item.EmployeeCost = (Decimal?)CleanDataValue(reader[ColumnNames.EmployeeCost]);
            item.JobRelatedFlag = (bool)CleanDataValue(reader[ColumnNames.JobRelatedFlag]);
            item.JobRequiredFlag = (bool)CleanDataValue(reader[ColumnNames.JobRequiredFlag]);

            return item;
        }
        #endregion
    }
}