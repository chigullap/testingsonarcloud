﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerREALLaborLevel : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string RowNumber = "row_number";
            public static string Level = "level";
            public static string EntryName = "entry_name";
            public static string Description = "description";
            public static string Status = "active_flag";
        }
        #endregion

        #region main
        public LaborLevelExportCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user)
        {
            DataBaseCommand command = GetStoredProcCommand("REALlaborlevelexport_report", user.DatabaseName);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToLaborLevelExportCollection(reader);
            }
        }
        #endregion

        #region data mapping
        protected LaborLevelExportCollection MapToLaborLevelExportCollection(IDataReader reader)
        {
            LaborLevelExportCollection collection = new LaborLevelExportCollection();
            while (reader.Read())
            {
                collection.Add(MapToLaborLevelExport(reader));
            }
            return collection;
        }
        protected LaborLevelExport MapToLaborLevelExport(IDataReader reader)
        {
            LaborLevelExport item = new LaborLevelExport();

            item.DummyKey = (long)CleanDataValue(reader[ColumnNames.RowNumber]);
            item.Level = (string)CleanDataValue(reader[ColumnNames.Level]);
            item.EntryName = (string)CleanDataValue(reader[ColumnNames.EntryName]);
            item.Description = (string)CleanDataValue(reader[ColumnNames.Description]);
            item.Status = (string)CleanDataValue(reader[ColumnNames.Status]);

            return item;
        }
        #endregion
    }
}
