﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerBenefitPlanSearch : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String BenefitPlanDescriptionId = "benefit_plan_description_id";
            public static String BenefitPlanId = "benefit_plan_id";
            public static String BenefitPlanDescriptionField = "description";
        }
        #endregion

        #region main
        internal BenefitPlanSearchCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String criteria)
        {
            DataBaseCommand command = GetStoredProcCommand("BenefitPlan_report", user.DatabaseName);

            command.AddParameterWithValue("@codeLanguage", user.LanguageCode);
            command.AddParameterWithValue("@benefitPlanDescription", criteria = criteria == "" ? null : criteria); //if criteria is "", then make it null for the proc, otherwise leave it alone

            using (IDataReader reader = command.ExecuteReader())
                return MapToBenefitPlanDescriptionCollection(reader);
        }
        #endregion

        #region data mapping
        protected BenefitPlanSearchCollection MapToBenefitPlanDescriptionCollection(IDataReader reader)
        {
            BenefitPlanSearchCollection collection = new BenefitPlanSearchCollection();

            while (reader.Read())
                collection.Add(MapToBenefitPlanDescription(reader));

            return collection;
        }

        protected BenefitPlanSearch MapToBenefitPlanDescription(IDataReader reader)
        {
            BenefitPlanSearch item = new BenefitPlanSearch();
            base.MapToBase(item, reader);

            item.BenefitPlanDescriptionId = (long)CleanDataValue(reader[ColumnNames.BenefitPlanDescriptionId]);
            item.BenefitPlanId = (long)CleanDataValue(reader[ColumnNames.BenefitPlanId]);
            item.BenefitPlanDescriptionField = (String)CleanDataValue(reader[ColumnNames.BenefitPlanDescriptionField]);

            return item;
        }
        #endregion
    }
}
