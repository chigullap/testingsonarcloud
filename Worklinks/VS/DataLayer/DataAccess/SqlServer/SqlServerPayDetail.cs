﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerPayDetail : SqlServerBase
    {

        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayDetailId = "pay_detail_id";
            public static String PayrollProcessId = "payroll_process_id";
            public static String EmployeeId = "employee_id";
            public static String EmployeeNumber = "employee_number";
            public static String ProvinceStateCode = "code_province_state_cd";
            public static String PaycodeCode = "code_paycode_cd";
            public static String CurrentAmount = "current_amount";
            public static String CurrentUnit = "current_unit";
            public static String YearToDateAmount = "year_to_date_amount";
            public static String YearToDateUnit = "year_to_date_unit";
            public static String ExternalCode = "external_code";
            public static String EmployerNumber = "employer_number";
            public static String GrossAmount = "gross_amount";
            public static String ChequeDate = "cheque_date";
        }

        #region main

        public PayDetailCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, String paycodeAssociationCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayDetail_report", user.DatabaseName))
            {
                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                command.AddParameterWithValue("@paycodeAssociationTypeCode", paycodeAssociationCode);
                command.AddParameterWithValue("@wsibExcludeFromRemittanceExportFlag", false);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToPayDetailCollection(reader);
                }
            }
        }

        #region data mapping
        protected PayDetailCollection MapToPayDetailCollection(IDataReader reader)
        {
            PayDetailCollection collection = new PayDetailCollection();
            while (reader.Read())
            {
                collection.Add(MapToPayDetail(reader));
            }

            return collection;
        }
        protected PayDetail MapToPayDetail(IDataReader reader)
        {
            PayDetail payDetail = new PayDetail();

            MapToBase(payDetail, reader);

            payDetail.PayDetailId = (long)CleanDataValue(reader[ColumnNames.PayDetailId]);
            payDetail.PayrollProcessId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessId]);
            payDetail.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            payDetail.EmployeeNumber = (String)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            payDetail.ProvinceStateCode = (String)CleanDataValue(reader[ColumnNames.ProvinceStateCode]);
            payDetail.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            payDetail.CurrentAmount = (Decimal?)CleanDataValue(reader[ColumnNames.CurrentAmount]);
            payDetail.CurrentUnit = (Decimal)CleanDataValue(reader[ColumnNames.CurrentUnit]);
            payDetail.YearToDateAmount = (Decimal?)CleanDataValue(reader[ColumnNames.YearToDateAmount]);
            payDetail.YearToDateUnit = (Decimal?)CleanDataValue(reader[ColumnNames.YearToDateUnit]);
            payDetail.ExternalCode = (String)CleanDataValue(reader[ColumnNames.ExternalCode]);
            payDetail.EmployerNumber = (String)CleanDataValue(reader[ColumnNames.EmployerNumber]);
            payDetail.GrossAmount = (Decimal?)CleanDataValue(reader[ColumnNames.GrossAmount]);
            payDetail.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);

            return payDetail;
        }
        #endregion

        #endregion
    }
}