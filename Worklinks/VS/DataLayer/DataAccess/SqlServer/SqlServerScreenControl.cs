﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;



namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerScreenControl : SqlServerBase
    {
        public void Merge(DatabaseUser user, ScreenControl[] screenControlArray)
        {
            foreach (ScreenControl screenControl in screenControlArray)
            {
                if(screenControl.ResourceName != null)
                {
                    using (StoredProcedureCommand command = GetStoredProcCommand("ControlField_merge",user.DatabaseName))
                    {
                        try
                        {
                            command.BeginTransaction("ControlField_merge");

                            SqlParameter rowScreenControlIdParm = command.AddParameterWithValue("@controlFieldId", null);
                            command.AddParameterWithValue("@virtualPath", screenControl.VirtualPath);
                            command.AddParameterWithValue("@controlType", screenControl.ControlType);
                            command.AddParameterWithValue("@resourcename", screenControl.ResourceName);
                            command.AddParameterWithValue("@sortOrder", screenControl.SortOrder);

                            SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", screenControl.RowVersion, ParameterDirection.InputOutput);

                            command.AddParameterWithValue("@updateUser", user.UserName);
                            command.AddParameterWithValue("@updateDatetime", DateTime.Now);

                            command.ExecuteNonQuery();
                            //screenControl.ScreenControlId = (long)rowScreenControlIdParm.Value;
                            screenControl.RowVersion = (byte[])rowVersionParm.Value;
                            command.CommitTransaction();
                        }
                        catch (Exception exc)
                        {
                            command.RollbackTransaction();
                            HandleException(exc, command.ReturnCode);
                        }
                    }
                }

            }
        }
    }
}
