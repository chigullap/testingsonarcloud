﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerPayrollBatch : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string PayrollBatchId = "payroll_batch_id";
            public static string PayrollBatchTypeCode = "code_payroll_batch_type_cd";
            public static string PayrollProcessRunTypeCode = "code_payroll_process_run_type_cd";
            public static string PayrollProcessRunTypeCodeDescription = "code_payroll_process_run_type_description";
            public static string BatchSource = "batch_number";
            public static string BatchStatusCode = "code_payroll_batch_status_cd";
            public static string PayrollProcessGroupCode = "code_payroll_process_group_cd";
            public static string PayrollProcessGroupCodeDescription = "code_payroll_process_group_description";
            public static string Description = "description";
            public static string PayrollBatchStatusCode = "code_payroll_batch_status_cd";
            public static string PayrollBatchStatusCodeDescription = "code_payroll_batch_status_description";
            public static string TotalHours = "total_hours";
            public static string TotalAmount = "total_amount";
            public static string PayrollProcessStatusCode = "code_payroll_process_status_cd";
            public static string PayrollProcessId = "payroll_process_id";
            public static string ProcessedFlag = "processed_flag";
            public static string systemGeneratedFlag = "system_generated_flag";
            public static string PayrollBatchAdjustmentOtherAmountId = "payroll_batch_id";
            public static string EmployeeId = "employee_id";
            public static string HealthTaxBaseAmount = "health_tax_base_amount";
            public static string HealthTaxAssessedAmount = "health_tax_assessed_amount";
            public static string WorkersCompensationBoardBaseAmount = "workers_compensation_board_base_amount";
            public static string WorkersCompensationBoardAssessedAmount = "workers_compensation_board_assessed_amount";
            public static string EmploymentInsuranceInsurableHour = "employment_insurance_insurable_hour";
            public static string EmploymentInsuranceInsurableEarningAmount = "employment_insurance_insurable_earning_amount";
            public static string ProvincialParentalInsurancePlanInsurableEarningAmount = "provincial_parental_insurance_plan_insurable_earning_amount";
            public static string CppQppPensionableAmount = "cpp_qpp_pensionable_amount";
        }
        #endregion

        #region select
        internal PayrollBatchCollection Select(DatabaseUser user, PayrollBatchCriteria criteria)
        {
            DataBaseCommand command = GetStoredProcCommand("PayrollBatch_select", user.DatabaseName);

            command.AddParameterWithValue("@payrollBatchId", criteria.PayrollBatchId);
            command.AddParameterWithValue("@payrollProcessId", criteria.PayrollProcessId);
            command.AddParameterWithValue("@performingCalc", criteria.PerformingCalc);
            command.AddParameterWithValue("@payrollProcessRunTypeCode", criteria.PayrollProcessRunTypeCode);
            command.AddParameterWithValue("@payrollProcessGroupCode", criteria.PayrollProcessGroupCode);
            command.AddParameterWithValue("@batchNumber", criteria.BatchSource);

            using (IDataReader reader = command.ExecuteReader())
                return MapToPayrollBatchCollection(reader);
        }
        internal PayrollBatchReportCollection SelectPayrollBatchSummary(DatabaseUser user, PayrollBatchCriteria criteria)
        {
            DataBaseCommand command = GetStoredProcCommand("PayrollBatch_report", user.DatabaseName);

            command.AddParameterWithValue("@payrollBatchId", criteria.PayrollBatchId);
            command.AddParameterWithValue("@batchSource", criteria.BatchSource);
            command.AddParameterWithValue("@description", criteria.Description);
            command.AddParameterWithValue("@payrollBatchStatusCode", criteria.PayrollBatchStatusCode);
            command.AddParameterWithValue("@payrollProcessRunTypeCode", criteria.PayrollProcessRunTypeCode);
            command.AddParameterWithValue("@payrollProcessGroupCode", criteria.PayrollProcessGroupCode);
            command.AddParameterWithValue("@processedFlag", criteria.ProcessedFlag);

            using (IDataReader reader = command.ExecuteReader())
                return MapToPayrollBatchReportCollection(reader);
        }
        #endregion

        #region update
        public void BatchUpdate(DatabaseUser user, PayrollBatchCollection items)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollBatch_BatchUpdateProcessIds", user.DatabaseName))
            {
                command.BeginTransaction("PayrollBatch_BatchUpdateProcessIds");

                //first table parameter
                SqlParameter parmPayrollBatch = new SqlParameter();
                parmPayrollBatch.ParameterName = "@parmPayrollBatch";
                parmPayrollBatch.SqlDbType = SqlDbType.Structured;

                DataTable tablePayrollBatch = LoadPayrollBatchDataTable(items);
                parmPayrollBatch.Value = tablePayrollBatch;
                command.AddParameter(parmPayrollBatch);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        private DataTable LoadPayrollBatchDataTable(PayrollBatchCollection items)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("payroll_batch_id", typeof(long));
            table.Columns.Add("batch_number", typeof(string));
            table.Columns.Add("description", typeof(string));
            table.Columns.Add("code_payroll_batch_type_cd", typeof(string));
            table.Columns.Add("code_payroll_batch_status_cd", typeof(string));
            table.Columns.Add("code_payroll_process_run_type_cd", typeof(string));
            table.Columns.Add("code_payroll_process_group_cd", typeof(string));
            table.Columns.Add("payroll_process_id", typeof(long));
            table.Columns.Add("system_generated_flag", typeof(bool));

            //loop thru and add values
            foreach (PayrollBatch item in items)
            {
                table.Rows.Add(new object[]
                {
                    item.PayrollBatchId,
                    item.BatchSource,
                    item.Description,
                    item.PayrollBatchTypeCode,
                    item.PayrollBatchStatusCode,
                    item.PayrollProcessRunTypeCode,
                    item.PayrollProcessGroupCode,
                    item.PayrollProcessId,
                    item.SystemGeneratedFlag
                });
            }

            return table;
        }
        public void Update(DatabaseUser user, PayrollBatch payrollBatch)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollBatch_update", user.DatabaseName))
            {
                command.BeginTransaction("PayrollBatch_update");

                command.AddParameterWithValue("@payrollBatchId", payrollBatch.PayrollBatchId);

                command.AddParameterWithValue("@batchNumber", payrollBatch.BatchSource);
                command.AddParameterWithValue("@batchStatusCd", payrollBatch.PayrollBatchStatusCode);
                command.AddParameterWithValue("@payrollProcessRunTypeCode", payrollBatch.PayrollProcessRunTypeCode);
                command.AddParameterWithValue("@payrollProcessGroupCode", payrollBatch.PayrollProcessGroupCode);
                command.AddParameterWithValue("@description", payrollBatch.Description);
                command.AddParameterWithValue("@payrollProcessId", payrollBatch.PayrollProcessId);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", payrollBatch.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                payrollBatch.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }
        #endregion

        public string GetProcessGroupCode(DatabaseUser user, long employeeId)
        {
            using (StoredProcedureCommand command = GetStoredProcCommand("GetPayProcessGroupByEmployeeId_select", user.DatabaseName))
            {
                string payProcessCode = "";

                try
                {
                    command.BeginTransaction("GetPayProcessGroupByEmployeeId_select");

                    command.AddParameterWithValue("@employeeId", employeeId);

                    SqlParameter returnValueParm = command.AddParameterWithValue("@returnValue", "", ParameterDirection.Output);
                    returnValueParm.Size = 32;

                    command.ExecuteNonQuery();

                    payProcessCode = returnValueParm.Value.ToString();

                    command.CommitTransaction();
                }
                catch (Exception exc)
                {
                    command.RollbackTransaction();
                    HandleException(exc, command.ReturnCode);
                }

                return payProcessCode;
            }
        }
        public PayrollPeriodCollection GetPayrollPeriodIdFromPayrollProcessGroupCode(DatabaseUser user, PayrollBatchCriteria payrollBatchCriteria)
        {
            //create criteria to call the "PayrollPeriod_select" proc in the SqlServerPayrollPeriod.cs file
            PayrollPeriodCriteria payrollPeriodCriteria = new PayrollPeriodCriteria();
            payrollPeriodCriteria.PayrollProcessGroupCode = payrollBatchCriteria.PayrollProcessGroupCode;

            if (payrollBatchCriteria.PayrollProcessRunTypeCode != "ADJUST" && payrollBatchCriteria.PayrollProcessRunTypeCode != "OFFCYCLE")
            {
                payrollPeriodCriteria.GetMinOpenPeriodFlag = true;
                payrollPeriodCriteria.GetMaxClosedPeriodFlag = false;
            }
            else
            {
                //adjustments use the previous closed payroll period
                payrollPeriodCriteria.GetMinOpenPeriodFlag = false;
                payrollPeriodCriteria.GetMaxClosedPeriodFlag = true;
            }

            SqlServerPayrollPeriod _client = new SqlServerPayrollPeriod();
            PayrollPeriodCollection payrollPeriods = _client.Select(user, payrollPeriodCriteria);

            return payrollPeriods;
        }

        #region insert
        public PayrollBatch Insert(DatabaseUser user, PayrollBatch payrollBatch)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollBatch_insert", user.DatabaseName))
            {
                command.BeginTransaction("PayrollBatch_insert");

                SqlParameter payrollBatchIdParm = command.AddParameterWithValue("@payrollBatchId", payrollBatch.PayrollBatchId, ParameterDirection.Output);

                command.AddParameterWithValue("@batchNumber", payrollBatch.BatchSource);
                command.AddParameterWithValue("@batchStatusCd", payrollBatch.PayrollBatchStatusCode);
                command.AddParameterWithValue("@payrollProcessRunTypeCode", payrollBatch.PayrollProcessRunTypeCode);
                command.AddParameterWithValue("@payrollProcessGroupCode", payrollBatch.PayrollProcessGroupCode);
                command.AddParameterWithValue("@description", payrollBatch.Description);
                command.AddParameterWithValue("@systemGeneratedFlag", payrollBatch.SystemGeneratedFlag);
                command.AddParameterWithValue("@codePayrollBatchTypeCd", payrollBatch.PayrollBatchTypeCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", payrollBatch.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                payrollBatch.PayrollBatchId = Convert.ToInt64(payrollBatchIdParm.Value);

                payrollBatch.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }

            return payrollBatch;
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, PayrollBatch payrollBatch)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollBatch_delete", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollBatch_delete");

                    command.AddParameterWithValue("@payrollBatchId", payrollBatch.PayrollBatchId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected PayrollBatchCollection MapToPayrollBatchCollection(IDataReader reader)
        {
            PayrollBatchCollection collection = new PayrollBatchCollection();

            while (reader.Read())
                collection.Add(MapToPayrollBatch(reader));

            return collection;
        }
        protected PayrollBatchReportCollection MapToPayrollBatchReportCollection(IDataReader reader)
        {
            PayrollBatchReportCollection collection = new PayrollBatchReportCollection();

            while (reader.Read())
                collection.Add(MapToPayrollBatchReport(reader));

            return collection;
        }
        protected PayrollBatchReport MapToPayrollBatchReport(IDataReader reader)
        {
            PayrollBatchReport payrollBatchReport = new PayrollBatchReport();
            MapToPayrollBatchReport(payrollBatchReport, reader);

            return payrollBatchReport;
        }
        protected PayrollBatchReport MapToPayrollBatchReport(PayrollBatchReport report, IDataReader reader)
        {
            base.MapToBase(report, reader);
            MapToPayrollBatch(report, reader);

            report.PayrollBatchId = (long)CleanDataValue(reader[ColumnNames.PayrollBatchId]);
            report.PayrollBatchTypeCode = (string)CleanDataValue(reader[ColumnNames.PayrollBatchTypeCode]);
            report.PayrollProcessRunTypeCode = (string)CleanDataValue(reader[ColumnNames.PayrollProcessRunTypeCode]);
            report.PayrollProcessRunTypeCodeDescription = (string)CleanDataValue(reader[ColumnNames.PayrollProcessRunTypeCodeDescription]);
            report.BatchSource = (string)CleanDataValue(reader[ColumnNames.BatchSource]);
            report.PayrollProcessGroupCode = (string)CleanDataValue(reader[ColumnNames.PayrollProcessGroupCode]);
            report.PayrollProcessGroupCodeDescription = (string)CleanDataValue(reader[ColumnNames.PayrollProcessGroupCodeDescription]);
            report.Description = (string)CleanDataValue(reader[ColumnNames.Description]);
            report.PayrollBatchStatusCode = (string)CleanDataValue(reader[ColumnNames.PayrollBatchStatusCode]);
            report.PayrollBatchStatusCodeDescription = (string)CleanDataValue(reader[ColumnNames.PayrollBatchStatusCodeDescription]);
            report.TotalHours = (decimal?)CleanDataValue(reader[ColumnNames.TotalHours]);
            report.TotalAmount = (decimal?)CleanDataValue(reader[ColumnNames.TotalAmount]);
            report.PayrollProcessStatusCode = (string)CleanDataValue(reader[ColumnNames.PayrollProcessStatusCode]);
            report.ProcessedFlag = (bool)CleanDataValue(reader[ColumnNames.ProcessedFlag]);
            report.SystemGeneratedFlag = (bool)CleanDataValue(reader[ColumnNames.systemGeneratedFlag]);

            return report;
        }
        protected PayrollBatch MapToPayrollBatch(IDataReader reader)
        {
            PayrollBatch payrollBatch = new PayrollBatch();
            MapToPayrollBatch(payrollBatch, reader);

            return payrollBatch;
        }
        protected void MapToPayrollBatch(PayrollBatch payrollBatch, IDataReader reader)
        {
            base.MapToBase(payrollBatch, reader);

            payrollBatch.PayrollBatchId = (long)CleanDataValue(reader[ColumnNames.PayrollBatchId]);
            payrollBatch.BatchSource = (string)CleanDataValue(reader[ColumnNames.BatchSource]);
            payrollBatch.Description = (string)CleanDataValue(reader[ColumnNames.Description]);
            payrollBatch.PayrollBatchTypeCode = (string)CleanDataValue(reader[ColumnNames.PayrollBatchTypeCode]);
            payrollBatch.PayrollProcessRunTypeCode = (string)CleanDataValue(reader[ColumnNames.PayrollProcessRunTypeCode]);
            payrollBatch.PayrollBatchStatusCode = (string)CleanDataValue(reader[ColumnNames.PayrollBatchStatusCode]);
            payrollBatch.PayrollProcessGroupCode = (string)CleanDataValue(reader[ColumnNames.PayrollProcessGroupCode]);
            payrollBatch.PayrollProcessId = (long?)CleanDataValue(reader[ColumnNames.PayrollProcessId]);
            payrollBatch.SystemGeneratedFlag = (bool)CleanDataValue(reader[ColumnNames.systemGeneratedFlag]);
        }
        #endregion
    }
}