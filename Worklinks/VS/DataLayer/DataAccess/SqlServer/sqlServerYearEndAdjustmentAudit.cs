﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerYearEndAdjustmentAudit : SqlServerBase
    {
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String YearEndAdjustmentAuditId = "year_end_adjustment_audit_id";
            public static String TableName = "table_name";
            public static String ColumnName = "column_name";
            public static String Id = "id";
            public static String OldValue = "old_value";
            public static String NewValue = "new_value";
            public static String SystemAdjustmentFlag = "system_adjustment_flag";
            public static String DeletedFlag = "deleted_flag";
        }
        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, YearEndAdjustment yearEndAdjustmentObj, bool deletedFlag)
        {
            using (DataBaseCommand command = GetStoredProcCommand("YearEndAdjustmentAudit_insert", user.DatabaseName))
            {
                command.BeginTransaction("YearEndAdjustmentAudit_insert");

                SqlParameter vendorIdParm = command.AddParameterWithValue("@yearEndAdjustmentAuditId", -1, ParameterDirection.Output);
                command.AddParameterWithValue("@tableName", yearEndAdjustmentObj.TableName);
                command.AddParameterWithValue("@columnName", yearEndAdjustmentObj.ColumnName);
                command.AddParameterWithValue("@id", yearEndAdjustmentObj.Id);
                command.AddParameterWithValue("@oldValue", yearEndAdjustmentObj.ColumnValue);
                command.AddParameterWithValue("@newValue", null);
                command.AddParameterWithValue("@systemAdjustmentFlag", false);
                command.AddParameterWithValue("@deletedFlag", deletedFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", new byte[8], ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
    }
}