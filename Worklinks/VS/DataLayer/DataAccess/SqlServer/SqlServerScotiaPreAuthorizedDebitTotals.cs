﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerScotiaPreAuthorizedDebitTotals : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string BusinessTaxNumber = "business_tax_number";
            public static string Amount = "amount";
            public static string CodePreAuthorizedBankCd = "code_pre_authorized_bank_cd";
            public static string PreAuthorizedTransitNumber = "pre_authorized_transit_number";
            public static string PreAuthorizedAccountNumber = "pre_authorized_account_number";
            public static string AbaNumber = "aba_number";
            public static string PaymentDueDate = "payment_due_date";
            public static string ProcessGroupDescription = "process_group_description";
        }
        #endregion

        #region main
        public ScotiaPreAuthorizedDebitTotalsCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            DataBaseCommand command = GetStoredProcCommand("ScotiaPreAuthorizedDebitTotals_select", user.DatabaseName);
            command.AddParameterWithValue("@databaseName", user.DatabaseName);
            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToScotiaPreAuthorizedDebitTotalsCollection(reader);
            }
        }
        #endregion

        #region data mapping
        protected ScotiaPreAuthorizedDebitTotalsCollection MapToScotiaPreAuthorizedDebitTotalsCollection(IDataReader reader)
        {
            ScotiaPreAuthorizedDebitTotalsCollection collection = new ScotiaPreAuthorizedDebitTotalsCollection();
            while (reader.Read())
            {
                collection.Add(MapToScotiaPreAuthorizedDebitTotals(reader));
            }

            return collection;
        }
        protected ScotiaPreAuthorizedDebitTotals MapToScotiaPreAuthorizedDebitTotals(IDataReader reader)
        {
            ScotiaPreAuthorizedDebitTotals item = new ScotiaPreAuthorizedDebitTotals();

            item.BusinessTaxNumber = (string)CleanDataValue(reader[ColumnNames.BusinessTaxNumber]);
            item.Amount = (Decimal)CleanDataValue(reader[ColumnNames.Amount]);
            item.CodePreAuthorizedBankCd = (string)CleanDataValue(reader[ColumnNames.CodePreAuthorizedBankCd]);
            item.PreAuthorizedTransitNumber = (string)(CleanDataValue(reader[ColumnNames.PreAuthorizedTransitNumber]));
            item.PreAuthorizedAccountNumber = (string)CleanDataValue(reader[ColumnNames.PreAuthorizedAccountNumber]);
            item.AbaNumber = (string)CleanDataValue(reader[ColumnNames.AbaNumber]);
            item.PaymentDueDate = (DateTime)CleanDataValue(reader[ColumnNames.PaymentDueDate]);
            item.ProcessGroupDescription = (string)CleanDataValue(reader[ColumnNames.ProcessGroupDescription]);

            return item;
        }
        #endregion
    }
}
