﻿using System;
using System.Data;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerREALCustomExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string RowNumber = "row_number";
            public static string EmployeeNumber = "employee_number";
            public static string EmployeeFirstName = "first_name";
            public static string EmployeeLastName = "last_name";
            public static string TransactionDate = "transaction_date";
            public static string PayCodeType = "paycode_type_description";
            public static string PayCode = "code_paycode_cd";
            public static string PayCodeDescription = "paycode_description";
            public static string CurrentHours = "current_unit";
            public static string CurrentRate = "rate";
            public static string CurrentAmount = "current_amount";
            public static string CostCentreNumber = "cost_centre_number";
            public static string CostCentreDescription = "cost_centre_description";
            public static string PayPeriod = "period";
        }
        #endregion

        #region main
        public REALCustomExportCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            DataBaseCommand command = GetStoredProcCommand("REALcustom_export", user.DatabaseName);
            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToREALCustomExportCollection(reader);
        }
        #endregion

        #region data mapping
        protected REALCustomExportCollection MapToREALCustomExportCollection(IDataReader reader)
        {
            REALCustomExportCollection collection = new REALCustomExportCollection();

            while (reader.Read())
                collection.Add(MapToREALCustomExport(reader));

            return collection;
        }
        protected REALCustomExport MapToREALCustomExport(IDataReader reader)
        {
            REALCustomExport item = new REALCustomExport();

            item.RowNumber = (long)CleanDataValue(reader[ColumnNames.RowNumber]);
            item.EmployeeNumber = (string)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.EmployeeFirstName = (string)CleanDataValue(reader[ColumnNames.EmployeeFirstName]);
            item.EmployeeLastName = (string)CleanDataValue(reader[ColumnNames.EmployeeLastName]);
            item.TransactionDate = (DateTime)CleanDataValue(reader[ColumnNames.TransactionDate]);
            item.PayCodeType = (string)CleanDataValue(reader[ColumnNames.PayCodeType]);
            item.PayCode = (string)CleanDataValue(reader[ColumnNames.PayCode]);
            item.PayCodeDescription = (string)CleanDataValue(reader[ColumnNames.PayCodeDescription]);
            item.CurrentHours = CleanDataValue(reader[ColumnNames.CurrentHours]).ToString();
            item.CurrentRate = CleanDataValue(reader[ColumnNames.CurrentRate]).ToString();
            item.CurrentAmount = CleanDataValue(reader[ColumnNames.CurrentAmount]).ToString();
            item.CostCentreNumber = (string)CleanDataValue(reader[ColumnNames.CostCentreNumber]);
            item.CostCentreDescription = (string)CleanDataValue(reader[ColumnNames.CostCentreDescription]);
            item.PayPeriod = CleanDataValue(reader[ColumnNames.PayPeriod]).ToString();

            return item;
        }
        #endregion
    }
}