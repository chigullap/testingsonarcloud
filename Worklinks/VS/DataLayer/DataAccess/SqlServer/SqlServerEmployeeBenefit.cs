﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeBenefit : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeBenefitId = "employee_benefit_id";
            public static String EmployeeId = "employee_id";
            public static String EffectiveDate = "effective_date";
            public static String BenefitPolicyId = "benefit_policy_id";
            public static String BenefitStatusCode = "code_benefit_status_cd";
            public static String BenefitCoverageCode = "code_benefit_coverage_cd";
            public static String BenefitSmokerCode = "code_benefit_smoker_cd";
            public static String EligibilityDate = "eligibility_date";
            public static String EnrolmentDate = "enrolment_date";
        }
        #endregion

        #region main
        internal EmployeeBenefitCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeeId, bool securityOverrideFlag)
        {
            return Select(user, employeeId, null, securityOverrideFlag);
        }

        internal EmployeeBenefitCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeeId, long? employeeBenefitId, bool securityOverrideFlag)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeeBenefit_select", user.DatabaseName);

            command.AddParameterWithValue("@securityOverride", securityOverrideFlag);
            command.AddParameterWithValue("@employeeId", employeeId);
            command.AddParameterWithValue("@employeeBenefitId", employeeBenefitId);
            command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
            command.AddParameterWithValue("@securityUserId", user.SecurityUserId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToEmployeeBenefitCollection(reader);
        }

        public EmployeeBenefit Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeBenefit employeeBenefit)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeBenefit_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeBenefit_insert");

                SqlParameter employeeBenefitIdParam = command.AddParameterWithValue("@employeeBenefitId", employeeBenefit.EmployeeBenefitId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", employeeBenefit.EmployeeId);
                command.AddParameterWithValue("@effectiveDate", employeeBenefit.EffectiveDate);
                command.AddParameterWithValue("@benefitPolicyId", employeeBenefit.BenefitPolicyId);
                command.AddParameterWithValue("@benefitStatusCode", employeeBenefit.BenefitStatusCode);
                command.AddParameterWithValue("@benefitCoverageCode", employeeBenefit.BenefitCoverageCode);
                command.AddParameterWithValue("@benefitSmokerCode", employeeBenefit.BenefitSmokerCode);
                command.AddParameterWithValue("@eligibilityDate", employeeBenefit.EligibilityDate);
                command.AddParameterWithValue("@enrolmentDate", employeeBenefit.EnrolmentDate);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", employeeBenefit.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                employeeBenefit.EmployeeBenefitId = Convert.ToInt64(employeeBenefitIdParam.Value);
                employeeBenefit.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return employeeBenefit;
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeBenefit employeeBenefit)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeBenefit_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeBenefit_update");

                    command.AddParameterWithValue("@employeeBenefitId", employeeBenefit.EmployeeBenefitId);
                    command.AddParameterWithValue("@employeeId", employeeBenefit.EmployeeId);
                    command.AddParameterWithValue("@effectiveDate", employeeBenefit.EffectiveDate);
                    command.AddParameterWithValue("@benefitPolicyId", employeeBenefit.BenefitPolicyId);
                    command.AddParameterWithValue("@benefitStatusCode", employeeBenefit.BenefitStatusCode);
                    command.AddParameterWithValue("@benefitCoverageCode", employeeBenefit.BenefitCoverageCode);
                    command.AddParameterWithValue("@benefitSmokerCode", employeeBenefit.BenefitSmokerCode);
                    command.AddParameterWithValue("@eligibilityDate", employeeBenefit.EligibilityDate);
                    command.AddParameterWithValue("@enrolmentDate", employeeBenefit.EnrolmentDate);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", employeeBenefit.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    employeeBenefit.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeBenefit employeeBenefit)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeBenefit_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeBenefit_delete");

                    command.AddParameterWithValue("@employeeBenefitId", employeeBenefit.EmployeeBenefitId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeBenefitCollection MapToEmployeeBenefitCollection(IDataReader reader)
        {
            EmployeeBenefitCollection collection = new EmployeeBenefitCollection();

            while (reader.Read())
                collection.Add(MapToEmployeeBenefit(reader));

            return collection;
        }

        protected EmployeeBenefit MapToEmployeeBenefit(IDataReader reader)
        {
            EmployeeBenefit item = new EmployeeBenefit();
            base.MapToBase(item, reader);

            item.EmployeeBenefitId = (long)CleanDataValue(reader[ColumnNames.EmployeeBenefitId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EffectiveDate = (DateTime?)CleanDataValue(reader[ColumnNames.EffectiveDate]);
            item.BenefitPolicyId = (long)CleanDataValue(reader[ColumnNames.BenefitPolicyId]);
            item.BenefitStatusCode = (String)CleanDataValue(reader[ColumnNames.BenefitStatusCode]);
            item.BenefitCoverageCode = (String)CleanDataValue(reader[ColumnNames.BenefitCoverageCode]);
            item.BenefitSmokerCode = (String)CleanDataValue(reader[ColumnNames.BenefitSmokerCode]);
            item.EligibilityDate = (DateTime?)CleanDataValue(reader[ColumnNames.EligibilityDate]);
            item.EnrolmentDate = (DateTime?)CleanDataValue(reader[ColumnNames.EnrolmentDate]);

            return item;
        }
        #endregion
    }
}
