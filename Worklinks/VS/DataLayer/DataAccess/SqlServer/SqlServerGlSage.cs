﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerGlSage : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String Header1RecType = "header1_rec_type";
            public static String Header1BatchId = "header1_batch_id";
            public static String Header1BatchEntry = "header1_batch_entry";
            public static String Header1SourceLedger = "header1_source_ledger";
            public static String Header1SourceType = "header1_source_type";
            public static String Header1JournalDescription = "header1_journal_description";
            public static String Header1FiscalYear = "header1_fiscal_year";
            public static String Header1FiscalMonth = "header1_fiscal_month";
            public static String Header1DateEntry = "header1_date_entry";
            public static String Header2RecType = "header2_rec_type";
            public static String Header2BatchId = "header2_batch_id";
            public static String Header2JournalEntry = "header2_journal_entry";
            public static String Header2TransactionNumber = "header2_transaction_number";
            public static String Header2AccountNumber = "header2_account_number";
            public static String Header2TransactionAmount = "header2_transaction_amount";
            public static String Header2TransactionDesc = "header2_transaction_desc";
            public static String Header2TransactionRef = "header2_transaction_ref";
            public static String Header2TransactionDate = "header2_transaction_date";
        }
        #endregion

        #region main
        internal GlSageCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            DataBaseCommand command = GetStoredProcCommand("GLReport_select", user.DatabaseName);
            command.AddParameterWithValue("@databaseName", user.DatabaseName);
            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToGlSageCollection(reader);
            }
        }
        #endregion


        #region data mapping
        protected GlSageCollection MapToGlSageCollection(IDataReader reader)
        {
            int i = 1;
            GlSageCollection collection = new GlSageCollection();
            while (reader.Read())
            {
                collection.Add(MapToGlSage(reader, i++));
            }

            return collection;
        }
        protected GlSage MapToGlSage(IDataReader reader, int key)
        {
            GlSage item = new GlSage();

            item.GlSageId = Convert.ToInt64(key);

            item.Header1RecType = (string)CleanDataValue(reader[ColumnNames.Header1RecType]);
            item.Header1BatchId = (string)CleanDataValue(reader[ColumnNames.Header1BatchId]);
            item.Header1BatchEntry = CleanDataValue(reader[ColumnNames.Header1BatchEntry]).ToString();
            item.Header1SourceLedger = (string)CleanDataValue(reader[ColumnNames.Header1SourceLedger]);
            item.Header1SourceType = (string)CleanDataValue(reader[ColumnNames.Header1SourceType]);
            item.Header1JournalDescription = (string)CleanDataValue(reader[ColumnNames.Header1JournalDescription]);
            item.Header1FiscalYear = CleanDataValue(reader[ColumnNames.Header1FiscalYear]).ToString();
            item.Header1FiscalMonth = CleanDataValue(reader[ColumnNames.Header1FiscalMonth]).ToString();
            item.Header1DateEntry = (DateTime)CleanDataValue(reader[ColumnNames.Header1DateEntry]);
            item.Header2RecType = (string)CleanDataValue(reader[ColumnNames.Header2RecType]);
            item.Header2BatchId = (string)CleanDataValue(reader[ColumnNames.Header2BatchId]);
            item.Header2JournalEntry = CleanDataValue(reader[ColumnNames.Header2JournalEntry]).ToString();
            item.Header2TransactionNumber = (string)CleanDataValue(reader[ColumnNames.Header2TransactionNumber]).ToString();
            item.Header2AccountNumber = (string)CleanDataValue(reader[ColumnNames.Header2AccountNumber]);
            item.Header2TransactionAmount = (decimal)CleanDataValue(reader[ColumnNames.Header2TransactionAmount]);
            item.Header2TransactionDesc = (string)CleanDataValue(reader[ColumnNames.Header2TransactionDesc]);
            item.Header2TransactionRef = (string)CleanDataValue(reader[ColumnNames.Header2TransactionRef]);
            item.Header2TransactionDate = (DateTime)CleanDataValue(reader[ColumnNames.Header2TransactionDate]);

            return item;
        }
        #endregion
    }
}
