﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerOrganizationUnitLevel : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string OrganizationUnitLevelId = "organization_unit_level_id";
            public static string ParentOrganizationUnitLevelId = "parent_organization_unit_level_id";
            public static string LanguageCode = "code_language_cd";
            public static string Description = "description";
            public static string UsedOnPayslipFlag = "used_on_payslip_flag";
            public static string OrgOverrideFlag = "org_override_flag";
            public static string EmployeesExistFlag = "employees_exist_flag";
            public static string OrgUnitsCount = "org_units_count";
        }
        #endregion

        #region select
        internal OrganizationUnitLevelCollection Select(DatabaseUser user)
        {
            using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnitLevel_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@langaugeCode", user.LanguageCode);

                OrganizationUnitLevelCollection collection = null;

                using (IDataReader reader = command.ExecuteReader())
                    collection = MapToCollection(reader);

                return collection;
            }
        }
        #endregion

        #region insert
        public OrganizationUnitLevel Insert(DatabaseUser user, OrganizationUnitLevel item)
        {
            OrganizationUnitLevel organizationUnitLevel = new OrganizationUnitLevel();

            using (StoredProcedureCommand command = GetStoredProcCommand("OrganizationUnitLevel_insert", user.DatabaseName))
            {
                try
                {
                    command.BeginTransaction("OrganizationUnitLevel_insert");

                    SqlParameter organizationUnitLevelIdParm = command.AddParameterWithValue("@organizationUnitLevelId", item.OrganizationUnitLevelId, ParameterDirection.InputOutput);
                    SqlParameter parentOrganizationUnitLevelIdParm = command.AddParameterWithValue("@parentOrganizationUnitLevelId", item.ParentOrganizationUnitLevelId, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@usedOnPayslipFlag", item.UsedOnPayslipFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;
                    item.OrganizationUnitLevelId = (long)organizationUnitLevelIdParm.Value;
                    item.ParentOrganizationUnitLevelId = CheckProcReturnVal(parentOrganizationUnitLevelIdParm);

                    command.CommitTransaction();

                    organizationUnitLevel = item;
                }
                catch (Exception exc)
                {
                    command.RollbackTransaction();
                    HandleException(exc, ((SqlException)exc).Number);
                }

                return organizationUnitLevel;
            }
        }
        private long? CheckProcReturnVal(SqlParameter ParentOrganizationUnitLevelIdParm)
        {
            //If return value is -1 then assign null and return....
            if ((long)ParentOrganizationUnitLevelIdParm.Value == -1)
                return null;

            return (long)ParentOrganizationUnitLevelIdParm.Value;
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, OrganizationUnitLevel item)
        {
            try
            {
                using (StoredProcedureCommand command = GetStoredProcCommand("OrganizationUnitLevel_update", user.DatabaseName))
                {
                    command.BeginTransaction("OrganizationUnitLevel_update");

                    command.AddParameterWithValue("@organizationUnitLevelId", item.OrganizationUnitLevelId);
                    command.AddParameterWithValue("@usedOnPayslipFlag", item.UsedOnPayslipFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, OrganizationUnitLevel item)
        {
            try
            {
                using (StoredProcedureCommand command = GetStoredProcCommand("OrganizationUnitLevel_delete", user.DatabaseName))
                {
                    command.BeginTransaction("OrganizationUnitLevel_delete");

                    command.AddParameterWithValue("@organizationUnitLevelId", item.OrganizationUnitLevelId);
                    command.AddParameterWithValue("@rowVersion", item.RowVersion);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected OrganizationUnitLevelCollection MapToCollection(IDataReader reader)
        {
            OrganizationUnitLevelCollection collection = new OrganizationUnitLevelCollection();

            while (reader.Read())
                collection.Add(MapToItem(reader));

            return collection;
        }
        protected OrganizationUnitLevel MapToItem(IDataReader reader)
        {
            OrganizationUnitLevel item = new OrganizationUnitLevel();
            base.MapToBase(item, reader);

            item.OrganizationUnitLevelId = (long)CleanDataValue(reader[ColumnNames.OrganizationUnitLevelId]);
            item.ParentOrganizationUnitLevelId = (long?)CleanDataValue(reader[ColumnNames.ParentOrganizationUnitLevelId]);
            item.LanguageCode = (String)CleanDataValue(reader[ColumnNames.LanguageCode]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            item.UsedOnPayslipFlag = (bool)CleanDataValue(reader[ColumnNames.UsedOnPayslipFlag]);
            item.OrgOverrideFlag = (bool)CleanDataValue(reader[ColumnNames.OrgOverrideFlag]);
            item.EmployeesExistFlag = (bool)CleanDataValue(reader[ColumnNames.EmployeesExistFlag]);
            item.OrgUnitsCount = (int)CleanDataValue(reader[ColumnNames.OrgUnitsCount]);

            return item;
        }
        #endregion
    }
}