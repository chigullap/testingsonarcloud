﻿using System;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerReportParameter : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ReportParameterId = "report_parameter_id";
            public static String ReportId = "report_id";
            public static String ParameterName = "parameter_name";
            public static String ParameterOrder = "parameter_order";
            public static String ParameterType = "parameter_type";
        }
        #endregion

        #region main
        /// <summary>
        /// returns a ReportParameter
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        internal ReportParameterCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long reportId)
        {
            using (WLP.DataLayer.SqlClient.DataBaseCommand command = GetStoredProcCommand("ReportParameter_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@reportId", reportId);

                ReportParameterCollection items = null;

                using (System.Data.IDataReader reader = command.ExecuteReader())
                    items = MapToReportParameterCollection(reader);

                return items;
            }
        }
        #endregion

        #region data mapping
        /// <summary>
        /// maps result set to ReportParameter collection
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected ReportParameterCollection MapToReportParameterCollection(System.Data.IDataReader reader)
        {
            ReportParameterCollection collection = new ReportParameterCollection();

            while (reader.Read())
                collection.Add(MapToReportParameter(reader));

            return collection;
        }
        /// <summary>
        /// maps columns to ReportParameter properties
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected ReportParameter MapToReportParameter(System.Data.IDataReader reader)
        {
            ReportParameter item = new ReportParameter();
            base.MapToBase(item, reader);

            item.ReportParameterId = (long)CleanDataValue(reader[ColumnNames.ReportParameterId]);
            item.ReportId = (long)CleanDataValue(reader[ColumnNames.ReportId]);
            item.ParameterName = (String)CleanDataValue(reader[ColumnNames.ParameterName]);
            item.ParameterOrder = (int)CleanDataValue(reader[ColumnNames.ParameterOrder]);
            item.ParameterType = (String)CleanDataValue(reader[ColumnNames.ParameterType]);

            return item;
        }
        #endregion
    }
}