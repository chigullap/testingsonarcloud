﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerSecurityUserPasswordHistory : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String SecurityUserPasswordHistoryId = "security_user_password_history_id";
            public static String SecurityUserId = "security_user_id";
            public static String UserName = "user_name";
            public static String Password = "password";
            public static String PasswordSalt = "password_salt";
            public static String PersonId = "person_id";
            public static String LastPasswordChangedDatetime = "last_password_changed_datetime";
        }
        #endregion

        #region main

        internal SecurityUserPasswordHistoryCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String userName)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SecurityUserPasswordHistory_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@userName", userName);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToSecurityUserPasswordHistoryCollection(reader);
                }
            }
        }
        //keep a history of user passwords to compare to when preventing password re-use
        public void InsertPasswordHistory(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, SecurityUserPasswordHistory passHistory)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SecurityUserPasswordHistory_insert", user.DatabaseName))
            {
                command.BeginTransaction("SecurityUserPasswordHistory_insert");

                SqlParameter SecurityUserPassHistoryIdParm = command.AddParameterWithValue("@securityUserPasswordHistoryId", passHistory.SecurityUserPasswordHistoryId, ParameterDirection.Output);

                command.AddParameterWithValue("@securityUserId", passHistory.SecurityUserId);
                command.AddParameterWithValue("@userName", passHistory.UserName);
                command.AddParameterWithValue("@oldPassword", passHistory.Password);
                command.AddParameterWithValue("@oldSalt", passHistory.PasswordSalt);
                command.AddParameterWithValue("@personId", passHistory.PersonId);
                command.AddParameterWithValue("@LastPasswordChangedDatetime", Time);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", passHistory.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@createUser", passHistory.CreateUser);
                command.AddParameterWithValue("@createDatetime", Time);

                command.ExecuteNonQuery();
                passHistory.SecurityUserPasswordHistoryId = Convert.ToInt64(SecurityUserPassHistoryIdParm.Value);
                command.CommitTransaction();
            }
        }

        #endregion


        #region data mapping
        protected SecurityUserPasswordHistoryCollection MapToSecurityUserPasswordHistoryCollection(IDataReader reader)
        {
            SecurityUserPasswordHistoryCollection collection = new SecurityUserPasswordHistoryCollection();

            while (reader.Read())
            {
                collection.Add(MapToSecurityUserPasswordHistory(reader));
            }

            return collection;
        }
        protected SecurityUserPasswordHistory MapToSecurityUserPasswordHistory(IDataReader reader)
        {
            SecurityUserPasswordHistory item = new SecurityUserPasswordHistory();
            base.MapToBase(item, reader);

            item.SecurityUserPasswordHistoryId = (long)CleanDataValue(reader[ColumnNames.SecurityUserPasswordHistoryId]);
            item.SecurityUserId = (long)CleanDataValue(reader[ColumnNames.SecurityUserId]);
            item.UserName = (String)CleanDataValue(reader[ColumnNames.UserName]);
            item.Password = (String)CleanDataValue(reader[ColumnNames.Password]);
            item.PasswordSalt = (String)CleanDataValue(reader[ColumnNames.PasswordSalt]);
            item.PersonId = (long?)CleanDataValue(reader[ColumnNames.PersonId]);
            item.LastPasswordChangedDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.LastPasswordChangedDatetime]);

            return item;
        }
        #endregion
    }
}
