﻿using System;
using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerPayrollBreakdown : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayrollBreakdownId = "payroll_breakdown_id";
            public static String Description = "description";
            public static String GrossPay = "gross_pay";
            public static String NetPay = "net_pay";
            public static String Taxes = "taxes";
            public static String Deductions = "deductions";
        }
        #endregion

        #region main
        internal PayrollBreakdownCollection Select(DatabaseUser user, String userName, String breakdownType, String filteredBy)
        {
            DataBaseCommand command = GetStoredProcCommand("PayrollBreakdown_select", user.DatabaseName);

            command.AddParameterWithValue("@userName", userName);
            command.AddParameterWithValue("@breakdownType", breakdownType);
            command.AddParameterWithValue("@filteredBy", filteredBy);
            command.AddParameterWithValue("@languageCode", user.LanguageCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCollection(reader);
        }
        #endregion

        #region data mapping
        protected PayrollBreakdownCollection MapToCollection(IDataReader reader)
        {
            PayrollBreakdownCollection collection = new PayrollBreakdownCollection();

            while (reader.Read())
                collection.Add(MapToItem(reader));

            return collection;
        }
        protected PayrollBreakdown MapToItem(IDataReader reader)
        {
            PayrollBreakdown item = new PayrollBreakdown();

            item.PayrollBreakdownId = (long)CleanDataValue(reader[ColumnNames.PayrollBreakdownId]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            item.GrossPay = (Decimal)CleanDataValue(reader[ColumnNames.GrossPay]);
            item.NetPay = (Decimal)CleanDataValue(reader[ColumnNames.NetPay]);
            item.Taxes = (Decimal)CleanDataValue(reader[ColumnNames.Taxes]);
            item.Deductions = (Decimal)CleanDataValue(reader[ColumnNames.Deductions]);

            return item;
        }
        #endregion
    }
}