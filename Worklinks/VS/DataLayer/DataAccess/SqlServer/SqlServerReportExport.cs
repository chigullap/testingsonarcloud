﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerReportExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ExportId = "export_id";
            public static String Name = "name";
            public static String ReportId = "report_id";
            public static String FileFormat = "file_format";
            public static String CodeExportTypeCd = "code_export_type_cd";
            public static String ActiveFlag = "active_flag";
            public static String HasChildrenFlag = "has_children_flag";
        }
        #endregion

        #region select
        internal ReportExportCollection Select(DatabaseUser user, long? exportId)
        {
            DataBaseCommand command = GetStoredProcCommand("ReportExport_select", user.DatabaseName);
            command.AddParameterWithValue("@exportId", exportId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToReportExportCollection(reader);
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, ReportExport export)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("ReportExport_update", user.DatabaseName))
                {
                    command.BeginTransaction("ReportExport_update");

                    command.AddParameterWithValue("@exportId", export.ExportId);
                    command.AddParameterWithValue("@name", export.Name);
                    command.AddParameterWithValue("@reportId", export.ReportId);
                    command.AddParameterWithValue("@fileFormat", export.FileFormat);
                    command.AddParameterWithValue("@codeExportTypeCd", export.ExportTypeCode);
                    command.AddParameterWithValue("@activeFlag", export.ActiveFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", export.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    export.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region insert
        public ReportExport Insert(DatabaseUser user, ReportExport export)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ReportExport_insert", user.DatabaseName))
            {
                command.BeginTransaction("ReportExport_insert");

                SqlParameter exportIdParm = command.AddParameterWithValue("@exportId", export.ExportId, ParameterDirection.Output);
                command.AddParameterWithValue("@name", export.Name);
                command.AddParameterWithValue("@reportId", export.ReportId);
                command.AddParameterWithValue("@fileFormat", export.FileFormat);
                command.AddParameterWithValue("@codeExportTypeCd", export.ExportTypeCode);
                command.AddParameterWithValue("@activeFlag", export.ActiveFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", export.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                export.ExportId = Convert.ToInt64(exportIdParm.Value);
                export.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return export;
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, ReportExport export)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("ReportExport_delete", user.DatabaseName))
                {
                    command.BeginTransaction("ReportExport_delete");
                    command.AddParameterWithValue("@exportId", export.ExportId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected ReportExportCollection MapToReportExportCollection(IDataReader reader)
        {
            ReportExportCollection collection = new ReportExportCollection();

            while (reader.Read())
                collection.Add(MapToReportExport(reader));

            return collection;
        }

        protected ReportExport MapToReportExport(IDataReader reader)
        {
            ReportExport item = new ReportExport();
            base.MapToBase(item, reader);

            item.ExportId = (long)CleanDataValue(reader[ColumnNames.ExportId]);
            item.Name = (String)CleanDataValue(reader[ColumnNames.Name]);
            item.ReportId = (long?)CleanDataValue(reader[ColumnNames.ReportId]);
            item.FileFormat = (String)CleanDataValue(reader[ColumnNames.FileFormat]);
            item.ExportTypeCode = (String)CleanDataValue(reader[ColumnNames.CodeExportTypeCd]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.HasChildrenFlag = (bool)CleanDataValue(reader[ColumnNames.HasChildrenFlag]);

            return item;
        }
        #endregion
    }
}