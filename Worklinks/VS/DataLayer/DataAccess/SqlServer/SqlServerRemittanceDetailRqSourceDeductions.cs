﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerRemittanceDetailRqSourceDeductions : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String DummyKey = "row_number";
            public static String RevenuQuebecBusinessTaxNumber = "revenu_quebec_tax_id";
            public static String DatabaseColumns = "database_columns";
            public static String ChequeDate = "cheque_date";
            public static String PaymentAmount = "payment_amount";
            public static String Period = "period";
            public static String StartDate = "start_date";
            public static String CutoffDate = "cutoff_date";
            public static String PayrollProcessId = "payroll_process_id";
        }
        #endregion

        #region main
        internal RemittanceDetailRqSourceDeductionsCollection Select(DatabaseUser user, long? rqExportId)
        {
            DataBaseCommand command = GetStoredProcCommand("RemittanceDetailRqSourceDeductions_report", user.DatabaseName);
            command.AddParameterWithValue("@rqExportId", rqExportId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToRemittanceDetailRqSourceDeductionsCollection(reader);
        }
        #endregion

        #region data mapping
        protected RemittanceDetailRqSourceDeductionsCollection MapToRemittanceDetailRqSourceDeductionsCollection(IDataReader reader)
        {
            RemittanceDetailRqSourceDeductionsCollection collection = new RemittanceDetailRqSourceDeductionsCollection();

            while (reader.Read())
                collection.Add(MapToRemittanceDetailRqSourceDeductions(reader));

            return collection;
        }
        protected RemittanceDetailRqSourceDeductions MapToRemittanceDetailRqSourceDeductions(IDataReader reader)
        {
            RemittanceDetailRqSourceDeductions item = new RemittanceDetailRqSourceDeductions();

            item.DummyKey = (long)CleanDataValue(reader[ColumnNames.DummyKey]);
            item.RevenuQuebecBusinessTaxNumber = (string)CleanDataValue(reader[ColumnNames.RevenuQuebecBusinessTaxNumber]);
            item.DatabaseColumns = (string)CleanDataValue(reader[ColumnNames.DatabaseColumns]);
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.PaymentAmount = (Decimal)CleanDataValue(reader[ColumnNames.PaymentAmount]);
            item.Period = (Decimal)CleanDataValue(reader[ColumnNames.Period]);
            item.StartDate = (DateTime)CleanDataValue(reader[ColumnNames.StartDate]);
            item.CutoffDate = (DateTime)CleanDataValue(reader[ColumnNames.CutoffDate]);
            item.PayrollProcessId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessId]);

            return item;
        }
        #endregion
    }
}