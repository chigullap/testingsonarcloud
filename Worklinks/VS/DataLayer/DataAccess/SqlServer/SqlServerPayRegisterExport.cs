﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerPayRegisterExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayrollProcessExportQueueId = "payroll_process_export_queue_id";
            public static String ExportId = "export_id";
            public static String ExportFtpId = "export_ftp_id";
            public static String ActiveFlag = "active_flag";
            public static String UnzipFlag = "unzip_flag";
        }
        #endregion

        #region select
        internal PayRegisterExportCollection Select(DatabaseUser user, long? exportQueueId, string codeExportFtpTypeCd)
        {
            DataBaseCommand command = GetStoredProcCommand("PayRegisterExport_select", user.DatabaseName);
            command.AddParameterWithValue("@exportQueueId", exportQueueId);
            command.AddParameterWithValue("@codeExportFtpTypeCd", codeExportFtpTypeCd); 

            using (IDataReader reader = command.ExecuteReader())
                return MapToPayRegisterExportCollection(reader);
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, PayRegisterExport export)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayRegisterExport_update", user.DatabaseName))
                {
                    command.BeginTransaction("PayRegisterExportp_update");

                    command.AddParameterWithValue("@exportQueueId", export.PayrollProcessExportQueueId);
                    command.AddParameterWithValue("@exportId", export.ExportId);
                    command.AddParameterWithValue("@exportFtpId", export.ExportFtpId);
                    command.AddParameterWithValue("@activeFlag", export.ActiveFlag);
                    command.AddParameterWithValue("@unzipFlag", export.UnzipFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", export.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    export.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region insert
        public PayRegisterExport Insert(DatabaseUser user, PayRegisterExport export)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayRegisterExport_insert", user.DatabaseName))
            {
                command.BeginTransaction("PayRegisterExport_insert");

                SqlParameter payrollProcessExportQueueIdParm = command.AddParameterWithValue("@exportQueueId", export.PayrollProcessExportQueueId, ParameterDirection.Output);
                command.AddParameterWithValue("@exportId", export.ExportId);
                command.AddParameterWithValue("@exportFtpId", export.ExportFtpId);
                command.AddParameterWithValue("@activeFlag", export.ActiveFlag);
                command.AddParameterWithValue("@unzipFlag", export.UnzipFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", export.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                export.PayrollProcessExportQueueId = Convert.ToInt64(payrollProcessExportQueueIdParm.Value);
                export.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return export;
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, PayRegisterExport export)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayRegisterExport_delete", user.DatabaseName))
                {
                    command.BeginTransaction("PayRegisterExport_delete");
                    command.AddParameterWithValue("@exportQueueId", export.PayrollProcessExportQueueId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected PayRegisterExportCollection MapToPayRegisterExportCollection(IDataReader reader)
        {
            PayRegisterExportCollection collection = new PayRegisterExportCollection();

            while (reader.Read())
                collection.Add(MapToPayRegisterExport(reader));

            return collection;
        }

        protected PayRegisterExport MapToPayRegisterExport(IDataReader reader)
        {
            PayRegisterExport item = new PayRegisterExport();
            base.MapToBase(item, reader);

            item.PayrollProcessExportQueueId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessExportQueueId]);
            item.ExportId = (long)CleanDataValue(reader[ColumnNames.ExportId]);

            if (CleanDataValue(reader[ColumnNames.ExportFtpId]) != null)
                item.ExportFtpId = (long)CleanDataValue(reader[ColumnNames.ExportFtpId]);

            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.UnzipFlag = (bool)CleanDataValue(reader[ColumnNames.UnzipFlag]);

            return item;
        }
        #endregion
    }
}