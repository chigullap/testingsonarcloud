﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerREALAdjustmentRule : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string Key = "key";
            public static string PersonNumber = "id";
            public static string PersonName = "person_name";
            public static string EffectiveDate = "effective_date";
            public static string Job = "job";
            public static string Rate = "flat_rate";
        }
        #endregion

        #region main
        public AdjustmentRuleExportCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user)
        {
            DataBaseCommand command = GetStoredProcCommand("REALAdjustmentRuleExport_report", user.DatabaseName);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToAdjustmentRuleExportCollection(reader);
            }
        }
        #endregion

        #region data mapping
        protected AdjustmentRuleExportCollection MapToAdjustmentRuleExportCollection(IDataReader reader)
        {
            AdjustmentRuleExportCollection collection = new AdjustmentRuleExportCollection();
            while (reader.Read())
            {
                collection.Add(MapToAdjustmentRuleExport(reader));
            }
            return collection;
        }
        protected AdjustmentRuleExport MapToAdjustmentRuleExport(IDataReader reader)
        {
            AdjustmentRuleExport item = new AdjustmentRuleExport();

            item.DummyKey = (long)CleanDataValue(reader[ColumnNames.Key]);
            item.PersonNumber = (string)CleanDataValue(reader[ColumnNames.PersonNumber]);
            item.PersonName = (string)CleanDataValue(reader[ColumnNames.PersonName]);
            item.EffectiveDate = (DateTime)CleanDataValue(reader[ColumnNames.EffectiveDate]);
            item.Job = (string)CleanDataValue(reader[ColumnNames.Job]);
            item.Rate = (decimal)CleanDataValue(reader[ColumnNames.Rate]);

            return item;
        }
        #endregion
    }
}
