﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerAccrualPolicy : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String AccrualPolicyId = "accrual_policy_id";
            public static String EnglishDescription = "english_description";
            public static String FrenchDescription = "french_description";
        }
        #endregion

        #region main
        internal AccrualPolicyCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? policyId)
        {
            DataBaseCommand command = GetStoredProcCommand("AccrualPolicy_select", user.DatabaseName);

            command.AddParameterWithValue("@accrualPolicyId", policyId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToAccrualPolicyCollection(reader);
        }
        public AccrualPolicy Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualPolicy policy)
        {
            using (DataBaseCommand command = GetStoredProcCommand("AccrualPolicy_insert", user.DatabaseName))
            {
                command.BeginTransaction("AccrualPolicy_insert");

                SqlParameter policyIdParm = command.AddParameterWithValue("@accrualPolicyId", policy.AccrualPolicyId, ParameterDirection.Output);
                command.AddParameterWithValue("@englishDescription", policy.EnglishDescription);
                command.AddParameterWithValue("@frenchDescription", policy.FrenchDescription);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", policy.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                policy.AccrualPolicyId = Convert.ToInt64(policyIdParm.Value);
                policy.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return policy;
            }
        }
        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualPolicy policy)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("AccrualPolicy_update", user.DatabaseName))
                {
                    command.BeginTransaction("AccrualPolicy_update");

                    command.AddParameterWithValue("@accrualPolicyId", policy.AccrualPolicyId);
                    command.AddParameterWithValue("@englishDescription", policy.EnglishDescription);
                    command.AddParameterWithValue("@frenchDescription", policy.FrenchDescription);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", policy.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    policy.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualPolicy policy)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("AccrualPolicy_delete", user.DatabaseName))
                {
                    command.BeginTransaction("AccrualPolicy_delete");

                    command.AddParameterWithValue("@accrualPolicyId", policy.AccrualPolicyId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected AccrualPolicyCollection MapToAccrualPolicyCollection(IDataReader reader)
        {
            AccrualPolicyCollection collection = new AccrualPolicyCollection();

            while (reader.Read())
                collection.Add(MapToAccrualPolicy(reader));

            return collection;
        }
        protected AccrualPolicy MapToAccrualPolicy(IDataReader reader)
        {
            AccrualPolicy item = new AccrualPolicy();
            base.MapToBase(item, reader);

            item.AccrualPolicyId = (long)CleanDataValue(reader[ColumnNames.AccrualPolicyId]);
            item.EnglishDescription = (String)CleanDataValue(reader[ColumnNames.EnglishDescription]);
            item.FrenchDescription = (String)CleanDataValue(reader[ColumnNames.FrenchDescription]);

            return item;
        }
        #endregion
    }
}