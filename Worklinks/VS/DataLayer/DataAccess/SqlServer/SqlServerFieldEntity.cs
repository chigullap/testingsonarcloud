﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerFieldEntity : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ControlFieldValueId = "control_field_value_id";
            public static String ControlFieldId = "control_field_id";
            public static String FieldIdentifier = "field_identifier";
            public static String CodeLanguageCd = "code_language_cd";
            public static String SecurityRoleId = "security_role_id";
            public static String AttributeIdentifier = "attribute_identifier";
            public static String FieldValue = "value";
        }
        #endregion

        #region main
        internal FieldEntityCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, decimal formId, String attributeIdentifier, int roleId)
        {
            DataBaseCommand command = GetStoredProcCommand("FormField_select", user.DatabaseName);
            command.AddParameterWithValue("@formId", formId);
            command.AddParameterWithValue("@languageCode", user.LanguageCode);
            command.AddParameterWithValue("@attributeIdentifier", attributeIdentifier);
            command.AddParameterWithValue("@roleId", roleId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToFieldEntityCollection(reader);
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, FieldEntity entity)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("ControlFieldValue_update", user.DatabaseName))
                {
                    command.BeginTransaction("ControlFieldValue_update");

                    command.AddParameterWithValue("@controlFieldValueId", entity.ControlFieldValueId);
                    command.AddParameterWithValue("@securityRoleId", entity.SecurityRoleId);
                    command.AddParameterWithValue("@fieldValue", entity.FieldValue.ToString());

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", entity.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    entity.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public FieldEntity Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, FieldEntity entity)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ControlFieldValue_insert", user.DatabaseName))
            {
                command.BeginTransaction("ControlFieldValue_insert");

                SqlParameter controlFieldValueIdParm = command.AddParameterWithValue("@controlFieldValueId", entity.ControlFieldValueId, ParameterDirection.Output);
                command.AddParameterWithValue("@controlFieldId", entity.ControlFieldId);
                command.AddParameterWithValue("@codeLanguageCd", entity.CodeLanguageCd);
                command.AddParameterWithValue("@securityRoleId", entity.SecurityRoleId);
                command.AddParameterWithValue("@attributeIdentifier", entity.AttributeIdentifier);
                command.AddParameterWithValue("@value", entity.FieldValue.ToString());

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", entity.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                entity.ControlFieldValueId = Convert.ToInt64(controlFieldValueIdParm.Value);
                entity.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();
                return entity;
            }
        }
        #endregion

        #region data mapping
        protected FieldEntityCollection MapToFieldEntityCollection(IDataReader reader)
        {
            FieldEntityCollection collection = new FieldEntityCollection();
            while (reader.Read())
            {
                collection.Add(MapToFieldEntity(reader));
            }

            return collection;
        }
        protected FieldEntity MapToFieldEntity(IDataReader reader)
        {
            FieldEntity field = new FieldEntity();
            base.MapToBase(field, reader);

            field.ControlFieldValueId = (long)CleanDataValue(reader[ColumnNames.ControlFieldValueId]);
            field.ControlFieldId = (long)CleanDataValue(reader[ColumnNames.ControlFieldId]);
            field.FieldIdentifier = (String)CleanDataValue(reader[ColumnNames.FieldIdentifier]);
            field.CodeLanguageCd = (String)CleanDataValue(reader[ColumnNames.CodeLanguageCd]);
            field.SecurityRoleId = (long)CleanDataValue(reader[ColumnNames.SecurityRoleId]);
            field.AttributeIdentifier = (String)CleanDataValue(reader[ColumnNames.AttributeIdentifier]);
            field.FieldValue = (String)(CleanDataValue(reader[ColumnNames.FieldValue]));

            return field;
        }
        #endregion
    }
}