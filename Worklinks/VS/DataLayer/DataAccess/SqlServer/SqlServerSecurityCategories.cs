﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerSecurityCategories : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String SecurityCategoryId = "security_category_id";
            public static String SelectedFlag = "selected_flag";
            public static String HierarchicalSortOrder = "hierarchical_sort_order";
            public static String ReferenceTableName = "reference_table_name";
            public static String ParentIdentifier = "parent_identifier";
            public static String SecurityCategoryDescription = "description";
        }
        #endregion

        #region main

        internal SecurityCategoryCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, bool isViewMode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SecurityCategoryWithDescription_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@isViewMode", isViewMode);
                command.AddParameterWithValue("@codeLanguageCd", user.LanguageCode);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToSecurityCategoryCollection(reader);
                }
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, SecurityCategory category)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SecurityCategory_update", user.DatabaseName))
                {
                    command.BeginTransaction("SecurityCategory_update");

                    command.AddParameterWithValue("@securityCategoryId", category.SecurityCategoryId);
                    command.AddParameterWithValue("@selectedFlag", category.SelectedFlag);
                    command.AddParameterWithValue("@hierarchicalSortOrder", category.HierarchicalSortOrder);
                    command.AddParameterWithValue("@referenceTableName", category.ReferenceTableName);
                    command.AddParameterWithValue("@parentIdentifier", category.ParentIdentifier);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", category.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                    category.RowVersion = (byte[])rowVersionParm.Value;
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void RebuildSecurity(WLP.BusinessLayer.BusinessObjects.DatabaseUser user)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Security_rebuild", user.DatabaseName))
            {
                command.BeginTransaction("Security_rebuild");
                command.AddParameterWithValue("@updateUser", user.UserName);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        #endregion

        #region data mapping
        protected SecurityCategoryCollection MapToSecurityCategoryCollection(IDataReader reader)
        {
            SecurityCategoryCollection collection = new SecurityCategoryCollection();

            while (reader.Read())
            {
                collection.Add(MapToSecurityCategory(reader));
            }

            return collection;
        }
        protected SecurityCategory MapToSecurityCategory(IDataReader reader)
        {
            SecurityCategory item = new SecurityCategory();
            base.MapToBase(item, reader);

            item.SecurityCategoryId = (long)CleanDataValue(reader[ColumnNames.SecurityCategoryId]);
            item.SelectedFlag = (bool)CleanDataValue(reader[ColumnNames.SelectedFlag]);
            item.HierarchicalSortOrder = (int)CleanDataValue(reader[ColumnNames.HierarchicalSortOrder]);
            item.ReferenceTableName = (String)CleanDataValue(reader[ColumnNames.ReferenceTableName]);
            item.ParentIdentifier = (String)CleanDataValue(reader[ColumnNames.ParentIdentifier]);
            item.SecurityCategoryDescription = (String)CleanDataValue(reader[ColumnNames.SecurityCategoryDescription]);

            return item;
        }
        #endregion
    }
}