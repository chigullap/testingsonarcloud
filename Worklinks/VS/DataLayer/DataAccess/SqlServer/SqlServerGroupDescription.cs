﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerGroupDescription : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String GroupId = "group_id";
            public static String EnglishDesc = "english_description";
            public static String FrenchDescription = "french_description";
            public static String DatabaseSecurityRoleId = "database_security_role_id";
            public static String GuiSecurityRoleId = "gui_security_role_id";
            public static String SecurityUserSecurityRoleId = "security_role_id";
            public static String WorklinksAdministrationFlag = "worklinks_administration_flag";
        }
        #endregion

        #region select
        internal GroupDescriptionCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String criteria, long? groupId, bool? worklinksAdministrationFlag)
        {
            DataBaseCommand command = GetStoredProcCommand("GroupDescription_select", user.DatabaseName);

            command.AddParameterWithValue("@groupDescription", criteria = criteria == "" ? null : criteria); //if criteria is "", then make it null for the proc, otherwise leave it alone
            command.AddParameterWithValue("@groupId", groupId);
            command.AddParameterWithValue("@worklinksAdministrationFlag", worklinksAdministrationFlag);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToGroupDescriptionCollection(reader);
            }
        }

        internal GroupDescriptionCollection SelectUserGroups(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long securityUserId, bool isViewMode, bool? worklinksAdministrationFlag)
        {
            DataBaseCommand command = GetStoredProcCommand("UserGroupDescription_select", user.DatabaseName);

            command.AddParameterWithValue("@securityUserId", securityUserId);
            command.AddParameterWithValue("@isViewMode", isViewMode);
            command.AddParameterWithValue("@worklinksAdministrationFlag", worklinksAdministrationFlag);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToGroupDescriptionCollection(reader);
            }
        }
        #endregion

        #region update
        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, GroupDescription grpDesc)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("GroupDescription_update", user.DatabaseName))
                {
                    command.BeginTransaction("GroupDescription_update");

                    command.AddParameterWithValue("@groupId", grpDesc.GroupId);
                    command.AddParameterWithValue("@englishDescription", grpDesc.EnglishDesc);
                    command.AddParameterWithValue("@frenchDescription", grpDesc.FrenchDesc);
                    command.AddParameterWithValue("@databaseSecurityRoleId", grpDesc.DatabaseSecurityRoleId);
                    command.AddParameterWithValue("@guiSecurityRoleId", grpDesc.GuiSecurityRoleId);
                    command.AddParameterWithValue("@worklinksAdministrationFlag", grpDesc.WorklinksAdministrationFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", grpDesc.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    grpDesc.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region insert
        public GroupDescription InsertGroupDescription(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, GroupDescription group)
        {
            using (DataBaseCommand command = GetStoredProcCommand("GroupDescription_insert", user.DatabaseName))
            {
                command.BeginTransaction("GroupDescription_insert");

                SqlParameter GroupIdParm = command.AddParameterWithValue("@groupId", group.GroupId, ParameterDirection.Output);

                command.AddParameterWithValue("@englishDescription", group.EnglishDesc);
                command.AddParameterWithValue("@frenchDescription", group.FrenchDesc);
                command.AddParameterWithValue("@databaseSecurityRoleId", group.DatabaseSecurityRoleId);
                command.AddParameterWithValue("@guiSecurityRoleId", group.GuiSecurityRoleId);
                command.AddParameterWithValue("@worklinksAdministrationFlag", group.WorklinksAdministrationFlag);

                //NOTE:  view will automatically insert the "CodeSecurityRoleTypeCd" value of "GRP" (for "Group").

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", group.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                //retrieve PK
                group.GroupId = Convert.ToInt64(GroupIdParm.Value);
                group.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return group;
            }
        }

        public GroupDescription InsertUserGroup(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, GroupDescription grpDesc, long securityUserId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("UserGroupDescription_insert", user.DatabaseName))
            {
                command.BeginTransaction("UserGroupDescription_insert");

                grpDesc.SecurityUserSecurityRoleId = -1; //this will be null, so set it to -1

                SqlParameter GroupIdParm = command.AddParameterWithValue("@securityUserRoleId", grpDesc.SecurityUserSecurityRoleId, ParameterDirection.Output);
                command.AddParameterWithValue("@userIdentifier", securityUserId);
                command.AddParameterWithValue("@securityRoleId", grpDesc.GroupId);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", grpDesc.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                //retrieve PK
                grpDesc.SecurityUserSecurityRoleId = Convert.ToInt64(GroupIdParm.Value);
                grpDesc.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }

            return grpDesc;
        }

        public void UpdateSecurityClientUserRoles(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, string adminDatabaseName, string ngSecurityClientId, string userName, long securityUserId, string roles)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SecurityClientUser_updateRoles", adminDatabaseName))
            {
                command.BeginTransaction("SecurityClientUser_updateRoles");

                command.AddParameterWithValue("@ngSecurityClientId", ngSecurityClientId);
                command.AddParameterWithValue("@userIdentifier", securityUserId);
                command.AddParameterWithValue("@userName", userName);
                command.AddParameterWithValue("@roles", roles);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        #endregion

        #region delete
        public void DeleteUserGroup(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, GroupDescription grpDesc, long securityUserId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("UserGroupDescription_delete", user.DatabaseName))
                {
                    command.BeginTransaction("UserGroupDescription_delete");

                    command.AddParameterWithValue("@userIdentifier", securityUserId);
                    command.AddParameterWithValue("@securityRoleId", grpDesc.GroupId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected GroupDescriptionCollection MapToGroupDescriptionCollection(IDataReader reader)
        {
            GroupDescriptionCollection collection = new GroupDescriptionCollection();
            while (reader.Read())
            {
                collection.Add(MapToGroupDescription(reader));
            }

            return collection;
        }

        protected GroupDescription MapToGroupDescription(IDataReader reader)
        {
            GroupDescription desc = new GroupDescription();
            base.MapToBase(desc, reader);

            desc.GroupId = (long)CleanDataValue(reader[ColumnNames.GroupId]);
            desc.EnglishDesc = (String)CleanDataValue(reader[ColumnNames.EnglishDesc]);
            desc.FrenchDesc = (String)CleanDataValue(reader[ColumnNames.FrenchDescription]);
            desc.DatabaseSecurityRoleId = (long?)CleanDataValue(reader[ColumnNames.DatabaseSecurityRoleId]);
            desc.GuiSecurityRoleId = (long?)CleanDataValue(reader[ColumnNames.GuiSecurityRoleId]);
            desc.WorklinksAdministrationFlag = (bool)CleanDataValue(reader[ColumnNames.WorklinksAdministrationFlag]);

            try
            {
                //this column will exist when "SelectUserGroups" is called but not when "Select" is called.
                desc.SecurityUserSecurityRoleId = (long?)CleanDataValue(reader[ColumnNames.SecurityUserSecurityRoleId]);
                desc.IsGroupSelected = (desc.SecurityUserSecurityRoleId != null);
            }
            catch (System.IndexOutOfRangeException)
            {
                //do nothing, "Select" was executed and this column is not part of the query results.
            }

            return desc;
        }
        #endregion
    }
}