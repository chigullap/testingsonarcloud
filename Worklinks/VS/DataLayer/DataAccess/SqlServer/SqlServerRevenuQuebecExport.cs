﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerRevenuQuebecExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String RevenuQuebecExportId = "revenu_quebec_export_id";
            public static String Data = "data";
            public static String CodeRevenuQuebecRemittancePeriodCd = "code_revenu_quebec_remittance_period_cd";
            public static String RevenuQuebecFileTransmissionDate = "revenu_quebec_file_transmission_date";
        }
        #endregion

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, RevenuQuebecExport item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("RevenuQuebecExport_insert", user.DatabaseName))
            {
                command.BeginTransaction("RevenuQuebecExport_insert");

                SqlParameter rqExportIdParm = command.AddParameterWithValue("@revenuQuebecExportId", item.RevenuQuebecExportId, ParameterDirection.Output);
                command.AddParameterWithValue("@rbcSequenceNumber", item.RbcSequenceNumber);
                command.AddParameterWithValue("@data", item.Data);
                command.AddParameterWithValue("@codeRevenuQuebecRemittancePeriodCd", item.CodeRevenuQuebecRemittancePeriodCd);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.RevenuQuebecExportId = Convert.ToInt64(rqExportIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }

        public void UpdateRevenuQuebecExportFileTransmissionDate(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long rqExportId, DateTime exportDate)
        {
            using (DataBaseCommand command = GetStoredProcCommand("RevenuQuebecExport_UpdateRevenuQuebecFileTransmissionDate", user.DatabaseName))
            {
                command.BeginTransaction("RevenuQuebecExport_UpdateRevenuQuebecFileTransmissionDate");

                command.AddParameterWithValue("@rqExportId", rqExportId);
                command.AddParameterWithValue("@exportDate", exportDate);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        public RevenuQuebecExportCollection CheckForUnprocessedRevenuQuebecRemittanceOnDatabase(WLP.BusinessLayer.BusinessObjects.DatabaseUser user)
        {
            DataBaseCommand command = GetStoredProcCommand("RevenuQuebecExport_selectUnprocessed", user.DatabaseName);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToRevenuQuebecExportCollection(reader);
            }
        }

        public RevenuQuebecExportCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? rqExportId)
        {
            DataBaseCommand command = GetStoredProcCommand("RevenuQuebecExport_select", user.DatabaseName);
            command.AddParameterWithValue("@rqExportId", rqExportId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToRevenuQuebecExportCollection(reader);
            }
        }

        #region data mapping
        protected RevenuQuebecExportCollection MapToRevenuQuebecExportCollection(IDataReader reader)
        {
            RevenuQuebecExportCollection collection = new RevenuQuebecExportCollection();
            while (reader.Read())
            {
                collection.Add(MapToRevenuQuebecExport(reader));
            }

            return collection;
        }
        protected RevenuQuebecExport MapToRevenuQuebecExport(IDataReader reader)
        {
            RevenuQuebecExport item = new RevenuQuebecExport();
            base.MapToBase(item, reader);

            item.RevenuQuebecExportId = (long)CleanDataValue(reader[ColumnNames.RevenuQuebecExportId]);
            item.Data = (byte[])CleanDataValue(reader[ColumnNames.Data]);
            item.CodeRevenuQuebecRemittancePeriodCd = (String)CleanDataValue(reader[ColumnNames.CodeRevenuQuebecRemittancePeriodCd]);
            item.RevenuQuebecFileTransmissionDate = (DateTime?)CleanDataValue(reader[ColumnNames.RevenuQuebecFileTransmissionDate]);

            return item;
        }

        #endregion


    }
}
