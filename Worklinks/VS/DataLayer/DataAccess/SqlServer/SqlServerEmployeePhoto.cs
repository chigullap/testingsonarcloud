﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerEmployeePhoto : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeePhotoId = "employee_photo_id";
            public static String EmployeeId = "employee_id";
            public static String Data = "data";
        }
        #endregion

        #region main
        public EmployeePhotoCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeeId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeePhoto_select", user.DatabaseName);
            command.AddParameterWithValue("@employeeId", employeeId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToEmployeePhotoCollection(reader);
            }
        }

        public EmployeePhoto Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeePhoto photo)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePhoto_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeePhoto_insert");

                SqlParameter employeePhotoIdParm = command.AddParameterWithValue("@employeePhotoId", photo.EmployeePhotoId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", photo.EmployeeId);
                command.AddParameterWithValue("@data", photo.Data);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", photo.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                photo.EmployeePhotoId = Convert.ToInt64(employeePhotoIdParm.Value);
                photo.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return photo;
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeePhoto photo)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePhoto_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePhoto_update");

                    command.AddParameterWithValue("@employeePhotoId", photo.EmployeePhotoId);
                    command.AddParameterWithValue("@employeeId", photo.EmployeeId);

                    if (photo.Data == null)
                        command.AddParameterWithValue("@data", photo.Data, DbType.Byte);
                    else
                        command.AddParameterWithValue("@data", photo.Data);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", photo.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    photo.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeePhoto photo)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePhoto_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePhoto_delete");
                    command.AddParameterWithValue("@employeeId", photo.EmployeeId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeePhotoCollection MapToEmployeePhotoCollection(IDataReader reader)
        {
            EmployeePhotoCollection collection = new EmployeePhotoCollection();
            while (reader.Read())
            {
                collection.Add(MapToEmployeePhoto(reader));
            }

            return collection;
        }

        protected EmployeePhoto MapToEmployeePhoto(IDataReader reader)
        {
            EmployeePhoto item = new EmployeePhoto();
            base.MapToBase(item, reader);

            item.EmployeePhotoId = (long)CleanDataValue(reader[ColumnNames.EmployeePhotoId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.Data = (byte[])CleanDataValue(reader[ColumnNames.Data]);

            return item;
        }
        #endregion
    }
}