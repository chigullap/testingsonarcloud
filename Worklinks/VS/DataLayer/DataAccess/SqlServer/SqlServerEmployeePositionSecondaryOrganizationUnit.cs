﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeePositionSecondaryOrganizationUnit : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeePositionSecondaryOrganizationUnitId = "employee_position_secondary_organization_unit_id";
            public static String EmployeePositionSecondaryId = "employee_position_secondary_id";
            public static String OrganizationUnitLevelId = "organization_unit_level_id";
            public static String OrganizationUnitId = "organization_unit_id";
            public static String OrganizationUnitStartDate = "organization_unit_start_date";
            public static String UsedOnSecondaryEmployeePositionFlag = "used_on_secondary_employee_position_flag";
        }
        #endregion

        #region main
        public EmployeePositionSecondaryOrganizationUnitCollection Select(DatabaseUser user, long? employeePositionSecondaryId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeePositionSecondaryOrganizationUnit_select", user.DatabaseName);

            command.AddParameterWithValue("@employeePositionSecondaryId", employeePositionSecondaryId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCollection(reader);
        }

        public EmployeePositionSecondaryOrganizationUnitCollection SelectBatchEmployeePositionOrganizationUnit(DatabaseUser user, EmployeePositionSecondaryCollection secondaryPositions)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionOrganizationUnit_batchSelectByEmployeeSecondaryPositionId", user.DatabaseName))
            {
                SqlParameter parmSecondaryPositionids = new SqlParameter();
                parmSecondaryPositionids.ParameterName = "@parmSecondaryPositionIds";
                parmSecondaryPositionids.SqlDbType = SqlDbType.Structured;

                DataTable tableSecondaryPositionids = LoadEmployeePositionIdSecondaryDataTable(secondaryPositions);
                parmSecondaryPositionids.Value = tableSecondaryPositionids;
                command.AddParameter(parmSecondaryPositionids);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToBulk(reader);
            }
        }
        private DataTable LoadEmployeePositionIdSecondaryDataTable(EmployeePositionSecondaryCollection secondaryPositions)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_position_secondary_id", typeof(long));

            foreach (EmployeePositionSecondary secPosition in secondaryPositions)
            {
                table.Rows.Add(new Object[]
                {
                    secPosition.EmployeePositionSecondaryId
                });
            }

            return table;
        }

        public void Delete(DatabaseUser user, EmployeePositionSecondaryOrganizationUnit item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionSecondaryOrganizationUnit_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePositionSecondaryOrganizationUnit_delete");

                    command.AddParameterWithValue("@employeePositionSecondaryOrganizationUnitId", item.EmployeePositionSecondaryOrganizationUnitId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeePositionSecondaryOrganizationUnitCollection MapToCollection(IDataReader reader)
        {
            EmployeePositionSecondaryOrganizationUnitCollection collection = new EmployeePositionSecondaryOrganizationUnitCollection();

            while (reader.Read())
                collection.Add(MapToItem(reader));

            return collection;
        }
        protected EmployeePositionSecondaryOrganizationUnit MapToItem(IDataReader reader)
        {
            EmployeePositionSecondaryOrganizationUnit item = new EmployeePositionSecondaryOrganizationUnit();
            base.MapToBase(item, reader);

            item.EmployeePositionSecondaryOrganizationUnitId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionSecondaryOrganizationUnitId]);
            item.EmployeePositionSecondaryId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionSecondaryId]);
            item.OrganizationUnitLevelId = (long)CleanDataValue(reader[ColumnNames.OrganizationUnitLevelId]);
            item.OrganizationUnitId = (long?)CleanDataValue(reader[ColumnNames.OrganizationUnitId]);
            item.OrganizationUnitStartDate = (DateTime?)CleanDataValue(reader[ColumnNames.OrganizationUnitStartDate]);
            item.UsedOnSecondaryEmployeePositionFlag = (bool)CleanDataValue(reader[ColumnNames.UsedOnSecondaryEmployeePositionFlag]);

            return item;
        }

        protected EmployeePositionSecondaryOrganizationUnitCollection MapToBulk(IDataReader reader)
        {
            EmployeePositionSecondaryOrganizationUnitCollection collection = new EmployeePositionSecondaryOrganizationUnitCollection();

            while (reader.Read())
                collection.Add(MapToBulkItem(reader));

            return collection;
        }
        protected EmployeePositionSecondaryOrganizationUnitBulk MapToBulkItem(IDataReader reader)
        {
            EmployeePositionSecondaryOrganizationUnitBulk item = new EmployeePositionSecondaryOrganizationUnitBulk();
            base.MapToBase(item, reader);

            item.EmployeePositionSecondaryOrganizationUnitId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionSecondaryOrganizationUnitId]);
            item.EmployeePositionSecondaryId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionSecondaryId]);
            item.OrganizationUnitLevelId = (long)CleanDataValue(reader[ColumnNames.OrganizationUnitLevelId]);
            item.OrganizationUnitId = (long?)CleanDataValue(reader[ColumnNames.OrganizationUnitId]);
            item.OrganizationUnitStartDate = (DateTime?)CleanDataValue(reader[ColumnNames.OrganizationUnitStartDate]);
            item.UsedOnSecondaryEmployeePositionFlag = (bool)CleanDataValue(reader[ColumnNames.UsedOnSecondaryEmployeePositionFlag]);

            return item;
        }

        #endregion
    }
}