﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerRevenueQuebecTransmitterRecord : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String RevenueQuebecTransmitterRecordId = "revenue_quebec_transmitter_record_id";
            public static String TransmitterNumber = "transmitter_number";
            public static String RevenueQuebecTransmitterTypeCode = "code_revenue_quebec_transmitter_type_cd";
            public static String TransmitterName = "transmitter_name";
            public static String TransmitterPersonId = "transmitter_person_id";
            public static String CertificationNumber = "certification_number";
            public static String SoftwareName = "software_name";
            public static String LanguageCode = "code_language_cd";
            public static String PartnerIdentifier = "partner_identifier";
            public static String ProductIdentifier = "product_identifier";
            public static String TestCaseNumber = "test_case_number";
        }
        #endregion

        #region main
        internal RevenueQuebecTransmitterRecordCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long revenueQuebecTransmitterRecordId)
        {
            DataBaseCommand command = GetStoredProcCommand("RevenueQuebecTransmitterRecord_select", user.DatabaseName);
            command.AddParameterWithValue("@revenueQuebecTransmitterRecordId", revenueQuebecTransmitterRecordId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToRevenueQuebecTransmitterRecordCollection(reader);
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, RevenueQuebecTransmitterRecord item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("RevenueQuebecTransmitterRecord_update",user.DatabaseName))
            {
                command.BeginTransaction("RevenueQuebecTransmitterRecord_update");

                command.AddParameterWithValue("@revenueQuebecTransmitterRecordId", item.RevenueQuebecTransmitterRecordId);
                command.AddParameterWithValue("@revenueQuebecTransmitterTypeCode", item.RevenueQuebecTransmitterTypeCode);
                command.AddParameterWithValue("@transmitterNumber", item.TransmitterNumber);
                command.AddParameterWithValue("@transmitterName", item.TransmitterName);
                command.AddParameterWithValue("@transmitterPersonId", item.TransmitterPersonId);
                command.AddParameterWithValue("@certificationNumber", item.CertificationNumber);
                command.AddParameterWithValue("@softwareName", item.SoftwareName);
                command.AddParameterWithValue("@languageCode", item.LanguageCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                item.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();
            }
        }

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, RevenueQuebecTransmitterRecord item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("RevenueQuebecTransmitterRecord_insert", user.DatabaseName))
            {
                command.BeginTransaction("RevenueQuebecTransmitterRecord_insert");

                SqlParameter idParm = command.AddParameterWithValue("@revenueQuebecTransmitterRecordId", item.RevenueQuebecTransmitterRecordId, ParameterDirection.Output);
                command.AddParameterWithValue("@revenueQuebecTransmitterTypeCode", item.RevenueQuebecTransmitterTypeCode);
                command.AddParameterWithValue("@transmitterNumber", item.TransmitterNumber);
                command.AddParameterWithValue("@transmitterName", item.TransmitterName);
                command.AddParameterWithValue("@transmitterPersonId", item.TransmitterPersonId);
                command.AddParameterWithValue("@certificationNumber", item.CertificationNumber);
                command.AddParameterWithValue("@languageCode", item.LanguageCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.RevenueQuebecTransmitterRecordId = Convert.ToInt64(idParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }

        //public void Delete(YearEnd yearEnd)
        //{
        //    using (DataBaseCommand command = GetStoredProcCommand("YearEnd_delete"))
        //    {
        //        command.BeginTransaction("YearEnd_delete");
        //        command.AddParameterWithValue("@yearEndId", yearEnd.YearEndId);

        //        command.ExecuteNonQuery();
        //        command.CommitTransaction();
        //    }
        //}
        #endregion

        #region data mapping
        protected RevenueQuebecTransmitterRecordCollection MapToRevenueQuebecTransmitterRecordCollection(IDataReader reader)
        {
            RevenueQuebecTransmitterRecordCollection collection = new RevenueQuebecTransmitterRecordCollection();
            while (reader.Read())
            {
                collection.Add(MapToRevenueQuebecTransmitterRecord(reader));
            }

            return collection;
        }
        protected RevenueQuebecTransmitterRecord MapToRevenueQuebecTransmitterRecord(IDataReader reader)
        {
            RevenueQuebecTransmitterRecord item = new RevenueQuebecTransmitterRecord();
            base.MapToBase(item, reader);

            item.RevenueQuebecTransmitterRecordId = (long)CleanDataValue(reader[ColumnNames.RevenueQuebecTransmitterRecordId]);
            item.TransmitterNumber = (String)CleanDataValue(reader[ColumnNames.TransmitterNumber]);
            item.RevenueQuebecTransmitterTypeCode = (String)CleanDataValue(reader[ColumnNames.RevenueQuebecTransmitterTypeCode]);
            item.TransmitterName = (String)CleanDataValue(reader[ColumnNames.TransmitterName]);
            item.TransmitterPersonId = (long)CleanDataValue(reader[ColumnNames.TransmitterPersonId]);
            item.CertificationNumber = (String)CleanDataValue(reader[ColumnNames.CertificationNumber]);
            item.SoftwareName = (String)CleanDataValue(reader[ColumnNames.SoftwareName]);
            item.LanguageCode = (String)CleanDataValue(reader[ColumnNames.LanguageCode]);
            item.PartnerIdentifier = (String)CleanDataValue(reader[ColumnNames.PartnerIdentifier]);
            item.ProductIdentifier = (String)CleanDataValue(reader[ColumnNames.ProductIdentifier]);
            item.TestCaseNumber = (String)CleanDataValue(reader[ColumnNames.TestCaseNumber]);

            return item;
        }
        #endregion
    }
}
