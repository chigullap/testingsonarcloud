﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmail : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmailId = "email_id";
            public static String Name = "name";
            public static String Description = "description";
            public static String EmailTemplateTypeCode = "code_email_template_type_cd";
        }
        #endregion

        #region main
        internal EmailCollection Select(DatabaseUser user, long? emailId)
        {
            DataBaseCommand command = GetStoredProcCommand("Email_select", user.DatabaseName);

            command.AddParameterWithValue("@emailId", emailId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToEmailCollection(reader);
        }
        public Email Insert(DatabaseUser user, Email item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Email_insert", user.DatabaseName))
            {
                command.BeginTransaction("Email_insert");

                SqlParameter idParm = command.AddParameterWithValue("@emailId", item.EmailId, ParameterDirection.Output);
                command.AddParameterWithValue("@name", item.Name);
                command.AddParameterWithValue("@description", item.Description);
                command.AddParameterWithValue("@emailTemplateTypeCode", item.EmailTemplateTypeCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.EmailId = Convert.ToInt64(idParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        public void Update(DatabaseUser user, Email item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("Email_update", user.DatabaseName))
                {
                    command.BeginTransaction("Email_update");

                    command.AddParameterWithValue("@emailId", item.EmailId);
                    command.AddParameterWithValue("@name", item.Name);
                    command.AddParameterWithValue("@description", item.Description);
                    command.AddParameterWithValue("@emailTemplateTypeCode", item.EmailTemplateTypeCode);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, Email item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("Email_delete", user.DatabaseName))
                {
                    command.BeginTransaction("Email_delete");

                    command.AddParameterWithValue("@emailId", item.EmailId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmailCollection MapToEmailCollection(IDataReader reader)
        {
            EmailCollection collection = new EmailCollection();

            while (reader.Read())
                collection.Add(MapToEmail(reader));

            return collection;
        }
        protected Email MapToEmail(IDataReader reader)
        {
            Email item = new Email();
            base.MapToBase(item, reader);

            item.EmailId = (long)CleanDataValue(reader[ColumnNames.EmailId]);
            item.Name = (String)CleanDataValue(reader[ColumnNames.Name]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            item.EmailTemplateTypeCode = (String)CleanDataValue(reader[ColumnNames.EmailTemplateTypeCode]);

            return item;
        }
        #endregion
    }
}