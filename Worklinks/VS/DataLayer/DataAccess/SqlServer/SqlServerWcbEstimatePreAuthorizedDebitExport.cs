﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerWcbEstimatePreAuthorizedDebitExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
        }
        #endregion

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ChequeWcbPreAuthorizedDebitExport item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ChequeWcbPreAuthorizedDebitExport_insert", user.DatabaseName))
            {
                command.BeginTransaction("ChequeWcbPreAuthorizedDebitExport_insert");

                SqlParameter chequeWcbPreAuthorizedDebitExportIdParm = command.AddParameterWithValue("@chequeWcbPreAuthorizedDebitExportId", item.ChequeWcbPreAuthorizedDebitExportId, ParameterDirection.Output);
                command.AddParameterWithValue("@rbcSequenceNumber", item.RbcSequenceNumber);
                command.AddParameterWithValue("@data", item.Data);
                command.AddParameterWithValue("@codeWsibCd", item.CodeWsibCd);
                command.AddParameterWithValue("@remittanceDate", item.RemittanceDate);
                command.AddParameterWithValue("@codeWcbChequeRemittanceFrequencyCd", item.CodeWcbChequeRemittanceFrequencyCd);
                command.AddParameterWithValue("@totalAmount", item.TotalAmount);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.ChequeWcbPreAuthorizedDebitExportId = Convert.ToInt64(chequeWcbPreAuthorizedDebitExportIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }

        public void UpdateChequeWcbEstimatePreAuthorizedDebitExportFileTransmissionDate(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long chequeWcbPreAuthorizedDebitExportId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ChequeWcbPreAuthorizedDebitExport_UpdateFileTransmissionDate", user.DatabaseName))
            {
                command.BeginTransaction("ChequeWcbPreAuthorizedDebitExport_UpdateFileTransmissionDate");

                command.AddParameterWithValue("@chequeWcbPreAuthorizedDebitExportId", chequeWcbPreAuthorizedDebitExportId);
                command.AddParameterWithValue("@exportDate", DateTime.Now);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
    }
}
