﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeCustomFieldConfig : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeCustomFieldConfigId = "employee_custom_field_config_id";
            public static String EmployeeCustomFieldNameCode = "code_employee_custom_field_name_cd";
            public static String DataTypeCode = "code_datatype_cd";
            public static String Length = "length";
            public static String Precision = "precision";
            public static String SortOrder = "sort_order";
            public static String ActiveFlag = "active_flag";
            public static String EnglishDescription = "english_description";
            public static String FrenchDescription = "french_description";
        }
        #endregion

        #region main
        internal EmployeeCustomFieldConfigCollection Select(DatabaseUser user, long? employeeCustomFieldConfigId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeeCustomFieldConfig_select", user.DatabaseName);

            command.AddParameterWithValue("@employeeCustomFieldConfigId", employeeCustomFieldConfigId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToEmployeeCustomFieldConfigCollection(reader);
        }
        public EmployeeCustomFieldConfig Insert(DatabaseUser user, EmployeeCustomFieldConfig item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeCustomFieldConfig_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeCustomFieldConfig_insert");

                SqlParameter idParm = command.AddParameterWithValue("@employeeCustomFieldConfigId", item.EmployeeCustomFieldConfigId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeCustomFieldNameCode", item.EmployeeCustomFieldNameCode);
                command.AddParameterWithValue("@dataTypeCode", item.DataTypeCode);
                command.AddParameterWithValue("@length", item.Length);
                command.AddParameterWithValue("@precision", item.Precision);
                command.AddParameterWithValue("@sortOrder", item.SortOrder);
                command.AddParameterWithValue("@activeFlag", item.ActiveFlag);
                command.AddParameterWithValue("@englishDescription", item.EnglishDescription);
                command.AddParameterWithValue("@frenchDescription", item.FrenchDescription);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.EmployeeCustomFieldConfigId = Convert.ToInt64(idParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        public void Update(DatabaseUser user, EmployeeCustomFieldConfig item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeCustomFieldConfig_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeCustomFieldConfig_update");

                    command.AddParameterWithValue("@employeeCustomFieldConfigId", item.EmployeeCustomFieldConfigId);
                    command.AddParameterWithValue("@employeeCustomFieldNameCode", item.EmployeeCustomFieldNameCode);
                    command.AddParameterWithValue("@dataTypeCode", item.DataTypeCode);
                    command.AddParameterWithValue("@length", item.Length);
                    command.AddParameterWithValue("@precision", item.Precision);
                    command.AddParameterWithValue("@sortOrder", item.SortOrder);
                    command.AddParameterWithValue("@activeFlag", item.ActiveFlag);
                    command.AddParameterWithValue("@englishDescription", item.EnglishDescription);
                    command.AddParameterWithValue("@frenchDescription", item.FrenchDescription);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, EmployeeCustomFieldConfig item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeCustomFieldConfig_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeCustomFieldConfig_delete");

                    command.AddParameterWithValue("@employeeCustomFieldConfigId", item.EmployeeCustomFieldConfigId);
                    command.AddParameterWithValue("@employeeCustomFieldNameCode", item.EmployeeCustomFieldNameCode);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeCustomFieldConfigCollection MapToEmployeeCustomFieldConfigCollection(IDataReader reader)
        {
            EmployeeCustomFieldConfigCollection collection = new EmployeeCustomFieldConfigCollection();

            while (reader.Read())
                collection.Add(MapToEmployeeCustomFieldConfig(reader));

            return collection;
        }
        protected EmployeeCustomFieldConfig MapToEmployeeCustomFieldConfig(IDataReader reader)
        {
            EmployeeCustomFieldConfig item = new EmployeeCustomFieldConfig();
            base.MapToBase(item, reader);

            item.EmployeeCustomFieldConfigId = (long)CleanDataValue(reader[ColumnNames.EmployeeCustomFieldConfigId]);
            item.EmployeeCustomFieldNameCode = (String)CleanDataValue(reader[ColumnNames.EmployeeCustomFieldNameCode]);
            item.DataTypeCode = (String)CleanDataValue(reader[ColumnNames.DataTypeCode]);
            item.Length = (int)CleanDataValue(reader[ColumnNames.Length]);
            item.Precision = (int)CleanDataValue(reader[ColumnNames.Precision]);
            item.SortOrder = (int)CleanDataValue(reader[ColumnNames.SortOrder]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.EnglishDescription = (String)CleanDataValue(reader[ColumnNames.EnglishDescription]);
            item.FrenchDescription = (String)CleanDataValue(reader[ColumnNames.FrenchDescription]);

            return item;
        }
        #endregion
    }
}