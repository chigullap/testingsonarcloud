﻿using System;
using System.Data;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerFieldEditor : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String FormDescId = "form_description_id";
            public static String FormId = "form_id";
            public static String FormDescriptionField = "description";
        }
        #endregion

        #region main
        internal FormDescriptionCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String criteria)
        {
            DataBaseCommand command = GetStoredProcCommand("FormDescription_select", user.DatabaseName);

            command.AddParameterWithValue("@codeLanguage", user.LanguageCode);
            command.AddParameterWithValue("@formDescription", criteria = criteria == "" ? null : criteria); //if criteria is "", then make it null for the proc, otherwise leave it alone

            using (IDataReader reader = command.ExecuteReader())
                return MapToFormDescriptionCollection(reader);
        }
        #endregion

        #region data mapping
        protected FormDescriptionCollection MapToFormDescriptionCollection(IDataReader reader)
        {
            FormDescriptionCollection collection = new FormDescriptionCollection();

            while (reader.Read())
                collection.Add(MapToFormDescription(reader));

            return collection;
        }
        protected FormDescription MapToFormDescription(IDataReader reader)
        {
            FormDescription form = new FormDescription();
            base.MapToBase(form, reader);

            form.FormDescriptionId = (long)CleanDataValue(reader[ColumnNames.FormDescId]);
            form.FormId = (long)CleanDataValue(reader[ColumnNames.FormId]);
            form.FormDescriptionField = (String)CleanDataValue(reader[ColumnNames.FormDescriptionField]);

            return form;
        }
        #endregion
    }
}