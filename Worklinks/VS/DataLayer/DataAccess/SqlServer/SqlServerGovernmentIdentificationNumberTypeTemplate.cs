﻿using System;
using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerGovernmentIdentificationNumberTypeTemplate : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String GovernmentIdentificationNumberTypeTemplateId = "government_identification_number_type_template_id";
            public static String GovernmentIdentificationNumberTypeCode = "code_government_identification_number_type_cd";
            public static String LanguageCode = "code_language_cd";
            public static String EntryFormat1 = "entry_format_1";
            public static String Label1 = "label_1";
            public static String Mandatory1Flag = "mandatory_1_flag";
            public static String Visible1Flag = "visible_1_flag";
            public static String RegularExpression1 = "regular_expression_1";
            public static String EntryFormat2 = "entry_format_2";
            public static String Label2 = "label_2";
            public static String Mandatory2Flag = "mandatory_2_flag";
            public static String Visible2Flag = "visible_2_flag";
            public static String RegularExpression2 = "regular_expression_2";
            public static String EntryFormat3 = "entry_format_3";
            public static String Label3 = "label_3";
            public static String Mandatory3Flag = "mandatory_3_flag";
            public static String Visible3Flag = "visible_3_flag";
            public static String RegularExpression3 = "regular_expression_3";
        }
        #endregion

        #region select
        internal GovernmentIdentificationNumberTypeTemplateCollection Select(DatabaseUser user, String numberTypeCode)
        {
            DataBaseCommand command = GetStoredProcCommand("GovernmentIdentificationNumberTypeTemplate_select", user.DatabaseName);

            command.AddParameterWithValue("@governmentIdentificationNumberTypeCode", numberTypeCode);
            command.AddParameterWithValue("@languageCode", user.LanguageCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToGovernmentIdentificationNumberTypeTemplateCollection(reader);
        }
        #endregion

        #region data mapping
        protected GovernmentIdentificationNumberTypeTemplateCollection MapToGovernmentIdentificationNumberTypeTemplateCollection(IDataReader reader)
        {
            GovernmentIdentificationNumberTypeTemplateCollection collection = new GovernmentIdentificationNumberTypeTemplateCollection();

            while (reader.Read())
                collection.Add(MapToGovernmentIdentificationNumberTypeTemplate(reader));

            return collection;
        }
        protected GovernmentIdentificationNumberTypeTemplate MapToGovernmentIdentificationNumberTypeTemplate(IDataReader reader)
        {
            GovernmentIdentificationNumberTypeTemplate item = new GovernmentIdentificationNumberTypeTemplate();
            base.MapToBase(item, reader);

            item.GovernmentIdentificationNumberTypeTemplateId = (long)CleanDataValue(reader[ColumnNames.GovernmentIdentificationNumberTypeTemplateId]);
            item.GovernmentIdentificationNumberTypeCode = (String)CleanDataValue(reader[ColumnNames.GovernmentIdentificationNumberTypeCode]);
            item.LanguageCode = (String)CleanDataValue(reader[ColumnNames.LanguageCode]);
            item.EntryFormat1 = (String)CleanDataValue(reader[ColumnNames.EntryFormat1]);
            item.Label1 = (String)CleanDataValue(reader[ColumnNames.Label1]);
            item.Mandatory1Flag = (bool)CleanDataValue(reader[ColumnNames.Mandatory1Flag]);
            item.Visible1Flag = (bool)CleanDataValue(reader[ColumnNames.Visible1Flag]);
            item.RegularExpression1 = (String)CleanDataValue(reader[ColumnNames.RegularExpression1]);
            item.EntryFormat2 = (String)CleanDataValue(reader[ColumnNames.EntryFormat2]);
            item.Label2 = (String)CleanDataValue(reader[ColumnNames.Label2]);
            item.Mandatory2Flag = (bool)CleanDataValue(reader[ColumnNames.Mandatory2Flag]);
            item.Visible2Flag = (bool)CleanDataValue(reader[ColumnNames.Visible2Flag]);
            item.RegularExpression2 = (String)CleanDataValue(reader[ColumnNames.RegularExpression2]);
            item.EntryFormat3 = (String)CleanDataValue(reader[ColumnNames.EntryFormat3]);
            item.Label3 = (String)CleanDataValue(reader[ColumnNames.Label3]);
            item.Mandatory3Flag = (bool)CleanDataValue(reader[ColumnNames.Mandatory3Flag]);
            item.Visible3Flag = (bool)CleanDataValue(reader[ColumnNames.Visible3Flag]);
            item.RegularExpression3 = (String)CleanDataValue(reader[ColumnNames.RegularExpression3]);

            return item;
        }
        #endregion
    }
}