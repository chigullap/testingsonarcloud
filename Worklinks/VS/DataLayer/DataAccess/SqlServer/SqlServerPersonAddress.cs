﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerPersonAddress : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PersonAddressId = "person_address_id";
            public static String PersonId = "person_id";
            public static String AddressId = "address_id";
            public static String PersonAddressTypeCode = "code_person_address_type_cd";
            public static String PrimaryFlag = "primary_flag";
            public static String AddressLine1 = "address_line_1";
            public static String AddressLine2 = "address_line_2";
            public static String City = "city";
            public static String PostalZipCode = "postal_zip_code";
            public static String ProvinceStateCode = "code_province_state_cd";
            public static String CountryCode = "code_country_cd";
            public static String ThreeCharacterCountryCode = "three_character_country_code";
            public static String SharedCount = "shared_count";
        }
        #endregion

        #region select
        internal PersonAddressCollection Select(DatabaseUser user, long? personId, long? personAddressId, long? addressId, bool removeFrenchAccents = false)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PersonAddress_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@personId", personId);
                command.AddParameterWithValue("@personAddressId", personAddressId);
                command.AddParameterWithValue("@addressId", addressId);
                command.AddParameterWithValue("@removeFrenchAccents", removeFrenchAccents);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToPersonAddressCollection(reader);
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, PersonAddress address)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PersonAddress_update", user.DatabaseName))
                {
                    command.BeginTransaction("PersonAddress_update");

                    command.AddParameterWithValue("@personAddressId", address.PersonAddressId);
                    command.AddParameterWithValue("@personId", address.PersonId);
                    command.AddParameterWithValue("@personAddressTypeCode", address.PersonAddressTypeCode);
                    command.AddParameterWithValue("@primaryFlag", address.PrimaryFlag);
                    command.AddParameterWithValue("@addressId", address.AddressId);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", address.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                    address.RowVersion = (byte[])rowVersionParm.Value;
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region insert
        public void Insert(DatabaseUser user, PersonAddress address)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PersonAddress_insert", user.DatabaseName))
            {
                command.BeginTransaction("PersonAddress_insert");

                SqlParameter personAddressIdParm = command.AddParameterWithValue("@personAddressId", address.PersonAddressId, ParameterDirection.Output);
                command.AddParameterWithValue("@personId", address.PersonId);
                command.AddParameterWithValue("@personAddressTypeCode", address.PersonAddressTypeCode);
                command.AddParameterWithValue("@primaryFlag", address.PrimaryFlag);
                command.AddParameterWithValue("@addressId", address.AddressId);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", address.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                address.PersonAddressId = Convert.ToInt64(personAddressIdParm.Value);
                address.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, PersonAddress address)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PersonAddress_delete", user.DatabaseName))
                {
                    command.BeginTransaction("PersonAddress_delete");

                    command.AddParameterWithValue("@PersonAddressId", address.PersonAddressId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void DeleteByPersonId(DatabaseUser user, long personId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PersonAddress_deleteByPersonId", user.DatabaseName))
                {
                    command.BeginTransaction("PersonAddress_deleteByPersonId");

                    command.AddParameterWithValue("@PersonId", personId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region import
        public void ImportAddressUsingService(String database, String username, List<AddressImport> address)
        {
            DataBaseCommand command = GetStoredProcCommand("Address_batchInsert", database);

            using (command)
            {
                command.BeginTransaction("Address_batchInsert");

                //table parameter
                SqlParameter parmAddress = new SqlParameter();
                parmAddress.ParameterName = "@parmAddress";
                parmAddress.SqlDbType = SqlDbType.Structured;

                DataTable tableAddress = LoadAddressDataTable(address);
                parmAddress.Value = tableAddress;
                command.AddParameter(parmAddress);

                command.AddParameterWithValue("@updateUser", username);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        private DataTable LoadAddressDataTable(List<AddressImport> address)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_identifier", typeof(String));
            table.Columns.Add("address_line_1", typeof(String));
            table.Columns.Add("address_line_2", typeof(String));
            table.Columns.Add("city", typeof(String));
            table.Columns.Add("postal_zip_code", typeof(String));
            table.Columns.Add("code_province_state_cd", typeof(String));
            table.Columns.Add("code_country_cd", typeof(String));
            table.Columns.Add("code_person_address_type_cd", typeof(String));
            table.Columns.Add("delete_flag", typeof(bool));

            //loop thru and add values
            foreach (AddressImport item in address)
            {
                table.Rows.Add(new Object[]
                {
                    item.EmployeeIdentifier,
                    item.AddressLine1,
                    item.AddressLine2,
                    item.City,
                    item.PostalZipCode,
                    item.CodeProvinceStateCd,
                    item.CodeCountryCd,
                    null,
                    null
                });
            }

            return table;
        }

        public void ImportAddress(DatabaseUser user, WorkLinksPersonAddressCollection items)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Address_import", user.DatabaseName))
            {
                command.BeginTransaction("Address_import");

                //table parameter
                SqlParameter parmAddress = new SqlParameter();
                parmAddress.ParameterName = "@parmAddress";
                parmAddress.SqlDbType = SqlDbType.Structured;

                DataTable tableAddress = LoadAddressDataTable(items);
                parmAddress.Value = tableAddress;
                command.AddParameter(parmAddress);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        private DataTable LoadAddressDataTable(WorkLinksPersonAddressCollection items)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_identifier", typeof(String));
            table.Columns.Add("address_line_1", typeof(String));
            table.Columns.Add("address_line_2", typeof(String));
            table.Columns.Add("city", typeof(String));
            table.Columns.Add("postal_zip_code", typeof(String));
            table.Columns.Add("code_province_state_cd", typeof(String));
            table.Columns.Add("code_country_cd", typeof(String));
            table.Columns.Add("code_person_address_type_cd", typeof(String));
            table.Columns.Add("delete_flag", typeof(bool));

            foreach (WorkLinksPersonAddress item in items)
            {
                table.Rows.Add(new Object[]
                {
                    item.ImportExternalIdentifier,
                    item.AddressLine1,
                    item.AddressLine2,
                    item.City,
                    item.PostalZipCode,
                    item.ProvinceStateCode,
                    item.CountryCode,
                    item.PersonAddressTypeCode,
                    item.DeleteFlag
                });
            }

            return table;
        }
        #endregion

        #region data mapping
        protected PersonAddressCollection MapToPersonAddressCollection(IDataReader reader)
        {
            PersonAddressCollection collection = new PersonAddressCollection();

            while (reader.Read())
                collection.Add(MapToPersonAddress(reader));

            return collection;
        }

        protected PersonAddress MapToPersonAddress(IDataReader reader)
        {
            PersonAddress address = new PersonAddress();
            base.MapToBase(address, reader, true);

            address.PersonAddressId = (long)CleanDataValue(reader[ColumnNames.PersonAddressId]);
            address.PersonId = (long)CleanDataValue(reader[ColumnNames.PersonId]);
            address.PersonAddressTypeCode = (String)CleanDataValue(reader[ColumnNames.PersonAddressTypeCode]);
            address.PrimaryFlag = (bool)CleanDataValue(reader[ColumnNames.PrimaryFlag]);
            address.AddressId = (long)CleanDataValue(reader[ColumnNames.AddressId]);
            address.AddressLine1 = (String)CleanDataValue(reader[ColumnNames.AddressLine1]);
            address.AddressLine2 = (String)CleanDataValue(reader[ColumnNames.AddressLine2]);
            address.City = (String)CleanDataValue(reader[ColumnNames.City]);
            address.PostalZipCode = (String)CleanDataValue(reader[ColumnNames.PostalZipCode]);
            address.ProvinceStateCode = (String)CleanDataValue(reader[ColumnNames.ProvinceStateCode]);
            address.CountryCode = (String)CleanDataValue(reader[ColumnNames.CountryCode]);
            address.ThreeCharacterCountryCode = (String)CleanDataValue(reader[ColumnNames.ThreeCharacterCountryCode]); 
            address.RowVersion = (byte[])CleanDataValue(reader[ColumnNames.RowVersion]);
            address.SharedCount = (int)CleanDataValue(reader[ColumnNames.SharedCount]);

            return address;
        }
        #endregion
    }
}