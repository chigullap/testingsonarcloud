﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerSalaryPlanGradeRuleCode : SqlServerBase
    {
        internal SalaryPlanGradeRuleCodeCollection Select(DatabaseUser user)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SalaryPlanGradeRulePaycode_select", user.DatabaseName))
            {
                command.AddParameter(new SqlParameter("@salaryPlanGradeRuleId", SqlDbType.BigInt) {Value = null});

                SalaryPlanGradeRuleCodeCollection codeTableRows;

                using (IDataReader reader = command.ExecuteReader())
                    codeTableRows = MapToCollection(reader);

                return codeTableRows;
            }
        }

        internal SalaryPlanGradeRuleCode Insert(DatabaseUser user, SalaryPlanGradeRuleCode item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SalaryPlanGradeRulePaycode_insert", user.DatabaseName))
                {
                    SqlParameter idParam = new SqlParameter("@salaryPlanGradeRulePaycodeId", SqlDbType.BigInt) { Direction = ParameterDirection.Output };
                    command.AddParameter(idParam);
                    command.AddParameter(new SqlParameter("@salaryPlanGradeRuleId", SqlDbType.BigInt) {Value = item.SalaryPlanGradeRuleId});
                    command.AddParameter(new SqlParameter("@inputPaycode", SqlDbType.VarChar, 8) {Value = item.InputPayCode});
                    command.AddParameter(new SqlParameter("@outputPaycode", SqlDbType.VarChar, 8) {Value = item.OutputPayCode});

                    SqlParameter rowVersionParm = new SqlParameter("@rowVersion", SqlDbType.Timestamp)
                    {
                        Value = item.RowVersion,
                        Direction = ParameterDirection.Output
                    };
                    command.AddParameter(rowVersionParm);
                    command.AddParameterWithValue("@createUser", user.UserName);
                    command.AddParameterWithValue("@createDatetime", Time);

                    command.ExecuteNonQuery();

                    item.SalaryPlanGradeRuleCodeId = Convert.ToInt64(idParam.Value);
                    item.RowVersion = (byte[]) rowVersionParm.Value;
                }
            }
            catch (SqlException e)
            {
                HandleException(e, e.Number);
                throw;
            }

            return item;
        }

        internal SalaryPlanGradeRuleCode Update(DatabaseUser user, SalaryPlanGradeRuleCode item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SalaryPlanGradeRulePaycode_update", user.DatabaseName))
                {
                    command.AddParameter(new SqlParameter("@salaryPlanGradeRulePaycodeId", SqlDbType.BigInt){Value = item.SalaryPlanGradeRuleCodeId});
                    command.AddParameter(new SqlParameter("@salaryPlanGradeRuleId", SqlDbType.BigInt){ Value = item.SalaryPlanGradeRuleId});
                    command.AddParameter(new SqlParameter("@inputPaycode", SqlDbType.VarChar, 8){Value = item.InputPayCode});
                    command.AddParameter(new SqlParameter("@outputPaycode", SqlDbType.VarChar, 8) { Value = item.OutputPayCode });

                    SqlParameter rowVersionParm = new SqlParameter("@rowVersion", SqlDbType.Timestamp)
                    {
                        Value = item.RowVersion,
                        Direction = ParameterDirection.InputOutput
                    };
                    command.AddParameter(rowVersionParm);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;
                }
            }
            catch (SqlException exc)
            {
                HandleException(exc, exc.Number);
            }

            return item;
        }

        internal void Delete(DatabaseUser user, SalaryPlanGradeRuleCode item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SalaryPlanGradeRulePaycode_delete", user.DatabaseName))
                {
                    command.AddParameter(new SqlParameter("@salaryPlanGradeRulePaycodeId", SqlDbType.BigInt){Value = item.SalaryPlanGradeRuleCodeId});
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException exc)
            {
                HandleException(exc, exc.Number);
            }
        }

        private class GradeRuleCodeColumnNames : ColumnNames
        {
            internal const string SalaryPlanGradeRuleCodeId = "salary_plan_grade_rule_paycode_id";
            internal const string SalaryPlanGradeRuleId = "salary_plan_grade_rule_id";
            internal const string InputPaycode = "input_code_paycode_cd";
            internal const string OutputPaycode = "output_code_paycode_cd";
        }

        private SalaryPlanGradeRuleCodeCollection MapToCollection(IDataReader reader)
        {
            SalaryPlanGradeRuleCodeCollection coll = new SalaryPlanGradeRuleCodeCollection();

            while (reader.Read())
            {
                SalaryPlanGradeRuleCode ruleCode = new SalaryPlanGradeRuleCode
                {
                    SalaryPlanGradeRuleCodeId = (long) CleanDataValue(reader[GradeRuleCodeColumnNames.SalaryPlanGradeRuleCodeId]),
                    SalaryPlanGradeRuleId = (long) CleanDataValue(reader[GradeRuleCodeColumnNames.SalaryPlanGradeRuleId]),
                    InputPayCode = (string) CleanDataValue(reader[GradeRuleCodeColumnNames.InputPaycode]),
                    OutputPayCode = (string) CleanDataValue(reader[GradeRuleCodeColumnNames.OutputPaycode])
                };

                MapToBase(ruleCode, reader, true);

                coll.Add(ruleCode);
            }

            return coll;    
        }
    }
}
