﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerGrievance : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String GrievanceId = "grievance_id";
            public static String GrievanceTypeCode = "code_grievance_type_cd";
            public static String GrievanceFiledByCode = "code_grievance_filed_by_cd";
            public static String ReferenceEmployeeId = "reference_employee_id";
            public static String ReferenceLabourUnionId = "reference_labour_union_id";
            public static String Reference = "reference";
            public static String GrievanceDate = "grievance_date";
            public static String Note = "note";
            public static String ContactReference = "contact_reference";
            public static String GrievanceStatusCode = "code_grievance_status_cd";
            public static String LastStatusDatetime = "last_status_datetime";
            public static String GrievanceResolutionCode = "code_grievance_resolution_cd";
            public static String ResolutionDate = "resolution_date";
            public static String ResolutionNote = "resolution_note";
        }
        #endregion

        #region main
        /// <summary>
        /// returns an Grievance
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        internal GrievanceCollection Select(DatabaseUser user, long GrievanceId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Grievance_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@grievanceId", GrievanceId);

                GrievanceCollection grievances = null;

                using (IDataReader reader = command.ExecuteReader())
                {
                    grievances = MapToGrievanceCollection(reader);
                }

                return grievances;
            }
        }
        #endregion

        #region Grievance summary
        internal GrievanceCollection SelectGrievanceSummary(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, GrievanceCriteria criteria)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Grievance_report", user.DatabaseName))
            {
                command.AddParameterWithValue("@grievanceTypeCode", criteria.GrievanceTypeCode);
                command.AddParameterWithValue("@grievanceStatusCode", criteria.GrievanceStatusCode);
                command.AddParameterWithValue("@grievanceStartDate", criteria.GrievanceStartDate);
                command.AddParameterWithValue("@grievanceEndDate", criteria.GrievanceEndDate);
                command.AddParameterWithValue("@note", criteria.Note);
                command.AddParameterWithValue("@maxRecords", criteria.MaxRecords);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToGrievanceSummaryCollection(reader);
                }
            }
        }
        #endregion

        #region data mapping
        /// <summary>
        /// maps result set to Grievance collection
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected GrievanceCollection MapToGrievanceCollection(IDataReader reader)
        {
            GrievanceCollection collection = new GrievanceCollection();
            while (reader.Read())
            {
                collection.Add(MapToGrievance(reader));
            }

            return collection;
        }

        /// <summary>
        /// maps columns to Grievance properties
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected Grievance MapToGrievance(IDataReader reader)
        {
            Grievance Grievance = new Grievance();
            base.MapToBase(Grievance, reader);

            Grievance.GrievanceId = (long)CleanDataValue(reader[ColumnNames.GrievanceId]);
            Grievance.GrievanceTypeCode = (String)CleanDataValue(reader[ColumnNames.GrievanceTypeCode]);
            Grievance.GrievanceFiledByCode = (String)CleanDataValue(reader[ColumnNames.GrievanceFiledByCode]);
            Grievance.Reference = (String)CleanDataValue(reader[ColumnNames.Reference]);
            Grievance.ReferenceEmployeeId = (long?)CleanDataValue(reader[ColumnNames.ReferenceEmployeeId]);
            Grievance.ReferenceLabourUnionId = (long?)CleanDataValue(reader[ColumnNames.ReferenceLabourUnionId]);
            Grievance.GrievanceDate = (DateTime)CleanDataValue(reader[ColumnNames.GrievanceDate]);
            Grievance.Note = (String)CleanDataValue(reader[ColumnNames.Note]);
            Grievance.ContactReference = (String)CleanDataValue(reader[ColumnNames.ContactReference]);
            Grievance.GrievanceStatusCode = (String)CleanDataValue(reader[ColumnNames.GrievanceStatusCode]);
            Grievance.LastStatusDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.LastStatusDatetime]);
            Grievance.GrievanceResolutionCode = (String)CleanDataValue(reader[ColumnNames.GrievanceResolutionCode]);
            Grievance.ResolutionDate = (DateTime?)CleanDataValue(reader[ColumnNames.ResolutionDate]);
            Grievance.ResolutionNote = (String)CleanDataValue(reader[ColumnNames.ResolutionNote]);

            return Grievance;
        }

        /// <summary>
        /// maps result set to Grievancesummary collection
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected GrievanceCollection MapToGrievanceSummaryCollection(IDataReader reader)
        {
            GrievanceCollection collection = new GrievanceCollection();
            while (reader.Read())
            {
                collection.Add(MapToGrievanceSummary(reader));
            }

            return collection;
        }
        /// <summary>
        /// maps columns to Grievance summary properties
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected Grievance MapToGrievanceSummary(IDataReader reader)
        {
            Grievance grievance = new Grievance();

            grievance.GrievanceId = (long)CleanDataValue(reader[ColumnNames.GrievanceId]);
            grievance.GrievanceTypeCode = (String)CleanDataValue(reader[ColumnNames.GrievanceTypeCode]);
            grievance.GrievanceFiledByCode = (String)CleanDataValue(reader[ColumnNames.GrievanceFiledByCode]);
            grievance.Reference = (String)CleanDataValue(reader[ColumnNames.Reference]);
            grievance.GrievanceDate = (DateTime)CleanDataValue(reader[ColumnNames.GrievanceDate]);
            grievance.Note = (String)CleanDataValue(reader[ColumnNames.Note]);
            grievance.ContactReference = (String)CleanDataValue(reader[ColumnNames.ContactReference]);
            grievance.GrievanceStatusCode = (String)CleanDataValue(reader[ColumnNames.GrievanceStatusCode]);
            grievance.LastStatusDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.LastStatusDatetime]);
            grievance.GrievanceResolutionCode = (String)CleanDataValue(reader[ColumnNames.GrievanceResolutionCode]);
            grievance.ResolutionDate = (DateTime?)CleanDataValue(reader[ColumnNames.ResolutionDate]);
            grievance.ResolutionNote = (String)CleanDataValue(reader[ColumnNames.ResolutionNote]);

            return grievance;
        }
        #endregion

        #region update
        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Grievance Grievance)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("Grievance_update", user.DatabaseName))
                {
                    command.BeginTransaction("Grievance_update");

                    command.AddParameterWithValue("@grievanceId", Grievance.GrievanceId);
                    command.AddParameterWithValue("@grievanceTypeCode", Grievance.GrievanceTypeCode);
                    command.AddParameterWithValue("@grievanceFiledByCode", Grievance.GrievanceFiledByCode);
                    command.AddParameterWithValue("@reference", Grievance.Reference);
                    command.AddParameterWithValue("@referenceEmployeeId", Grievance.ReferenceEmployeeId);
                    command.AddParameterWithValue("@referenceLabourUnionId", Grievance.ReferenceLabourUnionId);
                    command.AddParameterWithValue("@grievanceDate", Grievance.GrievanceDate);
                    command.AddParameterWithValue("@note", Grievance.Note);
                    command.AddParameterWithValue("@contactReference", Grievance.ContactReference);
                    command.AddParameterWithValue("@grievanceStatusCode", Grievance.GrievanceStatusCode);
                    command.AddParameterWithValue("@lastStatusDatetime", Grievance.LastStatusDatetime);
                    command.AddParameterWithValue("@grievanceResolutionCode", Grievance.GrievanceResolutionCode);
                    command.AddParameterWithValue("@resolutionDate", Grievance.ResolutionDate);
                    command.AddParameterWithValue("@resolutionNote", Grievance.ResolutionNote);

                    //command.AddParameterWithValue("@citizenshipCode", Grievance.CitizenshipCode);
                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", Grievance.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    Grievance.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();

                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region insert
        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Grievance data)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Grievance_insert", user.DatabaseName))
            {
                command.BeginTransaction("Grievance_insert");
                SqlParameter GrievanceIdParm = command.AddParameterWithValue("@grievanceId", data.GrievanceId, ParameterDirection.Output);
                command.AddParameterWithValue("@grievanceTypeCode", data.GrievanceTypeCode);
                command.AddParameterWithValue("@grievanceFiledByCode", data.GrievanceFiledByCode);
                command.AddParameterWithValue("@reference", data.Reference);
                command.AddParameterWithValue("@referenceEmployeeId", data.ReferenceEmployeeId);
                command.AddParameterWithValue("@referenceLabourUnionId", data.ReferenceLabourUnionId);
                command.AddParameterWithValue("@grievanceDate", data.GrievanceDate);
                command.AddParameterWithValue("@note", data.Note);
                command.AddParameterWithValue("@contactReference", data.ContactReference);
                command.AddParameterWithValue("@grievanceStatusCode", data.GrievanceStatusCode);
                command.AddParameterWithValue("@lastStatusDatetime", data.LastStatusDatetime);
                command.AddParameterWithValue("@grievanceResolutionCode", data.GrievanceResolutionCode);
                command.AddParameterWithValue("@resolutionDate", data.ResolutionDate);
                command.AddParameterWithValue("@resolutionNote", data.ResolutionNote);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", data.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                data.GrievanceId = Convert.ToInt64(GrievanceIdParm.Value);
                data.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();

            }
        }
        #endregion

        #region delete
        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Grievance Grievance)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("Grievance_delete", user.DatabaseName))
                {
                    command.BeginTransaction("Grievance_delete");
                    command.AddParameterWithValue("@GrievanceId", Grievance.GrievanceId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion
    }
}