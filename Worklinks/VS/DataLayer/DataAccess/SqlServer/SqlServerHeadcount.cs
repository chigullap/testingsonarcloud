﻿using System;
using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerHeadcount : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String HeadcountCode = "headcount_cd";
            public static String Description = "description";
            public static String Count = "count";
            public static String Percentage = "percentage";
            public static String ChartTitle = "chart_title";
        }
        #endregion

        #region main
        internal HeadcountCollection Select(DatabaseUser user)
        {
            DataBaseCommand command = GetStoredProcCommand("Headcount_select", user.DatabaseName);

            command.AddParameterWithValue("@languageCode", user.LanguageCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCollection(reader);
        }
        internal HeadcountCollection SelectByOrganizationUnitLevelId(DatabaseUser user, long organizationUnitLevelId)
        {
            DataBaseCommand command = GetStoredProcCommand("Headcount_selectByOrganizationUnitLevelId", user.DatabaseName);

            command.AddParameterWithValue("@organizationUnitLevelId", organizationUnitLevelId);
            command.AddParameterWithValue("@languageCode", user.LanguageCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCollection(reader);
        }
        #endregion

        #region data mapping
        protected HeadcountCollection MapToCollection(IDataReader reader)
        {
            HeadcountCollection collection = new HeadcountCollection();

            while (reader.Read())
                collection.Add(MapToItem(reader));

            return collection;
        }
        protected Headcount MapToItem(IDataReader reader)
        {
            Headcount item = new Headcount();

            item.HeadcountCode = (String)CleanDataValue(reader[ColumnNames.HeadcountCode]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            item.Count = (Decimal)CleanDataValue(reader[ColumnNames.Count]);
            item.Percentage = (Decimal)CleanDataValue(reader[ColumnNames.Percentage]);
            item.ChartTitle = (String)CleanDataValue(reader[ColumnNames.ChartTitle]);

            return item;
        }
        #endregion
    }
}