﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerPayrollMasterPayment : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayrollMasterPaymentId = "payroll_master_payment_id";
            public static String PayrollProcessId = "payroll_process_id";
            public static String EmployeeId = "employee_id";
            public static String Amount = "amount";
            public static String ChequeNumber = "cheque_number";
            public static String CodeEmployeeBankingSequenceCd = "code_employee_banking_sequence_cd";
            public static String CodeEmployeeBankCd = "code_employee_bank_cd";
            public static String BankingCountryCode = "code_banking_country_cd";
            public static String TransitNumber = "transit_number";
            public static String AccountNumber = "account_number";
            public static String CodeStatusCd = "code_status_cd";

            //for exports
            public static String CodeCompanyCd = "code_company_cd";
            public static String EmployeeNumber = "employee_number";
            public static String ChequeDate = "cheque_date";
            public static String EmployeeName = "employee_name";

            //for generating the payroll master data
            public static String Reason = "reason";
        }
        #endregion

        #region select
        public PayrollMasterPaymentCollection GetCheque(DatabaseUser user, long payrollProcessId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollMasterPayment_selectCheque", user.DatabaseName))
            {
                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToPayrollMasterPaymentChequeCollection(reader);
            }
        }
        public PayrollMasterPaymentCollection GetDirectDeposit(DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            DataBaseCommand command = GetStoredProcCommand("PayrollMasterPayment_selectDirectDeposit", user.DatabaseName);

            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
            command.AddParameterWithValue("@employeeNumber", employeeNumber);

            using (IDataReader reader = command.ExecuteReader())
                return MapToPayrollMasterPaymentDirectDepositCollection(reader);
        }
        #endregion

        #region update
        public void UpdateSequenceNumber(DatabaseUser user, PayrollProcess process)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollMasterPayment_updateSequenceNumber", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollMasterPayment_updateSequenceNumber");

                    command.AddParameterWithValue("@payrollProcessId", process.PayrollProcessId);

                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void UpdateStatus(DatabaseUser user, PayrollMasterPayment item, String status)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollMasterPayment_updateStatus", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollMasterPayment_updateStatus");

                    command.AddParameterWithValue("@payrollMasterPaymentId", item.PayrollMasterPaymentId);
                    command.AddParameterWithValue("@codeStatusCd", status);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void BatchUpdate(DatabaseUser user, RbcEftEdiCollection items, ScotiaBankEdiCollection scotia, string status)
        {
            DataBaseCommand command = GetStoredProcCommand("PayrollMasterPayment_batchStatusUpdate", user.DatabaseName);

            using (command)
            {
                command.BeginTransaction("PayrollMasterPayment_batchStatusUpdate");

                //first table parameter
                SqlParameter parmPayrollMasterPayment = new SqlParameter();
                parmPayrollMasterPayment.ParameterName = "@parmPayrollMasterPayment";
                parmPayrollMasterPayment.SqlDbType = SqlDbType.Structured;

                DataTable tablePayrollMasterPayment = null;

                if (items != null && items.Count > 0)
                    tablePayrollMasterPayment = LoadPayrollMasterPaymentDataTableRbc(items);
                else if (scotia != null && scotia.Count > 0)
                    tablePayrollMasterPayment = LoadPayrollMasterPaymentDataTableScotia(scotia);

                parmPayrollMasterPayment.Value = tablePayrollMasterPayment;
                command.AddParameter(parmPayrollMasterPayment);

                command.AddParameterWithValue("@codeStatusCd", status);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        private DataTable LoadPayrollMasterPaymentDataTableRbc(RbcEftEdiCollection items)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("payroll_master_payment_id", typeof(long));

            //loop thru and add values
            foreach (RbcEftEdi item in items)
            {
                table.Rows.Add(new Object[]
                {
                    item.PayrollMasterPaymentId
                });
            }

            return table;
        }
        private DataTable LoadPayrollMasterPaymentDataTableScotia(ScotiaBankEdiCollection items)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("payroll_master_payment_id", typeof(long));

            //loop thru and add values
            foreach (ScotiaBankEdi item in items)
            {
                table.Rows.Add(new Object[]
                {
                    item.PayrollMasterPaymentId
                });
            }

            return table;
        }
        #endregion

        #region insert
        public PayrollMasterPayment Insert(DatabaseUser user, PayrollMasterPayment payment)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollMasterPayment_insert", user.DatabaseName))
            {
                command.BeginTransaction("PayrollMasterPayment_insert");

                SqlParameter payrollMasterPaymentIdParm = command.AddParameterWithValue("@payrollMasterPaymentId", payment.PayrollMasterPaymentId, ParameterDirection.Output);
                command.AddParameterWithValue("@payrollMasterId", payment.PayrollMasterId);
                command.AddParameterWithValue("@payrollProcessId", payment.PayrollProcessId);
                command.AddParameterWithValue("@employeeId", payment.EmployeeId);
                command.AddParameterWithValue("@amount", payment.Amount);
                command.AddParameterWithValue("@chequeNumber", payment.ChequeNumber);
                command.AddParameterWithValue("@codeEmployeeBankingSequenceCd", payment.CodeEmployeeBankingSequenceCd);
                command.AddParameterWithValue("@codeEmployeeBankCd", payment.CodeEmployeeBankCd);
                command.AddParameterWithValue("@bankingCountryCode", payment.BankingCountryCode);
                command.AddParameterWithValue("@transitNumber", payment.TransitNumber);
                command.AddParameterWithValue("@accountNumber", payment.AccountNumber);
                command.AddParameterWithValue("@codeStatusCd", payment.CodeStatusCd);
                command.AddParameterWithValue("@directDepositFlag", payment.DirectDepositFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", payment.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                payment.PayrollMasterPaymentId = Convert.ToInt64(payrollMasterPaymentIdParm.Value);
                payment.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return payment;
            }
        }
        #endregion

        #region delete
        public void DeleteById(DatabaseUser user, long payrollMasterPaymentId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollMasterPayment_deleteById", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollMasterPayment_deleteById");

                    command.AddParameterWithValue("@payrollMasterPaymentId", payrollMasterPaymentId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void DeleteByProcessId(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollMasterPayment_delete", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollMasterPayment_delete");

                    command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        public PayrollMasterPaymentErrorCollection GenerateData(DatabaseUser user, long payrollProcessId)
        {
            DataBaseCommand command = GetStoredProcCommand("PayrollMasterPayment_generate", user.DatabaseName);

            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

            command.AddParameterWithValue("@updateUser", user.UserName);
            command.AddParameterWithValue("@updateDateTime", DateTime.Now);

            using (IDataReader reader = command.ExecuteReader())
                return MapToPayrollMasterPaymentErrorCollection(reader);
        }
        public long GetSequenceNumber(DatabaseUser user, string sequenceName)
        {
            long sequence = 0;

            DataBaseCommand command = GetStoredProcCommand("Sequence_getNextKey", user.DatabaseName);
            command.BeginTransaction("Sequence_getNextKey");

            command.AddParameterWithValue("@name", sequenceName);
            command.AddParameterWithValue("@updateUser", user.UserName);
            command.AddParameterWithValue("@updateDatetime", Time);

            SqlParameter sequenceReturnParm = command.AddParameterWithValue("@nextKey", 0, ParameterDirection.InputOutput);
            sequenceReturnParm.Size = 32;

            command.ExecuteNonQuery();
            sequence = Convert.ToInt64(sequenceReturnParm.Value);
            command.CommitTransaction();

            return sequence;
        }

        #region data mapping
        protected PayrollMasterPaymentCollection MapToPayrollMasterPaymentChequeCollection(IDataReader reader)
        {
            PayrollMasterPaymentCollection collection = new PayrollMasterPaymentCollection();

            while (reader.Read())
                collection.Add(MapToPayrollMasterPaymentCheque(reader));

            return collection;
        }
        protected PayrollMasterPayment MapToPayrollMasterPaymentCheque(IDataReader reader)
        {
            PayrollMasterPayment item = new PayrollMasterPayment();
            base.MapToBase(item, reader);

            item.PayrollMasterPaymentId = ((long)CleanDataValue(reader[ColumnNames.PayrollMasterPaymentId]));
            item.CodeCompanyCd = ((String)CleanDataValue(reader[ColumnNames.CodeCompanyCd]));
            item.EmployeeNumber = ((String)CleanDataValue(reader[ColumnNames.EmployeeNumber]));
            item.EmployeeId = ((long)CleanDataValue(reader[ColumnNames.EmployeeId]));
            item.PayrollProcessId = ((long)CleanDataValue(reader[ColumnNames.PayrollProcessId]));
            item.ChequeNumber = ((String)CleanDataValue(reader[ColumnNames.ChequeNumber]));
            item.Amount = ((Decimal)CleanDataValue(reader[ColumnNames.Amount]));
            item.ChequeDate = ((DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]));

            return item;
        }
        protected PayrollMasterPaymentCollection MapToPayrollMasterPaymentDirectDepositCollection(IDataReader reader)
        {
            PayrollMasterPaymentCollection collection = new PayrollMasterPaymentCollection();

            while (reader.Read())
                collection.Add(MapToPayrollMasterPaymentDirectDeposit(reader));

            return collection;
        }
        protected PayrollMasterPayment MapToPayrollMasterPaymentDirectDeposit(IDataReader reader)
        {
            PayrollMasterPayment item = new PayrollMasterPayment();
            base.MapToBase(item, reader);

            item.PayrollMasterPaymentId = (long)CleanDataValue(reader[ColumnNames.PayrollMasterPaymentId]);
            item.CodeCompanyCd = (String)CleanDataValue(reader[ColumnNames.CodeCompanyCd]);
            item.EmployeeNumber = (String)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.PayrollProcessId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessId]);
            item.CodeEmployeeBankingSequenceCd = (String)CleanDataValue(reader[ColumnNames.CodeEmployeeBankingSequenceCd]);
            item.ChequeNumber = (String)CleanDataValue(reader[ColumnNames.ChequeNumber]);
            item.EmployeeName = (String)CleanDataValue(reader[ColumnNames.EmployeeName]);
            item.CodeEmployeeBankCd = (String)CleanDataValue(reader[ColumnNames.CodeEmployeeBankCd]);
            item.BankingCountryCode = (String)CleanDataValue(reader[ColumnNames.BankingCountryCode]);
            item.TransitNumber = (String)CleanDataValue(reader[ColumnNames.TransitNumber]);
            item.AccountNumber = (String)CleanDataValue(reader[ColumnNames.AccountNumber]);
            item.Amount = (Decimal)CleanDataValue(reader[ColumnNames.Amount]);
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.CodeStatusCd = (String)CleanDataValue(reader[ColumnNames.CodeStatusCd]);

            return item;
        }
        protected PayrollMasterPaymentErrorCollection MapToPayrollMasterPaymentErrorCollection(IDataReader reader)
        {
            PayrollMasterPaymentErrorCollection collection = new PayrollMasterPaymentErrorCollection();

            while (reader.Read())
                collection.Add(PayrollMasterPaymentError(reader));

            return collection;
        }
        protected PayrollMasterPaymentError PayrollMasterPaymentError(IDataReader reader)
        {
            PayrollMasterPaymentError item = new PayrollMasterPaymentError();

            item.PayrollMasterPaymentId = (long)CleanDataValue(reader[ColumnNames.PayrollMasterPaymentId]);
            item.Reason = (String)CleanDataValue(reader[ColumnNames.Reason]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EmployeeNumber = (String)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.Amount = (Decimal)CleanDataValue(reader[ColumnNames.Amount]);

            return item;
        }
        #endregion
    }
}