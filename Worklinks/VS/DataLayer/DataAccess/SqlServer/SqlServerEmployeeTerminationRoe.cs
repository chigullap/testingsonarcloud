﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeTerminationRoe : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeTerminationRoeId = "employee_termination_roe_id";
            public static String EmployeePositionId = "employee_position_id";
            public static String FirstDayWorked = "first_day_worked";
            public static String PayrollPeriodCutoffDate = "payroll_period_cutoff_date";
            public static String Amount = "amount";
            public static String CodeEmployeeTerminationRoePaidLeaveCd = "code_employee_termination_roe_paid_leave_cd";
            public static String ExpectedDateOfRecall = "expected_date_of_recall";
            public static String CodeEmployeeTerminationRoeDateRecallCd = "code_employee_termination_roe_date_recall_cd";
            public static String ContactLastName = "contact_last_name";
            public static String ContactFirstName = "contact_first_name";
            public static String ContactTelephone = "contact_telephone";
            public static String ContactTelephoneExtension = "contact_telephone_extension";
            public static String Comment1 = "comment1";
            public static String Comment2 = "comment2";
            public static String Comment3 = "comment3";
            public static String VacationPay = "vacation_pay";
            public static String CodeRoeVacationPayTypeCd = "code_roe_vacation_pay_type_cd";
            public static String StatutoryHolidayPay1Date = "statutory_holiday_pay_1_date";
            public static String StatutoryHolidayPay1Amount = "statutory_holiday_pay_1_amount";
            public static String StatutoryHolidayPay2Date = "statutory_holiday_pay_2_date";
            public static String StatutoryHolidayPay2Amount = "statutory_holiday_pay_2_amount";
            public static String StatutoryHolidayPay3Date = "statutory_holiday_pay_3_date";
            public static String StatutoryHolidayPay3Amount = "statutory_holiday_pay_3_amount";
            public static String FirstOtherMoniesCd = "first_other_monies_cd";
            public static String OtherMoniesFirstAmount = "other_monies_first_amount";
            public static String SecondOtherMoniesCd = "second_other_monies_cd";
            public static String OtherMoniesSecondAmount = "other_monies_second_amount";
            public static String ThirdOtherMoniesCd = "third_other_monies_cd";
            public static String OtherMoniesThirdAmount = "other_monies_third_amount";
            //used for summary, display only
            public static String TerminationDate = "effective_date";
            public static String CodeRoeReasonCd = "code_termination_reason_cd";
            public static String LastDayPaid = "last_day_paid";

        }
        #endregion

        #region main
        internal EmployeeTerminationRoeCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeePositionId)
        {
            return Select(user, employeePositionId, null);
        }

        internal EmployeeTerminationRoeCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeePositionId, long? EmployeeTerminationRoeId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeeTerminationRoe_select", user.DatabaseName);
            command.AddParameterWithValue("@employeePositionId", employeePositionId);
            command.AddParameterWithValue("@EmployeeTerminationRoeId", EmployeeTerminationRoeId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToEmployeeTerminationRoeCollection(reader);
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeTerminationRoe roe)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeTerminationRoe_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeTerminationRoe_update");

                    command.AddParameterWithValue("@EmployeeTerminationRoeId", roe.EmployeeTerminationRoeId);
                    command.AddParameterWithValue("@employeePositionId", roe.EmployeePositionId);
                    command.AddParameterWithValue("@amount", roe.Amount);
                    command.AddParameterWithValue("@codeEmployeeTerminationRoePaidLeaveCd", roe.CodeEmployeeTerminationRoePaidLeaveCd);
                    command.AddParameterWithValue("@expectedDateOfRecall", roe.ExpectedDateOfRecall);
                    command.AddParameterWithValue("@codeEmployeeTerminationRoeDateRecallCd", roe.CodeEmployeeTerminationRoeDateRecallCd);
                    command.AddParameterWithValue("@contactLastName", roe.ContactLastName);
                    command.AddParameterWithValue("@contactFirstName", roe.ContactFirstName);
                    command.AddParameterWithValue("@contactTelephone", roe.ContactTelephone);
                    command.AddParameterWithValue("@contactTelephoneExtension", roe.ContactTelephoneExtension);
                    command.AddParameterWithValue("@comment1", roe.Comment1);
                    command.AddParameterWithValue("@comment2", roe.Comment2);
                    command.AddParameterWithValue("@comment3", roe.Comment3);
                    command.AddParameterWithValue("@vacationPay", roe.VacationPay);
                    command.AddParameterWithValue("@codeRoeVacationPayTypeCd", roe.CodeRoeVacationPayTypeCd);
                    command.AddParameterWithValue("@statutoryHolidayPay1Date", roe.StatutoryHolidayPay1Date);
                    command.AddParameterWithValue("@statutoryHolidayPay1Amount", roe.StatutoryHolidayPay1Amount);
                    command.AddParameterWithValue("@statutoryHolidayPay2Date", roe.StatutoryHolidayPay2Date);
                    command.AddParameterWithValue("@statutoryHolidayPay2Amount", roe.StatutoryHolidayPay2Amount);
                    command.AddParameterWithValue("@statutoryHolidayPay3Date", roe.StatutoryHolidayPay3Date);
                    command.AddParameterWithValue("@statutoryHolidayPay3Amount", roe.StatutoryHolidayPay3Amount);
                    command.AddParameterWithValue("@otherMoniesFirstCd", roe.FirstOtherMoniesCd);
                    command.AddParameterWithValue("@otherMoniesFirstAmount", roe.OtherMoniesFirstAmount);
                    command.AddParameterWithValue("@otherMoniesSecondCd", roe.SecondOtherMoniesCd);
                    command.AddParameterWithValue("@otherMoniesSecondAmount", roe.OtherMoniesSecondAmount);
                    command.AddParameterWithValue("@otherMoniesThirdCd", roe.ThirdOtherMoniesCd);
                    command.AddParameterWithValue("@otherMoniesThirdAmount", roe.OtherMoniesThirdAmount);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", roe.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    roe.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public EmployeeTerminationRoe Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeTerminationRoe roe)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeTerminationRoe_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeTerminationRoe_insert");

                SqlParameter EmployeeTerminationRoeIdParm = command.AddParameterWithValue("@EmployeeTerminationRoeId", roe.EmployeeTerminationRoeId, ParameterDirection.Output);

                command.AddParameterWithValue("@employeePositionId", roe.EmployeePositionId);
                command.AddParameterWithValue("@firstDayWorked", roe.FirstDayWorked);
                command.AddParameterWithValue("@payrollPeriodCutoffDate", roe.PayrollPeriodCutoffDate);
                command.AddParameterWithValue("@amount", roe.Amount);
                command.AddParameterWithValue("@codeEmployeeTerminationRoePaidLeaveCd", roe.CodeEmployeeTerminationRoePaidLeaveCd);
                command.AddParameterWithValue("@expectedDateOfRecall", roe.ExpectedDateOfRecall);
                command.AddParameterWithValue("@codeEmployeeTerminationRoeDateRecallCd", roe.CodeEmployeeTerminationRoeDateRecallCd);
                command.AddParameterWithValue("@contactLastName", roe.ContactLastName);
                command.AddParameterWithValue("@contactFirstName", roe.ContactFirstName);
                command.AddParameterWithValue("@contactTelephone", roe.ContactTelephone);
                command.AddParameterWithValue("@contactTelephoneExtension", roe.ContactTelephoneExtension);
                command.AddParameterWithValue("@comment1", roe.Comment1);
                command.AddParameterWithValue("@comment2", roe.Comment2);
                command.AddParameterWithValue("@comment3", roe.Comment3);
                command.AddParameterWithValue("@vacationPay", roe.VacationPay);
                command.AddParameterWithValue("@codeRoeVacationPayTypeCd", roe.CodeRoeVacationPayTypeCd);
                command.AddParameterWithValue("@statutoryHolidayPay1Date", roe.StatutoryHolidayPay1Date);
                command.AddParameterWithValue("@statutoryHolidayPay1Amount", roe.StatutoryHolidayPay1Amount);
                command.AddParameterWithValue("@statutoryHolidayPay2Date", roe.StatutoryHolidayPay2Date);
                command.AddParameterWithValue("@statutoryHolidayPay2Amount", roe.StatutoryHolidayPay2Amount);
                command.AddParameterWithValue("@statutoryHolidayPay3Date", roe.StatutoryHolidayPay3Date);
                command.AddParameterWithValue("@statutoryHolidayPay3Amount", roe.StatutoryHolidayPay3Amount);
                command.AddParameterWithValue("@otherMoniesFirstCd", roe.FirstOtherMoniesCd);
                command.AddParameterWithValue("@otherMoniesFirstAmount", roe.OtherMoniesFirstAmount);
                command.AddParameterWithValue("@otherMoniesSecondCd", roe.SecondOtherMoniesCd);
                command.AddParameterWithValue("@otherMoniesSecondAmount", roe.OtherMoniesSecondAmount);
                command.AddParameterWithValue("@otherMoniesThirdCd", roe.ThirdOtherMoniesCd);
                command.AddParameterWithValue("@otherMoniesThirdAmount", roe.OtherMoniesThirdAmount);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", roe.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                roe.EmployeeTerminationRoeId = Convert.ToInt64(EmployeeTerminationRoeIdParm.Value);
                roe.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();
                return roe;
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeTerminationRoe roe)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeTerminationRoe_delete", user.DatabaseName))
                {

                    command.BeginTransaction("EmployeeTerminationRoe_delete");
                    command.AddParameterWithValue("@EmployeeTerminationRoeId", roe.EmployeeTerminationRoeId);
                    command.AddParameterWithValue("@employeePositionId", roe.EmployeePositionId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeTerminationRoeCollection MapToEmployeeTerminationRoeCollection(IDataReader reader)
        {
            EmployeeTerminationRoeCollection collection = new EmployeeTerminationRoeCollection();
            while (reader.Read())
            {
                collection.Add(MapToEmployeeTerminationRoe(reader));
            }

            return collection;
        }
        protected EmployeeTerminationRoe MapToEmployeeTerminationRoe(IDataReader reader)
        {
            EmployeeTerminationRoe roe = new EmployeeTerminationRoe();
            base.MapToBase(roe, reader);

            roe.EmployeeTerminationRoeId = (long)CleanDataValue(reader[ColumnNames.EmployeeTerminationRoeId]);
            roe.EmployeePositionId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionId]);
            roe.FirstDayWorked = (DateTime?)CleanDataValue(reader[ColumnNames.FirstDayWorked]);
            roe.PayrollPeriodCutoffDate = (DateTime?)CleanDataValue(reader[ColumnNames.PayrollPeriodCutoffDate]);
            roe.Amount = (decimal?)CleanDataValue(reader[ColumnNames.Amount]);
            roe.CodeEmployeeTerminationRoePaidLeaveCd = (String)CleanDataValue(reader[ColumnNames.CodeEmployeeTerminationRoePaidLeaveCd]);
            roe.ExpectedDateOfRecall = (DateTime?)CleanDataValue(reader[ColumnNames.ExpectedDateOfRecall]);
            roe.CodeEmployeeTerminationRoeDateRecallCd = (String)CleanDataValue(reader[ColumnNames.CodeEmployeeTerminationRoeDateRecallCd]); 
            roe.ContactLastName = (String)CleanDataValue(reader[ColumnNames.ContactLastName]);
            roe.ContactFirstName = (String)CleanDataValue(reader[ColumnNames.ContactFirstName]);
            roe.ContactTelephone = (String)CleanDataValue(reader[ColumnNames.ContactTelephone]);
            roe.ContactTelephoneExtension = (String)CleanDataValue(reader[ColumnNames.ContactTelephoneExtension]);
            roe.Comment1 = (String)CleanDataValue(reader[ColumnNames.Comment1]);
            roe.Comment2 = (String)CleanDataValue(reader[ColumnNames.Comment2]);
            roe.Comment3 = (String)CleanDataValue(reader[ColumnNames.Comment3]);
            roe.VacationPay = (decimal?)CleanDataValue(reader[ColumnNames.VacationPay]);
            roe.CodeRoeVacationPayTypeCd = (String)CleanDataValue(reader[ColumnNames.CodeRoeVacationPayTypeCd]);
            roe.StatutoryHolidayPay1Date = (DateTime?)CleanDataValue(reader[ColumnNames.StatutoryHolidayPay1Date]);
            roe.StatutoryHolidayPay1Amount = (decimal?)CleanDataValue(reader[ColumnNames.StatutoryHolidayPay1Amount]);
            roe.StatutoryHolidayPay2Date = (DateTime?)CleanDataValue(reader[ColumnNames.StatutoryHolidayPay2Date]);
            roe.StatutoryHolidayPay2Amount = (decimal?)CleanDataValue(reader[ColumnNames.StatutoryHolidayPay2Amount]);
            roe.StatutoryHolidayPay3Date = (DateTime?)CleanDataValue(reader[ColumnNames.StatutoryHolidayPay3Date]);
            roe.StatutoryHolidayPay3Amount = (decimal?)CleanDataValue(reader[ColumnNames.StatutoryHolidayPay3Amount]);
            roe.FirstOtherMoniesCd = (String)CleanDataValue(reader[ColumnNames.FirstOtherMoniesCd]);
            roe.OtherMoniesFirstAmount = (decimal?)CleanDataValue(reader[ColumnNames.OtherMoniesFirstAmount]);
            roe.SecondOtherMoniesCd = (String)CleanDataValue(reader[ColumnNames.SecondOtherMoniesCd]);
            roe.OtherMoniesSecondAmount = (decimal?)CleanDataValue(reader[ColumnNames.OtherMoniesSecondAmount]);
            roe.ThirdOtherMoniesCd = (String)CleanDataValue(reader[ColumnNames.ThirdOtherMoniesCd]);
            roe.OtherMoniesThirdAmount = (decimal?)CleanDataValue(reader[ColumnNames.OtherMoniesThirdAmount]);
            roe.TerminationDate = (DateTime?)CleanDataValue(reader[ColumnNames.TerminationDate]);
            roe.CodeRoeReasonCd = (String)CleanDataValue(reader[ColumnNames.CodeRoeReasonCd]);
            roe.LastDayPaid = (DateTime?)CleanDataValue(reader[ColumnNames.LastDayPaid]);

            return roe;
        }
        #endregion
    }
}