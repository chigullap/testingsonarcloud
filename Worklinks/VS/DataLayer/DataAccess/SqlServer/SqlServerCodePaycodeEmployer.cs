﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerCodePaycodeEmployer : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PaycodeEmployerId = "paycode_employer_id";
            public static String PaycodeCode = "code_paycode_cd";
            public static String OffsetGeneralLedgerMask = "offset_general_ledger_mask";
        }
        #endregion

        #region main
        internal CodePaycodeEmployerCollection Select(DatabaseUser user, String paycodeCode)
        {
            DataBaseCommand command = GetStoredProcCommand("CodePaycodeEmployer_select", user.DatabaseName);

            command.AddParameterWithValue("@paycodeCode", paycodeCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCodeCollection(reader);
        }
        public CodePaycodeEmployer Insert(DatabaseUser user, CodePaycodeEmployer item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CodePaycodeEmployer_insert", user.DatabaseName))
            {
                command.BeginTransaction("CodePaycodeEmployer_insert");

                SqlParameter paycodeEmployerIdParm = command.AddParameterWithValue("@paycodeEmployerId", item.PaycodeEmployerId, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@paycodeCode", item.PaycodeCode);
                command.AddParameterWithValue("@offsetGeneralLedgerMask", item.OffsetGeneralLedgerMask);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.PaycodeEmployerId = (long)paycodeEmployerIdParm.Value;
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        public void Update(DatabaseUser user, CodePaycodeEmployer item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CodePaycodeEmployer_update", user.DatabaseName))
                {
                    command.BeginTransaction("CodePaycodeEmployer_update");

                    command.AddParameterWithValue("@paycodeCode", item.PaycodeCode);
                    command.AddParameterWithValue("@offsetGeneralLedgerMask", item.OffsetGeneralLedgerMask);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, CodePaycode item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CodePaycodeEmployer_delete", user.DatabaseName))
                {
                    command.BeginTransaction("CodePaycodeEmployer_delete");

                    command.AddParameterWithValue("@paycodeCode", item.PaycodeCode);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected CodePaycodeEmployerCollection MapToCodeCollection(IDataReader reader)
        {
            CodePaycodeEmployerCollection collection = new CodePaycodeEmployerCollection();

            while (reader.Read())
                collection.Add(MapToCode(reader));

            return collection;
        }
        protected CodePaycodeEmployer MapToCode(IDataReader reader)
        {
            CodePaycodeEmployer item = new CodePaycodeEmployer();
            base.MapToBase(item, reader);

            item.PaycodeEmployerId = (long)CleanDataValue(reader[ColumnNames.PaycodeEmployerId]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.OffsetGeneralLedgerMask = (String)CleanDataValue(reader[ColumnNames.OffsetGeneralLedgerMask]);

            return item;
        }
        #endregion 
    }
}