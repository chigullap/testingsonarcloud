﻿using System;
using System.Data;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerImportExportXml : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ImportExportXmlId = "import_export_xml_id";
            public static String ImportExportId = "import_export_id";
            public static String ProcessingOrder = "processing_order";
            public static String Data = "data";
            public static String InputFileParameterRequiredFlag = "input_file_parameter_required_flag";
            public static String InputFileParameterName = "input_file_parameter_name";
        }
        #endregion

        #region main
        internal ImportExportXmlSummaryCollection SelectImportExportXmlSummary(DatabaseUser user, long? importExportId, String label)
        {
            DataBaseCommand command = GetStoredProcCommand("ImportExportXml_report",user.DatabaseName);
            command.AddParameterWithValue("@importExportId", importExportId);
            command.AddParameterWithValue("@label", label);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToImportExportXmlSummaryCollection(reader);
            }
        }

       
        #endregion


        #region data mapping

        protected ImportExportXmlSummaryCollection MapToImportExportXmlSummaryCollection(IDataReader reader)
        {
            ImportExportXmlSummaryCollection collection = new ImportExportXmlSummaryCollection();
            while (reader.Read())
            {
                collection.Add(MapToImportExportXmlSummary(reader));
            }

            return collection;
        }

        protected ImportExportXmlSummary MapToImportExportXmlSummary(IDataReader reader)
        {
            ImportExportXmlSummary item = new ImportExportXmlSummary();
            base.MapToBase(item, reader);

            item.ImportExportXmlId = (long)CleanDataValue(reader[ColumnNames.ImportExportXmlId]);
            item.ImportExportId = (long)CleanDataValue(reader[ColumnNames.ImportExportId]);
            item.ProcessingOrder = (short)CleanDataValue(reader[ColumnNames.ProcessingOrder]);
            item.Data = (byte[])CleanDataValue(reader[ColumnNames.Data]);
            item.InputFileParameterRequiredFlag = (bool)CleanDataValue(reader[ColumnNames.InputFileParameterRequiredFlag]);
            item.InputFileParameterName = (String)CleanDataValue(reader[ColumnNames.InputFileParameterName]);
            return item;
        }
        #endregion
    }
}
