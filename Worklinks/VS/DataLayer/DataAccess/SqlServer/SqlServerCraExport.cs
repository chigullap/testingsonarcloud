﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerCraExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String CraExportId = "cra_export_id";
            public static String RbcSequenceNumber = "rbc_sequence_number";
            public static String Data = "data";
            public static String CodeCraRemittancePeriodCd = "code_cra_remittance_period_cd";
            public static String CraFileTransmissionDate = "cra_file_transmission_date";
        }
        #endregion

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CraExport item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CraExport_insert", user.DatabaseName))
            {
                command.BeginTransaction("CraExport_insert");

                SqlParameter craExportIdParm = command.AddParameterWithValue("@craExportId", item.CraExportId, ParameterDirection.Output);
                command.AddParameterWithValue("@rbcSequenceNumber", item.RbcSequenceNumber);
                command.AddParameterWithValue("@data", item.Data);
                command.AddParameterWithValue("@codeCraRemittancePeriodCd", item.CodeCraRemittancePeriodCd);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.CraExportId = Convert.ToInt64(craExportIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }

        public void UpdateCraExportFileTransmissionDate(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long craExportId, DateTime exportDate)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CraExport_UpdateCraFileTransmissionDate", user.DatabaseName))
            {
                command.BeginTransaction("CraExport_UpdateCraFileTransmissionDate");

                command.AddParameterWithValue("@craExportId", craExportId);
                command.AddParameterWithValue("@exportDate", exportDate);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        public CraExportCollection CheckForUnprocessedCraRemittanceOnDatabase(WLP.BusinessLayer.BusinessObjects.DatabaseUser user)
        {
            DataBaseCommand command = GetStoredProcCommand("CraExport_selectUnprocessed", user.DatabaseName);
            
            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToCraExportCollection(reader);
            }
        }

        public CraExportCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? craExportId)
        {
            DataBaseCommand command = GetStoredProcCommand("CraExport_select", user.DatabaseName);
            command.AddParameterWithValue("@craExportId", craExportId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToCraExportCollection(reader);
            }
        }

        #region data mapping
        protected CraExportCollection MapToCraExportCollection(IDataReader reader)
        {
            CraExportCollection collection = new CraExportCollection();
            while (reader.Read())
            {
                collection.Add(MapToCraExport(reader));
            }

            return collection;
        }
        protected CraExport MapToCraExport(IDataReader reader)
        {
            CraExport item = new CraExport();
            base.MapToBase(item, reader);

            item.CraExportId = (long)CleanDataValue(reader[ColumnNames.CraExportId]);
            item.RbcSequenceNumber = (long)CleanDataValue(reader[ColumnNames.RbcSequenceNumber]);
            item.Data = (byte[])CleanDataValue(reader[ColumnNames.Data]);
            item.CodeCraRemittancePeriodCd = (String)CleanDataValue(reader[ColumnNames.CodeCraRemittancePeriodCd]);
            item.CraFileTransmissionDate = (DateTime?)CleanDataValue(reader[ColumnNames.CraFileTransmissionDate]);

            return item;
        }

        #endregion


    }
}
