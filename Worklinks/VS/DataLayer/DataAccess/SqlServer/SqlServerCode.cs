﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerCode : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String Code = "cd";
            public static String SortOrder = "sort_order";
            public static String ActiveFlag = "active_flag";
            public static String SystemFlag = "system_flag";
            public static String StartDate = "start_date";
            public static String EndDate = "end_date";
            public static String LanguageCode = "code_language_cd";
            public static String Description = "description";
            public static String ImportExternalIdentifier = "import_external_identifier";
        }
        #endregion

        private String GetCodeTableName(CodeTableType type)
        {
            String tableName = null;

            switch (type)
            {
                case CodeTableType.TitleCode:
                    tableName = "title";
                    break;
                case CodeTableType.GenderCode:
                    tableName = "gender";
                    break;
                case CodeTableType.LanguageCode:
                    tableName = "language";
                    break;
                case CodeTableType.BilingualismCode:
                    tableName = "bilingualism";
                    break;
                case CodeTableType.CitizenshipCode:
                    tableName = "citizenship";
                    break;
                case CodeTableType.CountryCode:
                    tableName = "country";
                    break;
                case CodeTableType.EmploymentEquityCode:
                    tableName = "employment_equity";
                    break;
                case CodeTableType.MaritalStatusCode:
                    tableName = "marital_status";
                    break;
                case CodeTableType.ProvinceStateCode:
                    tableName = "province_state";
                    break;
                case CodeTableType.PaycodeTypeCode:
                    tableName = "paycode_type";
                    break;
                case CodeTableType.PaycodeCode:
                    tableName = "paycode";
                    break;
                case CodeTableType.EmployeeSkillCode:
                    tableName = "employee_skill";
                    break;
                case CodeTableType.EmployeeCourseNameCode:
                    tableName = "employee_course_name";
                    break;
                case CodeTableType.EmployeeCourseTypeCode:
                    tableName = "employee_course_type";
                    break;
                case CodeTableType.EmployeeCourseResultCode:
                    tableName = "employee_course_result";
                    break;
                case CodeTableType.EmployeeCoursePenaltyCode:
                    tableName = "employee_course_penalty";
                    break;
                case CodeTableType.EmployeeBankingTypeCode:
                    tableName = "employee_bank";
                    break;
                case CodeTableType.EmployeeBankingSequenceNumberTypeCode:
                    tableName = "employee_banking_sequence";
                    break;
                case CodeTableType.EmployeeBankingRestrictionCodeType:
                    tableName = "employee_banking_restriction";
                    break;
                case CodeTableType.EmployeeCompanyPropertyCode:
                    tableName = "employee_company_property";
                    break;
                case CodeTableType.EmployeeEducationDegreeCode:
                    tableName = "employee_education_degree";
                    break;
                case CodeTableType.EmployeeEducationMajorCode:
                    tableName = "employee_education_major";
                    break;
                case CodeTableType.EmployeeEducationCreditHourCode:
                    tableName = "employee_education_credit_hour";
                    break;
                case CodeTableType.EmployeeLicenseCertificateCode:
                    tableName = "employee_license_certificate";
                    break;
                case CodeTableType.EmployeeAwardHonourTypeCode:
                    tableName = "award_honour_type";
                    break;
                case CodeTableType.DisciplineTypeCode:
                    tableName = "discipline_type";
                    break;
                case CodeTableType.DisciplineCode:
                    tableName = "discipline";
                    break;
                case CodeTableType.DisciplineResolutionCode:
                    tableName = "discipline_resolution";
                    break;
                case CodeTableType.DisciplineActionStepCode:
                    tableName = "discipline_action_step";
                    break;
                case CodeTableType.PersonAddressTypeCode:
                    tableName = "person_address_type";
                    break;
                case CodeTableType.ContactRelationshipCode:
                    tableName = "contact_relationship";
                    break;
                case CodeTableType.GrievanceTypeCode:
                    tableName = "grievance_type";
                    break;
                case CodeTableType.GrievanceStatusCode:
                    tableName = "grievance_status";
                    break;
                case CodeTableType.ContactChannelTypeCode:
                    tableName = "contact_channel_type";
                    break;
                case CodeTableType.PayrollProcessGroupCode:
                    tableName = "payroll_process_group";
                    break;
                case CodeTableType.PayrollProcessRunTypeCode:
                    tableName = "payroll_process_run_type";
                    break;
                case CodeTableType.GrievanceFiledByCode:
                    tableName = "grievance_filed_by";
                    break;
                case CodeTableType.GrievanceResolutionCode:
                    tableName = "grievance_resolution";
                    break;
                case CodeTableType.GrievanceActionStepCode:
                    tableName = "grievance_action_step";
                    break;
                case CodeTableType.GrievanceActionStepResolutonCode:
                    tableName = "grievance_action_step_resoluton";
                    break;
                case CodeTableType.EmployeePositionStatusCode:
                    tableName = "employee_position_status";
                    break;
                case CodeTableType.EmployeePositionActionCode:
                    tableName = "employee_position_action";
                    break;
                case CodeTableType.EmployeePositionReasonCode:
                    tableName = "employee_position_reason";
                    break;
                case CodeTableType.PermanencyTypeCode:
                    tableName = "permanency_type";
                    break;
                case CodeTableType.EmploymentTypeCode:
                    tableName = "employment_type";
                    break;
                case CodeTableType.ShiftCode:
                    tableName = "shift";
                    break;
                case CodeTableType.EmployeeTypeCode:
                    tableName = "employee_type";
                    break;
                case CodeTableType.PaymentFrequencyTypeCode:
                    tableName = "payment_frequency";
                    break;
                case CodeTableType.ExportFtpTypeCode:
                    tableName = "export_ftp_type";
                    break;
                case CodeTableType.PaymentMethodCode:
                    tableName = "payment_method";
                    break;
                case CodeTableType.WSIBCode:
                    tableName = "wsib";
                    break;
                case CodeTableType.PayrollProcessAutoSelectPaycodeModeCode:
                    tableName = "payroll_process_auto_select_paycode_mode";
                    break;
                case CodeTableType.PayrollProcessStatusCode:
                    tableName = "payroll_process_status";
                    break;
                case CodeTableType.DayOfWeekCode:
                    tableName = "day_of_week";
                    break;
                case CodeTableType.OtherMoniesTypeCode:
                    tableName = "other_monies";
                    break;
                case CodeTableType.PayrollBatchStatusCode:
                    tableName = "payroll_batch_status";
                    break;
                case CodeTableType.EmployeeTerminationReasonType:
                    tableName = "termination_reason";
                    break;
                case CodeTableType.EmployeeTerminationNetPayMethodType:
                    tableName = "net_pay_method";
                    break;
                case CodeTableType.YesNoTypeCode:
                    tableName = "yes_no";
                    break;
                case CodeTableType.ROEPaidLeaveTypeCode:
                    tableName = "employee_termination_ROE_paid_leave";
                    break;
                case CodeTableType.PaymentFrequencyCode:
                    tableName = "payment_frequency";
                    break;
                case CodeTableType.ROEDateRecallTypeCode:
                    tableName = "employee_termination_ROE_date_recall";
                    break;
                case CodeTableType.System:
                    tableName = "system";
                    break;
                case CodeTableType.TrueFalseTypeCode:
                    tableName = "true_false";
                    break;
                case CodeTableType.CodeYearEndStatusTypeCode:
                    tableName = "year_end_status";
                    break;
                case CodeTableType.FieldSecurityTypeCode:
                    tableName = "control_field_security";
                    break;
                case CodeTableType.RevenueQuebecTypeOfPackageCodeType:
                    tableName = "Revenue_Quebec_Type_Of_Package";
                    break;
                case CodeTableType.CanadaRevenueAgencyBondPaymentTypeCode:
                    tableName = "canada_revenue_agency_bond_payment_type";
                    break;
                case CodeTableType.CodeRoeVacationPayType:
                    tableName = "roe_vacation_pay_type";
                    break;
                case CodeTableType.ExportTypeCode:
                    tableName = "export_type";
                    break;
                case CodeTableType.YearEndFormCode:
                    tableName = "year_end_form";
                    break;
                case CodeTableType.EmployeePositionDayOfWeekCode:
                    tableName = "employee_position_day_of_week";
                    break;
                case CodeTableType.FileTypeCode:
                    tableName = "file_type";
                    break;
                case CodeTableType.PaycodePayslipCode:
                    tableName = "paycode_payslip";
                    break;
                case CodeTableType.PaycodeRateBasedOnCode:
                    tableName = "paycode_rate_based_on";
                    break;
                case CodeTableType.PaycodeFurtherIdentificationCode:
                    tableName = "paycode_further_identification";
                    break;
                case CodeTableType.PaycodeIncomePayTypeCode:
                    tableName = "paycode_income_pay_type";
                    break;
                case CodeTableType.PaycodeIncomeReferenceCode:
                    tableName = "paycode_income_reference";
                    break;
                case CodeTableType.EmploymentDateCode:
                    tableName = "employment_date";
                    break;
                case CodeTableType.EntitlementBasedOnCode:
                    tableName = "entitlement_based_on";
                    break;
                case CodeTableType.PaycodeActivationFrequencyCode:
                    tableName = "paycode_activation_frequency";
                    break;
                case CodeTableType.CollectiveAgreementCode:
                    tableName = "collective_agreement";
                    break;
                case CodeTableType.UnionCode:
                    tableName = "union";
                    break;
                case CodeTableType.EntitlementRecurrenceCode:
                    tableName = "entitlement_recurrence";
                    break;
                case CodeTableType.CodeCraRemittancePeriod:
                    tableName = "cra_remittance_period";
                    break;
                case CodeTableType.YearEndFormBoxCode:
                    tableName = "year_end_form_box";
                    break;
                case CodeTableType.CodeRevenuQuebecRemittancePeriod:
                    tableName = "revenu_quebec_remittance_period";
                    break;
                case CodeTableType.RemittanceImportStatusCode:
                    tableName = "remittance_import_status";
                    break;
                case CodeTableType.GovernmentIdentificationNumberTypeCode:
                    tableName = "government_identification_number_type";
                    break;
                case CodeTableType.GarnishmentTypeCode:
                    tableName = "garnishment_type";
                    break;
                case CodeTableType.WcbChequeRemittanceFrequencyCode:
                    tableName = "wcb_cheque_remittance_frequency";
                    break;
                case CodeTableType.MonthCode:
                    tableName = "month";
                    break;
                case CodeTableType.DayCode:
                    tableName = "day";
                    break;
                case CodeTableType.SmokerCode:
                    tableName = "smoker";
                    break;
                case CodeTableType.StatutoryDeductionTypeCode:
                    tableName = "statutory_deduction_type";
                    break;
                case CodeTableType.BankingCountryCode:
                    tableName = "banking_country";
                    break;
                case CodeTableType.PayrollProcessGroupCountryCode:
                    tableName = "payroll_process_group_country";
                    break;
                case CodeTableType.EmailTemplateTypeCode:
                    tableName = "email_template_type";
                    break;
                case CodeTableType.EmailTriggerFieldCode:
                    tableName = "email_trigger_field";
                    break;
                case CodeTableType.EmployeeBankingDeleteAccountCode:
                    tableName = "employee_banking_delete_account";
                    break;
                case CodeTableType.EmployeeMembershipCode:
                    tableName = "employee_membership";
                    break;
                case CodeTableType.EmployeeAssociationCode:
                    tableName = "employee_association";
                    break;
                case CodeTableType.CodeBankAccountTypeCode:
                    tableName = "bank_account_type";
                    break;
                case CodeTableType.EmployeeCustomFieldNameCode:
                    tableName = "employee_custom_field_name";
                    break;
                case CodeTableType.DataTypeCode:
                    tableName = "datatype";
                    break;
                case CodeTableType.CodeProbationStatusCode:
                    tableName = "probation_status";
                    break;
                case CodeTableType.BenefitPlanTypeCode:
                    tableName = "benefit_plan_type";
                    break;
                case CodeTableType.BenefitPlanStatusCode:
                    tableName = "benefit_plan_status";
                    break;
                case CodeTableType.BenefitStatusCode:
                    tableName = "benefit_status";
                    break;
                case CodeTableType.BenefitCoverageCode:
                    tableName = "benefit_coverage";
                    break;
                case CodeTableType.BenefitSmokerCode:
                    tableName = "benefit_smoker";
                    break;
                case CodeTableType.EmployeeExportTypeCode:
                    tableName = "employee_export_type";
                    break;
                case CodeTableType.RemittanceMethodCode:
                    tableName = "remittance_method";
                    break;
                case CodeTableType.CodeSalaryPlanAdvanceBasedOn:
                    tableName = "salary_plan_advance_based_on";
                    break;
                default:
                    tableName = type.ToString().ToLower();
                    break;
            }

            return tableName;
        }
        private String GetCodeParentColumnName(CodeTableType type)
        {
            String columnName = null;

            switch (type)
            {
                case CodeTableType.ProvinceStateCode:
                    columnName = "code_country_cd";
                    break;
                case CodeTableType.EmployeePositionReasonCode:
                    columnName = "code_employee_position_action_cd";
                    break;
                case CodeTableType.EmployeePositionActionCode:
                    columnName = "code_employee_position_status_cd";
                    break;
                case CodeTableType.PaycodeCode:
                    columnName = "recurring_income_code_flag";
                    break;
                case CodeTableType.GovernmentIdentificationNumberTypeCode:
                    columnName = "code_country_cd";
                    break;
                case CodeTableType.BankingCountryCode:
                    columnName = "code_country_cd";
                    break;
                default:
                    columnName = null;
                    break;
            }

            return columnName;
        }

        #region main
        internal CodeCollection SelectContactRelationshipCode(String databaseName, String language)
        {
            return Select(databaseName, language, CodeTableType.ContactRelationshipCode); //only need to supply databaseName arg if DynamicsServiceImport is using this method
        }
        internal CodeCollection SelectPaycodeTypeCode(String databaseName, String language, string paycode)
        {
            return Select(databaseName, language, CodeTableType.PaycodeTypeCode); //only need to supply databaseName arg if DynamicsServiceImport is using this method
        }
        internal CodeCollection Select(String databaseName, String language, CodeTableType type)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Code_select", databaseName))
            {
                command.AddParameterWithValue("@codeName", GetCodeTableName(type));
                command.AddParameterWithValue("@languageTypeCd", language);

                String parentColumnName = GetCodeParentColumnName(type);

                command.AddParameterWithValue("@parentColumnName", parentColumnName);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToCodeCollection(type, reader, parentColumnName);
            }
        }
        internal CodeCollection Select(String databaseName, String language, CodeTableType type, String parentColumnName, String parentColumnValue)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Code_select", databaseName))
            {
                command.AddParameterWithValue("@codeName", GetCodeTableName(type));
                command.AddParameterWithValue("@languageTypeCd", language);
                command.AddParameterWithValue("@parentColumnName", parentColumnName);
                command.AddParameterWithValue("@parentColumnValue", parentColumnValue);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToCodeCollection(type, reader, parentColumnName);
            }
        }
        internal CodeCollection SelectDisciplineTypeCode(DatabaseUser user, string disciplineCode)
        {
            return SelectDisciplineTypeCode(user, CodeTableType.DisciplineTypeCode, disciplineCode);
        }
        protected CodeCollection SelectDisciplineTypeCode(DatabaseUser user, CodeTableType type, string disciplineCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("DisciplineTypeCode_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@languageCode", user.LanguageCode);
                command.AddParameterWithValue("@disciplineCode", disciplineCode);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToCodeCollection(type, reader, null);
            }
        }
        internal CodeCollection SelectProvinceStateCode(DatabaseUser user, String country)
        {
            return SelectProvinceStateCode(user, CodeTableType.ProvinceStateCode, country);
        }
        protected CodeCollection SelectProvinceStateCode(DatabaseUser user, CodeTableType type, String countryCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ProvinceStateCode_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@languageTypeCode", user.LanguageCode);
                command.AddParameterWithValue("@countryCode", countryCode);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToCodeCollection(type, reader, null);
            }
        }
        internal CodeCollection SelectDayCode(DatabaseUser user, String monthCode)
        {
            return SelectDayCode(user, CodeTableType.DayCode, monthCode);
        }
        protected CodeCollection SelectDayCode(DatabaseUser user, CodeTableType type, String monthCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("DayCode_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@languageCode", user.LanguageCode);
                command.AddParameterWithValue("@monthCode", monthCode);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToCodeCollection(type, reader, null);
            }
        }
        internal CodeCollection SelectYearEndFormBoxCode(DatabaseUser user, String yearEndFormCode)
        {
            return SelectYearEndFormBoxCode(user, CodeTableType.YearEndFormBoxCode, yearEndFormCode);
        }
        protected CodeCollection SelectYearEndFormBoxCode(DatabaseUser user, CodeTableType type, String yearEndFormCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("YearEndFormBoxCode_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@languageCode", user.LanguageCode);
                command.AddParameterWithValue("@yearEndFormCode", yearEndFormCode);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToCodeCollection(type, reader, null);
            }
        }
        internal CodeCollection SelectContactChannelTypeCode(DatabaseUser user, String codeContactChannelCategoryCode)
        {
            return SelectContactChannelTypeCode(user, CodeTableType.ContactChannelTypeCode, codeContactChannelCategoryCode);
        }
        protected CodeCollection SelectContactChannelTypeCode(DatabaseUser user, CodeTableType type, String contactChannelCategoryCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ContactChannelType_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@languageTypeCode", user.LanguageCode);
                command.AddParameterWithValue("@contactChannelCategoryCode", contactChannelCategoryCode);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToCodeCollection(type, reader, null);
            }
        }
        internal CodeCollection SelectPaycodesByType(DatabaseUser user, String paycodeTypeCode, String paycodeCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PaycodeByType_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@languageTypeCode", user.LanguageCode);
                command.AddParameterWithValue("@paycodeTypeCode", paycodeTypeCode);
                command.AddParameterWithValue("@paycodeCode", paycodeCode);

                String parentColumnName = GetCodeParentColumnName(CodeTableType.PaycodeCode);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToCodeCollection(CodeTableType.PaycodeCode, reader, parentColumnName);
            }
        }
        internal bool IsPaycodeGarnishment(DatabaseUser user, String paycode, String paycodeType)
        {
            using (StoredProcedureCommand command = GetStoredProcCommand("IsPaycodeGarnishment_select", user.DatabaseName))
            {
                bool temp = false;

                command.AddParameterWithValue("@paycode", paycode);
                command.AddParameterWithValue("@paycodeType", paycodeType);

                SqlParameter returnValueParm = command.AddParameterWithValue("@returnValue", temp, ParameterDirection.Output);

                command.ExecuteNonQuery();

                temp = (bool)returnValueParm.Value;

                return temp;
            }
        }
        internal CodeCollection SelectUnionCollectiveAgreementCode(DatabaseUser user, String unionCode)
        {
            return SelectUnionCollectiveAgreementCode(user, CodeTableType.CollectiveAgreementCode, unionCode);
        }
        protected CodeCollection SelectUnionCollectiveAgreementCode(DatabaseUser user, CodeTableType type, String unionCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("UnionCollectiveAgreemntCode_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@languageTypeCode", user.LanguageCode);
                command.AddParameterWithValue("@unionCode", unionCode);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToCodeCollection(type, reader, null);
            }
        }
        internal CodeCollection SelectEmployeeBankCode(DatabaseUser user, String bankingCountryCode)
        {
            return SelectEmployeeBankCode(user, CodeTableType.EmployeeBankingTypeCode, bankingCountryCode);
        }
        protected CodeCollection SelectEmployeeBankCode(DatabaseUser user, CodeTableType type, String bankingCountryCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeBankCode_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@languageCode", user.LanguageCode);
                command.AddParameterWithValue("@bankingCountryCode", bankingCountryCode);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToCodeCollection(type, reader, null);
            }
        }
        #endregion

        #region data mapping
        protected CodeCollection MapToCodeCollection(CodeTableType type, IDataReader reader, String parentColumnName)
        {
            CodeCollection collection = new CodeCollection();

            while (reader.Read())
                collection.Add(MapToCode(type, reader, parentColumnName));

            return collection;
        }
        protected CodeObject MapToCode(CodeTableType type, IDataReader reader, String parentColumnName)
        {
            CodeObject code = new CodeObject();

            //not used here...yet ....base.MapToBase(code, reader);
            code.Code = CleanDataValue(reader[ColumnNames.Code]).ToString();

            if (parentColumnName != null && !parentColumnName.Equals(String.Empty))
            {
                if (CleanDataValue(reader[parentColumnName]) != null)
                    code.ParentCode = Convert.ToString(CleanDataValue(reader[parentColumnName]));
                else
                    code.ParentCode = null;
            }

            code.SortOrder = (short)CleanDataValue(reader[ColumnNames.SortOrder]);
            code.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            code.SystemFlag = (bool)CleanDataValue(reader[ColumnNames.SystemFlag]);
            code.StartDate = (DateTime)CleanDataValue(reader[ColumnNames.StartDate]);
            code.EndDate = (DateTime)CleanDataValue(reader[ColumnNames.EndDate]);
            code.LanguageCode = (String)CleanDataValue(reader[ColumnNames.LanguageCode]);
            code.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            code.ImportExternalIdentifier = (String)CleanDataValue(reader[ColumnNames.ImportExternalIdentifier]);

            return code;
        }
        #endregion
    }
}