﻿using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerSalaryPlanGrade : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string SalaryPlanGradeId = "salary_plan_grade_id";
            public static string SalaryPlanId = "salary_plan_id";
            public static string Name = "name";
            public static string ActiveFlag = "active_flag";
            public static string EnglishDescription = "english_description";
            public static string FrenchDescription = "french_description";

            public static string OrganizationUnitId = "organization_unit_id";
            public static string ImportExternalIdentifier = "import_external_identifier";
        }
        #endregion

        #region select
        internal SalaryPlanGradeCollection Select(DatabaseUser user, long? salaryPlanGradeId, long? salaryPlanId, string languageCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SalaryPlanGrade_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@salaryPlanGradeId", salaryPlanGradeId);
                command.AddParameterWithValue("@salaryPlanId", salaryPlanId);
                command.AddParameterWithValue("@languageCode", salaryPlanId);

                SalaryPlanGradeCollection collection = null;

                using (IDataReader reader = command.ExecuteReader())
                    collection = MapToGradeCollection(reader);

                return collection;
            }
        }

        internal SalaryPlanGradeOrgUnitDescriptionComboCollection PopulateComboBoxWithSalaryPlanGrades(DatabaseUser user, long salaryPlanId, long organizationUnitLevelId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SalaryPlanGradePopulateNameCombo_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@salaryPlanId", salaryPlanId);
                command.AddParameterWithValue("@organizationUnitLevelId", organizationUnitLevelId);                

                SalaryPlanGradeOrgUnitDescriptionComboCollection collection = null;

                using (IDataReader reader = command.ExecuteReader())
                    collection = MapToSalaryPlanGradeOrgUnitDescriptionComboCollection(reader);

                return collection;
            }
        }

        #endregion

        #region data mapping
        protected SalaryPlanGradeCollection MapToGradeCollection(IDataReader reader)
        {
            SalaryPlanGradeCollection collection = new SalaryPlanGradeCollection();

            while (reader.Read())
                collection.Add(MapToGrade(reader));

            return collection;
        }
        protected SalaryPlanGrade MapToGrade(IDataReader reader)
        {
            SalaryPlanGrade item = new SalaryPlanGrade();
            base.MapToBase(item, reader);

            item.SalaryPlanGradeId = (long)CleanDataValue(reader[ColumnNames.SalaryPlanGradeId]);
            item.SalaryPlanId = (long)CleanDataValue(reader[ColumnNames.SalaryPlanId]);
            item.Name = (string)CleanDataValue(reader[ColumnNames.Name]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.EnglishDescription = (string)CleanDataValue(reader[ColumnNames.EnglishDescription]);
            item.FrenchDescription = (string)CleanDataValue(reader[ColumnNames.FrenchDescription]);

            return item;
        }


        protected SalaryPlanGradeOrgUnitDescriptionComboCollection MapToSalaryPlanGradeOrgUnitDescriptionComboCollection(IDataReader reader)
        {
            SalaryPlanGradeOrgUnitDescriptionComboCollection collection = new SalaryPlanGradeOrgUnitDescriptionComboCollection();

            while (reader.Read())
                collection.Add(MapToSalaryPlanGradeOrgUnitDescriptionCombo(reader));

            return collection;
        }
        protected SalaryPlanGradeOrgUnitDescriptionCombo MapToSalaryPlanGradeOrgUnitDescriptionCombo(IDataReader reader)
        {
            SalaryPlanGradeOrgUnitDescriptionCombo item = new SalaryPlanGradeOrgUnitDescriptionCombo();

            item.OrganizationUnitId = (long)CleanDataValue(reader[ColumnNames.OrganizationUnitId]);
            item.ImportExternalIdentifier = (string)CleanDataValue(reader[ColumnNames.ImportExternalIdentifier]);
            item.EnglishDescription = (string)CleanDataValue(reader[ColumnNames.EnglishDescription]);
            item.FrenchDescription = (string)CleanDataValue(reader[ColumnNames.FrenchDescription]);

            return item;
        }
        #endregion
    }
}