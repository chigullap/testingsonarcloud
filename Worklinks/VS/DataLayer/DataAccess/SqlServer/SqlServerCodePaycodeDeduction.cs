﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerCodePaycodeDeduction : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PaycodeDeductionId = "paycode_deduction_id";
            public static String PaycodeCode = "code_paycode_cd";
            public static String PaycodeRateBasedOnCode = "code_paycode_rate_based_on_cd";
            public static String BeforeIncomeTaxDeductionFlag = "before_income_tax_deduction_flag";
            public static String BeforeBonusTaxDeductionFlag = "before_bonus_tax_deduction_flag";
            public static String BeforeJamaicaTaxDeductionFlag = "before_jamaica_tax_deduction_flag";
            public static String BeforeRetroactiveTaxDeductionFlag = "before_retroactive_tax_deduction_flag";
        }
        #endregion

        #region select
        internal CodePaycodeDeductionCollection Select(DatabaseUser user, String paycodeCode)
        {
            DataBaseCommand command = GetStoredProcCommand("CodePaycodeDeduction_select", user.DatabaseName);

            command.AddParameterWithValue("@paycodeCode", paycodeCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCodeCollection(reader);
        }
        #endregion

        #region insert
        public CodePaycodeDeduction Insert(DatabaseUser user, CodePaycodeDeduction item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CodePaycodeDeduction_insert", user.DatabaseName))
            {
                command.BeginTransaction("CodePaycodeDeduction_insert");

                SqlParameter paycodeDeductionIdParm = command.AddParameterWithValue("@paycodeDeductionId", item.PaycodeDeductionId, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@paycodeCode", item.PaycodeCode);
                command.AddParameterWithValue("@paycodeRateBasedOnCode", item.PaycodeRateBasedOnCode);
                command.AddParameterWithValue("@beforeIncomeTaxDeductionFlag", item.BeforeIncomeTaxDeductionFlag);
                command.AddParameterWithValue("@beforeBonusTaxDeductionFlag", item.BeforeBonusTaxDeductionFlag);
                command.AddParameterWithValue("@beforeJamaicaTaxDeductionFlag", item.BeforeJamaicaTaxDeductionFlag);
                command.AddParameterWithValue("@beforeRetroactiveTaxDeductionFlag", item.BeforeRetroactiveTaxDeductionFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.PaycodeDeductionId = (long)paycodeDeductionIdParm.Value;
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, CodePaycodeDeduction item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CodePaycodeDeduction_update", user.DatabaseName))
                {
                    command.BeginTransaction("CodePaycodeDeduction_update");

                    command.AddParameterWithValue("@paycodeCode", item.PaycodeCode);
                    command.AddParameterWithValue("@paycodeRateBasedOnCode", item.PaycodeRateBasedOnCode);
                    command.AddParameterWithValue("@beforeIncomeTaxDeductionFlag", item.BeforeIncomeTaxDeductionFlag);
                    command.AddParameterWithValue("@beforeBonusTaxDeductionFlag", item.BeforeBonusTaxDeductionFlag);
                    command.AddParameterWithValue("@beforeJamaicaTaxDeductionFlag", item.BeforeJamaicaTaxDeductionFlag);
                    command.AddParameterWithValue("@beforeRetroactiveTaxDeductionFlag", item.BeforeRetroactiveTaxDeductionFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, CodePaycode item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CodePaycodeDeduction_delete", user.DatabaseName))
                {
                    command.BeginTransaction("CodePaycodeDeduction_delete");

                    command.AddParameterWithValue("@paycodeCode", item.PaycodeCode);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected CodePaycodeDeductionCollection MapToCodeCollection(IDataReader reader)
        {
            CodePaycodeDeductionCollection collection = new CodePaycodeDeductionCollection();

            while (reader.Read())
                collection.Add(MapToCode(reader));

            return collection;
        }
        protected CodePaycodeDeduction MapToCode(IDataReader reader)
        {
            CodePaycodeDeduction code = new CodePaycodeDeduction();
            base.MapToBase(code, reader);

            code.PaycodeDeductionId = (long)CleanDataValue(reader[ColumnNames.PaycodeDeductionId]);
            code.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            code.PaycodeRateBasedOnCode = (String)CleanDataValue(reader[ColumnNames.PaycodeRateBasedOnCode]);
            code.BeforeIncomeTaxDeductionFlag = (bool)CleanDataValue(reader[ColumnNames.BeforeIncomeTaxDeductionFlag]);
            code.BeforeBonusTaxDeductionFlag = (bool)CleanDataValue(reader[ColumnNames.BeforeBonusTaxDeductionFlag]);
            code.BeforeJamaicaTaxDeductionFlag = (bool)CleanDataValue(reader[ColumnNames.BeforeJamaicaTaxDeductionFlag]);
            code.BeforeRetroactiveTaxDeductionFlag = (bool)CleanDataValue(reader[ColumnNames.BeforeRetroactiveTaxDeductionFlag]);

            return code;
        }
        #endregion 
    }
}