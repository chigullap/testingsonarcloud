﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerChequeWcbRemittance : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String CodeWcbCd = "code_wsib_cd";
            public static String RemittanceDate = "remittance_date";
            public static String PaymentAmount = "payment_amount";
            public static String AssessableEarnings = "assessable_earnings";
            public static String VendorName = "name";
            public static String WorkersCompensationAccountNumber = "workers_compensation_account_number";
            public static String AddressLine1 = "address_line_1";
            public static String AddressLine2 = "address_line_2";
            public static String City = "city";
            public static String CodeProvinceStateCd = "code_province_state_cd";
            public static String PostalZipCode = "postal_zip_code";
            public static String CodeCountryCd = "code_country_cd";
            public static String BusinessTaxNumber = "business_tax_number";
            public static String CodePreAuthorizedBankCd = "code_pre_authorized_bank_cd";
            public static String PreAuthorizedTransitNumber = "pre_authorized_transit_number";
            public static String PreAuthorizedAccountNumber = "pre_authorized_account_number";
            public static String PaymentDueDate = "payment_due_date";
            public static String ChequeWcbPreAuthorizedDebitExportId = "cheque_wcb_pre_authorized_debit_export_id";

            //used with GetChequeWcbRemittanceSchedule method
            public static String ReportingPeriodStartDate = "reporting_period_start_date";
            public static String ReportingPeriodEndDate = "reporting_period_end_date";
            public static String CodeWcbChequeRemittanceFrequencyCd = "code_wcb_cheque_remittance_frequency_cd";
        }
        #endregion

        #region main

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ChequeWcbRemittance_Insert", user.DatabaseName))
            {
                command.BeginTransaction("ChequeWcbRemittance_Insert");

                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        public ChequeWcbRemittanceSummaryCollection GetRbcChequeWcbRemittanceFileData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ChequeWcbRemittanceSchedule remit, bool estimatesOnly)
        {
            DataBaseCommand command = GetStoredProcCommand("ChequeWcbGetRemittanceSummary_select", user.DatabaseName);

            command.AddParameterWithValue("@remittanceDate", remit.RemittanceDate);
            command.AddParameterWithValue("@remittancePeriodEndDate", remit.ReportingPeriodEndDate);
            command.AddParameterWithValue("@codeWcbCode", remit.CodeWcbCd);
            command.AddParameterWithValue("@estimatesOnly", estimatesOnly); //used only in creating pre-authorized debit file for wcb estimates to only grab the estimate amounts

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToChequeWcbRemittanceSummaryCollection(reader);
            }
        }

        public ChequeWcbRemittanceScheduleCollection GetChequeWcbRemittanceSchedule(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, DateTime date, bool queryForInvoice)
        {
            DataBaseCommand command = GetStoredProcCommand("ChequeWcbGetSchedule_select", user.DatabaseName);

            command.AddParameterWithValue("@date", date);
            command.AddParameterWithValue("@queryForInvoice", queryForInvoice);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToChequeWcbRemittanceScheduleCollection(reader);
            }
        }
        
        public void UpdateChequeWcbRemittanceChequeWcbExportIdColumn(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, DateTime date, long chequeWcbExportId, string codeWcbCd)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ChequeWcbRemittance_updateChequeWcbExportId", user.DatabaseName))
            {
                command.BeginTransaction("ChequeWcbRemittance_updateChequeWcbExportId");

                command.AddParameterWithValue("@date", date);
                command.AddParameterWithValue("@chequeWcbExportId", chequeWcbExportId);
                command.AddParameterWithValue("@codeWcbCd", codeWcbCd);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        #endregion

        #region data mapping
        protected ChequeWcbRemittanceSummaryCollection MapToChequeWcbRemittanceSummaryCollection(IDataReader reader)
        {
            ChequeWcbRemittanceSummaryCollection collection = new ChequeWcbRemittanceSummaryCollection();
            while (reader.Read())
            {
                collection.Add(MapToChequeWcbRemittanceSummary(reader));
            }

            return collection;
        }

        protected ChequeWcbRemittanceSummary MapToChequeWcbRemittanceSummary(IDataReader reader)
        {
            ChequeWcbRemittanceSummary item = new ChequeWcbRemittanceSummary();

            item.CodeWcbCd = (String)CleanDataValue(reader[ColumnNames.CodeWcbCd]);
            item.PaymentAmount = (decimal)CleanDataValue(reader[ColumnNames.PaymentAmount]);
            item.AssessableEarnings = (decimal)CleanDataValue(reader[ColumnNames.AssessableEarnings]);
            item.RemittanceDate = (DateTime)CleanDataValue(reader[ColumnNames.RemittanceDate]);
            item.VendorName = (string)CleanDataValue(reader[ColumnNames.VendorName]);
            item.WorkersCompensationAccountNumber = (string)CleanDataValue(reader[ColumnNames.WorkersCompensationAccountNumber]);
            item.AddressLine1 = (string)CleanDataValue(reader[ColumnNames.AddressLine1]);
            item.AddressLine2 = (string)CleanDataValue(reader[ColumnNames.AddressLine2]);
            item.City = (string)CleanDataValue(reader[ColumnNames.City]);
            item.CodeProvinceStateCd = (string)CleanDataValue(reader[ColumnNames.CodeProvinceStateCd]);
            item.PostalZipCode = (string)CleanDataValue(reader[ColumnNames.PostalZipCode]);
            item.CodeCountryCd = (string)CleanDataValue(reader[ColumnNames.CodeCountryCd]);
            item.BusinessTaxNumber = (string)CleanDataValue(reader[ColumnNames.BusinessTaxNumber]);
            item.CodePreAuthorizedBankCd = (string)CleanDataValue(reader[ColumnNames.CodePreAuthorizedBankCd]);
            item.PreAuthorizedTransitNumber= (string)CleanDataValue(reader[ColumnNames.PreAuthorizedTransitNumber]);
            item.PreAuthorizedAccountNumber= (string)CleanDataValue(reader[ColumnNames.PreAuthorizedAccountNumber]);
            item.PaymentDueDate = (DateTime?)CleanDataValue(reader[ColumnNames.PaymentDueDate]);
            item.ChequeWcbPreAuthorizedDebitExportId = (long?)CleanDataValue(reader[ColumnNames.ChequeWcbPreAuthorizedDebitExportId]);

            return item;
        }

        protected ChequeWcbRemittanceScheduleCollection MapToChequeWcbRemittanceScheduleCollection(IDataReader reader)
        {
            ChequeWcbRemittanceScheduleCollection collection = new ChequeWcbRemittanceScheduleCollection();
            while (reader.Read())
            {
                collection.Add(MapToChequeWcbRemittanceSchedule(reader));
            }

            return collection;
        }

        protected ChequeWcbRemittanceSchedule MapToChequeWcbRemittanceSchedule(IDataReader reader)
        {
            ChequeWcbRemittanceSchedule item = new ChequeWcbRemittanceSchedule();

            item.CodeWcbCd = (String)CleanDataValue(reader[ColumnNames.CodeWcbCd]);
            item.RemittanceDate = (DateTime)CleanDataValue(reader[ColumnNames.RemittanceDate]);
            item.ReportingPeriodStartDate = (DateTime)CleanDataValue(reader[ColumnNames.ReportingPeriodStartDate]);
            item.ReportingPeriodEndDate = (DateTime)CleanDataValue(reader[ColumnNames.ReportingPeriodEndDate]);
            item.CodeWcbChequeRemittanceFrequencyCd = (String)CleanDataValue(reader[ColumnNames.CodeWcbChequeRemittanceFrequencyCd]);

            return item;
        }
        #endregion
    }
}
