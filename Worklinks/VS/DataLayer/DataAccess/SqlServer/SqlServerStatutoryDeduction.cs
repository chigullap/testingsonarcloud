﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerStatutoryDeduction : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string StatutoryDeductionId = "statutory_deduction_id";
            public static string EmployeeId = "employee_id";
            public static string StatutoryDeductionTypeCode = "code_statutory_deduction_type_cd";
            public static string ProvinceStateCode = "code_province_state_cd";
            public static string BusinessNumberId = "business_number_id";
            public static string ActiveFlag = "active_flag";
            public static string PayEmploymentInsuranceFlag = "pay_employment_insurance_flag";
            public static string PayCanadaPensionPlanFlag = "pay_canada_pension_plan_flag";
            public static string ParentalInsurancePlanFlag = "parental_insurance_plan_flag";
            public static string HealthTaxId = "health_tax_id";
            public static string FederalTaxClaim = "federal_tax_claim";
            public static string FederalAdditionalTax = "federal_additional_tax";
            public static string FederalDesignatedAreaDeduction = "federal_designated_area_deduction";
            public static string FederalAuthorizedAnnualDeduction = "federal_authorized_annual_deduction";
            public static string FederalAuthorizedAnnualTaxCredit = "federal_authorized_annual_tax_credit";
            public static string FederalLabourTaxCredit = "federal_labour_tax_credit";
            public static string FederalPayTaxFlag = "federal_pay_tax_flag";
            public static string ProvincialTaxClaim = "provincial_tax_claim";
            public static string ProvincialAdditionalTax = "provincial_additional_tax";
            public static string ProvincialDesignatedAreaDeduction = "provincial_designated_area_deduction";
            public static string ProvincialAuthorizedAnnualDeduction = "provincial_authorized_annual_deduction";
            public static string ProvincialAuthorizedAnnualTaxCredit = "provincial_authorized_annual_tax_credit";
            public static string ProvincialLabourTaxCredit = "provincial_labour_tax_credit";
            public static string ProvincialPayTaxFlag = "provincial_pay_tax_flag";
            public static string EstimatedAnnualIncome = "estimated_annual_income";
            public static string EstimatedAnnualExpense = "estimated_annual_expense";
            public static string EstimatedNetIncome = "estimated_net_income";
            public static string CommissionPercentage = "commission_percentage";
            public static string OverridePercentage = "override_percentage";
            public static string PayNationalInsuranceServiceFlag = "pay_national_insurance_service_flag";
            public static string TotalExemption = "total_exemption";
            public static string PayNationalInsuranceCorporationFlag = "pay_national_insurance_corporation_flag";
            public static string TDAU1Amount = "td_au_1_amount";
            public static string PayNationalInsuranceBoardFlag = "pay_national_insurance_board_flag";
            public static string TD1Amount = "td_1_amount";
            public static string PayNationalInsuranceSchemeFlag = "pay_national_insurance_scheme_flag";
            public static string PayNationalHealthTaxFlag = "pay_national_health_tax_flag";
            public static string PayEducationTaxFlag = "pay_education_tax_flag";
            public static string IsWeek1Month1EmployeeForCurrentYearFlag = "is_week_1_month_1_employee_for_current_year_flag";
            public static string WeekNumber = "week_number";
            public static string MonthNumber = "month_number";
            public static string TotalGrossPayToDate = "total_gross_pay_to_date";
            public static string Prerequisites = "prerequisites";
            public static string NiToDate = "ni_to_date";
            public static string GrossPayLessDeductions = "gross_pay_less_deductions";
            public static string Superannuation = "superannuation";
            public static string ApprovedExp = "approved_exp";
            public static string TotalTaxToDateDeterminedByTaxTable = "total_tax_to_date_determined_by_tax_table";
        }
        #endregion

        #region select
        internal StatutoryDeductionCollection Select(DatabaseUser user, long employeeId, bool securityOverrideFlag)
        {
            using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeduction_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@employeeId", employeeId);
                command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
                command.AddParameterWithValue("@securityUserId", user.SecurityUserId);
                command.AddParameterWithValue("@securityOverride", securityOverrideFlag);

                StatutoryDeductionCollection statutoryDeductions = null;

                using (IDataReader reader = command.ExecuteReader())
                    statutoryDeductions = MapToCollection(reader);

                return statutoryDeductions;
            }
        }
        #endregion

        #region insert
        public void Insert(DatabaseUser user, StatutoryDeduction item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeduction_insert", user.DatabaseName))
            {
                command.BeginTransaction("StatutoryDeduction_insert");

                SqlParameter itemIdParm = command.AddParameterWithValue("@statutoryDeductionId", item.StatutoryDeductionId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", item.EmployeeId);
                command.AddParameterWithValue("@statutoryDeductionTypeCode", item.StatutoryDeductionTypeCode);
                command.AddParameterWithValue("@provinceStateCode", item.ProvinceStateCode);
                command.AddParameterWithValue("@businessNumberId", item.BusinessNumberId);
                command.AddParameterWithValue("@activeFlag", item.ActiveFlag);
                command.AddParameterWithValue("@payEmploymentInsuranceFlag", item.PayEmploymentInsuranceFlag);
                command.AddParameterWithValue("@payCanadaPensionPlanFlag", item.PayCanadaPensionPlanFlag);
                command.AddParameterWithValue("@parentalInsurancePlanFlag", item.ParentalInsurancePlanFlag);
                command.AddParameterWithValue("@healthTaxId", item.HealthTaxId);
                command.AddParameterWithValue("@federalTaxClaim", item.FederalTaxClaim);
                command.AddParameterWithValue("@federalAdditionalTax", item.FederalAdditionalTax);
                command.AddParameterWithValue("@federalDesignatedAreaDeduction", item.FederalDesignatedAreaDeduction);
                command.AddParameterWithValue("@federalAuthorizedAnnualDeduction", item.FederalAuthorizedAnnualDeduction);
                command.AddParameterWithValue("@federalAuthorizedAnnualTaxCredit", item.FederalAuthorizedAnnualTaxCredit);
                command.AddParameterWithValue("@federalLabourTaxCredit", item.FederalLabourTaxCredit);
                command.AddParameterWithValue("@federalPayTaxFlag", item.FederalPayTaxFlag);
                command.AddParameterWithValue("@provincialTaxClaim", item.ProvincialTaxClaim);
                command.AddParameterWithValue("@provincialAdditionalTax", item.ProvincialAdditionalTax);
                command.AddParameterWithValue("@provincialDesignatedAreaDeduction", item.ProvincialDesignatedAreaDeduction);
                command.AddParameterWithValue("@provincialAuthorizedAnnualDeduction", item.ProvincialAuthorizedAnnualDeduction);
                command.AddParameterWithValue("@provincialAuthorizedAnnualTaxCredit", item.ProvincialAuthorizedAnnualTaxCredit);
                command.AddParameterWithValue("@provincialLabourTaxCredit", item.ProvincialLabourTaxCredit);
                command.AddParameterWithValue("@provincialPayTaxFlag", item.ProvincialPayTaxFlag);
                command.AddParameterWithValue("@estimatedAnnualIncome", item.EstimatedAnnualIncome);
                command.AddParameterWithValue("@estimatedAnnualExpense", item.EstimatedAnnualExpense);
                command.AddParameterWithValue("@estimatedNetIncome", item.EstimatedNetIncome);
                command.AddParameterWithValue("@commissionPercentage", item.CommissionPercentage);
                command.AddParameterWithValue("@overridePercentage", item.OverridePercentage);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.StatutoryDeductionId = Convert.ToInt64(itemIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }
        public void InsertBarbados(DatabaseUser user, StatutoryDeduction item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeductionBarbados_insert", user.DatabaseName))
            {
                command.BeginTransaction("StatutoryDeductionBarbados_insert");

                command.AddParameterWithValue("@statutoryDeductionId", item.StatutoryDeductionId);
                command.AddParameterWithValue("@payNationalInsuranceServiceFlag", item.PayNationalInsuranceServiceFlag);
                command.AddParameterWithValue("@totalExemption", item.TotalExemption);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }
        public void InsertSaintLucia(DatabaseUser user, StatutoryDeduction item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeductionSaintLucia_insert", user.DatabaseName))
            {
                command.BeginTransaction("StatutoryDeductionSaintLucia_insert");

                command.AddParameterWithValue("@statutoryDeductionId", item.StatutoryDeductionId);
                command.AddParameterWithValue("@payNationalInsuranceCorporationFlag", item.PayNationalInsuranceCorporationFlag);
                command.AddParameterWithValue("@tdAu1Amount", item.TDAU1Amount);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }
        public void InsertTrinidad(DatabaseUser user, StatutoryDeduction item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeductionTrinidad_insert", user.DatabaseName))
            {
                command.BeginTransaction("StatutoryDeductionTrinidad_insert");

                command.AddParameterWithValue("@statutoryDeductionId", item.StatutoryDeductionId);
                command.AddParameterWithValue("@payNationalInsuranceBoardFlag", item.PayNationalInsuranceBoardFlag);
                command.AddParameterWithValue("@td1Amount", item.TD1Amount);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }
        public void InsertJamaica(DatabaseUser user, StatutoryDeduction item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeductionJamaica_insert", user.DatabaseName))
            {
                command.BeginTransaction("StatutoryDeductionJamaica_insert");

                command.AddParameterWithValue("@statutoryDeductionId", item.StatutoryDeductionId);
                command.AddParameterWithValue("@payNationalInsuranceSchemeFlag", item.PayNationalInsuranceSchemeFlag);
                command.AddParameterWithValue("@payNationalHealthTaxFlag", item.PayNationalHealthTaxFlag);
                command.AddParameterWithValue("@payEducationTaxFlag", item.PayEducationTaxFlag);
                command.AddParameterWithValue("@isWeek1Month1EmployeeForCurrentYearFlag", item.IsWeek1Month1EmployeeForCurrentYearFlag);
                command.AddParameterWithValue("@weekNumber", item.WeekNumber);
                command.AddParameterWithValue("@monthNumber", item.MonthNumber);
                command.AddParameterWithValue("@totalGrossPayToDate", item.TotalGrossPayToDate);
                command.AddParameterWithValue("@prerequisites", item.Prerequisites);
                command.AddParameterWithValue("@niToDate", item.NiToDate);
                command.AddParameterWithValue("@grossPayLessDeductions", item.GrossPayLessDeductions);
                command.AddParameterWithValue("@superannuation", item.Superannuation);
                command.AddParameterWithValue("@approvedExp", item.ApprovedExp);
                command.AddParameterWithValue("@totalTaxToDateDeterminedByTaxTable", item.TotalTaxToDateDeterminedByTaxTable);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeduction_update", user.DatabaseName))
                {
                    command.BeginTransaction("StatutoryDeduction_update");

                    command.AddParameterWithValue("@statutoryDeductionId", item.StatutoryDeductionId);
                    command.AddParameterWithValue("@employeeId", item.EmployeeId);
                    command.AddParameterWithValue("@statutoryDeductionTypeCode", item.StatutoryDeductionTypeCode);
                    command.AddParameterWithValue("@provinceStateCode", item.ProvinceStateCode);
                    command.AddParameterWithValue("@businessNumberId", item.BusinessNumberId);
                    command.AddParameterWithValue("@activeFlag", item.ActiveFlag);
                    command.AddParameterWithValue("@payEmploymentInsuranceFlag", item.PayEmploymentInsuranceFlag);
                    command.AddParameterWithValue("@payCanadaPensionPlanFlag", item.PayCanadaPensionPlanFlag);
                    command.AddParameterWithValue("@parentalInsurancePlanFlag", item.ParentalInsurancePlanFlag);
                    command.AddParameterWithValue("@healthTaxId", item.HealthTaxId);
                    command.AddParameterWithValue("@federalTaxClaim", item.FederalTaxClaim);
                    command.AddParameterWithValue("@federalAdditionalTax", item.FederalAdditionalTax);
                    command.AddParameterWithValue("@federalDesignatedAreaDeduction", item.FederalDesignatedAreaDeduction);
                    command.AddParameterWithValue("@federalAuthorizedAnnualDeduction", item.FederalAuthorizedAnnualDeduction);
                    command.AddParameterWithValue("@federalAuthorizedAnnualTaxCredit", item.FederalAuthorizedAnnualTaxCredit);
                    command.AddParameterWithValue("@federalLabourTaxCredit", item.FederalLabourTaxCredit);
                    command.AddParameterWithValue("@federalPayTaxFlag", item.FederalPayTaxFlag);
                    command.AddParameterWithValue("@provincialTaxClaim", item.ProvincialTaxClaim);
                    command.AddParameterWithValue("@provincialAdditionalTax", item.ProvincialAdditionalTax);
                    command.AddParameterWithValue("@provincialDesignatedAreaDeduction", item.ProvincialDesignatedAreaDeduction);
                    command.AddParameterWithValue("@provincialAuthorizedAnnualDeduction", item.ProvincialAuthorizedAnnualDeduction);
                    command.AddParameterWithValue("@provincialAuthorizedAnnualTaxCredit", item.ProvincialAuthorizedAnnualTaxCredit);
                    command.AddParameterWithValue("@provincialLabourTaxCredit", item.ProvincialLabourTaxCredit);
                    command.AddParameterWithValue("@provincialPayTaxFlag", item.ProvincialPayTaxFlag);
                    command.AddParameterWithValue("@estimatedAnnualIncome", item.EstimatedAnnualIncome);
                    command.AddParameterWithValue("@estimatedAnnualExpense", item.EstimatedAnnualExpense);
                    command.AddParameterWithValue("@estimatedNetIncome", item.EstimatedNetIncome);
                    command.AddParameterWithValue("@commissionPercentage", item.CommissionPercentage);
                    command.AddParameterWithValue("@overridePercentage", item.OverridePercentage);
                    command.AddParameterWithValue("@overrideConcurrencyCheckFlag", item.OverrideConcurrencyCheckFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void UpdateBarbados(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeductionBarbados_update", user.DatabaseName))
                {
                    command.BeginTransaction("StatutoryDeductionBarbados_update");

                    command.AddParameterWithValue("@statutoryDeductionId", item.StatutoryDeductionId);
                    command.AddParameterWithValue("@payNationalInsuranceServiceFlag", item.PayNationalInsuranceServiceFlag);
                    command.AddParameterWithValue("@totalExemption", item.TotalExemption);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void UpdateSaintLucia(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeductionSaintLucia_update", user.DatabaseName))
                {
                    command.BeginTransaction("StatutoryDeductionSaintLucia_update");

                    command.AddParameterWithValue("@statutoryDeductionId", item.StatutoryDeductionId);
                    command.AddParameterWithValue("@payNationalInsuranceCorporationFlag", item.PayNationalInsuranceCorporationFlag);
                    command.AddParameterWithValue("@tdAu1Amount", item.TDAU1Amount);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void UpdateTrinidad(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeductionTrinidad_update", user.DatabaseName))
                {
                    command.BeginTransaction("StatutoryDeductionTrinidad_update");

                    command.AddParameterWithValue("@statutoryDeductionId", item.StatutoryDeductionId);
                    command.AddParameterWithValue("@payNationalInsuranceBoardFlag", item.PayNationalInsuranceBoardFlag);
                    command.AddParameterWithValue("@td1Amount", item.TD1Amount);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void UpdateJamaica(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeductionJamaica_update", user.DatabaseName))
                {
                    command.BeginTransaction("StatutoryDeductionJamaica_update");

                    command.AddParameterWithValue("@statutoryDeductionId", item.StatutoryDeductionId);
                    command.AddParameterWithValue("@payNationalInsuranceSchemeFlag", item.PayNationalInsuranceSchemeFlag);
                    command.AddParameterWithValue("@payNationalHealthTaxFlag", item.PayNationalHealthTaxFlag);
                    command.AddParameterWithValue("@payEducationTaxFlag", item.PayEducationTaxFlag);
                    command.AddParameterWithValue("@isWeek1Month1EmployeeForCurrentYearFlag", item.IsWeek1Month1EmployeeForCurrentYearFlag);
                    command.AddParameterWithValue("@weekNumber", item.WeekNumber);
                    command.AddParameterWithValue("@monthNumber", item.MonthNumber);
                    command.AddParameterWithValue("@totalGrossPayToDate", item.TotalGrossPayToDate);
                    command.AddParameterWithValue("@prerequisites", item.Prerequisites);
                    command.AddParameterWithValue("@niToDate", item.NiToDate);
                    command.AddParameterWithValue("@grossPayLessDeductions", item.GrossPayLessDeductions);
                    command.AddParameterWithValue("@superannuation", item.Superannuation);
                    command.AddParameterWithValue("@approvedExp", item.ApprovedExp);
                    command.AddParameterWithValue("@totalTaxToDateDeterminedByTaxTable", item.TotalTaxToDateDeterminedByTaxTable);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeduction_delete", user.DatabaseName))
                {
                    command.BeginTransaction("StatutoryDeduction_delete");

                    command.AddParameterWithValue("@statutoryDeductionId", item.StatutoryDeductionId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void DeleteBarbados(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeductionBarbados_delete", user.DatabaseName))
                {
                    command.BeginTransaction("StatutoryDeductionBarbados_delete");

                    command.AddParameterWithValue("@statutoryDeductionId", item.StatutoryDeductionId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void DeleteSaintLucia(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeductionSaintLucia_delete", user.DatabaseName))
                {
                    command.BeginTransaction("StatutoryDeductionSaintLucia_delete");

                    command.AddParameterWithValue("@statutoryDeductionId", item.StatutoryDeductionId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void DeleteTrinidad(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeductionTrinidad_delete", user.DatabaseName))
                {
                    command.BeginTransaction("StatutoryDeductionTrinidad_delete");

                    command.AddParameterWithValue("@statutoryDeductionId", item.StatutoryDeductionId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void DeleteJamaica(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeductionJamaica_delete", user.DatabaseName))
                {
                    command.BeginTransaction("StatutoryDeductionJamaica_delete");

                    command.AddParameterWithValue("@statutoryDeductionId", item.StatutoryDeductionId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region import
        public void ImportStatDeductionViaService(String database, String username, List<StatDeductionImport> statDed)
        {
            DataBaseCommand command = GetStoredProcCommand("StatutoryDeduction_import", database);

            using (command)
            {
                command.BeginTransaction("StatutoryDeduction_import");

                //table parameter
                SqlParameter parmStatutoryDeduction = new SqlParameter();
                parmStatutoryDeduction.ParameterName = "@parmStatutoryDeduction";
                parmStatutoryDeduction.SqlDbType = SqlDbType.Structured;

                DataTable tableStatutoryDeduction = LoadStatutoryDeductionDataTable(statDed);
                parmStatutoryDeduction.Value = tableStatutoryDeduction;
                command.AddParameter(parmStatutoryDeduction);

                command.AddParameterWithValue("@updateUser", username);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        private DataTable LoadStatutoryDeductionDataTable(List<StatDeductionImport> statDed)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_identifier", typeof(String));
            table.Columns.Add("employer_number", typeof(String));
            table.Columns.Add("code_statutory_deduction_type_cd", typeof(String));
            table.Columns.Add("code_province_state_cd", typeof(String));
            table.Columns.Add("pay_employment_insurance_flag", typeof(bool));
            table.Columns.Add("pay_canada_pension_plan_flag", typeof(bool));
            table.Columns.Add("parental_insurance_plan_flag", typeof(bool));
            table.Columns.Add("federal_tax_claim", typeof(Decimal));
            table.Columns.Add("federal_additional_tax", typeof(Decimal));
            table.Columns.Add("federal_designated_area_deduction", typeof(Decimal));
            table.Columns.Add("federal_authorized_annual_deduction", typeof(Decimal));
            table.Columns.Add("federal_authorized_annual_tax_credit", typeof(Decimal));
            table.Columns.Add("federal_labour_tax_credit", typeof(Decimal));
            table.Columns.Add("federal_pay_tax_flag", typeof(bool));
            table.Columns.Add("provincial_tax_claim", typeof(Decimal));
            table.Columns.Add("provincial_additional_tax", typeof(Decimal));
            table.Columns.Add("provincial_designated_area_deduction", typeof(Decimal));
            table.Columns.Add("provincial_authorized_annual_deduction", typeof(Decimal));
            table.Columns.Add("provincial_authorized_annual_tax_credit", typeof(Decimal));
            table.Columns.Add("provincial_labour_tax_credit", typeof(Decimal));
            table.Columns.Add("provincial_pay_tax_flag", typeof(bool));
            table.Columns.Add("estimated_annual_income", typeof(Decimal));
            table.Columns.Add("estimated_annual_expense", typeof(Decimal));
            table.Columns.Add("estimated_net_income", typeof(Decimal));
            table.Columns.Add("commission_percentage", typeof(Decimal));

            foreach (StatDeductionImport item in statDed)
            {
                table.Rows.Add(new Object[]
                {
                    item.EmployeeIdentifier,
                    item.EmployerNumber,
                    "CAN", //hardcode for now - code_statutory_deduction_type_cd will be added to the import file later
                    item.CodeProvinceStateCd,
                    item.PayEmploymentInsuranceFlag,
                    item.PayCanadaPensionPlanFlag,
                    item.ParentalInsurancePlanFlag,
                    item.FederalTaxClaim,
                    item.FederalAdditionalTax,
                    item.FederalDesignatedAreaDeduction,
                    item.FederalAuthorizedAnnualDeduction,
                    item.FederalAuthorizedAnnualTaxCredit,
                    item.FederalLabourTaxCredit,
                    item.FederalPayTaxFlag,
                    item.ProvincialTaxClaim,
                    item.ProvincialAdditionalTax,
                    item.ProvincialDesignatedAreaDeduction,
                    item.ProvincialAuthorizedAnnualDeduction,
                    item.ProvincialAuthorizedAnnualTaxCredit,
                    item.ProvincialLabourTaxCredit,
                    item.ProvincialPayTaxFlag,
                    item.EstimatedAnnualIncome,
                    item.EstimatedAnnualExpense,
                    item.EstimatedNetIncome,
                    item.CommissionPercentage
                });
            }

            return table;
        }
        public void ImportStatDeduction(DatabaseUser user, WorklinksStatutoryDeductionCollection items)
        {
            DataBaseCommand command = GetStoredProcCommand("StatutoryDeduction_import", user.DatabaseName);

            using (command)
            {
                command.BeginTransaction("StatutoryDeduction_import");

                //table parameter
                SqlParameter parmStatutoryDeduction = new SqlParameter();
                parmStatutoryDeduction.ParameterName = "@parmStatutoryDeduction";
                parmStatutoryDeduction.SqlDbType = SqlDbType.Structured;

                DataTable tableStatutoryDeduction = LoadStatutoryDeductionDataTable(items);
                parmStatutoryDeduction.Value = tableStatutoryDeduction;
                command.AddParameter(parmStatutoryDeduction);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        private DataTable LoadStatutoryDeductionDataTable(WorklinksStatutoryDeductionCollection items)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_identifier", typeof(String));
            table.Columns.Add("employer_number", typeof(String));
            table.Columns.Add("code_statutory_deduction_type_cd", typeof(String));
            table.Columns.Add("code_province_state_cd", typeof(String));
            table.Columns.Add("pay_employment_insurance_flag", typeof(bool));
            table.Columns.Add("pay_canada_pension_plan_flag", typeof(bool));
            table.Columns.Add("parental_insurance_plan_flag", typeof(bool));
            table.Columns.Add("federal_tax_claim", typeof(Decimal));
            table.Columns.Add("federal_additional_tax", typeof(Decimal));
            table.Columns.Add("federal_designated_area_deduction", typeof(Decimal));
            table.Columns.Add("federal_authorized_annual_deduction", typeof(Decimal));
            table.Columns.Add("federal_authorized_annual_tax_credit", typeof(Decimal));
            table.Columns.Add("federal_labour_tax_credit", typeof(Decimal));
            table.Columns.Add("federal_pay_tax_flag", typeof(bool));
            table.Columns.Add("provincial_tax_claim", typeof(Decimal));
            table.Columns.Add("provincial_additional_tax", typeof(Decimal));
            table.Columns.Add("provincial_designated_area_deduction", typeof(Decimal));
            table.Columns.Add("provincial_authorized_annual_deduction", typeof(Decimal));
            table.Columns.Add("provincial_authorized_annual_tax_credit", typeof(Decimal));
            table.Columns.Add("provincial_labour_tax_credit", typeof(Decimal));
            table.Columns.Add("provincial_pay_tax_flag", typeof(bool));
            table.Columns.Add("estimated_annual_income", typeof(Decimal));
            table.Columns.Add("estimated_annual_expense", typeof(Decimal));
            table.Columns.Add("estimated_net_income", typeof(Decimal));
            table.Columns.Add("commission_percentage", typeof(Decimal));
            table.Columns.Add("payNationalInsuranceSchemeFlag", typeof(bool));
            table.Columns.Add("payNationalHealthTaxFlag", typeof(bool));
            table.Columns.Add("payEducationTaxFlag", typeof(bool));
            table.Columns.Add("isWeek1Month1EmployeeForCurrentYearFlag", typeof(bool));
            table.Columns.Add("weekNumber", typeof(int));
            table.Columns.Add("monthNumber", typeof(int));
            table.Columns.Add("prerequisites", typeof(decimal));
            table.Columns.Add("niToDate", typeof(decimal));
            table.Columns.Add("totalGrossPayToDate", typeof(decimal));
            table.Columns.Add("grossPayLessDeductions", typeof(decimal));
            table.Columns.Add("superannuation", typeof(decimal));
            table.Columns.Add("approvedExp", typeof(decimal));
            table.Columns.Add("totalTaxToDateDeterminedByTaxTable", typeof(decimal));

            foreach (WorklinksStatutoryDeduction item in items)
            {
                table.Rows.Add(new Object[]
                {
                    item.ImportExternalIdentifier,
                    item.EmployerExternalIdentifier,
                    item.StatutoryDeductionTypeCode,
                    item.ProvinceStateCode,
                    item.PayEmploymentInsuranceFlag,
                    item.PayCanadaPensionPlanFlag,
                    item.ParentalInsurancePlanFlag,
                    item.FederalTaxClaim,
                    item.FederalAdditionalTax,
                    item.FederalDesignatedAreaDeduction,
                    item.FederalAuthorizedAnnualDeduction,
                    item.FederalAuthorizedAnnualTaxCredit,
                    item.FederalLabourTaxCredit,
                    item.FederalPayTaxFlag,
                    item.ProvincialTaxClaim,
                    item.ProvincialAdditionalTax,
                    item.ProvincialDesignatedAreaDeduction,
                    item.ProvincialAuthorizedAnnualDeduction,
                    item.ProvincialAuthorizedAnnualTaxCredit,
                    item.ProvincialLabourTaxCredit,
                    item.ProvincialPayTaxFlag,
                    item.EstimatedAnnualIncome,
                    item.EstimatedAnnualExpense,
                    item.EstimatedNetIncome,
                    item.CommissionPercentage,
                    item.PayNationalInsuranceSchemeFlag,
                    item.PayNationalHealthTaxFlag,
                    item.PayEducationTaxFlag,
                    item.IsWeek1Month1EmployeeForCurrentYearFlag,
                    item.WeekNumber,
                    item.MonthNumber,
                    item.Prerequisites,
                    item.NiToDate,
                    item.TotalGrossPayToDate,
                    item.GrossPayLessDeductions,
                    item.Superannuation,
                    item.ApprovedExp,
                    item.TotalTaxToDateDeterminedByTaxTable
                });
            }

            return table;
        }
        #endregion

        #region data mapping
        protected StatutoryDeductionCollection MapToCollection(IDataReader reader)
        {
            StatutoryDeductionCollection collection = new StatutoryDeductionCollection();

            while (reader.Read())
                collection.Add(MapToItem(reader));

            return collection;
        }
        protected StatutoryDeduction MapToItem(IDataReader reader)
        {
            StatutoryDeduction item = new StatutoryDeduction();
            base.MapToBase(item, reader);

            item.StatutoryDeductionId = (long)CleanDataValue(reader[ColumnNames.StatutoryDeductionId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.StatutoryDeductionTypeCode = (string)CleanDataValue(reader[ColumnNames.StatutoryDeductionTypeCode]);
            item.ProvinceStateCode = (string)CleanDataValue(reader[ColumnNames.ProvinceStateCode]);
            item.BusinessNumberId = (long?)CleanDataValue(reader[ColumnNames.BusinessNumberId]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.PayEmploymentInsuranceFlag = (bool)CleanDataValue(reader[ColumnNames.PayEmploymentInsuranceFlag]);
            item.PayCanadaPensionPlanFlag = (bool)CleanDataValue(reader[ColumnNames.PayCanadaPensionPlanFlag]);
            item.ParentalInsurancePlanFlag = (bool)CleanDataValue(reader[ColumnNames.ParentalInsurancePlanFlag]);
            item.HealthTaxId = (long?)CleanDataValue(reader[ColumnNames.HealthTaxId]);
            item.FederalTaxClaim = (int?)CleanDataValue(reader[ColumnNames.FederalTaxClaim]);
            item.FederalAdditionalTax = (decimal?)CleanDataValue(reader[ColumnNames.FederalAdditionalTax]);
            item.FederalDesignatedAreaDeduction = (int?)CleanDataValue(reader[ColumnNames.FederalDesignatedAreaDeduction]);
            item.FederalAuthorizedAnnualDeduction = (int?)CleanDataValue(reader[ColumnNames.FederalAuthorizedAnnualDeduction]);
            item.FederalAuthorizedAnnualTaxCredit = (int?)CleanDataValue(reader[ColumnNames.FederalAuthorizedAnnualTaxCredit]);
            item.FederalLabourTaxCredit = (int?)CleanDataValue(reader[ColumnNames.FederalLabourTaxCredit]);
            item.FederalPayTaxFlag = (bool)CleanDataValue(reader[ColumnNames.FederalPayTaxFlag]);
            item.ProvincialTaxClaim = (int?)CleanDataValue(reader[ColumnNames.ProvincialTaxClaim]);
            item.ProvincialAdditionalTax = (decimal?)CleanDataValue(reader[ColumnNames.ProvincialAdditionalTax]);
            item.ProvincialDesignatedAreaDeduction = (int?)CleanDataValue(reader[ColumnNames.ProvincialDesignatedAreaDeduction]);
            item.ProvincialAuthorizedAnnualDeduction = (int?)CleanDataValue(reader[ColumnNames.ProvincialAuthorizedAnnualDeduction]);
            item.ProvincialAuthorizedAnnualTaxCredit = (int?)CleanDataValue(reader[ColumnNames.ProvincialAuthorizedAnnualTaxCredit]);
            item.ProvincialLabourTaxCredit = (int?)CleanDataValue(reader[ColumnNames.ProvincialLabourTaxCredit]);
            item.ProvincialPayTaxFlag = (bool)CleanDataValue(reader[ColumnNames.ProvincialPayTaxFlag]);
            item.EstimatedAnnualIncome = (int?)CleanDataValue(reader[ColumnNames.EstimatedAnnualIncome]);
            item.EstimatedAnnualExpense = (int?)CleanDataValue(reader[ColumnNames.EstimatedAnnualExpense]);
            item.EstimatedNetIncome = (int?)CleanDataValue(reader[ColumnNames.EstimatedNetIncome]);
            item.CommissionPercentage = (decimal?)CleanDataValue(reader[ColumnNames.CommissionPercentage]);
            item.OverridePercentage = (decimal?)CleanDataValue(reader[ColumnNames.OverridePercentage]);
            item.PayNationalInsuranceServiceFlag = (bool)CleanDataValue(reader[ColumnNames.PayNationalInsuranceServiceFlag]);
            item.TotalExemption = (decimal?)CleanDataValue(reader[ColumnNames.TotalExemption]);
            item.PayNationalInsuranceCorporationFlag = (bool)CleanDataValue(reader[ColumnNames.PayNationalInsuranceCorporationFlag]);
            item.TDAU1Amount = (decimal?)CleanDataValue(reader[ColumnNames.TDAU1Amount]);
            item.PayNationalInsuranceBoardFlag = (bool)CleanDataValue(reader[ColumnNames.PayNationalInsuranceBoardFlag]);
            item.TD1Amount = (decimal?)CleanDataValue(reader[ColumnNames.TD1Amount]);
            item.PayNationalInsuranceSchemeFlag = (bool)CleanDataValue(reader[ColumnNames.PayNationalInsuranceSchemeFlag]);
            item.PayNationalHealthTaxFlag = (bool)CleanDataValue(reader[ColumnNames.PayNationalHealthTaxFlag]);
            item.PayEducationTaxFlag = (bool)CleanDataValue(reader[ColumnNames.PayEducationTaxFlag]);
            item.IsWeek1Month1EmployeeForCurrentYearFlag = (bool)CleanDataValue(reader[ColumnNames.IsWeek1Month1EmployeeForCurrentYearFlag]);
            item.WeekNumber = (int?)CleanDataValue(reader[ColumnNames.WeekNumber]);
            item.MonthNumber = (int?)CleanDataValue(reader[ColumnNames.MonthNumber]);
            item.TotalGrossPayToDate = (decimal?)CleanDataValue(reader[ColumnNames.TotalGrossPayToDate]);
            item.Prerequisites = (decimal?)CleanDataValue(reader[ColumnNames.Prerequisites]);
            item.NiToDate = (decimal?)CleanDataValue(reader[ColumnNames.NiToDate]);
            item.GrossPayLessDeductions = (decimal?)CleanDataValue(reader[ColumnNames.GrossPayLessDeductions]);
            item.Superannuation = (decimal?)CleanDataValue(reader[ColumnNames.Superannuation]);
            item.ApprovedExp = (decimal?)CleanDataValue(reader[ColumnNames.ApprovedExp]);
            item.TotalTaxToDateDeterminedByTaxTable = (decimal?)CleanDataValue(reader[ColumnNames.TotalTaxToDateDeterminedByTaxTable]);

            return item;
        }
        #endregion
    }
}