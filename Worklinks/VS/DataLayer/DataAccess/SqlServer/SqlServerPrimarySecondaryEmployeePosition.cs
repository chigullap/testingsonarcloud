﻿using System;
using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects.CalcModel;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerPrimarySecondaryEmployeePosition : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PrimarySecondaryEmployeePositionId = "primary_secondary_employee_position_id";
            public static String EmployeeId = "employee_id";
            public static String EmployeePositionId = "employee_position_id";
            public static String EmployeePositionSecondaryId = "employee_position_secondary_id";
            public static String SalaryPlanGradeStepId = "salary_plan_grade_step_id";
            public static String GradeHour = "grade_hour";
            public static String PlanHour = "plan_hour";
            public static String MinimumHour = "minimum_hour";
            public static String SalaryPlanAdvanceBasedOnCode = "code_salary_plan_advance_based_on_cd";
            public static String ProbationHour = "probation_hour";
            public static String AdvanceSalaryPlanGradeStepId = "advance_salary_plan_grade_step_id";
            public static String AdvanceAmount = "advance_amount";
        }
        #endregion

        #region main
        internal PrimarySecondaryEmployeePositionCollection Select(DatabaseUser user, long payrollProcessId, DateTime cutoffDate)
        {
            DataBaseCommand command = GetStoredProcCommand("PrimarySecondaryEmployeePosition_select", user.DatabaseName);

            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
            command.AddParameterWithValue("@cutoffDate", cutoffDate);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCollection(reader);
        }
        #endregion

        #region data mapping
        protected PrimarySecondaryEmployeePositionCollection MapToCollection(IDataReader reader)
        {
            PrimarySecondaryEmployeePositionCollection collection = new PrimarySecondaryEmployeePositionCollection();

            while (reader.Read())
                collection.Add(MapToItem(reader));

            return collection;
        }
        protected PrimarySecondaryEmployeePosition MapToItem(IDataReader reader)
        {
            PrimarySecondaryEmployeePosition item = new PrimarySecondaryEmployeePosition();

            item.PrimarySecondaryEmployeePositionId = (long)CleanDataValue(reader[ColumnNames.PrimarySecondaryEmployeePositionId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EmployeePositionId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionId]);
            item.EmployeePositionSecondaryId = (long?)CleanDataValue(reader[ColumnNames.EmployeePositionSecondaryId]);
            item.SalaryPlanGradeStepId = (long?)CleanDataValue(reader[ColumnNames.SalaryPlanGradeStepId]);
            item.GradeHour = (decimal?)CleanDataValue(reader[ColumnNames.GradeHour]);
            item.PlanHour = (decimal?)CleanDataValue(reader[ColumnNames.PlanHour]);
            item.MinimumHour = (int?)CleanDataValue(reader[ColumnNames.MinimumHour]);
            item.SalaryPlanAdvanceBasedOnCode = (string)CleanDataValue(reader[ColumnNames.SalaryPlanAdvanceBasedOnCode]);
            item.ProbationHour = (int?)CleanDataValue(reader[ColumnNames.ProbationHour]);
            item.AdvanceSalaryPlanGradeStepId = (long?)CleanDataValue(reader[ColumnNames.AdvanceSalaryPlanGradeStepId]);
            item.AdvanceAmount = (decimal?)CleanDataValue(reader[ColumnNames.AdvanceAmount]);

            return item;
        }
        #endregion
    }
}