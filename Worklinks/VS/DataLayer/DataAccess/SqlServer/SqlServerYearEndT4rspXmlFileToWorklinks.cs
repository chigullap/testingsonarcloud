﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerYearEndT4rspXmlFileToWorklinks : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String YearEndT4rspId = "year_end_t4rsp_id";
            public static String Revision = "revision";
            public static String PreviousRevisionYearEndT4rspId = "previous_revision_year_end_t4rsp_id";
            public static String EmployerNumber = "employer_number";
            public static String EmployeeId = "employee_id";
            public static String Year = "year";
            public static String Box14 = "box_14";
            public static String Box16Amount = "box_16_amount";
            public static String Box18Amount = "box_18_amount";
            public static String Box20Amount = "box_20_amount";
            public static String Box22Amount = "box_22_amount";
            public static String Box24Flag = "box_24_flag";
            public static String Box25Amount = "box_25_amount";
            public static String Box26Amount = "box_26_amount";
            public static String Box27Amount = "box_27_amount";
            public static String Box28Amount = "box_28_amount";
            public static String Box30Amount = "box_30_amount";
            public static String Box34Amount = "box_34_amount";
            public static String Box35Amount = "box_35_amount";
            public static String Box36 = "box_36";
            public static String Box60 = "box_60";
            public static String Box61 = "box_61";
            public static String Box40Amount = "box_40_amount";
            public static String ActiveFlag = "active_flag";
        }
        #endregion

        #region main
        public YearEndT4rspCollection Select(DatabaseUser user, int year, bool getOrginals = false, bool getAmended = false)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndT4rsp_select", user.DatabaseName);

            command.AddParameterWithValue("@year", year);
            command.AddParameterWithValue("@getOriginals", getOrginals);
            command.AddParameterWithValue("@getAmended", getAmended);

            using (IDataReader reader = command.ExecuteReader())
                return MapToYearEndT4RspCollection(reader);
        }
        public YearEndT4rspCollection SelectSingleT4rspByKey(DatabaseUser user, long key)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndT4rspByKeyId_select", user.DatabaseName);

            command.AddParameterWithValue("@YearEndT4rspId", key);

            using (IDataReader reader = command.ExecuteReader())
                return MapToYearEndT4RspCollection(reader);
        }
        public YearEndT4rsp Insert(DatabaseUser user, YearEndT4rsp yearEndT4rspObj)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndT4rsp_insert", user.DatabaseName);

            using (command)
            {
                command.BeginTransaction("YearEndT4rsp_insert");

                SqlParameter yearEndT4rspIdParm = command.AddParameterWithValue("@yearEndT4rspId", yearEndT4rspObj.YearEndT4rspId, ParameterDirection.Output);
                command.AddParameterWithValue("@revision", yearEndT4rspObj.Revision);
                command.AddParameterWithValue("@previousRevisionYearEndT4rspId", yearEndT4rspObj.PreviousRevisionYearEndT4rspId);
                command.AddParameterWithValue("@employerNumber", yearEndT4rspObj.EmployerNumber);
                command.AddParameterWithValue("@employeeId", yearEndT4rspObj.EmployeeId);
                command.AddParameterWithValue("@year", yearEndT4rspObj.Year);
                command.AddParameterWithValue("@box14", yearEndT4rspObj.Box14);
                command.AddParameterWithValue("@box16Amount", yearEndT4rspObj.Box16Amount);
                command.AddParameterWithValue("@box18Amount", yearEndT4rspObj.Box18Amount);
                command.AddParameterWithValue("@box20Amount", yearEndT4rspObj.Box20Amount);
                command.AddParameterWithValue("@box22Amount", yearEndT4rspObj.Box22Amount);
                command.AddParameterWithValue("@box24Flag", yearEndT4rspObj.Box24Flag);
                command.AddParameterWithValue("@box25Amount", yearEndT4rspObj.Box25Amount);
                command.AddParameterWithValue("@box26Amount", yearEndT4rspObj.Box26Amount);
                command.AddParameterWithValue("@box27Amount", yearEndT4rspObj.Box27Amount);
                command.AddParameterWithValue("@box28Amount", yearEndT4rspObj.Box28Amount);
                command.AddParameterWithValue("@box30Amount", yearEndT4rspObj.Box30Amount);
                command.AddParameterWithValue("@box34Amount", yearEndT4rspObj.Box34Amount);
                command.AddParameterWithValue("@box35Amount", yearEndT4rspObj.Box35Amount);
                command.AddParameterWithValue("@box36", yearEndT4rspObj.Box36);
                command.AddParameterWithValue("@box60", yearEndT4rspObj.Box60);
                command.AddParameterWithValue("@box61", yearEndT4rspObj.Box61);
                command.AddParameterWithValue("@box40Amount", yearEndT4rspObj.Box40Amount);
                command.AddParameterWithValue("@activeFlag", yearEndT4rspObj.ActiveFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", yearEndT4rspObj.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                yearEndT4rspObj.YearEndT4rspId = Convert.ToInt64(yearEndT4rspIdParm.Value);
                yearEndT4rspObj.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return yearEndT4rspObj;
            }
        }
        public void Update(DatabaseUser user, long keyId, bool activeFlag)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("YearEndT4rsp_update", user.DatabaseName))
                {
                    command.BeginTransaction("YearEndT4rsp_update");

                    command.AddParameterWithValue("@keyId", keyId);
                    command.AddParameterWithValue("@activeFlag", activeFlag);

                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, String year)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("YearEndT4rsp_delete", user.DatabaseName))
                {
                    command.BeginTransaction("YearEndT4rsp_delete");

                    command.AddParameterWithValue("@year", Convert.ToDecimal(year));

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected YearEndT4rspCollection MapToYearEndT4RspCollection(IDataReader reader)
        {
            YearEndT4rspCollection collection = new YearEndT4rspCollection();

            while (reader.Read())
                collection.Add(MapToYearEndT4Rsp(reader));

            return collection;
        }
        protected YearEndT4rsp MapToYearEndT4Rsp(IDataReader reader)
        {
            YearEndT4rsp item = new YearEndT4rsp();
            base.MapToBase(item, reader);

            item.YearEndT4rspId = (long)CleanDataValue(reader[ColumnNames.YearEndT4rspId]);
            item.Revision = (int)CleanDataValue(reader[ColumnNames.Revision]);
            item.PreviousRevisionYearEndT4rspId = (long?)CleanDataValue(reader[ColumnNames.PreviousRevisionYearEndT4rspId]);
            item.EmployerNumber = (String)CleanDataValue(reader[ColumnNames.EmployerNumber]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.Year = (Decimal)CleanDataValue(reader[ColumnNames.Year]);
            item.Box14 = (String)CleanDataValue(reader[ColumnNames.Box14]);
            item.Box16Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box16Amount]);
            item.Box18Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box18Amount]);
            item.Box20Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box20Amount]);
            item.Box22Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box22Amount]);
            item.Box24Flag = (bool)CleanDataValue(reader[ColumnNames.Box24Flag]);
            item.Box25Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box25Amount]);
            item.Box26Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box26Amount]);
            item.Box27Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box27Amount]);
            item.Box28Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box28Amount]);
            item.Box30Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box30Amount]);
            item.Box34Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box34Amount]);
            item.Box35Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box35Amount]);
            item.Box36 = (String)CleanDataValue(reader[ColumnNames.Box36]);
            item.Box60 = (String)CleanDataValue(reader[ColumnNames.Box60]);
            item.Box61 = (String)CleanDataValue(reader[ColumnNames.Box61]);
            item.Box40Amount = (Decimal?)CleanDataValue(reader[ColumnNames.Box40Amount]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);

            return item;
        }
        #endregion       
    }
}