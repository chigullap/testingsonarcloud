﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerVendor : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String VendorId = "vendor_id";
            public static String Number = "number";
            public static String Name = "name";
            public static String AddressId = "address_id";
            public static String GarnishmentTypeCode = "code_garnishment_type_cd";
            public static String PhoneNumber = "phone_number";
            public static String PhoneNumberExtension = "phone_number_extension";
            public static String EmailAddress = "email_address";
        }
        #endregion

        #region select
        internal VendorCollection Select(DatabaseUser user, long vendorId)
        {
            DataBaseCommand command = GetStoredProcCommand("Vendor_select", user.DatabaseName);

            command.AddParameterWithValue("@vendorId", vendorId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToVendorCollection(reader);
        }
        #endregion

        #region insert
        public Vendor Insert(DatabaseUser user, Vendor vendor)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Vendor_insert", user.DatabaseName))
            {
                command.BeginTransaction("Vendor_insert");

                SqlParameter vendorIdParm = command.AddParameterWithValue("@vendorId", vendor.VendorId, ParameterDirection.Output);
                command.AddParameterWithValue("@number", vendor.Number);
                command.AddParameterWithValue("@name", vendor.Name);
                command.AddParameterWithValue("@addressId", vendor.AddressId);
                command.AddParameterWithValue("@garnishmentTypeCode", vendor.GarnishmentTypeCode);
                command.AddParameterWithValue("@phoneNumber", vendor.PhoneNumber);
                command.AddParameterWithValue("@phoneNumberExtension", vendor.PhoneNumberExtension);
                command.AddParameterWithValue("@emailAddress", vendor.EmailAddress);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", vendor.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                vendor.VendorId = Convert.ToInt64(vendorIdParm.Value);
                vendor.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return vendor;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, Vendor vendor)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("Vendor_update", user.DatabaseName))
                {
                    command.BeginTransaction("Vendor_update");

                    command.AddParameterWithValue("@vendorId", vendor.VendorId);
                    command.AddParameterWithValue("@number", vendor.Number);
                    command.AddParameterWithValue("@name", vendor.Name);
                    command.AddParameterWithValue("@garnishmentTypeCode", vendor.GarnishmentTypeCode);
                    command.AddParameterWithValue("@phoneNumber", vendor.PhoneNumber);
                    command.AddParameterWithValue("@phoneNumberExtension", vendor.PhoneNumberExtension);
                    command.AddParameterWithValue("@emailAddress", vendor.EmailAddress);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", vendor.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);
                    
                    command.ExecuteNonQuery();

                    vendor.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, Vendor vendor)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("Vendor_delete", user.DatabaseName))
                {
                    command.BeginTransaction("Vendor_delete");

                    command.AddParameterWithValue("@vendorId", vendor.VendorId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected VendorCollection MapToVendorCollection(IDataReader reader)
        {
            VendorCollection collection = new VendorCollection();

            while (reader.Read())
                collection.Add(MapToVendor(reader));

            return collection;
        }
        protected Vendor MapToVendor(IDataReader reader)
        {
            Vendor item = new Vendor();
            base.MapToBase(item, reader);

            item.VendorId = (long)CleanDataValue(reader[ColumnNames.VendorId]);
            item.Name = (String)CleanDataValue(reader[ColumnNames.Name]);
            item.Number = (String)CleanDataValue(reader[ColumnNames.Number]);
            item.AddressId = (long)CleanDataValue(reader[ColumnNames.AddressId]);
            item.GarnishmentTypeCode = (String)CleanDataValue(reader[ColumnNames.GarnishmentTypeCode]);
            item.PhoneNumber = (String)CleanDataValue(reader[ColumnNames.PhoneNumber]);
            item.PhoneNumberExtension = (String)CleanDataValue(reader[ColumnNames.PhoneNumberExtension]);
            item.EmailAddress = (String)CleanDataValue(reader[ColumnNames.EmailAddress]);

            return item;
        }
        #endregion
    }
}