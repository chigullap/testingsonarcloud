﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerPaycodeSalaryMap : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PaycodeSalaryMapId = "paycode_salary_map_id";
            public static String CodePaycodeCd = "code_paycode_cd";
            public static String CodeEmployeePositionStatusCd = "code_employee_position_status_cd";
            public static String LockSalaryRateFlag = "lock_salary_rate_flag";
        }
        #endregion

        #region main
        public PaycodeSalaryMapCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PaycodeSalaryMap_select", user.DatabaseName))
            {
                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToPaycodeSalaryMapCollection(reader);
                }
            }
        }
        #endregion

        #region data mapping
        protected PaycodeSalaryMapCollection MapToPaycodeSalaryMapCollection(IDataReader reader)
        {
            PaycodeSalaryMapCollection collection = new PaycodeSalaryMapCollection();
            while (reader.Read())
            {
                collection.Add(MapToPaycodeSalaryMap(reader));
            }

            return collection;
        }
        protected PaycodeSalaryMap MapToPaycodeSalaryMap(IDataReader reader)
        {
            PaycodeSalaryMap item = new PaycodeSalaryMap();

            item.PaycodeSalaryMapId = (long)CleanDataValue(reader[ColumnNames.PaycodeSalaryMapId]);
            item.CodePaycodeCd = (String)CleanDataValue(reader[ColumnNames.CodePaycodeCd]);
            item.CodeEmployeePositionStatusCd = (String)CleanDataValue(reader[ColumnNames.CodeEmployeePositionStatusCd]);
            item.LockSalaryRateFlag = (bool)CleanDataValue(reader[ColumnNames.LockSalaryRateFlag]);

            return item;
        }

        #endregion
    }
}
