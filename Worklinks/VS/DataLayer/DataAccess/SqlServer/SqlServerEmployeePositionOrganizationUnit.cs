﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeePositionOrganizationUnit : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeePositionOrganizationUnitId = "employee_position_organization_unit_id";
            public static String EmployeePositionId = "employee_position_id";
            public static String OrganizationUnitLevelId = "organization_unit_level_id";
            public static String OrganizationUnitId = "organization_unit_id";
            public static String OrganizationUnitStartDate = "organization_unit_start_date";
        }
        #endregion

        #region select
        public EmployeePositionOrganizationUnitCollection SelectBatch(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionOrganizationUnit_batchSelect", user.DatabaseName))
            {
                SqlParameter parmOrganizationUnit = new SqlParameter();
                parmOrganizationUnit.ParameterName = "@parmOrganizationUnit";
                parmOrganizationUnit.SqlDbType = SqlDbType.Structured;

                DataTable tableOrganizationUnit = LoadEmployeePositionOrganizationUnitDataTable(importExternalIdentifiers);
                parmOrganizationUnit.Value = tableOrganizationUnit;
                command.AddParameter(parmOrganizationUnit);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToEmployeePositionOrganizationUnitCollection(reader);
            }
        }
        private DataTable LoadEmployeePositionOrganizationUnitDataTable(List<String> importExternalIdentifiers)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("import_external_identifier", typeof(string));

            for (int i = 0; i < importExternalIdentifiers.Count; i++)
            {
                table.Rows.Add(new Object[]
                {
                    importExternalIdentifiers[i]
                });
            }

            return table;
        }
        #endregion

        #region insert
        public void Insert(DatabaseUser user, EmployeePositionOrganizationUnit item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionOrganizationUnit_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeePositionOrganizationUnit_insert");

                SqlParameter employeePositionIdParm = command.AddParameterWithValue("@employeePositionOrganizationUnitId", item.EmployeePositionOrganizationUnitId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeePositionId", item.EmployeePositionId);
                command.AddParameterWithValue("@organizationUnitId", item.OrganizationUnitId);
                command.AddParameterWithValue("@organizationUnitStartDate", item.OrganizationUnitStartDate);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.EmployeePositionId = Convert.ToInt64(employeePositionIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, EmployeePositionOrganizationUnit item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionOrganizationUnit_update", user.DatabaseName))
            {
                command.BeginTransaction("EmployeePositionOrganizationUnit_update");

                command.AddParameterWithValue("@employeePositionOrganizationUnitId", item.EmployeePositionOrganizationUnitId);
                command.AddParameterWithValue("@employeePositionId", item.EmployeePositionId);
                command.AddParameterWithValue("@organizationUnitId", item.OrganizationUnitId);
                command.AddParameterWithValue("@organizationUnitStartDate", item.OrganizationUnitStartDate);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, EmployeePositionOrganizationUnit item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionOrganizationUnit_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePositionOrganizationUnit_delete");

                    command.AddParameterWithValue("@employeePositionOrganizationUnitId", item.EmployeePositionOrganizationUnitId);
                    command.AddParameterWithValue("@updateUser", user.UserName);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeePositionOrganizationUnitCollection MapToEmployeePositionOrganizationUnitCollection(IDataReader reader)
        {
            EmployeePositionOrganizationUnitCollection collection = new EmployeePositionOrganizationUnitCollection();

            while (reader.Read())
                collection.Add(MapToEmployeePositionOrganizationUnit(reader));

            return collection;
        }
        protected EmployeePositionOrganizationUnitCollection MapToEmployeePositionOrganizationUnitCollectionBulk(IDataReader reader)
        {
            EmployeePositionOrganizationUnitCollection collection = new EmployeePositionOrganizationUnitCollection();

            while (reader.Read())
                collection.Add(MapToEmployeePositionOrganizationUnitBulk(reader));

            return collection;
        }
        protected EmployeePositionOrganizationUnit MapToEmployeePositionOrganizationUnit(IDataReader reader)
        {
            EmployeePositionOrganizationUnit item = new EmployeePositionOrganizationUnit();
            base.MapToBase(item, reader);

            item.EmployeePositionOrganizationUnitId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionOrganizationUnitId]);
            item.EmployeePositionId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionId]);
            item.OrganizationUnitLevelId = (long)CleanDataValue(reader[ColumnNames.OrganizationUnitLevelId]);
            item.OrganizationUnitId = (long?)CleanDataValue(reader[ColumnNames.OrganizationUnitId]);
            item.OrganizationUnitStartDate = (DateTime?)CleanDataValue(reader[ColumnNames.OrganizationUnitStartDate]);

            return item;
        }
        protected EmployeePositionOrganizationUnit MapToEmployeePositionOrganizationUnitBulk(IDataReader reader)
        {
            EmployeePositionOrganizationUnitBulk item = new EmployeePositionOrganizationUnitBulk();
            base.MapToBase(item, reader);

            item.EmployeePositionOrganizationUnitId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionOrganizationUnitId]);
            item.EmployeePositionId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionId]);
            item.OrganizationUnitLevelId = (long)CleanDataValue(reader[ColumnNames.OrganizationUnitLevelId]);
            item.OrganizationUnitId = (long?)CleanDataValue(reader[ColumnNames.OrganizationUnitId]);
            item.OrganizationUnitStartDate = (DateTime?)CleanDataValue(reader[ColumnNames.OrganizationUnitStartDate]);

            return item;
        }
        #endregion
    }
}