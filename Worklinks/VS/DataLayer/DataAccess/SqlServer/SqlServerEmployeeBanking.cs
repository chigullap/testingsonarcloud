﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeBanking : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string EmployeeBankingId = "employee_banking_id";
            public static string EmployeeId = "employee_id";
            public static string EmployeeBankingSequenceCode = "code_employee_banking_sequence_cd";
            public static string EmployeeBankingCode = "code_employee_bank_cd";
            public static string BankingCountryCode = "code_banking_country_cd";
            public static string TransitNumber = "transit_number";
            public static string AccountNumber = "account_number";
            public static string AbaNumber = "aba_number";
            public static string Percentage = "percentage";
            public static string Amount = "amount";
            public static string EmployeeBankingRestrictionCode = "code_employee_banking_restriction_cd";
            public static string CodeBankAccountTypeCode = "code_bank_account_type_cd";
        }
        #endregion

        #region select
        internal EmployeeBankingCollection Select(DatabaseUser user, long employeeId, bool securityOverrideFlag)
        {
            return Select(user, employeeId, null, securityOverrideFlag);
        }
        internal EmployeeBankingCollection Select(DatabaseUser user, long employeeId, long? employeeBankingId, bool securityOverrideFlag)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeeBanking_select", user.DatabaseName);

            command.AddParameterWithValue("@securityOverride", securityOverrideFlag);
            command.AddParameterWithValue("@employeeId", employeeId);
            command.AddParameterWithValue("@employeeBankingId", employeeBankingId);
            command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
            command.AddParameterWithValue("@securityUserId", user.SecurityUserId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCollection(reader);
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, EmployeeBanking item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeBanking_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeBanking_update");

                    command.AddParameterWithValue("@employeeBankingId", item.EmployeeBankingId);
                    command.AddParameterWithValue("@employeeId", item.EmployeeId);
                    command.AddParameterWithValue("@employeeBankingSequenceCode", item.CodeEmployeeBankingSequenceCode);
                    command.AddParameterWithValue("@employeeBankCode", item.EmployeeBankingCode);
                    command.AddParameterWithValue("@bankingCountryCode", item.BankingCountryCode);
                    command.AddParameterWithValue("@transitNumber", item.Transit_number);
                    command.AddParameterWithValue("@accountNumber", item.Account_number);
                    command.AddParameterWithValue("@abaNumber", item.AbaNumber);
                    command.AddParameterWithValue("@percentage", item.Percentage);
                    command.AddParameterWithValue("@amount", item.Amount);
                    command.AddParameterWithValue("@employeeBankingRestrictionCode", item.CodeEmployeeBankingRestrictionCode);
                    command.AddParameterWithValue("@overrideConcurrencyCheckFlag", item.OverrideConcurrencyCheckFlag);
                    command.AddParameterWithValue("@codeBankAccountTypeCd", item.CodeBankAccountTypeCode);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region insert
        public EmployeeBanking Insert(DatabaseUser user, EmployeeBanking item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeBanking_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeBank_insert");

                SqlParameter employeeBankingIdParm = command.AddParameterWithValue("@employeeBankingId", item.EmployeeBankingId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", item.EmployeeId);
                command.AddParameterWithValue("@employeeBankingSequenceCode", item.CodeEmployeeBankingSequenceCode);
                command.AddParameterWithValue("@employeeBankCode", item.EmployeeBankingCode);
                command.AddParameterWithValue("@bankingCountryCode", item.BankingCountryCode);
                command.AddParameterWithValue("@transitNumber", item.Transit_number);
                command.AddParameterWithValue("@accountNumber", item.Account_number);
                command.AddParameterWithValue("@abaNumber", item.AbaNumber);                
                command.AddParameterWithValue("@percentage", item.Percentage);
                command.AddParameterWithValue("@amount", item.Amount);
                command.AddParameterWithValue("@employeeBankingRestrictionCode", item.CodeEmployeeBankingRestrictionCode);
                command.AddParameterWithValue("@codeBankAccountTypeCd", item.CodeBankAccountTypeCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.EmployeeBankingId = Convert.ToInt64(employeeBankingIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        #endregion

        #region import
        public void ImportBanking(DatabaseUser user, WorkLinksEmployeeBankingCollection items)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeBanking_import", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeBanking_import");

                //table parameter
                SqlParameter parmBanking = new SqlParameter();
                parmBanking.ParameterName = "@parmBanking";
                parmBanking.SqlDbType = SqlDbType.Structured;

                DataTable tableBanking = LoadBankingDataTable(items);
                parmBanking.Value = tableBanking;
                command.AddParameter(parmBanking);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        private DataTable LoadBankingDataTable(WorkLinksEmployeeBankingCollection items)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_banking_id", typeof(long));
            table.Columns.Add("employee_id", typeof(long));
            table.Columns.Add("code_employee_banking_sequence_cd", typeof(string));
            table.Columns.Add("code_employee_bank_cd", typeof(string));
            table.Columns.Add("code_banking_country_cd", typeof(string));
            table.Columns.Add("transit_number", typeof(string));
            table.Columns.Add("account_number", typeof(string));
            table.Columns.Add("aba_number", typeof(string));
            table.Columns.Add("percentage", typeof(decimal));
            table.Columns.Add("amount", typeof(decimal));
            table.Columns.Add("code_employee_banking_restriction_cd", typeof(string));
            table.Columns.Add("code_bank_account_type_cd", typeof(string));
            table.Columns.Add("import_external_identifier", typeof(string));
            table.Columns.Add("delete_flag", typeof(bool));

            foreach (WorkLinksEmployeeBanking item in items)
            {
                table.Rows.Add(new Object[]
                {
                    item.EmployeeBankingId,
                    item.EmployeeId,
                    item.CodeEmployeeBankingSequenceCode,
                    item.EmployeeBankingCode,
                    item.BankingCountryCode,
                    item.Transit_number,
                    item.Account_number,
                    (item.AbaNumber == "") ? null : item.AbaNumber,
                    item.Percentage,
                    item.Amount,
                    "2", //hardcode - this was originally done in the management layer
                    item.CodeBankAccountTypeCode,
                    item.ImportExternalIdentifier,
                    item.DeleteFlag
                });
            }

            return table;
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, EmployeeBanking item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeBanking_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeBanking_delete");

                    command.AddParameterWithValue("@employeeBankingId", item.EmployeeBankingId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeBankingCollection MapToCollection(IDataReader reader)
        {
            EmployeeBankingCollection collection = new EmployeeBankingCollection();

            while (reader.Read())
                collection.Add(MapToItem(reader));

            return collection;
        }
        protected EmployeeBanking MapToItem(IDataReader reader)
        {
            EmployeeBanking item = new EmployeeBanking();
            base.MapToBase(item, reader);

            item.EmployeeBankingId = (long)CleanDataValue(reader[ColumnNames.EmployeeBankingId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EmployeeBankingCode = (string)CleanDataValue(reader[ColumnNames.EmployeeBankingCode]);
            item.BankingCountryCode = (string)CleanDataValue(reader[ColumnNames.BankingCountryCode]);
            item.CodeEmployeeBankingSequenceCode = (string)CleanDataValue(reader[ColumnNames.EmployeeBankingSequenceCode]);
            item.Transit_number = (string)CleanDataValue(reader[ColumnNames.TransitNumber]);
            item.Account_number = (string)CleanDataValue(reader[ColumnNames.AccountNumber]);
            item.AbaNumber = (string)CleanDataValue(reader[ColumnNames.AbaNumber]);
            item.Percentage = (decimal?)CleanDataValue(reader[ColumnNames.Percentage]);
            item.Amount = (decimal?)CleanDataValue(reader[ColumnNames.Amount]);
            item.CodeEmployeeBankingRestrictionCode = (string)CleanDataValue(reader[ColumnNames.EmployeeBankingRestrictionCode]);
            item.CodeBankAccountTypeCode = (string)CleanDataValue(reader[ColumnNames.CodeBankAccountTypeCode]);

            return item;
        }
        #endregion
    }
}