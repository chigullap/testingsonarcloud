﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerRemittanceImport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            //for remittance import screen
            public static String RemittanceImportId = "remittance_import_id";
            public static String ImportDate = "import_date";
            public static String RemittanceImportStatusCode = "code_remittance_import_status_cd";
            public static String Notes = "notes";
            public static String WarningFlag = "warning_flag";

            //for remittance import approval screen
            public static String RemittanceImportDetailId = "remittance_import_detail_id";
            public static String RemittanceDestination = "remittance_destination";
            public static String AccountNumber = "account_number";
            public static String RemittanceAmount = "remittance_amount";
            public static String GrossPayroll = "gross_payroll";
            public static String EmployeeCount = "employee_count";
        }
        #endregion

        #region select
        internal RemittanceImportCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? remittanceImportId)
        {
            DataBaseCommand command = GetStoredProcCommand("RemittanceImport_select", user.DatabaseName);

            command.AddParameterWithValue("@remittanceImportId", remittanceImportId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToRemittanceImportCollection(reader);
        }
        //internal RemittanceImportDetailCollection SelectDetail(DatabaseUser user, long remittanceImportId)
        //{
        //    DataBaseCommand command = GetStoredProcCommand("RemittanceImportDetail_select", user.DatabaseName);

        //    command.AddParameterWithValue("@remittanceImportId", remittanceImportId);
        //    command.AddParameterWithValue("@languageCode", user.LanguageCode);

        //    using (IDataReader reader = command.ExecuteReader())
        //        return MapToRemittanceImportDetailCollection(reader);
        //}
        #endregion

        #region update
        //public void Process(DatabaseUser user, long remittanceImportId, String remittanceImportStatusCode)
        //{
        //    try
        //    {
        //        using (DataBaseCommand command = GetStoredProcCommand("RemittanceImport_process", user.DatabaseName))
        //        {
        //            command.BeginTransaction("RemittanceImport_process");

        //            command.AddParameterWithValue("@remittanceImportId", remittanceImportId);
        //            command.AddParameterWithValue("@remittanceImportStatusCode", remittanceImportStatusCode);
        //            command.AddParameterWithValue("@updateUser", user.UserName);
        //            command.AddParameterWithValue("@updateDatetime", Time);

        //            command.ExecuteNonQuery();
        //            command.CommitTransaction();
        //        }
        //    }
        //    catch (Exception exc)
        //    {
        //        HandleException(exc, ((SqlException)exc).Number);
        //    }
        //}
        #endregion

        #region insert
        public void InsertBatch(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, RemittanceImport remittanceImport)
        {
            using (DataBaseCommand command = GetStoredProcCommand("RemittanceImport_batchInsert", user.DatabaseName))
            {
                command.BeginTransaction("RemittanceImport_batchInsert");

                //first table parameter
                SqlParameter parmRemittanceImport = new SqlParameter();
                parmRemittanceImport.ParameterName = "@parmRemittanceImport";
                parmRemittanceImport.SqlDbType = SqlDbType.Structured;

                DataTable tableRemittanceImport = LoadRemittanceImportDataTable(remittanceImport);
                parmRemittanceImport.Value = tableRemittanceImport;
                command.AddParameter(parmRemittanceImport);

                //second table parameter
                SqlParameter parmRemittanceImportDetail = new SqlParameter();
                parmRemittanceImportDetail.ParameterName = "@parmRemittanceImportDetail";
                parmRemittanceImportDetail.SqlDbType = SqlDbType.Structured;

                DataTable tableRemittanceImportDetail = LoadRemittanceImportDetailDataTable(remittanceImport);
                parmRemittanceImportDetail.Value = tableRemittanceImportDetail;
                command.AddParameter(parmRemittanceImportDetail);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        private DataTable LoadRemittanceImportDataTable(RemittanceImport remittanceImport)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("remittance_import_id", typeof(long));
            table.Columns.Add("code_remittance_import_status_cd", typeof(String));
            table.Columns.Add("import_export_log_id", typeof(long));
            table.Columns.Add("payroll_process_id", typeof(long));

            table.Rows.Add(new Object[]
            {
                remittanceImport.RemittanceImportId,
                remittanceImport.RemittanceImportStatusCode,
                remittanceImport.ImportExportLogId,
                remittanceImport.PayrollProcessId
            });

            return table;
        }
        private DataTable LoadRemittanceImportDetailDataTable(RemittanceImport remittanceImport)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("remittance_import_detail_id", typeof(long));
            table.Columns.Add("remittance_import_id", typeof(long));
            table.Columns.Add("employee_import_external_identifier", typeof(string));
            table.Columns.Add("last_name", typeof(string));
            table.Columns.Add("first_name", typeof(string));
            table.Columns.Add("federal_business_number", typeof(string));
            table.Columns.Add("quebec_business_number", typeof(string));
            table.Columns.Add("workers_compensation_import_external_identifier", typeof(string));
            table.Columns.Add("workers_compensation_assessable", typeof(decimal));
            table.Columns.Add("employer_workers_compensation_amount", typeof(decimal));
            table.Columns.Add("employer_health_tax_code_province_state_cd", typeof(string));
            table.Columns.Add("tax_code_province_state_cd", typeof(string));
            table.Columns.Add("eht_code", typeof(string));
            table.Columns.Add("employer_health_tax", typeof(decimal));
            table.Columns.Add("employer_health_tax_assessable", typeof(decimal));
            table.Columns.Add("gross_taxable_income", typeof(decimal));
            table.Columns.Add("federal_tax", typeof(decimal));
            table.Columns.Add("canada_pension_plan", typeof(decimal));
            table.Columns.Add("employer_canada_pension_plan", typeof(decimal));
            table.Columns.Add("employment_insurance", typeof(decimal));
            table.Columns.Add("employer_employment_insurance", typeof(decimal));
            table.Columns.Add("quebec_tax", typeof(decimal));
            table.Columns.Add("quebec_pension_plan", typeof(decimal));
            table.Columns.Add("employer_quebec_pension_plan", typeof(decimal));
            table.Columns.Add("quebec_parental_insurance_plan", typeof(decimal));
            table.Columns.Add("employer_quebec_parental_insurance_plan", typeof(decimal));
            table.Columns.Add("nunavut_tax", typeof(decimal));
            table.Columns.Add("northwest_territories_tax", typeof(decimal));
            table.Columns.Add("canada_revenue_agency_garnishment", typeof(decimal));
            table.Columns.Add("social_insurance_number", typeof(string));
            table.Columns.Add("revenue_quebec_garnishement", typeof(decimal));
            table.Columns.Add("third_party_paycode_1", typeof(string));
            table.Columns.Add("third_party_amount_1", typeof(decimal));
            table.Columns.Add("third_party_paycode_2", typeof(string));
            table.Columns.Add("third_party_amount_2", typeof(decimal));
            table.Columns.Add("third_party_paycode_3", typeof(string));
            table.Columns.Add("third_party_amount_3", typeof(decimal));
            table.Columns.Add("third_party_paycode_4", typeof(string));
            table.Columns.Add("third_party_amount_4", typeof(decimal));
            table.Columns.Add("third_party_paycode_5", typeof(string));
            table.Columns.Add("third_party_amount_5", typeof(decimal));
            table.Columns.Add("third_party_paycode_6", typeof(string));
            table.Columns.Add("third_party_amount_6", typeof(decimal));
            table.Columns.Add("third_party_paycode_7", typeof(string));
            table.Columns.Add("third_party_amount_7", typeof(decimal));
            table.Columns.Add("third_party_paycode_8", typeof(string));
            table.Columns.Add("third_party_amount_8", typeof(decimal));
            table.Columns.Add("third_party_paycode_9", typeof(string));
            table.Columns.Add("third_party_amount_9", typeof(decimal));
            table.Columns.Add("third_party_paycode_10", typeof(string));
            table.Columns.Add("third_party_amount_10", typeof(decimal));
            //used for creating cheques
            table.Columns.Add("payroll_chq_amount", typeof(decimal));
            table.Columns.Add("address_line_1", typeof(string));
            table.Columns.Add("address_line_2", typeof(string));
            table.Columns.Add("city", typeof(string));
            table.Columns.Add("postal_zip_code", typeof(string));
            table.Columns.Add("code_province_state_cd", typeof(string));
            table.Columns.Add("code_country_cd", typeof(string));

            foreach (RemittanceImportDetail item in remittanceImport.RemittanceImportDetail)
            {
                table.Rows.Add(new Object[]
                {
                    item.RemittanceImportDetailId,
                    item.RemittanceImportId,
                    item.EmployeeImportExternalIdentifier,
                    item.LastName,
                    item.FirstName,
                    item.FederalBusinessNumber,
                    item.QuebecBusinessNumber,
                    item.WorkersCompensationImportExternalIdentifier,
                    item.WorkersCompensationAssessable,
                    item.EmployerWorkersCompensationAmount,
                    item.EmployerHealthTaxProvinceStateCode == string.Empty ? null : item.EmployerHealthTaxProvinceStateCode,
                    item.TaxProvince == string.Empty ? null : item.TaxProvince,
                    item.EHTCode == string.Empty ? null : item.EHTCode,
                    item.EmployerHealthTax,
                    item.EmployerHealthTaxAssessable,
                    item.GrossTaxableIncome,
                    item.FederalTax,
                    item.CanadaPensionPlan,
                    item.EmployerCanadaPensionPlan,
                    item.EmploymentInsurance,
                    item.EmployerEmploymentInsurance,
                    item.QuebecTax,
                    item.QuebecPensionPlan,
                    item.EmployerQuebecPensionPlan,
                    item.QuebecParentalInsurancePlan,
                    item.EmployerQuebecParentalInsurancePlan,
                    item.NunavutTax,
                    item.NorthwestTerritoriesTax,
                    item.CanadaRevenueAgencyGarnishment,
                    item.SocialInsuranceNumber,
                    item.RevenueQuebecGarnishement,
                    item.ThirdPartyPaycode1,
                    item.ThirdPartyAmount1,
                    item.ThirdPartyPaycode2,
                    item.ThirdPartyAmount2,
                    item.ThirdPartyPaycode3,
                    item.ThirdPartyAmount3,
                    item.ThirdPartyPaycode4,
                    item.ThirdPartyAmount4,
                    item.ThirdPartyPaycode5,
                    item.ThirdPartyAmount5,
                    item.ThirdPartyPaycode6,
                    item.ThirdPartyAmount6,
                    item.ThirdPartyPaycode7,
                    item.ThirdPartyAmount7,
                    item.ThirdPartyPaycode8,
                    item.ThirdPartyAmount8,
                    item.ThirdPartyPaycode9,
                    item.ThirdPartyAmount9,
                    item.ThirdPartyPaycode10,
                    item.ThirdPartyAmount10,
                    item.PayrollChqAmount,
                    item.AddressLine1,
                    item.AddressLine2,
                    item.City,
                    item.PostalZipCode,
                    item.ProvinceStateCode,
                    item.CountryCode
            });
            }

            return table;
        }
        public String ProcessRemittanceImport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            String validationMessage = String.Empty;

            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollMaster_RemittanceImport", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollMaster_RemittanceImport");
                    command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                    SqlParameter messageParm = command.AddParameterWithValue("@validationMessage", validationMessage, ParameterDirection.Output);
                    messageParm.Size = 4048;
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDateTime", DateTime.Now);

                    command.ExecuteNonQuery();

                    validationMessage = Convert.ToString(messageParm.Value);

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }

            return validationMessage;
        }
        #endregion

        #region delete
        public void DeleteByProcessId(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("RemittanceImport_delete", user.DatabaseName))
                {
                    command.BeginTransaction("RemittanceImport_delete");
                    command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected RemittanceImportCollection MapToRemittanceImportCollection(IDataReader reader)
        {
            RemittanceImportCollection collection = new RemittanceImportCollection();

            while (reader.Read())
                collection.Add(MapToRemittanceImport(reader));

            return collection;
        }
        protected RemittanceImport MapToRemittanceImport(IDataReader reader)
        {
            RemittanceImport item = new RemittanceImport();
            base.MapToBase(item, reader);

            item.RemittanceImportId = (long)CleanDataValue(reader[ColumnNames.RemittanceImportId]);
            item.ImportDate = (DateTime)CleanDataValue(reader[ColumnNames.ImportDate]);
            item.RemittanceImportStatusCode = (String)CleanDataValue(reader[ColumnNames.RemittanceImportStatusCode]);
            item.Notes = (String)CleanDataValue(reader[ColumnNames.Notes]);

            if (CleanDataValue(reader[ColumnNames.WarningFlag]) != null)
                item.WarningFlag = (bool)CleanDataValue(reader[ColumnNames.WarningFlag]);

            return item;
        }
        //protected RemittanceImportDetailCollection MapToRemittanceImportDetailCollection(IDataReader reader)
        //{
        //    RemittanceImportDetailCollection collection = new RemittanceImportDetailCollection();

        //    while (reader.Read())
        //        collection.Add(MapToRemittanceImportDetail(reader));

        //    return collection;
        //}
        //protected RemittanceImportDetail MapToRemittanceImportDetail(IDataReader reader)
        //{
        //    RemittanceImportDetail item = new RemittanceImportDetail();

        //    item.RemittanceImportDetailId = (long)CleanDataValue(reader[ColumnNames.RemittanceImportDetailId]);
        //    item.RemittanceDestination = (String)CleanDataValue(reader[ColumnNames.RemittanceDestination]);
        //    item.AccountNumber = (String)CleanDataValue(reader[ColumnNames.AccountNumber]);
        //    item.RemittanceAmount = (Decimal)CleanDataValue(reader[ColumnNames.RemittanceAmount]);
        //    item.GrossPayroll = (Decimal)CleanDataValue(reader[ColumnNames.GrossPayroll]);
        //    item.EmployeeCount = (int)CleanDataValue(reader[ColumnNames.EmployeeCount]);

        //    return item;
        //}
        #endregion
    }
}