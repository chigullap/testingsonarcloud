﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerLabourUnion : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String LabourUnionId = "labour_union_id";
            public static String PersonId = "person_id";
            public static String EffectiveDate = "effective_date";
            public static String EnglishDescription = "english_description";
            public static String FrenchDescription = "french_description";
            public static String LastName = "last_name";
            public static String FirstName = "first_name";
            public static String AddressLine1 = "address_line_1";
            public static String ContactTitleCode = "code_title_cd";
            public static String PhoneNumber = "primary_contact_value";
        }
        #endregion

        #region select
        internal LabourUnionSummaryCollection Select(DatabaseUser user, long? labourUnionId)
        {
            DataBaseCommand command = GetStoredProcCommand("LabourUnion_select", user.DatabaseName);

            command.AddParameterWithValue("@labourUnionId", labourUnionId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToLabourUnionCollection(reader);
        }
        #endregion

        #region insert
        public LabourUnion Insert(DatabaseUser user, LabourUnion labourUnion)
        {
            using (DataBaseCommand command = GetStoredProcCommand("LabourUnion_insert", user.DatabaseName))
            {
                command.BeginTransaction("LabourUnion_insert");

                SqlParameter labourUnionIdParm = command.AddParameterWithValue("@labourUnionId", labourUnion.LabourUnionId, ParameterDirection.Output);
                command.AddParameterWithValue("@personId", labourUnion.PersonId);
                command.AddParameterWithValue("@effectiveDate", labourUnion.EffectiveDate);
                command.AddParameterWithValue("@englishDescription", labourUnion.EnglishDescription);
                command.AddParameterWithValue("@frenchDescription", labourUnion.FrenchDescription);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                labourUnion.LabourUnionId = Convert.ToInt64(labourUnionIdParm.Value);

                command.CommitTransaction();

                return labourUnion;
            }
        }
        #endregion

        #region update
        public LabourUnion Update(DatabaseUser user, LabourUnion labourUnion)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("LabourUnion_update", user.DatabaseName))
                {
                    command.BeginTransaction("LabourUnion_update");

                    command.AddParameterWithValue("@labourUnionId", labourUnion.LabourUnionId);
                    command.AddParameterWithValue("@personId", labourUnion.PersonId);
                    command.AddParameterWithValue("@effectiveDate", labourUnion.EffectiveDate);
                    command.AddParameterWithValue("@englishDescription", labourUnion.EnglishDescription);
                    command.AddParameterWithValue("@frenchDescription", labourUnion.FrenchDescription);

                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();

                    return labourUnion;
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
                return null;
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, LabourUnion labourUnion)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("LabourUnion_delete", user.DatabaseName))
                {
                    command.BeginTransaction("LabourUnion_delete");

                    command.AddParameterWithValue("@labourUnionId", labourUnion.LabourUnionId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected LabourUnionSummaryCollection MapToLabourUnionCollection(IDataReader reader)
        {
            LabourUnionSummaryCollection collection = new LabourUnionSummaryCollection();

            while (reader.Read())
                collection.Add(MapToLabourUnion(reader));

            return collection;
        }
        protected LabourUnionSummary MapToLabourUnion(IDataReader reader)
        {
            LabourUnionSummary item = new LabourUnionSummary();
            base.MapToBase(item, reader);

            item.LabourUnionId = (long)CleanDataValue(reader[ColumnNames.LabourUnionId]);
            item.PersonId = (long)CleanDataValue(reader[ColumnNames.PersonId]);
            item.EffectiveDate = (DateTime?)CleanDataValue(reader[ColumnNames.EffectiveDate]);
            item.EnglishDescription = (String)CleanDataValue(reader[ColumnNames.EnglishDescription]);
            item.FrenchDescription = (String)CleanDataValue(reader[ColumnNames.FrenchDescription]);
            item.LastName = (String)CleanDataValue(reader[ColumnNames.LastName]);
            item.FirstName = (String)CleanDataValue(reader[ColumnNames.FirstName]);
            item.AddressLine1 = (String)CleanDataValue(reader[ColumnNames.AddressLine1]);
            item.TitleCode = (String)CleanDataValue(reader[ColumnNames.ContactTitleCode]);
            item.PhoneNumber = (String)CleanDataValue(reader[ColumnNames.PhoneNumber]);

            return item;
        }
        #endregion
    }
}