﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerEmailSentBySystem : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmailSentBySystemId = "email_sent_by_system_id";
            public static String EmailTo = "email_to";
            public static String EmailFrom = "email_from";
            public static String EmailCC = "email_cc";
            public static String EmailBCC = "email_bcc";
            public static String EmailSubject = "email_subject";
            public static String EmailBody = "email_body";
            public static String EmailSmtp = "email_smtp";
            public static String EmailEnableSSL = "email_enable_ssl";
            public static String EmailPort = "email_port";
            public static String EmailFilename = "email_filename";
            public static String EmailData = "email_data";
        }
        #endregion
        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmailSentBySystem email)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmailSentBySystem_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmailSentBySystem_insert");

                SqlParameter EmailSentBySystemIdParm = command.AddParameterWithValue("@emailSentBySystemId", email.EmailSentBySystemId, ParameterDirection.Output);

                command.AddParameterWithValue("@emailTo", email.EmailTo);
                command.AddParameterWithValue("@emailFrom", email.EmailFrom);
                command.AddParameterWithValue("@EmailCC", email.EmailCC);
                command.AddParameterWithValue("@EmailBCC", email.EmailBCC);
                command.AddParameterWithValue("@EmailSubject", email.EmailSubject);
                command.AddParameterWithValue("@EmailBody", email.EmailBody);
                command.AddParameterWithValue("@EmailSmtp", email.EmailSmtp);
                command.AddParameterWithValue("@EmailEnableSSL", email.EmailEnableSSL);
                command.AddParameterWithValue("@EmailPort", email.EmailPort);
                command.AddParameterWithValue("@EmailFilename", email.EmailFilename);
                command.AddParameterWithValue("@EmailData", email.EmailData);                

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", email.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                email.EmailSentBySystemId = Convert.ToInt64(EmailSentBySystemIdParm.Value);
                email.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();
            }
        }

        //not used by worklinks at the moment but can be wired in for the future.  This is a way to get the invoice stored in the "email_sent_by_system" table
        public EmailSentBySystemCollection GetInvoiceByEmailSentBySystemId(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long emailSentBySystemId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmailSentBySystem_selectByKey", user.DatabaseName);
            command.AddParameterWithValue("@emailSentBySystemId", emailSentBySystemId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToEmailSentBySystemCollection(reader);
            }
        }

        #region data mapping
        protected EmailSentBySystemCollection MapToEmailSentBySystemCollection(IDataReader reader)
        {
            EmailSentBySystemCollection collection = new EmailSentBySystemCollection();
            while (reader.Read())
            {
                collection.Add(MapToEmailSentBySystem(reader));
            }

            return collection;
        }
        protected EmailSentBySystem MapToEmailSentBySystem(IDataReader reader)
        {
            EmailSentBySystem item = new EmailSentBySystem();
            base.MapToBase(item, reader);

            item.EmailSentBySystemId = (long)CleanDataValue(reader[ColumnNames.EmailSentBySystemId]);

            item.EmailTo = (String)CleanDataValue(reader[ColumnNames.EmailTo]);
            item.EmailFrom = (String)CleanDataValue(reader[ColumnNames.EmailFrom]);
            item.EmailCC = (String)CleanDataValue(reader[ColumnNames.EmailCC]);
            item.EmailBCC = (String)CleanDataValue(reader[ColumnNames.EmailBCC]);
            item.EmailSubject = (String)CleanDataValue(reader[ColumnNames.EmailSubject]);
            item.EmailBody = (String)CleanDataValue(reader[ColumnNames.EmailBody]);
            item.EmailSmtp = (String)CleanDataValue(reader[ColumnNames.EmailSmtp]);
            item.EmailEnableSSL = (bool)CleanDataValue(reader[ColumnNames.EmailEnableSSL]);
            item.EmailPort = (int)CleanDataValue(reader[ColumnNames.EmailPort]);
            item.EmailFilename = (String)CleanDataValue(reader[ColumnNames.EmailFilename]);
            item.EmailData = (byte[])CleanDataValue(reader[ColumnNames.EmailData]);       

            return item;
        }

        #endregion
    }
}