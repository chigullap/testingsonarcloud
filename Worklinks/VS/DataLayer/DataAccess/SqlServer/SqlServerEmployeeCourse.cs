﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeCourse : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeCourseId = "employee_course_id";
            public static String EmployeeId = "employee_id";
            public static String EmployeeCourseNameCode = "code_employee_course_name_cd";
            public static String RegistrationDate = "registration_date";
            public static String AttendedFlag = "attended_flag";
            public static String CertificateIssued = "certificate_issued";
            public static String Note = "note";
            public static String EmployeeCourseTypeCode = "code_employee_course_type_cd";
            public static String Duration = "duration";
            public static String Mark = "mark";
            public static String EmployeeTuitionAmount = "employee_tuition_amount";
            public static String EmployerTuitionAmount = "employer_tuition_amount";
            public static String EmployeeOtherAmount = "employee_other_amount";
            public static String EmployerOtherAmount = "employer_other_amount";
            public static String CourseExpirationDate = "course_expiration_date";
            public static String StartDate = "start_date";
            public static String CompletionDate = "completion_date";
            public static String CompletedFlag = "completed_flag";
            public static String JobRequiredFlag = "job_required_flag";
            public static String AlternateAttendant = "alternate_attendant";
            public static String EmployeeCourseResultCode = "code_employee_course_result_cd";
            public static String EmployeeCoursePenaltyCode = "code_employee_course_penalty_cd";
            public static String AttachmentId = "attachment_id";
        }
        #endregion

        #region select
        internal EmployeeCourseCollection Select(DatabaseUser user, long employeeId)
        {
            return Select(user, employeeId, null);
        }
        internal EmployeeCourseCollection Select(DatabaseUser user, long employeeId, long? employeeCourseId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeeCourse_select", user.DatabaseName);

            command.AddParameterWithValue("@employeeId", employeeId);
            command.AddParameterWithValue("@employeeCourseId", employeeCourseId);
            command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
            command.AddParameterWithValue("@securityUserId", user.SecurityUserId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToEmployeeCourseCollection(reader);
        }
        #endregion

        #region insert
        public EmployeeCourse Insert(DatabaseUser user, EmployeeCourse course)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeCourse_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeCourse_insert");

                SqlParameter employeeCourseIdParm = command.AddParameterWithValue("@employeeCourseId", course.EmployeeCourseId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", course.EmployeeId);
                command.AddParameterWithValue("@employeeCourseNameCode", course.EmployeeCourseNameCode);
                command.AddParameterWithValue("@registrationDate", course.RegistrationDate);
                command.AddParameterWithValue("@attendedFlag", course.AttendedFlag);
                command.AddParameterWithValue("@certificateIssued", course.CertificateIssued);
                command.AddParameterWithValue("@note", course.Note);
                command.AddParameterWithValue("@employeeCourseTypeCode", course.EmployeeCourseTypeCode);
                command.AddParameterWithValue("@duration", course.Duration);
                command.AddParameterWithValue("@mark", course.Mark);
                command.AddParameterWithValue("@employeeTuitionAmount", course.EmployeeTuitionAmount);
                command.AddParameterWithValue("@employerTuitionAmount", course.EmployerTuitionAmount);
                command.AddParameterWithValue("@employeeOtherAmount", course.EmployeeOtherAmount);
                command.AddParameterWithValue("@employerOtherAmount", course.EmployerOtherAmount);
                command.AddParameterWithValue("@courseExpirationDate", course.CourseExpirationDate);
                command.AddParameterWithValue("@startDate", course.StartDate);
                command.AddParameterWithValue("@completionDate", course.CompletionDate);
                command.AddParameterWithValue("@completedFlag", course.CompletedFlag);
                command.AddParameterWithValue("@jobRequiredFlag", course.JobRequiredFlag);
                command.AddParameterWithValue("@alternateAttendant", course.AlternateAttendant);
                command.AddParameterWithValue("@employeeCourseResultCode", course.EmployeeCourseResultCode);
                command.AddParameterWithValue("@employeeCoursePenaltyCode", course.EmployeeCoursePenaltyCode);

                if (course.AttachmentId != -1)
                    command.AddParameterWithValue("@attachmentId", course.AttachmentId);
                else
                    command.AddParameterWithValue("@attachmentId", null);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", course.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                course.EmployeeCourseId = Convert.ToInt64(employeeCourseIdParm.Value);
                course.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return course;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, EmployeeCourse course)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeCourse_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeCourse_update");

                    command.AddParameterWithValue("@employeeCourseId", course.EmployeeCourseId);
                    command.AddParameterWithValue("@employeeId", course.EmployeeId);
                    command.AddParameterWithValue("@employeeCourseNameCode", course.EmployeeCourseNameCode);
                    command.AddParameterWithValue("@registrationDate", course.RegistrationDate);
                    command.AddParameterWithValue("@attendedFlag", course.AttendedFlag);
                    command.AddParameterWithValue("@certificateIssued", course.CertificateIssued);
                    command.AddParameterWithValue("@note", course.Note);
                    command.AddParameterWithValue("@employeeCourseTypeCode", course.EmployeeCourseTypeCode);
                    command.AddParameterWithValue("@duration", course.Duration);
                    command.AddParameterWithValue("@mark", course.Mark);
                    command.AddParameterWithValue("@employeeTuitionAmount", course.EmployeeTuitionAmount);
                    command.AddParameterWithValue("@employerTuitionAmount", course.EmployerTuitionAmount);
                    command.AddParameterWithValue("@employeeOtherAmount", course.EmployeeOtherAmount);
                    command.AddParameterWithValue("@employerOtherAmount", course.EmployerOtherAmount);
                    command.AddParameterWithValue("@courseExpirationDate", course.CourseExpirationDate);
                    command.AddParameterWithValue("@startDate", course.StartDate);
                    command.AddParameterWithValue("@completionDate", course.CompletionDate);
                    command.AddParameterWithValue("@completedFlag", course.CompletedFlag);
                    command.AddParameterWithValue("@jobRequiredFlag", course.JobRequiredFlag);
                    command.AddParameterWithValue("@alternateAttendant", course.AlternateAttendant);
                    command.AddParameterWithValue("@employeeCourseResultCode", course.EmployeeCourseResultCode);
                    command.AddParameterWithValue("@employeeCoursePenaltyCode", course.EmployeeCoursePenaltyCode);
                    command.AddParameterWithValue("@attachmentId", course.AttachmentId);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", course.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    course.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, EmployeeCourse course)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeCourse_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeCourse_delete");

                    command.AddParameterWithValue("@employeeCourseId", course.EmployeeCourseId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeCourseCollection MapToEmployeeCourseCollection(IDataReader reader)
        {
            EmployeeCourseCollection collection = new EmployeeCourseCollection();

            while (reader.Read())
                collection.Add(MapToEmployeeCourse(reader));

            return collection;
        }
        protected EmployeeCourse MapToEmployeeCourse(IDataReader reader)
        {
            EmployeeCourse item = new EmployeeCourse();
            base.MapToBase(item, reader);

            item.EmployeeCourseId = (long)CleanDataValue(reader[ColumnNames.EmployeeCourseId]); ;
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EmployeeCourseNameCode = (String)CleanDataValue(reader[ColumnNames.EmployeeCourseNameCode]);
            item.RegistrationDate = (DateTime?)CleanDataValue(reader[ColumnNames.RegistrationDate]);
            item.AttendedFlag = (bool)CleanDataValue(reader[ColumnNames.AttendedFlag]); ;
            item.CertificateIssued = (bool)CleanDataValue(reader[ColumnNames.CertificateIssued]); ;
            item.Note = (String)CleanDataValue(reader[ColumnNames.Note]);
            item.EmployeeCourseTypeCode = (String)CleanDataValue(reader[ColumnNames.EmployeeCourseTypeCode]);
            item.Duration = (Decimal)CleanDataValue(reader[ColumnNames.Duration]);
            item.Mark = (Decimal)CleanDataValue(reader[ColumnNames.Mark]);
            item.EmployeeTuitionAmount = (Decimal)CleanDataValue(reader[ColumnNames.EmployeeTuitionAmount]);
            item.EmployerTuitionAmount = (Decimal)CleanDataValue(reader[ColumnNames.EmployerTuitionAmount]);
            item.EmployeeOtherAmount = (Decimal)CleanDataValue(reader[ColumnNames.EmployeeOtherAmount]);
            item.EmployerOtherAmount = (Decimal)CleanDataValue(reader[ColumnNames.EmployerOtherAmount]);
            item.CourseExpirationDate = (DateTime?)CleanDataValue(reader[ColumnNames.CourseExpirationDate]);
            item.StartDate = (DateTime?)CleanDataValue(reader[ColumnNames.StartDate]);
            item.CompletionDate = (DateTime?)CleanDataValue(reader[ColumnNames.CompletionDate]);
            item.CompletedFlag = (bool)CleanDataValue(reader[ColumnNames.CompletedFlag]);
            item.JobRequiredFlag = (bool)CleanDataValue(reader[ColumnNames.JobRequiredFlag]);
            item.AlternateAttendant = (String)CleanDataValue((reader)[ColumnNames.AlternateAttendant]);
            item.EmployeeCourseResultCode = (String)CleanDataValue(reader[ColumnNames.EmployeeCourseNameCode]);
            item.EmployeeCoursePenaltyCode = (String)CleanDataValue(reader[ColumnNames.EmployeeCoursePenaltyCode]);
            item.AttachmentId = (long?)CleanDataValue(reader[ColumnNames.AttachmentId]);

            return item;
        }
        #endregion
    }
}