﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerWizardCacheItem : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String WizardCacheItemId = "wizard_cache_item_id";
            public static String WizardCacheId = "wizard_cache_id";
            public static String WizardItemId = "wizard_item_id";
            public static String ItemName = "item_name";
            public static String DataType = "data_type";
            public static String Item = "item";
            public static String ActiveFlag = "active_flag";
            public static String WizardIndex = "wizard_index";
            public static String NextWizardIndex = "next_wizard_index";
            public static String PreviousWizardIndex = "previous_wizard_index";
            public static String MandatoryFlag = "mandatory_flag";
            public static String VisibleFlag = "visible_flag";
        }
        #endregion

        #region select
        internal WizardCacheItemCollection Select(DatabaseUser user, long wizardId, long? wizardCacheId, String itemName)
        {
            using (DataBaseCommand command = GetStoredProcCommand("WizardCacheItem_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@wizardId", wizardId);
                command.AddParameterWithValue("@wizardCacheId", wizardCacheId);

                if (itemName.Equals(WizardCache.ItemType.None))
                    command.AddParameterWithValue("@itemName", null);
                else
                    command.AddParameterWithValue("@itemName", itemName);

                WizardCacheItemCollection wizardCacheItems = null;

                using (IDataReader reader = command.ExecuteReader())
                    wizardCacheItems = MapToWizardCacheItemCollection(reader);

                return wizardCacheItems;
            }
        }
        public WizardCacheItemCollection SelectBatch(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            using (DataBaseCommand command = GetStoredProcCommand("WizardCacheItem_batchSelect", user.DatabaseName))
            {
                SqlParameter parmWizardCache = new SqlParameter();
                parmWizardCache.ParameterName = "@parmWizardCache";
                parmWizardCache.SqlDbType = SqlDbType.Structured;

                DataTable tableWizardCache = LoadWizardCacheItemDataTable(importExternalIdentifiers);
                parmWizardCache.Value = tableWizardCache;
                command.AddParameter(parmWizardCache);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToWizardCacheItemCollection(reader);
            }
        }
        private DataTable LoadWizardCacheItemDataTable(List<String> importExternalIdentifiers)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("wizard_cache_id", typeof(long));
            table.Columns.Add("wizard_id", typeof(long));
            table.Columns.Add("template_flag", typeof(bool));
            table.Columns.Add("active_flag", typeof(bool));
            table.Columns.Add("description", typeof(String));
            table.Columns.Add("import_external_identifier", typeof(String));
            table.Columns.Add("wizard_cache_item_id", typeof(long));
            table.Columns.Add("wizard_item_id", typeof(long));
            table.Columns.Add("data_type", typeof(String));
            table.Columns.Add("item", typeof(String));
            table.Columns.Add("visible_flag", typeof(bool));

            for (int i = 0; i < importExternalIdentifiers.Count; i++)
            {
                table.Rows.Add(new Object[]
                {
                    null,
                    null,
                    null,
                    null,
                    null,
                    importExternalIdentifiers[i],
                    null,
                    null,
                    null,
                    null,
                    null
                });
            }

            return table;
        }
        internal WizardCacheItemCollection SelectWizardCacheItemSummary(DatabaseUser user, long wizardId, long? wizardItemId, long? wizardCacheId, bool templateFlag, String description)
        {
            using (DataBaseCommand command = GetStoredProcCommand("WizardCacheItem_report", user.DatabaseName))
            {
                command.AddParameterWithValue("@wizardId", wizardId);
                command.AddParameterWithValue("@wizardItemId", wizardItemId);
                command.AddParameterWithValue("@wizardCacheId", wizardCacheId);
                command.AddParameterWithValue("@templateFlag", templateFlag);
                command.AddParameterWithValue("@description", description);

                WizardCacheItemCollection wizardCacheItems = null;

                using (IDataReader reader = command.ExecuteReader())
                    wizardCacheItems = MapToWizardCacheItemCollection(reader);

                return wizardCacheItems;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, WizardCacheItem wizardCacheItem)
        {
            try
            {
                using (StoredProcedureCommand command = GetStoredProcCommand("WizardCacheItem_update", user.DatabaseName))
                {
                    command.BeginTransaction("WizardCacheItem_update");

                    command.AddParameterWithValue("@wizardCacheItemId", wizardCacheItem.WizardCacheItemId);
                    command.AddParameterWithValue("@wizardCacheId", wizardCacheItem.WizardCacheId);
                    command.AddParameterWithValue("@wizardItemId", wizardCacheItem.WizardItemId);
                    command.AddParameterWithValue("@datatype", wizardCacheItem.DataType);
                    command.AddParameterWithValue("@item", wizardCacheItem.DataXML);
                    command.AddParameterWithValue("@visibleFlag", wizardCacheItem.VisibleFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", wizardCacheItem.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    wizardCacheItem.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region insert
        public void Insert(DatabaseUser user, WizardCacheItem wizardCacheItem)
        {
            using (DataBaseCommand command = GetStoredProcCommand("WizardCacheItem_insert", user.DatabaseName))
            {
                command.BeginTransaction("WizardCacheItem_insert");

                SqlParameter wizardCacheItemIdParm = command.AddParameterWithValue("@wizardCacheItemId", wizardCacheItem.WizardCacheItemId, ParameterDirection.Output);
                command.AddParameterWithValue("@wizardCacheId", wizardCacheItem.WizardCacheId);
                command.AddParameterWithValue("@wizardItemId", wizardCacheItem.WizardItemId);
                command.AddParameterWithValue("@dataType", wizardCacheItem.DataType);
                command.AddParameterWithValue("@item", wizardCacheItem.DataXML);
                command.AddParameterWithValue("@visibleFlag", wizardCacheItem.VisibleFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", wizardCacheItem.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                wizardCacheItem.WizardCacheItemId = Convert.ToInt64(wizardCacheItemIdParm.Value);
                wizardCacheItem.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, WizardCacheItem WizardCacheItem)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("WizardCacheItem_delete", user.DatabaseName))
                {
                    command.BeginTransaction("WizardCacheItem_delete");

                    command.AddParameterWithValue("@wizardCacheItemId", WizardCacheItem.WizardCacheItemId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected WizardCacheItemCollection MapToWizardCacheItemCollection(IDataReader reader)
        {
            WizardCacheItemCollection collection = new WizardCacheItemCollection();

            while (reader.Read())
                collection.Add(MapToWizardCacheItem(reader));

            return collection;
        }
        protected WizardCacheItem MapToWizardCacheItem(IDataReader reader)
        {
            WizardCacheItem item = new WizardCacheItem();
            base.MapToBase(item, reader);

            if (CleanDataValue(reader[ColumnNames.WizardCacheItemId]) != null)
                item.WizardCacheItemId = (long)CleanDataValue(reader[ColumnNames.WizardCacheItemId]);

            item.WizardCacheId = (long?)CleanDataValue(reader[ColumnNames.WizardCacheId]);
            item.WizardItemId = (long)CleanDataValue(reader[ColumnNames.WizardItemId]);
            item.ItemName = (String)CleanDataValue(reader[ColumnNames.ItemName]);
            item.DataType = (String)CleanDataValue(reader[ColumnNames.DataType]);
            item.DataXML = (String)CleanDataValue(reader[ColumnNames.Item]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.WizardIndex = (short)CleanDataValue(reader[ColumnNames.WizardIndex]);
            item.NextWizardIndex = (short?)CleanDataValue(reader[ColumnNames.NextWizardIndex]);
            item.PreviousWizardIndex = (short?)CleanDataValue(reader[ColumnNames.PreviousWizardIndex]);
            item.MandatoryFlag = (bool)CleanDataValue(reader[ColumnNames.MandatoryFlag]);
            item.VisibleFlag = (bool)CleanDataValue(reader[ColumnNames.VisibleFlag]);

            return item;
        }
        #endregion
    }
}