﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerChequeHealthTaxExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ChequeHealthTaxExportId = "cheque_health_tax_export_id";
            public static String RbcSequenceNumber = "rbc_sequence_number";
            public static string RbcChequeNumber = "rbc_cheque_number";
            public static String Data = "data";
            public static String CodeProvinceStateCd = "code_province_state_cd";
            public static String RemittanceDate = "remittance_date";
            public static String CodeWcbChequeRemittanceFrequencyCd = "code_wcb_cheque_remittance_frequency_cd";
            public static String ChequeFileTransmissionDate = "cheque_file_transmission_date";
        }
        #endregion

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ChequeHealthTaxExport item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ChequeHealthTaxExport_insert", user.DatabaseName))
            {
                command.BeginTransaction("ChequeHealthTaxExport_insert");

                SqlParameter chequeHealthTaxExportIdParm = command.AddParameterWithValue("@chequeHealthTaxExportId", item.ChequeHealthTaxExportId, ParameterDirection.Output);
                command.AddParameterWithValue("@rbcSequenceNumber", item.RbcSequenceNumber);
                command.AddParameterWithValue("@rbcChequeNumber", item.RbcChequeNumber);
                command.AddParameterWithValue("@data", item.Data);
                command.AddParameterWithValue("@codeProvinceStateCd", item.CodeProvinceStateCd);
                command.AddParameterWithValue("@remittanceDate", item.RemittanceDate);
                command.AddParameterWithValue("@codeWcbChequeRemittanceFrequencyCd", item.CodeWcbChequeRemittanceFrequencyCd);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.ChequeHealthTaxExportId = Convert.ToInt64(chequeHealthTaxExportIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }

        public void UpdateChequeHealthTaxExportFileTransmissionDate(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long chequeHealthTaxExportId, DateTime exportDate)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ChequeHealthTaxExport_UpdateChequeHealthTaxFileTransmissionDate", user.DatabaseName))
            {
                command.BeginTransaction("ChequeHealthTaxExport_UpdateChequeHealthTaxFileTransmissionDate");

                command.AddParameterWithValue("@chequeHealthTaxExportId", chequeHealthTaxExportId);
                command.AddParameterWithValue("@exportDate", exportDate);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        public ChequeHealthTaxExportCollection CheckForUnprocessedHealthTaxRemittanceOnDatabase(WLP.BusinessLayer.BusinessObjects.DatabaseUser user)
        {
            DataBaseCommand command = GetStoredProcCommand("ChequeHealthTaxExport_selectUnprocessed", user.DatabaseName);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToChequeHealthTaxExportCollection(reader);
            }
        }

        public ChequeHealthTaxExportCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? chequeHealthTaxExportId)
        {
            DataBaseCommand command = GetStoredProcCommand("ChequeHealthTaxExport_select", user.DatabaseName);
            command.AddParameterWithValue("@chequeHealthTaxExportId", chequeHealthTaxExportId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToChequeHealthTaxExportCollection(reader);
            }
        }

        #region data mapping
        protected ChequeHealthTaxExportCollection MapToChequeHealthTaxExportCollection(IDataReader reader)
        {
            ChequeHealthTaxExportCollection collection = new ChequeHealthTaxExportCollection();
            while (reader.Read())
            {
                collection.Add(MapToChequeHealthTaxExport(reader));
            }

            return collection;
        }
        protected ChequeHealthTaxExport MapToChequeHealthTaxExport(IDataReader reader)
        {
            ChequeHealthTaxExport item = new ChequeHealthTaxExport();
            base.MapToBase(item, reader);

            item.ChequeHealthTaxExportId = (long)CleanDataValue(reader[ColumnNames.ChequeHealthTaxExportId]);
            item.RbcSequenceNumber = (long)CleanDataValue(reader[ColumnNames.RbcSequenceNumber]);
            item.RbcChequeNumber = (long)CleanDataValue(reader[ColumnNames.RbcChequeNumber]);
            item.Data = (byte[])CleanDataValue(reader[ColumnNames.Data]);
            item.CodeProvinceStateCd = (String)CleanDataValue(reader[ColumnNames.CodeProvinceStateCd]);
            item.RemittanceDate = (DateTime)CleanDataValue(reader[ColumnNames.RemittanceDate]);
            item.CodeWcbChequeRemittanceFrequencyCd = (String)CleanDataValue(reader[ColumnNames.CodeWcbChequeRemittanceFrequencyCd]);
            item.ChequeFileTransmissionDate = (DateTime?)CleanDataValue(reader[ColumnNames.ChequeFileTransmissionDate]);

            return item;
        }

        #endregion

    }
}
