﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerYearEndNr4PayerAgent : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String YearEndNr4PayerAgentId = "year_end_nr4_payer_agent_id";
            public static String Name = "name";
            public static String AddressId = "address_id";
            public static String NonResidentAccountNumber = "non_resident_account_number";
        }
        #endregion

        public YearEndNr4PayerAgentCollection SelectNr4PayerAgentByKey(DatabaseUser user, long yearEndNr4PayerAgentId)
        {      
            DataBaseCommand command = GetStoredProcCommand("YearEndNr4ByKeyId_select", user.DatabaseName);

            command.AddParameterWithValue("@yearEndNr4PayerAgentId", yearEndNr4PayerAgentId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToYearEndNr4PayerAgentCollecion(reader);
        }

        #region data mapping
        protected YearEndNr4PayerAgentCollection MapToYearEndNr4PayerAgentCollecion(IDataReader reader)
        {
            YearEndNr4PayerAgentCollection collection = new YearEndNr4PayerAgentCollection();

            while (reader.Read())
                collection.Add(MapToYearEndNr4PayerAgent(reader));

            return collection;
        }
        protected YearEndNr4PayerAgent MapToYearEndNr4PayerAgent(IDataReader reader)
        {
            YearEndNr4PayerAgent item = new YearEndNr4PayerAgent();
            base.MapToBase(item, reader);

            item.YearEndNr4PayerAgentId = (long)CleanDataValue(reader[ColumnNames.YearEndNr4PayerAgentId]);
            item.Name = (string)CleanDataValue(reader[ColumnNames.Name]);
            item.AddressId = (long)CleanDataValue(reader[ColumnNames.AddressId]);
            item.NonResidentAccountNumber = (string)CleanDataValue(reader[ColumnNames.NonResidentAccountNumber]);

            return item;
        }
        #endregion
    }
}
