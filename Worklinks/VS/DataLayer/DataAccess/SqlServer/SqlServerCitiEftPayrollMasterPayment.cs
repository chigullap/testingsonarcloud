﻿using System;
using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerCitiEftPayrollMasterPayment : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayrollMasterPaymentId = "payroll_master_payment_id";
            public static String CurrentDate1 = "current_date1";
            public static String ChequeAmount = "cheque_amount";
            public static String CurrentDate2 = "current_date2";
            public static String ChequeNumber = "cheque_number";
            public static String CompanyShortName = "company_short_name";
            public static String CompanyLongName = "company_long_name";
            public static String BankAccountNumber = "bank_account_number";
            public static String EmployeeName = "employee_name";
            public static String BankTransInstCode = "bank_trans_inst_code";
        }
        #endregion

        #region main
        public CitiEftPayrollMasterPaymentCollection Select(DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            DataBaseCommand command = GetStoredProcCommand("CitiEftPayrollMasterPayment_select", user.DatabaseName);

            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
            command.AddParameterWithValue("@employeeNumber", employeeNumber);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCitiEftPayrollMasterPaymentCollection(reader);
        }
        #endregion

        #region data mapping
        protected CitiEftPayrollMasterPaymentCollection MapToCitiEftPayrollMasterPaymentCollection(IDataReader reader)
        {
            CitiEftPayrollMasterPaymentCollection collection = new CitiEftPayrollMasterPaymentCollection();

            while (reader.Read())
                collection.Add(MapToCitiEftPayrollMasterPayment(reader));

            return collection;
        }
        protected CitiEftPayrollMasterPayment MapToCitiEftPayrollMasterPayment(IDataReader reader)
        {
            CitiEftPayrollMasterPayment item = new CitiEftPayrollMasterPayment();

            item.PayrollMasterPaymentId = (long)CleanDataValue(reader[ColumnNames.PayrollMasterPaymentId]);
            item.CurrentDate1 = (String)CleanDataValue(reader[ColumnNames.CurrentDate1]);
            item.ChequeAmount = (String)CleanDataValue(reader[ColumnNames.ChequeAmount]);
            item.CurrentDate2 = (String)CleanDataValue(reader[ColumnNames.CurrentDate2]);
            item.ChequeNumber = (String)CleanDataValue(reader[ColumnNames.ChequeNumber]);
            item.CompanyShortName = (String)CleanDataValue(reader[ColumnNames.CompanyShortName]);
            item.CompanyLongName = (String)CleanDataValue(reader[ColumnNames.CompanyLongName]);
            item.BankAccountNumber = (String)CleanDataValue(reader[ColumnNames.BankAccountNumber]);
            item.EmployeeName = (String)CleanDataValue(reader[ColumnNames.EmployeeName]);
            item.BankTransInstCode = (String)CleanDataValue(reader[ColumnNames.BankTransInstCode]);

            return item;
        }
        #endregion
    }
}