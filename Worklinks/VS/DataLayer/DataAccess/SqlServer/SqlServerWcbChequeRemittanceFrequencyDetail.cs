﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerWcbChequeRemittanceFrequencyDetail : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String WcbChequeRemittanceFrequencyDetailId = "wcb_cheque_remittance_frequency_detail_id";
            public static String WcbChequeRemittanceFrequencyCode = "code_wcb_cheque_remittance_frequency_cd";
            public static String RemittanceDateMonthCode = "remittance_date_code_month_cd";
            public static String RemittanceDateDayCode = "remittance_date_code_day_cd";
            public static String ReportingPeriodStartDateMonthCode = "reporting_period_start_date_code_month_cd";
            public static String ReportingPeriodStartDateDayCode = "reporting_period_start_date_code_day_cd";
            public static String RemittanceDate = "remittance_date";
            public static String ReportingPeriodStartDate = "reporting_period_start_date";
            public static String ReportingPeriodEndDate = "reporting_period_end_date";
            public static String BasedOnEstimateFlag = "based_on_estimate_flag";
            public static String RemitInCurrentYearFlag = "remit_in_current_year_flag";
        }
        #endregion

        #region main
        internal WcbChequeRemittanceFrequencyDetailCollection Select(DatabaseUser user, String wcbChequeRemittanceFrequencyCode)
        {
            DataBaseCommand command = GetStoredProcCommand("WcbChequeRemittanceFrequencyDetail_select", user.DatabaseName);

            command.AddParameterWithValue("@wcbChequeRemittanceFrequencyCode", wcbChequeRemittanceFrequencyCode);
            command.AddParameterWithValue("@languageCode", user.LanguageCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCollection(reader);
        }
        public WcbChequeRemittanceFrequencyDetail Insert(DatabaseUser user, WcbChequeRemittanceFrequencyDetail item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("WcbChequeRemittanceFrequencyDetail_insert", user.DatabaseName))
            {
                command.BeginTransaction("WcbChequeRemittanceFrequencyDetail_insert");

                SqlParameter keyParm = command.AddParameterWithValue("@wcbChequeRemittanceFrequencyDetailId", item.WcbChequeRemittanceFrequencyDetailId, ParameterDirection.Output);
                command.AddParameterWithValue("@wcbChequeRemittanceFrequencyCode", item.WcbChequeRemittanceFrequencyCode);
                command.AddParameterWithValue("@remittanceDateMonthCode", item.RemittanceDateMonthCode);
                command.AddParameterWithValue("@remittanceDateDayCode", item.RemittanceDateDayCode);
                command.AddParameterWithValue("@reportingPeriodStartDateMonthCode", item.ReportingPeriodStartDateMonthCode);
                command.AddParameterWithValue("@reportingPeriodStartDateDayCode", item.ReportingPeriodStartDateDayCode);
                command.AddParameterWithValue("@basedOnEstimateFlag", item.BasedOnEstimateFlag);
                command.AddParameterWithValue("@remitInCurrentYearFlag", item.RemitInCurrentYearFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.WcbChequeRemittanceFrequencyDetailId = Convert.ToInt64(keyParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        public void Update(DatabaseUser user, WcbChequeRemittanceFrequencyDetail item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("WcbChequeRemittanceFrequencyDetail_update", user.DatabaseName))
                {
                    command.BeginTransaction("WcbChequeRemittanceFrequencyDetail_update");

                    command.AddParameterWithValue("@wcbChequeRemittanceFrequencyDetailId", item.WcbChequeRemittanceFrequencyDetailId);
                    command.AddParameterWithValue("@wcbChequeRemittanceFrequencyCode", item.WcbChequeRemittanceFrequencyCode);
                    command.AddParameterWithValue("@remittanceDateMonthCode", item.RemittanceDateMonthCode);
                    command.AddParameterWithValue("@remittanceDateDayCode", item.RemittanceDateDayCode);
                    command.AddParameterWithValue("@reportingPeriodStartDateMonthCode", item.ReportingPeriodStartDateMonthCode);
                    command.AddParameterWithValue("@reportingPeriodStartDateDayCode", item.ReportingPeriodStartDateDayCode);
                    command.AddParameterWithValue("@basedOnEstimateFlag", item.BasedOnEstimateFlag);
                    command.AddParameterWithValue("@remitInCurrentYearFlag", item.RemitInCurrentYearFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, WcbChequeRemittanceFrequencyDetail item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("WcbChequeRemittanceFrequencyDetail_delete", user.DatabaseName))
                {
                    command.BeginTransaction("WcbChequeRemittanceFrequencyDetail_delete");

                    command.AddParameterWithValue("@wcbChequeRemittanceFrequencyDetailId", item.WcbChequeRemittanceFrequencyDetailId);
                    command.AddParameterWithValue("@wcbChequeRemittanceFrequencyCode", item.WcbChequeRemittanceFrequencyCode);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected WcbChequeRemittanceFrequencyDetailCollection MapToCollection(IDataReader reader)
        {
            WcbChequeRemittanceFrequencyDetailCollection collection = new WcbChequeRemittanceFrequencyDetailCollection();

            while (reader.Read())
                collection.Add(MapToItem(reader));

            return collection;
        }
        protected WcbChequeRemittanceFrequencyDetail MapToItem(IDataReader reader)
        {
            WcbChequeRemittanceFrequencyDetail item = new WcbChequeRemittanceFrequencyDetail();
            base.MapToBase(item, reader);

            item.WcbChequeRemittanceFrequencyDetailId = (long)CleanDataValue(reader[ColumnNames.WcbChequeRemittanceFrequencyDetailId]);
            item.WcbChequeRemittanceFrequencyCode = (String)CleanDataValue(reader[ColumnNames.WcbChequeRemittanceFrequencyCode]);
            item.RemittanceDateMonthCode = (String)CleanDataValue(reader[ColumnNames.RemittanceDateMonthCode]);
            item.RemittanceDateDayCode = (String)CleanDataValue(reader[ColumnNames.RemittanceDateDayCode]);
            item.ReportingPeriodStartDateMonthCode = (String)CleanDataValue(reader[ColumnNames.ReportingPeriodStartDateMonthCode]);
            item.ReportingPeriodStartDateDayCode = (String)CleanDataValue(reader[ColumnNames.ReportingPeriodStartDateDayCode]);
            item.RemittanceDate = (DateTime)CleanDataValue(reader[ColumnNames.RemittanceDate]);
            item.ReportingPeriodStartDate = (DateTime)CleanDataValue(reader[ColumnNames.ReportingPeriodStartDate]);
            item.ReportingPeriodEndDate = (DateTime)CleanDataValue(reader[ColumnNames.ReportingPeriodEndDate]);
            item.BasedOnEstimateFlag = (bool)CleanDataValue(reader[ColumnNames.BasedOnEstimateFlag]);
            item.RemitInCurrentYearFlag = (bool)CleanDataValue(reader[ColumnNames.RemitInCurrentYearFlag]);

            return item;
        }
        #endregion
    }
}