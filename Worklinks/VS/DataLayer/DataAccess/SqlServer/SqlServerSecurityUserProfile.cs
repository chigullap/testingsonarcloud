﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerSecurityUserProfile : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String SecurityUserProfileId = "security_user_profile_id";
            public static String UserIdentifier = "user_identifier";
            public static String ProfileData = "profile_data";
        }
        #endregion

        #region main
        internal SecurityUserProfileCollection Select(String databaseName, String userName)
        {
            DataBaseCommand command = GetStoredProcCommand("SecurityUserProfile_select", databaseName);
            command.AddParameterWithValue("@userName", userName);


            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToSecurityLabelRoleCollection(reader);
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, SecurityUserProfile item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SecurityUserProfile_update", user.DatabaseName))
                {
                    command.BeginTransaction("SecurityUserProfile_update");

                    SqlParameter SecurityUserProfileIdParm = command.AddParameterWithValue("@securityUserProfileId", item.SecurityUserProfileId, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@userIdentifier", user.SecurityUserId);
                    command.AddParameterWithValue("@profile_data", item.ProfileData);


                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    item.SecurityUserProfileId = Convert.ToInt64(SecurityUserProfileIdParm.Value);
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long roleId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SecurityLabelRole_delete", user.DatabaseName))
                {

                    command.BeginTransaction("SecurityLabelRole_delete");
                    command.AddParameterWithValue("@securityRoleId", roleId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected SecurityUserProfileCollection MapToSecurityLabelRoleCollection(IDataReader reader)
        {
            SecurityUserProfileCollection collection = new SecurityUserProfileCollection();
            while (reader.Read())
            {
                collection.Add(MapToSecurityUserProfile(reader));
            }

            return collection;
        }
        protected SecurityUserProfile MapToSecurityUserProfile(IDataReader reader)
        {
            SecurityUserProfile item = new SecurityUserProfile();
            base.MapToBase(item, reader);

            item.SecurityUserProfileId = (long)CleanDataValue(reader[ColumnNames.SecurityUserProfileId]);
        //    item.UserIdentifier = (String)CleanDataValue(reader[ColumnNames.UserIdentifier]);
            item.ProfileData = (String)CleanDataValue(reader[ColumnNames.ProfileData]);
            item.ModifiedFlag = false;
            return item;
        }
        #endregion
    }
}