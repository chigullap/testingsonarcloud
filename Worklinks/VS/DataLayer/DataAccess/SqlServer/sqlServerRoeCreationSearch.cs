﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class sqlServerRoeCreationSearch : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeId = "employee_id";
            public static String EmployeeNumber = "employee_number";
            public static String LastName = "last_name";
            public static String FirstName = "first_name";
            public static String PayrollProcessGroupCode = "code_payroll_process_group_cd";
            public static String OrgUnitDescription = "organization_unit_description";
            public static String TerminationDate = "termination_date";
            public static String EmployeePositionStatusCode = "code_employee_position_status_description";
            public static String EmployeePositionId = "employee_position_id";
        }
        #endregion

        #region main

        internal RoeCreationSearchResultsCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String processGroup)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeeRoePending_report", user.DatabaseName);
            command.AddParameterWithValue("@processGroup", processGroup);
            command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
            command.AddParameterWithValue("@securityUserId", user.SecurityUserId);
            command.AddParameterWithValue("@languageCode", user.LanguageCode);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToRoeCreationSearchResultsCollection(reader);
            }
        }

        public void SetPendingRoeStatusForEmployeeId(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeePosition coll)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionRoeStatus_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePositionRoeStatus_update");

                    command.AddParameterWithValue("@employeePositionId", coll.EmployeePositionId);
                    command.AddParameterWithValue("@codeRoeCreationStatusCd", coll.CodeRoeCreationStatusCd);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", coll.RowVersion, ParameterDirection.InputOutput);

                    command.ExecuteNonQuery();
                    coll.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected RoeCreationSearchResultsCollection MapToRoeCreationSearchResultsCollection(IDataReader reader)
        {
            RoeCreationSearchResultsCollection collection = new RoeCreationSearchResultsCollection();
            while (reader.Read())
            {
                collection.Add(MapToRoeCreationSearchResults(reader));
            }

            return collection;
        }

        protected RoeCreationSearchResults MapToRoeCreationSearchResults(IDataReader reader)
        {
            RoeCreationSearchResults roeObj = new RoeCreationSearchResults();

            roeObj.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            roeObj.EmployeeNumber = (String)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            roeObj.LastName = (String)CleanDataValue(reader[ColumnNames.LastName]);
            roeObj.FirstName = (String)CleanDataValue(reader[ColumnNames.FirstName]);
            roeObj.PayrollProcessGroupCode = (String)CleanDataValue(reader[ColumnNames.PayrollProcessGroupCode]);
            roeObj.OrgUnitDescription = (String)CleanDataValue(reader[ColumnNames.OrgUnitDescription]);
            roeObj.TerminationDate = (DateTime?)CleanDataValue(reader[ColumnNames.TerminationDate]);
            roeObj.EmployeePositionStatusCode = (String)CleanDataValue(reader[ColumnNames.EmployeePositionStatusCode]);
            roeObj.EmployeePositionId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionId]);

            return roeObj;
        }
        #endregion
    }
}