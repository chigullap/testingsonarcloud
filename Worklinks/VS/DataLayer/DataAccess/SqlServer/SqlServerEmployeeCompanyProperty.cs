﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeCompanyProperty : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeCompanyPropertyId = "employee_company_property_id";
            public static String EmployeeId = "employee_id";
            public static String EmployeeCompanyPropertyCode = "code_employee_company_property_cd";
            public static String Description = "description";
            public static String IssueDate = "issue_date";
            public static String IssueCondition = "issue_condition";
            public static String ReturnDate = "return_date";
            public static String ReturnCondition = "return_condition";
            public static String ReissueDate = "reissue_date";
            public static String ExpiryDate = "expiry_date";
            public static String Cost = "cost";
            public static String Notes = "notes";
        }
        #endregion

        #region select
        internal EmployeeCompanyPropertyCollection SelectCompanyProperty(DatabaseUser user, long employeeId)
        {
            return Select(user, employeeId, null);
        }
        internal EmployeeCompanyPropertyCollection Select(DatabaseUser user, long employeeId, long? employeeCompanyPropertyId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeCompanyProperty_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@employeeId", employeeId);
                command.AddParameterWithValue("@employeeCompanyPropertyId", employeeCompanyPropertyId);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToCollection(reader);
            }
        }
        #endregion

        #region insert
        public EmployeeCompanyProperty Insert(DatabaseUser user, EmployeeCompanyProperty item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeCompanyProperty_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeCompanyProperty_insert");

                SqlParameter idParm = command.AddParameterWithValue("@employeeCompanyPropertyId", item.EmployeeCompanyPropertyId, ParameterDirection.Output);

                command.AddParameterWithValue("@employeeId", item.EmployeeId);
                command.AddParameterWithValue("@employeeCompanyPropertyCode", item.EmployeeCompanyPropertyCode);
                command.AddParameterWithValue("@description", item.Description);
                command.AddParameterWithValue("@issueDate", item.IssueDate);
                command.AddParameterWithValue("@issueCondition", item.IssueCondition);
                command.AddParameterWithValue("@returnDate", item.ReturnDate);
                command.AddParameterWithValue("@returnCondition", item.ReturnCondition);
                command.AddParameterWithValue("@reissueDate", item.ReissueDate);
                command.AddParameterWithValue("@expiryDate", item.ExpiryDate);
                command.AddParameterWithValue("@cost", item.Cost);
                command.AddParameterWithValue("@notes", item.Notes);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.EmployeeCompanyPropertyId = Convert.ToInt64(idParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return Select(user, item.EmployeeId, item.EmployeeCompanyPropertyId)[0];
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, EmployeeCompanyProperty item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeCompanyProperty_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeCompanyProperty_update");

                    command.AddParameterWithValue("@employeeCompanyPropertyId", item.EmployeeCompanyPropertyId);
                    command.AddParameterWithValue("@employeeId", item.EmployeeId);
                    command.AddParameterWithValue("@employeeCompanyPropertyCode", item.EmployeeCompanyPropertyCode);
                    command.AddParameterWithValue("@description", item.Description);
                    command.AddParameterWithValue("@issueDate", item.IssueDate);
                    command.AddParameterWithValue("@issueCondition", item.IssueCondition);
                    command.AddParameterWithValue("@returnDate", item.ReturnDate);
                    command.AddParameterWithValue("@returnCondition", item.ReturnCondition);
                    command.AddParameterWithValue("@reissueDate", item.ReissueDate);
                    command.AddParameterWithValue("@expiryDate", item.ExpiryDate);
                    command.AddParameterWithValue("@cost", item.Cost);
                    command.AddParameterWithValue("@notes", item.Notes);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, EmployeeCompanyProperty property)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeCompanyProperty_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeCompanyProperty_delete");

                    command.AddParameterWithValue("@employeeCompanyPropertyId", property.EmployeeCompanyPropertyId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeCompanyPropertyCollection MapToCollection(IDataReader reader)
        {
            EmployeeCompanyPropertyCollection collection = new EmployeeCompanyPropertyCollection();

            while (reader.Read())
                collection.Add(MapToItem(reader));

            return collection;
        }
        protected EmployeeCompanyProperty MapToItem(IDataReader reader)
        {
            EmployeeCompanyProperty item = new EmployeeCompanyProperty();
            base.MapToBase(item, reader);

            item.EmployeeCompanyPropertyId = (long)CleanDataValue(reader[ColumnNames.EmployeeCompanyPropertyId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EmployeeCompanyPropertyCode = (String)CleanDataValue(reader[ColumnNames.EmployeeCompanyPropertyCode]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            item.IssueDate = (DateTime?)CleanDataValue(reader[ColumnNames.IssueDate]);
            item.IssueCondition = (String)CleanDataValue(reader[ColumnNames.IssueCondition]);
            item.ReturnDate = (DateTime?)CleanDataValue(reader[ColumnNames.ReturnDate]);
            item.ReturnCondition = (String)CleanDataValue(reader[ColumnNames.ReturnCondition]);
            item.ReissueDate = (DateTime?)CleanDataValue(reader[ColumnNames.ReissueDate]);
            item.ExpiryDate = (DateTime?)CleanDataValue(reader[ColumnNames.ExpiryDate]);
            item.Cost = (Decimal?)CleanDataValue(reader[ColumnNames.Cost]);
            item.Notes = (String)CleanDataValue(reader[ColumnNames.Notes]);

            return item;
        }
        #endregion
    }
}