﻿using System;
using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerCitiChequeDetails : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayrollMasterPaymentId = "payroll_master_payment_id";
            public static String PayrollMasterId = "payroll_master_id";
            public static String EmployeeNumber = "employee_number";
            public static String ChequeAmount = "cheque_amount";
            public static String ChequeDate = "cheque_date";
            public static String CompanyCode = "code_company_cd";
            public static String EmployeeId = "employee_id";
            public static String PayrollProcessId = "payroll_process_id";
            public static String ProcessChequeDate = "process_cheque_date";
            public static String SequenceNumber = "sequence_number";
            public static String EmployeeName = "employee_name";
            public static String CodeLanguage = "code_language_cd";
            public static String AddressLine1 = "address_line_1";
            public static String AddressLine2 = "address_line_2";
            public static String City = "city";
            public static String CodeProvinceStateCd = "code_province_state_cd";
            public static String CodeCountryCd = "code_country_cd";
            public static String PostalZipCode = "postal_zip_code";
            public static String CutoffDate = "cutoff_date";
        }
        #endregion

        #region main
        public CitiChequeDetailsCollection Select(DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            using (DataBaseCommand command = GetStoredProcCommand("citiChequeData_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                command.AddParameterWithValue("@employeeNumber", employeeNumber);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToCitiChequeDetailsCollection(reader);
            }
        }
        //used to get payroll master payment records and store then into an object for special case
        public CitiChequeDetailsCollection GetPayrollMasterPaymentData(DatabaseUser user, long payrollMasterPaymentId)
        {
            DataBaseCommand command = GetStoredProcCommand("PayrollMasterPaymentByKeyToCitiCheque_select", user.DatabaseName);

            command.AddParameterWithValue("@payrollMasterPaymentId", payrollMasterPaymentId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCitiChequeDetailsCollection(reader);
        }
        #endregion

        #region data mapping
        protected CitiChequeDetailsCollection MapToCitiChequeDetailsCollection(IDataReader reader)
        {
            CitiChequeDetailsCollection collection = new CitiChequeDetailsCollection();

            while (reader.Read())
                collection.Add(MapToCitiChequeDetails(reader));

            return collection;
        }
        protected CitiChequeDetails MapToCitiChequeDetails(IDataReader reader)
        {
            CitiChequeDetails item = new CitiChequeDetails();

            item.PayrollMasterPaymentId = (long)CleanDataValue(reader[ColumnNames.PayrollMasterPaymentId]);
            item.PayrollMasterId = (long)CleanDataValue(reader[ColumnNames.PayrollMasterId]);
            item.EmployeeNumber = (String)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.ChequeAmount = (Decimal)CleanDataValue(reader[ColumnNames.ChequeAmount]);
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.CompanyCode = (String)CleanDataValue(reader[ColumnNames.CompanyCode]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.PayrollProcessId = ((long)CleanDataValue(reader[ColumnNames.PayrollProcessId]));
            item.ProcessChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ProcessChequeDate]);
            item.SequenceNumber = Convert.ToString(CleanDataValue(reader[ColumnNames.SequenceNumber]));
            item.EmployeeName = (String)CleanDataValue(reader[ColumnNames.EmployeeName]);
            item.CodeLanguage = (String)CleanDataValue(reader[ColumnNames.CodeLanguage]);
            item.AddressLine1 = (String)CleanDataValue(reader[ColumnNames.AddressLine1]);
            item.AddressLine2 = (String)CleanDataValue(reader[ColumnNames.AddressLine2]);
            item.City = (String)CleanDataValue(reader[ColumnNames.City]);
            item.CodeProvinceStateCd = (String)CleanDataValue(reader[ColumnNames.CodeProvinceStateCd]);
            item.CodeCountryCd = (String)CleanDataValue(reader[ColumnNames.CodeCountryCd]);
            item.PostalZipCode = (String)CleanDataValue(reader[ColumnNames.PostalZipCode]);
            item.CutoffDate = (DateTime)CleanDataValue(reader[ColumnNames.CutoffDate]);

            return item;
        }
        #endregion
    }
}