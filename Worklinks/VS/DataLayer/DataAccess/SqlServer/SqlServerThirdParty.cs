﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerThirdParty : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string CodeThirdPartyCd = "code_third_party_cd";
            public static string EnglishDescription = "english_description";
            public static string FrenchDescription = "french_description";
            public static string CodeProvinceStateCode = "code_province_state_cd";
            public static string AccountNumber = "account_number";
            public static string SortOrder = "sort_order";
            public static string ImportExternalIdentifier = "import_external_identifier";
            public static string ActiveFlag = "active_flag";
            public static string CodePaycodeCode = "code_paycode_cd";
            public static string ExcludeFromRemittanceExportFlag = "exclude_from_remittance_export_flag";
            public static string VendorId = "vendor_id";
            public static string SystemFlag = "system_flag";
            public static string CodeWcbChequeRemittanceFrequencyCd = "code_wcb_cheque_remittance_frequency_cd";
            public static string CodeRemittanceMethodCd = "code_remittance_method_cd";
            public static string CodeThirdPartyBankCd = "code_third_party_bank_cd";
            public static string CodeBankingCountryCd = "code_banking_country_cd";
            public static string ThirdPartyTransitNumber = "third_party_transit_number";
            public static string ThirdPartyAccountNumber = "third_party_account_number";
            public static string BusinessNumberId = "business_number_id";
        }
        #endregion

        #region main
        internal CodeThirdPartyCollection Select(DatabaseUser user, string thirdPartyCode)
        {
            DataBaseCommand command = GetStoredProcCommand("ThirdPartyCode_select", user.DatabaseName);
            command.AddParameterWithValue("@thirdPartyCode", thirdPartyCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCodeThirdPartyCollection(reader);
        }

        public void Delete(DatabaseUser user, CodeThirdParty thirdParty)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("ThirdPartyCode_delete", user.DatabaseName))
                {
                    command.BeginTransaction("ThirdPartyCode_delete");

                    command.AddParameterWithValue("@thirdPartyCode", thirdParty.CodeThirdPartyCd);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Update(DatabaseUser user, CodeThirdParty thirdParty)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("ThirdPartyCode_update", user.DatabaseName))
                {
                    command.BeginTransaction("ThirdPartyCode_update");

                    command.AddParameterWithValue("@codeThirdPartyCd", thirdParty.CodeThirdPartyCd);
                    command.AddParameterWithValue("@codeProvinceStateCode", thirdParty.CodeProvinceStateCode);
                    command.AddParameterWithValue("@accountNumber", thirdParty.AccountNumber);
                    command.AddParameterWithValue("@sortOrder", thirdParty.SortOrder);
                    command.AddParameterWithValue("@importExternalIdentifier", thirdParty.ImportExternalIdentifier);
                    command.AddParameterWithValue("@activeFlag", thirdParty.ActiveFlag);
                    command.AddParameterWithValue("@codePaycodeCode", thirdParty.CodePaycodeCode);
                    command.AddParameterWithValue("@excludeFromRemittanceExportFlag", thirdParty.ExcludeFromRemittanceExportFlag);
                    command.AddParameterWithValue("@vendorId", thirdParty.VendorId);
                    command.AddParameterWithValue("@systemFlag", thirdParty.SystemFlag);
                    command.AddParameterWithValue("@codeWcbChequeRemittanceFrequencyCd", thirdParty.CodeWcbChequeRemittanceFrequencyCd);
                    command.AddParameterWithValue("@codeRemittanceMethodCd", thirdParty.CodeRemittanceMethodCd);
                    command.AddParameterWithValue("@codeThirdPartyBankCd", thirdParty.CodeThirdPartyBankCd);
                    command.AddParameterWithValue("@codeBankingCountryCd", thirdParty.CodeBankingCountryCd);
                    command.AddParameterWithValue("@thirdPartyTransitNumber", thirdParty.ThirdPartyTransitNumber);
                    command.AddParameterWithValue("@thirdPartyAccountNumber", thirdParty.ThirdPartyAccountNumber);
                    command.AddParameterWithValue("@businessNumberId", thirdParty.BusinessNumberId);

                    command.AddParameterWithValue("@englishDescription", thirdParty.EnglishDescription);
                    command.AddParameterWithValue("@frenchDescription", thirdParty.FrenchDescription);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", thirdParty.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    thirdParty.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public CodeThirdParty Insert(DatabaseUser user, CodeThirdParty thirdParty)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ThirdPartyCode_insert", user.DatabaseName))
            {
                command.BeginTransaction("ThirdPartyCode_insert");

                SqlParameter wsibCodeParm = command.AddParameterWithValue("@codeThirdPartyCd", thirdParty.CodeThirdPartyCd, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@codeProvinceStateCode", thirdParty.CodeProvinceStateCode);
                command.AddParameterWithValue("@accountNumber", thirdParty.AccountNumber);
                command.AddParameterWithValue("@sortOrder", thirdParty.SortOrder);
                command.AddParameterWithValue("@importExternalIdentifier", thirdParty.ImportExternalIdentifier);
                command.AddParameterWithValue("@activeFlag", thirdParty.ActiveFlag);
                command.AddParameterWithValue("@codePaycodeCode", thirdParty.CodePaycodeCode);
                command.AddParameterWithValue("@excludeFromRemittanceExportFlag", thirdParty.ExcludeFromRemittanceExportFlag);
                command.AddParameterWithValue("@vendorId", thirdParty.VendorId);
                command.AddParameterWithValue("@systemFlag", thirdParty.SystemFlag);
                command.AddParameterWithValue("@codeWcbChequeRemittanceFrequencyCd", thirdParty.CodeWcbChequeRemittanceFrequencyCd);
                command.AddParameterWithValue("@codeRemittanceMethodCd", thirdParty.CodeRemittanceMethodCd);
                command.AddParameterWithValue("@codeThirdPartyBankCd", thirdParty.CodeThirdPartyBankCd);
                command.AddParameterWithValue("@codeBankingCountryCd", thirdParty.CodeBankingCountryCd);
                command.AddParameterWithValue("@thirdPartyTransitNumber", thirdParty.ThirdPartyTransitNumber);
                command.AddParameterWithValue("@thirdPartyAccountNumber", thirdParty.ThirdPartyAccountNumber);
                command.AddParameterWithValue("@businessNumberId", thirdParty.BusinessNumberId);

                command.AddParameterWithValue("@englishDescription", thirdParty.EnglishDescription);
                command.AddParameterWithValue("@frenchDescription", thirdParty.FrenchDescription);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", thirdParty.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                thirdParty.CodeThirdPartyCd = (String)wsibCodeParm.Value;
                thirdParty.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return thirdParty;
            }
        }
        #endregion

        #region data mapping
        protected CodeThirdPartyCollection MapToCodeThirdPartyCollection(IDataReader reader)
        {
            CodeThirdPartyCollection collection = new CodeThirdPartyCollection();

            while (reader.Read())
                collection.Add(MapToCodeThirdParty(reader));

            return collection;
        }
        protected CodeThirdParty MapToCodeThirdParty(IDataReader reader)
        {
            CodeThirdParty item = new CodeThirdParty();
            base.MapToBase(item, reader);

            item.CodeThirdPartyCd = (string)CleanDataValue(reader[ColumnNames.CodeThirdPartyCd]);
            item.EnglishDescription = (string)CleanDataValue(reader[ColumnNames.EnglishDescription]);
            item.FrenchDescription = (string)CleanDataValue(reader[ColumnNames.FrenchDescription]);
            item.CodeProvinceStateCode = (string)CleanDataValue(reader[ColumnNames.CodeProvinceStateCode]);
            item.AccountNumber = (string)CleanDataValue(reader[ColumnNames.AccountNumber]);
            item.SortOrder = (short)CleanDataValue(reader[ColumnNames.SortOrder]);
            item.ImportExternalIdentifier = (string)CleanDataValue(reader[ColumnNames.ImportExternalIdentifier]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.CodePaycodeCode = (string)CleanDataValue(reader[ColumnNames.CodePaycodeCode]);
            item.ExcludeFromRemittanceExportFlag = (bool)CleanDataValue(reader[ColumnNames.ExcludeFromRemittanceExportFlag]);
            item.VendorId = (long?)CleanDataValue(reader[ColumnNames.VendorId]);
            item.SystemFlag = (bool)CleanDataValue(reader[ColumnNames.SystemFlag]);
            item.CodeWcbChequeRemittanceFrequencyCd = (string)CleanDataValue(reader[ColumnNames.CodeWcbChequeRemittanceFrequencyCd]);
            item.CodeRemittanceMethodCd = (string)CleanDataValue(reader[ColumnNames.CodeRemittanceMethodCd]);
            item.CodeThirdPartyBankCd = (string)CleanDataValue(reader[ColumnNames.CodeThirdPartyBankCd]);
            item.CodeBankingCountryCd = (string)CleanDataValue(reader[ColumnNames.CodeBankingCountryCd]);
            item.ThirdPartyTransitNumber = (string)CleanDataValue(reader[ColumnNames.ThirdPartyTransitNumber]);
            item.ThirdPartyAccountNumber = (string)CleanDataValue(reader[ColumnNames.ThirdPartyAccountNumber]);
            item.BusinessNumberId = (long?)CleanDataValue(reader[ColumnNames.BusinessNumberId]);

            return item;
        }
        #endregion
    }
}
