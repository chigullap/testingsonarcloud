﻿using System;
using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerCICWeeklyExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeId = "employee_id";
            public static String EmployeeNumber = "employee_number";
            public static String GovernmentIdentificationNumber1 = "government_identification_number_1";
            public static String BirthDate = "birth_date";
            public static String Status = "status";
        }
        #endregion

        #region main
        public CICWeeklyExportCollection Select(DatabaseUser user, decimal year)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CICPlusWeeklyExport_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@year", year);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToCICWeeklyExportDetailCollection(reader);
            }
        }
        #endregion

        #region data mapping
        protected CICWeeklyExportCollection MapToCICWeeklyExportDetailCollection(IDataReader reader)
        {
            CICWeeklyExportCollection collection = new CICWeeklyExportCollection();

            while (reader.Read())
                collection.Add(MapToCICWeeklyExportDetail(reader));

            return collection;
        }
        protected CICWeeklyExport MapToCICWeeklyExportDetail(IDataReader reader)
        {
            CICWeeklyExport item = new CICWeeklyExport();

            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EmployeeNumber = (String)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.GovernmentIdentificationNumber1 = (String)CleanDataValue(reader[ColumnNames.GovernmentIdentificationNumber1]);
            item.BirthDate = (DateTime)CleanDataValue(reader[ColumnNames.BirthDate]);
            item.Status = (String)CleanDataValue(reader[ColumnNames.Status]);

            return item;
        }
        #endregion
    }
}
