﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects.Import;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerExternalPaycodeImportMap : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ExternalPaycodeImportMapId = "external_paycode_import_map_id";
            public static String ExternalPaycodeImportId = "external_paycode_import_id";
            public static String ExternalIdentifier1 = "external_identifier_1";
            public static String ExternalIdentifier2 = "external_identifier_2";
            public static String ExternalIdentifier3 = "external_identifier_3";
            public static String ExternalIdentifier4 = "external_identifier_4";
            public static String PaycodeCode = "code_paycode_cd";
        }
        #endregion

        #region main


        internal ExternalPaycodeImportMapCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String externalPaycodeImportName)
        {
            DataBaseCommand command = GetStoredProcCommand("ExternalPaycodeImportMap_select", user.DatabaseName);
            command.AddParameterWithValue("@externalPaycodeImportName", externalPaycodeImportName);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToExternalPaycodeImportMapCollection(reader);
            }
        }

       
        #endregion


        #region data mapping
        protected ExternalPaycodeImportMapCollection MapToExternalPaycodeImportMapCollection(IDataReader reader)
        {
            ExternalPaycodeImportMapCollection collection = new ExternalPaycodeImportMapCollection();
            while (reader.Read())
            {
                collection.Add(MapToExternalPaycodeImportMap(reader));
            }

            return collection;
        }
        protected ExternalPaycodeImportMap MapToExternalPaycodeImportMap(IDataReader reader)
        {
            ExternalPaycodeImportMap skill = new ExternalPaycodeImportMap();
            base.MapToBase(skill, reader);

            skill.ExternalPaycodeImportMapId = (long)CleanDataValue(reader[ColumnNames.ExternalPaycodeImportMapId]);
            skill.ExternalPaycodeImportId = (long)CleanDataValue(reader[ColumnNames.ExternalPaycodeImportId]);
            skill.ExternalIdentifier1 = (String)CleanDataValue(reader[ColumnNames.ExternalIdentifier1]);
            skill.ExternalIdentifier2 = (String)CleanDataValue(reader[ColumnNames.ExternalIdentifier2]);
            skill.ExternalIdentifier3 = (String)CleanDataValue(reader[ColumnNames.ExternalIdentifier3]);
            skill.ExternalIdentifier4 = (String)CleanDataValue(reader[ColumnNames.ExternalIdentifier4]);
            skill.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);

            return skill;
        }
        #endregion
    }
}
