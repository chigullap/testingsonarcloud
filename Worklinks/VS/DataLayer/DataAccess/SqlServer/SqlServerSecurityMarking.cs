﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerSecurityMarking : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String HasChildrenFlag = "has_children_flag";
            public static String SecurityMarkingId = "security_marking_id";
            public static String AlternateSecurityMarkingCode = "alternate_security_marking_code";
            public static String SecurityCategoryId = "security_category_id";
            public static String ReferenceTableIdentifier = "reference_table_identifier";
            public static String Description = "description";
        }
        #endregion

        #region main

        internal SecurityMarkingCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, int hierarchicalSortOrder)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SecurityMarking_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@hierarchicalSortOrder", hierarchicalSortOrder);
                command.AddParameterWithValue("@languageCode", user.LanguageCode);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToSecurityMarkingCollection(reader);
                }
            }
        }



        #endregion


        #region data mapping
        protected SecurityMarkingCollection MapToSecurityMarkingCollection(IDataReader reader)
        {
            SecurityMarkingCollection collection = new SecurityMarkingCollection();

            while (reader.Read())
            {
                collection.Add(MapToSecurityMarking(reader));
            }

            return collection;
        }
        protected SecurityMarking MapToSecurityMarking(IDataReader reader)
        {
            SecurityMarking item = new SecurityMarking();
            base.MapToBase(item, reader);

            item.HasChildrenFlag = (bool)CleanDataValue(reader[ColumnNames.HasChildrenFlag]);
            item.SecurityMarkingId = (long)CleanDataValue(reader[ColumnNames.SecurityMarkingId]);
            item.AlternateSecurityMarkingCode = (String)CleanDataValue(reader[ColumnNames.AlternateSecurityMarkingCode]);
            item.SecurityCategoryId = (long)CleanDataValue(reader[ColumnNames.SecurityCategoryId]);
            item.ReferenceTableIdentifier = (String)CleanDataValue(reader[ColumnNames.ReferenceTableIdentifier]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);

            return item;
        }
        #endregion
    }
}
