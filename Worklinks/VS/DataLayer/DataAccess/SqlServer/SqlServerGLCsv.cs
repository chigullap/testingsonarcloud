﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerGLCsv : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String Amount = "Amount";
            public static String GlAccount = "GL Account";
            public static String Company = "Company";
            public static String CostCenter = "Cost Ctr";
            public static String CreditDebit = "Debit / Credit";
        }
        #endregion

        #region main
        internal GlCsvCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            DataBaseCommand command = GetStoredProcCommand("GLReport_select", user.DatabaseName);
            command.AddParameterWithValue("@databaseName", user.DatabaseName);
            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToGlCsvCollection(reader);
            }
        }
        #endregion


        #region data mapping
        protected GlCsvCollection MapToGlCsvCollection(IDataReader reader)
        {
            int i = 1;
            GlCsvCollection collection = new GlCsvCollection();
            while (reader.Read())
            {
                collection.Add(MapToGlCsv(reader, i++));
            }

            return collection;
        }
        protected GlCsv MapToGlCsv(IDataReader reader, int key)
        {
            GlCsv item = new GlCsv();

            item.GlCsvId = Convert.ToInt64(key);

            item.Amount = (Decimal)CleanDataValue(reader[ColumnNames.Amount]);
            item.GlAccount = (String)CleanDataValue(reader[ColumnNames.GlAccount]);
            item.Company = (String)CleanDataValue(reader[ColumnNames.Company]);
            item.CostCenter = (String)CleanDataValue(reader[ColumnNames.CostCenter]);
            item.CreditDebit = (String)CleanDataValue(reader[ColumnNames.CreditDebit]);

            return item;
        }
        #endregion
    }
}
