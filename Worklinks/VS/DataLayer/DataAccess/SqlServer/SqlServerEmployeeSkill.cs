﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeSkill : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeSkillId = "employee_skill_id";
            public static String EmployeeId = "employee_id";
            public static String EmployeeSkillCode = "code_employee_skill_cd";
            public static String DateAcquired = "date_acquired";
            public static String ExpiryDate = "expiry_date";
            public static String JobRelatedFlag = "job_related_flag";
            public static String JobRequiredFlag = "job_required_flag";
            public static String AttachmentId = "attachment_id";
        }
        #endregion

        #region main
        internal EmployeeSkillCollection Select(DatabaseUser user, long employeeId)
        {
            return Select(user, employeeId, null);
        }

        internal EmployeeSkillCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeeId, long? employeeSkillId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeeSkill_select", user.DatabaseName);
            command.AddParameterWithValue("@employeeId", employeeId);
            command.AddParameterWithValue("@employeeSkillId", employeeSkillId);
            command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
            command.AddParameterWithValue("@securityUserId", user.SecurityUserId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToEmployeeSkillCollection(reader);
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeSkill skill)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeSkill_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeSkill_update");

                    command.AddParameterWithValue("@employeeSkillId", skill.EmployeeSkillId);
                    command.AddParameterWithValue("@employeeId", skill.EmployeeId);
                    command.AddParameterWithValue("@codeEmployeeSkillCd", skill.EmployeeSkillCode);
                    command.AddParameterWithValue("@dateAcquired", skill.DateAcquired);
                    command.AddParameterWithValue("@expiryDate", skill.ExpiryDate);
                    command.AddParameterWithValue("jobrelatedflag", skill.JobRelatedFlag);
                    command.AddParameterWithValue("jobrequiredflag", skill.JobRequiredFlag);
                    command.AddParameterWithValue("@attachmentId", skill.AttachmentId);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", skill.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    skill.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public EmployeeSkill Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeSkill skill)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeSkill_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeSkill_insert");

                SqlParameter employeeSkillIdParm = command.AddParameterWithValue("@employeeSkillId", skill.EmployeeSkillId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", skill.EmployeeId);
                command.AddParameterWithValue("@codeEmployeeSkillCd", skill.EmployeeSkillCode);
                command.AddParameterWithValue("@dateAcquired", skill.DateAcquired);
                command.AddParameterWithValue("@expiryDate", skill.ExpiryDate);
                command.AddParameterWithValue("jobrelatedflag", skill.JobRelatedFlag);
                command.AddParameterWithValue("jobrequiredflag", skill.JobRequiredFlag);
                command.AddParameterWithValue("@attachmentId", skill.AttachmentId);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", skill.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                skill.EmployeeSkillId = Convert.ToInt64(employeeSkillIdParm.Value);
                skill.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();

                return skill;
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeSkill skill)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeSkill_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeSkill_delete");
                    command.AddParameterWithValue("@employeeSkillId", skill.EmployeeSkillId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeSkillCollection MapToEmployeeSkillCollection(IDataReader reader)
        {
            EmployeeSkillCollection collection = new EmployeeSkillCollection();
            while (reader.Read())
            {
                collection.Add(MapToEmployeeSkill(reader));
            }

            return collection;
        }

        protected EmployeeSkill MapToEmployeeSkill(IDataReader reader)
        {
            EmployeeSkill skill = new EmployeeSkill();
            base.MapToBase(skill, reader);

            skill.EmployeeSkillId = (long)CleanDataValue(reader[ColumnNames.EmployeeSkillId]);
            skill.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            skill.EmployeeSkillCode = (String)CleanDataValue(reader[ColumnNames.EmployeeSkillCode]);
            skill.DateAcquired = (DateTime?)CleanDataValue(reader[ColumnNames.DateAcquired]);
            skill.ExpiryDate = (DateTime?)CleanDataValue(reader[ColumnNames.ExpiryDate]);
            skill.JobRelatedFlag = (bool)CleanDataValue(reader[ColumnNames.JobRelatedFlag]);
            skill.JobRequiredFlag = (bool)CleanDataValue(reader[ColumnNames.JobRequiredFlag]);
            skill.AttachmentId = (long?)CleanDataValue(reader[ColumnNames.AttachmentId]);

            return skill;
        }
        #endregion
    }
}