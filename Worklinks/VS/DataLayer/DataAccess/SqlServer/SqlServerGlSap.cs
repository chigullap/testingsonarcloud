﻿using System;
using System.Data;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerGlSap : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String LegalEntity = "legal_entity";
            public static String CostCentre = "cost_centre";
            public static String Account = "account";
            public static String DebitAmount = "debit_amount";
            public static String CreditAmount = "credit_amount";
            public static String DebitCreditIndicator = "debit_credit_indicator";
        }
        #endregion

        #region main
        internal GlSapCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            DataBaseCommand command = GetStoredProcCommand("GLReport_select", user.DatabaseName);
            command.AddParameterWithValue("@databaseName", user.DatabaseName);
            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToGlCsvCollection(reader);
            }
        }
        #endregion

        #region data mapping
        protected GlSapCollection MapToGlCsvCollection(IDataReader reader)
        {
            int i = 1;
            GlSapCollection collection = new GlSapCollection();
            while (reader.Read())
            {
                collection.Add(MapToGlCsv(reader, i++));
            }

            return collection;
        }
        protected GlSap MapToGlCsv(IDataReader reader, int key)
        {
            GlSap item = new GlSap();

            item.GlSapId = Convert.ToInt64(key);

            item.LegalEntity = (String)CleanDataValue(reader[ColumnNames.LegalEntity]);
            item.CostCentre = (String)CleanDataValue(reader[ColumnNames.CostCentre]);
            item.Account = (String)CleanDataValue(reader[ColumnNames.Account]);
            item.DebitAmount = (Decimal)CleanDataValue(reader[ColumnNames.DebitAmount]);
            item.CreditAmount = (Decimal)CleanDataValue(reader[ColumnNames.CreditAmount]);
            item.DebitCreditIndicator = (String)CleanDataValue(reader[ColumnNames.DebitCreditIndicator]);

            return item;
        }
        #endregion
    }
}
