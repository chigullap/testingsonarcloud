﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerSecurityRoleForm : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String SecurityRoleFormId = "security_role_form_id";
            public static String FormId = "form_id";
            public static String Label = "label";
            public static String FormDescription = "description";
            public static String SecurityRoleId = "security_role_id";
            public static String ViewFlag = "view_flag";
            public static String AddFlag = "add_flag";
            public static String UpdateFlag = "update_flag";
            public static String DeleteFlag = "delete_flag";
        }
        #endregion

        #region main
        internal FormSecurityCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long roleId, bool? worklinksAdministrationFlag)
        {
            DataBaseCommand command = GetStoredProcCommand("FormSecurity_select", user.DatabaseName);

            command.AddParameterWithValue("@codeLanguage", user.LanguageCode);
            command.AddParameterWithValue("@roleId", roleId);
            command.AddParameterWithValue("@worklinksAdministrationFlag", worklinksAdministrationFlag);

            using (IDataReader reader = command.ExecuteReader())
                return MapToFormSecurityCollection(reader);
        }
        public FormSecurity InsertSecurityRoleForm(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, FormSecurity form, long roleId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SecurityRoleForm_insert", user.DatabaseName))
            {
                command.BeginTransaction("SecurityRoleForm_insert");

                SqlParameter SecurityRoleFormIdParm = command.AddParameterWithValue("@securityRoleFormId", form.SecurityRoleFormId, ParameterDirection.Output);
                command.AddParameterWithValue("@formId", form.FormId);
                command.AddParameterWithValue("@securityRoleId", roleId);
                command.AddParameterWithValue("@viewFlag", form.ViewFlag);
                command.AddParameterWithValue("@addFlag", form.AddFlag);
                command.AddParameterWithValue("@updateFlag", form.UpdateFlag);
                command.AddParameterWithValue("@deleteFlag", form.DeleteFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", form.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                //retrieve PK
                form.SecurityRoleFormId = Convert.ToInt64(SecurityRoleFormIdParm.Value);
                form.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }

            return form;
        }
        public void UpdateSecurityRoleForm(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, FormSecurity form)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SecurityRoleForm_update", user.DatabaseName))
                {
                    command.BeginTransaction("SecurityRoleForm_update");

                    command.AddParameterWithValue("@securityRoleFormId", form.SecurityRoleFormId);
                    command.AddParameterWithValue("@viewFlag", form.ViewFlag);
                    command.AddParameterWithValue("@addFlag", form.AddFlag);
                    command.AddParameterWithValue("@updateFlag", form.UpdateFlag);
                    command.AddParameterWithValue("@deleteFlag", form.DeleteFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", form.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    form.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void DeleteSecurityRoleForm(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, FormSecurity form, long roleId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SecurityRoleForm_delete", user.DatabaseName))
                {
                    command.BeginTransaction("SecurityRoleForm_delete");

                    command.AddParameterWithValue("@securityRoleFormId", form.SecurityRoleFormId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void UpdateSecurityClienRoleClaim(DatabaseUser user, String authDatabaseName, String ngSecurityClientId, long roleId, string type, FormSecurityCollection forms)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SecurityClientRoleClaim_update", authDatabaseName))
                {
                    command.BeginTransaction("SecurityClientRoleClaim_update");

                    command.AddParameterWithValue("@ngSecurityClientId", ngSecurityClientId);
                    command.AddParameterWithValue("@securityClientRoleIdentifier", roleId);
                    command.AddParameterWithValue("@type", type);

                    SqlParameter parmClaims = new SqlParameter();
                    parmClaims.ParameterName = "@claims";
                    parmClaims.SqlDbType = SqlDbType.Structured;

                    DataTable claims = LoadClaims(forms);
                    parmClaims.Value = claims;
                    command.AddParameter(parmClaims);

                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        private DataTable LoadClaims(FormSecurityCollection forms)
        {
            DataTable table = new DataTable();
            table.Columns.Add("claim_identifier", typeof(string));
            table.Columns.Add("view_flag", typeof(bool));
            table.Columns.Add("add_flag", typeof(bool));
            table.Columns.Add("update_flag", typeof(bool));
            table.Columns.Add("delete_flag", typeof(bool));

            foreach (FormSecurity item in forms)
            {
                if (item.ViewFlag||item.UpdateFlag||item.AddFlag||item.DeleteFlag)
                {
                    table.Rows.Add(new Object[]
                    {
                        item.FormId,
                        item.ViewFlag,
                        item.AddFlag,
                        item.UpdateFlag,
                        item.DeleteFlag
                    });
                }
            }

            return table;
        }
        #endregion



        #region data mapping
        protected FormSecurityCollection MapToFormSecurityCollection(IDataReader reader)
        {
            FormSecurityCollection collection = new FormSecurityCollection();

            while (reader.Read())
                collection.Add(MapToFormSecurity(reader));

            return collection;
        }

        protected FormSecurity MapToFormSecurity(IDataReader reader)
        {
            FormSecurity form = new FormSecurity();
            base.MapToBase(form, reader);

            form.FormId = (long)CleanDataValue(reader[ColumnNames.FormId]);
            form.Label = (String)CleanDataValue(reader[ColumnNames.Label]);
            form.SecurityRoleFormId = (long?)CleanDataValue(reader[ColumnNames.SecurityRoleFormId]);
            form.FormDescription = (String)CleanDataValue(reader[ColumnNames.FormDescription]);
            form.SecurityRoleId = (long?)CleanDataValue(reader[ColumnNames.SecurityRoleId]);
            form.ViewFlag = (bool)CleanDataValue(reader[ColumnNames.ViewFlag]);
            form.AddFlag = (bool)CleanDataValue(reader[ColumnNames.AddFlag]);
            form.UpdateFlag = (bool)CleanDataValue(reader[ColumnNames.UpdateFlag]);
            form.DeleteFlag = (bool)CleanDataValue(reader[ColumnNames.DeleteFlag]);

            return form;
        }
        #endregion
    }
}