﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeRoeAmountDetail : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeRoeAmountDetailId = "employee_roe_amount_detail_id";
            public static String EmployeeRoeAmountId = "employee_roe_amount_id";
            public static String PayrollPeriodId = "payroll_period_id";
            public static String InsurableEarningAmount = "insurable_earning_amount";
            public static String PeriodYear = "period_year";
            public static String Period = "period";
            public static String StartDate = "start_date";
            public static String CutoffDate = "cutoff_date";
            public static String ChequeDate = "cheque_date";
        }
        #endregion

        #region main

        internal EmployeeRoeAmountDetailCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeePositionId, long employeeId, bool securityOverrideFlag)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeeRoeAmount_select", user.DatabaseName);

            command.AddParameterWithValue("@employeePositionId", employeePositionId);
            command.AddParameterWithValue("@employeeId", employeeId);
            command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
            command.AddParameterWithValue("@securityUserId", user.SecurityUserId);
            command.AddParameterWithValue("@securityOverride", securityOverrideFlag);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToEmployeeRoeAmountDetailCollection(reader);
            }
        }

        internal EmployeeRoeAmountDetailSummaryCollection SelectSummary(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeePositionId, long employeeId, bool securityOverrideFlag)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeeRoeAmountDetail_report", user.DatabaseName);

            command.AddParameterWithValue("@employeePositionId", employeePositionId);
            command.AddParameterWithValue("@employeeId", employeeId);
            command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
            command.AddParameterWithValue("@securityUserId", user.SecurityUserId);
            command.AddParameterWithValue("@securityOverride", securityOverrideFlag);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToEmployeeRoeAmountDetailSummaryCollection(reader);
            }
        }

        public EmployeeRoeAmountDetail Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeRoeAmountDetail employeeRoeAmountDetail)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeRoeAmountDetail_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeRoeAmountDetail_update");

                    command.AddParameterWithValue("@employeeRoeAmountDetailId", employeeRoeAmountDetail.EmployeeRoeAmountDetailId);
                    command.AddParameterWithValue("@employeeRoeAmountId", employeeRoeAmountDetail.EmployeeRoeAmountId);
                    command.AddParameterWithValue("@payrollPeriodId", employeeRoeAmountDetail.PayrollPeriodId);
                    command.AddParameterWithValue("@insurableEarningAmount", employeeRoeAmountDetail.InsurableEarningAmount);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", employeeRoeAmountDetail.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    employeeRoeAmountDetail.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();

                    return employeeRoeAmountDetail;
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
                return null;
            }
        }

        public EmployeeRoeAmountDetail Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeRoeAmountDetail employeeRoeAmountDetail)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeRoeAmountDetail_insert",user.DatabaseName))
            {
                command.BeginTransaction("EmployeeRoeAmountDetail_insert");

                SqlParameter employeeRoeAmountDetailIdParm = command.AddParameterWithValue("@employeeRoeAmountDetailId", employeeRoeAmountDetail.EmployeeRoeAmountDetailId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeRoeAmountId", employeeRoeAmountDetail.EmployeeRoeAmountId);
                command.AddParameterWithValue("@payrollPeriodId", employeeRoeAmountDetail.PayrollPeriodId);
                command.AddParameterWithValue("@insurableEarningAmount", employeeRoeAmountDetail.InsurableEarningAmount);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", employeeRoeAmountDetail.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                employeeRoeAmountDetail.EmployeeRoeAmountDetailId = Convert.ToInt64(employeeRoeAmountDetailIdParm.Value);
                employeeRoeAmountDetail.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();

                return employeeRoeAmountDetail;
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeRoeAmount employeeRoeAmount)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeRoeAmount_delete", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeRoeAmount_delete");
                command.AddParameterWithValue("@employeeRoeAmountId", employeeRoeAmount.EmployeeRoeAmountId);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        #endregion

        #region data mapping
        protected EmployeeRoeAmountDetailCollection MapToEmployeeRoeAmountDetailCollection(IDataReader reader)
        {
            EmployeeRoeAmountDetailCollection collection = new EmployeeRoeAmountDetailCollection();
            while (reader.Read())
            {
                collection.Add(MapToEmployeeRoeAmountDetail(reader));
            }

            return collection;
        }
        protected EmployeeRoeAmountDetail MapToEmployeeRoeAmountDetail(IDataReader reader)
        {
            EmployeeRoeAmountDetail employeeRoeDetail = new EmployeeRoeAmountDetail();
            base.MapToBase(employeeRoeDetail, reader);

            employeeRoeDetail.EmployeeRoeAmountDetailId = (long)CleanDataValue(reader[ColumnNames.EmployeeRoeAmountDetailId]);
            employeeRoeDetail.EmployeeRoeAmountId = (long)CleanDataValue(reader[ColumnNames.EmployeeRoeAmountId]);
            employeeRoeDetail.PayrollPeriodId = (long)CleanDataValue(reader[ColumnNames.PayrollPeriodId]);
            employeeRoeDetail.InsurableEarningAmount = (Decimal)CleanDataValue(reader[ColumnNames.InsurableEarningAmount]);

            return employeeRoeDetail;
        }
        protected EmployeeRoeAmountDetailSummaryCollection MapToEmployeeRoeAmountDetailSummaryCollection(IDataReader reader)
        {
            EmployeeRoeAmountDetailSummaryCollection collection = new EmployeeRoeAmountDetailSummaryCollection();
            while (reader.Read())
            {
                collection.Add(MapToEmployeeRoeAmountDetailSummary(reader));
            }

            return collection;
        }
        protected EmployeeRoeAmountDetailSummary MapToEmployeeRoeAmountDetailSummary(IDataReader reader)
        {
            EmployeeRoeAmountDetailSummary employeeRoeDetailSummary = new EmployeeRoeAmountDetailSummary();
            base.MapToBase(employeeRoeDetailSummary, reader);

            employeeRoeDetailSummary.EmployeeRoeAmountDetailId = (long)CleanDataValue(reader[ColumnNames.EmployeeRoeAmountDetailId]);
            employeeRoeDetailSummary.EmployeeRoeAmountId = (long)CleanDataValue(reader[ColumnNames.EmployeeRoeAmountId]);
            employeeRoeDetailSummary.PayrollPeriodId = (long)CleanDataValue(reader[ColumnNames.PayrollPeriodId]);
            employeeRoeDetailSummary.InsurableEarningAmount = (Decimal)CleanDataValue(reader[ColumnNames.InsurableEarningAmount]);
            employeeRoeDetailSummary.PeriodYear = (Decimal)CleanDataValue(reader[ColumnNames.PeriodYear]);
            employeeRoeDetailSummary.Period = (Decimal)CleanDataValue(reader[ColumnNames.Period]);
            employeeRoeDetailSummary.StartDate = (DateTime)CleanDataValue(reader[ColumnNames.StartDate]);
            employeeRoeDetailSummary.CutoffDate = (DateTime)CleanDataValue(reader[ColumnNames.CutoffDate]);
            employeeRoeDetailSummary.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);

            return employeeRoeDetailSummary;
        }
        #endregion
    }
}