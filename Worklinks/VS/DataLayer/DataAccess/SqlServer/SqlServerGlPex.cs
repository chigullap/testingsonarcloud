﻿using System;
using System.Data;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerGlPex : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string GeneralLedgerAccount = "general_ledger_account";
            public static string DebitAmount = "debit_amount";
            public static string CreditAmount = "credit_amount";
            public static string PeriodYear = "period_year";
            public static string Period = "period";
            public static string Sequence = "sequence";
            public static string ChequeDate = "cheque_date";
            public static string GlAccountDescription = "gl_account_description";
            public static string CostCenter = "cost_center";
            public static string PayElement = "pay_element";
            public static string PayServId = "pay_serv_id";
            public static string HRISID = "hrisid";
            public static string Hours = "hours";
        }
        #endregion

        #region main
        internal GlPexCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            DataBaseCommand command = GetStoredProcCommand("GLReport_select", user.DatabaseName);
            command.AddParameterWithValue("@databaseName", user.DatabaseName);
            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToGlPexCollection(reader);
            }
        }
        #endregion

        #region data mapping
        protected GlPexCollection MapToGlPexCollection(IDataReader reader)
        {
            int i = 1;
            GlPexCollection collection = new GlPexCollection();
            while (reader.Read())
            {
                collection.Add(MapToGlPex(reader, i++));
            }

            return collection;
        }

        protected GlPex MapToGlPex(IDataReader reader, int key)
        {
            GlPex item = new GlPex();

            item.GlPexId = Convert.ToInt64(key);

            item.GeneralLedgerAccount = (string)CleanDataValue(reader[ColumnNames.GeneralLedgerAccount]);
            item.DebitAmount = (decimal)CleanDataValue(reader[ColumnNames.DebitAmount]);
            item.CreditAmount = (decimal)CleanDataValue(reader[ColumnNames.CreditAmount]);
            item.PeriodYear = (decimal)CleanDataValue(reader[ColumnNames.PeriodYear]);
            item.Period = (decimal)CleanDataValue(reader[ColumnNames.Period]);
            item.Sequence = (decimal)CleanDataValue(reader[ColumnNames.Sequence]);
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.GlAccountDescription = (string)CleanDataValue(reader[ColumnNames.GlAccountDescription]);
            item.CostCenter = (string)CleanDataValue(reader[ColumnNames.CostCenter]);
            item.PayElement = (string)CleanDataValue(reader[ColumnNames.PayElement]);
            item.PayServId = (string)CleanDataValue(reader[ColumnNames.PayServId]);
            item.HRISID = (string)CleanDataValue(reader[ColumnNames.HRISID]);
            item.Hours = (string)CleanDataValue(reader[ColumnNames.Hours]);

            return item;
        }
        #endregion
    }
}
