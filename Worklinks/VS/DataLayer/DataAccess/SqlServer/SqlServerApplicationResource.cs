﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections.Specialized;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;


namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerApplicationResource : SqlServerBase
    {
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ResourceKey = "resource_key";
            public static String Value = "value";
        }

        public ApplicationResourceCollection GetResourceByCultureAndKey(String databaseName, long securityRoleId, String languageCode, String virtualPath, String resourceKey)
        {
            using (DataBaseCommand command = GetStoredProcCommand("GetApplicationResource", databaseName))
            {
                command.AddParameterWithValue("@virtualPath", virtualPath);
                command.AddParameterWithValue("@resourceKey", resourceKey);
                command.AddParameterWithValue("@languageCode", languageCode);
                command.AddParameterWithValue("@securityRoleId", securityRoleId);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapResourceToListDictionary(reader);
                }
            }
        }

        #region data mapping
        internal ApplicationResourceCollection MapResourceToListDictionary(IDataReader reader)
        {
            ApplicationResourceCollection collection = new ApplicationResourceCollection();
            while (reader.Read())
            {
                String key = (String)CleanDataValue(reader[ColumnNames.ResourceKey]);
                String value = (String)CleanDataValue(reader[ColumnNames.Value]);
                collection.Add(new ApplicationResource() { Key = key, Value = value });
            }

            return collection;
        }
 
        #endregion

    }
}
