﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.CalcModel;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerPayrollTransaction : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string PayrollTransactionId = "payroll_transaction_id";
            public static string PayrollBatchId = "payroll_batch_id";
            public static string EmployeeId = "employee_id";
            public static string EmployeeNumber = "employee_number";
            public static string EmployeeName = "employee_name";
            public static string TransactionDate = "transaction_date";
            public static string PaycodeCode = "code_paycode_cd";
            public static string PayCodeTypeCode = "code_paycode_type_cd";
            public static string PaycodeDescription = "paycode_description";
            public static string PaycodeTypeDescription = "paycode_type_description";
            public static string SubtractHourFromSalaryFlag = "subtract_hour_from_salary_flag";
            public static string Units = "units";
            public static string Rate = "rate";
            public static string EntryTypeCode = "code_entry_type_cd";
            public static string EmploymentInsuranceHour = "employment_insurance_hour";
            public static string OverrideBonusDeductionBeforeTaxPercent = "override_bonus_deduction_before_tax_percent";
            public static string FederalTaxAmount = "federal_tax_amount";
            public static string ProvincialTaxAmount = "provincial_tax_amount";
            public static string BaseSalaryRate = "base_salary_rate";
            public static string OrganizationUnit = "organization_unit";
            public static string OrganizationUnitDescription = "organization_unit_description";
            public static string RetroactiveNumberOfPayPeriods = "retroactive_number_of_pay_periods";
            public static string YearlyMaximum = "yearly_maximum";
            public static string TotalAmount = "total_amount";
            public static string TransactionStartTime = "transaction_start_time";
            public static string TransactionEndTime = "transaction_end_time";
            public static string RelatedPayrollTransactionId = "related_payroll_transaction_id";
            public static string IncomeTaxFlag = "income_tax_flag";
            public static string QuebecTaxFlag = "quebec_tax_flag";
            public static string TaxOverrideFlag = "tax_override_flag";
            public static string CashTaxableFlag = "cash_taxable_flag";
            public static string CanadaQuebecPensionPlanFlag = "canada_quebec_pension_plan_flag";
            public static string ProvincialParentalInsurancePlanFlag = "provincial_parental_insurance_plan_flag";
            public static string EmploymentInsuranceFlag = "employment_insurance_flag";
            public static string BeforeIncomeTaxDeductionFlag = "before_income_tax_deduction_flag";
            public static string BeforeBonusTaxDeductionFlag = "before_bonus_tax_deduction_flag";
            public static string WorkersCompensationBoardFlag = "workers_compensation_board_flag";
            public static string ProvincialHealthTaxFlag = "provincial_health_tax_flag";
            public static string PaycodeTypeCode = "code_paycode_type_cd";
            public static string PaycodeIncomePayTypeCode = "code_paycode_income_pay_type_cd";
            public static string PaycodeRateBasedOnCode = "code_paycode_rate_based_on_cd";
            public static string OriginalImportFlag = "original_import_flag";

            //for employee paycode calcs
            public static string PayrollProcessRunTypeCode = "code_payroll_process_run_type_cd";
            public static string EmployeePaycodeId = "employee_paycode_id";
            public static string PaycodeActivationFrequencyCode = "code_paycode_activation_frequency_cd";
            public static string PayrollCountThisMonth = "payroll_count_this_month";
            public static string RequiredMinimumIncomeAmount = "required_minimum_income_amount";
            public static string YearlyMaximumAmount = "yearly_maximum_amount";
            public static string PeriodMaximumAmount = "period_maximum_amount";
            public static string RecurringIncomeCodeFlag = "recurring_income_code_flag";
            public static string YtdIncomeAmount = "ytd_income_amount";
            public static string YtdPaycodeAmount = "ytd_paycode_amount";
            public static string PeriodToDateAmount = "period_to_date_amount";
            public static string IncomeOrganizationUnit = "income_organization_unit";
            public static string IncomeAmount = "income_amount";
            public static string AmountPercentage = "amount_percentage";
            public static string AmountRate = "amount_rate";
            public static string AmountUnits = "amount_units";
            public static string ArrearsOffsetPaycodeCode = "arrears_offset_code_paycode_cd";
            public static string IncludeInArrearsFlag = "include_in_arrears_flag";

            //for rate progression
            public static string CurrentPeriodPlanHour = "current_period_plan_hour";
            public static string CurrentPeriodGradeHour = "current_period_grade_hour";
            public static string SalaryPlanId = "salary_plan_id";
            public static string SalaryPlanGradeId = "salary_plan_grade_id";
            public static string SalaryPlanGradeStepId = "salary_plan_grade_step_id";
            public static string CurrentStepAmount = "current_step_amount";
            public static string PreviousTotalGradeHour = "previous_total_grade_hour";
            public static string PreviousTotalPlanHour = "previous_total_plan_hour";
            public static string AdvancementToMinimumHour = "advancement_to_minimum_hour";
            public static string SalaryPlanAdvanceBasedOnCode = "code_salary_plan_advance_based_on_cd";
            public static string AdvanceToAmount = "advance_to_amount";
            public static string ProbationHour = "probation_hour";
            public static string AdvanceSalaryPlanGradeStepId = "advance_salary_plan_grade_step_id";
            public static string AmountRateFactor = "amount_rate_factor";
            public static string PayrollProcessId = "payroll_process_id";
            public static string AutoPopulateRateFlag = "auto_populate_rate_flag";

            //bonus
            public static string MinimumHour = "minimum_hour";
            public static string MaximumHour = "maximum_hour";
            public static string Amount = "amount";
            public static string OutputPaycode = "output_code_paycode_cd";
        }
        #endregion

        #region select
        public PayrollTransactionPayrollMasterCollection GetPayrollTransactionPayrollMaster(DatabaseUser user, long[] batchIds)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollTransaction_PayrollMaster", user.DatabaseName))
            {
                SqlParameter table = new SqlParameter();
                table.ParameterName = "@parmPayrollBatchId";
                table.SqlDbType = SqlDbType.Structured;

                DataTable tablePayrollMaster = LoadPayrollBatchId(batchIds);
                table.Value = tablePayrollMaster;
                command.AddParameter(table);

                PayrollTransactionPayrollMasterCollection coll = null;

                using (IDataReader reader = command.ExecuteReader())
                    coll = MapToPayrollTransactionPayrollMasterCollection(reader);

                return coll;
            }
        }
        public BonusRuleCollection GetBonusRule(DatabaseUser user, DateTime cutoffDate)
        {
            using (DataBaseCommand command = GetStoredProcCommand("BonusRule_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@cutoffDate", cutoffDate);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToBonusRuleCollection(reader);
            }
        }
        public PayrollTransactionEmployeePaycodeCollection GetPayrollTransactionEmployeePaycode(DatabaseUser user, long payrollProcessId)
        {
            DataBaseCommand command = GetStoredProcCommand("PayrollTransaction_EmployeePaycode", user.DatabaseName);

            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToPayrollTransactionEmployeePaycodeCollection(reader);
        }
        public EmployeePaycodeCalculationCollection GetEmployeePaycodeAmount(DatabaseUser user, long payrollProcessId, bool newArrearsProcessingFlag)
        {
            DataBaseCommand command = GetStoredProcCommand("payrolltransaction_generateEmployeePaycodeAmount", user.DatabaseName);

            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

            using (IDataReader reader = command.ExecuteReader())
                return LoadEmployeePaycodeCalculationCollection(reader, newArrearsProcessingFlag);
        }
        public PayrollTransactionCollection CheckForBonusWithDeduction(DatabaseUser user, long[] batchIds)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePaycodeBonusWithDeduction_select", user.DatabaseName))
            {
                SqlParameter table = new SqlParameter();
                table.ParameterName = "@parmPayrollBatchId";
                table.SqlDbType = SqlDbType.Structured;

                DataTable tablePayrollMaster = LoadPayrollBatchId(batchIds);
                table.Value = tablePayrollMaster;
                command.AddParameter(table);

                PayrollTransactionCollection coll = null;

                using (IDataReader reader = command.ExecuteReader())
                    coll = MapToMapToEmployeePaycodeBonusWithDeduction_selectCollection(reader);

                return coll;
            }
        }
        private DataTable LoadPayrollBatchId(long[] batchIds)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("payroll_batch_id", typeof(long));

            foreach (long batchId in batchIds)
                table.Rows.Add(new Object[] { batchId });

            return table;
        }
        internal PayrollTransactionSummaryCollection SelectPayrollTransactionSummary(DatabaseUser user, long payrollBatchId)
        {
            DataBaseCommand command = GetStoredProcCommand("PayrollTransaction_report", user.DatabaseName);

            command.AddParameterWithValue("@payrollBatchId", payrollBatchId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToPayrollTransactionSummaryCollection(reader);
        }
        internal PayrollTransactionRateProgressionCollection SelectPayrollTransactionRateProgression(DatabaseUser user, long payrollProcessId)
        {
            DataBaseCommand command = GetStoredProcCommand("PayrollTransactionRateProgression_select", user.DatabaseName);

            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToPayrollTransactionRateProgressionCollection(reader);
        }
        internal PayrollTransactionRateProgressionBonusCollection SelectPayrollTransactionRateProgressionBonus(DatabaseUser user, long payrollProcessId, BonusRuleCollection rule)
        {
            DataBaseCommand command = GetStoredProcCommand("PayrollTransactionBonus_select", user.DatabaseName);

            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToPayrollTransactionRateProgressionBonusCollection(reader, rule);
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, PayrollTransaction item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollTransaction_update", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollTransaction_update");

                    command.AddParameterWithValue("@payrollTransactionId", item.PayrollTransactionId);
                    command.AddParameterWithValue("@payrollBatchId", item.PayrollBatchId);
                    command.AddParameterWithValue("@employeeId", item.EmployeeId);
                    command.AddParameterWithValue("@paycodeCode", item.PaycodeCode);
                    command.AddParameterWithValue("@entryTypeCode", item.EntryTypeCode);
                    command.AddParameterWithValue("@rate", item.Rate);
                    command.AddParameterWithValue("@units", item.Units);
                    command.AddParameterWithValue("@transactionDate", item.TransactionDate);
                    command.AddParameterWithValue("@employmentInsuranceHour", item.EmploymentInsuranceHour);
                    command.AddParameterWithValue("@federalTaxAmount", item.FederalTaxAmount);
                    command.AddParameterWithValue("@provincialTaxAmount", item.ProvincialTaxAmount);
                    command.AddParameterWithValue("@baseSalaryRate", item.BaseSalaryRate);
                    command.AddParameterWithValue("@organizationUnit", item.OrganizationUnit);
                    command.AddParameterWithValue("@retroactiveNumberOfPayPeriods", item.RetroactiveNumberOfPayPeriods);
                    command.AddParameterWithValue("@transactionStartTime", item.TransactionStartTime);
                    command.AddParameterWithValue("@transactionEndTime", item.TransactionEndTime);
                    command.AddParameterWithValue("@relatedPayrollTransactionId", item.RelatedPayrollTransactionId);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region insert
        public void Insert(DatabaseUser user, PayrollTransaction item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollTransaction_insert", user.DatabaseName))
            {
                command.BeginTransaction("PayrollTransaction_insert");

                SqlParameter payrollTransactionIdParam = command.AddParameterWithValue("@payrollTransactionId", item.PayrollTransactionId, ParameterDirection.Output);
                command.AddParameterWithValue("@payrollBatchId", item.PayrollBatchId);
                command.AddParameterWithValue("@employeeId", item.EmployeeId);
                command.AddParameterWithValue("@rate", item.Rate);
                command.AddParameterWithValue("@units", item.Units);
                command.AddParameterWithValue("@transactionDate", item.TransactionDate);
                command.AddParameterWithValue("@paycodeCode", item.PaycodeCode);
                command.AddParameterWithValue("@entryTypeCode", item.EntryTypeCode);
                command.AddParameterWithValue("@employmentInsuranceHour", item.EmploymentInsuranceHour);
                command.AddParameterWithValue("@federalTaxAmount", item.FederalTaxAmount);
                command.AddParameterWithValue("@provincialTaxAmount", item.ProvincialTaxAmount);
                command.AddParameterWithValue("@baseSalaryRate", item.BaseSalaryRate);
                command.AddParameterWithValue("@organizationUnit", item.OrganizationUnit);
                command.AddParameterWithValue("@retroactiveNumberOfPayPeriods", item.RetroactiveNumberOfPayPeriods);
                command.AddParameterWithValue("@transactionStartTime", item.TransactionStartTime);
                command.AddParameterWithValue("@transactionEndTime", item.TransactionEndTime);
                command.AddParameterWithValue("@relatedPayrollTransactionId", item.RelatedPayrollTransactionId);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.PayrollTransactionId = Convert.ToInt64(payrollTransactionIdParam.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }
        public PayrollTransactionErrorCollection InsertBatch(DatabaseUser user, PayrollTransactionCollection items)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollTransaction_batchInsert", user.DatabaseName))
            {
                command.BeginTransaction("PayrollTransaction_batchInsert");

                SqlParameter parmPayrollTransaction = new SqlParameter();
                parmPayrollTransaction.ParameterName = "@parmPayrollTransaction";
                parmPayrollTransaction.SqlDbType = SqlDbType.Structured;

                DataTable tablePayrollMaster = LoadPayrollTransactionDataTable(items);
                parmPayrollTransaction.Value = tablePayrollMaster;
                command.AddParameter(parmPayrollTransaction);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();

                return new PayrollTransactionErrorCollection();
            }
        }
        private DataTable LoadPayrollTransactionDataTable(PayrollTransactionCollection items)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("payroll_transaction_id", typeof(long));
            table.Columns.Add("payroll_batch_id", typeof(long));
            table.Columns.Add("employee_id", typeof(long));
            table.Columns.Add("transaction_date", typeof(DateTime));
            table.Columns.Add("code_paycode_cd", typeof(String));
            table.Columns.Add("code_entry_type_cd", typeof(String));
            table.Columns.Add("units", typeof(Decimal));
            table.Columns.Add("rate", typeof(Decimal));
            table.Columns.Add("base_salary_rate", typeof(Decimal));
            table.Columns.Add("employment_insurance_hour", typeof(Decimal));
            table.Columns.Add("federal_tax_amount", typeof(Decimal));
            table.Columns.Add("provincial_tax_amount", typeof(Decimal));
            table.Columns.Add("import_external_identifier", typeof(String));
            table.Columns.Add("organization_unit", typeof(String));
            table.Columns.Add("transaction_start_time", typeof(TimeSpan));
            table.Columns.Add("transaction_end_time", typeof(TimeSpan));
            table.Columns.Add("original_import_flag", typeof(bool));

            //loop thru and add values
            foreach (PayrollTransaction item in items)
            {
                table.Rows.Add(new Object[]
                {
                    item.PayrollTransactionId,
                    item.PayrollBatchId,
                    item.EmployeeId,
                    item.TransactionDate,
                    item.PaycodeCode,
                    item.EntryTypeCode,
                    item.Units,
                    item.Rate,
                    item.BaseSalaryRate,
                    item.EmploymentInsuranceHour,
                    item.FederalTaxAmount,
                    item.ProvincialTaxAmount,
                    item.ImportExternalIdentifier,
                    item.OrganizationUnit,
                    item.TransactionStartTime,
                    item.TransactionEndTime,
                    item.OriginalImportFlag
                });
            }

            return table;
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, PayrollTransaction item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollTransaction_delete", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollTransaction_delete");

                    command.AddParameterWithValue("@PayrollTransactionId", item.PayrollTransactionId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void DeleteByBatchId(DatabaseUser user, long payrollBatchId)
        {
            try
            {
                DataBaseCommand command = GetStoredProcCommand("PayrollTransactionsMulti_delete", user.DatabaseName);

                using (command)
                {
                    command.BeginTransaction("PayrollTransactionsMulti_delete");

                    command.AddParameterWithValue("@payrollBatchId", payrollBatchId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void DeleteImportBatchCascade(DatabaseUser user, string codePayrollProcessRunTypeCode, string codePayrollProcessGroupCd)
        {
            try
            {
                DataBaseCommand command = GetStoredProcCommand("PayrollBatchFtpImportMulti_delete", user.DatabaseName);

                using (command)
                {
                    command.BeginTransaction("PayrollBatchFtpImportMulti_delete");

                    command.AddParameterWithValue("@codePayrollProcessRunTypeCd", codePayrollProcessRunTypeCode);
                    command.AddParameterWithValue("@codePayrollProcessGroupCd", codePayrollProcessGroupCd);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        #endregion

        #region statutory deduction import service
        public void ImportPayrollTransaction(String database, String username, System.Collections.Generic.List<PayrollTransactionImportUsedInService> trans)
        {
            DataBaseCommand command = GetStoredProcCommand("PayrollTransaction_batchInsert", database);

            using (command)
            {
                command.BeginTransaction("PayrollTransaction_batchInsert");

                //table parameter
                SqlParameter parmPayrollTransaction = new SqlParameter();
                parmPayrollTransaction.ParameterName = "@parmPayrollTransaction";
                parmPayrollTransaction.SqlDbType = SqlDbType.Structured;

                DataTable tablePayrollTransaction = LoadPayrollTransactionDataTableImport(trans);
                parmPayrollTransaction.Value = tablePayrollTransaction;
                command.AddParameter(parmPayrollTransaction);

                command.AddParameterWithValue("@updateUser", username);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        private DataTable LoadPayrollTransactionDataTableImport(System.Collections.Generic.List<PayrollTransactionImportUsedInService> trans)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("payroll_transaction_id", typeof(long));
            table.Columns.Add("payroll_batch_id", typeof(long));
            table.Columns.Add("employee_id", typeof(long));
            table.Columns.Add("transaction_date", typeof(DateTime));
            table.Columns.Add("code_paycode_cd", typeof(String));
            table.Columns.Add("code_entry_type_cd", typeof(String));
            table.Columns.Add("units", typeof(Decimal));
            table.Columns.Add("rate", typeof(Decimal));
            table.Columns.Add("base_salary_rate", typeof(Decimal));
            table.Columns.Add("employment_insurance_hour", typeof(Decimal));
            table.Columns.Add("federal_tax_amount", typeof(Decimal));
            table.Columns.Add("provincial_tax_amount", typeof(Decimal));
            table.Columns.Add("import_external_identifier", typeof(String));

            //loop thru and add values
            foreach (PayrollTransactionImportUsedInService item in trans)
            {
                table.Rows.Add(new Object[]
                {
                    -1,
                    -1,
                    -1,
                    item.TransactionDate,
                    item.PaycodeCode,
                    "NORM",
                    item.Units,
                    item.Rate,
                    null,
                    0,
                    null,
                    null,
                    item.EmployeeNumber
                });
            }

            return table;
        }

        #endregion

        #region data mapping
        protected PayrollTransactionRateProgressionCollection MapToPayrollTransactionRateProgressionCollection(IDataReader reader)
        {
            PayrollTransactionRateProgressionCollection collection = new PayrollTransactionRateProgressionCollection();

            while (reader.Read())
                collection.Add(MapToPayrollTransactionRateProgression(reader));

            return collection;
        }
        protected PayrollTransactionRateProgressionBonusCollection MapToPayrollTransactionRateProgressionBonusCollection(IDataReader reader, BonusRuleCollection rule)
        {
            PayrollTransactionRateProgressionBonusCollection collection = new PayrollTransactionRateProgressionBonusCollection();

            while (reader.Read())
                collection.Add(MapToPayrollTransactionRateProgressionBonus(reader, rule));

            return collection;
        }
        protected PayrollTransactionEmployeePaycodeCollection MapToPayrollTransactionEmployeePaycodeCollection(IDataReader reader)
        {
            PayrollTransactionEmployeePaycodeCollection collection = new PayrollTransactionEmployeePaycodeCollection();

            while (reader.Read())
                collection.Add(MapToPayrollTransactionEmployeePaycode(reader));

            return collection;
        }
        protected EmployeePaycodeCalculationCollection LoadEmployeePaycodeCalculationCollection(IDataReader reader, bool newArrearsProcessingFlag)
        {
            EmployeePaycodeCalculationCollection collection = new EmployeePaycodeCalculationCollection(newArrearsProcessingFlag);

            while (reader.Read())
            {
                long employeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
                long employeePaycodeId = (long)CleanDataValue(reader[ColumnNames.EmployeePaycodeId]);
                String paycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
                String payrollProcessRunTypeCode = (String)CleanDataValue(reader[ColumnNames.PayrollProcessRunTypeCode]);
                String paycodeActivationFrequencyCode = (String)CleanDataValue(reader[ColumnNames.PaycodeActivationFrequencyCode]);
                int payrollCountThisMonth = (int)CleanDataValue(reader[ColumnNames.PayrollCountThisMonth]);
                Decimal? requiredMinimumIncomeAmount = (Decimal?)CleanDataValue(reader[ColumnNames.RequiredMinimumIncomeAmount]);
                Decimal? yearlyMaximumAmount = (Decimal?)CleanDataValue(reader[ColumnNames.YearlyMaximumAmount]);
                Decimal? periodMaximumAmount = (Decimal?)CleanDataValue(reader[ColumnNames.PeriodMaximumAmount]);
                bool? beforeBonusTaxDeductionFlag = (bool?)CleanDataValue(reader[ColumnNames.BeforeBonusTaxDeductionFlag]);
                bool? beforeIncomeTaxDeductionFlag = (bool?)CleanDataValue(reader[ColumnNames.BeforeIncomeTaxDeductionFlag]);
                String paycodeTypeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeTypeCode]);
                String paycodeIncomePayTypeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeIncomePayTypeCode]);
                String paycodeRateBasedOnCode = (String)CleanDataValue(reader[ColumnNames.PaycodeRateBasedOnCode]);
                bool recurringIncomeCodeFlag = (bool)CleanDataValue(reader[ColumnNames.RecurringIncomeCodeFlag]);
                String organizationUnit = (String)CleanDataValue(reader[ColumnNames.OrganizationUnit]);
                Decimal? ytdIncomeAmount = (Decimal?)CleanDataValue(reader[ColumnNames.YtdIncomeAmount]);
                Decimal? ytdPaycodeAmount = (Decimal?)CleanDataValue(reader[ColumnNames.YtdPaycodeAmount]);
                Decimal? periodToDateAmount = (Decimal?)CleanDataValue(reader[ColumnNames.PeriodToDateAmount]);
                //String incomeOrganizationUnit = (String)CleanDataValue(reader[ColumnNames.IncomeOrganizationUnit]);
                //Decimal? incomeAmount = (Decimal?)CleanDataValue(reader[ColumnNames.IncomeAmount]);
                Decimal? amountPercentage = (Decimal?)CleanDataValue(reader[ColumnNames.AmountPercentage]);
                Decimal? amountRate = (Decimal?)CleanDataValue(reader[ColumnNames.AmountRate]);
                Decimal? amountUnits = (Decimal?)CleanDataValue(reader[ColumnNames.AmountUnits]);
                bool includeInArrearsFlag = (bool)CleanDataValue(reader[ColumnNames.IncludeInArrearsFlag]);
                String arrearsOffsetPaycodeCode = (String)CleanDataValue(reader[ColumnNames.ArrearsOffsetPaycodeCode]);
                //String paycodeIncomePayTypeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeIncomePayTypeCode]);

                collection.Add
                    (
                        newArrearsProcessingFlag,
                        employeeId,
                        employeePaycodeId,
                        paycodeCode,
                        payrollProcessRunTypeCode,
                        paycodeActivationFrequencyCode,
                        payrollCountThisMonth,
                        requiredMinimumIncomeAmount,
                        yearlyMaximumAmount,
                        periodMaximumAmount,
                        beforeBonusTaxDeductionFlag,
                        beforeIncomeTaxDeductionFlag,
                        paycodeTypeCode,
                        paycodeIncomePayTypeCode,
                        paycodeRateBasedOnCode,
                        recurringIncomeCodeFlag,
                        organizationUnit,
                        ytdIncomeAmount,
                        ytdPaycodeAmount,
                        periodToDateAmount,
                        //                        incomeOrganizationUnit,
                        //                        incomeAmount,
                        amountPercentage,
                        amountRate,
                        amountUnits,
                        includeInArrearsFlag,
                        arrearsOffsetPaycodeCode
                    //                        paycodeIncomePayTypeCode
                    );
            }
            return collection;
        }
        protected PayrollTransactionPayrollMasterCollection MapToPayrollTransactionPayrollMasterCollection(IDataReader reader)
        {
            PayrollTransactionPayrollMasterCollection collection = new PayrollTransactionPayrollMasterCollection();

            while (reader.Read())
                collection.Add(MapToPayrollTransactionPayrollMaster(reader));

            return collection;
        }
        protected PayrollTransactionSummaryCollection MapToPayrollTransactionSummaryCollection(IDataReader reader)
        {
            PayrollTransactionSummaryCollection collection = new PayrollTransactionSummaryCollection();

            while (reader.Read())
                collection.Add(MapToPayrollTransactionSummary(reader));

            return collection;
        }
        protected PayrollTransactionSummary MapToPayrollTransactionSummary(IDataReader reader)
        {
            PayrollTransactionSummary item = new PayrollTransactionSummary();
            base.MapToBase(item, reader);

            item.PayrollTransactionId = (long)CleanDataValue(reader[ColumnNames.PayrollTransactionId]);
            item.PayrollBatchId = (long)CleanDataValue(reader[ColumnNames.PayrollBatchId]);
            item.EmployeeNumber = (String)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EmployeeName = (String)CleanDataValue(reader[ColumnNames.EmployeeName]);
            item.TransactionDate = (DateTime)CleanDataValue(reader[ColumnNames.TransactionDate]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.PaycodeDescription = (string) CleanDataValue(reader[ColumnNames.PaycodeDescription]);
            item.SubtractHourFromSalaryFlag = (bool)CleanDataValue(reader[ColumnNames.SubtractHourFromSalaryFlag]);
            item.EntryTypeCode = (String)CleanDataValue(reader[ColumnNames.EntryTypeCode]);
            item.PaycodeTypeCode = (String)CleanDataValue(reader[ColumnNames.PayCodeTypeCode]);
            item.PaycodeTypeDescription = (string)CleanDataValue(reader[ColumnNames.PaycodeTypeDescription]);
            item.Units = (Decimal)CleanDataValue(reader[ColumnNames.Units]);
            item.Rate = (Decimal)CleanDataValue(reader[ColumnNames.Rate]);
            item.EmploymentInsuranceHour = (Decimal)CleanDataValue(reader[ColumnNames.EmploymentInsuranceHour]);
            item.FederalTaxAmount = (Decimal?)CleanDataValue(reader[ColumnNames.FederalTaxAmount]);
            item.ProvincialTaxAmount = (Decimal?)CleanDataValue(reader[ColumnNames.ProvincialTaxAmount]);
            item.BaseSalaryRate = (Decimal?)CleanDataValue(reader[ColumnNames.BaseSalaryRate]);
            item.OrganizationUnit = (String)CleanDataValue(reader[ColumnNames.OrganizationUnit]);
            item.OrganizationUnitDescription = (String)CleanDataValue(reader[ColumnNames.OrganizationUnitDescription]);
            item.RetroactiveNumberOfPayPeriods = (int?)CleanDataValue(reader[ColumnNames.RetroactiveNumberOfPayPeriods]);
            item.TransactionStartTime = (TimeSpan?)CleanDataValue(reader[ColumnNames.TransactionStartTime]);
            item.TransactionEndTime = (TimeSpan?)CleanDataValue(reader[ColumnNames.TransactionEndTime]);
            item.RelatedPayrollTransactionId = (long?)CleanDataValue(reader[ColumnNames.RelatedPayrollTransactionId]);

            return item;
        }
        protected PayrollTransactionCollection MapToMapToEmployeePaycodeBonusWithDeduction_selectCollection(IDataReader reader)
        {
            PayrollTransactionCollection collection = new PayrollTransactionCollection();
            int i = -1;

            while (reader.Read())
                collection.Add(MapToEmployeePaycodeBonusWithDeduction_select(reader, i--));

            return collection;
        }
        protected PayrollTransactionPayrollMaster MapToPayrollTransactionPayrollMaster(IDataReader reader)
        {
            PayrollTransactionPayrollMaster item = new PayrollTransactionPayrollMaster();

            base.MapToBase(item, reader);

            item.PayrollTransactionId = (long)CleanDataValue(reader[ColumnNames.PayrollTransactionId]);
            item.PayrollBatchId = (long)CleanDataValue(reader[ColumnNames.PayrollBatchId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.TransactionDate = (DateTime)CleanDataValue(reader[ColumnNames.TransactionDate]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.EntryTypeCode = (String)CleanDataValue(reader[ColumnNames.EntryTypeCode]);
            item.Units = (Decimal)CleanDataValue(reader[ColumnNames.Units]);
            item.Rate = (Decimal)CleanDataValue(reader[ColumnNames.Rate]);
            item.EmploymentInsuranceHour = Convert.ToDecimal(CleanDataValue(reader[ColumnNames.EmploymentInsuranceHour]));
            item.OverrideBonusDeductionBeforeTaxPercent = (Decimal?)CleanDataValue(reader[ColumnNames.OverrideBonusDeductionBeforeTaxPercent]);
            item.FederalTaxAmount = (Decimal?)CleanDataValue(reader[ColumnNames.FederalTaxAmount]);
            item.ProvincialTaxAmount = (Decimal?)CleanDataValue(reader[ColumnNames.ProvincialTaxAmount]);
            item.BaseSalaryRate = (Decimal?)CleanDataValue(reader[ColumnNames.BaseSalaryRate]);
            item.TransactionStartTime = (TimeSpan?)CleanDataValue(reader[ColumnNames.TransactionStartTime]);
            item.TransactionEndTime = (TimeSpan?)CleanDataValue(reader[ColumnNames.TransactionEndTime]);
            item.RelatedPayrollTransactionId = (long?)CleanDataValue(reader[ColumnNames.RelatedPayrollTransactionId]);
            item.OrganizationUnit = (String)CleanDataValue(reader[ColumnNames.OrganizationUnit]);

            item.IncomeTaxFlag = (bool)CleanDataValue(reader[ColumnNames.IncomeTaxFlag]);
            item.QuebecTaxFlag = (bool)CleanDataValue(reader[ColumnNames.QuebecTaxFlag]);
            item.TaxOverrideFlag = (bool)CleanDataValue(reader[ColumnNames.TaxOverrideFlag]);
            item.CashTaxableFlag = (bool)CleanDataValue(reader[ColumnNames.CashTaxableFlag]);
            item.CanadaQuebecPensionPlanFlag = (bool)CleanDataValue(reader[ColumnNames.CanadaQuebecPensionPlanFlag]);
            item.ProvincialParentalInsurancePlanFlag = (bool)CleanDataValue(reader[ColumnNames.ProvincialParentalInsurancePlanFlag]);
            item.EmploymentInsuranceFlag = (bool)CleanDataValue(reader[ColumnNames.EmploymentInsuranceFlag]);
            item.BeforeIncomeTaxDeductionFlag = (bool)CleanDataValue(reader[ColumnNames.BeforeIncomeTaxDeductionFlag]);
            item.BeforeBonusTaxDeductionFlag = (bool)CleanDataValue(reader[ColumnNames.BeforeBonusTaxDeductionFlag]);
            item.WorkersCompensationBoardFlag = (bool)CleanDataValue(reader[ColumnNames.WorkersCompensationBoardFlag]);
            item.ProvincialHealthTaxFlag = (bool)CleanDataValue(reader[ColumnNames.ProvincialHealthTaxFlag]);
            item.PaycodeTypeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeTypeCode]);
            item.PaycodeIncomePayTypeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeIncomePayTypeCode]);

            return item;
        }
        protected PayrollTransactionEmployeePaycode MapToPayrollTransactionEmployeePaycode(IDataReader reader)
        {
            PayrollTransactionEmployeePaycode item = new PayrollTransactionEmployeePaycode();
            this.MapToBase(item, reader);
            this.MapToPayrollTransaction(item, reader);

            item.PaycodeTypeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeTypeCode]);
            item.PaycodeIncomePayTypeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeIncomePayTypeCode]);

            return item;
        }
        protected void MapToPayrollTransaction(PayrollTransaction item, IDataReader reader)
        {
            item.PayrollTransactionId = (long)CleanDataValue(reader[ColumnNames.PayrollTransactionId]);
            item.PayrollBatchId = (long)CleanDataValue(reader[ColumnNames.PayrollBatchId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.TransactionDate = (DateTime)CleanDataValue(reader[ColumnNames.TransactionDate]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.EntryTypeCode = (String)CleanDataValue(reader[ColumnNames.EntryTypeCode]);
            item.Units = (Decimal)CleanDataValue(reader[ColumnNames.Units]);
            item.Rate = (Decimal)CleanDataValue(reader[ColumnNames.Rate]);
            item.EmploymentInsuranceHour = Convert.ToDecimal(CleanDataValue(reader[ColumnNames.EmploymentInsuranceHour]));
            item.OverrideBonusDeductionBeforeTaxPercent = (Decimal?)CleanDataValue(reader[ColumnNames.OverrideBonusDeductionBeforeTaxPercent]);
            item.FederalTaxAmount = (Decimal?)CleanDataValue(reader[ColumnNames.FederalTaxAmount]);
            item.ProvincialTaxAmount = (Decimal?)CleanDataValue(reader[ColumnNames.ProvincialTaxAmount]);
            item.BaseSalaryRate = (Decimal?)CleanDataValue(reader[ColumnNames.BaseSalaryRate]);
            item.TransactionStartTime = (TimeSpan?)CleanDataValue(reader[ColumnNames.TransactionStartTime]);
            item.TransactionEndTime = (TimeSpan?)CleanDataValue(reader[ColumnNames.TransactionEndTime]);
            item.RelatedPayrollTransactionId = (long?)CleanDataValue(reader[ColumnNames.RelatedPayrollTransactionId]);
        }
        protected PayrollTransaction MapToPayrollTransaction(IDataReader reader, PayrollTransaction item, long payrollTransactionId)
        {

            item.PayrollTransactionId = payrollTransactionId;
            item.PayrollBatchId = (long)CleanDataValue(reader[ColumnNames.PayrollBatchId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.TransactionDate = (DateTime)CleanDataValue(reader[ColumnNames.TransactionDate]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.EntryTypeCode = (String)CleanDataValue(reader[ColumnNames.EntryTypeCode]);
            item.Units = (Decimal)CleanDataValue(reader[ColumnNames.Units]);
            item.Rate = (Decimal)CleanDataValue(reader[ColumnNames.Rate]);
            item.EmploymentInsuranceHour = Convert.ToDecimal(CleanDataValue(reader[ColumnNames.EmploymentInsuranceHour]));
            item.OverrideBonusDeductionBeforeTaxPercent = (Decimal?)CleanDataValue(reader[ColumnNames.OverrideBonusDeductionBeforeTaxPercent]);
            item.FederalTaxAmount = (Decimal?)CleanDataValue(reader[ColumnNames.FederalTaxAmount]);
            item.ProvincialTaxAmount = (Decimal?)CleanDataValue(reader[ColumnNames.ProvincialTaxAmount]);
            item.BaseSalaryRate = (Decimal?)CleanDataValue(reader[ColumnNames.BaseSalaryRate]);
            //item.OrganizationUnitHierarchyId = (long?)CleanDataValue(reader[ColumnNames.OrganizationUnitHierarchyId]);

            item.TransactionStartTime = (TimeSpan?)CleanDataValue(reader[ColumnNames.TransactionStartTime]);
            item.TransactionEndTime = (TimeSpan?)CleanDataValue(reader[ColumnNames.TransactionEndTime]);
            item.RelatedPayrollTransactionId = (long?)CleanDataValue(reader[ColumnNames.RelatedPayrollTransactionId]);

            return item;
        }
        protected PayrollTransactionRateProgression MapToPayrollTransactionRateProgression(IDataReader reader)
        {
            PayrollTransactionRateProgression item = new PayrollTransactionRateProgression();
            base.MapToBase(item, reader);

            item.PayrollTransactionId = (long)CleanDataValue(reader[ColumnNames.PayrollTransactionId]);
            item.PayrollBatchId = (long)CleanDataValue(reader[ColumnNames.PayrollBatchId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.TransactionDate = (DateTime)CleanDataValue(reader[ColumnNames.TransactionDate]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.EntryTypeCode = (String)CleanDataValue(reader[ColumnNames.EntryTypeCode]);
            item.Units = (Decimal)CleanDataValue(reader[ColumnNames.Units]);
            item.Rate = (Decimal)CleanDataValue(reader[ColumnNames.Rate]);
            item.EmploymentInsuranceHour = Convert.ToDecimal(CleanDataValue(reader[ColumnNames.EmploymentInsuranceHour]));
            item.OverrideBonusDeductionBeforeTaxPercent = (Decimal?)CleanDataValue(reader[ColumnNames.OverrideBonusDeductionBeforeTaxPercent]);
            item.FederalTaxAmount = (Decimal?)CleanDataValue(reader[ColumnNames.FederalTaxAmount]);
            item.ProvincialTaxAmount = (Decimal?)CleanDataValue(reader[ColumnNames.ProvincialTaxAmount]);
            item.BaseSalaryRate = (Decimal?)CleanDataValue(reader[ColumnNames.BaseSalaryRate]);
            //item.OrganizationUnitHierarchyId = (long?)CleanDataValue(reader[ColumnNames.OrganizationUnitHierarchyId]);

            item.TransactionStartTime = (TimeSpan?)CleanDataValue(reader[ColumnNames.TransactionStartTime]);
            item.TransactionEndTime = (TimeSpan?)CleanDataValue(reader[ColumnNames.TransactionEndTime]);
            item.RelatedPayrollTransactionId = (long?)CleanDataValue(reader[ColumnNames.RelatedPayrollTransactionId]);

            item.CurrentPeriodPlanHour = (Decimal?)CleanDataValue(reader[ColumnNames.CurrentPeriodPlanHour]);
            item.CurrentPeriodGradeHour = (Decimal?)CleanDataValue(reader[ColumnNames.CurrentPeriodGradeHour]);
            item.SalaryPlanId = (long?)CleanDataValue(reader[ColumnNames.SalaryPlanId]);
            item.SalaryPlanGradeId = (long?)CleanDataValue(reader[ColumnNames.SalaryPlanGradeId]);
            item.SalaryPlanGradeStepId = (long?)CleanDataValue(reader[ColumnNames.SalaryPlanGradeStepId]);
            item.CurrentStepAmount = (Decimal?)CleanDataValue(reader[ColumnNames.CurrentStepAmount]);
            item.PreviousTotalGradeHour = (Decimal?)CleanDataValue(reader[ColumnNames.PreviousTotalGradeHour]);
            item.PreviousTotalPlanHour = (Decimal?)CleanDataValue(reader[ColumnNames.PreviousTotalPlanHour]);
            item.AdvancementToMinimumHour = (int?)CleanDataValue(reader[ColumnNames.AdvancementToMinimumHour]);
            item.SalaryPlanAdvanceBasedOnCode = (String)CleanDataValue(reader[ColumnNames.SalaryPlanAdvanceBasedOnCode]);
            item.AdvanceToAmount = (Decimal?)CleanDataValue(reader[ColumnNames.AdvanceToAmount]);
            item.ProbationHour = Convert.ToDecimal(CleanDataValue(reader[ColumnNames.ProbationHour]));
            item.AdvanceSalaryPlanGradeStepId = (long?)CleanDataValue(reader[ColumnNames.AdvanceSalaryPlanGradeStepId]);
            item.AmountRateFactor = (Decimal)CleanDataValue(reader[ColumnNames.AmountRateFactor]);
            item.PayrollProcessId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessId]);
            item.AutoPopulateRateFlag = (bool)CleanDataValue(reader[ColumnNames.AutoPopulateRateFlag]);

            return item;
        }
        protected PayrollTransactionRateProgressionBonus MapToPayrollTransactionRateProgressionBonus(IDataReader reader, BonusRuleCollection rule)
        {
            PayrollTransactionRateProgressionBonus item = new PayrollTransactionRateProgressionBonus(rule);
            base.MapToBase(item, reader);

            item.PayrollTransactionId = (long)CleanDataValue(reader[ColumnNames.PayrollTransactionId]);
            item.PayrollBatchId = (long)CleanDataValue(reader[ColumnNames.PayrollBatchId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.TransactionDate = (DateTime)CleanDataValue(reader[ColumnNames.TransactionDate]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.EntryTypeCode = (String)CleanDataValue(reader[ColumnNames.EntryTypeCode]);
            item.Units = (Decimal)CleanDataValue(reader[ColumnNames.Units]);
            item.Rate = (Decimal)CleanDataValue(reader[ColumnNames.Rate]);
            item.EmploymentInsuranceHour = Convert.ToDecimal(CleanDataValue(reader[ColumnNames.EmploymentInsuranceHour]));
            item.OverrideBonusDeductionBeforeTaxPercent = (Decimal?)CleanDataValue(reader[ColumnNames.OverrideBonusDeductionBeforeTaxPercent]);
            item.FederalTaxAmount = (Decimal?)CleanDataValue(reader[ColumnNames.FederalTaxAmount]);
            item.ProvincialTaxAmount = (Decimal?)CleanDataValue(reader[ColumnNames.ProvincialTaxAmount]);
            item.BaseSalaryRate = (Decimal?)CleanDataValue(reader[ColumnNames.BaseSalaryRate]);
            //item.OrganizationUnitHierarchyId = (long?)CleanDataValue(reader[ColumnNames.OrganizationUnitHierarchyId]);

            item.TransactionStartTime = (TimeSpan?)CleanDataValue(reader[ColumnNames.TransactionStartTime]);
            item.TransactionEndTime = (TimeSpan?)CleanDataValue(reader[ColumnNames.TransactionEndTime]);
            item.RelatedPayrollTransactionId = (long?)CleanDataValue(reader[ColumnNames.RelatedPayrollTransactionId]);

            item.OutputPaycode = (String)CleanDataValue(reader[ColumnNames.OutputPaycode]);
            item.AmountRateFactor = (Decimal)CleanDataValue(reader[ColumnNames.AmountRateFactor]);

            item.CurrentPeriodPlanHour = (Decimal?)CleanDataValue(reader[ColumnNames.CurrentPeriodPlanHour]);
            item.SalaryPlanId = (long?)CleanDataValue(reader[ColumnNames.SalaryPlanId]);
            item.SalaryPlanGradeId = (long?)CleanDataValue(reader[ColumnNames.SalaryPlanGradeId]);
            item.PreviousTotalPlanHour = (Decimal?)CleanDataValue(reader[ColumnNames.PreviousTotalPlanHour]);
            item.PayrollProcessId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessId]);

            return item;
        }
        protected BonusRuleCollection MapToBonusRuleCollection(IDataReader reader)
        {
            BonusRuleCollection collection = new BonusRuleCollection();
            while (reader.Read())
                collection.Add(MapToBonusRule(reader));

            return collection;
        }
        protected BonusRule MapToBonusRule(IDataReader reader)
        {
            BonusRule item = new BonusRule();

            item.SalaryPlanId = (long)CleanDataValue(reader[ColumnNames.SalaryPlanId]);
            item.Amount = (Decimal)CleanDataValue(reader[ColumnNames.Amount]);
            item.MinimumHour = (int)CleanDataValue(reader[ColumnNames.MinimumHour]);
            item.MaximumHour = (int)CleanDataValue(reader[ColumnNames.MaximumHour]);
            //            item.OutputPaycode = (String)CleanDataValue(reader[ColumnNames.OutputPaycode]);

            return item;
        }
        protected PayrollTransaction MapToEmployeePaycodeBonusWithDeduction_select(IDataReader reader, int i)
        {
            PayrollTransaction item = new PayrollTransaction();

            item.PayrollTransactionId = i;
            item.PayrollBatchId = (long)CleanDataValue(reader[ColumnNames.PayrollBatchId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.TransactionDate = (DateTime)CleanDataValue(reader[ColumnNames.TransactionDate]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.EntryTypeCode = (String)CleanDataValue(reader[ColumnNames.EntryTypeCode]);
            item.Units = (Decimal)CleanDataValue(reader[ColumnNames.Units]);
            item.Rate = (Decimal)CleanDataValue(reader[ColumnNames.Rate]);
            item.EmploymentInsuranceHour = Convert.ToDecimal(CleanDataValue(reader[ColumnNames.EmploymentInsuranceHour]));
            item.FederalTaxAmount = (Decimal?)CleanDataValue(reader[ColumnNames.FederalTaxAmount]);
            item.ProvincialTaxAmount = (Decimal?)CleanDataValue(reader[ColumnNames.ProvincialTaxAmount]);
            item.BaseSalaryRate = (Decimal?)CleanDataValue(reader[ColumnNames.BaseSalaryRate]);
            //item.OrganizationUnitHierarchyId = (long?)CleanDataValue(reader[ColumnNames.OrganizationUnitHierarchyId]);

            item.TransactionStartTime = (TimeSpan?)CleanDataValue(reader[ColumnNames.TransactionStartTime]);
            item.TransactionEndTime = (TimeSpan?)CleanDataValue(reader[ColumnNames.TransactionEndTime]);
            item.RelatedPayrollTransactionId = (long?)CleanDataValue(reader[ColumnNames.RelatedPayrollTransactionId]);

            return item;
        }

        #endregion
    }
}