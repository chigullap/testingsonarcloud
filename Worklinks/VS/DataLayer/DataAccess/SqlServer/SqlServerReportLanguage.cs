﻿using System;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerReportLanguage : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ReportLanguageId = "report_language_id";
            public static String ReportId = "report_id";
            public static String LanguageCode = "code_language_cd";
            public static String Identifier = "identifier";
            public static String Value = "value";
        }
        #endregion

        #region main
        /// <summary>
        /// returns a Report
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        internal ReportLanguageCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long reportId)
        {
            using (WLP.DataLayer.SqlClient.DataBaseCommand command = GetStoredProcCommand("ReportLanguage_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@reportId", reportId);

                ReportLanguageCollection items = null;

                using (System.Data.IDataReader reader = command.ExecuteReader())
                    items = MapToReportCollection(reader);

                return items;
            }
        }

        #endregion

        #region data mapping
        /// <summary>
        /// maps result set to Report collection
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected ReportLanguageCollection MapToReportCollection(System.Data.IDataReader reader)
        {
            ReportLanguageCollection collection = new ReportLanguageCollection();

            while (reader.Read())
                collection.Add(MapToReport(reader));

            return collection;
        }
        /// <summary>
        /// maps columns to Report properties
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected ReportLanguage MapToReport(System.Data.IDataReader reader)
        {
            ReportLanguage item = new ReportLanguage();
            base.MapToBase(item, reader);

            item.ReportLanguageId = (long)CleanDataValue(reader[ColumnNames.ReportLanguageId]);
            item.ReportId = (long)CleanDataValue(reader[ColumnNames.ReportId]);
            item.LanguageCode = (String)CleanDataValue(reader[ColumnNames.LanguageCode]);
            item.Identifier = (String)CleanDataValue(reader[ColumnNames.Identifier]);
            item.Value = (String)CleanDataValue(reader[ColumnNames.Value]);

            return item;
        }
        #endregion
    }
}