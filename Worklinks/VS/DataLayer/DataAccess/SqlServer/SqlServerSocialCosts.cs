﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerSocialCosts : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String DummyKey = "row_number";
            public static String EmployeeNumber = "employee_number";
            public static String BenefitName = "benefit_name";
            public static String BenefitValue = "benefit_value";
            public static String StartDate = "start_date";
            public static String CutoffDate = "cutoff_date";
        }
        #endregion

        #region main

        public SocialCostsCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            DataBaseCommand command = GetStoredProcCommand("SocialCosts_report", user.DatabaseName);
            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToSocialCostsCollection(reader);
            }
        }
        #endregion

        #region data mapping
        protected SocialCostsCollection MapToSocialCostsCollection(IDataReader reader)
        {
            SocialCostsCollection collection = new SocialCostsCollection();
            while (reader.Read())
            {
                collection.Add(MapToSocialCosts(reader));
            }

            return collection;
        }
        protected SocialCosts MapToSocialCosts(IDataReader reader)
        {
            SocialCosts item = new SocialCosts();

            item.DummyKey = (long)CleanDataValue(reader[ColumnNames.DummyKey]);
            item.EmployeeNumber = (string)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.BenefitName = (string)CleanDataValue(reader[ColumnNames.BenefitName]);
            item.BenefitValue = (decimal)CleanDataValue(reader[ColumnNames.BenefitValue]);
            item.StartDate = (DateTime)CleanDataValue(reader[ColumnNames.StartDate]);
            item.CutoffDate = (DateTime)CleanDataValue(reader[ColumnNames.CutoffDate]);

            return item;
        }
        #endregion
    }
}
