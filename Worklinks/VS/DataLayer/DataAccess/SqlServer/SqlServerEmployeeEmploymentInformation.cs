﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeEmploymentInformation : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeEmploymentInformationId = "employee_employment_information_id";
            public static String EmployeeId = "employee_id";
            public static String HireDate = "hire_date";
            public static String TerminationDate = "termination_date";
            public static String RehireDate = "rehire_date";
            public static String ProbationDate = "probation_date";
            public static String SeniorityDate = "seniority_date";
            public static String LastReviewDate = "last_review_date";
            public static String NextReviewDate = "next_review_date";
            public static String IncreaseDate = "increase_date";
            public static String AnniversaryDate = "anniversary_date";
            public static String LunchDuration = "lunch_duration";
            public static String DaysPerWeek = "days_per_week";
            public static String PayrollProcessGroupCode = "code_payroll_process_group_cd";
            public static String WSIBCode = "code_wsib_cd";
            public static String ExcludeMassPayslipFileFlag = "exclude_mass_payslip_file_flag";
            public static String ExcludeMassPayslipPrintFlag = "exclude_mass_payslip_print_flag";
            public static String PensionEffectiveDate = "pension_effective_date";
            public static String ProbationHour = "probation_hour";
            public static String CodeProbationStatusCd = "code_probation_status_cd";            

            //for employee employment information import
            public static String ImportExternalIdentifier = "import_external_identifier";
        }
        #endregion

        #region select
        internal EmployeeEmploymentInformationCollection Select(DatabaseUser user, long id, bool securityOverrideFlag)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeEmploymentInformation_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@securityOverride", securityOverrideFlag);
                command.AddParameterWithValue("@employeeId", id);
                command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
                command.AddParameterWithValue("@securityUserId", user.SecurityUserId);

                EmployeeEmploymentInformationCollection EmployeeEmploymentInformations = null;

                using (IDataReader reader = command.ExecuteReader())
                    EmployeeEmploymentInformations = MapToEmployeeEmploymentInformationCollection(reader);

                return EmployeeEmploymentInformations;
            }
        }
        public WorkLinksEmployeeEmploymentInformationCollection SelectBatch(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeEmploymentInformation_batchSelect", user.DatabaseName))
            {
                SqlParameter parmEmployeeEmploymentInformation = new SqlParameter();
                parmEmployeeEmploymentInformation.ParameterName = "@parmEmployeeEmploymentInformation";
                parmEmployeeEmploymentInformation.SqlDbType = SqlDbType.Structured;

                DataTable tableEmployeeEmploymentInformation = LoadEmployeeEmploymentInformationDataTable(importExternalIdentifiers);
                parmEmployeeEmploymentInformation.Value = tableEmployeeEmploymentInformation;
                command.AddParameter(parmEmployeeEmploymentInformation);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToWorkLinksEmployeeEmploymentInformationCollection(reader);
            }
        }
        private DataTable LoadEmployeeEmploymentInformationDataTable(List<String> importExternalIdentifiers)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("import_external_identifier", typeof(String));

            for (int i = 0; i < importExternalIdentifiers.Count; i++)
            {
                table.Rows.Add(new Object[]
                {
                    importExternalIdentifiers[i]
                });
            }

            return table;
        }
        #endregion

        #region insert
        public void Insert(DatabaseUser user, EmployeeEmploymentInformation item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeEmploymentInformation_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeEmploymentInformation_insert");

                SqlParameter employeeEmploymentInformationIdParm = command.AddParameterWithValue("@EmployeeEmploymentInformationId", item.EmployeeEmploymentInformationId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", item.EmployeeId);
                command.AddParameterWithValue("@hireDate", item.HireDate);
                command.AddParameterWithValue("@terminationDate", item.TerminationDate);
                command.AddParameterWithValue("@rehireDate", item.RehireDate);
                command.AddParameterWithValue("@probationDate", item.ProbationDate);
                command.AddParameterWithValue("@seniorityDate", item.SeniorityDate);
                command.AddParameterWithValue("@lastReviewDate", item.LastReviewDate);
                command.AddParameterWithValue("@nextReviewDate", item.NextReviewDate);
                command.AddParameterWithValue("@increaseDate", item.IncreaseDate);
                command.AddParameterWithValue("@anniversaryDate", item.AnniversaryDate);
                command.AddParameterWithValue("@lunchDuration", item.LunchDuration);
                command.AddParameterWithValue("@daysPerWeek", item.DaysPerWeek);
                command.AddParameterWithValue("@payrollProcessGroupCode", item.PayrollProcessGroupCode);
                command.AddParameterWithValue("@wsibCode", item.WSIBCode);
                command.AddParameterWithValue("@excludeMassPayslipFileFlag", item.ExcludeMassPayslipFileFlag);
                command.AddParameterWithValue("@excludeMassPayslipPrintFlag", item.ExcludeMassPayslipPrintFlag);
                command.AddParameterWithValue("@pensionEffectiveDate", item.PensionEffectiveDate);
                command.AddParameterWithValue("@probationHour", item.ProbationHour);
                command.AddParameterWithValue("@codeProbationStatusCd", item.CodeProbationStatusCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.EmployeeEmploymentInformationId = Convert.ToInt64(employeeEmploymentInformationIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, EmployeeEmploymentInformation item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeEmploymentInformation_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeEmploymentInformation_update");

                    command.AddParameterWithValue("@employeeEmploymentInformationId", item.EmployeeEmploymentInformationId);
                    command.AddParameterWithValue("@employeeId", item.EmployeeId);
                    command.AddParameterWithValue("@hireDate", item.HireDate);
                    command.AddParameterWithValue("@terminationDate", item.TerminationDate);
                    command.AddParameterWithValue("@rehireDate", item.RehireDate);
                    command.AddParameterWithValue("@probationDate", item.ProbationDate);
                    command.AddParameterWithValue("@seniorityDate", item.SeniorityDate);
                    command.AddParameterWithValue("@lastReviewDate", item.LastReviewDate);
                    command.AddParameterWithValue("@nextReviewDate", item.NextReviewDate);
                    command.AddParameterWithValue("@increaseDate", item.IncreaseDate);
                    command.AddParameterWithValue("@anniversaryDate", item.AnniversaryDate);
                    command.AddParameterWithValue("@lunchDuration", item.LunchDuration);
                    command.AddParameterWithValue("@daysPerWeek", item.DaysPerWeek);
                    command.AddParameterWithValue("@payrollProcessGroupCode", item.PayrollProcessGroupCode);
                    command.AddParameterWithValue("@wsibCode", item.WSIBCode);
                    command.AddParameterWithValue("@excludeMassPayslipFileFlag", item.ExcludeMassPayslipFileFlag);
                    command.AddParameterWithValue("@excludeMassPayslipPrintFlag", item.ExcludeMassPayslipPrintFlag);
                    command.AddParameterWithValue("@pensionEffectiveDate", item.PensionEffectiveDate);
                    command.AddParameterWithValue("@probationHour", item.ProbationHour);
                    command.AddParameterWithValue("@codeProbationStatusCd", item.CodeProbationStatusCode);
                    command.AddParameterWithValue("@overrideConcurrencyCheckFlag", item.OverrideConcurrencyCheckFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, EmployeeEmploymentInformation EmployeeEmploymentInformation)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeEmploymentInformation_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeEmploymentInformation_delete");

                    command.AddParameterWithValue("@employeeEmploymentInformationId", EmployeeEmploymentInformation.EmployeeEmploymentInformationId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region import
        public void Import(DatabaseUser user, WorkLinksEmployeeEmploymentInformationCollection items)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmploymentInformation_import", user.DatabaseName))
            {
                command.BeginTransaction("EmploymentInformation_import");

                //table parameter
                SqlParameter parmEmployeeEmploymentInformation = new SqlParameter();
                parmEmployeeEmploymentInformation.ParameterName = "@parmEmployeeEmploymentInformation";
                parmEmployeeEmploymentInformation.SqlDbType = SqlDbType.Structured;

                DataTable tableEmployeeEmploymentInformation = LoadEmployeeEmploymentInformationDataTable(items);
                parmEmployeeEmploymentInformation.Value = tableEmployeeEmploymentInformation;
                command.AddParameter(parmEmployeeEmploymentInformation);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        private DataTable LoadEmployeeEmploymentInformationDataTable(WorkLinksEmployeeEmploymentInformationCollection items)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_employment_information_id", typeof(long));
            table.Columns.Add("employee_id", typeof(long));
            table.Columns.Add("hire_date", typeof(DateTime));
            table.Columns.Add("termination_date date", typeof(DateTime));
            table.Columns.Add("rehire_date", typeof(DateTime));
            table.Columns.Add("probation_date", typeof(DateTime));
            table.Columns.Add("seniority_date", typeof(DateTime));
            table.Columns.Add("last_review_date", typeof(DateTime));
            table.Columns.Add("next_review_date", typeof(DateTime));
            table.Columns.Add("increase_date", typeof(DateTime));
            table.Columns.Add("anniversary_date", typeof(DateTime));
            table.Columns.Add("lunch_duration", typeof(Decimal));
            table.Columns.Add("days_per_week", typeof(Decimal));
            table.Columns.Add("exclude_mass_payslip_file_flag", typeof(bool));
            table.Columns.Add("exclude_mass_payslip_print_flag", typeof(bool));
            table.Columns.Add("code_wsib_cd", typeof(String));
            table.Columns.Add("code_payroll_process_group_cd", typeof(String));
            table.Columns.Add("pension_effective_date", typeof(DateTime));
            table.Columns.Add("import_external_identifier", typeof(String));

            foreach (WorkLinksEmployeeEmploymentInformation item in items)
            {
                table.Rows.Add(new Object[]
                {
                    item.EmployeeEmploymentInformationId,
                    item.EmployeeId,
                    item.HireDate,
                    item.TerminationDate,
                    item.RehireDate,
                    item.ProbationDate,
                    item.SeniorityDate,
                    item.LastReviewDate,
                    item.NextReviewDate,
                    item.IncreaseDate,
                    item.AnniversaryDate,
                    item.LunchDuration,
                    item.DaysPerWeek,
                    item.ExcludeMassPayslipFileFlag,
                    item.ExcludeMassPayslipPrintFlag,
                    item.WSIBCode,
                    item.PayrollProcessGroupCode,
                    item.PensionEffectiveDate,
                    item.ImportExternalIdentifier
                });
            }

            return table;
        }
        #endregion

        #region data mapping
        protected EmployeeEmploymentInformationCollection MapToEmployeeEmploymentInformationCollection(IDataReader reader)
        {
            EmployeeEmploymentInformationCollection collection = new EmployeeEmploymentInformationCollection();

            while (reader.Read())
                collection.Add(MapToEmployeeEmploymentInformation(reader));

            return collection;
        }
        protected EmployeeEmploymentInformation MapToEmployeeEmploymentInformation(IDataReader reader)
        {
            EmployeeEmploymentInformation item = new EmployeeEmploymentInformation();
            base.MapToBase(item, reader);

            item.EmployeeEmploymentInformationId = (long)CleanDataValue(reader[ColumnNames.EmployeeEmploymentInformationId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.HireDate = (DateTime?)CleanDataValue(reader[ColumnNames.HireDate]);
            item.TerminationDate = (DateTime?)CleanDataValue(reader[ColumnNames.TerminationDate]);
            item.RehireDate = (DateTime?)CleanDataValue(reader[ColumnNames.RehireDate]);
            item.ProbationDate = (DateTime?)CleanDataValue(reader[ColumnNames.ProbationDate]);
            item.SeniorityDate = (DateTime?)CleanDataValue(reader[ColumnNames.SeniorityDate]);
            item.LastReviewDate = (DateTime?)CleanDataValue(reader[ColumnNames.LastReviewDate]);
            item.NextReviewDate = (DateTime?)CleanDataValue(reader[ColumnNames.NextReviewDate]);
            item.IncreaseDate = (DateTime?)CleanDataValue(reader[ColumnNames.IncreaseDate]);
            item.AnniversaryDate = (DateTime?)CleanDataValue(reader[ColumnNames.AnniversaryDate]);
            item.LunchDuration = (Decimal?)CleanDataValue(reader[ColumnNames.LunchDuration]);
            item.DaysPerWeek = (Decimal?)CleanDataValue(reader[ColumnNames.DaysPerWeek]);
            item.PayrollProcessGroupCode = (String)CleanDataValue(reader[ColumnNames.PayrollProcessGroupCode]);
            item.WSIBCode = (String)CleanDataValue(reader[ColumnNames.WSIBCode]);
            item.ExcludeMassPayslipFileFlag = (bool)CleanDataValue(reader[ColumnNames.ExcludeMassPayslipFileFlag]);
            item.ExcludeMassPayslipPrintFlag = (bool)CleanDataValue(reader[ColumnNames.ExcludeMassPayslipPrintFlag]);
            item.PensionEffectiveDate = (DateTime?)CleanDataValue(reader[ColumnNames.PensionEffectiveDate]);
            item.ProbationHour = (int?)CleanDataValue(reader[ColumnNames.ProbationHour]);
            item.CodeProbationStatusCode = (string)CleanDataValue(reader[ColumnNames.CodeProbationStatusCd]);


            return item;
        }
        protected WorkLinksEmployeeEmploymentInformationCollection MapToWorkLinksEmployeeEmploymentInformationCollection(IDataReader reader)
        {
            WorkLinksEmployeeEmploymentInformationCollection collection = new WorkLinksEmployeeEmploymentInformationCollection();

            while (reader.Read())
                collection.Add(MapToWorkLinksEmployeeEmploymentInformation(reader));

            return collection;
        }
        protected WorkLinksEmployeeEmploymentInformation MapToWorkLinksEmployeeEmploymentInformation(IDataReader reader)
        {
            WorkLinksEmployeeEmploymentInformation item = new WorkLinksEmployeeEmploymentInformation();
            base.MapToBase(item, reader);

            item.EmployeeEmploymentInformationId = (long)CleanDataValue(reader[ColumnNames.EmployeeEmploymentInformationId]);
            item.ImportExternalIdentifier = (String)CleanDataValue(reader[ColumnNames.ImportExternalIdentifier]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.HireDate = (DateTime?)CleanDataValue(reader[ColumnNames.HireDate]);
            item.TerminationDate = (DateTime?)CleanDataValue(reader[ColumnNames.TerminationDate]);
            item.RehireDate = (DateTime?)CleanDataValue(reader[ColumnNames.RehireDate]);
            item.ProbationDate = (DateTime?)CleanDataValue(reader[ColumnNames.ProbationDate]);
            item.SeniorityDate = (DateTime?)CleanDataValue(reader[ColumnNames.SeniorityDate]);
            item.LastReviewDate = (DateTime?)CleanDataValue(reader[ColumnNames.LastReviewDate]);
            item.NextReviewDate = (DateTime?)CleanDataValue(reader[ColumnNames.NextReviewDate]);
            item.IncreaseDate = (DateTime?)CleanDataValue(reader[ColumnNames.IncreaseDate]);
            item.AnniversaryDate = (DateTime?)CleanDataValue(reader[ColumnNames.AnniversaryDate]);
            item.LunchDuration = (Decimal?)CleanDataValue(reader[ColumnNames.LunchDuration]);
            item.DaysPerWeek = (Decimal?)CleanDataValue(reader[ColumnNames.DaysPerWeek]);
            item.PayrollProcessGroupCode = (String)CleanDataValue(reader[ColumnNames.PayrollProcessGroupCode]);
            item.WSIBCode = (String)CleanDataValue(reader[ColumnNames.WSIBCode]);
            item.ExcludeMassPayslipFileFlag = (bool)CleanDataValue(reader[ColumnNames.ExcludeMassPayslipFileFlag]);
            item.ExcludeMassPayslipPrintFlag = (bool)CleanDataValue(reader[ColumnNames.ExcludeMassPayslipPrintFlag]);
            item.PensionEffectiveDate = (DateTime?)CleanDataValue(reader[ColumnNames.PensionEffectiveDate]);
            item.ProbationHour = (int?)CleanDataValue(reader[ColumnNames.ProbationHour]);
            item.CodeProbationStatusCode = (string)CleanDataValue(reader[ColumnNames.CodeProbationStatusCd]);

            return item;
        }
        #endregion
    }
}