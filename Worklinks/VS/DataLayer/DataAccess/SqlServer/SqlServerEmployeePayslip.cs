﻿using System;
using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeePayslip : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayrollProcessId = "payroll_process_id";
            public static String EmployeeId = "employee_id";
            public static String EmployeeNumber = "employee_number";
            public static String CutoffDate = "cutoff_date";
            public static String ChequeDate = "cheque_date";
            public static String TotalGross = "total_gross";
            public static String TotalDeductions = "total_deductions";
            public static String TotalTaxes = "total_taxes";
            public static String NetPay = "net_pay";
        }
        #endregion

        #region main
        internal EmployeePayslipCollection Select(DatabaseUser user)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeePayslip_select", user.DatabaseName);

            command.AddParameterWithValue("@userName", user.UserName);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCollection(reader);
        }
        #endregion

        #region data mapping
        protected EmployeePayslipCollection MapToCollection(IDataReader reader)
        {
            EmployeePayslipCollection collection = new EmployeePayslipCollection();

            while (reader.Read())
                collection.Add(MapToItem(reader));

            return collection;
        }
        protected EmployeePayslip MapToItem(IDataReader reader)
        {
            EmployeePayslip item = new EmployeePayslip();

            item.PayrollProcessId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EmployeeNumber = (String)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.CutoffDate = (DateTime)CleanDataValue(reader[ColumnNames.CutoffDate]);
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.TotalGross = (Decimal)CleanDataValue(reader[ColumnNames.TotalGross]);
            item.TotalDeductions = (Decimal)CleanDataValue(reader[ColumnNames.TotalDeductions]);
            item.TotalTaxes = (Decimal)CleanDataValue(reader[ColumnNames.TotalTaxes]);
            item.NetPay = (Decimal)CleanDataValue(reader[ColumnNames.NetPay]);

            return item;
        }
        #endregion
    }
}