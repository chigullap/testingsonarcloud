﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerPreAuthorizedDebitExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
        }
        #endregion

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, PreAuthorizedDebitExport item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PreAuthorizedDebitExport_insert", user.DatabaseName))
            {
                command.BeginTransaction("PreAuthorizedDebitExport_insert");

                SqlParameter preAuthorizedDebitExportIdParm = command.AddParameterWithValue("@preAuthorizedDebitExportId", item.PreAuthorizedDebitExportId, ParameterDirection.Output);
                command.AddParameterWithValue("@payrollProcessId", item.PayrollProcessId);
                command.AddParameterWithValue("@sequenceNumber", item.SequenceNumber);
                command.AddParameterWithValue("@data", item.Data);
                command.AddParameterWithValue("@totalAmount", item.TotalAmount);
                command.AddParameterWithValue("@padFileType", item.PadFileType);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.PreAuthorizedDebitExportId = Convert.ToInt64(preAuthorizedDebitExportIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }

        public void UpdatePreAuthorizedDebitExportFileTransmissionDate(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long preAuthorizedDebitExportId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PreAuthorizedDebitExport_UpdateFileTransmissionDate", user.DatabaseName))
            {
                command.BeginTransaction("PreAuthorizedDebitExport_UpdateFileTransmissionDate");

                command.AddParameterWithValue("@preAuthorizedDebitExportId", preAuthorizedDebitExportId);
                command.AddParameterWithValue("@exportDate", DateTime.Now);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
    }
}
