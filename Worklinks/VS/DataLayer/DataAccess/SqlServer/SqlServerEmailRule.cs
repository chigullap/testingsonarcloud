﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmailRule : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmailRuleId = "email_rule_id";
            public static String EmailId = "email_id";
            public static String EmailTriggerFieldCode = "code_email_trigger_field_cd";
            public static String TriggerFieldOffsetDays = "trigger_field_offset_days";
            public static String EmailTo = "email_to";
            public static String OrganizationUnit = "organization_unit";
            public static String OrganizationUnitDescription = "organization_unit_description";
        }
        #endregion

        #region main
        internal EmailRuleCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long emailId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmailRule_select", user.DatabaseName);

            command.AddParameterWithValue("@emailId", emailId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToEmailRuleCollection(reader);
        }
        public EmailRule Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmailRule item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmailRule_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmailRule_insert");

                SqlParameter ruleIdParm = command.AddParameterWithValue("@emailRuleId", item.EmailRuleId, ParameterDirection.Output);
                command.AddParameterWithValue("@emailId", item.EmailId);
                command.AddParameterWithValue("@emailTriggerFieldCode", item.EmailTriggerFieldCode);
                command.AddParameterWithValue("@triggerFieldOffsetDays", item.TriggerFieldOffsetDays);
                command.AddParameterWithValue("@emailTo", item.EmailTo);
                command.AddParameterWithValue("@organizationUnit", item.OrganizationUnit);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.EmailRuleId = Convert.ToInt64(ruleIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmailRule item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmailRule_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmailRule_update");

                    command.AddParameterWithValue("@emailRuleId", item.EmailRuleId);
                    command.AddParameterWithValue("@emailId", item.EmailId);
                    command.AddParameterWithValue("@emailTriggerFieldCode", item.EmailTriggerFieldCode);
                    command.AddParameterWithValue("@triggerFieldOffsetDays", item.TriggerFieldOffsetDays);
                    command.AddParameterWithValue("@emailTo", item.EmailTo);
                    command.AddParameterWithValue("@organizationUnit", item.OrganizationUnit);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmailRule item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmailRule_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmailRule_delete");

                    command.AddParameterWithValue("@emailRuleId", item.EmailRuleId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmailRuleCollection MapToEmailRuleCollection(IDataReader reader)
        {
            EmailRuleCollection collection = new EmailRuleCollection();

            while (reader.Read())
                collection.Add(MapToEmailRule(reader));

            return collection;
        }
        protected EmailRule MapToEmailRule(IDataReader reader)
        {
            EmailRule item = new EmailRule();
            base.MapToBase(item, reader);

            item.EmailRuleId = (long)CleanDataValue(reader[ColumnNames.EmailRuleId]);
            item.EmailId = (long)CleanDataValue(reader[ColumnNames.EmailId]);
            item.EmailTriggerFieldCode = (String)CleanDataValue(reader[ColumnNames.EmailTriggerFieldCode]);
            item.TriggerFieldOffsetDays = (int)CleanDataValue(reader[ColumnNames.TriggerFieldOffsetDays]);
            item.OrganizationUnit = (String)CleanDataValue(reader[ColumnNames.OrganizationUnit]);
            item.OrganizationUnitDescription = (String)CleanDataValue(reader[ColumnNames.OrganizationUnitDescription]);
            item.EmailTo = (String)CleanDataValue(reader[ColumnNames.EmailTo]);

            return item;
        }
        #endregion
    }
}