﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerAccrualEntitlement : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String AccrualEntitlementId = "accrual_entitlement_id";
            public static String EnglishDescription = "english_description";
            public static String FrenchDescription = "french_description";
            public static String EntitlementBasedOnCode = "code_entitlement_based_on_cd";
            public static String PaycodeCode = "code_paycode_cd";
        }
        #endregion

        #region main
        internal AccrualEntitlementCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? entitlementId)
        {
            DataBaseCommand command = GetStoredProcCommand("AccrualEntitlement_select", user.DatabaseName);

            command.AddParameterWithValue("@accrualEntitlementId", entitlementId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToAccrualEntitlementCollection(reader);
        }
        public AccrualEntitlement Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlement entitlement)
        {
            using (DataBaseCommand command = GetStoredProcCommand("AccrualEntitlement_insert", user.DatabaseName))
            {
                command.BeginTransaction("AccrualEntitlement_insert");

                SqlParameter entitlementIdParm = command.AddParameterWithValue("@accrualEntitlementId", entitlement.AccrualEntitlementId, ParameterDirection.Output);
                command.AddParameterWithValue("@englishDescription", entitlement.EnglishDescription);
                command.AddParameterWithValue("@frenchDescription", entitlement.FrenchDescription);
                command.AddParameterWithValue("@entitlementBasedOnCode", entitlement.EntitlementBasedOnCode);
                command.AddParameterWithValue("@paycodeCode", entitlement.PaycodeCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", entitlement.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                entitlement.AccrualEntitlementId = Convert.ToInt64(entitlementIdParm.Value);
                entitlement.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return entitlement;
            }
        }
        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlement entitlement)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("AccrualEntitlement_update", user.DatabaseName))
                {
                    command.BeginTransaction("AccrualEntitlement_update");

                    command.AddParameterWithValue("@accrualEntitlementId", entitlement.AccrualEntitlementId);
                    command.AddParameterWithValue("@englishDescription", entitlement.EnglishDescription);
                    command.AddParameterWithValue("@frenchDescription", entitlement.FrenchDescription);
                    command.AddParameterWithValue("@entitlementBasedOnCode", entitlement.EntitlementBasedOnCode);
                    command.AddParameterWithValue("@paycodeCode", entitlement.PaycodeCode);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", entitlement.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    entitlement.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlement entitlement)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("AccrualEntitlement_delete", user.DatabaseName))
                {
                    command.BeginTransaction("AccrualEntitlement_delete");

                    command.AddParameterWithValue("@accrualEntitlementId", entitlement.AccrualEntitlementId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected AccrualEntitlementCollection MapToAccrualEntitlementCollection(IDataReader reader)
        {
            AccrualEntitlementCollection collection = new AccrualEntitlementCollection();

            while (reader.Read())
                collection.Add(MapToAccrualEntitlement(reader));

            return collection;
        }
        protected AccrualEntitlement MapToAccrualEntitlement(IDataReader reader)
        {
            AccrualEntitlement item = new AccrualEntitlement();
            base.MapToBase(item, reader);

            item.AccrualEntitlementId = (long)CleanDataValue(reader[ColumnNames.AccrualEntitlementId]);
            item.EnglishDescription = (String)CleanDataValue(reader[ColumnNames.EnglishDescription]);
            item.FrenchDescription = (String)CleanDataValue(reader[ColumnNames.FrenchDescription]);
            item.EntitlementBasedOnCode = (String)CleanDataValue(reader[ColumnNames.EntitlementBasedOnCode]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);

            return item;
        }
        #endregion
    }
}