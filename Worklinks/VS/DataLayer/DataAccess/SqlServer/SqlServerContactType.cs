﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerContactType : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ContactTypeId = "contact_type_id";
            public static String ContactId = "contact_id";
            public static String SortOrder = "sort_order";
            public static String ContactTypeCode = "code_contact_type_cd";
            public static String ContactTypeChannelCode = "code_contact_channel_type_cd";
        }
        #endregion

        #region main
        internal ContactTypeCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeeId, String contactTypeCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ContactType_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@employeeId", employeeId);
                command.AddParameterWithValue("@contactTypeCode", contactTypeCode);
                command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
                command.AddParameterWithValue("@securityUserId", user.SecurityUserId);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToContactTypeCollection(reader);
            }
        }
        internal CodeContactChannelTypeCollection SelectTypeByEmployeeId(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long personId, String categoryCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ContactChannelTypeByEmployeeId_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@personId", personId);
                command.AddParameterWithValue("@contactChannelCategoryCode", categoryCode);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToContactTypeByEmployeeIdCollection(reader);
            }
        }
        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ContactTypeCollection contactTypes, System.Collections.Generic.List<ContactType> deletedItems)
        {
            foreach (ContactType type in deletedItems)
                Delete(user, type);

            foreach (ContactType type in contactTypes)
            {
                if (type.ContactTypeId < 0)
                    Insert(user, type);
                else
                    Update(user, type);
            }
        }
        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ContactType type)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("ContactType_update", user.DatabaseName))
                {
                    command.BeginTransaction("ContactType_update");

                    command.AddParameterWithValue("@contactTypeId", type.ContactTypeId);
                    command.AddParameterWithValue("@contactId", type.ContactId);
                    command.AddParameterWithValue("@contactTypeCode", type.ContactTypeCode);
                    command.AddParameterWithValue("@sortOrder", type.SortOrder);

                    command.AddParameterWithValue("@rowVersion", type.RowVersion);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public ContactType Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ContactType type)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("ContactType_insert", user.DatabaseName))
                {
                    command.BeginTransaction("ContactType_insert");

                    SqlParameter contactTypeIdParm = command.AddParameterWithValue("@contactTypeId", type.ContactTypeId, ParameterDirection.Output);
                    command.AddParameterWithValue("@contactId", type.ContactId);
                    command.AddParameterWithValue("@contactTypeCode", type.ContactTypeCode);
                    command.AddParameterWithValue("@sortOrder", type.SortOrder);

                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    type.ContactTypeId = Convert.ToInt64(contactTypeIdParm.Value);
                    command.CommitTransaction();
                }

                return null;
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
                return null;
            }
        }
        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ContactType contact)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("ContactType_delete", user.DatabaseName))
                {
                    command.BeginTransaction("ContactType_delete");
                    command.AddParameterWithValue("@contactTypeId", contact.ContactTypeId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void DeleteByContactId(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long contactId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("ContactType_deleteByContactId", user.DatabaseName))
                {
                    command.BeginTransaction("ContactType_deleteByContactId");
                    command.AddParameterWithValue("@contactId", contactId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected ContactTypeCollection MapToContactTypeCollection(IDataReader reader)
        {
            ContactTypeCollection collection = new ContactTypeCollection();

            while (reader.Read())
                collection.Add(MapToContactType(reader));

            return collection;
        }
        protected ContactType MapToContactType(IDataReader reader)
        {
            ContactType contactType = new ContactType();
            base.MapToBase(contactType, reader);

            contactType.ContactTypeId = (long)CleanDataValue(reader[ColumnNames.ContactTypeId]);
            contactType.ContactId = (long)CleanDataValue(reader[ColumnNames.ContactId]);
            contactType.SortOrder = (short)CleanDataValue(reader[ColumnNames.SortOrder]);
            contactType.ContactTypeCode = (String)CleanDataValue(reader[ColumnNames.ContactTypeCode]);

            return contactType;
        }
        protected CodeContactChannelTypeCollection MapToContactTypeByEmployeeIdCollection(IDataReader reader)
        {
            CodeContactChannelTypeCollection collection = new CodeContactChannelTypeCollection();

            while (reader.Read())
                collection.Add(MapToContactTypeByEmployeeId(reader));

            return collection;
        }
        protected CodeContactChannelType MapToContactTypeByEmployeeId(IDataReader reader)
        {
            CodeContactChannelType contactType = new CodeContactChannelType();
            contactType.ContactTypeChannelCode = (String)CleanDataValue(reader[ColumnNames.ContactTypeChannelCode]);
            return contactType;
        }
        #endregion
    }
}