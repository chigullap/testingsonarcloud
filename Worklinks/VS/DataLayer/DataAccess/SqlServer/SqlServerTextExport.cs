﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerTextExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String OutPut = "output";
        }
        #endregion

        #region main

        public String[] Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String procName, String[] parmNames, Object[] parmValues)
        {
            List<String> dataCollection;
            DataBaseCommand command = GetStoredProcCommand(procName, user.DatabaseName);

            for (int i = 0; i < parmNames.Length  ; i++)
                command.AddParameterWithValue(parmNames[i], parmValues[i]);

            using (IDataReader reader = command.ExecuteReader())
            {
                dataCollection = MapToList(reader);
            }

            return dataCollection.ToArray();
        }
        #endregion

        #region data mapping

        protected List<String> MapToList(IDataReader reader)
        {
            List<String> list = new List<String>();
            while (reader.Read())
            {
                list.Add(MapToString(reader));
            }

            return list;
        }

        protected String MapToString(IDataReader reader)
        {
            return (String)CleanDataValue(reader[ColumnNames.OutPut]);
        }
        #endregion

    }
}
