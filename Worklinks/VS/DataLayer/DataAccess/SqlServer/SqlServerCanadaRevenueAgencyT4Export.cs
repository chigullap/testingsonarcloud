﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerCanadaRevenueAgencyT4Export : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String CanadaRevenueAgencyT4ExportId = "canada_revenue_agency_t4_export_id";
            public static String CanadaRevenueAgencyT619Id = "canada_revenue_agency_t619_id";
            public static String YearEndFormCode = "code_year_end_form_cd";
            public static String YearEndId = "year_end_id";
            public static String CurrentRevision = "current_revision";
        }
        #endregion

        #region main
        internal CanadaRevenueAgencyT4ExportCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long yearEndId, String exportType)
        {
            DataBaseCommand command = GetStoredProcCommand("CanadaRevenueAgencyT4Export_select", user.DatabaseName);
            command.AddParameterWithValue("@yearEndId", yearEndId);
            command.AddParameterWithValue("@exportType", exportType);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToCanadaRevenueAgencyT4ExportCollection(reader);
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CanadaRevenueAgencyT4Export T4Export)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CanadaRevenueAgencyT4Export_update", user.DatabaseName))
                {
                    command.BeginTransaction("CanadaRevenueAgencyT4Export_update");

                    command.AddParameterWithValue("@canadaRevenueAgencyT4ExportId", T4Export.CanadaRevenueAgencyT4ExportId);
                    command.AddParameterWithValue("@canadaRevenueAgencyT619Id", T4Export.CanadaRevenueAgencyT619Id);
                    command.AddParameterWithValue("@yearEndFormCode", T4Export.YearEndFormCode);
                    command.AddParameterWithValue("@yearEndId", T4Export.YearEndId);
                    command.AddParameterWithValue("@currentRevision", T4Export.CurrentRevision);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", T4Export.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    T4Export.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CanadaRevenueAgencyT4Export T4Export)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CanadaRevenueAgencyT4Export_insert", user.DatabaseName))
            {
                command.BeginTransaction("CanadaRevenueAgencyT4Export_insert");

                SqlParameter idParm = command.AddParameterWithValue("@canadaRevenueAgencyT4ExportId", T4Export.CanadaRevenueAgencyT4ExportId, ParameterDirection.Output);
                command.AddParameterWithValue("@canadaRevenueAgencyT619Id", T4Export.CanadaRevenueAgencyT619Id);
                command.AddParameterWithValue("@yearEndFormCode", T4Export.YearEndFormCode);
                command.AddParameterWithValue("@yearEndId", T4Export.YearEndId);
                command.AddParameterWithValue("@currentRevision", T4Export.CurrentRevision);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", T4Export.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                T4Export.YearEndId = Convert.ToInt64(idParm.Value);
                T4Export.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

            }
        }

        //public void Delete(YearEnd yearEnd)
        //{
        //    using (DataBaseCommand command = GetStoredProcCommand("YearEnd_delete"))
        //    {
        //        command.BeginTransaction("YearEnd_delete");
        //        command.AddParameterWithValue("@yearEndId", yearEnd.YearEndId);

        //        command.ExecuteNonQuery();
        //        command.CommitTransaction();
        //    }
        //}
        #endregion

        #region data mapping
        protected CanadaRevenueAgencyT4ExportCollection MapToCanadaRevenueAgencyT4ExportCollection(IDataReader reader)
        {
            CanadaRevenueAgencyT4ExportCollection collection = new CanadaRevenueAgencyT4ExportCollection();
            while (reader.Read())
            {
                collection.Add(MapToCanadaRevenueAgencyT4Export(reader));
            }

            return collection;
        }
        protected CanadaRevenueAgencyT4Export MapToCanadaRevenueAgencyT4Export(IDataReader reader)
        {
            CanadaRevenueAgencyT4Export item = new CanadaRevenueAgencyT4Export();
            base.MapToBase(item, reader);

            item.CanadaRevenueAgencyT4ExportId = (long)CleanDataValue(reader[ColumnNames.CanadaRevenueAgencyT4ExportId]);
            item.CanadaRevenueAgencyT619Id = (long)CleanDataValue(reader[ColumnNames.CanadaRevenueAgencyT619Id]);
            item.YearEndFormCode = (String)CleanDataValue(reader[ColumnNames.YearEndFormCode]);
            item.YearEndId = (long)CleanDataValue(reader[ColumnNames.YearEndId]);
            item.CurrentRevision = (int)CleanDataValue(reader[ColumnNames.CurrentRevision]);

            return item;
        }
        #endregion
    }
}