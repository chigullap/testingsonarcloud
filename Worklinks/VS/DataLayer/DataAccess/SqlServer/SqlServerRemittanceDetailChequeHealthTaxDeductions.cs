﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerRemittanceDetailChequeHealthTaxDeductions : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string DummyKey = "row_number";
            public static string CodeHealthTaxCd = "code_health_tax_cd";
            public static string ChequeDate = "cheque_date";
            public static string Premium = "premium";
            public static string AssessableEarnings = "earnings";
            public static string Period = "period";
            public static string StartDate = "start_date";
            public static string CutoffDate = "cutoff_date";
            public static string PayrollProcessId = "payroll_process_id";
        }
        #endregion

        #region main
        internal RemittanceDetailChequeHealthTaxDeductionsCollection Select(DatabaseUser user, long? chequeHealthTaxExportId, string codeHealthTaxCode)
        {
            DataBaseCommand command = GetStoredProcCommand("RemittanceDetailHealthTax_report", user.DatabaseName);

            command.AddParameterWithValue("@chequeHealthTaxExportId", chequeHealthTaxExportId);
            command.AddParameterWithValue("@codeHealthTaxCode", codeHealthTaxCode);            

            using (IDataReader reader = command.ExecuteReader())
                return MapToRemittanceDetailChequeHealthTaxDeductionsCollection(reader);
        }
        #endregion

        #region data mapping
        protected RemittanceDetailChequeHealthTaxDeductionsCollection MapToRemittanceDetailChequeHealthTaxDeductionsCollection(IDataReader reader)
        {
            RemittanceDetailChequeHealthTaxDeductionsCollection collection = new RemittanceDetailChequeHealthTaxDeductionsCollection();

            while (reader.Read())
                collection.Add(MapToRemittanceDetailChequeHealthTaxDeductions(reader));

            return collection;
        }
        protected RemittanceDetailChequeHealthTaxDeductions MapToRemittanceDetailChequeHealthTaxDeductions(IDataReader reader)
        {
            RemittanceDetailChequeHealthTaxDeductions item = new RemittanceDetailChequeHealthTaxDeductions();

            item.DummyKey = (long)CleanDataValue(reader[ColumnNames.DummyKey]);
            item.CodeHealthTaxCd = (string)CleanDataValue(reader[ColumnNames.CodeHealthTaxCd]);
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.Premium = (decimal)CleanDataValue(reader[ColumnNames.Premium]);
            item.AssessableEarnings = (decimal)CleanDataValue(reader[ColumnNames.AssessableEarnings]);
            item.Period = (decimal)CleanDataValue(reader[ColumnNames.Period]);
            item.StartDate = (DateTime)CleanDataValue(reader[ColumnNames.StartDate]);
            item.CutoffDate = (DateTime)CleanDataValue(reader[ColumnNames.CutoffDate]);
            item.PayrollProcessId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessId]);

            return item;
        }
        #endregion
    }
}