﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerCodeSystem : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String CodeSystemCd = "code_system_cd";
            public static String Description = "description";
            public static String Notes = "notes";
        }
        #endregion

        #region select
        internal CodeSystemCollection Select(DatabaseUser user)
        {
            DataBaseCommand command = GetStoredProcCommand("CodeSystemAndDescription_select", user.DatabaseName);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCodeSystemCollection(reader);
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, CodeSystem code)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CodeSystemAndDescription_update", user.DatabaseName))
                {
                    command.BeginTransaction("CodeSystemAndDescription_update");

                    command.AddParameterWithValue("@codeSystemCd", code.CodeSystemCd);
                    command.AddParameterWithValue("@description", code.Description);
                    command.AddParameterWithValue("@notes", code.Notes);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", code.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    code.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region insert
        public CodeSystem Insert(DatabaseUser user, CodeSystem code)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CodeSystemAndDescription_insert", user.DatabaseName))
            {
                command.BeginTransaction("CodeSystemAndDescription_insert");

                command.AddParameterWithValue("@codeSystemCd", code.CodeSystemCd);
                command.AddParameterWithValue("@description", code.Description);
                command.AddParameterWithValue("@notes", code.Notes);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", code.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                code.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return code;
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, CodeSystem code)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CodeSystemAndDescription_delete", user.DatabaseName))
                {
                    command.BeginTransaction("CodeSystemAndDescription_delete");
                    command.AddParameterWithValue("@codeSystemCd", code.CodeSystemCd);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected CodeSystemCollection MapToCodeSystemCollection(IDataReader reader)
        {
            CodeSystemCollection collection = new CodeSystemCollection();

            while (reader.Read())
                collection.Add(MapToCodeSystem(reader));

            return collection;
        }

        protected CodeSystem MapToCodeSystem(IDataReader reader)
        {
            CodeSystem item = new CodeSystem();
            base.MapToBase(item, reader);

            item.CodeSystemCd = (String)CleanDataValue(reader[ColumnNames.CodeSystemCd]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            item.Notes = (String)CleanDataValue(reader[ColumnNames.Notes]);

            return item;
        }
        #endregion
    }
}