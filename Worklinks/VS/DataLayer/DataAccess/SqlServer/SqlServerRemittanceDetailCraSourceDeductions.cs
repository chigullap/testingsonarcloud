﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerRemittanceDetailCraSourceDeductions : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String DummyKey = "row_number";
            public static String BusinessTaxNumber = "business_tax_number";
            public static String ChequeDate = "cheque_date";
            public static String EmployeeCount = "employee_count";
            public static String PaymentAmount = "payment_amount";
            public static String TotalTaxableIncome = "total_taxable_income";
            public static String Period = "period";
            public static String StartDate = "start_date";
            public static String CutoffDate = "cutoff_date";
            public static String PayrollProcessId = "payroll_process_id";
        }
        #endregion

        #region main
        internal RemittanceDetailCraSourceDeductionsCollection Select(DatabaseUser user, long? craExportId, string remitCode, DateTime remitDate)
        {
            DataBaseCommand command = GetStoredProcCommand("RemittanceDetailCraSourceDeductions_report", user.DatabaseName);

            command.AddParameterWithValue("@craExportId", craExportId);
            command.AddParameterWithValue("@remitCode", remitCode);
            command.AddParameterWithValue("@remitDate", remitDate);
            

            using (IDataReader reader = command.ExecuteReader())
                return MapToRemittanceDetailCraSourceDeductionsCollection(reader);
        }
        #endregion

        #region data mapping
        protected RemittanceDetailCraSourceDeductionsCollection MapToRemittanceDetailCraSourceDeductionsCollection(IDataReader reader)
        {
            RemittanceDetailCraSourceDeductionsCollection collection = new RemittanceDetailCraSourceDeductionsCollection();

            while (reader.Read())
                collection.Add(MapToRemittanceDetailCraSourceDeductions(reader));

            return collection;
        }
        protected RemittanceDetailCraSourceDeductions MapToRemittanceDetailCraSourceDeductions(IDataReader reader)
        {
            RemittanceDetailCraSourceDeductions item = new RemittanceDetailCraSourceDeductions();

            item.DummyKey = (long)CleanDataValue(reader[ColumnNames.DummyKey]);
            item.BusinessTaxNumber = (String)CleanDataValue(reader[ColumnNames.BusinessTaxNumber]);
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.EmployeeCount = CleanDataValue(reader[ColumnNames.EmployeeCount]).ToString();
            item.PaymentAmount = (Decimal)CleanDataValue(reader[ColumnNames.PaymentAmount]);
            item.TotalTaxableIncome = (Decimal)CleanDataValue(reader[ColumnNames.TotalTaxableIncome]);
            item.Period = (Decimal)CleanDataValue(reader[ColumnNames.Period]);
            item.StartDate = (DateTime)CleanDataValue(reader[ColumnNames.StartDate]);
            item.CutoffDate = (DateTime)CleanDataValue(reader[ColumnNames.CutoffDate]);
            item.PayrollProcessId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessId]);

            return item;
        }
        #endregion
    }
}