﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerStatutoryHolidayDate : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String StatutoryHolidayDateId = "statutory_holiday_date_id";
            public static String EnglishDescription = "english_description";
            public static String FrenchDescription = "french_description";
            public static String ProvinceStateCode = "code_province_state_cd";
            public static String HolidayDate = "holiday_date";
            public static String OrganizationUnit = "organization_unit";
            public static String Description = "organization_unit_description";
        }
        #endregion

        #region select
        internal StatutoryHolidayDateCollection Report(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, StatutoryDeductionCriteria criteria)
        {
            DataBaseCommand command = GetStoredProcCommand("StatutoryHolidayDate_report", user.DatabaseName);

            command.AddParameterWithValue("@codeLanguage", user.LanguageCode);
            command.AddParameterWithValue("@year", criteria.Year);
            command.AddParameterWithValue("@provinceStateCode", criteria.ProvinceStateCode);
            command.AddParameterWithValue("@description", criteria.Description = criteria.Description == "" ? null : criteria.Description); //if criteria is "", then make it null for the proc, otherwise leave it alone

            using (IDataReader reader = command.ExecuteReader())
                return MapToStatutoryHolidayDateCollection(reader);
        }
        internal StatutoryHolidayDateCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? statutoryHolidayDateId)
        {
            DataBaseCommand command = GetStoredProcCommand("StatutoryHolidayDate_select", user.DatabaseName);

            command.AddParameterWithValue("@statutoryHolidayDateId", statutoryHolidayDateId);
            command.AddParameterWithValue("@codeLanguage", user.LanguageCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToStatutoryHolidayDateCollection(reader);
        }
        #endregion

        #region insert
        public StatutoryHolidayDate Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, StatutoryHolidayDate statutoryHolidayDate)
        {
            using (DataBaseCommand command = GetStoredProcCommand("StatutoryHolidayDate_insert", user.DatabaseName))
            {
                command.BeginTransaction("StatutoryHolidayDate_insert");

                SqlParameter statutoryHolidayDateIdParm = command.AddParameterWithValue("@statutoryHolidayDateId", statutoryHolidayDate.StatutoryHolidayDateId, ParameterDirection.Output);
                command.AddParameterWithValue("@englishDescription", statutoryHolidayDate.EnglishDescription);
                command.AddParameterWithValue("@frenchDescription", statutoryHolidayDate.FrenchDescription);
                command.AddParameterWithValue("@provinceStateCode", statutoryHolidayDate.ProvinceStateCode);
                command.AddParameterWithValue("@holidayDate", statutoryHolidayDate.HolidayDate);
                command.AddParameterWithValue("@organizationUnit", statutoryHolidayDate.OrganizationUnit);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", statutoryHolidayDate.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                statutoryHolidayDate.StatutoryHolidayDateId = Convert.ToInt64(statutoryHolidayDateIdParm.Value);
                statutoryHolidayDate.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return statutoryHolidayDate;
            }
        }
        #endregion

        #region update
        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, StatutoryHolidayDate statutoryHolidayDate)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("StatutoryHolidayDate_update", user.DatabaseName))
                {
                    command.BeginTransaction("StatutoryHolidayDate_update");

                    command.AddParameterWithValue("@statutoryHolidayDateId", statutoryHolidayDate.StatutoryHolidayDateId);
                    command.AddParameterWithValue("@englishDescription", statutoryHolidayDate.EnglishDescription);
                    command.AddParameterWithValue("@frenchDescription", statutoryHolidayDate.FrenchDescription);
                    command.AddParameterWithValue("@provinceStateCode", statutoryHolidayDate.ProvinceStateCode);
                    command.AddParameterWithValue("@holidayDate", statutoryHolidayDate.HolidayDate);
                    command.AddParameterWithValue("@organizationUnit", statutoryHolidayDate.OrganizationUnit);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", statutoryHolidayDate.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    statutoryHolidayDate.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, StatutoryHolidayDate statutoryHolidayDate)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("StatutoryHolidayDate_delete", user.DatabaseName))
                {
                    command.BeginTransaction("StatutoryHolidayDate_delete");

                    command.AddParameterWithValue("@statutoryHolidayDateId", statutoryHolidayDate.StatutoryHolidayDateId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected StatutoryHolidayDateCollection MapToStatutoryHolidayDateCollection(IDataReader reader)
        {
            StatutoryHolidayDateCollection collection = new StatutoryHolidayDateCollection();

            while (reader.Read())
                collection.Add(MapToStatutoryHolidayDate(reader));

            return collection;
        }
        protected StatutoryHolidayDate MapToStatutoryHolidayDate(IDataReader reader)
        {
            StatutoryHolidayDate item = new StatutoryHolidayDate();
            base.MapToBase(item, reader);

            item.StatutoryHolidayDateId = (long)CleanDataValue(reader[ColumnNames.StatutoryHolidayDateId]);
            item.EnglishDescription = (String)CleanDataValue(reader[ColumnNames.EnglishDescription]);
            item.FrenchDescription = (String)CleanDataValue(reader[ColumnNames.FrenchDescription]);
            item.ProvinceStateCode = (String)CleanDataValue(reader[ColumnNames.ProvinceStateCode]);
            item.HolidayDate = (DateTime)CleanDataValue(reader[ColumnNames.HolidayDate]);
            item.OrganizationUnit = (String)CleanDataValue(reader[ColumnNames.OrganizationUnit]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);

            return item;
        }
        #endregion
    }
}