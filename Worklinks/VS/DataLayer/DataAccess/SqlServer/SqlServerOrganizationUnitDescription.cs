﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerOrganizationUnitDescription : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string OrganizationUnitId = "organization_unit_id";
            public static String LanguageCode = "code_language_cd";
            public static String OrganizationUnitDescription = "description";
        }
        #endregion

        #region main
        internal OrganizationUnitDescriptionCollection SelectByOrganizationUnitId(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, OrganizationUnitCriteria criteria)
        {
            using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnitDescription_selectByOrganizationUnitId", user.DatabaseName))
            {
                command.AddParameterWithValue("@organizationUnitId", criteria.OrganizationUnitId);
                OrganizationUnitDescriptionCollection organizationUnitDescRows = null;

                using (IDataReader reader = command.ExecuteReader())
                {
                    organizationUnitDescRows = MapToCodeTableCollection(reader);
                }
                return organizationUnitDescRows;
            }
        }

        public OrganizationUnitDescription Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, OrganizationUnitDescription data)
        {
            using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnitDescription_insert", user.DatabaseName))
            {
                command.BeginTransaction("OrganizationUnitDescription_insert");

                command.AddParameterWithValue("@organizationUnitId", data.OrganizationUnitId);
                command.AddParameterWithValue("@langaugeCode", data.LanguageCode);
                command.AddParameterWithValue("@description", data.UnitDescription);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", data.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                data.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();
                return data;
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, OrganizationUnitDescription data)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnitDescription_update", user.DatabaseName))
                {
                    command.BeginTransaction("OrganizationUnitDescription_update");

                    command.AddParameterWithValue("@organizationUnitId", data.OrganizationUnitId);
                    command.AddParameterWithValue("@languageCode", data.LanguageCode);
                    command.AddParameterWithValue("@description", data.UnitDescription);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", data.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    data.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, OrganizationUnitDescription data)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnitDescription_delete", user.DatabaseName))
                {
                    command.BeginTransaction("OrganizationUnitDescription_delete");
                    command.AddParameterWithValue("@organizationUnitId", data.OrganizationUnitId);
                    command.AddParameterWithValue("@rowVersion", data.RowVersion);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected OrganizationUnitDescriptionCollection MapToCodeTableCollection(IDataReader reader)
        {
            OrganizationUnitDescriptionCollection collection = new OrganizationUnitDescriptionCollection();
            while (reader.Read())
            {
                collection.Add(MapToOrganizationUnitDescRows(reader));
            }
            return collection;
        }
        protected OrganizationUnitDescription MapToOrganizationUnitDescRows(IDataReader reader)
        {
            OrganizationUnitDescription organizationUnitDescription = new OrganizationUnitDescription();
            base.MapToBase(organizationUnitDescription, reader);

            organizationUnitDescription.OrganizationUnitId = (long)CleanDataValue(reader[ColumnNames.OrganizationUnitId]);
            organizationUnitDescription.LanguageCode = (string)CleanDataValue(reader[ColumnNames.LanguageCode]);
            organizationUnitDescription.UnitDescription = (string)CleanDataValue(reader[ColumnNames.OrganizationUnitDescription]);

            return organizationUnitDescription;
        }
        #endregion
    }
}