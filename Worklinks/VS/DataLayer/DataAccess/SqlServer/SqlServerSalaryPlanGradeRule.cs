﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerSalaryPlanGradeRule : SqlServerBase
    {
        public SalaryPlanGradeRuleCollection Select(DatabaseUser user)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SalaryPlanGradeRule_select", user.DatabaseName))
            {
                command.AddParameter(new SqlParameter("@salaryPlanGradeRuleId", SqlDbType.BigInt){Value = null});

                SalaryPlanGradeRuleCollection collection;

                using (IDataReader reader = command.ExecuteReader())
                    collection = MapCollection(reader);

                return collection;
            }
        }

        #region insert
        public SalaryPlanGradeRule Insert(DatabaseUser user, SalaryPlanGradeRule item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SalaryPlanGradeRule_insert", user.DatabaseName))
                {
                    command.BeginTransaction("SalaryPlanGradeRule_insert");

                    SqlParameter idParm = command.AddParameterWithValue("@salaryPlanGradeRuleId", item.SalaryPlanGradeRuleId, ParameterDirection.Output);
                    command.AddParameterWithValue("@automaticAdvanceFlag", item.AutomaticAdvanceFlag);
                    command.AddParameterWithValue("@minimumHour", item.MinimumHour);
                    command.AddParameterWithValue("@codeSalaryPlanAdvanceBasedOnCd", item.CodeSalaryPlanAdvanceBasedOnCd);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.SalaryPlanGradeRuleId = Convert.ToInt64(idParm.Value);
                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();

                    return item;
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
                return null;
            }
        }

        public void Update(DatabaseUser user, SalaryPlanGradeRule item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SalaryPlanGradeRule_update", user.DatabaseName))
                {
                    command.BeginTransaction("SalaryPlanGradeRule_update");

                    command.AddParameterWithValue("@salaryPlanGradeRuleId", item.SalaryPlanGradeRuleId);
                    command.AddParameterWithValue("@automaticAdvanceFlag", item.AutomaticAdvanceFlag);
                    command.AddParameterWithValue("@minimumHour", item.MinimumHour);
                    command.AddParameterWithValue("@codeSalaryPlanAdvanceBasedOnCd", item.CodeSalaryPlanAdvanceBasedOnCd);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        private SalaryPlanGradeRuleCollection MapCollection(IDataReader reader)
        {
            SalaryPlanGradeRuleCollection collection = new SalaryPlanGradeRuleCollection();

            while (reader.Read())
                collection.Add(MapItem(reader));

            return collection;
        }

        protected SalaryPlanGradeRule MapItem(IDataReader reader)
        {
            SalaryPlanGradeRule item = new SalaryPlanGradeRule();
            MapToBase(item, reader);

            item.SalaryPlanGradeRuleId = (long)CleanDataValue(reader[SalaryPlanGradeRuleColumns.SalaryPlanGradeRuleId]);
            item.MinimumHour = (int)CleanDataValue(reader[SalaryPlanGradeRuleColumns.MinimumHour]);
            item.AutomaticAdvanceFlag = (bool) CleanDataValue(reader[SalaryPlanGradeRuleColumns.AutomaticAdvanceFlag]);
            item.CodeSalaryPlanAdvanceBasedOnCd = (string) CleanDataValue(reader[SalaryPlanGradeRuleColumns.SalaryPlanAdvanceBasedOnCode]);

            return item;
        }

        protected class SalaryPlanGradeRuleColumns : ColumnNames
        {
            public const string MinimumHour = "minimum_hour";
            public const string SalaryPlanGradeRuleId = "salary_plan_grade_rule_id";
            public const string AutomaticAdvanceFlag = "automatic_advance_flag";
            public const string SalaryPlanAdvanceBasedOnCode = "code_salary_plan_advance_based_on_cd";
        }
    }
}
