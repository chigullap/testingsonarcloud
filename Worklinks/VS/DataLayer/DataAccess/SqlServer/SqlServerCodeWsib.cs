﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerCodeWsib : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string WsibCode = "code_wsib_cd";
            public static string EnglishDescription = "english_description";
            public static string FrenchDescription = "french_description";
            public static string ImportExternalIdentifier = "import_external_identifier";
            public static string WorkersCompensationAccountNumber = "workers_compensation_account_number";
            public static string ProvinceStateCode = "code_province_state_cd";
            public static string PaycodeCode = "code_paycode_cd";
            public static string SortOrder = "sort_order";
            public static string ActiveFlag = "active_flag";
            public static string SystemFlag = "system_flag";
            public static string ExcludeFromRemittanceExportFlag = "exclude_from_remittance_export_flag";
            public static string GenerateChequeForRemittanceFlag = "generate_cheque_for_remittance_flag";
            public static string CodeWcbChequeRemittanceFrequencyCd = "code_wcb_cheque_remittance_frequency_cd";
            public static string VendorId = "vendor_id";
            public static string BusinessNumberId = "business_number_id";
            public static string HideInEmploymentScreenFlag = "hide_in_employment_screen_flag";
        }
        #endregion

        #region main
        internal CodeWsibCollection Select(DatabaseUser user, string wsibCode)
        {
            DataBaseCommand command = GetStoredProcCommand("WsibCode_select", user.DatabaseName);
            command.AddParameterWithValue("@wsibCode", wsibCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCodeWsibCollection(reader);
        }
        public CodeWsib Insert(DatabaseUser user, CodeWsib codeWsib)
        {
            using (DataBaseCommand command = GetStoredProcCommand("WsibCode_insert", user.DatabaseName))
            {
                command.BeginTransaction("WsibCode_insert");

                SqlParameter wsibCodeParm = command.AddParameterWithValue("@wsibCode", codeWsib.WsibCode, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@englishDescription", codeWsib.EnglishDescription);
                command.AddParameterWithValue("@frenchDescription", codeWsib.FrenchDescription);
                command.AddParameterWithValue("@importExternalIdentifier", codeWsib.ImportExternalIdentifier);
                command.AddParameterWithValue("@workersCompensationAccountNumber", codeWsib.WorkersCompensationAccountNumber);
                command.AddParameterWithValue("@provinceStateCode", codeWsib.ProvinceStateCode);
                command.AddParameterWithValue("@paycodeCode", codeWsib.PaycodeCode);
                command.AddParameterWithValue("@sortOrder", codeWsib.SortOrder);
                command.AddParameterWithValue("@activeFlag", codeWsib.ActiveFlag);
                command.AddParameterWithValue("@systemFlag", codeWsib.SystemFlag);
                command.AddParameterWithValue("@excludeFromRemittanceExportFlag", codeWsib.ExcludeFromRemittanceExportFlag);
                command.AddParameterWithValue("@generateChequeForRemittanceFlag", codeWsib.GenerateChequeForRemittanceFlag);
                command.AddParameterWithValue("@codeWcbChequeRemittanceFrequencyCd", codeWsib.CodeWcbChequeRemittanceFrequencyCd);
                command.AddParameterWithValue("@vendorId", codeWsib.VendorId);
                command.AddParameterWithValue("@businessNumberId", codeWsib.BusinessNumberId);
                command.AddParameterWithValue("@hideInEmploymentScreenFlag", codeWsib.HideInEmploymentScreenFlag);                

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", codeWsib.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                codeWsib.WsibCode = (String)wsibCodeParm.Value;
                codeWsib.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return codeWsib;
            }
        }
        public void Update(DatabaseUser user, CodeWsib codeWsib)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("WsibCode_update", user.DatabaseName))
                {
                    command.BeginTransaction("WsibCode_update");

                    command.AddParameterWithValue("@wsibCode", codeWsib.WsibCode);
                    command.AddParameterWithValue("@englishDescription", codeWsib.EnglishDescription);
                    command.AddParameterWithValue("@frenchDescription", codeWsib.FrenchDescription);
                    command.AddParameterWithValue("@importExternalIdentifier", codeWsib.ImportExternalIdentifier);
                    command.AddParameterWithValue("@workersCompensationAccountNumber", codeWsib.WorkersCompensationAccountNumber);
                    command.AddParameterWithValue("@provinceStateCode", codeWsib.ProvinceStateCode);
                    command.AddParameterWithValue("@paycodeCode", codeWsib.PaycodeCode);
                    command.AddParameterWithValue("@sortOrder", codeWsib.SortOrder);
                    command.AddParameterWithValue("@activeFlag", codeWsib.ActiveFlag);
                    command.AddParameterWithValue("@systemFlag", codeWsib.SystemFlag);
                    command.AddParameterWithValue("@excludeFromRemittanceExportFlag", codeWsib.ExcludeFromRemittanceExportFlag);
                    command.AddParameterWithValue("@generateChequeForRemittanceFlag", codeWsib.GenerateChequeForRemittanceFlag);
                    command.AddParameterWithValue("@codeWcbChequeRemittanceFrequencyCd", codeWsib.CodeWcbChequeRemittanceFrequencyCd);
                    command.AddParameterWithValue("@vendorId", codeWsib.VendorId);
                    command.AddParameterWithValue("@businessNumberId", codeWsib.BusinessNumberId);
                    command.AddParameterWithValue("@hideInEmploymentScreenFlag", codeWsib.HideInEmploymentScreenFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", codeWsib.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    codeWsib.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, CodeWsib codeWsib)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("WsibCode_delete", user.DatabaseName))
                {
                    command.BeginTransaction("WsibCode_delete");

                    command.AddParameterWithValue("@wsibCode", codeWsib.WsibCode);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected CodeWsibCollection MapToCodeWsibCollection(IDataReader reader)
        {
            CodeWsibCollection collection = new CodeWsibCollection();

            while (reader.Read())
                collection.Add(MapToCodeWsib(reader));

            return collection;
        }
        protected CodeWsib MapToCodeWsib(IDataReader reader)
        {
            CodeWsib item = new CodeWsib();
            base.MapToBase(item, reader);

            item.WsibCode = (String)CleanDataValue(reader[ColumnNames.WsibCode]);
            item.EnglishDescription = (String)CleanDataValue(reader[ColumnNames.EnglishDescription]);
            item.FrenchDescription = (String)CleanDataValue(reader[ColumnNames.FrenchDescription]);
            item.ImportExternalIdentifier = (String)CleanDataValue(reader[ColumnNames.ImportExternalIdentifier]);
            item.WorkersCompensationAccountNumber = (String)CleanDataValue(reader[ColumnNames.WorkersCompensationAccountNumber]);
            item.ProvinceStateCode = (String)CleanDataValue(reader[ColumnNames.ProvinceStateCode]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.SortOrder = (short)CleanDataValue(reader[ColumnNames.SortOrder]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.SystemFlag = (bool)CleanDataValue(reader[ColumnNames.SystemFlag]);
            item.ExcludeFromRemittanceExportFlag = (bool)CleanDataValue(reader[ColumnNames.ExcludeFromRemittanceExportFlag]);
            item.GenerateChequeForRemittanceFlag = (bool)CleanDataValue(reader[ColumnNames.GenerateChequeForRemittanceFlag]);
            item.CodeWcbChequeRemittanceFrequencyCd = (string)CleanDataValue(reader[ColumnNames.CodeWcbChequeRemittanceFrequencyCd]);
            item.VendorId = (long?)CleanDataValue(reader[ColumnNames.VendorId]);
            item.BusinessNumberId = (long?)CleanDataValue(reader[ColumnNames.BusinessNumberId]);
            item.HideInEmploymentScreenFlag = (bool)CleanDataValue(reader[ColumnNames.HideInEmploymentScreenFlag]);            

            return item;
        }
        #endregion
    }
}