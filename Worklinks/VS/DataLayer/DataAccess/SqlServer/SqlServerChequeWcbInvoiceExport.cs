﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerChequeWcbInvoiceExport : SqlServerBase
    {
        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ChequeWcbInvoiceExport chequeWcbInvoice)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ChequeWcbInvoiceExport_insert", user.DatabaseName))
            {
                command.BeginTransaction("ChequeWcbInvoiceExport_insert");

                SqlParameter ChequeWcbInvoiceExportIdIdParm = command.AddParameterWithValue("@chequeWcbInvoiceExportId", chequeWcbInvoice.ChequeWcbInvoiceExportId, ParameterDirection.Output);

                command.AddParameterWithValue("@codeWsibCd", chequeWcbInvoice.CodeWsibCd);
                command.AddParameterWithValue("@remittanceDate", chequeWcbInvoice.RemittanceDate);
                command.AddParameterWithValue("@emailSentBySystemId", chequeWcbInvoice.EmailSentBySystem.EmailSentBySystemId);


                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", chequeWcbInvoice.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                chequeWcbInvoice.ChequeWcbInvoiceExportId = Convert.ToInt64(ChequeWcbInvoiceExportIdIdParm.Value);
                chequeWcbInvoice.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();
            }
        }
    }
}