﻿using System;
using System.Data;

using WLP.DataLayer.SqlClient;


namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerBase : IDisposable
    {
        /// <summary>
        /// returns a stored proc command
        /// </summary>
        /// <param name="storedProcedureName"></param>
        /// <returns></returns>
        //protected StoredProcedureCommand GetStoredProcCommand(String storedProcedureName)
        //{
        //    return MainDatabase.GetStoredProcCommand(storedProcedureName);
        //}

        protected StoredProcedureCommand GetStoredProcCommand(String storedProcedureName, String databaseName)
        {
            StoredProcedureCommand command = MainDatabase.GetStoredProcCommand(storedProcedureName);
            command.Connection.ChangeDatabase(databaseName);
            return command;
        }

        protected class ColumnNames
        {
            public static String RowVersion = "row_version";
            public static String CreateUser = "create_user";
            public static String CreateDatetime = "create_datetime";
            public static String UpdateUser = "update_user";
            public static String UpdateDatetime = "update_datetime";
        }

        //protected String UserName
        //{
        //    get { return ((System.Threading.Thread.CurrentPrincipal).Identity).Name; }
        //}

        protected DateTime Time
        {
            get { return DateTime.Now; }
        }

        private Database MainDatabase
        {
            get { return DatabaseInfo.MainDatabase; }
        }

        protected Object CleanDataValue(Object dataValue)
        {
            return dataValue == DBNull.Value ? null : dataValue;
        }

        protected Object ValueOrDBNull(Object dataValue)
        {
            return dataValue == null ? DBNull.Value : dataValue;
        }

        protected int GetReaderColumnIndex(System.Data.SqlClient.SqlDataReader reader, String columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).Equals(columnName))
                {
                    return i;
                }
            }
            return -1;
        }

        protected bool ReaderColumnExists(System.Data.SqlClient.SqlDataReader reader, String columnName)
        {
            if (GetReaderColumnIndex(reader, columnName) < 0)
                return false;
            else
                return true;
        }

        protected bool HandleException(Exception exc, int SqlErrorNumber)
        {
            switch (SqlErrorNumber)
            {
                case 50010:
                    throw new SqlServerException(exc.Message, SqlServerException.ExceptionCodes.DataConcurrency, exc);
                case 50020:
                    throw new SqlServerException(exc.Message, SqlServerException.ExceptionCodes.ForeignKeyConstraint, exc);
                default:
                    throw new SqlServerException(exc.Message, SqlServerException.ExceptionCodes.Other, exc);
            }
        }

        #region data mapping
        internal void MapToBase(WLP.BusinessLayer.BusinessObjects.BusinessObject obj, IDataReader reader)
        {
            MapToBase(obj, reader, true);
        }
        protected bool ColumnExists(IDataReader reader, String columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).Equals(columnName))
                    return true;
            }
            return false;
        }
        internal void MapToBase(WLP.BusinessLayer.BusinessObjects.BusinessObject obj, IDataReader reader, bool mapRowVersion)
        {
            if (mapRowVersion)
            {
                if (CleanDataValue(reader[ColumnNames.RowVersion]) == null || ((byte[])CleanDataValue(reader[ColumnNames.RowVersion])).Length < 1)
                {
                    obj.RowVersion = new byte[8];

                }
                else
                {
                    obj.RowVersion = (byte[])CleanDataValue(reader[ColumnNames.RowVersion]);
                }

            }
            obj.CreateUser = (String)CleanDataValue(reader[ColumnNames.CreateUser]);
            obj.CreateDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.CreateDatetime]);
            obj.UpdateUser = (String)CleanDataValue(reader[ColumnNames.UpdateUser]);
            obj.UpdateDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.UpdateDatetime]);
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
        }

        #endregion
    }
}