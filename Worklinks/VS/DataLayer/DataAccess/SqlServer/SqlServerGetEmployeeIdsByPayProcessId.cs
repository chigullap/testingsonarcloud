﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerGetEmployeeIdsByPayProcessId : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeNumber = "employee_number";
            public static String RowCount = "row_count";
        }
        #endregion

        public String[] GetEmployeesByPayProcess(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            String[] employeeIds = null;
            int rowCount = 0;
            int i = 0;

            DataBaseCommand command = GetStoredProcCommand("GetEmployeeCountByPayProcess_select", user.DatabaseName);
            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
            SqlParameter rowCountParm = command.AddParameterWithValue("@rowCount", rowCount, ParameterDirection.Output);
            rowCountParm.Size = 32;

            command.ExecuteNonQuery();
            rowCount = (int)rowCountParm.Value;

            if (rowCount > 0)
            {
                employeeIds = new String[rowCount];
                
                DataBaseCommand commandProc = GetStoredProcCommand("GetEmployeesByPayProcess_select", user.DatabaseName);
                commandProc.AddParameterWithValue("@payrollProcessId", payrollProcessId);

                using (IDataReader reader = commandProc.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        employeeIds[i] = (String)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
                        i++;
                    }
                }
            }

            return employeeIds;
        }
    }
}
