﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerCanadaRevenueAgencyBondExportFile : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String CanadaRevenueAgencyBondExportId = "canada_revenue_agency_bond_export_id";
            public static String PayrollProcessId = "payroll_process_id";
            public static String TransmitterOrganizationNumber = "transmitter_organization_number";
            public static String OrganizationNumber = "organization_number";
            public static String TransmissionDate = "transmission_date";
            public static String CodeCanadaRevenueAgencyBondPaymentTypeCd = "code_canada_revenue_agency_bond_payment_type_cd";
            public static String ContactFax = "contact_fax";
            public static String ContactEmail = "contact_email";
            public static String ChequeDate = "cheque_date";
            public static String SequenceNumber = "sequence_number";
            public static String SocialInsuranceNumber = "social_insurance_number";
            public static String EmployeeName = "employee_name";
            public static String MasterPayDetailId = "master_pay_detail_id";
            public static String PPaycodeCurrentDollars = "PPaycodeCurrentDollars";
            public static String BirthDate = "birth_date";
        }
        #endregion

        #region main
        internal CanadaRevenueAgencyBondExportFileCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            DataBaseCommand command = GetStoredProcCommand("CanadaRevenueAgencyBondExportFile_select", user.DatabaseName);
            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToCanadaRevenueAgencyBondExportFileCollection(reader);
            }
        }
        #endregion
        
        #region data mapping
        protected CanadaRevenueAgencyBondExportFileCollection MapToCanadaRevenueAgencyBondExportFileCollection(IDataReader reader)
        {
            CanadaRevenueAgencyBondExportFileCollection collection = new CanadaRevenueAgencyBondExportFileCollection();
            while (reader.Read())
            {
                collection.Add(MapToCanadaRevenueAgencyBondExportFile(reader));
            }

            return collection;
        }
        protected CanadaRevenueAgencyBondExportFile MapToCanadaRevenueAgencyBondExportFile(IDataReader reader)
        {
            CanadaRevenueAgencyBondExportFile item = new CanadaRevenueAgencyBondExportFile();
            
            item.CanadaRevenueAgencyBondExportId = (long)CleanDataValue(reader[ColumnNames.CanadaRevenueAgencyBondExportId]);
            item.PayrollProcessId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessId]);
            item.TransmitterOrganizationNumber = (Decimal)CleanDataValue(reader[ColumnNames.TransmitterOrganizationNumber]);
            item.OrganizationNumber = (Decimal)CleanDataValue(reader[ColumnNames.OrganizationNumber]);
            item.TransmissionDate = (DateTime?)CleanDataValue(reader[ColumnNames.TransmissionDate]);
            item.CodeCanadaRevenueAgencyBondPaymentTypeCd = (String)CleanDataValue(reader[ColumnNames.CodeCanadaRevenueAgencyBondPaymentTypeCd]);
            item.ContactFax = (Int64?)CleanDataValue(reader[ColumnNames.ContactFax]);
            item.ContactEmail = (String)CleanDataValue(reader[ColumnNames.ContactEmail]);
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.SequenceNumber = (long)CleanDataValue(reader[ColumnNames.SequenceNumber]);
            item.SocialInsuranceNumber = (String)CleanDataValue(reader[ColumnNames.SocialInsuranceNumber]);
            item.EmployeeName = (String)CleanDataValue(reader[ColumnNames.EmployeeName]);
            item.MasterPayDetailId = (long)CleanDataValue(reader[ColumnNames.MasterPayDetailId]);
            item.PPaycodeCurrentDollars = (Decimal)CleanDataValue(reader[ColumnNames.PPaycodeCurrentDollars]);
            item.BirthDate = (DateTime?)CleanDataValue(reader[ColumnNames.BirthDate]);

            return item;
        }
        #endregion
    }
}