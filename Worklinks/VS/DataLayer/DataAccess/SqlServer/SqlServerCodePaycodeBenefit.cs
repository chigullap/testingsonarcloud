﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerCodePaycodeBenefit : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PaycodeBenefitId = "paycode_benefit_id";
            public static String PaycodeCode = "code_paycode_cd";
            public static String PaycodeRateBasedOnCode = "code_paycode_rate_based_on_cd";
            public static String PaycodeFurtherIdentificationCode = "code_paycode_further_identification_cd";
            public static String TaxOverrideFlag = "tax_override_flag";
            public static String IncomeTaxFlag = "income_tax_flag";
            public static String CanadaQuebecPensionPlanFlag = "canada_quebec_pension_plan_flag";
            public static String EmploymentInsuranceFlag = "employment_insurance_flag";
            public static String ProvincialParentalInsurancePlanFlag = "provincial_parental_insurance_plan_flag";
            public static String NorthWestTerritoryTaxFlag = "north_west_territory_tax_flag";
            public static String NunavutTaxFlag = "nunavut_tax_flag";
            public static String QuebecTaxFlag = "quebec_tax_flag";
            public static String ProvincialHealthTaxFlag = "provincial_health_tax_flag";
            public static String WorkersCompensationBoardFlag = "workers_compensation_board_flag";
            public static String FederalTaxPaycodeCode = "federal_tax_code_paycode_cd";
            public static String QuebecTaxPaycodeCode = "quebec_tax_code_paycode_cd";
            public static String OffsetGeneralLedgerMask = "offset_general_ledger_mask";
            public static String IncludeAsIncomeFlag = "include_as_income_flag";
        }
        #endregion

        #region select
        internal CodePaycodeBenefitCollection Select(DatabaseUser user, String paycodeCode)
        {
            DataBaseCommand command = GetStoredProcCommand("CodePaycodeBenefit_select", user.DatabaseName);

            command.AddParameterWithValue("@paycodeCode", paycodeCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCodeCollection(reader);
        }
        #endregion

        #region insert
        public CodePaycodeBenefit Insert(DatabaseUser user, CodePaycodeBenefit item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CodePaycodeBenefit_insert", user.DatabaseName))
            {
                command.BeginTransaction("CodePaycodeBenefit_insert");

                SqlParameter paycodeBenefitIdParm = command.AddParameterWithValue("@paycodeBenefitId", item.PaycodeBenefitId, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@paycodeCode", item.PaycodeCode);
                command.AddParameterWithValue("@paycodeRateBasedOnCode", item.PaycodeRateBasedOnCode);
                command.AddParameterWithValue("@taxOverrideFlag", item.TaxOverrideFlag);
                command.AddParameterWithValue("@incomeTaxFlag", item.IncomeTaxFlag);
                command.AddParameterWithValue("@canadaQuebecPensionPlanFlag", item.CanadaQuebecPensionPlanFlag);
                command.AddParameterWithValue("@employmentInsuranceFlag", item.EmploymentInsuranceFlag);
                command.AddParameterWithValue("@provincialParentalInsurancePlanFlag", item.ProvincialParentalInsurancePlanFlag);
                command.AddParameterWithValue("@quebecTaxFlag", item.QuebecTaxFlag);
                command.AddParameterWithValue("@provincialHealthTaxFlag", item.ProvincialHealthTaxFlag);
                command.AddParameterWithValue("@workersCompensationBoardFlag", item.WorkersCompensationBoardFlag);
                command.AddParameterWithValue("@federalTaxPaycodeCode", item.FederalTaxPaycodeCode);
                command.AddParameterWithValue("@quebecTaxPaycodeCode", item.QuebecTaxPaycodeCode);
                command.AddParameterWithValue("@offsetGeneralLedgerMask", item.OffsetGeneralLedgerMask);
                command.AddParameterWithValue("@includeAsIncomeFlag", item.IncludeAsIncomeFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.PaycodeBenefitId = (long)paycodeBenefitIdParm.Value;
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, CodePaycodeBenefit item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CodePaycodeBenefit_update", user.DatabaseName))
                {
                    command.BeginTransaction("CodePaycodeBenefit_update");

                    command.AddParameterWithValue("@paycodeCode", item.PaycodeCode);
                    command.AddParameterWithValue("@paycodeRateBasedOnCode", item.PaycodeRateBasedOnCode);
                    command.AddParameterWithValue("@taxOverrideFlag", item.TaxOverrideFlag);
                    command.AddParameterWithValue("@incomeTaxFlag", item.IncomeTaxFlag);
                    command.AddParameterWithValue("@canadaQuebecPensionPlanFlag", item.CanadaQuebecPensionPlanFlag);
                    command.AddParameterWithValue("@employmentInsuranceFlag", item.EmploymentInsuranceFlag);
                    command.AddParameterWithValue("@provincialParentalInsurancePlanFlag", item.ProvincialParentalInsurancePlanFlag);
                    command.AddParameterWithValue("@quebecTaxFlag", item.QuebecTaxFlag);
                    command.AddParameterWithValue("@provincialHealthTaxFlag", item.ProvincialHealthTaxFlag);
                    command.AddParameterWithValue("@workersCompensationBoardFlag", item.WorkersCompensationBoardFlag);
                    command.AddParameterWithValue("@federalTaxPaycodeCode", item.FederalTaxPaycodeCode);
                    command.AddParameterWithValue("@quebecTaxPaycodeCode", item.QuebecTaxPaycodeCode);
                    command.AddParameterWithValue("@offsetGeneralLedgerMask", item.OffsetGeneralLedgerMask);
                    command.AddParameterWithValue("@includeAsIncomeFlag", item.IncludeAsIncomeFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, CodePaycode item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CodePaycodeBenefit_delete", user.DatabaseName))
                {
                    command.BeginTransaction("CodePaycodeBenefit_delete");

                    command.AddParameterWithValue("@paycodeCode", item.PaycodeCode);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected CodePaycodeBenefitCollection MapToCodeCollection(IDataReader reader)
        {
            CodePaycodeBenefitCollection collection = new CodePaycodeBenefitCollection();

            while (reader.Read())
                collection.Add(MapToCode(reader));

            return collection;
        }
        protected CodePaycodeBenefit MapToCode(IDataReader reader)
        {
            CodePaycodeBenefit item = new CodePaycodeBenefit();
            base.MapToBase(item, reader);

            item.PaycodeBenefitId = (long)CleanDataValue(reader[ColumnNames.PaycodeBenefitId]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.PaycodeRateBasedOnCode = (String)CleanDataValue(reader[ColumnNames.PaycodeRateBasedOnCode]);
            item.TaxOverrideFlag = (bool)CleanDataValue(reader[ColumnNames.TaxOverrideFlag]);
            item.IncomeTaxFlag = (bool)CleanDataValue(reader[ColumnNames.IncomeTaxFlag]);
            item.CanadaQuebecPensionPlanFlag = (bool)CleanDataValue(reader[ColumnNames.CanadaQuebecPensionPlanFlag]);
            item.EmploymentInsuranceFlag = (bool)CleanDataValue(reader[ColumnNames.EmploymentInsuranceFlag]);
            item.ProvincialParentalInsurancePlanFlag = (bool)CleanDataValue(reader[ColumnNames.ProvincialParentalInsurancePlanFlag]);
            item.QuebecTaxFlag = (bool)CleanDataValue(reader[ColumnNames.QuebecTaxFlag]);
            item.ProvincialHealthTaxFlag = (bool)CleanDataValue(reader[ColumnNames.ProvincialHealthTaxFlag]);
            item.WorkersCompensationBoardFlag = (bool)CleanDataValue(reader[ColumnNames.WorkersCompensationBoardFlag]);
            item.FederalTaxPaycodeCode = (String)CleanDataValue(reader[ColumnNames.FederalTaxPaycodeCode]);
            item.QuebecTaxPaycodeCode = (String)CleanDataValue(reader[ColumnNames.QuebecTaxPaycodeCode]);
            item.OffsetGeneralLedgerMask = (String)CleanDataValue(reader[ColumnNames.OffsetGeneralLedgerMask]);
            item.IncludeAsIncomeFlag = (bool)CleanDataValue(reader[ColumnNames.IncludeAsIncomeFlag]);

            return item;
        }
        #endregion 
    }
}