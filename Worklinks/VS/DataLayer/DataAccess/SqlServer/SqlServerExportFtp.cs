﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerExportFtp : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ExportFtpId = "export_ftp_id";
            public static String FtpName = "ftp_name";
            public static String ExportFtpTypeCode = "code_export_ftp_type_cd";
            public static String UriName = "uri_name";
            public static String Description = "description";
            public static String Login = "login";
            public static String Password = "password";
            public static String Port = "port";
            public static String EnableSslFlag = "enable_ssl_flag";
            public static String BinaryModeFlag = "binary_mode_flag";
            public static String PassiveModeFlag = "passive_mode_flag";
            public static String HasChildrenFlag = "has_children_flag";
            public static String Directory = "directory";
            public static String UnixPathFlag = "unix_path_flag";
        }
        #endregion

        #region select
        internal ExportFtpCollection Select(DatabaseUser user, long? exportFtpId, String codeExportEftTypeCd, string ftpName)
        {
            DataBaseCommand command = GetStoredProcCommand("ExportFtp_select", user.DatabaseName);
            command.AddParameterWithValue("@exportFtpId", exportFtpId);
            command.AddParameterWithValue("@codeExportEftTypeCd", codeExportEftTypeCd);
            command.AddParameterWithValue("@ftpName", ftpName);

            using (IDataReader reader = command.ExecuteReader())
                return MapToExportFtpCollection(reader);
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, ExportFtp export)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("ExportFtp_update", user.DatabaseName))
                {
                    command.BeginTransaction("ExportFtp_update");

                    command.AddParameterWithValue("@exportFtpId", export.ExportFtpId);
                    command.AddParameterWithValue("@exportFtpTypeCode", export.ExportFtpTypeCode);
                    command.AddParameterWithValue("@ftpName", export.FtpName);
                    command.AddParameterWithValue("@uriName", export.UriName);
                    command.AddParameterWithValue("@description", export.Description);
                    command.AddParameterWithValue("@login", export.Login);
                    command.AddParameterWithValue("@password", export.Password);
                    command.AddParameterWithValue("@port", export.Port);
                    command.AddParameterWithValue("@enableSslFlag", export.EnableSslFlag);
                    command.AddParameterWithValue("@binaryModeFlag", export.BinaryModeFlag);
                    command.AddParameterWithValue("@passiveModeFlag", export.PassiveModeFlag);
                    command.AddParameterWithValue("@directory", export.Directory);
                    command.AddParameterWithValue("@unixPathFlag", export.UnixPathFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", export.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    export.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region insert
        public ExportFtp Insert(DatabaseUser user, ExportFtp export)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ExportFtp_insert", user.DatabaseName))
            {
                command.BeginTransaction("ExportFtp_insert");

                SqlParameter exportIdParm = command.AddParameterWithValue("@exportFtpId", export.ExportFtpId, ParameterDirection.Output);
                command.AddParameterWithValue("@exportFtpTypeCode", export.ExportFtpTypeCode);
                command.AddParameterWithValue("@ftpName", export.FtpName);                
                command.AddParameterWithValue("@uriName", export.UriName);
                command.AddParameterWithValue("@description", export.Description);
                command.AddParameterWithValue("@login", export.Login);
                command.AddParameterWithValue("@password", export.Password);
                command.AddParameterWithValue("@port", export.Port);
                command.AddParameterWithValue("@enableSslFlag", export.EnableSslFlag);
                command.AddParameterWithValue("@binaryModeFlag", export.BinaryModeFlag);
                command.AddParameterWithValue("@passiveModeFlag", export.PassiveModeFlag);
                command.AddParameterWithValue("@directory", export.Directory);
                command.AddParameterWithValue("@unixPathFlag", export.UnixPathFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", export.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                export.ExportFtpId = Convert.ToInt64(exportIdParm.Value);
                export.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return export;
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, ExportFtp export)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("ExportFtp_delete", user.DatabaseName))
                {
                    command.BeginTransaction("ExportFtp_delete");
                    command.AddParameterWithValue("@exportFtpId", export.ExportFtpId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected ExportFtpCollection MapToExportFtpCollection(IDataReader reader)
        {
            ExportFtpCollection collection = new ExportFtpCollection();

            while (reader.Read())
                collection.Add(MapToExportFtp(reader));

            return collection;
        }

        protected ExportFtp MapToExportFtp(IDataReader reader)
        {
            ExportFtp item = new ExportFtp();
            base.MapToBase(item, reader);

            item.ExportFtpId = (long)CleanDataValue(reader[ColumnNames.ExportFtpId]);
            item.ExportFtpTypeCode = (String)CleanDataValue(reader[ColumnNames.ExportFtpTypeCode]);
            item.FtpName = (string)CleanDataValue(reader[ColumnNames.FtpName]);
            item.UriName = (String)CleanDataValue(reader[ColumnNames.UriName]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            item.Login = (String)CleanDataValue(reader[ColumnNames.Login]);
            item.Password = (String)CleanDataValue(reader[ColumnNames.Password]);
            item.Port = (int)CleanDataValue(reader[ColumnNames.Port]);
            item.EnableSslFlag = (bool)CleanDataValue(reader[ColumnNames.EnableSslFlag]);
            item.BinaryModeFlag = (bool)CleanDataValue(reader[ColumnNames.BinaryModeFlag]);
            item.PassiveModeFlag = (bool)CleanDataValue(reader[ColumnNames.PassiveModeFlag]);
            item.HasChildrenFlag = (bool)CleanDataValue(reader[ColumnNames.HasChildrenFlag]);
            item.Directory = (String)CleanDataValue(reader[ColumnNames.Directory]);
            item.UnixPathFlag = (bool)CleanDataValue(reader[ColumnNames.UnixPathFlag]);

            return item;
        }
        #endregion
    }
}