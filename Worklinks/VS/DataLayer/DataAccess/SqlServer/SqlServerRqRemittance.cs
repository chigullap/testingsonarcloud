﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerRqRemittance : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String DummyKey = "row_number";
            public static String RevenuQuebecTaxId = "revenu_quebec_tax_id";
            public static String ChequeDate = "cheque_date";
            public static String EmployeeCount = "employee_count";
            public static String QuebecTax = "quebec_tax";
            public static String TotalQuebecPensionPlanContribution = "total_quebec_pension_plan_contribution";
            public static String TotalHealthTaxLevy = "total_health_tax_levy";
            public static String TotalQuebecParentalInsurancePlanContribution = "total_quebec_parental_insurance_plan_contribution";
            public static String EmployerWorkersCompensationPremium = "employer_workers_compensation_premium";
            public static String RemittancePeriodStartDate = "remittance_period_start_date";
            public static String RemittancePeriodEndDate = "remittance_period_end_date";
            public static String EffectiveEntryDate = "effective_entry_date";
            public static String RqSubdivisionCode = "rq_subdivision_code";
            public static String RqFormCode = "rq_form_code";
        }
        #endregion

        #region main

        //insert happens after a POST of the payroll
        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, decimal periodYear)
        {
            using (DataBaseCommand command = GetStoredProcCommand("RevenuQuebecRemittance_Insert", user.DatabaseName))
            {
                command.BeginTransaction("RevenuQuebecRemittance_Insert");

                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                command.AddParameterWithValue("@periodYear", periodYear);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        public void UpdateRevenuQuebecRemittanceRevenuQuebecExportIdColumn(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long rqExportId, DateTime date, string rqRemittancePeriodCode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("RevenuQuebecRemittance_updateRevenuQuebecExportId", user.DatabaseName))
            {
                command.BeginTransaction("RevenuQuebecRemittance_updateRevenuQuebecExportId");

                command.AddParameterWithValue("@rqExportId", rqExportId);
                command.AddParameterWithValue("@date", date);
                command.AddParameterWithValue("@rqRemittancePeriodCode", rqRemittancePeriodCode);                

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        public RevenuQuebecRemittanceSummaryCollection GetRevenueQuebecExportData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, DateTime date, string rqRemittancePeriodCode)
        {
            DataBaseCommand command = GetStoredProcCommand("RevenuQuebecRemittance_selectSourceData", user.DatabaseName);

            command.AddParameterWithValue("@date", date);
            command.AddParameterWithValue("@rqRemitCode", rqRemittancePeriodCode);
            
            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToRevenuQuebecRemittanceSummaryCollection(reader);
            }
        }
        #endregion

        #region data mapping
        protected RevenuQuebecRemittanceSummaryCollection MapToRevenuQuebecRemittanceSummaryCollection(IDataReader reader)
        {
            RevenuQuebecRemittanceSummaryCollection collection = new RevenuQuebecRemittanceSummaryCollection();
            while (reader.Read())
            {
                collection.Add(MapToRevenuQuebecRemittanceSummary(reader));
            }

            return collection;
        }
        protected RevenuQuebecRemittanceSummary MapToRevenuQuebecRemittanceSummary(IDataReader reader)
        {
            RevenuQuebecRemittanceSummary item = new RevenuQuebecRemittanceSummary();

            item.DummyKey = (long)CleanDataValue(reader[ColumnNames.DummyKey]);
            item.RevenuQuebecTaxId = (String)CleanDataValue(reader[ColumnNames.RevenuQuebecTaxId]);
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.EmployeeCount = (Int64)CleanDataValue(reader[ColumnNames.EmployeeCount]);
            item.QuebecTax = (decimal)CleanDataValue(reader[ColumnNames.QuebecTax]);
            item.TotalQuebecPensionPlanContribution = (decimal)CleanDataValue(reader[ColumnNames.TotalQuebecPensionPlanContribution]);
            item.TotalHealthTaxLevy = (decimal)CleanDataValue(reader[ColumnNames.TotalHealthTaxLevy]);
            item.TotalQuebecParentalInsurancePlanContribution = (decimal)CleanDataValue(reader[ColumnNames.TotalQuebecParentalInsurancePlanContribution]);
            item.EmployerWorkersCompensationPremium = (decimal)CleanDataValue(reader[ColumnNames.EmployerWorkersCompensationPremium]);
            item.RemittancePeriodStartDate = (DateTime?)CleanDataValue(reader[ColumnNames.RemittancePeriodStartDate]);
            item.RemittancePeriodEndDate = (DateTime?)CleanDataValue(reader[ColumnNames.RemittancePeriodEndDate]);
            item.EffectiveEntryDate = (DateTime?)CleanDataValue(reader[ColumnNames.EffectiveEntryDate]);
            item.RqSubdivisionCode = (string)CleanDataValue(reader[ColumnNames.RqSubdivisionCode]);
            item.RqFormCode = (string)CleanDataValue(reader[ColumnNames.RqFormCode]);

            return item;
        }
        #endregion
    }
}
