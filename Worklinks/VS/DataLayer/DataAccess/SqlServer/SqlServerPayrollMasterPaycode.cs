﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerPayrollMasterPaycode : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayrollMasterPaycodeId = "payroll_master_paycode_id";
            public static String HasCurrentFlag = "has_current_flag";
            public static String PayrollMasterId = "payroll_master_id";
            public static String PayrollProcessId = "payroll_process_id";
            public static String PayrollPeriodId = "payroll_period_id";
            public static String EmployeeId = "employee_id";
            public static String CodePaycodeCd = "code_paycode_cd";
            public static String Amount = "amount";
            public static String UnitsInAmount = "units_in_amount";
            public static String PeriodYear = "period_year";
            public static String ChequeMonth = "cheque_month";
            public static String MtdMmount = "mtd_amount";
            public static String MtdUnitsInAmount = "mtd_units_in_amount";
            public static String YtdAmount = "ytd_amount";
            public static String YtdUnitsInAmount = "ytd_units_in_amount";

            //for exports
            public static String CodeCompanyCd = "code_company_cd";
            public static String EmployeeNumber = "employee_number";
            public static String ExternalCode = "external_code";
        }
        #endregion

        #region main
        public PayrollMasterPaycodeCollection Select(DatabaseUser user, long payrollProcessId, bool? includeGarnishments, String paycodeAssociationCode, bool? hasCurrent)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollMasterPaycode_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                command.AddParameterWithValue("@includeGarnishments", includeGarnishments);
                command.AddParameterWithValue("@paycodeAssociationCode", paycodeAssociationCode);
                command.AddParameterWithValue("@hasCurrent", hasCurrent);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToPayrollMasterPaycodeCollection(reader);
            }
        }
        public void DeleteByProcessId(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollMasterPaycode_delete", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollMasterPaycode_delete");

                    command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void GenerateData(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollMasterPaycode_generate", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollMasterPaycode_generateAmount");

                    command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDateTime", DateTime.Now);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected PayrollMasterPaycodeCollection MapToPayrollMasterPaycodeCollection(IDataReader reader)
        {
            PayrollMasterPaycodeCollection collection = new PayrollMasterPaycodeCollection();

            while (reader.Read())
                collection.Add(MapToPayrollMasterPaycode(reader));

            return collection;
        }
        protected PayrollMasterPaycode MapToPayrollMasterPaycode(IDataReader reader)
        {
            PayrollMasterPaycode item = new PayrollMasterPaycode();

            MapToBase(item, reader);

            item.PayrollMasterPaycodeId = (long)CleanDataValue(reader[ColumnNames.PayrollMasterPaycodeId]);
            item.CodeCompanyCd = (String)CleanDataValue(reader[ColumnNames.CodeCompanyCd]);
            item.EmployeeNumber = (String)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.PayrollProcessId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessId]);
            item.CodePaycodeCd = (String)CleanDataValue(reader[ColumnNames.CodePaycodeCd]);
            item.HasCurrentFlag = Convert.ToBoolean(CleanDataValue(reader[ColumnNames.HasCurrentFlag]));
            item.UnitsInAmount = (Decimal)CleanDataValue(reader[ColumnNames.UnitsInAmount]);
            item.Amount = (Decimal)CleanDataValue(reader[ColumnNames.Amount]);
            item.YtdUnitsInAmount = (Decimal)CleanDataValue(reader[ColumnNames.YtdUnitsInAmount]);
            item.YtdAmount = (Decimal)CleanDataValue(reader[ColumnNames.YtdAmount]);
            item.ExternalCode = (String)CleanDataValue(reader[ColumnNames.ExternalCode]);

            return item;
        }
        #endregion
    }
}