﻿using System;
using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerUserAdmin : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PersonId = "person_id";
            public static String LastName = "last_name";
            public static String FirstName = "first_name";
            public static String UserName = "user_name";
            public static String LastLoginDatetime = "last_login_datetime";
            public static String IsLockedOutFlag = "is_locked_out_flag";
            public static String LastLockoutDatetime = "last_lockout_datetime";
            public static String LastPasswordChangedDatetime = "last_password_changed_datetime";
            public static String PasswordExpiryDatetime = "password_expiry_datetime";
            public static String LastLogoutDatetime = "last_logout_datetime";
            public static String LoginExpiryDatetime = "login_expiry_datetime";
            public static String WorklinksAdministrationFlag = "worklinks_administration_flag";
        }
        #endregion

        #region security client user
        public int SelectSecurityClientUser(DatabaseUser user, string authDatabaseName, long securityUserId)
        {
            int rowsReturned = -1;

            using (DataBaseCommand command = GetStoredProcCommand("SecurityClientUser_select", authDatabaseName))
            {
                command.AddParameterWithValue("@importExternalIdentifier", securityUserId.ToString());

                using (IDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        rowsReturned = Convert.ToInt16(reader[0]);
                    }
                }
            }

            return rowsReturned;
        }

        #endregion

        #region select
        internal UserSummaryCollection Select(DatabaseUser user, UserAdminSearchCriteria criteria)
        {
            using (DataBaseCommand command = GetStoredProcCommand("UserSummary_report", user.DatabaseName))
            {
                command.AddParameterWithValue("@lastName", criteria.LastName);
                command.AddParameterWithValue("@firstName", criteria.FirstName);
                command.AddParameterWithValue("@userName", criteria.UserName);
                command.AddParameterWithValue("@isLockedOutFlag", criteria.IsLockedOutFlag);
                command.AddParameterWithValue("@worklinksAdministrationFlag", criteria.WorklinksAdministrationFlag);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToSecurityUserCollection(reader);
            }
        }
        public String GetEmailByEmployeeId(DatabaseUser user, long employeeId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PrimaryEmailByEmployeeId_select", user.DatabaseName))
            {
                String email = "";
                command.AddParameterWithValue("@employeeId", employeeId);

                using (IDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (reader["Email"] != DBNull.Value)
                            email = (String)reader["Email"];
                        else
                            email = null;
                    }
                }
                return email;
            }
        }
        #endregion

        #region data mapping
        protected UserSummaryCollection MapToSecurityUserCollection(IDataReader reader)
        {
            UserSummaryCollection collection = new UserSummaryCollection();

            while (reader.Read())
                collection.Add(MapToSecurityUser(reader));

            return collection;
        }

        protected UserSummary MapToSecurityUser(IDataReader reader)
        {
            UserSummary item = new UserSummary();

            item.PersonId = (long)CleanDataValue(reader[ColumnNames.PersonId]);
            item.LastName = (String)CleanDataValue(reader[ColumnNames.LastName]);
            item.FirstName = (String)CleanDataValue(reader[ColumnNames.FirstName]);
            item.UserName = (String)CleanDataValue(reader[ColumnNames.UserName]);
            item.LastLoginDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.LastLoginDatetime]);
            item.IsLockedOutFlag = (bool)CleanDataValue(reader[ColumnNames.IsLockedOutFlag]);
            item.LastLockoutDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.LastLockoutDatetime]);
            item.LastPasswordChangedDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.LastPasswordChangedDatetime]);
            item.PasswordExpiryDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.PasswordExpiryDatetime]);
            item.LastLogoutDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.LastLogoutDatetime]);
            item.LoginExpiryDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.LoginExpiryDatetime]);
            item.WorklinksAdministrationFlag = (bool)CleanDataValue(reader[ColumnNames.WorklinksAdministrationFlag]);

            return item;
        }
        #endregion
    }
}