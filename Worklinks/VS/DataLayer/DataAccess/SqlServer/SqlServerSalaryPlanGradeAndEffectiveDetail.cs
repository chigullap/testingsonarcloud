﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerSalaryPlanGradeAndEffectiveDetail : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string SalaryPlanGradeEffectiveId = "salary_plan_grade_effective_id";
            public static string EffectiveDate = "effective_date";
            public static string RetroactiveDate = "retroactive_date";
            public static string Name = "name";
            public static string EnglishDescription = "english_description";
            public static string FrenchDescription = "french_description";
            public static string Step1 = "Step1";
            public static string Step2 = "Step2";
            public static string Step3 = "Step3";
        }
        #endregion

        #region select
        internal SalaryPlanGradeAndEffectiveDetailCollection Select(DatabaseUser user, long salaryPlanId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SalaryPlanGradeAndEffectiveDetail_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@salaryPlanId", salaryPlanId);

                SalaryPlanGradeAndEffectiveDetailCollection collection = null;

                using (IDataReader reader = command.ExecuteReader())
                    collection = MapToSalaryPlanGradeAndEffectiveDetailCollection(reader);

                return collection;
            }
        }

        internal SalaryPlanGradeMinEffectiveDateCollection SelectMinEffectiveDate(DatabaseUser user, long organizationUnitId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SalaryPlan_getMinEffectiveDate", user.DatabaseName))
            {
                command.AddParameterWithValue("@OrganizationUnitId", organizationUnitId);

                SalaryPlanGradeMinEffectiveDateCollection collection = null;

                using (IDataReader reader = command.ExecuteReader())
                    collection = MapToSalaryPlanGradeMinEffectiveDateCollection(reader);

                return collection;
            }
        }

        public void AddNewSalaryPlanGradeAndSteps(DatabaseUser user, SalaryPlanGradeAndEffectiveDetail item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SalaryPlan_AddNewSalaryPlanGradeAndSteps", user.DatabaseName))
                {
                    command.BeginTransaction("SalaryPlan_AddNewSalaryPlanGradeAndSteps");

                    command.AddParameterWithValue("@salaryPlanId", item.SalaryPlanId);
                    command.AddParameterWithValue("@organizationUnitId", Convert.ToInt64(item.Name));
                    command.AddParameterWithValue("@effectiveDate", item.EffectiveDate);
                    command.AddParameterWithValue("@retroDate", item.RetroactiveDate);
                    command.AddParameterWithValue("@rate1", item.Step1);
                    command.AddParameterWithValue("@rate2", item.Step2);
                    command.AddParameterWithValue("@rate3", item.Step3);

                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected SalaryPlanGradeAndEffectiveDetailCollection MapToSalaryPlanGradeAndEffectiveDetailCollection(IDataReader reader)
        {
            SalaryPlanGradeAndEffectiveDetailCollection collection = new SalaryPlanGradeAndEffectiveDetailCollection();

            while (reader.Read())
                collection.Add(MapToSalaryPlanGradeAndEffectiveDetail(reader));

            return collection;
        }
        protected SalaryPlanGradeAndEffectiveDetail MapToSalaryPlanGradeAndEffectiveDetail(IDataReader reader)
        {
            SalaryPlanGradeAndEffectiveDetail item = new SalaryPlanGradeAndEffectiveDetail();

            item.SalaryPlanGradeEffectiveId = (long)CleanDataValue(reader[ColumnNames.SalaryPlanGradeEffectiveId]);
            item.EffectiveDate = (DateTime)CleanDataValue(reader[ColumnNames.EffectiveDate]);
            item.RetroactiveDate = (DateTime?)CleanDataValue(reader[ColumnNames.RetroactiveDate]);
            item.Name = (string)CleanDataValue(reader[ColumnNames.Name]);
            item.EnglishDescription = (string)CleanDataValue(reader[ColumnNames.EnglishDescription]);
            item.FrenchDescription = (string)CleanDataValue(reader[ColumnNames.FrenchDescription]);
            item.Step1 = (decimal?)CleanDataValue(reader[ColumnNames.Step1]);
            item.Step2 = (decimal?)CleanDataValue(reader[ColumnNames.Step2]);
            item.Step3 = (decimal?)CleanDataValue(reader[ColumnNames.Step3]);

            return item;
        }

        protected SalaryPlanGradeMinEffectiveDateCollection MapToSalaryPlanGradeMinEffectiveDateCollection(IDataReader reader)
        {
            SalaryPlanGradeMinEffectiveDateCollection collection = new SalaryPlanGradeMinEffectiveDateCollection();

            while (reader.Read())
                collection.Add(MapToSalaryPlanGradeMinEffectiveDate(reader));

            return collection;
        }
        protected SalaryPlanGradeMinEffectiveDate MapToSalaryPlanGradeMinEffectiveDate(IDataReader reader)
        {
            SalaryPlanGradeMinEffectiveDate item = new SalaryPlanGradeMinEffectiveDate();

            item.EffectiveDate = CleanDataValue(reader[ColumnNames.EffectiveDate]).ToString();

            return item;
        }
        #endregion
    }
}