﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerBenefitPolicySearch : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String BenefitPolicyDescriptionId = "benefit_policy_description_id";
            public static String BenefitPolicyId = "benefit_policy_id";
            public static String BenefitPolicyDescriptionField = "description";
        }
        #endregion

        #region main
        internal BenefitPolicySearchCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String criteria)
        {
            DataBaseCommand command = GetStoredProcCommand("BenefitPolicy_report", user.DatabaseName);

            command.AddParameterWithValue("@codeLanguage", user.LanguageCode);
            command.AddParameterWithValue("@benefitPolicyDescription", criteria = criteria == "" ? null : criteria); //if criteria is "", then make it null for the proc, otherwise leave it alone

            using (IDataReader reader = command.ExecuteReader())
                return MapToBenefitPolicyDescriptionCollection(reader);
        }
        #endregion

        #region data mapping
        protected BenefitPolicySearchCollection MapToBenefitPolicyDescriptionCollection(IDataReader reader)
        {
            BenefitPolicySearchCollection collection = new BenefitPolicySearchCollection();

            while (reader.Read())
                collection.Add(MapToBenefitPolicyDescription(reader));

            return collection;
        }

        protected BenefitPolicySearch MapToBenefitPolicyDescription(IDataReader reader)
        {
            BenefitPolicySearch item = new BenefitPolicySearch();
            base.MapToBase(item, reader);

            item.BenefitPolicyDescriptionId = (long)CleanDataValue(reader[ColumnNames.BenefitPolicyDescriptionId]);
            item.BenefitPolicyId = (long)CleanDataValue(reader[ColumnNames.BenefitPolicyId]);
            item.BenefitPolicyDescriptionField = (String)CleanDataValue(reader[ColumnNames.BenefitPolicyDescriptionField]);

            return item;
        }
        #endregion
    }
}
