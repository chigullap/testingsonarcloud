﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerCodeTable : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String CodeTableId = "cd";
            public static String ParentCodeTableId = "parent_cd";
            public static String ActiveFlag = "active_flag";
            public static String SystemFlag = "system_flag";
            public static String SortOrder = "sort_order";
            public static String StartDate = "start_date";
            public static String EndDate = "end_date";
            public static String ImportExternalIdentifier = "import_external_identifier";
        }
        #endregion

        #region main
        /// <summary>
        /// returns code table rows
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        internal CodeTableCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, string tableName, String parentCodeTableName)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CodeTable_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@codeTableName", tableName);
                command.AddParameterWithValue("@parentCodeTableName", parentCodeTableName);

                CodeTableCollection codeTableRows = null;

                using (IDataReader reader = command.ExecuteReader())
                {
                    codeTableRows = MapToCodeTableCollection(reader);
                }

                return codeTableRows;
            }
        }

        public CodeTable Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CodeTable codeTable, string codeTableName, String parentCodeTableName)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CodeTable_update", user.DatabaseName))
                {
                    command.BeginTransaction("CodeTable_update");

                    command.AddParameterWithValue("@codeTableName", codeTableName);
                    command.AddParameterWithValue("@parentCodeTableName", parentCodeTableName);
                    command.AddParameterWithValue("@code", codeTable.CodeTableId);
                    command.AddParameterWithValue("@parentCode", codeTable.ParentCodeTableId);
                    command.AddParameterWithValue("@activeFlag", codeTable.ActiveFlag);
                    command.AddParameterWithValue("@systemFlag", codeTable.SystemFlag);
                    command.AddParameterWithValue("@sortOrder", codeTable.SortOrder);
                    command.AddParameterWithValue("@startDate", codeTable.StartDate);
                    command.AddParameterWithValue("@endDate", codeTable.EndDate);
                    command.AddParameterWithValue("@importExternalIdentifier", codeTable.ImportExternalIdentifier);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", codeTable.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    codeTable.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();

                    return codeTable;
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
                return null;
            }
        }

        public CodeTable Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CodeTable codeTable, string codeTableName, String parentCodeTableName)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CodeTable_insert", user.DatabaseName))
            {
                command.BeginTransaction("CodeTable_insert");

                command.AddParameterWithValue("@codeTableName", codeTableName);
                command.AddParameterWithValue("@parentCodeTableName", parentCodeTableName);
                command.AddParameterWithValue("@code", codeTable.CodeTableId);
                command.AddParameterWithValue("@parentCode", codeTable.ParentCodeTableId);
                command.AddParameterWithValue("@activeFlag", codeTable.ActiveFlag);
                command.AddParameterWithValue("@systemFlag", codeTable.SystemFlag);
                command.AddParameterWithValue("@sortOrder", codeTable.SortOrder);
                command.AddParameterWithValue("@startDate", codeTable.StartDate);
                command.AddParameterWithValue("@endDate", codeTable.EndDate);
                command.AddParameterWithValue("@importExternalIdentifier", codeTable.ImportExternalIdentifier);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", codeTable.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                codeTable.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();

                return codeTable;
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CodeTable codeTable, string codeTableName)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CodeTable_delete", user.DatabaseName))
                {
                    command.BeginTransaction("CodeTable_delete");
                    command.AddParameterWithValue("@codeTableName", codeTableName);
                    command.AddParameterWithValue("@code", codeTable.CodeTableId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        /// <summary>
        /// maps result set to code table collection
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected CodeTableCollection MapToCodeTableCollection(IDataReader reader)
        {
            CodeTableCollection collection = new CodeTableCollection();
            while (reader.Read())
            {
                collection.Add(MapToCodeTableRows(reader));
            }

            return collection;
        }

        /// <summary>
        /// maps columns to code table properties
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected CodeTable MapToCodeTableRows(IDataReader reader)
        {
            CodeTable codeTable = new CodeTable();
            base.MapToBase(codeTable, reader);
            codeTable.CodeTableId = (string)CleanDataValue(reader[ColumnNames.CodeTableId]);
            codeTable.ParentCodeTableId = (string)CleanDataValue(reader[ColumnNames.ParentCodeTableId]);
            codeTable.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            codeTable.SystemFlag = (bool)CleanDataValue(reader[ColumnNames.SystemFlag]);
            codeTable.SortOrder = Convert.ToInt64(CleanDataValue(reader[ColumnNames.SortOrder]));
            codeTable.StartDate = (DateTime?)CleanDataValue(reader[ColumnNames.StartDate]);
            codeTable.EndDate = (DateTime?)CleanDataValue(reader[ColumnNames.EndDate]);
            codeTable.ImportExternalIdentifier = (String)CleanDataValue(reader[ColumnNames.ImportExternalIdentifier]);

            return codeTable;
        }
        #endregion
    }
}