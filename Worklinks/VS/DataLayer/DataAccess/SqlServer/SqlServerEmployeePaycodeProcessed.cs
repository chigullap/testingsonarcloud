﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeePaycodeProcessed : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeePaycodeProcessedId = "employee_paycode_processed_id";
            public static String PayrollProcessId = "payroll_process_id";
            public static String EmployeeId = "employee_id";
            public static String PaycodeCode = "code_paycode_cd";
            public static String Description = "description";
            public static String AmountRate = "amount_rate";
            public static String EmploymentInsuranceInsurableHoursPerUnit = "employment_insurance_insurable_hours_per_unit";
            public static String StartDate = "start_date";
            public static String CutoffDate = "cutoff_date";
            public static String CodeCalculateRateFromCd = "code_calculate_rate_from_cd";
            public static String PayPeriodMinimum = "pay_period_minimum";
            public static String PayPeriodMaximum = "pay_period_maximum";
            public static String MonthlyMaximum = "monthly_maximum";
            public static String YearlyMaximum = "yearly_maximum";
            public static String AmountUnits = "amount_units";
            public static String AmountPercentage = "amount_percentage";
            public static String ExcludeAmount = "exclude_amount";
            public static String ExcludePercentage = "exclude_percentage";
            public static String GroupIncomeFactor = "group_income_factor";
            public static String RoundUpTo = "round_up_to";
            public static String RatePerRoundUpTo = "rate_per_round_up_to";
            public static String ExemptAmount = "exempt_amount";
            public static String ExemptAmountCodeNetGrossCd = "exempt_amount_code_net_gross_cd";
            public static String ExemptAmountCodeBeforeAfterCd = "exempt_amount_code_before_after_cd";
            public static String ExemptPercentage = "exempt_percentage";
            public static String ExemptPercentageCodeNetGrossCd = "exempt_percentage_code_net_gross_cd";
            public static String ExemptPercentageCodeBeforeAfterCd = "exempt_percentage_code_before_after_cd";
            public static String PaycodeTypeCode = "code_paycode_type_cd";
            public static String SharedCount = "shared_count";
            public static String GarnishmentVendorId = "garnishment_vendor_id";
            public static String GarnishmentOrderNumber = "garnishment_order_number";
            public static String OrganizationUnit = "organization_unit";
            public static String ReportGroupCodePaycodeTypeCd = "report_group_code_paycode_type_cd";
            public static String ReportDisplayUnitFlag = "report_display_unit_flag";
            public static String GarnishmentFlag = "garnishment_flag";
            public static String OriginalCreateUser = "original_create_user";
            public static String OriginalCreateDatetime = "original_create_datetime";
            public static String OriginalUpdateUser = "original_update_user";
            public static String OriginalUpdateDatetime = "original_update_datetime";
        }
        #endregion

        #region select
        internal EmployeePaycodeProcessedCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, long employeeId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePaycodeProcessed_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                command.AddParameterWithValue("@employeeId", employeeId);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToEmployeePaycodeProcessedCollection(reader);
            }
        }
        #endregion

        #region insert
        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long[] items, long payrollProcessId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePaycodeProcessed_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeePaycodeProcessed_insert");

                //table parameter
                SqlParameter table = new SqlParameter();
                table.ParameterName = "@parmEmployeeIds";
                table.SqlDbType = SqlDbType.Structured;

                DataTable tablePayrollMaster = LoadEmployeePaycodeProcessedDataTable(items);
                table.Value = tablePayrollMaster;

                command.AddParameter(table);

                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        private DataTable LoadEmployeePaycodeProcessedDataTable(long[] items)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_id", typeof(long));

            for (int i = 0; i < items.Length; i++)
                table.Rows.Add(new Object[] { items[i] });

            return table;
        }
        #endregion

        #region delete
        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePaycodeProcessed_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePaycodeProcessed_delete");

                    command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeePaycodeProcessedCollection MapToEmployeePaycodeProcessedCollection(IDataReader reader)
        {
            EmployeePaycodeProcessedCollection collection = new EmployeePaycodeProcessedCollection();

            while (reader.Read())
                collection.Add(MapToEmployeePaycodeProcessed(reader));

            return collection;
        }
        protected EmployeePaycodeProcessed MapToEmployeePaycodeProcessed(IDataReader reader)
        {
            EmployeePaycodeProcessed item = new EmployeePaycodeProcessed();
            base.MapToBase(item, reader);

            item.EmployeePaycodeProcessedId = (long)CleanDataValue(reader[ColumnNames.EmployeePaycodeProcessedId]);
            item.PayrollProcessId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.AmountRate = (Decimal?)CleanDataValue(reader[ColumnNames.AmountRate]);
            item.EmploymentInsuranceInsurableHoursPerUnit = (Decimal?)CleanDataValue(reader[ColumnNames.EmploymentInsuranceInsurableHoursPerUnit]);
            item.StartDate = (DateTime?)CleanDataValue(reader[ColumnNames.StartDate]);
            item.CutoffDate = (DateTime?)CleanDataValue(reader[ColumnNames.CutoffDate]);
            item.PayPeriodMinimum = (Decimal?)CleanDataValue(reader[ColumnNames.PayPeriodMinimum]);
            item.PayPeriodMaximum = (Decimal?)CleanDataValue(reader[ColumnNames.PayPeriodMaximum]);
            item.MonthlyMaximum = (Decimal?)CleanDataValue(reader[ColumnNames.MonthlyMaximum]);
            item.YearlyMaximum = (Decimal?)CleanDataValue(reader[ColumnNames.YearlyMaximum]);
            item.PaycodeTypeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeTypeCode]);
            item.AmountUnits = (Decimal?)CleanDataValue(reader[ColumnNames.AmountUnits]);
            item.AmountPercentage = (Decimal?)CleanDataValue(reader[ColumnNames.AmountPercentage]);
            item.ExcludeAmount = (Decimal?)CleanDataValue(reader[ColumnNames.ExcludeAmount]);
            item.ExcludePercentage = (Decimal?)CleanDataValue(reader[ColumnNames.ExcludePercentage]);
            item.GroupIncomeFactor = (Decimal?)CleanDataValue(reader[ColumnNames.GroupIncomeFactor]);
            item.RoundUpTo = (Decimal?)CleanDataValue(reader[ColumnNames.RoundUpTo]);
            item.RatePerRoundUpTo = (Decimal?)CleanDataValue(reader[ColumnNames.RatePerRoundUpTo]);
            item.ExemptAmount = (Decimal?)CleanDataValue(reader[ColumnNames.ExemptAmount]);
            item.ExemptAmountCodeNetGrossCd = (String)CleanDataValue(reader[ColumnNames.ExemptAmountCodeNetGrossCd]);
            item.ExemptAmountCodeBeforeAfterCd = (String)CleanDataValue(reader[ColumnNames.ExemptAmountCodeBeforeAfterCd]);
            item.ExemptPercentage = (Decimal?)CleanDataValue(reader[ColumnNames.ExemptPercentage]);
            item.ExemptPercentageCodeNetGrossCd = (String)CleanDataValue(reader[ColumnNames.ExemptPercentageCodeNetGrossCd]);
            item.ExemptPercentageCodeBeforeAfterCd = (String)CleanDataValue(reader[ColumnNames.ExemptPercentageCodeBeforeAfterCd]);
            item.GarnishmentVendorId = (long?)CleanDataValue(reader[ColumnNames.GarnishmentVendorId]);
            item.GarnishmentOrderNumber = (String)CleanDataValue(reader[ColumnNames.GarnishmentOrderNumber]);
            item.OrganizationUnit = (String)CleanDataValue(reader[ColumnNames.OrganizationUnit]);
            item.ReportGroupCodePaycodeTypeCd = (String)CleanDataValue(reader[ColumnNames.ReportGroupCodePaycodeTypeCd]);
            item.ReportDisplayUnitFlag = (bool)CleanDataValue(reader[ColumnNames.ReportDisplayUnitFlag]);
            item.GarnishmentFlag = (bool)CleanDataValue(reader[ColumnNames.GarnishmentFlag]);
            item.OriginalCreateUser = (String)CleanDataValue(reader[ColumnNames.OriginalCreateUser]);
            item.OriginalCreateDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.OriginalCreateDatetime]);
            item.OriginalUpdateUser = (String)CleanDataValue(reader[ColumnNames.OriginalUpdateUser]);
            item.OriginalUpdateDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.OriginalUpdateDatetime]);

            return item;
        }
        #endregion
    }
}