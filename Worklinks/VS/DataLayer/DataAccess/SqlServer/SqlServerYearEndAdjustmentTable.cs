﻿using System;
using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerYearEndAdjustmentTable : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String YearEndAdjustmentTableId = "year_end_adjustment_table_id";
            public static String TableName = "table_name";
            public static String CodeLanguageCode = "code_language_cd";
            public static String Description = "description";
            public static String YearEndFormCode = "code_year_end_form_cd";
        }
        #endregion

        #region main
        internal YearEndAdjustmentTableCollection GetYearEndAdjustmentTables(DatabaseUser user)
        {
            using (DataBaseCommand command = GetStoredProcCommand("YearEndAdjustmentTable_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@codeLanguage", user.LanguageCode);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToYearEndAdjustmentTableCollection(reader);
            }
        }
        #endregion

        #region data mapping
        protected YearEndAdjustmentTableCollection MapToYearEndAdjustmentTableCollection(IDataReader reader)
        {
            YearEndAdjustmentTableCollection collection = new YearEndAdjustmentTableCollection();

            while (reader.Read())
                collection.Add(MapToYearEndAdjustmentTable(reader));

            return collection;
        }
        protected YearEndAdjustmentTable MapToYearEndAdjustmentTable(IDataReader reader)
        {
            YearEndAdjustmentTable yearEnd = new YearEndAdjustmentTable();

            yearEnd.YearEndAdjustmentTableId = (long)CleanDataValue(reader[ColumnNames.YearEndAdjustmentTableId]);
            yearEnd.TableName = (String)CleanDataValue(reader[ColumnNames.TableName]);
            yearEnd.CodeLanguageCode = (String)CleanDataValue(reader[ColumnNames.CodeLanguageCode]);
            yearEnd.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            yearEnd.YearEndFormCode = (String)CleanDataValue(reader[ColumnNames.YearEndFormCode]);

            return yearEnd;
        }
        #endregion
    }
}