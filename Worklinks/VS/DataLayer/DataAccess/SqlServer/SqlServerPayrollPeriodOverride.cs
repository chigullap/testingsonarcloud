﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerPayrollPeriodOverride : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayrollPeriodOverrideId = "payroll_period_override_id";
            public static String PeriodYear = "period_year";
            public static String Period = "period";
            public static String PayrollProcessGroupCode = "code_payroll_process_group_cd";
            public static String StartDate = "start_date";
            public static String CutoffDate = "cutoff_date";
        }
        #endregion

        #region select
        internal PayrollPeriodOverrideCollection Select(DatabaseUser user, String payrollProcessGroupCode, decimal periodYear, decimal period)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollPeriodOverride_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@payrollProcessGroupCode", payrollProcessGroupCode);
                command.AddParameterWithValue("@periodYear", periodYear);
                command.AddParameterWithValue("@period", period);

                PayrollPeriodOverrideCollection payrollPeriods = null;

                using (IDataReader reader = command.ExecuteReader())
                    payrollPeriods = MapToPayrollPeriodOverrideCollection(reader);

                return payrollPeriods;
            }
        }
       
        #endregion

        #region data mapping
        protected PayrollPeriodOverrideCollection MapToPayrollPeriodOverrideCollection(IDataReader reader)
        {
            PayrollPeriodOverrideCollection collection = new PayrollPeriodOverrideCollection();

            while (reader.Read())
                collection.Add(MapToPayrollPeriod(reader));

            return collection;
        }
        protected PayrollPeriodOverride MapToPayrollPeriod(IDataReader reader)
        {
            PayrollPeriodOverride item = new PayrollPeriodOverride();
            base.MapToBase(item, reader);

            item.PayrollPeriodOverrideId = (long)CleanDataValue(reader[ColumnNames.PayrollPeriodOverrideId]);
            item.PeriodYear = (Decimal)CleanDataValue(reader[ColumnNames.PeriodYear]);
            item.Period = (Decimal)CleanDataValue(reader[ColumnNames.Period]);
            item.PayrollProcessGroupCode = (String)CleanDataValue(reader[ColumnNames.PayrollProcessGroupCode]);
            item.StartDate = (DateTime)CleanDataValue(reader[ColumnNames.StartDate]);
            item.CutoffDate = (DateTime)CleanDataValue(reader[ColumnNames.CutoffDate]);

            return item;
        }
      
        #endregion
    }
}