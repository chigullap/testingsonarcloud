﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerWizardCache : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String WizardCacheId = "wizard_cache_id";
            public static String WizardId = "wizard_id";
            public static String TemplateFlag = "template_flag";
            public static String ActiveFlag = "active_flag";
            public static String Description = "description";
            public static String ImportExternalIdentifier = "import_external_identifier";
            public static String CountryCode = "code_country_cd";
        }
        #endregion

        #region select
        internal WizardCacheCollection Select(DatabaseUser user, long? wizardCacheId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("WizardCache_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@wizardCacheId", wizardCacheId);

                WizardCacheCollection wizardCaches = null;

                using (IDataReader reader = command.ExecuteReader())
                    wizardCaches = MapToWizardCacheCollection(reader);

                return wizardCaches;
            }
        }
        public WizardCacheCollection SelectBatch(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            using (DataBaseCommand command = GetStoredProcCommand("WizardCache_batchSelect", user.DatabaseName))
            {
                SqlParameter parmWizardCache = new SqlParameter();
                parmWizardCache.ParameterName = "@parmWizardCache";
                parmWizardCache.SqlDbType = SqlDbType.Structured;

                DataTable tableWizardCache = LoadWizardCacheDataTable(importExternalIdentifiers);
                parmWizardCache.Value = tableWizardCache;
                command.AddParameter(parmWizardCache);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToWizardCacheCollection(reader);
            }
        }
        private DataTable LoadWizardCacheDataTable(List<String> importExternalIdentifiers)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("wizard_cache_id", typeof(long));
            table.Columns.Add("wizard_id", typeof(long));
            table.Columns.Add("template_flag", typeof(bool));
            table.Columns.Add("active_flag", typeof(bool));
            table.Columns.Add("description", typeof(String));
            table.Columns.Add("import_external_identifier", typeof(String));
            table.Columns.Add("country_code_cd", typeof(String));
            table.Columns.Add("wizard_cache_item_id", typeof(long));
            table.Columns.Add("wizard_item_id", typeof(long));
            table.Columns.Add("data_type", typeof(String));
            table.Columns.Add("item", typeof(String));
            table.Columns.Add("visible_flag", typeof(bool));

            for (int i = 0; i < importExternalIdentifiers.Count; i++)
            {
                table.Rows.Add(new Object[]
                {
                    null,
                    null,
                    null,
                    null,
                    null,
                    importExternalIdentifiers[i],
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                });
            }

            return table;
        }
        #endregion

        #region insert
        public void Insert(DatabaseUser user, WizardCache wizardCache)
        {
            using (DataBaseCommand command = GetStoredProcCommand("WizardCache_insert", user.DatabaseName))
            {
                command.BeginTransaction("WizardCache_insert");

                SqlParameter wizardCacheIdParm = command.AddParameterWithValue("@wizardCacheId", wizardCache.WizardCacheId, ParameterDirection.Output);
                command.AddParameterWithValue("@wizardId", wizardCache.WizardId);
                command.AddParameterWithValue("@templateFlag", wizardCache.TemplateFlag);
                command.AddParameterWithValue("@activeFlag", wizardCache.ActiveFlag);
                command.AddParameterWithValue("@description", wizardCache.Description);
                command.AddParameterWithValue("@importExternalIdentifier", wizardCache.ImportExternalIdentifier);
                command.AddParameterWithValue("@countryCode", wizardCache.CountryCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", wizardCache.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                wizardCache.WizardCacheId = Convert.ToInt64(wizardCacheIdParm.Value);
                wizardCache.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, WizardCache wizardCache)
        {
            try
            {
                using (StoredProcedureCommand command = GetStoredProcCommand("WizardCache_update", user.DatabaseName))
                {
                    command.BeginTransaction("WizardCache_update");

                    command.AddParameterWithValue("@wizardCacheId", wizardCache.WizardCacheId);
                    command.AddParameterWithValue("@wizardId", wizardCache.WizardId);
                    command.AddParameterWithValue("@templateFlag", wizardCache.TemplateFlag);
                    command.AddParameterWithValue("@activeFlag", wizardCache.ActiveFlag);
                    command.AddParameterWithValue("@description", wizardCache.Description);
                    command.AddParameterWithValue("@importExternalIdentifier", wizardCache.ImportExternalIdentifier);
                    command.AddParameterWithValue("@countryCode", wizardCache.CountryCode);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", wizardCache.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    wizardCache.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, long wizardCacheId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("WizardCache_delete", user.DatabaseName))
                {
                    command.BeginTransaction("WizardCache_delete");

                    command.AddParameterWithValue("@wizardCacheId", wizardCacheId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region import
        public void Import(DatabaseUser user, WizardCacheImportCollection importCollection)
        {
            using (DataBaseCommand command = GetStoredProcCommand("WizardCache_import", user.DatabaseName))
            {
                command.BeginTransaction("WizardCache_import");

                //table parameter
                SqlParameter parmWizardCache = new SqlParameter();
                parmWizardCache.ParameterName = "@parmWizardCache";
                parmWizardCache.SqlDbType = SqlDbType.Structured;

                DataTable tableWizardCache = LoadWizardCacheDataTable(importCollection);
                parmWizardCache.Value = tableWizardCache;
                command.AddParameter(parmWizardCache);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        private DataTable LoadWizardCacheDataTable(WizardCacheImportCollection importCollection)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("wizard_cache_id", typeof(long));
            table.Columns.Add("wizard_id", typeof(long));
            table.Columns.Add("template_flag", typeof(bool));
            table.Columns.Add("active_flag", typeof(bool));
            table.Columns.Add("description", typeof(String));
            table.Columns.Add("import_external_identifier", typeof(String));
            table.Columns.Add("country_code_cd", typeof(String));
            table.Columns.Add("wizard_cache_item_id", typeof(long));
            table.Columns.Add("wizard_item_id", typeof(long));
            table.Columns.Add("data_type", typeof(String));
            table.Columns.Add("item", typeof(String));
            table.Columns.Add("visible_flag", typeof(bool));

            foreach (WizardCacheImport import in importCollection)
            {
                table.Rows.Add(new Object[]
                {
                    import.WizardCacheId,
                    import.WizardId,
                    import.TemplateFlag,
                    import.ActiveFlag,
                    import.Description,
                    import.ImportExternalIdentifier,
                    import.CountryCode,
                    import.WizardCacheItemId,
                    import.WizardItemId,
                    import.DataType,
                    import.DataXML,
                    import.VisibleFlag
                });
            }

            return table;
        }
        #endregion

        #region data mapping
        protected WizardCacheCollection MapToWizardCacheCollection(IDataReader reader)
        {
            WizardCacheCollection collection = new WizardCacheCollection();

            while (reader.Read())
                collection.Add(MapToWizardCache(reader));

            return collection;
        }
        protected WizardCache MapToWizardCache(IDataReader reader)
        {
            WizardCache item = new WizardCache();
            base.MapToBase(item, reader);

            item.WizardCacheId = (long)CleanDataValue(reader[ColumnNames.WizardCacheId]);
            item.WizardId = (long)CleanDataValue(reader[ColumnNames.WizardId]);
            item.TemplateFlag = (bool)CleanDataValue(reader[ColumnNames.TemplateFlag]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            item.ImportExternalIdentifier = (String)CleanDataValue(reader[ColumnNames.ImportExternalIdentifier]);
            item.CountryCode = (String)CleanDataValue(reader[ColumnNames.CountryCode]);

            return item;
        }
        #endregion
    }
}