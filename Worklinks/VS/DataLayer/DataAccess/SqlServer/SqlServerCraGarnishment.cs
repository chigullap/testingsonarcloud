﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerCraGarnishment : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String RowNumber = "row_number";
            public static String ChequeDate = "cheque_date";
            public static String EmployeeName = "employee_name";
            public static String SocialInsuranceNumber = "social_insurance_number";
            public static String GarnishmentAmount = "amount";
        }
        #endregion

        #region main

        public CraGarnishmentCollection GetCraEftGarnishmentDeductionData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            DataBaseCommand command = GetStoredProcCommand("CraGarnishment_selectSourceData", user.DatabaseName);
            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
            command.AddParameterWithValue("@employeeNumber", employeeNumber);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToCraGarnishmentCollection(reader);
            }
        }

        #endregion

        #region data mapping
        protected CraGarnishmentCollection MapToCraGarnishmentCollection(IDataReader reader)
        {
            CraGarnishmentCollection collection = new CraGarnishmentCollection();
            while (reader.Read())
            {
                collection.Add(MapToCraGarnishment(reader));
            }

            return collection;
        }
        protected CraGarnishment MapToCraGarnishment(IDataReader reader)
        {
            CraGarnishment item = new CraGarnishment();

            item.RowNumber = (long)CleanDataValue(reader[ColumnNames.RowNumber]);
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.EmployeeName = (string)CleanDataValue(reader[ColumnNames.EmployeeName]);
            item.SocialInsuranceNumber = (string)CleanDataValue(reader[ColumnNames.SocialInsuranceNumber]);
            item.GarnishmentAmount = (decimal)CleanDataValue(reader[ColumnNames.GarnishmentAmount]);

            return item;
        }
        #endregion
    }
}
