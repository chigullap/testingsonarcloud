﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeePosition : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeePositionId = "employee_position_id";
            public static String NextEmployeePositionId = "next_employee_position_id";
            public static String EmployeeId = "employee_id";
            public static String EffectiveDate = "effective_date";
            public static String EffectiveSequence = "effective_sequence";
            public static String EmployeePositionActionCode = "code_employee_position_action_cd";
            public static String EmployeePositionReasonCode = "code_employee_position_reason_cd";
            public static String EmployeePositionStatusCode = "code_employee_position_status_cd";
            public static String OrganizationUnitId = "organization_unit_id";
            public static String OrganizationUnitEntryDate = "organization_unit_entry_date";
            public static String PermanencyTypeCode = "code_permanency_type_cd";
            public static String EmploymentTypeCode = "code_employment_type_cd";
            public static String ReportsToEmployeeId = "reports_to_employee_id";
            public static String ShiftCode = "code_shift_cd";
            public static String SalaryPlanId = "salary_plan_id";
            public static String SalaryPlanGradeId = "salary_plan_grade_id";
            public static String SalaryPlanGradeStepId = "salary_plan_grade_step_id";
            public static String EmployeeTypeCode = "code_employee_type_cd";
            public static String PaymentMethodCode = "code_payment_method_cd";
            public static String StandardHours = "standard_hours";
            public static String CompensationAmount = "compensation_amount";
            public static String DeleteBankingInfoFlag = "delete_banking_info_flag";
            public static String LockedStandardHours = "locked_standard_hours";
            public static String LockedCompensationAmount = "locked_compensation_amount";
            public static String AccrualPolicyId = "accrual_policy_id";
            public static String LabourUnionId = "labour_union_id";
            public static String UnionSeniorityDate = "union_seniority_date";
            public static String CreditedServiceDate = "credited_service_date";
            public static String PayrollProcessGroupCode = "code_payroll_process_group_cd";
            public static String PaymentFrequencyCode = "code_payment_frequency_cd";
            public static String PeriodsPerYear = "periods_per_year";
            public static String SeniorityRank = "seniority_rank";
            public static String BenefitPolicyId = "benefit_policy_id";

            //for terminations
            public static String PayrollProcessId = "payroll_process_id";
            public static String CodeTerminationReasonCode = "code_termination_reason_cd";
            public static String Notes = "notes";
            public static String ExtraNotes = "extra_notes";
            public static String CodeNetPayMethodCode = "code_net_pay_method_cd";
            public static String CodeRoeCreationStatusCd = "code_roe_creation_status_cd";
            public static String LastDayPaid = "last_day_paid";
            public static String EmployeeBankingDeleteAccountCode = "code_employee_banking_delete_account_cd";

            //for summary
            public static String RehireDate = "rehire_date";
            public static String HireDate = "hire_date";
            public static String PayrollPeriodCutoffDate = "cutoff_date";
            public static String OrganizationUnitLevelId = "organization_unit_level_id";
            public static String ParentOrganizationUnitId = "parent_organization_unit_id";
            public static String OrganizationUnitStartDate = "organization_unit_start_date";
            public static String EmployeePositionOrganizationUnitId = "employee_position_organization_unit_id";
            public static String OrganizationUnitDescription = "organization_unit_description";

            //for employee employment information import
            public static String ImportExternalIdentifier = "import_external_identifier";

            //org unit lookup
            public static String OrganizationUnit = "organization_unit";
            public static String UsedOnSecondaryEmployeePositionFlag = "used_on_secondary_employee_position_flag";

            //for MapToEmployeePositionEmployeeIdTransactionDateCollectionBulk mapping
            public static string DummyKey = "dummy_key";
        }
        #endregion

        #region select
        internal EmployeePositionCollection Select(DatabaseUser user, EmployeePositionCriteria criteria)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePosition_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@securityOverride", criteria.SecurityOverrideFlag);
                command.AddParameterWithValue("@employeeId", criteria.EmployeeId);
                command.AddParameterWithValue("@employeePositionId", criteria.EmployeePositionId);
                command.AddParameterWithValue("@getCurrentPositionFlag", criteria.GetCurrentPositionFlag);
                command.AddParameterWithValue("@getLatestRecordFlag", criteria.GetLatestRecordFlag);
                command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
                command.AddParameterWithValue("@securityUserId", user.SecurityUserId);
                command.AddParameterWithValue("@nextEmployeePositionId", criteria.NextEmployeePositionId);
                command.AddParameterWithValue("@transactionDate", criteria.TransactionDate);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToEmployeePositionCollection(reader);
            }
        }
        internal EmployeePositionSummaryCollection SelectEmployeePositionSummary(DatabaseUser user, EmployeePositionCriteria criteria)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePosition_report", user.DatabaseName))
            {
                command.AddParameterWithValue("@employeeId", criteria.EmployeeId);
                command.AddParameterWithValue("@languageCode", user.LanguageCode);
                command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
                command.AddParameterWithValue("@securityUserId", user.SecurityUserId);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToEmployeePositionSummaryCollection(reader);
            }
        }

        public EmployeePositionCollection SelectBatchEmployeePosition(DatabaseUser user, EmployeePositionCollection employeePositionIds, bool securityOverrideFlag)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePosition_batchSelect", user.DatabaseName))
            {
                SqlParameter parmEmployeePosition = new SqlParameter();
                parmEmployeePosition.ParameterName = "@parmEmployeePosition";
                parmEmployeePosition.SqlDbType = SqlDbType.Structured;

                DataTable tableEmployeePosition = LoadEmployeePositionDataTable(employeePositionIds);
                parmEmployeePosition.Value = tableEmployeePosition;
                command.AddParameter(parmEmployeePosition);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToEmployeePositionCollection(reader);
            }
        }
        private DataTable LoadEmployeePositionDataTable(EmployeePositionCollection collection)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("EmployeePositionId", typeof(long));

            //loop thru and add values
            for (int i = 0; i < collection.Count; i++)
            {
                table.Rows.Add(new Object[]
                {
                    collection[i].EmployeePositionId
                });
            }

            return table;
        }

        public EmployeePositionEmployeeIdTransactionDateCollection GetBatchPositionsByEmployeeIdAndTransactionDate(DatabaseUser user, Dictionary<string, BusinessLayer.BusinessObjects.PayrollTransaction> data)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionByEmployeeIdAndTransactionDate_batchSelect", user.DatabaseName))
            {
                SqlParameter parmPositionEmployeeIdAndTransactionDate = new SqlParameter();
                parmPositionEmployeeIdAndTransactionDate.ParameterName = "@parmEmployeePositionEmpIdTransDate";
                parmPositionEmployeeIdAndTransactionDate.SqlDbType = SqlDbType.Structured;

                DataTable tablePositionEmployeeIdTransactionDate = LoadEmployeePositionEmployeeIdTransActionDateDataTable(data);
                parmPositionEmployeeIdAndTransactionDate.Value = tablePositionEmployeeIdTransactionDate;
                command.AddParameter(parmPositionEmployeeIdAndTransactionDate);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToEmployeePositionEmployeeIdTransactionDateCollectionBulkCollection(reader);
            }
        }

        private DataTable LoadEmployeePositionEmployeeIdTransActionDateDataTable(Dictionary<string, BusinessLayer.BusinessObjects.PayrollTransaction> data)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_id", typeof(long));
            table.Columns.Add("transaction_date", typeof(DateTime));

            //loop thru and add values
            foreach (KeyValuePair<string, BusinessLayer.BusinessObjects.PayrollTransaction> trans in data)
            {
                table.Rows.Add(new Object[]
                {
                    trans.Value.EmployeeId,
                    trans.Value.TransactionDate
                });
            }

            return table;
        }

        protected EmployeePositionEmployeeIdTransactionDateCollection MapToEmployeePositionEmployeeIdTransactionDateCollectionBulkCollection(IDataReader reader)
        {
            EmployeePositionEmployeeIdTransactionDateCollection collection = new EmployeePositionEmployeeIdTransactionDateCollection();

            while (reader.Read())
                collection.Add(MapToEmployeePositionEmployeeIdTransactionDateCollectionBulk(reader));

            return collection;
        }
        protected EmployeePositionEmployeeIdTransactionDate MapToEmployeePositionEmployeeIdTransactionDateCollectionBulk(IDataReader reader)
        {
            EmployeePositionEmployeeIdTransactionDate item = new EmployeePositionEmployeeIdTransactionDate();
            MapToEmployeePosition(reader, item);
            item.DummyKey = (String)CleanDataValue(reader[ColumnNames.DummyKey]);

            return item;
        }

        public EmployeePositionOrganizationUnitCollection SelectBatchEmployeePositionOrganizationUnitLevelSummary(DatabaseUser user, EmployeePositionCollection positionIds)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionOrganizationUnitLevelSummary_batchSelectByPositionId", user.DatabaseName))
            {
                SqlParameter parmPositionids = new SqlParameter();
                parmPositionids.ParameterName = "@parmPositionIds";
                parmPositionids.SqlDbType = SqlDbType.Structured;

                DataTable tablePositionids = LoadEmployeePositionDataTable(positionIds);
                parmPositionids.Value = tablePositionids;
                command.AddParameter(parmPositionids);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToEmployeePositionOrganizationUnitBulkCollection(reader);
            }
        }
        protected EmployeePositionOrganizationUnitCollection MapToEmployeePositionOrganizationUnitBulkCollection(IDataReader reader)
        {
            EmployeePositionOrganizationUnitCollection collection = new EmployeePositionOrganizationUnitCollection();

            //add item to collection
            while (reader.Read())
                collection.Add(MapToEmployeePositionOrganizationUnitBulk(reader));

            return collection;
        }
        protected EmployeePositionOrganizationUnitBulk MapToEmployeePositionOrganizationUnitBulk(IDataReader reader)
        {
            EmployeePositionOrganizationUnitBulk item = new EmployeePositionOrganizationUnitBulk();
            base.MapToBase(item, reader);

            if (CleanDataValue(reader[ColumnNames.EmployeePositionOrganizationUnitId]) != null)
                item.EmployeePositionOrganizationUnitId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionOrganizationUnitId]);

            item.EmployeePositionId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionId]);
            item.OrganizationUnitLevelId = (long)CleanDataValue(reader[ColumnNames.OrganizationUnitLevelId]);
            item.OrganizationUnitId = (long?)CleanDataValue(reader[ColumnNames.OrganizationUnitId]);
            item.OrganizationUnitStartDate = (DateTime?)CleanDataValue(reader[ColumnNames.OrganizationUnitStartDate]);
            item.UsedOnSecondaryEmployeePositionFlag = (bool)CleanDataValue(reader[ColumnNames.UsedOnSecondaryEmployeePositionFlag]);

            return item;
        }

        public EmployeePositionCollection SelectBatchEmployeePosition(DatabaseUser user, SalaryEmployeeCollection tempEmployees, bool securityOverrideFlag)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePosition_batchSelect", user.DatabaseName))
            {
                SqlParameter parmEmployeePosition = new SqlParameter();
                parmEmployeePosition.ParameterName = "@parmEmployeePosition";
                parmEmployeePosition.SqlDbType = SqlDbType.Structured;

                DataTable tableEmployeePosition = LoadEmployeePositionDataTable(tempEmployees);
                parmEmployeePosition.Value = tableEmployeePosition;
                command.AddParameter(parmEmployeePosition);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToEmployeePositionCollection(reader);
            }
        }
        private DataTable LoadEmployeePositionDataTable(SalaryEmployeeCollection collection)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("EmployeePositionId", typeof(long));

            //loop thru and add values
            for (int i = 0; i < collection.Count; i++)
            {
                table.Rows.Add(new Object[]
                {
                    collection[i].EmployeePositionId
                });
            }

            return table;
        }
        public WorkLinksEmployeePositionCollection SelectLatestBatch(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePosition_batchSelectLatest", user.DatabaseName))
            {
                SqlParameter parmEmployeePosition = new SqlParameter();
                parmEmployeePosition.ParameterName = "@parmEmployeePosition";
                parmEmployeePosition.SqlDbType = SqlDbType.Structured;

                DataTable tableEmployeePosition = LoadEmployeePositionDataTable(importExternalIdentifiers);
                parmEmployeePosition.Value = tableEmployeePosition;
                command.AddParameter(parmEmployeePosition);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToWorkLinksEmployeePositionCollection(reader);
            }
        }
        private DataTable LoadEmployeePositionDataTable(List<String> importExternalIdentifiers)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("import_external_identifier", typeof(String));

            for (int i = 0; i < importExternalIdentifiers.Count; i++)
            {
                table.Rows.Add(new Object[]
                {
                    importExternalIdentifiers[i]
                });
            }

            return table;
        }
        public EmployeePositionCollection CheckForRehiresWithArrearsRecordsForNewPeriod(DatabaseUser user, long payrollPeriodId, bool securityOverride)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionRehireCheckArrears_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@payrollPeriodId", payrollPeriodId);
                command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
                command.AddParameterWithValue("@securityUserId", user.SecurityUserId);
                command.AddParameterWithValue("@securityOverride", securityOverride);

                using (IDataReader reader = command.ExecuteReader())
                    return MapTwoFieldsToEmployeePositionCollection(reader);
            }
        }
        internal SummaryDataForRoeInsert GetSummaryDataForROEInsert(DatabaseUser user, long employeePositionId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("GetROEdata_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@employeePositionId", employeePositionId);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToSummaryDataObject(reader);
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, EmployeePosition item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePosition_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePosition_update");

                    command.AddParameterWithValue("@employeePositionId", item.EmployeePositionId);
                    command.AddParameterWithValue("@nextEmployeePositionId", item.NextEmployeePositionId);
                    command.AddParameterWithValue("@employeeId", item.EmployeeId);
                    command.AddParameterWithValue("@effectiveDate", item.EffectiveDate);
                    command.AddParameterWithValue("@effectiveSequence", item.EffectiveSequence);
                    command.AddParameterWithValue("@employeePositionActionCode", item.EmployeePositionActionCode);
                    command.AddParameterWithValue("@employeePositionReasonCode", item.EmployeePositionReasonCode);
                    command.AddParameterWithValue("@employeePositionStatusCode", item.EmployeePositionStatusCode);
                    command.AddParameterWithValue("@permanencyTypeCode", item.PermanencyTypeCode);
                    command.AddParameterWithValue("@employmentTypeCode", item.EmploymentTypeCode);
                    command.AddParameterWithValue("@reportsToEmployeeId", item.ReportsToEmployeeId);
                    command.AddParameterWithValue("@shiftCode", item.ShiftCode);
                    command.AddParameterWithValue("@salaryPlanId", item.SalaryPlanId);
                    command.AddParameterWithValue("@salaryPlanGradeId", item.SalaryPlanGradeId);
                    command.AddParameterWithValue("@salaryPlanGradeStepId", item.SalaryPlanGradeStepId);
                    command.AddParameterWithValue("@employeeTypeCode", item.EmployeeTypeCode);
                    command.AddParameterWithValue("@paymentMethodCode", item.PaymentMethodCode);
                    command.AddParameterWithValue("@standardHours", item.StandardHours);
                    command.AddParameterWithValue("@compensationAmount", item.CompensationAmount);
                    command.AddParameterWithValue("@deleteBankingInfoFlag", item.DeleteBankingInfoFlag);

                    //new for terminations, if not passed, the update proc nulls the values...
                    command.AddParameterWithValue("@payrollProcessId", item.PayrollProcessId);
                    command.AddParameterWithValue("@codeTerminationReasonCode", item.CodeTerminationReasonCode);
                    command.AddParameterWithValue("@notes", item.Notes);
                    command.AddParameterWithValue("@extraNotes", item.ExtraNotes);
                    command.AddParameterWithValue("@codeNetPayMethodCode", item.CodeNetPayMethodCode);
                    command.AddParameterWithValue("@codeRoeCreationStatusCd", item.CodeRoeCreationStatusCd);
                    command.AddParameterWithValue("@lastDayPaid", item.LastDayPaid);
                    command.AddParameterWithValue("@lockedStandardHours", item.LockedStandardHours);
                    command.AddParameterWithValue("@lockedCompensationAmount", item.LockedCompensationAmount);
                    command.AddParameterWithValue("@accrualPolicyId", item.AccrualPolicyId);
                    command.AddParameterWithValue("@labourUnionId", item.LabourUnionId);
                    command.AddParameterWithValue("@unionSeniorityDate", item.UnionSeniorityDate);
                    command.AddParameterWithValue("@creditedServiceDate", item.CreditedServiceDate);
                    command.AddParameterWithValue("@employeeBankingDeleteAccountCode", item.EmployeeBankingDeleteAccountCode);
                    command.AddParameterWithValue("@seniorityRank", item.SeniorityRank);
                    command.AddParameterWithValue("@organizationUnit", item.OrganizationUnit);
                    command.AddParameterWithValue("@benefitPolicyId", item.BenefitPolicyId);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);

                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    foreach (SqlParameter param in command.Parameters)
                        Debug.Write(param.ToString() + "=\"" + param.Value + "\";\n");

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void UpdatePendingRoesIfNotCompletedAndRehired(DatabaseUser user, EmployeePosition item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePosition_updateRoeCodeOnRehire", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePosition_updateRoeCodeOnRehire");

                    command.AddParameterWithValue("@employeeId", item.EmployeeId);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region insert

        public void BatchInsertUpdate(DatabaseUser user, EmployeePositionCollection items, long payrollProcessId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePosition_batchInsert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeePosition_batchInsert");

                //first table parameter POSITIONS
                SqlParameter parmEmployeePosition = new SqlParameter();
                parmEmployeePosition.ParameterName = "@parmEmployeePosition";
                parmEmployeePosition.SqlDbType = SqlDbType.Structured;

                DataTable tableEmployeePosition = LoadBatchInsertEmployeePositionDataTable(items);
                parmEmployeePosition.Value = tableEmployeePosition;
                command.AddParameter(parmEmployeePosition);

                //second table parameter ORG UNITS
                SqlParameter parmEmployeePositionOrganizationUnits = new SqlParameter();
                parmEmployeePositionOrganizationUnits.ParameterName = "@parmEmployeePositionOrgUnits";
                parmEmployeePositionOrganizationUnits.SqlDbType = SqlDbType.Structured;

                DataTable tableOrganizationUnits = LoadEmployeePositionOrganizationUnitsDataTable(items);
                parmEmployeePositionOrganizationUnits.Value = tableOrganizationUnits;
                command.AddParameter(parmEmployeePositionOrganizationUnits);

                //third table parameter WORKDAYS
                SqlParameter parmEmployeePositionWorkdays = new SqlParameter();
                parmEmployeePositionWorkdays.ParameterName = "@parmEmployeePositionWorkdays";
                parmEmployeePositionWorkdays.SqlDbType = SqlDbType.Structured;

                DataTable tableWorkdays = LoadEmployeePositionWorkdaysDataTable(items);
                parmEmployeePositionWorkdays.Value = tableWorkdays;
                command.AddParameter(parmEmployeePositionWorkdays);

                //fourth table parameter SECONDARY POSITION
                SqlParameter parmEmployeePositionSecondary = new SqlParameter();
                parmEmployeePositionSecondary.ParameterName = "@parmEmployeePositionSecondaryPositions";
                parmEmployeePositionSecondary.SqlDbType = SqlDbType.Structured;

                DataTable tableSecondary = LoadEmployeePositionSecondaryDataTable(items);
                parmEmployeePositionSecondary.Value = tableSecondary;
                command.AddParameter(parmEmployeePositionSecondary);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);
                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        private DataTable LoadBatchInsertEmployeePositionDataTable(EmployeePositionCollection items)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_position_id", typeof(long));
            table.Columns.Add("next_employee_position_id", typeof(long));
            table.Columns.Add("employee_id", typeof(long));
            table.Columns.Add("effective_date", typeof(DateTime));
            table.Columns.Add("effective_sequence", typeof(short));
            table.Columns.Add("code_employee_position_action_cd", typeof(string));
            table.Columns.Add("code_employee_position_reason_cd", typeof(string));
            table.Columns.Add("code_employee_position_status_cd", typeof(string));
            table.Columns.Add("code_permanency_type_cd", typeof(string));
            table.Columns.Add("code_employment_type_cd", typeof(string));
            table.Columns.Add("reports_to_employee_id", typeof(long));
            table.Columns.Add("code_shift_cd", typeof(string));
            table.Columns.Add("salary_plan_id", typeof(long));
            table.Columns.Add("salary_plan_grade_id", typeof(long));
            table.Columns.Add("salary_plan_grade_step_id", typeof(long));
            table.Columns.Add("code_employee_type_cd", typeof(string));
            table.Columns.Add("code_payment_method_cd", typeof(string));
            table.Columns.Add("standard_hours", typeof(Decimal));
            table.Columns.Add("compensation_amount", typeof(Decimal));
            table.Columns.Add("delete_banking_info_flag", typeof(bool));
            table.Columns.Add("payroll_process_id", typeof(long));
            table.Columns.Add("code_termination_reason_cd", typeof(string));
            table.Columns.Add("notes", typeof(string));
            table.Columns.Add("extra_notes", typeof(string));
            table.Columns.Add("code_net_pay_method_cd", typeof(string));
            table.Columns.Add("code_roe_creation_status_cd", typeof(string));
            table.Columns.Add("last_day_paid", typeof(DateTime));
            table.Columns.Add("locked_standard_hours", typeof(Decimal));
            table.Columns.Add("locked_compensation_amount", typeof(Decimal));
            table.Columns.Add("accrual_policy_id", typeof(long));
            table.Columns.Add("labour_union_id", typeof(long));
            table.Columns.Add("union_seniority_date", typeof(DateTime));
            table.Columns.Add("credited_service_date", typeof(DateTime));
            table.Columns.Add("code_employee_banking_delete_account_cd", typeof(string));
            table.Columns.Add("seniority_rank", typeof(int));
            table.Columns.Add("organization_unit", typeof(string));
            table.Columns.Add("benefit_policy_id", typeof(long));

            foreach (EmployeePosition item in items)
            {
                table.Rows.Add(new Object[]
                {
                    item.EmployeePositionId,
                    item.NextEmployeePositionId,
                    item.EmployeeId,
                    item.EffectiveDate,
                    item.EffectiveSequence,
                    item.EmployeePositionActionCode,
                    item.EmployeePositionReasonCode,
                    item.EmployeePositionStatusCode,
                    item.PermanencyTypeCode,
                    item.EmploymentTypeCode,
                    item.ReportsToEmployeeId,
                    item.ShiftCode,
                    item.SalaryPlanId,
                    item.SalaryPlanGradeId,
                    item.SalaryPlanGradeStepId,
                    item.EmployeeTypeCode,
                    item.PaymentMethodCode,
                    item.StandardHours,
                    item.CompensationAmount,
                    item.DeleteBankingInfoFlag,
                    item.PayrollProcessId,
                    item.CodeTerminationReasonCode,
                    item.Notes,
                    item.ExtraNotes,
                    item.CodeNetPayMethodCode,
                    item.CodeRoeCreationStatusCd,
                    item.LastDayPaid,
                    item.LockedStandardHours,
                    item.LockedCompensationAmount,
                    item.AccrualPolicyId,
                    item.LabourUnionId,
                    item.UnionSeniorityDate,
                    item.CreditedServiceDate,
                    item.EmployeeBankingDeleteAccountCode,
                    item.SeniorityRank,
                    item.OrganizationUnit,
                    item.BenefitPolicyId
                });
            }

            return table;
        }
        private DataTable LoadEmployeePositionOrganizationUnitsDataTable(EmployeePositionCollection items)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_position_id", typeof(long));
            table.Columns.Add("organization_unit_id", typeof(long));
            table.Columns.Add("organization_unit_start_date", typeof(DateTime));

            foreach (EmployeePosition item in items)
            {
                foreach (EmployeePositionOrganizationUnit unit in item.EmployeePositionOrganizationUnits)
                {
                    table.Rows.Add(new Object[]
                    {
                        item.EmployeePositionId,
                        unit.OrganizationUnitId,
                        unit.OrganizationUnitStartDate
                    });
                }
            }

            return table;
        }
        private DataTable LoadEmployeePositionWorkdaysDataTable(EmployeePositionCollection items)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_position_id", typeof(long));
            table.Columns.Add("is_workday_flag", typeof(bool));
            table.Columns.Add("day_of_week", typeof(String));

            foreach (EmployeePosition item in items)
            {
                foreach (EmployeePositionWorkday workday in item.Workdays)
                {
                    table.Rows.Add(new Object[]
                    {
                        item.EmployeePositionId,
                        workday.IsWorkdayFlag,
                        workday.DayOfWeek
                    });
                }
            }

            return table;
        }
        private DataTable LoadEmployeePositionSecondaryDataTable(EmployeePositionCollection items)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("dummy_id", typeof(long));
            table.Columns.Add("employee_position_id", typeof(long));
            table.Columns.Add("compensation_amount", typeof(decimal));
            table.Columns.Add("salary_plan_id", typeof(long));
            table.Columns.Add("salary_plan_grade_id", typeof(long));
            table.Columns.Add("salary_plan_grade_step_id", typeof(long));
            table.Columns.Add("organization_unit", typeof(string));
            table.Columns.Add("organization_unit_start_dates", typeof(string));

            int i = 0;

            foreach (EmployeePosition item in items)
            {
                foreach (EmployeePositionSecondary secondary in item.SecondaryPositions)
                {
                    table.Rows.Add(new Object[]
                    {
                        --i,
                        item.EmployeePositionId,
                        secondary.CompensationAmount,
                        secondary.SalaryPlanId,
                        secondary.SalaryPlanGradeId,
                        secondary.SalaryPlanGradeStepId,
                        secondary.OrganizationUnit,
                        secondary.OrganizationUnitDates
                    });
                }
            }

            return table;
        }

        public void Insert(DatabaseUser user, EmployeePosition item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePosition_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeePosition_insert");

                SqlParameter EmployeePositionIdParm = command.AddParameterWithValue("@employeePositionId", item.EmployeePositionId, ParameterDirection.Output);

                command.AddParameterWithValue("@nextEmployeePositionId", item.NextEmployeePositionId);
                command.AddParameterWithValue("@employeeId", item.EmployeeId);
                command.AddParameterWithValue("@effectiveDate", item.EffectiveDate);
                command.AddParameterWithValue("@effectiveSequence", item.EffectiveSequence);
                command.AddParameterWithValue("@employeePositionActionCode", item.EmployeePositionActionCode);
                command.AddParameterWithValue("@employeePositionReasonCode", item.EmployeePositionReasonCode);
                command.AddParameterWithValue("@employeePositionStatusCode", item.EmployeePositionStatusCode);
                command.AddParameterWithValue("@permanencyTypeCode", item.PermanencyTypeCode);
                command.AddParameterWithValue("@employmentTypeCode", item.EmploymentTypeCode);
                command.AddParameterWithValue("@reportsToEmployeeId", item.ReportsToEmployeeId);
                command.AddParameterWithValue("@shiftCode", item.ShiftCode);
                command.AddParameterWithValue("@salaryPlanId", item.SalaryPlanId);
                command.AddParameterWithValue("@salaryPlanGradeId", item.SalaryPlanGradeId);
                command.AddParameterWithValue("@salaryPlanGradeStepId", item.SalaryPlanGradeStepId);
                command.AddParameterWithValue("@employeeTypeCode", item.EmployeeTypeCode);
                command.AddParameterWithValue("@paymentMethodCode", item.PaymentMethodCode);
                command.AddParameterWithValue("@standardHours", item.StandardHours);
                command.AddParameterWithValue("@compensationAmount", item.CompensationAmount);
                command.AddParameterWithValue("@deleteBankingInfoFlag", item.DeleteBankingInfoFlag);

                //new for terminations, if not passed, the update proc nulls the values...
                command.AddParameterWithValue("@payrollProcessId", item.PayrollProcessId);
                command.AddParameterWithValue("@codeTerminationReasonCode", item.CodeTerminationReasonCode);
                command.AddParameterWithValue("@notes", item.Notes);
                command.AddParameterWithValue("@extraNotes", item.ExtraNotes);
                command.AddParameterWithValue("@codeNetPayMethodCode", item.CodeNetPayMethodCode);
                command.AddParameterWithValue("@codeRoeCreationStatusCd", item.CodeRoeCreationStatusCd);
                command.AddParameterWithValue("@lastDayPaid", item.LastDayPaid);
                command.AddParameterWithValue("@lockedStandardHours", item.LockedStandardHours);
                command.AddParameterWithValue("@lockedCompensationAmount", item.LockedCompensationAmount);
                command.AddParameterWithValue("@accrualPolicyId", item.AccrualPolicyId);
                command.AddParameterWithValue("@labourUnionId", item.LabourUnionId);
                command.AddParameterWithValue("@unionSeniorityDate", item.UnionSeniorityDate);
                command.AddParameterWithValue("@creditedServiceDate", item.CreditedServiceDate);
                command.AddParameterWithValue("@employeeBankingDeleteAccountCode", item.EmployeeBankingDeleteAccountCode);
                command.AddParameterWithValue("@seniorityRank", item.SeniorityRank);
                command.AddParameterWithValue("@organizationUnit", item.OrganizationUnit);
                command.AddParameterWithValue("@benefitPolicyId", item.BenefitPolicyId);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.EmployeePositionId = Convert.ToInt64(EmployeePositionIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, EmployeePosition item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePosition_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePosition_delete");

                    command.AddParameterWithValue("@employeePositionId", item.EmployeePositionId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void DeleteEmployeeTermination(DatabaseUser user, long employeePositionId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionTermination_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePositionTermination_delete");

                    command.AddParameterWithValue("@employeePositionId", employeePositionId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region import
        public void ImportEmployeePosition(DatabaseUser user, WorkLinksEmployeePositionCollection items)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePosition_import", user.DatabaseName))
            {
                command.BeginTransaction("EmployeePosition_import");

                //first table parameter
                SqlParameter parmEmployeePosition = new SqlParameter();
                parmEmployeePosition.ParameterName = "@parmEmployeePosition";
                parmEmployeePosition.SqlDbType = SqlDbType.Structured;

                DataTable tableEmployeePosition = LoadEmployeePositionDataTable(items);
                parmEmployeePosition.Value = tableEmployeePosition;
                command.AddParameter(parmEmployeePosition);

                //second table parameter
                SqlParameter parmOrganizationUnit = new SqlParameter();
                parmOrganizationUnit.ParameterName = "@parmOrganizationUnit";
                parmOrganizationUnit.SqlDbType = SqlDbType.Structured;

                DataTable tableOrganizationUnit = LoadEmployeePositionOrganizationUnitDataTable(items);
                parmOrganizationUnit.Value = tableOrganizationUnit;
                command.AddParameter(parmOrganizationUnit);

                //third table parameter
                SqlParameter parmWorkday = new SqlParameter();
                parmWorkday.ParameterName = "@parmWorkday";
                parmWorkday.SqlDbType = SqlDbType.Structured;

                DataTable tableWorkday = LoadEmployeePositionWorkdayDataTable(items);
                parmWorkday.Value = tableWorkday;
                command.AddParameter(parmWorkday);

                //fourth table parameter
                SqlParameter parmSecondary = new SqlParameter();
                parmSecondary.ParameterName = "@parmSecondary";
                parmSecondary.SqlDbType = SqlDbType.Structured;

                DataTable tableSecondary = LoadEmployeePositionSecondaryDataTable(items);
                parmSecondary.Value = tableSecondary;
                command.AddParameter(parmSecondary);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        private DataTable LoadEmployeePositionDataTable(WorkLinksEmployeePositionCollection items)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_position_id", typeof(long));
            table.Columns.Add("next_employee_position_id", typeof(long));
            table.Columns.Add("employee_id", typeof(long));
            table.Columns.Add("effective_date", typeof(DateTime));
            table.Columns.Add("effective_sequence", typeof(short));
            table.Columns.Add("code_employee_position_action_cd", typeof(string));
            table.Columns.Add("code_employee_position_reason_cd", typeof(string));
            table.Columns.Add("code_employee_position_status_cd", typeof(string));
            table.Columns.Add("code_permanency_type_cd", typeof(string));
            table.Columns.Add("code_employment_type_cd", typeof(string));
            table.Columns.Add("reports_to_employee_id", typeof(long));
            table.Columns.Add("code_shift_cd", typeof(string));
            table.Columns.Add("salary_plan_id", typeof(long));
            table.Columns.Add("salary_plan_grade_id", typeof(long));
            table.Columns.Add("salary_plan_grade_step_id", typeof(long));
            table.Columns.Add("code_employee_type_cd", typeof(string));
            table.Columns.Add("code_payment_method_cd", typeof(string));
            table.Columns.Add("standard_hours", typeof(Decimal));
            table.Columns.Add("compensation_amount", typeof(Decimal));
            table.Columns.Add("delete_banking_info_flag", typeof(bool));
            table.Columns.Add("payroll_process_id", typeof(long));
            table.Columns.Add("code_termination_reason_cd", typeof(string));
            table.Columns.Add("notes", typeof(string));
            table.Columns.Add("extra_notes", typeof(string));
            table.Columns.Add("code_net_pay_method_cd", typeof(string));
            table.Columns.Add("code_roe_creation_status_cd", typeof(string));
            table.Columns.Add("last_day_paid", typeof(DateTime));
            table.Columns.Add("locked_standard_hours", typeof(Decimal));
            table.Columns.Add("locked_compensation_amount", typeof(Decimal));
            table.Columns.Add("accrual_policy_id", typeof(long));
            table.Columns.Add("import_external_identifier", typeof(string));
            table.Columns.Add("seniority_rank", typeof(int));
            table.Columns.Add("organization_unit", typeof(string));
            table.Columns.Add("benefit_policy_id", typeof(long));

            foreach (WorkLinksEmployeePosition item in items)
            {
                table.Rows.Add(new Object[]
                {
                    item.EmployeePositionId,
                    item.NextEmployeePositionId,
                    item.EmployeeId,
                    item.EffectiveDate,
                    item.EffectiveSequence,
                    item.EmployeePositionActionCode,
                    item.EmployeePositionReasonCode,
                    item.EmployeePositionStatusCode,
                    item.PermanencyTypeCode,
                    item.EmploymentTypeCode,
                    item.ReportsToEmployeeId,
                    item.ShiftCode,
                    item.SalaryPlanId,
                    item.SalaryPlanGradeId,
                    item.SalaryPlanGradeStepId,
                    item.EmployeeTypeCode,
                    item.PaymentMethodCode,
                    item.StandardHours,
                    item.CompensationAmount,
                    item.DeleteBankingInfoFlag,
                    item.PayrollProcessId,
                    item.CodeTerminationReasonCode,
                    item.Notes,
                    item.ExtraNotes,
                    item.CodeNetPayMethodCode,
                    item.CodeRoeCreationStatusCd,
                    item.LastDayPaid,
                    item.LockedStandardHours,
                    item.LockedCompensationAmount,
                    item.AccrualPolicyId,
                    item.ImportExternalIdentifier,
                    item.SeniorityRank,
                    item.OrganizationUnit,
                    item.BenefitPolicyId
                });
            }

            return table;
        }
        private DataTable LoadEmployeePositionOrganizationUnitDataTable(WorkLinksEmployeePositionCollection items)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_position_organization_unit_id", typeof(long));
            table.Columns.Add("employee_position_id", typeof(long));
            table.Columns.Add("organization_unit_id", typeof(long));
            table.Columns.Add("organization_unit_start_date", typeof(DateTime));
            table.Columns.Add("import_external_identifier", typeof(String));

            foreach (WorkLinksEmployeePosition item in items)
            {
                foreach (EmployeePositionOrganizationUnit unit in item.EmployeePositionOrganizationUnits)
                {
                    table.Rows.Add(new Object[]
                    {
                        unit.EmployeePositionOrganizationUnitId,
                        unit.EmployeePositionId,
                        unit.OrganizationUnitId,
                        unit.OrganizationUnitStartDate,
                        item.ImportExternalIdentifier
                    });
                }
            }

            return table;
        }
        private DataTable LoadEmployeePositionWorkdayDataTable(WorkLinksEmployeePositionCollection items)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_position_workday_id", typeof(long));
            table.Columns.Add("employee_position_id", typeof(long));
            table.Columns.Add("is_workday_flag", typeof(bool));
            table.Columns.Add("day_of_week", typeof(String));
            table.Columns.Add("import_external_identifier", typeof(String));

            foreach (WorkLinksEmployeePosition item in items)
            {
                foreach (EmployeePositionWorkday workday in item.Workdays)
                {
                    table.Rows.Add(new Object[]
                    {
                        workday.EmployeePositionWorkdayId,
                        workday.EmployeePositionId,
                        workday.IsWorkdayFlag,
                        workday.DayOfWeek,
                        item.ImportExternalIdentifier
                    });
                }
            }

            return table;
        }
        private DataTable LoadEmployeePositionSecondaryDataTable(WorkLinksEmployeePositionCollection items)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_position_secondary_id", typeof(long));
            table.Columns.Add("employee_position_id", typeof(long));
            table.Columns.Add("compensation_amount", typeof(decimal));
            table.Columns.Add("salary_plan_id", typeof(long));
            table.Columns.Add("salary_plan_grade_id", typeof(long));
            table.Columns.Add("salary_plan_grade_step_id", typeof(long));
            table.Columns.Add("organization_unit", typeof(string));
            table.Columns.Add("import_external_identifier", typeof(string));
            table.Columns.Add("organization_unit_dates", typeof(string));

            foreach (WorkLinksEmployeePosition item in items)
            {
                foreach (EmployeePositionSecondary secondary in item.SecondaryPositions)
                {
                    table.Rows.Add(new Object[]
                    {
                        secondary.EmployeePositionSecondaryId,
                        secondary.EmployeePositionId,
                        secondary.CompensationAmount,
                        secondary.SalaryPlanId,
                        secondary.SalaryPlanGradeId,
                        secondary.SalaryPlanGradeStepId,
                        secondary.OrganizationUnit,
                        item.ImportExternalIdentifier,
                        secondary.OrganizationUnitDates
                    });
                }
            }

            return table;
        }

        #endregion

        public void ResetDataAttributes(DatabaseUser user, long employeeId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Employee_resetDataAttributes", user.DatabaseName))
            {
                command.BeginTransaction("Employee_resetDataAttributes");

                command.AddParameterWithValue("@employeeId", employeeId);
                command.AddParameterWithValue("@updateUser", user.UserName);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        public DateTime? IsProcessDateOpen(DatabaseUser user, long? payrollProcessId)
        {
            DateTime? processedDate = null;

            DataBaseCommand command = GetStoredProcCommand("EmployeePosition_isPayrollProcessOpen", user.DatabaseName);

            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

            using (IDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                    processedDate = (DateTime?)CleanDataValue(reader[0]);
            }

            return processedDate;
        }

        #region reports
        internal EmployeePositionOrganizationUnitCollection SelectEmployeePositionOrganizationUnitLevelSummary(DatabaseUser user, long employeePositionId)
        {
            DataBaseCommand command = GetStoredProcCommand("GetEmployeePositionOrganizationUnitLevel", user.DatabaseName);
            command.AddParameterWithValue("@employeePositionId", employeePositionId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToEmployeePositionOrganizationUnitLevelSummaryCollection(reader);
        }
        internal String SelectEmployeePositionOrganizationUnitLevelHrxmlMatch(DatabaseUser user, String costCenter)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeePositionOrganizationUnitLevel_select", user.DatabaseName);
            command.AddParameterWithValue("@costCenter", costCenter);

            String item = null;
            using (IDataReader reader = command.ExecuteReader())
            {
                //add item to collection
                while (reader.Read())
                {
                    if (item != null) throw new Exception("More than one organization unit returned from EmployeePositionOrganizationUnitLevel_select");
                    item = (String)CleanDataValue(reader[ColumnNames.OrganizationUnit]);
                }

                return item;
            }
        }
        #endregion

        #region data mapping
        protected EmployeePositionSummaryCollection MapToEmployeePositionSummaryCollection(IDataReader reader)
        {
            EmployeePositionSummaryCollection collection = new EmployeePositionSummaryCollection();

            while (reader.Read())
                collection.Add(MapToEmployeePositionSummary(reader));

            return collection;
        }
        protected EmployeePositionCollection MapToEmployeePositionCollection(IDataReader reader)
        {
            EmployeePositionCollection collection = new EmployeePositionCollection();

            while (reader.Read())
                collection.Add(MapToEmployeePosition(reader));

            return collection;
        }
        protected EmployeePositionCollection MapTwoFieldsToEmployeePositionCollection(IDataReader reader)
        {
            EmployeePositionCollection collection = new EmployeePositionCollection();

            while (reader.Read())
                collection.Add(MapTwoFieldsToEmployeePosition(reader));

            return collection;
        }
        protected WorkLinksEmployeePositionCollection MapToWorkLinksEmployeePositionCollection(IDataReader reader)
        {
            WorkLinksEmployeePositionCollection collection = new WorkLinksEmployeePositionCollection();

            while (reader.Read())
                collection.Add(MapToWorkLinksEmployeePosition(reader));

            return collection;
        }
        protected EmployeePositionSummary MapToEmployeePositionSummary(IDataReader reader)
        {
            EmployeePositionSummary item = new EmployeePositionSummary();
            MapToEmployeePosition(reader, item);
            item.OrganizationUnitDescription = (String)CleanDataValue(reader[ColumnNames.OrganizationUnitDescription]);

            return item;
        }
        protected EmployeePosition MapToEmployeePosition(IDataReader reader)
        {
            EmployeePosition item = new EmployeePosition();
            MapToEmployeePosition(reader, item);

            return item;
        }
        protected EmployeePosition MapTwoFieldsToEmployeePosition(IDataReader reader)
        {
            EmployeePosition item = new EmployeePosition();
            MapTwoFieldsToEmployeePosition(reader, item);

            return item;
        }
        protected WorkLinksEmployeePosition MapToWorkLinksEmployeePosition(IDataReader reader)
        {
            WorkLinksEmployeePosition item = new WorkLinksEmployeePosition();
            MapToEmployeePosition(reader, item);
            item.ImportExternalIdentifier = (String)CleanDataValue(reader[ColumnNames.ImportExternalIdentifier]);

            return item;
        }
        protected EmployeePosition MapToEmployeePosition(IDataReader reader, EmployeePosition item)
        {
            base.MapToBase(item, reader);

            item.EmployeePositionId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionId]);
            item.NextEmployeePositionId = (long?)CleanDataValue(reader[ColumnNames.NextEmployeePositionId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EffectiveDate = (DateTime?)CleanDataValue(reader[ColumnNames.EffectiveDate]);
            item.EffectiveSequence = (short)CleanDataValue(reader[ColumnNames.EffectiveSequence]);
            item.EmployeePositionActionCode = (String)CleanDataValue(reader[ColumnNames.EmployeePositionActionCode]);
            item.EmployeePositionReasonCode = (String)CleanDataValue(reader[ColumnNames.EmployeePositionReasonCode]);
            item.EmployeePositionStatusCode = (String)CleanDataValue(reader[ColumnNames.EmployeePositionStatusCode]);
            item.PermanencyTypeCode = (String)CleanDataValue(reader[ColumnNames.PermanencyTypeCode]);
            item.EmploymentTypeCode = (String)CleanDataValue(reader[ColumnNames.EmploymentTypeCode]);
            item.ReportsToEmployeeId = (long?)CleanDataValue(reader[ColumnNames.ReportsToEmployeeId]);
            item.ShiftCode = (String)CleanDataValue(reader[ColumnNames.ShiftCode]);
            item.SalaryPlanId = (long?)CleanDataValue(reader[ColumnNames.SalaryPlanId]);
            item.SalaryPlanGradeId = (long?)CleanDataValue(reader[ColumnNames.SalaryPlanGradeId]);
            item.SalaryPlanGradeStepId = (long?)CleanDataValue(reader[ColumnNames.SalaryPlanGradeStepId]);
            item.EmployeeTypeCode = (String)CleanDataValue(reader[ColumnNames.EmployeeTypeCode]);
            item.PaymentMethodCode = (String)CleanDataValue(reader[ColumnNames.PaymentMethodCode]);
            item.StandardHours = (Decimal)CleanDataValue(reader[ColumnNames.StandardHours]);
            item.CompensationAmount = (Decimal?)CleanDataValue(reader[ColumnNames.CompensationAmount]);
            item.DeleteBankingInfoFlag = (bool)CleanDataValue(reader[ColumnNames.DeleteBankingInfoFlag]);
            item.LockedStandardHours = (Decimal?)CleanDataValue(reader[ColumnNames.LockedStandardHours]);
            item.LockedCompensationAmount = (Decimal?)CleanDataValue(reader[ColumnNames.LockedCompensationAmount]);
            item.AccrualPolicyId = (long?)CleanDataValue(reader[ColumnNames.AccrualPolicyId]);
            item.LabourUnionId = (long?)CleanDataValue(reader[ColumnNames.LabourUnionId]);
            item.UnionSeniorityDate = (DateTime?)CleanDataValue(reader[ColumnNames.UnionSeniorityDate]);
            item.CreditedServiceDate = (DateTime?)CleanDataValue(reader[ColumnNames.CreditedServiceDate]);
            item.PayrollProcessGroupCode = (String)CleanDataValue(reader[ColumnNames.PayrollProcessGroupCode]);
            item.PaymentFrequencyCode = (String)CleanDataValue(reader[ColumnNames.PaymentFrequencyCode]);
            item.PeriodsPerYear = (Int16)CleanDataValue(reader[ColumnNames.PeriodsPerYear]);
            item.SeniorityRank = (int?)CleanDataValue(reader[ColumnNames.SeniorityRank]);
            item.BenefitPolicyId = (long?)CleanDataValue(reader[ColumnNames.BenefitPolicyId]);

            //for terminations
            item.PayrollProcessId = (long?)CleanDataValue(reader[ColumnNames.PayrollProcessId]);
            item.CodeTerminationReasonCode = (String)CleanDataValue(reader[ColumnNames.CodeTerminationReasonCode]);
            item.Notes = (String)CleanDataValue(reader[ColumnNames.Notes]);
            item.ExtraNotes = (String)CleanDataValue(reader[ColumnNames.ExtraNotes]);
            item.CodeNetPayMethodCode = (String)CleanDataValue(reader[ColumnNames.CodeNetPayMethodCode]);
            item.CodeRoeCreationStatusCd = (String)CleanDataValue(reader[ColumnNames.CodeRoeCreationStatusCd]);
            item.LastDayPaid = (DateTime?)CleanDataValue(reader[ColumnNames.LastDayPaid]);
            item.EmployeeBankingDeleteAccountCode = (String)CleanDataValue(reader[ColumnNames.EmployeeBankingDeleteAccountCode]);

            return item;
        }
        protected EmployeePosition MapTwoFieldsToEmployeePosition(IDataReader reader, EmployeePosition item)
        {
            base.MapToBase(item, reader);

            item.EmployeePositionId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);

            return item;
        }
        protected EmployeePositionOrganizationUnitCollection MapToEmployeePositionOrganizationUnitLevelSummaryCollection(IDataReader reader)
        {
            EmployeePositionOrganizationUnitCollection collection = new EmployeePositionOrganizationUnitCollection();

            //add item to collection
            while (reader.Read())
                collection.Add(MapToEmployeePositionOrganizationUnitLevelSummary(reader));

            return collection;
        }
        protected EmployeePositionOrganizationUnit MapToEmployeePositionOrganizationUnitLevelSummary(IDataReader reader)
        {
            EmployeePositionOrganizationUnit item = new EmployeePositionOrganizationUnit();
            base.MapToBase(item, reader);



            if (CleanDataValue(reader[ColumnNames.EmployeePositionOrganizationUnitId]) != null)
                item.EmployeePositionOrganizationUnitId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionOrganizationUnitId]);

            item.EmployeePositionId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionId]);
            item.OrganizationUnitLevelId = (long)CleanDataValue(reader[ColumnNames.OrganizationUnitLevelId]);
            item.OrganizationUnitId = (long?)CleanDataValue(reader[ColumnNames.OrganizationUnitId]);
            item.OrganizationUnitStartDate = (DateTime?)CleanDataValue(reader[ColumnNames.OrganizationUnitStartDate]);
            item.UsedOnSecondaryEmployeePositionFlag = (bool)CleanDataValue(reader[ColumnNames.UsedOnSecondaryEmployeePositionFlag]);


            return item;
        }
        protected SummaryDataForRoeInsert MapToSummaryDataObject(IDataReader reader)
        {
            SummaryDataForRoeInsert item = new SummaryDataForRoeInsert();

            while (reader.Read())
            {
                item.EffectiveDate = (DateTime?)CleanDataValue(reader[ColumnNames.EffectiveDate]);
                item.CodeTerminationReasonCd = (String)CleanDataValue(reader[ColumnNames.CodeTerminationReasonCode]);
                item.rehireDate = (DateTime?)CleanDataValue(reader[ColumnNames.RehireDate]);
                item.hireDate = (DateTime?)CleanDataValue(reader[ColumnNames.HireDate]);
                item.cutoffDate = (DateTime?)CleanDataValue(reader[ColumnNames.PayrollPeriodCutoffDate]);
            }

            return item;
        }
        #endregion
    }
}