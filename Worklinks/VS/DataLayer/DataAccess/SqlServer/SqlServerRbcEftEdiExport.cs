﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerRbcEftEdiExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayrollMasterPaymentId = "payroll_master_payment_id";
            public static String CodeCompanyCd = "code_company_cd";
            public static String EmployeeNumber = "employee_number";
            public static String EmployeeId = "employee_id";
            public static String PayrollProcessId = "payroll_process_id";
            public static String CodeEmployeeBankingSequenceCode = "code_employee_banking_sequence_cd";
            public static String EmployeeName = "employee_name";
            public static String BankInstCode = "bank_inst_code";
            public static String BankTransactionNumber = "bank_transaction_number";
            public static String EmployeeBankAccountNumber = "employee_bank_account_number";
            public static String ChequeAmount = "cheque_amount";
            public static String ChequeDate = "cheque_date";
            public static String DirectDepositFlag = "direct_deposit_flag";
            public static String RbcSequenceNumber = "sequence_number";
            public static String EmployeeAddressLine1 = "employee_address_line_1";
            public static String EmployeeAddressLine2 = "employee_address_line_2";
            public static String EmployeeCity = "employee_city";
            public static String EmployeeCodeProvinceStateCd = "employee_code_province_state_cd";
            public static String EmployeePostalZipCode = "employee_postal_zip_code";
            public static String EmployeeCodeCountryCd = "employee_code_country_cd";
            public static String CodeStatusCd = "code_status_cd";
            public static String CodeBankAccountTypeCd = "code_bank_account_type_cd";
            public static String CodeBankingCountryCd = "code_banking_country_cd";
            public static String EmployerAddressLine1 = "employer_address_line_1";
            public static String EmployerAddressLine2 = "employer_address_line_2";
            public static String EmployerCity = "employer_city";
            public static String EmployerCodeProvinceStateCd = "employer_code_province_state_cd";
            public static String EmployerPostalZipCode = "employer_postal_zip_code";
            public static String EmployerCodeCountryCd = "employer_code_country_cd";
            public static String ManualChequeFlag = "manual_cheque_flag";
        }
        #endregion

        #region main
        public RbcEftEdiCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            DataBaseCommand command = GetStoredProcCommand("RbcEftEdi_select", user.DatabaseName);
            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
            command.AddParameterWithValue("@employeeNumber", employeeNumber);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToRbcEftEdiCollection(reader);
            }
        }
        #endregion

        #region data mapping
        protected RbcEftEdiCollection MapToRbcEftEdiCollection(IDataReader reader)
        {
            RbcEftEdiCollection collection = new RbcEftEdiCollection();
            while (reader.Read())
            {
                collection.Add(MapToRbcEftEdi(reader));
            }

            return collection;
        }
        protected RbcEftEdi MapToRbcEftEdi(IDataReader reader)
        {
            RbcEftEdi item = new RbcEftEdi();
            base.MapToBase(item, reader);

            item.PayrollMasterPaymentId = (long)CleanDataValue(reader[ColumnNames.PayrollMasterPaymentId]);
            item.CodeCompanyCd = (string)CleanDataValue(reader[ColumnNames.CodeCompanyCd]);
            item.EmployeeNumber = (string)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.PayrollProcessId = Convert.ToString(CleanDataValue(reader[ColumnNames.PayrollProcessId]));
            item.CodeEmployeeBankingSequenceCode = (string)CleanDataValue(reader[ColumnNames.CodeEmployeeBankingSequenceCode]);
            item.EmployeeName = (string)CleanDataValue(reader[ColumnNames.EmployeeName]);
            item.BankInstCode = (string)CleanDataValue(reader[ColumnNames.BankInstCode]);
            item.BankTransactionNumber = (string)CleanDataValue(reader[ColumnNames.BankTransactionNumber]);
            item.EmployeeBankAccountNumber = (string)CleanDataValue(reader[ColumnNames.EmployeeBankAccountNumber]);
            item.ChequeAmount = (decimal)CleanDataValue(reader[ColumnNames.ChequeAmount]);
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.DirectDepositFlag = (bool)CleanDataValue(reader[ColumnNames.DirectDepositFlag]);
            item.RbcSequenceNumber = (long?)CleanDataValue(reader[ColumnNames.RbcSequenceNumber]);
            item.EmployeeAddressLine1 = (string)CleanDataValue(reader[ColumnNames.EmployeeAddressLine1]);
            item.EmployeeAddressLine2 = (string)CleanDataValue(reader[ColumnNames.EmployeeAddressLine2]);
            item.EmployeeCity = (string)CleanDataValue(reader[ColumnNames.EmployeeCity]);
            item.EmployeeProvinceStateCode = (string)CleanDataValue(reader[ColumnNames.EmployeeCodeProvinceStateCd]);
            item.EmployeePostalZipCode = (string)CleanDataValue(reader[ColumnNames.EmployeePostalZipCode]);
            item.EmployeeCountryCode = (string)CleanDataValue(reader[ColumnNames.EmployeeCodeCountryCd]);
            item.CodeStatusCd = (string)CleanDataValue(reader[ColumnNames.CodeStatusCd]);
            item.CodeBankAccountTypeCd = (string)CleanDataValue(reader[ColumnNames.CodeBankAccountTypeCd]);
            item.CodeBankingCountryCd = (string)CleanDataValue(reader[ColumnNames.CodeBankingCountryCd]);
            item.EmployerAddressLine1 = (string)CleanDataValue(reader[ColumnNames.EmployerAddressLine1]);
            item.EmployerAddressLine2 = (string)CleanDataValue(reader[ColumnNames.EmployerAddressLine2]);
            item.EmployerCity = (string)CleanDataValue(reader[ColumnNames.EmployerCity]);
            item.EmployerProvinceStateCode = (string)CleanDataValue(reader[ColumnNames.EmployerCodeProvinceStateCd]);
            item.EmployerPostalZipCode = (string)CleanDataValue(reader[ColumnNames.EmployerPostalZipCode]);
            item.EmployerCountryCode = (string)CleanDataValue(reader[ColumnNames.EmployerCodeCountryCd]);
            item.ManualChequeFlag = (bool)CleanDataValue(reader[ColumnNames.ManualChequeFlag]);

            return item;
        }
        #endregion
    }
}
