﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;



namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerSalaryEmployee : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeePositionId = "employee_position_id";
            public static String EmployeeId = "employee_id";
            public static String CompensationAmount = "compensation_amount";
            public static String CodePayrollProcessGroupCd = "code_payroll_process_group_cd";
            public static String CodeEmployeePositionStatusCd = "code_employee_position_status_cd";
            public static String StandardHours = "standard_hours";
            public static String CodePaymentMethodCd = "code_payment_method_cd";
            public static String LockedCompensationAmount = "locked_compensation_amount";
            public static String LockedStandardHours = "locked_standard_hours";
            public static String CanBeProratedFlag = "can_be_prorated_flag";
            public static String PaycodeCode = "code_paycode_cd";
            public static String AmountRateFactor = "amount_rate_factor";
            public static String AutoPopulateRateFlag = "auto_populate_rate_flag";
            public static String IncludeEmploymentInsuranceHoursFlag = "include_employment_insurance_hours_flag";
            public static String UseSalaryStandardHourFlag = "use_salary_standard_hour_flag";
            public static String PrimaryRecordFlag = "primary_record_flag";
            public static String WorkdayCount = "workday_count";
            public static String WorkDaysPerWeek = "work_days_per_week";
            public static String HoursPerWeek = "hours_per_week";
            public static String StartDateRange = "start_date_range";
            public static String EndDateRange = "end_date_range";
            public static String OffsetHour = "offset_hour";
        }
        #endregion

        #region main

        internal SalaryEmployeeCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String payrollProcessGroupCode, bool prorationEnabledFlag)
        {
            DataBaseCommand command = GetStoredProcCommand("SalaryEmployee_select", user.DatabaseName);
            command.AddParameterWithValue("@codePayrollProcessGroupCd", payrollProcessGroupCode);
            command.AddParameterWithValue("@prorationEnabledFlag", prorationEnabledFlag);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToSalaryEmployeeCollection(reader);
            }
        }
        #endregion

        #region data mapping

        protected SalaryEmployeeCollection MapToSalaryEmployeeCollection(IDataReader reader)
        {
            SalaryEmployeeCollection collection = new SalaryEmployeeCollection();
            while (reader.Read())
            {
                collection.Add(MapToSalaryEmployee(reader));
            }

            return collection;
        }

        protected SalaryEmployee MapToSalaryEmployee(IDataReader reader)
        {
            SalaryEmployee item = new SalaryEmployee();
            
            //base.MapToBase(item, reader);  no need to capture updateuser/rowversion/updatedatetime as the returned values will be used in calcs and inserted into table with new rowversions.

            item.EmployeePositionId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.CompensationAmount = (Decimal)CleanDataValue(reader[ColumnNames.CompensationAmount]);
            item.CodePayrollProcessGroupCd = (String)CleanDataValue(reader[ColumnNames.CodePayrollProcessGroupCd]);
            item.CodeEmployeePositionStatusCd = (String)CleanDataValue(reader[ColumnNames.CodeEmployeePositionStatusCd]);
            item.StandardHours = (Decimal)CleanDataValue(reader[ColumnNames.StandardHours]);
            item.CodePaymentMethodCd = (String)CleanDataValue(reader[ColumnNames.CodePaymentMethodCd]);
            item.LockedCompensationAmount = (Decimal?)CleanDataValue(reader[ColumnNames.LockedCompensationAmount]);
            item.LockedStandardHours = (Decimal?)CleanDataValue(reader[ColumnNames.LockedStandardHours]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);

            if (CleanDataValue(reader[ColumnNames.AmountRateFactor]) != null)
                item.AmountRateFactor = (Decimal)CleanDataValue(reader[ColumnNames.AmountRateFactor]);
            if (CleanDataValue(reader[ColumnNames.AutoPopulateRateFlag]) != null)
                item.AutoPopulateRateFlag = (bool)CleanDataValue(reader[ColumnNames.AutoPopulateRateFlag]);
            if (CleanDataValue(reader[ColumnNames.IncludeEmploymentInsuranceHoursFlag]) != null)
                item.IncludeEmploymentInsuranceHoursFlag = (bool)CleanDataValue(reader[ColumnNames.IncludeEmploymentInsuranceHoursFlag]);
            if (CleanDataValue(reader[ColumnNames.UseSalaryStandardHourFlag]) != null)
                item.UseSalaryStandardHourFlag = (bool)CleanDataValue(reader[ColumnNames.UseSalaryStandardHourFlag]);
            if (CleanDataValue(reader[ColumnNames.StartDateRange]) != null)
                item.StartDateRange = (DateTime)CleanDataValue(reader[ColumnNames.StartDateRange]);
            if (CleanDataValue(reader[ColumnNames.EndDateRange]) != null)
                item.EndDateRange = (DateTime)CleanDataValue(reader[ColumnNames.EndDateRange]);
            if (CleanDataValue(reader[ColumnNames.OffsetHour]) != null)
                item.OffsetHour = (Decimal)CleanDataValue(reader[ColumnNames.OffsetHour]);
            

            item.PrimaryRecordFlag = (bool)CleanDataValue(reader[ColumnNames.PrimaryRecordFlag]);
            item.WorkdayCount = (int?)CleanDataValue(reader[ColumnNames.WorkdayCount]);
            item.WorkDaysPerWeek = (int)CleanDataValue(reader[ColumnNames.WorkDaysPerWeek]);
            item.HoursPerWeek = (Decimal)CleanDataValue(reader[ColumnNames.HoursPerWeek]);
            




            return item;
        }
        #endregion

    }
}
