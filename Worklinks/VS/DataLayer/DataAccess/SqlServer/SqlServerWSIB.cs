﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerWSIBSummary : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String WSIBEffectiveId = "code_wsib_effective_id";
            public static String WSIBCode = "code_wsib_cd";
            public static String WorkersCompensationAccountNumber = "workers_compensation_account_number";
            public static String ProvinceStateCode = "code_province_state_cd";
            public static String WorkersCompensationRate = "workers_compensation_rate";
        }
        #endregion

        #region main

        internal WSIBSummaryCollection SelectWSIBSummary(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, DateTime cutoffDate)
        {
            DataBaseCommand command = GetStoredProcCommand("WSIB_report",user.DatabaseName);
            command.AddParameterWithValue("@cutoffDate", cutoffDate);
            command.AddParameterWithValue("@wsibExcludeFromRemittanceExportFlag", false);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToWSIBSummaryCollection(reader);
            }
        }


        #endregion


        #region data mapping
        protected WSIBSummaryCollection MapToWSIBSummaryCollection(IDataReader reader)
        {
            WSIBSummaryCollection collection = new WSIBSummaryCollection();
            while (reader.Read())
            {
                collection.Add(MapToWSIBSummary(reader));
            }

            return collection;
        }
        protected WSIBSummary MapToWSIBSummary(IDataReader reader)
        {
            WSIBSummary item = new WSIBSummary();
            base.MapToBase(item, reader);

            item.WSIBEffectiveId = (long)CleanDataValue(reader[ColumnNames.WSIBEffectiveId]);
            item.WSIBCode = (String)CleanDataValue(reader[ColumnNames.WSIBCode]);
            item.WorkersCompensationAccountNumber = (String)CleanDataValue(reader[ColumnNames.WorkersCompensationAccountNumber]);
            item.ProvinceStateCode = (String)CleanDataValue(reader[ColumnNames.ProvinceStateCode]);
            item.WorkersCompensationRate = (Decimal?)CleanDataValue(reader[ColumnNames.WorkersCompensationRate]);

            return item;
        }
        #endregion
    }
}
