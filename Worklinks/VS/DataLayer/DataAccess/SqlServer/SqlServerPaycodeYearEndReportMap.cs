﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerPaycodeYearEndReportMap : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string YearEndReportMapId = "year_end_report_map_id";
            public static string EffectiveYear = "effective_year";
            public static string YearEndFormCode = "code_year_end_form_cd";
            public static string PaycodeCode = "code_paycode_cd";
            public static string YearEndFormBoxCode = "code_year_end_form_box_cd";
            public static string CountryCode = "code_country_cd";
            public static string ProvinceStateCode = "code_province_state_cd";
        }
        #endregion

        #region main
        public PaycodeYearEndReportMapCollection Select(DatabaseUser user, string paycodeCode)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEndReportMap_select", user.DatabaseName);

            command.AddParameterWithValue("@paycodeCode", paycodeCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToPaycodeYearEndReportMapCollection(reader);
        }
        public PaycodeYearEndReportMap Insert(DatabaseUser user, PaycodeYearEndReportMap item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("YearEndReportMap_insert", user.DatabaseName))
                {
                    command.BeginTransaction("YearEndReportMap_insert");

                    SqlParameter yearEndReportMapIdParm = command.AddParameterWithValue("@yearEndReportMapId", item.YearEndReportMapId, ParameterDirection.Output);
                    command.AddParameterWithValue("@effectiveYear", item.EffectiveYear);
                    command.AddParameterWithValue("@yearEndFormCode", item.YearEndFormCode);
                    command.AddParameterWithValue("@paycodeCode", item.PaycodeCode);
                    command.AddParameterWithValue("@yearEndFormBoxCode", item.YearEndFormBoxCode);
                    command.AddParameterWithValue("@countryCode", item.CountryCode);
                    command.AddParameterWithValue("@provinceStateCode", item.ProvinceStateCode);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.YearEndReportMapId = Convert.ToInt64(yearEndReportMapIdParm.Value);
                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();

                    return item;
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
                return null;
            }
        }
        public void Update(DatabaseUser user, PaycodeYearEndReportMap item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("YearEndReportMap_update", user.DatabaseName))
                {
                    command.BeginTransaction("YearEndReportMap_update");

                    command.AddParameterWithValue("@yearEndReportMapId", item.YearEndReportMapId);
                    command.AddParameterWithValue("@effectiveYear", item.EffectiveYear);
                    command.AddParameterWithValue("@yearEndFormCode", item.YearEndFormCode);
                    command.AddParameterWithValue("@paycodeCode", item.PaycodeCode);
                    command.AddParameterWithValue("@yearEndFormBoxCode", item.YearEndFormBoxCode);
                    command.AddParameterWithValue("@countryCode", item.CountryCode);
                    command.AddParameterWithValue("@provinceStateCode", item.ProvinceStateCode);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, PaycodeYearEndReportMap item, bool deleteByPaycode = false)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("YearEndReportMap_delete", user.DatabaseName))
                {
                    command.BeginTransaction("YearEndReportMap_delete");

                    command.AddParameterWithValue("@yearEndReportMapId", item.YearEndReportMapId);
                    command.AddParameterWithValue("@paycodeCode", item.PaycodeCode);
                    command.AddParameterWithValue("@deleteByPaycode", deleteByPaycode);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected PaycodeYearEndReportMapCollection MapToPaycodeYearEndReportMapCollection(IDataReader reader)
        {
            PaycodeYearEndReportMapCollection collection = new PaycodeYearEndReportMapCollection();

            while (reader.Read())
                collection.Add(MapToPaycodeYearEndReportMap(reader));

            return collection;
        }
        protected PaycodeYearEndReportMap MapToPaycodeYearEndReportMap(IDataReader reader)
        {
            PaycodeYearEndReportMap item = new PaycodeYearEndReportMap();
            MapToBase(item, reader);

            item.YearEndReportMapId = (long)CleanDataValue(reader[ColumnNames.YearEndReportMapId]);
            item.EffectiveYear = (decimal)CleanDataValue(reader[ColumnNames.EffectiveYear]);
            item.YearEndFormCode = (string)CleanDataValue(reader[ColumnNames.YearEndFormCode]);
            item.PaycodeCode = (string)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.YearEndFormBoxCode = (string)CleanDataValue(reader[ColumnNames.YearEndFormBoxCode]);
            item.CountryCode = (string)CleanDataValue(reader[ColumnNames.CountryCode]);
            item.ProvinceStateCode = (string)CleanDataValue(reader[ColumnNames.ProvinceStateCode]);

            return item;
        }
        #endregion
    }
}