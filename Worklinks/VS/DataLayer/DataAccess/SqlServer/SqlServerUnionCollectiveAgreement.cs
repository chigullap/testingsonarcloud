﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerUnionCollectiveAgreement : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String UnionCollectiveAgreementId = "union_collective_agreement_id";
            public static String EnglishDescription = "english_description";
            public static String FrenchDescription = "french_description";
            public static String LabourUnionId = "labour_union_id";
            public static String CollectiveAgreementStartDate = "collective_agreement_start_date";
            public static String CollectiveAgreementEnd_date = "collective_agreement_end_date";
            public static String AttachmentId = "attachment_id";
        }
        #endregion

        #region select
        internal UnionCollectiveAgreementCollection Select(DatabaseUser user, long? labourUnionId)
        {
            DataBaseCommand command = GetStoredProcCommand("UnionCollectiveAgreement_select", user.DatabaseName);

            command.AddParameterWithValue("@labourUnionId", labourUnionId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToUnionCollectiveAgreementCollection(reader);
        }
        #endregion

        #region insert
        public UnionCollectiveAgreement Insert(DatabaseUser user, UnionCollectiveAgreement collectiveAgreement)
        {
            using (DataBaseCommand command = GetStoredProcCommand("UnionCollectiveAgreement_insert", user.DatabaseName))
            {
                command.BeginTransaction("UnionCollectiveAgreement_insert");

                SqlParameter collectiveAgreementIdParam = command.AddParameterWithValue("@unionCollectiveAgreementId", collectiveAgreement.UnionCollectiveAgreementId, ParameterDirection.Output);
                command.AddParameterWithValue("@englishDescription", collectiveAgreement.EnglishDescription);
                command.AddParameterWithValue("@frenchDescription", collectiveAgreement.FrenchDescription);
                command.AddParameterWithValue("@labourUnionId", collectiveAgreement.LabourUnionId);
                command.AddParameterWithValue("@collectiveAgreementStartDate", collectiveAgreement.CollectiveAgreementStartDate);
                command.AddParameterWithValue("@collectiveAgreementEndDate", collectiveAgreement.CollectiveAgreementEndDate);
                command.AddParameterWithValue("@attachmentId", collectiveAgreement.AttachmentId);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", collectiveAgreement.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                collectiveAgreement.UnionCollectiveAgreementId = Convert.ToInt64(collectiveAgreementIdParam.Value);
                collectiveAgreement.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return collectiveAgreement;
            }
        }
        #endregion

        #region
        public void Update(DatabaseUser user, UnionCollectiveAgreement collectiveAgreement)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("UnionCollectiveAgreement_update", user.DatabaseName))
                {
                    command.BeginTransaction("UnionCollectiveAgreement_update");

                    command.AddParameterWithValue("@unionCollectiveAgreementId", collectiveAgreement.UnionCollectiveAgreementId);
                    command.AddParameterWithValue("@englishDescription", collectiveAgreement.EnglishDescription);
                    command.AddParameterWithValue("@frenchDescription", collectiveAgreement.FrenchDescription);
                    command.AddParameterWithValue("@labourUnionId", collectiveAgreement.LabourUnionId);
                    command.AddParameterWithValue("@collectiveAgreementStartDate", collectiveAgreement.CollectiveAgreementStartDate);
                    command.AddParameterWithValue("@collectiveAgreementEndDate", collectiveAgreement.CollectiveAgreementEndDate);
                    command.AddParameterWithValue("@attachmentId", collectiveAgreement.AttachmentId);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", collectiveAgreement.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    collectiveAgreement.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, UnionCollectiveAgreement collectiveAgreement)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("UnionCollectiveAgreement_delete", user.DatabaseName))
                {
                    command.BeginTransaction("UnionCollectiveAgreement_delete");

                    command.AddParameterWithValue("@unionCollectiveAgreementId", collectiveAgreement.UnionCollectiveAgreementId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected UnionCollectiveAgreementCollection MapToUnionCollectiveAgreementCollection(IDataReader reader)
        {
            UnionCollectiveAgreementCollection collection = new UnionCollectiveAgreementCollection();

            while (reader.Read())
                collection.Add(MapToUnionCollectiveAgreement(reader));

            return collection;
        }
        protected UnionCollectiveAgreement MapToUnionCollectiveAgreement(IDataReader reader)
        {
            UnionCollectiveAgreement item = new UnionCollectiveAgreement();
            base.MapToBase(item, reader);

            item.UnionCollectiveAgreementId = (long)CleanDataValue(reader[ColumnNames.UnionCollectiveAgreementId]);
            item.EnglishDescription = (String)CleanDataValue(reader[ColumnNames.EnglishDescription]);
            item.FrenchDescription = (String)CleanDataValue(reader[ColumnNames.FrenchDescription]);
            item.LabourUnionId = (long)CleanDataValue(reader[ColumnNames.LabourUnionId]);
            item.CollectiveAgreementStartDate = (DateTime?)CleanDataValue(reader[ColumnNames.CollectiveAgreementStartDate]);
            item.CollectiveAgreementEndDate = (DateTime?)CleanDataValue(reader[ColumnNames.CollectiveAgreementEnd_date]);
            item.AttachmentId = (long?)CleanDataValue(reader[ColumnNames.AttachmentId]);

            return item;
        }
        #endregion
    }
}