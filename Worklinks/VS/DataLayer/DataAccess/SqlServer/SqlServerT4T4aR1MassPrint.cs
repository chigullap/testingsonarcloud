﻿using System;
using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerT4T4aR1MassPrint : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String T4T4aR1MassPrintId = "id";
            public static String EmployeeNumber = "employee_number";
            public static String Year = "year";
            public static String EmployerNumber = "employer_number";
            public static String TaxableCodeProvinceStateCd = "taxable_code_province_state_cd";
            public static String EmployeeId = "employee_id";
        }
        #endregion

        #region main
        internal T4T4aR1MassPrintCollection SelectT4(DatabaseUser user, long year)
        {
            using (DataBaseCommand command = GetStoredProcCommand("T4MassPrint_report", user.DatabaseName))
            {
                command.AddParameterWithValue("@databaseName", null); //not used for this operation
                command.AddParameterWithValue("@year", year);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToT4T4aR1MassPrintCollection(reader);
            }
        }
        internal T4T4aR1MassPrintCollection SelectT4RSP(DatabaseUser user, long year)
        {
            using (DataBaseCommand command = GetStoredProcCommand("T4RSPMassPrint_report", user.DatabaseName))
            {
                command.AddParameterWithValue("@databaseName", null); //not used for this operation
                command.AddParameterWithValue("@year", year);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToT4T4aR1MassPrintCollection(reader);
            }
        }
        internal T4T4aR1MassPrintCollection SelectT4A(DatabaseUser user, long year)
        {
            using (DataBaseCommand command = GetStoredProcCommand("T4AMassPrint_report", user.DatabaseName))
            {
                command.AddParameterWithValue("@databaseName", null); //not used for this operation
                command.AddParameterWithValue("@year", year);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToT4T4aR1MassPrintCollection(reader);
            }
        }
        internal T4T4aR1MassPrintCollection SelectT4ARCA(DatabaseUser user, long year)
        {
            using (DataBaseCommand command = GetStoredProcCommand("T4ARCAMassPrint_report", user.DatabaseName))
            {
                command.AddParameterWithValue("@databaseName", null); //not used for this operation
                command.AddParameterWithValue("@year", year);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToT4T4aR1MassPrintCollection(reader);
            }
        }
        internal T4T4aR1MassPrintCollection SelectNR4(DatabaseUser user, long year)
        {
            using (DataBaseCommand command = GetStoredProcCommand("NR4MassPrint_report", user.DatabaseName))
            {
                command.AddParameterWithValue("@databaseName", null); //not used for this operation
                command.AddParameterWithValue("@year", year);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToT4T4aR1MassPrintCollection(reader);
            }
        }
        internal T4T4aR1MassPrintCollection SelectR1(DatabaseUser user, long year)
        {
            using (DataBaseCommand command = GetStoredProcCommand("R1MassPrint_report", user.DatabaseName))
            {
                command.AddParameterWithValue("@databaseName", null); //not used for this operation
                command.AddParameterWithValue("@year", year);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToT4T4aR1MassPrintCollection(reader);
            }
        }
        internal T4T4aR1MassPrintCollection SelectR2(DatabaseUser user, long year)
        {
            using (DataBaseCommand command = GetStoredProcCommand("R2MassPrint_report", user.DatabaseName))
            {
                command.AddParameterWithValue("@databaseName", null); //not used for this operation
                command.AddParameterWithValue("@year", year);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToT4T4aR1MassPrintCollection(reader);
            }
        }
        #endregion

        #region data mapping
        protected T4T4aR1MassPrintCollection MapToT4T4aR1MassPrintCollection(IDataReader reader)
        {
            T4T4aR1MassPrintCollection collection = new T4T4aR1MassPrintCollection();
            long i = 1; //used for key

            while (reader.Read())
                collection.Add(MapToT4T4aR1MassPrint(reader, i++));

            return collection;
        }
        protected T4T4aR1MassPrint MapToT4T4aR1MassPrint(IDataReader reader, long i)
        {
            T4T4aR1MassPrint item = new T4T4aR1MassPrint();

            item.T4T4aR1MassPrintId = i;
            item.EmployeeNumber = (String)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.Year = CleanDataValue(reader[ColumnNames.Year]).ToString();
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);

            if (ColumnExists(reader, ColumnNames.EmployerNumber))
                item.EmployerNumber = (String)CleanDataValue(reader[ColumnNames.EmployerNumber]);

            if (ColumnExists(reader, ColumnNames.TaxableCodeProvinceStateCd))
                item.TaxableCodeProvinceStateCd = (String)CleanDataValue(reader[ColumnNames.TaxableCodeProvinceStateCd]);

            return item;
        }
        #endregion
    }
}