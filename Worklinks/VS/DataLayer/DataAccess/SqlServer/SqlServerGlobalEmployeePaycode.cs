﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerGlobalEmployeePaycode : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String GlobalEmployeePaycodeId = "global_employee_paycode_id";
            public static String PaycodeCode = "code_paycode_cd";
            public static String PaycodeTypeCode = "code_paycode_type_cd";
            public static String AmountRate = "amount_rate";
            public static String AmountUnits = "amount_units";
            public static String AmountPercentage = "amount_percentage";
            public static String EmploymentInsuranceInsurableHoursPerUnit = "employment_insurance_insurable_hours_per_unit";
            public static String StartDate = "start_date";
            public static String CutoffDate = "cutoff_date";
            public static String PayPeriodMinimum = "pay_period_minimum";
            public static String PayPeriodMaximum = "pay_period_maximum";
            public static String MonthlyMaximum = "monthly_maximum";
            public static String YearlyMaximum = "yearly_maximum";
            public static String ExcludeAmount = "exclude_amount";
            public static String ExcludePercentage = "exclude_percentage";
            public static String GroupIncomeFactor = "group_income_factor";
            public static String RoundUpTo = "round_up_to";
            public static String RatePerRoundUpTo = "rate_per_round_up_to";
            public static String ExemptAmount = "exempt_amount";
            public static String ExemptAmountCodeNetGrossCode = "exempt_amount_code_net_gross_cd";
            public static String ExemptAmountCodeBeforeAfterCode = "exempt_amount_code_before_after_cd";
            public static String ExemptPercentage = "exempt_percentage";
            public static String ExemptPercentageCodeNetGrossCode = "exempt_percentage_code_net_gross_cd";
            public static String ExemptPercentageCodeBeforeAfterCode = "exempt_percentage_code_before_after_cd";
            public static String GarnishmentVendorId = "garnishment_vendor_id";
            public static String GarnishmentOrderNumber = "garnishment_order_number";
        }
        #endregion

        #region main
        internal EmployeePaycodeCollection Select(DatabaseUser user, long? globalEmployeePaycodeId)
        {
            DataBaseCommand command = GetStoredProcCommand("GlobalEmployeePaycode_select", user.DatabaseName);

            command.AddParameterWithValue("@globalEmployeePaycodeId", globalEmployeePaycodeId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToEmployeePaycodeCollection(reader);
        }
        public EmployeePaycode Insert(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("GlobalEmployeePaycode_insert", user.DatabaseName))
            {
                command.BeginTransaction("GlobalEmployeePaycode_insert");

                SqlParameter globalEmployeePaycodeIdParm = command.AddParameterWithValue("@globalEmployeePaycodeId", employeePaycode.EmployeePaycodeId, ParameterDirection.Output);
                command.AddParameterWithValue("@paycodeCode", employeePaycode.PaycodeCode);
                command.AddParameterWithValue("@amountRate", employeePaycode.AmountRate);
                command.AddParameterWithValue("@amountUnits", employeePaycode.AmountUnits);
                command.AddParameterWithValue("@amountPercentage", employeePaycode.AmountPercentage);
                command.AddParameterWithValue("@employmentInsuranceInsurableHoursPerUnit", employeePaycode.EmploymentInsuranceInsurableHoursPerUnit);
                command.AddParameterWithValue("@startDate", employeePaycode.StartDate);
                command.AddParameterWithValue("@cutoffDate", employeePaycode.CutoffDate);
                command.AddParameterWithValue("@payPeriodMinimum", employeePaycode.PayPeriodMinimum);
                command.AddParameterWithValue("@payPeriodMaximum", employeePaycode.PayPeriodMaximum);
                command.AddParameterWithValue("@monthlyMaximum", employeePaycode.MonthlyMaximum);
                command.AddParameterWithValue("@yearlyMaximum", employeePaycode.YearlyMaximum);
                command.AddParameterWithValue("@excludeAmount", employeePaycode.ExcludeAmount);
                command.AddParameterWithValue("@excludePercentage", employeePaycode.ExcludePercentage);
                command.AddParameterWithValue("@groupIncomeFactor", employeePaycode.GroupIncomeFactor);
                command.AddParameterWithValue("@roundUpTo", employeePaycode.RoundUpTo);
                command.AddParameterWithValue("@ratePerRoundUpTo", employeePaycode.RatePerRoundUpTo);
                command.AddParameterWithValue("@exemptAmount", employeePaycode.ExemptAmount);
                command.AddParameterWithValue("@exemptAmountCodeNetGrossCode", employeePaycode.ExemptAmountCodeNetGrossCd);
                command.AddParameterWithValue("@exemptAmountCodeBeforeAfterCode", employeePaycode.ExemptAmountCodeBeforeAfterCd);
                command.AddParameterWithValue("@exemptPercentage", employeePaycode.ExcludePercentage);
                command.AddParameterWithValue("@exemptPercentageCodeNetGrossCode", employeePaycode.ExemptPercentageCodeNetGrossCd);
                command.AddParameterWithValue("@exemptPercentageCodeBeforeAfterCode", employeePaycode.ExemptPercentageCodeBeforeAfterCd);
                command.AddParameterWithValue("@garnishmentVendorId", employeePaycode.GarnishmentVendorId);
                command.AddParameterWithValue("@garnishmentOrderNumber", employeePaycode.GarnishmentOrderNumber);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", employeePaycode.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                employeePaycode.EmployeePaycodeId = Convert.ToInt64(globalEmployeePaycodeIdParm.Value);
                employeePaycode.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return employeePaycode;
            }
        }
        public void Update(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("GlobalEmployeePaycode_update", user.DatabaseName))
                {
                    command.BeginTransaction("GlobalEmployeePaycode_update");

                    command.AddParameterWithValue("@globalEmployeePaycodeId", employeePaycode.EmployeePaycodeId);
                    command.AddParameterWithValue("@paycodeCode", employeePaycode.PaycodeCode);
                    command.AddParameterWithValue("@amountRate", employeePaycode.AmountRate);
                    command.AddParameterWithValue("@amountUnits", employeePaycode.AmountUnits);
                    command.AddParameterWithValue("@amountPercentage", employeePaycode.AmountPercentage);
                    command.AddParameterWithValue("@employmentInsuranceInsurableHoursPerUnit", employeePaycode.EmploymentInsuranceInsurableHoursPerUnit);
                    command.AddParameterWithValue("@startDate", employeePaycode.StartDate);
                    command.AddParameterWithValue("@cutoffDate", employeePaycode.CutoffDate);
                    command.AddParameterWithValue("@payPeriodMinimum", employeePaycode.PayPeriodMinimum);
                    command.AddParameterWithValue("@payPeriodMaximum", employeePaycode.PayPeriodMaximum);
                    command.AddParameterWithValue("@monthlyMaximum", employeePaycode.MonthlyMaximum);
                    command.AddParameterWithValue("@yearlyMaximum", employeePaycode.YearlyMaximum);
                    command.AddParameterWithValue("@excludeAmount", employeePaycode.ExcludeAmount);
                    command.AddParameterWithValue("@excludePercentage", employeePaycode.ExcludePercentage);
                    command.AddParameterWithValue("@groupIncomeFactor", employeePaycode.GroupIncomeFactor);
                    command.AddParameterWithValue("@roundUpTo", employeePaycode.RoundUpTo);
                    command.AddParameterWithValue("@ratePerRoundUpTo", employeePaycode.RatePerRoundUpTo);
                    command.AddParameterWithValue("@exemptAmount", employeePaycode.ExemptAmount);
                    command.AddParameterWithValue("@exemptAmountCodeNetGrossCode", employeePaycode.ExemptAmountCodeNetGrossCd);
                    command.AddParameterWithValue("@exemptAmountCodeBeforeAfterCode", employeePaycode.ExemptAmountCodeBeforeAfterCd);
                    command.AddParameterWithValue("@exemptPercentage", employeePaycode.ExcludePercentage);
                    command.AddParameterWithValue("@exemptPercentageCodeNetGrossCode", employeePaycode.ExemptPercentageCodeNetGrossCd);
                    command.AddParameterWithValue("@exemptPercentageCodeBeforeAfterCode", employeePaycode.ExemptPercentageCodeBeforeAfterCd);
                    command.AddParameterWithValue("@garnishmentVendorId", employeePaycode.GarnishmentVendorId);
                    command.AddParameterWithValue("@garnishmentOrderNumber", employeePaycode.GarnishmentOrderNumber);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", employeePaycode.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    employeePaycode.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("GlobalEmployeePaycode_delete", user.DatabaseName))
                {
                    command.BeginTransaction("GlobalEmployeePaycode_delete");

                    command.AddParameterWithValue("@globalEmployeePaycodeId", employeePaycode.EmployeePaycodeId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeePaycodeCollection MapToEmployeePaycodeCollection(IDataReader reader)
        {
            EmployeePaycodeCollection collection = new EmployeePaycodeCollection();

            while (reader.Read())
                collection.Add(MapToEmployeePaycode(reader));

            return collection;
        }
        protected EmployeePaycode MapToEmployeePaycode(IDataReader reader)
        {
            EmployeePaycode item = new EmployeePaycode();
            base.MapToBase(item, reader);

            item.EmployeePaycodeId = (long)CleanDataValue(reader[ColumnNames.GlobalEmployeePaycodeId]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.PaycodeTypeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeTypeCode]);
            item.AmountRate = (Decimal?)CleanDataValue(reader[ColumnNames.AmountRate]);
            item.AmountUnits = (Decimal?)CleanDataValue(reader[ColumnNames.AmountUnits]);
            item.AmountPercentage = (Decimal?)CleanDataValue(reader[ColumnNames.AmountPercentage]);
            item.EmploymentInsuranceInsurableHoursPerUnit = (Decimal?)CleanDataValue(reader[ColumnNames.EmploymentInsuranceInsurableHoursPerUnit]);
            item.StartDate = (DateTime?)CleanDataValue(reader[ColumnNames.StartDate]);
            item.CutoffDate = (DateTime?)CleanDataValue(reader[ColumnNames.CutoffDate]);
            item.PayPeriodMinimum = (Decimal?)CleanDataValue(reader[ColumnNames.PayPeriodMinimum]);
            item.PayPeriodMaximum = (Decimal?)CleanDataValue(reader[ColumnNames.PayPeriodMaximum]);
            item.MonthlyMaximum = (Decimal?)CleanDataValue(reader[ColumnNames.MonthlyMaximum]);
            item.YearlyMaximum = (Decimal?)CleanDataValue(reader[ColumnNames.YearlyMaximum]);
            item.ExcludeAmount = (Decimal?)CleanDataValue(reader[ColumnNames.ExcludeAmount]);
            item.ExcludePercentage = (Decimal?)CleanDataValue(reader[ColumnNames.ExcludePercentage]);
            item.GroupIncomeFactor = (Decimal?)CleanDataValue(reader[ColumnNames.GroupIncomeFactor]);
            item.RoundUpTo = (Decimal?)CleanDataValue(reader[ColumnNames.RoundUpTo]);
            item.RatePerRoundUpTo = (Decimal?)CleanDataValue(reader[ColumnNames.RatePerRoundUpTo]);
            item.ExemptAmount = (Decimal?)CleanDataValue(reader[ColumnNames.ExemptAmount]);
            item.ExemptAmountCodeNetGrossCd = (String)CleanDataValue(reader[ColumnNames.ExemptAmountCodeNetGrossCode]);
            item.ExemptAmountCodeBeforeAfterCd = (String)CleanDataValue(reader[ColumnNames.ExemptAmountCodeBeforeAfterCode]);
            item.ExemptPercentage = (Decimal?)CleanDataValue(reader[ColumnNames.ExemptPercentage]);
            item.ExemptPercentageCodeNetGrossCd = (String)CleanDataValue(reader[ColumnNames.ExemptPercentageCodeNetGrossCode]);
            item.ExemptPercentageCodeBeforeAfterCd = (String)CleanDataValue(reader[ColumnNames.ExemptPercentageCodeBeforeAfterCode]);
            item.GarnishmentVendorId = (long?)CleanDataValue(reader[ColumnNames.GarnishmentVendorId]);
            item.GarnishmentOrderNumber = (String)CleanDataValue(reader[ColumnNames.GarnishmentOrderNumber]);

            return item;
        }
        #endregion
    }
}