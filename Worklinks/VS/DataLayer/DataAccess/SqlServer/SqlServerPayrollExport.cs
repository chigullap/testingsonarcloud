﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerPayrollExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string PayrollExportId = "payroll_export_id";
            public static string PayrollProcessId = "payroll_process_id";
            public static string SequenceNumber = "sequence_number";
            public static string Data = "data";
            public static string EftType = "code_eft_type_cd";
            public static string FileSentFlag = "file_sent_flag";
            public static string ProcessGroupDescription = "process_group_description";
        }
        #endregion

        #region main
        public PayrollExportCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, string eftType)
        {
            DataBaseCommand command = GetStoredProcCommand("PayrollExport_select", user.DatabaseName);
            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
            command.AddParameterWithValue("@codeEftType", eftType);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToPayrollExportCollection(reader);
            }
        }

        public PayrollExportCollection GetDirectDepositCraGarnishmentExportData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, DateTime date, bool usingRbcEdiForDirectDepositsFlag, bool usingCraGarnishmentFlag, bool usingScotiaBankEdiForDirectDepositsFlag)
        {
            DataBaseCommand command = GetStoredProcCommand("PayrollExportRemittance_select", user.DatabaseName);
            command.AddParameterWithValue("@date", date);
            command.AddParameterWithValue("@usingRbcEdiForDirectDepositsFlag", usingRbcEdiForDirectDepositsFlag);
            command.AddParameterWithValue("@usingCraGarnishmentFlag", usingCraGarnishmentFlag);
            command.AddParameterWithValue("@usingScotiaBankEdiForDirectDepositsFlag", usingScotiaBankEdiForDirectDepositsFlag);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapRemittanceToPayrollExportCollection(reader);
            }
        }

        public void UpdateFileSentFlag(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, PayrollExport item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollExport_updateFileSentFlag", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollExport_updateFileSentFlag");

                    command.AddParameterWithValue("@payrollExportId", item.PayrollExportId);
                    command.AddParameterWithValue("@fileSentFlag", item.FileSentFlag);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, PayrollExport item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollExport_insert", user.DatabaseName))
            {
                command.BeginTransaction("PayrollExport_insert");

                SqlParameter payrollExportIdParm = command.AddParameterWithValue("@payrollExportId", item.PayrollExportId, ParameterDirection.Output);
                command.AddParameterWithValue("@payrollProcessId", item.PayrollProcessId);
                command.AddParameterWithValue("@sequenceNumber", item.SequenceNumber);
                command.AddParameterWithValue("@data", item.Data);
                command.AddParameterWithValue("@codeEftType", item.EftType);
                command.AddParameterWithValue("@fileSentFlag", item.FileSentFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.PayrollExportId = Convert.ToInt64(payrollExportIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }

        #endregion

        #region data mapping
        protected PayrollExportCollection MapToPayrollExportCollection(IDataReader reader)
        {
            PayrollExportCollection collection = new PayrollExportCollection();
            while (reader.Read())
            {
                collection.Add(MapToPayrollExport(reader));
            }

            return collection;
        }
        protected PayrollExport MapToPayrollExport(IDataReader reader)
        {
            PayrollExport item = new PayrollExport();
            base.MapToBase(item, reader);

            item.PayrollExportId = (long)CleanDataValue(reader[ColumnNames.PayrollExportId]);
            item.PayrollProcessId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessId]);
            item.SequenceNumber = (long?)CleanDataValue(reader[ColumnNames.SequenceNumber]);
            item.Data = (byte[])CleanDataValue(reader[ColumnNames.Data]);
            item.EftType = (string)CleanDataValue(reader[ColumnNames.EftType]);
            item.FileSentFlag = (bool)CleanDataValue(reader[ColumnNames.FileSentFlag]);

            return item;
        }
        protected PayrollExportCollection MapRemittanceToPayrollExportCollection(IDataReader reader)
        {
            PayrollExportCollection collection = new PayrollExportCollection();
            while (reader.Read())
            {
                collection.Add(MapRemittanceToPayrollExport(reader));
            }

            return collection;
        }
        protected PayrollExport MapRemittanceToPayrollExport(IDataReader reader)
        {
            PayrollExport item = new PayrollExport();
            base.MapToBase(item, reader);

            item.PayrollExportId = (long)CleanDataValue(reader[ColumnNames.PayrollExportId]);
            item.PayrollProcessId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessId]);
            item.SequenceNumber = (long?)CleanDataValue(reader[ColumnNames.SequenceNumber]);
            item.Data = (byte[])CleanDataValue(reader[ColumnNames.Data]);
            item.EftType = (string)CleanDataValue(reader[ColumnNames.EftType]);
            item.FileSentFlag = (bool)CleanDataValue(reader[ColumnNames.FileSentFlag]);
            item.ProcessGroupDescription = (string)CleanDataValue(reader[ColumnNames.ProcessGroupDescription]);            

            return item;
        }

        #endregion
    }
}
