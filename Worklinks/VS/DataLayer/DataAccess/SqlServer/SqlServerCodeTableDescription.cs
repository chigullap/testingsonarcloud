﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerCodeTableDescription : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string Id = "id";
            public static String CodeTableDescCd = "cd";
            public static String LanguageCode = "code_language_cd";
            public static String TableDescription = "description";
        }
        #endregion

        #region main
        /// <summary>
        /// returns code table description rows
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        internal CodeTableDescriptionCollection Select(DatabaseUser user, string tableName, string code)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CodeTableDescription_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@codeName", tableName);
                command.AddParameterWithValue("@code", code);

                CodeTableDescriptionCollection codeTableDescRows = null;

                using (IDataReader reader = command.ExecuteReader())
                    codeTableDescRows = MapToCodeTableCollection(reader);

                return codeTableDescRows;
            }
        }
        public void Update(DatabaseUser user, CodeTableDescription codeTableDesc, string codeTableName)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CodeTableDescription_update", user.DatabaseName))
                {
                    command.BeginTransaction("CodeTableDescription_update");

                    command.AddParameterWithValue("@codeTableName", codeTableName);
                    command.AddParameterWithValue("@codeDescriptionId", codeTableDesc.Id);
                    command.AddParameterWithValue("@code", codeTableDesc.CodeTableDescCd);
                    command.AddParameterWithValue("@languageCode", codeTableDesc.LanguageCode);
                    command.AddParameterWithValue("@description", codeTableDesc.TableDescription);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", codeTableDesc.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    codeTableDesc.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public CodeTableDescription Insert(DatabaseUser user, CodeTableDescription codeTableDesc, string codeTableName)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CodeTableDescription_insert", user.DatabaseName))
            {
                command.BeginTransaction("CodeTableDescription_insert");

                command.AddParameterWithValue("@codeTableName", codeTableName);
                SqlParameter codeDescriptionIdParm = command.AddParameterWithValue("@codeDescriptionId", codeTableDesc.Id, ParameterDirection.Output);
                command.AddParameterWithValue("@code", codeTableDesc.CodeTableDescCd);
                command.AddParameterWithValue("@languageCode", codeTableDesc.LanguageCode);
                command.AddParameterWithValue("@description", codeTableDesc.TableDescription);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", codeTableDesc.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                codeTableDesc.Id = Convert.ToInt64(codeDescriptionIdParm.Value);
                codeTableDesc.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return codeTableDesc;
            }
        }
        public void Delete(DatabaseUser user, CodeTableDescription codeTableDesc, string codeTableName)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CodeTableDescription_delete", user.DatabaseName))
                {
                    command.BeginTransaction("CodeTableDescription_delete");

                    command.AddParameterWithValue("@codeTableName", codeTableName);
                    command.AddParameterWithValue("@codeDescriptionId", codeTableDesc.Id);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        /// <summary>
        /// maps result set to code table description collection
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected CodeTableDescriptionCollection MapToCodeTableCollection(IDataReader reader)
        {
            CodeTableDescriptionCollection collection = new CodeTableDescriptionCollection();

            while (reader.Read())
                collection.Add(MapToCodeTableDescRows(reader));

            return collection;
        }
        /// <summary>
        /// maps columns to code table properties
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected CodeTableDescription MapToCodeTableDescRows(IDataReader reader)
        {
            CodeTableDescription codeTable = new CodeTableDescription();
            base.MapToBase(codeTable, reader);

            codeTable.Id = Convert.ToInt64(CleanDataValue(reader[ColumnNames.Id]));
            codeTable.CodeTableDescCd = (string)CleanDataValue(reader[ColumnNames.CodeTableDescCd]);
            codeTable.LanguageCode = (string)CleanDataValue(reader[ColumnNames.LanguageCode]);
            codeTable.TableDescription = (string)CleanDataValue(reader[ColumnNames.TableDescription]);

            return codeTable;
        }
        #endregion
    }
}