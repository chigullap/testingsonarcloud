﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerPayrollProcessGroupOverridePeriod : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayrollProcessGroupOverridePeriodId = "payroll_process_group_override_period_id";
            public static String Year = "year";
            public static String PayrollProcessGroupCode = "code_payroll_process_group_cd";
            public static String Period = "period";
        }
        #endregion

        #region select
        internal PayrollProcessGroupOverridePeriodCollection Select(DatabaseUser user, String payrollProcessGroupCode)
        {
            DataBaseCommand command = GetStoredProcCommand("PayrollProcessGroupOverridePeriod_select", user.DatabaseName);

            command.AddParameterWithValue("@payrollProcessGroupCode", payrollProcessGroupCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToPayrollProcessGroupOverridePeriodCollection(reader);
        }
        #endregion

        #region insert
        public PayrollProcessGroupOverridePeriod Insert(DatabaseUser user, PayrollProcessGroupOverridePeriod overridePeriod)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PayrollProcessGroupOverridePeriod_insert", user.DatabaseName))
            {
                command.BeginTransaction("PayrollProcessGroupOverridePeriod_insert");

                SqlParameter overridePeriodIdParm = command.AddParameterWithValue("@payrollProcessGroupOverridePeriodId", overridePeriod.PayrollProcessGroupOverridePeriodId, ParameterDirection.Output);
                command.AddParameterWithValue("@year", overridePeriod.Year);
                command.AddParameterWithValue("@payrollProcessGroupCode", overridePeriod.PayrollProcessGroupCode);
                command.AddParameterWithValue("@period", overridePeriod.Period);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", overridePeriod.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                overridePeriod.PayrollProcessGroupOverridePeriodId = Convert.ToInt64(overridePeriodIdParm.Value);
                overridePeriod.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return overridePeriod;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, PayrollProcessGroupOverridePeriod overridePeriod)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollProcessGroupOverridePeriod_update", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollProcessGroupOverridePeriod_update");

                    command.AddParameterWithValue("@payrollProcessGroupOverridePeriodId", overridePeriod.PayrollProcessGroupOverridePeriodId);
                    command.AddParameterWithValue("@year", overridePeriod.Year);
                    command.AddParameterWithValue("@payrollProcessGroupCode", overridePeriod.PayrollProcessGroupCode);
                    command.AddParameterWithValue("@period", overridePeriod.Period);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", overridePeriod.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    overridePeriod.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, PayrollProcessGroupOverridePeriod overridePeriod)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollProcessGroupOverridePeriod_delete", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollProcessGroupOverridePeriod_delete");

                    command.AddParameterWithValue("@payrollProcessGroupOverridePeriodId", overridePeriod.PayrollProcessGroupOverridePeriodId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected PayrollProcessGroupOverridePeriodCollection MapToPayrollProcessGroupOverridePeriodCollection(IDataReader reader)
        {
            PayrollProcessGroupOverridePeriodCollection collection = new PayrollProcessGroupOverridePeriodCollection();

            while (reader.Read())
                collection.Add(MapToPayrollProcessGroupOverridePeriod(reader));

            return collection;
        }
        protected PayrollProcessGroupOverridePeriod MapToPayrollProcessGroupOverridePeriod(IDataReader reader)
        {
            PayrollProcessGroupOverridePeriod item = new PayrollProcessGroupOverridePeriod();
            base.MapToBase(item, reader);

            item.PayrollProcessGroupOverridePeriodId = (long)CleanDataValue(reader[ColumnNames.PayrollProcessGroupOverridePeriodId]);
            item.Year = (int)CleanDataValue(reader[ColumnNames.Year]);
            item.PayrollProcessGroupCode = (String)CleanDataValue(reader[ColumnNames.PayrollProcessGroupCode]);
            item.Period = (int)CleanDataValue(reader[ColumnNames.Period]);

            return item;
        }
        #endregion
    }
}