﻿using System.Data;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using System.Data.SqlClient;
using System;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerPayrollMasterDeduction : SqlServerBase
    {
        public void DeleteByProcessId(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollMasterDeduction_delete", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollMasterDeduction_delete");
                    command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void GenerateData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollMasterDeduction_generate", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollMasterDeduction_generateAmount");
                    command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDateTime", DateTime.Now);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
    }
}
 