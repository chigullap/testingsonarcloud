﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerRoe : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String RoeId = "roe_id";
            public static String Data = "data";
        }
        #endregion

        #region main
        internal RoeCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long roeId)
        {
            DataBaseCommand command = GetStoredProcCommand("Roe_select", user.DatabaseName);
            command.AddParameterWithValue("@roeId", roeId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToRoeCollection(reader);
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Roe item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("Roe_update", user.DatabaseName))
                {
                    command.BeginTransaction("Roe_update");

                    command.AddParameterWithValue("@roeId", item.RoeId);
                    command.AddParameterWithValue("@data", item.Data);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    item.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Roe item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Roe_insert", user.DatabaseName))
            {
                command.BeginTransaction("Roe_insert");

                SqlParameter employeeSkillIdParm = command.AddParameterWithValue("@roeId", item.RoeId, ParameterDirection.Output);
                command.AddParameterWithValue("@data", item.Data);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                item.RoeId = Convert.ToInt64(employeeSkillIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Roe item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("Roe_delete", user.DatabaseName))
                {
                    command.BeginTransaction("Roe_delete");
                    command.AddParameterWithValue("@roeId", item.RoeId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected RoeCollection MapToRoeCollection(IDataReader reader)
        {
            RoeCollection collection = new RoeCollection();
            while (reader.Read())
            {
                collection.Add(MapToRoe(reader));
            }

            return collection;
        }
        protected Roe MapToRoe(IDataReader reader)
        {
            Roe item = new Roe();
            base.MapToBase(item, reader);

            item.RoeId = (long)CleanDataValue(reader[ColumnNames.RoeId]);
            item.Data = (byte[])CleanDataValue(reader[ColumnNames.Data]);

            return item;
        }

        #endregion
    }
}