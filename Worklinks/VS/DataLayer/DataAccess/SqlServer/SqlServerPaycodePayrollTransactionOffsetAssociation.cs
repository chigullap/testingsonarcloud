﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerPaycodePayrollTransactionOffsetAssociation : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayrollTransactionOffsetAssociationId = "payroll_transaction_offset_association_id";
            public static String PaycodeCode = "code_paycode_cd";
            public static String OffsetCodePaycodeCode = "offset_code_paycode_cd";
            public static String OffsetMultiplier = "offset_multiplier";
            public static String UseBaseSalaryRateFlag = "use_base_salary_rate_flag";
            public static String PaycodeTypeCode = "code_paycode_type_cd";
            public static String PaycodeIncomePayTypeCode = "code_paycode_income_pay_type_cd";

        }
        #endregion

        #region main
        public PaycodePayrollTransactionOffsetAssociationCollection Select(DatabaseUser user, String paycodeCode)
        {
            DataBaseCommand command = GetStoredProcCommand("PayrollTransactionOffsetAssociation_select", user.DatabaseName);

            command.AddParameterWithValue("@paycodeCode", paycodeCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToPaycodePayrollTransactionOffsetAssociationCollection(reader);
        }
        public PaycodePayrollTransactionOffsetAssociation Insert(DatabaseUser user, PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollTransactionOffsetAssociation_insert", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollTransactionOffsetAssociation_insert");

                    SqlParameter payrollTransactionOffsetAssociationIdParm = command.AddParameterWithValue("@payrollTransactionOffsetAssociationId", paycodePayrollTransactionOffsetAssociation.PayrollTransactionOffsetAssociationId, ParameterDirection.Output);
                    command.AddParameterWithValue("@paycodeCode", paycodePayrollTransactionOffsetAssociation.PaycodeCode);
                    command.AddParameterWithValue("@offsetCodePaycodeCode", paycodePayrollTransactionOffsetAssociation.OffsetCodePaycodeCode);
                    command.AddParameterWithValue("@offsetMultiplier", paycodePayrollTransactionOffsetAssociation.OffsetMultiplier);
                    command.AddParameterWithValue("@useBaseSalaryRateFlag", paycodePayrollTransactionOffsetAssociation.UseBaseSalaryRateFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", paycodePayrollTransactionOffsetAssociation.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    paycodePayrollTransactionOffsetAssociation.PayrollTransactionOffsetAssociationId = Convert.ToInt64(payrollTransactionOffsetAssociationIdParm.Value);
                    paycodePayrollTransactionOffsetAssociation.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();

                    return paycodePayrollTransactionOffsetAssociation;
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
                return null;
            }
        }
        public void Update(DatabaseUser user, PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollTransactionOffsetAssociation_update", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollTransactionOffsetAssociation_update");

                    command.AddParameterWithValue("@payrollTransactionOffsetAssociationId", paycodePayrollTransactionOffsetAssociation.PayrollTransactionOffsetAssociationId);
                    command.AddParameterWithValue("@paycodeCode", paycodePayrollTransactionOffsetAssociation.PaycodeCode);
                    command.AddParameterWithValue("@offsetCodePaycodeCode", paycodePayrollTransactionOffsetAssociation.OffsetCodePaycodeCode);
                    command.AddParameterWithValue("@offsetMultiplier", paycodePayrollTransactionOffsetAssociation.OffsetMultiplier);
                    command.AddParameterWithValue("@useBaseSalaryRateFlag", paycodePayrollTransactionOffsetAssociation.UseBaseSalaryRateFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", paycodePayrollTransactionOffsetAssociation.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    paycodePayrollTransactionOffsetAssociation.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation, bool deleteByPaycode = false)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PayrollTransactionOffsetAssociation_delete", user.DatabaseName))
                {
                    command.BeginTransaction("PayrollTransactionOffsetAssociation_delete");

                    command.AddParameterWithValue("@payrollTransactionOffsetAssociationId", paycodePayrollTransactionOffsetAssociation.PayrollTransactionOffsetAssociationId);
                    command.AddParameterWithValue("@paycodeCode", paycodePayrollTransactionOffsetAssociation.PaycodeCode);
                    command.AddParameterWithValue("@deleteByPaycode", deleteByPaycode);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected PaycodePayrollTransactionOffsetAssociationCollection MapToPaycodePayrollTransactionOffsetAssociationCollection(IDataReader reader)
        {
            PaycodePayrollTransactionOffsetAssociationCollection collection = new PaycodePayrollTransactionOffsetAssociationCollection();

            while (reader.Read())
                collection.Add(MapToPaycodePayrollTransactionOffsetAssociation(reader));

            return collection;
        }
        protected PaycodePayrollTransactionOffsetAssociation MapToPaycodePayrollTransactionOffsetAssociation(IDataReader reader)
        {
            PaycodePayrollTransactionOffsetAssociation item = new PaycodePayrollTransactionOffsetAssociation();
            base.MapToBase(item, reader);

            item.PayrollTransactionOffsetAssociationId = (long)CleanDataValue(reader[ColumnNames.PayrollTransactionOffsetAssociationId]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.OffsetCodePaycodeCode = (String)CleanDataValue(reader[ColumnNames.OffsetCodePaycodeCode]);
            item.OffsetMultiplier = (Decimal)CleanDataValue(reader[ColumnNames.OffsetMultiplier]);
            item.UseBaseSalaryRateFlag = (bool)CleanDataValue(reader[ColumnNames.UseBaseSalaryRateFlag]);
            item.PaycodeTypeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeTypeCode]);
            item.PaycodeIncomePayTypeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeIncomePayTypeCode]);

            return item;
        }
        #endregion
    }
}