﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerErrorLog : SqlServerBase
    {
        #region main

        public void InsertErrorLog(DatabaseUser user, String methodName, Exception exc)
        {
            using (DataBaseCommand command = GetStoredProcCommand("ErrorLog_insert",user.DatabaseName))
            {
                command.BeginTransaction("ErrorLog_insert");

                command.AddParameterWithValue("@methodName", methodName);
                command.AddParameterWithValue("@errorMessage", exc.Message);
                command.AddParameterWithValue("@stackTrace", exc.StackTrace);
                command.AddParameterWithValue("@errorNumber",exc.HResult);
                command.AddParameterWithValue("@createUser", user.UserName);
                command.AddParameterWithValue("@createDateTime", Time);

                command.ExecuteNonQuery();
               
                command.CommitTransaction();
            }
        }
       
        #endregion
    }
}

