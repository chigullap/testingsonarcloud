﻿//using System;
//using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerPensionBasicContribution : SqlServerBase
    {
        //        #region column names
        //        protected new class ColumnNames : SqlServerBase.ColumnNames
        //        {
        //            public static String PensionBasicContributionId = "pension_basic_contribution_id";
        //            public static String EffectiveYear = "effective_year";
        //            public static String BasicAmountPercentage = "basic_amount_percentage";

        //            public static String TotalContribution = "ppaycodeytddollars";
        //            public static String AmountPercentage = "amount_percentage";
        //        }
        //        #endregion

        //        #region main

        //        internal PensionBasicContributionCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? pensionBasicContributionId)
        //        {
        //            using (DataBaseCommand command = GetStoredProcCommand("PensionBasicContribution_select", user.DatabaseName))
        //            {
        //                command.AddParameterWithValue("@pensionBasicContributionId", pensionBasicContributionId);

        //                using (IDataReader reader = command.ExecuteReader())
        //                {
        //                    return MapToPensionBasicContributionCollection(reader);
        //                }
        //            }
        //        }

        //        internal Decimal SelectPensionContributionTotal(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long empId, Decimal year, Boolean optionalContributionFlag)
        //        {
        //            using (DataBaseCommand command = GetStoredProcCommand("ContributionTotalForCurrentYear_select", user.DatabaseName))
        //            {
        //                command.AddParameterWithValue("@employeeId", empId);
        //                command.AddParameterWithValue("@year", year);
        //                command.AddParameterWithValue("@optionalContributionFlag", optionalContributionFlag);

        //                using (IDataReader reader = command.ExecuteReader())
        //                {
        //                    return MapToPensionContributionTotal(reader);
        //                }
        //            }
        //        }

        //        internal Decimal SelectPensionContributionPercentage(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long empId)
        //        {
        //            using (DataBaseCommand command = GetStoredProcCommand("ContributionPercentageForOptionalContribution_select", user.DatabaseName))
        //            {
        //                command.AddParameterWithValue("@employeeId", empId);

        //                using (IDataReader reader = command.ExecuteReader())
        //                {
        //                    return MapToPensionContributionPercentage(reader);
        //                }
        //            }
        //        }

        public void EmployeePaycodeSetMaximumPensionAmount(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePaycode_bmsSetMaximumPensionAmount", user.DatabaseName))
            {
                command.BeginTransaction("EmployeePaycode_bmsSetMaximumPensionAmount");
                command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        //        public void SetOptionalPaycodeContributionData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long empId, Decimal optionalPensionRate, Decimal optionalContributionTotal, Decimal optionalYearlyMaximum)
        //        {
        //            using (DataBaseCommand command = GetStoredProcCommand("EmployeePaycodePensionOptionalPaycode_insert_update", user.DatabaseName))
        //            {
        //                command.BeginTransaction("EmployeePaycodePensionOptionalPaycode_insert_update");
        //                command.AddParameterWithValue("@employeeId", empId);
        //                command.AddParameterWithValue("@amountPercentage", optionalPensionRate);
        //                command.AddParameterWithValue("@optionalContributionToDate", optionalContributionTotal);
        //                command.AddParameterWithValue("@yearlyMaximum", optionalYearlyMaximum);

        //                command.ExecuteNonQuery();
        //                command.CommitTransaction();
        //            }
        //        }

        //        #endregion


        //        #region data mapping
        //        protected PensionBasicContributionCollection MapToPensionBasicContributionCollection(IDataReader reader)
        //        {
        //            PensionBasicContributionCollection collection = new PensionBasicContributionCollection();
        //            while (reader.Read())
        //            {
        //                collection.Add(MapToPensionBasicContribution(reader));
        //            }

        //            return collection;
        //        }
        //        protected PensionBasicContribution MapToPensionBasicContribution(IDataReader reader)
        //        {
        //            PensionBasicContribution pensionContribution = new PensionBasicContribution();
        //            base.MapToBase(pensionContribution, reader);

        //            pensionContribution.PensionBasicContributionId = (long)CleanDataValue(reader[ColumnNames.PensionBasicContributionId]);
        //            pensionContribution.EffectiveYear = (Decimal)CleanDataValue(reader[ColumnNames.EffectiveYear]);
        //            pensionContribution.BasicAmountPercentage = (Decimal)CleanDataValue(reader[ColumnNames.BasicAmountPercentage]);

        //            return pensionContribution;
        //        }

        //        protected Decimal MapToPensionContributionTotal(IDataReader reader)
        //        {
        //            Decimal total = 0;
        //            while (reader.Read())
        //            {
        //                total = (Decimal)CleanDataValue(reader[ColumnNames.TotalContribution]);
        //            }

        //            return total;
        //        }
        //        protected Decimal MapToPensionContributionPercentage(IDataReader reader)
        //        {
        //            Decimal total = 0;
        //            while (reader.Read())
        //            {
        //                total = (Decimal)CleanDataValue(reader[ColumnNames.AmountPercentage]);
        //            }

        //            return total;
        //        }

        //        #endregion
    }
}
