﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerYearEndAdjustment : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ColumnName = "column_name";
            public static String TableName = "table_name";
            public static String Id = "id";
            public static String Description = "description";
            public static String ColumnValue = "column_value";
            public static String Visible = "visible_flag";
            public static String ModifyFlag = "modify_flag";
            public static String DataType = "datatype";
            public static String Precision = "precision";
            public static String Scale = "scale";
            public static String Minimum = "minimum";
            public static String Maximum = "maximum";

            //used for year end negative amount validation
            public static String EmployeeId = "employee_id";
        }
        #endregion

        #region select
        internal YearEndAdjustmentCollection GetYearEndAdjustment(DatabaseUser user, long employeeId, Decimal year, long yearEndAdjustmentTableId, String employerNumber, String provinceStateCode, bool? visibleFlag, long revision, String columnImportIdentifier)
        {
            using (DataBaseCommand command = GetStoredProcCommand("YearEndAdjustment_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@employeeId", employeeId);
                command.AddParameterWithValue("@year", year);
                command.AddParameterWithValue("@yearEndAdjustmentTableId", yearEndAdjustmentTableId);
                command.AddParameterWithValue("@languageCode", user.LanguageCode);
                command.AddParameterWithValue("@employerNumber", employerNumber);
                command.AddParameterWithValue("@provinceStateCode", provinceStateCode);
                command.AddParameterWithValue("@visibleFlag", visibleFlag);
                command.AddParameterWithValue("@revision", revision);
                command.AddParameterWithValue("@columnImportIdentifier", columnImportIdentifier);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToYearEndAdjustmentCollection(reader);
            }
        }
        public long? SelectYearEndNegativeAmountEmployee(DatabaseUser user, Decimal year, int revision)
        {
            using (DataBaseCommand command = GetStoredProcCommand("YearEndNegativeAmountEmployee_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@year", year);
                command.AddParameterWithValue("@revision", revision);

                using (IDataReader reader = command.ExecuteReader())
                    return MapEmployeeId(reader);
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, YearEndAdjustment yearEndObj)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("YearEndAdjustment_update", user.DatabaseName))
                {
                    command.BeginTransaction("YearEndAdjustment_update");

                    command.AddParameterWithValue("@tableName", yearEndObj.TableName);
                    command.AddParameterWithValue("@columnName", yearEndObj.ColumnName);
                    command.AddParameterWithValue("@id", yearEndObj.Id);
                    command.AddParameterWithValue("@value", yearEndObj.ColumnValue);
                    command.AddParameterWithValue("@adjustmentValue", yearEndObj.AdjustmentValue);

                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected YearEndAdjustmentCollection MapToYearEndAdjustmentCollection(IDataReader reader)
        {
            YearEndAdjustmentCollection collection = new YearEndAdjustmentCollection();

            while (reader.Read())
                collection.Add(MapToYearEndAdjustment(reader));

            return collection;
        }
        protected YearEndAdjustment MapToYearEndAdjustment(IDataReader reader)
        {
            YearEndAdjustment yearEnd = new YearEndAdjustment();

            yearEnd.ColumnName = (String)CleanDataValue(reader[ColumnNames.ColumnName]);
            yearEnd.TableName = (String)CleanDataValue(reader[ColumnNames.TableName]);
            yearEnd.Id = (long)CleanDataValue(reader[ColumnNames.Id]);
            yearEnd.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            yearEnd.ColumnValue = (String)CleanDataValue(reader[ColumnNames.ColumnValue]);
            yearEnd.Visible = Convert.ToBoolean(CleanDataValue(reader[ColumnNames.Visible]));
            yearEnd.ModifyFlag = Convert.ToBoolean(CleanDataValue(reader[ColumnNames.ModifyFlag]));
            yearEnd.DataType = (String)CleanDataValue(reader[ColumnNames.DataType]);
            yearEnd.Precision = (Int32)CleanDataValue(reader[ColumnNames.Precision]);

            if (CleanDataValue(reader[ColumnNames.Scale]) == null)
                yearEnd.Scale = "";
            else
                yearEnd.Scale = CleanDataValue(reader[ColumnNames.Scale]).ToString();

            yearEnd.Minimum = (Decimal?)CleanDataValue(reader[ColumnNames.Minimum]);
            yearEnd.Maximum = (Decimal?)CleanDataValue(reader[ColumnNames.Maximum]);

            return yearEnd;
        }
        protected long? MapEmployeeId(IDataReader reader)
        {
            long? employeeId = null;

            while (reader.Read())
                employeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);

            return employeeId;
        }
        #endregion
    }
}