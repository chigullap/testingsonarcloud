﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerSecurityUser : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String SecurityUserId = "security_user_id";
            public static String UserName = "user_name";
            public static String Email = "email";
            public static String ContactChannelId = "contact_channel_id";
            public static String FirstName = "first_name";
            public static String LastName = "last_name";
            public static String Password = "password";
            public static String PasswordSalt = "password_salt";
            public static String PersonId = "person_id";
            public static String LastFailedPasswordAttemptDatetime = "last_failed_password_attempt_datetime";
            public static String LastFailedPasswordAttemptCount = "last_failed_password_attempt_count";
            public static String LastLoginDate = "last_login_datetime";
            public static String WorklinksAdministrationFlag = "worklinks_administration_flag";
            public static String IsLockedOutFlag = "is_locked_out_flag";
            public static String LastLockoutDatetime = "last_lockout_datetime";
            public static String LastPasswordChangedDatetime = "last_password_changed_datetime";
            public static String Comment = "comment";
            public static String LastActivityDatetime = "last_activity_datetime";
            public static String PasswordExpiryDatetime = "password_expiry_datetime";
            public static String LoginExpiryDatetime = "login_expiry_datetime";
            public static String AttachmentId = "attachment_id";
        }
        #endregion

        #region main
        internal SecurityUserCollection Select(String databaseName, string userName)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SecurityUser_select", databaseName))
            {
                command.AddParameterWithValue("@userName", userName);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToSecurityUserCollection(reader);
                }
            }
        }

        //AUTH proc
        internal bool DoesUserExistOnAuth(string databaseName, string userName)
        {
            bool userFoundOnAuth = false;

            using (DataBaseCommand command = GetStoredProcCommand("SecurityUser_select", databaseName))
            {
                command.AddParameterWithValue("@userName", userName);
                //command.AddParameterWithValue("@clientName", null);

                using (IDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        userFoundOnAuth = true;
                        break;
                    }
                }
            }

            return userFoundOnAuth;
        }

        internal SecurityUserCollection SelectByEmployeeId(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeeId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SecurityUserByEmployeeId_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@employeeId", employeeId);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToSecurityUserCollection(reader);
                }
            }
        }

        internal SecurityUserCollection SelectSecurityUserPerson(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long personId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SecurityUserPerson_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@personId", personId);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToSecurityUserCollection(reader);
                }
            }
        }

        public void UpdateSecurityUser(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, string overrideDatabaseName, SecurityUser securityUser)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SecurityUser_update", overrideDatabaseName))
                {
                    command.BeginTransaction("SecurityUser_update");

                    //for updates on AUTH, null out the securityUserId so userName will be used to find the user and perform the update
                    if (securityUser.SecurityUserId != -1)
                        command.AddParameterWithValue("@securityUserId", securityUser.SecurityUserId);
                    else
                        command.AddParameterWithValue("@securityUserId", null);

                    command.AddParameterWithValue("@userName", securityUser.UserName);
                    command.AddParameterWithValue("@isLockedOutFlag", securityUser.IsLockedOutFlag);
                    command.AddParameterWithValue("@loginExpiryDatetime", securityUser.LoginExpiryDatetime);
                    command.AddParameterWithValue("@worklinksAdministrationFlag", securityUser.WorklinksAdministrationFlag);
                    command.AddParameterWithValue("@attachmentId", securityUser.AttachmentId);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", securityUser.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                    securityUser.RowVersion = (byte[])rowVersionParm.Value;
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void UpdatePassword(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, string adminDatabaseName, String username, SecurityUser securityUser)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SecurityUser_updatePassword", adminDatabaseName))
                {
                    command.BeginTransaction("SecurityUser_updatePassword");

                    command.AddParameterWithValue("@securityUserId", securityUser.SecurityUserId);
                    command.AddParameterWithValue("@userName", username);
                    command.AddParameterWithValue("@newPassword", securityUser.Password);
                    command.AddParameterWithValue("@newSalt", securityUser.PasswordSalt);
                    command.AddParameterWithValue("@passwordExpiry", securityUser.PasswordExpiryDatetime);
                    command.AddParameterWithValue("@LastPasswordChangedDatetime", Time);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", securityUser.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                    securityUser.RowVersion = (byte[])rowVersionParm.Value;
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void UpdateLoginInfo(String databaseName, SecurityUser user)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SecurityUser_updateLoginInfo", databaseName))
                {
                    command.BeginTransaction("SecurityUser_updateLoginInfo");

                    command.AddParameterWithValue("@securityUserId", user.SecurityUserId);
                    command.AddParameterWithValue("@userName", user.UserName);
                    command.AddParameterWithValue("@lastLoginDatetime", user.LastLoginDate);
                    command.AddParameterWithValue("@lastFailedPasswordAttemptDatetime", user.LastFailedPasswordAttemptDatetime);
                    command.AddParameterWithValue("@lastFailedPasswordAttemptCount", user.LastFailedPasswordAttemptCount);
                    command.AddParameterWithValue("@isLockedOutFlag", user.IsLockedOutFlag);
                    command.AddParameterWithValue("@lastLockoutDatetime", user.LastLockoutDatetime);
                    command.AddParameterWithValue("@loginExpiryDatetime", user.LoginExpiryDatetime);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", user.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                    user.RowVersion = (byte[])rowVersionParm.Value;
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        //public void UpdateLogoutUser(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String userName)
        //{
        //    try
        //    {
        //        using (DataBaseCommand command = GetStoredProcCommand("SecurityUser_updateLogOutInfo", user.DatabaseName))
        //        {
        //            command.BeginTransaction("SecurityUser_updateLogOutInfo");

        //            command.AddParameterWithValue("@userName", userName);
        //            command.AddParameterWithValue("@lastLogoutDatetime", Time);
        //            command.AddParameterWithValue("@updateUser", userName);
        //            command.AddParameterWithValue("@updateDatetime", Time);

        //            command.ExecuteNonQuery();
        //            command.CommitTransaction();
        //        }
        //    }
        //    catch (Exception exc)
        //    {
        //        HandleException(exc, ((SqlException)exc).Number);
        //    }
        //}

        public SecurityUser Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String databaseNameOverride, SecurityUser securityUser)
        {
            using (DataBaseCommand command = GetStoredProcCommand("SecurityUser_insert", databaseNameOverride))
            {
                command.BeginTransaction("SecurityUser_insert");

                SqlParameter SecurityUserIdParm = command.AddParameterWithValue("@securityUserId", securityUser.SecurityUserId, ParameterDirection.Output);
                command.AddParameterWithValue("@userName", securityUser.UserName);
                command.AddParameterWithValue("@newPassword", securityUser.Password);
                command.AddParameterWithValue("@newSalt", securityUser.PasswordSalt);
                command.AddParameterWithValue("@personId", securityUser.PersonId);
                command.AddParameterWithValue("@passwordExpiry", Time);
                command.AddParameterWithValue("@LastPasswordChangedDatetime", Time);
                command.AddParameterWithValue("@loginExpiryDatetime", securityUser.LoginExpiryDatetime);
                command.AddParameterWithValue("@worklinksAdministrationFlag", securityUser.WorklinksAdministrationFlag);
                command.AddParameterWithValue("@attachmentId", securityUser.AttachmentId);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", securityUser.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@createUser", user.UserName);
                command.AddParameterWithValue("@createDatetime", Time);

                command.ExecuteNonQuery();
                securityUser.SecurityUserId = Convert.ToInt64(SecurityUserIdParm.Value);
                securityUser.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();

                return securityUser;
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long personId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SecurityUser_delete", user.DatabaseName))
                {
                    command.BeginTransaction("SecurityUser_delete");
                    command.AddParameterWithValue("@personId", personId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void InsertSecurityUserClaim(DatabaseUser user, SecurityUser securityUser, String authDatabaseName, String ngSecurityClientId, String type, String value)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("SecurityUserClaim_insert", authDatabaseName))
                {
                    command.BeginTransaction("SecurityUserClaim_insert");
                    command.AddParameterWithValue("@userName", securityUser.UserName);
                    command.AddParameterWithValue("@ngSecurityClientId", ngSecurityClientId);
                    command.AddParameterWithValue("@type", type);
                    command.AddParameterWithValue("@value", value);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected SecurityUserCollection MapToSecurityUserCollection(IDataReader reader)
        {
            SecurityUserCollection collection = new SecurityUserCollection();

            while (reader.Read())
            {
                collection.Add(MapToSecurityUser(reader));
            }

            return collection;
        }

        protected SecurityUser MapToSecurityUser(IDataReader reader)
        {
            SecurityUser item = new SecurityUser();
            base.MapToBase(item, reader);

            item.SecurityUserId = (long)CleanDataValue(reader[ColumnNames.SecurityUserId]);
            item.UserName = (String)CleanDataValue(reader[ColumnNames.UserName]);
            item.Email = (String)CleanDataValue(reader[ColumnNames.Email]);
            item.ContactChannelId = (long?)CleanDataValue(reader[ColumnNames.ContactChannelId]);
            item.FirstName = (String)CleanDataValue(reader[ColumnNames.FirstName]);
            item.LastName = (String)CleanDataValue(reader[ColumnNames.LastName]);
            item.Password = (String)CleanDataValue(reader[ColumnNames.Password]);
            item.PasswordSalt = (String)CleanDataValue(reader[ColumnNames.PasswordSalt]);
            item.PersonId = (long?)CleanDataValue(reader[ColumnNames.PersonId]);
            item.LastFailedPasswordAttemptDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.LastFailedPasswordAttemptDatetime]);
            item.LastFailedPasswordAttemptCount = (int)CleanDataValue(reader[ColumnNames.LastFailedPasswordAttemptCount]);
            item.LastLoginDate = (DateTime?)CleanDataValue(reader[ColumnNames.LastLoginDate]);
            item.WorklinksAdministrationFlag = (bool)CleanDataValue(reader[ColumnNames.WorklinksAdministrationFlag]);
            item.IsLockedOutFlag = (bool)CleanDataValue(reader[ColumnNames.IsLockedOutFlag]);
            item.LastLockoutDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.LastLockoutDatetime]);
            item.LastPasswordChangedDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.LastPasswordChangedDatetime]);
            item.Comment = (String)CleanDataValue(reader[ColumnNames.Comment]);
            item.LastActivityDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.LastActivityDatetime]);
            item.PasswordExpiryDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.PasswordExpiryDatetime]);
            item.LoginExpiryDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.LoginExpiryDatetime]);
            item.AttachmentId = (long?)CleanDataValue(reader[ColumnNames.AttachmentId]);

            return item;
        }
        #endregion
    }
}