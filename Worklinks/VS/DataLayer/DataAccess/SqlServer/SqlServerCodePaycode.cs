﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerCodePaycode : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PaycodeCode = "code_paycode_cd";
            public static String PaycodeTypeCode = "code_paycode_type_cd";
            public static String EnglishDescription = "english_description";
            public static String FrenchDescription = "french_description";
            public static String ImportExternalIdentifier = "import_external_identifier";
            public static String SortOrder = "sort_order";
            public static String StartDate = "start_date";
            public static String EndDate = "end_date";
            public static String ActiveFlag = "active_flag";
            public static String SystemFlag = "system_flag";
            public static String IncludeInPayslipFlag = "include_in_payslip_flag";
            public static String IncludeInPayRegisterFlag = "include_in_pay_register_flag";
            public static String GeneralLedgerMask = "general_ledger_mask";
            public static String AllowGeneralLedgerOverrideFlag = "allow_general_ledger_override_flag";
            public static String AutoPopulateRateFlag = "auto_populate_rate_flag";
            public static String AmountRateFactor = "amount_rate_factor";
           public static String IncludeEmploymentInsuranceHoursFlag = "include_employment_insurance_hours_flag";
            public static String ReportGroupCodePaycodeType = "report_group_code_paycode_type_cd";
            public static String ReportDisplayUnitFlag = "report_display_unit_flag";
            public static String GarnishmentFlag = "garnishment_flag";
            public static String IncludeInArrearsFlag = "include_in_arrears_flag";
            public static String RecurringIncomeCodeFlag = "recurring_income_code_flag";
            public static String VacationCalculationOverrideFlag = "vacation_calculation_override_flag";
            public static String SubtractHourFromSalaryFlag = "subtract_hour_from_salary_flag";
            public static String UseSalaryStandardHourFlag = "use_salary_standard_hour_flag";
            public static String PaycodeActivationFrequencyCode = "code_paycode_activation_frequency_cd";
            public static String PayPeriodMaximumAmount = "pay_period_maximum_amount";
            public static String PayPeriodMinimumAmount = "pay_period_minimum_amount";
            public static String YearlyMaximumAmount = "yearly_maximum_amount";
            public static String YearlyMinimumAmount = "yearly_minimum_amount";
            public static String RequiredMinimumIncomeAmount = "required_minimum_income_amount";
            public static String TaxOverrideFlag = "tax_override_flag";
        }
        #endregion

        #region select
        internal CodePaycodeCollection Select(DatabaseUser user, String paycodeCode, bool useExternalFlag)
        {
            DataBaseCommand command = GetStoredProcCommand("CodePaycode_select", user.DatabaseName);

            command.AddParameterWithValue("@paycodeCode", useExternalFlag ? null : paycodeCode);
            command.AddParameterWithValue("@importExternalIdentifier", useExternalFlag ? paycodeCode : null);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCodeCollection(reader);
        }
        #endregion

        #region insert
        public CodePaycode Insert(DatabaseUser user, CodePaycode paycode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CodePaycode_insert", user.DatabaseName))
            {
                command.BeginTransaction("CodePaycode_insert");

                SqlParameter codePaycodeParm = command.AddParameterWithValue("@paycodeCode", paycode.PaycodeCode, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@paycodeTypeCode", paycode.PaycodeTypeCode);
                command.AddParameterWithValue("@englishDescription", paycode.EnglishDescription);
                command.AddParameterWithValue("@frenchDescription", paycode.FrenchDescription);
                command.AddParameterWithValue("@importExternalIdentifier", paycode.PaycodeCodeImportExternalIdentifier);
                command.AddParameterWithValue("@sortOrder", paycode.SortOrder);
                command.AddParameterWithValue("@activeFlag", paycode.ActiveFlag);
                command.AddParameterWithValue("@systemFlag", paycode.SystemFlag);
                command.AddParameterWithValue("@includeInPayslipFlag", paycode.IncludeInPayslipFlag);
                command.AddParameterWithValue("@includeInPayRegisterFlag", paycode.IncludeInPayRegisterFlag);
                command.AddParameterWithValue("@generalLedgerMask", paycode.GeneralLedgerMask);
                command.AddParameterWithValue("@allowGeneralLedgerOverrideFlag", paycode.AllowGeneralLedgerOverrideFlag);
                command.AddParameterWithValue("@autoPopulateRateFlag", paycode.AutoPopulateRateFlag);
                command.AddParameterWithValue("@amountRateFactor", paycode.AmountRateFactor);
                command.AddParameterWithValue("@includeEmploymentInsuranceHoursFlag", paycode.IncludeEmploymentInsuranceHoursFlag);
                command.AddParameterWithValue("@reportGroupPaycodeTypeCode", paycode.ReportGroupCodePaycodeTypeCd);
                command.AddParameterWithValue("@reportDisplayUnitFlag", paycode.ReportDisplayUnitFlag);
                command.AddParameterWithValue("@garnishmentFlag", paycode.GarnishmentFlag);
                command.AddParameterWithValue("@includeInArrearsFlag", paycode.IncludeInArrearsFlag);
                command.AddParameterWithValue("@recurringIncomeCodeFlag", paycode.RecurringIncomeCodeFlag);
                command.AddParameterWithValue("@vacationCalculationOverrideFlag", paycode.VacationCalculationOverrideFlag);
                command.AddParameterWithValue("@subtractHourFromSalaryFlag", paycode.SubtractHourFromSalaryFlag);
                command.AddParameterWithValue("@useSalaryStandardHourFlag", paycode.UseSalaryStandardHourFlag);
                command.AddParameterWithValue("@paycodeActivationFrequencyCode", paycode.PaycodeActivationFrequencyCode);
                command.AddParameterWithValue("@payPeriodMaximumAmount", paycode.PayPeriodMaximumAmount);
                command.AddParameterWithValue("@payPeriodMinimumAmount", paycode.PayPeriodMinimumAmount);
                command.AddParameterWithValue("@yearlyMaximumAmount", paycode.YearlyMaximumAmount);
                command.AddParameterWithValue("@yearlyMinimumAmount", paycode.YearlyMinimumAmount);
                command.AddParameterWithValue("@requiredMinimumIncomeAmount", paycode.RequiredMinimumIncomeAmount);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", paycode.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                paycode.PaycodeCode = (String)codePaycodeParm.Value;
                paycode.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return paycode;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, CodePaycode paycode, bool overrideConcurrencyCheckFlag)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CodePaycode_update", user.DatabaseName))
                {
                    command.BeginTransaction("CodePaycode_update");

                    command.AddParameterWithValue("@paycodeCode", paycode.PaycodeCode);
                    command.AddParameterWithValue("@paycodeTypeCode", paycode.PaycodeTypeCode);
                    command.AddParameterWithValue("@englishDescription", paycode.EnglishDescription);
                    command.AddParameterWithValue("@frenchDescription", paycode.FrenchDescription);
                    command.AddParameterWithValue("@importExternalIdentifier", paycode.PaycodeCodeImportExternalIdentifier);
                    command.AddParameterWithValue("@sortOrder", paycode.SortOrder);
                    command.AddParameterWithValue("@activeFlag", paycode.ActiveFlag);
                    command.AddParameterWithValue("@systemFlag", paycode.SystemFlag);
                    command.AddParameterWithValue("@includeInPayslipFlag", paycode.IncludeInPayslipFlag);
                    command.AddParameterWithValue("@includeInPayRegisterFlag", paycode.IncludeInPayRegisterFlag);
                    command.AddParameterWithValue("@generalLedgerMask", paycode.GeneralLedgerMask);
                    command.AddParameterWithValue("@allowGeneralLedgerOverrideFlag", paycode.AllowGeneralLedgerOverrideFlag);
                    command.AddParameterWithValue("@autoPopulateRateFlag", paycode.AutoPopulateRateFlag);
                    command.AddParameterWithValue("@amountRateFactor", paycode.AmountRateFactor);
                    command.AddParameterWithValue("@includeEmploymentInsuranceHoursFlag", paycode.IncludeEmploymentInsuranceHoursFlag);
                    command.AddParameterWithValue("@reportGroupPaycodeTypeCode", paycode.ReportGroupCodePaycodeTypeCd);
                    command.AddParameterWithValue("@reportDisplayUnitFlag", paycode.ReportDisplayUnitFlag);
                    command.AddParameterWithValue("@garnishmentFlag", paycode.GarnishmentFlag);
                    command.AddParameterWithValue("@includeInArrearsFlag", paycode.IncludeInArrearsFlag);
                    command.AddParameterWithValue("@recurringIncomeCodeFlag", paycode.RecurringIncomeCodeFlag);
                    command.AddParameterWithValue("@vacationCalculationOverrideFlag", paycode.VacationCalculationOverrideFlag);
                    command.AddParameterWithValue("@subtractHourFromSalaryFlag", paycode.SubtractHourFromSalaryFlag);
                    command.AddParameterWithValue("@useSalaryStandardHourFlag", paycode.UseSalaryStandardHourFlag);
                    command.AddParameterWithValue("@paycodeActivationFrequencyCode", paycode.PaycodeActivationFrequencyCode);
                    command.AddParameterWithValue("@payPeriodMaximumAmount", paycode.PayPeriodMaximumAmount);
                    command.AddParameterWithValue("@payPeriodMinimumAmount", paycode.PayPeriodMinimumAmount);
                    command.AddParameterWithValue("@yearlyMaximumAmount", paycode.YearlyMaximumAmount);
                    command.AddParameterWithValue("@yearlyMinimumAmount", paycode.YearlyMinimumAmount);
                    command.AddParameterWithValue("@requiredMinimumIncomeAmount", paycode.RequiredMinimumIncomeAmount);
                    command.AddParameterWithValue("@overrideConcurrencyCheckFlag", overrideConcurrencyCheckFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", paycode.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    paycode.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, CodePaycode paycode)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("CodePaycode_delete", user.DatabaseName))
                {
                    command.BeginTransaction("CodePaycode_delete");

                    command.AddParameterWithValue("@paycodeCode", paycode.PaycodeCode);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected CodePaycodeCollection MapToCodeCollection(IDataReader reader)
        {
            CodePaycodeCollection collection = new CodePaycodeCollection();

            while (reader.Read())
                collection.Add(MapToCode(reader));

            return collection;
        }

        protected CodePaycode MapToCode(IDataReader reader)
        {
            CodePaycode code = new CodePaycode();
            base.MapToBase(code, reader);

            code.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            code.PaycodeTypeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeTypeCode]);
            code.EnglishDescription = (String)CleanDataValue(reader[ColumnNames.EnglishDescription]);
            code.FrenchDescription = (String)CleanDataValue(reader[ColumnNames.FrenchDescription]);
            code.PaycodeCodeImportExternalIdentifier = (String)CleanDataValue(reader[ColumnNames.ImportExternalIdentifier]);
            code.SortOrder = (short)CleanDataValue(reader[ColumnNames.SortOrder]);
            code.StartDate = (DateTime)CleanDataValue(reader[ColumnNames.StartDate]);
            code.EndDate = (DateTime)CleanDataValue(reader[ColumnNames.EndDate]);
            code.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            code.SystemFlag = (bool)CleanDataValue(reader[ColumnNames.SystemFlag]);
            code.IncludeInPayslipFlag = (bool)CleanDataValue(reader[ColumnNames.IncludeInPayslipFlag]);
            code.IncludeInPayRegisterFlag = (bool)CleanDataValue(reader[ColumnNames.IncludeInPayRegisterFlag]);
            code.GeneralLedgerMask = (String)CleanDataValue(reader[ColumnNames.GeneralLedgerMask]);
            code.AllowGeneralLedgerOverrideFlag = (bool)CleanDataValue(reader[ColumnNames.AllowGeneralLedgerOverrideFlag]);
            code.AutoPopulateRateFlag = (bool)CleanDataValue(reader[ColumnNames.AutoPopulateRateFlag]);
            code.AmountRateFactor = (Decimal)CleanDataValue(reader[ColumnNames.AmountRateFactor]);
            code.IncludeEmploymentInsuranceHoursFlag = (bool)CleanDataValue(reader[ColumnNames.IncludeEmploymentInsuranceHoursFlag]);
            code.ReportGroupCodePaycodeTypeCd = (String)CleanDataValue(reader[ColumnNames.ReportGroupCodePaycodeType]);
            code.ReportDisplayUnitFlag = (bool)CleanDataValue(reader[ColumnNames.ReportDisplayUnitFlag]);
            code.GarnishmentFlag = (bool)CleanDataValue(reader[ColumnNames.GarnishmentFlag]);
            code.IncludeInArrearsFlag = (bool)CleanDataValue(reader[ColumnNames.IncludeInArrearsFlag]);
            code.RecurringIncomeCodeFlag = (bool)CleanDataValue(reader[ColumnNames.RecurringIncomeCodeFlag]);
            code.VacationCalculationOverrideFlag = (bool)CleanDataValue(reader[ColumnNames.VacationCalculationOverrideFlag]);
            code.SubtractHourFromSalaryFlag = (bool)CleanDataValue(reader[ColumnNames.SubtractHourFromSalaryFlag]);
            code.UseSalaryStandardHourFlag = (bool)CleanDataValue(reader[ColumnNames.UseSalaryStandardHourFlag]);
            code.PaycodeActivationFrequencyCode = (String)CleanDataValue(reader[ColumnNames.PaycodeActivationFrequencyCode]);
            code.PayPeriodMaximumAmount = (Decimal?)CleanDataValue(reader[ColumnNames.PayPeriodMaximumAmount]);
            code.PayPeriodMinimumAmount = (Decimal?)CleanDataValue(reader[ColumnNames.PayPeriodMinimumAmount]);
            code.YearlyMaximumAmount = (Decimal?)CleanDataValue(reader[ColumnNames.YearlyMaximumAmount]);
            code.YearlyMinimumAmount = (Decimal?)CleanDataValue(reader[ColumnNames.YearlyMinimumAmount]);
            code.RequiredMinimumIncomeAmount = (Decimal?)CleanDataValue(reader[ColumnNames.RequiredMinimumIncomeAmount]);
            code.TaxOverrideFlag = (bool)CleanDataValue(reader[ColumnNames.TaxOverrideFlag]);

            return code;
        }
        #endregion 
    }
}