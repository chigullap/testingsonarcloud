﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerYearEnd : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String YearEndId = "year_end_id";
            public static String Year = "year";
            public static String CodeYearEndStatusCd = "code_year_end_status_cd";
            public static String T4PrintDatetime = "t4_print_datetime";
            public static String T4aPrintDatetime = "t4a_print_datetime";
            public static String R1PrintDatetime = "r1_print_datetime";
            public static String R2PrintDatetime = "r2_print_datetime";
            public static String NR4PrintDatetime = "nr4_print_datetime";
            public static String T4MassFileDatetime = "t4_mass_file_datetime";
            public static String T4aMassFileDatetime = "t4a_mass_file_datetime";
            public static String R1MassFileDatetime = "r1_mass_file_datetime";
            public static String R2MassFileDatetime = "r2_mass_file_datetime";
            public static String NR4MassFileDatetime = "nr4_mass_file_datetime";
            public static String T4ExportDatetime = "t4_export_datetime";
            public static String T4aExportDatetime = "t4a_export_datetime";
            public static String R1ExportDatetime = "r1_export_datetime";
            public static String R2ExportDatetime = "r2_export_datetime";
            public static String NR4ExportDatetime = "nr4_export_datetime";
            public static String CurrentRevision = "current_revision";
        }
        #endregion

        #region main
        internal YearEndCollection Select(DatabaseUser user, Decimal? year, bool getLatestFlag)
        {
            DataBaseCommand command = GetStoredProcCommand("YearEnd_select", user.DatabaseName);

            command.AddParameterWithValue("@year", year);
            command.AddParameterWithValue("@getLatestFlag", getLatestFlag);

            using (IDataReader reader = command.ExecuteReader())
            {
                command.AddParameterWithValue("@year", year);
                return MapToYearEndCollection(reader);
            }
        }
        public void Update(DatabaseUser user, YearEnd yearEnd)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("YearEnd_update", user.DatabaseName))
                {
                    command.BeginTransaction("YearEnd_update");

                    command.AddParameterWithValue("@yearEndId", yearEnd.YearEndId);
                    command.AddParameterWithValue("@year", yearEnd.Year);
                    command.AddParameterWithValue("@codeYearEndStatusCd", yearEnd.CodeYearEndStatusCd);
                    command.AddParameterWithValue("@t4PrintDatetime", yearEnd.T4PrintDatetime);
                    command.AddParameterWithValue("@t4aPrintDatetime", yearEnd.T4aPrintDatetime);
                    command.AddParameterWithValue("@r1PrintDatetime", yearEnd.R1PrintDatetime);
                    command.AddParameterWithValue("@r2PrintDatetime", yearEnd.R2PrintDatetime);
                    command.AddParameterWithValue("@nr4PrintDatetime", yearEnd.NR4PrintDatetime);
                    command.AddParameterWithValue("@t4MassFileDatetime", yearEnd.T4MassFileDatetime);
                    command.AddParameterWithValue("@t4aMassFileDatetime", yearEnd.T4aMassFileDatetime);
                    command.AddParameterWithValue("@r1MassFileDatetime", yearEnd.R1MassFileDatetime);
                    command.AddParameterWithValue("@r2MassFileDatetime", yearEnd.R2MassFileDatetime);
                    command.AddParameterWithValue("@nr4MassFileDatetime", yearEnd.NR4MassFileDatetime);
                    command.AddParameterWithValue("@t4ExportDatetime", yearEnd.T4ExportDatetime);
                    command.AddParameterWithValue("@t4aExportDatetime", yearEnd.T4aExportDatetime);
                    command.AddParameterWithValue("@r1ExportDatetime", yearEnd.R1ExportDatetime);
                    command.AddParameterWithValue("@r2ExportDatetime", yearEnd.R2ExportDatetime);
                    command.AddParameterWithValue("@nr4ExportDatetime", yearEnd.NR4ExportDatetime);
                    command.AddParameterWithValue("@currentRevision", yearEnd.CurrentRevision);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", yearEnd.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    yearEnd.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public YearEnd Insert(DatabaseUser user, YearEnd yearEnd)
        {
            using (DataBaseCommand command = GetStoredProcCommand("YearEnd_insert", user.DatabaseName))
            {
                command.BeginTransaction("YearEnd_insert");

                SqlParameter vendorIdParm = command.AddParameterWithValue("@yearEndId", yearEnd.YearEndId, ParameterDirection.Output);
                SqlParameter yearParm = command.AddParameterWithValue("@year", yearEnd.Year, ParameterDirection.InputOutput);

                //stacy commented out 2016-NOV-01.  Not used since it was put in years ago.  Remove eventually, might be used in new year end version
                //SqlDbType type = new SqlDbType();
                //type = SqlDbType.Decimal;

                command.AddParameterWithValue("@codeYearEndStatusCd", yearEnd.CodeYearEndStatusCd);
                command.AddParameterWithValue("@t4PrintDatetime", yearEnd.T4PrintDatetime);
                command.AddParameterWithValue("@t4aPrintDatetime", yearEnd.T4aPrintDatetime);
                command.AddParameterWithValue("@r1PrintDatetime", yearEnd.R1PrintDatetime);
                command.AddParameterWithValue("@r2PrintDatetime", yearEnd.R2PrintDatetime);
                command.AddParameterWithValue("@nr4PrintDatetime", yearEnd.NR4PrintDatetime);
                command.AddParameterWithValue("@t4MassFileDatetime", yearEnd.T4MassFileDatetime);
                command.AddParameterWithValue("@t4aMassFileDatetime", yearEnd.T4aMassFileDatetime);
                command.AddParameterWithValue("@r1MassFileDatetime", yearEnd.R1MassFileDatetime);
                command.AddParameterWithValue("@r2MassFileDatetime", yearEnd.R2MassFileDatetime);
                command.AddParameterWithValue("@nr4MassFileDatetime", yearEnd.NR4MassFileDatetime);
                command.AddParameterWithValue("@t4ExportDatetime", yearEnd.T4ExportDatetime);
                command.AddParameterWithValue("@t4aExportDatetime", yearEnd.T4aExportDatetime);
                command.AddParameterWithValue("@r1ExportDatetime", yearEnd.R1ExportDatetime);
                command.AddParameterWithValue("@r2ExportDatetime", yearEnd.R2ExportDatetime);
                command.AddParameterWithValue("@nr4ExportDatetime", yearEnd.NR4ExportDatetime);
                command.AddParameterWithValue("@currentRevision", yearEnd.CurrentRevision);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", yearEnd.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                yearEnd.YearEndId = Convert.ToInt64(vendorIdParm.Value);
                yearEnd.RowVersion = (byte[])rowVersionParm.Value;
                yearEnd.Year = (Decimal)yearParm.Value;

                command.CommitTransaction();

                return yearEnd;
            }
        }
        public void Delete(DatabaseUser user, YearEnd yearEnd)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("YearEnd_delete", user.DatabaseName))
                {
                    command.BeginTransaction("YearEnd_delete");

                    command.AddParameterWithValue("@yearEndId", yearEnd.YearEndId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void PopulateYearEndTables(DatabaseUser user, Decimal year)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("YearEnd_generate", user.DatabaseName))
                {
                    command.BeginTransaction("YearEnd_generate");

                    command.AddParameterWithValue("@year", year);
                    command.AddParameterWithValue("@user", user.UserName);
                    command.AddParameterWithValue("@current", DateTime.Now);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected YearEndCollection MapToYearEndCollection(IDataReader reader)
        {
            YearEndCollection collection = new YearEndCollection();

            while (reader.Read())
                collection.Add(MapToYearEnd(reader));

            return collection;
        }
        protected YearEnd MapToYearEnd(IDataReader reader)
        {
            YearEnd item = new YearEnd();
            base.MapToBase(item, reader);

            item.YearEndId = (long)CleanDataValue(reader[ColumnNames.YearEndId]);
            item.Year = (Decimal)CleanDataValue(reader[ColumnNames.Year]);
            item.CodeYearEndStatusCd = (String)CleanDataValue(reader[ColumnNames.CodeYearEndStatusCd]);
            item.T4PrintDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.T4PrintDatetime]);
            item.T4aPrintDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.T4aPrintDatetime]);
            item.R1PrintDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.R1PrintDatetime]);
            item.R2PrintDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.R2PrintDatetime]);
            item.NR4PrintDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.NR4PrintDatetime]);
            item.T4MassFileDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.T4MassFileDatetime]);
            item.T4aMassFileDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.T4aMassFileDatetime]);
            item.R1MassFileDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.R1MassFileDatetime]);
            item.R2MassFileDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.R2MassFileDatetime]);
            item.NR4MassFileDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.NR4MassFileDatetime]);
            item.T4ExportDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.T4ExportDatetime]);
            item.T4aExportDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.T4aExportDatetime]);
            item.R1ExportDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.R1ExportDatetime]);
            item.R2ExportDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.R2ExportDatetime]);
            item.NR4ExportDatetime = (DateTime?)CleanDataValue(reader[ColumnNames.NR4ExportDatetime]);
            item.CurrentRevision = (int)CleanDataValue(reader[ColumnNames.CurrentRevision]);

            return item;
        }
        #endregion
    }
}