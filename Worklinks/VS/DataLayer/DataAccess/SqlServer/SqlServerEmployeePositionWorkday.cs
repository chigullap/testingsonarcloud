﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerEmployeePositionWorkday : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeePositionWorkdayId = "employee_position_workday_id";
            public static String EmployeePositionId = "employee_position_id";
            public static String IsWorkdayFlag = "is_workday_flag";
            public static String DayOfWeek = "day_of_week";
        }
        #endregion

        #region select
        public EmployeePositionWorkdayCollection Select(DatabaseUser user, long employeePositionId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeePositionWorkday_select", user.DatabaseName);

            command.AddParameterWithValue("@employeePositionId", employeePositionId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToEmployeePositionWorkdayCollection(reader);
        }

        public EmployeePositionWorkdayCollection SelectBatchEmployeePositionWorkday(DatabaseUser user, EmployeePositionCollection positionIds)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionWorkday_batchSelectByPositionId", user.DatabaseName))
            {
                SqlParameter parmPositionids = new SqlParameter();
                parmPositionids.ParameterName = "@parmPositionIds";
                parmPositionids.SqlDbType = SqlDbType.Structured;

                DataTable tablePositionids = LoadEmployeePositionIdDataTable(positionIds);
                parmPositionids.Value = tablePositionids;
                command.AddParameter(parmPositionids);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToEmployeePositionWorkdayCollection(reader);
            }
        }
        private DataTable LoadEmployeePositionIdDataTable(EmployeePositionCollection positionIds)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("employee_position_id", typeof(long));

            foreach (EmployeePosition position in positionIds)
            {
                table.Rows.Add(new Object[]
                {
                    position.EmployeePositionId
                });
            }

            return table;
        }

        public EmployeePositionWorkdayCollection SelectBatch(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionWorkday_batchSelect", user.DatabaseName))
            {
                SqlParameter parmWorkday = new SqlParameter();
                parmWorkday.ParameterName = "@parmWorkday";
                parmWorkday.SqlDbType = SqlDbType.Structured;

                DataTable tableWorkday = LoadEmployeePositionWorkdayDataTable(importExternalIdentifiers);
                parmWorkday.Value = tableWorkday;
                command.AddParameter(parmWorkday);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToEmployeePositionWorkdayCollection(reader);
            }
        }
        private DataTable LoadEmployeePositionWorkdayDataTable(List<String> importExternalIdentifiers)
        {
            DataTable table = new DataTable();

            //declare columns
            table.Columns.Add("import_external_identifier", typeof(string));

            for (int i = 0; i < importExternalIdentifiers.Count; i++)
            {
                table.Rows.Add(new Object[]
                {
                    importExternalIdentifiers[i]
                });
            }

            return table;
        }
        #endregion

        #region insert
        public EmployeePositionWorkday Insert(DatabaseUser user, EmployeePositionWorkday workday)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionWorkday_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeePositionWorkday_insert");

                SqlParameter employeePositionWorkdayIdParm = command.AddParameterWithValue("@employeePositionWorkdayId", workday.EmployeePositionWorkdayId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeePositionId", workday.EmployeePositionId);
                command.AddParameterWithValue("@isWorkdayFlag", workday.IsWorkdayFlag);
                command.AddParameterWithValue("@dayOfWeek", workday.DayOfWeek);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", workday.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                workday.EmployeePositionWorkdayId = Convert.ToInt64(employeePositionWorkdayIdParm.Value);
                workday.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return workday;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, EmployeePositionWorkday workday)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionWorkday_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePositionWorkday_update");

                    command.AddParameterWithValue("@employeePositionWorkdayId", workday.EmployeePositionWorkdayId);
                    command.AddParameterWithValue("@employeePositionId", workday.EmployeePositionId);
                    command.AddParameterWithValue("@isWorkdayFlag", workday.IsWorkdayFlag);
                    command.AddParameterWithValue("@dayOfWeek", workday.DayOfWeek);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", workday.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    workday.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, EmployeePositionWorkday workday)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionWorkday_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePositionWorkday_delete");

                    command.AddParameterWithValue("@employeePositionWorkdayId", workday.EmployeePositionWorkdayId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeePositionWorkdayCollection MapToEmployeePositionWorkdayCollection(IDataReader reader)
        {
            EmployeePositionWorkdayCollection collection = new EmployeePositionWorkdayCollection();

            while (reader.Read())
                collection.Add(MapToEmployeePositionWorkday(reader));

            return collection;
        }
        protected EmployeePositionWorkday MapToEmployeePositionWorkday(IDataReader reader)
        {
            EmployeePositionWorkday item = new EmployeePositionWorkday();
            base.MapToBase(item, reader);

            item.EmployeePositionWorkdayId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionWorkdayId]);
            item.EmployeePositionId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionId]);
            item.IsWorkdayFlag = (bool)CleanDataValue(reader[ColumnNames.IsWorkdayFlag]);
            item.DayOfWeek = (String)CleanDataValue(reader[ColumnNames.DayOfWeek]);

            return item;
        }
        #endregion
    }
}