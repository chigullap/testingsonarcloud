﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeWsibHealthAndSafetyReport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeWsibHealthAndSafetyReportId = "employee_wsib_health_and_safety_report_id";
            public static String EmployeeId = "employee_id";
            public static String LastName = "last_name";
            public static String FirstName = "first_name";
            public static String MiddleName = "middle_name";
            public static String GenderCode = "code_gender_cd";
            public static String BirthDate = "birth_date";
            public static String Occupation = "occupation";
            public static String NameOfEmployer = "name_of_employer";
            public static String AddressId = "address_id";
            public static String AddressLine1 = "address_line_1";
            public static String AddressLine2 = "address_line_2";
            public static String City = "city";
            public static String PostalZipCode = "postal_zip_code";
            public static String ProvinceStateCode = "code_province_state_cd";
            public static String CountryCode = "code_country_cd";
            public static String NatureOfIndustry = "nature_of_industry";
            public static String Branch = "branch";
            public static String IncidentDate = "incident_date";
            public static String HourStartedWork = "hour_started_work";
            public static String CauseOfIncident = "cause_of_incident";
            public static String CausedByMachinery = "caused_by_machinery";
            public static String MachineAndPartName = "machine_and_part_name";
            public static String MachineMovedByMechanicalPower = "machine_moved_by_mechanical_power";
            public static String Injuries = "injuries";
            public static String DisabledFromEarningFullWages = "disabled_from_earning_full_wages";
            public static String DateSigned = "date_signed";
        }
        #endregion

        #region main
        internal EmployeeWsibHealthAndSafetyReportCollection Select(DatabaseUser user, long employeeId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeeWsibHealthAndSafetyReport_select", user.DatabaseName);

            command.AddParameterWithValue("@employeeId", employeeId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToEmployeeWsibHealthAndSafetyReportCollection(reader);
        }
        internal EmployeeWsibHealthAndSafetyReportPersonCollection SelectPerson(DatabaseUser user, long employeeId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeeWsibHealthAndSafetyReport_Person_select", user.DatabaseName);

            command.AddParameterWithValue("@employeeId", employeeId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToEmployeeWsibHealthAndSafetyReportPersonCollection(reader);
        }
        #endregion

        #region insert
        public EmployeeWsibHealthAndSafetyReport Insert(DatabaseUser user, EmployeeWsibHealthAndSafetyReport item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeWsibHealthAndSafetyReport_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeWsibHealthAndSafetyReport_insert");

                SqlParameter itemIdParm = command.AddParameterWithValue("@employeeWsibHealthAndSafetyReportId", item.EmployeeWsibHealthAndSafetyReportId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", item.EmployeeId);
                command.AddParameterWithValue("@nameOfEmployer", item.NameOfEmployer);
                command.AddParameterWithValue("@addressId", item.AddressId);
                command.AddParameterWithValue("@natureOfIndustry", item.NatureOfIndustry);
                command.AddParameterWithValue("@branch", item.Branch);
                command.AddParameterWithValue("@incidentDate", item.IncidentDate);
                command.AddParameterWithValue("@hourStartedWork", item.HourStartedWork);
                command.AddParameterWithValue("@causeOfIncident", item.CauseOfIncident);
                command.AddParameterWithValue("@causedByMachinery", item.CausedByMachinery);
                command.AddParameterWithValue("@machineAndPartName", item.MachineAndPartName);
                command.AddParameterWithValue("@machineMovedByMechanicalPower", item.MachineMovedByMechanicalPower);
                command.AddParameterWithValue("@injuries", item.Injuries);
                command.AddParameterWithValue("@disabledFromEarningFullWages", item.DisabledFromEarningFullWages);
                command.AddParameterWithValue("@dateSigned", item.DateSigned);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.EmployeeWsibHealthAndSafetyReportId = Convert.ToInt64(itemIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, EmployeeWsibHealthAndSafetyReport item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeWsibHealthAndSafetyReport_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeWsibHealthAndSafetyReport_update");

                    command.AddParameterWithValue("@employeeWsibHealthAndSafetyReportId", item.EmployeeWsibHealthAndSafetyReportId);
                    command.AddParameterWithValue("@employeeId", item.EmployeeId);
                    command.AddParameterWithValue("@nameOfEmployer", item.NameOfEmployer);
                    command.AddParameterWithValue("@addressId", item.AddressId);
                    command.AddParameterWithValue("@natureOfIndustry", item.NatureOfIndustry);
                    command.AddParameterWithValue("@branch", item.Branch);
                    command.AddParameterWithValue("@incidentDate", item.IncidentDate);
                    command.AddParameterWithValue("@hourStartedWork", item.HourStartedWork);
                    command.AddParameterWithValue("@causeOfIncident", item.CauseOfIncident);
                    command.AddParameterWithValue("@causedByMachinery", item.CausedByMachinery);
                    command.AddParameterWithValue("@machineAndPartName", item.MachineAndPartName);
                    command.AddParameterWithValue("@machineMovedByMechanicalPower", item.MachineMovedByMechanicalPower);
                    command.AddParameterWithValue("@injuries", item.Injuries);
                    command.AddParameterWithValue("@disabledFromEarningFullWages", item.DisabledFromEarningFullWages);
                    command.AddParameterWithValue("@overrideConcurrencyCheckFlag", item.OverrideConcurrencyCheckFlag);
                    command.AddParameterWithValue("@dateSigned", item.DateSigned);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, EmployeeWsibHealthAndSafetyReport item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeWsibHealthAndSafetyReport_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeWsibHealthAndSafetyReport_delete");

                    command.AddParameterWithValue("@employeeWsibHealthAndSafetyReportId", item.EmployeeWsibHealthAndSafetyReportId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeWsibHealthAndSafetyReportCollection MapToEmployeeWsibHealthAndSafetyReportCollection(IDataReader reader)
        {
            EmployeeWsibHealthAndSafetyReportCollection collection = new EmployeeWsibHealthAndSafetyReportCollection();

            while (reader.Read())
                collection.Add(MapToEmployeeWsibHealthAndSafetyReport(reader));

            return collection;
        }
        protected EmployeeWsibHealthAndSafetyReport MapToEmployeeWsibHealthAndSafetyReport(IDataReader reader)
        {
            EmployeeWsibHealthAndSafetyReport item = new EmployeeWsibHealthAndSafetyReport();
            base.MapToBase(item, reader);

            item.EmployeeWsibHealthAndSafetyReportId = (long)CleanDataValue(reader[ColumnNames.EmployeeWsibHealthAndSafetyReportId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.NameOfEmployer = (long)CleanDataValue(reader[ColumnNames.NameOfEmployer]);
            item.AddressId = (long)CleanDataValue(reader[ColumnNames.AddressId]);
            item.AddressLine1 = (String)CleanDataValue(reader[ColumnNames.AddressLine1]);
            item.AddressLine2 = (String)CleanDataValue(reader[ColumnNames.AddressLine2]);
            item.City = (String)CleanDataValue(reader[ColumnNames.City]);
            item.PostalZipCode = (String)CleanDataValue(reader[ColumnNames.PostalZipCode]);
            item.ProvinceStateCode = (String)CleanDataValue(reader[ColumnNames.ProvinceStateCode]);
            item.CountryCode = (String)CleanDataValue(reader[ColumnNames.CountryCode]);
            item.NatureOfIndustry = (String)CleanDataValue(reader[ColumnNames.NatureOfIndustry]);
            item.Branch = (String)CleanDataValue(reader[ColumnNames.Branch]);
            item.IncidentDate = (DateTime)CleanDataValue(reader[ColumnNames.IncidentDate]);
            item.HourStartedWork = (TimeSpan)CleanDataValue(reader[ColumnNames.HourStartedWork]);
            item.CauseOfIncident = (String)CleanDataValue(reader[ColumnNames.CauseOfIncident]);
            item.CausedByMachinery = (String)CleanDataValue(reader[ColumnNames.CausedByMachinery]);
            item.MachineAndPartName = (String)CleanDataValue(reader[ColumnNames.MachineAndPartName]);
            item.MachineMovedByMechanicalPower = (String)CleanDataValue(reader[ColumnNames.MachineMovedByMechanicalPower]);
            item.Injuries = (String)CleanDataValue(reader[ColumnNames.Injuries]);
            item.DisabledFromEarningFullWages = (String)CleanDataValue(reader[ColumnNames.DisabledFromEarningFullWages]);
            item.DateSigned = (DateTime)CleanDataValue(reader[ColumnNames.DateSigned]);

            return item;
        }
        protected EmployeeWsibHealthAndSafetyReportPersonCollection MapToEmployeeWsibHealthAndSafetyReportPersonCollection(IDataReader reader)
        {
            EmployeeWsibHealthAndSafetyReportPersonCollection collection = new EmployeeWsibHealthAndSafetyReportPersonCollection();

            while (reader.Read())
                collection.Add(MapToEmployeeWsibHealthAndSafetyReportPerson(reader));

            return collection;
        }
        protected EmployeeWsibHealthAndSafetyReportPerson MapToEmployeeWsibHealthAndSafetyReportPerson(IDataReader reader)
        {
            EmployeeWsibHealthAndSafetyReportPerson item = new EmployeeWsibHealthAndSafetyReportPerson();
            base.MapToBase(item, reader);

            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.LastName = (String)CleanDataValue(reader[ColumnNames.LastName]);
            item.FirstName = (String)CleanDataValue(reader[ColumnNames.FirstName]);
            item.MiddleName = (String)CleanDataValue(reader[ColumnNames.MiddleName]);
            item.GenderCode = (String)CleanDataValue(reader[ColumnNames.GenderCode]);
            item.BirthDate = (DateTime?)CleanDataValue(reader[ColumnNames.BirthDate]);
            item.AddressLine1 = (String)CleanDataValue(reader[ColumnNames.AddressLine1]);
            item.AddressLine2 = (String)CleanDataValue(reader[ColumnNames.AddressLine2]);
            item.City = (String)CleanDataValue(reader[ColumnNames.City]);
            item.PostalZipCode = (String)CleanDataValue(reader[ColumnNames.PostalZipCode]);
            item.ProvinceStateCode = (String)CleanDataValue(reader[ColumnNames.ProvinceStateCode]);
            item.CountryCode = (String)CleanDataValue(reader[ColumnNames.CountryCode]);
            item.Occupation = (String)CleanDataValue(reader[ColumnNames.Occupation]);

            return item;
        }
        #endregion
    }
}