﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerCraWcbExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String CraWcbExportId = "cra_wcb_export_id";
            public static String RbcSequenceNumber = "rbc_sequence_number";
            public static String Data = "data";
            public static String CodeCraRemittancePeriodCd = "code_cra_remittance_period_cd";
            public static String CraFileTransmissionDate = "cra_file_transmission_date";
        }
        #endregion

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CraWcbExport item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CraWcbExport_insert", user.DatabaseName))
            {
                command.BeginTransaction("CraWcbExport_insert");

                SqlParameter craWcbExportIdParm = command.AddParameterWithValue("@craWcbExportId", item.CraWcbExportId, ParameterDirection.Output);
                command.AddParameterWithValue("@rbcSequenceNumber", item.RbcSequenceNumber);
                command.AddParameterWithValue("@data", item.Data);
                command.AddParameterWithValue("@codeCraRemittancePeriodCd", item.CodeCraRemittancePeriodCd);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.CraWcbExportId = Convert.ToInt64(craWcbExportIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();
            }
        }

        public void UpdateCraWcbExportFileTransmissionDate(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long craWcbExportId, DateTime exportDate)
        {
            using (DataBaseCommand command = GetStoredProcCommand("CraWcbExport_UpdateCraFileTransmissionDate", user.DatabaseName))
            {
                command.BeginTransaction("CraWcbExport_UpdateCraFileTransmissionDate");

                command.AddParameterWithValue("@craWcbExportId", craWcbExportId);
                command.AddParameterWithValue("@exportDate", exportDate);

                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }

        public CraWcbExportCollection CheckForUnprocessedCraWcbRemittanceOnDatabase(WLP.BusinessLayer.BusinessObjects.DatabaseUser user)
        {
            DataBaseCommand command = GetStoredProcCommand("CraWcbExport_selectUnprocessed", user.DatabaseName);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToCraWcbExportCollection(reader);
            }
        }

        public CraWcbExportCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? craWcbExportId)
        {
            DataBaseCommand command = GetStoredProcCommand("CraWcbExport_select", user.DatabaseName);
            command.AddParameterWithValue("@craWcbExportId", craWcbExportId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToCraWcbExportCollection(reader);
            }
        }

        #region data mapping
        protected CraWcbExportCollection MapToCraWcbExportCollection(IDataReader reader)
        {
            CraWcbExportCollection collection = new CraWcbExportCollection();
            while (reader.Read())
            {
                collection.Add(MapToCraWcbExport(reader));
            }

            return collection;
        }
        protected CraWcbExport MapToCraWcbExport(IDataReader reader)
        {
            CraWcbExport item = new CraWcbExport();
            base.MapToBase(item, reader);

            item.CraWcbExportId = (long)CleanDataValue(reader[ColumnNames.CraWcbExportId]);
            item.RbcSequenceNumber = (long)CleanDataValue(reader[ColumnNames.RbcSequenceNumber]);
            item.Data = (byte[])CleanDataValue(reader[ColumnNames.Data]);
            item.CodeCraRemittancePeriodCd = (String)CleanDataValue(reader[ColumnNames.CodeCraRemittancePeriodCd]);
            item.CraFileTransmissionDate = (DateTime?)CleanDataValue(reader[ColumnNames.CraFileTransmissionDate]);

            return item;
        }

        #endregion
    }
}
