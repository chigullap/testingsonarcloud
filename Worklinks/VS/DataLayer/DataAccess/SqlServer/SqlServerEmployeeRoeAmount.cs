﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeRoeAmount : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeRoeAmountId = "employee_roe_amount_id";
            public static String EmployeePositionId = "employee_position_id";
            public static String TotalInsurableHours = "total_insurable_hours";
            public static String TotalInsurableEarningAmount = "total_insurable_earning_amount";
            public static String RoeFileProcessedFlag = "roe_file_processed_flag";
            public static String RoeId = "roe_id";
            public static String BusinessNumber = "business_number";
            public static String PaymentFrequencyCode = "code_payment_frequency_cd";
        }
        #endregion

        #region main

        internal EmployeeRoeAmountCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeePositionId, long employeeId, bool securityOverrideFlag)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeeRoeAmount_select", user.DatabaseName);

            command.AddParameterWithValue("@employeePositionId", employeePositionId);
            command.AddParameterWithValue("@employeeId", employeeId);
            command.AddParameterWithValue("@updateUser", user.UserName);
            command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
            command.AddParameterWithValue("@securityUserId", user.SecurityUserId);
            command.AddParameterWithValue("@securityOverride", securityOverrideFlag);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToEmployeeRoeAmountCollection(reader);
            }
        }

        internal EmployeeRoeAmountSummaryCollection SelectEmployeeRoeAmountSummary(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeePositionId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeeRoeAmount_report", user.DatabaseName);

            command.AddParameterWithValue("@employeePositionId", employeePositionId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToEmployeeRoeAmountSummaryCollection(reader);
            }
        }

        public void Update(DatabaseUser user, EmployeeRoeAmount employeeRoeAmount)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeRoeAmount_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeRoeAmount_update");

                    command.AddParameterWithValue("@employeeRoeAmountId", employeeRoeAmount.EmployeeRoeAmountId);
                    command.AddParameterWithValue("@employeePositionId", employeeRoeAmount.EmployeePositionId);
                    command.AddParameterWithValue("@totalInsurableHours", employeeRoeAmount.TotalInsurableHours);
                    command.AddParameterWithValue("@totalInsurableEarningAmount", employeeRoeAmount.TotalInsurableEarningAmount);
                    command.AddParameterWithValue("@roeFileProcessedFlag", employeeRoeAmount.RoeFileProcessedFlag);
                    command.AddParameterWithValue("@roeId", employeeRoeAmount.RoeId);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", employeeRoeAmount.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    employeeRoeAmount.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public EmployeeRoeAmount Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeRoeAmount employeeRoeAmount)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeRoeAmount_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeRoeAmount_insert");

                SqlParameter employeeRoeAmountIdParm = command.AddParameterWithValue("@employeeRoeAmountId", employeeRoeAmount.EmployeeRoeAmountId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeePositionId", employeeRoeAmount.EmployeePositionId);
                command.AddParameterWithValue("@totalInsurableHours", employeeRoeAmount.TotalInsurableHours);
                command.AddParameterWithValue("@totalInsurableEarningAmount", employeeRoeAmount.TotalInsurableEarningAmount);
                command.AddParameterWithValue("@roeFileProcessedFlag", employeeRoeAmount.RoeFileProcessedFlag);
                command.AddParameterWithValue("@roeId", employeeRoeAmount.RoeId);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", employeeRoeAmount.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                employeeRoeAmount.EmployeeRoeAmountId = Convert.ToInt64(employeeRoeAmountIdParm.Value);
                employeeRoeAmount.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();

                return employeeRoeAmount;
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeRoeAmount employeeRoeAmount)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeRoeAmount_delete", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeRoeAmount_delete");
                command.AddParameterWithValue("@employeeRoeAmountId", employeeRoeAmount.EmployeeRoeAmountId);

                command.ExecuteNonQuery();
                command.CommitTransaction();
            }
        }
        #endregion

        #region data mapping
        protected EmployeeRoeAmountCollection MapToEmployeeRoeAmountCollection(IDataReader reader)
        {
            EmployeeRoeAmountCollection collection = new EmployeeRoeAmountCollection();
            while (reader.Read())
            {
                collection.Add(MapToEmployeeRoeAmount(reader));
            }

            return collection;
        }
        protected EmployeeRoeAmountSummaryCollection MapToEmployeeRoeAmountSummaryCollection(IDataReader reader)
        {
            EmployeeRoeAmountSummaryCollection collection = new EmployeeRoeAmountSummaryCollection();
            while (reader.Read())
            {
                collection.Add(MapToEmployeeRoeAmountSummary(reader));
            }

            return collection;
        }
        protected EmployeeRoeAmount MapToEmployeeRoeAmount(IDataReader reader)
        {
            EmployeeRoeAmount employeeRoe = new EmployeeRoeAmount();
            return MapToEmployeeRoeAmount(employeeRoe, reader);

        }
        protected EmployeeRoeAmount MapToEmployeeRoeAmount(EmployeeRoeAmount employeeRoe, IDataReader reader)
        {
            base.MapToBase(employeeRoe, reader);

            employeeRoe.EmployeeRoeAmountId = (long)CleanDataValue(reader[ColumnNames.EmployeeRoeAmountId]);
            employeeRoe.EmployeePositionId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionId]);
            employeeRoe.TotalInsurableHours = (Decimal)CleanDataValue(reader[ColumnNames.TotalInsurableHours]);
            employeeRoe.TotalInsurableEarningAmount = (Decimal)CleanDataValue(reader[ColumnNames.TotalInsurableEarningAmount]);
            employeeRoe.RoeFileProcessedFlag = (Boolean)CleanDataValue(reader[ColumnNames.RoeFileProcessedFlag]);
            employeeRoe.RoeId = (long?)CleanDataValue(reader[ColumnNames.RoeId]);

            return employeeRoe;
        }
        protected EmployeeRoeAmountSummary MapToEmployeeRoeAmountSummary(IDataReader reader)
        {
            EmployeeRoeAmountSummary employeeRoe = new EmployeeRoeAmountSummary();
            MapToEmployeeRoeAmount(employeeRoe, reader);

            employeeRoe.BusinessNumber = (String)CleanDataValue(reader[ColumnNames.BusinessNumber]);
            employeeRoe.PaymentFrequencyCode = (String)CleanDataValue(reader[ColumnNames.PaymentFrequencyCode]);

            return employeeRoe;
        }
        #endregion
    }
}