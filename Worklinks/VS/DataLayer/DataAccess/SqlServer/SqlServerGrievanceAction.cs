﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerGrievanceAction : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String GrievanceActionId = "grievance_action_id";
            public static String GrievanceId = "grievance_id";
            public static String GrievanceActionStepCode = "code_grievance_action_step_cd";
            public static String ActionDate = "action_date";
            public static String DiscussedWith = "discussed_with";
            public static String Note = "note";
            public static String GrievanceActionStepResolutonCode = "code_grievance_action_step_resoluton_cd";
        }
        #endregion

        #region main
        /// <summary>
        /// returns an Grievance
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        internal GrievanceActionCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long grievanceId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("GrievanceAction_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@grievanceId", grievanceId);

                GrievanceActionCollection actions = null;

                using (IDataReader reader = command.ExecuteReader())
                {
                    actions = MapToGrievanceActionCollection(reader);
                }

                return actions;
            }
        }
        #endregion

        #region data mapping
        /// <summary>
        /// maps result set to Grievance collection
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected GrievanceActionCollection MapToGrievanceActionCollection(IDataReader reader)
        {
            GrievanceActionCollection collection = new GrievanceActionCollection();
            while (reader.Read())
            {
                collection.Add(MapToGrievanceAction(reader));
            }

            return collection;
        }

        /// <summary>
        /// maps columns to Grievance properties
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected GrievanceAction MapToGrievanceAction(IDataReader reader)
        {
            GrievanceAction action = new GrievanceAction();
            base.MapToBase(action, reader);

            action.GrievanceActionId = (long)CleanDataValue(reader[ColumnNames.GrievanceActionId]);
            action.GrievanceId = (long)CleanDataValue(reader[ColumnNames.GrievanceId]);
            action.GrievanceActionStepCode = (String)CleanDataValue(reader[ColumnNames.GrievanceActionStepCode]);
            action.ActionDate = (DateTime)CleanDataValue(reader[ColumnNames.ActionDate]);
            action.DiscussedWith = (String)CleanDataValue(reader[ColumnNames.DiscussedWith]);
            action.Note = (String)CleanDataValue(reader[ColumnNames.Note]);
            action.GrievanceActionStepResolutonCode = (String)CleanDataValue(reader[ColumnNames.GrievanceActionStepResolutonCode]);

            return action;
        }

        #endregion

        #region update
        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, GrievanceAction data)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("GrievanceAction_update", user.DatabaseName))
                {
                    command.BeginTransaction("GrievanceAction_update");

                    command.AddParameterWithValue("@grievanceActionId", data.GrievanceActionId);
                    command.AddParameterWithValue("@grievanceId", data.GrievanceId);
                    command.AddParameterWithValue("@grievanceActionStepCode", data.GrievanceActionStepCode);
                    command.AddParameterWithValue("@actionDate", data.ActionDate);
                    command.AddParameterWithValue("@discussedWith", data.DiscussedWith);
                    command.AddParameterWithValue("@note", data.Note);
                    command.AddParameterWithValue("@GrievanceActionStepResolutonCode", data.GrievanceActionStepResolutonCode);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", data.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    data.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();

                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region insert
        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, GrievanceAction data)
        {
            using (DataBaseCommand command = GetStoredProcCommand("GrievanceAction_insert", user.DatabaseName))
            {
                command.BeginTransaction("GrievanceAction_insert");
                SqlParameter GrievanceActionIdParm = command.AddParameterWithValue("@grievanceActionId", data.GrievanceActionId, ParameterDirection.Output);

                command.AddParameterWithValue("@grievanceId", data.GrievanceId);
                command.AddParameterWithValue("@grievanceActionStepCode", data.GrievanceActionStepCode);
                command.AddParameterWithValue("@actionDate", data.ActionDate);
                command.AddParameterWithValue("@discussedWith", data.DiscussedWith);
                command.AddParameterWithValue("@note", data.Note);
                command.AddParameterWithValue("@grievanceActionStepResolutonCode", data.GrievanceActionStepResolutonCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", data.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                data.GrievanceActionId = Convert.ToInt64(GrievanceActionIdParm.Value);
                data.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();

            }
        }
        #endregion

        #region delete
        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, GrievanceAction Grievance)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("GrievanceAction_delete", user.DatabaseName))
                {
                    command.BeginTransaction("GrievanceAction_delete");
                    command.AddParameterWithValue("@grievanceActionId", Grievance.GrievanceActionId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();

                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion
    }
}