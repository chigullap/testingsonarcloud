﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerPaycodeAssociation : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PaycodeAssociationId = "paycode_association_id";
            public static String PaycodeCode = "code_paycode_cd";
            public static String PaycodeAssociationTypeCode = "code_paycode_association_type_cd";
            public static String ExternalCode = "external_code";
            public static String ProvinceStateCode = "code_province_state_cd";
        }
        #endregion

        #region select
        internal PaycodeAssociationCollection Select(DatabaseUser user, String paycodeAssociationTypeCode)
        {
            DataBaseCommand command = GetStoredProcCommand("PaycodeAssociation_select", user.DatabaseName);
            command.AddParameterWithValue("@paycodeAssociationTypeCode", paycodeAssociationTypeCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToPaycodeAssociationCollection(reader);
        }
        #endregion

        #region insert
        public PaycodeAssociation Insert(DatabaseUser user, PaycodeAssociation paycode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PaycodeAssociation_insert", user.DatabaseName))
            {
                command.BeginTransaction("PaycodeAssociation_insert");

                SqlParameter paycodeIdParm = command.AddParameterWithValue("@paycodeAssociationId", paycode.PaycodeAssociationId, ParameterDirection.Output);
                command.AddParameterWithValue("@paycodeCode", paycode.PaycodeCode);
                command.AddParameterWithValue("@paycodeAssociationTypeCode", paycode.PaycodeAssociationTypeCode);
                command.AddParameterWithValue("@externalCode", paycode.ExternalCode);
                command.AddParameterWithValue("@provinceStateCode", paycode.ProvinceStateCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", paycode.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                paycode.PaycodeAssociationId = Convert.ToInt64(paycodeIdParm.Value);
                paycode.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return paycode;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, PaycodeAssociation paycode)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PaycodeAssociation_update", user.DatabaseName))
                {
                    command.BeginTransaction("PaycodeAssociation_update");

                    command.AddParameterWithValue("@paycodeAssociationId", paycode.PaycodeAssociationId);
                    command.AddParameterWithValue("@paycodeCode", paycode.PaycodeCode);
                    command.AddParameterWithValue("@paycodeAssociationTypeCode", paycode.PaycodeAssociationTypeCode);
                    command.AddParameterWithValue("@externalCode", paycode.ExternalCode);
                    command.AddParameterWithValue("@provinceStateCode", paycode.ProvinceStateCode);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", paycode.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    paycode.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, PaycodeAssociation paycode)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PaycodeAssociation_delete", user.DatabaseName))
                {
                    command.BeginTransaction("PaycodeAssociation_delete");
                    command.AddParameterWithValue("@paycodeAssociationId", paycode.PaycodeAssociationId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected PaycodeAssociationCollection MapToPaycodeAssociationCollection(IDataReader reader)
        {
            PaycodeAssociationCollection collection = new PaycodeAssociationCollection();

            while (reader.Read())
                collection.Add(MapToPaycodeAssociation(reader));

            return collection;
        }

        protected PaycodeAssociation MapToPaycodeAssociation(IDataReader reader)
        {
            PaycodeAssociation item = new PaycodeAssociation();
            base.MapToBase(item, reader);

            item.PaycodeAssociationId = (long)CleanDataValue(reader[ColumnNames.PaycodeAssociationId]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.PaycodeAssociationTypeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeAssociationTypeCode]);
            item.ExternalCode = (String)CleanDataValue(reader[ColumnNames.ExternalCode]);
            item.ProvinceStateCode = (String)CleanDataValue(reader[ColumnNames.ProvinceStateCode]);

            return item;
        }
        #endregion
    }
}