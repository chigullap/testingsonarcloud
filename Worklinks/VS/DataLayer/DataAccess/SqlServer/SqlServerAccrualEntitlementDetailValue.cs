﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerAccrualEntitlementDetailValue : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String AccrualEntitlementDetailValueId = "accrual_entitlement_detail_value_id";
            public static String AccrualEntitlementDetailId = "accrual_entitlement_detail_id";
            public static String EffectiveMonth = "effective_month";
            public static String AmountPercentage = "amount_percentage";
        }
        #endregion

        #region main
        internal AccrualEntitlementDetailValueCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long entitlementDetailId)
        {
            DataBaseCommand command = GetStoredProcCommand("AccrualEntitlementDetailValue_select", user.DatabaseName);

            command.AddParameterWithValue("@accrualEntitlementDetailId", entitlementDetailId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToAccrualEntitlementDetailValueCollection(reader);
        }
        public AccrualEntitlementDetailValue Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetailValue detailValue)
        {
            using (DataBaseCommand command = GetStoredProcCommand("AccrualEntitlementDetailValue_insert", user.DatabaseName))
            {
                command.BeginTransaction("AccrualEntitlementDetailValue_insert");

                SqlParameter detailValueIdParam = command.AddParameterWithValue("@accrualEntitlementDetailValueId", detailValue.AccrualEntitlementDetailValueId, ParameterDirection.Output);
                command.AddParameterWithValue("@accrualEntitlementDetailId", detailValue.AccrualEntitlementDetailId);
                command.AddParameterWithValue("@effectiveMonth", detailValue.EffectiveMonth);
                command.AddParameterWithValue("@amountPercentage", detailValue.AmountPercentage);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", detailValue.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                detailValue.AccrualEntitlementDetailValueId = Convert.ToInt64(detailValueIdParam.Value);
                detailValue.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return detailValue;
            }
        }
        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetailValue detailValue)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("AccrualEntitlementDetailValue_update", user.DatabaseName))
                {
                    command.BeginTransaction("AccrualEntitlementDetailValue_update");

                    command.AddParameterWithValue("@accrualEntitlementDetailValueId", detailValue.AccrualEntitlementDetailValueId);
                    command.AddParameterWithValue("@accrualEntitlementDetailId", detailValue.AccrualEntitlementDetailId);
                    command.AddParameterWithValue("@effectiveMonth", detailValue.EffectiveMonth);
                    command.AddParameterWithValue("@amountPercentage", detailValue.AmountPercentage);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", detailValue.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    detailValue.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, AccrualEntitlementDetailValue detailValue)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("AccrualEntitlementDetailValue_delete", user.DatabaseName))
                {
                    command.BeginTransaction("AccrualEntitlementDetailValue_delete");

                    command.AddParameterWithValue("@accrualEntitlementDetailValueId", detailValue.AccrualEntitlementDetailValueId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected AccrualEntitlementDetailValueCollection MapToAccrualEntitlementDetailValueCollection(IDataReader reader)
        {
            AccrualEntitlementDetailValueCollection collection = new AccrualEntitlementDetailValueCollection();

            while (reader.Read())
                collection.Add(MapToAccrualEntitlementDetailValue(reader));

            return collection;
        }
        protected AccrualEntitlementDetailValue MapToAccrualEntitlementDetailValue(IDataReader reader)
        {
            AccrualEntitlementDetailValue item = new AccrualEntitlementDetailValue();
            base.MapToBase(item, reader);

            item.AccrualEntitlementDetailValueId = (long)CleanDataValue(reader[ColumnNames.AccrualEntitlementDetailValueId]);
            item.AccrualEntitlementDetailId = (long)CleanDataValue(reader[ColumnNames.AccrualEntitlementDetailId]);
            item.EffectiveMonth = (int?)CleanDataValue(reader[ColumnNames.EffectiveMonth]);
            item.AmountPercentage = (Decimal?)CleanDataValue(reader[ColumnNames.AmountPercentage]);

            return item;
        }
        #endregion
    }
}