﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeePositionLabourDistribution : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public const String EmployeePositionLabourDistributionId = "employee_position_labour_distribution_id";
            public const String EmployeePositionId = "employee_position_id";
            public const String OrganizationUnitId = "organization_unit_id";
            public const String Percentage = "percentage";
            public const String Description = "description";
            public const String OrganizationUnit = "organization_unit";
        }
        #endregion

        #region select
        internal EmployeePositionLabourDistributionSummaryCollection Select(DatabaseUser user, long employeePositionId, String codeLanguageCd)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionLabourDistribution_report", user.DatabaseName))
            {
                command.AddParameterWithValue("@employeePositionId", employeePositionId);
                command.AddParameterWithValue("@codeLanguageCd", codeLanguageCd);

                EmployeePositionLabourDistributionSummaryCollection collection = null;

                using (IDataReader reader = command.ExecuteReader())
                    collection = MapToEmployeePositionLabourDistributionSummaryCollection(user, reader);

                return collection;
            }
        }
        #endregion

        #region insert
        public EmployeePositionLabourDistributionSummary Insert(DatabaseUser user, EmployeePositionLabourDistributionSummary summary)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionLabourDistribution_insert", user.DatabaseName))
            {
                try
                {
                    command.BeginTransaction("EmployeePositionLabourDistribution_insert");

                    SqlParameter EmployeePositionLabourDistributionIdParm = command.AddParameterWithValue("@employeePositionLabourDistributionId", summary.EmployeePositionLabourDistributionId, ParameterDirection.Output);
                    command.AddParameterWithValue("@employeePositionId", summary.EmployeePositionId);
                    command.AddParameterWithValue("@percentage", summary.Percentage);
                    command.AddParameterWithValue("@organizationUnit", summary.OrganizationUnit);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", summary.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    summary.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
                catch (Exception exc)
                {
                    command.RollbackTransaction();
                    HandleException(exc, ((SqlException)exc).Number);
                }

                return summary;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, EmployeePositionLabourDistributionSummary summary)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionLabourDistribution_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePositionLabourDistribution_update");

                    command.AddParameterWithValue("@employeePositionLabourDistributionId", summary.EmployeePositionLabourDistributionId);
                    command.AddParameterWithValue("@percentage", summary.Percentage);
                    command.AddParameterWithValue("@organizationUnit", summary.OrganizationUnit);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", summary.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    summary.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, EmployeePositionLabourDistributionSummary summary)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePositionLabourDistribution_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePositionLabourDistribution_delete");

                    command.AddParameterWithValue("@employeePositionLabourDistributionId", summary.EmployeePositionLabourDistributionId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeePositionLabourDistributionSummaryCollection MapToEmployeePositionLabourDistributionSummaryCollection(DatabaseUser user, IDataReader reader)
        {
            EmployeePositionLabourDistributionSummaryCollection collection = new EmployeePositionLabourDistributionSummaryCollection();

            while (reader.Read())
                collection.Add(MapToEmployeePositionLabourDistributionSummary(user, reader, collection));

            return collection;
        }
        protected EmployeePositionLabourDistributionSummary MapToEmployeePositionLabourDistributionSummary(DatabaseUser user, IDataReader reader, EmployeePositionLabourDistributionSummaryCollection collection)
        {
            EmployeePositionLabourDistributionSummary item = new EmployeePositionLabourDistributionSummary();
            base.MapToBase(item, reader);

            item.EmployeePositionLabourDistributionId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionLabourDistributionId]);
            item.EmployeePositionId = (long)CleanDataValue(reader[ColumnNames.EmployeePositionId]);
            item.OrganizationUnit = (String)CleanDataValue(reader[ColumnNames.OrganizationUnit]);
            item.Percentage = (Decimal)CleanDataValue(reader[ColumnNames.Percentage]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);

            return item;
        }
        #endregion
    }
}