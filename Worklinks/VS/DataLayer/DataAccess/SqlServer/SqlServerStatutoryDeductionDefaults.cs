﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerStatutoryDeductionDefaults : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String StatutoryDeductionDefaultId = "statutory_deduction_default_id";
            public static String EffectiveDate = "effective_date";
            public static String ProvinceStateCode = "code_province_state_cd";
            public static String BusinessNumberId = "business_number_id";
            public static String ActiveFlag = "active_flag";
            public static String PayEmploymentInsuranceFlag = "pay_employment_insurance_flag";
            public static String PayCanadaPensionPlanFlag = "pay_canada_pension_plan_flag";
            public static String ParentalInsurancePlanFlag = "parental_insurance_plan_flag";
            public static String FederalTaxClaim = "federal_tax_claim";
            public static String FederalAdditionalTax = "federal_additional_tax";
            public static String FederalDesignatedAreaDeduction = "federal_designated_area_deduction";
            public static String FederalAuthorizedAnnualDeduction = "federal_authorized_annual_deduction";
            public static String FederalAuthorizedAnnualTaxCredit = "federal_authorized_annual_tax_credit";
            public static String FederalLabourTaxCredit = "federal_labour_tax_credit";
            public static String FederalPayTaxFlag = "federal_pay_tax_flag";
            public static String ProvincialTaxClaim = "provincial_tax_claim";
            public static String ProvincialAdditionalTax = "provincial_additional_tax";
            public static String ProvincialDesignatedAreaDeduction = "provincial_designated_area_deduction";
            public static String ProvincialAuthorizedAnnualDeduction = "provincial_authorized_annual_deduction";
            public static String ProvincialAuthorizedAnnualTaxCredit = "provincial_authorized_annual_tax_credit";
            public static String ProvincialLabourTaxCredit = "provincial_labour_tax_credit";
            public static String ProvincialPayTaxFlag = "provincial_pay_tax_flag";
            public static String EstimatedAnnualIncome = "estimated_annual_income";
            public static String EstimatedAnnualExpense = "estimated_annual_expense";
            public static String EstimatedNetIncome = "estimated_net_income";
            public static String CommissionPercentage = "commission_percentage";
        }
        #endregion

        #region main
        /// <summary>
        /// returns a StatutoryDeductionDefault
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        internal StatutoryDeductionDefaultCollection Select(DatabaseUser user, String codeProvinceStateCd)
        {
            using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeductionDefault_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@codeProvinceStateCd", codeProvinceStateCd);

                StatutoryDeductionDefaultCollection StatutoryDeductionDefaults = null;

                using (IDataReader reader = command.ExecuteReader())
                    StatutoryDeductionDefaults = MapToStatutoryDeductionDefaultCollection(reader);

                return StatutoryDeductionDefaults;
            }
        }
        //public void Update(StatutoryDeduction StatutoryDeduction)
        //{
        //    using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeductionDefault_update"))
        //    {
        //        command.BeginTransaction("StatutoryDeductionDefault_update");
        //        command.AddParameterWithValue("@statutoryDeductionId", StatutoryDeduction.StatutoryDeductionId);
        //        command.AddParameterWithValue("@employeeId", StatutoryDeduction.EmployeeId);
        //        command.AddParameterWithValue("@provinceStateCode", StatutoryDeduction.ProvinceStateCode);
        //        command.AddParameterWithValue("@businessNumberId", StatutoryDeduction.BusinessNumberId);
        //        command.AddParameterWithValue("@activeFlag", StatutoryDeduction.ActiveFlag);
        //        command.AddParameterWithValue("@payEmploymentInsuranceFlag", StatutoryDeduction.PayEmploymentInsuranceFlag);
        //        command.AddParameterWithValue("@payCanadaPensionPlanFlag", StatutoryDeduction.PayCanadaPensionPlanFlag);
        //        command.AddParameterWithValue("@parentalInsurancePlanFlag", StatutoryDeduction.ParentalInsurancePlanFlag);
        //        command.AddParameterWithValue("@federalTaxClaim", StatutoryDeduction.FederalTaxClaim);
        //        command.AddParameterWithValue("@federalAdditionalTax", StatutoryDeduction.FederalAdditionalTax);
        //        command.AddParameterWithValue("@federalDesignatedAreaDeduction", StatutoryDeduction.FederalDesignatedAreaDeduction);
        //        command.AddParameterWithValue("@federalAuthorizedAnnualDeduction", StatutoryDeduction.FederalAuthorizedAnnualDeduction);
        //        command.AddParameterWithValue("@federalAuthorizedAnnualTaxCredit", StatutoryDeduction.FederalAuthorizedAnnualTaxCredit);
        //        command.AddParameterWithValue("@federalLabourTaxCredit", StatutoryDeduction.FederalLabourTaxCredit);
        //        command.AddParameterWithValue("@federalPayTaxFlag", StatutoryDeduction.FederalPayTaxFlag);
        //        command.AddParameterWithValue("@provincialTaxClaim", StatutoryDeduction.ProvincialTaxClaim);
        //        command.AddParameterWithValue("@provincialAdditionalTax", StatutoryDeduction.ProvincialAdditionalTax);
        //        command.AddParameterWithValue("@provincialDesignatedAreaDeduction", StatutoryDeduction.ProvincialDesignatedAreaDeduction);
        //        command.AddParameterWithValue("@provincialAuthorizedAnnualDeduction", StatutoryDeduction.ProvincialAuthorizedAnnualDeduction);
        //        command.AddParameterWithValue("@provincialAuthorizedAnnualTaxCredit", StatutoryDeduction.ProvincialAuthorizedAnnualTaxCredit);
        //        command.AddParameterWithValue("@provincialLabourTaxCredit", StatutoryDeduction.ProvincialLabourTaxCredit);
        //        command.AddParameterWithValue("@provincialPayTaxFlag", StatutoryDeduction.ProvincialPayTaxFlag);
        //        command.AddParameterWithValue("@estimatedAnnualIncome", StatutoryDeduction.EstimatedAnnualIncome);
        //        command.AddParameterWithValue("@estimatedAnnualExpense", StatutoryDeduction.EstimatedAnnualExpense);
        //        command.AddParameterWithValue("@estimatedNetIncome", StatutoryDeduction.EstimatedNetIncome);
        //        command.AddParameterWithValue("@commissionPercentage", StatutoryDeduction.CommissionPercentage);

        //        SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", StatutoryDeduction.RowVersion, ParameterDirection.InputOutput);
        //        command.AddParameterWithValue("@updateUser", StatutoryDeduction.UpdateUser);
        //        command.AddParameterWithValue("@updateDatetime", StatutoryDeduction.UpdateDatetime);

        //        command.ExecuteNonQuery();
        //        StatutoryDeduction.RowVersion = (byte[])rowVersionParm.Value;
        //        command.CommitTransaction();
        //    }
        //}
        //public void Insert(StatutoryDeduction item)
        //{
        //    using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeductionDefault_insert"))
        //    {
        //        command.BeginTransaction("StatutoryDeductionDefault_insert");
        //        SqlParameter statutoryDeductionIdParm = command.AddParameterWithValue("@statutoryDeductionId", item.StatutoryDeductionId, ParameterDirection.Output);
        //        command.AddParameterWithValue("@employeeId", item.EmployeeId);
        //        command.AddParameterWithValue("@provinceStateCode", item.ProvinceStateCode);
        //        command.AddParameterWithValue("@businessNumberId", item.BusinessNumberId);
        //        command.AddParameterWithValue("@activeFlag", item.ActiveFlag);
        //        command.AddParameterWithValue("@payEmploymentInsuranceFlag", item.PayEmploymentInsuranceFlag);
        //        command.AddParameterWithValue("@payCanadaPensionPlanFlag", item.PayCanadaPensionPlanFlag);
        //        command.AddParameterWithValue("@parentalInsurancePlanFlag", item.ParentalInsurancePlanFlag);
        //        command.AddParameterWithValue("@federalTaxClaim", item.FederalTaxClaim);
        //        command.AddParameterWithValue("@federalAdditionalTax", item.FederalAdditionalTax);
        //        command.AddParameterWithValue("@federalDesignatedAreaDeduction", item.FederalDesignatedAreaDeduction);
        //        command.AddParameterWithValue("@federalAuthorizedAnnualDeduction", item.FederalAuthorizedAnnualDeduction);
        //        command.AddParameterWithValue("@federalAuthorizedAnnualTaxCredit", item.FederalAuthorizedAnnualTaxCredit);
        //        command.AddParameterWithValue("@federalLabourTaxCredit", item.FederalLabourTaxCredit);
        //        command.AddParameterWithValue("@federalPayTaxFlag", item.FederalPayTaxFlag);
        //        command.AddParameterWithValue("@provincialTaxClaim", item.ProvincialTaxClaim);
        //        command.AddParameterWithValue("@provincialAdditionalTax", item.ProvincialAdditionalTax);
        //        command.AddParameterWithValue("@provincialDesignatedAreaDeduction", item.ProvincialDesignatedAreaDeduction);
        //        command.AddParameterWithValue("@provincialAuthorizedAnnualDeduction", item.ProvincialAuthorizedAnnualDeduction);
        //        command.AddParameterWithValue("@provincialAuthorizedAnnualTaxCredit", item.ProvincialAuthorizedAnnualTaxCredit);
        //        command.AddParameterWithValue("@provincialLabourTaxCredit", item.ProvincialLabourTaxCredit);
        //        command.AddParameterWithValue("@provincialPayTaxFlag", item.ProvincialPayTaxFlag);
        //        command.AddParameterWithValue("@estimatedAnnualIncome", item.EstimatedAnnualIncome);
        //        command.AddParameterWithValue("@estimatedAnnualExpense", item.EstimatedAnnualExpense);
        //        command.AddParameterWithValue("@estimatedNetIncome", item.EstimatedNetIncome);
        //        command.AddParameterWithValue("@commissionPercentage", item.CommissionPercentage);

        //        SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
        //        command.AddParameterWithValue("@updateUser", item.UpdateUser);
        //        command.AddParameterWithValue("@updateDatetime", item.UpdateDatetime);

        //        command.ExecuteNonQuery();
        //        item.StatutoryDeductionId = Convert.ToInt64(statutoryDeductionIdParm.Value);
        //        item.RowVersion = (byte[])rowVersionParm.Value;
        //        command.CommitTransaction();
        //    }
        //}
        public void Delete(DatabaseUser user, StatutoryDeductionDefault StatutoryDeductionDefault)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("StatutoryDeductionDefault_delete", user.DatabaseName))
                {
                    command.BeginTransaction("StatutoryDeductionDefault_delete");

                    command.AddParameterWithValue("@statutoryDeductionDefaultId", StatutoryDeductionDefault.StatutoryDeductionDefaultId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        /// <summary>
        /// maps result set to StatutoryDeductionDefault collection
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected StatutoryDeductionDefaultCollection MapToStatutoryDeductionDefaultCollection(IDataReader reader)
        {
            StatutoryDeductionDefaultCollection collection = new StatutoryDeductionDefaultCollection();

            while (reader.Read())
                collection.Add(MapToStatutoryDeductionDefault(reader));

            return collection;
        }
        /// <summary>
        /// maps columns to StatutoryDeductionDefault properties
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected StatutoryDeductionDefault MapToStatutoryDeductionDefault(IDataReader reader)
        {
            StatutoryDeductionDefault item = new StatutoryDeductionDefault();
            base.MapToBase(item, reader);

            item.StatutoryDeductionDefaultId = (long)CleanDataValue(reader[ColumnNames.StatutoryDeductionDefaultId]);
            item.EffectiveDate = (DateTime)CleanDataValue(reader[ColumnNames.EffectiveDate]);
            item.ProvinceStateCode = (String)CleanDataValue(reader[ColumnNames.ProvinceStateCode]);
            item.BusinessNumberId = (long?)CleanDataValue(reader[ColumnNames.BusinessNumberId]);

            if (CleanDataValue(reader[ColumnNames.ActiveFlag]) != null)
                item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);

            if (CleanDataValue(reader[ColumnNames.PayEmploymentInsuranceFlag]) != null)
                item.PayEmploymentInsuranceFlag = (bool)CleanDataValue(reader[ColumnNames.PayEmploymentInsuranceFlag]);

            if (CleanDataValue(reader[ColumnNames.PayCanadaPensionPlanFlag]) != null)
                item.PayCanadaPensionPlanFlag = (bool)CleanDataValue(reader[ColumnNames.PayCanadaPensionPlanFlag]);

            if (CleanDataValue(reader[ColumnNames.ParentalInsurancePlanFlag]) != null)
                item.ParentalInsurancePlanFlag = (bool)CleanDataValue(reader[ColumnNames.ParentalInsurancePlanFlag]);

            item.FederalTaxClaim = (int?)CleanDataValue(reader[ColumnNames.FederalTaxClaim]);
            item.FederalAdditionalTax = (Decimal?)CleanDataValue(reader[ColumnNames.FederalAdditionalTax]);
            item.FederalDesignatedAreaDeduction = (int?)CleanDataValue(reader[ColumnNames.FederalDesignatedAreaDeduction]);
            item.FederalAuthorizedAnnualDeduction = (int?)CleanDataValue(reader[ColumnNames.FederalAuthorizedAnnualDeduction]);
            item.FederalAuthorizedAnnualTaxCredit = (int?)CleanDataValue(reader[ColumnNames.FederalAuthorizedAnnualTaxCredit]);
            item.FederalLabourTaxCredit = (int?)CleanDataValue(reader[ColumnNames.FederalLabourTaxCredit]);

            if (CleanDataValue(reader[ColumnNames.FederalPayTaxFlag]) != null)
                item.FederalPayTaxFlag = (bool)CleanDataValue(reader[ColumnNames.FederalPayTaxFlag]);

            item.ProvincialTaxClaim = (int?)CleanDataValue(reader[ColumnNames.ProvincialTaxClaim]);
            item.ProvincialAdditionalTax = (Decimal?)CleanDataValue(reader[ColumnNames.ProvincialAdditionalTax]);
            item.ProvincialDesignatedAreaDeduction = (int?)CleanDataValue(reader[ColumnNames.ProvincialDesignatedAreaDeduction]);
            item.ProvincialAuthorizedAnnualDeduction = (int?)CleanDataValue(reader[ColumnNames.ProvincialAuthorizedAnnualDeduction]);
            item.ProvincialAuthorizedAnnualTaxCredit = (int?)CleanDataValue(reader[ColumnNames.ProvincialAuthorizedAnnualTaxCredit]);
            item.ProvincialLabourTaxCredit = (int?)CleanDataValue(reader[ColumnNames.ProvincialLabourTaxCredit]);

            if (CleanDataValue(reader[ColumnNames.ProvincialPayTaxFlag]) != null)
                item.ProvincialPayTaxFlag = (bool)CleanDataValue(reader[ColumnNames.ProvincialPayTaxFlag]);

            item.EstimatedAnnualIncome = (int?)CleanDataValue(reader[ColumnNames.EstimatedAnnualIncome]);
            item.EstimatedAnnualExpense = (int?)CleanDataValue(reader[ColumnNames.EstimatedAnnualExpense]);
            item.EstimatedNetIncome = (int?)CleanDataValue(reader[ColumnNames.EstimatedNetIncome]);
            item.CommissionPercentage = (Decimal?)CleanDataValue(reader[ColumnNames.CommissionPercentage]);

            return item;
        }
        #endregion
    }
}