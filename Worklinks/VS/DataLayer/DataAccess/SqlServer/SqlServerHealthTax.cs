﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerHealthTax : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string HealthTaxId = "health_tax_id";
            public static string CodeHealthTaxCd = "code_health_tax_cd";
            public static string EnglishDescription = "english_description";
            public static string FrenchDescription = "french_description";
            public static string ProvinceStateCode = "code_province_state_cd";
            public static string ActiveFlag = "active_flag";
            public static string HealthRemittanceAccountNumber = "health_remittance_account_number";
            public static string GenerateChequeForRemittanceFlag = "generate_cheque_for_remittance_flag";
            public static string VendorId = "vendor_id";
            public static string ImportExternalIdentifier = "import_external_identifier";
        }
        #endregion

        #region select
        internal HealthTaxCollection Select(DatabaseUser user, long? healthTaxId, bool isCeridianBilling)
        {
            DataBaseCommand command = GetStoredProcCommand("HealthTax_select", user.DatabaseName);

            command.AddParameterWithValue("@healthTaxId", healthTaxId);
            command.AddParameterWithValue("@isCeridianBilling", isCeridianBilling);

            using (IDataReader reader = command.ExecuteReader())
                return MapToHealthTaxCollection(reader);
        }
        #endregion

        #region insert
        public HealthTax Insert(DatabaseUser user, HealthTax item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("HealthTax_insert", user.DatabaseName))
                {
                    command.BeginTransaction("HealthTax_insert");

                    SqlParameter idParm = command.AddParameterWithValue("@healthTaxId", item.HealthTaxId, ParameterDirection.Output);
                    command.AddParameterWithValue("@codeHealthTaxCode", item.CodeHealthTaxCd);
                    command.AddParameterWithValue("@englishDescription", item.EnglishDescription);
                    command.AddParameterWithValue("@frenchDescription", item.FrenchDescription);
                    command.AddParameterWithValue("@provinceStateCode", item.ProvinceStateCode);
                    command.AddParameterWithValue("@activeFlag", item.ActiveFlag);
                    command.AddParameterWithValue("@healthRemittanceAccountNumber", item.HealthRemittanceAccountNumber);
                    command.AddParameterWithValue("@generateChequeForRemittanceFlag", item.GenerateChequeForRemittanceFlag);
                    command.AddParameterWithValue("@vendorId", item.VendorId);
                    command.AddParameterWithValue("importExternalIdentifier", item.ImportExternalIdentifier);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.HealthTaxId = Convert.ToInt64(idParm.Value);
                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();

                    return item;
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
                return null;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, HealthTax item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("HealthTax_update", user.DatabaseName))
                {
                    command.BeginTransaction("HealthTax_update");

                    command.AddParameterWithValue("@healthTaxId", item.HealthTaxId);
                    command.AddParameterWithValue("@codeHealthTaxCode", item.CodeHealthTaxCd);
                    command.AddParameterWithValue("@englishDescription", item.EnglishDescription);
                    command.AddParameterWithValue("@frenchDescription", item.FrenchDescription);
                    command.AddParameterWithValue("@provinceStateCode", item.ProvinceStateCode);
                    command.AddParameterWithValue("@activeFlag", item.ActiveFlag);
                    command.AddParameterWithValue("@healthRemittanceAccountNumber", item.HealthRemittanceAccountNumber);
                    command.AddParameterWithValue("@generateChequeForRemittanceFlag", item.GenerateChequeForRemittanceFlag);
                    command.AddParameterWithValue("@vendorId", item.VendorId);
                    command.AddParameterWithValue("importExternalIdentifier", item.ImportExternalIdentifier);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, HealthTax healthTax)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("HealthTax_delete", user.DatabaseName))
                {
                    command.BeginTransaction("HealthTax_delete");

                    command.AddParameterWithValue("@healthTaxId", healthTax.HealthTaxId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected HealthTaxCollection MapToHealthTaxCollection(IDataReader reader)
        {
            HealthTaxCollection collection = new HealthTaxCollection();

            while (reader.Read())
                collection.Add(MapToHealthTax(reader));

            return collection;
        }
        protected HealthTax MapToHealthTax(IDataReader reader)
        {
            HealthTax item = new HealthTax();
            base.MapToBase(item, reader);

            item.HealthTaxId = (long)CleanDataValue(reader[ColumnNames.HealthTaxId]);
            item.CodeHealthTaxCd = (string)CleanDataValue(reader[ColumnNames.CodeHealthTaxCd]);
            item.EnglishDescription = (string)CleanDataValue(reader[ColumnNames.EnglishDescription]);
            item.FrenchDescription = (string)CleanDataValue(reader[ColumnNames.FrenchDescription]);
            item.ProvinceStateCode = (string)CleanDataValue(reader[ColumnNames.ProvinceStateCode]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.HealthRemittanceAccountNumber = (string)CleanDataValue(reader[ColumnNames.HealthRemittanceAccountNumber]);
            item.GenerateChequeForRemittanceFlag = (bool)CleanDataValue(reader[ColumnNames.GenerateChequeForRemittanceFlag]);
            item.VendorId = (long?)CleanDataValue(reader[ColumnNames.VendorId]);
            item.ImportExternalIdentifier = (string)CleanDataValue(reader[ColumnNames.ImportExternalIdentifier]);

            return item;
        }
        #endregion
    }
}