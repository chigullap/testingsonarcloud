﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerBenefitPlanDetail : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String BenefitPlanDetailId = "benefit_plan_detail_id";
            public static String BenefitPlanId = "benefit_plan_id";
            public static String EffectiveDate = "effective_date";
            public static String BenefitPlanTypeCode = "code_benefit_plan_type_cd";
            public static String BenefitPlanStatusCode = "code_benefit_plan_status_cd";
            public static String VendorId = "vendor_id";
            public static String EmployeePaycodeCode = "code_employee_paycode_cd";
            public static String EmployerPaycodeCode = "code_employer_paycode_cd";
        }
        #endregion

        #region main
        internal BenefitPlanDetailCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long benefitPlanId, long benefitPlanDetailId)
        {
            DataBaseCommand command = GetStoredProcCommand("BenefitPlanDetail_select", user.DatabaseName);

            command.AddParameterWithValue("@benefitPlanId", benefitPlanId);
            command.AddParameterWithValue("@benefitPlanDetailId", benefitPlanDetailId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToBenefitPlanDetailCollection(reader);
        }

        public BenefitPlanDetail Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPlanDetail benefitPlanDetail)
        {
            using (DataBaseCommand command = GetStoredProcCommand("BenefitPlanDetail_insert", user.DatabaseName))
            {
                command.BeginTransaction("BenefitPlanDetail_insert");

                SqlParameter benefitPlanDetailIdParam = command.AddParameterWithValue("@benefitPlanDetailId", benefitPlanDetail.BenefitPlanDetailId, ParameterDirection.Output);
                command.AddParameterWithValue("@benefitPlanId", benefitPlanDetail.BenefitPlanId);
                command.AddParameterWithValue("@effectiveDate", benefitPlanDetail.EffectiveDate);
                command.AddParameterWithValue("@benefitPlanTypeCode", benefitPlanDetail.BenefitPlanTypeCode);
                command.AddParameterWithValue("@benefitPlanStatusCode", benefitPlanDetail.BenefitPlanStatusCode);
                command.AddParameterWithValue("@vendorId", benefitPlanDetail.VendorId);
                command.AddParameterWithValue("@employeePaycodeCode", benefitPlanDetail.EmployeePaycodeCode);
                command.AddParameterWithValue("@employerPaycodeCode", benefitPlanDetail.EmployerPaycodeCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", benefitPlanDetail.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                benefitPlanDetail.BenefitPlanDetailId = Convert.ToInt64(benefitPlanDetailIdParam.Value);
                benefitPlanDetail.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return benefitPlanDetail;
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPlanDetail benefitPlanDetail)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("BenefitPlanDetail_update", user.DatabaseName))
                {
                    command.BeginTransaction("BenefitPlanDetail_update");

                    command.AddParameterWithValue("@benefitPlanDetailId", benefitPlanDetail.BenefitPlanDetailId);
                    command.AddParameterWithValue("@benefitPlanId", benefitPlanDetail.BenefitPlanId);
                    command.AddParameterWithValue("@effectiveDate", benefitPlanDetail.EffectiveDate);
                    command.AddParameterWithValue("@benefitPlanTypeCode", benefitPlanDetail.BenefitPlanTypeCode);
                    command.AddParameterWithValue("@benefitPlanStatusCode", benefitPlanDetail.BenefitPlanStatusCode);
                    command.AddParameterWithValue("@vendorId", benefitPlanDetail.VendorId);
                    command.AddParameterWithValue("@employeePaycodeCode", benefitPlanDetail.EmployeePaycodeCode);
                    command.AddParameterWithValue("@employerPaycodeCode", benefitPlanDetail.EmployerPaycodeCode);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", benefitPlanDetail.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    benefitPlanDetail.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPlanDetail benefitPlanDetail)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("BenefitPlanDetail_delete", user.DatabaseName))
                {
                    command.BeginTransaction("BenefitPlanDetail_delete");

                    command.AddParameterWithValue("@benefitPlanDetailId", benefitPlanDetail.BenefitPlanDetailId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected BenefitPlanDetailCollection MapToBenefitPlanDetailCollection(IDataReader reader)
        {
            BenefitPlanDetailCollection collection = new BenefitPlanDetailCollection();

            while (reader.Read())
                collection.Add(MapToBenefitPlanDetail(reader));

            return collection;
        }

        protected BenefitPlanDetail MapToBenefitPlanDetail(IDataReader reader)
        {
            BenefitPlanDetail item = new BenefitPlanDetail();
            base.MapToBase(item, reader);

            item.BenefitPlanDetailId = (long)CleanDataValue(reader[ColumnNames.BenefitPlanDetailId]);
            item.BenefitPlanId = (long)CleanDataValue(reader[ColumnNames.BenefitPlanId]);
            item.EffectiveDate = (DateTime?)CleanDataValue(reader[ColumnNames.EffectiveDate]);
            item.BenefitPlanTypeCode = (String)CleanDataValue(reader[ColumnNames.BenefitPlanTypeCode]);
            item.BenefitPlanStatusCode = (String)CleanDataValue(reader[ColumnNames.BenefitPlanStatusCode]);
            item.VendorId = (long?)CleanDataValue(reader[ColumnNames.VendorId]);
            item.EmployeePaycodeCode = (String)CleanDataValue(reader[ColumnNames.EmployeePaycodeCode]);
            item.EmployerPaycodeCode = (String)CleanDataValue(reader[ColumnNames.EmployerPaycodeCode]);

            return item;
        }
        #endregion
    }
}
