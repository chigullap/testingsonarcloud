﻿using System;
using System.Data;
using System.Data.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerCodeWsibEffective : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String CodeWsibEffectiveId = "code_wsib_effective_id";
            public static String WsibCode = "code_wsib_cd";
            public static String EffectiveDate = "effective_date";
            public static String WorkersCompensationRate = "workers_compensation_rate";
            public static String EstimatedValue = "estimated_value";
            public static String MaximumAssessableEarning = "maximum_assessable_earning";
        }
        #endregion

        #region select
        internal CodeWsibEffectiveCollection Select(DatabaseUser user, String wsibCode)
        {
            DataBaseCommand command = GetStoredProcCommand("WsibEffectiveCode_select", user.DatabaseName);

            command.AddParameterWithValue("@wsibCode", wsibCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCodeWsibEffectiveCollection(reader);
        }
        #endregion

        #region insert
        public CodeWsibEffective Insert(DatabaseUser user, CodeWsibEffective codeWsibEffective)
        {
            using (DataBaseCommand command = GetStoredProcCommand("WsibEffectiveCode_insert", user.DatabaseName))
            {
                command.BeginTransaction("WsibEffectiveCode_insert");

                SqlParameter wsibCodeEffectiveParm = command.AddParameterWithValue("@codeWsibEffectiveId", codeWsibEffective.CodeWsibEffectiveId, ParameterDirection.InputOutput);

                command.AddParameterWithValue("@wsibCode", codeWsibEffective.WsibCode);
                command.AddParameterWithValue("@effectiveDate", codeWsibEffective.EffectiveDate);
                command.AddParameterWithValue("@workersCompensationRate", codeWsibEffective.WorkersCompensationRate);
                command.AddParameterWithValue("@estimatedValue", codeWsibEffective.EstimatedValue);
                command.AddParameterWithValue("@maximumAssessableEarning", codeWsibEffective.MaximumAssessableEarning);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", codeWsibEffective.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                codeWsibEffective.CodeWsibEffectiveId = (long)wsibCodeEffectiveParm.Value;
                codeWsibEffective.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return codeWsibEffective;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, CodeWsibEffective codeWsibEffective)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("WsibEffectiveCode_update", user.DatabaseName))
                {
                    command.BeginTransaction("WsibEffectiveCode_update");

                    command.AddParameterWithValue("@codeWsibEffectiveId", codeWsibEffective.CodeWsibEffectiveId);
                    command.AddParameterWithValue("@wsibCode", codeWsibEffective.WsibCode);
                    command.AddParameterWithValue("@effectiveDate", codeWsibEffective.EffectiveDate);
                    command.AddParameterWithValue("@workersCompensationRate", codeWsibEffective.WorkersCompensationRate);
                    command.AddParameterWithValue("@estimatedValue", codeWsibEffective.EstimatedValue);
                    command.AddParameterWithValue("@maximumAssessableEarning", codeWsibEffective.MaximumAssessableEarning);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", codeWsibEffective.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    codeWsibEffective.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, CodeWsibEffective codeWsibEffective)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("WsibEffectiveCode_delete", user.DatabaseName))
                {
                    command.BeginTransaction("WsibEffectiveCode_delete");

                    command.AddParameterWithValue("@codeWsibEffectiveId", codeWsibEffective.CodeWsibEffectiveId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected CodeWsibEffectiveCollection MapToCodeWsibEffectiveCollection(IDataReader reader)
        {
            CodeWsibEffectiveCollection collection = new CodeWsibEffectiveCollection();

            while (reader.Read())
                collection.Add(MapToCodeWsibEffective(reader));

            return collection;
        }

        protected CodeWsibEffective MapToCodeWsibEffective(IDataReader reader)
        {
            CodeWsibEffective item = new CodeWsibEffective();
            base.MapToBase(item, reader);

            item.CodeWsibEffectiveId = (long)CleanDataValue(reader[ColumnNames.CodeWsibEffectiveId]);
            item.WsibCode = (String)CleanDataValue(reader[ColumnNames.WsibCode]);
            item.EffectiveDate = (DateTime)CleanDataValue(reader[ColumnNames.EffectiveDate]);
            item.WorkersCompensationRate = (Decimal)CleanDataValue(reader[ColumnNames.WorkersCompensationRate]);
            item.EstimatedValue = (Decimal?)CleanDataValue(reader[ColumnNames.EstimatedValue]);
            item.MaximumAssessableEarning = (Decimal?)CleanDataValue(reader[ColumnNames.MaximumAssessableEarning]);

            return item;
        }
        #endregion
    }
}