﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerEmployeeEmploymentEquity : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeEmploymentEquityId = "employee_employment_equity_id";
            public static String EmployeeId = "employee_id";
            public static String EmploymentEquityCode = "code_employment_equity_cd";
        }
        #endregion

        #region select
        internal EmployeeEmploymentEquityCollection Select(DatabaseUser user, long employeeId)
        {
            DataBaseCommand command = GetStoredProcCommand("EmployeeEmploymentEquity_select", user.DatabaseName);

            command.AddParameterWithValue("@employeeId", employeeId);

            using (IDataReader reader = command.ExecuteReader())
                return MapToEmployeeEmploymentEquityCollection(reader);
        }
        #endregion

        #region insert
        public EmployeeEmploymentEquity Insert(DatabaseUser user, EmployeeEmploymentEquity item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeEmploymentEquity_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeEmploymentEquity_insert");

                SqlParameter employeeEmploymentEquityIdParm = command.AddParameterWithValue("@employeeEmploymentEquityId", item.EmployeeEmploymentEquityId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", item.EmployeeId);
                command.AddParameterWithValue("@employmentEquityCode", item.EmploymentEquityCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.EmployeeId = Convert.ToInt64(employeeEmploymentEquityIdParm.Value);
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, EmployeeEmploymentEquity item)
        {
            try
            {
                using (StoredProcedureCommand command = GetStoredProcCommand("EmployeeEmploymentEquity_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeEmploymentEquity_update");

                    command.AddParameterWithValue("@employeeEmploymentEquityId", item.EmployeeEmploymentEquityId);
                    command.AddParameterWithValue("@employeeId", item.EmployeeId);
                    command.AddParameterWithValue("@employmentEquityCode", item.EmploymentEquityCode);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, long employeeId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeEmploymentEquity_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeEmploymentEquity_delete");

                    command.AddParameterWithValue("@employeeId", employeeId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeEmploymentEquityCollection MapToEmployeeEmploymentEquityCollection(IDataReader reader)
        {
            EmployeeEmploymentEquityCollection collection = new EmployeeEmploymentEquityCollection();

            while (reader.Read())
                collection.Add(MapToEmployeeEmploymentEquity(reader));

            return collection;
        }
        protected EmployeeEmploymentEquity MapToEmployeeEmploymentEquity(IDataReader reader)
        {
            EmployeeEmploymentEquity item = new EmployeeEmploymentEquity();
            base.MapToBase(item, reader);

            item.EmployeeEmploymentEquityId = (long)CleanDataValue(reader[ColumnNames.EmployeeEmploymentEquityId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EmploymentEquityCode = (String)CleanDataValue(reader[ColumnNames.EmploymentEquityCode]);

            return item;
        }
        #endregion
    }
}