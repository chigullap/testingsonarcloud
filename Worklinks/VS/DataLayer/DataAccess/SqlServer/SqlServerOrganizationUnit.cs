﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerOrganizationUnit : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String OrganizationUnitId = "organization_unit_id";
            public static String HasChildrenFlag = "has_children_flag";
            public static String ActiveFlag = "active_flag";
            public static String OrganizationUnitLevelId = "organization_unit_level_id";
            public static String LanguageCode = "code_language_cd";
            public static String Description = "Description";
            public static String IsAssociatedToLevelFlag = "is_associate_to_level_flag";
            public static String ParentOrganizationUnitId = "parent_organization_unit_id";
            public static String GeneralLedgerSegment = "general_ledger_segment";
            public static String ImportExternalIndentifier = "import_external_identifier";
        }
        #endregion

        #region select
        internal OrganizationUnitCollection Select(DatabaseUser user, long? organizationUnitId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnit_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@organizationUnitId", organizationUnitId);
                command.AddParameterWithValue("@languageCode", user.LanguageCode);

                OrganizationUnitCollection collection = null;

                using (IDataReader reader = command.ExecuteReader())
                    collection = MapToCollection(reader);

                return collection;
            }
        }
        internal OrganizationUnitCollection SelectByParentOrganizationUnitId(DatabaseUser user, long? parentOrganizationUnitId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnit_selectByParentOrganizationUnitId", user.DatabaseName))
            {
                command.AddParameterWithValue("@parentOrganizationUnitId", parentOrganizationUnitId);
                command.AddParameterWithValue("@languageCode", user.LanguageCode);

                OrganizationUnitCollection collection = null;

                using (IDataReader reader = command.ExecuteReader())
                    collection = MapToCollection(reader);

                return collection;
            }
        }
        internal OrganizationUnitCollection SelectByOrganizationUnitLevelId(DatabaseUser user, OrganizationUnitCriteria criteria)
        {
            using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnit_selectByOrganizationUnitLevelId", user.DatabaseName))
            {
                command.AddParameterWithValue("@organizationUnitLevelId", criteria.OrganizationUnitLevelId);
                command.AddParameterWithValue("@languageCode", user.LanguageCode);

                OrganizationUnitCollection collection = null;

                using (IDataReader reader = command.ExecuteReader())
                    collection = MapToCollection(reader);

                return collection;
            }
        }
        internal OrganizationUnitCollection SelectWithLevelDescription(DatabaseUser user, long? organizationUnitId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnit_selectWithLevelDescription", user.DatabaseName))
            {
                command.AddParameterWithValue("@organizationUnitId", organizationUnitId);
                command.AddParameterWithValue("@languageCode", user.LanguageCode);

                OrganizationUnitCollection collection = null;

                using (IDataReader reader = command.ExecuteReader())
                    collection = MapToCollection(reader);

                return collection;
            }
        }
        #endregion

        #region insert
        public OrganizationUnit Insert(DatabaseUser user, OrganizationUnit data)
        {
            OrganizationUnit organizationUnit = new OrganizationUnit();

            using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnit_insert", user.DatabaseName))
            {
                try
                {
                    command.BeginTransaction("OrganizationUnit_insert");

                    SqlParameter organizationUnitIdParm = command.AddParameterWithValue("@organizationUnitId", data.OrganizationUnitId, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@organizationUnitLevelId", data.OrganizationUnitLevelId);
                    command.AddParameterWithValue("@activeFlag", data.ActiveFlag);
                    command.AddParameterWithValue("@generalLedgerSegment", data.GeneralLedgerSegment);
                    command.AddParameterWithValue("@importExternalIdentifier", data.ImportExternalIdentifier);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", data.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    data.RowVersion = (byte[])rowVersionParm.Value;
                    data.OrganizationUnitId = (long)organizationUnitIdParm.Value;

                    command.CommitTransaction();

                    organizationUnit = data;
                }
                catch (Exception exc)
                {
                    command.RollbackTransaction();
                    HandleException(exc, ((SqlException)exc).Number);
                }

                return organizationUnit;
            }
        }
        #endregion

        #region update
        public void Update(DatabaseUser user, OrganizationUnit data)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnit_update", user.DatabaseName))
                {
                    command.BeginTransaction("OrganizationUnit_update");

                    command.AddParameterWithValue("@organizationUnitId", data.OrganizationUnitId);
                    command.AddParameterWithValue("@activeFlag", data.ActiveFlag);
                    command.AddParameterWithValue("@generalLedgerSegment", data.GeneralLedgerSegment);
                    command.AddParameterWithValue("@importExternalIdentifier", data.ImportExternalIdentifier);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", data.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    data.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(DatabaseUser user, OrganizationUnit data)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("OrganizationUnit_delete", user.DatabaseName))
                {

                    command.BeginTransaction("OrganizationUnit_delete");

                    command.AddParameterWithValue("@organizationUnitId", data.OrganizationUnitId);
                    command.AddParameterWithValue("@rowVersion", data.RowVersion);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected OrganizationUnitCollection MapToCollection(IDataReader reader)
        {
            OrganizationUnitCollection collection = new OrganizationUnitCollection();

            while (reader.Read())
                collection.Add(MapToItem(reader));

            return collection;
        }
        protected OrganizationUnit MapToItem(IDataReader reader)
        {
            OrganizationUnit item = new OrganizationUnit();
            base.MapToBase(item, reader);

            item.OrganizationUnitId = (long)CleanDataValue(reader[ColumnNames.OrganizationUnitId]);

            if (ColumnExists(reader, ColumnNames.HasChildrenFlag))
                item.HasChildrenFlag = (bool)CleanDataValue(reader[ColumnNames.HasChildrenFlag]);

            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.OrganizationUnitLevelId = (long)CleanDataValue(reader[ColumnNames.OrganizationUnitLevelId]);
            item.LanguageCode = (String)CleanDataValue(reader[ColumnNames.LanguageCode]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            item.IsAssociatedToLevelFlag = (int)CleanDataValue(reader[ColumnNames.IsAssociatedToLevelFlag]);
            item.ParentOrganizationUnitId = (long?)CleanDataValue(reader[ColumnNames.ParentOrganizationUnitId]);
            item.GeneralLedgerSegment = (String)CleanDataValue(reader[ColumnNames.GeneralLedgerSegment]);
            if (ColumnExists(reader, ColumnNames.ImportExternalIndentifier))
                item.ImportExternalIdentifier = (String)CleanDataValue(reader[ColumnNames.ImportExternalIndentifier]);

            return item;
        }
        #endregion
    }
}