﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerPersonAddressTypeCodeTable : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String CodeTableId = "cd";
            public static String ActiveFlag = "active_flag";
            public static String SystemFlag = "system_flag";
            public static String SortOrder = "sort_order";
            public static String StartDate = "start_date";
            public static String EndDate = "end_date";
            public static String ImportExternalIdentifier = "import_external_identifier";
            public static String Priority = "priority";
        }
        #endregion


        #region main
        internal PersonAddressTypeCodeTableCollection Select(DatabaseUser user)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PersonAddressTypeCodeTable_select", user.DatabaseName))
            {
                PersonAddressTypeCodeTableCollection personAddressTypeCodeTableRows = null;

                using (IDataReader reader = command.ExecuteReader())
                {
                    personAddressTypeCodeTableRows = MapToPersonAddressTypeCodeTableCollection(reader);
                }

                return personAddressTypeCodeTableRows;
            }
        }

        internal PersonAddressTypeCodeTable SelectByPriority(DatabaseUser user, short priority)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PersonAddressTypeCodeTable_selectByPriority", user.DatabaseName))
            {
                command.AddParameterWithValue("@priority", priority);

                PersonAddressTypeCodeTable personAddressTypeCodeTable = null;

                using (IDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        personAddressTypeCodeTable = MapToPersonAddressTypeCodeTable(reader);
                    }
                }

                return personAddressTypeCodeTable;
            }
        }

        internal PersonAddressTypeCodeTable Update(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTable)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PersonAddressTypeCodeTable_update", user.DatabaseName))
                {
                    command.BeginTransaction("PersonAddressTypeCodeTable_update");

                    command.AddParameterWithValue("@code", personAddressTypeCodeTable.CodeTableId);
                    command.AddParameterWithValue("@sortOrder", personAddressTypeCodeTable.SortOrder);
                    command.AddParameterWithValue("@startDate", personAddressTypeCodeTable.StartDate);
                    command.AddParameterWithValue("@endDate", personAddressTypeCodeTable.EndDate);
                    command.AddParameterWithValue("@activeFlag", personAddressTypeCodeTable.ActiveFlag);
                    command.AddParameterWithValue("@systemFlag", personAddressTypeCodeTable.SystemFlag);
                    command.AddParameterWithValue("@importExternalIdentifier", personAddressTypeCodeTable.ImportExternalIdentifier);
                    command.AddParameterWithValue("@priority", personAddressTypeCodeTable.Priority);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", personAddressTypeCodeTable.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    personAddressTypeCodeTable.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();

                    return personAddressTypeCodeTable;
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
                return null;
            }
        }

        internal PersonAddressTypeCodeTable Insert(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTable)
        {
            using (DataBaseCommand command = GetStoredProcCommand("PersonAddressTypeCodeTable_insert", user.DatabaseName))
            {
                command.BeginTransaction("PersonAddressTypeCodeTable_insert");

                command.AddParameterWithValue("@code", personAddressTypeCodeTable.CodeTableId);
                command.AddParameterWithValue("@activeFlag", personAddressTypeCodeTable.ActiveFlag);
                command.AddParameterWithValue("@systemFlag", personAddressTypeCodeTable.SystemFlag);
                command.AddParameterWithValue("@sortOrder", personAddressTypeCodeTable.SortOrder);
                command.AddParameterWithValue("@startDate", personAddressTypeCodeTable.StartDate);
                command.AddParameterWithValue("@endDate", personAddressTypeCodeTable.EndDate);
                command.AddParameterWithValue("@importExternalIdentifier", personAddressTypeCodeTable.ImportExternalIdentifier);
                command.AddParameterWithValue("@priority", personAddressTypeCodeTable.Priority);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", personAddressTypeCodeTable.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                personAddressTypeCodeTable.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();

                return personAddressTypeCodeTable;
            }
        }

        internal void Delete(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTable)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PersonAddressTypeCodeTable_delete", user.DatabaseName))
                {
                    command.BeginTransaction("PersonAddressTypeCodeTable_delete");
                    command.AddParameterWithValue("@code", personAddressTypeCodeTable.CodeTableId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        #endregion

        #region data mapping
        protected PersonAddressTypeCodeTableCollection MapToPersonAddressTypeCodeTableCollection(IDataReader reader)
        {
            PersonAddressTypeCodeTableCollection collection = new PersonAddressTypeCodeTableCollection();
            while (reader.Read())
            {
                collection.Add(MapToPersonAddressTypeCodeTable(reader));
            }

            return collection;
        }

        protected PersonAddressTypeCodeTable MapToPersonAddressTypeCodeTable(IDataReader reader)
        {
            PersonAddressTypeCodeTable codeTable = new PersonAddressTypeCodeTable();
            base.MapToBase(codeTable, reader);
            codeTable.CodeTableId = (string)CleanDataValue(reader[ColumnNames.CodeTableId]);
            codeTable.ParentCodeTableId = null; 
            codeTable.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            codeTable.SystemFlag = (bool)CleanDataValue(reader[ColumnNames.SystemFlag]);
            codeTable.SortOrder = Convert.ToInt64(CleanDataValue(reader[ColumnNames.SortOrder]));
            codeTable.StartDate = (DateTime?)CleanDataValue(reader[ColumnNames.StartDate]);
            codeTable.EndDate = (DateTime?)CleanDataValue(reader[ColumnNames.EndDate]);
            codeTable.ImportExternalIdentifier = (String)CleanDataValue(reader[ColumnNames.ImportExternalIdentifier]);
            codeTable.Priority = Convert.ToInt64(CleanDataValue(reader[ColumnNames.Priority]));

            return codeTable;
        }

        #endregion
    }
}
