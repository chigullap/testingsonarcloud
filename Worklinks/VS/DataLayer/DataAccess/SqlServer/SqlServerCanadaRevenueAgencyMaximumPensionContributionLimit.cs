﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Data;
//using System.Data.SqlClient;

//using WorkLinks.BusinessLayer.BusinessObjects;
//using WLP.DataLayer.SqlClient;

//namespace WorkLinks.DataLayer.DataAccess.SqlServer
//{
//    internal class SqlServerCanadaRevenueAgencyMaximumPensionContributionLimit : SqlServerBase
//    {
//        #region column names
//        protected new class ColumnNames : SqlServerBase.ColumnNames
//        {
//            public static String CanadaRevenueAgencyMaximumPensionContributionLimitId = "canada_revenue_agency_maximum_pension_contribution_limit_id";
//            public static String Year = "year";
//            public static String Amount = "amount";
//        }
//        #endregion

//        #region main
//        internal CanadaRevenueAgencyMaximumPensionContributionLimitCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, decimal year)
//        {
//            DataBaseCommand command = GetStoredProcCommand("CanadaRevenueAgencyMaximumPensionContributionLimit_select", user.DatabaseName);
//            command.AddParameterWithValue("@year", year);

//            using (IDataReader reader = command.ExecuteReader())
//            {
//                return MapToCanadaRevenueAgencyMaximumPensionContributionLimitCollection(reader);
//            }
//        }
        
//        #endregion

//        #region data mapping
//        protected CanadaRevenueAgencyMaximumPensionContributionLimitCollection MapToCanadaRevenueAgencyMaximumPensionContributionLimitCollection(IDataReader reader)
//        {
//            CanadaRevenueAgencyMaximumPensionContributionLimitCollection collection = new CanadaRevenueAgencyMaximumPensionContributionLimitCollection();
//            while (reader.Read())
//            {
//                collection.Add(MapToCanadaRevenueAgencyMaximumPensionContributionLimit(reader));
//            }

//            return collection;
//        }
//        protected CanadaRevenueAgencyMaximumPensionContributionLimit MapToCanadaRevenueAgencyMaximumPensionContributionLimit(IDataReader reader)
//        {
//            CanadaRevenueAgencyMaximumPensionContributionLimit item = new CanadaRevenueAgencyMaximumPensionContributionLimit();
//            base.MapToBase(item, reader);

//            item.CanadaRevenueAgencyMaximumPensionContributionLimitId = (long)CleanDataValue(reader[ColumnNames.CanadaRevenueAgencyMaximumPensionContributionLimitId]);
//            item.Year = (Decimal)CleanDataValue(reader[ColumnNames.Year]);
//            item.Amount = (Decimal)CleanDataValue(reader[ColumnNames.Amount]);

//            return item;
//        }
//        #endregion
//    }
//}
