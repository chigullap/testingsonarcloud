﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeeAwardHonour : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeeAwardHonourId = "award_honour_id";
            public static String EmployeeId = "employee_id";
            public static String EmployeeAwardHonourTypeCode = "code_award_honour_type_cd";
            public static String Grantor = "grantor";
            public static String IssueDate = "issue_date";
            public static String AttachmentId = "attachment_id";
            public static String Notes = "notes";
        }
        #endregion

        #region main
        internal EmployeeAwardHonourCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeeId)
        {
            return Select(user, employeeId, null);
        }

        internal EmployeeAwardHonourCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long employeeId, long? employeeAwardHonourId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeAwardHonour_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@employeeId", employeeId);
                command.AddParameterWithValue("@employeeAwardHonourId", employeeAwardHonourId);
                command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
                command.AddParameterWithValue("@securityUserId", user.SecurityUserId);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToEmployeeAwardHonourCollection(reader);
                }
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeAwardHonour awardHonour)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeAwardHonour_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeAwardHonour_update");

                    command.AddParameterWithValue("@employeeAwardHonourId", awardHonour.EmployeeAwardHonourId);
                    command.AddParameterWithValue("@employeeId", awardHonour.EmployeeId);
                    command.AddParameterWithValue("@codeEmployeeAwardHonourTypeCd", awardHonour.EmployeeAwardHonourTypeCode);
                    command.AddParameterWithValue("@grantor", awardHonour.Grantor);
                    command.AddParameterWithValue("@issueDate", awardHonour.IssueDate);
                    command.AddParameterWithValue("@attachmentId", awardHonour.AttachmentId);
                    command.AddParameterWithValue("@notes", awardHonour.Notes);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", awardHonour.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    awardHonour.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public EmployeeAwardHonour Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeAwardHonour awardHonour)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeeAwardHonour_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeeAwardHonour_insert");

                SqlParameter employeeAwardHonourIdParm = command.AddParameterWithValue("@employeeAwardHonourId", awardHonour.EmployeeAwardHonourId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", awardHonour.EmployeeId);
                command.AddParameterWithValue("@codeEmployeeAwardHonourTypeCd", awardHonour.EmployeeAwardHonourTypeCode);
                command.AddParameterWithValue("@grantor", awardHonour.Grantor);
                command.AddParameterWithValue("@issueDate", awardHonour.IssueDate);
                command.AddParameterWithValue("@attachmentId", awardHonour.AttachmentId);
                command.AddParameterWithValue("@notes", awardHonour.Notes);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", awardHonour.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@createUser", user.UserName);
                command.AddParameterWithValue("@createDatetime", Time);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                awardHonour.EmployeeAwardHonourId = Convert.ToInt64(employeeAwardHonourIdParm.Value);
                awardHonour.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();

                return awardHonour;
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeeAwardHonour awardHonour)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeeAwardHonour_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeeAwardHonour_delete");
                    command.AddParameterWithValue("@employeeAwardHonourId", awardHonour.EmployeeAwardHonourId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected EmployeeAwardHonourCollection MapToEmployeeAwardHonourCollection(IDataReader reader)
        {
            EmployeeAwardHonourCollection collection = new EmployeeAwardHonourCollection();
            while (reader.Read())
            {
                collection.Add(MapToEmployeeAwardHonour(reader));
            }

            return collection;
        }

        protected EmployeeAwardHonour MapToEmployeeAwardHonour(IDataReader reader)
        {
            EmployeeAwardHonour awardHonour = new EmployeeAwardHonour();
            base.MapToBase(awardHonour, reader);

            awardHonour.EmployeeAwardHonourId = (long)CleanDataValue(reader[ColumnNames.EmployeeAwardHonourId]);
            awardHonour.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            awardHonour.EmployeeAwardHonourTypeCode = (String)CleanDataValue(reader[ColumnNames.EmployeeAwardHonourTypeCode]);
            awardHonour.Grantor = (String)CleanDataValue(reader[ColumnNames.Grantor]);
            awardHonour.IssueDate = (DateTime?)CleanDataValue(reader[ColumnNames.IssueDate]);
            awardHonour.AttachmentId = (long?)CleanDataValue(reader[ColumnNames.AttachmentId]);
            awardHonour.Notes = (String)CleanDataValue(reader[ColumnNames.Notes]);

            return awardHonour;
        }
        #endregion
    }
}