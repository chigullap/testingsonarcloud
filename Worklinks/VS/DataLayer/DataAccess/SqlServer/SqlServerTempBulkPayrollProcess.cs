﻿using System;
using System.Data;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerTempBulkPayrollProcess : SqlServerBase
    {
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PayrollPeriodId = "payroll_period_id";
            public static String PayrollProcessGroupCode = "code_payroll_process_group_cd";
            public static String EmployeeCount = "employee_count";
            public static String TransactionCount = "transaction_count";
            public static String StatutoryDeductionCount = "statutory_deduction_count";
            public static String PayrollProcessStatusCode = "code_payroll_process_status_cd";
        }
        internal TempBulkPayrollProcessCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user)
        {
            DataBaseCommand command = GetStoredProcCommand("TemporaryBulkPayrollProcess_select", user.DatabaseName);

            using (IDataReader reader = command.ExecuteReader())
                return MapToTempBulkPayrollProcessCollection(reader);
        }
        protected TempBulkPayrollProcessCollection MapToTempBulkPayrollProcessCollection(IDataReader reader)
        {
            TempBulkPayrollProcessCollection collection = new TempBulkPayrollProcessCollection();

            while (reader.Read())
                collection.Add(MapToTempBulkPayrollProcess(reader));

            return collection;
        }
        protected TempBulkPayrollProcess MapToTempBulkPayrollProcess(IDataReader reader)
        {
            TempBulkPayrollProcess item = new TempBulkPayrollProcess();
            //base.MapToBase(item, reader);

            item.PayrollPeriodId = (long)CleanDataValue(reader[ColumnNames.PayrollPeriodId]);
            item.PayrollProcessGroupCode = (String)CleanDataValue(reader[ColumnNames.PayrollProcessGroupCode]);
            item.EmployeeCount = (int)CleanDataValue(reader[ColumnNames.EmployeeCount]);
            item.TransactionCount = (int)CleanDataValue(reader[ColumnNames.TransactionCount]);
            item.StatutoryDeductionCount = (int)CleanDataValue(reader[ColumnNames.StatutoryDeductionCount]);
            item.PayrollProcessStatusCode = (String)CleanDataValue(reader[ColumnNames.PayrollProcessStatusCode]);

            return item;
        }
    }
}