﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerEmployeePaycode : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String EmployeePaycodeId = "employee_paycode_id";
            public static String EmployeeId = "employee_id";
            public static String PaycodeCode = "code_paycode_cd";
            public static String Description = "description";
            public static String AmountRate = "amount_rate";
            public static String EmploymentInsuranceInsurableHoursPerUnit = "employment_insurance_insurable_hours_per_unit";
            public static String StartDate = "start_date";
            public static String CutoffDate = "cutoff_date";
            public static String CodeCalculateRateFromCd = "code_calculate_rate_from_cd";
            public static String PayPeriodMinimum = "pay_period_minimum";
            public static String PayPeriodMaximum = "pay_period_maximum";
            public static String MonthlyMaximum = "monthly_maximum";
            public static String YearlyMaximum = "yearly_maximum";
            public static String AmountUnits = "amount_units";
            public static String AmountPercentage = "amount_percentage";
            public static String ExcludeAmount = "exclude_amount";
            public static String ExcludePercentage = "exclude_percentage";
            public static String GroupIncomeFactor = "group_income_factor";
            public static String RoundUpTo = "round_up_to";
            public static String RatePerRoundUpTo = "rate_per_round_up_to";
            public static String ExemptAmount = "exempt_amount";
            public static String ExemptAmountCodeNetGrossCd = "exempt_amount_code_net_gross_cd";
            public static String ExemptAmountCodeBeforeAfterCd = "exempt_amount_code_before_after_cd";
            public static String ExemptPercentage = "exempt_percentage";
            public static String ExemptPercentageCodeNetGrossCd = "exempt_percentage_code_net_gross_cd";
            public static String ExemptPercentageCodeBeforeAfterCd = "exempt_percentage_code_before_after_cd";
            public static String PaycodeTypeCode = "code_paycode_type_cd";
            public static String SharedCount = "shared_count";
            public static String GarnishmentVendorId = "garnishment_vendor_id";
            public static String GarnishmentOrderNumber = "garnishment_order_number";
            public static String OrganizationUnit = "organization_unit";
            public static String OrganizationUnitDescription = "organization_unit_description";
            public static String EligibleByStatus = "eligible_by_status";
            public static String GlobalEmployeePaycodeFlag = "global_employee_paycode_flag";
            public static String ReportGroupCodePaycodeTypeCd = "report_group_code_paycode_type_cd";
            public static String ReportDisplayUnitFlag = "report_display_unit_flag";
            public static String GarnishmentFlag = "garnishment_flag";
            public static String VacationCalculationOverrideFlag = "vacation_calculation_override_flag";
        }
        #endregion

        #region select
        internal EmployeePaycodeCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? employeeId, bool securityOverrideFlag, bool useGlobalPaycode)
        {
            return Select(user, employeeId, null, null, securityOverrideFlag, useGlobalPaycode);
        }
        internal EmployeePaycodeCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? employeeId, long? employeePaycodeId, String PaycodeCode, bool securityOverrideFlag, bool useGlobalPaycode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePaycode_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@securityOverride", securityOverrideFlag);
                command.AddParameterWithValue("@employeeId", employeeId);
                command.AddParameterWithValue("@employeePaycodeId", employeePaycodeId);
                command.AddParameterWithValue("@codePaycodeCd", PaycodeCode);
                command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
                command.AddParameterWithValue("@securityUserId", user.SecurityUserId);
                command.AddParameterWithValue("@useGlobalPaycode", useGlobalPaycode);

                using (IDataReader reader = command.ExecuteReader())
                    return MapToEmployeePaycodeCollection(reader);
            }
        }
        #endregion

        #region insert
        public EmployeePaycode Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeePaycode employeePaycode)
        {
            using (DataBaseCommand command = GetStoredProcCommand("EmployeePaycode_insert", user.DatabaseName))
            {
                command.BeginTransaction("EmployeePaycode_insert");

                SqlParameter employeePaycodeIdParm = command.AddParameterWithValue("@employeePaycodeId", employeePaycode.EmployeePaycodeId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", employeePaycode.EmployeeId);
                command.AddParameterWithValue("@codePaycodeCd", employeePaycode.PaycodeCode);
                command.AddParameterWithValue("@amountRate", employeePaycode.AmountRate);
                command.AddParameterWithValue("@employmentInsuranceInsurableHoursPerUnit", employeePaycode.EmploymentInsuranceInsurableHoursPerUnit);
                command.AddParameterWithValue("@startDate", employeePaycode.StartDate);
                command.AddParameterWithValue("@cutoffDate", employeePaycode.CutoffDate);
                command.AddParameterWithValue("@payPeriodMinimum", employeePaycode.PayPeriodMinimum);
                command.AddParameterWithValue("@payPeriodMaximum", employeePaycode.PayPeriodMaximum);
                command.AddParameterWithValue("@monthlyMaximum", employeePaycode.MonthlyMaximum);
                command.AddParameterWithValue("@yearlyMaximum", employeePaycode.YearlyMaximum);
                command.AddParameterWithValue("@amountUnits", employeePaycode.AmountUnits);
                command.AddParameterWithValue("@amountPercentage", employeePaycode.AmountPercentage);
                command.AddParameterWithValue("@excludeAmount", employeePaycode.ExcludeAmount);
                command.AddParameterWithValue("@excludePercentage", employeePaycode.ExcludePercentage);
                command.AddParameterWithValue("@groupIncomeFactor", employeePaycode.GroupIncomeFactor);
                command.AddParameterWithValue("@roundUpTo", employeePaycode.RoundUpTo);
                command.AddParameterWithValue("@ratePerRoundUpTo", employeePaycode.RatePerRoundUpTo);
                command.AddParameterWithValue("@exemptAmount", employeePaycode.ExemptAmount);
                command.AddParameterWithValue("@exemptAmountCodeNetGrossCd", employeePaycode.ExemptAmountCodeNetGrossCd);
                command.AddParameterWithValue("@exemptAmountCodeBeforeAfterCd", employeePaycode.ExemptAmountCodeBeforeAfterCd);
                command.AddParameterWithValue("@exemptPercentage", employeePaycode.ExemptPercentage);
                command.AddParameterWithValue("@exemptPercentageCodeNetGrossCd", employeePaycode.ExemptPercentageCodeNetGrossCd);
                command.AddParameterWithValue("@exemptPercentageCodeBeforeAfterCd", employeePaycode.ExemptPercentageCodeBeforeAfterCd);
                command.AddParameterWithValue("@garnishmentVendorId", employeePaycode.GarnishmentVendorId);
                command.AddParameterWithValue("@garnishmentOrderNumber", employeePaycode.GarnishmentOrderNumber);
                command.AddParameterWithValue("@organizationUnit", employeePaycode.OrganizationUnit);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", employeePaycode.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@createUser", user.UserName);
                command.AddParameterWithValue("@createDatetime", Time);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                employeePaycode.EmployeePaycodeId = Convert.ToInt64(employeePaycodeIdParm.Value);
                employeePaycode.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return employeePaycode;
            }
        }
        #endregion

        #region update
        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeePaycode employeePaycode)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePaycode_update", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePaycode_update");

                    command.AddParameterWithValue("@employeePaycodeId", employeePaycode.EmployeePaycodeId);
                    command.AddParameterWithValue("@employeeId", employeePaycode.EmployeeId);
                    command.AddParameterWithValue("@codePaycodeCd", employeePaycode.PaycodeCode);
                    command.AddParameterWithValue("@amountRate", employeePaycode.AmountRate);
                    command.AddParameterWithValue("@employmentInsuranceInsurableHoursPerUnit", employeePaycode.EmploymentInsuranceInsurableHoursPerUnit);
                    command.AddParameterWithValue("@startDate", employeePaycode.StartDate);
                    command.AddParameterWithValue("@cutoffDate", employeePaycode.CutoffDate);
                    command.AddParameterWithValue("@payPeriodMinimum", employeePaycode.PayPeriodMinimum);
                    command.AddParameterWithValue("@payPeriodMaximum", employeePaycode.PayPeriodMaximum);
                    command.AddParameterWithValue("@monthlyMaximum", employeePaycode.MonthlyMaximum);
                    command.AddParameterWithValue("@yearlyMaximum", employeePaycode.YearlyMaximum);
                    command.AddParameterWithValue("@amountUnits", employeePaycode.AmountUnits);
                    command.AddParameterWithValue("@amountPercentage", employeePaycode.AmountPercentage);
                    command.AddParameterWithValue("@excludeAmount", employeePaycode.ExcludeAmount);
                    command.AddParameterWithValue("@excludePercentage", employeePaycode.ExcludePercentage);
                    command.AddParameterWithValue("@groupIncomeFactor", employeePaycode.GroupIncomeFactor);
                    command.AddParameterWithValue("@roundUpTo", employeePaycode.RoundUpTo);
                    command.AddParameterWithValue("@ratePerRoundUpTo", employeePaycode.RatePerRoundUpTo);
                    command.AddParameterWithValue("@exemptAmount", employeePaycode.ExemptAmount);
                    command.AddParameterWithValue("@exemptAmountCodeNetGrossCd", employeePaycode.ExemptAmountCodeNetGrossCd);
                    command.AddParameterWithValue("@exemptAmountCodeBeforeAfterCd", employeePaycode.ExemptAmountCodeBeforeAfterCd);
                    command.AddParameterWithValue("@exemptPercentage", employeePaycode.ExemptPercentage);
                    command.AddParameterWithValue("@exemptPercentageCodeNetGrossCd", employeePaycode.ExemptPercentageCodeNetGrossCd);
                    command.AddParameterWithValue("@exemptPercentageCodeBeforeAfterCd", employeePaycode.ExemptPercentageCodeBeforeAfterCd);
                    command.AddParameterWithValue("@overrideConcurrencyCheck", employeePaycode.OverrideConcurrencyCheck);
                    command.AddParameterWithValue("@garnishmentVendorId", employeePaycode.GarnishmentVendorId);
                    command.AddParameterWithValue("@garnishmentOrderNumber", employeePaycode.GarnishmentOrderNumber);
                    command.AddParameterWithValue("@organizationUnit", employeePaycode.OrganizationUnit);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", employeePaycode.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    employeePaycode.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region delete
        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, EmployeePaycode employeePaycode)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("EmployeePaycode_delete", user.DatabaseName))
                {
                    command.BeginTransaction("EmployeePaycode_delete");

                    command.AddParameterWithValue("@employeePaycodeId", employeePaycode.EmployeePaycodeId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        public bool CheckRecurringIncomeCode(WLP.BusinessLayer.BusinessObjects.DatabaseUser user)
        {
            using (StoredProcedureCommand command = GetStoredProcCommand("RecurringIncomeCode_select", user.DatabaseName))
            {
                bool temp = false;
                SqlParameter returnValueParm = command.AddParameterWithValue("@returnValue", false, ParameterDirection.Output);

                command.ExecuteNonQuery();

                temp = (bool)returnValueParm.Value;

                return temp;
            }
        }

        #region data mapping
        protected EmployeePaycodeCollection MapToEmployeePaycodeCollection(IDataReader reader)
        {
            EmployeePaycodeCollection collection = new EmployeePaycodeCollection();

            while (reader.Read())
                collection.Add(MapToPaycode(reader));

            return collection;
        }
        protected EmployeePaycode MapToPaycode(IDataReader reader)
        {
            EmployeePaycode item = new EmployeePaycode();
            base.MapToBase(item, reader);

            item.EmployeePaycodeId = (long)CleanDataValue(reader[ColumnNames.EmployeePaycodeId]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.PaycodeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeCode]);
            item.AmountRate = (Decimal?)CleanDataValue(reader[ColumnNames.AmountRate]);
            item.EmploymentInsuranceInsurableHoursPerUnit = (Decimal?)CleanDataValue(reader[ColumnNames.EmploymentInsuranceInsurableHoursPerUnit]);
            item.StartDate = (DateTime?)CleanDataValue(reader[ColumnNames.StartDate]);
            item.CutoffDate = (DateTime?)CleanDataValue(reader[ColumnNames.CutoffDate]);
            item.PayPeriodMinimum = (Decimal?)CleanDataValue(reader[ColumnNames.PayPeriodMinimum]);
            item.PayPeriodMaximum = (Decimal?)CleanDataValue(reader[ColumnNames.PayPeriodMaximum]);
            item.MonthlyMaximum = (Decimal?)CleanDataValue(reader[ColumnNames.MonthlyMaximum]);
            item.YearlyMaximum = (Decimal?)CleanDataValue(reader[ColumnNames.YearlyMaximum]);
            item.PaycodeTypeCode = (String)CleanDataValue(reader[ColumnNames.PaycodeTypeCode]);
            item.AmountUnits = (Decimal?)CleanDataValue(reader[ColumnNames.AmountUnits]);
            item.AmountPercentage = (Decimal?)CleanDataValue(reader[ColumnNames.AmountPercentage]);
            item.ExcludeAmount = (Decimal?)CleanDataValue(reader[ColumnNames.ExcludeAmount]);
            item.ExcludePercentage = (Decimal?)CleanDataValue(reader[ColumnNames.ExcludePercentage]);
            item.GroupIncomeFactor = (Decimal?)CleanDataValue(reader[ColumnNames.GroupIncomeFactor]);
            item.RoundUpTo = (Decimal?)CleanDataValue(reader[ColumnNames.RoundUpTo]);
            item.RatePerRoundUpTo = (Decimal?)CleanDataValue(reader[ColumnNames.RatePerRoundUpTo]);
            item.ExemptAmount = (Decimal?)CleanDataValue(reader[ColumnNames.ExemptAmount]);
            item.ExemptAmountCodeNetGrossCd = (String)CleanDataValue(reader[ColumnNames.ExemptAmountCodeNetGrossCd]);
            item.ExemptAmountCodeBeforeAfterCd = (String)CleanDataValue(reader[ColumnNames.ExemptAmountCodeBeforeAfterCd]);
            item.ExemptPercentage = (Decimal?)CleanDataValue(reader[ColumnNames.ExemptPercentage]);
            item.ExemptPercentageCodeNetGrossCd = (String)CleanDataValue(reader[ColumnNames.ExemptPercentageCodeNetGrossCd]);
            item.ExemptPercentageCodeBeforeAfterCd = (String)CleanDataValue(reader[ColumnNames.ExemptPercentageCodeBeforeAfterCd]);
            item.GarnishmentVendorId = (long?)CleanDataValue(reader[ColumnNames.GarnishmentVendorId]);
            item.GarnishmentOrderNumber = (String)CleanDataValue(reader[ColumnNames.GarnishmentOrderNumber]);
            item.OrganizationUnit = (String)CleanDataValue(reader[ColumnNames.OrganizationUnit]);
            item.OrganizationUnitDescription = (String)CleanDataValue(reader[ColumnNames.OrganizationUnitDescription]);
            item.EligibleByStatus = Convert.ToBoolean(CleanDataValue(reader[ColumnNames.EligibleByStatus]));
            item.GlobalEmployeePaycodeFlag = (bool)CleanDataValue(reader[ColumnNames.GlobalEmployeePaycodeFlag]);
            item.ReportGroupCodePaycodeTypeCd = (String)CleanDataValue(reader[ColumnNames.ReportGroupCodePaycodeTypeCd]);
            item.ReportDisplayUnitFlag = (bool)CleanDataValue(reader[ColumnNames.ReportDisplayUnitFlag]);
            item.GarnishmentFlag = (bool)CleanDataValue(reader[ColumnNames.GarnishmentFlag]);
            item.VacationCalculationOverrideFlag = (bool)CleanDataValue(reader[ColumnNames.VacationCalculationOverrideFlag]);
    
            return item;
        }
        #endregion
    }
}