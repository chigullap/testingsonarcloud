﻿using System;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerCodeWcbChequeRemittanceFrequency : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String WcbChequeRemittanceFrequencyCode = "code_wcb_cheque_remittance_frequency_cd";
            public static String EnglishDescription = "english_description";
            public static String FrenchDescription = "french_description";
            public static String ImportExternalIdentifier = "import_external_identifier";
            public static String ActiveFlag = "active_flag";
            public static String HasChildrenFlag = "has_children_flag";
            public static String ScheduleType = "schedule_type";
        }
        #endregion

        #region main
        internal CodeWcbChequeRemittanceFrequencyCollection Select(DatabaseUser user, String wcbChequeRemittanceFrequencyCode)
        {
            DataBaseCommand command = GetStoredProcCommand("WcbChequeRemittanceFrequencyCode_select", user.DatabaseName);

            command.AddParameterWithValue("@wcbChequeRemittanceFrequencyCode", wcbChequeRemittanceFrequencyCode);

            using (IDataReader reader = command.ExecuteReader())
                return MapToCollection(reader);
        }
        public CodeWcbChequeRemittanceFrequency Insert(DatabaseUser user, CodeWcbChequeRemittanceFrequency item)
        {
            using (DataBaseCommand command = GetStoredProcCommand("WcbChequeRemittanceFrequencyCode_insert", user.DatabaseName))
            {
                command.BeginTransaction("WcbChequeRemittanceFrequencyCode_insert");

                SqlParameter wcbChequeRemittanceFrequencyCodeParm = command.AddParameterWithValue("@wcbChequeRemittanceFrequencyCode", item.WcbChequeRemittanceFrequencyCode, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@englishDescription", item.EnglishDescription);
                command.AddParameterWithValue("@frenchDescription", item.FrenchDescription);
                command.AddParameterWithValue("@importExternalIdentifier", item.ImportExternalIdentifier);
                command.AddParameterWithValue("@activeFlag", item.ActiveFlag);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();

                item.WcbChequeRemittanceFrequencyCode = (String)wcbChequeRemittanceFrequencyCodeParm.Value;
                item.RowVersion = (byte[])rowVersionParm.Value;

                command.CommitTransaction();

                return item;
            }
        }
        public void Update(DatabaseUser user, CodeWcbChequeRemittanceFrequency item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("WcbChequeRemittanceFrequencyCode_update", user.DatabaseName))
                {
                    command.BeginTransaction("WcbChequeRemittanceFrequencyCode_update");

                    command.AddParameterWithValue("@wcbChequeRemittanceFrequencyCode", item.WcbChequeRemittanceFrequencyCode);
                    command.AddParameterWithValue("@englishDescription", item.EnglishDescription);
                    command.AddParameterWithValue("@frenchDescription", item.FrenchDescription);
                    command.AddParameterWithValue("@importExternalIdentifier", item.ImportExternalIdentifier);
                    command.AddParameterWithValue("@activeFlag", item.ActiveFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", item.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();

                    item.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        public void Delete(DatabaseUser user, CodeWcbChequeRemittanceFrequency item)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("WcbChequeRemittanceFrequencyCode_delete", user.DatabaseName))
                {
                    command.BeginTransaction("WcbChequeRemittanceFrequencyCode_delete");

                    command.AddParameterWithValue("@wcbChequeRemittanceFrequencyCode", item.WcbChequeRemittanceFrequencyCode);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected CodeWcbChequeRemittanceFrequencyCollection MapToCollection(IDataReader reader)
        {
            CodeWcbChequeRemittanceFrequencyCollection collection = new CodeWcbChequeRemittanceFrequencyCollection();

            while (reader.Read())
                collection.Add(MapToItem(reader));

            return collection;
        }
        protected CodeWcbChequeRemittanceFrequency MapToItem(IDataReader reader)
        {
            CodeWcbChequeRemittanceFrequency item = new CodeWcbChequeRemittanceFrequency();
            base.MapToBase(item, reader);

            item.WcbChequeRemittanceFrequencyCode = (String)CleanDataValue(reader[ColumnNames.WcbChequeRemittanceFrequencyCode]);
            item.EnglishDescription = (String)CleanDataValue(reader[ColumnNames.EnglishDescription]);
            item.FrenchDescription = (String)CleanDataValue(reader[ColumnNames.FrenchDescription]);
            item.ImportExternalIdentifier = (String)CleanDataValue(reader[ColumnNames.ImportExternalIdentifier]);
            item.ActiveFlag = (bool)CleanDataValue(reader[ColumnNames.ActiveFlag]);
            item.HasChildrenFlag = (bool)CleanDataValue(reader[ColumnNames.HasChildrenFlag]);
            item.ScheduleType = (string)CleanDataValue(reader[ColumnNames.ScheduleType]);

            return item;
        }
        #endregion
    }
}