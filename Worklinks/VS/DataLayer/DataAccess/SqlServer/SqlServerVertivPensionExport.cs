﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerVertivPensionExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string EmployeeId = "employee_id";
            public static string EmployeeNumber = "employee_number";
            public static string EmployerRequiredContributionAmount1 = "employer_required_contribution_amount1";
            public static string MemberVoluntaryMatchedContributionAmount1 = "member_voluntary_matched_contribution_amount_1";
            public static string MemberVoluntaryUnmatchedContributionAmount1 = "member_voluntary_unmatched_contribution_amount_1";
            public static string EmployerVoluntaryMatchingContributionAmount1 = "employer_voluntary_matching_contribution_amount_1";
            public static string MemberVoluntaryMatchedContributionAmount2 = "member_voluntary_matched_contribution_amount_2";
            public static string MemberVoluntaryUnmatchedContributionAmount2 = "member_voluntary_unmatched_contribution_amount_2";
            public static string EmployerRequiredContributionAmount2 = "employer_required_contribution_amount_2";
            public static string MemberVoluntaryMatchedContributionAmount3 = "member_voluntary_matched_contribution_amount_3";
            public static string MemberVoluntaryUnmatchedContributionAmount3 = "member_voluntary_unmatched_contribution_amount_3";
            public static string EmployerVoluntaryMatchingContributionAmount2 = "employer_voluntary_matching_contribution_amount_2";
            public static string EmployerVoluntaryMatchingContributionAmount3 = "employer_voluntary_matching_contribution_amount_3";
            public static string PayStartDate = "pay_start_date";
            public static string PayEndDate = "pay_end_date";
        }
        #endregion

        #region main

        public VertivSunLifePensionExportCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            DataBaseCommand command = GetStoredProcCommand("PensionExport_select", user.DatabaseName);
            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToVetivSunLifePensionExportCollection(reader);
            }
        }
        #endregion

        #region data mapping
        protected VertivSunLifePensionExportCollection MapToVetivSunLifePensionExportCollection(IDataReader reader)
        {
            VertivSunLifePensionExportCollection collection = new VertivSunLifePensionExportCollection();
            while (reader.Read())
            {
                collection.Add(MapToVertivSunLifePensionExport(reader));
            }

            return collection;
        }
        protected VertivSunLifePensionExport MapToVertivSunLifePensionExport(IDataReader reader)
        {
            VertivSunLifePensionExport item = new VertivSunLifePensionExport();

            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.EmployeeNumber = (string)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.EmployerRequiredContributionAmount1 = (string)CleanDataValue(reader[ColumnNames.EmployerRequiredContributionAmount1]);
            item.MemberVoluntaryMatchedContributionAmount1 = (string)CleanDataValue(reader[ColumnNames.MemberVoluntaryMatchedContributionAmount1]);
            item.MemberVoluntaryUnmatchedContributionAmount1 = (string)CleanDataValue(reader[ColumnNames.MemberVoluntaryUnmatchedContributionAmount1]);
            item.EmployerVoluntaryMatchingContributionAmount1 = (string)CleanDataValue(reader[ColumnNames.EmployerVoluntaryMatchingContributionAmount1]);
            item.MemberVoluntaryMatchedContributionAmount2 = (string)CleanDataValue(reader[ColumnNames.MemberVoluntaryMatchedContributionAmount2]);
            item.MemberVoluntaryUnmatchedContributionAmount2 = (string)CleanDataValue(reader[ColumnNames.MemberVoluntaryUnmatchedContributionAmount2]);
            item.EmployerRequiredContributionAmount2 = (string)CleanDataValue(reader[ColumnNames.EmployerRequiredContributionAmount2]);
            item.MemberVoluntaryMatchedContributionAmount3 = (string)CleanDataValue(reader[ColumnNames.MemberVoluntaryMatchedContributionAmount3]);
            item.MemberVoluntaryUnmatchedContributionAmount3 = (string)CleanDataValue(reader[ColumnNames.MemberVoluntaryUnmatchedContributionAmount3]);
            item.EmployerVoluntaryMatchingContributionAmount2 = (string)CleanDataValue(reader[ColumnNames.EmployerVoluntaryMatchingContributionAmount2]);
            item.EmployerVoluntaryMatchingContributionAmount3 = (string)CleanDataValue(reader[ColumnNames.EmployerVoluntaryMatchingContributionAmount3]);
            item.PayStartDate = (string)CleanDataValue(reader[ColumnNames.PayStartDate]);
            item.PayEndDate = (string)CleanDataValue(reader[ColumnNames.PayEndDate]);

            return item;
        }
        #endregion

    }
}
