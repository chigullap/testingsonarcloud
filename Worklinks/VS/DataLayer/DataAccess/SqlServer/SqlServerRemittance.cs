﻿using System;
using System.Data;
using WLP.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerRemittance : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String RemittanceId = "remittance_id";
            public static String RemittanceType = "remittance_type";
            public static String Description = "description";
            public static String EffectiveEntryDate = "effective_entry_date";
            public static String RemittancePeriodStartDate = "remittance_period_start_date";
            public static String RemittancePeriodEndDate = "remittance_period_end_date";
            public static String RemittanceAmount = "remittance_amount";
            public static String FileTransmissionDateString = "file_transmission_date";
            public static String RbcSequenceNumber = "file_id";
            public static String DetailType = "detail_type";
            public static String RemitCode = "remit_code";
            public static String ExportId = "export_id";
            public static String RemitDateString = "remit_date";
        }
        #endregion

        #region main
        internal RemittanceCollection Select(DatabaseUser user)
        {
            DataBaseCommand command = GetStoredProcCommand("Remittance_report", user.DatabaseName);

            using (IDataReader reader = command.ExecuteReader())
                return MapToRemittanceCollection(reader);
        }
        #endregion

        #region data mapping
        protected RemittanceCollection MapToRemittanceCollection(IDataReader reader)
        {
            RemittanceCollection collection = new RemittanceCollection();
            int i = -1;

            while (reader.Read())
                collection.Add(MapToRemittance(reader, i--));

            return collection;
        }
        protected Remittance MapToRemittance(IDataReader reader, int i)
        {
            Remittance item = new Remittance();

            item.RemittanceId = (long)i;
            item.RemittanceType = (String)CleanDataValue(reader[ColumnNames.RemittanceType]);
            item.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            item.EffectiveEntryDate = (DateTime?)CleanDataValue(reader[ColumnNames.EffectiveEntryDate]);
            item.RemittancePeriodStartDate = (DateTime?)CleanDataValue(reader[ColumnNames.RemittancePeriodStartDate]);
            item.RemittancePeriodEndDate = (DateTime?)CleanDataValue(reader[ColumnNames.RemittancePeriodEndDate]);
            item.RemittanceAmount = (Decimal)CleanDataValue(reader[ColumnNames.RemittanceAmount]);
            item.FileTransmissionDateString = (String)CleanDataValue(reader[ColumnNames.FileTransmissionDateString]);
            item.RbcSequenceNumber = (long?)CleanDataValue(reader[ColumnNames.RbcSequenceNumber]);
            item.DetailType = (String)CleanDataValue(reader[ColumnNames.DetailType]);
            item.RemitCode = (String)CleanDataValue(reader[ColumnNames.RemitCode]);
            item.ExportId = Convert.ToString(CleanDataValue(reader[ColumnNames.ExportId]));
            item.RemitDateString = (String)CleanDataValue(reader[ColumnNames.RemitDateString]);

            return item;
        }
        #endregion
    }
}