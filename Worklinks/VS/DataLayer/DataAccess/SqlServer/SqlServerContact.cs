﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerContact : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String ContactId = "contact_id";
            public static String EmployeeId = "employee_id";
            public static String PersonId = "person_id";
            public static String LastName = "last_name";
            public static String FirstName = "first_name";
            public static String ContactRelationshipCode = "code_contact_relationship_cd";
            public static String EffectiveDate = "effective_date";
        }
        #endregion

        #region main

        internal ContactCollection Select(DatabaseUser user, long employeeId, long? contactId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Contact_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@employeeId", employeeId);
                command.AddParameterWithValue("@contactId", contactId);
                command.AddParameterWithValue("@securityRoleId", user.SecurityRoleId);
                command.AddParameterWithValue("@securityUserId", user.SecurityUserId);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToContactCollection(reader);
                }
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Contact contact)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("Contact_update", user.DatabaseName))
                {
                    command.BeginTransaction("Contact_update");

                    command.AddParameterWithValue("@contactId", contact.ContactId);
                    command.AddParameterWithValue("@employeeId", contact.EmployeeId);
                    command.AddParameterWithValue("@personId", contact.PersonId);
                    command.AddParameterWithValue("@effectiveDate", contact.EffectiveDate);
                    command.AddParameterWithValue("@contactRelationshipCode", contact.ContactRelationshipCode);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", contact.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    contact.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();

                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Contact contact)
        {
            using (DataBaseCommand command = GetStoredProcCommand("Contact_insert", user.DatabaseName))
            {
                command.BeginTransaction("Contact_insert");

                SqlParameter contactIdParm = command.AddParameterWithValue("@contactId", contact.ContactId, ParameterDirection.Output);
                command.AddParameterWithValue("@employeeId", contact.EmployeeId);
                command.AddParameterWithValue("@personId", contact.PersonId);
                command.AddParameterWithValue("@effectiveDate", contact.EffectiveDate);
                command.AddParameterWithValue("@contactRelationshipCode", contact.ContactRelationshipCode);

                SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", contact.RowVersion, ParameterDirection.InputOutput);
                command.AddParameterWithValue("@updateUser", user.UserName);
                command.AddParameterWithValue("@updateDatetime", Time);

                command.ExecuteNonQuery();
                contact.ContactId = Convert.ToInt64(contactIdParm.Value);
                contact.RowVersion = (byte[])rowVersionParm.Value;
                command.CommitTransaction();
            }
        }
        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, Contact contact)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("Contact_delete", user.DatabaseName))
                {
                    command.BeginTransaction("Contact_delete");
                    command.AddParameterWithValue("@contactId", contact.ContactId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();

                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected ContactCollection MapToContactCollection(IDataReader reader)
        {
            ContactCollection collection = new ContactCollection();
            while (reader.Read())
            {
                collection.Add(MapToContact(reader));
            }

            return collection;
        }
        protected Contact MapToContact(IDataReader reader)
        {
            Contact contact = new Contact();
            base.MapToBase(contact, reader, false);
            contact.RowVersion = (byte[])CleanDataValue(reader[ColumnNames.RowVersion]);
            contact.ContactId = (long)CleanDataValue(reader[ColumnNames.ContactId]);
            contact.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            contact.PersonId = (long)CleanDataValue(reader[ColumnNames.PersonId]);
            contact.EffectiveDate = (DateTime)CleanDataValue(reader[ColumnNames.EffectiveDate]);
            contact.ContactRelationshipCode = (String)CleanDataValue(reader[ColumnNames.ContactRelationshipCode]);

            return contact;
        }

        protected ContactSummaryCollection MapToContactSummaryCollection(IDataReader reader)
        {
            ContactSummaryCollection collection = new ContactSummaryCollection();
            while (reader.Read())
            {
                collection.Add(MapToContactSummary(reader));
            }

            return collection;
        }

        protected ContactSummary MapToContactSummary(IDataReader reader)
        {
            ContactSummary contact = new ContactSummary();

            contact.ContactId = (long)CleanDataValue(reader[ColumnNames.ContactId]);
            contact.LastName = (String)CleanDataValue(reader[ColumnNames.LastName]);
            contact.FirstName = (String)CleanDataValue(reader[ColumnNames.FirstName]);
            contact.EffectiveDate = (DateTime)CleanDataValue(reader[ColumnNames.EffectiveDate]);
            contact.ContactRelationshipCode = (String)CleanDataValue(reader[ColumnNames.ContactRelationshipCode]);

            return contact;
        }
        #endregion
    }
}