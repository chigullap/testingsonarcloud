﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    internal class SqlServerPersonContactChannel : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String PersonContactChannelId = "person_contact_channel_id";
            public static String PersonId = "person_id";
            public static String PrimaryFlag = "primary_flag";
            public static String ContactChannelId = "contact_channel_id";
            public static String SharedCount = "shared_count";

        }
        #endregion

        #region main
        internal PersonContactChannelCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? personId, String contactChannelCategoryCode)
        {
            return Select(user, personId, contactChannelCategoryCode, null);
        }

        internal PersonContactChannelCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? personId, String contactChannelCategoryCode, long? contactChannelId)
        {
            DataBaseCommand command = GetStoredProcCommand("PersonContactChannel_select", user.DatabaseName);
            command.AddParameterWithValue("@personId", personId);
            command.AddParameterWithValue("@contactChannelCategoryCode", contactChannelCategoryCode);
            command.AddParameterWithValue("@contactChannelId", contactChannelId);


            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToPersonContactChannelCollection(reader);
            }
        }

        public void Update(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, PersonContactChannel channel)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PersonContactChannel_update", user.DatabaseName))
                {
                    command.BeginTransaction("PersonContactChannel_update");

                    command.AddParameterWithValue("@personContactChannelId", channel.PersonContactChannelId);
                    command.AddParameterWithValue("@personId", channel.PersonId);
                    command.AddParameterWithValue("@primaryFlag", channel.PrimaryFlag);
                    command.AddParameterWithValue("@contactChannelId", channel.ContactChannelId);
                    command.AddParameterWithValue("@contactChannelTypeCode", channel.ContactChannelTypeCode);
                    command.AddParameterWithValue("@overrideConcurrencyCheckFlag", channel.OverrideConcurrencyCheckFlag);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", channel.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    channel.RowVersion = (byte[])rowVersionParm.Value;
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void Insert(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, PersonContactChannel channel)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PersonContactChannel_insert", user.DatabaseName))
                {
                    command.BeginTransaction("PersonContactChannel_insert");

                    SqlParameter PersonContactChannelIdParm = command.AddParameterWithValue("@personContactChannelId", channel.PersonContactChannelId, ParameterDirection.Output);
                    command.AddParameterWithValue("@personId", channel.PersonId);
                    command.AddParameterWithValue("@primaryFlag", channel.PrimaryFlag);
                    command.AddParameterWithValue("@contactChannelId", channel.ContactChannelId);
                    command.AddParameterWithValue("@contactChannelTypeCode", channel.ContactChannelTypeCode);

                    SqlParameter rowVersionParm = command.AddParameterWithValue("@rowVersion", channel.RowVersion, ParameterDirection.InputOutput);
                    command.AddParameterWithValue("@updateUser", user.UserName);
                    command.AddParameterWithValue("@updateDatetime", Time);

                    command.ExecuteNonQuery();
                    channel.PersonContactChannelId = Convert.ToInt64(PersonContactChannelIdParm.Value);
                    channel.RowVersion = (byte[])rowVersionParm.Value;

                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public String GetContactChannelCategory(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, PersonContactChannel channel)
        {
            using (DataBaseCommand command = GetStoredProcCommand("GetContactChannelCategory_select", user.DatabaseName))
            {
                String category = "";
                command.BeginTransaction("GetContactChannelCategory_select");
                command.AddParameterWithValue("@codeContactChannelCd", channel.ContactChannelTypeCode);
                SqlParameter categoryParam = command.AddParameterWithValue("@procReturn", category, ParameterDirection.InputOutput);
                categoryParam.Size = -1;

                command.ExecuteNonQuery();

                category = categoryParam.Value.ToString();
                command.CommitTransaction();

                return category;
            }
        }

        public void Delete(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, PersonContactChannel channel)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PersonContactChannel_delete", user.DatabaseName))
                {
                    command.BeginTransaction("PersonContactChannel_delete");
                    command.AddParameterWithValue("@personContactChannelId", channel.PersonContactChannelId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }

        public void DeleteByPersonId(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long personId)
        {
            try
            {
                using (DataBaseCommand command = GetStoredProcCommand("PersonContactChannel_deleteByPersonId", user.DatabaseName))
                {
                    command.BeginTransaction("PersonContactChannel_deleteByPersonId");
                    command.AddParameterWithValue("@personId", personId);

                    command.ExecuteNonQuery();
                    command.CommitTransaction();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc, ((SqlException)exc).Number);
            }
        }
        #endregion

        #region data mapping
        protected PersonContactChannelCollection MapToPersonContactChannelCollection(IDataReader reader)
        {
            PersonContactChannelCollection collection = new PersonContactChannelCollection();
            while (reader.Read())
            {
                collection.Add(MapToPersonContactChannel(reader));
            }

            return collection;
        }
        protected PersonContactChannel MapToPersonContactChannel(IDataReader reader)
        {
            PersonContactChannel contact = new PersonContactChannel();
            base.MapToBase(contact, reader, false);

            contact.PersonContactChannelId = (long)CleanDataValue(reader[ColumnNames.PersonContactChannelId]);
            contact.PersonId = (long)CleanDataValue(reader[ColumnNames.PersonId]);
            contact.PrimaryFlag = (bool)CleanDataValue(reader[ColumnNames.PrimaryFlag]);
            contact.ContactChannelId = (long)CleanDataValue(reader[ColumnNames.ContactChannelId]);
            contact.RowVersion = (byte[])CleanDataValue(reader[ColumnNames.RowVersion]);
            contact.SharedCount = (int)CleanDataValue(reader[ColumnNames.SharedCount]);

            return contact;
        }
        #endregion
    }
}