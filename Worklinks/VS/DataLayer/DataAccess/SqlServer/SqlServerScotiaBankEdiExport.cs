﻿using System;
using System.Data;
using System.Data.SqlClient;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.SqlServer
{
    public class SqlServerScotiaBankEdiExport : SqlServerBase
    {
        #region column names
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static string PayrollMasterPaymentId = "payroll_master_payment_id";
            public static string CodeCompanyCd = "code_company_cd";
            public static string EmployeeNumber = "employee_number";
            public static string EmployeeId = "employee_id";
            public static string PayrollProcessId = "payroll_process_id";
            public static string CodeEmployeeBankingSequenceCode = "code_employee_banking_sequence_cd";
            public static string EmployeeName = "employee_name";
            public static string BankInstCode = "bank_inst_code";
            public static string BankTransactionNumber = "bank_transaction_number";
            public static string EmployeeBankAccountNumber = "employee_bank_account_number";
            public static string AbaNumber = "aba_number";
            public static string ChequeAmount = "cheque_amount";
            public static string ChequeDate = "cheque_date";
            public static string DirectDepositFlag = "direct_deposit_flag";
            public static string SequenceNumber = "sequence_number";
            public static string CodeStatusCd = "code_status_cd";
            public static string CodeBankingCountryCd = "code_banking_country_cd";
        }
        #endregion

        #region main
        public ScotiaBankEdiCollection Select(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            DataBaseCommand command = GetStoredProcCommand("ScotiaBankEdi_select", user.DatabaseName);
            command.AddParameterWithValue("@payrollProcessId", payrollProcessId);
            command.AddParameterWithValue("@employeeNumber", employeeNumber);

            using (IDataReader reader = command.ExecuteReader())
            {
                return MapToScotiaBankEdiCollection(reader);
            }
        }
        #endregion

        #region data mapping
        protected ScotiaBankEdiCollection MapToScotiaBankEdiCollection(IDataReader reader)
        {
            ScotiaBankEdiCollection collection = new ScotiaBankEdiCollection();
            while (reader.Read())
            {
                collection.Add(MapToScotiaBankEdi(reader));
            }

            return collection;
        }
        protected ScotiaBankEdi MapToScotiaBankEdi(IDataReader reader)
        {
            ScotiaBankEdi item = new ScotiaBankEdi();
            base.MapToBase(item, reader);

            item.PayrollMasterPaymentId = (long)CleanDataValue(reader[ColumnNames.PayrollMasterPaymentId]);
            item.CodeCompanyCd = (string)CleanDataValue(reader[ColumnNames.CodeCompanyCd]);
            item.EmployeeNumber = (string)CleanDataValue(reader[ColumnNames.EmployeeNumber]);
            item.EmployeeId = (long)CleanDataValue(reader[ColumnNames.EmployeeId]);
            item.PayrollProcessId = Convert.ToString(CleanDataValue(reader[ColumnNames.PayrollProcessId]));
            item.CodeEmployeeBankingSequenceCode = (string)CleanDataValue(reader[ColumnNames.CodeEmployeeBankingSequenceCode]);
            item.EmployeeName = (string)CleanDataValue(reader[ColumnNames.EmployeeName]);
            item.BankInstCode = (string)CleanDataValue(reader[ColumnNames.BankInstCode]);
            item.BankTransactionNumber = (string)CleanDataValue(reader[ColumnNames.BankTransactionNumber]);
            item.EmployeeBankAccountNumber = (string)CleanDataValue(reader[ColumnNames.EmployeeBankAccountNumber]);
            item.AbaNumber = (string)CleanDataValue(reader[ColumnNames.AbaNumber]);
            item.ChequeAmount = (decimal)CleanDataValue(reader[ColumnNames.ChequeAmount]);
            item.ChequeDate = (DateTime)CleanDataValue(reader[ColumnNames.ChequeDate]);
            item.DirectDepositFlag = (bool)CleanDataValue(reader[ColumnNames.DirectDepositFlag]);
            item.SequenceNumber = (long?)CleanDataValue(reader[ColumnNames.SequenceNumber]);
            item.CodeStatusCd = (string)CleanDataValue(reader[ColumnNames.CodeStatusCd]);
            item.CodeBankingCountryCd = (string)CleanDataValue(reader[ColumnNames.CodeBankingCountryCd]);

            return item;
        }
        #endregion
    }
}
