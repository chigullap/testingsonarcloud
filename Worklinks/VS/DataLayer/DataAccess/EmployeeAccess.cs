﻿using System;
using System.Collections.Generic;
using System.Transactions;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.CalcModel;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;

namespace WorkLinks.DataLayer.DataAccess
{
    public class EmployeeAccess
    {
        #region properties
        SqlServer.SqlServerPerson _sqlPerson = new SqlServer.SqlServerPerson();
        SqlServer.SqlServerEmployee _sqlEmployee = new SqlServer.SqlServerEmployee();
        SqlServer.SqlServerEmployeeEmploymentEquity _sqlEmployeeEmploymentEquity = new SqlServer.SqlServerEmployeeEmploymentEquity();
        SqlServer.SqlServerEmployeeSkill _sqlEmployeeSkill = new SqlServer.SqlServerEmployeeSkill();
        SqlServer.SqlServerEmployeeCourse _sqlEmployeeCourse = new SqlServer.SqlServerEmployeeCourse();
        SqlServer.SqlServerEmployeeBanking _SqlServerEmployeeBanking = new SqlServer.SqlServerEmployeeBanking();
        SqlServer.SqlServerEmployeeEducation _sqlEmployeeEducation = new SqlServer.SqlServerEmployeeEducation();
        SqlServer.SqlServerEmployeeBanking _sqlEmployeeBanking = new SqlServer.SqlServerEmployeeBanking();
        SqlServer.SqlServerEmployeeCompanyProperty _sqlEmployeeCompanyProperty = new SqlServer.SqlServerEmployeeCompanyProperty();
        SqlServer.SqlServerContact _sqlContact = new SqlServer.SqlServerContact();
        SqlServer.SqlServerContactType _sqlContactType = new SqlServer.SqlServerContactType();
        SqlServer.SqlServerEmployeeLicenseCertificate _sqlEmployeeLicenseCertificate = new SqlServer.SqlServerEmployeeLicenseCertificate();
        SqlServer.SqlServerEmployeeDiscipline _sqlEmployeeDiscipline = new SqlServer.SqlServerEmployeeDiscipline();
        SqlServer.SqlServerEmployeeDisciplineAction _sqlEmployeeDisciplineAction = new SqlServer.SqlServerEmployeeDisciplineAction();
        SqlServer.SqlServerEmployeeAwardHonour _sqlEmployeeAwardHonour = new SqlServer.SqlServerEmployeeAwardHonour();
        SqlServer.SqlServerEmployeePosition _sqlEmployeePosition = new SqlServer.SqlServerEmployeePosition();
        SqlServer.SqlServerEmployeePaycode _sqlEmployeePaycode = new SqlServer.SqlServerEmployeePaycode();
        SqlServer.SqlServerEmployeeVacationHourlyRate _sqlServerEmployeeVacationHourlyRate = new SqlServer.SqlServerEmployeeVacationHourlyRate();
        SqlServer.SqlServerEmployeePositionOrganizationUnit _sqlServerEmployeePositionOrganizationUnit = new SqlServer.SqlServerEmployeePositionOrganizationUnit();
        SqlServer.SqlServerEmployeeEmploymentInformation _sqlServerEmployeeEmploymentInformation = new SqlServer.SqlServerEmployeeEmploymentInformation();
        SqlServer.SqlServerEmployeeTerminationOther _sqlServerEmployeeTerminationOther = new SqlServer.SqlServerEmployeeTerminationOther();
        SqlServer.SqlServerEmployeeTerminationRoe _sqlServerEmployeeTerminationRoe = new SqlServer.SqlServerEmployeeTerminationRoe();
        SqlServer.SqlServerGetEmployeeIdsByPayProcessId _sqlGetEmployeeIds = new SqlServer.SqlServerGetEmployeeIdsByPayProcessId();
        SqlServer.SqlServerVendor _sqlServerVendor = new SqlServer.SqlServerVendor();
        SqlServer.SqlServerEmployeeWsibHealthAndSafetyReport _sqlServerEmployeeWsibHealthAndSafetyReport = new SqlServer.SqlServerEmployeeWsibHealthAndSafetyReport();
        SqlServer.SqlServerStatutoryDeduction _sqlServerStatutoryDeduction = new SqlServer.SqlServerStatutoryDeduction();
        SqlServer.SqlServerStatutoryDeductionDefaults _sqlServerStatutoryDeductionDefault = new SqlServer.SqlServerStatutoryDeductionDefaults();
        SqlServer.SqlServerBusinessNumber _sqlServerBusinessNumber = new SqlServer.SqlServerBusinessNumber();
        SqlServer.SqlServerEmployeePaycodeProcessed _sqlServerEmployeePaycodeProcessed = new SqlServer.SqlServerEmployeePaycodeProcessed();
        SqlServer.SqlServerPensionBasicContribution _sqlServerPensionBasicContribution = new SqlServer.SqlServerPensionBasicContribution();
        //SqlServer.SqlServerCanadaRevenueAgencyMaximumPensionContributionLimit _sqlServerCanadaRevenueAgencyMaximumPensionContributionLimit = new SqlServer.SqlServerCanadaRevenueAgencyMaximumPensionContributionLimit();
        SqlServer.SqlServerYearEndAdjustment _sqlGetYearEndNegativeAmountEmployee = new SqlServer.SqlServerYearEndAdjustment();
        SqlServer.SqlServerEmployeePhoto _sqlEmployeePhoto = new SqlServer.SqlServerEmployeePhoto();
        SqlServer.SqlServerAttachment _sqlAttachment = new SqlServer.SqlServerAttachment();
        SqlServer.SqlServerEmployeeRemittanceStub _sqlServerEmployeeRemittanceStub = new SqlServer.SqlServerEmployeeRemittanceStub();
        SqlServer.SqlServerEmployeePositionWorkday _sqlEmployeePositionWorkday = new SqlServer.SqlServerEmployeePositionWorkday();
        SqlServer.SqlServerGlobalEmployeePaycode _sqlGlobalEmployeePaycode = new SqlServer.SqlServerGlobalEmployeePaycode();
        SqlServer.SqlServerStatutoryHolidayDate _sqlServerStatutoryHolidayDate = new SqlServer.SqlServerStatutoryHolidayDate();
        SqlServer.SqlServerEmployeeMembership _sqlEmployeeMembership = new SqlServer.SqlServerEmployeeMembership();
        SqlServer.SqlServerEmployeeCustomFieldConfig _sqlServerEmployeeCustomFieldConfig = new SqlServer.SqlServerEmployeeCustomFieldConfig();
        SqlServer.SqlServerEmployeeCustomField _sqlServerEmployeeCustomField = new SqlServer.SqlServerEmployeeCustomField();
        SqlServer.SqlServerEmployeePositionSecondary _sqlServerEmployeePositionSecondary = new SqlServer.SqlServerEmployeePositionSecondary();
        SqlServer.SqlServerEmployeePositionSecondaryOrganizationUnit _sqlServerEmployeePositionSecondaryOrganizationUnit = new SqlServer.SqlServerEmployeePositionSecondaryOrganizationUnit();
        SqlServer.SqlServerSalaryPlan _sqlServerSalaryPlan = new SqlServer.SqlServerSalaryPlan();
        SqlServer.SqlServerSalaryPlanGrade _sqlServerSalaryPlanGrade = new SqlServer.SqlServerSalaryPlanGrade();
        SqlServer.SqlServerSalaryPlanGradeStep _sqlServerSalaryPlanGradeStep = new SqlServer.SqlServerSalaryPlanGradeStep();
        SqlServer.SqlServerPrimarySecondaryEmployeePosition _sqlPrimarySecondaryEmployeePosition = new SqlServer.SqlServerPrimarySecondaryEmployeePosition();
        SqlServer.SqlServerSalaryPlanGradeAccumulation _sqlServerSalaryPlanGradeAccumulation = new SqlServer.SqlServerSalaryPlanGradeAccumulation();
        SqlServer.SqlServerEmployeeBenefit _sqlServerEmployeeBenefit = new SqlServer.SqlServerEmployeeBenefit();

        SqlServer.SqlServerSalaryPlanGradeAndEffectiveDetail _sqlServerSalaryPlanGradeAndEffectiveDetail = new SqlServer.SqlServerSalaryPlanGradeAndEffectiveDetail();

        #endregion

        #region employee biographical
        public EmployeeCollection GetEmployee(DatabaseUser user, long employeeId, bool securityOverrideFlag)
        {
            return _sqlEmployee.Select(user, employeeId, securityOverrideFlag);
        }
        public EmployeeCollection GetEmployeeBatch(DatabaseUser user, String[] employeeIdentifiers)
        {
            return _sqlEmployee.SelectBatch(user, employeeIdentifiers);
        }
        public EmployeeSummaryCollection GetEmployeeSummary(DatabaseUser user, EmployeeCriteria criteria)
        {
            return _sqlEmployee.SelectEmployeeSummary(user, criteria);
        }
        public EmployeeCollection GetEmployeeByPersonId(DatabaseUser user, long personId)
        {
            return _sqlEmployee.SelectEmployeeByPersonId(user, personId);
        }
        public void UpdateEmployee(DatabaseUser user, Employee employee)
        {
            _sqlEmployee.Update(user, employee);
        }
        public void UpdateEmployeeBatch(DatabaseUser user, WorkLinksEmployeeCollection items)
        {
            _sqlEmployee.UpdateBatch(user, items);
        }
        public void InsertEmployee(DatabaseUser user, Employee employee)
        {
            _sqlEmployee.Insert(user, employee);
        }
        public void DeleteEmployee(DatabaseUser user, Employee employee)
        {
            _sqlEmployee.Delete(user, employee);
        }

        public IEnumerable<EmployeeBiographic> GetEmployeeBiographics(DatabaseUser user, IEnumerable<long> employeeIds)
        {
            return _sqlEmployee.SelectEmployeeBiographics(user, employeeIds);
        }

        #endregion

        #region employee employment equity
        public EmployeeEmploymentEquityCollection GetEmployeeEmploymentEquity(DatabaseUser user, long employeeId)
        {
            return _sqlEmployeeEmploymentEquity.Select(user, employeeId);
        }
        public EmployeeEmploymentEquity InsertEmployeeEmploymentEquity(DatabaseUser user, EmployeeEmploymentEquity employmentEquity)
        {
            return _sqlEmployeeEmploymentEquity.Insert(user, employmentEquity);
        }
        public void UpdateEmployeeEmploymentEquity(DatabaseUser user, EmployeeEmploymentEquity employmentEquity)
        {
            _sqlEmployeeEmploymentEquity.Update(user, employmentEquity);
        }
        public void DeleteEmployeeEmploymentEquity(DatabaseUser user, long employeeId)
        {
            _sqlEmployeeEmploymentEquity.Delete(user, employeeId);
        }
        #endregion

        #region employee skill
        public EmployeeSkillCollection GetEmployeeSkill(DatabaseUser user, long employeeId)
        {
            return _sqlEmployeeSkill.Select(user, employeeId);
        }
        public void UpdateEmployeeSkill(DatabaseUser user, EmployeeSkill skill)
        {
            _sqlEmployeeSkill.Update(user, skill);
        }
        public void DeleteEmployeeSkill(DatabaseUser user, EmployeeSkill skill)
        {
            _sqlEmployeeSkill.Delete(user, skill);
        }
        public EmployeeSkill InsertEmployeeSkill(DatabaseUser user, EmployeeSkill skill)
        {
            return _sqlEmployeeSkill.Insert(user, skill);
        }
        #endregion

        #region employee course
        public EmployeeCourseCollection GetEmployeeCourse(DatabaseUser user, long employeeId)
        {
            return _sqlEmployeeCourse.Select(user, employeeId);
        }
        public void UpdateEmployeeCourse(DatabaseUser user, EmployeeCourse course)
        {
            _sqlEmployeeCourse.Update(user, course);
        }
        public void DeleteEmployeeCourse(DatabaseUser user, EmployeeCourse course)
        {
            _sqlEmployeeCourse.Delete(user, course);
        }
        public EmployeeCourse InsertEmployeeCourse(DatabaseUser user, EmployeeCourse course)
        {
            return _sqlEmployeeCourse.Insert(user, course);
        }
        #endregion

        #region employee membership
        public EmployeeMembershipCollection GetEmployeeMembership(DatabaseUser user, long employeeId)
        {
            return _sqlEmployeeMembership.Select(user, employeeId);
        }
        public EmployeeMembership InsertEmployeeMembership(DatabaseUser user, EmployeeMembership membership)
        {
            return _sqlEmployeeMembership.Insert(user, membership);
        }
        public void UpdateEmployeeMembership(DatabaseUser user, EmployeeMembership membership)
        {
            _sqlEmployeeMembership.Update(user, membership);
        }
        public void DeleteEmployeeMembership(DatabaseUser user, EmployeeMembership membership)
        {
            _sqlEmployeeMembership.Delete(user, membership);
        }
        #endregion

        #region employee banking
        public EmployeeBankingCollection GetEmployeeBanking(DatabaseUser user, long employeeId, bool securityOverrideFlag)
        {
            return _sqlEmployeeBanking.Select(user, employeeId, securityOverrideFlag);
        }
        public void UpdateEmployeeBanking(DatabaseUser user, EmployeeBanking bank)
        {
            _sqlEmployeeBanking.Update(user, bank);
        }
        public void DeleteEmployeeBanking(DatabaseUser user, EmployeeBanking bank)
        {
            _sqlEmployeeBanking.Delete(user, bank);
        }
        public EmployeeBanking InsertEmployeeBanking(DatabaseUser user, EmployeeBanking bank)
        {
            return _sqlEmployeeBanking.Insert(user, bank);
        }
        public void ImportBanking(DatabaseUser user, WorkLinksEmployeeBankingCollection existingBanking)
        {
            _sqlEmployeeBanking.ImportBanking(user, existingBanking);
        }
        #endregion

        #region employee education
        public EmployeeEducationCollection GetEmployeeEducation(DatabaseUser user, long employeeId)
        {
            return _sqlEmployeeEducation.Select(user, employeeId);
        }
        public void UpdateEmployeeEducation(DatabaseUser user, EmployeeEducation education)
        {
            _sqlEmployeeEducation.Update(user, education);
        }
        public EmployeeEducation InsertEmployeeEducation(DatabaseUser user, EmployeeEducation education)
        {
            return _sqlEmployeeEducation.Insert(user, education);
        }
        public void DeleteEmployeeEducation(DatabaseUser user, EmployeeEducation education)
        {
            _sqlEmployeeEducation.Delete(user, education);
        }
        #endregion

        #region employee company property
        public EmployeeCompanyPropertyCollection GetEmployeeCompanyProperty(DatabaseUser user, long employeeId)
        {
            return _sqlEmployeeCompanyProperty.SelectCompanyProperty(user, employeeId);
        }
        public void UpdateEmployeeCompanyProperty(DatabaseUser user, EmployeeCompanyProperty property)
        {
            _sqlEmployeeCompanyProperty.Update(user, property);
        }
        public EmployeeCompanyProperty InsertEmployeeCompanyProperty(DatabaseUser user, EmployeeCompanyProperty property)
        {
            return _sqlEmployeeCompanyProperty.Insert(user, property);
        }
        public void DeleteEmployeeCompanyProperty(DatabaseUser user, EmployeeCompanyProperty property)
        {
            _sqlEmployeeCompanyProperty.Delete(user, property);
        }
        #endregion

        #region employee contact
        public ContactCollection GetContact(DatabaseUser user, long employeeId, long? contactId)
        {
            ContactCollection contacts = _sqlContact.Select(user, employeeId, contactId);
            foreach (Contact contact in contacts)
            {
                PersonCollection persons = _sqlPerson.Select(user, contact.PersonId);
                persons[0].CopyTo(contact); //assuming 1 person per employee
            }

            return contacts;
        }
        public void UpdateContact(DatabaseUser user, Contact contact)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                _sqlPerson.Update(user, contact);
                _sqlContact.Update(user, contact);

                scope.Complete();
            }
        }
        public void InsertContact(DatabaseUser user, Contact contact)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                _sqlPerson.Insert(user, user.DatabaseName, contact);
                _sqlContact.Insert(user, contact);

                scope.Complete();
            }
        }
        public void DeleteContact(DatabaseUser user, Contact contact)
        {
            _sqlContact.Delete(user, contact);
        }
        #endregion

        #region employee contact type
        public ContactTypeCollection GetContactType(DatabaseUser user, long employeeId, String contactTypeCode)
        {
            ContactTypeCollection contactTypes = _sqlContactType.Select(user, employeeId, contactTypeCode);
            foreach (ContactType contactType in contactTypes)
            {
                ContactCollection contacts = _sqlContact.Select(user, employeeId, contactType.ContactId);
                contacts[0].CopyTo(contactType); //assuming 1 contact per contact type

                PersonCollection persons = _sqlPerson.Select(user, contactType.PersonId);
                persons[0].CopyTo(contactType); //assuming 1 person per employee
            }

            return contactTypes;
        }
        public void UpdateContactType(DatabaseUser user, ContactTypeCollection contactTypes, System.Collections.Generic.List<ContactType> deletedItems)
        {
            _sqlContactType.Update(user, contactTypes, deletedItems);
        }
        public void UpdateContactType(DatabaseUser user, ContactType contactType)
        {
            _sqlContactType.Update(user, contactType);
        }
        public ContactType InsertContactType(DatabaseUser user, ContactType contactType)
        {
            return _sqlContactType.Insert(user, contactType);
        }
        public void DeleteContactType(DatabaseUser user, ContactType contactType)
        {
            _sqlContactType.Delete(user, contactType);
        }
        public void DeleteContactTypeByContactId(DatabaseUser user, long contactId)
        {
            _sqlContactType.DeleteByContactId(user, contactId);
        }
        public CodeContactChannelTypeCollection ContactChannelTypeByEmployeeId(DatabaseUser user, long personId, string contactChannelCategoryCode)
        {
            return _sqlContactType.SelectTypeByEmployeeId(user, personId, contactChannelCategoryCode);
        }
        #endregion

        #region bms pension contributions
        public void EmployeePaycodeSetMaximumPensionAmount(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            _sqlServerPensionBasicContribution.EmployeePaycodeSetMaximumPensionAmount(user, payrollProcessId);
        }
        //public PensionBasicContributionCollection GetPensionBasicContributionTable(DatabaseUser user, long? pensionContributionId)
        //{
        //    return _sqlServerPensionBasicContribution.Select(user, pensionContributionId);
        //}
        //public Decimal GetPensionContributionTotal(DatabaseUser user, long empId, Decimal year, Boolean optionalContributionFlag)
        //{
        //    return _sqlServerPensionBasicContribution.SelectPensionContributionTotal(user, empId, year, optionalContributionFlag);
        //}
        //public CanadaRevenueAgencyMaximumPensionContributionLimitCollection GetMaxContribution(DatabaseUser user, Decimal year)
        //{
        //    return _sqlServerCanadaRevenueAgencyMaximumPensionContributionLimit.Select(user, year);
        //}
        //public Decimal SelectPensionContributionPercentage(DatabaseUser user, long empId)
        //{
        //    return _sqlServerPensionBasicContribution.SelectPensionContributionPercentage(user, empId);
        //}
        //public void SetBasePaycodeContributionData(DatabaseUser user, long empId, Decimal basicPensionRate, Decimal baseContributionTotal, Decimal baseYearlyMaximum)
        //{
        //    _sqlServerPensionBasicContribution.SetBasePaycodeContributionData(user, empId, basicPensionRate, baseContributionTotal, baseYearlyMaximum);
        //}
        //public void SetOptionalPaycodeContributionData(DatabaseUser user, long empId, Decimal optionalContributionPercentage, Decimal optionalContributionTotal, Decimal optionalYearlyMaximum)
        //{
        //    _sqlServerPensionBasicContribution.SetOptionalPaycodeContributionData(user, empId, optionalContributionPercentage, optionalContributionTotal, optionalYearlyMaximum);
        //}
        #endregion

        #region employee license certificate
        public EmployeeLicenseCertificateCollection GetEmployeeLicenseCertificates(DatabaseUser user, long employeeId)
        {
            return _sqlEmployeeLicenseCertificate.Select(user, employeeId);
        }
        public void UpdateEmployeeLicenseCertificate(DatabaseUser user, EmployeeLicenseCertificate licenseCertificate)
        {
            _sqlEmployeeLicenseCertificate.Update(user, licenseCertificate);
        }
        public void DeleteEmployeeLicenseCertificate(DatabaseUser user, EmployeeLicenseCertificate licenseCertificate)
        {
            _sqlEmployeeLicenseCertificate.Delete(user, licenseCertificate);
        }
        public EmployeeLicenseCertificate InsertEmployeeLicenseCertificate(DatabaseUser user, EmployeeLicenseCertificate licenseCertificate)
        {
            return _sqlEmployeeLicenseCertificate.Insert(user, licenseCertificate);
        }
        #endregion

        #region employee award honour
        public EmployeeAwardHonourCollection GetEmployeeAwardHonour(DatabaseUser user, long employeeId)
        {
            return _sqlEmployeeAwardHonour.Select(user, employeeId);
        }
        public void UpdateEmployeeAwardHonour(DatabaseUser user, EmployeeAwardHonour awardHonour)
        {
            _sqlEmployeeAwardHonour.Update(user, awardHonour);
        }
        public void DeleteEmployeeAwardHonour(DatabaseUser user, EmployeeAwardHonour awardHonour)
        {
            _sqlEmployeeAwardHonour.Delete(user, awardHonour);
        }
        public EmployeeAwardHonour InsertEmployeeAwardHonour(DatabaseUser user, EmployeeAwardHonour awardHonour)
        {
            return _sqlEmployeeAwardHonour.Insert(user, awardHonour);
        }
        #endregion

        #region employee discipline
        public EmployeeDisciplineCollection GetEmployeeDiscipline(DatabaseUser user, long employeeId)
        {
            return _sqlEmployeeDiscipline.Select(user, employeeId);
        }
        public void UpdateEmployeeDiscipline(DatabaseUser user, EmployeeDiscipline discipline)
        {
            _sqlEmployeeDiscipline.Update(user, discipline);
        }
        public void DeleteEmployeeDiscipline(DatabaseUser user, EmployeeDiscipline discipline)
        {
            _sqlEmployeeDiscipline.Delete(user, discipline);
        }
        public EmployeeDiscipline InsertEmployeeDiscipline(DatabaseUser user, EmployeeDiscipline discipline)
        {
            return _sqlEmployeeDiscipline.Insert(user, discipline);
        }
        #endregion

        #region employee discipline action
        public EmployeeDisciplineActionCollection GetEmployeeDisciplineAction(DatabaseUser user, long employeeDisciplineId)
        {
            return _sqlEmployeeDisciplineAction.Select(user, employeeDisciplineId);
        }
        public void UpdateEmployeeDisciplineAction(DatabaseUser user, EmployeeDisciplineAction disciplineAction)
        {
            _sqlEmployeeDisciplineAction.Update(user, disciplineAction);
        }
        public void DeleteEmployeeDisciplineAction(DatabaseUser user, EmployeeDisciplineAction disciplineAction)
        {
            _sqlEmployeeDisciplineAction.Delete(user, disciplineAction);
        }
        public EmployeeDisciplineAction InsertEmployeeDisciplineAction(DatabaseUser user, EmployeeDisciplineAction disciplineAction)
        {
            return _sqlEmployeeDisciplineAction.Insert(user, disciplineAction);
        }
        #endregion

        #region employee paycode
        public bool CheckRecurringIncomeCode(DatabaseUser user)
        {
            return _sqlEmployeePaycode.CheckRecurringIncomeCode(user);
        }
        public EmployeePaycodeCollection GetEmployeePaycode(DatabaseUser user, long? employeeId, bool securityOverrideFlag, bool useGlobalPaycode)
        {
            return _sqlEmployeePaycode.Select(user, employeeId, securityOverrideFlag, useGlobalPaycode);
        }
        public void UpdateEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycodeId)
        {
            _sqlEmployeePaycode.Update(user, employeePaycodeId);
        }
        public EmployeePaycode InsertEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycodeId)
        {
            return _sqlEmployeePaycode.Insert(user, employeePaycodeId);
        }
        public void DeleteEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycodeId)
        {
            _sqlEmployeePaycode.Delete(user, employeePaycodeId);
        }
        #endregion

        #region employee vacation hourly rate
        public EmployeeVacationHourlyRateCollection GetEmployeeVacationHourlyRate(DatabaseUser user, long employeeId)
        {
            return _sqlServerEmployeeVacationHourlyRate.Select(user, employeeId);
        }
        #endregion

        #region employee paycode processed
        public EmployeePaycodeProcessedCollection SelectEmployeePaycodeProcessed(DatabaseUser user, long payrollProcessId, long employeeId)
        {
            return _sqlServerEmployeePaycodeProcessed.Select(user, payrollProcessId, employeeId);
        }
        public void InsertEmployeePaycodeProcessed(DatabaseUser user, long[] items, long payrollProcessId)
        {
            _sqlServerEmployeePaycodeProcessed.Insert(user, items, payrollProcessId);
        }
        public void DeleteEmployeePaycodeProcessed(DatabaseUser user, long payrollProcessId)
        {
            _sqlServerEmployeePaycodeProcessed.Delete(user, payrollProcessId);
        }
        #endregion

        #region employee termination
        //termination other
        public EmployeeTerminationOtherCollection GetEmployeeTerminationOther(DatabaseUser user, long employeePositionId)
        {
            return _sqlServerEmployeeTerminationOther.Select(user, employeePositionId);
        }
        public EmployeeTerminationOther InsertEmployeeTerminationOther(DatabaseUser user, EmployeeTerminationOther item)
        {
            return _sqlServerEmployeeTerminationOther.Insert(user, item);
        }
        public void DeleteEmployeeTerminationOther(DatabaseUser user, EmployeeTerminationOther item)
        {
            _sqlServerEmployeeTerminationOther.Delete(user, item);
        }
        public void UpdateEmployeeTerminationOther(DatabaseUser user, EmployeeTerminationOther item)
        {
            _sqlServerEmployeeTerminationOther.Update(user, item);
        }

        //termination roe
        public EmployeeTerminationRoeCollection GetEmployeeTerminationRoe(DatabaseUser user, long employeePositionId)
        {
            return _sqlServerEmployeeTerminationRoe.Select(user, employeePositionId);
        }
        public void UpdateEmployeeTerminationRoe(DatabaseUser user, EmployeeTerminationRoe item)
        {
            _sqlServerEmployeeTerminationRoe.Update(user, item);
        }
        public void DeleteEmployeeTerminationRoe(DatabaseUser user, EmployeeTerminationRoe item)
        {
            _sqlServerEmployeeTerminationRoe.Delete(user, item);
        }
        public EmployeeTerminationRoe InsertEmployeeTerminationRoe(DatabaseUser user, EmployeeTerminationRoe item)
        {
            return _sqlServerEmployeeTerminationRoe.Insert(user, item);
        }
        #endregion

        #region employee position
        
        public EmployeePositionEmployeeIdTransactionDateCollection GetBatchPositionsByEmployeeIdAndTransactionDate(DatabaseUser user, Dictionary<string, BusinessLayer.BusinessObjects.PayrollTransaction> data)
        {
            return _sqlEmployeePosition.GetBatchPositionsByEmployeeIdAndTransactionDate(user, data);
        }

        public EmployeePositionCollection GetEmployeePosition(DatabaseUser user, EmployeePositionCriteria criteria)
        {
            return _sqlEmployeePosition.Select(user, criteria);
        }

        public EmployeePositionCollection GetBatchEmployeePosition(DatabaseUser user, EmployeePositionCollection positionIds, bool securityOverrideFlag)
        {
            return _sqlEmployeePosition.SelectBatchEmployeePosition(user, positionIds, securityOverrideFlag);
        }
        public EmployeePositionWorkdayCollection GetBatchEmployeePostionWorkday(DatabaseUser user, EmployeePositionCollection positionIds)
        {
            return _sqlEmployeePositionWorkday.SelectBatchEmployeePositionWorkday(user, positionIds);
        }
        public EmployeePositionOrganizationUnitCollection GetBatchEmployeePositionOrganizationUnitLevelSummary(DatabaseUser user, EmployeePositionCollection positionIds)
        {
            return _sqlEmployeePosition.SelectBatchEmployeePositionOrganizationUnitLevelSummary(user, positionIds);
        }

        public EmployeePositionSecondaryCollection GetBatchEmployeeSecondaryPosition(DatabaseUser user, EmployeePositionCollection positionIds, string languageCode)
        {
            return _sqlServerEmployeePositionSecondary.SelectBatchEmployeePositionSecondaryPosition(user, positionIds, languageCode);
        }
        public EmployeePositionSecondaryOrganizationUnitCollection GetBatchEmployeePositionSecondaryOrganizationUnit(DatabaseUser user, EmployeePositionSecondaryCollection secondaryPositions)
        {
            return _sqlServerEmployeePositionSecondaryOrganizationUnit.SelectBatchEmployeePositionOrganizationUnit(user, secondaryPositions);
        }
        public EmployeePositionCollection GetBatchEmployeePosition(DatabaseUser user, SalaryEmployeeCollection tempEmployees, bool securityOverrideFlag)
        {
            return _sqlEmployeePosition.SelectBatchEmployeePosition(user, tempEmployees, securityOverrideFlag);
        }
        public WorkLinksEmployeePositionCollection GetBatchLatestEmployeePosition(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            return _sqlEmployeePosition.SelectLatestBatch(user, importExternalIdentifiers);
        }
        public EmployeePositionSummaryCollection GetEmployeePositionSummary(DatabaseUser user, EmployeePositionCriteria criteria)
        {
            return _sqlEmployeePosition.SelectEmployeePositionSummary(user, criteria);
        }
        public EmployeePositionCollection CheckForRehiresWithArrearsRecordsForNewPeriod(DatabaseUser user, long payrollPeriodId, bool securityOverride)
        {
            return _sqlEmployeePosition.CheckForRehiresWithArrearsRecordsForNewPeriod(user, payrollPeriodId, securityOverride);
        }
        public PrimarySecondaryEmployeePositionCollection GetPrimarySecondaryEmployeePosition(DatabaseUser user, long payrollProcessId, DateTime cutoffDate)
        {
            return _sqlPrimarySecondaryEmployeePosition.Select(user, payrollProcessId, cutoffDate);
        }
        public void UpdateEmployeePosition(DatabaseUser user, EmployeePosition item)
        {
            _sqlEmployeePosition.Update(user, item);
        }
        public void UpdatePendingRoesIfNotCompletedAndRehired(DatabaseUser user, EmployeePosition item)
        {
            _sqlEmployeePosition.UpdatePendingRoesIfNotCompletedAndRehired(user, item);
        }
        public void DeleteEmployeePosition(DatabaseUser user, EmployeePosition item)
        {
            _sqlEmployeePosition.Delete(user, item);
        }
        public void DeleteEmployeeTermination(DatabaseUser user, long employeePositionId)
        {
            _sqlEmployeePosition.DeleteEmployeeTermination(user, employeePositionId);
        }

        public void BatchInsertUpdateEmployeePosition(DatabaseUser user, EmployeePositionCollection items, long payrollProcessId)
        {
            _sqlEmployeePosition.BatchInsertUpdate(user, items, payrollProcessId);
        }

        public void InsertEmployeePosition(DatabaseUser user, EmployeePosition item)
        {
            _sqlEmployeePosition.Insert(user, item);
        }
        public void ImportEmployeePosition(DatabaseUser user, WorkLinksEmployeePositionCollection items)
        {
            _sqlEmployeePosition.ImportEmployeePosition(user, items);
        }
        public SummaryDataForRoeInsert GetSummaryDataForROEInsert(DatabaseUser user, long employeePositionId)
        {
            return _sqlEmployeePosition.GetSummaryDataForROEInsert(user, employeePositionId);
        }
        #endregion

        #region employee position workday
        public EmployeePositionWorkdayCollection GetEmployeePositionWorkday(DatabaseUser user, long employeePositionId)
        {
            return _sqlEmployeePositionWorkday.Select(user, employeePositionId);
        }
        public EmployeePositionWorkdayCollection GetEmployeePositionWorkdayBatch(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            return _sqlEmployeePositionWorkday.SelectBatch(user, importExternalIdentifiers);
        }
        public EmployeePositionWorkday InsertEmployeePositionWorkday(DatabaseUser user, EmployeePositionWorkday workday)
        {
            return _sqlEmployeePositionWorkday.Insert(user, workday);
        }
        public void DeleteEmployeePositionWorkday(DatabaseUser user, EmployeePositionWorkday workday)
        {
            _sqlEmployeePositionWorkday.Delete(user, workday);
        }
        public void UpdateEmployeePositionWorkday(DatabaseUser user, EmployeePositionWorkday workday)
        {
            _sqlEmployeePositionWorkday.Update(user, workday);
        }
        #endregion

        #region employee position organization unit level report
        public EmployeePositionOrganizationUnitCollection GetEmployeePositionOrganizationUnitLevelSummary(DatabaseUser user, long employeePositionId)
        {
            return _sqlEmployeePosition.SelectEmployeePositionOrganizationUnitLevelSummary(user, employeePositionId);
        }
        public String SelectEmployeePositionOrganizationUnitLevelHrxmlMatch(DatabaseUser user, String costCenter)
        {
            return _sqlEmployeePosition.SelectEmployeePositionOrganizationUnitLevelHrxmlMatch(user, costCenter);
        }
        #endregion

        #region employee position organization unit level
        public EmployeePositionOrganizationUnitCollection GetEmployeePositionOrganizationUnitBatch(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            return _sqlServerEmployeePositionOrganizationUnit.SelectBatch(user, importExternalIdentifiers);
        }
        public void UpdateEmployeePositionOrganizationUnit(DatabaseUser user, EmployeePositionOrganizationUnit item)
        {
            _sqlServerEmployeePositionOrganizationUnit.Update(user, item);
        }
        public void ResetDataAttributes(DatabaseUser user, long employeeId)
        {
            _sqlEmployeePosition.ResetDataAttributes(user, employeeId);
        }
        public DateTime? IsProcessDateOpen(DatabaseUser user, long? payrollProcessId)
        {
            return _sqlEmployeePosition.IsProcessDateOpen(user, payrollProcessId);
        }
        public void DeleteEmployeePositionOrganizationUnit(DatabaseUser user, EmployeePositionOrganizationUnit item)
        {
            _sqlServerEmployeePositionOrganizationUnit.Delete(user, item);
        }
        public void InsertEmployeePositionOrganizationUnit(DatabaseUser user, EmployeePositionOrganizationUnit item)
        {
            _sqlServerEmployeePositionOrganizationUnit.Insert(user, item);
        }
        #endregion

        #region employee position secondary

        public EmployeePositionSecondaryCollection GetEmployeePositionSecondaryBatch(DatabaseUser user, List<string> importExternalIdentifiers)
        {
            return _sqlServerEmployeePositionSecondary.SelectBatch(user, importExternalIdentifiers);
        }
        public EmployeePositionSecondaryCollection GetEmployeePositionSecondary(DatabaseUser user, long? employeePositionSecondaryId, long? employeePositionId, string languageCode, string organizationUnit)
        {
            return _sqlServerEmployeePositionSecondary.Select(user, employeePositionSecondaryId, employeePositionId, languageCode, organizationUnit);
        }
        public EmployeePositionSecondary InsertEmployeePositionSecondary(DatabaseUser user, EmployeePositionSecondary item)
        {
            return _sqlServerEmployeePositionSecondary.Insert(user, item);
        }
        public EmployeePositionSecondary UpdateEmployeePositionSecondary(DatabaseUser user, EmployeePositionSecondary item)
        {
            return _sqlServerEmployeePositionSecondary.Update(user, item);
        }
        public void DeleteEmployeePositionSecondary(DatabaseUser user, EmployeePositionSecondary item)
        {
            _sqlServerEmployeePositionSecondary.Delete(user, item);
        }
        #endregion

        #region SalaryPlanGradeAccumulation
        public void DeleteSalaryPlanGradeAccumulationByCreateEmployeePositionId(DatabaseUser user, long createEmployeePositionId)
        {
            _sqlServerSalaryPlanGradeAccumulation.DeleteByCreateEmployeePositionId(user, createEmployeePositionId);
        }
        #endregion

        #region salary plan grade and effective date detail
        public SalaryPlanGradeAndEffectiveDetailCollection GetSalaryPlanGradeAndEffectiveDetail(DatabaseUser user, long salaryPlanId)
        {
            return _sqlServerSalaryPlanGradeAndEffectiveDetail.Select(user, salaryPlanId);
        }
        public SalaryPlanGradeMinEffectiveDateCollection SelectMinEffectiveDate(DatabaseUser user, long organizationUnitId)
        {
            return _sqlServerSalaryPlanGradeAndEffectiveDetail.SelectMinEffectiveDate(user, organizationUnitId);
        }
        public void AddNewSalaryPlanGradeAndSteps(DatabaseUser user, SalaryPlanGradeAndEffectiveDetail item)
        {
            _sqlServerSalaryPlanGradeAndEffectiveDetail.AddNewSalaryPlanGradeAndSteps(user, item);
        }
        #endregion

        #region employee position secondary organization unit
        public EmployeePositionSecondaryOrganizationUnitCollection GetEmployeePositionSecondaryOrganizationUnit(DatabaseUser user, long? employeePositionSecondaryId)
        {
            return _sqlServerEmployeePositionSecondaryOrganizationUnit.Select(user, employeePositionSecondaryId);
        }
        public void DeleteEmployeePositionSecondaryOrganizationUnit(DatabaseUser user, EmployeePositionSecondaryOrganizationUnit item)
        {
            _sqlServerEmployeePositionSecondaryOrganizationUnit.Delete(user, item);
        }
        #endregion

        #region employee wsib health and safety report
        public EmployeeWsibHealthAndSafetyReportCollection GetEmployeeWsibHealthAndSafetyReport(DatabaseUser user, long employeeId)
        {
            return _sqlServerEmployeeWsibHealthAndSafetyReport.Select(user, employeeId);
        }
        public EmployeeWsibHealthAndSafetyReportPersonCollection GetEmployeeWsibHealthAndSafetyReportPerson(DatabaseUser user, long employeeId)
        {
            return _sqlServerEmployeeWsibHealthAndSafetyReport.SelectPerson(user, employeeId);
        }
        public void InsertEmployeeWsibHealthAndSafetyReport(DatabaseUser user, EmployeeWsibHealthAndSafetyReport item)
        {
            _sqlServerEmployeeWsibHealthAndSafetyReport.Insert(user, item);
        }
        public void UpdateEmployeeWsibHealthAndSafetyReport(DatabaseUser user, EmployeeWsibHealthAndSafetyReport item)
        {
            _sqlServerEmployeeWsibHealthAndSafetyReport.Update(user, item);
        }
        public void DeleteEmployeeWsibHealthAndSafetyReport(DatabaseUser user, EmployeeWsibHealthAndSafetyReport item)
        {
            _sqlServerEmployeeWsibHealthAndSafetyReport.Delete(user, item);
        }
        #endregion

        #region statutory deduction
        public StatutoryDeductionCollection GetStatutoryDeduction(DatabaseUser user, long id, bool securityOverrideFlag)
        {
            return _sqlServerStatutoryDeduction.Select(user, id, securityOverrideFlag);
        }
        public void InsertStatutoryDeduction(DatabaseUser user, StatutoryDeduction item)
        {
            _sqlServerStatutoryDeduction.Insert(user, item);
        }
        public void UpdateStatutoryDeduction(DatabaseUser user, StatutoryDeduction item)
        {
            _sqlServerStatutoryDeduction.Update(user, item);
        }
        public void DeleteStatutoryDeduction(DatabaseUser user, StatutoryDeduction item)
        {
            _sqlServerStatutoryDeduction.Delete(user, item);
        }
        public void ImportStatutoryDeduction(DatabaseUser user, WorklinksStatutoryDeductionCollection items)
        {
            _sqlServerStatutoryDeduction.ImportStatDeduction(user, items);
        }
        #endregion

        #region statutory deduction barbados
        public void InsertStatutoryDeductionBarbados(DatabaseUser user, StatutoryDeduction item)
        {
            _sqlServerStatutoryDeduction.InsertBarbados(user, item);
        }
        public void UpdateStatutoryDeductionBarbados(DatabaseUser user, StatutoryDeduction item)
        {
            _sqlServerStatutoryDeduction.UpdateBarbados(user, item);
        }
        public void DeleteStatutoryDeductionBarbados(DatabaseUser user, StatutoryDeduction item)
        {
            _sqlServerStatutoryDeduction.DeleteBarbados(user, item);
        }
        #endregion

        #region statutory deduction saint lucia
        public void InsertStatutoryDeductionSaintLucia(DatabaseUser user, StatutoryDeduction item)
        {
            _sqlServerStatutoryDeduction.InsertSaintLucia(user, item);
        }
        public void UpdateStatutoryDeductionSaintLucia(DatabaseUser user, StatutoryDeduction item)
        {
            _sqlServerStatutoryDeduction.UpdateSaintLucia(user, item);
        }
        public void DeleteStatutoryDeductionSaintLucia(DatabaseUser user, StatutoryDeduction item)
        {
            _sqlServerStatutoryDeduction.DeleteSaintLucia(user, item);
        }
        #endregion

        #region statutory deduction trinidad
        public void InsertStatutoryDeductionTrinidad(DatabaseUser user, StatutoryDeduction item)
        {
            _sqlServerStatutoryDeduction.InsertTrinidad(user, item);
        }
        public void UpdateStatutoryDeductionTrinidad(DatabaseUser user, StatutoryDeduction item)
        {
            _sqlServerStatutoryDeduction.UpdateTrinidad(user, item);
        }
        public void DeleteStatutoryDeductionTrinidad(DatabaseUser user, StatutoryDeduction item)
        {
            _sqlServerStatutoryDeduction.DeleteTrinidad(user, item);
        }
        #endregion

        #region statutory deduction jamaica
        public void InsertStatutoryDeductionJamaica(DatabaseUser user, StatutoryDeduction item)
        {
            _sqlServerStatutoryDeduction.InsertJamaica(user, item);
        }
        public void UpdateStatutoryDeductionJamaica(DatabaseUser user, StatutoryDeduction item)
        {
            _sqlServerStatutoryDeduction.UpdateJamaica(user, item);
        }
        public void DeleteStatutoryDeductionJamaica(DatabaseUser user, StatutoryDeduction item)
        {
            _sqlServerStatutoryDeduction.DeleteJamaica(user, item);
        }
        #endregion

        #region statutory deduction defaults
        public StatutoryDeductionDefaultCollection GetStatutoryDeductionDefaults(DatabaseUser user, String codeProvinceStateCd)
        {
            return _sqlServerStatutoryDeductionDefault.Select(user, codeProvinceStateCd);
        }
        #endregion

        #region business number
        public BusinessNumberCollection GetBusinessNumber(DatabaseUser user, long? businessNumberId)
        {
            return _sqlServerBusinessNumber.Select(user, businessNumberId);
        }
        public string GetBusinessNumberFromEmployeeNumber(DatabaseUser user, string employerNumber)
        {
            return _sqlServerBusinessNumber.SelectByEmployerNumber(user, employerNumber);
        }
        public BusinessNumber InsertBusinessNumber(DatabaseUser user, BusinessNumber businessNumber)
        {
            return _sqlServerBusinessNumber.Insert(user, businessNumber);
        }
        public void UpdateBusinessNumber(DatabaseUser user, BusinessNumber businessNumber)
        {
            _sqlServerBusinessNumber.Update(user, businessNumber);
        }
        public void DeleteBusinessNumber(DatabaseUser user, BusinessNumber businessNumber)
        {
            _sqlServerBusinessNumber.Delete(user, businessNumber);
        }
        #endregion

        #region EmployeeEmploymentInformation
        public EmployeeEmploymentInformationCollection GetEmployeeEmploymentInformation(DatabaseUser user, long id, bool securityOverrideFlag)
        {
            return _sqlServerEmployeeEmploymentInformation.Select(user, id, securityOverrideFlag);
        }
        public WorkLinksEmployeeEmploymentInformationCollection GetEmployeeEmploymentInformationBatch(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            return _sqlServerEmployeeEmploymentInformation.SelectBatch(user, importExternalIdentifiers);
        }
        public void UpdateEmployeeEmploymentInformation(DatabaseUser user, EmployeeEmploymentInformation item)
        {
            _sqlServerEmployeeEmploymentInformation.Update(user, item);
        }
        public void DeleteEmployeeEmploymentInformation(DatabaseUser user, EmployeeEmploymentInformation item)
        {
            _sqlServerEmployeeEmploymentInformation.Delete(user, item);
        }
        public void InsertEmployeeEmploymentInformation(DatabaseUser user, EmployeeEmploymentInformation item)
        {
            _sqlServerEmployeeEmploymentInformation.Insert(user, item);
        }
        public void ImportEmployeeEmploymentInformation(DatabaseUser user, WorkLinksEmployeeEmploymentInformationCollection items)
        {
            _sqlServerEmployeeEmploymentInformation.Import(user, items);
        }
        #endregion

        #region GetEmployeeIds from payroll process
        public String[] GetEmployeesByPayProcess(DatabaseUser user, long payrollProcessId)
        {
            return _sqlGetEmployeeIds.GetEmployeesByPayProcess(user, payrollProcessId);
        }
        #endregion

        #region vendor
        public VendorCollection GetVendor(DatabaseUser user, long VendorId)
        {
            return _sqlServerVendor.Select(user, VendorId); //Stacy - supply -1 when we want all vendors returned rather than a specific one.  select proc contains the logic
        }
        public void UpdateVendor(DatabaseUser user, Vendor vendor)
        {
            _sqlServerVendor.Update(user, vendor);
        }
        public Vendor InsertVendor(DatabaseUser user, Vendor vendor)
        {
            return _sqlServerVendor.Insert(user, vendor);
        }
        public void DeleteVendor(DatabaseUser user, Vendor vendor)
        {
            _sqlServerVendor.Delete(user, vendor);
        }
        #endregion

        #region get year end negative amount employee
        public long? GetYearEndNegativeAmountEmployee(DatabaseUser user, Decimal year, int revision)
        {
            return _sqlGetYearEndNegativeAmountEmployee.SelectYearEndNegativeAmountEmployee(user, year, revision);
        }
        #endregion

        #region employee photo
        public EmployeePhotoCollection GetEmployeePhoto(DatabaseUser user, long employeeId)
        {
            return _sqlEmployeePhoto.Select(user, employeeId);
        }
        public EmployeePhoto InsertEmployeePhoto(DatabaseUser user, EmployeePhoto photo)
        {
            return _sqlEmployeePhoto.Insert(user, photo);
        }
        public void UpdateEmployeePhoto(DatabaseUser user, EmployeePhoto photo)
        {
            _sqlEmployeePhoto.Update(user, photo);
        }

        public void DeleteEmployeePhoto(DatabaseUser user, EmployeePhoto photo)
        {
            _sqlEmployeePhoto.Delete(user, photo);
        }
        #endregion

        #region attachment
        public AttachmentCollection GetAttachment(DatabaseUser user, long attachmentId)
        {
            return _sqlAttachment.Select(user, attachmentId);
        }
        public Attachment InsertAttachment(DatabaseUser user, Attachment attachment)
        {
            return _sqlAttachment.Insert(user, attachment);
        }
        public void UpdateAttachment(DatabaseUser user, Attachment attachment)
        {
            _sqlAttachment.Update(user, attachment);
        }
        public void DeleteAttachment(DatabaseUser user, Attachment attachment)
        {
            _sqlAttachment.Delete(user, attachment);
        }
        #endregion

        #region employee remittance stub
        public EmployeeRemittanceStubCollection GetEmployeeRemittanceStub(DatabaseUser user, long employeePaycodeId)
        {
            return _sqlServerEmployeeRemittanceStub.Select(user, employeePaycodeId);
        }
        public EmployeeRemittanceStub InsertEmployeeRemittanceStub(DatabaseUser user, EmployeeRemittanceStub stub)
        {
            return _sqlServerEmployeeRemittanceStub.Insert(user, stub);
        }
        public void UpdateEmployeeRemittanceStub(DatabaseUser user, EmployeeRemittanceStub stub)
        {
            _sqlServerEmployeeRemittanceStub.Update(user, stub);
        }
        public void DeleteEmployeeRemittanceStub(DatabaseUser user, EmployeeRemittanceStub stub)
        {
            _sqlServerEmployeeRemittanceStub.Delete(user, stub);
        }
        #endregion

        #region global employee paycode
        public EmployeePaycodeCollection GetGlobalEmployeePaycode(DatabaseUser user, long? globalEmployeePaycodeId)
        {
            return _sqlGlobalEmployeePaycode.Select(user, globalEmployeePaycodeId);
        }
        public EmployeePaycode InsertGlobalEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            return _sqlGlobalEmployeePaycode.Insert(user, employeePaycode);
        }
        public void UpdateGlobalEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            _sqlGlobalEmployeePaycode.Update(user, employeePaycode);
        }
        public void DeleteGlobalEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            _sqlGlobalEmployeePaycode.Delete(user, employeePaycode);
        }
        #endregion

        #region statutory holiday date
        public StatutoryHolidayDateCollection GetStatutoryHolidayDateReport(DatabaseUser user, StatutoryDeductionCriteria criteria)
        {
            return _sqlServerStatutoryHolidayDate.Report(user, criteria);
        }
        public StatutoryHolidayDateCollection GetStatutoryHolidayDate(DatabaseUser user, long? statutoryHolidayDateId)
        {
            return _sqlServerStatutoryHolidayDate.Select(user, statutoryHolidayDateId);
        }
        public StatutoryHolidayDate InsertStatutoryHolidayDate(DatabaseUser user, StatutoryHolidayDate statutoryHolidayDate)
        {
            return _sqlServerStatutoryHolidayDate.Insert(user, statutoryHolidayDate);
        }
        public void UpdateStatutoryHolidayDate(DatabaseUser user, StatutoryHolidayDate statutoryHolidayDate)
        {
            _sqlServerStatutoryHolidayDate.Update(user, statutoryHolidayDate);
        }
        public void DeleteStatutoryHolidayDate(DatabaseUser user, StatutoryHolidayDate statutoryHolidayDate)
        {
            _sqlServerStatutoryHolidayDate.Delete(user, statutoryHolidayDate);
        }
        #endregion

        #region employee custom field
        public EmployeeCustomFieldCollection GetEmployeeCustomField(DatabaseUser user, long? employeeId)
        {
            return _sqlServerEmployeeCustomField.Select(user, employeeId);
        }
        public EmployeeCustomField InsertEmployeeCustomField(DatabaseUser user, EmployeeCustomField item)
        {
            return _sqlServerEmployeeCustomField.Insert(user, item);
        }
        public void UpdateEmployeeCustomField(DatabaseUser user, EmployeeCustomField item)
        {
            _sqlServerEmployeeCustomField.Update(user, item);
        }
        public void DeleteEmployeeCustomField(DatabaseUser user, EmployeeCustomField item)
        {
            _sqlServerEmployeeCustomField.Delete(user, item);
        }
        #endregion

        #region employee custom field config
        public EmployeeCustomFieldConfigCollection GetEmployeeCustomFieldConfig(DatabaseUser user, long? employeeCustomFieldConfigId)
        {
            return _sqlServerEmployeeCustomFieldConfig.Select(user, employeeCustomFieldConfigId);
        }
        public EmployeeCustomFieldConfig InsertEmployeeCustomFieldConfig(DatabaseUser user, EmployeeCustomFieldConfig item)
        {
            return _sqlServerEmployeeCustomFieldConfig.Insert(user, item);
        }
        public void UpdateEmployeeCustomFieldConfig(DatabaseUser user, EmployeeCustomFieldConfig item)
        {
            _sqlServerEmployeeCustomFieldConfig.Update(user, item);
        }
        public void DeleteEmployeeCustomFieldConfig(DatabaseUser user, EmployeeCustomFieldConfig item)
        {
            _sqlServerEmployeeCustomFieldConfig.Delete(user, item);
        }
        #endregion

        #region salary plan
        public SalaryPlanCollection GetSalaryPlan(DatabaseUser user, long? salaryPlanId)
        {
            return _sqlServerSalaryPlan.Select(user, salaryPlanId);
        }
        public SalaryPlanOrganizationUnitCollection GetSalaryPlanOrganizationUnit(DatabaseUser user, string organizationUnit)
        {
            return _sqlServerSalaryPlan.SelectOrganizationUnit(user, organizationUnit);
        }
        #endregion

        #region salary plan grade
        public SalaryPlanGradeCollection GetSalaryPlanGrade(DatabaseUser user, long? salaryPlanGradeId, long? salaryPlanId, string languageCode)
        {
            return _sqlServerSalaryPlanGrade.Select(user, salaryPlanGradeId, salaryPlanId, languageCode);
        }

        public SalaryPlanGradeOrgUnitDescriptionComboCollection PopulateComboBoxWithSalaryPlanGrades(DatabaseUser user, long salaryPlanId, long organizationUnitLevelId)
        {
            return _sqlServerSalaryPlanGrade.PopulateComboBoxWithSalaryPlanGrades(user, salaryPlanId, organizationUnitLevelId);
        }
        #endregion

        #region salary plan grade step
        public SalaryPlanGradeStepCollection GetSalaryPlanGradeStep(DatabaseUser user, long? salaryPlanGradeStepId, long? salaryPlanGradeId)
        {
            return _sqlServerSalaryPlanGradeStep.Select(user, salaryPlanGradeStepId, salaryPlanGradeId);
        }
        public SalaryPlanGradeStepDetailCollection GetSalaryPlanGradeStepDetail(DatabaseUser user, long? salaryPlanGradeStepDetailId, long? salaryPlanGradeStepId)
        {
            return _sqlServerSalaryPlanGradeStep.SelectDetail(user, salaryPlanGradeStepDetailId, salaryPlanGradeStepId);
        }
        #endregion

        #region salary plan grade accumulation total
        public SalaryPlanGradeAccumulationTotalsCollection GetSalaryPlanGradeAccumulationTotals(DatabaseUser user, long employeeId)
        {
            return _sqlServerSalaryPlanGradeAccumulation.SelectTotals(user, employeeId);
        }
            #endregion
        #region employee benefit
        public EmployeeBenefitCollection GetEmployeeBenefit(DatabaseUser user, long employeeId, bool securityOverrideFlag)
        {
            return _sqlServerEmployeeBenefit.Select(user, employeeId, securityOverrideFlag);
        }
        public EmployeeBenefitCollection GetEmployeeBenefit(DatabaseUser user, long employeeId, long? employeeBenefitId, bool securityOverrideFlag)
        {
            return _sqlServerEmployeeBenefit.Select(user, employeeId, employeeBenefitId, securityOverrideFlag);
        }
        public EmployeeBenefit InsertEmployeeBenefit(DatabaseUser user, EmployeeBenefit employeeBenefit)
        {
            return _sqlServerEmployeeBenefit.Insert(user, employeeBenefit);
        }
        public void UpdateEmployeeBenefit(DatabaseUser user, EmployeeBenefit employeeBenefit)
        {
            _sqlServerEmployeeBenefit.Update(user, employeeBenefit);
        }
        public void DeleteEmployeeBenefit(DatabaseUser user, EmployeeBenefit employeeBenefit)
        {
            _sqlServerEmployeeBenefit.Delete(user, employeeBenefit);
        }
        #endregion
    }
}