﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess
{
    public class DynamicsAccess
    {
        #region fields
        private SqlServer.SqlServerVendor _sqlServerVendor = null;
        private SqlServer.SqlServerWSIBSummary _sqlServerWSIBSummary = null;
        #endregion

        #region properties
        private SqlServer.SqlServerVendor SqlServerVendor
        {
            get
            {
                if (_sqlServerVendor == null)
                    _sqlServerVendor = new SqlServer.SqlServerVendor();

                return _sqlServerVendor;
            }
        }
        private SqlServer.SqlServerWSIBSummary SqlServerWSIBSummary
        {
            get
            {
                if (_sqlServerWSIBSummary == null)
                    _sqlServerWSIBSummary = new SqlServer.SqlServerWSIBSummary();

                return _sqlServerWSIBSummary;
            }
        }
        #endregion

        #region Vendor
        public VendorCollection GetVendor(DatabaseUser user, long vendorId)
        {
            return SqlServerVendor.Select(user, vendorId);
        }
        #endregion

        #region WSIB
        public WSIBSummaryCollection GetWSIBSummary(DatabaseUser user, DateTime cutoffDate)
        {
            return SqlServerWSIBSummary.SelectWSIBSummary(user, cutoffDate);
        }
        #endregion
    }
}