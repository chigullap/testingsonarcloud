﻿using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.DataLayer.DataAccess.SqlServer;

namespace WorkLinks.DataLayer.DataAccess
{
    public class SalaryPlanAccess
    {
        private readonly SqlServerSalaryPlanGradeRule _salaryPlanGradeRule = new SqlServerSalaryPlanGradeRule();
        private readonly SqlServerSalaryPlanGradeRuleCode _salaryPlanGradeRuleCode = new SqlServerSalaryPlanGradeRuleCode();

        public SalaryPlanGradeRuleCollection GetSalaryPlanGradeRules(DatabaseUser user)
        {
            return _salaryPlanGradeRule.Select(user);
        }

        public SalaryPlanGradeRule InsertPlanGradeRule(DatabaseUser user, SalaryPlanGradeRule item)
        {
            return _salaryPlanGradeRule.Insert(user, item);
        }

        public void UpdatePlanGradeRule(DatabaseUser user, SalaryPlanGradeRule item)
        {
            _salaryPlanGradeRule.Update(user, item);
        }

        public SalaryPlanGradeRuleCodeCollection GetSalaryPlanGradeRuleCodes(DatabaseUser user)
        {
            return _salaryPlanGradeRuleCode.Select(user);
        }

        public SalaryPlanGradeRuleCode InsertGradeRuleCode(DatabaseUser user, SalaryPlanGradeRuleCode code)
        {
            return _salaryPlanGradeRuleCode.Insert(user, code);
        }

        public SalaryPlanGradeRuleCode UpdateGradeRuleCode(DatabaseUser user, SalaryPlanGradeRuleCode code)
        {
            return _salaryPlanGradeRuleCode.Update(user, code);
        }

        public void DeleteGradeRuleCode(DatabaseUser user, SalaryPlanGradeRuleCode code)
        {
            _salaryPlanGradeRuleCode.Delete(user, code);
        }
    }
}
