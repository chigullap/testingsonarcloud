﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Import;

namespace WorkLinks.DataLayer.DataAccess
{
    public class XmlAccess
    {
        #region fields
        private SqlServer.SqlServerImportExport _clientImportExport;
        private SqlServer.SqlServerImportExportXml _clientImportExportXml;
        private SqlServer.SqlServerImportExportLog _clientImportExportLog;
        private SqlServer.SqlServerExternalPaycodeImportMap _clientExternalPaycodeImportMap;
        #endregion 

        #region properties
        private SqlServer.SqlServerImportExport ClientImportExport
        {
            get
            {
                if (_clientImportExport == null)
                    _clientImportExport = new SqlServer.SqlServerImportExport();
                return _clientImportExport;
            }
        }
        private SqlServer.SqlServerImportExportXml ClientImportExportXml
        {
            get
            {
                if (_clientImportExportXml == null)
                    _clientImportExportXml = new SqlServer.SqlServerImportExportXml();
                return _clientImportExportXml;
            }
        }
        private SqlServer.SqlServerImportExportLog ClientImportExportLog
        {
            get
            {
                if (_clientImportExportLog == null)
                    _clientImportExportLog = new SqlServer.SqlServerImportExportLog();
                return _clientImportExportLog;
            }
        }
        private SqlServer.SqlServerExternalPaycodeImportMap ClientExternalPaycodeImportMap
        {
            get
            {
                if (_clientExternalPaycodeImportMap == null)
                    _clientExternalPaycodeImportMap = new SqlServer.SqlServerExternalPaycodeImportMap();
                return _clientExternalPaycodeImportMap;
            }
        }

        #endregion

        public ImportExportSummaryCollection GetImportExportSummary(WLP.BusinessLayer.BusinessObjects.DatabaseUser user)
        {
            return ClientImportExport.SelectImportExportSummary(user);
        }

        public ImportExportCollection GetImportExport(DatabaseUser user, long? importExportId, String label)
        {
            return ClientImportExport.Select(user, importExportId, label);
        }

        public ImportExportXmlSummaryCollection GetImportExportXmlSummary(DatabaseUser user, long importExportId)
        {
            return ClientImportExportXml.SelectImportExportXmlSummary(user, importExportId, null);
        }
        public ImportExportXmlSummaryCollection GetImportExportXmlSummary(DatabaseUser user, String label)
        {
            return ClientImportExportXml.SelectImportExportXmlSummary(user, null, label);
        }

        #region import_export_log

        public void UpdateImportExportLog(DatabaseUser user, ImportExportLog item)
        {
            ClientImportExportLog.Update(user, item);
        }
        public void InsertImportExportLog(DatabaseUser user, ImportExportLog item)
        {
            ClientImportExportLog.Insert(user, item);
        }
        public ImportExportLogCollection GetImportExportLog(DatabaseUser user, Guid uniqueIdentifier)
        {
            return ClientImportExportLog.Select(user, uniqueIdentifier);
        }
        #endregion

        public ExternalPaycodeImportMapCollection GetExternalPaycodeImportMap(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String externalPaycodeImportName)
        {
            return ClientExternalPaycodeImportMap.Select(user, externalPaycodeImportName);
        }



      

    }
}
