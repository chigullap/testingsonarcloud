﻿using System;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess
{
    public class ReportAccess
    {
        #region fields
        private SqlServer.SqlServerReport _sqlServerReport = null;
        private SqlServer.SqlServerReportParameter _sqlServerReportParameter = null;
        private SqlServer.SqlServerReportLanguage _sqlServerReportLanguage = null;
        private SqlServer.SqlServerT4T4aR1MassPrint _sqlServerT4T4aR1MassPrint = null;
        private SqlServer.SqlServerGLCsv _sqlServerGLCsv = null;
        private SqlServer.SqlServerCanadaRevenueAgencyBondExport _sqlServerCanadaRevenueAgencyBondExport = null;
        private SqlServer.SqlServerCanadaRevenueAgencyBondExportFile _sqlServerCanadaRevenueAgencyBondExportFile = null;
        private SqlServer.SqlServerReportExport _sqlServerReportExport = new SqlServer.SqlServerReportExport();
        private SqlServer.SqlServerGlSap _sqlServerGlSap = new SqlServer.SqlServerGlSap();
        private SqlServer.SqlServerGlSage _sqlServerGlSage = new SqlServer.SqlServerGlSage();
        private SqlServer.SqlServerGlPex _sqlServerGlPex = new SqlServer.SqlServerGlPex();
        //throwaway class below
        private SqlServer.SqlServerSTEPayrollEngineTesting _sqlServerSTEPayrollEngineTesting = new SqlServer.SqlServerSTEPayrollEngineTesting();
        #endregion

        #region properties
        private SqlServer.SqlServerReport SqlServerReport
        {
            get
            {
                if (_sqlServerReport == null)
                    _sqlServerReport = new SqlServer.SqlServerReport();

                return _sqlServerReport;
            }
        }
        private SqlServer.SqlServerReportParameter SqlServerReportParameter
        {
            get
            {
                if (_sqlServerReportParameter == null)
                    _sqlServerReportParameter = new SqlServer.SqlServerReportParameter();

                return _sqlServerReportParameter;
            }
        }
        private SqlServer.SqlServerReportLanguage SqlServerReportLanguage
        {
            get
            {
                if (_sqlServerReportLanguage == null)
                    _sqlServerReportLanguage = new SqlServer.SqlServerReportLanguage();
                return _sqlServerReportLanguage;
            }
        }
        private SqlServer.SqlServerT4T4aR1MassPrint SqlServerT4T4aR1MassPrint
        {
            get
            {
                if (_sqlServerT4T4aR1MassPrint == null)
                    _sqlServerT4T4aR1MassPrint = new SqlServer.SqlServerT4T4aR1MassPrint();

                return _sqlServerT4T4aR1MassPrint;
            }
        }
        private SqlServer.SqlServerGLCsv SqlServerGLCsv
        {
            get
            {
                if (_sqlServerGLCsv == null)
                    _sqlServerGLCsv = new SqlServer.SqlServerGLCsv();

                return _sqlServerGLCsv;
            }
        }
        private SqlServer.SqlServerGlSage SqlServerGlSage
        {
            get
            {
                if (_sqlServerGlSage == null)
                    _sqlServerGlSage = new SqlServer.SqlServerGlSage();

                return _sqlServerGlSage;
            }
        }
        private SqlServer.SqlServerGlPex SqlServerGlPex
        {
            get
            {
                if (_sqlServerGlPex == null)
                    _sqlServerGlPex = new SqlServer.SqlServerGlPex();

                return _sqlServerGlPex;
            }
        }
        private SqlServer.SqlServerGlSap SqlServerGlSap
        {
            get
            {
                if (_sqlServerGlSap == null)
                    _sqlServerGlSap = new SqlServer.SqlServerGlSap();

                return _sqlServerGlSap;
            }
        }
        private SqlServer.SqlServerCanadaRevenueAgencyBondExport SqlServerCanadaRevenueAgencyBondExport
        {
            get
            {
                if (_sqlServerCanadaRevenueAgencyBondExport == null)
                    _sqlServerCanadaRevenueAgencyBondExport = new SqlServer.SqlServerCanadaRevenueAgencyBondExport();

                return _sqlServerCanadaRevenueAgencyBondExport;
            }
        }
        private SqlServer.SqlServerCanadaRevenueAgencyBondExportFile SqlServerCanadaRevenueAgencyBondExportFile
        {
            get
            {
                if (_sqlServerCanadaRevenueAgencyBondExportFile == null)
                    _sqlServerCanadaRevenueAgencyBondExportFile = new SqlServer.SqlServerCanadaRevenueAgencyBondExportFile();

                return _sqlServerCanadaRevenueAgencyBondExportFile;
            }
        }
        #endregion

        #region report
        public ReportCollection GetReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String reportName)
        {
            return SqlServerReport.Select(user, reportName);
        }
        public ReportCollection GetReportById(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long reportId)
        {
            return SqlServerReport.SelectById(user, reportId);
        }
        #endregion

        #region report parameter
        public ReportParameterCollection GetReportParameter(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long reportId)
        {
            return SqlServerReportParameter.Select(user, reportId);
        }
        public T4T4aR1MassPrintCollection GetMassT4ReportValues(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long year)
        {
            return SqlServerT4T4aR1MassPrint.SelectT4(user, year);
        }
        public T4T4aR1MassPrintCollection GetMassT4RSPReportValues(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long year)
        {
            return SqlServerT4T4aR1MassPrint.SelectT4RSP(user, year);
        }
        public T4T4aR1MassPrintCollection GetMassT4AReportValues(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long year)
        {
            return SqlServerT4T4aR1MassPrint.SelectT4A(user, year);
        }
        public T4T4aR1MassPrintCollection GetMassT4ARCAReportValues(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long year)
        {
            return SqlServerT4T4aR1MassPrint.SelectT4ARCA(user, year);
        }
        public T4T4aR1MassPrintCollection GetMassNR4ReportValues(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long year)
        {
            return SqlServerT4T4aR1MassPrint.SelectNR4(user, year);
        }
        public T4T4aR1MassPrintCollection GetMassR1ReportValues(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long year)
        {
            return SqlServerT4T4aR1MassPrint.SelectR1(user, year);
        }
        public T4T4aR1MassPrintCollection GetMassR2ReportValues(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long year)
        {
            return SqlServerT4T4aR1MassPrint.SelectR2(user, year);
        }
        #endregion

        public GlCsvCollection GetGlData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            return SqlServerGLCsv.Select(user, payrollProcessId);
        }
        public GlSapCollection GetGlSapData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            return SqlServerGlSap.Select(user, payrollProcessId);
        }
        public GlSageCollection GetGlSageData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            return SqlServerGlSage.Select(user, payrollProcessId);
        }
        public GlPexCollection GetGlPexData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            return SqlServerGlPex.Select(user, payrollProcessId);
        }

        #region report language
        public ReportLanguageCollection GetReportLanguage(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long reportId)
        {
            return SqlServerReportLanguage.Select(user, reportId);
        }
        #endregion 

        #region CSB export
        public CanadaRevenueAgencyBondExportCollection GetCanadaRevenueAgencyBondExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            return SqlServerCanadaRevenueAgencyBondExport.Select(user, payrollProcessId);
        }
        public CanadaRevenueAgencyBondExport InsertCanadaRevenueAgencySavingsBondExportData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CanadaRevenueAgencyBondExport export)
        {
            return SqlServerCanadaRevenueAgencyBondExport.Insert(user, export);
        }
        public void UpdateCanadaRevenueAgencySavingsBondExportData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, CanadaRevenueAgencyBondExport export)
        {
            SqlServerCanadaRevenueAgencyBondExport.Update(user, export);
        }
        public CanadaRevenueAgencyBondExportFileCollection GetExportFileData(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long payrollProcessId)
        {
            return SqlServerCanadaRevenueAgencyBondExportFile.Select(user, payrollProcessId);
        }
        #endregion

        #region report export
        public ReportExportCollection GetReportExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? exportId)
        {
            return _sqlServerReportExport.Select(user, exportId);
        }
        public void UpdateReportExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ReportExport export)
        {
            _sqlServerReportExport.Update(user, export);
        }
        public ReportExport InsertReportExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ReportExport export)
        {
            return _sqlServerReportExport.Insert(user, export);
        }
        public void DeleteReportExport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, ReportExport export)
        {
            _sqlServerReportExport.Delete(user, export);
        }
        #endregion

        #region STE engine testing
        public void InsertPayrollEngineOutput(String databaseName, PayrollEngineOutput payEngineOutput)
        {
            _sqlServerSTEPayrollEngineTesting.InsertPayrollEngineOutput(databaseName, payEngineOutput);
        }
        #endregion
    }
}