﻿using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess
{
    public class EmailAccess
    {
        #region fields
        SqlServer.SqlServerEmail _sqlServerEmail = new SqlServer.SqlServerEmail();
        SqlServer.SqlServerEmailTemplate _sqlServerEmailTemplate = new SqlServer.SqlServerEmailTemplate();
        SqlServer.SqlServerEmailRule _sqlServerEmailRule = new SqlServer.SqlServerEmailRule();
        #endregion

        #region email
        public EmailCollection GetEmail(DatabaseUser user, long? emailId)
        {
            return _sqlServerEmail.Select(user, emailId);
        }
        public Email InsertEmail(DatabaseUser user, Email item)
        {
            return _sqlServerEmail.Insert(user, item);
        }
        public void UpdateEmail(DatabaseUser user, Email item)
        {
            _sqlServerEmail.Update(user, item);
        }
        public void DeleteEmail(DatabaseUser user, Email item)
        {
            _sqlServerEmail.Delete(user, item);
        }
        #endregion

        #region email template
        public EmailTemplateCollection GetEmailTemplate(DatabaseUser user, long emailId)
        {
            return _sqlServerEmailTemplate.Select(user, emailId);
        }
        public EmailTemplate InsertEmailTemplate(DatabaseUser user, EmailTemplate item)
        {
            return _sqlServerEmailTemplate.Insert(user, item);
        }
        public void UpdateEmailTemplate(DatabaseUser user, EmailTemplate item)
        {
            _sqlServerEmailTemplate.Update(user, item);
        }
        public void DeleteEmailTemplate(DatabaseUser user, EmailTemplate item)
        {
            _sqlServerEmailTemplate.Delete(user, item);
        }
        #endregion

        #region email rules
        public EmailRuleCollection GetEmailRule(DatabaseUser user, long emailId)
        {
            return _sqlServerEmailRule.Select(user, emailId);
        }
        public EmailRule InsertEmailRule(DatabaseUser user, EmailRule item)
        {
            return _sqlServerEmailRule.Insert(user, item);
        }
        public void UpdateEmailRule(DatabaseUser user, EmailRule item)
        {
            _sqlServerEmailRule.Update(user, item);
        }
        public void DeleteEmailRule(DatabaseUser user, EmailRule item)
        {
            _sqlServerEmailRule.Delete(user, item);
        }
        #endregion
    }
}