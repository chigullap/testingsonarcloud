﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess
{
    public class CodeAccess
    {
        #region properties
        SqlServer.SqlServerCode _sql = new SqlServer.SqlServerCode();
        SqlServer.SqlServerCodePaycode _sqlPayCode = new SqlServer.SqlServerCodePaycode();
        SqlServer.SqlServerCodePaycodeBenefit _sqlServerCodePaycodeBenefit = new SqlServer.SqlServerCodePaycodeBenefit();
        SqlServer.SqlServerCodePaycodeDeduction _sqlServerCodePaycodeDeduction = new SqlServer.SqlServerCodePaycodeDeduction();
        SqlServer.SqlServerCodePaycodeIncome _sqlServerCodePaycodeIncome = new SqlServer.SqlServerCodePaycodeIncome();
        SqlServer.SqlServerCodePaycodeEmployer _sqlServerCodePaycodeEmployer = new SqlServer.SqlServerCodePaycodeEmployer();
        SqlServer.SqlServerCodeTableIndex _sqlCodeTableIndex = new SqlServer.SqlServerCodeTableIndex();
        SqlServer.SqlServerCodeTable _sqlCodeTable = new SqlServer.SqlServerCodeTable();
        SqlServer.SqlServerPersonAddressTypeCodeTable _sqlPersonAddressTypeCodeTable = new SqlServer.SqlServerPersonAddressTypeCodeTable();
        SqlServer.SqlServerCodeTableDescription _sqlCodeTableDesc = new SqlServer.SqlServerCodeTableDescription();
        SqlServer.SqlServerCodePayrollProcessingGroup _sqlCodePayrollProcessingGroup = new SqlServer.SqlServerCodePayrollProcessingGroup();
        SqlServer.SqlServerFieldEditor _sqlFieldEditor = new SqlServer.SqlServerFieldEditor();
        SqlServer.SqlServerFieldEntity _sqlFieldEntity = new SqlServer.SqlServerFieldEntity();
        SqlServer.SqlServerFieldLanguage _sqlFieldLanguage = new SqlServer.SqlServerFieldLanguage();
        SqlServer.SqlServerPaycodeSalaryMap _sqlPaycodeSalaryMap = new SqlServer.SqlServerPaycodeSalaryMap();
        SqlServer.SqlServerCodeSystem _sqlCodeSystem = new SqlServer.SqlServerCodeSystem();
        SqlServer.SqlServerPaycodeAssociationType _sqlPaycodeAssociationType = new SqlServer.SqlServerPaycodeAssociationType();
        SqlServer.SqlServerPaycodeAssociation _sqlPaycodeAssociation = new SqlServer.SqlServerPaycodeAssociation();
        SqlServer.SqlServerCodeWsib _sqlServerCodeWsib = new SqlServer.SqlServerCodeWsib();
        SqlServer.SqlServerThirdParty _sqlServerThirdParty = new SqlServer.SqlServerThirdParty();
        SqlServer.SqlServerCodeWsibEffective _sqlServerCodeWsibEffective = new SqlServer.SqlServerCodeWsibEffective();
        SqlServer.SqlServerPaycodeAttachedPaycodeProvision _sqlServerPaycodeAttachedPaycodeProvision = new SqlServer.SqlServerPaycodeAttachedPaycodeProvision();
        SqlServer.SqlServerPayrollProcessGroupOverridePeriod _sqlServerPayrollProcessGroupOverridePeriod = new SqlServer.SqlServerPayrollProcessGroupOverridePeriod();
        SqlServer.SqlServerCodeWcbChequeRemittanceFrequency _sqlServerCodeWcbChequeRemittanceFrequency = new SqlServer.SqlServerCodeWcbChequeRemittanceFrequency();
        SqlServer.SqlServerWcbChequeRemittanceFrequencyDetail _sqlServerWcbChequeRemittanceFrequencyDetail = new SqlServer.SqlServerWcbChequeRemittanceFrequencyDetail();
        SqlServer.SqlServerSalaryPlanCodeTable _sqlSalaryPlanCodeTable = new SqlServer.SqlServerSalaryPlanCodeTable();
        SqlServer.SqlServerRevenueQuebecBusiness _sqlServerRevenueQuebecBusiness = new SqlServer.SqlServerRevenueQuebecBusiness();
        #endregion

        #region field language editor
        public LanguageEntityCollection GetFieldLanguageInfo(DatabaseUser user, Decimal formId, String attributeIdentifier, String englishLanguageCode)
        {
            return _sqlFieldLanguage.Select(user, formId, attributeIdentifier, englishLanguageCode);
        }
        public void UpdateFieldLanguage(DatabaseUser user, LanguageEntity entity, String englishLanguageCode, String frenchLanguageCode)
        {
            _sqlFieldLanguage.UpdateLanguage(user, entity, englishLanguageCode, frenchLanguageCode);
        }
        public LanguageEntity InsertFieldLanguage(DatabaseUser user, LanguageEntity entity, String englishLanguageCode, String frenchLanguageCode)
        {
            return _sqlFieldLanguage.InsertLanguage(user, entity, englishLanguageCode, frenchLanguageCode);
        }
        #endregion

        #region field editor
        public FormDescriptionCollection GetFormInfo(DatabaseUser user, String criteria)
        {
            return _sqlFieldEditor.Select(user, criteria);
        }
        public FieldEntityCollection GetFieldInfo(DatabaseUser user, Decimal formId, String attributeIdentifier, int roleId)
        {
            return _sqlFieldEntity.Select(user, formId, attributeIdentifier, roleId);
        }
        public void UpdateFieldEntity(DatabaseUser user, FieldEntity entity)
        {
            _sqlFieldEntity.Update(user, entity);
        }
        public FieldEntity InsertFieldEntity(DatabaseUser user, FieldEntity entity)
        {
            return _sqlFieldEntity.Insert(user, entity);
        }
        #endregion

        #region code table description calls
        public CodeTableDescriptionCollection GetCodeTableDescriptionRows(DatabaseUser user, String tableName, String code)
        {
            return _sqlCodeTableDesc.Select(user, tableName, code);
        }
        public CodeTable UpdateCodeTableDescRow(DatabaseUser user, CodeTable codeTableDesc, String codeTableName)
        {
            foreach (CodeTableDescription ctd in codeTableDesc.Descriptions)
            {
                if (ctd.CreateUser == null || ctd.UpdateUser == null)
                {
                    ctd.CreateUser = codeTableDesc.CreateUser;
                    ctd.UpdateUser = codeTableDesc.UpdateUser;
                }

                if (ctd.TableDescription.Length > 0 && ctd.Id > 0) //if modifying an existing description (key not -1), perform an update
                    _sqlCodeTableDesc.Update(user, ctd, codeTableName);
                else if (ctd.TableDescription.Length > 0 && ctd.Id < 0) //if adding a new description (key is -1, -2), perform an insert
                    _sqlCodeTableDesc.Insert(user, ctd, codeTableName);
                else if (ctd.TableDescription.Length == 0 && ctd.Id > 0)
                    _sqlCodeTableDesc.Delete(user, ctd, codeTableName); //if a description has been changed to "", then delete it
            }

            return codeTableDesc;
        }

        public PersonAddressTypeCodeTable UpdatePersonAddressTypeCodeTableDescRow(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTableDesc)
        {
            foreach (CodeTableDescription ctd in personAddressTypeCodeTableDesc.Descriptions)
            {
                if (ctd.CreateUser == null || ctd.UpdateUser == null)
                {
                    ctd.CreateUser = personAddressTypeCodeTableDesc.CreateUser;
                    ctd.UpdateUser = personAddressTypeCodeTableDesc.UpdateUser;
                }

                if (ctd.TableDescription.Length > 0 && ctd.Id > 0) //if modifying an existing description (key not -1), perform an update
                    _sqlCodeTableDesc.Update(user, ctd, PersonAddressTypeCodeTable.TableName);
                else if (ctd.TableDescription.Length > 0 && ctd.Id < 0) //if adding a new description (key is -1, -2), perform an insert
                    _sqlCodeTableDesc.Insert(user, ctd, PersonAddressTypeCodeTable.TableName);
                else if (ctd.TableDescription.Length == 0 && ctd.Id > 0)
                    _sqlCodeTableDesc.Delete(user, ctd, PersonAddressTypeCodeTable.TableName); //if a description has been changed to "", then delete it
            }

            return personAddressTypeCodeTableDesc;
        }

        public CodeTable InsertCodeTableDescRow(DatabaseUser user, CodeTable codeTableDesc, String codeTableName)
        {
            if (codeTableDesc.Descriptions != null)
            {
                foreach (CodeTableDescription ctd in codeTableDesc.Descriptions)
                {
                    if (ctd.TableDescription.Length > 0) //if a description is present, insert it
                    {
                        if (ctd.CreateUser == null)
                        {
                            ctd.CreateUser = codeTableDesc.CreateUser;
                            ctd.UpdateUser = codeTableDesc.UpdateUser;
                        }

                        if (ctd.CodeTableDescCd == "")
                            ctd.CodeTableDescCd = codeTableDesc.Key;

                        _sqlCodeTableDesc.Insert(user, ctd, codeTableName);
                    }
                }
            }

            return codeTableDesc;
        }

        public PersonAddressTypeCodeTable InsertPersonAddressTypeCodeTableDescRow(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTableDesc)
        {
            if (personAddressTypeCodeTableDesc.Descriptions != null)
            {
                foreach (CodeTableDescription ctd in personAddressTypeCodeTableDesc.Descriptions)
                {
                    if (ctd.TableDescription.Length > 0) //if a description is present, insert it
                    {
                        if (ctd.CreateUser == null)
                        {
                            ctd.CreateUser = personAddressTypeCodeTableDesc.CreateUser;
                            ctd.UpdateUser = personAddressTypeCodeTableDesc.UpdateUser;
                        }

                        if (ctd.CodeTableDescCd == "")
                            ctd.CodeTableDescCd = personAddressTypeCodeTableDesc.Key;

                        _sqlCodeTableDesc.Insert(user, ctd, PersonAddressTypeCodeTable.TableName);
                    }
                }
            }

            return personAddressTypeCodeTableDesc;
        }

        public void DeleteCodeTableDescRow(DatabaseUser user, CodeTable codeTableDesc, String codeTableName)
        {
            foreach (CodeTableDescription ctd in codeTableDesc.Descriptions)
                _sqlCodeTableDesc.Delete(user, ctd, codeTableName);
        }

        public void DeletePersonAddressTypeCodeTableDescRow(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTableDesc)
        {
            foreach (CodeTableDescription ctd in personAddressTypeCodeTableDesc.Descriptions)
                _sqlCodeTableDesc.Delete(user, ctd, PersonAddressTypeCodeTable.TableName);
        }
        #endregion

        #region codeTableCalls
        public CodeTableCollection GetCodeTableRows(DatabaseUser user, String tableName, String parentTableName)
        {
            CodeTableCollection collection = _sqlCodeTable.Select(user, tableName, parentTableName);

            foreach (CodeTable codeTable in collection)
                codeTable.Descriptions = _sqlCodeTableDesc.Select(user, tableName, codeTable.CodeTableId);

            return collection;
        }
        public CodeTable UpdateCodeTableRow(DatabaseUser user, CodeTable codeTable, String codeTableName, String parentTableName)
        {
            return _sqlCodeTable.Update(user, codeTable, codeTableName, parentTableName);
        }
        public CodeTable InsertCodeTableRow(DatabaseUser user, CodeTable codeTable, String codeTableName, String parentTableName)
        {
            return _sqlCodeTable.Insert(user, codeTable, codeTableName, parentTableName);
        }
        public void DeleteCodeTableRow(DatabaseUser user, CodeTable codeTable, String codeTableName)
        {
            _sqlCodeTable.Delete(user, codeTable, codeTableName);
        }
        #endregion

        #region salary plan code table
        public SalaryPlanCodeTableCollection GetSalaryPlanCodeTableRows(DatabaseUser user, String tableName, String parentSalaryPlanCode)
        {
            SalaryPlanCodeTableCollection collection = _sqlSalaryPlanCodeTable.Select(user, parentSalaryPlanCode);

            foreach (SalaryPlanCodeTable codeTable in collection)
                codeTable.Descriptions = _sqlCodeTableDesc.Select(user, tableName, codeTable.CodeTableId);

            return collection;
        }
        public SalaryPlanCodeTable InsertSalaryPlanCodeTableRow(DatabaseUser user, SalaryPlanCodeTable salaryPlanCodeTable)
        {
            return _sqlSalaryPlanCodeTable.Insert(user, salaryPlanCodeTable);
        }
        public SalaryPlanCodeTable UpdateSalaryPlanCodeTableRow(DatabaseUser user, SalaryPlanCodeTable salaryPlanCodeTable)
        {
            return _sqlSalaryPlanCodeTable.Update(user, salaryPlanCodeTable);
        }
        public void DeleteSalaryPlanCodeTableRow(DatabaseUser user, SalaryPlanCodeTable salaryPlanCodeTable)
        {
            _sqlSalaryPlanCodeTable.Delete(user, salaryPlanCodeTable);
        }
        #endregion

        #region personAddressTypeCodeTableCalls
        public PersonAddressTypeCodeTableCollection GetPersonAddressTypeCodeTableRows(DatabaseUser user)
        {
            PersonAddressTypeCodeTableCollection collection = _sqlPersonAddressTypeCodeTable.Select(user);

            foreach (PersonAddressTypeCodeTable codeTable in collection)
                codeTable.Descriptions = _sqlCodeTableDesc.Select(user, PersonAddressTypeCodeTable.TableName, codeTable.CodeTableId);

            return collection;
        }

        public PersonAddressTypeCodeTable GetPersonAddressTypeCodeTableRow(DatabaseUser user, short priority)
        {
            PersonAddressTypeCodeTable codeTable = _sqlPersonAddressTypeCodeTable.SelectByPriority(user, priority);

            if (codeTable != null)
            {
                codeTable.Descriptions = _sqlCodeTableDesc.Select(user, PersonAddressTypeCodeTable.TableName, codeTable.CodeTableId);
            }

            return codeTable;
        }
        public PersonAddressTypeCodeTable InsertPersonAddressTypeCodeTableRow(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTable)
        {
            return _sqlPersonAddressTypeCodeTable.Insert(user, personAddressTypeCodeTable);
        }
        public PersonAddressTypeCodeTable UpdatePersonAddressTypeCodeTableRow(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTable)
        {
            return _sqlPersonAddressTypeCodeTable.Update(user, personAddressTypeCodeTable);
        }
        public void DeletePersonAddressTypeCodeTableRow(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTable)
        {
            _sqlPersonAddressTypeCodeTable.Delete(user, personAddressTypeCodeTable);
        }
        #endregion

        #region codeTableIndexCalls
        public CodeTableIndexCollection GetCodeTableIndex(DatabaseUser user, String tableName)
        {
            return _sqlCodeTableIndex.Select(user, tableName);
        }
        #endregion

        #region codeCollectionCalls
        public CodeCollection GetCodeTable(String databaseName, String language, CodeTableType type)
        {
            return _sql.Select(databaseName, language, type);
        }
        public CodeCollection GetCodeTable(String databaseName, String language, CodeTableType type, String parentColumnName, String parentColumnValue)
        {
            return _sql.Select(databaseName, language, type, parentColumnName, parentColumnValue);
        }
        public CodeCollection GetDisciplineTypeCode(DatabaseUser user, string disciplineCode)
        {
            return _sql.SelectDisciplineTypeCode(user, disciplineCode);
        }
        public CodeCollection GetProvinceStateCode(DatabaseUser user, String country)
        {
            return _sql.SelectProvinceStateCode(user, country);
        }
        public CodeCollection GetDayCode(DatabaseUser user, String monthCode)
        {
            return _sql.SelectDayCode(user, monthCode);
        }
        public CodeCollection GetYearEndFormBoxCode(DatabaseUser user, String yearEndFormCode)
        {
            return _sql.SelectYearEndFormBoxCode(user, yearEndFormCode);
        }
        public CodeCollection GetContactChannelTypeCode(DatabaseUser user, String contactChannelCategoryCode)
        {
            return _sql.SelectContactChannelTypeCode(user, contactChannelCategoryCode);
        }
        public CodeCollection GetContactRelationshipCode(String databaseName, String language)
        {
            return _sql.SelectContactRelationshipCode(databaseName, language);
        }
        public CodeCollection GetPaycodeTypeCode(String databaseName, String language, String paycode)
        {
            return _sql.SelectPaycodeTypeCode(databaseName, language, paycode);
        }
        public CodeCollection GetPaycodesByType(DatabaseUser user, String paycodeTypeCode, String paycodeCode)
        {
            return _sql.SelectPaycodesByType(user, paycodeTypeCode, paycodeCode);
        }
        public bool IsPaycodeGarnishment(DatabaseUser user, String paycode, String paycodeType)
        {
            return _sql.IsPaycodeGarnishment(user, paycode, paycodeType);
        }
        public CodeCollection GetUnionCollectiveAgreementCode(DatabaseUser user, String unionCode)
        {
            return _sql.SelectUnionCollectiveAgreementCode(user, unionCode);
        }
        public CodeCollection GetEmployeeBankCode(DatabaseUser user, String bankingCountryCode)
        {
            return _sql.SelectEmployeeBankCode(user, bankingCountryCode);
        }
        #endregion

        #region code paycode
        public CodePaycodeCollection GetCodePaycode(DatabaseUser user, String paycode, bool useExternalFlag)
        {
            return _sqlPayCode.Select(user, paycode, useExternalFlag);
        }
        public void UpdateCodePaycode(DatabaseUser user, CodePaycode paycodeCode, bool overrideConcurrencyCheckFlag)
        {
            _sqlPayCode.Update(user, paycodeCode, overrideConcurrencyCheckFlag);
        }
        public CodePaycode InsertCodePaycode(DatabaseUser user, CodePaycode paycode)
        {
            return _sqlPayCode.Insert(user, paycode);
        }
        public void DeleteCodePaycode(DatabaseUser user, CodePaycode paycode)
        {
            _sqlPayCode.Delete(user, paycode);
        }
        #endregion

        #region code paycode benefit
        public CodePaycodeBenefitCollection GetCodePaycodeBenefit(DatabaseUser user, String paycodeCode)
        {
            return _sqlServerCodePaycodeBenefit.Select(user, paycodeCode);
        }
        public void UpdateCodePaycodeBenefit(DatabaseUser user, CodePaycodeBenefit paycode)
        {
            _sqlServerCodePaycodeBenefit.Update(user, paycode);
        }
        public CodePaycodeBenefit InsertCodePaycodeBenefit(DatabaseUser user, CodePaycodeBenefit paycode)
        {
            return _sqlServerCodePaycodeBenefit.Insert(user, paycode);
        }
        public void DeleteCodePaycodeBenefit(DatabaseUser user, CodePaycode paycode)
        {
            _sqlServerCodePaycodeBenefit.Delete(user, paycode);
        }
        #endregion

        #region code paycode deduction
        public CodePaycodeDeductionCollection GetCodePaycodeDeduction(DatabaseUser user, String paycodeCode)
        {
            return _sqlServerCodePaycodeDeduction.Select(user, paycodeCode);
        }
        public void UpdateCodePaycodeDeduction(DatabaseUser user, CodePaycodeDeduction paycode)
        {
            _sqlServerCodePaycodeDeduction.Update(user, paycode);
        }
        public CodePaycodeDeduction InsertCodePaycodeDeduction(DatabaseUser user, CodePaycodeDeduction paycode)
        {
            return _sqlServerCodePaycodeDeduction.Insert(user, paycode);
        }
        public void DeleteCodePaycodeDeduction(DatabaseUser user, CodePaycode paycode)
        {
            _sqlServerCodePaycodeDeduction.Delete(user, paycode);
        }
        #endregion

        #region code paycode income
        public CodePaycodeIncomeCollection GetCodePaycodeIncome(DatabaseUser user, String paycodeCode)
        {
            return _sqlServerCodePaycodeIncome.Select(user, paycodeCode);
        }
        public void UpdateCodePaycodeIncome(DatabaseUser user, CodePaycodeIncome paycode)
        {
            _sqlServerCodePaycodeIncome.Update(user, paycode);
        }
        public CodePaycodeIncome InsertCodePaycodeIncome(DatabaseUser user, CodePaycodeIncome paycode)
        {
            return _sqlServerCodePaycodeIncome.Insert(user, paycode);
        }
        public void DeleteCodePaycodeIncome(DatabaseUser user, CodePaycode paycode)
        {
            _sqlServerCodePaycodeIncome.Delete(user, paycode);
        }
        #endregion

        #region code paycode employer
        public CodePaycodeEmployerCollection GetCodePaycodeEmployer(DatabaseUser user, String paycodeCode)
        {
            return _sqlServerCodePaycodeEmployer.Select(user, paycodeCode);
        }
        public void UpdateCodePaycodeEmployer(DatabaseUser user, CodePaycodeEmployer paycode)
        {
            _sqlServerCodePaycodeEmployer.Update(user, paycode);
        }
        public CodePaycodeEmployer InsertCodePaycodeEmployer(DatabaseUser user, CodePaycodeEmployer paycode)
        {
            return _sqlServerCodePaycodeEmployer.Insert(user, paycode);
        }
        public void DeleteCodePaycodeEmployer(DatabaseUser user, CodePaycode paycode)
        {
            _sqlServerCodePaycodeEmployer.Delete(user, paycode);
        }
        #endregion

        #region paycode attached paycode provision
        public PaycodeAttachedPaycodeProvisionCollection GetPaycodeAttachedPaycodeProvision(DatabaseUser user, String paycodeCode)
        {
            return _sqlServerPaycodeAttachedPaycodeProvision.Select(user, paycodeCode);
        }
        public PaycodeAttachedPaycodeProvision InsertPaycodeAttachedPaycodeProvision(DatabaseUser user, PaycodeAttachedPaycodeProvision attachedPaycode)
        {
            return _sqlServerPaycodeAttachedPaycodeProvision.Insert(user, attachedPaycode);
        }
        public void DeletePaycodeAttachedPaycodeProvision(DatabaseUser user, String paycodeCode)
        {
            _sqlServerPaycodeAttachedPaycodeProvision.Delete(user, paycodeCode);
        }
        #endregion

        #region code payroll processing group
        public PayrollProcessingGroupCollection GetPayrollProcessingGroup(DatabaseUser user, String payrollProcessGroupCode)
        {
            return _sqlCodePayrollProcessingGroup.Select(user, payrollProcessGroupCode);
        }
        public void UpdatePayrollProcessingGroup(DatabaseUser user, PayrollProcessingGroup ppG)
        {
            _sqlCodePayrollProcessingGroup.Update(user, ppG);
        }
        public PayrollProcessingGroup InsertPayrollProcessingGroupCode(DatabaseUser user, PayrollProcessingGroup ppG)
        {
            return _sqlCodePayrollProcessingGroup.InsertCode(user, ppG);
        }
        public PayrollProcessingGroup InsertPayrollProcessingGroupCodeDesc(DatabaseUser user, PayrollProcessingGroup ppG)
        {
            return _sqlCodePayrollProcessingGroup.InsertCodeDesc(user, ppG);
        }
        #endregion

        #region payroll process group override period
        public PayrollProcessGroupOverridePeriodCollection GetPayrollProcessGroupOverridePeriod(DatabaseUser user, String payrollProcessGroupCode)
        {
            return _sqlServerPayrollProcessGroupOverridePeriod.Select(user, payrollProcessGroupCode);
        }
        public PayrollProcessGroupOverridePeriod InsertPayrollProcessGroupOverridePeriod(DatabaseUser user, PayrollProcessGroupOverridePeriod overridePeriod)
        {
            return _sqlServerPayrollProcessGroupOverridePeriod.Insert(user, overridePeriod);
        }
        public void UpdatePayrollProcessGroupOverridePeriod(DatabaseUser user, PayrollProcessGroupOverridePeriod overridePeriod)
        {
            _sqlServerPayrollProcessGroupOverridePeriod.Update(user, overridePeriod);
        }
        public void DeletePayrollProcessGroupOverridePeriod(DatabaseUser user, PayrollProcessGroupOverridePeriod overridePeriod)
        {
            _sqlServerPayrollProcessGroupOverridePeriod.Delete(user, overridePeriod);
        }
        #endregion

        #region code system
        public CodeSystemCollection GetCodeSystem(DatabaseUser user)
        {
            return _sqlCodeSystem.Select(user);
        }
        public void UpdateCodeSystem(DatabaseUser user, CodeSystem code)
        {
            _sqlCodeSystem.Update(user, code);
        }
        public CodeSystem InsertCodeSystem(DatabaseUser user, CodeSystem code)
        {
            return _sqlCodeSystem.Insert(user, code);
        }
        public void DeleteCodeSystem(DatabaseUser user, CodeSystem code)
        {
            _sqlCodeSystem.Delete(user, code);
        }
        #endregion

        #region paycode association
        public PaycodeAssociationTypeCollection GetPaycodeAssociationType(DatabaseUser user, String paycodeAssociationTypeCode)
        {
            return _sqlPaycodeAssociationType.Select(user, paycodeAssociationTypeCode);
        }
        public PaycodeAssociationCollection GetPaycodeAssociation(DatabaseUser user, String paycodeAssociationTypeCode)
        {
            return _sqlPaycodeAssociation.Select(user, paycodeAssociationTypeCode);
        }
        public PaycodeAssociationType InsertPaycodeAssociationType(DatabaseUser user, PaycodeAssociationType paycode)
        {
            return _sqlPaycodeAssociationType.Insert(user, paycode);
        }
        public PaycodeAssociation InsertPaycodeAssociation(DatabaseUser user, PaycodeAssociation paycode)
        {
            return _sqlPaycodeAssociation.Insert(user, paycode);
        }
        public void UpdatePaycodeAssociationType(DatabaseUser user, PaycodeAssociationType paycode)
        {
            _sqlPaycodeAssociationType.Update(user, paycode);
        }
        public void UpdatePaycodeAssociation(DatabaseUser user, PaycodeAssociation paycode)
        {
            _sqlPaycodeAssociation.Update(user, paycode);
        }
        public void DeletePaycodeAssociationType(DatabaseUser user, PaycodeAssociationType paycode)
        {
            _sqlPaycodeAssociationType.Delete(user, paycode);
        }
        public void DeletePaycodeAssociation(DatabaseUser user, PaycodeAssociation paycode)
        {
            _sqlPaycodeAssociation.Delete(user, paycode);
        }
        #endregion

        #region third party
        public CodeThirdPartyCollection GetCodeThirdParty(DatabaseUser user, string thirdPartyCode)
        {
            return _sqlServerThirdParty.Select(user, thirdPartyCode);
        }
        public void DeleteThirdPartyCode(DatabaseUser user, CodeThirdParty thirdParty)
        {
            _sqlServerThirdParty.Delete(user, thirdParty);
        }
        public void UpdateThirdPartyCode(DatabaseUser user, CodeThirdParty thirdParty)
        {
            _sqlServerThirdParty.Update(user, thirdParty);
        }
        public CodeThirdParty InsertThirdPartyCode(DatabaseUser user, CodeThirdParty thirdParty)
        {
            return _sqlServerThirdParty.Insert(user, thirdParty);
        }
        #endregion

        #region code wsib
        public CodeWsibCollection GetCodeWsib(DatabaseUser user, String wsibCode)
        {
            return _sqlServerCodeWsib.Select(user, wsibCode);
        }
        public void UpdateCodeWsib(DatabaseUser user, CodeWsib codeWsib)
        {
            _sqlServerCodeWsib.Update(user, codeWsib);
        }
        public CodeWsib InsertCodeWsib(DatabaseUser user, CodeWsib codeWsib)
        {
            return _sqlServerCodeWsib.Insert(user, codeWsib);
        }
        public void DeleteCodeWsib(DatabaseUser user, CodeWsib codeWsib)
        {
            _sqlServerCodeWsib.Delete(user, codeWsib);
        }
        public CodeWsibEffectiveCollection GetCodeWsibEffective(DatabaseUser user, String wsibCode)
        {
            return _sqlServerCodeWsibEffective.Select(user, wsibCode);
        }
        public CodeWsibEffective InsertCodeWsibEffective(DatabaseUser user, CodeWsibEffective codeWsibEffective)
        {
            return _sqlServerCodeWsibEffective.Insert(user, codeWsibEffective);
        }
        public void UpdateCodeWsibEffective(DatabaseUser user, CodeWsibEffective codeWsibEffective)
        {
            _sqlServerCodeWsibEffective.Update(user, codeWsibEffective);
        }
        public void DeleteCodeWsibEffective(DatabaseUser user, CodeWsibEffective codeWsibEffective)
        {
            _sqlServerCodeWsibEffective.Delete(user, codeWsibEffective);
        }
        #endregion

        #region code wcb cheque remittance frequency
        public CodeWcbChequeRemittanceFrequencyCollection GetCodeWcbChequeRemittanceFrequency(DatabaseUser user, String wcbChequeRemittanceFrequencyCode)
        {
            return _sqlServerCodeWcbChequeRemittanceFrequency.Select(user, wcbChequeRemittanceFrequencyCode);
        }
        public CodeWcbChequeRemittanceFrequency InsertCodeWcbChequeRemittanceFrequency(DatabaseUser user, CodeWcbChequeRemittanceFrequency codeWcbChequeRemittanceFrequency)
        {
            return _sqlServerCodeWcbChequeRemittanceFrequency.Insert(user, codeWcbChequeRemittanceFrequency);
        }
        public void UpdateCodeWcbChequeRemittanceFrequency(DatabaseUser user, CodeWcbChequeRemittanceFrequency codeWcbChequeRemittanceFrequency)
        {
            _sqlServerCodeWcbChequeRemittanceFrequency.Update(user, codeWcbChequeRemittanceFrequency);
        }
        public void DeleteCodeWcbChequeRemittanceFrequency(DatabaseUser user, CodeWcbChequeRemittanceFrequency codeWcbChequeRemittanceFrequency)
        {
            _sqlServerCodeWcbChequeRemittanceFrequency.Delete(user, codeWcbChequeRemittanceFrequency);
        }
        public WcbChequeRemittanceFrequencyDetailCollection GetWcbChequeRemittanceFrequencyDetail(DatabaseUser user, String wcbChequeRemittanceFrequencyCode)
        {
            return _sqlServerWcbChequeRemittanceFrequencyDetail.Select(user, wcbChequeRemittanceFrequencyCode);
        }
        public WcbChequeRemittanceFrequencyDetail InsertWcbChequeRemittanceFrequencyDetail(DatabaseUser user, WcbChequeRemittanceFrequencyDetail wcbChequeRemittanceFrequencyDetail)
        {
            return _sqlServerWcbChequeRemittanceFrequencyDetail.Insert(user, wcbChequeRemittanceFrequencyDetail);
        }
        public void UpdateWcbChequeRemittanceFrequencyDetail(DatabaseUser user, WcbChequeRemittanceFrequencyDetail wcbChequeRemittanceFrequencyDetail)
        {
            _sqlServerWcbChequeRemittanceFrequencyDetail.Update(user, wcbChequeRemittanceFrequencyDetail);
        }
        public void DeleteWcbChequeRemittanceFrequencyDetail(DatabaseUser user, WcbChequeRemittanceFrequencyDetail wcbChequeRemittanceFrequencyDetail)
        {
            _sqlServerWcbChequeRemittanceFrequencyDetail.Delete(user, wcbChequeRemittanceFrequencyDetail);
        }
        #endregion

        public PaycodeSalaryMapCollection GetSalaryPaycodes(DatabaseUser user)
        {
            return _sqlPaycodeSalaryMap.Select(user);
        }

        #region revenue quebec business 

        public RevenueQuebecBusiness GetRevenueQuebecBusiness(DatabaseUser user)
        {
            return _sqlServerRevenueQuebecBusiness.Select(user);
        }

        public RevenueQuebecBusiness UpdateRevenueQuebecBusiness(DatabaseUser user, RevenueQuebecBusiness revenueQuebecBusiness)
        {
            return _sqlServerRevenueQuebecBusiness.Update(user, revenueQuebecBusiness);
        }

        #endregion
    }
}