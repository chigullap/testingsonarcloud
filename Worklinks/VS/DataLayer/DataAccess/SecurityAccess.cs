﻿using System;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.DataLayer.DataAccess.SqlServer;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess
{
    public class SecurityAccess
    {
        private SqlServerSecurityLabelRole _sqlServerSecurityLabelRole;
        private SqlServerSecurityUser _sqlServerSecurityUser = new SqlServerSecurityUser();
        private SqlServerUserAdmin _sqlServerUserAdmin = new SqlServerUserAdmin();
        private SqlServerSecurityUserPasswordHistory _sqlServerSecurityUserPasswordHistory = new SqlServerSecurityUserPasswordHistory();
        private SqlServerSecurityMarking _sqlServerSecurityMarking = new SqlServerSecurityMarking();
        private SqlServerSecurityCategories _sqlServerSecurityCategories = new SqlServerSecurityCategories();
        private SqlServerGroupDescription _sqlGroupDesc = new SqlServerGroupDescription();
        private SqlServerRoleDescription _sqlRoleDesc = new SqlServerRoleDescription();
        private SqlServerSecurityRoleForm _sqlSecurityRoleForm = new SqlServerSecurityRoleForm();
        private SqlServerSecurityUserLoginHistory _sqlServerSecurityUserLoginHistory = new SqlServerSecurityUserLoginHistory();
        private SqlServerSecurityUserProfile _sqlServerSecurityUserProfile = new SqlServerSecurityUserProfile();

        #region properties
        private SqlServerSecurityLabelRole SqlServerSecurityLabelRole
        {
            get
            {
                if (_sqlServerSecurityLabelRole == null)
                    _sqlServerSecurityLabelRole = new SqlServerSecurityLabelRole();
                return _sqlServerSecurityLabelRole;
            }
        }
        #endregion

        #region security
        public SecurityUserCollection GetSecurityUser(String databaseName, String userName)
        {
            return _sqlServerSecurityUser.Select(databaseName, userName);
        }
        public SecurityUserCollection DoesSecurityUserAlreadyExist(DatabaseUser user, long employeeId)
        {
            return _sqlServerSecurityUser.SelectByEmployeeId(user, employeeId);
        }
        public SecurityUserPasswordHistoryCollection GetPasswordHistory(DatabaseUser user, String userName)
        {
            return _sqlServerSecurityUserPasswordHistory.Select(user, userName);
        }
        //public void UpdateLogoutUser(DatabaseUser user, String userName)
        //{
        //    _sqlServerSecurityUser.UpdateLogoutUser(user, userName);
        //}
        public void InsertSecurityUserPasswordHistory(DatabaseUser user, SecurityUserPasswordHistory passHistory)
        {
            _sqlServerSecurityUserPasswordHistory.InsertPasswordHistory(user, passHistory);
        }
        public void UpdateSecurityUserPassword(DatabaseUser user, string adminDatabaseName, String username, SecurityUser securityUser)
        {
            _sqlServerSecurityUser.UpdatePassword(user, adminDatabaseName, username, securityUser);
        }
        public void UpdateSecurityUser(DatabaseUser user, string overrideDatabaseName, SecurityUser securityUser)
        {
            _sqlServerSecurityUser.UpdateSecurityUser(user, overrideDatabaseName, securityUser);
        }
        public void UpdateSecurityUserLoginInfo(String databaseName, SecurityUser user)
        {
            _sqlServerSecurityUser.UpdateLoginInfo(databaseName, user);
        }
        public bool DoesUserExistOnAuth(string databaseName, string userName)
        {
            return _sqlServerSecurityUser.DoesUserExistOnAuth(databaseName, userName);
        }

        #endregion

        #region user admin
        public int SelectSecurityClientUser(DatabaseUser user, string authDatabaseName, long securityUserId)
        {
            return _sqlServerUserAdmin.SelectSecurityClientUser(user, authDatabaseName, securityUserId);
        }
        public UserSummaryCollection GetUserSummary(DatabaseUser user, UserAdminSearchCriteria criteria)
        {
            return _sqlServerUserAdmin.Select(user, criteria);
        }
        public SecurityUserCollection GetSecurityUserPerson(DatabaseUser user, long personId)
        {
            return _sqlServerSecurityUser.SelectSecurityUserPerson(user, personId);
        }
        public SecurityUser InsertSecurityUser(DatabaseUser databaseUser, String databaseNameOverride, SecurityUser user)
        {
            return _sqlServerSecurityUser.Insert(databaseUser, databaseNameOverride, user);
        }
        public void InsertSecurityUserClaim(DatabaseUser user, SecurityUser securityUser, String authDatabaseName, String ngSecurityClientId, String type, String value)
        {
            _sqlServerSecurityUser.InsertSecurityUserClaim(user, securityUser, authDatabaseName, ngSecurityClientId, type, value);
        }
        #endregion

        public void DeleteUser(DatabaseUser user, long personId)
        {
            _sqlServerSecurityUser.Delete(user, personId);
        }

        #region security marking
        public SecurityMarkingCollection GetSecurityMarking(DatabaseUser user, int hierarchicalSortOrder)
        {
            return _sqlServerSecurityMarking.Select(user, hierarchicalSortOrder);
        }
        #endregion

        #region security categories
        public SecurityCategoryCollection GetSecurityCategories(DatabaseUser user, bool isViewMode)
        {
            return _sqlServerSecurityCategories.Select(user, isViewMode);
        }
        public void UpdateCategories(DatabaseUser user, SecurityCategory category)
        {
            _sqlServerSecurityCategories.Update(user, category);
        }
        public void RebuildSecurity(DatabaseUser user)
        {
            _sqlServerSecurityCategories.RebuildSecurity(user);
        }
        #endregion

        #region security label role
        public SecurityLabelRoleCollection GetSecurityLabelRole(DatabaseUser user, long securityRoleId)
        {
            return SqlServerSecurityLabelRole.Select(user, securityRoleId);
        }
        public void InsertSecurityLabelRole(DatabaseUser user, SecurityLabelRole item)
        {
            SqlServerSecurityLabelRole.Insert(user, item);
        }
        public void DeleteSecurityLabelRole(DatabaseUser user, long roleId)
        {
            SqlServerSecurityLabelRole.Delete(user, roleId);
        }
        #endregion

        #region Role Description
        public RoleDescriptionCollection GetRoleDescriptions(DatabaseUser user, String criteria, long? roleId, bool? worklinksAdministrationFlag)
        {
            return _sqlRoleDesc.Select(user, criteria, roleId, worklinksAdministrationFlag);
        }
        public void UpdateRoleDesc(DatabaseUser user, RoleDescription roleDesc)
        {
            _sqlRoleDesc.Update(user, roleDesc);
        }
        public RoleDescription InsertRoleDescription(DatabaseUser user, RoleDescription roleDesc)
        {
            return _sqlRoleDesc.InsertRoleDescription(user, roleDesc);

        }
        #region auth
        public void InsertSecurityClientRoleDescription(DatabaseUser user, String authDatabaseName, String ngSecurityClientId, ISecurityRoleDescription roleDesc)
        {
            _sqlRoleDesc.InsertSecurityClientRoleDescription(user, authDatabaseName, ngSecurityClientId, roleDesc);
        }
        public void UpdateSecurityClientRoleDescription(DatabaseUser user, String authDatabaseName, String ngSecurityClientId, ISecurityRoleDescription roleDesc)
        {
            _sqlRoleDesc.UpdateSecurityClientRoleDescription(user, authDatabaseName, ngSecurityClientId, roleDesc);
        }
        public void UpdateSecurityClientRoleHierarchy(DatabaseUser user, String authDatabaseName, String ngSecurityClientId, GroupDescription group)
        {
            _sqlRoleDesc.UpdateSecurityClientRoleHierarchy(user, authDatabaseName, ngSecurityClientId, group);
        }


        #endregion

        #endregion

        #region Group Description
        public RoleDescriptionCollection GetRoleDescriptionType(DatabaseUser user, String securityRoleType, bool? worklinksAdministrationFlag)
        {
            return _sqlRoleDesc.SelectType(user, null, securityRoleType, null, worklinksAdministrationFlag);
        }
        public GroupDescriptionCollection GetUserGroups(DatabaseUser user, long securityUserId, bool isViewMode, bool? worklinksAdministrationFlag)
        {
            return _sqlGroupDesc.SelectUserGroups(user, securityUserId, isViewMode, worklinksAdministrationFlag);
        }
        public GroupDescriptionCollection GetGroupDescriptions(DatabaseUser user, String criteria, long? groupId, bool? worklinksAdministrationFlag)
        {
            return _sqlGroupDesc.Select(user, criteria, groupId, worklinksAdministrationFlag);
        }
        public void UpdateGroupDesc(DatabaseUser user, GroupDescription grpDesc)
        {
            _sqlGroupDesc.Update(user, grpDesc);
        }
        public GroupDescription InsertGroupDescription(DatabaseUser user, GroupDescription grpDesc)
        {
            return _sqlGroupDesc.InsertGroupDescription(user, grpDesc);
        }
        public GroupDescription InsertUserGroup(DatabaseUser user, GroupDescription grpDesc, long securityUserId)
        {
            return _sqlGroupDesc.InsertUserGroup(user, grpDesc, securityUserId);
        }
        public void DeleteUserGroup(DatabaseUser user, GroupDescription grpDesc, long securityUserId)
        {
            _sqlGroupDesc.DeleteUserGroup(user, grpDesc, securityUserId);
        }
        public void UpdateSecurityClientUserRoles(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, string adminDatabaseName, string ngSecurityClientId, string userName, long securityUserId, string roles)
        {
            _sqlGroupDesc.UpdateSecurityClientUserRoles(user, adminDatabaseName, ngSecurityClientId, userName, securityUserId, roles);
        }
        #endregion

        #region Form Security
        public FormSecurityCollection GetFormSecurityInfo(DatabaseUser user, long roleId, bool? worklinksAdministrationFlag)
        {
            return _sqlSecurityRoleForm.Select(user, roleId, worklinksAdministrationFlag);
        }
        public FormSecurity InsertSecurityRoleForm(DatabaseUser user, FormSecurity form, long roleId)
        {
            return _sqlSecurityRoleForm.InsertSecurityRoleForm(user, form, roleId);
        }
        public void UpdateSecurityRoleForm(DatabaseUser user, FormSecurity form)
        {
            _sqlSecurityRoleForm.UpdateSecurityRoleForm(user, form);
        }
        public void DeleteSecurityRoleForm(DatabaseUser user, FormSecurity form, long roleId)
        {
            _sqlSecurityRoleForm.DeleteSecurityRoleForm(user, form, roleId);
        }
        public void UpdateSecurityClienRoleClaim(DatabaseUser user, String authDatabaseName, String ngSecurityClientId, long roleId, string type, FormSecurityCollection forms)
        {
            _sqlSecurityRoleForm.UpdateSecurityClienRoleClaim(user, authDatabaseName, ngSecurityClientId, roleId, type, forms);
        }
        #endregion

        #region SecurityUserLoginHistory
        public SecurityUserLoginHistoryCollection GetSecurityUserLoginHistory(String databaseName, String userName, int passwordAttemptWindow)
        {
            return _sqlServerSecurityUserLoginHistory.Select(databaseName, userName, passwordAttemptWindow);
        }
        public void InsertSecurityUserLoginHistory(String databaseName, SecurityUserLoginHistory item)
        {
            _sqlServerSecurityUserLoginHistory.Insert(databaseName, item);
        }
        #endregion

        public String GetEmailByEmployeeId(DatabaseUser user, long employeeId)
        {
            return _sqlServerUserAdmin.GetEmailByEmployeeId(user, employeeId);
        }

        #region SecurityUserProfile
        public SecurityUserProfileCollection GetSecurityUserProfile(String databaseName, String userName)
        {
            return _sqlServerSecurityUserProfile.Select(databaseName, userName);
        }
        public void UpdateSecurityUserProfile(DatabaseUser user, SecurityUserProfile item)
        {
            _sqlServerSecurityUserProfile.Update(user, item);
        }
        #endregion
    }
}