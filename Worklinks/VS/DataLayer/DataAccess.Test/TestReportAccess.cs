﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Test;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.DataLayer.DataAccess.Test
{
    public class TestReportAccess
    {
        #region properties

        SqlServer.SqlServerTestReport _sqlServerTestReport = new SqlServer.SqlServerTestReport();
        SqlServer.SqlServerTestReportParameter _sqlServerTestReportParameter = new SqlServer.SqlServerTestReportParameter();
        SqlServer.SqlServerTestReportExpectedValue _sqlServerTestReportExpectedValue = new SqlServer.SqlServerTestReportExpectedValue();
        
        #endregion

        #region report testing

        public TestReportCollection GetTestReports(DatabaseUser user)
        {
            return _sqlServerTestReport.Select(user);
        }

        public TestReportParameterCollection GetTestReportParameters(DatabaseUser user, long testReportId)
        {
            return _sqlServerTestReportParameter.Select(user, testReportId);
        }

        public TestReportExpectedValueCollection GetTestReportExpectedValues(DatabaseUser user, long testReportId)
        {
            return _sqlServerTestReportExpectedValue.Select(user, testReportId);
        }

        #endregion

    }
}
