﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Test;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.Test.SqlServer
{
    public class SqlServerTestReport : SqlServerBase
    {
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String TestReportId = "test_report_id";
            public static String TestName = "test_name";
            public static String Description = "description";
            public static String ReportName = "report_name";
            public static String ReportType = "report_type";
            public static String OverrideReportPath = "override_report_path";
        }

        #region main

        public TestReportCollection Select(DatabaseUser user)
        {
            using (DataBaseCommand command = GetStoredProcCommand("TestReport_select", user.DatabaseName))
            {
                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToTestReportCollection(reader);
                }
            }
        }
        #endregion

        #region data mapping
        protected TestReportCollection MapToTestReportCollection(IDataReader reader)
        {
            TestReportCollection collection = new TestReportCollection();
            while (reader.Read())
            {
                collection.Add(MapToTestReport(reader));
            }

            return collection;
        }

        protected TestReport MapToTestReport(IDataReader reader)
        {
            TestReport testReport = new TestReport();
            MapToBase(testReport, reader);

            testReport.TestReportId = (long)CleanDataValue(reader[ColumnNames.TestReportId]);
            testReport.TestName = (String)CleanDataValue(reader[ColumnNames.TestName]);
            testReport.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            testReport.ReportName = (String)CleanDataValue(reader[ColumnNames.ReportName]);
            testReport.ReportType = (String)CleanDataValue(reader[ColumnNames.ReportType]);
            testReport.OverrideReportPath = (String)CleanDataValue(reader[ColumnNames.OverrideReportPath]);

            return testReport;
        }
        #endregion
    }
}