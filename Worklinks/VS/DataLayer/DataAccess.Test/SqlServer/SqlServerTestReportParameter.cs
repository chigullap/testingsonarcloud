﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Test;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.Test.SqlServer
{
    public class SqlServerTestReportParameter : SqlServerBase
    {
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String TestReportParameterId = "test_report_parameter_id";
            public static String TestReportId = "test_report_id";
            public static String ParameterName = "parameter_name";
            public static String Value = "value";
        }

        #region main

        public TestReportParameterCollection Select(DatabaseUser user, long testReportId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("TestReportParameter_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@testReportId", testReportId);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToTestReportParameterCollection(reader);
                }
            }
        }
        #endregion

        #region data mapping
        protected TestReportParameterCollection MapToTestReportParameterCollection(IDataReader reader)
        {
            TestReportParameterCollection collection = new TestReportParameterCollection();
            while (reader.Read())
            {
                collection.Add(MapToTestReportParameter(reader));
            }

            return collection;
        }

        protected TestReportParameter MapToTestReportParameter(IDataReader reader)
        {
            TestReportParameter testReportParameter = new TestReportParameter();
            MapToBase(testReportParameter, reader);

            testReportParameter.TestReportParameterId = (long)CleanDataValue(reader[ColumnNames.TestReportParameterId]);
            testReportParameter.TestReportId = (long)CleanDataValue(reader[ColumnNames.TestReportId]);
            testReportParameter.ParameterName = (String)CleanDataValue(reader[ColumnNames.ParameterName]);
            testReportParameter.Value = (String)CleanDataValue(reader[ColumnNames.Value]);
            
            return testReportParameter;
        }
        #endregion
    }
}