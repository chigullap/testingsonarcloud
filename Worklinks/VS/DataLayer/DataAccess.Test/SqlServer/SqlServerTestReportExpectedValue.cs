﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Test;
using WLP.DataLayer.SqlClient;

namespace WorkLinks.DataLayer.DataAccess.Test.SqlServer
{
    public class SqlServerTestReportExpectedValue : SqlServerBase
    {
        protected new class ColumnNames : SqlServerBase.ColumnNames
        {
            public static String TestReportExpectedValueId = "test_report_expected_value_id";
            public static String TestReportId = "test_report_id";
            public static String Description = "description";
            public static String Datatype = "datatype";
            public static String RowNumber = "row_number";
            public static String ColumnNumber = "column_number";
            public static String ExpectedValue = "expected_value";
        }

        #region main

        public TestReportExpectedValueCollection Select(DatabaseUser user, long testReportId)
        {
            using (DataBaseCommand command = GetStoredProcCommand("TestReportExpectedValue_select", user.DatabaseName))
            {
                command.AddParameterWithValue("@testReportId", testReportId);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return MapToTestReportExpectedValueCollection(reader);
                }
            }
        }
        #endregion

        #region data mapping
        protected TestReportExpectedValueCollection MapToTestReportExpectedValueCollection(IDataReader reader)
        {
            TestReportExpectedValueCollection collection = new TestReportExpectedValueCollection();
            while (reader.Read())
            {
                collection.Add(MapToTestReportExpectedValue(reader));
            }

            return collection;
        }

        protected TestReportExpectedValue MapToTestReportExpectedValue(IDataReader reader)
        {
            TestReportExpectedValue testReportExpectedValue = new TestReportExpectedValue();
            MapToBase(testReportExpectedValue, reader);

            testReportExpectedValue.TestReportExpectedValueId = (long)CleanDataValue(reader[ColumnNames.TestReportExpectedValueId]);
            testReportExpectedValue.TestReportId = (long)CleanDataValue(reader[ColumnNames.TestReportId]);
            testReportExpectedValue.Description = (String)CleanDataValue(reader[ColumnNames.Description]);
            testReportExpectedValue.Datatype = (String)CleanDataValue(reader[ColumnNames.Datatype]);
            testReportExpectedValue.RowNumber = (Int32)CleanDataValue(reader[ColumnNames.RowNumber]);
            testReportExpectedValue.ColumnNumber = (Int32)CleanDataValue(reader[ColumnNames.ColumnNumber]);
            testReportExpectedValue.ExpectedValue = (String)CleanDataValue(reader[ColumnNames.ExpectedValue]);

            return testReportExpectedValue;
        }
        #endregion
    }
}