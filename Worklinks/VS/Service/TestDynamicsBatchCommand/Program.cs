﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessLogic;

namespace TestDynamicsBatchCommand
{
    class Program
    {
        static void Main(string[] args)
        {

          WorkLinksDynamicsManagement dynamicsMgmt = new WorkLinksDynamicsManagement();


            dynamicsMgmt.DeserializeDynamicsFile
            (
                new DatabaseUser() { DatabaseName = "WorkLinks", UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name, LanguageCode = "EN" },
                @"\\wqain01\PayrollFiles\AT\output",
                @"\\wqain01\PayrollFiles\AT\output_processed",
                "john@mckay.ws",
                null,
                null,
                "smtp.teksavvy.com",
                25,
                false,
                "john@mckay.ws",
                @"\\wqain01\PayrollFiles\AT\input",
                WorkLinks.BusinessLayer.BusinessObjects.All.Version.VersionNumber, 
                "mckayj");

        }
    }
}
