﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using WorkLinks.Service.Web.PrivateWebService.Contract;

namespace WorkLinks.Service.Web.PublicWebService.WebClient
{
    public class FileClient : IFile
    {

        #region fields
        private ChannelFactory<IFile> _factory = null;
        private String _wcfServiceUrl = null;
        private long _maxReceivedMessageSize;
        private long _receiveTimeoutTicks;
        private long _sendTimeoutTicks;
        #endregion

        #region properties
        private ChannelFactory<IFile> Factory
        {
            get
            {
                if (_factory == null)
                {
                    BasicHttpBinding binding = new BasicHttpBinding();
                    binding.MaxReceivedMessageSize = _maxReceivedMessageSize;
                    binding.ReceiveTimeout = new TimeSpan(_receiveTimeoutTicks);
                    binding.SendTimeout = new TimeSpan(_sendTimeoutTicks);


                    _factory = new ChannelFactory<IFile>(binding, new EndpointAddress(_wcfServiceUrl));
                }

                return _factory;
            }
        }
        #endregion

        public FileChannelWrapper CreateChannel()
        {
            return new FileChannelWrapper(Factory.CreateChannel());
        }

        #region constructors/destructors
        public FileClient(String wcfServiceUrl, String wcfServiceName, long maxReceivedMessageSize, long receiveTimeoutTicks, long sendTimeoutTicks)
        {
            _wcfServiceUrl = new Uri(new Uri(wcfServiceUrl), wcfServiceName).ToString();
            _maxReceivedMessageSize = maxReceivedMessageSize;
            _receiveTimeoutTicks = receiveTimeoutTicks;
            _sendTimeoutTicks = sendTimeoutTicks;
        }
        #endregion

        public byte[] UploadRequest(byte[] fileData, String fileName, String remoteServerName, String queueName, String queueLabel)
        {
            using (FileChannelWrapper wrapper = CreateChannel())
            {
                return wrapper.Channel.UploadRequest(fileData, fileName, remoteServerName,queueName, queueLabel);
            }
        }


        //public void UploadRequest(System.IO.Stream fileData)
        //{
        //    using (FileChannelWrapper wrapper = CreateChannel())
        //    {
        //        wrapper.Channel.UploadRequest(fileData);
        //    }
        //}
    }


    public class FileChannelWrapper : IDisposable
    {
        IFile _channel = null;

        public IFile Channel
        {
            get
            {
                return _channel;
            }
        }

        public FileChannelWrapper(IFile channel)
        {
            _channel = channel;
        }



        public void Dispose()
        {
            try
            {
                ((IClientChannel)_channel).Close();
            }
            catch (FaultException)
            {
                ((IClientChannel)_channel).Abort();
            }
            catch (CommunicationException)
            {
                ((IClientChannel)_channel).Abort();
            }

        }
    }
}