﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkLinks.Service.Web.PublicWebService.Common
{
    public class ApplicationParameter
    {
        #region fields
        #region from web.config
        private const String _wcfServiceURL = "WcfServiceURL";
        private const String _wcfCodeServiceName = "WcfCodeServiceName";
        private const String _wcfMaxReceivedMessageSize = "WcfMaxReceivedMessageSize";
        private const String _wcfReceiveTimeout = "WcfReceiveTimeout";
        private const String _wcfSendTimeout = "WcfSendTimeout";
        private const String _msmqQueueName = "MsmqQueueName";
        private const String _msmqQueueLabel = "MsmqQueueLabel";
        #endregion
        #endregion

        #region properties
        #region from web.config
        public static String WcfServiceUrl
        {
            get { return System.Configuration.ConfigurationManager.AppSettings.Get(_wcfServiceURL); }
        }
        public static String WcfCodeServiceName
        {
            get { return System.Configuration.ConfigurationManager.AppSettings.Get(_wcfCodeServiceName); }
        }
        public static long WcfMaxReceivedMessageSize
        {
            get { return Convert.ToInt64(System.Configuration.ConfigurationManager.AppSettings.Get(_wcfMaxReceivedMessageSize)); }
        }
        public static String WcfReceiveTimeout
        {
            get { return System.Configuration.ConfigurationManager.AppSettings.Get(_wcfReceiveTimeout); }
        }
        public static String WcfSendTimeout
        {
            get { return System.Configuration.ConfigurationManager.AppSettings.Get(_wcfSendTimeout); }
        }
        public static String MsmqQueueName
        {
            get { return System.Configuration.ConfigurationManager.AppSettings.Get(_msmqQueueName); }
        }
        public static String MsmqQueueLabel
        {
            get { return System.Configuration.ConfigurationManager.AppSettings.Get(_msmqQueueLabel); }
        }
        #endregion
        #endregion
    }
}