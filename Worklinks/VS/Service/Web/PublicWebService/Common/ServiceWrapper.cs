﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WorkLinks.Service.Web.PublicWebService.WebClient;

namespace WorkLinks.Service.Web.PublicWebService.Common
{
    public class ServiceWrapper
    {
        #region fields
        private static FileClient _fileClient = null;
        #endregion


        #region properties
        public static FileClient FileClient
        {
            get
            {
                if (_fileClient == null)
                    _fileClient = new FileClient
                        (
                            ApplicationParameter.WcfServiceUrl,
                            ApplicationParameter.WcfCodeServiceName,
                            ApplicationParameter.WcfMaxReceivedMessageSize,
                            DecodeTimeFrameToTicks(ApplicationParameter.WcfReceiveTimeout),
                            DecodeTimeFrameToTicks(ApplicationParameter.WcfSendTimeout)
                        );

                return _fileClient;
            }

        }

        private static long DecodeTimeFrameToTicks(String timeframe)
        {
            String[] time = timeframe.Split(':');
            return (new TimeSpan(Convert.ToInt32(time[0]), Convert.ToInt32(time[1]), Convert.ToInt32(time[2]))).Ticks;
        }
        #endregion
    }

}