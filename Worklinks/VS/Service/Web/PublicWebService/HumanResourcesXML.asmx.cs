﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using WorkLinks.Service.Web.PublicWebService.Common;

namespace WorkLinks.Service.Web.PublicWebService
{
    /// <summary>
    /// Summary description for HumanResourcesXML
    /// </summary>
    [WebService(Namespace = "http://worklinks.ca/services/hrxml")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class HumanResourcesXML : System.Web.Services.WebService
    {

        [WebMethod]
        public byte[] UploadFile(byte[] fileData)
        {
            return ServiceWrapper.FileClient.UploadRequest(fileData, "HrXml.xml", HttpContext.Current.Request.UserHostAddress, ApplicationParameter.MsmqQueueName, ApplicationParameter.MsmqQueueLabel);
        }

    }
}
