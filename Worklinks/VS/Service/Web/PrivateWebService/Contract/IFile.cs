﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WorkLinks.Service.Web.PrivateWebService.Contract
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IFile" in both code and config file together.
    [ServiceContract]
    public interface IFile
    {
        [OperationContract]
        byte[] UploadRequest(byte[] fileData, String fileName, String remoteServerName,String queueName, String queueLabel);

        //[OperationContract]
        //void UploadRequest(Stream fileData);
    }
}
