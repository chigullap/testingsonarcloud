﻿using System;
using System.IO;
using System.Messaging;
using NLog;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessLogic;
using WorkLinks.BusinessLayer.BusinessObjects.Central;
using WorkLinks.Service.Web.PrivateWebService.Common;

namespace WorkLinks.Service.Web.PrivateWebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "File" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select File.svc or File.svc.cs at the Solution Explorer and start debugging.
    public class File : Contract.IFile
    {
        private readonly XmlManagement _xmlManagment = new XmlManagement();
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger(typeof(File));

        public byte[] UploadRequest(byte[] fileData, string inputFileName, string remoteServerName, string queueName, string queueLabel)
        {
            string filePath = null;
            string sourceBodid = null;
            string sourceCreationDateTime = null;

            _logger.Info($"Upload request received: inputFileName={inputFileName};remoteServerName={remoteServerName};queueName={queueName};queueLabel={queueLabel}");

            try
            {
                if (!Directory.Exists(ApplicationParameter.MessageDirectoryPathName))
                {
                    Directory.CreateDirectory(ApplicationParameter.MessageDirectoryPathName);
                }

                if (!Directory.Exists(ApplicationParameter.XsltWorkingDirectoryPathName))
                {
                    Directory.CreateDirectory(ApplicationParameter.XsltWorkingDirectoryPathName);
                }

                (string queueIdentifier, string bodid, string creationDate) = QueueIdentifier.IdentifyQueue(fileData);

                if (ApplicationParameter.QueueSplittingEnabled && !string.IsNullOrWhiteSpace(queueIdentifier))
                {
                    _logger.Info($"Renaming passed queue name {queueName} based on xml content...");
                    queueName = $"{queueName}_{queueIdentifier.Substring(0,3).ToLowerInvariant()}";
                    _logger.Info($"Queue renamed to {queueName}");
                }

                sourceBodid = bodid;
                sourceCreationDateTime = creationDate;

                string fileName = $"{Guid.NewGuid()}__{sourceBodid ?? "Missing"}.xml";
               
                filePath = Path.Combine(ApplicationParameter.MessageDirectoryPathName, fileName);
                _logger.Info($"Writing request bytes to {filePath}");
                System.IO.File.WriteAllBytes(filePath, fileData);

                //validate 
                if (sourceBodid == null)
                {
                    sourceBodid = "***BODID MISSING***";
                    throw new Exception("upload missing BODID");
                }

                MessageQueue queue;
                if (MessageQueue.Exists(queueName))
                {
                    _logger.Info("Retrieving existing queue");
                    queue = new MessageQueue(queueName);
                }
                else
                {
                    // Create the Queue
                    _logger.Info("Creating new queue");
                    MessageQueue.Create(queueName, true);
                    queue = new MessageQueue(queueName);
                }

                RemoteImportExportLog log = new RemoteImportExportLog
                {
                    ImportFlag = true,
                    FilePath = filePath,
                    RemoteServer = remoteServerName,
                    UpdateUser = "HRXML",
                    UpdateDatetime = DateTime.Now
                };

                byte[] rtn;
                using (MessageQueueTransaction tran = new MessageQueueTransaction())
                {
                    tran.Begin();

                    Message msg = new Message
                    {
                        Formatter = new XmlMessageFormatter(new[] {typeof(RemoteImportExportLog)}),
                        Body = log
                    };

                    _logger.Info($"Sending message to queue {queueName}");
                    queue.Send(msg, queueLabel, tran);
                    _logger.Info("Message queued successfully");

                    rtn = BuildResponse(filePath,
                        Path.Combine(ApplicationParameter.XsltWorkingDirectoryPathName, Path.GetFileName(filePath)),
                        "Accepted",
                        sourceCreationDateTime,
                        sourceBodid);

                    tran.Commit();
                }

                return rtn;
            }
            catch (Exception exc)
            {
                _logger.Error($"Error occurred: {exc}");
                ErrorManagement.HandleError(new DatabaseUser {DatabaseName = ApplicationParameter.DatabaseName, UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name, LanguageCode = "EN"}, "File.UploadRequest", exc);
                return BuildResponse(filePath, Path.Combine(ApplicationParameter.XsltWorkingDirectoryPathName, Path.GetFileName(filePath)), "Rejected", sourceCreationDateTime, sourceBodid);
            }
        }

        private byte[] BuildResponse(string inputFilePath, string outputFilePath, string responseExpression, string sourceCreateDateTime, string sourceBodid)
        {
            DatabaseUser user = new DatabaseUser
            {
                DatabaseName = ApplicationParameter.DatabaseName,
                UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name,
                LanguageCode = "EN"
            };
            return _xmlManagment.BuildResponse(user, inputFilePath, outputFilePath, responseExpression, sourceCreateDateTime, sourceBodid);
        }
    }
}