﻿using System.IO;
using System.Text;

namespace WorkLinks.Service.Web.PrivateWebService.Common
{
    public class StreamConverter
    {
        /// <summary>
        /// Checks the beginning of the UTF-8 or UTF-16 stream for a BOM
        /// </summary>
        /// <param name="sourceStream">Stream to check</param>
        /// <returns>A new BOM-less stream should the BOM exist, the original stream if not</returns>
        public Stream RemoveByteOrderMark(Stream sourceStream)
        {
            byte[] preamble = Encoding.UTF8.GetPreamble();
            byte[] buffer = new byte[preamble.Length];
            sourceStream.Read(buffer, 0, preamble.Length);

            if (StartsWith(buffer, preamble))
            {
                buffer = new byte[16 * 1024];
                MemoryStream ms = new MemoryStream();

                int read;
                while ((read = sourceStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                ms.Flush();
                ms.Position = 0;

                return ms;
            }

            sourceStream.Position = 0;
            return sourceStream;
        }

        private static bool StartsWith(byte[] thisArray, byte[] otherArray)
        {
            if (otherArray.Length > thisArray.Length) return false;

            for (int i = 0; i < otherArray.Length; i++)
            {
                if (thisArray[i] != otherArray[i])
                {
                    return false;
                }
            }

            return true;
        }

        public byte[] ConvertStreamToByteArray(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
