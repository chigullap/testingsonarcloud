﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkLinks.Service.Web.PrivateWebService.Common
{
    public class ApplicationParameter
    {
        #region fields
        #region from web.config
        private const String _messageDirectoryPathName = "MessageDirectoryPathName";
        private const String _databaseName = "DatabaseName";
        private const String _xsltWorkingDirectoryPathName = "XsltWorkingDirectoryPathName";
        private const string _queueSplittingEnabled = "QueueSplittingEnabled";
        #endregion
        #endregion

        #region properties
        #region from web.config
        public static String MessageDirectoryPathName
        {
            get { return System.Configuration.ConfigurationManager.AppSettings.Get(_messageDirectoryPathName); }
        }
        public static String DatabaseName
        {
            get { return System.Configuration.ConfigurationManager.AppSettings.Get(_databaseName); }
        }
        public static String XsltWorkingDirectoryPathName
        {
            get { return System.Configuration.ConfigurationManager.AppSettings.Get(_xsltWorkingDirectoryPathName); }
        }

        public static bool QueueSplittingEnabled
        {
            get
            {
                bool.TryParse(System.Configuration.ConfigurationManager.AppSettings.Get(_queueSplittingEnabled), out bool result);
                return result;
            }
        }
        
        #endregion
        #endregion

    }
}