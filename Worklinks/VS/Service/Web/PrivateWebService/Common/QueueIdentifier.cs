﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace WorkLinks.Service.Web.PrivateWebService.Common
{
    public class QueueIdentifier
    {
        public static (string QueueIdentifier, string BODID, string CreationDate) IdentifyQueue(byte[] xmlSource)
        {
            if (xmlSource == null || xmlSource.Length == 0)
            {
                throw new Exception("Empty file received");
            }
            
            const string ERROR = "error";
            string sourceBodidNode = null;
            string sourceCreationDateTimeNode = null;
            string logicalId;
            XDocument doc;

            using (Stream bomlessStream = new StreamConverter().RemoveByteOrderMark(new MemoryStream(xmlSource)))
            {
                doc = XDocument.Load(new XmlTextReader(bomlessStream));
                bomlessStream.Close();
            }

            XElement senderNode = doc.Descendants().FirstOrDefault(el => el.Name.LocalName == "Sender");
            //step one, find the application area
            XElement nodes = doc.Descendants().FirstOrDefault(el => el.Name.LocalName == @"ApplicationArea");
            if (nodes != null)
            {
                sourceBodidNode = nodes.Descendants().FirstOrDefault(el => el.Name.LocalName == "BODID")?.Value;
                sourceCreationDateTimeNode = nodes.Descendants().FirstOrDefault(el => el.Name.LocalName == "CreationDateTime")?.Value;
            }

            if (senderNode == null)
            {
                logicalId = ERROR;
            }
            else
            {
                logicalId = senderNode.Descendants().FirstOrDefault(el => el.Name.LocalName == "LogicalID")?.Value ?? ERROR;
            }


            return (logicalId, sourceBodidNode, sourceCreationDateTimeNode);
        }
    }
}