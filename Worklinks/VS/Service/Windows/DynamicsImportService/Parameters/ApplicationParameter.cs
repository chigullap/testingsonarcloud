﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessLogic;

namespace DynamicsImportService.Parameters
{
    public static class ApplicationParameter
    {

        #region fields

        #region from web.config
        private const String _centralDatabaseName = "CentralDatabaseName";
        #endregion

        private const String _dynamicsXMLOutputFolderCode = "DYNOUT";
        private const String _dynamicsXMLProcessedFolderCode = "XMLPROCD";
        private const String _dbVersion = "VERSION";
        private const String _emailFromAddress = "MAILFROM";
        private const String _emailLoginUser = "MAILLOGN";
        private const String _emailPassword = "MAILPASS";
        private const String _emailPort = "MAILPORT";
        private const String _emailSSL = "MAILSSL";
        private const String _emailSMTPServerAddress = "MAILSMTP";
        private const String _dynamicsInboxScanInterval = "GPINBXSC";
        private const String _dynamicsOutboxScanInterval = "GPOTBXSC";
        private const String _dynamicsInboxTimeoutInterval = "GPINBXTO";
        private const String _dynamicsOutboxTimeoutInterval = "GPOTBXTO";
        private const String _dynamicsGPInputFolderCode = "GPINPUT";
        private const String _dynamicsWorklinksAdminEmailCode = "WLADMAIL";
        private const String _dynamicsWorklinksSupportEmailCode = "WLSPMAIL";

        private static CodeCollection CodeSystemCollection = (new CodeManagement()).GetCodeTable(ApplicationParameter.CentralDatabaseName, "EN", CodeTableType.System);
        #endregion

        #region global properties
        #region from web.config
        public static String CentralDatabaseName
        {
            get { return System.Configuration.ConfigurationManager.AppSettings.Get(_centralDatabaseName); }
        }
        #endregion

        public static String GetSystemValue(String code)
        {
            return CodeSystemCollection[code].Description;
        }

        public static String DynamicsInboxScanInterval
        {
            get { return GetSystemValue(_dynamicsInboxScanInterval); }
        }
        public static String DynamicsOutboxScanInterval
        {
            get { return GetSystemValue(_dynamicsOutboxScanInterval); }
        }
        public static String DynamicsInboxTimeoutInterval
        {
            get { return GetSystemValue(_dynamicsInboxTimeoutInterval); }
        }
        public static String DynamicsOutboxTimeoutInterval
        {
            get { return GetSystemValue(_dynamicsOutboxTimeoutInterval); }
        }
        public static String DynamicsGPInputFolder
        {
            get { return GetSystemValue(_dynamicsGPInputFolderCode); }
        }
        public static String DynamicsWorklinksAdminEmailCode
        {
            get { return GetSystemValue(_dynamicsWorklinksAdminEmailCode); }
        }
        public static String DynamicsWorklinksSupportEmailCode
        {
            get { return GetSystemValue(_dynamicsWorklinksSupportEmailCode); }
        }
        public static String DynamicsXMLOutputFolder
        {
            get { return GetSystemValue(_dynamicsXMLOutputFolderCode); }
        }
        public static String DynamicsXMLProcessedFolder
        {
            get { return GetSystemValue(_dynamicsXMLProcessedFolderCode); }
        }
        public static String EmailFromAddress
        {
            get { return GetSystemValue(_emailFromAddress); }
        }
        public static String EmailLoginUser
        {
            get { return GetSystemValue(_emailLoginUser); }
        }
        public static String EmailPassword
        {
            get { return GetSystemValue(_emailPassword); }
        }
        public static String EmailSMTPServerAddress
        {
            get { return GetSystemValue(_emailSMTPServerAddress); }
        }
        public static int EmailPort
        {
            get { return Convert.ToInt32(GetSystemValue(_emailPort)); }
        }
        public static bool EmailSSL
        {
            get { return Convert.ToBoolean(GetSystemValue(_emailSSL)); }
        }
        public static String DatabaseVersion
        {
            get { return GetSystemValue(_dbVersion); }
        }
        #endregion
    }
}
