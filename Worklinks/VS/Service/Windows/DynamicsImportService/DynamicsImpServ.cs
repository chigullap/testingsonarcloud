﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.IO;
using WorkLinks.BusinessLayer.BusinessLogic;
using System.Configuration;
using System.Timers;
using DynamicsImportService.Parameters;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.BusinessLayer.BusinessObjects;
//using System.Threading;

namespace DynamicsImportService
{
    public partial class DynamicsImpServ : ServiceBase
    {
        #region fields
        private Timer _mainTimer = new Timer();
        private Timer _inboxTimer = new Timer();
        private Timer _outboxTimer = new Timer();

        private WorkLinksDynamicsManagement _dynamicsMgmt = new WorkLinksDynamicsManagement();
        private ReportManagement _reportManagement = new ReportManagement();
        private bool _connectionDropped = false;
        private const String processUserName = "worklinksprocess";
        //admin email
        private String worklinksAdminEmail = ApplicationParameter.DynamicsWorklinksAdminEmailCode;
        private String worklinksSupportEmail = ApplicationParameter.DynamicsWorklinksSupportEmailCode;
        private String emailFrom = ApplicationParameter.EmailFromAddress;
        private String emailLoginUser = ApplicationParameter.EmailLoginUser;
        private String emailLoginPassword = ApplicationParameter.EmailPassword;
        private String emailSmtp = ApplicationParameter.EmailSMTPServerAddress;
        private int emailPort = ApplicationParameter.EmailPort;
        //inbox/outbox monitoring
        private Int32 inboxMonitorInterval = Convert.ToInt32(ApplicationParameter.DynamicsInboxScanInterval) * 1000;
        private Int32 inboxMonitorTimeout = Convert.ToInt32(ApplicationParameter.DynamicsInboxTimeoutInterval) * 1000;
        private Int32 outboxMonitorInterval = Convert.ToInt32(ApplicationParameter.DynamicsOutboxScanInterval) * 1000;
        private Int32 outboxMonitorTimeout = Convert.ToInt32(ApplicationParameter.DynamicsOutboxTimeoutInterval) * 1000;
        //email messages
        private String inboxMsgSub = "Inbox directory not found";
        private String inboxMsgBody = "The inbox directory, specified by the code GPINPUT (" + ApplicationParameter.DynamicsGPInputFolder + ") was not found.  ";
        private String outboxMsgSub = "Outbox directory not found";
        private String outboxMsgBody = "The outbox directory, specified by the code DYNOUT (" + ApplicationParameter.DynamicsXMLOutputFolder + ") was not found.  ";
        private String processedMsgSub = "Processed directory not found";
        private String processedMsgBody = "The processed directory, specified by the code XMLPROCD (" + ApplicationParameter.DynamicsXMLProcessedFolder + ") was not found.  ";

        private String inboxMonitorMsgSub = "Dynamics Batch Command is taking too much time";
        private String inboxMonitorMsgBody = "The Dynamics Batch Command process is taking too much time on one file.  It has passed the timeout set at: " + ApplicationParameter.DynamicsInboxTimeoutInterval + " seconds.  ";
        private String outboxMonitorMsgSub = "Dynamics Import Service is taking too much time";
        private String outboxMonitorMsgBody = "The Dynamics Import Service is taking too much time on one file.  It has passed the timeout set at: " + ApplicationParameter.DynamicsOutboxTimeoutInterval + " seconds.  ";


        //private const String _reportPingIntervalSecondsKey = "ReportPingIntervalSeconds";
        //private int _reportPingIntervalSeconds = 0;
        //BackgroundWorker _reportPingWorker = null;
        #endregion

        #region properties
        //private BackgroundWorker ReportPingWorker
        //{
        //    get
        //    {
        //        if (_reportPingWorker == null)
        //            _reportPingWorker = new BackgroundWorker();
        //        return _reportPingWorker;
        //    }
        //}
        //private int ReportPingIntervalSeconds
        //{
        //    get 
        //    {
        //        if (_reportPingIntervalSeconds == 0)
        //            _reportPingIntervalSeconds = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get(_reportPingIntervalSecondsKey));

        //        return _reportPingIntervalSeconds;
        //    }
        //}
        #endregion

        public DynamicsImpServ()
        {
            InitializeComponent();
            //InitializeBackGroundWorker();

            if (!System.Diagnostics.EventLog.SourceExists("ServiceSource"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "ServiceSource", "ServiceLog");
            }
            _eventLog1.Source = "ServiceSource";
            _eventLog1.Log = "ServiceLog";

            SetTimerArgs();
            SetInboxTimerArgs();
            SetOutboxTimerArgs();
        }

        #region timer setups
        private void SetTimerArgs()
        {
            _mainTimer.Elapsed += new ElapsedEventHandler(_mainTimer_Elapsed);
            _mainTimer.Interval = 60000; //1 min
            _mainTimer.AutoReset = true;
            _mainTimer.Enabled = true;
        }

        private void SetInboxTimerArgs()
        {
            _inboxTimer.Elapsed += new ElapsedEventHandler(_inboxTimer_Elapsed);
            _inboxTimer.Interval = inboxMonitorInterval;
            _inboxTimer.AutoReset = true;
            _inboxTimer.Enabled = true;
        }

        private void SetOutboxTimerArgs()
        {
            _outboxTimer.Elapsed += new ElapsedEventHandler(_outboxTimer_Elapsed);
            _outboxTimer.Interval = outboxMonitorInterval;
            _outboxTimer.AutoReset = true;
            _outboxTimer.Enabled = true;
        }
        #endregion

        void _inboxTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //sort to get oldest file first, using linq
            DirectoryInfo info = new DirectoryInfo(ApplicationParameter.DynamicsGPInputFolder);
            FileInfo[] file = info.GetFiles().OrderBy(p => p.LastWriteTime).ToArray();

            if (file.Count() > 0)
            {
                DateTime fileDate = file[0].LastWriteTime;

                //do a time compare and also make sure file is within 2 years.  This is because if the file described in the path parameter does not exist, this method returns 12/31/1600.
                if (fileDate < DateTime.Now.AddSeconds(-inboxMonitorTimeout / 1000) && fileDate.Year > DateTime.Now.Year - 2)
                {
                    //passed timeout threshold
                    SendEmailToSupport(worklinksAdminEmail, emailFrom, emailLoginUser, emailLoginPassword, emailSmtp, inboxMonitorMsgSub, inboxMonitorMsgBody + "The file is:  " + ApplicationParameter.DynamicsGPInputFolder + "\\" + file[0]);
                    //increase threshold to avoid too many emails, reset below after file is processed
                    _inboxTimer.Interval *= 2;
                }
                else
                    _inboxTimer.Interval = inboxMonitorInterval;
            }
            else
                _inboxTimer.Interval = inboxMonitorInterval;
        }

        void _outboxTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //sort to get oldest file first, using linq
            DirectoryInfo info = new DirectoryInfo(ApplicationParameter.DynamicsXMLOutputFolder);
            FileInfo[] file = info.GetFiles().OrderBy(p => p.LastWriteTime).ToArray();

            if (file.Count() > 0)
            {
                DateTime fileDate = file[0].LastWriteTime;

                //do a time compare and also make sure file is within 2 years.  This is because if the file described in the path parameter does not exist, this method returns 12/31/1600.
                if (fileDate < DateTime.Now.AddSeconds(-outboxMonitorTimeout / 1000) && fileDate.Year > DateTime.Now.Year - 2)
                {
                    //passed timeout threshold
                    SendEmailToSupport(worklinksAdminEmail, emailFrom, emailLoginUser, emailLoginPassword, emailSmtp, outboxMonitorMsgSub, outboxMonitorMsgBody + "The file is:  " + ApplicationParameter.DynamicsXMLOutputFolder + "\\" + file[0]);
                    //increase threshold to avoid too many emails, reset below after file is processed
                    _outboxTimer.Interval *= 2;
                }
                else
                    _outboxTimer.Interval = outboxMonitorInterval;
            }
            else
                _outboxTimer.Interval = outboxMonitorInterval;
        }

        //private void InitializeBackGroundWorker()
        //{
        //    ReportPingWorker.WorkerSupportsCancellation = true;

        //    // Set event handlers
        //    this.ReportPingWorker.DoWork += ReportPingWorker_DoWork;
        //    this.ReportPingWorker.RunWorkerCompleted += ReportPingWorker_RunWorkerCompleted;

        //}

        //void ReportPingWorker_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    // Start processing.
        //    while (!ReportPingWorker.CancellationPending)
        //    {
        //        try
        //        {
        //            _reportManagement.GetReport(processUserName,"Ping",new string[0],"PDF");
        //        }
        //        catch (Exception exc)
        //        {
        //            _eventLog1.WriteEntry("Exception message is:  " + exc.Message);
        //            _eventLog1.WriteEntry("Exception stacktrace is:  " + exc.StackTrace);
        //        }
        //        finally
        //        {
        //            //sleep until next cycle
        //            System.Threading.Thread.Sleep(ReportPingIntervalSeconds *1000);
        //        }
        //    }
        //}
        //void ReportPingWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        //{
        //    throw new NotImplementedException();
        //}

        void _mainTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //check if we can connect to the path
            if (!_connectionDropped && !Directory.Exists(ApplicationParameter.DynamicsXMLOutputFolder))
            {
                _connectionDropped = true;
            }
            else
            {
                //if the connection was dropped and it can connect to the path, restart the service.
                if (_connectionDropped && Directory.Exists(ApplicationParameter.DynamicsXMLOutputFolder))
                {
                    //restart the service
                    OnStart(null);
                }
            }
        }

        protected override void OnStart(string[] args)
        {
            _eventLog1.WriteEntry("OnStart triggered");

            // Start ping thread
            //ReportPingWorker.RunWorkerAsync();

            //If the inbox directory cannot be found, send an email indicating so.
            if (!Directory.Exists(ApplicationParameter.DynamicsGPInputFolder))
                SendEmailToSupport(worklinksAdminEmail, emailFrom, emailLoginUser, emailLoginPassword, emailSmtp, inboxMsgSub, inboxMsgBody);
            else
                _inboxTimer.Start();

            //If the outbox directory cannot be found, send an email indicating so.
            if (!Directory.Exists(ApplicationParameter.DynamicsXMLOutputFolder))
                SendEmailToSupport(worklinksAdminEmail, emailFrom, emailLoginUser, emailLoginPassword, emailSmtp, outboxMsgSub, outboxMsgBody);
            else
                _outboxTimer.Start();

            //If the processed directory cannot be found, send an email indicating so.
            if (!Directory.Exists(ApplicationParameter.DynamicsXMLProcessedFolder))
                SendEmailToSupport(worklinksAdminEmail, emailFrom, emailLoginUser, emailLoginPassword, emailSmtp, processedMsgSub, processedMsgBody);

            FileSystemWatcher watcher = new FileSystemWatcher();
            try
            {
                //set up monitoring here
                watcher.Filter = "*.xml";

                //webservice call to get the path to the xml files generated by DynamicsBatchCommand
                watcher.Path = ApplicationParameter.DynamicsXMLOutputFolder;

                //Enable the FileSystemWatcher events
                watcher.EnableRaisingEvents = true;

                //subscribe to the file created event
                watcher.Created += new FileSystemEventHandler(watcher_FileCreated);

                //process any files already existing in the directory
                foreach (string file in Directory.GetFiles(watcher.Path, watcher.Filter))
                {
                    string path = Path.GetDirectoryName(file);
                    string fileName = Path.GetFileName(file);
                    FileSystemEventArgs fileEventArgs = new FileSystemEventArgs(WatcherChangeTypes.Created, path, fileName);
                    watcher_FileCreated(this, fileEventArgs); //raise the event
                }

                _connectionDropped = false;
            }
            catch (Exception e)
            {
                _eventLog1.WriteEntry("Exception was triggered, error is:" + e.Message + " and the Inner exception is: " + e.InnerException);
            }
        }

        void SendEmailToSupport(String emailTo, String emailFrom, String emailUser, String emailPassword, String emailSmtp, String subject, String body)
        {
            EmailManagement.SendMessage(emailTo, emailFrom, null, null, subject, body, emailSmtp, Convert.ToInt32(emailPort), false, emailUser, emailPassword);
        }

        void watcher_FileCreated(object sender, FileSystemEventArgs e)
        {
            _eventLog1.WriteEntry("Starting to process file, path is: " + e.FullPath);

            //restart the timer if it has stopped
            if (!_mainTimer.Enabled)
                _mainTimer.Start();

            //wait 5 seconds before attempting to process file
            System.Threading.Thread.Sleep(5 * 1000);

            try
            {
                if (e.FullPath.Contains("PR.xml"))
                {
                    // "_PR.xml" is dynamics data needing to be inserted into the worklinks database.  Trigger the proceses to deserialize the xml file written from dynamicsBatchProcess and send email.
                    _dynamicsMgmt.DeserializeDynamicsFile(new DatabaseUser() { DatabaseName = Parameters.ApplicationParameter.CentralDatabaseName, UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name, LanguageCode = "EN" }, ApplicationParameter.DynamicsXMLOutputFolder, ApplicationParameter.DynamicsXMLProcessedFolder, emailFrom, emailLoginUser, emailLoginPassword,
                        emailSmtp, emailPort, ApplicationParameter.EmailSSL, worklinksSupportEmail, ApplicationParameter.DynamicsGPInputFolder, ApplicationParameter.DatabaseVersion, processUserName);

                    _eventLog1.WriteEntry("The file, " + e.FullPath + ", has been processed!");
                }
                else
                {
                    //write to log
                    _eventLog1.WriteEntry("This is not a recognized file, " + e.FullPath);
                }
            }
            catch (Exception exc)
            {
                _eventLog1.WriteEntry("Exception was triggered with " + e.FullPath + " file, could still be in use, waiting 5 seconds and trying again.");
                _eventLog1.WriteEntry("Exception message is:  " + exc.Message);
                _eventLog1.WriteEntry("Exception stacktrace is:  " + exc.StackTrace);
                System.Threading.Thread.Sleep(5 * 1000);
                watcher_FileCreated(this, e);
            }
        }

        protected override void OnStop()
        {
            _eventLog1.WriteEntry("OnStop triggered");
            //ReportPingWorker.CancelAsync();
        }

        #region unused events
        //extra events
        protected override void OnPause()
        {
            _eventLog1.WriteEntry("OnPause triggered");
        }
        protected override void OnContinue()
        {
            _eventLog1.WriteEntry("OnContinue triggered");
        }
        protected override void OnShutdown()
        {
            _eventLog1.WriteEntry("OnShutdown triggered");
        }
        #endregion

    }
}
