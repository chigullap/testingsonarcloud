﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessLogic;
using System.Configuration;

namespace CraExportEngine.Parameters
{
    public static class ApplicationParameter
    {
        #region from web.config
        private const string _centralDatabaseName = "CentralDatabaseName";
        #endregion

        //config file values
        public static string OverrideDateConfig = ConfigurationManager.AppSettings["OverrideDate"];
        public static int PollIntervalSecondsKey = Convert.ToInt32(ConfigurationManager.AppSettings["PollIntervalSeconds"]);

        //RBC EFT Source Deductions
        private const string _isaSenderInterchangeIDSourceDed = "RBC_ID1S";
        private const string _isaReceiverInterchangeIDSourceDed = "RBC_ID2S";
        private const string _isaTestIndicatorSourceDed = "RBC_INDS";
        private const string _craBankCodeTransitNumberSourceDed = "RBC_CRAT";
        private const string _craAccountNumberSourceDed = "RBC_CRAA";
        private const string _gsSenderInterchangeIdSourceDed = "RBC_SNID";
        private const string _gsApplicationReceiversCodeSourceDed = "RBC_ARCD";
        private const string _refItReferenceNumberSourceDed = "RBC_REF";
        private const string _n1PrNameSourceDed = "RBC_NM";
        private const string _directDepositBankCodeTransitNumber = "RBC_FI_T";
        private const string _directDepositAccountNumber = "RBC_FI_A";

        //Revenu Quebec Source Deductions
        private const string _rqFiTransitNumber = "RQFITRAN";
        private const string _rqBankAccount = "RQBNKACT";
        private const string _rqPartnerId = "RQPARTID";

        //mail
        private const string _emailToAddress = "MAILTO";
        private const string _emailFromAddress = "MAILFROM";
        private const string _emailCcAddress = "MAILCC";
        private const string _emailBccAddress = "MAILBCC";
        private const string _emailSMTPServerAddress = "MAILSMTP";
        private const string _emailPort = "MAILPORT";
        private const string _emailLoginUser = "MAILLOGN";
        private const string _emailPassword = "MAILPASS";
        private const string _emailSSL = "MAILSSL";

        private static CodeCollection CodeSystemCollection = (new CodeManagement()).GetCodeTable(ApplicationParameter.CentralDatabaseName, "EN", CodeTableType.System);

        #region global properties

        #region from web.config
        public static string CentralDatabaseName
        {
            get { return ConfigurationManager.AppSettings.Get(_centralDatabaseName); }
        }
        #endregion

        public static string GetSystemValue(string code)
        {
            return CodeSystemCollection[code].Description;
        }

        //RBC EFT SOURCE DEDUCTIONS
        public static string IsaSenderInterchangeIDSourceDed { get { return GetSystemValue(_isaSenderInterchangeIDSourceDed); } }
        public static string IsaReceiverInterchangeIDSourceDed { get { return GetSystemValue(_isaReceiverInterchangeIDSourceDed); } }
        public static string IsaTestIndicatorSourceDed { get { return GetSystemValue(_isaTestIndicatorSourceDed); } }
        public static string CraBankCodeTransitNumberSourceDed { get { return GetSystemValue(_craBankCodeTransitNumberSourceDed); } }
        public static string CraAccountNumberSourceDed { get { return GetSystemValue(_craAccountNumberSourceDed); } }
        public static string GsSenderInterchangeIdSourceDed { get { return GetSystemValue(_gsSenderInterchangeIdSourceDed); } }
        public static string GsApplicationReceiversCodeSourceDed { get { return GetSystemValue(_gsApplicationReceiversCodeSourceDed); } }
        public static string RefItReferenceNumberSourceDed { get { return GetSystemValue(_refItReferenceNumberSourceDed); } }
        public static string N1PrNameSourceDed { get { return GetSystemValue(_n1PrNameSourceDed); } }
        public static string DirectDepositBankCodeTransitNumber { get { return GetSystemValue(_directDepositBankCodeTransitNumber); } }
        public static string DirectDepositAccountNumber { get { return GetSystemValue(_directDepositAccountNumber); } }

        //Revenu Quebec Source Deductions
        public static string RqFiTransitNumber { get { return GetSystemValue(_rqFiTransitNumber); } }
        public static string RqBankAccount { get { return GetSystemValue(_rqBankAccount); } }
        public static string RqPartnerId { get { return GetSystemValue(_rqPartnerId); } }


        //SEND EMAIL TO SUPPORT IF EXCEPTION OCCURS
        public static string EmailToAddress { get { return GetSystemValue(_emailToAddress); } }
        public static string EmailFromAddress { get { return GetSystemValue(_emailFromAddress); } }
        public static string EmailCcAddress { get { return GetSystemValue(_emailCcAddress); } }
        public static string EmailBccAddress { get { return GetSystemValue(_emailBccAddress); } }
        public static string EmailSMTPServerAddress { get { return GetSystemValue(_emailSMTPServerAddress); } }
        public static int EmailPort { get { return Convert.ToInt32(GetSystemValue(_emailPort)); } }
        public static string EmailLoginUser { get { return GetSystemValue(_emailLoginUser); } }
        public static string EmailPassword { get { return GetSystemValue(_emailPassword); } }
        public static bool EmailSSL { get { return Convert.ToBoolean(GetSystemValue(_emailSSL)); } }

        #endregion
    }
}
