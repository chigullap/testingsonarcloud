﻿using System;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace CraExportEngine.Parameters
{
    public static class ParameterHelper
    {
        public static RbcEftSourceDeductionParameters CreateAndPopulateRbcSourceDeductionParamters()
        {
            RbcEftSourceDeductionParameters parms = new RbcEftSourceDeductionParameters();

            parms.IsaSenderInterchangeIDSourceDed = ApplicationParameter.IsaSenderInterchangeIDSourceDed;
            parms.IsaReceiverInterchangeIDSourceDed = ApplicationParameter.IsaReceiverInterchangeIDSourceDed;
            parms.IsaTestIndicatorSourceDed = ApplicationParameter.IsaTestIndicatorSourceDed;
            parms.DirectDepositBankCodeTransitNumber = ApplicationParameter.DirectDepositBankCodeTransitNumber;
            parms.DirectDepositAccountNumber = ApplicationParameter.DirectDepositAccountNumber;
            parms.CraBankCodeTransitNumberSourceDed = ApplicationParameter.CraBankCodeTransitNumberSourceDed;
            parms.CraAccountNumberSourceDed = ApplicationParameter.CraAccountNumberSourceDed;
            parms.GsSenderInterchangeIdSourceDed = ApplicationParameter.GsSenderInterchangeIdSourceDed;
            parms.GsApplicationReceiversCodeSourceDed = ApplicationParameter.GsApplicationReceiversCodeSourceDed;
            parms.RefItReferenceNumberSourceDed = ApplicationParameter.RefItReferenceNumberSourceDed;
            parms.N1PrNameSourceDed = ApplicationParameter.N1PrNameSourceDed;

            return parms;
        }

        public static RbcRqSourceDeductionParameters CreateAndPopulateRbcRqSourceDeductionParamters()
        {
            RbcRqSourceDeductionParameters parms = new RbcRqSourceDeductionParameters();

            parms.IsaSenderInterchangeIDSourceDed = ApplicationParameter.IsaSenderInterchangeIDSourceDed;
            parms.IsaReceiverInterchangeIDSourceDed = ApplicationParameter.IsaReceiverInterchangeIDSourceDed;
            parms.IsaTestIndicatorSourceDed = ApplicationParameter.IsaTestIndicatorSourceDed;
            parms.GsApplicationReceiversCodeSourceDed = ApplicationParameter.GsApplicationReceiversCodeSourceDed;
            parms.DirectDepositBankCodeTransitNumber = ApplicationParameter.DirectDepositBankCodeTransitNumber;
            parms.DirectDepositAccountNumber = ApplicationParameter.DirectDepositAccountNumber;
            parms.RqFiTransitNumber = ApplicationParameter.RqFiTransitNumber;
            parms.RqBankAccount = ApplicationParameter.RqBankAccount;
            parms.RqPartnerId = ApplicationParameter.RqPartnerId;
            parms.N1PrNameSourceDed = ApplicationParameter.N1PrNameSourceDed;

            return parms;
        }

        public static SendEmailParameters CreateAndPopulateCentralEmailParameters()
        {
            SendEmailParameters parms = new SendEmailParameters();

            parms.ToAddress = ApplicationParameter.EmailToAddress;
            parms.FromAddress = ApplicationParameter.EmailFromAddress;
            parms.CcAddress = ApplicationParameter.EmailCcAddress;
            parms.BccAddress = ApplicationParameter.EmailBccAddress;
            parms.Subject = "CRA EXPORT ENGINE HAS THROWN AN EXCEPTION";
            parms.SmtpHostName = ApplicationParameter.EmailSMTPServerAddress;
            parms.SmtpPortNumber = ApplicationParameter.EmailPort;
            parms.SmtpLogin = ApplicationParameter.EmailLoginUser;
            parms.SmtpPassword = ApplicationParameter.EmailPassword;
            parms.EnableSSL = ApplicationParameter.EmailSSL;

            return parms;
        }
    }
}
