﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

using CraExportEngine.Parameters;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessLogic;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;
using WorkLinks.BusinessLayer.BusinessLogic.Central;
using System.IO;
using System.Threading;

namespace CraExportEngine
{
    public partial class CraExportEngine : ServiceBase
    {
        #region fields
        BackgroundWorker _craWorker = null;
        private ReportManagement _reportMgmt = new ReportManagement();
        private EmployeeManagement _empMgmt = new EmployeeManagement();
        private const string _userName = "cra_export_engine";
        private const string _languageCode = "EN";
        DateTime _todaysDate = DateTime.MinValue;
        #endregion

        #region properties
        private BackgroundWorker CraWorker
        {
            get
            {
                if (_craWorker == null)
                    _craWorker = new BackgroundWorker();
                return _craWorker;
            }
        }
        #endregion

        #region SERVICE SECTION
        public CraExportEngine()
        {
            InitializeComponent();

            if (!EventLog.SourceExists("ServiceSource"))
            {
                EventLog.CreateEventSource("ServiceSource", "ServiceLog");
            }
            _eventLog.Source = "ServiceSource";
            _eventLog.Log = "ServiceLog";

            InitializeBackGroundWorker();
        }
        private void InitializeBackGroundWorker()
        {
            CraWorker.WorkerSupportsCancellation = true;

            // Set event handlers
            CraWorker.DoWork += new DoWorkEventHandler(CraWorker_DoWork);
            CraWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(CraWorker_RunWorkerCompleted);
        }
        protected override void OnStart(string[] args)
        {
            _eventLog.WriteEntry("OnStart triggered:  " + DateTime.Now.ToString());
            _eventLog.WriteEntry("sleeping for 20 seconds.");

            //Sleep for 20 seconds at the start so we can attach if we are debugging.
            Thread.Sleep(20000);

            //this is the remittance worker
            CraWorker.RunWorkerAsync();
        }
        protected override void OnStop()
        {
            CraWorker.CancelAsync();
            _eventLog.WriteEntry("OnStop triggered:  " + DateTime.Now.ToString());
        }
        #endregion

        #region REMITTANCE SECTION
        private void CraWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!CraWorker.CancellationPending)
            {
                try
                {
                    //perform remittances
                    PerformDirectDepositRemittance();
                    PerformCraRemits();
                    PerformChequeWcbRemits();
                    PerformChequeHealthTaxRemits();
                    PerformRqRemits();
                }
                catch (Exception ex)
                {
                    _eventLog.WriteEntry("Exception thrown:  " + ex.Message, EventLogEntryType.Error);

                    //send an email to support
                    SendEmailParameters emailParms = ParameterHelper.CreateAndPopulateCentralEmailParameters();
                    emailParms.Body = "An exception was thrown: Error is:  " + ex.Message + " and the Inner exception is:  " + ex.InnerException;

                    EmailManagement.SendMessage(emailParms);
                }
                finally
                {
                    //sleep until next cycle
                    _eventLog.WriteEntry("sleeping for " + ApplicationParameter.PollIntervalSecondsKey.ToString() + " seconds.");
                    Thread.Sleep(ApplicationParameter.PollIntervalSecondsKey * 1000);
                }
            }
        }
        private void CraWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //do nothing
        }

        #region Remit methods
        private void SetDate()
        {
            //get todays date
            _todaysDate = DateTime.Now;

            //check if we are overriding the date
            if (ApplicationParameter.OverrideDateConfig != "")
            {
                _todaysDate = Convert.ToDateTime(ApplicationParameter.OverrideDateConfig);
                //write to event log that we are overriding the date
                _eventLog.WriteEntry("DATE IS BEING OVERRIDDEN IN THE app.config WITH A VALUE OF:  " + _todaysDate.ToString(), EventLogEntryType.Warning);
            }
            else
                _eventLog.WriteEntry("DATE BEING USED CURRENTLY HAS A VALUE OF:  " + _todaysDate.ToString());
        }
        private void PerformDirectDepositRemittance()
        {
            //set dbuser to use the central db
            DatabaseUser user = new DatabaseUser() { DatabaseName = ApplicationParameter.CentralDatabaseName, UserName = _userName, LanguageCode = _languageCode };

            //get a list of companies using the pre-authorized debit / auto send of direct deposit payroll file from the central db
            _eventLog.WriteEntry("Getting a list of companies using the direct deposit / cra garnishment remittance");
            DirectDepositDatabaseMapCollection ddMapColl = new DirectDepositManagement().GetDirectDepositDatabaseMapCollection(user);

            //set date
            SetDate();

            _eventLog.WriteEntry("Start cycling through the list of companies using the direct deposit remittance, number of companies:  " + ddMapColl.Count);

            foreach (DirectDepositDatabaseMap map in ddMapColl)
            {
                //switch to the company database to get their direct deposit / cra garnishment file(s)
                user.DatabaseName = map.DatabaseName;

                /*  
                 *  To submit direct deposit files using this service, the customer must be using either:
                 *  1.  pre-authorized debit for RBC Canada AND use our RBC eft file type
                 *  2.  pre-authorized debit for SCOTIA Jamaica AND use our SCOTIA eft file type
                 *  2.  pre-authorized debit for RBC Canada AND use CRA Garnishments
                 *  3.  all of the above.
                */

                //pad
                bool usingPreAuthorizedDebitFlag = Convert.ToBoolean(new CodeManagement().GetCodeTable(user.DatabaseName, "EN", CodeTableType.System, "PA_DEBIT")[0].Description);

                //direct deposit/garn
                bool usingRbcEdiForDirectDepositsFlag = Convert.ToBoolean(new CodeManagement().GetCodeTable(user.DatabaseName, "EN", CodeTableType.System, "EFTTYPE")[0].Description == "RBC_EDI");
                bool usingScotiaBankEdiForDirectDepositsFlag = Convert.ToBoolean(new CodeManagement().GetCodeTable(user.DatabaseName, "EN", CodeTableType.System, "EFTTYPE")[0].Description == "SCOT_EDI");
                bool usingCraGarnishmentFlag = Convert.ToBoolean(new CodeManagement().GetCodeTable(user.DatabaseName, "EN", CodeTableType.System, "GARSUBMT")[0].Description);
                //ftp
                string rbcFtpname = new CodeManagement().GetCodeTable(user.DatabaseName, "EN", CodeTableType.System, "RBC_FTP")[0].Description;
                string scotiaFtpname = new CodeManagement().GetCodeTable(user.DatabaseName, "EN", CodeTableType.System, "SCOT_FTP")[0].Description;

                if (usingPreAuthorizedDebitFlag && (usingRbcEdiForDirectDepositsFlag || usingCraGarnishmentFlag || usingScotiaBankEdiForDirectDepositsFlag))
                {
                    /*
                     *  Unlike the other processes, this one does not create files and FTP them.  This is a send only process, therefore there's no need to check if there are any 
                     *  direct deposit/cra garnishment files that need to be re-sent due to FTP error.
                     *  
                     *  For the direct deposit/cra garnishment files, they are created during the POST of a payroll.  
                     *  
                     *  If there were FTP issues and the service was restarted, it will automatically grab them (if date is 2 business days before cheque_date and file_sent_flag = 0 for the file) 
                     *  and resend because the "file_sent_flag" is only updated from 0 to 1 after a successful FTP
                    */
                    _eventLog.WriteEntry("Checking for any direct deposit/cra garnishment files that need to be sent");

                    //2.  check if any direct deposit files and/or cra garnishment need to be sent
                    _reportMgmt.ExportDirectDepositCraGarnishmentFtps(user, _todaysDate, usingRbcEdiForDirectDepositsFlag, usingCraGarnishmentFlag, usingScotiaBankEdiForDirectDepositsFlag, rbcFtpname, scotiaFtpname);
                }
            }

            _eventLog.WriteEntry("Done cycling through the list of companies using the direct deposit remittance");
        }
        private void PerformChequeHealthTaxRemits()
        {
            //set dbuser to use the central db
            DatabaseUser user = new DatabaseUser() { DatabaseName = ApplicationParameter.CentralDatabaseName, UserName = _userName, LanguageCode = _languageCode };

            //get a list of companies using the cheque wcb remittance export from the central db
            _eventLog.WriteEntry("Getting a list of companies using the cheque health tax remittance export");
            ChequeHealthTaxRemittanceDatabaseMapCollection chequeHealthTaxRemitDbColl = new ChequeHealthRemittanceManagement().GetChequeHealthTaxRemittanceDatabaseMap(user);

            //do not need to gather static data for the rbc file from the central db because this uses the direct deposit/cheque file and all parms are on client db

            //set date
            SetDate();

            _eventLog.WriteEntry("Start cycling through the list of companies using the cheque health tax remittance export, number of companies:  " + chequeHealthTaxRemitDbColl.Count);

            foreach (ChequeHealthTaxRemittanceDatabaseMap map in chequeHealthTaxRemitDbColl)
            {
                //switch to the company database to get their export data
                user.DatabaseName = map.DatabaseName;

                //Check if the code system flag is turned on (we may turn it off to prevent files from being created)
                bool usingChequeWcbFlag = Convert.ToBoolean(new CodeManagement().GetCodeTable(user.DatabaseName, "EN", CodeTableType.System, "HTAX_CHQ")[0].Description);
                string rbcFtpname = new CodeManagement().GetCodeTable(user.DatabaseName, "EN", CodeTableType.System, "RBC_FTP")[0].Description;

                if (usingChequeWcbFlag)
                {
                    _eventLog.WriteEntry("Checking for any unprocessed cheque health tax files that need to be sent");

                    //1.  Check if there are any unprocessed export files in the database that need to be sent (in the case there were ftp issues and the service was restarted)
                    _reportMgmt.CheckForUnprocessedChequeHealthTaxExports(user, rbcFtpname);

                    _eventLog.WriteEntry("Checking for any cheque health tax files that need to be sent");
                    //2.  Check if we need to create an invoice and email the customer, or if we have to create a cheque health tax export file and FTP it.
                    _reportMgmt.ExportChequeHealthTaxFtps(user, _todaysDate, rbcFtpname);
                }
            }

            _eventLog.WriteEntry("Done cycling through the list of companies using the cheque wcb remittance export");
        }
        private void PerformChequeWcbRemits()
        {
            //set dbuser to use the central db
            DatabaseUser user = new DatabaseUser() { DatabaseName = ApplicationParameter.CentralDatabaseName, UserName = _userName, LanguageCode = _languageCode };

            //get a list of companies using the cheque wcb remittance export from the central db
            _eventLog.WriteEntry("Getting a list of companies using the cheque wcb remittance export");
            ChequeWcbRemittanceDatabaseMapCollection chequeWcbRemitDbColl = new ChequeWcbRemittanceManagement().GetChequeWcbRemittanceDatabaseMap(user);

            //do not need to gather static data for the rbc file from the central db because this uses the direct deposit/cheque file and all parms are on client db

            //set date
            SetDate();

            _eventLog.WriteEntry("Start cycling through the list of companies using the cheque wcb remittance export, number of companies:  " + chequeWcbRemitDbColl.Count);

            foreach (ChequeWcbRemittanceDatabaseMap map in chequeWcbRemitDbColl)
            {
                //switch to the company database to get their export data
                user.DatabaseName = map.DatabaseName;

                //Check if the code system flag is turned on (we may turn it off to prevent files from being created)
                bool usingChequeWcbFlag = Convert.ToBoolean(new CodeManagement().GetCodeTable(user.DatabaseName, "EN", CodeTableType.System, "WCB_CHQ")[0].Description);

                if (usingChequeWcbFlag)
                {
                    _eventLog.WriteEntry("Checking for any unprocessed cheque wcb files that need to be sent");
                    string rbcFtpname = new CodeManagement().GetCodeTable(user.DatabaseName, "EN", CodeTableType.System, "RBC_FTP")[0].Description;

                    //1.  Check if there are any unprocessed export files in the database that need to be sent (in the case there were ftp issues and the service was restarted)
                    _reportMgmt.CheckForUnprocessedChequeWcbExports(user, rbcFtpname);

                    _eventLog.WriteEntry("Checking for any cheque wcb files that need to be sent");
                    //2.  Check if we need to create an invoice and email the customer, or if we have to create a cheque wcb export file and FTP it.
                    _reportMgmt.ExportChequeWcbFtps(user, _todaysDate, rbcFtpname);
                }
            }

            _eventLog.WriteEntry("Done cycling through the list of companies using the cheque wcb remittance export");
        }
        private void PerformCraRemits()
        {
            //set dbuser to use the central db
            DatabaseUser user = new DatabaseUser() { DatabaseName = ApplicationParameter.CentralDatabaseName, UserName = _userName, LanguageCode = _languageCode };

            //get a list of companies using the cra remittance export from the central db
            _eventLog.WriteEntry("Getting a list of companies using the cra remittance export");
            CraRemittanceDatabaseMapCollection craRemitDbColl = new CraRemittanceManagement().GetCraRemittanceDatabaseMap(user);

            //gather static data for the rbc file from the central db
            RbcEftSourceDeductionParameters parms = ParameterHelper.CreateAndPopulateRbcSourceDeductionParamters();

            //set date
            SetDate();

            _eventLog.WriteEntry("Start cycling through the list of companies using the cra remittance export, number of companies:  " + craRemitDbColl.Count);

            foreach (CraRemittanceDatabaseMap map in craRemitDbColl)
            {
                //switch to the company database to get their export data
                user.DatabaseName = map.DatabaseName;

                _eventLog.WriteEntry("Checking for any unprocessed cra files that need to be sent");
                string rbcFtpname = new CodeManagement().GetCodeTable(user.DatabaseName, "EN", CodeTableType.System, "RBC_FTP")[0].Description;

                //1.  check if there are any unprocessed export files in the database that need to be sent (in the case there were ftp issues and the service was restarted)
                _reportMgmt.CheckForUnprocessedExports(user, rbcFtpname);

                //2.  check if we need to create a CRA export file and FTP it.
                _eventLog.WriteEntry("Checking for any cra files that need to be sent");

                //assign this parameter which comes from the client db
                parms.CompanyShortName = new CodeManagement().GetCodeTable(user.DatabaseName, "EN", CodeTableType.System, "CMPSHORT")[0].Description;

                //get a list of the different remit frequencies for this company
                BusinessNumberCollection businessCollection = _empMgmt.GetBusinessNumber(user, null);
                var distinctRemitCodes = businessCollection.Select(x => x.CodeCraRemittancePeriodCd).Distinct();

                foreach (String remitCode in distinctRemitCodes)
                {
                    _reportMgmt.ExportCraFtps(user, _todaysDate, remitCode, parms, rbcFtpname);
                }
            }

            _eventLog.WriteEntry("Done cycling through the list of companies using the cra remittance export");
        }
        private void PerformRqRemits()
        {
            //set dbuser to use the central db
            DatabaseUser user = new DatabaseUser() { DatabaseName = ApplicationParameter.CentralDatabaseName, UserName = _userName, LanguageCode = _languageCode };

            //get a list of companies using the rq remittance export from the central db
            _eventLog.WriteEntry("Getting a list of companies using the rq remittance export");
            RevenuQuebecRemittanceDatabaseMapCollection rqRemitDbColl = new RevenuQuebecRemittanceManagement().GetRevenuQuebecRemittanceDatabaseMap(user);

            //gather static data for the rbc file from the central db
            RbcRqSourceDeductionParameters parms = ParameterHelper.CreateAndPopulateRbcRqSourceDeductionParamters();

            //set date
            SetDate();

            _eventLog.WriteEntry("Start cycling through the list of companies using the Revenu Quebec remittance export, number of companies:  " + rqRemitDbColl.Count);

            int numberOfUnprocessedFiles = 0;

            //right now customers only have 1 revenu quebec number but keeping the foreach for when we move to multiple revenu quebec numbers
            foreach (RevenuQuebecRemittanceDatabaseMap map in rqRemitDbColl)
            {
                //switch to the company database to get their export data
                user.DatabaseName = map.DatabaseName;

                _eventLog.WriteEntry("Checking for any unprocessed Revenue Quebec files that need to be sent");
                string rbcFtpname = new CodeManagement().GetCodeTable(user.DatabaseName, "EN", CodeTableType.System, "RBC_FTP")[0].Description;

                //1.  check if there are any unprocessed export files in the database that need to be sent (in the case there were ftp issues and the service was restarted)
                numberOfUnprocessedFiles = _reportMgmt.CheckForUnprocessedRevenuQuebecExports(user, rbcFtpname);

                _eventLog.WriteEntry("Done checking for any unprocessed Revenue Quebec files that need to be sent.  Number of files:  " + numberOfUnprocessedFiles);
                numberOfUnprocessedFiles = 0;

                //2.  check if we need to create a RQ export file and FTP it.
                _eventLog.WriteEntry("Checking for any Revenue Quebec files that need to be sent");

                //get a list of the different remit frequencies for this company
                BusinessNumberCollection businessCollection = _empMgmt.GetBusinessNumber(user, null);
                var distinctRemitCodes = businessCollection.Select(x => x.CodeRevenuQuebecRemittancePeriodCd).Distinct();

                foreach (String remitCode in distinctRemitCodes)
                {
                    _reportMgmt.ExportRqFtps(user, _todaysDate, remitCode, parms, rbcFtpname);
                }
            }

            _eventLog.WriteEntry("Done cycling through the list of companies using the Revenu Quebec remittance export.");
        }
        #endregion

        #endregion
    }
}
