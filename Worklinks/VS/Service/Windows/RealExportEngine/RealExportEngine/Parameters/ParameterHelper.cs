﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessLogic;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace RealExportEngine.Parameters
{
    public  class ParameterHelper
    {
        private const String _realFtp = "REAL_FTP";
        private const String _laborLevelExportFileName = "LL_FNAME";
        private const String _employeeExportFileName = "EI_FNAME";
        private const String _adjustmentRuleExportFileName = "AR_FNAME";

        public static String RealFtp { get { return GetSystemValue(_realFtp); } }
        public static String LaborLevelExportFileName { get { return GetSystemValue(_laborLevelExportFileName); } }
        public static String EmployeeExportFileName { get { return GetSystemValue(_employeeExportFileName); } }
        public static String AdjustmentRuleExportFileName { get { return GetSystemValue(_adjustmentRuleExportFileName); } }

        public static String GetSystemValue(String code)
        {
            return CodeSystemCollection[code].Description;
        }

        private static CodeCollection CodeSystemCollection = (new CodeManagement()).GetCodeTable(ApplicationParameter.CustomerDatabaseName, "EN", CodeTableType.System);

    }
}
