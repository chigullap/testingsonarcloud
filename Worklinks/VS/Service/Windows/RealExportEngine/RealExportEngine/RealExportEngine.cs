﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WorkLinks.BusinessLayer.BusinessLogic;
using WLP.BusinessLayer.BusinessObjects;
using RealExportEngine.Parameters;

namespace RealExportEngine
{
    public partial class RealExportEngine : ServiceBase
    {

        #region fields
        private WorkLinksDynamicsManagement _worklinksDynamicsManagement = new WorkLinksDynamicsManagement();
        private const string _userName = "wl_real_export_engine";
        private const string _languageCode = "EN";
        #endregion

        #region SERVICE SECTION
        public RealExportEngine()
        {
            InitializeComponent();

            if (!EventLog.SourceExists("RealExportEng"))
            {
                EventLog.CreateEventSource("RealExportEng", "RealExport");
            }
            _eventLog.Source = "RealExportEng";
            _eventLog.Log = "RealExport";
        }

        protected override void OnStart(string[] args)
        {
            _eventLog.WriteEntry("OnStart triggered:  " + DateTime.Now.ToString());
            _eventLog.WriteEntry("sleeping for 20 seconds.");

            //Sleep for 20 seconds at the start so we can attach if we are debugging.
            Thread.Sleep(20000);

            //set dbuser to use the customer db
            DatabaseUser user = new DatabaseUser() { DatabaseName = ApplicationParameter.CustomerDatabaseName, UserName = _userName, LanguageCode = _languageCode };

            _eventLog.WriteEntry("About to send export files to SFTP site");

            try
            {
                //export to SFTP
                _worklinksDynamicsManagement.CreateAndExportRealEmployeeFiles(
                        user, ParameterHelper.RealFtp, ParameterHelper.LaborLevelExportFileName, ParameterHelper.EmployeeExportFileName, ParameterHelper.AdjustmentRuleExportFileName);

                _eventLog.WriteEntry("Done exporting files.");
            }
            catch (Exception ex)
            {
                _eventLog.WriteEntry("Exception thrown:  " + ex.Message, EventLogEntryType.Error);
            }

            OnStop();
        }

        protected override void OnStop()
        {
            _eventLog.WriteEntry("OnStop triggered:  " + DateTime.Now.ToString());
        }
        #endregion
    }
}
