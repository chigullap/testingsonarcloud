﻿using System;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessLogic;

namespace StatDedImport.Parameters
{
    public static class ApplicationParameter
    {
        #region fields

        #region from web.config
        private const String _databaseName = "DatabaseName";
        #endregion

        private const String _statDedInputFolder = "STAT_IN";
        private const String _statDedOutputFolder = "STAT_OUT";
        private const String _dbVersion = "VERSION";

        private static CodeCollection CodeSystemCollection = (new CodeManagement()).GetCodeTable(ApplicationParameter.DatabaseName, "EN", CodeTableType.System);

        #endregion

        #region global properties

        #region from web.config
        public static String DatabaseName
        {
            get { return System.Configuration.ConfigurationManager.AppSettings.Get(_databaseName); }
        }
        #endregion

        public static String GetSystemValue(String code)
        {
            return CodeSystemCollection[code].Description;
        }
        public static String StatDedInputFolder
        {
            get { return GetSystemValue(_statDedInputFolder); }
        }
        public static String StatDedOutputFolder
        {
            get { return GetSystemValue(_statDedOutputFolder); }
        }
        public static String DatabaseVersion
        {
            get { return GetSystemValue(_dbVersion); }
        }

        #endregion
    }
}
