﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Timers;
using Nerdle.AutoConfig;
using NLog;
using QueueMonitoringService.Parameters;
using WorkLinks.BusinessLayer.BusinessLogic;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;

namespace QueueMonitoringService
{
    internal class QueueManager : IDisposable
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger(typeof(QueueManager));

        private readonly Timer _checkQueuesTimer;
        private readonly object _padLock = new object();
        private Dictionary<string, Dictionary<string, QueueMonitorResult>> _currentQueueSituation;

        public QueueManager()
        {
            _checkQueuesTimer = new Timer(10*1000); // Let's start at 10 seconds, the first refresh will configure based on config file
            _checkQueuesTimer.Elapsed += CheckQueues;
            _currentQueueSituation = new Dictionary<string, Dictionary<string, QueueMonitorResult>>();
        }

        #region IDisposable

        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose()
        {
            _currentQueueSituation?.Clear();
            _checkQueuesTimer?.Dispose();
        }

        #endregion

        private void CheckQueues(object sender, ElapsedEventArgs e)
        {
            try
            {
                _logger.Info("CheckQueues called, refreshing settings...");
                List<QueueWorker> queueWorkers = RefreshSettings();
                _logger.Info($"Queues configured for monitoring - {queueWorkers.Count}");

                foreach (QueueWorker worker in queueWorkers)
                {
                    _logger.Info($"Kicking off queue {worker.QueueName}...");
                    worker.CheckQueue(QueueWorkerComplete);
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Error checking queues during timer elapsed callback: {ex}");
            }
        }

        public void BeginMonitoring()
        {
            _checkQueuesTimer.Start();
        }

        public void StopMonitoring()
        {
            _checkQueuesTimer.Stop();
        }

        private void QueueWorkerComplete(QueueWorker worker)
        {
            _logger.Info($"Worker thread for queue {worker.QueueName} is complete");
            if (worker.QueueIsStuck)
            {
                _logger.Info($"Queue {worker.QueueName} has messages stuck...filtering for new messages since last report");
                List<QueueMonitorResult> newMessages = FilterForNewMessages(worker);

                if (newMessages.Count > 0)
                {
                    _logger.Info($"There are {newMessages.Count} new messages, sending alert...");
                    // Woooo, Wooooo, Wooooo....alert, alert!
                    SubmitAlertMessage(newMessages);
                }
            }
        }

        private void SubmitAlertMessage(IReadOnlyCollection<QueueMonitorResult> newMessages)
        {
            try
            {
                StringBuilder message = new StringBuilder($"The following queue(s) have messages that have overstayed their welcome.");

                message.AppendLine();
                message.AppendLine($"There are {newMessages.Count} new messages stuck.");

                foreach (QueueMonitorResult newMessage in newMessages.OrderBy(m=>m.QueueName))
                {
                    message.AppendLine(newMessage.ToString());
                }

                // Send email
                SendEmailParameters emailParms = ParameterHelper.CreateAndPopulateCentralEmailParameters();
                emailParms.Body = $"{message}";

                EmailManagement.SendMessage(emailParms);

                _logger.Info($"Email alert sent to {emailParms.ToAddress}, content {emailParms.Body}");
            }
            catch (Exception e)
            {
                _logger.Error($"Error sending alert message: {e}");
            }
        }

        private List<QueueMonitorResult> FilterForNewMessages(QueueWorker worker)
        {
            List<QueueMonitorResult> results = new List<QueueMonitorResult>();
            List<QueueMonitorResult> newMessages = new List<QueueMonitorResult>();

            lock (_padLock)
            {
                Dictionary<string, QueueMonitorResult> queue = _currentQueueSituation[worker.QueueName];

                // Loop through all the new messages, adding the new ones to the results list
                foreach (QueueMonitorResult message in worker.HangingMessages)
                {
                    if (queue.TryGetValue(message.MessageId, out QueueMonitorResult result))
                    {
                        results.Add(message);
                    }
                    else
                    {
                        newMessages.Add(message);
                    }
                }

                // Replace the current message list with the latest received
                _currentQueueSituation[worker.QueueName] = results.Union(newMessages).ToDictionary(m => m.MessageId, m => m);
            }

            // Return the new messages and let the caller determine what to do
            return newMessages;
        }

        private List<QueueWorker> RefreshSettings()
        {
            // Pull from app.config
            IQueueConfigurationSection queueConfigs = AutoConfig.Map<IQueueConfigurationSection>();

            if (queueConfigs == null)
            {
                const string message = "Could not find a queue configuration section in the app.config";
                _logger.Error(message);
                throw new Exception(message);
            }

            // Set the timer's interval in case someone changed it between calls
            _checkQueuesTimer.Interval = queueConfigs.PollTimeSeconds * 1000;

            // Now see if we have any queues configured
            List<IQueueConfiguration> configs = (queueConfigs.Configurations ?? Enumerable.Empty<IQueueConfiguration>()).ToList();

            // And set up the current queue sit.
            ConfigureQueueSituation(configs);

            return configs.Select(config => new QueueWorker(config, _logger)).ToList();
        }

        private void ConfigureQueueSituation(List<IQueueConfiguration> configs)
        {
            Dictionary<string, Dictionary<string, QueueMonitorResult>> tempQueues = new Dictionary<string, Dictionary<string, QueueMonitorResult>>();

            lock (_padLock)
            {
                foreach (IQueueConfiguration config in configs)
                {
                    tempQueues.Add(config.QueueName, _currentQueueSituation.ContainsKey(config.QueueName)
                        ? _currentQueueSituation[config.QueueName]
                        : new Dictionary<string, QueueMonitorResult>());
                }

                _currentQueueSituation = tempQueues;
            }
        }
    }

    internal class QueueWorker
    {
        private readonly long _messageHangSeconds;
        private readonly ILogger _logger;

        public QueueWorker(IQueueConfiguration config, ILogger logger)
        {
            QueueName = config.QueueName;
            _messageHangSeconds = config.MessageHangSeconds;
            _logger = logger;
        }

        public List<QueueMonitorResult> HangingMessages { get; private set; }

        public bool QueueIsStuck => HangingMessages?.Count > 0;

        public string QueueName { get; }

        public void CheckQueue(Action<QueueWorker> callback)
        {
            _logger.Info($"Checking queue {QueueName}...clearing previous messages.");
            HangingMessages = new List<QueueMonitorResult>();

            if (!string.IsNullOrEmpty(QueueName))
            {
                // Check queue
                if (MessageQueue.Exists(QueueName))
                {
                    using (MessageQueue queue = ConstructMessageQueue())
                    {
                        List<Message> messages = queue.GetAllMessages().Where(m => MessageIsStuck(m.ArrivedTime)).ToList();

                        _logger.Info($"Queue {QueueName} has {messages.Count} stuck messages on the queue.");

                        // Populate the hanging messages
                        HangingMessages.AddRange(messages.Select(m => new QueueMonitorResult
                        {
                            MessageId = m.Id,
                            QueueName = QueueName,
                            TimeOnQueue = GetTimeOnQueue(m.ArrivedTime),
                            Body = (RemoteImportExportLog)m.Body
                        }));
                    }
                }
                else
                {
                    _logger.Info($"Queue {QueueName} does not exist, aborting queue check.");
                }
            }

            callback?.Invoke(this);
        }

        private TimeSpan GetTimeOnQueue(DateTime timeArrived)
        {
            return DateTime.Now - timeArrived;
        }

        private bool MessageIsStuck(DateTime timeArrived)
        {
            return GetTimeOnQueue(timeArrived).Seconds >= _messageHangSeconds;
        }

        private MessageQueue ConstructMessageQueue()
        {
            MessageQueue queue = new MessageQueue(QueueName)
            {
                MessageReadPropertyFilter =
                {
                    Id = true,
                    ArrivedTime = true,
                    Body = true
                },
                Formatter = new XmlMessageFormatter(new []{typeof(RemoteImportExportLog)})
            };

            return queue;
        }
    }

    internal class QueueMonitorResult
    {
        public string QueueName { get; set; }
        public string MessageId { get; set; }
        public TimeSpan TimeOnQueue { get; set; }
        public RemoteImportExportLog Body { get; set; }

        #region Overrides of Object

        /// <summary>Returns a string that represents the current object.</summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return $"QueueName={QueueName} - MessageId={MessageId} - TimeOnQueue={TimeOnQueue:dd\\:hh\\:mm\\:ss} - File={Body?.FilePath ?? "No message body found, or unable to cast"}";
        }

        #endregion
    }
}