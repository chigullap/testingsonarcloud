﻿using System.Collections.Generic;

namespace QueueMonitoringService
{
    public interface IQueueConfiguration
    {
        string QueueName { get; set; }
        long MessageHangSeconds { get; set; }
    }

    public interface IQueueConfigurationSection
    {
        List<IQueueConfiguration> Configurations { get; set; }
        long PollTimeSeconds { get; set; }
    }
}
