﻿using System;
using System.ComponentModel;
using System.ServiceProcess;
using NLog;

namespace QueueMonitoringService
{
    public partial class MonitoringService : ServiceBase
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger(typeof(MonitoringService));
        private readonly QueueManager _queueManager;

        public MonitoringService()
        {
            InitializeComponent();
            InitializeBackGroundWorker();
            _queueManager = new QueueManager();
        }

        private BackgroundWorker QueueMonitoringThread { get; set; }

        protected override void OnStart(string[] args)
        {
            _logger.Info("Service Start called...starting to monitor import queues");
            QueueMonitoringThread.RunWorkerAsync();
            _logger.Info("Queues are now being monitored on their own thread");

        }

        protected override void OnStop()
        {
            _logger.Info("Service Stop called.");
            QueueMonitoringThread.CancelAsync();
        }

        private void InitializeBackGroundWorker()
        {
            QueueMonitoringThread = new BackgroundWorker {WorkerSupportsCancellation = true};
            QueueMonitoringThread.DoWork += QueueMonitoringThreadOnDoWork;
            QueueMonitoringThread.RunWorkerCompleted += QueueMonitoringThreadOnRunWorkerCompleted;
        }

        private void QueueMonitoringThreadOnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _logger.Info("Cleaning up the queue manager");
            _queueManager?.StopMonitoring();
            _queueManager?.Dispose();

            _logger.Info($"Shutting down import worker thread...{(e.Error == null ? "" : "due to an error")}");
            if (e.Error != null)
            {
                _logger.Error(e.Error);
            }
        }

        private void QueueMonitoringThreadOnDoWork(object sender, DoWorkEventArgs e)
        {
            _logger.Info("Import file worker thread called.");
            while (!QueueMonitoringThread.CancellationPending)
            {
                try
                {
                   _queueManager.BeginMonitoring();
                }
                catch (Exception exc)
                {
                    _logger.Error($"Error within the import file worker thread: {exc.Message}");
           
                    //wait for 10 seconds
                    System.Threading.Thread.Sleep(10 * 1000);
                }
                finally
                {
                    System.Threading.Thread.Sleep(10 * 1000);
                }
            }
        }
    }
}
