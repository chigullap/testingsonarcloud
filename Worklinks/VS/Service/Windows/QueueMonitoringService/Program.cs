﻿using System.ServiceProcess;

namespace QueueMonitoringService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun = new ServiceBase[]
            {
                new MonitoringService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
