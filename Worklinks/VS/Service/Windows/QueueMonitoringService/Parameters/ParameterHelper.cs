﻿using WorkLinks.BusinessLayer.BusinessObjects;

namespace QueueMonitoringService.Parameters
{
    public static class ParameterHelper
    {
        public static SendEmailParameters CreateAndPopulateCentralEmailParameters()
        {
            SendEmailParameters parms = new SendEmailParameters
            {
                ToAddress = ApplicationParameter.EmailToAddress,
                FromAddress = ApplicationParameter.EmailFromAddress,
                CcAddress = ApplicationParameter.EmailCcAddress,
                BccAddress = ApplicationParameter.EmailBccAddress,
                Subject = "Hrxml Import Queue Stuck",
                SmtpHostName = ApplicationParameter.EmailSMTPServerAddress,
                SmtpPortNumber = ApplicationParameter.EmailPort,
                SmtpLogin = ApplicationParameter.EmailLoginUser,
                SmtpPassword = ApplicationParameter.EmailPassword,
                EnableSSL = ApplicationParameter.EmailSSL
            };

            return parms;
        }
    }
}
