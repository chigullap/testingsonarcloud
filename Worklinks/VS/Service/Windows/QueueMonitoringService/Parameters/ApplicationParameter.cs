﻿using System;
using System.Configuration;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessLogic;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace QueueMonitoringService.Parameters
{
    public static class ApplicationParameter
    {
        private const string _centralDatabaseName = "CentralDatabaseName";
        private const string _emailToAddress = "MAILTO";
        private const string _emailFromAddress = "MAILFROM";
        private const string _emailCcAddress = "MAILCC";
        private const string _emailBccAddress = "MAILBCC";
        private const string _emailSMTPServerAddress = "MAILSMTP";
        private const string _emailPort = "MAILPORT";
        private const string _emailLoginUser = "MAILLOGN";
        private const string _emailPassword = "MAILPASS";
        private const string _emailSSL = "MAILSSL";

        private static readonly CodeCollection CodeSystemCollection = new CodeManagement().GetCodeTable(CentralDatabaseName, "EN", CodeTableType.System);

        public static string CentralDatabaseName => ConfigurationManager.AppSettings.Get(_centralDatabaseName);

        public static string GetSystemValue(string code)
        {
            return CodeSystemCollection[code].Description;
        }

        public static string EmailToAddress => GetSystemValue(_emailToAddress);
        public static string EmailFromAddress => GetSystemValue(_emailFromAddress);
        public static string EmailCcAddress => GetSystemValue(_emailCcAddress);
        public static string EmailBccAddress => GetSystemValue(_emailBccAddress);
        public static string EmailSMTPServerAddress => GetSystemValue(_emailSMTPServerAddress);
        public static int EmailPort => Convert.ToInt32(GetSystemValue(_emailPort));
        public static string EmailLoginUser => GetSystemValue(_emailLoginUser);
        public static string EmailPassword => GetSystemValue(_emailPassword);
        public static bool EmailSSL => Convert.ToBoolean(GetSystemValue(_emailSSL));
    }
}