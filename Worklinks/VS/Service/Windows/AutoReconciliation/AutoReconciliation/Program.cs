﻿using AutoReconciliation.Parameters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessLogic;
using WorkLinks.BusinessLayer.BusinessLogic.FTP;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace AutoReconciliation
{
    class Program
    {
        #region app.config
        //ftp info
        private static readonly string uriName = System.Configuration.ConfigurationManager.AppSettings["url"];
        private static readonly string login = System.Configuration.ConfigurationManager.AppSettings["user"];
        private static readonly string password = System.Configuration.ConfigurationManager.AppSettings["password"];
        private static readonly string archiveFolder = System.Configuration.ConfigurationManager.AppSettings["archiveFolder"];

        //file info
        private static readonly string remitFileInputPath = System.Configuration.ConfigurationManager.AppSettings["remitFileInputPath"];
        private static readonly string outputFolderPath = System.Configuration.ConfigurationManager.AppSettings["outputPath"];
        private static readonly string logFolderPath = System.Configuration.ConfigurationManager.AppSettings["logPath"];
        private static readonly string errorFolderPath = System.Configuration.ConfigurationManager.AppSettings["errorPath"];
        #endregion

        static void Main(string[] args)
        {
            try
            {
                GetResponseFilesFromFTP();
                ProcessRemitFiles();
            }
            catch (Exception ex)
            {
                //send an email to support
                string subject = $"EXCEPTION THROWN:  RBC Response Files {DateTime.Now}";
                SendEmailParameters emailParms = ParameterHelper.CreateAndPopulateCentralEmailParameters(subject);
                emailParms.Body = "An exception was thrown in the Auto Reconciliation engine: Error is:  " + ex.Message + " and the Inner exception is:  " + ex.InnerException;

                EmailManagement.SendMessage(emailParms);
            }
        }

        #region methods

        public static void GetResponseFilesFromFTP()
        {
            //this will connect to the ftp server, download the response files to the remittance folder specified in app.config, then move the files to the Archive folder on the ftp server
            SftpManagement ftpClass = new SftpManagement();
            ftpClass.DownloadAllFtpsFilesAndArchive(uriName, login, password, remitFileInputPath, archiveFolder);
        }

        public static void ProcessRemitFiles()
        {
            //get list of files in the directory
            string[] fileList = Directory.GetFiles(remitFileInputPath);
            bool exitLoop = false;

            //create 997 class
            ReconcileClass input = new ReconcileClass();

            using (StreamWriter logWriter = File.AppendText(logFolderPath + $"\\log_{DateTime.Now:yyyyMMddHHmmss}.txt"))
            {
                //write to log
                logWriter.Write(Environment.NewLine + "Starting processing" + Environment.NewLine + "------------------------------------------");

                int errorCount = 0;

                foreach (string inputFile in fileList)
                {
                    //make sure object is empty
                    input.ClearReconcileClass();

                    string file = null;
                    using (StreamReader sr = new StreamReader(inputFile))
                    {
                        //Read the file into a string and replace the ~ with a newline char
                        file = sr.ReadToEnd();
                        file = file.Replace("~", Environment.NewLine);

                        //find the delimiter being used
                        string delimiter = file.Substring(3, 1);

                        //used to separate each element then cherry pick certain items from the array...read the new string and based on the segment, split into string arrays.
                        using (StringReader stringRead = new StringReader(file))
                        {
                            string line;

                            while ((line = stringRead.ReadLine()) != null)
                            {
                                if (line.Substring(0, 2) == "ST") //see the file type, 997 or 824
                                {
                                    input.ST = line.Split((Convert.ToChar(delimiter)));
                                    //write to log
                                    logWriter.Write(Environment.NewLine + "File is type : " + input.ST[1]);
                                }
                                else if (line.Substring(0, 3) == "AK1") //isa header number is in here for 997 file
                                {
                                    input.AK1 = line.Split((Convert.ToChar(delimiter)));
                                    //write to log
                                    logWriter.Write(Environment.NewLine + "ISA HEADER#: " + input.AK1[2]);
                                }
                                else if (line.Substring(0, 3) == "AK9")
                                {
                                    input.AK9 = line.Split((Convert.ToChar(delimiter)));

                                    //these values all have to be equal or something is wrong
                                    if (input.AK9[2] != input.AK9[3] || input.AK9[2] != input.AK9[4])
                                    {
                                        //write to log
                                        logWriter.Write(Environment.NewLine + "*** ISSUE ***");
                                        logWriter.Write(Environment.NewLine + "File: " + Path.GetFileName(inputFile));
                                        logWriter.Write(Environment.NewLine + "File ISA HEADER#: " + input.AK1[2]);

                                        exitLoop = true;
                                    }
                                }
                                else if (line.Substring(0, 3) == "OTI")
                                {
                                    //this will get overwritten each time we hit an OTI segment but that is ok.  If there's a TED section, we spit out the troublesome ISA# to the log
                                    input.OTI = line.Split((Convert.ToChar(delimiter)));
                                }
                                else if (line.Substring(0, 3) == "TED")
                                {
                                    input.TED = line.Split((Convert.ToChar(delimiter)));

                                    //for CRA 824, it'll be PASSED or PENDING CREDIT APPROVAL.  If PENDING, we need to report that to the bank
                                    if (input.TED[2] == "PAYMENT PENDING CREDIT APPROVAL")
                                    {
                                        //write to log
                                        logWriter.Write(Environment.NewLine + "ISA HEADER#: " + input.OTI[8]);
                                        logWriter.Write(Environment.NewLine + "*** ISSUE ***");
                                        logWriter.Write(Environment.NewLine + "File: " + Path.GetFileName(inputFile));                                        
                                        logWriter.Write(Environment.NewLine + "PAYMENT PENDING CREDIT APPROVAL - check this file");

                                        exitLoop = true;
                                    }
                                    //funds in account are not available
                                    else if (input.TED.Length >= 7 && input.TED[7] == "B-FUNDS NOT AVAILABLE.")
                                    {
                                        //write to log
                                        logWriter.Write(Environment.NewLine + "*** ISSUE ***");
                                        logWriter.Write(Environment.NewLine + "File: " + Path.GetFileName(inputFile));
                                        logWriter.Write(Environment.NewLine + "Account#: " + input.OTI[3]);
                                        logWriter.Write(Environment.NewLine + "Funds are not available - check this file");

                                        exitLoop = true;
                                    }
                                    //account number issue
                                    else if (input.TED.Length >= 7 && input.TED[7].Contains("ACCOUNT NUMBER"))
                                    {
                                        //write to log
                                        logWriter.Write(Environment.NewLine + "*** ISSUE ***");
                                        logWriter.Write(Environment.NewLine + "File: " + Path.GetFileName(inputFile));
                                        logWriter.Write(Environment.NewLine + "Transaction Number#: " + input.OTI[3]);
                                        logWriter.Write(Environment.NewLine + "Account#: " + input.TED[2]);
                                        logWriter.Write(Environment.NewLine + "Account number issue - check this file");

                                        exitLoop = true;
                                    }
                                    //for CRA 824, it'll be PASSED or PENDING CREDIT APPROVAL.  Not a CRA file, the existence of a TED segment means something is wrong
                                    else if (input.TED[2] != "PAYMENT PASSED CREDIT APPROVAL")
                                    {
                                        //write to log
                                        logWriter.Write(Environment.NewLine + "ISA HEADER#: " + input.OTI[8]);
                                        logWriter.Write(Environment.NewLine + "*** ISSUE ***");
                                        logWriter.Write(Environment.NewLine + "File: " + Path.GetFileName(inputFile));
                                        logWriter.Write(Environment.NewLine + "Existence of TED segment means there's an issue - check this file");

                                        exitLoop = true;
                                    }
                                }

                                //there was an error in the 997 file, skip further processing
                                if (exitLoop)
                                {
                                    errorCount++;

                                    //send email with the file as an attachment
                                    SendEmail(false, file, inputFile);

                                    break;
                                }
                            }
                        }
                    }

                    //move file
                    MoveFilesToProcessedOrErrorFolder(exitLoop, inputFile, file);

                    if (!exitLoop)
                        logWriter.Write(Environment.NewLine + "file processed successfully");

                    //write to log
                    logWriter.Write(Environment.NewLine + "------------------------------------------");

                    //reset vars
                    exitLoop = false;
                }

                //write to log
                logWriter.Write(Environment.NewLine + "Processing complete");

                //SEND EMAIL
                //  If there are no files - send no email
                //  If there are files but no issues found - send 1 email
                if (fileList.Count() != 0 && errorCount == 0)
                    SendEmail(true);
            }
        }

        private static void MoveFilesToProcessedOrErrorFolder(bool exitLoop, string inputFile, string file)
        {
            string newPathAndNewFileName = "";

            //if this file had no errors, create a path to the processed folder, else create a path to the error folder
            if (!exitLoop)
                newPathAndNewFileName = Path.Combine(outputFolderPath, Path.GetFileName(inputFile));
            else if (exitLoop)
                newPathAndNewFileName = Path.Combine(errorFolderPath, Path.GetFileName(inputFile));

            //save original file with formatting
            File.WriteAllText(inputFile, file);

            //if filename doesnt already exist in destination, move file to processed folder with new name
            if (!File.Exists(newPathAndNewFileName))
                File.Move(inputFile, newPathAndNewFileName);
            else
            {
                //delete existing file and move file to processed folder
                File.Delete(newPathAndNewFileName);
                File.Move(inputFile, newPathAndNewFileName);
            }
        }

        private static void SendEmail(bool noErrors, string fileContents = null, string fileName = null)
        {
            string subject = $"RBC Response Files {DateTime.Now}" + (noErrors ? "" : " **Issues found * *");
            SendEmailParameters emailParms = ParameterHelper.CreateAndPopulateCentralEmailParameters(subject);

            if (noErrors)
                EmailManagement.SendMessage(emailParms);
            else
            {
                //convert string file contents to byte array
                emailParms.Attachment = new XmlManagement().ConvertStringToByteArray(fileContents);
                emailParms.FileName = Path.GetFileName(fileName);
                EmailManagement.SendMessage(emailParms);
            }
        }

        #endregion
    }

    public class ReconcileClass //contains segments from 997 and 824 that we care about
    {
        public string[] ST;
        public string[] AK1;
        public string[] AK9;
        public string[] TED;
        public string[] OTI;

        public void ClearReconcileClass()
        {
            ST = null;
            AK1 = null;
            AK9 = null;
            TED = null;
            OTI = null;
        }
    }
}
