﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace AutoReconciliation.Parameters
{
    public static class ParameterHelper
    {
        public static SendEmailParameters CreateAndPopulateCentralEmailParameters(string subject)
        {
            SendEmailParameters parms = new SendEmailParameters();

            parms.ToAddress = ApplicationParameter.EmailToAddress;
            parms.FromAddress = ApplicationParameter.EmailFromAddress;
            parms.CcAddress = ApplicationParameter.EmailCcAddress;
            parms.BccAddress = ApplicationParameter.EmailBccAddress;
            parms.Subject = subject;
            parms.SmtpHostName = ApplicationParameter.EmailSMTPServerAddress;
            parms.SmtpPortNumber = ApplicationParameter.EmailPort;
            parms.SmtpLogin = ApplicationParameter.EmailLoginUser;
            parms.SmtpPassword = ApplicationParameter.EmailPassword;
            parms.EnableSSL = ApplicationParameter.EmailSSL;

            return parms;
        }
    }
}
