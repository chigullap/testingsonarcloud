﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessLogic;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace AutoReconciliation.Parameters
{
    public static class ApplicationParameter
    {
        #region from web.config

        private const string _centralDatabaseName = "CentralDatabaseName";
        private const string _email = "Email";
        
        public static string CentralDatabaseName
        {
            get { return System.Configuration.ConfigurationManager.AppSettings.Get(_centralDatabaseName); }
        }
        public static string Email
        {
            get { return System.Configuration.ConfigurationManager.AppSettings.Get(_email); }
        }
        
        #endregion

        //mail

        private const string _emailFromAddress = "MAILFROM";
        private const string _emailCcAddress = "MAILCC";
        private const string _emailBccAddress = "MAILBCC";
        private const string _emailSMTPServerAddress = "MAILSMTP";
        private const string _emailPort = "MAILPORT";
        private const string _emailLoginUser = "MAILLOGN";
        private const string _emailPassword = "MAILPASS";
        private const string _emailSSL = "MAILSSL";

        private static CodeCollection CodeSystemCollection = (new CodeManagement()).GetCodeTable(CentralDatabaseName, "EN", CodeTableType.System);

        #region global properties

        public static string GetSystemValue(string code)
        {
            return CodeSystemCollection[code].Description;
        }

        //SEND EMAIL TO SUPPORT
        public static string EmailToAddress = Email;
        public static string EmailFromAddress { get { return GetSystemValue(_emailFromAddress); } }
        public static string EmailCcAddress { get { return GetSystemValue(_emailCcAddress); } }
        public static string EmailBccAddress { get { return GetSystemValue(_emailBccAddress); } }
        public static string EmailSMTPServerAddress { get { return GetSystemValue(_emailSMTPServerAddress); } }
        public static int EmailPort { get { return Convert.ToInt32(GetSystemValue(_emailPort)); } }
        public static string EmailLoginUser { get { return GetSystemValue(_emailLoginUser); } }
        public static string EmailPassword { get { return GetSystemValue(_emailPassword); } }
        public static bool EmailSSL { get { return Convert.ToBoolean(GetSystemValue(_emailSSL)); } }

        #endregion
    }
}
