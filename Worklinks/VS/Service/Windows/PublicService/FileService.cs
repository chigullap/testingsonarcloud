﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using WorkLinks.Service.Windows.PublicService.Common;

namespace WorkLinks.Service.Windows.PublicService
{
    public partial class FileService : ServiceBase
    {
        #region fields
        private BackgroundWorker _importFileWorker = null;
        private Dictionary<String, DateTime> _lastFiles = null;
        #endregion

        #region properties
        private BackgroundWorker ImportFileWorker
        {
            get
            {
                if (_importFileWorker == null)
                    _importFileWorker = new BackgroundWorker();
                return _importFileWorker;
            }
        }
        #endregion

        public FileService()
        {
            InitializeLog();
            InitializeComponent();
            InitializeBackGroundWorker();
        }
        private void InitializeLog()
        {
            if (!EventLog.SourceExists(ApplicationParameter.EventLogSourceName))
            {
                EventLog.CreateEventSource(ApplicationParameter.EventLogSourceName, ApplicationParameter.EventLogName);
            }
            EventLog.Source = ApplicationParameter.EventLogSourceName;
            EventLog.Log = ApplicationParameter.EventLogName;
        }
        private void InitializeBackGroundWorker()
        {
            ImportFileWorker.WorkerSupportsCancellation = true;
            // Set event handlers
            this.ImportFileWorker.DoWork += ImportFileWorker_DoWork;
            this.ImportFileWorker.RunWorkerCompleted += ImportFileWorker_RunWorkerCompleted;
        }



        public void ProcessFile(String filePath, String queueName, String queueLabel)
        {
            byte[] request = null;
            //lock file and get stream
            using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    fileStream.CopyTo(memoryStream);
                    request = memoryStream.ToArray();
                }

            }
            ServiceWrapper.FileClient.UploadRequest(request, Path.GetFileName(filePath), System.Environment.MachineName, queueName, queueLabel);
            System.Threading.Thread.Sleep(5 * 1000);
            File.Delete(filePath);

        }

        private Dictionary<String, DateTime> GetFiles()
        {
            Dictionary<String, DateTime> files = new Dictionary<string, DateTime>();

            foreach (String filePath in Directory.GetFiles(ApplicationParameter.DirectoryPathName))
            {
                files.Add(filePath, File.GetLastWriteTime(filePath));
            }

            return files;
        }
        private Dictionary<String, DateTime> GetUnchangedFiles(Dictionary<String, DateTime> currentFiles)
        {
            Dictionary<String, DateTime> unchangedFiles = new Dictionary<string, DateTime>();

            foreach (KeyValuePair<String, DateTime> file in currentFiles)
            {
                if (_lastFiles!=null && _lastFiles[file.Key]!=null && _lastFiles[file.Key].Equals(file.Value))
                {
                    unchangedFiles.Add(file.Key,file.Value);
                }
            }

            return unchangedFiles;
        }

        

        protected override void OnStart(string[] args)
        {
            EventLog.WriteEntry("Onstart Triggered");
            ImportFileWorker.RunWorkerAsync();
        }

        protected override void OnStop()
        {
            EventLog.WriteEntry("OnStop Triggered");
            ImportFileWorker.CancelAsync();
        }


        #region event handlers
        void ImportFileWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!ImportFileWorker.CancellationPending)
            {
                try
                {
                    //get current file listing
                    Dictionary<String, DateTime> currentFiles = GetFiles();
                    //get list of files that haven't changed since last lookup
                    foreach (KeyValuePair<String, DateTime> file in GetUnchangedFiles(currentFiles))
                    {
                        ProcessFile(file.Key, ApplicationParameter.MsmqQueueName, ApplicationParameter.MsmqQueueLabel);
                    }

                    _lastFiles = currentFiles;
                }
                catch (Exception exc)
                {
                    EventLog.WriteEntry(exc.Message);
                    _lastFiles = null;
                }
                finally
                {
                    System.Threading.Thread.Sleep(ApplicationParameter.DirectoryWatchIntervalSeconds * 1000);
                }
            }
        }

        void ImportFileWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                EventLog.WriteEntry(e.Error.Message, EventLogEntryType.Error);
            }
            else if (e.Cancelled)
            {

            }
            else
            {

            }
        }
        #endregion
    }
}
