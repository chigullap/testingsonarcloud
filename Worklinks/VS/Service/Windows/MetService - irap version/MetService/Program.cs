﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessLogic;

using met;

namespace MetService
{
    class Program
    {
        #region properties

        private static ConnectionStringSettings _connectionString = ConfigurationManager.ConnectionStrings["WorkLinks"];
        private static int _threadCount = Convert.ToInt16(ConfigurationManager.AppSettings["ThreadCount"]);
        private static String _databaseName = ConfigurationManager.AppSettings["DatabaseName"];
        private static Object lockObject = new Object();

        //will be filled when we get data from the db for the calc. (private static int _totalRowCount = 0;) //testing without db input, set to 5k
        private static int _totalRowCount = 0;
        private static int _rowIndex = -1;
        private static int _printRowIndex = -1;

        private static int _bonusRuns = 0;

        //keep track of open and closed threads
        private static int _threadOpenedCount = 0;
        private static int _threadClosedCount = 0;
        private static bool _controlThreadsActive = true;

        private static List<MetEmployeeData> _controlCollection = new List<MetEmployeeData>();
        private static List<MetEmployeeDataQC> _controlCollectionQC = new List<MetEmployeeDataQC>();
        private static List<PayrollEngineOutput> PayrollOutput = new List<PayrollEngineOutput>();

        //this runs behind firewall so dont need to go thru web services, go directly to mgmt layer
        private static ReportManagement _reportMgmt = new ReportManagement();
        #endregion

        static void Main(string[] args)
        {
            //setup threads CANADA
            LoadDataSyncThread();
        }

        private static void LoadDataSyncThread()
        {
            Console.WriteLine("Loading Main thread");
            Thread thread = new Thread(DataSync);
            thread.Name = String.Format("Main");
            thread.Start();
        }

        private static void DataSync()
        {
            try
            {
                //load data
                _controlCollection = GetData();       //normal canadian run
                //_controlCollection = GetData(true);     //arg1 = canadian bonus run
                //_controlCollectionQC = GetDataQC();   //normal quebec run
                //_controlCollectionQC = GetDataQC(true);   //arg1 = quebec bonus run

                //startup control threads
                _controlThreadsActive = true;

                LoadControlThreads();
                //LoadControlThreadsQC();
            }
            catch (Exception exc)
            {
                Console.WriteLine("Error trying to read table data from memory", exc);
                //shutdown control threads
                ShutdownControlThreads();
            }
        }

        private static void LoadControlThreads()
        {
            Console.WriteLine("Creating threads, number specified in config is: " + _threadCount);

            for (int i = 0; i < _threadCount; i++)
            {
                Thread thread = new Thread(new Program().ProcessData);
                thread.Name = String.Format("Thread{0}", i + 1);
                thread.Start();
            }
        }

        private static void LoadControlThreadsQC()
        {
            Console.WriteLine("Creating threads, number specified in config is: " + _threadCount);

            for (int i = 0; i < _threadCount; i++)
            {
                Thread thread = new Thread(new Program().ProcessDataQC);
                thread.Name = String.Format("Thread{0}", i + 1);
                thread.Start();
            }
        }

        private void ProcessData()
        {
            lock (lockObject) { _threadOpenedCount++; }

            while (_controlThreadsActive && _rowIndex < _totalRowCount)
            {
                try
                {
                    lock (lockObject) { _rowIndex++; }
                    if (_rowIndex < _totalRowCount)
                        ProcessCanadianPayroll(_controlCollection[_rowIndex]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Problem executing ProcessData");
                    throw ex;
                }
            }

            lock (lockObject) { _threadClosedCount++; }

            lock (lockObject)
            {
                if (_threadOpenedCount == _threadClosedCount && _threadOpenedCount == _threadCount)
                {
                    ////we are done doing calcs, this code is temp instead of doing db updates to get sum of timing cols
                    //Decimal total = 0;
                    //foreach (PayrollEngineOutput obj in PayrollOutput)
                    //{
                    //    total += obj.ElapsedTimeInMilliSeconds;
                    //}
                    TriggerThreadsToStoreResultsInDatabase();
                }
            }
        }

        private void ProcessDataQC()
        {
            lock (lockObject) { _threadOpenedCount++; }

            while (_controlThreadsActive && _rowIndex < _totalRowCount)
            {
                try
                {
                    lock (lockObject) { _rowIndex++; }
                    if (_rowIndex < _totalRowCount)
                        ProcessCanadianPayrollQC(_controlCollectionQC[_rowIndex]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Problem executing ProcessData");
                    throw ex;
                }
            }

            lock (lockObject) { _threadClosedCount++; }

            lock (lockObject)
            {
                if (_threadOpenedCount == _threadClosedCount && _threadOpenedCount == _threadCount)
                {
                    //we are done doing calcs, this code is temp instead of doing db updates to get sum of timing cols
                    Decimal total = 0;
                    foreach (PayrollEngineOutput obj in PayrollOutput)
                    {
                        total += obj.ElapsedTimeInMilliSeconds;
                    }
                }
                //TriggerThreadsToStoreResultsInDatabase();
            }
        }

        private static void TriggerThreadsToStoreResultsInDatabase()
        {
            Console.WriteLine("Number of items in the LIST are: " + PayrollOutput.Count);

            for (int i = 0; i < _threadCount; i++)
            {
                Thread thread = new Thread(new Program().StoreResultsInDatabase);
                thread.Name = String.Format("Thread{0}", i + 10);
                thread.Start();
            }
        }

        private void StoreResultsInDatabase()
        {
            while (_printRowIndex < _totalRowCount + _bonusRuns)
            {
                try
                {
                    //write to db, ensure no dupes are written
                    PayrollEngineOutput temp = null;
                    lock (lockObject)
                    {
                        _printRowIndex++;
                        if (_printRowIndex < _totalRowCount + _bonusRuns)
                        {
                            temp = PayrollOutput[_printRowIndex];
                            temp.BonusCPP = _printRowIndex + 1; //stacy test, this is my own test item, like a sequence number, so i can detect dupes and sort on db...
                        }
                    }
                    if (temp != null)
                    {
                        _reportMgmt.InsertPayrollEngineOutput(_databaseName, temp);
                        if ((temp.BonusCPP + 1) % 100 == 0)
                            Console.WriteLine("Stored items in db#: " + (temp.BonusCPP + 1));
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Problem executing StoreResultsInDatabase");
                    throw ex;
                }
            }
        }

        private static void ProcessCanadianPayroll(MetEmployeeData inputs)
        {
            try
            {
                #region BONUS RUN CODE
                if (inputs.CalculationType == CalculationType.Bonus)
                {
                    Stopwatch processTimerBonus = new Stopwatch();
                    processTimerBonus.Start();

                    PayrollEngineOutput payEngineResultsBonus = new PayrollEngineOutput();
                    _bonusRuns++;

                    CPTL tempEngine = new CPTL();
                    tempEngine.calcDeductions(inputs);

                    payEngineResultsBonus.EmployeeId = inputs.EmployeeId;
                    payEngineResultsBonus.EmployeeCPP = (inputs.dedCPP > 0) ? Convert.ToDecimal(inputs.dedCPP) / 100 : inputs.dedCPP;
                    payEngineResultsBonus.EmployeeEI = (inputs.dedEI > 0) ? Convert.ToDecimal(inputs.dedEI) / 100 : inputs.dedEI;
                    payEngineResultsBonus.CombinedTaxes = (inputs.dedITD > 0) ? Convert.ToDecimal(inputs.dedITD) / 100 : inputs.dedITD;
                    payEngineResultsBonus.FederalTax = (inputs.dedITD_Fed > 0) ? Convert.ToDecimal(inputs.dedITD_Fed) / 100 : inputs.dedITD_Fed;
                    payEngineResultsBonus.ProvincialTax = (inputs.dedITD_Prov > 0) ? Convert.ToDecimal(inputs.dedITD_Prov) / 100 : inputs.dedITD_Prov;
                    decimal cppEarningsBonus = (inputs.CPP_Earnings > 0) ? Convert.ToDecimal(inputs.CPP_Earnings) / 100 : inputs.CPP_Earnings;
                    decimal eiEarningsBonus = (inputs.EI_Earnings > 0) ? Convert.ToDecimal(inputs.EI_Earnings) / 100 : inputs.EI_Earnings;
                    payEngineResultsBonus.EmployerCppQpp = (inputs.employerCPPcost > 0) ? Convert.ToDecimal(inputs.employerCPPcost) / 100 : inputs.employerCPPcost;
                    payEngineResultsBonus.EmployerEI = (inputs.employerEIcost > 0) ? Convert.ToDecimal(inputs.employerEIcost) / 100 : inputs.employerEIcost;
                    decimal employeeEiReturnableBonus = (inputs.employeeEIreturnable > 0) ? Convert.ToDecimal(inputs.employeeEIreturnable) / 100 : inputs.employeeEIreturnable;

                    processTimerBonus.Stop();
                    payEngineResultsBonus.ElapsedTimeInMilliSeconds = Convert.ToDecimal(processTimerBonus.Elapsed.TotalMilliseconds);
                    payEngineResultsBonus.CreateUser = payEngineResultsBonus.UpdateUser = "metThreadingEngine";

                    //store in list
                    lock (PayrollOutput)
                    {
                        PayrollOutput.Add(payEngineResultsBonus);
                    }

                    //switch back to regular for run below.
                    inputs.CalculationType = CalculationType.RegularSalary;
                }
                #endregion

                PayrollEngineOutput payEngineResults = new PayrollEngineOutput();
                Stopwatch processTimer = new Stopwatch();
                processTimer.Start();

                CPTL engine = new CPTL();
                //calc taxes
                engine.calcDeductions(inputs);

                //store values in business object
                payEngineResults.EmployeeId = inputs.EmployeeId;

                //This is the return value for the amount of the calculated Canada(or Quebec) Pension Plan (CPP / QPP) deduction
                payEngineResults.EmployeeCPP = (inputs.dedCPP > 0) ? Convert.ToDecimal(inputs.dedCPP) / 100 : inputs.dedCPP;

                //This is the return value for the amount of the calculated Employment Insurance (IE) deduction
                payEngineResults.EmployeeEI = (inputs.dedEI > 0) ? Convert.ToDecimal(inputs.dedEI) / 100 : inputs.dedEI;

                //This is the return value for the amount of the calculated Income Tax deduction.
                payEngineResults.CombinedTaxes = (inputs.dedITD > 0) ? Convert.ToDecimal(inputs.dedITD) / 100 : inputs.dedITD;

                //This is the return value for the amount of the federal portion of the total calculated Income Tax deduction.
                payEngineResults.FederalTax = (inputs.dedITD_Fed > 0) ? Convert.ToDecimal(inputs.dedITD_Fed) / 100 : inputs.dedITD_Fed;

                //This is the return value for the amount of the provincial portion of the total calculated Income Tax deduction
                payEngineResults.ProvincialTax = (inputs.dedITD_Prov > 0) ? Convert.ToDecimal(inputs.dedITD_Prov) / 100 : inputs.dedITD_Prov;

                //This is the return value for the total amount of CPP pensionable earnings calculated by CPTL, on which the current calculated deduction for CPP contributions is based.
                //This amount should be used to accumulate year-to-date totals of CPP pensionable earnings for each employee, which will be required for year-end employee tax reporting(T4).
                decimal cppEarnings = (inputs.CPP_Earnings > 0) ? Convert.ToDecimal(inputs.CPP_Earnings) / 100 : inputs.CPP_Earnings;

                //This is the return value for the total amount of EI insurable earnings calculated by CPTL, on which the current calculated deduction for EI premiums is based.
                //This amount should be used to accumulate year - to - date totals of EI insurable earnings for each employee, which will be required for year-end employee tax reporting(T4).
                decimal eiEarnings = (inputs.EI_Earnings > 0) ? Convert.ToDecimal(inputs.EI_Earnings) / 100 : inputs.EI_Earnings;

                //This is the return value for the calculated amount of the employer Canada(or Quebec) Pension Plan(CPP / QPP) contribution based on the employee’s contributed(payroll deducted) amount.
                //As of January 1, 2014, the employer contribution remains as an amount equal to the employee payroll deducted contribution.
                payEngineResults.EmployerCppQpp = (inputs.employerCPPcost > 0) ? Convert.ToDecimal(inputs.employerCPPcost) / 100 : inputs.employerCPPcost;

                //This is the return value for the calculated amount of the employer portion of Employment Insurance(EI) premiums based on the employee’s premium amount.
                //As of January 1, 2014, the employer portion remains as an amount equal to 1.4 times the employee payroll deducted premium.
                payEngineResults.EmployerEI = (inputs.employerEIcost > 0) ? Convert.ToDecimal(inputs.employerEIcost) / 100 : inputs.employerEIcost;

                //This is the return value for the calculated amount of the employer EI cost savings that are returnable to an employee when an employer is eligible for a reduced EI rate
                decimal employeeEiReturnable = (inputs.employeeEIreturnable > 0) ? Convert.ToDecimal(inputs.employeeEIreturnable) / 100 : inputs.employeeEIreturnable;

                processTimer.Stop();
                payEngineResults.ElapsedTimeInMilliSeconds = Convert.ToDecimal(processTimer.Elapsed.TotalMilliseconds);
                payEngineResults.CreateUser = payEngineResults.UpdateUser = "metThreadingEngine";

                //store in list
                lock (PayrollOutput)
                {
                    PayrollOutput.Add(payEngineResults);
                }

                //old
                //pool.checkIn

                //stacy -->how to dispose of engine? or do we need to?
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void ProcessCanadianPayrollQC(MetEmployeeDataQC inputs)
        {
            try
            {
                #region CANADA BONUS RUN CODE
                if (inputs.canada.CalculationType == CalculationType.Bonus)
                {
                    Stopwatch processTimerBonus = new Stopwatch();
                    processTimerBonus.Start();

                    PayrollEngineOutput payEngineResultsBonus = new PayrollEngineOutput();
                    _bonusRuns++;

                    CPTL tempEngine = new CPTL();
                    tempEngine.calcDeductions(inputs.canada);

                    payEngineResultsBonus.EmployeeId = inputs.EmployeeId;
                    payEngineResultsBonus.EmployeeEI = (inputs.canada.dedEI > 0) ? Convert.ToDecimal(inputs.canada.dedEI) / 100 : inputs.canada.dedEI;
                    payEngineResultsBonus.CombinedTaxes = (inputs.canada.dedITD > 0) ? Convert.ToDecimal(inputs.canada.dedITD) / 100 : inputs.canada.dedITD;
                    payEngineResultsBonus.FederalTax = (inputs.canada.dedITD_Fed > 0) ? Convert.ToDecimal(inputs.canada.dedITD_Fed) / 100 : inputs.canada.dedITD_Fed;
                    decimal eiEarningsBonus = (inputs.canada.EI_Earnings > 0) ? Convert.ToDecimal(inputs.canada.EI_Earnings) / 100 : inputs.canada.EI_Earnings;
                    payEngineResultsBonus.EmployerEI = (inputs.canada.employerEIcost > 0) ? Convert.ToDecimal(inputs.canada.employerEIcost) / 100 : inputs.canada.employerEIcost;
                    decimal employeeEiReturnableBonus = (inputs.canada.employeeEIreturnable > 0) ? Convert.ToDecimal(inputs.canada.employeeEIreturnable) / 100 : inputs.canada.employeeEIreturnable;

                    processTimerBonus.Stop();
                    payEngineResultsBonus.ElapsedTimeInMilliSeconds = Convert.ToDecimal(processTimerBonus.Elapsed.TotalMilliseconds);
                    //payEngineResultsBonus.CreateUser = payEngineResultsBonus.UpdateUser = "metThreadingEngine";
                    payEngineResultsBonus.CreateUser = payEngineResultsBonus.UpdateUser = "BONUS_RUN"; //used for quebec bonus

                    //store in list
                    lock (PayrollOutput)
                    {
                        PayrollOutput.Add(payEngineResultsBonus);
                    }

                    //switch back to regular for run below.
                    inputs.canada.CalculationType = CalculationType.RegularSalary;
                }
                #endregion 

                #region canada

                PayrollEngineOutput payEngineResults = new PayrollEngineOutput();
                Stopwatch processTimer = new Stopwatch();
                processTimer.Start();

                //calc canadian taxes
                CPTL engineCanada = new CPTL();
                engineCanada.calcDeductions(inputs.canada);

                //This is the return value for the amount of the calculated Employment Insurance (IE) deduction
                payEngineResults.EmployeeEI = (inputs.canada.dedEI > 0) ? Convert.ToDecimal(inputs.canada.dedEI) / 100 : inputs.canada.dedEI;

                //This is the return value for the amount of the federal portion of the total calculated Income Tax deduction.
                payEngineResults.FederalTax = (inputs.canada.dedITD_Fed > 0) ? Convert.ToDecimal(inputs.canada.dedITD_Fed) / 100 : inputs.canada.dedITD_Fed;

                //This is the return value for the amount of the calculated Income Tax deduction.
                payEngineResults.CombinedTaxes = (inputs.canada.dedITD > 0) ? Convert.ToDecimal(inputs.canada.dedITD) / 100 : inputs.canada.dedITD;

                //This is the return value for the total amount of EI insurable earnings calculated by CPTL, on which the current calculated deduction for EI premiums is based.
                //This amount should be used to accumulate year - to - date totals of EI insurable earnings for each employee, which will be required for year-end employee tax reporting(T4).
                decimal eiEarnings = (inputs.canada.EI_Earnings > 0) ? Convert.ToDecimal(inputs.canada.EI_Earnings) / 100 : inputs.canada.EI_Earnings;

                //This is the return value for the calculated amount of the employer portion of Employment Insurance(EI) premiums based on the employee’s premium amount.
                //As of January 1, 2014, the employer portion remains as an amount equal to 1.4 times the employee payroll deducted premium.
                payEngineResults.EmployerEI = (inputs.canada.employerEIcost > 0) ? Convert.ToDecimal(inputs.canada.employerEIcost) / 100 : inputs.canada.employerEIcost;

                //This is the return value for the calculated amount of the employer EI cost savings that are returnable to an employee when an employer is eligible for a reduced EI rate
                decimal employeeEiReturnable = (inputs.canada.employeeEIreturnable > 0) ? Convert.ToDecimal(inputs.canada.employeeEIreturnable) / 100 : inputs.canada.employeeEIreturnable;

                #endregion

                #region QUEBEC BONUS RUN CODE
                if (inputs.quebec.CalculationType == CalculationTypeQC.Bonus)
                {
                    Stopwatch processTimerBonus = new Stopwatch();
                    processTimerBonus.Start();

                    PayrollEngineOutput payEngineResultsBonus = new PayrollEngineOutput();
                    _bonusRuns++;

                    QPTL tempEngine = new QPTL();
                    tempEngine.calcDeductions(inputs.quebec);

                    payEngineResultsBonus.EmployeeId = inputs.EmployeeId;
                    payEngineResultsBonus.QuebecProvincialTax = (inputs.quebec.dedQITD) > 0 ? Convert.ToDecimal(inputs.quebec.dedQITD) / 100 : inputs.quebec.dedQITD;
                    payEngineResultsBonus.QuebecHeathContribution = (inputs.quebec.dedQITD_Z > 0) ? Convert.ToDecimal(inputs.quebec.dedQITD_Z) / 100 : inputs.quebec.dedQITD_Z;
                    payEngineResultsBonus.EmployeeQpip = (inputs.quebec.dedQPIP > 0) ? Convert.ToDecimal(inputs.quebec.dedQPIP) / 100 : inputs.quebec.dedQPIP;
                    payEngineResultsBonus.EmployeeQPP = (inputs.quebec.dedQPP > 0) ? Convert.ToDecimal(inputs.quebec.dedQPP) / 100 : inputs.quebec.dedQPP;
                    payEngineResultsBonus.EmployerQpip = (inputs.quebec.employerQPIPcost > 0) ? Convert.ToDecimal(inputs.quebec.employerQPIPcost) / 100 : inputs.quebec.employerQPIPcost;
                    payEngineResultsBonus.EmployerCppQpp = (inputs.quebec.employerQPPcost > 0) ? Convert.ToDecimal(inputs.quebec.employerQPPcost) / 100 : inputs.quebec.employerQPPcost;
                    decimal qpipEarningsBonus = (inputs.quebec.QPIP_Earnings > 0) ? Convert.ToDecimal(inputs.quebec.QPIP_Earnings) / 100 : inputs.quebec.QPIP_Earnings;
                    decimal qppEarningsBonus = (inputs.quebec.QPP_Earnings > 0) ? Convert.ToDecimal(inputs.quebec.QPP_Earnings) / 100 : inputs.quebec.QPP_Earnings;

                    processTimerBonus.Stop();
                    payEngineResultsBonus.ElapsedTimeInMilliSeconds = Convert.ToDecimal(processTimerBonus.Elapsed.TotalMilliseconds);
                    //payEngineResultsBonus.CreateUser = payEngineResultsBonus.UpdateUser = "metThreadingEngine";
                    payEngineResultsBonus.CreateUser = payEngineResultsBonus.UpdateUser = "BONUS_RUN"; //used for quebec bonus

                    //store in list
                    lock (PayrollOutput)
                    {
                        PayrollOutput.Add(payEngineResultsBonus);
                    }

                    //switch back to regular for run below.
                    inputs.quebec.CalculationType = CalculationTypeQC.RegularSalary;
                }
                #endregion
                #region quebec

                //calc quebec taxes
                QPTL engine = new QPTL();
                engine.calcDeductions(inputs.quebec);

                /**************************store QUEBEC values in business object*/
                payEngineResults.EmployeeId = inputs.EmployeeId;

                //Calculated Quebec Income Tax deduction (including Requested Additional Deduction) for current payroll. The field is filled by the QPTL.calcDeductions(..) method.
                payEngineResults.QuebecProvincialTax = (inputs.quebec.dedQITD) > 0 ? Convert.ToDecimal(inputs.quebec.dedQITD) / 100 : inputs.quebec.dedQITD;

                //Calculated Quebec Health contribution portion of income tax deduction amount (QITD).The field is filled by the QPTL.calcDeductions(..) method.
                payEngineResults.QuebecHeathContribution = (inputs.quebec.dedQITD_Z > 0) ? Convert.ToDecimal(inputs.quebec.dedQITD_Z) / 100 : inputs.quebec.dedQITD_Z;

                //Calculated Quebec Parental Insurance Program (QPIP) deduction for current payroll. The field is filled by the QPTL.calcDeductions(..) method.
                payEngineResults.EmployeeQpip = (inputs.quebec.dedQPIP > 0) ? Convert.ToDecimal(inputs.quebec.dedQPIP) / 100 : inputs.quebec.dedQPIP;

                //Calculated Quebec Pension Plan (QPP) deduction for current payroll. The field is filled by the QPTL.calcDeductions(..) method.
                payEngineResults.EmployeeQPP = (inputs.quebec.dedQPP > 0) ? Convert.ToDecimal(inputs.quebec.dedQPP) / 100 : inputs.quebec.dedQPP;

                //Calculated employer QPIP contribution (cost) for the current payroll calculation. The field is filled by the QPTL.calcDeductions(..) method.
                payEngineResults.EmployerQpip = (inputs.quebec.employerQPIPcost > 0) ? Convert.ToDecimal(inputs.quebec.employerQPIPcost) / 100 : inputs.quebec.employerQPIPcost;

                //Calculated employer QPP contribution (cost) for the current payroll calculation. The field is filled by the QPTL.calcDeductions(..) method.
                payEngineResults.EmployerCppQpp = (inputs.quebec.employerQPPcost > 0) ? Convert.ToDecimal(inputs.quebec.employerQPPcost) / 100 : inputs.quebec.employerQPPcost;

                //Calculated QPIP Insurable Earnings amount. The field is filled by the QPTL.calcDeductions(..) method.
                decimal qpipEarnings = (inputs.quebec.QPIP_Earnings > 0) ? Convert.ToDecimal(inputs.quebec.QPIP_Earnings) / 100 : inputs.quebec.QPIP_Earnings;

                //Calculated QPP Pensionable Earnings amount. The field is filled by the QPTL.calcDeductions(..) method
                decimal qppEarnings = (inputs.quebec.QPP_Earnings > 0) ? Convert.ToDecimal(inputs.quebec.QPP_Earnings) / 100 : inputs.quebec.QPP_Earnings;

                processTimer.Stop();
                payEngineResults.ElapsedTimeInMilliSeconds = Convert.ToDecimal(processTimer.Elapsed.TotalMilliseconds);
                //payEngineResults.CreateUser = payEngineResults.UpdateUser = "metThreadingEngine";
                payEngineResults.CreateUser = payEngineResults.UpdateUser = "BONUS_RUN"; //used for quebec bonus

                //store in list
                lock (PayrollOutput)
                {
                    PayrollOutput.Add(payEngineResults);
                }
                #endregion

                //old
                //pool.checkIn

                //stacy -->how to dispose of engine?
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static List<MetEmployeeData> GetData(bool isBonus = false)
        {
            List<MetEmployeeData> tableData = new List<MetEmployeeData>();

            //create sql details
            SqlConnection connection = new SqlConnection(_connectionString.ConnectionString);
            SqlDataReader reader;
            SqlCommand command = new SqlCommand();

            //create sql
            /***************NOTES 2015 data
            processed date: 2015 - 06 - 11 07:13:31.340
            year:   2015
            period: 12
            group:  BWK
            start:  2015-05-30
            end:    2015-06-12
            cheque: 2015-06-17
            */
            /***************NOTES 2014 data
            processed date: 2014-10-30 12:08:14.037	
            year: 2014
            period: 22
            group: BWK
            start: 2014-10-18
            end: 2014-10-31
            cheque: 2014-11-05

            ********************/

            if (!isBonus)
                command.CommandText = "SELECT * FROM engine_test_input where taxable_code_province_state_cd != 'QC' and CAN_BONUS = 0 ";
            else  //this is a bonus
                command.CommandText = "SELECT * FROM engine_test_input where taxable_code_province_state_cd != 'QC' and CAN_BONUS > 0 ";
            //command.CommandText = "SELECT * FROM engine_test_input where taxable_code_province_state_cd != 'QC' and employee_id = 217 ";

            command.CommandType = CommandType.Text;
            command.Connection = connection;

            try
            {
                connection.Open();
                reader = command.ExecuteReader();

                int count = reader.FieldCount;
                while (reader.Read())
                {
                    //if (tableData.Count == 50) break;//load test code
                    //for (int i = 0; i < 3000; i++)//load test code
                    //{
                        _totalRowCount++;

                        //NOTE:  Enter the amounts WITHOUT a decimal point - 2 decimal points are assumed – e.g. 10, 000.00 should be entered as 1000000.

                        //EmployeeData temp = new EmployeeData(); //stacy - this is MET object, trying my wrapper
                        MetEmployeeData temp = new MetEmployeeData();
                        temp.EmployeeId = Convert.ToInt64(reader[1]);

                        Province employeeProvince = (Province)Enum.Parse(typeof(Province), Convert.ToString(reader[2]));

                        //set province.  we could also get our db to convert the province numbers instead of doing this.
                        temp.SetProvince(employeeProvince);

                        //this will be something we need to grab from db
                        temp.annualPayPeriods = 26;

                        //The birth date is used only to determine if the employee is eligible for an age-related exemption from CPP
                        temp.birthDate = null;

                        //Enter the total claim amount from the employee’s federal TD1 form. 
                        //If the employee has not completed a federal TD1 form, enter the value 1, which will result in the default Basic Claim Amount being used for payroll calculations. 
                        temp.TD1_Claim = (int)Math.Round(Convert.ToDecimal(reader[21]) * 100, MidpointRounding.AwayFromZero); //federal total claim amount

                        #region zeroed values
                        //declaration, CPTL can be configured to not calculate a federal income tax deduction by entering a value of 1 in this field.  Otherwise, the default value of 0 should be used.
                        temp.TD1_exemptITD = 0;

                        //Enter the amount of the annual deduction for living in a prescribed zone as indicated on the federal TD1 form.  If no amount is claimed, enter 0.
                        temp.TD1_HD = 0;
                        #endregion

                        //Enter the amount of additional income tax deductions per pay period requested by the employee on his / her federal TD1 form.  If no additional amount is requested, enter 0.
                        temp.TD1_L = (int)Math.Round(Convert.ToDecimal(reader[24]) * 100, MidpointRounding.AwayFromZero); //fed additional tax

                        #region zeroed values
                        //Enter the amount of annual taxable deductions, such as childcare expenses and support payments, etc., authorized by a tax services office or tax centre.
                        //The employee must have a letter from a tax office authorizing this deduction amount on file with the employer.  If no amount is claimed, enter 0.
                        temp.CRA_F1 = 0;

                        //stacy note - we dont have federal tax credits in our sample...
                        //Enter the amount of other federal tax credits, such as medical expenses and charitable donations, authorized by a tax services office or tax centre.
                        temp.CRA_K3 = 0;

                        //For commissioned employees, enter the amount of the total annual remuneration as reported on the employee’s federal TD1(X) form.
                        //If a commission income deduction calculation (calcType = 5) is being performed, the amount in this field must be greater than 0.
                        //If the employee is not remunerated by commission income, enter 0.
                        temp.TD1X_I1 = 0;

                        //For commissioned employees, enter the amount of the total annual commission expenses deduction as reported on the employee’s federal TD1(X) form.
                        //If the employee is not remunerated by commission income, enter 0.
                        temp.TD1X_E = 0;
                        #endregion

                        //Enter the total claim amount from the employee’s provincial TD1P form. 
                        //If the employee has not completed a provincial TD1P form, enter the value 1, which will result in the default Basic Claim Amount being used for payroll calculations.
                        temp.TD1P_Claim = (int)Math.Round(Convert.ToDecimal(reader[25]) * 100, MidpointRounding.AwayFromZero); //prov total claim amount

                        #region zeroed values
                        //CPTL can be configured to not calculate a provincial/ territorial income tax deduction by entering a value of 1 in this field.  Otherwise, the default value of 0 should be used.
                        temp.TD1P_exemptITD = 0;
                        #endregion

                        //Enter the amount of other provincial or territorial tax credits, such as medical expenses and charitable donations, authorized by a tax services office or tax centre. 
                        //If no amount is claimed, enter 0.
                        temp.CRA_K3P = (int)Math.Round(Convert.ToDecimal(reader[28]) * 100, MidpointRounding.AwayFromZero); //prov tax credit

                        //Enter the amount of additional provincial or territorial tax reduction based on applicable amounts reported on the provincial/ territorial Form TD1P. 
                        //If no amount is claimed, enter 0.
                        temp.CRA_Y = 0;

                        #region zeroed values
                        //Stacy note:  our input data would need this set, currenty no one is exempt in our test data.

                        //If the employee is exempt from contributing to Canada Pension Plan (CPP), enter 1 (true), otherwise enter 0 (false).
                        //If unknown, enter 0 (false).  The CPTL program can determine age related exemptions to CPP automatically if the birth date for the employee is provided in the birthDate field.
                        temp.isCPPexempt = 0;

                        //If the employee is exempt from paying Employment Insurance (EI) premiums, enter 1 (true), otherwise enter 0 (false).If unknown, enter 0 (false).
                        temp.isEIexempt = 0;

                        //If a Quebec employee is exempt from paying Quebec Parental Insurance Plan (PPIP) premiums, enter 1(true), otherwise enter 0 (false).  If unknown, enter 0 (false).
                        //Note: Currently, this factor applies only in the province of Quebec, therefore for all other provinces, the value should be 0 (false).
                        temp.isPPIPexempt = 0;

                        //end Stacy note:  our input data would need this set, currenty no one is exempt in our test data.
                        #endregion

                        //If an employer has qualified for a reduced EI contribution rate factor, enter the reduced employer EI rate.
                        //This amount will be used to determine the employer’s EI contribution cost and the amount of EI deduction that is returnable to the employee.
                        //If unknown, enter a Null value, and the default EI employer contribution factor (1.4) will be assumed.
                        //Enter the amount WITH a decimal point – e.g. 1.4 should be entered as 1.4.
                        temp.employer_EI_ratefactor = 1.4;

                        //Enter the total amount of year-to-date deductions of Canada Pension Plan (CPP) contributions for the employee. 
                        //This amount will be used to determine if the employee has reached the maximum contribution limit for the year. If unknown, enter 0.
                        temp.ytdCPP = (int)Math.Round(Convert.ToDecimal(reader[39]) * 100, MidpointRounding.AwayFromZero); //Ytd Cpp/Qpp Deducted

                        //Enter the total amount of year-to-date deductions of Employment Insurance (EI) premiums for the employee. 
                        //This amount will be used to determine if the employee has reached the maximum deduction limit for the year. If unknown, enter 0.
                        temp.ytdEI = (int)Math.Round(Convert.ToDecimal(reader[40]) * 100, MidpointRounding.AwayFromZero);//Ytd Ei Deducted

                        //Enter the total amount of year-to-date deductions of Quebec Parental Insurance Plan (QPIP) premiums for the employee (Quebec employees only).
                        //This amount will be used to determine if the employee has reached the maximum premium deduction limit for the year. 
                        //If unknown, enter 0.  Enter the amount WITHOUT a decimal point - 2 decimal points are assumed – e.g. 200.00 should be entered as 20000.
                        //Note: Only the province of Quebec administers this program, therefore the value in this field for employees of all other provinces should be 0.
                        if (employeeProvince != Province.QC)
                            temp.ytdPPIP = 0;
                        else
                            temp.ytdPPIP = (int)Math.Round(Convert.ToDecimal(reader[41]) * 100, MidpointRounding.AwayFromZero); //Ytd Ppip Deducted

                        #region zeroed values
                        //Enter the total amount of year-to-date deductions of Income Taxes withheld for the employee.  
                        //This amount will be used to determine if the employee’s current payroll income tax deduction can be reduced by an over-deduction on previous payrolls.  If unknown, enter 0.
                        //Stacy note:  our input data doesnt have this
                        temp.ytdITD = 0;

                        //Enter the total amount of year-to-date purchases of units of eligible labour-sponsored funds for the employee. 
                        //This amount will be used to determine the applicable tax credit that can be used to reduce income taxes payable for the year.  If unknown, enter 0.
                        temp.ytdLSFp = 0;

                        //Enter the total number of pay periods for allocation of the calculated annual Labour - Sponsored Funds(LSF) credit using the accelerated method approved by CRA.
                        //If unknown, or if the accelerated method is not applicable, enter 0.
                        temp.LSFp_P = 0;

                        //Enter the integer value corresponding to the appropriate type of income on which the payroll deduction calculation will be performed
                        //
                        //    1 Regular salary &/or wages
                        //    2 Pension income
                        //    3 Bonus income
                        //    4 Retroactive Pay Increase
                        //    5 Commission income

                        #endregion

                        if (!isBonus)
                            temp.CalculationType = CalculationType.RegularSalary;
                        else if (isBonus)
                            temp.CalculationType = CalculationType.Bonus; //(CalculationType)Enum.Parse(typeof(CalculationType), "3"); use this if we pass in a number...

                        //Enter the integer value of the current Pay Period number – i.e. the number corresponding to the position of the current pay period within the total number of pay periods in the year. 
                        //This is an optional field and is used to calculate the number of remaining pay periods in the current year.
                        //If the current pay period number is not known or not used, enter a value of 0.
                        //If ytd deduction values have been provided and you wish CPTL to base income tax deductions on values calculated from the ytd deductions of CPP, EI, PPIP and Income taxes, then you
                        //must provide a non-zero value in this field, otherwise the ytd values will be ignored.
                        //stacy note this:
                        temp.payPeriod = 12;

                        //Enter the integer value of the number of pay periods being paid by the current payroll amounts, if it exceeds the default value of 1.
                        //If the default value of 1 pay period is being paid, you may enter value of either 0 or 1, as a value of 0 will be assumed to indicate the default value of 1
                        temp.cntPP = 1;

                        //Enter the date of the current payroll payment in the format YYYY-MM-DD. 
                        //If an empty string is entered, the CPTL program will assume the current system date is the payroll payment date
                        //temp.payDate = String.Empty;

                        temp.payDate = "2015-06-17";

                        //Enter the amount of regular salary/wages paid to the employee for the defined pay period.
                        //Data entered in this field is used only when the calcType value is 1, 3, or 4 – for other calcTypes enter 0 in this field
                        if (temp.CalculationType == CalculationType.RegularSalary || temp.CalculationType == CalculationType.Bonus || temp.CalculationType == CalculationType.RetroPay)
                            temp.wagesAmount = (int)Math.Round(Convert.ToDecimal(reader[4]) * 100, MidpointRounding.AwayFromZero);
                        else
                            temp.wagesAmount = 0;

                        //Enter the amount of the current bonus to be paid to the employee for the defined pay period.
                        //Data entered in this field is only used when the calcType value is 3 – for other calcTypes enter 0 in this field.
                        //Note that a bonus deduction calculation(calcType = 3) also requires that amounts be entered for wagesAmount, annualPayPeriods and ytdPrevBonusAmount.
                        //The wagesAmount and annualPayPeriods data will only be used to estimate annual taxable income for purposes of determining tax rates applicable to the bonus income.
                        //The calculated payroll deductions will apply ONLY to the bonus amount entered in this field
                        if (temp.CalculationType == CalculationType.Bonus)
                            if (employeeProvince != Province.QC)
                            {
                                temp.bonusAmount = (int)Math.Round(Convert.ToDecimal(reader[14]) * 100, MidpointRounding.AwayFromZero);
                                if (temp.bonusAmount == 0) //this is not a bonus but a lump sum run
                                    temp.bonusAmount = (int)Math.Round(Convert.ToDecimal(reader[16]) * 100, MidpointRounding.AwayFromZero);
                            }
                            else
                                temp.bonusAmount = (int)Math.Round(Convert.ToDecimal(reader[17]), MidpointRounding.AwayFromZero);
                        else
                            temp.bonusAmount = 0;

                        #region for pension and other type runs
                        //Enter the amount of pension income paid to the employee for the defined pay period. 
                        //Data entered in this field is only used when the calcType value is 2 – for other calcTypes enter 0 in this field.
                        if (temp.CalculationType == CalculationType.Pension)
                            temp.pensionAmount = 0;
                        else
                            temp.pensionAmount = 0;

                        //Enter the amount of vacation pay paid to the employee for the defined pay period. 
                        //Data entered in this field is only used when the calcType value is 1,3 or 4 – for other calcTypes enter 0 in this field.
                        if (temp.CalculationType == CalculationType.RegularSalary || temp.CalculationType == CalculationType.Bonus || temp.CalculationType == CalculationType.RetroPay)
                            temp.vacationPayAmount = 0;
                        else
                            temp.vacationPayAmount = 0;

                        //Enter the total amount of year-to-date previous bonuses paid to the employee (NOT including the current bonus being paid). 
                        //Data entered in this field is only used when the calcType value is 3 – for other calcTypes enter 0 in this field.
                        //Note that a bonus deduction calculation (calcType = 3) also requires that amounts be entered for wagesAmount, annualPayPeriods and bonusAmount
                        //stacy we dont pass this
                        if (temp.CalculationType == CalculationType.Bonus)
                            temp.ytdPrevBonusAmount = 0;
                        else
                            temp.ytdPrevBonusAmount = 0;

                        //Enter the total amount of retroactive pay paid to the employee. Data entered in this field is only used when the calcType value is 4 – for other calcTypes enter 0 in this field.
                        //Note that a retroactive pay deduction calculation(calcType = 4) also requires that amounts be entered for wagesAmount, annualPayPeriods and retroPayPeriods.
                        //The wagesAmount and annualPayPeriods data will only be used to estimate annual taxable income for purposes ofdetermining tax rates applicable to the retroactive pay income.
                        //The calculated payroll deductions will apply ONLY to the retroactive pay amount entered in this field
                        //stacy we dont have this in our sample data
                        if (temp.CalculationType == CalculationType.RetroPay)
                            temp.retroPayAmount = (int)(Convert.ToDecimal(reader[15]) * 100);
                        else
                            temp.retroPayAmount = 0;

                        //Enter the number of pay periods to which the total retroactive pay amount is applicable for the employee.
                        //Data entered in this field is only used when the calcType value is 4 – for other calcTypes enter 0 in this field.
                        //Note that a retroactive pay deduction calculation(calcType = 4) also requires that amounts be entered for wagesAmount, annualPayPeriods and retroPayAmount.
                        //stacy we dont have this in our sample data
                        if (temp.CalculationType == CalculationType.RetroPay)
                            temp.retroPayPeriods = 0;
                        else
                            temp.retroPayPeriods = 0;

                        //Enter the amount of the current commission payment to the employee. 
                        //Data entered in this field is only used when the calcType value is 5 – for other calcTypes enter 0 in this field.
                        //Note that a commission income deduction calculation (calcType=5) also requires that amounts be entered for TD1X_I1, TD1X_E, and daysSincePrevCommPmt
                        //stacy we dont have this in our sample data
                        if (temp.CalculationType == CalculationType.Commission)
                            temp.commAmount = 0;
                        else
                            temp.commAmount = 0;

                        //Enter the number of days since the previous commission payment during the current calendar year.
                        //Data entered in this field is only used when the calcType value is 5 – for other calcTypes enter 0 in this field.
                        //If this is the first commission payment of the calendar year, enter 0.
                        //Note that a commission income deduction calculation(calcType = 5) also requires that amounts be entered for TD1X_I1, TD1X_E, and commAmount.
                        //stacy we dont have this in our sample data
                        if (temp.CalculationType == CalculationType.Commission)
                            temp.daysSincePrevCommPmt = 0;
                        else
                            temp.daysSincePrevCommPmt = 0;

                        #endregion

                        //stacy - this isnt clear in our inputs if they are EI insurable or not...
                        /*
                        If the taxable benefit is subject to EI, then you must enter it as the value for the “txCashBenefitsAmount” property of the EmployeeData object (even if it is not a Cash benefit).
                        CPTL treats the “txNonCashBenefitsAmount” field as exempt from EI, so all taxable benefits subject to EI should be included in the “txCashBenefitsAmount” field.
                        */

                        //Enter the amount of taxable cash benefits paid to the employee for the defined current pay period.  Data entered in this field can be used with any calcType value
                        temp.txCashBenefitsAmount = 0;
                        //temp.txCashBenefitsAmount = (int)Math.Round(Convert.ToDecimal(reader[12]) * 100, MidpointRounding.AwayFromZero);

                        //Enter the amount of taxable non-cash benefits provided to the employee for the defined current pay period.  Data entered in this field can be used with any calcType value
                        temp.txNonCashBenefitsAmount = (int)Math.Round(Convert.ToDecimal(reader[12]) * 100, MidpointRounding.AwayFromZero);
                        //temp.txNonCashBenefitsAmount = 0;

                        //Enter the amount of the current payroll deduction(s) for employee contributions to a registered pension plan (RPP), a registered retirement savings plan(RRSP), 
                        //or a retirement compensation arrangement(RCA).  Data entered in this field can be used with any calcType value.
                        temp.dedF = (int)Math.Round(Convert.ToDecimal(reader[13]) * 100, MidpointRounding.AwayFromZero);

                        //Enter the amount of the current payroll deduction(s)(e.g.RRSP / RPP, union dues, alimony or family maintenance) applicable only to the current bonus payment.
                        //Data entered in this field is only used when the calcType value is 3 (Bonus payments) – for other calcTypes enter 0 in this field.
                        //stacy we dont have this in our sample data
                        if (temp.CalculationType == CalculationType.Bonus)
                            temp.bonusDeductions = 0;
                        else
                            temp.bonusDeductions = 0;


                        #region zeroed values
                        //Enter the amount of the current payroll deduction(s) for union dues. Data entered in this field can be used with any calcType value.
                        //stacy we dont have this in our sample data
                        temp.dedU1 = 0;

                        //Enter the amount of the current payroll deduction(s) for alimony or family maintenance payments required by a legal document to be payroll - deducted.
                        //Data entered in this field can be used with any calcType value.
                        //stacy we dont have this in our sample data
                        temp.dedF2 = 0;



                        //Enter the amount of the current payroll deduction(s) (e.g. RRSP/RPP, union dues, alimony or family maintenance) applicable only to the current retroactive pay payment. 
                        //Data entered in this field is only used when the calcType value is 4(Retroactive pay payments) – for other calcTypes enter 0 in this field.
                        if (temp.CalculationType == CalculationType.RetroPay)
                            temp.retroDeductions = 0;
                        else
                            temp.retroDeductions = 0;
                        #endregion

                        #region questions and old data
                        //stacy what about lump sums?
                        //set LUMP SUM?????

                        /*
                        //STACY --> GET PROGRAM TO RUN FROM THIS DATA ABOVE, SEE IF ANY OF HTIS DATA BELOW NEEDS TO BE USED ANYWHERE.  CHECK OTHER THREAD ENGINE TO SEE IF UNUSED DATA IS HERE...
                        temp.EmployeeId = Convert.ToInt64(reader[1]);

                        temp.CppQppWages = Convert.ToDecimal(reader[5]);
                        //temp.CppQppDeducted = Convert.ToDecimal(reader[6]);

                        temp.EiWages = Convert.ToDecimal(reader[7]);
                        //temp.EiDeducted = Convert.ToDecimal(reader[8]);

                        temp.PpipWages = Convert.ToDecimal(reader[9]);
                        //temp.PpipDeducted = Convert.ToDecimal(reader[10]);

                        temp.CanLumpsum = Convert.ToDecimal(reader[16]);
                        temp.CanLumpsumQc = Convert.ToDecimal(reader[18]);

                        //temp.TaxDeducted = Convert.ToDecimal(reader[19]);
                        //temp.TaxQcDeducted = Convert.ToDecimal(reader[20]);

                        temp.DesignatedAreaDeduction = Convert.ToDecimal(reader[22]);
                        temp.AuthorizedAnnualDeduction = Convert.ToDecimal(reader[23]);

                        temp.ProvincialAuthorizedAnnualDeduction = Convert.ToInt16(reader[27]);
                        temp.ProvincialDesignatedAreaDeduction = Convert.ToInt16(reader[29]);

                        temp.Lcf = Convert.ToDecimal(reader[30]);
                        temp.Lcp = Convert.ToDecimal(reader[31]);

                        temp.NorthwestTerritoriesTaxCalculationBase = Convert.ToDecimal(reader[32]);
                        temp.NorthwestTerritoriesTaxAssessed = Convert.ToDecimal(reader[33]);
                        temp.NunavutTaxCalculationBase = Convert.ToDecimal(reader[34]);
                        temp.NunavutTaxAssessed = Convert.ToDecimal(reader[35]);

                        temp.EmployerEmploymentInsuranceAmount = Convert.ToDecimal(reader[36]);
                        temp.EmployerCanadaPensionPlanAmount = Convert.ToDecimal(reader[37]);
                        temp.EmployerProvincialParentalAmount = Convert.ToDecimal(reader[38]);

                        //temp.CppQppWagesCalculated = Convert.ToDecimal(reader[42]); ;
                        //temp.EiWagesCalculated = Convert.ToDecimal(reader[43]); ;
                        //temp.PpipWagesCalculated = Convert.ToDecimal(reader[44]);
                        */
                        #endregion

                        tableData.Add(temp);

                        //if (tableData.Count == 15000) break; //load test code
                    //}//load test code
                }

                connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("EXCEPTION");
                Console.WriteLine(ex.Message);
            }

            return tableData;
        }

        private static List<MetEmployeeDataQC> GetDataQC(bool isBonus = false)
        {
            List<MetEmployeeDataQC> tableData = new List<MetEmployeeDataQC>();

            //create sql details
            SqlConnection connection = new SqlConnection(_connectionString.ConnectionString);
            SqlDataReader reader;
            SqlCommand command = new SqlCommand();

            /***************NOTES for 2015 data
                processed date: 2015-06-11 07:13:31.340
                year: 2015
                period: 12
                group: BWK
                start: 2015-05-30
                end: 2015-06-12
                cheque: 2015-06-17
            
            ***************NOTES for 2014 data
                processid 25-a1220
                processed date: 2014-10-30 12:08:14.037	
                year: 2014
                period: 22
                group: BWK
                start: 2014-10-18
                end: 2014-10-31
                cheque: 2014-11-05

            *****************NOTES for 2015 input bonus table
                a1225
                process id 40
                SEMI
                period 14
                year 2015
                cheque 2015-07-31

            ********************/
            if (!isBonus)
                command.CommandText = "SELECT * FROM engine_test_input where taxable_code_province_state_cd = 'QC' ";
            else  //this is a bonus
                command.CommandText = "SELECT * FROM engine_test_input_bonus where taxable_code_province_state_cd = 'QC' and CAN_BONUS_QC > 0 "; //getting specific employees

            command.CommandType = CommandType.Text;
            command.Connection = connection;

            try
            {
                connection.Open();
                reader = command.ExecuteReader();

                int count = reader.FieldCount;
                while (reader.Read())
                {
                    if (tableData.Count == 15000) break;//load test code
                    for (int i = 0; i < 442; i++)//load test code
                    {
                        _totalRowCount++;

                        //NOTE:  Enter the amounts WITHOUT a decimal point - 2 decimal points are assumed – e.g. 10, 000.00 should be entered as 1000000.

                        //EmployeeData temp = new EmployeeData(); //stacy - this is MET object, trying my wrapper
                        MetEmployeeDataQC temp = new MetEmployeeDataQC();
                        temp.EmployeeId = Convert.ToInt64(reader[1]);

                        Province employeeProvince = (Province)Enum.Parse(typeof(Province), Convert.ToString(reader[2]));
                        //set province.  we could also get our db to convert the province numbers instead of doing this.
                        temp.canada.SetProvince(employeeProvince);

                        if (!isBonus)
                        {
                            temp.quebec.CalculationType = (CalculationTypeQC)Enum.Parse(typeof(CalculationTypeQC), "1");
                            temp.canada.CalculationType = (CalculationType)Enum.Parse(typeof(CalculationType), "1");
                        }
                        else if (isBonus)
                        {
                            temp.quebec.CalculationType = CalculationTypeQC.Bonus;
                            temp.canada.CalculationType = CalculationType.Bonus;
                        }

                        //this will be something we need to grab from db
                        if (!isBonus)
                            temp.quebec.annualPayPeriods = temp.canada.annualPayPeriods = 26;
                        else
                            temp.quebec.annualPayPeriods = temp.canada.annualPayPeriods = 24; //diff set of inputs for the bonus run...


                        //The birth date is used only to determine if the employee is eligible for an age-related exemption from CPP
                        temp.quebec.birthDate = temp.canada.birthDate = null;

                        //Number of pay periods covered by current payroll. Default = 0.
                        temp.quebec.cntPP = temp.canada.cntPP = 1;

                        /*  Current pay period portion of remuneration that gives entitlement to one of the following deductions: 
                            - The deduction for employment income situated on a reserve or premises,
                            - The deduction for employment income earned on a vessel,
                            - The deduction for employees of an international financial centre (IFC),
                            - The deduction for foreign specialists, foreign researchers, foreign researchers on a post-doctoral internship, foreign experts, foreign professors, foreign producers, foreign individuals holding a key position in a foreign production filmed in Quebec, foreign farm workers, members of the Canadian forces or members of a Canadian police force.
                        */
                        temp.quebec.dedOther = (int)Math.Round(Convert.ToDecimal(reader[22]) * 100, MidpointRounding.AwayFromZero);

                        //stuck value in here (before tax ded)
                        //Amount withheld from current pay period for contribution to an RRSP.
                        temp.quebec.dedRRSP = (int)Math.Round(Convert.ToDecimal(reader[13]) * 100, MidpointRounding.AwayFromZero);
                        temp.canada.dedF = temp.quebec.dedRRSP;

                        //Quebec Claim Amount (from line 10 of the employees Quebec TP-1015.3-V form for the tax year in which the payroll is being paid.
                        temp.quebec.E = (int)Math.Round(Convert.ToDecimal(reader[25]) * 100, MidpointRounding.AwayFromZero);

                        //Exempt from Quebec income tax - total estimated income less than E. 
                        //Value         Description 
                        //  0(default)  No exemption
                        //  1           Exempt from Quebec income tax
                        //if ((int)Math.Round(Convert.ToDecimal(reader[11]) * 100, MidpointRounding.AwayFromZero) < temp.quebec.E)
                        //    temp.quebec.exemptQITD = 1;
                        //else
                        temp.quebec.exemptQITD = 0;

                        //Employee - requested additional deduction of income tax (per pay period) – as requested by the individual on form TP-­-1017 -­-V; 
                        //or source deduction of income tax requested by a fisher on form TP - 1015.N - V; 
                        //or amount indicated on line 11 of form TP - 1015.3 - V, for the pay period.Default = 0.
                        temp.quebec.L = (int)Math.Round(Convert.ToDecimal(reader[26]) * 100, MidpointRounding.AwayFromZero); //prov additional tax

                        //Current payroll pay date (optional). Format: YYYY-MM-DD. Default: a null reference (current date).
                        if (!isBonus)
                            temp.quebec.payDate = temp.canada.payDate = "2015-06-17"; //2015
                        else
                            temp.quebec.payDate = temp.canada.payDate = "2015-07-31"; //bonus run uses diff period
                                                                                      //temp.quebec.payDate = temp.canada.payDate = "2014-11-05";

                        //Current pay period number (optional). Default = 0.
                        if (!isBonus)
                            temp.quebec.payPeriod = temp.canada.payPeriod = 12; //2015
                        else
                            temp.quebec.payPeriod = temp.canada.payPeriod = 14; //bonus run uses diff period
                                                                                //temp.quebec.payPeriod = temp.canada.payPeriod = 22;

                        if (temp.quebec.CalculationType == CalculationTypeQC.Bonus)
                        {
                            temp.quebec.bonusAmount = (int)Math.Round(Convert.ToDecimal(reader[17]) * 100, MidpointRounding.AwayFromZero);
                            temp.canada.bonusAmount = (int)Math.Round(Convert.ToDecimal(reader[14]) * 100, MidpointRounding.AwayFromZero);
                        }

                        //Current payroll taxable deductions related to bonus payment, rounded to the nearest penny (e.g. 100.00 should be entered as 10000). 
                        //Is used only for CalculationType = CalculationType.Bonus(calcType = 3).
                        if (temp.quebec.CalculationType == CalculationTypeQC.Bonus)
                            temp.quebec.bonusDeductions = 0;
                        else
                            temp.quebec.bonusDeductions = 0;

                        #region other calc types
                        //Current payroll commission pay amount, rounded to the nearest penny(e.g. 100.00 should be entered as 10000).
                        //Is used only for CalculationType = CalculationType.Commission(calcType = 5).
                        if (temp.quebec.CalculationType == CalculationTypeQC.Commission)
                            temp.quebec.commAmount = 0;
                        else
                            temp.quebec.commAmount = 0;

                        //Number of days since the previous commission payment in the current payroll tax year. Is used only for CalculationType = CalculationType.Commission (calcType = 5).
                        if (temp.quebec.CalculationType == CalculationTypeQC.Commission)
                            temp.quebec.daysSincePrevCommPmt = 0;
                        else
                            temp.quebec.daysSincePrevCommPmt = 0;

                        //Current payroll pension income amount, rounded to the nearest penny (e.g. 2,000.00 should be entered as 200000). 
                        //Is used only for CalculationType = CalculationType.Pension (calcType = 2).
                        if (temp.quebec.CalculationType == CalculationTypeQC.Pension)
                            temp.quebec.pensionAmount = 0;
                        else
                            temp.quebec.pensionAmount = 0;

                        //Current payroll taxable deductions related to retroactive pay payment, rounded to the nearest penny (e.g. 100.00 should be entered as 10000). 
                        //Is used only for CalculationType = CalculationType.RetroPay (calcType = 4).
                        if (temp.quebec.CalculationType == CalculationTypeQC.RetroPay)
                            temp.quebec.retroDeductions = 0;
                        else
                            temp.quebec.retroDeductions = 0;


                        //Current payroll retroactive pay amount, rounded to the nearest penny(e.g. 1, 000.00 should be entered as 100000).
                        //Is used only for CalculationType = CalculationType.RetroPay(calcType = 4).
                        if (temp.quebec.CalculationType == CalculationTypeQC.RetroPay)
                            temp.quebec.retroPayAmount = (int)(Convert.ToDecimal(reader[15]) * 100);
                        else
                            temp.quebec.retroPayAmount = 0;

                        //Current payroll retroactive pay payroll periods – i.e. the number of payroll periods that the retroactive pay amount covers. 
                        //Is used only for CalculationType = CalculationType.RetroPay (calcType = 4).
                        if (temp.quebec.CalculationType == CalculationTypeQC.RetroPay)
                            temp.quebec.retroPayPeriods = 0;
                        else
                            temp.quebec.retroPayPeriods = 0;

                        //stacy we dont pass this
                        //Year-to-date prior cumulative bonuses paid (currently used only in bonus and commission income calculations
                        //(CalculationType = CalculationType.Bonus or CalculationType = CalculationType.Commission)). Default = 0.
                        if (temp.quebec.CalculationType == CalculationTypeQC.Bonus || temp.quebec.CalculationType == CalculationTypeQC.Commission)
                            temp.quebec.ytdBonus = 0;
                        else
                            temp.quebec.ytdBonus = 0;

                        //Year-to-date prior cumulative pension income paid (currently used only in commission income calculations (CalculationType = CalculationType.Commission)). Default = 0.
                        if (temp.quebec.CalculationType == CalculationTypeQC.Pension)
                            temp.quebec.ytdPension = 0;

                        //Year-to-date prior cumulative taxable cash benefits paid (currently used only in commission income calculations(CalculationType = CalculationType.Commission)). 
                        //Default = 0.
                        if (temp.quebec.CalculationType == CalculationTypeQC.Commission)
                            temp.quebec.ytdtxCashBenefitsAmount = 0;

                        //Year-to-date prior cumulative taxable non-­cash benefits paid (currently used only in commission income calculations (CalculationType = CalculationType.Commission)). Default = 0.
                        if (temp.quebec.CalculationType == CalculationTypeQC.Commission)
                            temp.quebec.ytdtxNonCashBenefitsAmount = 0;

                        //Year-to-date prior cumulative vacation pay paid (currently used only in commission income calculations (CalculationType = CalculationType.Commission)). Default = 0.
                        if (temp.quebec.CalculationType == CalculationTypeQC.Commission)
                            temp.quebec.ytdVacationPay = 0;

                        //Year-to-date prior cumulative salary and/ or wages paid (currently used only in commission income calculations(CalculationType = CalculationType.Commission)). Default = 0.
                        if (temp.quebec.CalculationType == CalculationTypeQC.Commission)
                            temp.quebec.ytdWages = 0;

                        #endregion

                        #region zeroed values

                        //do our inputs need this?
                        //Current pay period deduction respecting the CIP (Cooperative Investment Plan), that is, 125% of the amount withheld from the employee's remuneration for the 
                        //purchase of preferred shares qualifying under the CIP.
                        temp.quebec.dedCIP = 0;

                        //Current payroll deduction for amount withheld for the pay period for the purchase of class A shares in the Fonds de solidarite des travailleurs du Quebec(FTQ).
                        temp.quebec.dedQ = 0;

                        //Current payroll deduction for amount withheld for the pay period for the purchase of class A or class B shares in Fondaction, the Fonds de developpement de la Confederation des syndicates nationaux pour la cooperation et l'emploi.
                        temp.quebec.dedQ1 = 0;

                        //Amount withheld from current pay period for contribution to an RCA (retirement compensation arrangement).
                        temp.quebec.dedRCA = 0;

                        //Amount withheld from current pay period for contribution to an RPP.
                        temp.quebec.dedRPP = 0;

                        //Current pay period security option deduction.
                        temp.quebec.dedSecurity = 0;

                        //Current pay period travel deduction for residents of designated remote areas.
                        temp.quebec.dedTravel = 0;

                        //need for inputs
                        //Exempt from Quebec Health contribution - requested by employee on TP-1015.3-V.
                        //Value         Description 
                        //  0(default)  No exemption
                        //  1           Exempt from Quebec Health contribution
                        temp.quebec.exemptQITD_Z = 0;

                        //need for inputs
                        //Quebec Parental Insurance Program (QPIP)exempt indicator.
                        //Value         Description 
                        //  0(default)  No exemption
                        //  1           Exempt from QPIP contributions.
                        temp.quebec.isQPIPexempt = 0;

                        //need for inputs
                        //Quebec Pension Plan(QPP) exempt indicator. 
                        //Value         Description
                        //  0(default)  No exemption
                        //  1           Exempt from QPP contributions.
                        temp.quebec.isQPPexempt = 0;

                        //Deductions indicated on line 19 of form TP-1015.3-V. Default = 0.
                        temp.quebec.J = 0;

                        //need for inputs
                        //Annual taxable deductions authorized by Revenu Quebec after the employee completes and submits form TP - 1016 - V.Default = 0.
                        temp.quebec.J1 = 0;

                        //need for inputs
                        //Non-refundable tax credits authorized by Revenu Quebec for the year after the employee completed form TP-1016-V (for example, the tax credit for charitable donations).
                        temp.quebec.K1 = 0;

                        //Current payroll vacation pay amount, rounded to the nearest penny (e.g. 100.00 should be entered as 10000). 
                        //Is used for CalculationType = CalculationType.RegularSalary, CalculationType.Bonus and CalculationType.RetroPay (calcType = 1, 3 and 4).
                        if (temp.quebec.CalculationType == CalculationTypeQC.RegularSalary || temp.quebec.CalculationType == CalculationTypeQC.Bonus || temp.quebec.CalculationType == CalculationTypeQC.RetroPay)
                            temp.quebec.vacationPayAmount = 0;
                        else
                            temp.quebec.vacationPayAmount = 0;

                        //Year-­to-date prior cumulative deduction respecting the CIP, that is, 125% of the amount withheld from the employee’s remuneration for the purchase of 
                        //preferred shares qualifying under the CIP. Default = 0.
                        temp.quebec.ytdCIP = 0;

                        //Year-to-date prior cumulative commissions paid (currently used only in commission income calculations(CalculationType = CalculationType.Commission)). Default = 0.
                        temp.quebec.ytdComm = 0;

                        //need this for inputs?
                        //Year-to-date prior cumulative additional source deductions of income tax requested by the employee on form TP-1017-V, TP-1015.N-V, or TP-1015.3-V. Default = 0.
                        temp.quebec.ytdL = 0;

                        //need this for inputs?
                        /* The portion of year-­to-date prior cumulative remuneration that gives entitlement to one of the following deductions: 
                            - The deduction for employment income situated on a reserve or premises,
                            - The deduction for employment income earned on a vessel,
                            - The deduction for employees of an international financial centre (IFC),
                            - The deduction for foreign specialists, foreign researchers, foreign researchers on a post-doctoral internship, foreign experts, foreign professors, foreign producers, 
                                foreign individuals holding a key position in a foreign production filmed in Quebec, foreign farm workers, members of the Canadian forces or members of a 
                                Canadian police force.
                            Default = 0;
                        */
                        temp.quebec.ytdOther = 0;

                        //Year-­to-­date prior cumulative amounts withheld for the purchase of class A shares in the Fonds de solidarite des travailleurs du Quebec(FTQ). Default = 0.
                        temp.quebec.ytdQ = 0;

                        //Year-­to-­date prior cumulative amounts withheld for the purchase of class A or class B shares in the Fonds de developpement de la Confederation des syndicates 
                        //nationaux pour la cooperation et l'emploi (called "Fondaction"). Default = 0.
                        temp.quebec.ytdQ1 = 0;

                        //Year-to-date prior Quebec Income Tax employee payroll deductions.Default = 0.
                        temp.quebec.ytdQITD = 0;

                        //Health contribution portion of year-to-date prior Quebec Income Tax employee payroll deductions (QITD). Default = 0.
                        temp.quebec.ytdQITD_Z = 0;

                        //Year-to-date prior Quebec Parental Insurance Program (QPIP) employer contributions.  Default = 0.
                        temp.quebec.ytdQPIP_employer = 0;

                        //Year-­to-­date prior cumulative amounts withheld as contributions paid under a retirement compensation arrangement. Default = 0.
                        temp.quebec.ytdRCA = 0;

                        //Year-­to-­date prior cumulative amounts withheld as contributions to an RPP.Default = 0.
                        temp.quebec.ytdRPP = 0;

                        //Year-­to-­date prior cumulative amounts withheld as contributions to an RRSP.Default = 0.
                        temp.quebec.ytdRRSP = 0;

                        //Year-­to-date prior cumulative security option deduction. Default = 0.
                        temp.quebec.ytdSecurity = 0;

                        //Year-­to-date prior cumulative travel deduction for residents of designated remote areas. Default = 0.
                        temp.quebec.ytdTravel = 0;

                        #endregion


                        //stacy - this isnt clear in our inputs if they are EI insurable or not...
                        /*
                        If the taxable benefit is subject to EI, then you must enter it as the value for the “txCashBenefitsAmount” property of the EmployeeData object (even if it is not a Cash benefit).
                        CPTL treats the “txNonCashBenefitsAmount” field as exempt from EI, so all taxable benefits subject to EI should be included in the “txCashBenefitsAmount” field.
                        */

                        //Current payroll taxable cash benefits amount, rounded to the nearest penny (e.g. 100.00 should be entered as 10000). 
                        //Is used for CalculationType = CalculationType.RegularSalary, CalculationType.Bonus and CalculationType.RetroPay (calcType = 1, 3 and 4).
                        if (temp.quebec.CalculationType == CalculationTypeQC.RegularSalary || temp.quebec.CalculationType == CalculationTypeQC.Bonus || temp.quebec.CalculationType == CalculationTypeQC.RetroPay)
                        //temp.quebec.txCashBenefitsAmount = 0;
                        {
                            temp.quebec.txCashBenefitsAmount = (int)Math.Round(Convert.ToDecimal(reader[52]) * 100, MidpointRounding.AwayFromZero);
                            temp.canada.txCashBenefitsAmount = (int)Math.Round(Convert.ToDecimal(reader[12]) * 100, MidpointRounding.AwayFromZero);
                        }
                        else
                            temp.quebec.txCashBenefitsAmount = temp.canada.txCashBenefitsAmount = 0;

                        //Current payroll taxable non-cash benefits amount, rounded to the nearest penny (e.g. 100.00 should be entered as 10000). 
                        //Is used for CalculationType = CalculationType.RegularSalary, CalculationType.Bonus and CalculationType.RetroPay(calcType = 1, 3 and 4).
                        if (temp.quebec.CalculationType == CalculationTypeQC.RegularSalary || temp.quebec.CalculationType == CalculationTypeQC.Bonus || temp.quebec.CalculationType == CalculationTypeQC.RetroPay)
                        {
                            temp.quebec.txNonCashBenefitsAmount = temp.canada.txNonCashBenefitsAmount = 0;
                            //temp.quebec.txNonCashBenefitsAmount = (int)Math.Round(Convert.ToDecimal(reader[12]) * 100, MidpointRounding.AwayFromZero);
                            //temp.canada.txNonCashBenefitsAmount = temp.quebec.txNonCashBenefitsAmount;
                        }
                        else
                            temp.quebec.txNonCashBenefitsAmount = temp.canada.txNonCashBenefitsAmount = 0;

                        //Current payroll regular salary/wages amount, rounded to the nearest penny (e.g. 5,000.00 should be entered as 500000). 
                        //Is used for CalculationType = CalculationType.RegularSalary, CalculationType.Bonus and CalculationType.RetroPay (calcType = 1, 3 and 4).
                        if (temp.quebec.CalculationType == CalculationTypeQC.RegularSalary || temp.quebec.CalculationType == CalculationTypeQC.Bonus || temp.quebec.CalculationType == CalculationTypeQC.RetroPay)
                        {
                            temp.quebec.wagesAmount = (int)Math.Round(Convert.ToDecimal(reader[51]) * 100, MidpointRounding.AwayFromZero);
                            temp.canada.wagesAmount = (int)Math.Round(Convert.ToDecimal(reader[4]) * 100, MidpointRounding.AwayFromZero);
                        }
                        else
                            temp.quebec.wagesAmount = temp.canada.wagesAmount = 0;

                        //Year-to-date prior Quebec Parental Insurance Program(QPIP) employee payroll deductions.  Default = 0.
                        temp.quebec.ytdQPIP = (int)Math.Round(Convert.ToDecimal(reader[41]) * 100, MidpointRounding.AwayFromZero); //Ytd Ppip Deducted

                        //Year-to-date prior Quebec Pension Plan (QPP) employee payroll deductions. Default = 0.
                        temp.quebec.ytdQPP = (int)Math.Round(Convert.ToDecimal(reader[39]) * 100, MidpointRounding.AwayFromZero); //Ytd Cpp/Qpp Deducted

                        //Enter the total claim amount from the employee’s federal TD1 form. 
                        //If the employee has not completed a federal TD1 form, enter the value 1, which will result in the default Basic Claim Amount being used for payroll calculations. 
                        temp.canada.TD1_Claim = (int)Math.Round(Convert.ToDecimal(reader[21]) * 100, MidpointRounding.AwayFromZero); //federal total claim amount

                        //Enter the amount of additional income tax deductions per pay period requested by the employee on his / her federal TD1 form.  If no additional amount is requested, enter 0.
                        temp.canada.TD1_L = (int)Math.Round(Convert.ToDecimal(reader[24]) * 100, MidpointRounding.AwayFromZero); //fed additional tax

                        //Enter the total claim amount from the employee’s provincial TD1P form. 
                        //If the employee has not completed a provincial TD1P form, enter the value 1, which will result in the default Basic Claim Amount being used for payroll calculations.
                        temp.canada.TD1P_Claim = (int)Math.Round(Convert.ToDecimal(reader[25]) * 100, MidpointRounding.AwayFromZero); //prov total claim amount

                        //Enter the amount of other provincial or territorial tax credits, such as medical expenses and charitable donations, authorized by a tax services office or tax centre. 
                        //If no amount is claimed, enter 0.
                        temp.canada.CRA_K3P = (int)Math.Round(Convert.ToDecimal(reader[28]) * 100, MidpointRounding.AwayFromZero); //prov tax credit

                        //Enter the amount of additional provincial or territorial tax reduction based on applicable amounts reported on the provincial/ territorial Form TD1P. 
                        //If no amount is claimed, enter 0.
                        temp.canada.CRA_Y = 0;

                        //If an employer has qualified for a reduced EI contribution rate factor, enter the reduced employer EI rate.
                        //This amount will be used to determine the employer’s EI contribution cost and the amount of EI deduction that is returnable to the employee.
                        //If unknown, enter a Null value, and the default EI employer contribution factor (1.4) will be assumed.
                        //Enter the amount WITH a decimal point – e.g. 1.4 should be entered as 1.4.
                        temp.canada.employer_EI_ratefactor = 1.4;

                        //Enter the total amount of year-to-date deductions of Canada Pension Plan (CPP) contributions for the employee. 
                        //This amount will be used to determine if the employee has reached the maximum contribution limit for the year. If unknown, enter 0.
                        temp.canada.ytdCPP = (int)Math.Round(Convert.ToDecimal(reader[39]) * 100, MidpointRounding.AwayFromZero); //Ytd Cpp/Qpp Deducted

                        //Enter the total amount of year-to-date deductions of Employment Insurance (EI) premiums for the employee. 
                        //This amount will be used to determine if the employee has reached the maximum deduction limit for the year. If unknown, enter 0.
                        temp.canada.ytdEI = (int)Math.Round(Convert.ToDecimal(reader[40]) * 100, MidpointRounding.AwayFromZero);//Ytd Ei Deducted

                        #region comments
                        /*
                         //Enter the total claim amount from the employee’s federal TD1 form. 
                         //If the employee has not completed a federal TD1 form, enter the value 1, which will result in the default Basic Claim Amount being used for payroll calculations. 
                         temp.TD1_Claim = (int)Math.Round(Convert.ToDecimal(reader[21]) * 100, MidpointRounding.AwayFromZero); //federal total claim amount

                         //If an employer has qualified for a reduced EI contribution rate factor, enter the reduced employer EI rate.
                         //This amount will be used to determine the employer’s EI contribution cost and the amount of EI deduction that is returnable to the employee.
                         //If unknown, enter a Null value, and the default EI employer contribution factor (1.4) will be assumed.
                         //Enter the amount WITH a decimal point – e.g. 1.4 should be entered as 1.4.
                         temp.employer_EI_ratefactor = 1.4;

                         //Enter the total amount of year-to-date deductions of Employment Insurance (EI) premiums for the employee. 
                         //This amount will be used to determine if the employee has reached the maximum deduction limit for the year. If unknown, enter 0.
                         temp.ytdEI = (int)Math.Round(Convert.ToDecimal(reader[40]) * 100, MidpointRounding.AwayFromZero);//Ytd Ei Deducted

                         //stacy note:  we will have to pass this in, reformat our input data
                         temp.CalculationType = (CalculationTypeQC)Enum.Parse(typeof(CalculationTypeQC), "1");

                         //stacy what about lump sums?
                         //set LUMP SUM?????

                         //STACY --> GET PROGRAM TO RUN FROM THIS DATA ABOVE, SEE IF ANY OF HTIS DATA BELOW NEEDS TO BE USED ANYWHERE.  CHECK OTHER THREAD ENGINE TO SEE IF UNUSED DATA IS HERE...
                         temp.EmployeeId = Convert.ToInt64(reader[1]);

                         temp.CppQppWages = Convert.ToDecimal(reader[5]);
                         //temp.CppQppDeducted = Convert.ToDecimal(reader[6]);

                         temp.EiWages = Convert.ToDecimal(reader[7]);
                         //temp.EiDeducted = Convert.ToDecimal(reader[8]);

                         temp.PpipWages = Convert.ToDecimal(reader[9]);
                         //temp.PpipDeducted = Convert.ToDecimal(reader[10]);

                         temp.CanLumpsum = Convert.ToDecimal(reader[16]);
                         temp.CanLumpsumQc = Convert.ToDecimal(reader[18]);

                         //temp.TaxDeducted = Convert.ToDecimal(reader[19]);
                         //temp.TaxQcDeducted = Convert.ToDecimal(reader[20]);

                         temp.DesignatedAreaDeduction = Convert.ToDecimal(reader[22]);
                         temp.AuthorizedAnnualDeduction = Convert.ToDecimal(reader[23]);

                         temp.ProvincialAuthorizedAnnualDeduction = Convert.ToInt16(reader[27]);
                         temp.ProvincialDesignatedAreaDeduction = Convert.ToInt16(reader[29]);

                         temp.Lcf = Convert.ToDecimal(reader[30]);
                         temp.Lcp = Convert.ToDecimal(reader[31]);

                         temp.NorthwestTerritoriesTaxCalculationBase = Convert.ToDecimal(reader[32]);
                         temp.NorthwestTerritoriesTaxAssessed = Convert.ToDecimal(reader[33]);
                         temp.NunavutTaxCalculationBase = Convert.ToDecimal(reader[34]);
                         temp.NunavutTaxAssessed = Convert.ToDecimal(reader[35]);

                         temp.EmployerEmploymentInsuranceAmount = Convert.ToDecimal(reader[36]);
                         temp.EmployerCanadaPensionPlanAmount = Convert.ToDecimal(reader[37]);
                         temp.EmployerProvincialParentalAmount = Convert.ToDecimal(reader[38]);

                         //temp.CppQppWagesCalculated = Convert.ToDecimal(reader[42]); ;
                         //temp.EiWagesCalculated = Convert.ToDecimal(reader[43]); ;
                         //temp.PpipWagesCalculated = Convert.ToDecimal(reader[44]);
                         */
                        #endregion

                        tableData.Add(temp);
                        if (tableData.Count == 15000) break; //load test code
                    }//load test code
                }

                connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("EXCEPTION");
                Console.WriteLine(ex.Message);
            }

            return tableData;
        }

        private static void ShutdownControlThreads()
        {
            _controlThreadsActive = false;

            while (_threadOpenedCount < _threadCount && _threadClosedCount < _threadCount)
            {
                Console.WriteLine(String.Format("Waiting for {0} Control threads to stop", _threadCount - _threadClosedCount));
                Thread.Sleep(5000);
            }
        }
    }

    public class MetEmployeeData : EmployeeData
    {
        public long EmployeeId = -1;

        public MetEmployeeData()
        {
        }
    }
    public class MetEmployeeDataQC
    {
        public long EmployeeId = -1;
        public EmployeeData canada;
        public EmployeeDataQC quebec;

        public MetEmployeeDataQC()
        {
            canada = new EmployeeData();
            quebec = new EmployeeDataQC();
        }
    }

}
