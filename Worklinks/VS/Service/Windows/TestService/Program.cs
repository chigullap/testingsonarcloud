﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TestService
{
    public class Program
    {
  


            static void Main(string[] args)
        {

            //****load filenames here****
            String[] fileNames = new String[]
            {

//@"\\wqasql01\e$\WorkLinks\FileTransfers\hrxml\backup\20150619\9abe24bc-99ab-4392-8f17-757dcb43f7a3__7b8f4135-29a3-4ce4-85ba-7abe671d5198.xml",
//@"\\wqasql01\e$\WorkLinks\FileTransfers\hrxml\0f98a778-e255-4fe8-b39c-ff3dfd388934__F6DED994-1DBC-4F11-8A1E-89B1EE2A7863.xml",
//@"C:\temp\hrxml\testMissingGender.xml"

//@"C:\temp\hrxml\withGenderMale.xml"
//@"C:\temp\hrxml\withGenderUnknown.xml"
//@"\\wprsql01\e$\WorkLinks\hrxml\input_processed\41527409-384d-4b4e-becd-c4495a642c43__f8e9e0f0-d342-4df7-92e4-c1e8b5bac509.xml"
//@"\\wprsql01\e$\WorkLinks\hrxml\input_processed\a6e51820-ed51-40d6-888c-7903485c0413__CF533F51-AA64-4E92-B539-7047A564E9F9.xml"
//@"\\wprsql01\e$\WorkLinks\hrxml\input_processed\025e4a4c-6f03-4f35-a364-b2a0e0d0d37e__21C0FB8F-421C-45DF-B799-5730AFFB50C9.xml"
//@"\\wprsql01\e$\WorkLinks\hrxml\input_processed\3d61fd52-1857-414e-9f5b-06fda3ecc224__FC63E115-8919-4390-BF32-3FCD964583D4.xml"

//@"\\wprsql01\e$\WorkLinks\hrxml\input_processed\b5ae141d-1250-459d-a25b-44f533f51423__FC0E6143-FD0F-4A95-B918-6BA57346D517.xml"
//@"\\wprsql01\e$\WorkLinks\hrxml\input_processed\4e93801a-2109-43ae-baf0-378ed623a128__42A09458-BD4A-4184-8B19-10153D4857C4.xml"
//@"\\wprsql01\e$\WorkLinks\hrxml\input_processed\5f71b5fc-6a85-4e51-b17b-dc7c48900142__80CE092D-A148-4028-AA4B-3B8539029CD1.xml"
//@"\\wprsql01\e$\WorkLinks\hrxml\input_processed\0a1f27eb-4fe2-4318-b400-bf3924c49ae8__286FA94D-7402-441A-BC17-A2B0AC8B4410.xml"
//@"\\wprsql01\e$\WorkLinks\hrxml\input_processed\01cbc2a7-ffae-4878-a293-ba0e33b2df40__97DB6B4A-40A9-41FB-BAF1-F9A8DA0E68A8.xml"
//@"\\wqasql01\e$\WorkLinks\FileTransfers\output\1847b92a-f79e-41de-999d-65db8c238824__6099157A-4BD0-42A8-B741-501FEF8DD40C.xml"


                //stacy test - my location for test files.  Leaving this comment in so i remember the path.
                //@"C:\temp\hrxmlTest\stacyTestHrXml1.xml",
                //@"C:\temp\hrxmlTest\stacyTestHrXml2.xml"
                //@"C:\temp\hrxmlTest\stacyTestHrXml3.xml"

            //if running same file twice, must have diff BOD_ID in import_export_log table.

                //@"\\wprsql01\e$\WorkLinks\hrxml\input_processed\7f2d1f5c-f74e-43a9-a983-1789a8065b72__086C2FE0-1EE9-49C8-A132-5FD2F03C4309.xml"
             //   @"C:\temp\20161027\multiaddress.xml"
@"C:\temp\20180830\4444a083-63fd-4c9a-ad99-a572fbe4e738__A7469330-24DA-43AF-8458-71E90ECE7C38.xml",
//@"C:\temp\20180819\dae3232d -568a-436d-8d1c-0f6881ced688__8321E043-1CE8-4C68-B049-6F0B58C211BF.xml",



            };

            int i = 1;
            foreach (String fileName in fileNames)
            {

                byte[] data = null;
                //            using (FileStream stream = new FileStream(@"C:\temp\hrxml\20150521\set2\" + fileName, FileMode.Open))
                using (FileStream stream = new FileStream(fileName, FileMode.Open))
                {
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        stream.CopyTo(memoryStream);
                        data = memoryStream.ToArray();
                    }
                }


                HrXml.HumanResourcesXML client = new HrXml.HumanResourcesXML();

                //qa credentials 
                client.Credentials = new NetworkCredential(@"ISLAND\ngaqa_hrxml_user", @"sNf_9%^d6%dLCQt");
                //prod credentials
                //client.Credentials = new NetworkCredential(@"ISLAND\ngaprod_hrxml_user", @"3+u6HVYkk=+g3?d");

                try
                {
                    Console.WriteLine("Uploading file " + i++ + "...");
                    byte[] blah = client.UploadFile(data);
                }
                catch (Exception exc)
                {
                    int j = 0;
                }
            }
        }
    }
}
