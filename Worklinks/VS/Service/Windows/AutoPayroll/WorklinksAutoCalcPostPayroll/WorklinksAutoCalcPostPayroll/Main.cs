﻿using System;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Threading;

using WorklinksAutoCalcPostPayroll.Parameters;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessLogic;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;
using WorkLinks.BusinessLayer.BusinessLogic.Central;

namespace WorklinksAutoCalcPostPayroll
{
    public partial class Main : ServiceBase
    {
        #region fields
        BackgroundWorker _autoPostWorker = null;
        private const string _userName = "wl_auto_payroll_engine";
        private const string _languageCode = "EN";
        DateTime _todaysDate = DateTime.MinValue;
        private PayrollManagement _payMgmt = new PayrollManagement();
        #endregion

        #region properties
        private BackgroundWorker AutoPostWorker
        {
            get
            {
                if (_autoPostWorker == null)
                    _autoPostWorker = new BackgroundWorker();
                return _autoPostWorker;
            }
        }
        #endregion

        #region SERVICE SECTION
        public Main()
        {
            InitializeComponent();
           
            if (!EventLog.SourceExists("WorklinksAutoPayroll"))
            {
                EventLog.CreateEventSource("WorklinksAutoPayroll", "AutoPayroll");
            }
            _eventLog.Source = "WorklinksAutoPayroll";
            _eventLog.Log = "AutoPayroll";

            InitializeBackGroundWorker();
        }
        private void InitializeBackGroundWorker()
        {
            AutoPostWorker.WorkerSupportsCancellation = true;

            // Set event handlers
            this.AutoPostWorker.DoWork += new DoWorkEventHandler(this.AutoPostWorker_DoWork);
            this.AutoPostWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.AutoPostWorker_RunWorkerCompleted);
        }
        protected override void OnStart(string[] args)
        {
            _eventLog.WriteEntry("OnStart triggered:  " + DateTime.Now.ToString());
            _eventLog.WriteEntry("sleeping for 20 seconds.");

            //Sleep for 20 seconds at the start so we can attach if we are debugging.
            Thread.Sleep(20000);

            //this is the auto post worker
            AutoPostWorker.RunWorkerAsync();
        }
        protected override void OnStop()
        {
            AutoPostWorker.CancelAsync();
            _eventLog.WriteEntry("OnStop triggered:  " + DateTime.Now.ToString());
        }
        #endregion

        #region auto calc post section
        private void AutoPostWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!AutoPostWorker.CancellationPending)
            {
                try
                {
                    //check if we need to auto calc/post a payroll
                    PerformAutoCalcPost();
                }
                catch (Exception ex)
                {
                    _eventLog.WriteEntry("Exception thrown:  " + ex.Message, EventLogEntryType.Error);

                    //send an email to support
                    SendEmailParameters emailParms = CreateAndPopulateCentralEmailParameters();
                    emailParms.Body = "An exception was thrown: Error is:  " + ex.Message + " and the Inner exception is:  " + ex.InnerException;

                    EmailManagement.SendMessage(emailParms);
                }
                finally
                {
                    //sleep until next cycle
                    _eventLog.WriteEntry("sleeping for " + ApplicationParameter.PollIntervalSecondsKey.ToString() + " seconds.");
                    Thread.Sleep(ApplicationParameter.PollIntervalSecondsKey * 1000);
                }
            }
        }

        private void AutoPostWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //do nothing
        }

        private void PerformAutoCalcPost()
        {
            //set dbuser to use the central db
            DatabaseUser user = new DatabaseUser() { DatabaseName = ApplicationParameter.CentralDatabaseName, UserName = _userName, LanguageCode = _languageCode };

            //get a list of companies using the auto calc/post process
            _eventLog.WriteEntry("Getting a list of companies using the auto calc/post process");
            AutoCalculatePayrollDatabaseMapCollection autoMapColl = new AutoCalculatePayrollManagement().GetAutoCalculatePayrollDatabaseMapCollection(user);

            //set date
            SetDate();

            _eventLog.WriteEntry("Start cycling through the list of companies using the auto calc/post process, number of companies:  " + autoMapColl.Count);

            foreach (AutoCalculatePayrollDatabaseMap map in autoMapColl)
            {
                //switch to the company database to get their code system and payroll data
                user.DatabaseName = map.DatabaseName;

                bool usingAutoCalcPayrollFlag = Convert.ToBoolean(new CodeManagement().GetCodeTable(user.DatabaseName, "EN", CodeTableType.System, "AUTOCPAY")[0].Description);
 
                //secondary check to see if they are using the auto calc/post process
                if (usingAutoCalcPayrollFlag)
                {
                    _eventLog.WriteEntry("Checking if payroll needs to be auto calculated and posted");
                    _payMgmt.AutoCalcPayroll(user, _todaysDate);
                }
            }

            _eventLog.WriteEntry("Done cycling through the list of companies using the auto calc/post process");
        }

        private void SetDate()
        {
            //get todays date
            _todaysDate = DateTime.Now;

            //check if we are overriding the date
            if (ApplicationParameter.OverrideDateConfig != "")
            {
                _todaysDate = Convert.ToDateTime(ApplicationParameter.OverrideDateConfig);
                //write to event log that we are overriding the date
                _eventLog.WriteEntry("DATE IS BEING OVERRIDDEN IN THE app.config WITH A VALUE OF:  " + _todaysDate.ToString(), EventLogEntryType.Warning);
            }
            else
                _eventLog.WriteEntry("DATE BEING USED CURRENTLY HAS A VALUE OF:  " + _todaysDate.ToString());
        }

        public static SendEmailParameters CreateAndPopulateCentralEmailParameters()
        {
            SendEmailParameters parms = new SendEmailParameters();

            parms.ToAddress = ApplicationParameter.EmailToAddress;
            parms.FromAddress = ApplicationParameter.EmailFromAddress;
            parms.CcAddress = ApplicationParameter.EmailCcAddress;
            parms.BccAddress = ApplicationParameter.EmailBccAddress;
            parms.Subject = "WORKLINKS AUTO CALC POST ENGINE HAS THROWN AN EXCEPTION";
            parms.SmtpHostName = ApplicationParameter.EmailSMTPServerAddress;
            parms.SmtpPortNumber = ApplicationParameter.EmailPort;
            parms.SmtpLogin = ApplicationParameter.EmailLoginUser;
            parms.SmtpPassword = ApplicationParameter.EmailPassword;
            parms.EnableSSL = ApplicationParameter.EmailSSL;

            return parms;
        }
        #endregion
    }
}
