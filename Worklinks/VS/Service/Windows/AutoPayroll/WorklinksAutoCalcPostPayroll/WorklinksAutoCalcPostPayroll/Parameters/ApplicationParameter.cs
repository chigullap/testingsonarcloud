﻿using System;

using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessLogic;
using System.Configuration;


namespace WorklinksAutoCalcPostPayroll.Parameters
{
    public static class ApplicationParameter
    {
        #region from web.config
        private const String _centralDatabaseName = "CentralDatabaseName";
        public static String OverrideDateConfig = ConfigurationManager.AppSettings["OverrideDate"];
        public static int PollIntervalSecondsKey = Convert.ToInt32(ConfigurationManager.AppSettings["PollIntervalSeconds"]);
        #endregion

        public static String CentralDatabaseName
        {
            get { return ConfigurationManager.AppSettings.Get(_centralDatabaseName); }
        }

        private static CodeCollection CodeSystemCollection = (new CodeManagement()).GetCodeTable(ApplicationParameter.CentralDatabaseName, "EN", CodeTableType.System);

        public static String GetSystemValue(String code)
        {
            return CodeSystemCollection[code].Description;
        }

        //mail
        private const String _emailToAddress = "MAILTO";
        private const String _emailFromAddress = "MAILFROM";
        private const String _emailCcAddress = "MAILCC";
        private const String _emailBccAddress = "MAILBCC";
        private const String _emailSMTPServerAddress = "MAILSMTP";
        private const String _emailPort = "MAILPORT";
        private const String _emailLoginUser = "MAILLOGN";
        private const String _emailPassword = "MAILPASS";
        private const String _emailSSL = "MAILSSL";

        //SEND EMAIL TO SUPPORT IF EXCEPTION OCCURS
        public static String EmailToAddress { get { return GetSystemValue(_emailToAddress); } }
        public static String EmailFromAddress { get { return GetSystemValue(_emailFromAddress); } }
        public static String EmailCcAddress { get { return GetSystemValue(_emailCcAddress); } }
        public static String EmailBccAddress { get { return GetSystemValue(_emailBccAddress); } }
        public static String EmailSMTPServerAddress { get { return GetSystemValue(_emailSMTPServerAddress); } }
        public static int EmailPort { get { return Convert.ToInt32(GetSystemValue(_emailPort)); } }
        public static String EmailLoginUser { get { return GetSystemValue(_emailLoginUser); } }
        public static String EmailPassword { get { return GetSystemValue(_emailPassword); } }
        public static bool EmailSSL { get { return Convert.ToBoolean(GetSystemValue(_emailSSL)); } }

    }
}