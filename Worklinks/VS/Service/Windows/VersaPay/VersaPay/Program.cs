﻿using System;
using System.Text;
using System.Threading;
using System.Net;

namespace VersaPay
{
    public class Program
    {
        public enum Operations
        {
            GetFunds,
            GetListOfTransactions,
            ShowTransaction,
            ListSentDebitAgreements,
            ViewDebitAgreement,
            ListReceivedDebitAgreements,
            CreateTransaction,
            ApproveTransaction,
            CancelTransaction
        }
        static void Main(string[] args)
        {
            DoVersaPayStuff();
        }

        public static void DoVersaPayStuff()
        {
            //FUND SOURCES API
            //simply return a list of the bank accounts, credit cards, and balance associated with your account
            string result1 = CallVersaPayWithNoXmlData(Operations.GetFunds, null);

            //DEBIT AGREEMENTS API
            //View a list of the latest debit agreements you have created. 50 agreements are returned per call.
            string result2 = CallVersaPayWithNoXmlData(Operations.ListSentDebitAgreements, null);

            //View Debit Agreement
            string result3 = CallVersaPayWithNoXmlData(Operations.ViewDebitAgreement, "65TMAKY9253K");

            //View a list of the latest debit agreements you have received
            string result4 = CallVersaPayWithNoXmlData(Operations.ListReceivedDebitAgreements, null);


            //TRANSACTIONS API
            //View a list of the latest transactions for your account. 50 transactions are returned per call.
            string result5 = CallVersaPayWithNoXmlData(Operations.GetListOfTransactions, null);

            //NOTE:  the bool in this call (true = use admin creds)
            string result6 = CallVersaPayWithXmlData(Operations.CreateTransaction, true, CreateTransactionXml("direct_debit", "555", "001", "25039", "9876543", "James", "Kirk"));
            //string result6 = CallVersaPayWithXmlData(Operations.CreateTransaction, false, CreateTransactionXml("direct_debit", "555", "001", "25039", "9876543", "James", "Kirk"));

            //Return a transaction’s details by supplying a transaction’s token attribute.
            string result7 = CallVersaPayWithNoXmlData(Operations.ShowTransaction, "4L5YIP6RIJHD");


            /*********************************************************************************************************************
                These two methods below (Approve/Cancel), they are of no use right now because all our stuff is being auto approved.  
                Email to VersaPay to ask how we do this to require approval.

            *********************************************************************************************************************/
            //Approve a transaction’s by supplying a transaction’s token attribute

            //string result8 = CallVersaPayWithNoXmlData(Operations.ApproveTransaction, "4L5YIP6RIJHD");

            //Cancel a new, wait_for_request_approval or wait_for_bank_account_verification transaction you created by supplying a transaction’s token attribute. 
            //Transactions cannot be cancelled after they have been sent to the bank and are in_progress

            //string result9 = CallVersaPayWithNoXmlData(Operations.CancelTransaction, "4L5YIP6RIJHD");
        }
        public static string CallVersaPayWithXmlData(Operations action, bool useAdminCredentials, string data)
        {
            String username = (useAdminCredentials) ? "XTdkNx_Q5o3pWNXHPr1J" : "zNGWeorgwJkk4EEFPnHi";
            String password = (useAdminCredentials) ? "yskRu3YBg56Txxs3r3eZ" : "t6msMoiWQ3aKNgGXST1S";
            string authInfo = username + ":" + password;
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));

            string url = "";

            switch (action)
            {
                case Operations.CreateTransaction:
                    url = "https://demo.versapay.com/api/transactions.xml";
                    break;
                default:
                    break;
            }

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "text/xml; encoding='utf-8'";
            request.Headers["Authorization"] = "Basic " + authInfo;

            byte[] bytes;
            bytes = Encoding.ASCII.GetBytes(data);
            request.ContentLength = bytes.Length;
            System.IO.Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            System.IO.Stream responseStream = response.GetResponseStream();
            string responseStr = new System.IO.StreamReader(responseStream).ReadToEnd();

            return responseStr;
        }

        public static string CallVersaPayWithNoXmlData(Operations action, string token)
        {
            String username = "XTdkNx_Q5o3pWNXHPr1J";
            String password = "yskRu3YBg56Txxs3r3eZ";
            string authInfo = username + ":" + password;
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));

            string url = "";

            switch (action)
            {
                case Operations.GetFunds:
                    url = "https://demo.versapay.com/api/funds.xml";
                    break;
                case Operations.GetListOfTransactions:
                    url = "https://demo.versapay.com/api/transactions.xml";
                    break;
                case Operations.ShowTransaction:
                    if (token == null) throw new Exception("token must have a value for this method");
                    url = "https://demo.versapay.com/api/transactions/" + token + ".xml";
                    break;
                case Operations.ListSentDebitAgreements:
                    url = "https://demo.versapay.com/api/debit_agreements/sent.xml";
                    break;
                case Operations.ViewDebitAgreement:
                    if (token == null) throw new Exception("token must have a value for this method");
                    url = "https://demo.versapay.com/api/debit_agreements/" + token + ".xml";
                    break;
                case Operations.ListReceivedDebitAgreements:
                    url = "https://demo.versapay.com/api/debit_agreements/received.xml";
                    break;
                case Operations.ApproveTransaction:
                    if (token == null) throw new Exception("token must have a value for this method");
                    url = "https://demo.versapay.com/api/transactions/" + token + "/approve.xml";
                    break;
                case Operations.CancelTransaction:
                    if (token == null) throw new Exception("token must have a value for this method");
                    url = "https://demo.versapay.com/api/transactions/" + token + "/cancel.xml";
                    break;
                default:
                    break;
            }

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = (action != Operations.ApproveTransaction && action != Operations.CancelTransaction) ? "GET" : "POST";
            request.Headers["Authorization"] = "Basic " + authInfo;
            request.ContentType = "text/xml; encoding='utf-8'";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();

            return responseString;
        }
        public static string CreateTransactionXml(string transactionType, string amountInCents, string institutionNumber, string branchNumber, string accountNumber, string firstName, string lastName)
        {
            string data = @"<?xml version=""1.0"" encoding=""UTF-8""?>";
            data += "<transaction><transaction_type>";
            data += transactionType;
            data += "</transaction_type><amount_in_cents>";
            data += amountInCents;
            data += "</amount_in_cents><institution_number>";
            data += institutionNumber;
            data += "</institution_number><branch_number>";
            data += branchNumber;
            data += "</branch_number><account_number>";
            data += accountNumber;
            data += "</account_number><first_name>";
            data += firstName;
            data += "</first_name><last_name>";
            data += lastName;
            data += "</last_name></transaction>";

            return data;
        }
    }
}
