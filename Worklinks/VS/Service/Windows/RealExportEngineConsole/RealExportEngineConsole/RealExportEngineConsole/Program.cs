﻿using RealExportEngineConsole.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessLogic;

namespace RealExportEngineConsole
{
    class Program
    {
        private const string _userName = "wl_real_export_engine";
        private const string _languageCode = "EN";

        static void Main(string[] args)
        {
            Console.WriteLine("Starting to do work");
            //set dbuser to use the customer db
            DatabaseUser user = new DatabaseUser() { DatabaseName = ApplicationParameter.CustomerDatabaseName, UserName = _userName, LanguageCode = _languageCode };

            try
            {
                Console.WriteLine("About to create the files and send them off");

                //export to SFTP
                new WorkLinksDynamicsManagement().CreateAndExportRealEmployeeFiles(
                        user, ParameterHelper.RealFtp, ParameterHelper.LaborLevelExportFileName, ParameterHelper.EmployeeExportFileName, ParameterHelper.AdjustmentRuleExportFileName);

                Console.WriteLine("Done creating and sending files");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception about to be thrown");
                throw ex;
            }
        }
    }
}
