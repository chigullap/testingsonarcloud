﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealExportEngineConsole.Parameters
{
    public static class ApplicationParameter
    {
        #region from app.config

        private const String _centralDatabaseName = "CentralDatabaseName";
        private const String _customerDatabaseName = "CustomerDatabaseName";

        #endregion

        public static String CentralDatabaseName
        {
            get { return ConfigurationManager.AppSettings.Get(_centralDatabaseName); }
        }

        public static String CustomerDatabaseName
        {
            get { return ConfigurationManager.AppSettings.Get(_customerDatabaseName); }
        }
    }
}
