﻿using System.Collections;
using System.ComponentModel;
using WorkLinks.Service.Windows.PrivateService.Parameters;

namespace WorkLinks.Service.Windows.PrivateService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        #region Overrides of Installer

        /// <summary>When overridden in a derived class, performs the installation.</summary>
        /// <param name="stateSaver">
        ///     An <see cref="T:System.Collections.IDictionary" /> used to save information needed to perform
        ///     a commit, rollback, or uninstall operation.
        /// </param>
        /// <exception cref="T:System.ArgumentException">The <paramref name="stateSaver" /> parameter is <see langword="null" />.</exception>
        /// <exception cref="T:System.Exception">
        ///     An exception occurred in the <see cref="E:System.Configuration.Install.Installer.BeforeInstall" /> event handler of
        ///     one of the installers in the collection.
        ///     -or-
        ///     An exception occurred in the <see cref="E:System.Configuration.Install.Installer.AfterInstall" /> event handler of
        ///     one of the installers in the collection.
        /// </exception>
        public override void Install(IDictionary stateSaver)
        {
            string serviceName = Context.Parameters["ServiceName"];
            if (!string.IsNullOrWhiteSpace(serviceName))
            {
                serviceInstaller1.ServiceName = serviceName;
                serviceInstaller1.DisplayName = serviceName;
            }

            base.Install(stateSaver);
        }

        #endregion
    }
}