﻿using System;
using WorkLinks.BusinessLayer.BusinessLogic.Central;

namespace WorkLinks.Service.Windows.PrivateService.Parameters
{
    public class ApplicationParameter
    {
        #region fields

        private static ImportExportManagement _importExportManagement;

        #region from web.config

        private const string _databaseName = "DatabaseName";
        private const string _eventLogSourceName = "EventLogSourceName";
        private const string _eventLogName = "EventLogName";
        private const string _mSMQImportQueueName = "MSMQImportQueueName";
        private const string _mSMQExportQueueName = "MSMQExportQueueName";
        private const string _outputDirectoryPath = "OutputDirectoryPath";
        private const string _importExportProcessingDirectory = "ImportExportProcessingDirectory";
        private const string _disableNgaResponse = "DisableNgaResponse";
        private const string _ngaHrxmlConfirmationUrl = "NgaHrxmlConfirmationUrl";
        private const string _ngaHrxmlConfirmationLogin = "NgaHrxmlConfirmationLogin";
        private const string _ngaHrxmlConfirmationPassword = "NgaHrxmlConfirmationPassword";
        private const string _queueSuffix = "QueueSuffix";

        #endregion

        #region from database

        private const string _hrmlAddressEnabledFlag = "HRX_ADDR";
        private const string _hrmlBankingEnabledFlag = "HRX_BANK";
        private const string _hrmlPayrollBatchEnabledFlag = "HRX_BTCH";
        private const string _hrxmlBiographicalEnabledFlag = "HRX_EBIO";
        private const string _hrxmlEmploymentInformationEnabledFlag = "HRX_EEI";
        private const string _hrmlPaycodeEnabledFlag = "HRX_EPAY";
        private const string _hrmlPositionEnabledFlag = "HRX_EPOS";
        private const string _hrmlEmailEnabledFlag = "HRX_MAIL";
        private const string _hrmlRehireEnabledFlag = "HRX_RHIR";
        private const string _hrmlStatutoryDeductionEnabledFlag = "HRX_STAT";
        private const string _hrmlPhoneEnabledFlag = "HRX_TELE";
        private const string _hrmlTerminationEnabledFlag = "HRX_TERM";
        private const string _hrxmlLeaveEnabledFlag = "HRX_LEAV";

        #endregion

        #endregion


        #region properties

        private static ImportExportManagement ImportExportManagement
        {
            get
            {
                if (_importExportManagement == null)
                    _importExportManagement = new ImportExportManagement();

                return _importExportManagement;
            }
        }

        #region from web.config

        public static string DatabaseName => System.Configuration.ConfigurationManager.AppSettings.Get(_databaseName);

        #endregion

        public static string EventLogSourceName => System.Configuration.ConfigurationManager.AppSettings.Get(_eventLogSourceName);

        public static string EventLogName => System.Configuration.ConfigurationManager.AppSettings.Get(_eventLogName);

        public static string MSMQImportQueueName => System.Configuration.ConfigurationManager.AppSettings.Get(_mSMQImportQueueName);

        public static string MSMQExportQueueName => System.Configuration.ConfigurationManager.AppSettings.Get(_mSMQExportQueueName);

        public static string OutputDirectoryPath => System.Configuration.ConfigurationManager.AppSettings.Get(_outputDirectoryPath);

        public static string ImportExportProcessingDirectory => System.Configuration.ConfigurationManager.AppSettings.Get(_importExportProcessingDirectory);

        public static bool DisableNgaResponse => Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings.Get(_disableNgaResponse));

        public static string NgaHrxmlConfirmationUrl => System.Configuration.ConfigurationManager.AppSettings.Get(_ngaHrxmlConfirmationUrl);

        public static string NgaHrxmlConfirmationLogin => System.Configuration.ConfigurationManager.AppSettings.Get(_ngaHrxmlConfirmationLogin);

        public static string NgaHrxmlConfirmationPassword => System.Configuration.ConfigurationManager.AppSettings.Get(_ngaHrxmlConfirmationPassword);

        public static bool HrmlAddressEnabledFlag => Convert.ToBoolean(ImportExportManagement.GetSystemCodeDescription(DatabaseName, _hrmlAddressEnabledFlag));

        public static bool HrmlBankingEnabledFlag => Convert.ToBoolean(ImportExportManagement.GetSystemCodeDescription(DatabaseName, _hrmlBankingEnabledFlag));

        public static bool HrmlPayrollBatchEnabledFlag => Convert.ToBoolean(ImportExportManagement.GetSystemCodeDescription(DatabaseName, _hrmlPayrollBatchEnabledFlag));

        public static bool HrxmlBiographicalEnabledFlag => Convert.ToBoolean(ImportExportManagement.GetSystemCodeDescription(DatabaseName, _hrxmlBiographicalEnabledFlag));

        public static bool HrxmlEmploymentInformationEnabledFlag => Convert.ToBoolean(ImportExportManagement.GetSystemCodeDescription(DatabaseName, _hrxmlEmploymentInformationEnabledFlag));

        public static bool HrmlPaycodeEnabledFlag => Convert.ToBoolean(ImportExportManagement.GetSystemCodeDescription(DatabaseName, _hrmlPaycodeEnabledFlag));

        public static bool HrmlPositionEnabledFlag => Convert.ToBoolean(ImportExportManagement.GetSystemCodeDescription(DatabaseName, _hrmlPositionEnabledFlag));

        public static bool HrmlEmailEnabledFlag => Convert.ToBoolean(ImportExportManagement.GetSystemCodeDescription(DatabaseName, _hrmlEmailEnabledFlag));

        public static bool HrmlRehireEnabledFlag => Convert.ToBoolean(ImportExportManagement.GetSystemCodeDescription(DatabaseName, _hrmlRehireEnabledFlag));

        public static bool HrmlStatutoryDeductionEnabledFlag => Convert.ToBoolean(ImportExportManagement.GetSystemCodeDescription(DatabaseName, _hrmlStatutoryDeductionEnabledFlag));

        public static bool HrmlPhoneEnabledFlag => Convert.ToBoolean(ImportExportManagement.GetSystemCodeDescription(DatabaseName, _hrmlPhoneEnabledFlag));

        public static bool HrmlTerminationEnabledFlag => Convert.ToBoolean(ImportExportManagement.GetSystemCodeDescription(DatabaseName, _hrmlTerminationEnabledFlag));

        public static bool HrxmlLeaveEnabledFlag => Convert.ToBoolean(ImportExportManagement.GetSystemCodeDescription(DatabaseName, _hrxmlLeaveEnabledFlag));

        public static string QueueSuffix => System.Configuration.ConfigurationManager.AppSettings.Get(_queueSuffix);

        #endregion
    }
}