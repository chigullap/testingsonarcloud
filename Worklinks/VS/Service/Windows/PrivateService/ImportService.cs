﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceProcess;
using NLog;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessLogic.Central;
using WorkLinks.Service.Windows.PrivateService.Common;
using WorkLinks.Service.Windows.PrivateService.Parameters;

namespace WorkLinks.Service.Windows.PrivateService
{
    partial class ImportService : ServiceBase
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger(typeof(ImportService));

        #region fields
        private BackgroundWorker _importFileWorker;
        private QueueManagement _queueManagement; 
        #endregion

        #region properties
        private BackgroundWorker ImportFileWorker => _importFileWorker ?? (_importFileWorker = new BackgroundWorker());

        private QueueManagement QueueManagement => _queueManagement ?? (_queueManagement = InitializeQueueManagement());

        #endregion

        public ImportService()
        {
            _logger.Info("Initializing event log...");
            InitializeLog();
            _logger.Info("Initializing component...");
            InitializeComponent();
            _logger.Info("Initializing background worker...");
            InitializeBackGroundWorker();
        }

        private QueueManagement InitializeQueueManagement()
        {
            string queueName = QueueIdentifier.IdentifyImportQueue();

            _logger.Info($"Initializing queue management for {queueName}...");

            return new QueueManagement(new DatabaseUser
                {
                    DatabaseName = ApplicationParameter.DatabaseName,
                    UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name,
                    LanguageCode = "EN"
                },
                queueName, 
                ApplicationParameter.MSMQExportQueueName,
                ApplicationParameter.OutputDirectoryPath,
                ApplicationParameter.ImportExportProcessingDirectory,
                ApplicationParameter.DisableNgaResponse,
                ApplicationParameter.NgaHrxmlConfirmationUrl,
                ApplicationParameter.NgaHrxmlConfirmationLogin,
                ApplicationParameter.NgaHrxmlConfirmationPassword,
                ApplicationParameter.HrmlAddressEnabledFlag,
                ApplicationParameter.HrmlBankingEnabledFlag,
                ApplicationParameter.HrmlPayrollBatchEnabledFlag,
                ApplicationParameter.HrxmlBiographicalEnabledFlag,
                ApplicationParameter.HrxmlEmploymentInformationEnabledFlag,
                ApplicationParameter.HrmlPaycodeEnabledFlag,
                ApplicationParameter.HrmlPaycodeEnabledFlag,
                ApplicationParameter.HrmlEmailEnabledFlag,
                ApplicationParameter.HrmlRehireEnabledFlag,
                ApplicationParameter.HrmlStatutoryDeductionEnabledFlag,
                ApplicationParameter.HrmlPhoneEnabledFlag,
                ApplicationParameter.HrmlTerminationEnabledFlag,
                ApplicationParameter.HrxmlLeaveEnabledFlag,
                false,
                _logger
            );
        }

        private void InitializeLog()
        {
            if (!EventLog.SourceExists(ApplicationParameter.EventLogSourceName))
            {
                EventLog.CreateEventSource(ApplicationParameter.EventLogSourceName, ApplicationParameter.EventLogName);
            }
            EventLog.Source = ApplicationParameter.EventLogSourceName;
            EventLog.Log = ApplicationParameter.EventLogName;
        }

        private void InitializeBackGroundWorker()
        {
            ImportFileWorker.WorkerSupportsCancellation = true;
            // Set event handlers
            ImportFileWorker.DoWork += ImportFileWorker_DoWork;
            ImportFileWorker.RunWorkerCompleted += ImportFileWorker_RunWorkerCompleted;
        }

        protected override void OnStart(string[] args)
        {
            EventLog.WriteEntry("Onstart Triggered");
            _logger.Info("Service Start called...starting to monitor import queues");
            ImportFileWorker.RunWorkerAsync();
            _logger.Info("Queue is now being monitored on its own thread");
        }

        protected override void OnStop()
        {
            EventLog.WriteEntry("OnStop Triggered");
            _logger.Info("Service Stop called.");
            ImportFileWorker.CancelAsync();
        }

        #region event handlers
        private void ImportFileWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            _logger.Info("Import file worker thread called.");
            while (!ImportFileWorker.CancellationPending)
            {
                try
                {
                    /*HACK*/
                    System.Threading.Thread.Sleep(30 * 1000);
                    QueueManagement.GetQueueStatus();
                }
                catch (Exception exc)
                {
                    _logger.Error($"Error within the import file worker thread: {exc.Message}");
                    EventLog.WriteEntry(exc.Message);
                    //wait for 10 seconds
                    System.Threading.Thread.Sleep(10 * 1000);
                }
                finally
                {
                    System.Threading.Thread.Sleep(10 * 1000);
                }
            }
        }

        private void ImportFileWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _logger.Info($"Shutting down import worker thread...{(e.Error == null ? "" : "due to an error")}");
            if (e.Error != null)
            {
                _logger.Error(e.Error);
                EventLog.WriteEntry(e.Error.Message, EventLogEntryType.Error);
            }
            else
            {
                EventLog.WriteEntry("Shutting down",EventLogEntryType.Information);
            }
        }
        #endregion
    }
}
