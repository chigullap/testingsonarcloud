﻿using System.ServiceProcess;
using WorkLinks.Service.Windows.PrivateService;

namespace PrivateService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            System.Threading.Thread.Sleep(0);
            ServiceBase[] ServicesToRun = new ServiceBase[] 
            { 
                new ImportService() 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
