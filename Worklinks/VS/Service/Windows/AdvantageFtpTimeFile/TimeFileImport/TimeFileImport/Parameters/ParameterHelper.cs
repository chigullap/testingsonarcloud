﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessLogic;
using WorkLinks.BusinessLayer.BusinessObjects;

namespace TimeFileImport.Parameters
{
    public class ParameterHelper
    {
        //codes
        private const string _advantageTimeFileFtpCode = "ADV_FTP";
        private const string _importExportProcessingDirectory = "IMPEXDIR";
        private const string _importDirtySave = "IMPDS";

        //properties
        public static string AdvantageTimeFileFtp { get { return GetSystemValue(_advantageTimeFileFtpCode); } }
        public static string ImportExportProcessingDirectory { get { return GetSystemValue(_importExportProcessingDirectory); } }
        public static string ImportDirtySave { get { return GetSystemValue(_importDirtySave); } }

        //methods to extract values from code system table
        public static string GetSystemValue(string code)
        {
            return CodeSystemCollection[code].Description;
        }

        //code system table
        private static CodeCollection CodeSystemCollection = (new CodeManagement()).GetCodeTable(ApplicationParameter.CustomerDatabaseName, "EN", CodeTableType.System);
    }
}
