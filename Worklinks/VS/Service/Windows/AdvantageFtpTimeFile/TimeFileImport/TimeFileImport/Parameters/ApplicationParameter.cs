﻿using System;
using System.Configuration;

namespace TimeFileImport.Parameters
{
    public static class ApplicationParameter
    {
        #region from app.config

        private const string _centralDatabaseName = "CentralDatabaseName";
        private const string _customerDatabaseName = "CustomerDatabaseName";
        private const string _filePatternToMatch = "FilePatternToMatch";
        private const string _filePatternToMatchExtension = "FilePatternToMatchExtension";
        

        private const string _importName = "ImportName";
        private const string _folderToMoveProcessedFiles = "FolderToMoveProcessedFiles";

        #endregion

        public static string CentralDatabaseName
        {
            get { return ConfigurationManager.AppSettings.Get(_centralDatabaseName); }
        }

        public static string CustomerDatabaseName
        {
            get { return ConfigurationManager.AppSettings.Get(_customerDatabaseName); }
        }

        public static string FilePatternToMatch
        {
            get { return ConfigurationManager.AppSettings.Get(_filePatternToMatch); }
        }
        public static string FilePatternToMatchExtension
        {
            get { return ConfigurationManager.AppSettings.Get(_filePatternToMatchExtension); }
        }        

        public static string ImportName
        {
            get { return ConfigurationManager.AppSettings.Get(_importName); }
        }

        public static string FolderToMoveProcessedFiles
        {
            get { return ConfigurationManager.AppSettings.Get(_folderToMoveProcessedFiles); }
        }
    }
}
