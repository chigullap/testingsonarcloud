﻿using System;
using TimeFileImport.Parameters;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessLogic;

namespace TimeFileImport
{
    class Program
    {
        private const string _userName = "wl_advantage_timefile_engine";
        private const string _languageCode = "EN";

        static void Main(string[] args)
        {
            Console.WriteLine("Starting to import time files");

            //set dbuser to use the customer db
            DatabaseUser user = new DatabaseUser() { DatabaseName = ApplicationParameter.CustomerDatabaseName, UserName = _userName, LanguageCode = _languageCode };

            try
            {
                Console.WriteLine("Checking if there are any new time files.  If so, they will be imported then moved when completed.");

                //export to SFTP
                new XmlManagement().CopyImportMoveAdvantageTimeFiles
                    (
                        user,
                        ParameterHelper.AdvantageTimeFileFtp,
                        ApplicationParameter.FilePatternToMatch,
                        ApplicationParameter.FilePatternToMatchExtension,
                        ApplicationParameter.ImportName,
                        ParameterHelper.ImportExportProcessingDirectory,
                        Convert.ToBoolean(ParameterHelper.ImportDirtySave),
                        ApplicationParameter.FolderToMoveProcessedFiles
                    );

                Console.WriteLine("Done importing and moving files");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception about to be thrown");
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }
    }
}
