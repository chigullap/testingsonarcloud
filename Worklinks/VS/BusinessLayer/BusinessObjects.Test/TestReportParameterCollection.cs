﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Test
{
    [Serializable]
    [CollectionDataContract]
    public class TestReportParameterCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<TestReportParameter>
    {

    }
}
