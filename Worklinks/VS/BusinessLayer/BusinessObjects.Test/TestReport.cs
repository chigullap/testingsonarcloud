﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Test
{
    [Serializable]
    [DataContract]
    public class TestReport: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long TestReportId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String TestName { get; set; }
        [DataMember]
        public String Description { get; set; }
        [DataMember]
        public String ReportName { get; set; }
        [DataMember]
        public String ReportType { get; set; }
        [DataMember]
        public String OverrideReportPath { get; set; }

        #endregion

        #region construct/destruct
        public TestReport()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(TestReport data)
        {
            base.CopyTo(data);

            data.TestReportId = TestReportId;
            data.TestName = TestName;
            data.Description = Description;
            data.ReportName = ReportName;
            data.ReportType = ReportType;
            data.OverrideReportPath = OverrideReportPath;
        }
        public new void Clear()
        {
            base.Clear();

            TestReportId = -1;
            TestName = "";
            Description = "";
            ReportName = "";
            ReportType = "";
            OverrideReportPath = "";
        }

        #endregion

    }
}
