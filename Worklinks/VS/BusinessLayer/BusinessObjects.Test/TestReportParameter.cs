﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Test
{
    [Serializable]
    [DataContract]
    public class TestReportParameter: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long TestReportParameterId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long TestReportId { get; set; }
        [DataMember]
        public String ParameterName { get; set; }
        [DataMember]
        public String Value { get; set; }

        #endregion

        #region construct/destruct
        public TestReportParameter()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(TestReportParameter data)
        {
            base.CopyTo(data);

            data.TestReportParameterId = TestReportParameterId;
            data.TestReportId = TestReportId;
            data.ParameterName = ParameterName;
            data.Value = Value;
        }
        public new void Clear()
        {
            base.Clear();

            TestReportParameterId = -1;
            TestReportId = -1;
            ParameterName = "";
            Value = "";
        }

        #endregion

    }
}
