﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Test
{
    [Serializable]
    [DataContract]
    public class TestReportExpectedValue: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long TestReportExpectedValueId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long TestReportId { get; set; }
        [DataMember]
        public String Description { get; set; }
        [DataMember]
        public String Datatype { get; set; }
        [DataMember]
        public Int32 RowNumber { get; set; }
        [DataMember]
        public Int32 ColumnNumber { get; set; }
        [DataMember]
        public String ExpectedValue { get; set; }
        #endregion

        #region construct/destruct
        public TestReportExpectedValue()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(TestReportExpectedValue data)
        {
            base.CopyTo(data);

            data.TestReportExpectedValueId = TestReportExpectedValueId;
            data.TestReportId = TestReportId;
            data.Description = Description;
            data.Datatype = Datatype;
            data.RowNumber = RowNumber;
            data.ColumnNumber = ColumnNumber;
            data.ExpectedValue = ExpectedValue;
        }
        public new void Clear()
        {
            base.Clear();

            TestReportExpectedValueId = -1;
            TestReportId = -1;
            Description = "";
            Datatype = "";
            RowNumber = 0;
            ColumnNumber = 0;
            ExpectedValue = "";
        }

        #endregion

    }
}
