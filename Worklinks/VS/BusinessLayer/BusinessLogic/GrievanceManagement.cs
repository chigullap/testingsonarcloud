﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.DataLayer.DataAccess;
using WorkLinks.DataLayer.DataAccess.SqlServer;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class GrievanceManagement
    {
        //used for cleaning grievance references
        private const String GrievanceFiledByCode_Other = "OTH";
        private const String GrievanceFiledByCode_Employee = "EMP";
        private const String GrievanceFiledByCode_Union = "UNN";

        //add data access
        GrievanceAccess _grievanceAccess = new GrievanceAccess();

        #region Grievance
        /// <summary>
        /// get Grievance
        /// </summary>
        public GrievanceCollection GetGrievance(DatabaseUser user, long id)
        {
            try
            {
                return _grievanceAccess.GetGrievance(user, id);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "GrievanceManagement - GetGrievance", ex);
                throw;
            }
        }

        public void UpdateGrievance(DatabaseUser user, Grievance grievance)
        {
            try
            {
                PrepareGrievance(user, grievance);
                _grievanceAccess.UpdateGrievance(user, grievance);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "GrievanceManagement - UpdateGrievance", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                {
                    EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                    throw new System.ServiceModel.FaultException<EmployeeServiceException>(concurrencyException, new System.ServiceModel.FaultReason(ex.Message));
                }
                else
                    throw;
            }
        }

        public void InsertGrievance(DatabaseUser user, Grievance grievance)
        {
            try
            {
                PrepareGrievance(user, grievance);
                _grievanceAccess.InsertGrievance(user, grievance);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "GrievanceManagement - InsertGrievance", ex);
                throw;
            }
        }

        public void DeleteGrievance(DatabaseUser user, Grievance grievance)
        {
            try
            {
                _grievanceAccess.DeleteGrievance(user, grievance);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "GrievanceManagement - DeleteGrievance", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                {
                    EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                    throw new System.ServiceModel.FaultException<EmployeeServiceException>(foreignKeyException, new System.ServiceModel.FaultReason(ex.Message));
                }
                else
                    throw;
            }
        }

        private void PrepareGrievance(DatabaseUser user, Grievance grievance)
        {
            try
            {
                if (!grievance.GrievanceFiledByCode.Equals(GrievanceFiledByCode_Other)) grievance.Reference = null;
                if (!grievance.GrievanceFiledByCode.Equals(GrievanceFiledByCode_Employee)) grievance.ReferenceEmployeeId = null;
                if (!grievance.GrievanceFiledByCode.Equals(GrievanceFiledByCode_Union)) grievance.ReferenceLabourUnionId = null;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "GrievanceManagement - PrepareGrievance", ex);
                throw;
            }
        }
        #endregion

        #region Grievance action
        public GrievanceActionCollection GetGrievanceAction(DatabaseUser user, long grievanceId)
        {
            try
            {
                return _grievanceAccess.GetGrievanceAction(user, grievanceId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "GrievanceManagement - GetGrievanceAction", ex);
                throw;
            }
        }

        public void UpdateGrievanceAction(DatabaseUser user, GrievanceAction data)
        {
            try
            {
                _grievanceAccess.UpdateGrievanceAction(user, data);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "GrievanceManagement - UpdateGrievanceAction", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                {
                    EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                    throw new System.ServiceModel.FaultException<EmployeeServiceException>(concurrencyException, new System.ServiceModel.FaultReason(ex.Message));
                }
                else
                    throw;
            }
        }

        public void InsertGrievanceAction(DatabaseUser user, GrievanceAction data)
        {
            try
            {
                _grievanceAccess.InsertGrievanceAction(user, data);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "GrievanceManagement - InsertGrievanceAction", ex);
                throw;
            }
        }

        public void DeleteGrievanceAction(DatabaseUser user, GrievanceAction data)
        {
            try
            {
                _grievanceAccess.DeleteGrievanceAction(user, data);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "GrievanceManagement - DeleteGrievanceAction", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                {
                    EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                    throw new System.ServiceModel.FaultException<EmployeeServiceException>(foreignKeyException, new System.ServiceModel.FaultReason(ex.Message));
                }
                else
                    throw;
            }
        }
        #endregion

        #region Grievance reports
        public GrievanceCollection GetGrievanceSummary(DatabaseUser user, GrievanceCriteria criteria)
        {
            try
            {
                GrievanceCollection Grievances = _grievanceAccess.GetGrievanceSummary(user, criteria);
                return Grievances;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "GrievanceManagement - GetGrievanceSummary", ex);
                throw;
            }
        }
        #endregion
    }
}