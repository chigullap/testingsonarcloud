﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Transactions;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.DataLayer.DataAccess;
using WorkLinks.DataLayer.DataAccess.SqlServer;
using System.Web.Security;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class SecurityManagement
    {
        #region fields
        int _saltByteSize = 128;
        #endregion

        //add data access
        SecurityAccess _securityAccess = new SecurityAccess();
        EmployeeAccess _empAccess = new EmployeeAccess();

        #region security user
        public bool ValidateUser(String databaseName, String userName, String password)
        {
            //is this user in our db?
            SecurityUser securityUser = GetUser(databaseName, userName);

            return securityUser != null && !securityUser.IsLockedOutFlag && GetPasswordHash(password, securityUser.PasswordSalt).Equals(securityUser.Password);
        }

        private SecurityUser GetUser(String databaseName, String userName)
        {
            SecurityUserCollection users = _securityAccess.GetSecurityUser(databaseName, userName);

            if (users.Count == 1)
                return users[0];
            else
                return null;
        }

        public bool DoesSecurityUserAlreadyExist(DatabaseUser user, long employeeId)
        {
            SecurityUserCollection users = _securityAccess.DoesSecurityUserAlreadyExist(user, employeeId);

            if (users.Count > 0)
                return true;
            else
                return false;
        }

        public bool ValidateUserName(DatabaseUser user, String username)
        {
            SecurityUserCollection users = _securityAccess.GetSecurityUser(user.DatabaseName, username);

            if (users.Count > 0)
                return false; //not valid as username is in use
            else
                return true;
        }

        public bool ValidateUserWithHistory(String databaseName, SecurityUserLoginHistory history, string password, int passwordAttemptWindow, int maxInvalidPasswordAttempts, String loginExpiryDays)
        {
            //is this user in our db?
            SecurityUser securityUser = GetUser(databaseName, history.UserName);

            //exclude admin from password/login expiry rules.
            if (securityUser != null && securityUser.UserName.ToLower() != "redisland")
            {
                //check for login expiry, if expired, set IsLockedOutFlag to true, and logic will execute in the ELSE statement.
                if (securityUser != null && securityUser.LoginExpiryDatetime != null && securityUser.LoginExpiryDatetime <= DateTime.Now)
                {
                    securityUser.IsLockedOutFlag = true;
                    securityUser.LastLockoutDatetime = DateTime.Now;
                }
            }

            if (securityUser != null && !securityUser.IsLockedOutFlag && GetPasswordHash(password, securityUser.PasswordSalt).Equals(securityUser.Password))
            {
                history.AuthenticatedFlag = true;

                //update the db with a successful login attempt.
                securityUser.LastFailedPasswordAttemptCount = 0;
                securityUser.LastLoginDate = System.DateTime.Now;
                securityUser.LoginExpiryDatetime = DateTime.Now.AddDays(Convert.ToInt16(loginExpiryDays));

                _securityAccess.UpdateSecurityUserLoginInfo(databaseName, securityUser);
            }
            else
            {
                if (securityUser != null)//username exists, locked or wrong password
                {
                    if (!securityUser.IsLockedOutFlag)
                    {
                        //update the db with a login failed attempt.
                        securityUser.LastFailedPasswordAttemptDatetime = System.DateTime.Now;
                        securityUser.LastFailedPasswordAttemptCount++;
                        SetLock(databaseName, securityUser, passwordAttemptWindow, maxInvalidPasswordAttempts);
                    }
                    _securityAccess.UpdateSecurityUserLoginInfo(databaseName, securityUser);
                }
                //ELSE...do nothing, wrong user name
            }

            _securityAccess.InsertSecurityUserLoginHistory(databaseName, history);

            return history.AuthenticatedFlag;
        }

        private void SetLock(String databaseName, SecurityUser securityUser, int passwordAttemptWindow, int maxInvalidPasswordAttempts)
        {
            SecurityUserLoginHistoryCollection history = _securityAccess.GetSecurityUserLoginHistory(databaseName, securityUser.UserName, passwordAttemptWindow);

            if ((history.Count + 1) >= maxInvalidPasswordAttempts || securityUser.LastFailedPasswordAttemptCount >= maxInvalidPasswordAttempts)
            {
                securityUser.IsLockedOutFlag = true;
                securityUser.LastLockoutDatetime = DateTime.Now;
            }
        }

        public bool ValidatePasswordHistory(DatabaseUser user, string userName, string password)
        {
            //get password history for the user
            SecurityUserPasswordHistoryCollection coll = _securityAccess.GetPasswordHistory(user, userName);
            bool bPrevUsed = false;

            if (coll != null && coll.Count > 0) //password history exists for this user
            {
                //for each object in the password history for the user:  get the history salt and hash it with the new password and see if it equals the history password entry
                foreach (SecurityUserPasswordHistory obj in coll)
                {
                    if (GetPasswordHash(password, obj.PasswordSalt) == obj.Password)
                    {
                        bPrevUsed = true;
                        break;
                    }
                }

                //If password was previously used then bPrevUsed is true so return false for validation control so it will not allow the user to continue.
                return !bPrevUsed;
            }
            else
                return true; //no password history so they can use the current password entry
        }

        //public void UpdateLogoutUser(DatabaseUser user, string userName)
        //{
        //    try
        //    {
        //        _securityAccess.UpdateLogoutUser(user, userName);
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorManagement.HandleError(user, "SecurityManagement - UpdateLogoutUser", ex);

        //        if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
        //        {
        //            EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
        //            throw new System.ServiceModel.FaultException<EmployeeServiceException>(concurrencyException, new System.ServiceModel.FaultReason(ex.Message));
        //        }
        //        else
        //            throw;
        //    }
        //}

        public SecurityUserCollection GetSecurityUser(String databaseName, String userName)
        {
            return _securityAccess.GetSecurityUser(databaseName, userName);
        }

        public SecurityUser InsertUser(DatabaseUser user, SecurityUser securityUser, String authDatabaseName, String ngSecurityClientId, String loginExpiryHours, String ngSecurity, String _ngDbSecurity, String _ngApiSecurity)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                if (securityUser.IsEmployee)//only insert needed is into SECURITY_USER as EMPLOYEE and PERSON tables would already be populated.
                {
                    //get the person_id
                    GetPersonIdFromEmployee(user, securityUser);

                    //hash the password
                    securityUser.PasswordSalt = CreateSalt();
                    securityUser.Password = GetPasswordHash(securityUser.PasswordEntry, securityUser.PasswordSalt);
                    securityUser.LoginExpiryDatetime = DateTime.Now.AddHours(Convert.ToInt16(loginExpiryHours));

                    //insert signature if one exists
                    if (securityUser.Signature != null)
                    {
                        if (securityUser.AttachmentObject.AttachmentId == -1)
                            _empAccess.InsertAttachment(user, securityUser.AttachmentObject);
                        else
                            _empAccess.UpdateAttachment(user, securityUser.AttachmentObject);

                        securityUser.AttachmentId = securityUser.AttachmentObject.AttachmentId;
                    }

                    //check if already exists on AUTH.  If so, no insert needed.
                    //if (!_securityAccess.DoesUserExistOnAuth(authDatabaseName, securityUser.UserName))
                    //{
                    //    //copy SECURITY_USER
                    //    SecurityUser centralUser = new SecurityUser();
                    //    securityUser.CopyTo(centralUser);

                    //    //auth db
                    //    //get person names from worklinks
                    //    GetPersonNames(user, centralUser);

                    //    //insert into PERSON table and capture the person_id
                    //    centralUser.PersonId = InsertPerson(user, authDatabaseName, centralUser);
                    //    _securityAccess.InsertSecurityUser(user, authDatabaseName, centralUser);
                    //}                       

                    //client db
                    _securityAccess.InsertSecurityUser(user, user.DatabaseName, securityUser);
                }
                else //not an employee, so insert into PERSON, SECURITY_USER, PERSON_CONTACT_CHANNEL, and CONTACT_CHANNEL tables
                {
                    //insert into SECURITY_USER table
                    securityUser.PasswordSalt = CreateSalt();
                    securityUser.Password = GetPasswordHash(securityUser.PasswordEntry, securityUser.PasswordSalt);
                    securityUser.LoginExpiryDatetime = DateTime.Now.AddHours(Convert.ToInt16(loginExpiryHours));

                    //insert signature if one exists
                    if (securityUser.Signature != null)
                    {
                        if (securityUser.AttachmentObject.AttachmentId == -1)
                            _empAccess.InsertAttachment(user, securityUser.AttachmentObject);
                        else
                            _empAccess.UpdateAttachment(user, securityUser.AttachmentObject);

                        securityUser.AttachmentId = securityUser.AttachmentObject.AttachmentId;
                    }

                    //check if already exists on AUTH.  If so, no insert needed.
                    if (!_securityAccess.DoesUserExistOnAuth(authDatabaseName, securityUser.UserName))
                    {
                        //    SecurityUser centralUser = new SecurityUser();
                        //    securityUser.CopyTo(centralUser);

                        //    centralUser.PersonId = InsertPerson(user, authDatabaseName, centralUser);
                        //    _securityAccess.InsertSecurityUser(user, authDatabaseName, centralUser);

                        //insert into PERSON table and capture the person_id
                        securityUser.PersonId = InsertPerson(user, user.DatabaseName, securityUser);
                        _securityAccess.InsertSecurityUser(user, user.DatabaseName, securityUser);
                    }

                
                }

                ////add security attributes to auth
                ////add angular access
                //_securityAccess.InsertSecurityUserClaim(user, securityUser, authDatabaseName, ngSecurityClientId, "NG", ngSecurity.ToLower());
                ////add database access
                //_securityAccess.InsertSecurityUserClaim(user, securityUser, authDatabaseName, ngSecurityClientId, "DB", _ngDbSecurity.ToLower());
                ////add api access
                //_securityAccess.InsertSecurityUserClaim(user, securityUser, authDatabaseName, ngSecurityClientId, "WEBAPI", _ngApiSecurity.ToLower());

                scope.Complete();

                return securityUser;

            }

        }

        private void GetPersonNames(DatabaseUser user, SecurityUser centralUser)
        {
            SecurityUser tmp = _securityAccess.GetSecurityUserPerson(user, Convert.ToInt64(centralUser.PersonId))[0];
            centralUser.FirstName = tmp.FirstName;
            centralUser.LastName = tmp.LastName;            
        }

        public void UpdateSecurityUser(DatabaseUser user, string username, string newPassword, SecurityUser securityUser, string passwordExpiryDays, string loginExpiryHours, string adminDatabaseName, bool onlyUpdateLocalDbForLockedOutFlag)
        {
            try
            {
                //if user is an employee and password reset has been requested, only perform a password update, otherwise nothing has changed
                if (securityUser.IsEmployee && newPassword != null && newPassword.Length > 0)
                {
                    //password reset, make password expire asap to force user to change what admin entered
                    securityUser.PasswordExpiryDatetime = DateTime.Now;
                    UpdateSecurityUserPassword(user, username, newPassword, securityUser, true, passwordExpiryDays, loginExpiryHours, adminDatabaseName);
                }
                else
                {
                    //non-employee has more updatable fields
                    if (newPassword != null && newPassword.Length > 0) //see if this was a password change request or not.
                    {
                        //password reset, make password expire asap to force user to change what admin entered
                        securityUser.PasswordExpiryDatetime = DateTime.Now;
                        UpdateSecurityUserPassword(user, username, newPassword, securityUser, true, passwordExpiryDays, loginExpiryHours, adminDatabaseName);
                    }

                    //update FirstName and LastName, if necessary
                    UpdatePersonsFirstAndLastName(user, securityUser, adminDatabaseName);
                }

                //check if admin is unlocking the user
                SecurityUser tempUser = GetUser(user.DatabaseName, username);

                if (tempUser != null) //new users without a group
                {
                    if (securityUser.IsLockedOutFlag == false && tempUser.IsLockedOutFlag == true)
                        securityUser.LoginExpiryDatetime = DateTime.Now.AddHours(Convert.ToInt16(loginExpiryHours));
                }

                //insert signature if one exists
                if (securityUser.Signature != null)
                {
                    if (securityUser.AttachmentObject.AttachmentId == -1)
                        _empAccess.InsertAttachment(user, securityUser.AttachmentObject);
                    else
                        _empAccess.UpdateAttachment(user, securityUser.AttachmentObject);

                    securityUser.AttachmentId = securityUser.AttachmentObject.AttachmentId;
                }

                SecurityUser centralUser = new SecurityUser();
                securityUser.CopyTo(centralUser);
                centralUser.SecurityUserId = -1;

                //employee or not, update the "IsLockedOutFlag" and "WorklinksAdministrationFlag" and attachment if applicable
                UpdateSecurityUser(user, user.DatabaseName, securityUser);

                //if the user exists on more than one client on the auth db, lockout should only be applied to the current database, not the auth database.
                if (onlyUpdateLocalDbForLockedOutFlag && centralUser.IsLockedOutFlag)
                    centralUser.IsLockedOutFlag = false;

                UpdateSecurityUser(user, adminDatabaseName, centralUser);

            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "SecurityManagement - UpdateSecurityUser", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                {
                    EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                    throw new System.ServiceModel.FaultException<EmployeeServiceException>(concurrencyException, new System.ServiceModel.FaultReason(ex.Message));
                }
                else
                    throw;
            }
        }

        private void UpdateSecurityUser(DatabaseUser user, string overrideDatabaseName, SecurityUser securityUser)
        {
            _securityAccess.UpdateSecurityUser(user, overrideDatabaseName, securityUser);
        }

        private void UpdatePersonsEmail(DatabaseUser user, SecurityUser securityUser)
        {
            if (securityUser.ContactChannelId != null)
            {
                //load a contact_channel object
                PersonManagement tempPersManagement = new PersonManagement();
                ContactChannelCollection coll = tempPersManagement.GetContactChannel(user, securityUser.ContactChannelId);

                //update the email and updateuser
                coll[0].PrimaryContactValue = securityUser.Email;
                coll[0].UpdateUser = securityUser.UpdateUser;

                //update the object
                tempPersManagement.UpdateContactChannel(user, coll[0]);
            }
            else
            {
                //insert into CONTACT_CHANNEL and PERSON_CONTACT_CHANNEL
                securityUser.ContactChannelId = InsertContactChannelEmail(user, securityUser);
            }
        }

        private void UpdatePersonsFirstAndLastName(DatabaseUser user, SecurityUser securityUser, string adminDatabaseName)
        {
            //get the PERSON database record and update FirstName and LastName if they are different
            PersonManagement tempPersManagement = new PersonManagement();
            PersonCollection tempPerson = tempPersManagement.GetPerson(user, Convert.ToInt64(securityUser.PersonId));

            //if there is a person record and either name is different, perform an update
            if (tempPerson.Count == 1 && (tempPerson[0].LastName != securityUser.LastName || tempPerson[0].FirstName != securityUser.FirstName))
            {
                //set params
                tempPerson[0].LastName = securityUser.LastName;
                tempPerson[0].FirstName = securityUser.FirstName;
                //call update
                tempPersManagement.UpdatePerson(user, tempPerson[0]);

                //update auth
                //tempPersManagement.UpdateAuthPerson(user, adminDatabaseName, securityUser.UserName, tempPerson[0]); //yuc
            }
        }

        public void UpdateSecurityUserPassword(DatabaseUser user, String username, String newPassword, SecurityUser securityUser, bool adminUpdatingPassword, String passwordExpiryDays, String loginExpiryHours, string adminDatabaseName)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                //add the old password to the history table
                SecurityUserPasswordHistory passHistory = CopySecurityUserToPasswordHistory(securityUser);
                _securityAccess.InsertSecurityUserPasswordHistory(user, passHistory);

                //get new Salt
                securityUser.PasswordSalt = CreateSalt();

                //hash new password with Salt
                securityUser.Password = GetPasswordHash(newPassword, securityUser.PasswordSalt);

                //set password expiry when user changes their password (not when admin resets it for them, as that password expires right away)
                if (!adminUpdatingPassword)
                    securityUser.PasswordExpiryDatetime = DateTime.Now.AddDays(Convert.ToInt16(passwordExpiryDays));
                else
                    //if admin reset the password, set the number of hours the user has to log in before account is locked
                    securityUser.LoginExpiryDatetime = DateTime.Now.AddHours(Convert.ToInt16(loginExpiryHours));

                //update the db with new info
                _securityAccess.UpdateSecurityUserPassword(user, adminDatabaseName, username, securityUser);

                scope.Complete();
            }
        }
        #endregion

        #region security categories
        public SecurityCategoryCollection GetSecurityCategories(DatabaseUser user, bool isViewMode)
        {
            return _securityAccess.GetSecurityCategories(user, isViewMode);
        }

        public void UpdateCategories(DatabaseUser user, SecurityCategoryCollection securityCategoryColl)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (SecurityCategory category in securityCategoryColl)
                        _securityAccess.UpdateCategories(user, category);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "SecurityManagement - UpdateCategories", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                {
                    EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                    throw new System.ServiceModel.FaultException<EmployeeServiceException>(concurrencyException, new System.ServiceModel.FaultReason(ex.Message));
                }
                else
                    throw;
            }
        }

        public void RebuildSecurity(DatabaseUser user)
        {
            _securityAccess.RebuildSecurity(user);
        }
        #endregion

        #region user admin

        public int SelectSecurityClientUser(DatabaseUser user, string authDatabaseName, long securityUserId)
        {
            return _securityAccess.SelectSecurityClientUser(user, authDatabaseName, securityUserId);
        }

        public UserSummaryCollection GetUserSummary(DatabaseUser user, UserAdminSearchCriteria criteria)
        {
            return _securityAccess.GetUserSummary(user, criteria);
        }

        public void DeleteUser(DatabaseUser user, long personId)
        {
            try
            {
                _securityAccess.DeleteUser(user, personId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "SecurityManagement - DeleteUser", ex);
                throw;
            }
        }

        public SecurityUserCollection GetPersonUserDetails(DatabaseUser user, long personId)
        {
            SecurityUserCollection securityUserPerson = new SecurityUserCollection();

            //fill the SecurityUser object based on personId
            securityUserPerson = _securityAccess.GetSecurityUserPerson(user, personId);

            //check to see if the person an employee, if so, fill in properties
            GetEmployeeDetailsByPersonId(user, securityUserPerson[0], personId);

            //attach their signature if they have one
            foreach (SecurityUser secUser in securityUserPerson)
            {
                if (secUser.AttachmentId != null)
                    secUser.AttachmentObject = _empAccess.GetAttachment(user, Convert.ToInt64(secUser.AttachmentId))[0];
            }

            return securityUserPerson;
        }

        private SecurityUser GetEmployeeDetailsByPersonId(DatabaseUser user, SecurityUser tempUser, long personId)
        {
            EmployeeCollection tempEmp = new EmployeeCollection();
            tempEmp = _empAccess.GetEmployeeByPersonId(user, personId);

            if (tempEmp.Count > 0)
            {
                tempUser.IsEmployee = true;
                tempUser.EmployeeId = tempEmp[0].EmployeeId;
            }
            else
            {
                tempUser.IsEmployee = false;
                tempUser.EmployeeId = 0;
            }
            return tempUser;
        }
        #endregion

        #region security marking
        public SecurityMarkingCollection GetSecurityMarking(DatabaseUser user, int hierarchicalSortOrder)
        {
            return _securityAccess.GetSecurityMarking(user, hierarchicalSortOrder);
        }
        #endregion

        #region security label role
        public SecurityLabelRoleCollection GetSecurityLabelRole(DatabaseUser user, long securityRoleId)
        {
            return _securityAccess.GetSecurityLabelRole(user, securityRoleId);
        }

        public void UpdateSecurityLabelRole(DatabaseUser user, long roleId, SecurityLabelRoleCollection labels)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //delete what is there
                    _securityAccess.DeleteSecurityLabelRole(user, roleId);

                    //insert new rows
                    foreach (SecurityLabelRole label in labels)
                        _securityAccess.InsertSecurityLabelRole(user, label);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "SecurityManagement - UpdateSecurityLabelRole", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                {
                    EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                    throw new System.ServiceModel.FaultException<EmployeeServiceException>(concurrencyException, new System.ServiceModel.FaultReason(ex.Message));
                }
                else
                    throw;
            }
        }
        #endregion

        #region Role Description
        public RoleDescriptionCollection GetRoleDescriptions(DatabaseUser user, String criteria, long? roleId, bool? worklinksAdministrationFlag)
        {
            return _securityAccess.GetRoleDescriptions(user, criteria, roleId, worklinksAdministrationFlag);
        }

        public void UpdateRoleDesc(DatabaseUser user, String authDatabaseName, String ngSecurityClientId, RoleDescription roleDesc)
        {
            try
            {
                _securityAccess.UpdateRoleDesc(user, roleDesc);
               // _securityAccess.UpdateSecurityClientRoleDescription(user, authDatabaseName, ngSecurityClientId, roleDesc);//yuc
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "SecurityManagement - UpdateRoleDesc", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                {
                    EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                    throw new System.ServiceModel.FaultException<EmployeeServiceException>(concurrencyException, new System.ServiceModel.FaultReason(ex.Message));
                }
                else
                    throw;
            }
        }

        public RoleDescription InsertRoleDescription(DatabaseUser user, String authDatabaseName, String ngSecurityClientId, RoleDescription roleDesc)
        {
            _securityAccess.InsertRoleDescription(user, roleDesc);
            //_securityAccess.InsertSecurityClientRoleDescription(user, authDatabaseName, ngSecurityClientId, roleDesc);//yuc

            return roleDesc;
        }
        #endregion

        #region Group Description
        public GroupDescriptionCollection GetUserGroups(DatabaseUser user, long securityUserId, bool isViewMode, bool? worklinksAdministrationFlag)
        {
            return _securityAccess.GetUserGroups(user, securityUserId, isViewMode, worklinksAdministrationFlag);
        }

        public GroupDescriptionCollection GetGroupDescriptions(DatabaseUser user, String criteria, long? groupId, bool? worklinksAdministrationFlag)
        {
            return _securityAccess.GetGroupDescriptions(user, criteria, groupId, worklinksAdministrationFlag);
        }

        public void UpdateGroupDesc(DatabaseUser user, string authDatabaseName, string ngSecurityClientId, GroupDescription grpDesc)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _securityAccess.UpdateGroupDesc(user, grpDesc);
                    //_securityAccess.UpdateSecurityClientRoleDescription(user, authDatabaseName, ngSecurityClientId, grpDesc);
                    //_securityAccess.UpdateSecurityClientRoleHierarchy(user, authDatabaseName, ngSecurityClientId, grpDesc);
                    FormSecurityCollection dbforms = new FormSecurityCollection();
                    dbforms.Add(new FormSecurity() { SecurityRoleFormId = grpDesc.DatabaseSecurityRoleId, ViewFlag = true , /**HACK**/FormId= grpDesc.DatabaseSecurityRoleId.Value });

                    //_securityAccess.UpdateSecurityClienRoleClaim(user, authDatabaseName, ngSecurityClientId, grpDesc.DatabaseSecurityRoleId.Value, "DBID", dbforms);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "SecurityManagement - UpdateGroupDesc", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                {
                    EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                    throw new System.ServiceModel.FaultException<EmployeeServiceException>(concurrencyException, new System.ServiceModel.FaultReason(ex.Message));
                }
                else
                    throw;
            }
        }


        public GroupDescription InsertGroupDescription(DatabaseUser user, String authDatabaseName, String ngSecurityClientId, GroupDescription grpDesc)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                _securityAccess.InsertGroupDescription(user, grpDesc);
                _securityAccess.InsertSecurityClientRoleDescription(user, authDatabaseName, ngSecurityClientId, grpDesc);
                _securityAccess.UpdateSecurityClientRoleHierarchy(user, authDatabaseName, ngSecurityClientId, grpDesc);

                scope.Complete();
            }

            return grpDesc;
        }

        public GroupDescriptionCollection UpdateUserGroups(DatabaseUser user, string authDatabaseName, string ngSecurityClientId, string userName, long securityUserId, GroupDescriptionCollection collection)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //decide which action should take place (insert/delete) based on the property "IsGroupSelected" and "SecurityUserSecurityRoleId"
                    for (int i = 0; i < collection.Count; i++)
                    {
                        //if group is selected to be added and it wasn't assigned when the page loaded, then add this group to the user
                        if (collection[i].IsGroupSelected && collection[i].SecurityUserSecurityRoleId == null)
                            _securityAccess.InsertUserGroup(user, collection[i], securityUserId);
                        //if group is not selected to be added and it was assigned when the page loaded, then delete this group from the user
                        else if (!collection[i].IsGroupSelected && collection[i].SecurityUserSecurityRoleId != null)
                            _securityAccess.DeleteUserGroup(user, collection[i], securityUserId);
                        //other cases in which the group is selected and the user was already assigned / group was not selected and user wasn't assigned it - require no action.
                    }
                    //UpdateSecurityClientUserRoles(user, authDatabaseName, ngSecurityClientId,  userName, securityUserId, collection); //yuc

                    scope.Complete();
                }
                return collection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "SecurityManagement - UpdateUserGroups", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                {
                    EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                    throw new System.ServiceModel.FaultException<EmployeeServiceException>(concurrencyException, new System.ServiceModel.FaultReason(ex.Message));
                }
                else
                    throw;
            }
        }

        public void UpdateSecurityClientUserRoles(DatabaseUser user, string authDatabaseName, string ngSecurityClientId, string userName, long securityUserId, GroupDescriptionCollection collection)
        {
            List<long> groupList = new List<long>(); 

            foreach (GroupDescription item in collection)
            {
                if (item.IsGroupSelected)
                    groupList.Add(item.GroupId);
            }
            string groupCsv = String.Join(",", groupList.ToArray());

            _securityAccess.UpdateSecurityClientUserRoles(user, authDatabaseName, ngSecurityClientId, userName, securityUserId, groupCsv);
        }

        private SecurityRole FillSecurityRoleObject(DatabaseUser user, SecurityRole temp, String roleTypeCode)
        {
            //set the correct properties for inserting into security role table for a group
            temp.CodeSecurityRoleTypeCd = roleTypeCode;
            temp.SecurityApplicationId = 1; //always "1" for the Worklinks app

            return temp;
        }
        #endregion

        #region Form Security
        public FormSecurityCollection GetFormSecurityInfo(DatabaseUser user, long roleId, bool? worklinksAdministrationFlag)
        {
            return _securityAccess.GetFormSecurityInfo(user, roleId, worklinksAdministrationFlag);
        }

        public FormSecurityCollection UpdateFormSecurity(DatabaseUser user, String authDatabaseName, String ngSecurityClientId, FormSecurityCollection collection, long roleId)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //decide which action should take place (insert/delete) based on the property "IsFormSelected" and "SecurityRoleFormId"
                    for (int i = 0; i < collection.Count; i++)
                    {
                        //if form has some flags selected and the form wasn't assigned anything when the page loaded, then add this form and the chosen options to security_role_form
                        if (collection[i].SecurityRoleFormId < 0 && (collection[i].ViewFlag == true || collection[i].AddFlag == true || collection[i].UpdateFlag == true || collection[i].DeleteFlag == true))
                            _securityAccess.InsertSecurityRoleForm(user, collection[i], roleId);

                        //in cases in which the form has options and the form already existed in the database, perform an update
                        else if (collection[i].SecurityRoleFormId > 0 && (collection[i].ViewFlag == true || collection[i].AddFlag == true || collection[i].UpdateFlag == true || collection[i].DeleteFlag == true))
                            _securityAccess.UpdateSecurityRoleForm(user, collection[i]);

                        //if form has no flags selected and the form was assigned options when the page loaded, then delete this form from security_role_form
                        else if (collection[i].SecurityRoleFormId > 0 && collection[i].ViewFlag == false && collection[i].AddFlag == false && collection[i].UpdateFlag == false && collection[i].DeleteFlag == false)
                            _securityAccess.DeleteSecurityRoleForm(user, collection[i], roleId);
                        //if form has no flags selected and the form was not assigned options when the page loaded, then do nothing
                    }
                    //_securityAccess.UpdateSecurityClienRoleClaim(user, authDatabaseName, ngSecurityClientId, roleId, "API", collection);//yuc
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "SecurityManagement - UpdateFormSecurity", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                {
                    EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                    throw new System.ServiceModel.FaultException<EmployeeServiceException>(concurrencyException, new System.ServiceModel.FaultReason(ex.Message));
                }
                else
                    throw;
            }
            return collection;
        }
        #endregion

        #region SecurityUserLoginHistory
        public SecurityUserLoginHistoryCollection GetSecurityUserLoginHistory(String databaseName, String userName, int passwordAttemptWindow)
        {
            return _securityAccess.GetSecurityUserLoginHistory(databaseName, userName, passwordAttemptWindow);
        }

        public void InsertSecurityUserLoginHistory(String databaseName, SecurityUserLoginHistory item)
        {
            _securityAccess.InsertSecurityUserLoginHistory(databaseName, item);
        }
        #endregion

        #region helper methods
        private string CreateSalt()
        {
            // generate a 128-bit salt using a secure PRNG
            byte[] salt = new byte[128];
            using (RandomNumberGenerator rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            return System.Text.Encoding.Default.GetString(salt);
        }

        public string GetPasswordHash(string password, string salt)
        {
            //byte[] crypt = Microsoft.AspNetCore.Cryptography.KeyDerivation.KeyDerivation.Pbkdf2(
            //        password,
            //        System.Text.UTF8Encoding.UTF8.GetBytes(salt),
            //        Microsoft.AspNetCore.Cryptography.KeyDerivation.KeyDerivationPrf.HMACSHA512,
            //        10000,
            //        512); //yuc


            //return Convert.ToBase64String(crypt);

            string saltAndPassword = String.Concat(password, salt);
            return FormsAuthentication.HashPasswordForStoringInConfigFile(saltAndPassword, "sha1");
        }

        private long InsertContactChannelEmail(DatabaseUser user, SecurityUser securityUser)
        {
            PersonContactChannel channel = new PersonContactChannel();

            //fill with values
            channel.ContactChannelTypeCode = "PERSONAL";
            channel.PrimaryContactValue = securityUser.Email;
            channel.PersonId = Convert.ToInt64(securityUser.PersonId);
            channel.PrimaryFlag = true;
            channel.CreateUser = securityUser.CreateUser;
            channel.UpdateUser = securityUser.UpdateUser;

            PersonManagement tempPersManagement = new PersonManagement();
            tempPersManagement.InsertPersonContactChannel(user, channel);

            return channel.ContactChannelId;
        }

        private long InsertPerson(DatabaseUser user, string overrideDatabaseName, SecurityUser securityUser)
        {
            Person tempPerson = new Person();
            tempPerson.PersonId = -1;
            tempPerson.LastName = securityUser.LastName;
            tempPerson.FirstName = securityUser.FirstName;
            tempPerson.DisabledFlag = false;
            tempPerson.StudentFlag = false;
            tempPerson.CreateUser = securityUser.UpdateUser;
            tempPerson.UpdateUser = securityUser.UpdateUser;

            PersonManagement persManage = new PersonManagement();
            persManage.InsertPerson(user, overrideDatabaseName, tempPerson);

            return tempPerson.PersonId;
        }

        

        private SecurityUserPasswordHistory CopySecurityUserToPasswordHistory(SecurityUser user)
        { 
            SecurityUserPasswordHistory tempHist = new SecurityUserPasswordHistory();
            tempHist.SecurityUserId = user.SecurityUserId;
            tempHist.UserName = user.UserName;
            tempHist.Password = user.Password;
            tempHist.PasswordSalt = user.PasswordSalt;
            tempHist.PersonId = user.PersonId;
            tempHist.CreateUser = user.UpdateUser; //using UpdateUser as it contains the logged in user, where CreateUser contains 'dbo'

            return tempHist;
        }

        private SecurityUser GetPersonIdFromEmployee(DatabaseUser user, SecurityUser securityUser)
        {
            //get person_id
            EmployeeManagement empManagement = new EmployeeManagement();
            EmployeeCollection emp = empManagement.GetEmployee(user, new EmployeeCriteria { EmployeeId = securityUser.EmployeeId });
            securityUser.PersonId = emp[0].PersonId;

            return securityUser;
        }

        public String GetEmailByEmployeeId(DatabaseUser user, long employeeId)
        {
            return _securityAccess.GetEmailByEmployeeId(user, employeeId);
        }
        #endregion

        #region SecurityUserProfile
        public SecurityUserProfileCollection GetSecurityUserProfile(String databaseName, String userName)
        {
            return _securityAccess.GetSecurityUserProfile(databaseName, userName);
        }

        public void UpdateSecurityUserProfile(DatabaseUser user, SecurityUserProfile item)
        {
            try
            {
                _securityAccess.UpdateSecurityUserProfile(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployee", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                {
                    EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                    throw new System.ServiceModel.FaultException<EmployeeServiceException>(concurrencyException, new System.ServiceModel.FaultReason(ex.Message));
                }
                else
                    throw;
            }
        }
        #endregion
    }
}