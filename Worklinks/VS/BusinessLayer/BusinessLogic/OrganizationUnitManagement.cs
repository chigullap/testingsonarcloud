﻿using System;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Transactions;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.DataLayer.DataAccess;
using WorkLinks.DataLayer.DataAccess.SqlServer;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class OrganizationUnitManagement
    {
        #region fields
        //add data access
        OrganizationUnitAccess _organizationUnitAccess = new OrganizationUnitAccess();
        #endregion

        #region properties
        private OrganizationUnitAccess OrganizationUnitAccess
        {
            get
            {
                if (_organizationUnitAccess == null)
                    _organizationUnitAccess = new OrganizationUnitAccess();

                return _organizationUnitAccess;
            }
        }
        #endregion

        #region organization unit
        public OrganizationUnitCollection GetOrganizationUnit(DatabaseUser user, long? organizationUnitId)
        {
            try
            {
                return OrganizationUnitAccess.GetOrganizationUnit(user, organizationUnitId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - GetOrganizationUnit", ex);
                throw;
            }
        }
        public OrganizationUnitCollection GetOrganizationUnitByParentOrganizationUnitId(DatabaseUser user, long? parentOrganizationUnitId)
        {
            try
            {
                return OrganizationUnitAccess.GetOrganizationUnitByParentOrganizationUnitId(user, parentOrganizationUnitId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - GetOrganizationUnitByParentOrganizationUnitId", ex);
                throw;
            }
        }
        public OrganizationUnitCollection GetOrganizationUnitByOrganizationUnitLevelId(DatabaseUser user, OrganizationUnitCriteria criteria)
        {
            try
            {
                return OrganizationUnitAccess.GetOrganizationUnitByOrganizationUnitLevelId(user, criteria);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - GetOrganizationUnitByOrganizationUnitLevelId", ex);
                throw;
            }
        }
        public OrganizationUnitCollection GetOrganizationUnitWithLevelDescription(DatabaseUser user, long? organizationUnitId)
        {
            try
            {
                return OrganizationUnitAccess.GetOrganizationUnitWithLevelDescription(user, organizationUnitId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - GetOrganizationUnitWithLevelDescription", ex);
                throw;
            }
        }
        public OrganizationUnit UpdateOrganizationUnit(DatabaseUser user, OrganizationUnit data)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //Update the Organization Unit Row (Reflect last update user, update time
                    OrganizationUnitAccess.UpdateOrganizationUnitRow(user, data);

                    foreach (OrganizationUnitDescription organizationUnitDescription in data.Descriptions)
                    {
                        if (organizationUnitDescription.CreateUser == null || organizationUnitDescription.UpdateUser == null)
                        {
                            organizationUnitDescription.CreateUser = data.CreateUser;
                            organizationUnitDescription.UpdateUser = data.UpdateUser;
                        }

                        if (organizationUnitDescription.UnitDescription.Length > 0 && organizationUnitDescription.OrganizationUnitId > 0)//if modifying an existing description (key not -1), perform an update
                            OrganizationUnitAccess.UpdateOrganizationUnitDescriptionRow(user, organizationUnitDescription);

                        else if (organizationUnitDescription.UnitDescription.Length > 0 && organizationUnitDescription.OrganizationUnitId < 0) //if adding a new description (key is -1, -2), perform an insert
                            OrganizationUnitAccess.InsertOrganizationUnitDescriptionRow(user, organizationUnitDescription);

                        else if (organizationUnitDescription.UnitDescription.Length == 0 && organizationUnitDescription.OrganizationUnitId > 0)
                            OrganizationUnitAccess.DeleteOrganizationUnitDescriptionRow(user, organizationUnitDescription);//if a description has been changed to "", then delete it
                    }

                    scope.Complete();
                }

                return data;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - UpdateOrganizationUnit", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 2601)
                    {
                        EmployeeServiceException uniqueIndexException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.UniqueIndexFault);
                        throw new FaultException<EmployeeServiceException>(uniqueIndexException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public OrganizationUnit InsertOrganizationUnit(DatabaseUser user, OrganizationUnit data)
        {
            try
            {
                OrganizationUnit tmpOrganizationUnit = new OrganizationUnit();

                using (TransactionScope scope = new TransactionScope())
                {
                    //insert the Organization Unit row
                    tmpOrganizationUnit = OrganizationUnitAccess.InsertOrganizationUnitRow(user, data);

                    //insert the Organization Unit descriptions
                    if (tmpOrganizationUnit.Descriptions != null)
                    {
                        foreach (OrganizationUnitDescription organizationUnitDescription in tmpOrganizationUnit.Descriptions)
                        {
                            if (organizationUnitDescription.UnitDescription.Length > 0) //if a description is present, insert it
                            {
                                if (organizationUnitDescription.CreateUser == null)
                                {
                                    organizationUnitDescription.CreateUser = organizationUnitDescription.CreateUser;
                                    organizationUnitDescription.UpdateUser = organizationUnitDescription.UpdateUser;
                                }

                                organizationUnitDescription.OrganizationUnitId = tmpOrganizationUnit.OrganizationUnitId;
                                OrganizationUnitAccess.InsertOrganizationUnitDescriptionRow(user, organizationUnitDescription);
                            }
                        }
                    }

                    scope.Complete();
                }

                return tmpOrganizationUnit;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - InsertOrganizationUnit", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 2601)
                    {
                        EmployeeServiceException uniqueIndexException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.UniqueIndexFault);
                        throw new FaultException<EmployeeServiceException>(uniqueIndexException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteOrganizationUnit(DatabaseUser user, OrganizationUnit data)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //delete the descriptions
                    foreach (OrganizationUnitDescription organizationUnitDescription in data.Descriptions)
                        OrganizationUnitAccess.DeleteOrganizationUnitDescriptionRow(user, organizationUnitDescription);

                    //now delete the organization unit row
                    OrganizationUnitAccess.DeleteOrganizationUnitRow(user, data);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - DeleteOrganizationUnit", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new System.ServiceModel.FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        public OrganizationUnitLevelDescriptionCollection GetOrganizationUnitLevelDescription(DatabaseUser user, long organizationUnitLevelId)
        {
            try
            {
                return OrganizationUnitAccess.GetOrganizationUnitLevelDescription(user, organizationUnitLevelId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - GetOrganizationUnitLevelDescription", ex);
                throw;
            }
        }

        #region organization unit level
        public OrganizationUnitLevelCollection GetOrganizationUnitLevel(DatabaseUser user)
        {
            try
            {
                return OrganizationUnitAccess.GetOrganizationUnitLevel(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - GetOrganizationUnitLevel", ex);
                throw;
            }
        }
        public OrganizationUnitLevel InsertOrganizationUnitLevel(DatabaseUser user, OrganizationUnitLevel data)
        {
            try
            {
                OrganizationUnitLevel tmpOrganizationUnitLevel = new OrganizationUnitLevel();

                using (TransactionScope scope = new TransactionScope())
                {
                    //insert the Organization Unit Level row
                    tmpOrganizationUnitLevel = OrganizationUnitAccess.InsertOrganizationUnitLevelRow(user, data);

                    //insert the Organization Unit Level descriptions
                    if (tmpOrganizationUnitLevel.Descriptions != null)
                    {
                        foreach (OrganizationUnitLevelDescription organizationUnitLevelDescription in tmpOrganizationUnitLevel.Descriptions)
                        {
                            if (organizationUnitLevelDescription.LevelDescription.Length > 0)//if a description is present, insert it
                            {
                                if (organizationUnitLevelDescription.CreateUser == null)
                                {
                                    organizationUnitLevelDescription.CreateUser = organizationUnitLevelDescription.CreateUser;
                                    organizationUnitLevelDescription.UpdateUser = organizationUnitLevelDescription.UpdateUser;
                                }

                                organizationUnitLevelDescription.OrganizationUnitLevelId = tmpOrganizationUnitLevel.OrganizationUnitLevelId;
                                OrganizationUnitAccess.InsertOrganizationUnitLevelDescriptionRow(user, organizationUnitLevelDescription);
                            }
                        }
                    }

                    scope.Complete();
                }

                return tmpOrganizationUnitLevel;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - InsertOrganizationUnitLevel", ex);
                throw;
            }
        }
        public OrganizationUnitLevel UpdateOrganizationUnitLevel(DatabaseUser user, OrganizationUnitLevel data)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //Update the Organization Unit Row (Reflect last update user, update time
                    OrganizationUnitAccess.UpdateOrganizationUnitLevelRow(user, data);

                    foreach (OrganizationUnitLevelDescription organizationUnitLevelDescription in data.Descriptions)
                    {
                        if (organizationUnitLevelDescription.CreateUser == null || organizationUnitLevelDescription.UpdateUser == null)
                        {
                            organizationUnitLevelDescription.CreateUser = data.CreateUser;
                            organizationUnitLevelDescription.UpdateUser = data.UpdateUser;
                        }

                        if (organizationUnitLevelDescription.LevelDescription.Length > 0 && organizationUnitLevelDescription.OrganizationUnitLevelId > 0) //if modifying an existing description (key not -1), perform an update
                            OrganizationUnitAccess.UpdateOrganizationUnitLevelDescriptionRow(user, organizationUnitLevelDescription);

                        else if (organizationUnitLevelDescription.LevelDescription.Length > 0 && organizationUnitLevelDescription.OrganizationUnitLevelId < 0) //if adding a new description (key is -1, -2), perform an insert
                            OrganizationUnitAccess.InsertOrganizationUnitLevelDescriptionRow(user, organizationUnitLevelDescription);

                        else if (organizationUnitLevelDescription.LevelDescription.Length == 0 && organizationUnitLevelDescription.OrganizationUnitLevelId > 0)
                            OrganizationUnitAccess.DeleteOrganizationUnitLevelDescriptionRow(user, organizationUnitLevelDescription); //if a description has been changed to "", then delete it
                    }

                    scope.Complete();
                }

                return data;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - UpdateOrganizationUnitLevel", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteOrganizationUnitLevel(DatabaseUser user, OrganizationUnitLevel data)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //delete the descriptions
                    foreach (OrganizationUnitLevelDescription organizationUnitLevelDescription in data.Descriptions)
                        OrganizationUnitAccess.DeleteOrganizationUnitLevelDescriptionRow(user, organizationUnitLevelDescription);

                    //now delete the organization unit level row
                    OrganizationUnitAccess.DeleteOrganizationUnitLevelRow(user, data);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - DeleteOrganizationUnitLevel", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new System.ServiceModel.FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region organization unit association
        public OrganizationUnitAssociationTreeCollection GetOrganizationUnitAssociationTree(DatabaseUser user, String codeLanguageCd, bool? checkActiveFlag)
        {
            try
            {
                return OrganizationUnitAccess.GetOrganizationUnitAssociationTree(user, codeLanguageCd, checkActiveFlag);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - GetOrganizationUnitAssociationTree", ex);
                throw;
            }
        }
        public OrganizationUnitAssociationCollection GetOrganizationUnitAssociation(DatabaseUser user, OrganizationUnitCriteria criteria)
        {
            try
            {
                return OrganizationUnitAccess.GetOrganizationUnitAssociation(user, criteria);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - GetOrganizationUnitAssociation", ex);
                throw;
            }
        }
        public OrganizationUnitAssociationCollection InsertOrganizationUnitAssociation(DatabaseUser user, OrganizationUnitAssociation data)
        {
            OrganizationUnitAssociationCollection organizationUnitAssociationCollection = new OrganizationUnitAssociationCollection();
            try
            {
                OrganizationUnitAccess.InsertOrganizationUnitAssociation(user, data);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - InsertOrganizationUnitAssociation", ex);
                throw;
            }
            return organizationUnitAssociationCollection;

        }
        public OrganizationUnitAssociationCollection DeleteOrganizationUnitAssociation(DatabaseUser user, OrganizationUnitAssociation data)
        {
            OrganizationUnitAssociationCollection organizationUnitAssociation = new OrganizationUnitAssociationCollection();
            try
            {
                OrganizationUnitAccess.DeleteOrganizationUnitAssociation(user, data);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - DeleteOrganizationUnitAssociation", ex);
                throw;
            }
            return organizationUnitAssociation;
        }
        #endregion

        #region employee position labour distribution
        public EmployeePositionLabourDistributionSummaryCollection GetEmployeePositionLabourDistribution(DatabaseUser user, long employeePositionId, String codeLanguageCd)
        {
            return OrganizationUnitAccess.GetEmployeePositionLabourDistribution(user, employeePositionId, codeLanguageCd);
        }
        public EmployeePositionLabourDistributionSummary UpdateEmployeePositionLabourDistribution(DatabaseUser user, EmployeePositionLabourDistributionSummary summary)
        {
            try
            {
                OrganizationUnitAccess.UpdateEmployeePositionLabourDistribution(user, summary);
                return summary;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - UpdateEmployeePositionLabourDistribution", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public EmployeePositionLabourDistributionSummary InsertEmployeePositionLabourDistribution(DatabaseUser user, EmployeePositionLabourDistributionSummary summary)
        {
            try
            {
                return OrganizationUnitAccess.InsertEmployeePositionLabourDistribution(user, summary);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - InsertEmployeePositionLabourDistribution", ex);
                throw;
            }
        }
        public void DeleteEmployeePositionLabourDistribution(DatabaseUser user, EmployeePositionLabourDistributionSummary summary)
        {
            try
            {
                OrganizationUnitAccess.DeleteEmployeePositionLabourDistribution(user, summary);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - DeleteEmployeePositionLabourDistribution", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new System.ServiceModel.FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        public bool CheckEmployeeExist(DatabaseUser user)
        {
            try
            {
                EmployeeSummaryCollection empSummary = new EmployeeAccess().GetEmployeeSummary(user, new EmployeeCriteria() { EmployeeNumber = null, SecurityOverrideFlag = true });

                if (empSummary.Count > 0)
                    return true; //employees exist
                else
                    return false; //no employees exist
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "OrganizationUnitManagement - CheckEmployeeExist", ex);
                throw;
            }
        }
    }
}