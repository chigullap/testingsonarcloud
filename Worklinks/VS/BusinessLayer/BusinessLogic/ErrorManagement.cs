﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.ComponentModel;
using System.Transactions;

using WorkLinks.DataLayer.DataAccess.SqlServer;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.DataLayer.DataAccess;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class ErrorManagement
    {
        public static void HandleError(DatabaseUser user, String methodName, Exception exc)
        {
            ErrorLogAccess _errorLogAccess = new ErrorLogAccess();
            _errorLogAccess.InsertErrorLog(user, methodName, exc);
        }
    }
}
