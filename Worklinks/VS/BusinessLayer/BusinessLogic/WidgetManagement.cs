﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.DataLayer.DataAccess;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class WidgetManagement
    {
        #region fields
        private WidgetAccess _widgetAccess = null;
        private EmployeeAccess _employeeAccess = null;
        #endregion

        #region properties
        private WidgetAccess WidgetAccess
        {
            get
            {
                if (_widgetAccess == null)
                    _widgetAccess = new WidgetAccess();

                return _widgetAccess;
            }
        }
        private EmployeeAccess EmployeeAccess
        {
            get
            {
                if (_employeeAccess == null)
                    _employeeAccess = new EmployeeAccess();

                return _employeeAccess;
            }
        }
        #endregion

        #region employee payslip
        public EmployeePayslipCollection GetEmployeePayslip(DatabaseUser user)
        {
            try
            {
                return WidgetAccess.GetEmployeePayslip(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WidgetManagement - GetEmployeePayslip", ex);
                throw;
            }
        }
        #endregion

        #region labour cost
        public LabourCostCollection GetLabourCost(DatabaseUser user, Decimal year)
        {
            try
            {
                return WidgetAccess.GetLabourCost(user, year);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WidgetManagement - GetLabourCost", ex);
                throw;
            }
        }
        #endregion

        #region headcount
        public HeadcountCollection GetHeadcount(DatabaseUser user)
        {
            try
            {
                return WidgetAccess.GetHeadcount(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WidgetManagement - GetHeadcount", ex);
                throw;
            }
        }
        public HeadcountCollection GetHeadcountByOrganizationUnitLevelId(DatabaseUser user, long organizationUnitLevelId)
        {
            try
            {
                return WidgetAccess.GetHeadcountByOrganizationUnitLevelId(user, organizationUnitLevelId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WidgetManagement - GetHeadcountByOrganizationUnitLevelId", ex);
                throw;
            }
        }
        #endregion

        #region employee profile
        public EmployeeProfileCollection GetEmployeeProfile(DatabaseUser user)
        {
            try
            {
                EmployeeProfileCollection collection = WidgetAccess.GetEmployeeProfile(user);

                if (collection.Count > 0)
                {
                    EmployeePhotoCollection photos = EmployeeAccess.GetEmployeePhoto(user, Convert.ToInt64(collection[0].EmployeeId));
                    if (photos.Count > 0)
                        collection[0].EmployeePhotoObject = photos[0];
                    else
                        collection[0].EmployeePhotoObject = null;
                }

                return collection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WidgetManagement - GetEmployeeProfile", ex);
                throw;
            }
        }
        #endregion

        #region payroll breakdown
        public PayrollBreakdownCollection GetPayrollBreakdown(DatabaseUser user, String userName, String breakdownType, String filteredBy)
        {
            try
            {
                return WidgetAccess.GetPayrollBreakdown(user, userName, breakdownType, filteredBy);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WidgetManagement - GetPayrollBreakdown", ex);
                throw;
            }
        }
        #endregion
    }
}