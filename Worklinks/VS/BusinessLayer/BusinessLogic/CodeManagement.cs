﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Transactions;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.DataLayer.DataAccess;
using WorkLinks.DataLayer.DataAccess.SqlServer;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class CodeManagement
    {
        #region fields
        private static Dictionary<String, Dictionary<CodeTableType, CodeCollection>> _cache = null;
        private static Object _addLock = new Object(); //threadsafety

        //access
        CodeAccess _access = new CodeAccess();
        EmployeeAccess _employeeAccess = new EmployeeAccess();
        SecurityAccess _securityAccess = new SecurityAccess();
        PayrollAccess _payrollAccess = new PayrollAccess();
        PayrollManagement _payrollManagement = new PayrollManagement();
        #endregion

        #region properties
        public static Dictionary<String, Dictionary<CodeTableType, CodeCollection>> Cache
        {
            get
            {
                if (_cache == null)
                    _cache = new Dictionary<String, Dictionary<CodeTableType, CodeCollection>>();

                return _cache;
            }
        }
        public EmployeeAccess EmployeeAccess
        {
            get
            {
                if (_employeeAccess == null)
                    _employeeAccess = new EmployeeAccess();

                return _employeeAccess;
            }
        }
        public PayrollManagement PayrollManagement
        {
            get
            {
                if (_payrollManagement == null)
                    _payrollManagement = new PayrollManagement();

                return _payrollManagement;
            }
        }
        #endregion

        #region field language editor
        public LanguageEntityCollection GetFieldLanguageInfo(DatabaseUser user, Decimal formId, String attributeIdentifier, String englishLanguageCode)
        {
            try
            {
                return _access.GetFieldLanguageInfo(user, formId, attributeIdentifier, englishLanguageCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetFieldLanguageInfo", ex);
                throw;
            }
        }
        public LanguageEntityCollection UpdateFieldLanguage(DatabaseUser user, LanguageEntityCollection entityCollection, String englishLanguageCode, String frenchLanguageCode)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (LanguageEntity fEntity in entityCollection)
                    {
                        if (fEntity.ControlFieldValueId > 0 && fEntity.FrenchControlFieldValueId > 0) //both English and French records need to be updated
                            _access.UpdateFieldLanguage(user, fEntity, englishLanguageCode, frenchLanguageCode);
                        else if (fEntity.ControlFieldValueId > 0 && fEntity.FrenchControlFieldValueId < 0) //the English record needs to be updated and the French record needs to be inserted
                        {
                            _access.UpdateFieldLanguage(user, fEntity, englishLanguageCode, frenchLanguageCode);
                            _access.InsertFieldLanguage(user, fEntity, englishLanguageCode, frenchLanguageCode);
                        }
                        else if (fEntity.ControlFieldValueId < 0 && fEntity.FrenchControlFieldValueId > 0) //the English record needs to be inserted and the French record needs to be updated
                        {
                            fEntity.CodeLanguageCd = englishLanguageCode;
                            _access.InsertFieldLanguage(user, fEntity, englishLanguageCode, frenchLanguageCode);
                            _access.UpdateFieldLanguage(user, fEntity, englishLanguageCode, frenchLanguageCode);
                        }
                        else //both the English and French records need to be inserted
                        {
                            fEntity.CodeLanguageCd = englishLanguageCode;
                            _access.InsertFieldLanguage(user, fEntity, englishLanguageCode, frenchLanguageCode);
                        }
                    }

                    scope.Complete();
                }

                return entityCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdateFieldLanguage", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region field editor
        public FormDescriptionCollection GetFormInfo(DatabaseUser user, String criteria)
        {
            try
            {
                return _access.GetFormInfo(user, criteria);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetFormInfo", ex);
                throw;
            }
        }
        public FieldEntityCollection GetFieldInfo(DatabaseUser user, Decimal formId, String attributeIdentifier, int roleId)
        {
            try
            {
                return _access.GetFieldInfo(user, formId, attributeIdentifier, roleId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetFieldInfo", ex);
                throw;
            }
        }
        public FieldEntityCollection UpdateFieldEntity(DatabaseUser user, FieldEntityCollection entityCollection)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (FieldEntity fEntity in entityCollection)
                    {
                        //logic here to do an update or an insert based on key values > 0
                        if (fEntity.ControlFieldValueId > 0)
                            _access.UpdateFieldEntity(user, fEntity);
                        else
                            _access.InsertFieldEntity(user, fEntity);
                    }

                    scope.Complete();
                }

                return entityCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdateFieldEntity", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region codeTableDescription calls
        public CodeTableDescriptionCollection GetCodeTableDescriptionRows(DatabaseUser user, String tableName, String code)
        {
            try
            {
                return _access.GetCodeTableDescriptionRows(user, tableName, code);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetCodeTableDescriptionRows", ex);
                throw;
            }
        }
        public CodeTable UpdateCodeTableDescRow(DatabaseUser user, CodeTable codeTableDesc, String codeTableName)
        {
            try
            {
                return _access.UpdateCodeTableDescRow(user, codeTableDesc, codeTableName);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdateCodeTableDescRow", ex);
                throw;
            }
        }
        public CodeTable InsertCodeTableDescRow(DatabaseUser user, CodeTable codeTableDesc, String codeTableName)
        {
            try
            {
                return _access.InsertCodeTableDescRow(user, codeTableDesc, codeTableName);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - InsertCodeTableDescRow", ex);
                throw;
            }
        }
        public void DeleteCodeTableDescRow(DatabaseUser user, CodeTable codeTableDesc, String codeTableName)
        {
            try
            {
                _access.DeleteCodeTableDescRow(user, codeTableDesc, codeTableName);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - DeleteCodeTableDescRow", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region codeTable calls
        public String DecodeCode(DatabaseUser user, CodeTableType type, String externalcode)
        {
            if (externalcode == null || externalcode == "")
                return null;

            CodeCollection collection = GetCodeTable(user, type);

            foreach (CodeObject code in collection)
            {
                String matchCode = (code.ImportExternalIdentifier ?? code.Code).ToLower();

                if (matchCode.Equals(externalcode.ToLower()))
                    return code.Code;
            }

            //if we are this far it's not found
            EmployeeServiceException exception = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.CodeMismatch);
            throw new FaultException<EmployeeServiceException>(exception, new FaultReason(String.Format("Cannot match code:{0} type:{1} in WorkLinks.", externalcode, type)));
        }
        public CodeCollection GetCodeTable(DatabaseUser user, CodeTableType type)
        {
            CodeCollection collection = null;

            //not caching code system values anymore
            if (type == CodeTableType.System)
                collection = GetCodeTable(user.DatabaseName, user.LanguageCode, type);
            else
            {
                Dictionary<CodeTableType, CodeCollection> codeTableCache = GetCodeTableCache(user.LanguageCode);

                if (!codeTableCache.TryGetValue(type, out collection))
                {
                    lock (_addLock) //don't thread table loads
                    {
                        collection = GetCodeTable(user.DatabaseName, user.LanguageCode, type);
                        codeTableCache.Add(type, collection);
                    }
                }
            }

            return collection;
        }
        public static Dictionary<CodeTableType, CodeCollection> GetCodeTableCache(String language)
        {
            Dictionary<CodeTableType, CodeCollection> codeTableCache = null;

            if (!Cache.TryGetValue(language, out codeTableCache))
            {
                lock (Cache)
                {
                    codeTableCache = new Dictionary<CodeTableType, CodeCollection>();
                    _cache.Add(language, codeTableCache);
                }
            }

            return codeTableCache;
        }
        public CodeCollection GetCodeTable(String databaseName, String language, CodeTableType type)
        {
            return _access.GetCodeTable(databaseName, language, type);
        }
        /// <summary>
        /// Use this method when need to retrieve data by parent column name and value. 
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="language"></param>
        /// <param name="type"></param>
        /// <param name="parentColumnName"></param>
        /// <param name="parentColumnValue"></param>
        /// <returns></returns>
        public CodeCollection GetCodeTable(String databaseName, String language, CodeTableType type, String parentColumnName, String parentColumnValue)
        {
            return _access.GetCodeTable(databaseName, language, type, parentColumnName, parentColumnValue);
        }
        public CodeCollection GetCodeTable(String databaseName, String language, CodeTableType type, String code)
        {
            CodeCollection collection = new CodeCollection();
            CodeCollection temp = GetCodeTable(databaseName, language, type);

            if (temp != null && temp[code] != null)
                collection.Add(temp[code]);

            return collection;
        }
        /// <summary>
        /// Use this method when need to retrieve data by parent column name and value.  
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="language"></param>
        /// <param name="type"></param>
        /// <param name="code"></param>
        /// <param name="parentColumnName"></param>
        /// <param name="parentColumnValue"></param>
        /// <returns></returns>
        public CodeCollection GetCodeTable(String databaseName, String language, CodeTableType type, String code, String parentColumnName, String parentColumnValue)
        {
            CodeCollection collection = new CodeCollection();
            CodeCollection temp = GetCodeTable(databaseName, language, type, parentColumnName, parentColumnValue);

            if (temp != null && temp[code] != null)
                collection.Add(temp[code]);

            return collection;
        }
        public CodeTableCollection GetCodeTableRows(DatabaseUser user, String tableName, String parentTableName)
        {
            try
            {
                return _access.GetCodeTableRows(user, tableName, parentTableName);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetCodeTableRows", ex);
                throw;
            }
        }
        public CodeTable UpdateCodeTableData(DatabaseUser user, CodeTable codeTable, String codeTableName, String parentTableName)
        {
            try
            {
                CodeTable tempCodeTable = new CodeTable();

                using (TransactionScope scope = new TransactionScope())
                {
                    //update the descriptions
                    tempCodeTable = UpdateCodeTableDescRow(user, codeTable, codeTableName);

                    //update the code table row
                    tempCodeTable = UpdateCodeTableRow(user, tempCodeTable, codeTableName, parentTableName);

                    scope.Complete();
                }

                return tempCodeTable;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdateCodeTableData", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public CodeTable UpdateCodeTableRow(DatabaseUser user, CodeTable codeTable, String codeTableName, String parentTableName)
        {
            try
            {
                return _access.UpdateCodeTableRow(user, codeTable, codeTableName, parentTableName);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdateCodeTableRow", ex);
                throw;
            }
        }
        public CodeTable InsertCodeTableRow(DatabaseUser user, CodeTable codeTable, String codeTableName, String parentTableName)
        {
            try
            {
                return _access.InsertCodeTableRow(user, codeTable, codeTableName, parentTableName);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - InsertCodeTableRow", ex);
                throw;
            }
        }
        public void DeleteCodeTableRow(DatabaseUser user, CodeTable codeTable, String codeTableName)
        {
            try
            {
                _access.DeleteCodeTableRow(user, codeTable, codeTableName);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - DeleteCodeTableRow", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        //inserts the Code Table row and then the Code Table Description rows
        public CodeTable InsertCodeTableData(DatabaseUser user, CodeTable codeTable, String codeTableName, String parentTableName)
        {
            try
            {
                CodeTable tempCodeTable = new CodeTable();

                using (TransactionScope scope = new TransactionScope())
                {
                    //insert the code table row
                    tempCodeTable = InsertCodeTableRow(user, codeTable, codeTableName, parentTableName);

                    //insert the code table descriptions, if there are any
                    tempCodeTable = InsertCodeTableDescRow(user, tempCodeTable, codeTableName);

                    scope.Complete();
                }

                return tempCodeTable;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - InsertCodeTableData", ex);
                throw;
            }
        }
        //deletes the Code Table Description rows and then the Code Table row
        public void DeleteCodeTableData(DatabaseUser user, CodeTable codeTable, String codeTableName)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    DeleteCodeTable(user, codeTable, codeTableName);
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - DeleteCodeTableData", ex);
                throw ex;
            }
        }
        public void DeleteCodeTable(DatabaseUser user, CodeTable codeTable, String codeTableName)
        {
            try
            {
                //delete the descriptions
                DeleteCodeTableDescRow(user, codeTable, codeTableName);

                //now delete the code table row
                DeleteCodeTableRow(user, codeTable, codeTableName);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - DeleteCodeTable", ex);
                throw ex;
            }
        }
        #endregion

        #region salary plan code table
        public SalaryPlanCodeTableCollection GetSalaryPlanCodeTableRows(DatabaseUser user, String tableName, String parentSalaryPlanCode)
        {
            try
            {
                return _access.GetSalaryPlanCodeTableRows(user, tableName, parentSalaryPlanCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetSalaryPlanCodeTableRows", ex);
                throw;
            }
        }
        public SalaryPlanCodeTable InsertSalaryPlanCodeTableData(DatabaseUser user, SalaryPlanCodeTable salaryPlanCodeTable)
        {
            try
            {
                SalaryPlanCodeTable tempSalaryPlanCodeTable = new SalaryPlanCodeTable();

                using (TransactionScope scope = new TransactionScope())
                {
                    //insert the code table row
                    tempSalaryPlanCodeTable = _access.InsertSalaryPlanCodeTableRow(user, salaryPlanCodeTable);

                    //insert the code table descriptions, if there are any
                    _access.InsertCodeTableDescRow(user, tempSalaryPlanCodeTable, "code_salary_plan");

                    scope.Complete();
                }

                return tempSalaryPlanCodeTable;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - InsertSalaryPlanCodeTableData", ex);
                throw;
            }
        }
        public SalaryPlanCodeTable UpdateSalaryPlanCodeTableData(DatabaseUser user, SalaryPlanCodeTable salaryPlanCodeTable)
        {
            try
            {
                SalaryPlanCodeTable tempSalaryPlanCodeTable = new SalaryPlanCodeTable();

                using (TransactionScope scope = new TransactionScope())
                {
                    //update the descriptions
                    tempSalaryPlanCodeTable = _access.UpdateSalaryPlanCodeTableRow(user, salaryPlanCodeTable);

                    //update the code table row
                    _access.UpdateCodeTableDescRow(user, tempSalaryPlanCodeTable, "code_salary_plan");

                    scope.Complete();
                }

                return tempSalaryPlanCodeTable;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdateSalaryPlanCodeTableData", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteSalaryPlanCodeTableData(DatabaseUser user, SalaryPlanCodeTable salaryPlanCodeTable)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //delete the descriptions
                    _access.DeleteCodeTableDescRow(user, salaryPlanCodeTable, "code_salary_plan");

                    //now delete the code table row
                    _access.DeleteSalaryPlanCodeTableRow(user, salaryPlanCodeTable);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - DeleteSalaryPlanCodeTableData", ex);
                throw ex;
            }
        }
        #endregion

        #region personAddressTypeCodeTable calls
        public PersonAddressTypeCodeTableCollection GetPersonAddressTypeCodeTableRows(DatabaseUser user)
        {
            try
            {
                return _access.GetPersonAddressTypeCodeTableRows(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetPersonAddressTypeCodeTableRows", ex);
                throw;
            }
        }
        public PersonAddressTypeCodeTable GetPersonAddressTypeCodeTableRow(DatabaseUser user, short priority)
        {
            try
            {
                return _access.GetPersonAddressTypeCodeTableRow(user, priority);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetPersonAddressTypeCodeTableRow", ex);
                throw;
            }
        }
        public PersonAddressTypeCodeTable InsertPersonAddressTypeCodeTableData(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTable)
        {
            try
            {
                PersonAddressTypeCodeTable personAddressTypeTempCodeTable = new PersonAddressTypeCodeTable();

                using (TransactionScope scope = new TransactionScope())
                {
                    //insert the code table row
                    personAddressTypeTempCodeTable = _access.InsertPersonAddressTypeCodeTableRow(user, personAddressTypeCodeTable);

                    //insert the code table descriptions, if there are any
                    personAddressTypeTempCodeTable = _access.InsertPersonAddressTypeCodeTableDescRow(user, personAddressTypeTempCodeTable);

                    scope.Complete();
                }

                return personAddressTypeTempCodeTable;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - InsertPersonAddressTypeCodeTableData", ex);
                throw;
            }
        }
        public PersonAddressTypeCodeTable UpdatePersonAddressTypeCodeTableData(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTable)
        {
            try
            {
                PersonAddressTypeCodeTable tempPersonAddressTypeCodeTable = new PersonAddressTypeCodeTable();

                using (TransactionScope scope = new TransactionScope())
                {
                    //update the descriptions
                    tempPersonAddressTypeCodeTable = _access.UpdatePersonAddressTypeCodeTableDescRow(user, personAddressTypeCodeTable);

                    //update the code table row
                    tempPersonAddressTypeCodeTable = _access.UpdatePersonAddressTypeCodeTableRow(user, tempPersonAddressTypeCodeTable);

                    scope.Complete();
                }

                return tempPersonAddressTypeCodeTable;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdatePersonAddressTypeCodeTableData", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeletePersonAddressTypeCodeTableData(DatabaseUser user, PersonAddressTypeCodeTable personAddressTypeCodeTable)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //delete the descriptions
                    _access.DeletePersonAddressTypeCodeTableDescRow(user, personAddressTypeCodeTable);

                    //now delete the code table row
                    _access.DeletePersonAddressTypeCodeTableRow(user, personAddressTypeCodeTable);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - DeletePersonAddressTypeCodeTableData", ex);
                throw ex;
            }
        }
        #endregion

        #region codeTableIndex calls
        public CodeTableIndexCollection GetCodeTableIndex(DatabaseUser user, String tableName)
        {
            try
            {
                return _access.GetCodeTableIndex(user, tableName);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetCodeTableIndex", ex);
                throw;
            }
        }
        #endregion

        #region codeCollection calls
        public CodeCollection GetDisciplineTypeCode(DatabaseUser user, string disciplineCode)
        {
            try
            {
                return _access.GetDisciplineTypeCode(user, disciplineCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetDisciplineTypeCode", ex);
                throw;
            }
        }
        public CodeCollection GetProvinceStateCode(DatabaseUser user, String country)
        {
            try
            {
                return _access.GetProvinceStateCode(user, country);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetProvinceStateCode", ex);
                throw;
            }
        }
        public CodeCollection GetDayCode(DatabaseUser user, String monthCode)
        {
            try
            {
                return _access.GetDayCode(user, monthCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetDayCode", ex);
                throw;
            }
        }
        public CodeCollection GetYearEndFormBoxCode(DatabaseUser user, String yearEndFormCode)
        {
            try
            {
                return _access.GetYearEndFormBoxCode(user, yearEndFormCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetYearEndFormBoxCode", ex);
                throw;
            }
        }
        public CodeCollection GetContactChannelTypeCode(DatabaseUser user, String contactChannelCategoryCode)
        {
            try
            {
                return _access.GetContactChannelTypeCode(user, contactChannelCategoryCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetContactChannelTypeCode", ex);
                throw;
            }
        }
        public CodeCollection GetContactChannelTypeCodeRemovingTypesInUse(DatabaseUser user, String contactChannelCategoryCode, long personId)
        {
            try
            {
                //gets a list of email types by type
                CodeCollection collection = _access.GetContactChannelTypeCode(user, contactChannelCategoryCode);

                //logic here to remove dupes
                for (int i = collection.Count; i > 0; i--)
                {
                    foreach (CodeContactChannelType item in EmployeeAccess.ContactChannelTypeByEmployeeId(user, personId, contactChannelCategoryCode))
                    {
                        if (item.ContactTypeChannelCode == collection[i - 1].Code) //employee is already assigned this contact type
                        {
                            collection.RemoveAt(i - 1); //remove this option
                            break;
                        }
                    }
                }

                return collection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetContactChannelTypeCodeRemovingTypesInUse", ex);
                throw;
            }
        }
        public CodeCollection GetContactChannelTypeCodeRemovingTypesInUseLeavingSelected(DatabaseUser user, String contactChannelCategoryCode, long personId, String selectedCode)
        {
            try
            {
                //gets a list of email types by type
                CodeCollection collection = _access.GetContactChannelTypeCode(user, contactChannelCategoryCode);

                //logic here to remove dupes
                for (int i = collection.Count; i > 0; i--)
                {
                    foreach (CodeContactChannelType item in EmployeeAccess.ContactChannelTypeByEmployeeId(user, personId, contactChannelCategoryCode))
                    {
                        if (item.ContactTypeChannelCode == collection[i - 1].Code && item.ContactTypeChannelCode != selectedCode) //employee is already assigned this contact type but leave if it's the selectedCode
                        {
                            collection.RemoveAt(i - 1); //remove this option
                            break;
                        }
                    }
                }

                return collection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetContactChannelTypeCodeRemovingTypesInUseLeavingSelected", ex);
                throw;
            }
        }
        public CodeCollection GetPaycodeTypeCode(DatabaseUser user, String paycode)
        {
            try
            {
                return _access.GetPaycodeTypeCode(user.DatabaseName, user.LanguageCode, paycode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetPaycodeTypeCode", ex);
                throw;
            }
        }
        public CodeCollection GetPaycodeByCode(DatabaseUser user, String paycodeCode)
        {
            try
            {
                return _access.GetPaycodesByType(user, null, paycodeCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetPaycodeByCode", ex);
                throw;
            }
        }
        public CodeCollection GetPaycodesByType(DatabaseUser user, String paycodeTypeCode)
        {
            try
            {
                return _access.GetPaycodesByType(user, paycodeTypeCode, null);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetPaycodesByType", ex);
                throw;
            }
        }
        public CodeCollection GetPaycodesByTypeRemovingPaycodesInUse(DatabaseUser user, String paycodeTypeCode, long? employeeId)
        {
            try
            {
                //gets a list of paycodes by type
                CodeCollection collection = _access.GetPaycodesByType(user, paycodeTypeCode, null);

                for (int i = collection.Count; i > 0; i--)
                {
                    foreach (EmployeePaycode item in EmployeeAccess.GetEmployeePaycode(user, employeeId, false, true))
                    {
                        if (item.PaycodeCode == collection[i - 1].Code) //employee is already assigned this paycode
                        {
                            collection.RemoveAt(i - 1); //remove this option
                            break;
                        }
                    }
                }

                return collection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetPaycodesByTypeRemovingPaycodesInUse", ex);
                throw;
            }
        }
        public CodeCollection GetPaycodesByTypeRemovingGlobalPaycodesInUse(DatabaseUser user, String paycodeTypeCode)
        {
            try
            {
                //gets a list of paycodes by type
                CodeCollection collection = _access.GetPaycodesByType(user, paycodeTypeCode, null);

                for (int i = collection.Count; i > 0; i--)
                {
                    foreach (EmployeePaycode item in EmployeeAccess.GetGlobalEmployeePaycode(user, null))
                    {
                        if (item.PaycodeCode == collection[i - 1].Code) //paycode is already assigned
                        {
                            collection.RemoveAt(i - 1); //remove this option
                            break;
                        }
                    }
                }

                return collection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetPaycodesByTypeRemovingGlobalPaycodesInUse", ex);
                throw;
            }
        }
        public CodeCollection GetRecurringIncomePaycodesRemovingGlobalPaycodesInUse(DatabaseUser user, String paycodeTypeCode)
        {
            try
            {
                //gets a list of paycodes by type
                CodeCollection collection = _access.GetPaycodesByType(user, paycodeTypeCode, null);

                RemoveNonRecurringIncomeCodes(collection);

                for (int i = collection.Count; i > 0; i--)
                {
                    foreach (EmployeePaycode item in EmployeeAccess.GetGlobalEmployeePaycode(user, null))
                    {
                        if (item.PaycodeCode == collection[i - 1].Code) //paycode is already assigned
                        {
                            collection.RemoveAt(i - 1); //remove this option
                            break;
                        }
                    }
                }

                return collection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetPaycodesByTypeRemovingGlobalPaycodesInUse", ex);
                throw;
            }
        }
        public CodeCollection RemoveNonRecurringIncomeCodes(CodeCollection collection)
        {
            for (int i = collection.Count; i > 0; i--)
            {
                if (Convert.ToBoolean(collection[i - 1].ParentCode) == false)
                    collection.RemoveAt(i - 1);
            }

            return collection;
        }
        public CodeCollection GetRecurringIncomePaycodesRemovingPaycodesInUse(DatabaseUser user, String paycodeTypeCode, long? employeeId)
        {
            try
            {
                //gets a list of Paycodes by type
                CodeCollection collection = _access.GetPaycodesByType(user, paycodeTypeCode, null);

                RemoveNonRecurringIncomeCodes(collection);

                for (int i = collection.Count; i > 0; i--)
                {
                    foreach (EmployeePaycode item in EmployeeAccess.GetEmployeePaycode(user, employeeId, false, true))
                    {
                        if (item.PaycodeCode == collection[i - 1].Code) //employee is already assigned this paycode
                        {
                            collection.RemoveAt(i - 1); //remove this option
                            break;
                        }
                    }
                }

                return collection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetRecurringIncomePaycodesRemovingPaycodesInUse", ex);
                throw;
            }
        }
        public CodeCollection GetPaycodesRemovingAssociationsInUse(DatabaseUser user, String paycodeAssociationTypeCode)
        {
            try
            {
                //gets a list of paycodes
                CodeCollection collection = _access.GetPaycodesByType(user, null, null);

                for (int i = collection.Count; i > 0; i--)
                {
                    foreach (PaycodeAssociation item in _access.GetPaycodeAssociation(user, paycodeAssociationTypeCode))
                    {
                        if (item.PaycodeCode == collection[i - 1].Code) //paycode is already assigned
                        {
                            collection.RemoveAt(i - 1); //remove this option
                            break;
                        }
                    }
                }

                return collection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetPaycodesRemovingAssociationsInUse", ex);
                throw;
            }
        }
        public bool IsPaycodeGarnishment(DatabaseUser user, String paycode, String paycodeType)
        {
            try
            {
                return _access.IsPaycodeGarnishment(user, paycode, paycodeType);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - IsPaycodeGarnishment", ex);
                throw;
            }
        }
        public CodeCollection GetContactRelationshipCode(DatabaseUser user)
        {
            try
            {
                return _access.GetContactRelationshipCode(user.DatabaseName, user.LanguageCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetContactRelationshipCode", ex);
                throw;
            }
        }
        public CodeCollection GetBusinessNumber(DatabaseUser user, long? businessNumberId)
        {
            try
            {
                //language ignored for now
                CodeCollection collection = new CodeCollection();

                foreach (BusinessNumber item in EmployeeAccess.GetBusinessNumber(user, businessNumberId))
                    collection.Add(new CodeObject() { Code = item.BusinessNumberId.ToString(), Description = item.LongDescription });

                return collection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetBusinessNumber", ex);
                throw;
            }
        }
        public CodeCollection GetBusinessAndEmployerNumber(DatabaseUser user, long? businessNumberId)
        {
            try
            {
                //language ignored for now
                CodeCollection collection = new CodeCollection();

                foreach (BusinessNumber item in EmployeeAccess.GetBusinessNumber(user, businessNumberId))
                    collection.Add(new CodeObject() { Code = item.BusinessNumberId.ToString(), Description = item.EmployerNumber });

                return collection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetBusinessAndEmployerNumber", ex);
                throw;
            }
        }
        public CodeCollection GetUnionCollectiveAgreementCode(DatabaseUser user, String unionCode)
        {
            try
            {
                return _access.GetUnionCollectiveAgreementCode(user, unionCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetUnionCollectiveAgreementCode", ex);
                throw;
            }
        }
        public CodeCollection GetEmployeeBankCode(DatabaseUser user, String bankingCountryCode)
        {
            try
            {
                return _access.GetEmployeeBankCode(user, bankingCountryCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetEmployeeBankCode", ex);
                throw;
            }
        }
        public CodeCollection GetEmployeeCustomFieldNameCodesRemovingInUse(DatabaseUser user)
        {
            try
            {
                //gets a list of employee custom field name codes
                CodeCollection collection = _access.GetCodeTable(user.DatabaseName, user.LanguageCode, CodeTableType.EmployeeCustomFieldNameCode);

                //logic here to remove dupes
                for (int i = collection.Count; i > 0; i--)
                {
                    foreach (EmployeeCustomFieldConfig item in EmployeeAccess.GetEmployeeCustomFieldConfig(user, null))
                    {
                        if (item.EmployeeCustomFieldNameCode == collection[i - 1].Code) //already assigned this employee custom field name
                        {
                            collection.RemoveAt(i - 1); //remove this option
                            break;
                        }
                    }
                }

                return collection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetEmployeeCustomFieldNameCodesRemovingInUse", ex);
                throw;
            }
        }
        #endregion

        #region code paycode
        public CodePaycodeCollection GetCodePaycode(DatabaseUser user, String paycode, bool useExternalFlag)
        {
            try
            {
                return _access.GetCodePaycode(user, paycode, useExternalFlag);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetCodePaycode", ex);
                throw;
            }
        }
        public void UpdateCodePaycode(DatabaseUser user, CodePaycode paycode, bool overrideConcurrencyCheckFlag)
        {
            try
            {
                _access.UpdateCodePaycode(user, paycode, overrideConcurrencyCheckFlag);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdateCodePaycode", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public CodePaycode InsertCodePaycode(DatabaseUser user, CodePaycode paycode)
        {
            try
            {
                return _access.InsertCodePaycode(user, paycode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - InsertCodePaycode", ex);
                throw;
            }
        }
        public void DeleteCodePaycode(DatabaseUser user, CodePaycode paycode)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    if (paycode.PaycodeTypeCode == ((short)CodePaycode.PaycodeType.Benefit).ToString())
                        _access.DeleteCodePaycodeBenefit(user, paycode);
                    else if (paycode.PaycodeTypeCode == ((short)CodePaycode.PaycodeType.Deduction).ToString())
                        _access.DeleteCodePaycodeDeduction(user, paycode);
                    else if (paycode.PaycodeTypeCode == ((short)CodePaycode.PaycodeType.Income).ToString())
                        _access.DeleteCodePaycodeIncome(user, paycode);
                    else if (paycode.PaycodeTypeCode == ((short)CodePaycode.PaycodeType.Employer).ToString())
                        _access.DeleteCodePaycodeEmployer(user, paycode);

                    _payrollAccess.DeletePaycodeYearEndReportMap(user, new PaycodeYearEndReportMap { PaycodeCode = paycode.PaycodeCode }, true);
                    _payrollAccess.DeletePaycodePayrollTransactionOffsetAssociation(user, new PaycodePayrollTransactionOffsetAssociation { PaycodeCode = paycode.PaycodeCode }, true);
                    DeletePaycodeAttachedPaycodeProvision(user, paycode.PaycodeCode);
                    _access.DeleteCodePaycode(user, paycode);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - DeleteCodePaycode", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region code paycode benefit
        public CodePaycodeBenefitCollection GetCodePaycodeBenefit(DatabaseUser user, String paycodeCode)
        {
            try
            {
                //get benefit paycodes or create a new one if none exist
                CodePaycodeBenefitCollection paycodeBenefitCollection = _access.GetCodePaycodeBenefit(user, paycodeCode);
                if (paycodeBenefitCollection.Count == 0)
                    paycodeBenefitCollection.Add(new CodePaycodeBenefit() { PaycodeCode = "" });

                //get paycode
                GetCodePaycode(user, paycodeCode, false)[0].CopyTo(paycodeBenefitCollection[0]);

                //get attached paycodes and add them to the paycodeBenefitCollection
                PaycodeAttachedPaycodeProvisionCollection attachedPaycodeProvisionCollection = GetPaycodeAttachedPaycodeProvision(user, paycodeCode);
                if (attachedPaycodeProvisionCollection.Count > 0)
                    paycodeBenefitCollection[0].AttachedPaycodes = attachedPaycodeProvisionCollection;

                return paycodeBenefitCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetCodePaycodeBenefit", ex);
                throw;
            }
        }
        public void UpdateCodePaycodeBenefit(DatabaseUser user, CodePaycodeBenefit paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //update existing paycode
                    UpdateCodePaycode(user, paycode, overrideConcurrencyCheckFlag);

                    //update existing or insert new benefit paycode
                    if (paycode.PaycodeBenefitId > 0)
                        _access.UpdateCodePaycodeBenefit(user, paycode);
                    else
                        _access.InsertCodePaycodeBenefit(user, paycode);

                    //delete existing attached paycodes
                    DeletePaycodeAttachedPaycodeProvision(user, paycode.PaycodeCode);

                    //null out the AttachedPaycodes collection if the PaycodeRateBasedOnCode is not "PAYDOLL" or "EIHOURS"
                    if (paycode.PaycodeRateBasedOnCode != "PAYDOLL" && paycode.PaycodeRateBasedOnCode != "EIHOURS")
                        paycode.AttachedPaycodes = null;

                    //insert new attached paycodes
                    foreach (PaycodeAttachedPaycodeProvision attachedPaycode in paycode.AttachedPaycodes)
                        InsertPaycodeAttachedPaycodeProvision(user, attachedPaycode);

                    if (paycodeImport)
                    {
                        //delete existing year end report mappings
                        _payrollAccess.DeletePaycodeYearEndReportMap(user, new PaycodeYearEndReportMap { PaycodeCode = paycode.PaycodeCode }, true);

                        //insert new year end report mappings
                        foreach (PaycodeYearEndReportMap yearEndReportMap in paycode.YearEndReportMappings)
                            _payrollAccess.InsertPaycodeYearEndReportMap(user, yearEndReportMap);

                        //delete existing offset associations 
                        _payrollAccess.DeletePaycodePayrollTransactionOffsetAssociation(user, new PaycodePayrollTransactionOffsetAssociation { PaycodeCode = paycode.PaycodeCode }, true);

                        //insert new offset associations
                        foreach (PaycodePayrollTransactionOffsetAssociation offsetAssociation in paycode.OffsetAssociations)
                            _payrollAccess.InsertPaycodePayrollTransactionOffsetAssociation(user, offsetAssociation);
                    }

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdateCodePaycodeBenefit", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public CodePaycodeBenefit InsertCodePaycodeBenefit(DatabaseUser user, CodePaycodeBenefit paycode)
        {
            try
            {
                CodePaycodeBenefit paycodeBenefit = new CodePaycodeBenefit();

                using (TransactionScope scope = new TransactionScope())
                {
                    //insert paycode
                    InsertCodePaycode(user, paycode);

                    //insert benefit paycode
                    paycodeBenefit = _access.InsertCodePaycodeBenefit(user, paycode);

                    //null out the AttachedPaycodes collection if the PaycodeRateBasedOnCode is not "PAYDOLL" or "EIHOURS"
                    if (paycode.PaycodeRateBasedOnCode != "PAYDOLL" && paycode.PaycodeRateBasedOnCode != "EIHOURS")
                        paycode.AttachedPaycodes = null;

                    //insert attached paycodes
                    foreach (PaycodeAttachedPaycodeProvision attachedPaycode in paycode.AttachedPaycodes)
                        InsertPaycodeAttachedPaycodeProvision(user, attachedPaycode);

                    //insert year end report mappings - this collection should be empty unless we are doing a paycode import
                    foreach (PaycodeYearEndReportMap yearEndReportMap in paycode.YearEndReportMappings)
                        _payrollAccess.InsertPaycodeYearEndReportMap(user, yearEndReportMap);

                    //insert offset associations - this collection should be empty unless we are doing a paycode import
                    foreach (PaycodePayrollTransactionOffsetAssociation offsetAssociation in paycode.OffsetAssociations)
                        _payrollAccess.InsertPaycodePayrollTransactionOffsetAssociation(user, offsetAssociation);

                    scope.Complete();
                }

                return paycodeBenefit;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - InsertCodePaycodeBenefit", ex);
                throw;
            }
        }
        #endregion

        #region code paycode deduction
        public CodePaycodeDeductionCollection GetCodePaycodeDeduction(DatabaseUser user, String paycodeCode)
        {
            try
            {
                //get deduction paycodes or create a new one if none exist
                CodePaycodeDeductionCollection paycodeDeductionCollection = _access.GetCodePaycodeDeduction(user, paycodeCode);
                if (paycodeDeductionCollection.Count == 0)
                    paycodeDeductionCollection.Add(new CodePaycodeDeduction() { PaycodeCode = "" });

                //get paycode
                GetCodePaycode(user, paycodeCode, false)[0].CopyTo(paycodeDeductionCollection[0]);

                //get attached paycodes and add them to the paycodeDeductionCollection
                PaycodeAttachedPaycodeProvisionCollection attachedPaycodeProvisionCollection = GetPaycodeAttachedPaycodeProvision(user, paycodeCode);
                if (attachedPaycodeProvisionCollection.Count > 0)
                    paycodeDeductionCollection[0].AttachedPaycodes = attachedPaycodeProvisionCollection;

                return paycodeDeductionCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetCodePaycodeDeduction", ex);
                throw;
            }
        }
        public void UpdateCodePaycodeDeduction(DatabaseUser user, CodePaycodeDeduction paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //update existing paycode
                    UpdateCodePaycode(user, paycode, overrideConcurrencyCheckFlag);

                    //update existing or insert new deduction paycode
                    if (paycode.PaycodeDeductionId > 0)
                        _access.UpdateCodePaycodeDeduction(user, paycode);
                    else
                        _access.InsertCodePaycodeDeduction(user, paycode);

                    //delete existing attached paycodes
                    DeletePaycodeAttachedPaycodeProvision(user, paycode.PaycodeCode);

                    //null out the AttachedPaycodes collection if the PaycodeRateBasedOnCode is not "PAYDOLL" or "EIHOURS"
                    if (paycode.PaycodeRateBasedOnCode != "PAYDOLL" && paycode.PaycodeRateBasedOnCode != "EIHOURS")
                        paycode.AttachedPaycodes = null;

                    //insert new attached paycodes
                    foreach (PaycodeAttachedPaycodeProvision attachedPaycode in paycode.AttachedPaycodes)
                        InsertPaycodeAttachedPaycodeProvision(user, attachedPaycode);

                    if (paycodeImport)
                    {
                        //delete existing year end report mappings
                        _payrollAccess.DeletePaycodeYearEndReportMap(user, new PaycodeYearEndReportMap { PaycodeCode = paycode.PaycodeCode }, true);

                        //insert new year end report mappings
                        foreach (PaycodeYearEndReportMap yearEndReportMap in paycode.YearEndReportMappings)
                            _payrollAccess.InsertPaycodeYearEndReportMap(user, yearEndReportMap);

                        //delete existing offset associations 
                        _payrollAccess.DeletePaycodePayrollTransactionOffsetAssociation(user, new PaycodePayrollTransactionOffsetAssociation { PaycodeCode = paycode.PaycodeCode }, true);

                        //insert new offset associations
                        foreach (PaycodePayrollTransactionOffsetAssociation offsetAssociation in paycode.OffsetAssociations)
                            _payrollAccess.InsertPaycodePayrollTransactionOffsetAssociation(user, offsetAssociation);
                    }

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdateCodePaycodeDeduction", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public CodePaycodeDeduction InsertCodePaycodeDeduction(DatabaseUser user, CodePaycodeDeduction paycode)
        {
            try
            {
                CodePaycodeDeduction paycodeDeduction = new CodePaycodeDeduction();

                using (TransactionScope scope = new TransactionScope())
                {
                    //insert paycode
                    InsertCodePaycode(user, paycode);

                    //insert deduction paycode
                    paycodeDeduction = _access.InsertCodePaycodeDeduction(user, paycode);

                    //null out the AttachedPaycodes collection if the PaycodeRateBasedOnCode is not "PAYDOLL" or "EIHOURS"
                    if (paycode.PaycodeRateBasedOnCode != "PAYDOLL" && paycode.PaycodeRateBasedOnCode != "EIHOURS")
                        paycode.AttachedPaycodes = null;

                    //insert attached paycodes
                    foreach (PaycodeAttachedPaycodeProvision attachedPaycode in paycode.AttachedPaycodes)
                        InsertPaycodeAttachedPaycodeProvision(user, attachedPaycode);

                    //insert year end report mappings - this collection should be empty unless we are doing a paycode import
                    foreach (PaycodeYearEndReportMap yearEndReportMap in paycode.YearEndReportMappings)
                        _payrollAccess.InsertPaycodeYearEndReportMap(user, yearEndReportMap);

                    //insert offset associations - this collection should be empty unless we are doing a paycode import
                    foreach (PaycodePayrollTransactionOffsetAssociation offsetAssociation in paycode.OffsetAssociations)
                        _payrollAccess.InsertPaycodePayrollTransactionOffsetAssociation(user, offsetAssociation);

                    scope.Complete();
                }

                return paycodeDeduction;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - InsertCodePaycodeDeduction", ex);
                throw;
            }
        }
        #endregion

        #region code paycode income
        public CodePaycodeIncomeCollection GetCodePaycodeIncome(DatabaseUser user, String paycodeCode)
        {
            try
            {
                //get income paycodes or create a new one if none exist
                CodePaycodeIncomeCollection paycodeIncomeCollection = _access.GetCodePaycodeIncome(user, paycodeCode);
                if (paycodeIncomeCollection.Count == 0)
                    paycodeIncomeCollection.Add(new CodePaycodeIncome() { PaycodeCode = "" });

                //get paycode
                GetCodePaycode(user, paycodeCode, false)[0].CopyTo(paycodeIncomeCollection[0]);

                return paycodeIncomeCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetCodePaycodeIncome", ex);
                throw;
            }
        }
        public void UpdateCodePaycodeIncome(DatabaseUser user, CodePaycodeIncome paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //update existing paycode
                    UpdateCodePaycode(user, paycode, overrideConcurrencyCheckFlag);

                    //update existing or insert new income paycode
                    if (paycode.PaycodeIncomeId > 0)
                        _access.UpdateCodePaycodeIncome(user, paycode);
                    else
                        _access.InsertCodePaycodeIncome(user, paycode);

                    if (paycodeImport)
                    {
                        //delete existing year end report mappings
                        _payrollAccess.DeletePaycodeYearEndReportMap(user, new PaycodeYearEndReportMap { PaycodeCode = paycode.PaycodeCode }, true);

                        //insert new year end report mappings
                        foreach (PaycodeYearEndReportMap yearEndReportMap in paycode.YearEndReportMappings)
                            _payrollAccess.InsertPaycodeYearEndReportMap(user, yearEndReportMap);

                        //delete existing offset associations 
                        _payrollAccess.DeletePaycodePayrollTransactionOffsetAssociation(user, new PaycodePayrollTransactionOffsetAssociation { PaycodeCode = paycode.PaycodeCode }, true);

                        //insert new offset associations
                        foreach (PaycodePayrollTransactionOffsetAssociation offsetAssociation in paycode.OffsetAssociations)
                            _payrollAccess.InsertPaycodePayrollTransactionOffsetAssociation(user, offsetAssociation);
                    }

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdateCodePaycodeIncome", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public CodePaycodeIncome InsertCodePaycodeIncome(DatabaseUser user, CodePaycodeIncome paycode)
        {
            try
            {
                CodePaycodeIncome paycodeIncome = new CodePaycodeIncome();

                using (TransactionScope scope = new TransactionScope())
                {
                    //insert paycode
                    InsertCodePaycode(user, paycode);

                    //insert income paycode
                    paycodeIncome = _access.InsertCodePaycodeIncome(user, paycode);

                    //insert year end report mappings - this collection should be empty unless we are doing a paycode import
                    foreach (PaycodeYearEndReportMap yearEndReportMap in paycode.YearEndReportMappings)
                        _payrollAccess.InsertPaycodeYearEndReportMap(user, yearEndReportMap);

                    //insert offset associations - this collection should be empty unless we are doing a paycode import
                    foreach (PaycodePayrollTransactionOffsetAssociation offsetAssociation in paycode.OffsetAssociations)
                        _payrollAccess.InsertPaycodePayrollTransactionOffsetAssociation(user, offsetAssociation);

                    scope.Complete();
                }

                return paycodeIncome;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - InsertCodePaycodeIncome", ex);
                throw;
            }
        }
        #endregion

        #region code paycode employer
        public CodePaycodeEmployerCollection GetCodePaycodeEmployer(DatabaseUser user, String paycodeCode)
        {
            try
            {
                //get employer paycodes or create a new one if none exist
                CodePaycodeEmployerCollection paycodeEmployerCollection = _access.GetCodePaycodeEmployer(user, paycodeCode);
                if (paycodeEmployerCollection.Count == 0)
                    paycodeEmployerCollection.Add(new CodePaycodeEmployer() { PaycodeCode = "" });

                //get paycode
                GetCodePaycode(user, paycodeCode, false)[0].CopyTo(paycodeEmployerCollection[0]);

                return paycodeEmployerCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetCodePaycodeEmployer", ex);
                throw;
            }
        }
        public void UpdateCodePaycodeEmployer(DatabaseUser user, CodePaycodeEmployer paycode, bool overrideConcurrencyCheckFlag, bool paycodeImport = false)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //update existing paycode
                    UpdateCodePaycode(user, paycode, overrideConcurrencyCheckFlag);

                    //update existing or insert new employer paycode
                    if (paycode.PaycodeEmployerId > 0)
                        _access.UpdateCodePaycodeEmployer(user, paycode);
                    else
                        _access.InsertCodePaycodeEmployer(user, paycode);

                    if (paycodeImport)
                    {
                        //delete existing year end report mappings
                        _payrollAccess.DeletePaycodeYearEndReportMap(user, new PaycodeYearEndReportMap { PaycodeCode = paycode.PaycodeCode }, true);

                        //insert new year end report mappings
                        foreach (PaycodeYearEndReportMap yearEndReportMap in paycode.YearEndReportMappings)
                            _payrollAccess.InsertPaycodeYearEndReportMap(user, yearEndReportMap);

                        //delete existing offset associations 
                        _payrollAccess.DeletePaycodePayrollTransactionOffsetAssociation(user, new PaycodePayrollTransactionOffsetAssociation { PaycodeCode = paycode.PaycodeCode }, true);

                        //insert new offset associations
                        foreach (PaycodePayrollTransactionOffsetAssociation offsetAssociation in paycode.OffsetAssociations)
                            _payrollAccess.InsertPaycodePayrollTransactionOffsetAssociation(user, offsetAssociation);
                    }

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdateCodePaycodeEmployer", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public CodePaycodeEmployer InsertCodePaycodeEmployer(DatabaseUser user, CodePaycodeEmployer paycode)
        {
            try
            {
                CodePaycodeEmployer paycodeEmployer = new CodePaycodeEmployer();

                using (TransactionScope scope = new TransactionScope())
                {
                    //insert paycode
                    InsertCodePaycode(user, paycode);

                    //insert income paycode
                    paycodeEmployer = _access.InsertCodePaycodeEmployer(user, paycode);

                    //insert year end report mappings - this collection should be empty unless we are doing a paycode import
                    foreach (PaycodeYearEndReportMap yearEndReportMap in paycode.YearEndReportMappings)
                        _payrollAccess.InsertPaycodeYearEndReportMap(user, yearEndReportMap);

                    //insert offset associations - this collection should be empty unless we are doing a paycode import
                    foreach (PaycodePayrollTransactionOffsetAssociation offsetAssociation in paycode.OffsetAssociations)
                        _payrollAccess.InsertPaycodePayrollTransactionOffsetAssociation(user, offsetAssociation);

                    scope.Complete();
                }

                return paycodeEmployer;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - InsertCodePaycodeEmployer", ex);
                throw;
            }
        }
        #endregion

        #region paycode attached paycode provision
        public PaycodeAttachedPaycodeProvisionCollection GetPaycodeAttachedPaycodeProvision(DatabaseUser user, String paycodeCode)
        {
            try
            {
                return _access.GetPaycodeAttachedPaycodeProvision(user, paycodeCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetPaycodeAttachedPaycodeProvision", ex);
                throw;
            }
        }
        public PaycodeAttachedPaycodeProvision InsertPaycodeAttachedPaycodeProvision(DatabaseUser user, PaycodeAttachedPaycodeProvision attachedPaycode)
        {
            try
            {
                return _access.InsertPaycodeAttachedPaycodeProvision(user, attachedPaycode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - InsertPaycodeAttachedPaycodeProvision", ex);
                throw;
            }
        }
        public void DeletePaycodeAttachedPaycodeProvision(DatabaseUser user, String paycodeCode)
        {
            try
            {
                _access.DeletePaycodeAttachedPaycodeProvision(user, paycodeCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - DeletePaycodeAttachedPaycodeProvision", ex);
                throw;
            }
        }
        #endregion

        #region paycode export
        public byte[] ExportPaycodeFile(DatabaseUser user, String paycodeCode, String paycodeType)
        {
            try
            {
                System.Xml.Serialization.XmlSerializer xml = null;
                System.IO.MemoryStream stream = new System.IO.MemoryStream();

                if (paycodeType == ((short)CodePaycode.PaycodeType.Benefit).ToString())
                {
                    CodePaycodeBenefit paycodeBenefit = GetCodePaycodeBenefit(user, paycodeCode)[0];
                    paycodeBenefit.YearEndReportMappings = _payrollAccess.GetPaycodeYearEndReportMap(user, paycodeCode);
                    paycodeBenefit.OffsetAssociations = _payrollAccess.GetPaycodePayrollTransactionOffsetAssociation(user, paycodeCode);

                    xml = new System.Xml.Serialization.XmlSerializer(typeof(CodePaycodeBenefit));

                    using (System.IO.TextWriter writer = new System.IO.StreamWriter(stream))
                    {
                        xml.Serialize(writer, paycodeBenefit);
                        writer.Close();
                    }
                }
                else if (paycodeType == ((short)CodePaycode.PaycodeType.Deduction).ToString())
                {
                    CodePaycodeDeduction paycodeDeduction = GetCodePaycodeDeduction(user, paycodeCode)[0];
                    paycodeDeduction.YearEndReportMappings = _payrollAccess.GetPaycodeYearEndReportMap(user, paycodeCode);
                    paycodeDeduction.OffsetAssociations = _payrollAccess.GetPaycodePayrollTransactionOffsetAssociation(user, paycodeCode);

                    xml = new System.Xml.Serialization.XmlSerializer(typeof(CodePaycodeDeduction));

                    using (System.IO.TextWriter writer = new System.IO.StreamWriter(stream))
                    {
                        xml.Serialize(writer, paycodeDeduction);
                        writer.Close();
                    }
                }
                else if (paycodeType == ((short)CodePaycode.PaycodeType.Income).ToString())
                {
                    CodePaycodeIncome paycodeIncome = GetCodePaycodeIncome(user, paycodeCode)[0];
                    paycodeIncome.YearEndReportMappings = _payrollAccess.GetPaycodeYearEndReportMap(user, paycodeCode);
                    paycodeIncome.OffsetAssociations = _payrollAccess.GetPaycodePayrollTransactionOffsetAssociation(user, paycodeCode);

                    xml = new System.Xml.Serialization.XmlSerializer(typeof(CodePaycodeIncome));

                    using (System.IO.TextWriter writer = new System.IO.StreamWriter(stream))
                    {
                        xml.Serialize(writer, paycodeIncome);
                        writer.Close();
                    }
                }

                return stream.ToArray();
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - ExportPaycodeFile", ex);
                throw;
            }
        }
        #endregion

        #region code system
        public CodeSystemCollection GetCodeSystem(DatabaseUser user)
        {
            return _access.GetCodeSystem(user);
        }
        public void UpdateCodeSystem(DatabaseUser user, CodeSystem code)
        {
            try
            {
                _access.UpdateCodeSystem(user, code);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdateCodeSystem", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public CodeSystem InsertCodeSystem(DatabaseUser user, CodeSystem code)
        {
            return _access.InsertCodeSystem(user, code);
        }
        public void DeleteCodeSystem(DatabaseUser user, CodeSystem code)
        {
            try
            {
                _access.DeleteCodeSystem(user, code);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - DeleteCodeSystem", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region paycode association type
        public PaycodeAssociationTypeCollection GetPaycodeAssociationType(DatabaseUser user, String paycodeAssociationTypeCode)
        {
            return _access.GetPaycodeAssociationType(user, paycodeAssociationTypeCode);
        }
        public PaycodeAssociationType InsertPaycodeAssociationType(DatabaseUser user, PaycodeAssociationType paycode)
        {
            return _access.InsertPaycodeAssociationType(user, paycode);
        }
        public void UpdatePaycodeAssociationType(DatabaseUser user, PaycodeAssociationType paycode)
        {
            try
            {
                _access.UpdatePaycodeAssociationType(user, paycode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdatePaycodeAssociationType", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeletePaycodeAssociationType(DatabaseUser user, PaycodeAssociationType paycode)
        {
            _access.DeletePaycodeAssociationType(user, paycode);
        }
        #endregion

        #region paycode association
        public PaycodeAssociationCollection GetPaycodeAssociation(DatabaseUser user, String paycodeAssociationTypeCode)
        {
            return _access.GetPaycodeAssociation(user, paycodeAssociationTypeCode);
        }
        public PaycodeAssociation InsertPaycodeAssociation(DatabaseUser user, PaycodeAssociation paycode)
        {
            return _access.InsertPaycodeAssociation(user, paycode);
        }
        public void UpdatePaycodeAssociation(DatabaseUser user, PaycodeAssociation paycode)
        {
            try
            {
                _access.UpdatePaycodeAssociation(user, paycode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdatePaycodeAssociation", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeletePaycodeAssociation(DatabaseUser user, PaycodeAssociation paycode)
        {
            try
            {
                _access.DeletePaycodeAssociation(user, paycode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - DeletePaycodeAssociation", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region code payroll processing group
        public Decimal GetCurrentPayrollYear(DatabaseUser user)
        {
            try
            {
                return _payrollAccess.GetCurrentPayrollYear(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - SelectCurrentPayrollYear", ex);
                throw;
            }
        }
        public PayrollProcessingGroupCollection GetPayrollProcessingGroup(DatabaseUser user)
        {
            try
            {
                return GetPayrollProcessingGroup(user, null);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetPayrollProcessingGroupOneArg", ex);
                throw;
            }
        }
        public PayrollProcessingGroupCollection GetPayrollProcessingGroup(DatabaseUser user, String payrollProcessGroupCode)
        {
            try
            {
                PayrollProcessingGroupCollection coll = new PayrollProcessingGroupCollection();
                PayrollPeriodCriteria criteria = new PayrollPeriodCriteria() { GetMinOpenPeriodFlag = true };

                coll = _access.GetPayrollProcessingGroup(user, payrollProcessGroupCode);

                foreach (PayrollProcessingGroup ppGroup in coll)
                {
                    criteria.PayrollProcessGroupCode = ppGroup.PayrollProcessGroupCode;

                    PayrollPeriodCollection ppCollection = _payrollAccess.GetPayrollPeriod(user, criteria);

                    if (ppCollection != null && ppCollection.Count > 0)
                    {
                        ppGroup.PeriodYear = ppCollection[0].PeriodYear;
                        ppGroup.Period = ppCollection[0].Period;
                        ppGroup.PeriodStartDate = ppCollection[0].StartDate;
                        ppGroup.PeriodCutoffDate = ppCollection[0].CutoffDate;
                    }
                }

                return coll;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetPayrollProcessingGroupTwoArg", ex);
                throw;
            }
        }
        public void UpdatePayrollProcessingGroup(DatabaseUser user, PayrollProcessingGroup ppG)
        {
            try
            {
                //check for English and French descriptions; if no French, then default it to English description
                if (ppG.PayrollProcessGroupFrenchDesc == "")
                    ppG.PayrollProcessGroupFrenchDesc = ppG.PayrollProcessGroupEnglishDesc;

                _access.UpdatePayrollProcessingGroup(user, ppG);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdatePayrollProcessingGroup", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public PayrollProcessingGroup InsertPayrollProcessingGroup(DatabaseUser user, PayrollProcessingGroup ppG)
        {
            try
            {
                //check for English and French descriptions; if no French, then default it to English description
                if (ppG.PayrollProcessGroupFrenchDesc == "")
                    ppG.PayrollProcessGroupFrenchDesc = ppG.PayrollProcessGroupEnglishDesc;

                PayrollProcessingGroup tempPPG = new PayrollProcessingGroup();
                PayrollPeriod tempPP = new PayrollPeriod()
                {
                    PayrollProcessGroupCode = ppG.PayrollProcessGroupCode,
                    PeriodYear = ppG.PeriodYear,
                    Period = ppG.Period,
                    StartDate = ppG.PeriodStartDate,
                    CutoffDate = ppG.PeriodCutoffDate,
                    UpdateUser = user.UserName
                };

                using (TransactionScope scope = new TransactionScope())
                {
                    //insert the code
                    tempPPG = _access.InsertPayrollProcessingGroupCode(user, ppG);
                    tempPPG = _access.InsertPayrollProcessingGroupCodeDesc(user, tempPPG);

                    //insert payroll period
                    _payrollAccess.InsertPayrollPeriod(user, tempPP);

                    scope.Complete();
                }

                return tempPPG;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - InsertPayrollProcessingGroup", ex);
                throw;
            }
        }
        public void DeletePayrollProcessingGroup(DatabaseUser user, PayrollProcessingGroup ppG)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //delete the payroll periods
                    PayrollPeriodCollection payrollPeriods = _payrollAccess.GetPayrollPeriodIdFromPayrollProcessGroupCode(user, new PayrollBatchCriteria() { PayrollProcessGroupCode = ppG.PayrollProcessGroupCode });
                    foreach (PayrollPeriod payrollPeriod in payrollPeriods)
                        PayrollManagement.DeletePayrollPeriod(user, payrollPeriod);

                    //convert to a CodeTable object and call "DeleteCodeTableData" above
                    CodeTable tmpCodeTable = ConvertToCodeTable(user, ppG);
                    DeleteCodeTable(user, tmpCodeTable, "code_payroll_process_group");

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - DeletePayrollProcessingGroup", ex);
                throw ex;
            }
        }
        public CodeTable ConvertToCodeTable(DatabaseUser user, PayrollProcessingGroup ppG)
        {
            try
            {
                CodeTable tmp = new CodeTable();
                tmp.CodeTableId = ppG.PayrollProcessGroupCode;
                tmp.RowVersion = ppG.RowVersion;
                tmp.UpdateUser = ppG.UpdateUser;
                tmp.Descriptions = new CodeTableDescriptionCollection();

                CodeTableDescription tmpCodeTableEngDesc = new CodeTableDescription { Id = ppG.PayrollProcessGroupCodeEnglishDescId, LanguageCode = ppG.PayrollProcessGroupCodeEnglishDescId.ToString() };
                CodeTableDescription tmpCodeTableFrDesc = new CodeTableDescription { Id = ppG.PayrollProcessGroupCodeFrenchDescId, LanguageCode = ppG.PayrollProcessGroupCodeFrenchDescId.ToString() };

                tmp.Descriptions.Add(tmpCodeTableEngDesc);
                tmp.Descriptions.Add(tmpCodeTableFrDesc);

                return tmp;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - ConvertToCodeTable", ex);
                throw;
            }
        }
        #endregion

        #region payroll process group override period
        public PayrollProcessGroupOverridePeriodCollection GetPayrollProcessGroupOverridePeriod(DatabaseUser user, String payrollProcessGroupCode)
        {
            try
            {
                return _access.GetPayrollProcessGroupOverridePeriod(user, payrollProcessGroupCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetPayrollProcessGroupOverridePeriod", ex);
                throw;
            }
        }
        public PayrollProcessGroupOverridePeriod InsertPayrollProcessGroupOverridePeriod(DatabaseUser user, PayrollProcessGroupOverridePeriod overridePeriod)
        {
            try
            {
                return _access.InsertPayrollProcessGroupOverridePeriod(user, overridePeriod);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - InsertPayrollProcessGroupOverridePeriod", ex);
                throw;
            }
        }
        public void UpdatePayrollProcessGroupOverridePeriod(DatabaseUser user, PayrollProcessGroupOverridePeriod overridePeriod)
        {
            try
            {
                _access.UpdatePayrollProcessGroupOverridePeriod(user, overridePeriod);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdatePayrollProcessGroupOverridePeriod", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeletePayrollProcessGroupOverridePeriod(DatabaseUser user, PayrollProcessGroupOverridePeriod overridePeriod)
        {
            try
            {
                _access.DeletePayrollProcessGroupOverridePeriod(user, overridePeriod);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - DeletePayrollProcessGroupOverridePeriod", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region third party
        public CodeThirdPartyCollection GetCodeThirdParty(DatabaseUser user, string thirdPartyCode)
        {
            return _access.GetCodeThirdParty(user, thirdPartyCode);
        }

        public void DeleteThirdPartyCode(DatabaseUser user, CodeThirdParty thirdParty)
        {
            try
            {
                _access.DeleteThirdPartyCode(user, thirdParty);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - DeleteThirdPartyCode", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }

        public void UpdateThirdPartyCode(DatabaseUser user, CodeThirdParty thirdParty)
        {
            try
            {
                _access.UpdateThirdPartyCode(user, thirdParty);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdateThirdPartyCode", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }

        public CodeThirdParty InsertThirdPartyCode(DatabaseUser user, CodeThirdParty thirdParty)
        {
            return _access.InsertThirdPartyCode(user, thirdParty);
        }
        #endregion

        #region code wsib
        public CodeWsibCollection GetCodeWsib(DatabaseUser user, String wsibCode)
        {
            return _access.GetCodeWsib(user, wsibCode);
        }
        public void UpdateCodeWsib(DatabaseUser user, CodeWsib codeWsib)
        {
            try
            {
                _access.UpdateCodeWsib(user, codeWsib);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdateCodeWsib", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public CodeWsib InsertCodeWsib(DatabaseUser user, CodeWsib codeWsib)
        {
            return _access.InsertCodeWsib(user, codeWsib);
        }
        public void DeleteCodeWsib(DatabaseUser user, CodeWsib codeWsib)
        {
            try
            {
                _access.DeleteCodeWsib(user, codeWsib);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - DeleteCodeWsib", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public CodeWsibEffectiveCollection GetCodeWsibEffective(DatabaseUser user, String wsibCode)
        {
            return _access.GetCodeWsibEffective(user, wsibCode);
        }
        public CodeWsibEffective InsertCodeWsibEffective(DatabaseUser user, CodeWsibEffective codeWsibEffective)
        {
            return _access.InsertCodeWsibEffective(user, codeWsibEffective);
        }
        public void UpdateCodeWsibEffective(DatabaseUser user, CodeWsibEffective codeWsibEffective)
        {
            try
            {
                _access.UpdateCodeWsibEffective(user, codeWsibEffective);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdateCodeWsibEffective", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteCodeWsibEffective(DatabaseUser user, CodeWsibEffective codeWsibEffective)
        {
            try
            {
                _access.DeleteCodeWsibEffective(user, codeWsibEffective);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - DeleteCodeWsibEffective", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region code wcb cheque remittance frequency
        public CodeWcbChequeRemittanceFrequencyCollection GetCodeWcbChequeRemittanceFrequency(DatabaseUser user, String wcbChequeRemittanceFrequencyCode)
        {
            try
            {
                return _access.GetCodeWcbChequeRemittanceFrequency(user, wcbChequeRemittanceFrequencyCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetCodeWcbChequeRemittanceFrequency", ex);
                throw;
            }
        }
        public CodeWcbChequeRemittanceFrequency InsertCodeWcbChequeRemittanceFrequency(DatabaseUser user, CodeWcbChequeRemittanceFrequency codeWcbChequeRemittanceFrequency)
        {
            try
            {
                return _access.InsertCodeWcbChequeRemittanceFrequency(user, codeWcbChequeRemittanceFrequency);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - InsertCodeWcbChequeRemittanceFrequency", ex);
                throw;
            }
        }
        public void UpdateCodeWcbChequeRemittanceFrequency(DatabaseUser user, CodeWcbChequeRemittanceFrequency codeWcbChequeRemittanceFrequency)
        {
            try
            {
                _access.UpdateCodeWcbChequeRemittanceFrequency(user, codeWcbChequeRemittanceFrequency);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdateCodeWcbChequeRemittanceFrequency", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteCodeWcbChequeRemittanceFrequency(DatabaseUser user, CodeWcbChequeRemittanceFrequency codeWcbChequeRemittanceFrequency)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    DeleteWcbChequeRemittanceFrequencyDetail(user, new WcbChequeRemittanceFrequencyDetail() { WcbChequeRemittanceFrequencyCode = codeWcbChequeRemittanceFrequency.WcbChequeRemittanceFrequencyCode });
                    _access.DeleteCodeWcbChequeRemittanceFrequency(user, codeWcbChequeRemittanceFrequency);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - DeleteCodeWcbChequeRemittanceFrequency", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public WcbChequeRemittanceFrequencyDetailCollection GetWcbChequeRemittanceFrequencyDetail(DatabaseUser user, String wcbChequeRemittanceFrequencyCode)
        {
            try
            {
                return _access.GetWcbChequeRemittanceFrequencyDetail(user, wcbChequeRemittanceFrequencyCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetWcbChequeRemittanceFrequencyDetail", ex);
                throw;
            }
        }
        public WcbChequeRemittanceFrequencyDetail InsertWcbChequeRemittanceFrequencyDetail(DatabaseUser user, WcbChequeRemittanceFrequencyDetail wcbChequeRemittanceFrequencyDetail)
        {
            try
            {
                return _access.InsertWcbChequeRemittanceFrequencyDetail(user, wcbChequeRemittanceFrequencyDetail);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - InsertWcbChequeRemittanceFrequencyDetail", ex);
                throw;
            }
        }
        public void UpdateWcbChequeRemittanceFrequencyDetail(DatabaseUser user, WcbChequeRemittanceFrequencyDetail wcbChequeRemittanceFrequencyDetail)
        {
            try
            {
                _access.UpdateWcbChequeRemittanceFrequencyDetail(user, wcbChequeRemittanceFrequencyDetail);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdateWcbChequeRemittanceFrequencyDetail", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteWcbChequeRemittanceFrequencyDetail(DatabaseUser user, WcbChequeRemittanceFrequencyDetail wcbChequeRemittanceFrequencyDetail)
        {
            try
            {
                _access.DeleteWcbChequeRemittanceFrequencyDetail(user, wcbChequeRemittanceFrequencyDetail);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - DeleteWcbChequeRemittanceFrequencyDetail", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region security
        public CodeCollection GetRoleDescription(DatabaseUser user, String securityRoleType, bool useSystemRoleId, bool? worklinksAdministrationFlag)
        {
            try
            {
                CodeCollection rtn = new CodeCollection();
                RoleDescriptionCollection collection = _securityAccess.GetRoleDescriptionType(user, securityRoleType, worklinksAdministrationFlag);

                //regardless of language used, desc will be stored in "EnglishDesc" from the proc so CodeObject will always be correct
                foreach (RoleDescription item in collection)
                    rtn.Add(new CodeObject() { Code = useSystemRoleId ? item.SystemRoleId.ToString() : item.Key, Description = item.EnglishDesc });

                return rtn;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetRoleDescription", ex);
                throw;
            }
        }
        public CodeCollection GetRoleDescriptionCombo(DatabaseUser user, String securityRoleType, String selectedValue, bool useSystemRoleId, bool? worklinksAdministrationFlag)
        {
            try
            {
                CodeCollection rtn = new CodeCollection();
                CodeObject obj = GetRoleDescription(user, securityRoleType, useSystemRoleId, worklinksAdministrationFlag)[selectedValue];

                if (obj != null)
                    rtn.Add(obj);

                return rtn;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - GetRoleDescriptionCombo", ex);
                throw;
            }
        }
        #endregion

        #region revenue quebec business 
        public RevenueQuebecBusiness GetRevenueQuebecBusiness(DatabaseUser user)
        {
            return _access.GetRevenueQuebecBusiness(user);
        }
        public bool ValidateModulus11CheckDigit(string businessId)
        {
            string first9Digits = null;
            string lastDigit = null;
            int lastDigitInteger = -1;
            int[] factors = null;
            int factorsIndex = -1;
            int factor = -1;
            string digit = null;
            int digitInteger = -1;
            int multiply = -1;
            int results = -1;
            int remainder = -1;
            int checkDigit = -1;

            // Get the first 9 digits
            first9Digits = businessId.Substring(0, 9);

            // Get the last digit
            lastDigit = businessId.Substring(9);
            lastDigitInteger = Int32.Parse(lastDigit);

            // Define the factors
            factors = new int[] { 2, 3, 4, 5, 6, 7 };

            // Loop through the first 9 digits in reverse order 
            factorsIndex = -1;
            results = 0;
            for (int i = first9Digits.Length - 1; i >= 0; i--)
            {
                // Get the Digit
                digit = first9Digits.Substring(i, 1);
                digitInteger = Int32.Parse(digit);

                // Determine the Factor
                factorsIndex++;
                if (factorsIndex < factors.Length)
                {
                    factor = factors[factorsIndex];
                }
                else
                {
                    factorsIndex = 0;
                    factor = factors[factorsIndex];
                }

                // Calculate the multiplication
                multiply = digitInteger * factor;

                // Add in the results
                results += multiply;
            } // for (int i = first9Digits.Length - 1; i >= 0; i--)

            // Calculate the modulus 11 of the results 
            remainder = results % 11;

            // Calculate the Check Digit
            checkDigit = 11 - remainder;
            if (checkDigit > 9)
            {
                checkDigit = checkDigit - 10;
            }

            if (lastDigitInteger == checkDigit)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public RevenueQuebecBusiness UpdateRevenueQuebecBusiness(DatabaseUser user, RevenueQuebecBusiness revenueQuebecBusiness)
        {
            try
            {
                return _access.UpdateRevenueQuebecBusiness(user, revenueQuebecBusiness);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - UpdateRevenueQuebecBusiness", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion
    }
}