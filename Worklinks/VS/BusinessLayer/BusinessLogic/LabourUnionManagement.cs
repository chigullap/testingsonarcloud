﻿using System;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Transactions;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.DataLayer.DataAccess;
using WorkLinks.DataLayer.DataAccess.SqlServer;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class LabourUnionManagement
    {
        #region fields
        LabourUnionAccess _labourUnionAccess = null;
        EmployeeAccess _employeeAccess = null;
        PersonAccess _personAccess = null;
        #endregion

        #region properties
        public LabourUnionAccess LabourUnionAccess
        {
            get
            {
                if (_labourUnionAccess == null)
                    _labourUnionAccess = new LabourUnionAccess();

                return _labourUnionAccess;
            }
        }
        public EmployeeAccess EmployeeAccess
        {
            get
            {
                if (_employeeAccess == null)
                    _employeeAccess = new EmployeeAccess();

                return _employeeAccess;
            }
        }
        public PersonAccess PersonAccess
        {
            get
            {
                if (_personAccess == null)
                    _personAccess = new PersonAccess();

                return _personAccess;
            }
        }
        #endregion

        #region labour union
        public LabourUnionSummaryCollection GetLabourUnion(DatabaseUser user, long? labourUnionId)
        {
            try
            {
                return LabourUnionAccess.GetLabourUnion(user, labourUnionId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "LabourUnionManagement - GetLabourUnionSummary", ex);
                throw;
            }
        }
        public LabourUnionSummary UpdateUnionContactData(DatabaseUser user, LabourUnionSummary union)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    UpdateLabourUnion(user, union);
                    PersonAccess.UpdateUnionContactPerson(user, union);

                    scope.Complete();
                }

                return union;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "LabourUnionManagement - UpdateUnionContactData", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                {
                    EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                    throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                }
                else
                    throw;
            }
        }
        public void UpdateLabourUnion(DatabaseUser user, LabourUnion unionContact)
        {
            try
            {
                LabourUnionAccess.UpdateLabourUnion(user, unionContact);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "LabourUnionManagement - UpdateUnionContact", ex);
                throw;
            }
        }
        public void DeleteLabourUnion(DatabaseUser user, LabourUnion unionContact)
        {
            try
            {
                LabourUnionAccess.DeleteLabourUnion(user, unionContact);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "LabourUnionManagement - DeleteUnionContact", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                {
                    EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                    throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                }
                else
                    throw;
            }
        }
        public LabourUnionSummary InsertUnionContactInformationSummaryData(DatabaseUser user, LabourUnionSummary unionContact)
        {
            try
            {
                Person unionPerson = new Person();
                unionPerson = GetPersonData(user, unionContact);

                //insert the new person record
                unionPerson = PersonAccess.InsertPerson(user, user.DatabaseName, unionPerson);

                //Now add to the CODE_UNION and UNION_CONTACT tables
                LabourUnion updatedUnionContactInfo = PrepareInsertUnionContact(user, unionContact, unionPerson);
                updatedUnionContactInfo = InsertUnionContact(user, updatedUnionContactInfo);

                LabourUnionSummary newSummaryObject = new LabourUnionSummary();
                newSummaryObject = GetNewValues(user, newSummaryObject, unionPerson, updatedUnionContactInfo);

                return newSummaryObject;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "LabourUnionManagement - InsertUnionContactInformationSummaryData", ex);
                throw;
            }
        }
        //Extract and prepare Person data from UnionContactInformationSummary object for INSERT
        public Person GetPersonData(DatabaseUser user, LabourUnionSummary unionContact)
        {
            try
            {
                Person unionPerson = new Person();

                unionPerson.FirstName = unionContact.FirstName;
                unionPerson.LastName = unionContact.LastName;
                unionPerson.KnownAsName = "N/A";
                unionPerson.CreateUser = unionContact.UpdateUser;
                unionPerson.UpdateUser = unionContact.UpdateUser;

                return unionPerson;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "LabourUnionManagement - GetPersonData", ex);
                throw;
            }
        }
        //prepares the UnionContactInformation object for INSERT
        public LabourUnion PrepareInsertUnionContact(DatabaseUser user, LabourUnion unionContact, Person person)
        {
            try
            {
                LabourUnion updatedUnionInfo = new LabourUnion();

                updatedUnionInfo.PersonId = person.PersonId;
                updatedUnionInfo.EnglishDescription = unionContact.EnglishDescription;
                updatedUnionInfo.FrenchDescription = unionContact.FrenchDescription;
                updatedUnionInfo.EffectiveDate = unionContact.EffectiveDate;
                updatedUnionInfo.UpdateUser = unionContact.UpdateUser;
                updatedUnionInfo.UpdateDatetime = unionContact.UpdateDatetime;

                return updatedUnionInfo;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "LabourUnionManagement - PrepareInsertUnionContact", ex);
                throw;
            }
        }
        //this takes the updated PERSON, UNION_CONTACT, and CODE_UNION objects and puts them back in a summary
        public LabourUnionSummary GetNewValues(DatabaseUser user, LabourUnionSummary unionSummary, Person person, LabourUnion unionContact)
        {
            try
            {
                unionSummary.PersonId = person.PersonId;
                unionSummary.LastName = person.LastName;
                unionSummary.FirstName = person.FirstName;
                unionSummary.RowVersion = person.RowVersion;
                unionSummary.LabourUnionId = unionContact.LabourUnionId;
                unionSummary.EnglishDescription = unionContact.EnglishDescription;
                unionSummary.FrenchDescription = unionContact.FrenchDescription;
                unionSummary.EffectiveDate = unionContact.EffectiveDate;
                unionSummary.UpdateUser = unionContact.UpdateUser;
                unionSummary.UpdateDatetime = unionContact.UpdateDatetime;

                return unionSummary;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "LabourUnionManagement - GetNewValues", ex);
                throw;
            }
        }
        public LabourUnion InsertUnionContact(DatabaseUser user, LabourUnion unionContact)
        {
            try
            {
                return LabourUnionAccess.InsertLabourUnion(user, unionContact);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "LabourUnionManagement - InsertUnionContact", ex);
                throw;
            }
        }
        #endregion

        #region union collective agreement
        public UnionCollectiveAgreementCollection GetUnionCollectiveAgreement(DatabaseUser user, long? labourUnionId)
        {
            try
            {
                return LabourUnionAccess.GetCollectiveAgreement(user, labourUnionId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "LabourUnionManagement - GetUnionCollectiveAgreement", ex);
                throw;
            }
        }
        public UnionCollectiveAgreement InsertCollectiveAgreement(DatabaseUser user, UnionCollectiveAgreement collectiveAgreement)
        {
            try
            {
                if (collectiveAgreement.AttachmentObjectCollection != null && collectiveAgreement.AttachmentObjectCollection.Count > 0)
                {
                    EmployeeAccess.InsertAttachment(user, collectiveAgreement.AttachmentObjectCollection[0]);
                    collectiveAgreement.AttachmentId = collectiveAgreement.AttachmentObjectCollection[0].AttachmentId;
                }
                else
                    collectiveAgreement.AttachmentId = null;

                return LabourUnionAccess.InsertUnionCollectiveAgreement(user, collectiveAgreement);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "LabourUnionManagement - InsertCollectiveAgreement", ex);
                throw;
            }
        }
        public void UpdateUnionCollectiveAgreement(DatabaseUser user, UnionCollectiveAgreement collectiveAgreement)
        {
            try
            {
                if (collectiveAgreement.AttachmentObjectCollection != null)
                {
                    if (collectiveAgreement.AttachmentObjectCollection[0].AttachmentId == -1)
                        EmployeeAccess.InsertAttachment(user, collectiveAgreement.AttachmentObjectCollection[0]);
                    else
                        EmployeeAccess.UpdateAttachment(user, collectiveAgreement.AttachmentObjectCollection[0]);

                    collectiveAgreement.AttachmentId = collectiveAgreement.AttachmentObjectCollection[0].AttachmentId;
                }

                LabourUnionAccess.UpdateUnionCollectiveAgreement(user, collectiveAgreement);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "LabourUnionManagement - UpdateUnionCollectiveAgreement", ex);
                throw;
            }
        }
        public void DeleteCollectiveAgreement(DatabaseUser user, UnionCollectiveAgreement collectiveAgreement)
        {
            try
            {
                LabourUnionAccess.DeleteUnionCollectiveAgreement(user, collectiveAgreement);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "LabourUnionManagement - DeleteCollectiveAgreement", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                {
                    EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                    throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                }
                else
                    throw;
            }
        }
        #endregion
    }
}