﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Transactions;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.DataLayer.DataAccess;
using WorkLinks.DataLayer.DataAccess.SqlServer;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class WizardManagement
    {
        #region fields
        private WizardAccess _wizardAccess = null;
        private EmployeeManagement _employeeManagement = null;
        private PersonManagement _personManagement = null;
        private WorkLinksDynamicsManagement _workLinksDynamicsManagement = null;
        #endregion

        #region properties
        private WizardAccess WizardAccess
        {
            get
            {
                if (_wizardAccess == null)
                    _wizardAccess = new WizardAccess();

                return _wizardAccess;
            }
        }
        private EmployeeManagement EmployeeManagement
        {
            get
            {
                if (_employeeManagement == null)
                    _employeeManagement = new EmployeeManagement();

                return _employeeManagement;
            }
        }
        private PersonManagement PersonManagement
        {
            get
            {
                if (_personManagement == null)
                    _personManagement = new PersonManagement();

                return _personManagement;
            }
        }
        private WorkLinksDynamicsManagement WorkLinksDynamicsManagement
        {
            get
            {
                if (_workLinksDynamicsManagement == null)
                    _workLinksDynamicsManagement = new WorkLinksDynamicsManagement();

                return _workLinksDynamicsManagement;
            }
        }
        #endregion

        #region wizard
        public WizardCacheCollection GetWizardCache(DatabaseUser user, long wizardId, long? wizardCacheId, WizardCache.ItemType itemtype)
        {
            return GetWizardCache(user, wizardId, wizardCacheId, itemtype.Equals(WizardCache.ItemType.None) ? null : itemtype.ToString());
        }
        public WizardCacheCollection GetWizardCache(DatabaseUser user, long wizardId, long? wizardCacheId, String itemName)
        {
            try
            {
                //try to get existing wizard cache
                WizardCacheCollection wizardCaches = WizardAccess.GetWizardCache(user, wizardCacheId);

                if (wizardCaches.Count < 1) //only 1 returned
                {
                    wizardCaches = new WizardCacheCollection();
                    wizardCaches.AddNew();
                    wizardCaches[0].WizardId = wizardId;
                }

                wizardCaches[0].Items = WizardAccess.GetWizardCacheItem(user, wizardId, wizardCacheId, itemName);

                return wizardCaches;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WizardManagement - GetWizardCache", ex);
                throw;
            }
        }
        public Dictionary<String, WizardCache> GetWizardCache(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            Dictionary<String, WizardCache> wizardCache = new Dictionary<String, WizardCache>();

            //get the pending employees from the database and add them to the dictionary
            foreach (WizardCache item in GetWizardCacheBatch(user, importExternalIdentifiers))
                wizardCache.Add(item.ImportExternalIdentifier, item);

            return wizardCache;
        }
        public WizardCacheCollection GetWizardCacheBatch(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            try
            {
                WizardCacheCollection wizardCacheCollection = WizardAccess.GetWizardCacheBatch(user, importExternalIdentifiers);
                WizardCacheItemCollection wizardCacheItemCollection = WizardAccess.GetWizardCacheItemBatch(user, importExternalIdentifiers);

                foreach (WizardCache wizardCache in wizardCacheCollection)
                {
                    wizardCache.Items = new WizardCacheItemCollection();

                    foreach (WizardCacheItem item in wizardCacheItemCollection)
                    {
                        if (item.WizardCacheId == wizardCache.WizardCacheId)
                            wizardCache.Items.Add(item);
                    }
                }

                return wizardCacheCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WizardManagement - GetWizardCacheBatch", ex);
                throw;
            }
        }
        public void ImportWizardCache(DatabaseUser user, WizardCacheImportCollection importCollection)
        {
            try
            {
                WizardAccess.ImportWizardCache(user, importCollection);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WizardManagement - ImportWizardCache", ex);
                throw;
            }
        }
        public void UpdateWizardCache(DatabaseUser user, WizardCache wizard)
        {
            try
            {
                if (wizard.WizardCacheId < 0)
                    WizardAccess.InsertWizardCache(user, wizard);
                else
                    WizardAccess.UpdateWizardCache(user, wizard);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WizardManagement - UpdateWizardCache", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void UpdateWizardCacheItem(DatabaseUser user, WizardCacheItem item)
        {
            try
            {
                if (item.WizardCacheItemId < 0)
                    WizardAccess.InsertWizardCacheItem(user, item);
                else
                    WizardAccess.UpdateWizardCacheItem(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WizardManagement - UpdateWizardCacheItem", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteWizardEmployee(DatabaseUser user, long wizardCacheId)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    WizardAccess.DeleteWizardEmployee(user, wizardCacheId);
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WizardManagement - DeleteWizardEmployee", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public Employee AddEmployee(DatabaseUser user, long wizardCacheId, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool autoAddSecondaryPositions)
        {
            try
            {
                Employee employee = null;
                WizardCache wizard = GetWizardCache(user, 1, wizardCacheId, WizardCache.ItemType.None)[0];

                using (TransactionScope scope = new TransactionScope())
                {
                    //save employee
                    employee = ((EmployeeCollection)wizard.Items[WizardCache.ItemType.EmployeeBiographicalControl].Data)[0];

                    //check if company has auto employee number generator turned on
                    if (autoGenerateEmployeeNumber.ToLower() == "on")
                    {
                        //get the last employee number used, add 1 to it, store it in the database, and return it
                        long sequenceId = WorkLinksDynamicsManagement.GetSequenceNumber(user, "EMPLOYEE_NUMBER");
                        String generatedEmployeeNumber = String.Format("{0:" + employeeNumberFormat + "}", sequenceId);
                        employee.EmployeeNumber = generatedEmployeeNumber;
                    }

                    EmployeeManagement.InsertEmployee(user, employee);

                    //save addresses
                    if (wizard.Items[WizardCache.ItemType.AddressDetailControl] != null)
                    {
                        PersonAddressCollection addresses = ((PersonAddressCollection)wizard.Items[WizardCache.ItemType.AddressDetailControl].Data);
                        if (addresses != null)
                        {
                            foreach (PersonAddress address in addresses)
                            {
                                address.PersonId = employee.PersonId;
                                PersonManagement.InsertPersonAddress(user, address);
                            }
                        }
                    }

                    //save phones
                    if (wizard.Items[WizardCache.ItemType.PersonPhoneControl] != null)
                    {
                        PersonContactChannelCollection phones = ((PersonContactChannelCollection)wizard.Items[WizardCache.ItemType.PersonPhoneControl].Data);
                        if (phones != null)
                        {
                            foreach (PersonContactChannel phone in phones)
                            {
                                phone.PersonId = employee.PersonId;
                                PersonManagement.InsertPersonContactChannel(user, phone);
                            }
                        }
                    }

                    //save Emails
                    if (wizard.Items[WizardCache.ItemType.PersonEmailControl] != null)
                    {
                        PersonContactChannelCollection emails = ((PersonContactChannelCollection)wizard.Items[WizardCache.ItemType.PersonEmailControl].Data);
                        if (emails != null)
                        {
                            foreach (PersonContactChannel email in emails)
                            {
                                email.PersonId = employee.PersonId;
                                PersonManagement.InsertPersonContactChannel(user, email);
                            }
                        }
                    }

                    //save Employee Information Control
                    if (wizard.Items[WizardCache.ItemType.EmployeeEmploymentInformationControl] != null)
                    {
                        EmployeeEmploymentInformationCollection employeeEmploymentInformationCollection = ((EmployeeEmploymentInformationCollection)wizard.Items[WizardCache.ItemType.EmployeeEmploymentInformationControl].Data);
                        if (employeeEmploymentInformationCollection != null)
                        {
                            foreach (EmployeeEmploymentInformation employeeEmploymentInformation in employeeEmploymentInformationCollection)
                            {
                                employeeEmploymentInformation.EmployeeId = employee.EmployeeId;
                                EmployeeManagement.InsertEmployeeEmploymentInformation(user, employeeEmploymentInformation);
                            }
                        }
                    }

                    //save Employee Position
                    if (wizard.Items[WizardCache.ItemType.EmployeePositionViewControl] != null)
                    {
                        EmployeePosition employeePosition = ((EmployeePositionCollection)wizard.Items[WizardCache.ItemType.EmployeePositionViewControl].Data)[0];
                        employeePosition.EmployeeId = employee.EmployeeId;
                        employeePosition.SetDefaultEffectiveDates();

                        EmployeeManagement.InsertEmployeePosition(user, employeePosition, null, null, null, null, null, null, null, autoAddSecondaryPositions); //the second last two "null" args are used for Benefited leave, leave and termination which is not used in the wizard
                    }

                    //save Statutory Deductions
                    if (wizard.Items[WizardCache.ItemType.StatutoryDeductionViewControl] != null)
                    {
                        StatutoryDeductionCollection statutoryDeductionCollection = ((StatutoryDeductionCollection)wizard.Items[WizardCache.ItemType.StatutoryDeductionViewControl].Data);
                        if (statutoryDeductionCollection != null)
                        {
                            foreach (StatutoryDeduction statutoryDeduction in statutoryDeductionCollection)
                            {
                                statutoryDeduction.EmployeeId = employee.EmployeeId;
                                EmployeeManagement.InsertStatutoryDeductionByType(user, statutoryDeduction);
                            }
                        }
                    }

                    //save Employee Banking Information
                    if (wizard.Items[WizardCache.ItemType.EmployeeBankingInformationControl] != null)
                    {
                        EmployeeBankingCollection employeeBankingCollection = ((EmployeeBankingCollection)wizard.Items[WizardCache.ItemType.EmployeeBankingInformationControl].Data);
                        if (employeeBankingCollection != null)
                        {
                            foreach (EmployeeBanking employeeBanking in employeeBankingCollection)
                            {
                                employeeBanking.EmployeeId = employee.EmployeeId;
                                EmployeeManagement.InsertEmployeeBanking(user, employeeBanking);
                            }
                        }
                    }

                    //save Employee Paycode
                    if (wizard.Items[WizardCache.ItemType.EmployeePaycodeModuleControl] != null)
                    {
                        EmployeePaycodeCollection employeePaycodeCollection = ((EmployeePaycodeCollection)wizard.Items[WizardCache.ItemType.EmployeePaycodeModuleControl].Data);
                        if (employeePaycodeCollection != null)
                        {
                            foreach (EmployeePaycode employeePaycode in employeePaycodeCollection)
                            {
                                employeePaycode.EmployeeId = employee.EmployeeId;
                                EmployeeManagement.InsertEmployeePaycode(user, employeePaycode);
                            }
                        }
                    }

                    //save Education
                    if (wizard.Items[WizardCache.ItemType.EmployeeEducationControl] != null)
                    {
                        EmployeeEducationCollection employeeEducationCollection = ((EmployeeEducationCollection)wizard.Items[WizardCache.ItemType.EmployeeEducationControl].Data);
                        if (employeeEducationCollection != null)
                        {
                            foreach (EmployeeEducation employeeEducation in employeeEducationCollection)
                            {
                                employeeEducation.EmployeeId = employee.EmployeeId;
                                EmployeeManagement.InsertEmployeeEducation(user, employeeEducation);
                            }
                        }
                    }

                    //save Skills
                    if (wizard.Items[WizardCache.ItemType.EmployeeSkillControl] != null)
                    {
                        EmployeeSkillCollection employeeSkillCollection = ((EmployeeSkillCollection)wizard.Items[WizardCache.ItemType.EmployeeSkillControl].Data);
                        if (employeeSkillCollection != null)
                        {
                            foreach (EmployeeSkill employeeSkill in employeeSkillCollection)
                            {
                                employeeSkill.EmployeeId = employee.EmployeeId;
                                EmployeeManagement.InsertEmployeeSkill(user, employeeSkill);
                            }
                        }
                    }

                    //save Company Property
                    if (wizard.Items[WizardCache.ItemType.EmployeeCompanyPropertyControl] != null)
                    {
                        EmployeeCompanyPropertyCollection employeeCompanyPropertyCollection = ((EmployeeCompanyPropertyCollection)wizard.Items[WizardCache.ItemType.EmployeeCompanyPropertyControl].Data);
                        if (employeeCompanyPropertyCollection != null)
                        {
                            foreach (EmployeeCompanyProperty employeeCompanyProperty in employeeCompanyPropertyCollection)
                            {
                                employeeCompanyProperty.EmployeeId = employee.EmployeeId;
                                EmployeeManagement.InsertEmployeeCompanyProperty(user, employeeCompanyProperty);
                            }
                        }
                    }

                    //save EmployeeLicences & Certificates
                    if (wizard.Items[WizardCache.ItemType.EmployeeLicenseCertificateControl] != null)
                    {
                        EmployeeLicenseCertificateCollection employeeLicenseCertificateCollection = ((EmployeeLicenseCertificateCollection)wizard.Items[WizardCache.ItemType.EmployeeLicenseCertificateControl].Data);
                        if (employeeLicenseCertificateCollection != null)
                        {
                            foreach (EmployeeLicenseCertificate employeeLicenseCertificate in employeeLicenseCertificateCollection)
                            {
                                employeeLicenseCertificate.EmployeeId = employee.EmployeeId;
                                EmployeeManagement.InsertEmployeeLicenseCertificate(user, employeeLicenseCertificate);
                            }
                        }
                    }

                    //disable wizard cache
                    wizard.ActiveFlag = false;
                    WizardAccess.UpdateWizardCache(user, wizard);

                    //commit
                    scope.Complete();
                }

                return employee;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WizardManagement - AddEmployee", ex);
                throw;
            }
        }
        public void ValidateAndHireEmployee(DatabaseUser user, WizardCacheImportCollection collection, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool autoAddSecondaryPositions)
        {
            //get all wizard cache id's
            List<String> importExternalIdentifiers = new List<String>();
            List<long> cacheIds = new List<long>();

            //build collections for existing caches/missing ones
            foreach (WizardCacheImport item in collection)
            {
                if (item.WizardCacheId < 1)
                    importExternalIdentifiers.Add(item.ImportExternalIdentifier);
                else
                    cacheIds.Add(item.WizardCacheId);
            }

            //load missing cache id's
            if (importExternalIdentifiers.Count > 0)
            {
                foreach (WizardCache cache in GetWizardCacheBatch(user, importExternalIdentifiers))
                    cacheIds.Add(cache.WizardCacheId);
            }

            //validate batch
            foreach (long wizardCacheId in cacheIds)
            {
                if (ValidateWizardEmployee(user, GetWizardCache(user, 1, wizardCacheId, WizardCache.ItemType.None), true))
                    AddEmployee(user, wizardCacheId, autoGenerateEmployeeNumber, employeeNumberFormat, autoAddSecondaryPositions);
            }
        }
        /// <summary>
        /// This method validates employee information.
        /// A new employee cannot be hired without the user providing valid employee, address, position, employment and statutory deduction information.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="wizardCaches"></param>
        /// <param name="isImport"></param>
        public bool ValidateWizardEmployee(DatabaseUser user, WizardCacheCollection wizardCaches, bool isImport = false)
        {
            //get the employee, address, position, employment information and statutory deduction records from the wizard cache
            EmployeeCollection employees = (EmployeeCollection)wizardCaches[0].Items[WizardCache.ItemType.EmployeeBiographicalControl]?.Data;
            PersonAddressCollection addresses = (PersonAddressCollection)wizardCaches[0].Items[WizardCache.ItemType.AddressDetailControl]?.Data;
            EmployeePositionCollection positions = (EmployeePositionCollection)wizardCaches[0].Items[WizardCache.ItemType.EmployeePositionViewControl]?.Data;
            EmployeeEmploymentInformationCollection employmentInformation = (EmployeeEmploymentInformationCollection)wizardCaches[0].Items[WizardCache.ItemType.EmployeeEmploymentInformationControl]?.Data;
            StatutoryDeductionCollection statutoryDeductions = (StatutoryDeductionCollection)wizardCaches[0].Items[WizardCache.ItemType.StatutoryDeductionViewControl]?.Data;

            if (employees != null && addresses != null && positions != null && employmentInformation != null && statutoryDeductions != null)
            {
                if (!isImport)
                    return true; //field validation for the add wizard is done on the screen

                //check validity of the employee, address, position, employment information and statutory deduction records and the presence of a "home" address
                return employees.IsValid && addresses.IsValid && positions.IsValid && employmentInformation.IsValid && statutoryDeductions.IsValid && addresses.HomeAddressExists;
            }

            return false;
        }
        #endregion
    }
}