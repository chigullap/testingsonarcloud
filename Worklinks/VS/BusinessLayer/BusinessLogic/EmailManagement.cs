﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.ServiceModel;
using System.Transactions;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.DataLayer.DataAccess;
using WorkLinks.DataLayer.DataAccess.SqlServer;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class EmailManagement
    {
        #region fields
        private EmailAccess _emailAccess = null;
        #endregion

        #region properties
        private EmailAccess EmailAccess
        {
            get
            {
                if (_emailAccess == null)
                    _emailAccess = new EmailAccess();

                return _emailAccess;
            }
        }
        #endregion

        #region email
        public EmailCollection GetEmail(DatabaseUser user, long? emailId)
        {
            try
            {
                return EmailAccess.GetEmail(user, emailId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmailManagement - GetEmail", ex);
                throw;
            }
        }
        public Email InsertEmail(DatabaseUser user, Email email)
        {
            try
            {
                return EmailAccess.InsertEmail(user, email);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmailManagement - InsertEmail", ex);
                throw;
            }
        }
        public void UpdateEmail(DatabaseUser user, Email email)
        {
            try
            {
                EmailAccess.UpdateEmail(user, email);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmailManagement - UpdateEmail", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 2601)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.UniqueIndexFault);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmail(DatabaseUser user, Email email)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    EmailAccess.DeleteEmailTemplate(user, new EmailTemplate() { EmailId = email.EmailId });
                    EmailAccess.DeleteEmail(user, email);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmailManagement - DeleteEmail", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region email template
        public EmailTemplateCollection GetEmailTemplate(DatabaseUser user, long emailId)
        {
            try
            {
                EmailTemplateCollection emailTemplates = EmailAccess.GetEmailTemplate(user, emailId);

                foreach (EmailTemplate template in emailTemplates)
                {
                    EmailCollection emails = EmailAccess.GetEmail(user, template.EmailId);
                    emails[0].CopyTo(template);
                }

                return emailTemplates;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmailManagement - GetEmailTemplate", ex);
                throw;
            }
        }
        public void InsertEmailTemplate(DatabaseUser user, EmailTemplate email)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    EmailAccess.InsertEmail(user, email);
                    EmailAccess.InsertEmailTemplate(user, email);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmailManagement - InsertEmailTemplate", ex);
                throw;
            }
        }
        public void UpdateEmailTemplate(DatabaseUser user, EmailTemplate email)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    EmailAccess.UpdateEmail(user, email);
                    EmailAccess.UpdateEmailTemplate(user, email);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmailManagement - UpdateEmailTemplate", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 2601)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.UniqueIndexFault);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmailTemplate(DatabaseUser user, EmailTemplate email)
        {
            try
            {
                EmailAccess.DeleteEmailTemplate(user, email);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmailManagement - DeleteEmailTemplate", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region email template
        public EmailRuleCollection GetEmailRule(DatabaseUser user, long emailId)
        {
            try
            {
                return EmailAccess.GetEmailRule(user, emailId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmailManagement - GetEmailRule", ex);
                throw;
            }
        }
        public void InsertEmailRule(DatabaseUser user, EmailRule email)
        {
            try
            {
                EmailAccess.InsertEmailRule(user, email);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmailManagement - InsertEmailRule", ex);
                throw;
            }
        }
        public void UpdateEmailRule(DatabaseUser user, EmailRule email)
        {
            try
            {
                EmailAccess.UpdateEmailRule(user, email);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmailManagement - UpdateEmailRule", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 2601)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.UniqueIndexFault);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmailRule(DatabaseUser user, EmailRule email)
        {
            try
            {
                EmailAccess.DeleteEmailRule(user, email);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmailManagement - DeleteEmailRule", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        public static void SendMessage(SendEmailParameters parms)
        {
            MailMessage message = new MailMessage();
            SmtpClient client = new SmtpClient();

            message.To.Add(parms.ToAddress);
            message.From = new MailAddress(parms.FromAddress);

            if (parms.CcAddress != null && !parms.CcAddress.Trim().Equals(string.Empty))
                message.CC.Add(parms.CcAddress);

            if (parms.BccAddress != null && !parms.BccAddress.Trim().Equals(string.Empty))
                message.Bcc.Add(parms.BccAddress);

            message.Subject = parms.Subject;
            message.Body = parms.Body;

            if (parms.Attachment != null)
            {
                Stream stream = new MemoryStream(parms.Attachment);
                System.Net.Mail.Attachment att = new System.Net.Mail.Attachment(stream, parms.FileName);
                message.Attachments.Add(att);
            }

            client.Host = parms.SmtpHostName;
            client.Port = parms.SmtpPortNumber;
            client.EnableSsl = parms.EnableSSL;
            client.Credentials = new System.Net.NetworkCredential(parms.SmtpLogin, parms.SmtpPassword);

            client.Send(message);
        }
    }
}