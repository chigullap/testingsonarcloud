﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.ServiceModel;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.CalcModel;
using WorkLinks.DataLayer.DataAccess.SqlServer;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    /// <summary>
    /// deals with worklinks microsoft dynamics management (WorkLinks database)
    /// </summary>
    public class WorkLinksDynamicsManagement
    {
        #region fields
        private EmployeeManagement _management = new EmployeeManagement();
        private PayrollManagement _payrollManagement = new PayrollManagement();
        private DataLayer.DataAccess.EmployeeAccess _access = new DataLayer.DataAccess.EmployeeAccess();
        private DataLayer.DataAccess.PayrollAccess _payrollAccess = new DataLayer.DataAccess.PayrollAccess();
        private static DataLayer.DataAccess.Dynamics.DynamicsAccess _dynamicsAccess = new DataLayer.DataAccess.Dynamics.DynamicsAccess();
        private ReportManagement _reportManagement = new ReportManagement();
        private EmployeeManagement _employeeManagement = new EmployeeManagement();
        #endregion

        #region job
        public byte[] CreatePensionExportFile(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                if (user.DatabaseName.ToUpper() == "A1235")
                    return _dynamicsAccess.CreateVertivPensionExportFile(user, payrollProcessId);
                else
                    return _dynamicsAccess.CreatePensionExportFile(user, payrollProcessId);

            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreatePensionExportFile", ex);
                throw;
            }
        }

        #region HSBC EFT
        public byte[] CreateHSBCEft(DatabaseUser user, long payrollProcessId, string employeeNumber, string eftType)
        {
            try
            {
                byte[] eftFile = null;
                PayrollExportCollection payExportColl = null;

                //if employeeNumber is not null, this method is being called to generate an EFT for a single employee.  Do not use database data
                if (employeeNumber == null)
                    payExportColl = _dynamicsAccess.CheckForEftOnDatabase(user, payrollProcessId, eftType);

                if (payExportColl != null && payExportColl.Count > 0)
                    return payExportColl[0].Data;
                else
                {
                    eftFile = _dynamicsAccess.CreateHSBCEft(user, payrollProcessId, employeeNumber);

                    //if employeeNumber is not null, this method is being called to generate an EFT for a single employee.  Do not store in database
                    if (employeeNumber == null && eftFile != null)
                    {
                        PayrollExport payExport = new PayrollExport() { PayrollProcessId = payrollProcessId, Data = eftFile, EftType = eftType, CreateUser = user.UserName, UpdateUser = user.UserName };
                        _dynamicsAccess.InsertEftIntoDatabase(user, payExport);
                    }
                }

                return eftFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateHSBCeft", ex);
                throw;
            }
        }
        #endregion

        public byte[] CreateCITIeft(DatabaseUser user, long payrollProcessId, String eftClientNumber, String employeeNumber, string eftType)
        {
            try
            {
                byte[] eftFile = null;
                PayrollExportCollection payExportColl = null;

                //if employeeNumber is not null, this method is being called to generate an EFT for a single employee.  Do not use database data
                if (employeeNumber == null)
                    payExportColl = _dynamicsAccess.CheckForEftOnDatabase(user, payrollProcessId, eftType);

                if (payExportColl != null && payExportColl.Count > 0)
                    return payExportColl[0].Data;
                else
                {
                    eftFile = _dynamicsAccess.CreateCITIeft(user, payrollProcessId, eftClientNumber, employeeNumber);

                    //if employeeNumber is not null, this method is being called to generate an EFT for a single employee.  Do not store in database
                    if (employeeNumber == null && eftFile != null)
                    {
                        PayrollExport payExport = new PayrollExport() { PayrollProcessId = payrollProcessId, Data = eftFile, EftType = eftType, CreateUser = user.UserName, UpdateUser = user.UserName };
                        _dynamicsAccess.InsertEftIntoDatabase(user, payExport);
                    }
                }

                return eftFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateCITIeft", ex);
                throw;
            }
        }
        public byte[] CreateCitiChequeEftFromPayrollMasterPaymentRecords(DatabaseUser user, String[] payrollMasterPaymentIdIds, String citiChequeAccountNumber, String companyShortName, String employeeNumber, long payrollProccessId, string originatorId, long employeeId)
        {
            try
            {
                byte[] eftFile = null;
                CitiChequeDetailsCollection cheques = new CitiChequeDetailsCollection();

                //get payroll master payment records and store them into a file collection (proc will take care of the necessary mappings)
                foreach (String payrollMasterPaymentId in payrollMasterPaymentIdIds)
                {
                    CitiChequeDetails details = GetPayrollMasterPaymentData(user, Convert.ToInt64(payrollMasterPaymentId));

                    //we need a unique payroll_master_payment_id to keep cheque numbers in sync, so perform an insert, get the key, assign it to the collection, then delete that newly added row
                    PayrollMasterPayment payment = new PayrollMasterPayment() { PayrollMasterId = details.PayrollMasterId, PayrollProcessId = payrollProccessId, EmployeeId = employeeId, CodeStatusCd = "NEW" };
                    _dynamicsAccess.InsertPayrollMasterPayment(user, payment);

                    //copy the id
                    details.PayrollMasterPaymentId = payment.PayrollMasterPaymentId;

                    //delete the new record
                    _dynamicsAccess.DeletePayrollMasterPaymentById(user, payment.PayrollMasterPaymentId);

                    //add object to collection
                    cheques.Add(details);
                }

                eftFile = CreateCitiChequeFile(user, cheques, citiChequeAccountNumber, companyShortName, cheques[0].PayrollProcessId, originatorId);

                return eftFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateCitiChequeEftFromPayrollMasterPaymentRecords", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        private CitiChequeDetails GetPayrollMasterPaymentData(DatabaseUser user, long payrollMasterPaymentId)
        {
            return _dynamicsAccess.GetPayrollMasterPaymentData(user, payrollMasterPaymentId)[0];
        }
        public byte[] CreateCitiChequeFile(DatabaseUser user, long payrollProcessId, String citiChequeAccountNumber, String companyShortName, String employeeNumber, string originatorId, string eftType)
        {
            try
            {
                byte[] eftFile = null;
                PayrollExportCollection payExportColl = null;

                //if employeeNumber is not null, this method is being called to generate an EFT for a single employee.  Do not use database data
                if (employeeNumber == null)
                    payExportColl = _dynamicsAccess.CheckForEftOnDatabase(user, payrollProcessId, eftType);

                if (payExportColl != null && payExportColl.Count > 0)
                    return payExportColl[0].Data;
                else
                {
                    eftFile = CreateCitiChequeFile(user, GetCitiChequeData(user, payrollProcessId, employeeNumber), citiChequeAccountNumber, companyShortName, payrollProcessId, originatorId);

                    //if employeeNumber is not null, this method is being called to generate an EFT for a single employee.  Do not store in database
                    if (employeeNumber == null && eftFile != null)
                    {
                        PayrollExport payExport = new PayrollExport() { PayrollProcessId = payrollProcessId, Data = eftFile, EftType = eftType, CreateUser = user.UserName, UpdateUser = user.UserName };
                        _dynamicsAccess.InsertEftIntoDatabase(user, payExport);
                    }
                }

                return eftFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateCitiChequeFile", ex);
                throw;
            }
        }
        public byte[] CreateCitiChequeFile(DatabaseUser user, CitiChequeDetailsCollection collection, String citiChequeAccountNumber, String companyShortName, long payrollProcessId, string originatorId)
        {
            try
            {
                return _dynamicsAccess.CreateCitiChequeFile(collection, citiChequeAccountNumber, companyShortName, payrollProcessId, originatorId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateCitiChequeFile", ex);
                throw;
            }
        }
        public CitiChequeDetailsCollection GetCitiChequeData(DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            try
            {
                return _dynamicsAccess.GetCitiChequeData(user, payrollProcessId, employeeNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetCitiChequeData", ex);
                throw;
            }
        }
        public byte[] CreateRRSPExport(DatabaseUser user, long payrollProcessId, String rrspHeader, String rrspGroupNumber)
        {
            try
            {
                byte[] rrspFile = null;

                //get the needed data via sql and put in business object and use the biz object data to fill the record sections of the file and return the file into the byte array
                rrspFile = CreateRRSPExport(user, GetRRSPData(user, payrollProcessId), rrspHeader, rrspGroupNumber);

                return rrspFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateRRSPExport", ex);
                throw;
            }
        }

        #region CIC Plus Weekly Export
        public byte[] CreateCicPlusWeeklyExportFile(DatabaseUser user, decimal periodYear)
        {
            try
            {
                byte[] cicPlusWeeklyFile = null;
                cicPlusWeeklyFile = CreateCicPlusWeeklyExport(user, GetCicPlusWeeklyExportData(user, periodYear));

                return cicPlusWeeklyFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateCicPlusWeeklyExportFile", ex);
                throw;
            }
        }

        public CICWeeklyExportCollection GetCicPlusWeeklyExportData(DatabaseUser user, decimal periodYear)
        {
            try
            {
                return _dynamicsAccess.GetCicPlusWeeklyExportData(user, periodYear);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetCicPlusWeeklyExportData", ex);
                throw;
            }
        }

        public byte[] CreateCicPlusWeeklyExport(DatabaseUser user, CICWeeklyExportCollection exportCollection)
        {
            try
            {
                return _dynamicsAccess.CreateCICPlusWeeklyExportFile(exportCollection);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateCicPlusWeeklyExport", ex);
                throw;
            }
        }

        #endregion

        #region REAL Custom Export Data
        public byte[] CreateRealCustomExport(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                return CreateRealCustomExportDataFile(user, GetRealCustomExportData(user, payrollProcessId));
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateRealCustomExport", ex);
                throw;
            }
        }
        public REALCustomExportCollection GetRealCustomExportData(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                return _dynamicsAccess.GetRealCustomExportData(user, payrollProcessId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetRealCustomExportData", ex);
                throw;
            }
        }
        public byte[] CreateRealCustomExportDataFile(DatabaseUser user, REALCustomExportCollection customExportColl)
        {
            try
            {
                return _dynamicsAccess.CreateRealCustomExportDataFile(user, customExportColl);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateRealCustomExportDataFile", ex);
                throw;
            }
        }
        #endregion

        #region social costs export
        public byte[] CreateSocialCostsExport(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                return CreateSocialCostsExportFile(user, GetSocialCostsData(user, payrollProcessId));
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateSocialCostsExport", ex);
                throw;
            }
        }
        public SocialCostsCollection GetSocialCostsData(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                return _dynamicsAccess.GetSocialCostsData(user, payrollProcessId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetSocialCostsData", ex);
                throw;
            }
        }
        public byte[] CreateSocialCostsExportFile(DatabaseUser user, SocialCostsCollection socialCostsColl)
        {
            try
            {
                return _dynamicsAccess.CreateSocialCostsExportFile(user, socialCostsColl);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateSocialCostsExportFile", ex);
                throw;
            }
        }
        #endregion

        public byte[] CreateGarnishmentExportFile(DatabaseUser user, long payrollProcessId, String empowerFein)
        {
            try
            {
                byte[] garnishmentFile = null;
                garnishmentFile = CreateGarnishmentExport(user, GetMasterPayDetailData(user, payrollProcessId), GetCalcMasterData(user, payrollProcessId), empowerFein);

                return garnishmentFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateGarnishmentExportFile", ex);
                throw;
            }
        }
        public byte[] CreateGarnishmentExport(DatabaseUser user, PayrollMasterPaycodeCollection payrollMasterPaycodeColl, PayrollMasterCollection calcMasterColl, String empowerFein)
        {
            try
            {
                return _dynamicsAccess.CreateGarnishmentExport(user, payrollMasterPaycodeColl, calcMasterColl, empowerFein);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateGarnishmentExport", ex);
                throw;
            }
        }
        public PayrollMasterCollection GetCalcMasterData(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                return _dynamicsAccess.GetPayrollMaster(user, payrollProcessId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetCalcMasterData", ex);
                throw;
            }
        }
        public PayrollMasterPaycodeCollection GetMasterPayDetailData(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                return _dynamicsAccess.GetPayrollMasterPaycode(user, payrollProcessId, true, null, null);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetMasterPayDetailData", ex);
                throw;
            }
        }
        public byte[] CreateEmpowerRemittanceExport(DatabaseUser user, long payrollProcessId, String revenueQuebecTaxId, bool useQuebecTaxId)
        {
            byte[] rrspFile = null;

            //get the needed data via sql and put in business object and use the biz object data to fill the record sections of the file and return the file into the byte array
            PayDetailCollection payDetails = _payrollAccess.GetPayDetail(user, payrollProcessId, "WCGREXP"); //hard code for empower tax export

            rrspFile = CreateEmpowerRemittanceExport(user, GetEmpowerRemittanceCalcMasterData(user, payrollProcessId), payDetails, revenueQuebecTaxId, useQuebecTaxId);

            return rrspFile;
        }
        public byte[] CreateEmpowerRemittanceExport(DatabaseUser user, PayrollMasterCollection calcCollection, PayDetailCollection payDetails, String revenueQuebecTaxId, bool useQuebecTaxId)
        {
            try
            {
                //add in master pay detail info
                return _dynamicsAccess.CreateEmpowerRemittanceExport(user, calcCollection, revenueQuebecTaxId, payDetails, useQuebecTaxId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateEmpowerRemittanceExport", ex);
                throw;
            }
        }
        public PayrollMasterCollection GetEmpowerRemittanceCalcMasterData(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                return _dynamicsAccess.GetPayrollMaster(user, payrollProcessId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetEmpowerRemittanceCalcMasterData", ex);
                throw;
            }
        }
        public byte[] CreateRRSPExport(DatabaseUser user, PayrollMasterPaycodeCollection collection, String rrspHeader, String rrspGroupNumber)
        {
            try
            {
                return _dynamicsAccess.CreateRRSPExport(user, collection, rrspHeader, rrspGroupNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateRRSPExport", ex);
                throw;
            }
        }
        public PayrollMasterPaycodeCollection GetRRSPData(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                return _dynamicsAccess.GetPayrollMasterPaycode(user, payrollProcessId, null, "RRSPXPRT", true); //HARDCODE ASSOCIATION FOR RRSP EXPORT
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetRRSPData", ex);
                throw;
            }
        }
        public String GetNamingFormatWithYearPeriodSequence(DatabaseUser user, String fileNameFormat, long payrollprocessid)
        {
            DataLayer.DataAccess.PayrollAccess tempObject = new DataLayer.DataAccess.PayrollAccess();
            PayrollProcessSummary payrollProcess = tempObject.GetPayrollProcessSummary(user, new PayrollProcessCriteria() { PayrollProcessId = payrollprocessid })[0];

            return XmlManagement.GenerateFileName(fileNameFormat, new EmployeeSummary(), payrollProcess, (int)payrollProcess.ExportSequenceNumber);
        }
        public String GetCitiNamingFormat(DatabaseUser user, String eftFileNameFormat, long payrollprocessid)
        {
            DataLayer.DataAccess.PayrollAccess tempObject = new DataLayer.DataAccess.PayrollAccess();
            PayrollProcessSummary payrollProcess = tempObject.GetPayrollProcessSummary(user, new PayrollProcessCriteria() { PayrollProcessId = payrollprocessid })[0];

            return XmlManagement.GenerateFileName(eftFileNameFormat, new EmployeeSummary(), payrollProcess, (int)payrollProcess.ExportSequenceNumber);
        }
        public byte[] CreateCitiEftAndChequeFile(DatabaseUser user, long payrollProcessId, String citiEftFileName, String citiChequeFileName, String eftClientNumber, String citiChequeAccountNumber, String companyName, String companyShortName, String employeeNumber, string originatorId, string eftType)
        {
            try
            {
                byte[] zipFile = null;
                Dictionary<String, byte[]> eftFiles = new Dictionary<String, byte[]>();
                PayrollExportCollection payExportColl = null;

                //if employeeNumber is not null, this method is being called to generate an EFT for a single employee.  Do not use database data
                if (employeeNumber == null)
                    payExportColl = _dynamicsAccess.CheckForEftOnDatabase(user, payrollProcessId, eftType);

                if (payExportColl != null && payExportColl.Count > 0)
                    zipFile = payExportColl[0].Data;
                else
                {
                    byte[] achFile = CreateCitiAch(user, GetEftData(user, payrollProcessId, employeeNumber), "RP", eftClientNumber, companyName, companyShortName);
                    byte[] chequeFile = CreateCitiChequeFile(user, GetCitiChequeData(user, payrollProcessId, employeeNumber), citiChequeAccountNumber, companyShortName, payrollProcessId, originatorId);

                    //if file null, dont add to zip...
                    if (achFile != null)
                        eftFiles.Add(GetCitiNamingFormat(user, citiEftFileName, payrollProcessId), achFile);
                    if (chequeFile != null)
                        eftFiles.Add(GetCitiNamingFormat(user, citiChequeFileName, payrollProcessId), chequeFile);

                    zipFile = ReportManagement.CreateZip(user, eftFiles);

                    //if employeeNumber is not null, this method is being called to generate an EFT for a single employee.  Do not store in database
                    if (employeeNumber == null && eftFiles != null)
                    {
                        PayrollExport payExport = new PayrollExport() { PayrollProcessId = payrollProcessId, Data = zipFile, EftType = eftType };
                        _dynamicsAccess.InsertEftIntoDatabase(user, payExport);
                    }
                }

                return zipFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateCitiEftAndChequeFile", ex);
                throw;
            }
        }
        public byte[] CreateCitiAchFile(DatabaseUser user, long payrollProcessId, String eftClientNumber, String companyName, String companyShortName, String employeeNumber, string eftType)
        {
            try
            {
                byte[] eftFile = null;
                PayrollExportCollection payExportColl = null;

                //if employeeNumber is not null, this method is being called to generate an EFT for a single employee.  Do not use database data
                if (employeeNumber == null)
                    payExportColl = _dynamicsAccess.CheckForEftOnDatabase(user, payrollProcessId, eftType);

                if (payExportColl != null && payExportColl.Count > 0)
                    return payExportColl[0].Data;
                else
                {
                    eftFile = CreateCitiAch(user, GetEftData(user, payrollProcessId, employeeNumber), "RP", eftClientNumber, companyName, companyShortName);

                    //if employeeNumber is not null, this method is being called to generate an EFT for a single employee.  Do not store in database
                    if (employeeNumber == null && eftFile != null)
                    {
                        PayrollExport payExport = new PayrollExport() { PayrollProcessId = payrollProcessId, Data = eftFile, EftType = eftType };
                        _dynamicsAccess.InsertEftIntoDatabase(user, payExport);
                    }
                }

                return eftFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateCitiAchFile", ex);
                throw;
            }
        }
        public byte[] CreateCitiAch(DatabaseUser user, PayrollMasterPaymentCollection collection, String status, String eftClientNumber, String companyName, String companyShortName)
        {
            try
            {
                return _dynamicsAccess.CreateCitiAch(user, collection, status, eftClientNumber, companyName, companyShortName);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateCitiAch", ex);
                throw;
            }
        }

        public byte[] GenerateRevenuQuebecSourceDeductionsFileFile(DatabaseUser user, RbcRqSourceDeductionParameters sourceParms, DateTime date, string rqRemittancePeriodCode, RevenuQuebecRemittanceSummaryCollection collection, long? rqExportId, out long insertedRqExportId)
        {
            try
            {
                //this will keep track of the inserted revenue_quebec_export record
                insertedRqExportId = -1;

                byte[] eftFile = null;
                RevenuQuebecExportCollection rqExportColl = null;

                //check if file exists on the database
                if (rqExportId != null)
                    rqExportColl = _dynamicsAccess.CheckForRqRemittanceOnDatabase(user, rqExportId); //used by future screen that may show what was stored in the db

                //if found on db, return it, otherwise create it
                if (rqExportColl != null && rqExportColl.Count > 0)
                    eftFile = rqExportColl[0].Data;
                else
                {
                    //"collection" will not be null if we get here but keep the check incase
                    if (collection != null && collection.Count > 0)
                    {
                        //get sequence number from db
                        long headerSequenceNumber = _dynamicsAccess.GetSequenceNumber(user, "GLOBAL_RBC_HEADER_SEQUENCE");
                        long transactionSequenceNumber = _dynamicsAccess.GetSequenceNumber(user, "GLOBAL_RBC_TRANSACTION_SEQUENCE");

                        eftFile = CreateRevenuQuebecExportFile(user, collection, sourceParms, rqRemittancePeriodCode, headerSequenceNumber, transactionSequenceNumber);

                        if (eftFile != null)
                        {
                            RevenuQuebecExport rqExport = new RevenuQuebecExport() { Data = eftFile, CodeRevenuQuebecRemittancePeriodCd = rqRemittancePeriodCode, RbcSequenceNumber = headerSequenceNumber };
                            _dynamicsAccess.InsertRevenuQuebecExportToDatabase(user, rqExport);

                            //assign the id of the revenu_quebec_export just inserted
                            insertedRqExportId = rqExport.RevenuQuebecExportId;

                            //update revenu quebec table(revenu_quebec_remittance) with processed flag and key from revenu_quebec_export table
                            _dynamicsAccess.UpdateRevenuQuebecRemittanceRevenuQuebecExportIdColumn(user, insertedRqExportId, date, rqRemittancePeriodCode);
                        }
                    }
                }

                return eftFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GenerateRevenuQuebecExportFile", ex);
                throw;
            }
        }

        private byte[] CreateZipForDatabaseInsert(DatabaseUser user, string rqRemittancePeriodCode, byte[] exportFile)
        {
            //set the name for the excel file and the zip file
            String exportFileName = String.Format("{0:yyyyMMddHHmmssfff}_{1}_RQ_{2}.xls", DateTime.Now, user.DatabaseName, rqRemittancePeriodCode);

            //store the excel filename and file in a dictionary prior to zipping
            Dictionary<string, byte[]> exports = new Dictionary<string, byte[]>();
            exports.Add(exportFileName, exportFile);

            //create the zip file
            return ReportManagement.CreateZip(user, exports);
        }

        public byte[] GenerateRBCEftWcbFile(DatabaseUser user, RbcEftSourceDeductionParameters sourceParms, DateTime date, String craRemittancePeriodCode, long? craWcbExportId, out long insertedCraWcbExportId)
        {
            try
            {
                //this will keep track of the inserted cra_wcb_export record
                insertedCraWcbExportId = -1;

                byte[] eftFile = null;
                CraWcbExportCollection craWcbExportColl = null;

                //check if file exists on the database
                if (craWcbExportId != null)
                    craWcbExportColl = _dynamicsAccess.CheckForCraWcbRemittanceOnDatabase(user, craWcbExportId); //used by future screen that may show what was stored in the db

                //if found on db, return it, otherwise create it
                if (craWcbExportColl != null && craWcbExportColl.Count > 0)
                    return craWcbExportColl[0].Data;
                else
                {
                    CraWcbRemittanceSummaryCollection collection = GetRbcEftWcbData(user, date, craRemittancePeriodCode);

                    if (collection != null && collection.Count > 0)
                    {
                        //do not create a file of the SumPaymentAmount is 0.  Just tag the record as processed so it is not looked at again.
                        if (collection.SumPaymentAmount == 0)
                            _dynamicsAccess.UpdateCraWcbRemittanceCraWcbExportIdColumn(user, date, null, craRemittancePeriodCode);
                        else
                        {
                            //get sequence number from db
                            long headerSequenceNumber = _dynamicsAccess.GetSequenceNumber(user, "GLOBAL_RBC_HEADER_SEQUENCE");
                            long transactionSequenceNumber = _dynamicsAccess.GetSequenceNumber(user, "GLOBAL_RBC_TRANSACTION_SEQUENCE");

                            eftFile = CreateRbcEftWcbFile(user, collection, sourceParms, craRemittancePeriodCode, headerSequenceNumber, transactionSequenceNumber);

                            if (eftFile != null)
                            {
                                CraWcbExport craWcbExport = new CraWcbExport() { Data = eftFile, CodeCraRemittancePeriodCd = craRemittancePeriodCode, RbcSequenceNumber = headerSequenceNumber };
                                _dynamicsAccess.InsertCraWcbExportToDatabase(user, craWcbExport);

                                //assign the id of the cra_export just inserted
                                insertedCraWcbExportId = craWcbExport.CraWcbExportId;

                                //update cra table(cra_wcb_remittance) with processed flag and key from cra_wcb_export table
                                _dynamicsAccess.UpdateCraWcbRemittanceCraWcbExportIdColumn(user, date, insertedCraWcbExportId, craRemittancePeriodCode);
                            }
                        }
                    }
                }

                return eftFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GenerateRBCEftWcbFile", ex);
                throw;
            }
        }

        public byte[] GenerateRBCEftSourceDeductionsFile(DatabaseUser user, RbcEftSourceDeductionParameters sourceParms, DateTime date, String craRemittancePeriodCode, long? craExportId, out long insertedCraExportId)
        {
            try
            {
                //this will keep track of the inserted cra_export record
                insertedCraExportId = -1;

                byte[] eftFile = null;
                CraExportCollection craExportColl = null;

                //check if file exists on the database
                if (craExportId != null)
                    craExportColl = _dynamicsAccess.CheckForCraRemittanceOnDatabase(user, craExportId); //used by future screen that may show what was stored in the db

                //if found on db, return it, otherwise create it
                if (craExportColl != null && craExportColl.Count > 0)
                    eftFile = craExportColl[0].Data;
                else
                {
                    CraRemittanceSummaryCollection collection = GetRbcEftSourceDeductionData(user, date, craRemittancePeriodCode);

                    if (collection != null && collection.Count > 0)
                    {
                        //get sequence number from db
                        long headerSequenceNumber = _dynamicsAccess.GetSequenceNumber(user, "GLOBAL_RBC_HEADER_SEQUENCE");
                        long transactionSequenceNumber = _dynamicsAccess.GetSequenceNumber(user, "GLOBAL_RBC_TRANSACTION_SEQUENCE");

                        eftFile = CreateRbcEftSourceDeductionFile(user, collection, sourceParms, craRemittancePeriodCode, headerSequenceNumber, transactionSequenceNumber);

                        if (eftFile != null)
                        {
                            CraExport craExport = new CraExport() { Data = eftFile, CodeCraRemittancePeriodCd = craRemittancePeriodCode, RbcSequenceNumber = headerSequenceNumber };
                            _dynamicsAccess.InsertCraExportToDatabase(user, craExport);

                            //assign the id of the cra_export just inserted
                            insertedCraExportId = craExport.CraExportId;

                            //update cra table(cra_remittance) with processed flag and key from cra_export table
                            _dynamicsAccess.UpdateCraRemittanceCraExportIdColumn(user, date, insertedCraExportId, craRemittancePeriodCode);
                        }
                    }
                }

                return eftFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GenerateRBCEftSourceDeductionsFile", ex);
                throw;
            }
        }

        public byte[] GenerateCraEftGarnishmentFile(DatabaseUser user, long payrollProcessId, RbcEftSourceDeductionParameters garnishmentParms, string employeeNumber)
        {
            try
            {
                byte[] eftFile = null;
                PayrollExportCollection payExportColl = null;

                //if employeeNumber is not null, this method is being called to generate an EFT for a single employee.  Do not use database data (not currently used, future)
                if (employeeNumber == null)
                    payExportColl = _dynamicsAccess.CheckForEftOnDatabase(user, payrollProcessId, garnishmentParms.GarnishmentEftType);

                //if found on db, return it, otherwise create it
                if (payExportColl != null && payExportColl.Count > 0)
                    return payExportColl[0].Data;
                else
                {
                    CraGarnishmentCollection collection = GetCraEftGarnishmentDeductionData(user, payrollProcessId, employeeNumber);

                    if (collection != null && collection.Count > 0)
                    {
                        //get sequence number from db
                        long headerSequenceNumber = _dynamicsAccess.GetSequenceNumber(user, "GLOBAL_RBC_HEADER_SEQUENCE");
                        long transactionSequenceNumber = _dynamicsAccess.GetSequenceNumber(user, "GLOBAL_RBC_TRANSACTION_SEQUENCE");

                        eftFile = CreateCraEftGarnishmentDeductionFile(user, collection, garnishmentParms, headerSequenceNumber, transactionSequenceNumber);

                        //if employeeNumber is not null, this method is being called to generate an EFT for a single employee.  Do not store in database
                        if (employeeNumber == null && eftFile != null)
                        {
                            PayrollExport payExport = new PayrollExport() { PayrollProcessId = payrollProcessId, Data = eftFile, EftType = garnishmentParms.GarnishmentEftType, SequenceNumber = headerSequenceNumber };
                            _dynamicsAccess.InsertEftIntoDatabase(user, payExport);
                        }
                    }
                }

                return eftFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GenerateCRAEftGarnishmentFile", ex);
                throw;
            }
        }

        public CraGarnishmentCollection GetCraEftGarnishmentDeductionData(DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            try
            {
                return _dynamicsAccess.GetCraEftGarnishmentDeductionData(user, payrollProcessId, employeeNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetCraGarnishmentEftData", ex);
                throw;
            }
        }
        public byte[] CreateCraEftGarnishmentDeductionFile(DatabaseUser user, CraGarnishmentCollection collection, RbcEftSourceDeductionParameters garnishmentParms, long headerSequenceNumber, long transactionSequenceNumber)
        {
            try
            {
                return _dynamicsAccess.CreateCraEftGarnishmentDeductionFile(user, collection, garnishmentParms, headerSequenceNumber, transactionSequenceNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateCraEftGarnishmentDeductionFile", ex);
                throw;
            }
        }

        public byte[] GenerateScotiaBankEdiFile(DatabaseUser user, long payrollProcessId, ScotiaBankEdiParameters ediParms, string employeeNumber)
        {
            try
            {
                byte[] eftFile = null;
                PayrollExportCollection payExportColl = null;

                //if employeeNumber is not null, this method is being called to generate an EFT for a single employee.  Do not use database data
                if (employeeNumber == null)
                    payExportColl = _dynamicsAccess.CheckForEftOnDatabase(user, payrollProcessId, ediParms.EftType);

                //if found on db, return it, otherwise create it
                if (payExportColl != null && payExportColl.Count > 0)
                    return payExportColl[0].Data;
                else
                {
                    ScotiaBankEdiCollection collection = GetScotiaBankEdiData(user, payrollProcessId, employeeNumber);
                    if (collection != null && collection.Count > 0)
                    {
                        //get sequence number from db
                        long headerSequenceNumber = _dynamicsAccess.GetSequenceNumber(user, "GLOBAL_SCOTIABANK_HEADER_SEQUENCE");

                        //wont need transaction sequence as it's stored for each payment in the payroll_master_payment.sequence_number on the post.
                        eftFile = CreateScotiaBankEdiFile(user, collection, "RP", ediParms, headerSequenceNumber);

                        //if employeeNumber is not null, this method is being called to generate an EFT for a single employee.  Do not store in database
                        if (employeeNumber == null && eftFile != null)
                        {
                            PayrollExport payExport = new PayrollExport() { PayrollProcessId = payrollProcessId, Data = eftFile, EftType = ediParms.EftType, SequenceNumber = headerSequenceNumber };
                            _dynamicsAccess.InsertEftIntoDatabase(user, payExport);
                        }
                    }
                }

                return eftFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GenerateScotiaBankEdiFile", ex);
                throw;
            }
        }

        public byte[] GenerateRBCEftEdiFile(DatabaseUser user, long payrollProcessId, RbcEftEdiParameters ediParms, string employeeNumber)
        {
            try
            {
                byte[] eftFile = null;
                PayrollExportCollection payExportColl = null;

                //if employeeNumber is not null, this method is being called to generate an EFT for a single employee.  Do not use database data
                if (employeeNumber == null)
                    payExportColl = _dynamicsAccess.CheckForEftOnDatabase(user, payrollProcessId, ediParms.EftType);

                //if found on db, return it, otherwise create it
                if (payExportColl != null && payExportColl.Count > 0)
                    return payExportColl[0].Data;
                else
                {
                    RbcEftEdiCollection collection = GetRbcEftEdiData(user, payrollProcessId, employeeNumber);

                    if (collection != null && collection.Count > 0)
                    {
                        //get sequence number from db
                        long headerSequenceNumber = _dynamicsAccess.GetSequenceNumber(user, "GLOBAL_RBC_HEADER_SEQUENCE");

                        //wont need transaction sequence as it's stored for each payment in the payroll_master_payment.rbc_sequence_number on the post.
                        eftFile = CreateRbcEftEdiFile(user, collection, "RP", ediParms, headerSequenceNumber);

                        //if employeeNumber is not null, this method is being called to generate an EFT for a single employee.  Do not store in database
                        if (employeeNumber == null && eftFile != null)
                        {
                            PayrollExport payExport = new PayrollExport() { PayrollProcessId = payrollProcessId, Data = eftFile, EftType = ediParms.EftType, SequenceNumber = headerSequenceNumber };
                            _dynamicsAccess.InsertEftIntoDatabase(user, payExport);
                        }
                    }
                }

                return eftFile;

            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GenerateRBCEftEdiFile", ex);
                throw;
            }
        }

        public byte[] CreateRevenuQuebecExportFile(DatabaseUser user, RevenuQuebecRemittanceSummaryCollection collection, RbcRqSourceDeductionParameters sourceParms, String rqRemittancePeriodCode, long headerSequenceNumber, long transactionSequenceNumber)
        {
            try
            {
                return _dynamicsAccess.CreateRevenuQuebecExportFile(user, collection, sourceParms, rqRemittancePeriodCode, headerSequenceNumber, transactionSequenceNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateRevenuQuebecExportFile", ex);
                throw;
            }
        }

        public byte[] CreateRbcEftWcbFile(DatabaseUser user, CraWcbRemittanceSummaryCollection collection, RbcEftSourceDeductionParameters sourceParms, String craRemittancePeriodCode, long headerSequenceNumber, long transactionSequenceNumber)
        {
            try
            {
                return _dynamicsAccess.CreateCraNsWcbSourceDeductionFile(user, collection, sourceParms, craRemittancePeriodCode, headerSequenceNumber, transactionSequenceNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateRbcEftWcbFile", ex);
                throw;
            }
        }

        public byte[] CreateRbcEftSourceDeductionFile(DatabaseUser user, CraRemittanceSummaryCollection collection, RbcEftSourceDeductionParameters sourceParms, String craRemittancePeriodCode, long headerSequenceNumber, long transactionSequenceNumber)
        {
            try
            {
                return _dynamicsAccess.CreateRbcEftSourceDeductionFile(user, collection, sourceParms, craRemittancePeriodCode, headerSequenceNumber, transactionSequenceNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateRbcEftSourceDeductionFile", ex);
                throw;
            }
        }
        public byte[] CreateRbcEftEdiFile(DatabaseUser user, RbcEftEdiCollection collection, String status, RbcEftEdiParameters ediParms, long headerSequenceNumber)
        {
            try
            {
                return _dynamicsAccess.CreateRbcEftEdiFile(user, collection, status, ediParms, headerSequenceNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateRbcEftEdiFile", ex);
                throw;
            }
        }

        public byte[] CreateScotiaBankEdiFile(DatabaseUser user, ScotiaBankEdiCollection collection, string status, ScotiaBankEdiParameters ediParms, long headerSequenceNumber)
        {
            try
            {
                return _dynamicsAccess.CreateScotiaBankEdiFile(user, collection, status, ediParms, headerSequenceNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateScotiaBankEdiFile", ex);
                throw;
            }
        }

        public RevenuQuebecRemittanceSummaryCollection GetRevenueQuebecExportData(DatabaseUser user, DateTime date, string rqRemittancePeriodCode)
        {
            try
            {
                return _dynamicsAccess.GetRevenueQuebecExportData(user, date, rqRemittancePeriodCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetRevenueQuebecExportData", ex);
                throw;
            }
        }

        public CraWcbRemittanceSummaryCollection GetRbcEftWcbData(DatabaseUser user, DateTime date, String craRemittancePeriodCode)
        {
            try
            {
                return _dynamicsAccess.GetRbcEftWcbData(user, date, craRemittancePeriodCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetRbcEftWcbData", ex);
                throw;
            }
        }
        public long GetSequenceNumber(DatabaseUser user, String sequenceName)
        {
            try
            {
                return _dynamicsAccess.GetSequenceNumber(user, sequenceName);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetSequenceNumber", ex);
                throw;
            }
        }
        public CraRemittanceSummaryCollection GetRbcEftSourceDeductionData(DatabaseUser user, DateTime date, String craRemittancePeriodCode)
        {
            try
            {
                return _dynamicsAccess.GetRbcEftSourceDeductionData(user, date, craRemittancePeriodCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetRbcEftSourceDeductionData", ex);
                throw;
            }
        }

        public ScotiaBankEdiCollection GetScotiaBankEdiData(DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            try
            {
                return _dynamicsAccess.GetScotiaBankEdiData(user, payrollProcessId, employeeNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetScotiaBankEdiData", ex);
                throw;
            }
        }

        public RbcEftEdiCollection GetRbcEftEdiData(DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            try
            {
                return _dynamicsAccess.GetRbcEftEdiData(user, payrollProcessId, employeeNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetRbcEftEdiData", ex);
                throw;
            }
        }
        public byte[] GenerateEftFile(DatabaseUser user, long payrollProcessId, String eftClientNumber, bool eftTransmissionFlag, String eftFileIndicator, String companyName, String companyShortName, String employeeNumber, string eftType)
        {
            try
            {
                byte[] eftFile = null;
                PayrollExportCollection payExportColl = null;

                //if employeeNumber is not null, this method is being called to generate an EFT for a single employee.  Do not use database data
                if (employeeNumber == null)
                    payExportColl = _dynamicsAccess.CheckForEftOnDatabase(user, payrollProcessId, eftType);

                if (payExportColl != null && payExportColl.Count > 0)
                    return payExportColl[0].Data;
                else
                {
                    eftFile = CreateEftFile(user, GetEftData(user, payrollProcessId, employeeNumber), "RP", eftClientNumber, companyName, companyShortName, eftTransmissionFlag, eftFileIndicator);

                    //if employeeNumber is not null, this method is being called to generate an EFT for a single employee.  Do not store in database
                    if (employeeNumber == null && eftFile != null)
                    {
                        PayrollExport payExport = new PayrollExport() { PayrollProcessId = payrollProcessId, Data = eftFile, EftType = eftType };
                        _dynamicsAccess.InsertEftIntoDatabase(user, payExport);
                    }
                }

                return eftFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GenerateEftFile", ex);
                throw;
            }
        }
        public PayrollMasterPaymentCollection GetEftData(DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            try
            {
                return _dynamicsAccess.GetEftData(user, payrollProcessId, employeeNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetPayrollMasterPaymentDirectDeposit", ex);
                throw;
            }
        }
        public byte[] CreateEftFile(DatabaseUser user, PayrollMasterPaymentCollection collection, String status, String eftClientNumber, String companyName, String companyShortName, bool eftTransmissionFlag, String eftFileIndicator)
        {
            try
            {
                return _dynamicsAccess.CreateEftFile(user, collection, status, eftClientNumber, companyName, companyShortName, eftTransmissionFlag, eftFileIndicator);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateEftFile", ex);
                throw;
            }
        }
        public void RevertUpdateTerminatedEmployees(DatabaseUser user, long payrollProcessId)
        {
            _payrollAccess.RevertUpdateTerminatedEmployees(user, payrollProcessId);
        }
        public void PostTransaction(DatabaseUser user, PayrollProcess process, string status, RbcEftEdiParameters ediParms, ScotiaBankEdiParameters scotiaEdiParms, SendEmailParameters emailParms, RbcEftSourceDeductionParameters garnishmentParms, bool craRemitFlag, bool rqRemitFlag, bool sendInvoice, bool garnishmentRemitFlag, bool wcbChequeRemitFlag, bool healthTaxChequeRemitFlag, bool autoAddSecondaryPositions)
        {
            try
            {
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    //update status code...and save current process with process date
                    process.PayrollProcessStatusCode = status;
                    process.ProcessedDate = DateTime.Now;
                    process.PostedUser = user.UserName;
                    _payrollAccess.UpdatePayrollProcess(user, process);

                    //update payroll_master_payment.sequence_number values
                    _dynamicsAccess.UpdateSequenceNumber(user, process);

                    //close the period
                    long newPayrollPeriodId = _payrollManagement.ClosePeriod(user, process);

                    //advance employee position to new step and amount
                    if (process.PayrollProcessRunTypeCode.ToLower() != "adjust")
                        AdvanceEmployeePosition(user, process, autoAddSecondaryPositions);

                    //check if any rehire (RH) records have become active since the current period has closed and a new period has started
                    EmployeePositionCollection employees = new EmployeePositionCollection();
                    bool securityOverride = false;

                    //check employee position for new rehires with arrears records
                    employees = _access.CheckForRehiresWithArrearsRecordsForNewPeriod(user, newPayrollPeriodId, securityOverride);

                    //if rehired employees with arrears records in this new period are found, make a batch and copy the transaction records over and update arrears...
                    if (employees.Count > 0)
                    {
                        foreach (EmployeePosition empPosition in employees)
                        {
                            //get employee arrears records
                            EmployeePaycodeArrearsCollection arrearsCollection = _payrollAccess.GetEmployeePaycodeArrears(user, empPosition.EmployeeId, null);

                            if (arrearsCollection.Count > 0)
                            {
                                PayrollPeriodCollection periodCollection = _payrollAccess.GetPayrollPeriod(user, new PayrollPeriodCriteria() { PayrollProcessGroupCode = process.PayrollProcessGroupCode, GetMinOpenPeriodFlag = true });

                                //insert a batch
                                long tempBatchId = InsertPayrollBatch(user, periodCollection, process.PayrollProcessGroupCode);

                                if (tempBatchId > 0)
                                {
                                    //add employee's arrears tranactions
                                    InsertPayrollBatchTrans(user, arrearsCollection, tempBatchId, process.PayrollProcessGroupCode, periodCollection[0].CutoffDate);

                                    //update the arrears records to be processed (tag the positionid and transaction id too)
                                    foreach (EmployeePaycodeArrears empArrears in arrearsCollection)
                                    {
                                        empArrears.ProcessingEmployeePositionId = empPosition.EmployeePositionId;
                                        empArrears.CodeEmployeePaycodeArrearsStatusCd = "PROC";
                                        _payrollAccess.UpdateEmployeeArrears(user, empArrears);
                                    }
                                }
                            }
                        }
                    }

                    if (process.PayrollProcessRunTypeCode.ToLower() != "adjust")
                    {
                        PerformRemittance(user, process.PayrollProcessId, process.PeriodYear, ediParms, scotiaEdiParms, garnishmentParms, craRemitFlag, rqRemitFlag, wcbChequeRemitFlag, healthTaxChequeRemitFlag);
                    }

                    scope.Complete();
                }

                //if customer is using our EFT, CRA, or RQ remittances then send the invoice (if they want to receive one).
                if (sendInvoice)
                    SendInvoiceViaEmail(user, process, ediParms, scotiaEdiParms, emailParms, garnishmentParms, craRemitFlag, rqRemitFlag);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - PostTransaction", ex);
                throw;
            }
        }

        private void SendInvoiceViaEmail(DatabaseUser user, PayrollProcess process, RbcEftEdiParameters ediParms, ScotiaBankEdiParameters scotiaEdiParms, SendEmailParameters emailParms, RbcEftSourceDeductionParameters garnishmentParms, bool craRemitFlag, bool rqRemitFlag)
        {
            try
            {
                //send Canada invoice
                if ((ediParms != null && ediParms.EftType == "R820DD") || garnishmentParms != null || craRemitFlag || rqRemitFlag)
                    CreateInvoiceAttachmentAndSendEmail(user, process.PayrollProcessId, emailParms);
                //send Jamaica invoice
                else if (scotiaEdiParms != null && scotiaEdiParms.EftType == "SCOT_EDI")
                    CreateJamaicaInvoiceAttachmentAndSendEmail(user, process.PayrollProcessId, emailParms);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - SendInvoiceViaEmail", ex);
                throw;
            }
        }

        private EmployeePositionCollection GetPositionids(PrimarySecondaryEmployeePositionCollection primarySecondaryCollection)
        {
            EmployeePositionCollection employeePositionIds = new EmployeePositionCollection();

            foreach (PrimarySecondaryEmployeePosition item in primarySecondaryCollection)
            {
                if ((item.SalaryPlanAdvanceBasedOnCode == "SP" && (item.PlanHour ?? 0) >= (item.ProbationHour ?? 0) && (item.PlanHour ?? 0) >= (item.MinimumHour ?? Decimal.MaxValue)) ||
                    (item.SalaryPlanAdvanceBasedOnCode == "GR" && (item.GradeHour ?? 0) >= (item.ProbationHour ?? 0) && (item.GradeHour ?? 0) >= (item.MinimumHour ?? Decimal.MaxValue)))
                {
                    if (employeePositionIds[item.EmployeePositionId.ToString()] == null)
                        employeePositionIds.Add(new EmployeePosition() { EmployeePositionId = item.EmployeePositionId });
                }
            }

            return employeePositionIds;
        }

        private void AdvanceEmployeePosition(DatabaseUser user, PayrollProcess process, bool autoAddSecondaryPositions)
        {
            EmployeePositionCollection positionsToUpdate = new EmployeePositionCollection();
            EmployeePositionCollection formerPositionsToUpdate = new EmployeePositionCollection();
            int keyCounter = 0;

            //data from the transactions
            PrimarySecondaryEmployeePositionCollection primarySecondaryCollection = _access.GetPrimarySecondaryEmployeePosition(user, process.PayrollProcessId, process.CutoffDate);

            if (primarySecondaryCollection != null && primarySecondaryCollection.Count > 0)
            {
                //get all positionids we care about and do a batch select
                EmployeePositionCollection employeePositionIds = GetPositionids(primarySecondaryCollection);

                //batch select the positions
                if (employeePositionIds.Count > 0)
                {
                    EmployeePositionCollection currentPositions = null;
                    currentPositions = _employeeManagement.GetBatchEmployeePosition(user, employeePositionIds, true);

                    foreach (PrimarySecondaryEmployeePosition item in primarySecondaryCollection)
                    {
                        if ((item.SalaryPlanAdvanceBasedOnCode == "SP" && (item.PlanHour ?? 0) >= (item.ProbationHour ?? 0) && (item.PlanHour ?? 0) >= (item.MinimumHour ?? Decimal.MaxValue)) ||
                            (item.SalaryPlanAdvanceBasedOnCode == "GR" && (item.GradeHour ?? 0) >= (item.ProbationHour ?? 0) && (item.GradeHour ?? 0) >= (item.MinimumHour ?? Decimal.MaxValue)))
                        {
                            EmployeePosition position = null;
                            EmployeePosition formerPosition = null;

                            //The primarySecondaryCollection contains the primary position id and it's secondaries, so we have multiple position ids
                            //This section creates the new primary position record if there's an advancement of the primary or secondary positions
                            if (formerPositionsToUpdate[item.EmployeePositionId.ToString()] == null)
                            {
                                //get current position from collection
                                position = currentPositions[item.EmployeePositionId.ToString()];

                                if (position.EffectiveDate <= process.CutoffDate) //skip future dated records
                                {
                                    formerPosition = position; //store the former position                            
                                    position = CreateNewEmployeePositionWithAdvancedStepAndAmount(position, process, item); //advance to new step and amount        

                                    //if object isnt already in the collection, add it
                                    if (formerPositionsToUpdate.IndexOf(formerPosition) == -1)
                                        formerPositionsToUpdate.Add(formerPosition);

                                    //set pk to -1, -2, etc
                                    position.EmployeePositionId = --keyCounter;
                                    positionsToUpdate.Add(position);
                                }
                            }

                            if (item.EmployeePositionSecondaryId != null && positionsToUpdate.Count > 0)
                            {
                                //populate secondary positions
                                PopulateSecondaryPositions(item, positionsToUpdate[keyCounter.ToString()]);
                            }
                        }
                    }
                    _employeeManagement.BatchInsertUpdateEmployeePosition(user, positionsToUpdate, formerPositionsToUpdate, null, null, null, null, null, null, autoAddSecondaryPositions, process.PayrollProcessId);
                }
            }
        }
        /// <summary>
        /// This method creates a new employee position from an existing position.
        /// It then advances the salary plan grade step and compensation amount and returns the new employee position.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="position"></param>
        /// <param name="process"></param>
        private EmployeePosition CreateNewEmployeePositionWithAdvancedStepAndAmount(EmployeePosition position, PayrollProcess process, PrimarySecondaryEmployeePosition item)
        {
            EmployeePosition newPosition = position.CreateNew();
            newPosition.EffectiveDate = process.CutoffDate;
            newPosition.EmployeePositionActionCode = "CHNG";

            if (item.EmployeePositionSecondaryId == null)
            {
                newPosition.SalaryPlanGradeStepId = item.AdvanceSalaryPlanGradeStepId;

                if (item.AdvanceAmount > newPosition.CompensationAmount)
                    newPosition.CompensationAmount = item.AdvanceAmount;
            }

            return newPosition;
        }

        private void PopulateSecondaryPositions(PrimarySecondaryEmployeePosition item, EmployeePosition position)
        {
            if (position.SecondaryPositions[item.EmployeePositionSecondaryId.ToString()] != null)
            {
                position.SecondaryPositions[item.EmployeePositionSecondaryId.ToString()].SalaryPlanGradeStepId = item.AdvanceSalaryPlanGradeStepId;

                if (item.AdvanceAmount > position.SecondaryPositions[item.EmployeePositionSecondaryId.ToString()].CompensationAmount)
                    position.SecondaryPositions[item.EmployeePositionSecondaryId.ToString()].CompensationAmount = Convert.ToDecimal(item.AdvanceAmount);
            }
        }

        private void PerformRemittance(DatabaseUser user, long payrollProcessId, decimal periodYear, RbcEftEdiParameters ediParms, ScotiaBankEdiParameters scotiaEdiParms, RbcEftSourceDeductionParameters garnishmentParms, bool craRemitFlag, bool rqRemitFlag, bool wcbChequeRemitFlag, bool healthTaxChequeRemitFlag)
        {
            //if the company is using an RBC EDI eft type for Direct Deposit/Cheques then create the eft file
            if (ediParms != null && ediParms.EftType == "R820DD")
                GenerateRBCEftEdiFile(user, payrollProcessId, ediParms, null);
            else if (scotiaEdiParms != null && scotiaEdiParms.EftType == "SCOT_EDI")
            {
                //if the company is using an ScotiaBank EDI type for Direct Deposit/Cheques then create the eft file
                GenerateScotiaBankEdiFile(user, payrollProcessId, scotiaEdiParms, null);
            }

            //if the company is using the CRA Garnishment edi eft type then create the eft file
            if (garnishmentParms != null)
                GenerateCraEftGarnishmentFile(user, payrollProcessId, garnishmentParms, null);

            //if customer is remitting CRA source deduction data
            if (craRemitFlag)
            {
                //store the CRA remittance data in the cra_remittance table to be picked up by an automated process
                _dynamicsAccess.InsertCraRemittanceData(user, payrollProcessId);
                //store the CRA WCB remittance data in the cra_wcb_remittance table to be picked up by an automated process
                _dynamicsAccess.InsertCraWcbRemittanceData(user, payrollProcessId);
            }

            //if customer is using cheques to remit wcb data, store the Cheque WCB remittance data in the cheque_wcb_remittance table to be picked up by an automated process
            if (wcbChequeRemitFlag)
                _dynamicsAccess.InsertChequeWcbRemittanceData(user, payrollProcessId);

            //if customer is using cheques to remit health tax data (non-quebec), store the cheque health tax remittance data in the cheque_health_tax_remittance table to be picked up by an automated process
            if (healthTaxChequeRemitFlag)
                _dynamicsAccess.InsertChequeHealthTaxRemittanceData(user, payrollProcessId, periodYear);

            //if customer is remitting Revenu Quebec source deduction data, store the RQ remittance data in the revenu_quebec_remittance table to be picked up by an automated process
            if (rqRemitFlag)
                _dynamicsAccess.InsertRqRemittanceData(user, payrollProcessId, periodYear);

            //if customer is using Pre-Authorized Debit for RBC CANADA, create PAD file and send to the bank
            if (ediParms != null && (ediParms.PreAuthorizedDebitFlag && ediParms.PreAuthorizedDebitFileType == "RBC_CANADA"))
                GeneratePreAuthorizedDebitFile(user, payrollProcessId, ediParms);
            else if (scotiaEdiParms != null && (scotiaEdiParms.PreAuthorizedDebitFlag && scotiaEdiParms.PreAuthorizedDebitFileType == "SCOTIA_JAMAICA"))
            {
                //if customer is using Pre-Authorized Debit for ScotiaBank Jamaica, create PAD file and send to the bank
                GenerateScotiaPreAuthorizedDebitFile(user, payrollProcessId, scotiaEdiParms);
            }
        }

        #region pre-authorized debit

        public void GenerateScotiaPreAuthorizedDebitFile(DatabaseUser user, long payrollProcessId, ScotiaBankEdiParameters ediParms)
        {
            try
            {
                byte[] eftFile = null;

                ScotiaPreAuthorizedDebitTotalsCollection collection = GetScotiaPreAuthorizedDebitData(user, payrollProcessId);

                if (collection != null && collection.Count > 0)
                {
                    //get sequence number from db
                    long headerSequenceNumber = _dynamicsAccess.GetSequenceNumber(user, "GLOBAL_SCOTIABANK_HEADER_SEQUENCE");

                    eftFile = CreateScotiaPreAuthorizedDebitFile(user, collection, ediParms, headerSequenceNumber);

                    //store file in the database
                    if (eftFile != null)
                    {
                        PreAuthorizedDebitExport padExport = new PreAuthorizedDebitExport()
                        {
                            PayrollProcessId = payrollProcessId,
                            SequenceNumber = headerSequenceNumber,
                            Data = eftFile,
                            TotalAmount = collection.TotalAmount,
                            PadFileType = ediParms.PreAuthorizedDebitFileType
                        };
                        _dynamicsAccess.InsertPreAuthorizedDebitExportToDatabase(user, padExport);

                        //this will FTP the file, update the pre_authorized_debit_export.cheque_file_transmission_date, and send an email to support
                        FtpPreAuthorizedDebitFileAndSendNotification(user, ediParms.ScotiaFtpName, padExport, collection[0].ProcessGroupDescription, null);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GenerateScotiaPreAuthorizedDebitFile", ex);
                throw;
            }
        }
        public ScotiaPreAuthorizedDebitTotalsCollection GetScotiaPreAuthorizedDebitData(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                return _dynamicsAccess.GetScotiaPreAuthorizedDebitData(user, payrollProcessId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetScotiaPreAuthorizedDebitData", ex);
                throw;
            }
        }
        public byte[] CreateScotiaPreAuthorizedDebitFile(DatabaseUser user, ScotiaPreAuthorizedDebitTotalsCollection collection, ScotiaBankEdiParameters ediParms, long headerSequenceNumber)
        {
            try
            {
                return _dynamicsAccess.CreateScotiaPreAuthorizedDebitFile(user, collection, ediParms, headerSequenceNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateScotiaPreAuthorizedDebitFile", ex);
                throw;
            }
        }

        public void GeneratePreAuthorizedDebitFile(DatabaseUser user, long payrollProcessId, RbcEftEdiParameters ediParms)
        {
            try
            {
                byte[] eftFile = null;

                PreAuthorizedDebitTotalsCollection collection = GetPreAuthorizedDebitData(user, payrollProcessId);

                if (collection != null && collection.Count > 0)
                {
                    //get sequence number from db
                    long headerSequenceNumber = _dynamicsAccess.GetSequenceNumber(user, "GLOBAL_RBC_HEADER_SEQUENCE");

                    eftFile = CreatePreAuthorizedDebitFile(user, collection, ediParms, headerSequenceNumber);

                    //store file in the database
                    if (eftFile != null)
                    {
                        PreAuthorizedDebitExport padExport = new PreAuthorizedDebitExport()
                        {
                            PayrollProcessId = payrollProcessId,
                            SequenceNumber = headerSequenceNumber,
                            Data = eftFile,
                            TotalAmount = collection.TotalAmount,
                            PadFileType = ediParms.PreAuthorizedDebitFileType
                        };

                        _dynamicsAccess.InsertPreAuthorizedDebitExportToDatabase(user, padExport);

                        //this will FTP the file, update the pre_authorized_debit_export.cheque_file_transmission_date, and send an email to support
                        FtpPreAuthorizedDebitFileAndSendNotification(user, ediParms.RbcFtpName, padExport, collection[0].ProcessGroupDescription, null);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GeneratePreAuthorizedDebitFile", ex);
                throw;
            }
        }

        public void FtpPreAuthorizedDebitFileAndSendNotification(DatabaseUser user, string rbcFtpName, PreAuthorizedDebitExport preAuthDebitExport, string processGroupDescription, ChequeWcbPreAuthorizedDebitExport wcbEstimatePreAuthDebitExport)
        {
            FTP.SftpManagement ftpClass = new FTP.SftpManagement();
            String fileName = null;
            SendEmailParameters emailParms = null;

            //get ftp protocol details
            ExportFtp exportFtp = _payrollAccess.GetExportFtp(user, null, "FTPS", rbcFtpName)[0]; //only will be 1 row

            //upload pre-authorized debit file for payroll to RBC
            if (preAuthDebitExport != null)
            {
                fileName = String.Format("{0:yyyyMMddHHmmssfff}_{1}_PRE_AUTHORIZED_DEBIT_{2}.txt", DateTime.Now, user.DatabaseName, preAuthDebitExport.PayrollProcessId);
                ftpClass.UploadFtpsFile(fileName, preAuthDebitExport.Data, exportFtp.UriName, exportFtp.Login, exportFtp.Password, exportFtp.EnableSslFlag, exportFtp.BinaryModeFlag, exportFtp.PassiveModeFlag);
                _dynamicsAccess.UpdatePreAuthorizedDebitExportFileTransmissionDate(user, preAuthDebitExport.PreAuthorizedDebitExportId);
                emailParms = _reportManagement.GetEmailParms(user, null, null, null, DateTime.Now, processGroupDescription, false, false, false, false, false, false, true, false, false, false);
            }
            //upload wcb estimate pre-authorized debit file for payroll to RBC
            else if (wcbEstimatePreAuthDebitExport != null)
            {
                fileName = String.Format("{0:yyyyMMddHHmmssfff}_{1}_WCB_ESTIMATE_PRE_AUTHORIZED_DEBIT_{2}.txt", DateTime.Now, user.DatabaseName, wcbEstimatePreAuthDebitExport.CodeWsibCd);
                ftpClass.UploadFtpsFile(fileName, wcbEstimatePreAuthDebitExport.Data, exportFtp.UriName, exportFtp.Login, exportFtp.Password, exportFtp.EnableSslFlag, exportFtp.BinaryModeFlag, exportFtp.PassiveModeFlag);
                _dynamicsAccess.UpdateChequeWcbEstimatePreAuthorizedDebitExportFileTransmissionDate(user, wcbEstimatePreAuthDebitExport.ChequeWcbPreAuthorizedDebitExportId);
                emailParms = _reportManagement.GetEmailParms(user, null, wcbEstimatePreAuthDebitExport.CodeWsibCd, null, DateTime.Now, null, false, false, false, false, false, false, false, true, false, false);
            }

            //Send notification once file transfer is complete
            if (emailParms != null)
                EmailManagement.SendMessage(emailParms);
        }

        public byte[] CreatePreAuthorizedDebitFile(DatabaseUser user, PreAuthorizedDebitTotalsCollection collection, RbcEftEdiParameters ediParms, long headerSequenceNumber)
        {
            try
            {
                return _dynamicsAccess.CreatePreAuthorizedDebitFile(user, collection, ediParms, headerSequenceNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreatePreAuthorizedDebitFile", ex);
                throw;
            }
        }

        public PreAuthorizedDebitTotalsCollection GetPreAuthorizedDebitData(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                return _dynamicsAccess.GetPreAuthorizedDebitData(user, payrollProcessId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetPreAuthorizedDebitData", ex);
                throw;
            }
        }
        #endregion

        #region cheque health tax remittance
        public ChequeHealthTaxRemittanceScheduleCollection GetChequeHealthTaxRemittanceSchedule(DatabaseUser user, DateTime date)
        {
            return _dynamicsAccess.GetChequeHealthTaxRemittanceSchedule(user, date);
        }

        #endregion

        #region cheque wcb remittance
        public ChequeWcbRemittanceScheduleCollection GetChequeWcbRemittanceSchedule(DatabaseUser user, DateTime date, bool queryForInvoice)
        {
            return _dynamicsAccess.GetChequeWcbRemittanceSchedule(user, date, queryForInvoice);
        }
        public void CreateInvoiceAttachmentAndSendEmail(DatabaseUser user, long payrollProcessId, SendEmailParameters emailParms)
        {
            //attach the invoice to the email
            emailParms.Attachment = new ReportManagement().GetReport(user, "CustomerInvoice", payrollProcessId, null, "PDF", null, null, null, null, null, null);

            //Send invoice once file transfer is complete
            emailParms.FileName = new ReportManagement().GenerateFileName(user, emailParms.FileName, payrollProcessId);
            EmailManagement.SendMessage(emailParms);
        }

        public void CreateJamaicaInvoiceAttachmentAndSendEmail(DatabaseUser user, long payrollProcessId, SendEmailParameters emailParms)
        {
            //attach the invoice to the email
            emailParms.Attachment = new ReportManagement().GetReport(user, "EftCustomerInvoiceJamaica", payrollProcessId, null, "PDF", null, null, null, null, null, null);

            //Send invoice once file transfer is complete
            emailParms.FileName = new ReportManagement().GenerateFileName(user, emailParms.FileName, payrollProcessId);
            EmailManagement.SendMessage(emailParms);
        }

        #endregion

        private long InsertPayrollBatch(DatabaseUser user, PayrollPeriodCollection period, String processGroupCode)
        {
            PayrollBatch newBatch = new PayrollBatch();
            newBatch.PayrollProcessGroupCode = processGroupCode;
            newBatch.BatchSource = processGroupCode + "-" + period[0].CutoffDate;
            newBatch.PayrollBatchStatusCode = "AP";
            newBatch.PayrollProcessRunTypeCode = "REGULAR";
            newBatch.Description = "BENEFITED LEAVE RETURN BATCH";
            newBatch.SystemGeneratedFlag = false;

            return _payrollAccess.InsertPayrollBatch(user, newBatch).PayrollBatchId;
        }
        public void InsertPayrollBatchTrans(DatabaseUser user, EmployeePaycodeArrearsCollection arrears, long payrollBatchId, String payrollProcessGroupCode, DateTime? transactionDate)
        {
            try
            {
                int i = 0;

                foreach (EmployeePaycodeArrears empArrears in arrears)
                {
                    empArrears.PayrollTransactionId = i--;
                    empArrears.EntryTypeCode = "NORM"; //Normal
                    empArrears.PayrollBatchId = payrollBatchId;
                    empArrears.UpdateUser = user.UserName;
                }

                //insert these records into the current batch
                foreach (PayrollTransaction arrearsTransactions in arrears)
                    _payrollAccess.InsertPayrollTransaction(user, arrearsTransactions);

                //copy the transaction id from the PayrollTransaction object to the EmployeePaycodeArrears object
                foreach (EmployeePaycodeArrears empArrears in arrears)
                    empArrears.PayrollTransactionId = ((PayrollTransaction)empArrears).PayrollTransactionId;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorklinksDynamicsManagement - InsertPayrollBatchTrans", ex);
                throw;
            }
        }
        #endregion

        #region CSB Export
        public byte[] CreateCSBExport(CanadaRevenueAgencyBondExportFileCollection collection)
        {
            return _dynamicsAccess.CreateCSBExport(collection);
        }
        #endregion

        #region temporary
        public KeyValuePair<String, String>[] GetDynamicsOutputFileList(DatabaseUser user, String dynamicsGPOutputFolder, long payrollProcessId)
        {
            try
            {
                //load dropdown
                if (!dynamicsGPOutputFolder.EndsWith(Path.DirectorySeparatorChar.ToString()))
                    dynamicsGPOutputFolder = dynamicsGPOutputFolder + Path.DirectorySeparatorChar;

                String folder = String.Format(dynamicsGPOutputFolder + @"{0}", payrollProcessId);
                String[] fileNames = Directory.GetFiles(folder, "*.txt");
                List<KeyValuePair<String, String>> files = new List<KeyValuePair<String, String>>();

                foreach (String fileName in fileNames)
                    files.Add(new KeyValuePair<String, String>(Path.GetFileName(fileName), fileName));

                return files.ToArray();
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetDynamicsOutputFileList", ex);
                throw;
            }
        }
        public String getFileText(DatabaseUser user, String filePath)
        {
            try
            {
                String rtn = null;

                using (StreamReader reader = new StreamReader(filePath))
                    rtn = reader.ReadToEnd();

                return rtn;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - getFileText", ex);
                throw;
            }
        }
        #endregion

        #region real export to ftp

        public void CreateAndExportRealEmployeeFiles(DatabaseUser user, string realFtpName, string laborFileName, string employeeFileName, string adjustmentFileName)
        {
            try
            {
                //generate the 3 exports
                Dictionary<String, byte[]> exportFiles = new Dictionary<string, byte[]>()
                {
                    //add the labor level export, labor level export, 
                    { String.Format(laborFileName, DateTime.Now), GenerateREALLaborLevelExportFile(user) ?? new byte[0] },
                    { String.Format(employeeFileName, DateTime.Now), GenerateREALEmployeeExportFile(user) ?? new byte[0] },
                    { String.Format(adjustmentFileName, DateTime.Now), GenerateREALAdjustmentRuleExportFile(user) ?? new byte[0] }
                };

                //get ftp protocol details
                ExportFtp exportFtp = _payrollManagement.GetExportFtp(user, null, null, realFtpName)[0]; //only will be 1 row

                FTP.SftpManagement ftpClass = new FTP.SftpManagement();

                //ftp the eft file to the bank.  
                foreach (KeyValuePair<String, byte[]> files in exportFiles) //send files out one at a time
                {
                    ftpClass.UploadSftpFile(exportFtp.UriName, exportFtp.Port, exportFtp.Directory, exportFtp.UnixPathFlag, exportFtp.Login, exportFtp.Password, files.Value, files.Key);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating or sending files", ex);
            }

        }
        #endregion

        #region adjustment rule export
        public byte[] GenerateREALAdjustmentRuleExportFile(DatabaseUser user)
        {
            try
            {
                return CreateREALAdjustmentRuleExportFile(user, GetREALAdjustmentRuleExportData(user));
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GenerateREALAdjustmentRuleExportFile", ex);
                throw;
            }
        }
        public AdjustmentRuleExportCollection GetREALAdjustmentRuleExportData(DatabaseUser user)
        {
            try
            {
                return _dynamicsAccess.GetREALAdjustmentRuleExportData(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetREALAdjustmentRuleExportData", ex);
                throw;
            }
        }
        public byte[] CreateREALAdjustmentRuleExportFile(DatabaseUser user, AdjustmentRuleExportCollection exportCollection)
        {
            try
            {
                return _dynamicsAccess.CreateREALAdjustmentRuleExportFile(exportCollection);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateREALLaborLevelExportFile", ex);
                throw;
            }
        }
        #endregion

        #region REAL Labor Level Export
        public byte[] GenerateREALLaborLevelExportFile(DatabaseUser user)
        {
            try
            {
                return CreateREALLaborLevelExportFile(user, GetREALLaborLevelExportData(user));
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GenerateREALLaborLevelExportFile", ex);
                throw;
            }
        }

        public LaborLevelExportCollection GetREALLaborLevelExportData(DatabaseUser user)
        {
            try
            {
                return _dynamicsAccess.GetREALLaborLevelExportData(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetREALLaborLevelExportData", ex);
                throw;
            }
        }
        public byte[] CreateREALLaborLevelExportFile(DatabaseUser user, LaborLevelExportCollection exportCollection)
        {
            try
            {
                return _dynamicsAccess.CreateREALLaborLevelExportFile(exportCollection);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateREALLaborLevelExportFile", ex);
                throw;
            }
        }
        #endregion

        #region REAL export
        public byte[] GenerateREALEmployeeExportFile(DatabaseUser user)
        {
            try
            {
                return CreateREALEmployeeExportFile(user, GetREALExportData(user));
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GenerateREALExportFile", ex);
                throw;
            }
        }

        public REALExportCollection GetREALExportData(DatabaseUser user)
        {
            try
            {
                return _dynamicsAccess.GetREALExportData(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - GetREALExportData", ex);
                throw;
            }
        }

        public byte[] CreateREALEmployeeExportFile(DatabaseUser user, REALExportCollection exportCollection)
        {
            try
            {
                return _dynamicsAccess.CreateREALEmployeeExportFile(exportCollection);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreateREALExportFile", ex);
                throw;
            }
        }

        #endregion
    }
}