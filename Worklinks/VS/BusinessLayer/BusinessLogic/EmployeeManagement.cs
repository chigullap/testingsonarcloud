﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Transactions;
using System.Web.Configuration;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;
using WorkLinks.DataLayer.DataAccess;
using WorkLinks.DataLayer.DataAccess.SqlServer;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class EmployeeManagement
    {
        #region fields
        private EmployeeAccess _employeeAccess = null;
        private CodeAccess _codeAccess = null;
        private PersonAccess _personAccess = null;
        private PayrollAccess _payrollAccess = null;
        private WizardAccess _wizardAccess = null;
        private AddressAccess _addressAccess = null;
        private YearEndAccess _yearEndAccess = null;
        private DataLayer.DataAccess.Dynamics.DynamicsAccess _dynamicsAccess = null;
        private CodeManagement _codeManagement = null;
        private ApplicationResourceManagement _guiManagement = null;
        private WizardManagement _wizardManagement = null;
        private PayrollManagement _payrollManagement = null;
        private PersonManagement _personManagement = null;
        private SalaryPlanAccess _salaryPlanAccess = null;

        #endregion

        #region properties

        public SalaryPlanAccess SalaryPlanAccess => _salaryPlanAccess ?? (_salaryPlanAccess = new SalaryPlanAccess());

        public EmployeeAccess EmployeeAccess
        {
            get
            {
                if (_employeeAccess == null)
                    _employeeAccess = new EmployeeAccess();

                return _employeeAccess;
            }
        }
        public CodeAccess CodeAccess
        {
            get
            {
                if (_codeAccess == null)
                    _codeAccess = new CodeAccess();

                return _codeAccess;
            }
        }
        public PersonAccess PersonAccess
        {
            get
            {
                if (_personAccess == null)
                    _personAccess = new PersonAccess();

                return _personAccess;
            }
        }
        public PayrollAccess PayrollAccess
        {
            get
            {
                if (_payrollAccess == null)
                    _payrollAccess = new PayrollAccess();

                return _payrollAccess;
            }
        }
        public WizardAccess WizardAccess
        {
            get
            {
                if (_wizardAccess == null)
                    _wizardAccess = new WizardAccess();

                return _wizardAccess;
            }
        }
        public AddressAccess AddressAccess
        {
            get
            {
                if (_addressAccess == null)
                    _addressAccess = new AddressAccess();

                return _addressAccess;
            }
        }
        public YearEndAccess YearEndAccess
        {
            get
            {
                if (_yearEndAccess == null)
                    _yearEndAccess = new YearEndAccess();

                return _yearEndAccess;
            }
        }
        private DataLayer.DataAccess.Dynamics.DynamicsAccess DynamicsAccess
        {
            get
            {
                if (_dynamicsAccess == null)
                    _dynamicsAccess = new DataLayer.DataAccess.Dynamics.DynamicsAccess();

                return _dynamicsAccess;
            }
        }
        private CodeManagement CodeManagement
        {
            get
            {
                if (_codeManagement == null)
                    _codeManagement = new CodeManagement();

                return _codeManagement;
            }
        }
        public ApplicationResourceManagement GuiManagement
        {
            get
            {
                if (_guiManagement == null)
                    _guiManagement = new ApplicationResourceManagement();

                return _guiManagement;
            }
        }
        private WizardManagement WizardManagement
        {
            get
            {
                if (_wizardManagement == null)
                    _wizardManagement = new WizardManagement();

                return _wizardManagement;
            }
        }
        public PayrollManagement PayrollManagement
        {
            get
            {
                if (_payrollManagement == null)
                    _payrollManagement = new PayrollManagement();

                return _payrollManagement;
            }
        }
        public PersonManagement PersonManagement
        {
            get
            {
                if (_personManagement == null)
                    _personManagement = new PersonManagement();

                return _personManagement;
            }
        }
        #endregion

        #region employee biographical
        public EmployeeCollection GetEmployee(DatabaseUser user, EmployeeCriteria criteria)
        {
            try
            {
                EmployeeCollection employees = EmployeeAccess.GetEmployee(user, (long)criteria.EmployeeId, criteria.SecurityOverrideFlag);

                foreach (Employee employee in employees)
                {
                    PersonCollection persons = PersonAccess.GetPerson(user, employee.PersonId);
                    GovernmentIdentificationNumberTypeTemplateCollection governmentIdentificationNumberTypes = PersonAccess.GetGovernmentIdentificationNumberTypeTemplate(user, persons[0].GovernmentIdentificationNumberTypeCode);

                    //assuming 1 person per employee
                    persons[0].CopyTo(employee);

                    //assuming 1 type per employee
                    if (governmentIdentificationNumberTypes.Count > 0)
                        governmentIdentificationNumberTypes[0].CopyTo(employee);

                    //include contacts if requested
                    if (criteria.IncludeContacts)
                        employee.Contacts = GetContact(user, employee.EmployeeId, null);

                    //include contact types if requested
                    if (criteria.IncludeContactTypes)
                        employee.ContactTypes = GetContactType(user, employee.EmployeeId, criteria.IncludeContactTypeCode);

                    //include primary address if requested
                    if (criteria.IncludePrimaryAddress)
                        employee.PrimaryAddress = PersonAccess.GetPrimaryPersonAddress(user, employee.PersonId);

                    //include StatutoryDeduction
                    if (criteria.IncludeStatutoryDeduction)
                    {
                        employee.StatutoryDeductionSummary = new StatutoryDeductionSummary();
                        GetStatutoryDeduction(user, employee.EmployeeId, criteria.SecurityOverrideFlag).ActiveStatutoryDeduction.CopyTo(employee.StatutoryDeductionSummary);
                        employee.StatutoryDeductionSummary.EmployerNumber = GetBusinessNumber(user, employee.StatutoryDeductionSummary.BusinessNumberId)[0].EmployerNumber;
                        employee.StatutoryDeductionSummary.DefaultProvincialTaxClaim = GetStatutoryDeductionDefaults(user, employee.StatutoryDeductionSummary.ProvinceStateCode)[0].ProvincialTaxClaim;
                    }

                    //include banking
                    if (criteria.IncludeBanking)
                        employee.BankingCollection = GetEmployeeBanking(user, employee.EmployeeId, criteria.SecurityOverrideFlag);

                    //include employment information 
                    if (criteria.IncludeEmploymentInformation)
                        employee.EmploymentInformation = GetEmployeeEmploymentInformation(user, employee.EmployeeId, criteria.SecurityOverrideFlag).FirstItem;

                    //include current employee position 
                    if (criteria.IncludeCurrentPosition)
                    {
                        PayrollPeriod payrollPeriod = PayrollAccess.GetPayrollPeriod(user, new PayrollPeriodCriteria() { PayrollProcessGroupCode = criteria.PayrollProcessGroupCode, GetMinOpenPeriodFlag = true })[0];
                        EmployeePositionCollection positions = EmployeeAccess.GetEmployeePosition(user, new EmployeePositionCriteria() { EmployeeId = employee.EmployeeId, SecurityOverrideFlag = criteria.SecurityOverrideFlag, });
                        employee.CurrentEmployeePosition = positions.GetCurrentEmployeePosition(payrollPeriod.CutoffDate);
                    }

                    //include employee photo
                    if (criteria.IncludeEmployeePhotoFlag)
                    {
                        EmployeePhotoCollection photos = GetEmployeePhoto(user, employee.EmployeeId);
                        if (photos.Count > 0)
                            employee.EmployeePhotoObject = photos[0];
                        else
                            employee.EmployeePhotoObject = null;
                    }

                    //include employee employment equities
                    if (criteria.IncludeEmployeeEmploymentEquityFlag)
                        employee.EmploymentEquities = GetEmployeeEmploymentEquity(user, employee.EmployeeId);

                    //load default paycodes
                    if (criteria.IncludePayCode)
                    {
                        employee.Paycodes = GetEmployeePaycode(user, employee.EmployeeId, criteria.SecurityOverrideFlag, true);

                        //if no one has paycodes, dynamicsBatchCommand will blow up when GP tries to import paycodes, therefore assign a dummy paycode
                        if (employee.Paycodes != null && employee.Paycodes.Count == 0)
                            employee.Paycodes = new EmployeePaycodeCollection() {
                                new EmployeePaycode {
                                    PaycodeCode = "SAL",
                                    PaycodeTypeCode = "1",
                                    AmountUnits = 0,
                                    AmountRate = 0,
                                    EmployeeId = employee.EmployeeId,
                                    CreateUser = employee.CreateUser,
                                    UpdateUser = employee.UpdateUser,
                                    CreateDatetime = DateTime.Now,
                                    UpdateDatetime = DateTime.Now
                                }
                            };
                    }
                }

                return employees;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployee", ex);
                throw;
            }
        }
        public EmployeeCollection GetEmployeeBatch(DatabaseUser user, String[] employeeIdentifiers)
        {
            try
            {
                return EmployeeAccess.GetEmployeeBatch(user, employeeIdentifiers);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeBatch", ex);
                throw;
            }
        }
        public void UpdateEmployee(DatabaseUser user, Employee employee, List<ContactType> deletedItems)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    PersonAccess.UpdatePerson(user, employee);
                    EmployeeAccess.UpdateEmployee(user, employee);

                    if ((employee.ContactTypes != null && employee.ContactTypes.Count > 0) || (deletedItems != null && deletedItems.Count > 0))
                        EmployeeAccess.UpdateContactType(user, employee.ContactTypes, deletedItems);

                    if (employee.EmployeePhotoObject != null)
                        UpdateEmployeePhoto(user, employee.EmployeePhotoObject);

                    if (employee.EmploymentEquities != null)
                    {
                        DeleteEmployeeEmploymentEquity(user, employee.EmployeeId);

                        foreach (EmployeeEmploymentEquity employmentEquity in employee.EmploymentEquities)
                            InsertEmployeeEmploymentEquity(user, employmentEquity);
                    }

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployee", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 2601)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.UniqueIndexFault);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public String GetEmployeeNumber(DatabaseUser user, long employeeId)
        {
            try
            {
                return EmployeeAccess.GetEmployee(user, employeeId, false)[0].EmployeeNumber;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeNumber", ex);
                throw;
            }
        }
        public bool CheckDatabaseForDuplicateSin(DatabaseUser user, string sin, long employeeId, string employeeNumber)
        {
            try
            {
                return PersonAccess.CheckDatabaseForDuplicateSin(user, sin, employeeId, employeeNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - SinRule", ex);
                throw;
            }
        }
        public void InsertEmployee(DatabaseUser user, Employee employee)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    PersonAccess.InsertPerson(user, user.DatabaseName, employee);
                    EmployeeAccess.InsertEmployee(user, employee);

                    if (employee.EmployeePhotoObject != null)
                    {
                        employee.EmployeePhotoObject.EmployeeId = employee.EmployeeId;
                        UpdateEmployeePhoto(user, employee.EmployeePhotoObject);
                    }

                    if (employee.EmploymentEquities != null)
                    {
                        foreach (EmployeeEmploymentEquity employmentEquity in employee.EmploymentEquities)
                        {
                            employmentEquity.EmployeeId = employee.EmployeeId;
                            InsertEmployeeEmploymentEquity(user, employmentEquity);
                        }
                    }

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployee", ex);
                throw;
            }
        }
        public void DeleteEmployee(DatabaseUser user, Employee employee)
        {
            try
            {
                DeleteEmployeeEmploymentEquity(user, employee.EmployeeId);
                EmployeeAccess.DeleteEmployee(user, employee);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployee", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region employee export

        public byte[] GetEmployeeBiographicsFile(DatabaseUser user, IEnumerable<long> employeeIds)
        {
            IEnumerable<EmployeeBiographicFileItem> bios = EmployeeAccess.GetEmployeeBiographics(user, employeeIds).Select(eb => eb.ToFileItem());

            FileHelpers.MultiRecordEngine engine = new FileHelpers.MultiRecordEngine(typeof(EmployeeBiographicsFileHeader), typeof(EmployeeBiographicFileItem));
            EmployeeBiographicsFileHeader[] headers = new[] { new EmployeeBiographicsFileHeader() };

            StringBuilder fileData = new StringBuilder(engine.WriteString(headers));
            fileData.Append(engine.WriteString(bios));

            var encoding = new UTF8Encoding(false);
            return encoding.GetBytes(fileData.ToString());
        }

        #endregion

        #region employee import
        public Dictionary<String, Employee> GetEmployeeCache(DatabaseUser user, IImportEmployeeIdentifier[] employees)
        {
            List<String> employeeIdentifiers = new List<String>();
            foreach (IImportEmployeeIdentifier employee in employees)
                employeeIdentifiers.Add(employee.ImportExternalIdentifier);

            Dictionary<String, Employee> employeeCache = new Dictionary<String, Employee>();

            //get the existing employees from the database and add them to the dictionary
            foreach (Employee employee in GetEmployeeBatch(user, employeeIdentifiers.ToArray()))
                employeeCache.Add(employee.EmployeeNumber, employee);

            return employeeCache;
        }
        private void EmployeeDecodeCode(DatabaseUser user, WorkLinksEmployee[] employees)
        {
            String mismatchMessage = "Cannot match code:{0} type:{1} in WorkLinks";

            //get codes
            CodeCollection maritalStatusCodes = CodeManagement.GetCodeTable(user, CodeTableType.MaritalStatusCode);
            CodeCollection bilingualismCodes = CodeManagement.GetCodeTable(user, CodeTableType.BilingualismCode);
            CodeCollection provinceStateCodes = CodeManagement.GetCodeTable(user, CodeTableType.ProvinceStateCode);
            CodeCollection citizenshipCodes = CodeManagement.GetCodeTable(user, CodeTableType.CitizenshipCode);
            CodeCollection languageCodes = CodeManagement.GetCodeTable(user, CodeTableType.LanguageCode);
            CodeCollection titleCodes = CodeManagement.GetCodeTable(user, CodeTableType.TitleCode);
            CodeCollection genderCodes = CodeManagement.GetCodeTable(user, CodeTableType.GenderCode);
            CodeCollection governmentIdentificationNumberTypeCodes = CodeManagement.GetCodeTable(user, CodeTableType.GovernmentIdentificationNumberTypeCode);
            CodeCollection smokerCodes = CodeManagement.GetCodeTable(user, CodeTableType.SmokerCode);
            CodeCollection employmentEquityCodes = CodeManagement.GetCodeTable(user, CodeTableType.EmploymentEquityCode);

            // WD-1147: default to Canada if the type isn't supplied            
            CodeObject defaultCitizenshipCode = citizenshipCodes.FirstOrDefault(a => a.Code.Equals("CA"));
            CodeObject defaultGovernmentIdentificationNumberTypeCode = governmentIdentificationNumberTypeCodes.FirstOrDefault(a => a.Code.Equals("CAN"));

            foreach (WorkLinksEmployee employee in employees)
            {
                //decode mariage
                if (!(employee.MaritalStatusExternalIdentifier == null) && !employee.MaritalStatusExternalIdentifier.Equals(""))
                {
                    employee.MaritalStatusCode = maritalStatusCodes.DecodeCode(employee.MaritalStatusCode);
                    if (employee.MaritalStatusCode == null)
                        employee.SetDecodeCodeValidationMessage(String.Format(mismatchMessage, employee.MaritalStatusExternalIdentifier, CodeTableType.MaritalStatusCode));
                }

                //decode BilingualismCode
                if (!(employee.BilingualismExternalIdentifier == null) && !employee.BilingualismExternalIdentifier.Equals(""))
                {
                    employee.BilingualismCode = bilingualismCodes.DecodeCode(employee.BilingualismExternalIdentifier);
                    if (employee.BilingualismCode == null)
                        employee.SetDecodeCodeValidationMessage(String.Format(mismatchMessage, employee.BilingualismExternalIdentifier, CodeTableType.BilingualismCode));
                }
                //decode Health Care ProvinceStateCode
                if (!(employee.HealthCareProvinceStateExternalIdentifier == null) && !employee.HealthCareProvinceStateExternalIdentifier.Equals(""))
                {
                    employee.HealthCareProvinceStateCode = provinceStateCodes.DecodeCode(employee.HealthCareProvinceStateExternalIdentifier);
                    if (employee.HealthCareProvinceStateCode == null)
                        employee.SetDecodeCodeValidationMessage(String.Format(mismatchMessage, employee.HealthCareProvinceStateExternalIdentifier, CodeTableType.ProvinceStateCode));
                }

                //decode CitizenshipCode
                if (!(employee.CitizenshipExternalIdentifier == null) && !employee.CitizenshipExternalIdentifier.Equals(""))
                {
                    employee.CitizenshipCode = citizenshipCodes.DecodeCode(employee.CitizenshipExternalIdentifier);
                    if (employee.CitizenshipCode == null)
                        employee.SetDecodeCodeValidationMessage(String.Format(mismatchMessage, employee.CitizenshipExternalIdentifier, CodeTableType.CitizenshipCode));
                }
                // WD-1147: default to Canada if the type isn't supplied
                else
                {
                    if (defaultCitizenshipCode != null)
                    {
                        employee.CitizenshipCode = defaultCitizenshipCode.Code;
                    }
                }

                // decode GovernmentIdentificationNumberTypeCode
                if (!string.IsNullOrEmpty(employee.GovernmentIdentificationNumberTypeCode))
                {
                    employee.GovernmentIdentificationNumberTypeCode = governmentIdentificationNumberTypeCodes.DecodeCode(employee.GovernmentIdentificationNumberTypeCode);
                }
                else
                {
                    if (defaultGovernmentIdentificationNumberTypeCode != null)
                    {
                        employee.GovernmentIdentificationNumberTypeCode = defaultGovernmentIdentificationNumberTypeCode.Code;
                    }
                }

                //decode LanguageCode
                if (!(employee.LanguageExternalIdentifier == null) && !employee.LanguageExternalIdentifier.Equals(""))
                {
                    employee.LanguageCode = languageCodes.DecodeCode(employee.LanguageExternalIdentifier);
                    if (employee.LanguageCode == null)
                        employee.SetDecodeCodeValidationMessage(String.Format(mismatchMessage, employee.LanguageExternalIdentifier, CodeTableType.LanguageCode));
                }

                //decode TitleCode
                if (!(employee.TitleExternalIdentifier == null) && !employee.TitleExternalIdentifier.Equals(""))
                {
                    employee.TitleCode = titleCodes.DecodeCode(employee.TitleExternalIdentifier);
                    if (employee.TitleCode == null)
                        employee.SetDecodeCodeValidationMessage(String.Format(mismatchMessage, employee.TitleExternalIdentifier, CodeTableType.TitleCode));
                }

                //decode GenderCode
                if (!(employee.GenderExternalIdentifier == null) && !employee.GenderExternalIdentifier.Equals(""))
                {
                    employee.GenderCode = genderCodes.DecodeCode(employee.GenderExternalIdentifier);
                    if (employee.GenderCode == null)
                        employee.SetDecodeCodeValidationMessage(String.Format(mismatchMessage, employee.GenderExternalIdentifier, CodeTableType.GenderCode));
                }

                // decode smoker code
                if (!string.IsNullOrEmpty(employee.SmokerCode))
                {
                    employee.SmokerCode = smokerCodes.DecodeCode(employee.SmokerCode);
                    if (employee.SmokerCode == null)
                        employee.SetDecodeCodeValidationMessage(string.Format(mismatchMessage, employee.SmokerCode, CodeTableType.SmokerCode));
                }

                // decode EmploymentEquityCode
                if (!string.IsNullOrEmpty(employee.EmploymentEquityExternalIdentifier))
                {
                    var equityCode = employmentEquityCodes.DecodeCode(employee.EmploymentEquityExternalIdentifier);
                    if (equityCode != null)
                    {
                        employee.EmploymentEquities = new EmployeeEmploymentEquityCollection();
                        employee.EmploymentEquities.Add(new EmployeeEmploymentEquity { EmploymentEquityCode = equityCode });
                    }
                }
            }
        }
        public void ProcessEmployee(DatabaseUser user, WorkLinksEmployee[] xsdObject, ImportExportLog log, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions)
        {
            Dictionary<String, Employee> employeeCache = GetEmployeeCache(user, xsdObject);
            ProcessEmployee(user, employeeCache, xsdObject, log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);
        }
        public void ProcessEmployee(DatabaseUser user, Dictionary<String, Employee> employeeCache, WorkLinksEmployee[] employees, ImportExportLog log, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions)
        {
            try
            {
                StringBuilder output = new StringBuilder();
                int failedRows = 0;
                int successfulRows = 0;

                using (TransactionScope scope = new TransactionScope())
                {
                    WorkLinksEmployeeCollection existingEmployees = new WorkLinksEmployeeCollection();
                    WorkLinksEmployeeCollection pendingEmployees = new WorkLinksEmployeeCollection();

                    EmployeeDecodeCode(user, employees);

                    foreach (WorkLinksEmployee employee in employees)
                    {
                        employee.EmployeeNumber = employee.ImportExternalIdentifier;

                        if (employee.MeetsEmployeeRequirements())
                        {
                            if (ValidateSIN(user, employee.GovernmentIdentificationNumber1, employee.GovernmentIdentificationNumberTypeCode))
                            {
                                successfulRows++;
                                if (employeeCache.ContainsKey(employee.ImportExternalIdentifier)) //employee was found in the database
                                {
                                    employee.EmployeeId = employeeCache[employee.ImportExternalIdentifier].EmployeeId;
                                    employee.PersonId = employeeCache[employee.ImportExternalIdentifier].PersonId;
                                    existingEmployees.Add(employee);
                                }
                                else
                                    pendingEmployees.Add(employee);
                            }
                            else
                            {
                                failedRows++;
                                output.Append("WorkLinksEmployee: Invalid SIN for Employee " + employee.EmployeeNumber + Environment.NewLine);
                            }
                        }
                        else
                        {
                            failedRows++;
                            output.Append(employee.GetValidationMessage());
                        }
                    }

                    ImportEmployee(user, existingEmployees, pendingEmployees, autoGenerateEmployeeNumber, employeeNumberFormat, autoAddSecondaryPositions);

                    if (importDirtySave || failedRows == 0)
                        scope.Complete();
                }

                output.AppendLine("----------");
                output.AppendLine("# rows total: " + (successfulRows + failedRows));
                output.AppendLine("# rows succeeded: " + successfulRows);
                output.AppendLine("# rows failed: " + failedRows);

                if (failedRows > 0)
                {
                    if (importDirtySave)
                        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n" + "----------\r\n");
                    else
                        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportCompleteProcessFailed") + "\r\n" + "----------\r\n");
                }
                else
                    output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n");

                log.SuccessFlag = true;
                log.ProcessingOutput += output.ToString();
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - ProcessEmployee", ex);
                throw;
            }
        }
        public bool ValidateSIN(DatabaseUser user, String SIN, String governmentIdentificationNumberTypeCode)
        {
            if (governmentIdentificationNumberTypeCode == "CAN")
            {
                try
                {
                    int firstPart = 0;
                    int secondPart = 0;
                    int finalNumber = 0;
                    int tempPart = 0;
                    String tempHolder = "";

                    //remove any dashes
                    SIN = SIN.Replace("-", "");

                    //PERFORM MOD 10 VALIDATION ON SIN

                    //1. Starting with the 1st digit, add every 2nd digit
                    for (int i = 0; i < 9; i += 2)
                        firstPart = firstPart + Convert.ToInt16(SIN.Substring(i, 1));

                    //2. Double and add the other digits. If the doubling results in more than 9, add the two digits in the result, i.e., 2 X 9 = 18, so 1 + 8 = 9.
                    for (int i = 1; i < 9; i += 2)
                    {
                        tempPart = Convert.ToInt16(SIN.Substring(i, 1)) * 2;

                        if (tempPart > 9)
                        {
                            tempHolder = tempPart.ToString();
                            tempPart = Convert.ToInt16(tempHolder.Substring(0, 1)) + Convert.ToInt16(tempHolder.Substring(1, 1));
                        }

                        secondPart += tempPart;
                    }

                    //3. Add the results of 1 and 3
                    finalNumber = firstPart + secondPart;

                    //4. The result must be divisible by 10 otherwise it is an invalid SIN
                    return (finalNumber % 10 == 0);
                }
                catch (Exception)
                {//if the formula above fails the sin is invalid (most likly because of length)
                    return false;
                }
            }
            else
                return true;//if not canadian return true.  there is no check for other types.
        }
        private WorkLinksPersonAddress[] GetPersonAddresses(WorkLinksEmployeeOnboarding[] employees)
        {
            WorkLinksPersonAddressCollection personAddresses = new WorkLinksPersonAddressCollection();
            foreach (WorkLinksEmployeeOnboarding employee in employees)
            {
                personAddresses.Add(employee.PersonAddress);
            }

            return personAddresses.ToArray();
        }
        private WorkLinksEmployeeBanking[] GetEmployeeBanking(WorkLinksEmployeeOnboarding[] employees)
        {
            WorkLinksEmployeeBankingCollection accounts = new WorkLinksEmployeeBankingCollection();
            long sequence = -1;
            foreach (WorkLinksEmployeeOnboarding employee in employees)
            {
                accounts.Add(employee.GetWorkLinksEmployeeBanks(ref sequence));
            }

            return accounts.ToArray();
        }
        private WorkLinksEmployeeEmploymentInformation[] GetEmploymentInformation(WorkLinksEmployeeOnboarding[] employees)
        {
            WorkLinksEmployeeEmploymentInformationCollection employments = new WorkLinksEmployeeEmploymentInformationCollection();
            foreach (WorkLinksEmployeeOnboarding employee in employees)
            {
                employments.Add(employee.EmployeeEmploymentInformation);
            }

            return employments.ToArray();
        }
        private WorkLinksEmployeePosition[] GetEmployeePosition(WorkLinksEmployeeOnboarding[] employees)
        {
            WorkLinksEmployeePositionCollection positions = new WorkLinksEmployeePositionCollection();
            foreach (WorkLinksEmployeeOnboarding employee in employees)
            {
                positions.Add(employee.EmployeePosition);
            }

            return positions.ToArray();
        }
        private WorklinksStatutoryDeduction[] GetStatutoryDeduction(WorkLinksEmployeeOnboarding[] employees)
        {
            WorklinksStatutoryDeductionCollection stats = new WorklinksStatutoryDeductionCollection();

            int i = -1;
            foreach (WorkLinksEmployeeOnboarding employee in employees)
            {
                employee.StatutoryDeduction.StatutoryDeductionId = i--;
                stats.Add(employee.StatutoryDeduction);
            }

            return stats.ToArray();
        }
        public void ProcessEmployeeOnboarding(DatabaseUser user, WorkLinksEmployeeOnboarding[] employees, ImportExportLog log, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions)
        {
            Dictionary<String, Employee> employeeCache = GetEmployeeCache(user, employees);
            ProcessEmployee(user, employeeCache, employees, log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);

            //address
            PersonManagement.ProcessAddress(user, employeeCache, GetPersonAddresses(employees), log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);

            //banking
            ProcessBanking(user, employeeCache, GetEmployeeBanking(employees), log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);

            //employment
            ProcessEmployment(user, employeeCache, GetEmploymentInformation(employees), log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);

            //position
            ProcessPosition(user, employeeCache, GetEmployeePosition(employees), log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);

            //statutory deduction
            ProcessStatutoryDeduction(user, employeeCache, GetStatutoryDeduction(employees), log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);
        }
        private void ImportEmployee(DatabaseUser user, WorkLinksEmployeeCollection existingEmployees, WorkLinksEmployeeCollection pendingEmployees, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool autoAddSecondaryPositions)
        {
            //update existing employees
            if (existingEmployees.Count > 0)
                _employeeAccess.UpdateEmployeeBatch(user, existingEmployees);

            //update pending employees or insert new employees
            if (pendingEmployees.Count > 0)
            {
                WizardCacheImportCollection collection = new WizardCacheImportCollection();
                int i = 0;

                foreach (WorkLinksEmployee employee in pendingEmployees)
                {
                    collection.Add(new WizardCacheImport
                    {
                        WizardCacheId = i--,
                        WizardId = 1, //1 = Add Employee Wizard
                        ImportExternalIdentifier = employee.ImportExternalIdentifier,
                        WizardItemId = 1, //1 = EmployeeBiographicalControl
                        Data = employee.EmployeeCollection
                    });
                }

                WizardManagement.ImportWizardCache(user, collection);
                WizardManagement.ValidateAndHireEmployee(user, collection, autoGenerateEmployeeNumber, employeeNumberFormat, autoAddSecondaryPositions);
            }
        }

        #endregion

        #region employee skill
        public EmployeeSkillCollection GetEmployeeSkill(DatabaseUser user, long employeeId)
        {
            try
            {
                EmployeeSkillCollection skillCollection = EmployeeAccess.GetEmployeeSkill(user, employeeId);

                /*foreach (EmployeeSkill skill in skillCollection)
                {
                    if (skill.AttachmentId != null)
                        skill.AttachmentObjectCollection = GetAttachment(user, Convert.ToInt64(skill.AttachmentId));
                }*/

                return skillCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeSkill", ex);
                throw;
            }
        }
        public void UpdateEmployeeSkill(DatabaseUser user, EmployeeSkill skill)
        {
            try
            {
                if (skill.AttachmentObjectCollection != null)
                {
                    if (skill.AttachmentObjectCollection[0].AttachmentId == -1)
                        InsertAttachment(user, skill.AttachmentObjectCollection[0]);
                    else
                        UpdateAttachment(user, skill.AttachmentObjectCollection[0]);

                    skill.AttachmentId = skill.AttachmentObjectCollection[0].AttachmentId;
                }

                EmployeeAccess.UpdateEmployeeSkill(user, skill);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeeSkill", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeeSkill(DatabaseUser user, EmployeeSkill skill)
        {
            try
            {
                if (skill.AttachmentObjectCollection != null && skill.AttachmentObjectCollection.Count > 0)
                    skill.AttachmentId = skill.AttachmentObjectCollection[0].AttachmentId;

                EmployeeAccess.DeleteEmployeeSkill(user, skill);

                if (skill.AttachmentObjectCollection != null && skill.AttachmentObjectCollection.Count > 0)
                    DeleteAttachment(user, skill.AttachmentObjectCollection[0]);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeSkill", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public EmployeeSkill InsertEmployeeSkill(DatabaseUser user, EmployeeSkill skill)
        {
            try
            {
                if (skill.AttachmentObjectCollection != null && skill.AttachmentObjectCollection.Count > 0)
                {
                    InsertAttachment(user, skill.AttachmentObjectCollection[0]);
                    skill.AttachmentId = skill.AttachmentObjectCollection[0].AttachmentId;
                }
                else
                    skill.AttachmentId = null;

                return EmployeeAccess.InsertEmployeeSkill(user, skill);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeSkill", ex);
                throw;
            }
        }
        #endregion

        #region employee course
        public EmployeeCourseCollection GetEmployeeCourse(DatabaseUser user, long employeeId)
        {
            try
            {
                EmployeeCourseCollection courseCollection = EmployeeAccess.GetEmployeeCourse(user, employeeId);

                /*foreach (EmployeeCourse course in courseCollection)
                {
                    if (course.AttachmentId != null)
                        course.AttachmentObjectCollection = GetAttachment(user, Convert.ToInt64(course.AttachmentId));
                }*/

                return courseCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeCourse", ex);
                throw;
            }
        }
        public void UpdateEmployeeCourse(DatabaseUser user, EmployeeCourse course)
        {
            try
            {
                if (course.AttachmentObjectCollection != null)
                {
                    if (course.AttachmentObjectCollection[0].AttachmentId == -1)
                        InsertAttachment(user, course.AttachmentObjectCollection[0]);
                    else
                        UpdateAttachment(user, course.AttachmentObjectCollection[0]);

                    course.AttachmentId = course.AttachmentObjectCollection[0].AttachmentId;
                }

                EmployeeAccess.UpdateEmployeeCourse(user, course);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeeCourse", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeeCourse(DatabaseUser user, EmployeeCourse course)
        {
            try
            {
                if (course.AttachmentObjectCollection != null && course.AttachmentObjectCollection.Count > 0)
                    course.AttachmentId = course.AttachmentObjectCollection[0].AttachmentId;

                EmployeeAccess.DeleteEmployeeCourse(user, course);

                if (course.AttachmentObjectCollection != null && course.AttachmentObjectCollection.Count > 0)
                    DeleteAttachment(user, course.AttachmentObjectCollection[0]);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeCourse", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public EmployeeCourse InsertEmployeeCourse(DatabaseUser user, EmployeeCourse course)
        {
            try
            {
                if (course.AttachmentObjectCollection != null && course.AttachmentObjectCollection.Count > 0)
                {
                    InsertAttachment(user, course.AttachmentObjectCollection[0]);
                    course.AttachmentId = course.AttachmentObjectCollection[0].AttachmentId;
                }
                else
                    course.AttachmentId = null;

                return EmployeeAccess.InsertEmployeeCourse(user, course);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeCourse", ex);
                throw;
            }
        }
        #endregion

        #region employee membership
        public EmployeeMembershipCollection GetEmployeeMembership(DatabaseUser user, long employeeId)
        {
            try
            {
                return EmployeeAccess.GetEmployeeMembership(user, employeeId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeMembership", ex);
                throw;
            }
        }
        public EmployeeMembership InsertEmployeeMembership(DatabaseUser user, EmployeeMembership membership)
        {
            try
            {
                return EmployeeAccess.InsertEmployeeMembership(user, membership);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeMembership", ex);
                throw;
            }
        }
        public void UpdateEmployeeMembership(DatabaseUser user, EmployeeMembership membership)
        {
            try
            {
                EmployeeAccess.UpdateEmployeeMembership(user, membership);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeeMembership", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeeMembership(DatabaseUser user, EmployeeMembership membership)
        {
            try
            {
                EmployeeAccess.DeleteEmployeeMembership(user, membership);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeMembership", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region employee banking
        public EmployeeBankingCollection GetEmployeeBanking(DatabaseUser user, long employeeId)
        {
            try
            {
                return EmployeeAccess.GetEmployeeBanking(user, employeeId, false);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeBanking-NoOverrideArg", ex);
                throw;
            }
        }
        public EmployeeBankingCollection GetEmployeeBanking(DatabaseUser user, long employeeId, bool securityOverrideFlag)
        {
            try
            {
                return EmployeeAccess.GetEmployeeBanking(user, employeeId, securityOverrideFlag);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeBanking-OverrideArg", ex);
                throw;
            }
        }
        public void UpdateEmployeeBanking(DatabaseUser user, EmployeeBanking bank)
        {
            try
            {
                EmployeeAccess.UpdateEmployeeBanking(user, bank);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeeBanking", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeeBanking(DatabaseUser user, EmployeeBanking bank)
        {
            try
            {
                EmployeeAccess.DeleteEmployeeBanking(user, bank);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeBanking", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public EmployeeBanking InsertEmployeeBanking(DatabaseUser user, EmployeeBanking bank)
        {
            try
            {
                bank.CodeEmployeeBankingRestrictionCode = "2";
                return EmployeeAccess.InsertEmployeeBanking(user, bank);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeBanking", ex);
                throw;
            }
        }
        #endregion

        #region banking import
        private Dictionary<String, WizardCache> BankingGetWizardCache(DatabaseUser user, WorkLinksEmployeeBanking[] xsdObject)
        {
            List<String> importExternalIdentifiers = new List<String>();

            foreach (WorkLinksEmployeeBanking bank in xsdObject)
                importExternalIdentifiers.Add(bank.ImportExternalIdentifier);

            return WizardManagement.GetWizardCache(user, importExternalIdentifiers);
        }
        private void BankingDecodeCode(DatabaseUser user, WorkLinksEmployeeBanking[] accounts)
        {
            string mismatchMessage = "Cannot match code:{0} type:{1} in WorkLinks";
            const string hackCanada = "CA";
            const string hackUS = "US";
            const string hackJM = "JM";

            //get codes
            CodeCollection employeeBankingSequenceNumberTypeCodes = CodeManagement.GetCodeTable(user, CodeTableType.EmployeeBankingSequenceNumberTypeCode);
            CodeCollection employeeBankCodes = CodeManagement.GetEmployeeBankCode(user, hackCanada);
            CodeCollection employeeBankCodesUS = CodeManagement.GetEmployeeBankCode(user, hackUS);
            CodeCollection employeeBankCodesJM = CodeManagement.GetEmployeeBankCode(user, hackJM);
            CodeCollection employeeBankingAccountTypeCodes = CodeManagement.GetCodeTable(user, CodeTableType.CodeBankAccountTypeCode);

            foreach (WorkLinksEmployeeBanking account in accounts)
            {
                //imports support canada only for now (removed former code since it wasn't properly looking up codes *****HACK*****)
                account.BankingCountryCode = (account.BankingCountryCodeImportExternalIdentifier == null) ? hackCanada : account.BankingCountryCodeImportExternalIdentifier;
                //hard code hack
                account.CodeEmployeeBankingRestrictionCode = "2";

                //EmployeeBankingSequenceNumberTypeCode
                if (!(account.SequenceImportExternalIdentifier == null) && !account.SequenceImportExternalIdentifier.Equals(""))
                {
                    account.CodeEmployeeBankingSequenceCode = employeeBankingSequenceNumberTypeCodes.DecodeCode(account.SequenceImportExternalIdentifier);
                    if (account.CodeEmployeeBankingSequenceCode == null)
                        account.SetDecodeCodeValidationMessage(String.Format(mismatchMessage, account.SequenceImportExternalIdentifier, CodeTableType.EmployeeBankingSequenceNumberTypeCode));
                }

                //employeeBankCode
                if (!(account.BankingCodeImportExternalIdentifier == null) && !account.BankingCodeImportExternalIdentifier.Equals(""))
                {
                    if (account.BankingCountryCode == "CA")
                        account.EmployeeBankingCode = employeeBankCodes.DecodeCode(account.BankingCodeImportExternalIdentifier);
                    //try JM account
                    else if (account.BankingCountryCode == "JM")
                        account.EmployeeBankingCode = employeeBankCodesJM.DecodeCode(account.BankingCodeImportExternalIdentifier);
                    //try US account
                    else
                        account.EmployeeBankingCode = employeeBankCodesUS.DecodeCode(account.BankingCodeImportExternalIdentifier);

                    if (account.EmployeeBankingCode == null)
                        account.SetDecodeCodeValidationMessage(String.Format(mismatchMessage, account.BankingCodeImportExternalIdentifier, CodeTableType.EmployeeBankingTypeCode));
                }

                //employeeBankingAccountTypeCodes
                if (!(account.BankingAccountTypeCodeImportExternalIdentifier == null) && !account.BankingAccountTypeCodeImportExternalIdentifier.Equals(""))
                {
                    account.CodeBankAccountTypeCode = employeeBankingAccountTypeCodes.DecodeCode(account.BankingAccountTypeCodeImportExternalIdentifier);
                    if (account.CodeBankAccountTypeCode == null)
                        account.SetDecodeCodeValidationMessage(String.Format(mismatchMessage, account.BankingAccountTypeCodeImportExternalIdentifier, CodeTableType.CodeBankAccountTypeCode));
                }
            }
        }
        public void ProcessBanking(DatabaseUser user, WorkLinksEmployeeBanking[] xsdObject, ImportExportLog log, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions)
        {
            Dictionary<String, Employee> employeeCache = GetEmployeeCache(user, xsdObject);
            ProcessBanking(user, employeeCache, xsdObject, log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);
        }
        public void ProcessBanking(DatabaseUser user, Dictionary<String, Employee> employeeCache, WorkLinksEmployeeBanking[] accounts, ImportExportLog log, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions)
        {
            try
            {
                StringBuilder output = new StringBuilder();
                int failedRows = 0;
                int successfulRows = 0;

                Dictionary<String, WizardCache> wizardCache = BankingGetWizardCache(user, accounts);

                using (TransactionScope scope = new TransactionScope())
                {
                    int i = -1;

                    WorkLinksEmployeeBankingCollection existingEmployeeBanking = new WorkLinksEmployeeBankingCollection();
                    WorkLinksEmployeeBankingCollection pendingEmployeeBanking = new WorkLinksEmployeeBankingCollection();

                    //translate codes
                    BankingDecodeCode(user, accounts);

                    foreach (WorkLinksEmployeeBanking banking in accounts)
                    {
                        if (banking.MeetsBankingRequirements())
                        {
                            if (employeeCache.ContainsKey(banking.ImportExternalIdentifier)) //employee was found in the database
                            {
                                successfulRows++;

                                banking.EmployeeBankingId = i--;
                                banking.EmployeeId = employeeCache[banking.ImportExternalIdentifier].EmployeeId;
                                existingEmployeeBanking.Add(banking);
                            }
                            else
                            {
                                if (wizardCache.ContainsKey(banking.ImportExternalIdentifier)) //employee is pending
                                {
                                    successfulRows++;

                                    banking.EmployeeBankingId = i--;
                                    pendingEmployeeBanking.Add(banking);
                                }
                                else //employee is missing
                                {
                                    i--;
                                    failedRows++;
                                    output.Append(String.Format(GuiManagement.GetResourceByLanguageAndKey(user, "ErrorMessages", "MissingEmployee"), banking.ImportExternalIdentifier) + "\r\n");
                                }
                            }
                        }
                        else
                        {
                            failedRows++;
                            output.Append(banking.GetValidationMessage());
                        }
                    }

                    ImportBanking(user, existingEmployeeBanking, pendingEmployeeBanking, autoGenerateEmployeeNumber, employeeNumberFormat, autoAddSecondaryPositions);

                    if (importDirtySave || failedRows == 0)
                        scope.Complete();
                }

                output.AppendLine("----------");
                output.AppendLine("# rows total: " + (successfulRows + failedRows));
                output.AppendLine("# rows succeeded: " + successfulRows);
                output.AppendLine("# rows failed: " + failedRows);

                if (failedRows > 0)
                {
                    if (importDirtySave)
                        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n" + "----------\r\n");
                    else
                        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportCompleteProcessFailed") + "\r\n" + "----------\r\n");
                }
                else
                    output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n");

                log.SuccessFlag = true;
                log.ProcessingOutput += output.ToString();
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - ProcessBanking", ex);
                throw;
            }
        }
        private void ImportBanking(DatabaseUser user, WorkLinksEmployeeBankingCollection existingEmployeeBanking, WorkLinksEmployeeBankingCollection pendingEmployeeBanking, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool autoAddSecondaryPositions)
        {
            //insert, update or delete existing bank accounts
            if (existingEmployeeBanking.Count > 0)
                EmployeeAccess.ImportBanking(user, existingEmployeeBanking);

            //update pending employee bank accounts or insert new bank accounts
            if (pendingEmployeeBanking.Count > 0)
            {
                WizardCacheImportCollection collection = new WizardCacheImportCollection();
                int i = 0;

                foreach (WorkLinksEmployeeBanking banking in pendingEmployeeBanking)
                {
                    if (!banking.DeleteFlag)
                    {
                        collection.Add(new WizardCacheImport
                        {
                            WizardCacheId = i--,
                            WizardId = 1, //1 = Add Employee Wizard
                            ImportExternalIdentifier = banking.ImportExternalIdentifier,
                            WizardItemId = 8, //8 = EmployeeBankingInformationControl
                            Data = banking.EmployeeBankingCollection
                        });
                    }
                }

                WizardManagement.ImportWizardCache(user, collection);
                WizardManagement.ValidateAndHireEmployee(user, collection, autoGenerateEmployeeNumber, employeeNumberFormat, autoAddSecondaryPositions);
            }
        }
        #endregion

        #region employee education
        public EmployeeEducationCollection GetEmployeeEducation(DatabaseUser user, long employeeId)
        {
            try
            {
                EmployeeEducationCollection educationCollection = EmployeeAccess.GetEmployeeEducation(user, employeeId);

                /*foreach (EmployeeEducation edu in educationCollection)
                {
                    if (edu.AttachmentId != null)
                        edu.AttachmentObjectCollection = GetAttachment(user, Convert.ToInt64(edu.AttachmentId));
                }*/

                return educationCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeEducation", ex);
                throw;
            }
        }
        public void UpdateEmployeeEducation(DatabaseUser user, EmployeeEducation education)
        {
            try
            {
                if (education.AttachmentObjectCollection != null)
                {
                    if (education.AttachmentObjectCollection[0].AttachmentId == -1)
                        InsertAttachment(user, education.AttachmentObjectCollection[0]);
                    else
                        UpdateAttachment(user, education.AttachmentObjectCollection[0]);

                    education.AttachmentId = education.AttachmentObjectCollection[0].AttachmentId;
                }

                EmployeeAccess.UpdateEmployeeEducation(user, education);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeeEducation", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public EmployeeEducation InsertEmployeeEducation(DatabaseUser user, EmployeeEducation education)
        {
            try
            {
                if (education.AttachmentObjectCollection != null && education.AttachmentObjectCollection.Count > 0)
                {
                    InsertAttachment(user, education.AttachmentObjectCollection[0]);
                    education.AttachmentId = education.AttachmentObjectCollection[0].AttachmentId;
                }
                else
                    education.AttachmentId = null;

                return EmployeeAccess.InsertEmployeeEducation(user, education);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeEducation", ex);
                throw;
            }
        }
        public void DeleteEmployeeEducation(DatabaseUser user, EmployeeEducation education)
        {
            try
            {
                if (education.AttachmentObjectCollection != null && education.AttachmentObjectCollection.Count > 0)
                    education.AttachmentId = education.AttachmentObjectCollection[0].AttachmentId;

                EmployeeAccess.DeleteEmployeeEducation(user, education);

                if (education.AttachmentObjectCollection != null && education.AttachmentObjectCollection.Count > 0)
                    DeleteAttachment(user, education.AttachmentObjectCollection[0]);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeEducation", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region employee company property
        public EmployeeCompanyPropertyCollection GetEmployeeCompanyProperty(DatabaseUser user, long employeeId)
        {
            try
            {
                return EmployeeAccess.GetEmployeeCompanyProperty(user, employeeId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeCompanyProperty", ex);
                throw;
            }
        }
        public void UpdateEmployeeCompanyProperty(DatabaseUser user, EmployeeCompanyProperty property)
        {
            try
            {
                EmployeeAccess.UpdateEmployeeCompanyProperty(user, property);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeeCompanyProperty", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public EmployeeCompanyProperty InsertEmployeeCompanyProperty(DatabaseUser user, EmployeeCompanyProperty property)
        {
            try
            {
                return EmployeeAccess.InsertEmployeeCompanyProperty(user, property);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeCompanyProperty", ex);
                throw;
            }
        }
        public void DeleteEmployeeCompanyProperty(DatabaseUser user, EmployeeCompanyProperty property)
        {
            try
            {
                EmployeeAccess.DeleteEmployeeCompanyProperty(user, property);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeCompanyProperty", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region employee contact
        public ContactCollection GetContact(DatabaseUser user, long employeeId, long? contactId)
        {
            try
            {
                return EmployeeAccess.GetContact(user, employeeId, contactId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetContact", ex);
                throw;
            }
        }
        public void UpdateContact(DatabaseUser user, Contact contact)
        {
            try
            {
                EmployeeAccess.UpdateContact(user, contact);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateContact", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void InsertContact(DatabaseUser user, Contact contact)
        {
            try
            {
                EmployeeAccess.InsertContact(user, contact);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertContact", ex);
                throw;
            }
        }
        public void DeleteContact(DatabaseUser user, Contact contact)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //delete contact type
                    EmployeeAccess.DeleteContactTypeByContactId(user, contact.ContactId);

                    //delete contact
                    EmployeeAccess.DeleteContact(user, contact);

                    //delete addresses associated with person
                    PersonAccess.DeletePersonAddressByPersonId(user, contact.PersonId);

                    //delete the contact channels associated with the person
                    PersonAccess.DeletePersonContactChannelByPersonId(user, contact.PersonId);

                    //call delete person
                    PersonAccess.DeletePerson(user, contact);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteContact", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region employee contact type
        public ContactTypeCollection GetContactType(DatabaseUser user, long employeeId, String contactTypeCode)
        {
            try
            {
                return EmployeeAccess.GetContactType(user, employeeId, contactTypeCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetContactType", ex);
                throw;
            }
        }
        public void UpdateContactType(DatabaseUser user, ContactType contactType)
        {
            try
            {
                EmployeeAccess.UpdateContactType(user, contactType);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateContactType", ex);
                throw;
            }
        }
        public ContactType InsertContactType(DatabaseUser user, ContactType contactType)
        {
            try
            {
                return EmployeeAccess.InsertContactType(user, contactType);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertContactType", ex);
                throw;
            }
        }
        public void DeleteContactType(DatabaseUser user, ContactType contactType)
        {
            try
            {
                EmployeeAccess.DeleteContactType(user, contactType);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteContactType", ex);
                throw;
            }
        }
        #endregion

        #region employee reports
        public EmployeeSummaryCollection GetEmployeeSummary(DatabaseUser user, EmployeeCriteria criteria)
        {
            try
            {
                EmployeeSummaryCollection employees = null;

                if (criteria.IsWizardSearch)
                    employees = GetPendingEmployeeSummary(user, criteria);
                else
                {
                    String positionStatusCodeWhereClause = null;

                    if (criteria.EmployeePositionStatusCode != null && criteria.EmployeePositionStatusCode.Count > 0)
                    {
                        foreach (CodeObject status in criteria.EmployeePositionStatusCode)
                            positionStatusCodeWhereClause += status.Code + ",";

                        positionStatusCodeWhereClause = positionStatusCodeWhereClause.Substring(0, positionStatusCodeWhereClause.Length - 1);
                    }

                    criteria.EmployeePositionStatusCodeWhereClause = positionStatusCodeWhereClause;
                    employees = EmployeeAccess.GetEmployeeSummary(user, criteria);
                }

                return employees;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeSummary", ex);
                throw;
            }
        }
        public EmployeeSummaryCollection GetPendingEmployeeSummary(DatabaseUser user, EmployeeCriteria criteria)
        {
            try
            {
                WizardCacheItemCollection items = WizardAccess.GetWizardCacheItemSummary(user, 1, 1, criteria.EmployeeId, criteria.TemplateFlag, criteria.Description);
                EmployeeSummaryCollection employees = new EmployeeSummaryCollection();

                foreach (WizardCacheItem item in items)
                {
                    Employee employee = ((EmployeeCollection)item.Data)[0];

                    if (StartWithOrNull(criteria.EmployeeNumber, employee.EmployeeNumber) || StartWithOrNull(criteria.FirstName, employee.FirstName) ||
                        StartWithOrNull(criteria.LastName, employee.LastName) || StartWithOrNull(criteria.SocialInsuranceNumber, employee.GovernmentIdentificationNumber1))
                    {
                        EmployeeSummary summary = new EmployeeSummary();
                        summary.RowVersion = employee.RowVersion;
                        summary.CreateDatetime = employee.CreateDatetime;
                        summary.CreateUser = employee.CreateUser;
                        summary.UpdateDatetime = employee.UpdateDatetime;
                        summary.UpdateUser = employee.UpdateUser;
                        summary.EmployeeId = (long)item.WizardCacheId;
                        summary.EmployeeNumber = employee.EmployeeNumber;
                        summary.LastName = employee.LastName;
                        summary.FirstName = employee.FirstName;
                        summary.BirthDate = employee.BirthDate;

                        bool addEmployee = true;
                        WizardCacheItemCollection employmentItems = WizardAccess.GetWizardCacheItemSummary(user, 1, 5, item.WizardCacheId, criteria.TemplateFlag, criteria.Description);
                        WizardCacheCollection cacheItems = WizardAccess.GetWizardCache(user, item.WizardCacheId);

                        if (employmentItems.Count > 0)
                            summary.PayrollProcessGroupCode = ((EmployeeEmploymentInformationCollection)employmentItems[0].Data)[0].PayrollProcessGroupCode;

                        if (cacheItems.Count > 0)
                        {
                            summary.Description = cacheItems[0].Description;
                            summary.CountryCode = cacheItems[0].CountryCode;
                        }

                        if (criteria.EmployeeNumber != null && !criteria.EmployeeNumber.Equals(String.Empty) && !(employee.EmployeeNumber ?? String.Empty).ToLower().StartsWith(criteria.EmployeeNumber.ToLower()))
                            addEmployee = false;

                        if (criteria.FirstName != null && !criteria.FirstName.Equals(String.Empty) && !(employee.FirstName ?? String.Empty).ToLower().StartsWith(criteria.FirstName.ToLower()))
                            addEmployee = false;

                        if (criteria.LastName != null && !criteria.LastName.Equals(String.Empty) && !(employee.LastName ?? String.Empty).ToLower().StartsWith(criteria.LastName.ToLower()))
                            addEmployee = false;

                        if (criteria.SocialInsuranceNumber != null && !criteria.SocialInsuranceNumber.Equals(String.Empty) && !(employee.GovernmentIdentificationNumber1 ?? String.Empty).ToLower().StartsWith(criteria.SocialInsuranceNumber.ToLower()))
                            addEmployee = false;

                        if (criteria.ImportExternalIdentifierNotUsed != null && !criteria.ImportExternalIdentifierNotUsed.Equals(String.Empty) && !(employee.ImportExternalIdentifier ?? String.Empty).ToLower().StartsWith(criteria.ImportExternalIdentifierNotUsed.ToLower()))
                            addEmployee = false;

                        if (criteria.Description != null && !criteria.Description.Equals(String.Empty) && !(summary.Description ?? String.Empty).ToLower().StartsWith(criteria.Description.ToLower()))
                            addEmployee = false;

                        if (addEmployee)
                            employees.Add(summary);
                    }
                }

                return employees;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetPendingEmployeeSummary", ex);
                throw;
            }
        }
        private bool StartWithOrNull(String criteria, String field)
        {
            if (criteria == null || criteria.Equals(String.Empty))
                return true;
            else
                if (field == null)
                return false;
            else
                return criteria.ToLower().StartsWith(field.ToLower());
        }
        #endregion

        #region employee license certificate
        public EmployeeLicenseCertificateCollection GetEmployeeLicenseCertificate(DatabaseUser user, long employeeId)
        {
            try
            {
                EmployeeLicenseCertificateCollection licenseCollection = EmployeeAccess.GetEmployeeLicenseCertificates(user, employeeId);

                /*foreach (EmployeeLicenseCertificate license in licenseCollection)
                {
                    if (license.AttachmentId != null)
                        license.AttachmentObjectCollection = GetAttachment(user, Convert.ToInt64(license.AttachmentId));
                }*/

                return licenseCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeLicenseCertificate", ex);
                throw;
            }
        }
        public void UpdateEmployeeLicenseCertificate(DatabaseUser user, EmployeeLicenseCertificate licenseCertificate)
        {
            try
            {
                if (licenseCertificate.AttachmentObjectCollection != null)
                {
                    if (licenseCertificate.AttachmentObjectCollection[0].AttachmentId == -1)
                        InsertAttachment(user, licenseCertificate.AttachmentObjectCollection[0]);
                    else
                        UpdateAttachment(user, licenseCertificate.AttachmentObjectCollection[0]);

                    licenseCertificate.AttachmentId = licenseCertificate.AttachmentObjectCollection[0].AttachmentId;
                }

                EmployeeAccess.UpdateEmployeeLicenseCertificate(user, licenseCertificate);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeeLicenseCertificate", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeeLicenseCertificate(DatabaseUser user, EmployeeLicenseCertificate licenseCertificate)
        {
            try
            {
                if (licenseCertificate.AttachmentObjectCollection != null && licenseCertificate.AttachmentObjectCollection.Count > 0)
                    licenseCertificate.AttachmentId = licenseCertificate.AttachmentObjectCollection[0].AttachmentId;

                EmployeeAccess.DeleteEmployeeLicenseCertificate(user, licenseCertificate);

                if (licenseCertificate.AttachmentObjectCollection != null && licenseCertificate.AttachmentObjectCollection.Count > 0)
                    DeleteAttachment(user, licenseCertificate.AttachmentObjectCollection[0]);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeLicenseCertificate", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public EmployeeLicenseCertificate InsertEmployeeLicenseCertificate(DatabaseUser user, EmployeeLicenseCertificate licenseCertificate)
        {
            try
            {
                if (licenseCertificate.AttachmentObjectCollection != null && licenseCertificate.AttachmentObjectCollection.Count > 0)
                {
                    InsertAttachment(user, licenseCertificate.AttachmentObjectCollection[0]);
                    licenseCertificate.AttachmentId = licenseCertificate.AttachmentObjectCollection[0].AttachmentId;
                }
                else
                    licenseCertificate.AttachmentId = null;

                return EmployeeAccess.InsertEmployeeLicenseCertificate(user, licenseCertificate);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeLicenseCertificate", ex);
                throw;
            }
        }
        #endregion

        #region employee paycode
        public bool CheckRecurringIncomeCode(DatabaseUser user)
        {
            try
            {
                return EmployeeAccess.CheckRecurringIncomeCode(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - CheckRecurringIncomeCode", ex);
                throw;
            }
        }
        public EmployeePaycodeCollection GetEmployeePaycode(DatabaseUser user, long employeeId)
        {
            try
            {
                EmployeePaycodeCollection employeePaycodeCollection = EmployeeAccess.GetEmployeePaycode(user, employeeId, true, true);

                foreach (EmployeePaycode paycode in employeePaycodeCollection)
                    paycode.RemittanceStubCollection = GetEmployeeRemittanceStub(user, paycode.EmployeePaycodeId);

                return employeePaycodeCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeePaycode4args", ex);
                throw;
            }
        }
        public EmployeePaycodeProcessedCollection GetEmployeePaycodeProcessed(DatabaseUser user, long payrollProcessId, long employeeId)
        {
            try
            {
                return EmployeeAccess.SelectEmployeePaycodeProcessed(user, payrollProcessId, employeeId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeePaycodeProcessed", ex);
                throw;
            }
        }
        public EmployeePaycodeCollection GetEmployeePaycode(DatabaseUser user, long employeeId, bool securityOverrideFlag, bool useGlobalPaycode)
        {
            try
            {
                EmployeePaycodeCollection employeePaycodeCollection = EmployeeAccess.GetEmployeePaycode(user, employeeId, securityOverrideFlag, useGlobalPaycode);

                foreach (EmployeePaycode paycode in employeePaycodeCollection)
                    paycode.RemittanceStubCollection = GetEmployeeRemittanceStub(user, paycode.EmployeePaycodeId);

                return employeePaycodeCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeePaycode5args", ex);
                throw;
            }
        }
        public EmployeePaycode InsertEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    EmployeePaycode insertedPaycode = EmployeeAccess.InsertEmployeePaycode(user, employeePaycode);

                    if (employeePaycode.RemittanceStubCollection != null && employeePaycode.RemittanceStubCollection.Count > 0)
                    {
                        employeePaycode.RemittanceStubCollection[0].EmployeePaycodeId = insertedPaycode.EmployeePaycodeId;
                        InsertEmployeeRemittanceStub(user, employeePaycode.RemittanceStubCollection[0]);
                    }

                    scope.Complete();

                    return insertedPaycode;
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeePaycode", ex);
                throw;
            }
        }
        public void UpdateEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    EmployeeAccess.UpdateEmployeePaycode(user, employeePaycode);

                    //delete existing employee remittance stub
                    DeleteEmployeeRemittanceStub(user, new EmployeeRemittanceStub() { EmployeePaycodeId = employeePaycode.EmployeePaycodeId });

                    //insert new employee remittance stub
                    if (employeePaycode.RemittanceStubCollection != null && employeePaycode.RemittanceStubCollection.Count > 0)
                    {
                        employeePaycode.RemittanceStubCollection[0].EmployeePaycodeId = employeePaycode.EmployeePaycodeId;
                        InsertEmployeeRemittanceStub(user, employeePaycode.RemittanceStubCollection[0]);
                    }

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeePaycode", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //delete employee remittance stub
                    DeleteEmployeeRemittanceStub(user, new EmployeeRemittanceStub() { EmployeePaycodeId = employeePaycode.EmployeePaycodeId });

                    //delete employee paycode
                    EmployeeAccess.DeleteEmployeePaycode(user, employeePaycode);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeePaycode", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void ProcessEmployeePaycodeImport(DatabaseUser user, EmployeePaycodeImportCollection employeePayCodes, ImportExportLog log)
        {
            try
            {
                StringBuilder output = new StringBuilder();
                int failedRows = 0;
                int successfulRows = 0;

                //sync employeeId's from external ones
                Dictionary<String, EmployeeSummary> employeeCache = new Dictionary<String, EmployeeSummary>();
                Dictionary<String, CodePaycode> paycodeCache = new Dictionary<String, CodePaycode>();

                foreach (EmployeePaycodeImport paycode in employeePayCodes)
                {
                    output.Append(SetEmployeeIdFromIdentifier(user, paycode, employeeCache));
                    output.Append(SetPaycodeCodeFromIdentifier(user, paycode, paycodeCache));
                    paycode.UpdateUser = user.UserName;
                    paycode.UpdateDatetime = DateTime.Now;
                }

                //validate some records can process
                if (employeePayCodes.HasAllErrors)
                {
                    output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportValidationFail") + "\r\n" + "----------\r\n");
                    log.ProcessingOutput += output.ToString();
                    log.SuccessFlag = false;
                }
                else
                {
                    //process all by employee by employee number
                    IEnumerable<EmployeePaycodeImport> query =
                        from employeePaycode in employeePayCodes
                        orderby employeePaycode.EmployeeId, employeePaycode.PaycodeCode
                        select employeePaycode;

                    long lastEmployeeId = -1;
                    String lastPaycodeCode = "";
                    EmployeePaycodeCollection currentEmployeePaycodes = null;

                    foreach (EmployeePaycodeImport import in query)
                    {
                        if (import.EmployeeId > 0 && import.PaycodeCode != null)
                        {
                            if (ValidatePaycodeEntryData(user, import)) //validate paycode data
                            {
                                successfulRows++;

                                if (import.EmployeeId != lastEmployeeId)
                                {
                                    lastEmployeeId = import.EmployeeId;
                                    lastPaycodeCode = null;
                                    currentEmployeePaycodes = GetEmployeePaycode(user, import.EmployeeId, true, false); //get current employee paycode list
                                }
                                if (currentEmployeePaycodes[import.PaycodeCode] == null)
                                {
                                    if (lastPaycodeCode != import.PaycodeCode)
                                        InsertEmployeePaycode(user, import);
                                    else //report dupe
                                        output.AppendLine(String.Format(GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "DuplicatePaycodeForEmployee"), import.ImportExternalIdentifier, import.PaycodeCode) + "\r\n");
                                }
                                else
                                {
                                    if (lastPaycodeCode != import.PaycodeCode)
                                        UpdateEmployeePaycode(user, import);
                                    else //report dupe
                                        output.AppendLine(String.Format(GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "DuplicatePaycodeForEmployee"), import.ImportExternalIdentifier, import.PaycodeCode) + "\r\n");
                                }
                            }
                            else
                            {
                                failedRows++;

                                output.AppendLine(String.Format(GuiManagement.GetResourceByLanguageAndKey(user, "ErrorMessages", "ImportPaycodeDataValidate"), import.ImportExternalIdentifier, import.PaycodeCode) + "\r\n");
                            }

                            lastPaycodeCode = import.PaycodeCode;
                        }
                        else
                            failedRows++;
                    }

                    output.AppendLine("----------");
                    output.AppendLine("# rows total: " + (successfulRows + failedRows));
                    output.AppendLine("# rows succeeded: " + successfulRows);
                    output.AppendLine("# rows failed: " + failedRows);

                    if (failedRows > 0)
                        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n" + "----------\r\n");
                    else
                        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n");

                    log.SuccessFlag = true;
                    log.WarningFlag = employeePayCodes.HasErrors;
                    log.ProcessingOutput += output.ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - ProcessEmployeePaycodeImport", ex);
                throw;
            }
        }
        public void ProcessEmployeePaycodeImport(DatabaseUser user, EmployeePaycodeCollection paycodes, ImportExportLog log, String employeeImportExternalIdenfier)
        {
            EmployeePaycodeImportCollection employeePayCodes = new EmployeePaycodeImportCollection(paycodes, employeeImportExternalIdenfier);
            ProcessEmployeePaycodeImport(user, employeePayCodes, log);
        }
        public void ProcessEmployeePaycodeImport(DatabaseUser user, WorkLinksEmployeePaycode xsdObject, ImportExportLog log)
        {
            EmployeePaycodeImportCollection employeePayCodes = new EmployeePaycodeImportCollection(xsdObject);
            ProcessEmployeePaycodeImport(user, employeePayCodes, log);
        }
        public bool ValidatePaycodeEntryData(DatabaseUser user, EmployeePaycode data)
        {
            bool valid = true;

            valid = (data.AmountRate == null || (data.AmountRate != null && data.AmountRate <= (Decimal)999999999.9999));
            valid = valid && (data.AmountUnits == null || (data.AmountUnits != null && data.AmountUnits <= (Decimal)99999999.99));
            valid = valid && (data.PayPeriodMinimum == null || (data.PayPeriodMinimum != null && data.PayPeriodMinimum >= 0 && data.PayPeriodMinimum <= (Decimal)999999999.99));
            valid = valid && (data.AmountPercentage == null || (data.AmountPercentage != null && data.AmountPercentage >= 0 && data.AmountPercentage <= (Decimal)9999.9999));
            valid = valid && (data.PayPeriodMaximum == null || (data.PayPeriodMaximum != null && data.PayPeriodMaximum >= 0 && data.PayPeriodMaximum <= (Decimal)999999999.99));
            valid = valid && (data.ExcludeAmount == null || (data.ExcludeAmount != null && data.ExcludeAmount >= 0 && data.ExcludeAmount <= (Decimal)999999999.99));
            valid = valid && (data.MonthlyMaximum == null || (data.MonthlyMaximum != null && data.MonthlyMaximum >= 0 && data.MonthlyMaximum <= (Decimal)999999999.99));
            valid = valid && (data.ExcludePercentage == null || (data.ExcludePercentage != null && data.ExcludePercentage >= 0 && data.ExcludePercentage <= (Decimal)9999.9999));
            valid = valid && (data.YearlyMaximum == null || (data.YearlyMaximum != null && data.YearlyMaximum >= 0 && data.YearlyMaximum <= (Decimal)999999999.99));
            valid = valid && (data.GroupIncomeFactor == null || (data.GroupIncomeFactor != null && data.GroupIncomeFactor >= 0 && data.GroupIncomeFactor <= (Decimal)99999.9999));
            valid = valid && (data.RoundUpTo == null || (data.RoundUpTo != null && data.RoundUpTo >= 0 && data.RoundUpTo <= 99999999));
            valid = valid && (data.RatePerRoundUpTo == null || (data.RatePerRoundUpTo != null && data.RatePerRoundUpTo >= 0 && data.RatePerRoundUpTo <= (Decimal)999999999.9999));
            valid = valid && (data.EmploymentInsuranceInsurableHoursPerUnit == null || (data.EmploymentInsuranceInsurableHoursPerUnit != null && data.EmploymentInsuranceInsurableHoursPerUnit >= 0 && data.EmploymentInsuranceInsurableHoursPerUnit <= (Decimal)9999.99));

            return valid;
        }
        private void DeleteEmployeePaycodes(DatabaseUser user, long employeeId)
        {
            foreach (EmployeePaycode paycode in _employeeAccess.GetEmployeePaycode(user, employeeId, true, false))
                _employeeAccess.DeleteEmployeePaycode(user, paycode);
        }
        public String SetEmployeeIdFromIdentifier(DatabaseUser user, IEmployeeIdentifier paycode, Dictionary<String, EmployeeSummary> employeeCache)
        {
            try
            {
                String output = null;

                if (paycode.ImportExternalIdentifier == "" || paycode.ImportExternalIdentifier == null)
                    output = String.Format(GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportMissingEmployeeNumber") + "\r\n");
                else
                {
                    if (employeeCache.ContainsKey(paycode.ImportExternalIdentifier))
                    {
                        if (employeeCache[paycode.ImportExternalIdentifier] != null)
                        {
                            paycode.EmployeeId = employeeCache[paycode.ImportExternalIdentifier].EmployeeId;
                            paycode.PayrollProcessGroupCode = employeeCache[paycode.ImportExternalIdentifier].PayrollProcessGroupCode;
                        }
                        else
                            paycode.HasErrors = true;
                    }
                    else
                    {
                        EmployeeSummaryCollection employees = EmployeeAccess.GetEmployeeSummary(user, new EmployeeCriteria() { EmployeeNumber = paycode.ImportExternalIdentifier, SecurityOverrideFlag = true, UseLikeStatementInProcSelect = false });

                        if (employees.Count == 1)
                        {
                            paycode.EmployeeId = employees[0].EmployeeId;
                            paycode.PayrollProcessGroupCode = employees[0].PayrollProcessGroupCode;
                            employeeCache.Add(paycode.ImportExternalIdentifier, employees[0]);
                        }
                        else if (employees.Count == 0)
                        {
                            output = String.Format(GuiManagement.GetResourceByLanguageAndKey(user, "ErrorMessages", "MissingEmployee"), paycode.ImportExternalIdentifier) + "\r\n";
                            paycode.HasErrors = true;
                            employeeCache.Add(paycode.ImportExternalIdentifier, null);
                        }
                        else
                            throw new Exception(String.Format("multiple employees selected for employeeNumber {0}", paycode.ImportExternalIdentifier));
                    }
                }

                return output;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - SetEmployeeIdFromIdentifier", ex);
                throw ex;
            }
        }

        public string GetPaycodeCodeFromIdentifier(DatabaseUser user, string paycodeIdentifier, Dictionary<string, string> paycodeCache)
        {
            string output = null;

            if (!paycodeCache.ContainsKey(paycodeIdentifier))
            {
                CodePaycodeCollection paycodes = CodeAccess.GetCodePaycode(user, paycodeIdentifier, true);

                if (paycodes.Count == 1)
                {
                    string paycode = paycodes[0].PaycodeCode;
                    paycodeCache.Add(paycodeIdentifier, paycode);
                }
                else if (paycodes.Count == 0)
                {
                    output = string.Format(GuiManagement.GetResourceByLanguageAndKey(user, "ErrorMessages", "MissingPaycode"), paycodeIdentifier) + "\r\n";
                    paycodeCache.Add(paycodeIdentifier, null);
                }
            }

            string cachedPaycode = paycodeCache[paycodeIdentifier];
            return cachedPaycode;
        }

        public String SetPaycodeCodeFromIdentifier(DatabaseUser user, IPaycodeIdentifier paycode, Dictionary<String, CodePaycode> paycodeCache)
        {
            try
            {
                String output = null;

                if (paycodeCache.ContainsKey(paycode.PaycodeCodeImportExternalIdentifier))
                {
                    CodePaycode cachedPaycode = paycodeCache[paycode.PaycodeCodeImportExternalIdentifier];

                    if (cachedPaycode != null)
                    {
                        paycode.PaycodeCode = cachedPaycode.PaycodeCode;
                        paycode.AmountRateFactor = cachedPaycode.AmountRateFactor;
                        paycode.IncludeEmploymentInsuranceHoursFlag = cachedPaycode.IncludeEmploymentInsuranceHoursFlag;
                        paycode.UseSalaryStandardHoursFlag = cachedPaycode.UseSalaryStandardHourFlag;
                        paycode.AutoPopulateRateFlag = cachedPaycode.AutoPopulateRateFlag;
                        paycode.PaycodeTypeCode = cachedPaycode.PaycodeTypeCode;
                    }
                }
                else
                {
                    CodePaycodeCollection paycodes = CodeAccess.GetCodePaycode(user, paycode.PaycodeCodeImportExternalIdentifier, true);

                    if (paycodes.Count == 1)
                    {
                        paycode.PaycodeCode = paycodes[0].PaycodeCode;
                        paycode.AmountRateFactor = paycodes[0].AmountRateFactor;
                        paycode.IncludeEmploymentInsuranceHoursFlag = paycodes[0].IncludeEmploymentInsuranceHoursFlag;
                        paycode.UseSalaryStandardHoursFlag = paycodes[0].UseSalaryStandardHourFlag;
                        paycode.PaycodeTypeCode = paycodes[0].PaycodeTypeCode;
                        paycode.AutoPopulateRateFlag = paycodes[0].AutoPopulateRateFlag;
                        paycodeCache.Add(paycode.PaycodeCodeImportExternalIdentifier, paycodes[0]);
                    }
                    else if (paycodes.Count == 0)
                    {
                        output = String.Format(GuiManagement.GetResourceByLanguageAndKey(user, "ErrorMessages", "MissingPaycode"), paycode.PaycodeCodeImportExternalIdentifier) + "\r\n";
                        paycode.HasErrors = true;
                        paycodeCache.Add(paycode.PaycodeCodeImportExternalIdentifier, null);
                    }
                }

                return output;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - SetPaycodeCodeFromIdentifier", ex);
                throw;
            }
        }
        public String SetOrganizationUnitFromIdentifier(DatabaseUser user, PayrollTransactionImport transaction, Dictionary<String, String> orgUnitCache)
        {
            String message = null;
            StringBuilder output = new StringBuilder();

            //try
            //{
            if (transaction.OrganizationUnitImportExternalIdentifier != null)
            {
                if (orgUnitCache.ContainsKey(String.Format("{0}|{1}", transaction.OrganizationUnitImportExternalIdentifier, transaction.EmployeeImportExternalIdentifier)))
                {
                    transaction.OrganizationUnit = orgUnitCache[String.Format("{0}|{1}", transaction.OrganizationUnitImportExternalIdentifier, transaction.EmployeeImportExternalIdentifier)];
                }
                else
                {

                    String orgUnits = MapEmployeeOrganizationUnit(user, transaction.OrganizationUnitImportExternalIdentifier, output);

                    if (orgUnits != null)
                    {
                        transaction.OrganizationUnit = orgUnits;
                        orgUnitCache.Add(String.Format("{0}|{1}", transaction.OrganizationUnitImportExternalIdentifier, transaction.EmployeeImportExternalIdentifier), orgUnits);
                    }
                    else
                    {
                        transaction.HasErrors = true;
                        output.AppendLine(String.Format("Cost center:{0} cannot be found in WorkLinks.", transaction.OrganizationUnitImportExternalIdentifier));
                    }
                }
            }
            //}
            ///////////VERY INEFFICIENT
            //catch (FaultException<EmployeeServiceException> ex)
            //{
            //    transaction.HasErrors = true;

            //    if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.CodeMismatch)
            //        output.AppendLine(ex.Message);
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            if (output.Length > 0)
                message += output.ToString();

            return message;
        }
        public Decimal CalculateRate(DatabaseUser user, long employeeId, Decimal baseRate, Decimal rateFactor, bool vacationCalculationOverrideFlag)
        {
            //try
            //{
            //calc current rate
            Decimal currentRate = Math.Round(baseRate * rateFactor, 4, MidpointRounding.AwayFromZero);

            //if codepaycode.vacation_calculation_override_flag is true, take the maximum rate from either the last rate for this employee on db table employee_vacation_hourly_rate or the current hourly rate.
            if (vacationCalculationOverrideFlag)
            {
                //get last rate from db table employee_vacation_hourly_rate
                EmployeeVacationHourlyRateCollection empRate = EmployeeAccess.GetEmployeeVacationHourlyRate(user, employeeId);

                if (empRate != null && empRate.Count > 0)
                    currentRate = (empRate[0].Rate >= currentRate) ? Math.Round(empRate[0].Rate, 4, MidpointRounding.AwayFromZero) : currentRate;
            }

            return currentRate;
            //}  /////VERY INEFFICIENT
            //catch (Exception ex)
            //{
            //    ErrorManagement.HandleError(user, "EmployeeManagement - CalculateRate", ex);
            //    throw;
            //}
        }
        #endregion

        #region employee award honour
        public EmployeeAwardHonourCollection GetEmployeeAwardHonour(DatabaseUser user, long employeeId)
        {
            try
            {
                EmployeeAwardHonourCollection awardCollection = EmployeeAccess.GetEmployeeAwardHonour(user, employeeId);

                /*foreach (EmployeeAwardHonour award in awardCollection)
                {
                    if (award.AttachmentId != null)
                        award.AttachmentObjectCollection = GetAttachment(user, Convert.ToInt64(award.AttachmentId));
                }*/

                return awardCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeAwardHonour", ex);
                throw;
            }
        }
        public void UpdateEmployeeAwardHonour(DatabaseUser user, EmployeeAwardHonour awardHonour)
        {
            try
            {
                if (awardHonour.AttachmentObjectCollection != null)
                {
                    if (awardHonour.AttachmentObjectCollection[0].AttachmentId == -1)
                        InsertAttachment(user, awardHonour.AttachmentObjectCollection[0]);
                    else
                        UpdateAttachment(user, awardHonour.AttachmentObjectCollection[0]);

                    awardHonour.AttachmentId = awardHonour.AttachmentObjectCollection[0].AttachmentId;
                }

                EmployeeAccess.UpdateEmployeeAwardHonour(user, awardHonour);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeAwardHonour", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeeAwardHonour(DatabaseUser user, EmployeeAwardHonour awardHonour)
        {
            try
            {
                if (awardHonour.AttachmentObjectCollection != null && awardHonour.AttachmentObjectCollection.Count > 0)
                    awardHonour.AttachmentId = awardHonour.AttachmentObjectCollection[0].AttachmentId;

                EmployeeAccess.DeleteEmployeeAwardHonour(user, awardHonour);

                if (awardHonour.AttachmentObjectCollection != null && awardHonour.AttachmentObjectCollection.Count > 0)
                    DeleteAttachment(user, awardHonour.AttachmentObjectCollection[0]);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeAwardHonour", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public EmployeeAwardHonour InsertEmployeeAwardHonour(DatabaseUser user, EmployeeAwardHonour awardHonour)
        {
            try
            {
                if (awardHonour.AttachmentObjectCollection != null && awardHonour.AttachmentObjectCollection.Count > 0)
                {
                    InsertAttachment(user, awardHonour.AttachmentObjectCollection[0]);
                    awardHonour.AttachmentId = awardHonour.AttachmentObjectCollection[0].AttachmentId;
                }
                else
                    awardHonour.AttachmentId = null;

                return EmployeeAccess.InsertEmployeeAwardHonour(user, awardHonour);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeAwardHonour", ex);
                throw;
            }
        }
        #endregion

        #region employee discipline
        public EmployeeDisciplineCollection GetEmployeeDiscipline(DatabaseUser user, long employeeId)
        {
            try
            {
                return EmployeeAccess.GetEmployeeDiscipline(user, employeeId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeDiscipline", ex);
                throw;
            }
        }
        public void UpdateEmployeeDiscipline(DatabaseUser user, EmployeeDiscipline discipline)
        {
            try
            {
                EmployeeAccess.UpdateEmployeeDiscipline(user, discipline);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeeDiscipline", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeeDiscipline(DatabaseUser user, EmployeeDiscipline discipline)
        {
            try
            {
                //Get all the Discipline Actions for this Discipline
                EmployeeDisciplineActionCollection employeeDisciplineActionCollection = GetEmployeeDisciplineAction(user, discipline.EmployeeDisciplineId);

                //Iterate through and delete each Displine Action
                foreach (EmployeeDisciplineAction employeeDisciplineAction in employeeDisciplineActionCollection)
                    DeleteEmployeeDisciplineAction(user, employeeDisciplineAction);

                //Delete The Disipline
                EmployeeAccess.DeleteEmployeeDiscipline(user, discipline);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeDiscipline", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public EmployeeDiscipline InsertEmployeeDiscipline(DatabaseUser user, EmployeeDiscipline discipline)
        {
            try
            {
                return EmployeeAccess.InsertEmployeeDiscipline(user, discipline);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeDiscipline", ex);
                throw;
            }
        }
        #endregion

        #region employee discipline action
        public EmployeeDisciplineActionCollection GetEmployeeDisciplineAction(DatabaseUser user, long employeeDisciplineId)
        {
            try
            {
                return EmployeeAccess.GetEmployeeDisciplineAction(user, employeeDisciplineId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeDisciplineAction", ex);
                throw;
            }
        }
        public void UpdateEmployeeDisciplineAction(DatabaseUser user, EmployeeDisciplineAction disciplineAction)
        {
            try
            {
                EmployeeAccess.UpdateEmployeeDisciplineAction(user, disciplineAction);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeeDisciplineAction", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeeDisciplineAction(DatabaseUser user, EmployeeDisciplineAction disciplineAction)
        {
            try
            {
                EmployeeAccess.DeleteEmployeeDisciplineAction(user, disciplineAction);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeDisciplineAction", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public EmployeeDisciplineAction InsertEmployeeDisciplineAction(DatabaseUser user, EmployeeDisciplineAction disciplineAction)
        {
            try
            {
                return EmployeeAccess.InsertEmployeeDisciplineAction(user, disciplineAction);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeDisciplineAction", ex);
                throw;
            }
        }
        #endregion

        #region employee termination
        //termination other
        public EmployeeTerminationOtherCollection GetEmployeeTerminationOther(DatabaseUser user, long employeePositionId)
        {
            try
            {
                return EmployeeAccess.GetEmployeeTerminationOther(user, employeePositionId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeTerminationOther", ex);
                throw;
            }
        }
        public void UpdateEmployeeTerminationOther(DatabaseUser user, EmployeeTerminationOther item)
        {
            try
            {
                if (item.EmployeeTerminationOtherId < 0)
                    EmployeeAccess.InsertEmployeeTerminationOther(user, item);
                else
                    EmployeeAccess.UpdateEmployeeTerminationOther(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeeTerminationOther", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeeTerminationOther(DatabaseUser user, EmployeeTerminationOther item)
        {
            try
            {
                EmployeeAccess.DeleteEmployeeTerminationOther(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeTerminationOther", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void InsertEmployeeTerminationOther(DatabaseUser user, EmployeeTerminationOther item)
        {
            try
            {
                EmployeeAccess.InsertEmployeeTerminationOther(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeTerminationOther", ex);
                throw;
            }
        }
        //termination roe
        public EmployeeTerminationRoeCollection GetEmployeeTerminationRoe(DatabaseUser user, long employeePositionId)
        {
            try
            {
                return EmployeeAccess.GetEmployeeTerminationRoe(user, employeePositionId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeTerminationRoe", ex);
                throw;
            }
        }
        public void UpdateEmployeeTerminationRoe(DatabaseUser user, EmployeeTerminationRoe item)
        {
            try
            {
                if (item.EmployeeTerminationRoeId < 0)
                    EmployeeAccess.InsertEmployeeTerminationRoe(user, item);
                else
                    EmployeeAccess.UpdateEmployeeTerminationRoe(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeeTerminationRoe", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeeTerminationRoe(DatabaseUser user, EmployeeTerminationRoe item)
        {
            try
            {
                EmployeeAccess.DeleteEmployeeTerminationRoe(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeTerminationRoe", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void InsertEmployeeTerminationRoe(DatabaseUser user, EmployeeTerminationRoe item)
        {
            try
            {
                EmployeeAccess.InsertEmployeeTerminationRoe(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeTerminationRoe", ex);
                throw;
            }
        }
        #endregion

        #region employee position
        //keep in mind WCF strips out collections
        public EmployeePositionSummaryCollection GetEmployeePositionSummary(DatabaseUser user, EmployeePositionCriteria criteria)
        {
            try
            {
                EmployeePositionSummaryCollection employeePositions = EmployeeAccess.GetEmployeePositionSummary(user, criteria);

                foreach (EmployeePosition position in employeePositions)
                {
                    long employeePositionId = position.EmployeePositionId;
                    position.EmployeePositionOrganizationUnits = GetEmployeePositionOrganizationUnitLevelSummary(user, employeePositionId);
                    position.SecondaryPositions = GetEmployeePositionSecondary(user, null, position.EmployeePositionId, user.LanguageCode);

                    if (criteria.IncludeWorkDays)
                        position.Workdays = GetEmployeePositionWorkday(user, employeePositionId);
                }

                return employeePositions;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeePositionSummary", ex);
                throw;
            }
        }

        public EmployeePositionEmployeeIdTransactionDateCollection BatchSelectPositionWorkdayOrgAndSecondary(DatabaseUser user, EmployeePositionEmployeeIdTransactionDateCollection positionTrans, bool securityOverrideFlag)
        {
            try
            {
                //Already have positions objects populated but since we can have duplicate employee positions in EmployeePositionEmployeeIdTransactionDateCollection,
                //make a unique collection to pass to batch select procs
                EmployeePositionCollection collection = new EmployeePositionCollection();

                //add unique positions to collection
                foreach (EmployeePositionEmployeeIdTransactionDate item in positionTrans)
                {
                    if (collection[item.EmployeePositionId.ToString()] == null)
                    {
                        EmployeePosition temp = new EmployeePosition();
                        item.CopyTo(temp);
                        collection.Add(temp);
                    }
                }

                //reuse existing batch select method
                GetBatchEmployeePosition(user, collection, securityOverrideFlag, true);

                //now assign complete positions to the EmployeePositionEmployeeIdTransactionDateCollection we started with
                foreach (EmployeePositionEmployeeIdTransactionDate item in positionTrans)
                {
                    foreach (EmployeePosition pos in collection)
                    {
                        if (item.EmployeePositionId == pos.EmployeePositionId)
                        {
                            pos.CopyTo(item);
                            break;
                        }
                    }
                }

                return positionTrans;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - BatchSelectPositionWorkdayOrgAndSecondary", ex);
                throw;
            }
        }

        public EmployeePositionCollection GetBatchEmployeePosition(DatabaseUser user, EmployeePositionCollection positionIds, bool securityOverrideFlag, bool positionAlreadyPopulated = false)
        {
            try
            {
                EmployeePositionCollection collection;

                //get positions
                if (!positionAlreadyPopulated)
                    collection = EmployeeAccess.GetBatchEmployeePosition(user, positionIds, securityOverrideFlag);
                else
                    collection = positionIds;

                //get workdays
                EmployeePositionWorkdayCollection workCollection = EmployeeAccess.GetBatchEmployeePostionWorkday(user, positionIds);

                //add workdays to position
                foreach (EmployeePosition position in collection)
                {
                    position.Workdays = new EmployeePositionWorkdayCollection();

                    foreach (EmployeePositionWorkday item in workCollection)
                    {
                        if (item.EmployeePositionId == position.EmployeePositionId)
                            position.Workdays.Add(item);
                    }
                }

                //get position org units
                EmployeePositionOrganizationUnitCollection orgUnits = EmployeeAccess.GetBatchEmployeePositionOrganizationUnitLevelSummary(user, positionIds);

                //add org units to position
                foreach (EmployeePosition position in collection)
                {
                    position.EmployeePositionOrganizationUnits = new EmployeePositionOrganizationUnitCollection();

                    foreach (EmployeePositionOrganizationUnit item in orgUnits)
                    {
                        if (item.EmployeePositionId == position.EmployeePositionId)
                            position.EmployeePositionOrganizationUnits.Add(item);
                    }
                }

                //get secondary positions
                EmployeePositionSecondaryCollection secondaryCollection = EmployeeAccess.GetBatchEmployeeSecondaryPosition(user, positionIds, user.LanguageCode);
                //get secondary position org units
                EmployeePositionSecondaryOrganizationUnitCollection secondaryOrgUnitCollection = EmployeeAccess.GetBatchEmployeePositionSecondaryOrganizationUnit(user, secondaryCollection);

                //add secondary org units to secondary positions
                foreach (EmployeePositionSecondary secPosition in secondaryCollection)
                {
                    secPosition.OrganizationUnits = new EmployeePositionSecondaryOrganizationUnitCollection();

                    foreach (EmployeePositionSecondaryOrganizationUnit item in secondaryOrgUnitCollection)
                    {
                        if (item.EmployeePositionSecondaryId == secPosition.EmployeePositionSecondaryId)
                            secPosition.OrganizationUnits.Add(item);
                    }
                }

                //add secondary position to position
                foreach (EmployeePosition position in collection)
                {
                    position.SecondaryPositions = new EmployeePositionSecondaryCollection();

                    foreach (EmployeePositionSecondary item in secondaryCollection)
                    {
                        if (item.EmployeePositionId == position.EmployeePositionId)
                            position.SecondaryPositions.Add(item);
                    }
                }

                return collection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetBatchEmployeePosition with EmployeePositionCollection in arg list", ex);
                throw;
            }
        }

        public EmployeePositionCollection GetBatchEmployeePosition(DatabaseUser user, SalaryEmployeeCollection tempEmployees, bool securityOverrideFlag)
        {
            try
            {
                return EmployeeAccess.GetBatchEmployeePosition(user, tempEmployees, securityOverrideFlag);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetBatchEmployeePosition", ex);
                throw;
            }
        }
        public EmployeePositionCollection GetEmployeePosition(DatabaseUser user, EmployeePositionCriteria criteria)
        {
            try
            {
                EmployeePositionCollection employeePositions = EmployeeAccess.GetEmployeePosition(user, criteria);

                if (criteria.IncludeWorkDays)
                {
                    foreach (EmployeePosition position in employeePositions)
                        position.Workdays = GetEmployeePositionWorkday(user, position.EmployeePositionId);
                }

                if (criteria.IncludeOrganizationUnit)
                    PopulateOrganizationalUnit(user, employeePositions);

                PopulateSecondaryPositions(user, employeePositions);

                return employeePositions;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeePosition", ex);
                throw;
            }
        }

        public WorkLinksEmployeePositionCollection GetBatchLatestEmployeePosition(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            try
            {
                WorkLinksEmployeePositionCollection positions = EmployeeAccess.GetBatchLatestEmployeePosition(user, importExternalIdentifiers);
                EmployeePositionWorkdayCollection workdays = EmployeeAccess.GetEmployeePositionWorkdayBatch(user, importExternalIdentifiers);
                EmployeePositionOrganizationUnitCollection organizationUnits = EmployeeAccess.GetEmployeePositionOrganizationUnitBatch(user, importExternalIdentifiers);
                EmployeePositionSecondaryCollection secondaryPositions = EmployeeAccess.GetEmployeePositionSecondaryBatch(user, importExternalIdentifiers);

                foreach (WorkLinksEmployeePosition position in positions)
                {
                    position.Workdays = new EmployeePositionWorkdayCollection();
                    position.EmployeePositionOrganizationUnits = new EmployeePositionOrganizationUnitCollection();
                    position.SecondaryPositions = new EmployeePositionSecondaryCollection();

                    foreach (EmployeePositionWorkday workday in workdays)
                    {
                        if (position.EmployeePositionId == workday.EmployeePositionId)
                            position.Workdays.Add(workday);
                    }

                    foreach (EmployeePositionOrganizationUnit organizationUnit in organizationUnits)
                    {
                        if (position.EmployeePositionId == organizationUnit.EmployeePositionId)
                            position.EmployeePositionOrganizationUnits.Add(organizationUnit);
                    }

                    foreach (EmployeePositionSecondary secondary in secondaryPositions)
                    {
                        if (position.EmployeePositionId == secondary.EmployeePositionId)
                            position.SecondaryPositions.Add(secondary);

                        EmployeePositionSecondaryOrganizationUnitCollection orgUnits = new EmployeePositionSecondaryOrganizationUnitCollection();
                        foreach (EmployeePositionOrganizationUnit unit in position.EmployeePositionOrganizationUnits)
                        {
                            orgUnits.Add(new EmployeePositionSecondaryOrganizationUnit()
                            {
                                OrganizationUnitLevelId = unit.OrganizationUnitLevelId,
                                OrganizationUnitId = unit.OrganizationUnitId,
                                OrganizationUnitStartDate = unit.OrganizationUnitStartDate,
                                UsedOnSecondaryEmployeePositionFlag = unit.UsedOnSecondaryEmployeePositionFlag
                            });
                        }
                    }
                }

                return positions;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeePositionBatch", ex);
                throw;
            }
        }
        public Dictionary<String, EmployeePosition> GetPositionCache(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            Dictionary<String, EmployeePosition> positionCache = new Dictionary<String, EmployeePosition>();

            //get the existing employee positions from the database and add them to the dictionary
            foreach (WorkLinksEmployeePosition position in GetBatchLatestEmployeePosition(user, importExternalIdentifiers))
                positionCache.Add(position.ImportExternalIdentifier, position);

            return positionCache;
        }
        //populate org units
        private void PopulateOrganizationalUnit(DatabaseUser user, EmployeePositionCollection positions)
        {
            foreach (EmployeePosition position in positions)
                position.EmployeePositionOrganizationUnits = GetEmployeePositionOrganizationUnitLevelSummary(user, position.EmployeePositionId);
        }
        //populate secondary positions
        private void PopulateSecondaryPositions(DatabaseUser user, EmployeePositionCollection positions)
        {
            foreach (EmployeePosition position in positions)
                position.SecondaryPositions = GetEmployeePositionSecondary(user, null, position.EmployeePositionId, user.LanguageCode);
        }
        ////set cutoff date
        //private void PopulateCutoffDate(EmployeePositionCollection positions, EmployeePositionCriteria criteria)
        //{
        //    PayrollPeriodCollection payrollPeriods = PayrollAccess.GetPayrollPeriod(new PayrollPeriodCriteria() { PayrollProcessGroupCode = criteria.PayrollProcessGroupCode, GetMinOpenPeriodFlag = true });
        //    if (payrollPeriods.Count > 0)
        //        positions.PayrollPeriodCutoffDate = payrollPeriods[0].StartDate;
        //    else
        //        positions.PayrollPeriodCutoffDate = DateTime.Now;
        //}
        private EmployeeEmploymentInformation PrepareEmployeeEmploymentInfoObj(DatabaseUser user, EmployeePosition item)
        {
            //this is only called in the case of a ReHire.  We set the Termination Date to be null in this case
            EmployeeEmploymentInformationCollection collection = EmployeeAccess.GetEmployeeEmploymentInformation(user, item.EmployeeId, false);
            collection[0].TerminationDate = null;
            collection[0].RehireDate = item.EffectiveDate;

            return collection[0];
        }
        public void UpdateEmployeePosition(DatabaseUser user, EmployeePosition item, bool autoAddSecondaryPositions)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //if this is a rehire, update the employee_employment_information record to have termination_date = null
                    if (item.EmployeePositionActionCode == "RH" || item.EmployeePositionActionCode == "RHRESET")
                        EmployeeAccess.UpdateEmployeeEmploymentInformation(user, PrepareEmployeeEmploymentInfoObj(user, item));

                    //get the paycode salary map data to be used below
                    PaycodeSalaryMapCollection salaryPaycodes = CodeAccess.GetSalaryPaycodes(user);

                    //use linq to match employee status record to paycode_salary_map
                    var salaryPaycodeForEmployee = (from salary in salaryPaycodes where salary.CodeEmployeePositionStatusCd == item.EmployeePositionStatusCode select salary).SingleOrDefault();

                    //only if there's a match do we enter this code (ex. Terminations will not enter here)
                    if (salaryPaycodeForEmployee != null)
                    {
                        //if EmployeePositionStatusCode mapping on paycode_salary_map.lock_salary_rate_flag = 1
                        if (salaryPaycodeForEmployee.LockSalaryRateFlag)
                        {
                            //need to get former position
                            EmployeePositionCollection formerPosition = EmployeeAccess.GetEmployeePosition(user, new EmployeePositionCriteria() { NextEmployeePositionId = item.EmployeePositionId, EmployeeId = item.EmployeeId });

                            //get former employee_position record's locked_compensation_amount. if it's null, get current records compensation_rate and store it in column employee_position.locked_compensation_amount
                            item.LockedCompensationAmount = (formerPosition[0].LockedCompensationAmount != null) ? formerPosition[0].LockedCompensationAmount : item.CompensationAmount;

                            //get former employee_position record's locked_standard_hours. if it's null, get current records standard_hours and store it in column employee_position.locked_standard_hours
                            item.LockedStandardHours = (formerPosition[0].LockedStandardHours != null) ? formerPosition[0].LockedStandardHours : item.StandardHours;
                        }
                        else //if paycode_salary_map.lock_salary_rate_flag = 0, set locked_compensation_amount = null and locked_standard_hours = null
                        {
                            item.LockedCompensationAmount = null;
                            item.LockedStandardHours = null;
                        }
                    }

                    EmployeeAccess.UpdateEmployeePosition(user, item);

                    if (autoAddSecondaryPositions)
                        AutoAddSecondaryPosition(item);

                    foreach (EmployeePositionOrganizationUnit unit in item.EmployeePositionOrganizationUnits)
                    {
                        //If level exist ...perform update...else perform insert....
                        //Stacy - April 30/2012 - As per John, do not Insert if OrganizationUnitId and OrganizationUnitStartDate are null
                        if (unit.EmployeePositionOrganizationUnitId == -1 && (unit.OrganizationUnitId != null && unit.OrganizationUnitStartDate != null))
                            EmployeeAccess.InsertEmployeePositionOrganizationUnit(user, unit);
                        //Stacy - April 30/2012 - As per John, do not Update if OrganizationUnitId and OrganizationUnitStartDate are null
                        else if (unit.OrganizationUnitId != null && unit.OrganizationUnitStartDate != null)
                            EmployeeAccess.UpdateEmployeePositionOrganizationUnit(user, unit);
                    }

                    foreach (EmployeePositionSecondary secondaryPosition in item.SecondaryPositions)
                    {
                        if (secondaryPosition.EmployeePositionSecondaryId < 0)
                        {
                            secondaryPosition.EmployeePositionId = item.EmployeePositionId;
                            secondaryPosition.UpdateUser = item.UpdateUser;
                            InsertEmployeePositionSecondary(user, secondaryPosition);
                        }
                        else
                        {
                            if (GetJobFromOrganizationUnit(secondaryPosition.OrganizationUnit).Equals(GetJobFromOrganizationUnit(item.OrganizationUnit)) &&
                                (secondaryPosition.SalaryPlanGradeStepId != item.SalaryPlanGradeStepId || secondaryPosition.CompensationAmount != item.RatePerHour))
                            {
                                secondaryPosition.SalaryPlanId = item.SalaryPlanId;
                                secondaryPosition.SalaryPlanGradeId = item.SalaryPlanGradeId;
                                secondaryPosition.SalaryPlanGradeStepId = item.SalaryPlanGradeStepId;
                                secondaryPosition.CompensationAmount = item.RatePerHour;
                                secondaryPosition.UpdateUser = item.UpdateUser;

                                UpdateEmployeePositionSecondary(user, secondaryPosition);
                            }
                        }
                    }

                    foreach (EmployeePositionWorkday workday in item.Workdays)
                    {
                        if (workday.EmployeePositionWorkdayId < 0)
                            InsertEmployeePositionWorkday(user, workday);
                        else
                            UpdateEmployeePositionWorkday(user, workday);
                    }

                    //call the Employee_resetDataAttributes proc
                    EmployeeAccess.ResetDataAttributes(user, item.EmployeeId);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeePosition", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        /// <summary>
        /// This method extracts the job from an organization unit string
        /// </summary>
        /// <param name="organizationUnit"></param>
        private string GetJobFromOrganizationUnit(string organizationUnit)
        {
            int pos = organizationUnit.LastIndexOf("/") + 1;
            return organizationUnit.Substring(pos, organizationUnit.Length - pos);
        }
        public bool IsProcessDateOpen(DatabaseUser user, long? payrollProcessId)
        {
            try
            {
                DateTime? processedDate = EmployeeAccess.IsProcessDateOpen(user, payrollProcessId);
                return processedDate == null;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - IsProcessDateOpen", ex);
                throw;
            }
        }
        public void DeleteEmployeeTermination(DatabaseUser user, EmployeePosition employeePosition)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (EmployeePositionOrganizationUnit unit in employeePosition.EmployeePositionOrganizationUnits)
                    {
                        if (unit.EmployeePositionOrganizationUnitId != -1)
                            EmployeeAccess.DeleteEmployeePositionOrganizationUnit(user, unit);
                    }

                    foreach (EmployeePositionWorkday workday in employeePosition.Workdays)
                    {
                        if (workday.EmployeePositionWorkdayId != -1)
                            EmployeeAccess.DeleteEmployeePositionWorkday(user, workday);
                    }

                    foreach (EmployeePositionSecondary secondary in employeePosition.SecondaryPositions)
                    {
                        if (secondary.EmployeePositionSecondaryId != -1)
                            DeleteEmployeePositionSecondary(user, secondary);
                    }

                    EmployeeAccess.DeleteEmployeeTermination(user, employeePosition.EmployeePositionId);

                    //call the Employee_resetDataAttributes proc
                    EmployeeAccess.ResetDataAttributes(user, employeePosition.EmployeeId);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeTermination", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeePosition(DatabaseUser user, EmployeePosition position, EmployeePosition formerPosition)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //set former position link to current
                    if (formerPosition != null)
                    {
                        formerPosition.NextEmployeePositionId = null;
                        EmployeeAccess.UpdateEmployeePosition(user, formerPosition);
                    }

                    foreach (EmployeePositionOrganizationUnit unit in position.EmployeePositionOrganizationUnits)
                    {
                        if (unit.EmployeePositionOrganizationUnitId != -1)
                            EmployeeAccess.DeleteEmployeePositionOrganizationUnit(user, unit);
                    }

                    foreach (EmployeePositionWorkday workday in position.Workdays)
                    {
                        if (workday.EmployeePositionWorkdayId != -1)
                            DeleteEmployeePositionWorkday(user, workday);
                    }

                    //if position action was RHRESET (rehire/reset their hours) then remove the negative hour entry we added when position was created.
                    if (position.EmployeePositionActionCode == "RHRESET")
                        DeleteSalaryPlanGradeAccumulationByCreateEmployeePositionId(user, position.EmployeePositionId);

                    foreach (EmployeePositionSecondary secondary in position.SecondaryPositions)
                    {
                        if (secondary.EmployeePositionSecondaryId != -1)
                            DeleteEmployeePositionSecondary(user, secondary);
                    }

                    EmployeeAccess.DeleteEmployeePosition(user, position);

                    //call the Employee_resetDataAttributes proc
                    EmployeeAccess.ResetDataAttributes(user, position.EmployeeId);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeePosition", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }

        public void BatchInsertUpdateEmployeePosition(DatabaseUser user, EmployeePositionCollection positions, EmployeePositionCollection formerPositions, DateTime? payrollPeriodCutoffDate, String payrollProcessGroupCode, String roeDefaultContactLastName, String roeDefaultContactFirstName, String roeDefaultContactTelephone, String roeDefaultContactTelephoneExt, bool autoAddSecondaryPositions, long payrollProcessId)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (EmployeePosition empPosition in positions)
                    {
                        //EmployeePosition formerPosition = formerPositions[empPosition.EmployeePositionId.ToString()];
                        EmployeePosition formerPosition = formerPositions.First(x => x.EmployeeId == empPosition.EmployeeId);

                        //if former position is on the same date, advance sequence number
                        if (formerPosition != null && ((DateTime)formerPosition.EffectiveDate).ToShortDateString().Equals(((DateTime)empPosition.EffectiveDate).ToShortDateString()))
                            empPosition.EffectiveSequence = (short)(formerPosition.EffectiveSequence + 1);

                        if (autoAddSecondaryPositions)
                            AutoAddSecondaryPosition(empPosition);

                        foreach (EmployeePositionSecondary secondaryPosition in empPosition.SecondaryPositions)
                        {
                            foreach (EmployeePositionSecondaryOrganizationUnit unit in secondaryPosition.OrganizationUnits)
                            {
                                if (!unit.UsedOnSecondaryEmployeePositionFlag)
                                {
                                    unit.OrganizationUnitId = null;
                                    unit.OrganizationUnitStartDate = null;
                                }
                            }
                        }
                    }

                    //batch update the position data 
                    if (positions.Count > 0)
                        EmployeeAccess.BatchInsertUpdateEmployeePosition(user, positions, payrollProcessId);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - BatchInsertEmployeePosition", ex);
                throw;
            }
        }

        public void InsertEmployeePosition(DatabaseUser user, EmployeePosition position, EmployeePosition formerPosition, DateTime? payrollPeriodCutoffDate, String payrollProcessGroupCode, String roeDefaultContactLastName, String roeDefaultContactFirstName, String roeDefaultContactTelephone, String roeDefaultContactTelephoneExt, bool autoAddSecondaryPositions)
        {
            try
            {
                //if former position is on the same date, advance sequence number
                if (formerPosition != null && ((DateTime)formerPosition.EffectiveDate).ToShortDateString().Equals(((DateTime)position.EffectiveDate).ToShortDateString()))
                    position.EffectiveSequence = (short)(formerPosition.EffectiveSequence + 1);

                //if term related we need to set the roe status
                if (position.IsTermRelated)
                    position.CodeRoeCreationStatusCd = "PEN";

                using (TransactionScope scope = new TransactionScope())
                {
                    //get the paycode salary map data to be used below
                    PaycodeSalaryMapCollection salaryPaycodes = CodeAccess.GetSalaryPaycodes(user);

                    //match employee status record to paycode_salary_map
                    PaycodeSalaryMap salaryPaycodeForEmployee = salaryPaycodes.GetFirstPaycodeSalaryMapByStatusCode(position.EmployeePositionStatusCode);

                    //only if there's a match do we enter this code (ex. Terminations will not enter here)
                    if (salaryPaycodeForEmployee != null)
                    {
                        //if EmployeePositionStatusCode mapping on paycode_salary_map.lock_salary_rate_flag = 1
                        if (salaryPaycodeForEmployee.LockSalaryRateFlag)
                        {
                            //get former employee_position record's locked_compensation_amount. if it's null, get current records compensation_rate and store it in column employee_position.locked_compensation_amount
                            position.LockedCompensationAmount = (formerPosition.LockedCompensationAmount != null) ? formerPosition.LockedCompensationAmount : position.CompensationAmount;

                            //get former employee_position record's locked_standard_hours. if it's null, get current records standard_hours and store it in column employee_position.locked_standard_hours
                            position.LockedStandardHours = (formerPosition.LockedStandardHours != null) ? formerPosition.LockedStandardHours : position.StandardHours;
                        }
                        else //if paycode_salary_map.lock_salary_rate_flag = 0, set locked_compensation_amount = null and locked_standard_hours = null
                        {
                            position.LockedCompensationAmount = null;
                            position.LockedStandardHours = null;
                        }
                    }

                    EmployeeAccess.InsertEmployeePosition(user, position);

                    if (autoAddSecondaryPositions)
                        AutoAddSecondaryPosition(position);

                    //set former position link to current
                    if (formerPosition != null)
                    {
                        formerPosition.NextEmployeePositionId = position.EmployeePositionId;
                        formerPosition.UpdateUser = position.UpdateUser;
                        EmployeeAccess.UpdateEmployeePosition(user, formerPosition);
                    }

                    //for a given employee who has been rehired (RH)- update all former records that are terminations and have pending roe's (PEN) to have an ROE status of not processed (NOTPROC).
                    if (position.EmployeePositionActionCode == "RH" || position.EmployeePositionActionCode == "RHRESET")
                    {
                        EmployeeAccess.UpdatePendingRoesIfNotCompletedAndRehired(user, position);

                        //check if this rehire (RH) is happening in a current/past period
                        if (payrollPeriodCutoffDate != null && payrollPeriodCutoffDate >= position.EffectiveDate)
                        {
                            //check if employee has arrears records
                            EmployeePaycodeArrearsCollection arrearsCollection = PayrollAccess.GetEmployeePaycodeArrears(user, position.EmployeeId, null);

                            if (arrearsCollection.Count > 0)
                            {
                                //insert a batch
                                long tempBatchId = InsertPayrollBatch(user, payrollPeriodCutoffDate, payrollProcessGroupCode);

                                if (tempBatchId > 0)
                                {
                                    //add employee's arrears tranactions
                                    InsertPayrollBatchTrans(user, arrearsCollection, tempBatchId, payrollProcessGroupCode, payrollPeriodCutoffDate);

                                    //update the arrears records to be processed (tag the positionid and transaction id too)
                                    foreach (EmployeePaycodeArrears empArrears in arrearsCollection)
                                    {
                                        empArrears.ProcessingEmployeePositionId = position.EmployeePositionId;
                                        empArrears.CodeEmployeePaycodeArrearsStatusCd = "PROC";
                                        PayrollAccess.UpdateEmployeeArrears(user, empArrears);
                                    }
                                }
                            }
                        }
                    }

                    foreach (EmployeePositionOrganizationUnit unit in position.EmployeePositionOrganizationUnits)
                    {
                        unit.EmployeePositionId = position.EmployeePositionId;
                        unit.UpdateUser = position.UpdateUser;
                        EmployeeAccess.InsertEmployeePositionOrganizationUnit(user, unit);
                    }

                    foreach (EmployeePositionWorkday workday in position.Workdays)
                    {
                        workday.EmployeePositionId = position.EmployeePositionId;
                        workday.UpdateUser = position.UpdateUser;
                        InsertEmployeePositionWorkday(user, workday);
                    }

                    //this will get populated in the foreach below if Rehiring with an hours RESET
                    SalaryPlanGradeAccumulationCollection negateAccumulationHoursCollection = new SalaryPlanGradeAccumulationCollection();

                    foreach (EmployeePositionSecondary secondaryPosition in position.SecondaryPositions)
                    {
                        secondaryPosition.EmployeePositionId = position.EmployeePositionId;
                        secondaryPosition.UpdateUser = position.UpdateUser;

                        if (GetJobFromOrganizationUnit(secondaryPosition.OrganizationUnit).Equals(GetJobFromOrganizationUnit(position.OrganizationUnit)) &&
                            (secondaryPosition.SalaryPlanGradeStepId != position.SalaryPlanGradeStepId || secondaryPosition.CompensationAmount != position.RatePerHour))
                        {
                            secondaryPosition.SalaryPlanId = position.SalaryPlanId;
                            secondaryPosition.SalaryPlanGradeId = position.SalaryPlanGradeId;
                            secondaryPosition.SalaryPlanGradeStepId = position.SalaryPlanGradeStepId;
                            secondaryPosition.CompensationAmount = position.RatePerHour;
                        }

                        //If this is a REHIRE with RESET for hours/step, skip the primary position as it's already taken care of
                        if (position.EmployeePositionActionCode == "RHRESET" && (GetJobFromOrganizationUnit(secondaryPosition.OrganizationUnit) != GetJobFromOrganizationUnit(position.OrganizationUnit)))
                        {
                            //reset secondary position
                            ResetStepAndCompAmount(user, secondaryPosition);
                        }

                        InsertEmployeePositionSecondary(user, secondaryPosition);
                    }

                    //get the accumulation totals and create negative records to insert to reset hours to 0 if rehiring and resetting hours
                    if (position.EmployeePositionActionCode == "RHRESET")
                    {
                        NegateAccumulationHoursTotal(user, position, negateAccumulationHoursCollection);

                        if (negateAccumulationHoursCollection != null && negateAccumulationHoursCollection.Count > 0)
                            PayrollAccess.InsertPayrollTransaction(user, negateAccumulationHoursCollection);
                    }

                    if (position.EmployeePositionActionCode == "TERM" || position.EmployeePositionActionCode == "LEAV" || position.EmployeePositionActionCode == "BENLEAV" || position.EmployeePositionActionCode == "LD0") /* HACK */
                    {
                        //position changed to "termination", therefore insert an ROE record with minimal data
                        EmployeeTerminationRoe tempRoe = CopyPositionDataToRoe(user, position, roeDefaultContactLastName, roeDefaultContactFirstName, roeDefaultContactTelephone, roeDefaultContactTelephoneExt);
                        EmployeeAccess.InsertEmployeeTerminationRoe(user, tempRoe);
                    }

                    //call the Employee_resetDataAttributes proc
                    EmployeeAccess.ResetDataAttributes(user, position.EmployeeId);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeePosition", ex);
                throw;
            }
        }

        private void NegateAccumulationHoursTotal(DatabaseUser user, EmployeePosition position, SalaryPlanGradeAccumulationCollection tempCollection)
        {
            SalaryPlanGradeAccumulationTotalsCollection totals = GetSalaryPlanGradeAccumulationTotals(user, position.EmployeeId);

            if (totals != null && totals.Count > 0)
            {
                foreach (SalaryPlanGradeAccumulationTotals total in totals)
                {
                    SalaryPlanGradeAccumulation temp = new SalaryPlanGradeAccumulation()
                    {
                        EmployeeId = position.EmployeeId,
                        SalaryPlanGradeId = Convert.ToInt64(total.SalaryPlanGradeId),
                        Hours = Convert.ToDecimal(total.TotalHours) * -1,
                        Amount = Convert.ToDecimal(total.TotalAmount) * -1,
                        CreateEmployeePositionId = position.EmployeePositionId,
                        UpdateUser = " - " + user.UserName,
                        //keep keys at -1, -2, etc. so grab last entry from collection and -1 from it.
                        SalaryPlanGradeAccumulationId = (tempCollection.Count > 0) ? (tempCollection[tempCollection.Count - 1].SalaryPlanGradeAccumulationId) - 1 : -1
                    };

                    tempCollection.Add(temp);
                }
            }
        }

        private void ResetStepAndCompAmount(DatabaseUser user, EmployeePositionSecondary secondaryPosition)
        {
            //reset the step
            SalaryPlanGradeStepCollection stepsColl = GetSalaryPlanGradeStep(user, null, secondaryPosition.SalaryPlanGradeId);
            foreach (SalaryPlanGradeStep step in stepsColl)
            {
                if (step.Name == "1")
                {
                    secondaryPosition.SalaryPlanGradeStepId = step.SalaryPlanGradeStepId;
                    break;
                }
            }
            //reset the comp amount
            SalaryPlanGradeStepDetail detail = GetSalaryPlanGradeStepDetail(user, null, secondaryPosition.SalaryPlanGradeStepId)[0];
            if (detail != null)
                secondaryPosition.CompensationAmount = detail.Amount;
        }

        /// <summary>
        /// This method automatically adds a secondary employee position record based on the primary position record.
        /// </summary>
        /// <param name="position"></param>
        private void AutoAddSecondaryPosition(EmployeePosition position)
        {
            if (position.SecondaryPositions == null || position.SecondaryPositions.Count == 0)
                position.SecondaryPositions.Add(CreateNewEmployeePositionSecondary(position));
            else
            {
                if (!position.SecondaryPositions.Any(x => x.OrganizationUnit.EndsWith(position.OrganizationUnit.Substring(position.OrganizationUnit.LastIndexOf('/') + 1))))
                    position.SecondaryPositions.Add(CreateNewEmployeePositionSecondary(position));
            }
        }
        /// <summary>
        /// This method creates a secondary employee position record based on an employee position record.
        /// </summary>
        /// <param name="position"></param>
        private EmployeePositionSecondary CreateNewEmployeePositionSecondary(EmployeePosition position)
        {
            EmployeePositionSecondaryOrganizationUnitCollection orgUnits = new EmployeePositionSecondaryOrganizationUnitCollection();

            foreach (EmployeePositionOrganizationUnit unit in position.EmployeePositionOrganizationUnits)
            {
                orgUnits.Add(new EmployeePositionSecondaryOrganizationUnit()
                {
                    OrganizationUnitLevelId = unit.OrganizationUnitLevelId,
                    OrganizationUnitId = unit.OrganizationUnitId,
                    OrganizationUnitStartDate = unit.OrganizationUnitStartDate,
                    UsedOnSecondaryEmployeePositionFlag = unit.UsedOnSecondaryEmployeePositionFlag
                });
            }

            return new EmployeePositionSecondary()
            {
                EmployeePositionId = position.EmployeePositionId,
                SalaryPlanId = position.SalaryPlanId,
                SalaryPlanGradeId = position.SalaryPlanGradeId,
                SalaryPlanGradeStepId = position.SalaryPlanGradeStepId,
                CompensationAmount = Convert.ToDecimal(position.RatePerHour),
                OrganizationUnits = orgUnits
            };
        }
        private long InsertPayrollBatch(DatabaseUser user, DateTime? cutoffDate, String processGroupCode)
        {
            PayrollPeriodCollection periods = PayrollAccess.GetPayrollPeriod(user, new PayrollPeriodCriteria() { PayrollProcessGroupCode = processGroupCode, GetMinOpenPeriodFlag = true });

            if (periods.Count > 0)
            {
                BusinessObjects.PayrollBatch newBatch = new BusinessObjects.PayrollBatch();
                newBatch.PayrollProcessGroupCode = processGroupCode;
                newBatch.BatchSource = processGroupCode + "-" + cutoffDate;
                newBatch.PayrollBatchStatusCode = "AP";
                newBatch.PayrollProcessRunTypeCode = "REGULAR";
                newBatch.Description = "BENEFITED LEAVE RETURN BATCH";
                newBatch.SystemGeneratedFlag = false;

                return PayrollAccess.InsertPayrollBatch(user, newBatch).PayrollBatchId;
            }
            else
                return -1;
        }
        public void InsertPayrollBatchTrans(DatabaseUser user, EmployeePaycodeArrearsCollection arrears, long payrollBatchId, String payrollProcessGroupCode, DateTime? transactionDate)
        {
            try
            {
                int i = 0;

                foreach (EmployeePaycodeArrears empArrears in arrears)
                {
                    empArrears.PayrollTransactionId = i--;
                    empArrears.EntryTypeCode = "NORM"; //Normal
                    empArrears.PayrollBatchId = payrollBatchId;
                    empArrears.UpdateUser = user.UserName;
                }

                //insert these records into the current batch
                foreach (BusinessObjects.PayrollTransaction arrearsTransaction in arrears)
                    PayrollAccess.InsertPayrollTransaction(user, arrearsTransaction);

                //copy the transaction id from the PayrollTransaction object to the EmployeePaycodeArrears object
                foreach (EmployeePaycodeArrears empArrears in arrears)
                    empArrears.PayrollTransactionId = ((BusinessObjects.PayrollTransaction)empArrears).PayrollTransactionId;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertPayrollBatchTrans", ex);
                throw;
            }
        }
        private EmployeeTerminationRoe CopyPositionDataToRoe(DatabaseUser user, EmployeePosition employeePosition, String roeDefaultContactLastName, String roeDefaultContactFirstName, String roeDefaultContactTelephone, String roeDefaultContactTelephoneExt)
        {
            EmployeeTerminationRoe newRoe = new EmployeeTerminationRoe();
            newRoe.EmployeePositionId = employeePosition.EmployeePositionId;
            newRoe.CreateUser = employeePosition.CreateUser;
            newRoe.UpdateUser = employeePosition.UpdateUser;

            //populate with default values
            newRoe.ContactLastName = roeDefaultContactLastName;
            newRoe.ContactFirstName = roeDefaultContactFirstName;
            newRoe.ContactTelephone = roeDefaultContactTelephone;
            newRoe.ContactTelephoneExtension = roeDefaultContactTelephoneExt;

            //get data needed for ROE insert...
            SummaryDataForRoeInsert tempSummObj = EmployeeAccess.GetSummaryDataForROEInsert(user, employeePosition.EmployeePositionId);

            if (tempSummObj.rehireDate != null)
                newRoe.FirstDayWorked = tempSummObj.rehireDate;
            else
                newRoe.FirstDayWorked = tempSummObj.hireDate;

            tempSummObj = null;

            return newRoe;
        }
        public EmployeePosition CreateEmployeePosition(DatabaseUser user)
        {
            try
            {
                EmployeePosition position = new EmployeePosition();
                position.EmployeePositionOrganizationUnits = GetEmployeePositionOrganizationUnitLevelSummary(user, -1);
                return position;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - CreateEmployeePosition", ex);
                throw;
            }
        }
        #endregion

        #region position import
        private String PositionDecodeCode(DatabaseUser user, WorkLinksEmployeePosition position)
        {
            String validationMessage = null;
            StringBuilder output = new StringBuilder();

            try
            {
                position.EmployeePositionReasonCode = CodeManagement.DecodeCode(user, CodeTableType.EmployeePositionReasonCode, position.EmployeePositionReasonCodeExternalIdentifier);
            }
            catch (FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.CodeMismatch)
                    output.AppendLine(ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            try
            {
                position.PermanencyTypeCode = CodeManagement.DecodeCode(user, CodeTableType.PermanencyTypeCode, position.PermanencyTypeCodeExternalIdentifier);
            }
            catch (FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.CodeMismatch)
                    output.AppendLine(ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            try
            {
                position.EmploymentTypeCode = CodeManagement.DecodeCode(user, CodeTableType.EmploymentTypeCode, position.EmploymentTypeCodeExternalIdentifier);
            }
            catch (FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.CodeMismatch)
                    output.AppendLine(ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            try
            {
                position.EmployeeTypeCode = CodeManagement.DecodeCode(user, CodeTableType.EmployeeTypeCode, position.EmployeeTypeCodeExternalIdentifier);
            }
            catch (FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.CodeMismatch)
                    output.AppendLine(ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            try
            {
                position.PaymentMethodCode = CodeManagement.DecodeCode(user, CodeTableType.PaymentMethodCode, position.PaymentMethodCodeExternalIdentifier);
            }
            catch (FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.CodeMismatch)
                    output.AppendLine(ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (output.Length > 0)
                validationMessage += output.ToString();

            return validationMessage;
        }
        private String ValidateOrganizationUnit(DatabaseUser user, WorkLinksEmployeePosition position)
        {
            String validationMessage = null;
            StringBuilder output = new StringBuilder();

            try
            {
                EmployeePositionOrganizationUnitCollection collection = new EmployeePositionOrganizationUnitCollection();
                String orgUnits = MapEmployeeOrganizationUnit(user, position.OrganationUnitImportExternalIdentifier1, output);

                int i = 0;

                if (orgUnits != null)
                {
                    foreach (String organizationUnitId in orgUnits.Substring(1).Split('/'))
                    {
                        EmployeePositionOrganizationUnitBulk orgUnit = new EmployeePositionOrganizationUnitBulk()
                        {
                            EmployeePositionOrganizationUnitId = i--,
                            OrganizationUnitLevelId = i--,
                            OrganizationUnitId = Convert.ToInt64(organizationUnitId),
                            OrganizationUnitStartDate = position.EffectiveDate
                        };

                        collection.Add(orgUnit);
                    }
                }

                position.EmployeePositionOrganizationUnits = collection;
            }
            catch (FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.CodeMismatch)
                    output.AppendLine(ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (output.Length > 0)
                validationMessage += output.ToString();

            return validationMessage;
        }
        private String MapEmployeeOrganizationUnit(DatabaseUser user, String organationUnitImportExternalIdentifier1, StringBuilder output)
        {
            String orgUnits = SelectEmployeePositionOrganizationUnitLevelHrxmlMatch(user, organationUnitImportExternalIdentifier1);

            if (orgUnits == null)
            {
                EmployeeServiceException exception = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.CodeMismatch);
                output.AppendLine(String.Format("Cost center:{0} cannot be found in WorkLinks.", organationUnitImportExternalIdentifier1));
            }

            return orgUnits;
        }
        private List<String> GetImportExternalIdentifiersFromPosition(DatabaseUser user, WorkLinksEmployeePosition[] xsdObject)
        {
            List<String> importExternalIdentifiers = new List<String>();

            foreach (WorkLinksEmployeePosition position in xsdObject)
                importExternalIdentifiers.Add(position.ImportExternalIdentifier);

            return importExternalIdentifiers;
        }
        private Dictionary<String, PayrollPeriod> GetPayrollPeriod(DatabaseUser user)
        {
            Dictionary<String, PayrollPeriod> payrollPeriodCache = new Dictionary<String, PayrollPeriod>();
            PayrollPeriodCollection payrollPeriods = PayrollManagement.GetPayrollPeriod(user, new PayrollPeriodCriteria() { GetMinOpenPeriodFlag = true });

            foreach (PayrollPeriod period in payrollPeriods)
                payrollPeriodCache.Add(period.PayrollProcessGroupCode, period);

            return payrollPeriodCache;
        }
        private Dictionary<String, CodeObject> GetLeaveReasonCode(DatabaseUser user)
        {
            Dictionary<String, CodeObject> leaveReasonCache = new Dictionary<String, CodeObject>();
            CodeCollection leaveReasonCodes = CodeManagement.GetCodeTable(user.DatabaseName, "EN", CodeTableType.EmployeePositionReasonCode);

            foreach (CodeObject code in leaveReasonCodes)
                leaveReasonCache.Add(code.Code, code);

            return leaveReasonCache;
        }
        private Dictionary<String, CodeObject> GetLeaveActionCode(DatabaseUser user, Dictionary<String, CodeObject> leaveReasonCache)
        {
            Dictionary<String, CodeObject> leaveActionCache = new Dictionary<String, CodeObject>();
            CodeCollection leaveActionCodes = CodeManagement.GetCodeTable(user.DatabaseName, "EN", CodeTableType.EmployeePositionActionCode);

            foreach (CodeObject leaveReason in leaveReasonCache.Values)
            {
                foreach (CodeObject leaveAction in leaveActionCodes)
                {
                    if (leaveAction.Code == leaveReason.ParentCode)
                        leaveActionCache.Add(leaveReason.Code + "_" + leaveAction.Code, leaveAction);
                }
            }

            return leaveActionCache;
        }
        public void ProcessPosition(DatabaseUser user, WorkLinksEmployeePosition[] xsdObject, ImportExportLog log, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions)
        {
            Dictionary<String, Employee> employeeCache = GetEmployeeCache(user, xsdObject);
            ProcessPosition(user, employeeCache, xsdObject, log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);
        }
        public void ProcessPosition(DatabaseUser user, Dictionary<string, Employee> employeeCache, WorkLinksEmployeePosition[] xsdObject, ImportExportLog log, string autoGenerateEmployeeNumber, string employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions)
        {
            try
            {
                StringBuilder output = new StringBuilder();
                int failedRows = 0;
                int successfulRows = 0;

                List<string> importExternalIdentifiers = GetImportExternalIdentifiersFromPosition(user, xsdObject);
                Dictionary<string, WizardCache> wizardCache = WizardManagement.GetWizardCache(user, importExternalIdentifiers);
                Dictionary<string, CodeObject> leaveReasonCache = GetLeaveReasonCode(user);
                Dictionary<string, CodeObject> leaveActionCache = GetLeaveActionCode(user, leaveReasonCache);

                Dictionary<string, EmployeePosition> positionCache = null;
                Dictionary<string, EmployeeEmploymentInformation> employmentCache = null;
                Dictionary<string, PayrollPeriod> payrollPeriodCache = null;
                Dictionary<string, DateTime?> cutoffDateCache = null;
                Dictionary<string, string> processGroupCache = null;
                PaycodeSalaryMapCollection salaryPaycodes = null;
                EmployeePaycodeArrearsCollection employeePaycodeArrears = null;
                char[] defaultWorkdays = null;

                //we only need these objects for existing employees
                if (employeeCache.Count > 0)
                {
                    positionCache = GetPositionCache(user, importExternalIdentifiers);
                    employmentCache = GetEmploymentInformationCache(user, importExternalIdentifiers);
                    payrollPeriodCache = GetPayrollPeriod(user);
                    cutoffDateCache = new Dictionary<string, DateTime?>();
                    processGroupCache = new Dictionary<string, string>();
                    salaryPaycodes = CodeAccess.GetSalaryPaycodes(user);
                    employeePaycodeArrears = PayrollAccess.GetEmployeePaycodeArrearsBatch(user, importExternalIdentifiers);
                }

                //we only need this object for pending employees
                if (wizardCache.Count > 0)
                    defaultWorkdays = CodeManagement.GetCodeTable(user.DatabaseName, "EN", CodeTableType.System, "DEWRKDAY")[0].Description.ToCharArray();

                using (TransactionScope scope = new TransactionScope())
                {
                    int i = 0;

                    //for existing employees
                    WorkLinksEmployeePositionCollection existingEmployeePositions = new WorkLinksEmployeePositionCollection();
                    WorkLinksEmployeePositionCollection formerExistingEmployeePositions = new WorkLinksEmployeePositionCollection();

                    //for pending employees
                    WorkLinksEmployeePositionCollection pendingEmployeePositions = new WorkLinksEmployeePositionCollection();
                    WorkLinksEmployeeEmploymentInformationCollection pendingEmployeeEmployment = new WorkLinksEmployeeEmploymentInformationCollection();

                    //this will get populated in the foreach below if Rehiring with an hours RESET
                    SalaryPlanGradeAccumulationCollection negateAccumulationHoursCollection = new SalaryPlanGradeAccumulationCollection();

                    foreach (WorkLinksEmployeePosition position in xsdObject)
                    {
                        string orgUnitValidationMessage = null;
                        string decodeCodeValidationMessage = PositionDecodeCode(user, position);

                        if (position.EmployeePositionReasonCode != null)
                        {
                            if (leaveReasonCache.ContainsKey(position.EmployeePositionReasonCode))
                            {
                                CodeObject leaveReasonCode = leaveReasonCache[position.EmployeePositionReasonCode];
                                CodeObject leaveActionCode = leaveActionCache[leaveReasonCode.Code + "_" + leaveReasonCode.ParentCode];

                                position.EmployeePositionStatusCode = leaveActionCode.ParentCode;
                                position.EmployeePositionActionCode = leaveActionCode.Code;
                            }
                        }

                        if (employeeCache.ContainsKey(position.ImportExternalIdentifier)) //employee was found in the database
                        {
                            EmployeePosition dbPosition = positionCache[position.ImportExternalIdentifier];

                            position.EmployeeId = dbPosition.EmployeeId;
                            //copy the salary plan info
                            position.SalaryPlanId = dbPosition.SalaryPlanId;
                            position.SalaryPlanGradeId = dbPosition.SalaryPlanGradeId;
                            position.SalaryPlanGradeStepId = dbPosition.SalaryPlanGradeStepId;

                            //map the org units
                            if (position.OrganationUnitImportExternalIdentifier1 != null)
                                orgUnitValidationMessage = ValidateOrganizationUnit(user, position);
                            else
                                position.EmployeePositionOrganizationUnits = dbPosition.EmployeePositionOrganizationUnits;

                            //map the salary
                            if (position.PaymentMethodCode == null)
                            {
                                position.PaymentMethodCode = dbPosition.PaymentMethodCode;
                                position.CompensationAmount = dbPosition.CompensationAmount;
                            }

                            //use last workdays and secondary positions
                            position.Workdays = dbPosition.Workdays;
                            position.SecondaryPositions = dbPosition.SecondaryPositions;

                            //if rehiring and resetting hours...
                            if (position.EmployeePositionActionCode == "RHRESET")
                            {
                                //reset positions to step 1 (Start rate)
                                foreach (EmployeePositionSecondary secondary in position.SecondaryPositions)
                                {
                                    //The primary position is also one of the secondary positions.  Update the primary to use the same step as secondary after it is updated
                                    if (secondary.SalaryPlanId == position.SalaryPlanId && secondary.SalaryPlanGradeId == position.SalaryPlanGradeId && secondary.SalaryPlanGradeStepId == position.SalaryPlanGradeStepId)
                                    {
                                        //reset secondary
                                        ResetStepAndCompAmount(user, secondary);

                                        //update primary
                                        position.SalaryPlanGradeStepId = secondary.SalaryPlanGradeStepId;
                                        position.CompensationAmount = secondary.CompensationAmount;
                                    }
                                    else
                                        ResetStepAndCompAmount(user, secondary);
                                }
                            }

                            //use last status if the status is "X"
                            if (position.EmployeePositionStatusCode == "X")
                                position.EmployeePositionStatusCode = dbPosition.EmployeePositionStatusCode;

                            //use last standard hours if not supplied
                            if (position.StandardHours < 0)
                                position.StandardHours = dbPosition.StandardHours;

                            if (position.MeetsPositionRequirements(decodeCodeValidationMessage, dbPosition, orgUnitValidationMessage))
                            {
                                successfulRows++;

                                if (position.EmployeePositionActionCode == "RH" || position.EmployeePositionActionCode == "RHRESET")
                                {
                                    //get current cutoff date
                                    EmployeeEmploymentInformation employmentInformation = employmentCache[position.ImportExternalIdentifier];
                                    PayrollPeriod payrollPeriod = payrollPeriodCache[employmentInformation.PayrollProcessGroupCode];

                                    cutoffDateCache.Add(position.ImportExternalIdentifier, payrollPeriod.CutoffDate);
                                    processGroupCache.Add(position.ImportExternalIdentifier, employmentInformation.PayrollProcessGroupCode);

                                    //get the accumulation totals and create a negative record to insert to reset hours to 0
                                    NegateAccumulationHoursTotal(user, position, negateAccumulationHoursCollection);
                                }

                                position.EmployeePositionId = i--;
                                existingEmployeePositions.Add(position);

                                WorkLinksEmployeePosition tempFormerPosition = new WorkLinksEmployeePosition();
                                tempFormerPosition.ImportExternalIdentifier = position.ImportExternalIdentifier;
                                dbPosition.CopyTo(tempFormerPosition);
                                formerExistingEmployeePositions.Add(tempFormerPosition);
                            }
                            else
                            {
                                failedRows++;
                                output.Append(position.GetValidationMessage(decodeCodeValidationMessage, dbPosition, orgUnitValidationMessage));
                            }
                        }
                        else
                        {
                            if (wizardCache.ContainsKey(position.ImportExternalIdentifier)) //employee is pending
                            {
                                orgUnitValidationMessage = ValidateOrganizationUnit(user, position);

                                if (position.MeetsPositionRequirements(decodeCodeValidationMessage, null, orgUnitValidationMessage))
                                {
                                    successfulRows++;

                                    //set the default workdays from the code system table
                                    EmployeePositionWorkdayCollection workdays = new EmployeePositionWorkdayCollection();
                                    for (int x = 0; x < defaultWorkdays.Length; x++)
                                    {
                                        workdays.Add(new EmployeePositionWorkday()
                                        {
                                            EmployeePositionWorkdayId = -(x + 1),
                                            EmployeePositionId = position.EmployeePositionId,
                                            IsWorkdayFlag = defaultWorkdays[x].ToString() == "1" ? true : false,
                                            DayOfWeek = Convert.ToString(x + 1)
                                        });

                                    }
                                    position.Workdays = workdays;

                                    //set the employment hire date using the position's effective date
                                    WizardCache cache = wizardCache[position.ImportExternalIdentifier];
                                    EmployeeEmploymentInformationCollection employmentCollection = null;

                                    if (cache.Items[cache.WizardCacheId.ToString() + '_' + WizardCache.ItemType.EmployeeEmploymentInformationControl] != null)
                                        employmentCollection = ((EmployeeEmploymentInformationCollection)cache.Items[cache.WizardCacheId.ToString() + '_' + WizardCache.ItemType.EmployeeEmploymentInformationControl].Data);

                                    if (employmentCollection != null)
                                    {
                                        WorkLinksEmployeeEmploymentInformation workLinksEmployment = new WorkLinksEmployeeEmploymentInformation();
                                        workLinksEmployment.ImportExternalIdentifier = position.ImportExternalIdentifier;

                                        employmentCollection[0].HireDate = position.EffectiveDate;
                                        employmentCollection[0].CopyTo(workLinksEmployment);

                                        pendingEmployeeEmployment.Add(workLinksEmployment);
                                    }

                                    position.EmployeePositionId = i--;
                                    pendingEmployeePositions.Add(position);
                                }
                                else
                                {
                                    failedRows++;
                                    output.Append(position.GetValidationMessage(decodeCodeValidationMessage, null, orgUnitValidationMessage));
                                }
                            }
                            else //employee is missing
                            {
                                failedRows++;
                                output.Append(String.Format(GuiManagement.GetResourceByLanguageAndKey(user, "ErrorMessages", "MissingEmployee"), position.ImportExternalIdentifier) + "\r\n");
                            }
                        }
                    }

                    ImportPosition(user, existingEmployeePositions, formerExistingEmployeePositions, salaryPaycodes, employeePaycodeArrears, payrollPeriodCache, cutoffDateCache, processGroupCache, pendingEmployeePositions, pendingEmployeeEmployment, autoGenerateEmployeeNumber, employeeNumberFormat, autoAddSecondaryPositions, negateAccumulationHoursCollection, importExternalIdentifiers);

                    if (importDirtySave || failedRows == 0)
                        scope.Complete();
                }

                output.AppendLine("----------");
                output.AppendLine("# rows total: " + (successfulRows + failedRows));
                output.AppendLine("# rows succeeded: " + successfulRows);
                output.AppendLine("# rows failed: " + failedRows);

                if (failedRows > 0)
                {
                    if (importDirtySave)
                        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n" + "----------\r\n");
                    else
                        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportCompleteProcessFailed") + "\r\n" + "----------\r\n");
                }
                else
                    output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n");

                log.SuccessFlag = true;
                log.ProcessingOutput += output.ToString();
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - ProcessPosition", ex);
                throw;
            }
        }
        private void ImportPosition(DatabaseUser user, WorkLinksEmployeePositionCollection existingEmployeePositions, WorkLinksEmployeePositionCollection formerExistingEmployeePositions, PaycodeSalaryMapCollection salaryPaycodes, EmployeePaycodeArrearsCollection employeePaycodeArrears, Dictionary<String, PayrollPeriod> payrollPeriodCache, Dictionary<String, DateTime?> cutoffDateCache, Dictionary<String, String> payrollProcessGroupCache, WorkLinksEmployeePositionCollection pendingEmployeePositions, WorkLinksEmployeeEmploymentInformationCollection pendingEmployeeEmployment, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool autoAddSecondaryPositions, SalaryPlanGradeAccumulationCollection negateAccumulationHoursCollection, List<string> importExternalIdentifiers)
        {
            //insert new employee positions
            if (existingEmployeePositions.Count > 0)
            {
                ImportEmployeePosition(user, existingEmployeePositions, formerExistingEmployeePositions, salaryPaycodes);
                ImportEmployeePositionRehire(user, existingEmployeePositions, employeePaycodeArrears, payrollPeriodCache, cutoffDateCache, payrollProcessGroupCache, negateAccumulationHoursCollection, importExternalIdentifiers);
            }

            //update pending employee employee positions or insert new employee positions
            if (pendingEmployeePositions.Count > 0)
            {
                WizardCacheImportCollection collection = new WizardCacheImportCollection();
                int i = 0;

                foreach (WorkLinksEmployeePosition position in pendingEmployeePositions)
                {
                    collection.Add(new WizardCacheImport
                    {
                        WizardCacheId = i--,
                        WizardId = 1, //1 = Add Employee Wizard
                        ImportExternalIdentifier = position.ImportExternalIdentifier,
                        WizardItemId = 6, //6 = EmployeePositionViewControl
                        Data = position.EmployeePositionCollection
                    });
                }

                WizardManagement.ImportWizardCache(user, collection);
                WizardManagement.ValidateAndHireEmployee(user, collection, autoGenerateEmployeeNumber, employeeNumberFormat, autoAddSecondaryPositions);
            }

            //update pending employee employment information
            if (pendingEmployeeEmployment != null)
                ImportEmployeeEmploymentInformation(user, new WorkLinksEmployeeEmploymentInformationCollection(), pendingEmployeeEmployment, autoGenerateEmployeeNumber, employeeNumberFormat, autoAddSecondaryPositions);
        }
        private void ImportEmployeePosition(DatabaseUser user, WorkLinksEmployeePositionCollection existingEmployeePositions, WorkLinksEmployeePositionCollection formerExistingEmployeePositions, PaycodeSalaryMapCollection salaryPaycodes)
        {
            foreach (WorkLinksEmployeePosition position in existingEmployeePositions)
            {
                foreach (WorkLinksEmployeePosition formerPosition in formerExistingEmployeePositions)
                {
                    if (formerPosition.EmployeeId == position.EmployeeId)
                    {
                        //if former position is on the same date, advance sequence number
                        if (formerPosition != null && ((DateTime)formerPosition.EffectiveDate).ToShortDateString().Equals(((DateTime)position.EffectiveDate).ToShortDateString()))
                        {
                            if (position.Equals(formerPosition))
                            {
                                position.SkipUpdate = true;
                            }

                            position.EffectiveSequence = (short)(formerPosition.EffectiveSequence + 1);
                        }

                        //if term related we need to set the roe status
                        if (position.IsTermRelated)
                            position.CodeRoeCreationStatusCd = "PEN";

                        //match employee status record to paycode_salary_map
                        PaycodeSalaryMap salaryPaycodeForEmployee = salaryPaycodes.GetFirstPaycodeSalaryMapByStatusCode(position.EmployeePositionStatusCode);

                        //only if there's a match do we enter this code (ex. Terminations will not enter here)
                        if (salaryPaycodeForEmployee != null)
                        {
                            //if EmployeePositionStatusCode mapping on paycode_salary_map.lock_salary_rate_flag = 1
                            if (salaryPaycodeForEmployee.LockSalaryRateFlag)
                            {
                                //get former employee_position record's locked_compensation_amount. if it's null, get current records compensation_rate and store it in column employee_position.locked_compensation_amount
                                position.LockedCompensationAmount = (formerPosition.LockedCompensationAmount != null) ? formerPosition.LockedCompensationAmount : position.CompensationAmount;

                                //get former employee_position record's locked_standard_hours. if it's null, get current records standard_hours and store it in column employee_position.locked_standard_hours
                                position.LockedStandardHours = (formerPosition.LockedStandardHours != null) ? formerPosition.LockedStandardHours : position.StandardHours;
                            }
                            else //if paycode_salary_map.lock_salary_rate_flag = 0, set locked_compensation_amount = null and locked_standard_hours = null
                            {
                                position.LockedCompensationAmount = null;
                                position.LockedStandardHours = null;
                            }
                        }

                        break;
                    }
                }
            }

            //batch insert employee position records
            EmployeeAccess.ImportEmployeePosition(user, existingEmployeePositions.UpdateablePositions);
        }
        private void ImportEmployeePositionRehire(DatabaseUser user, WorkLinksEmployeePositionCollection existingEmployeePositions, EmployeePaycodeArrearsCollection employeePaycodeArrears, Dictionary<string, PayrollPeriod> payrollPeriodCache, Dictionary<string, DateTime?> cutoffDateCache, Dictionary<string, string> payrollProcessGroupCache, SalaryPlanGradeAccumulationCollection negateAccumulationHoursCollection, List<string> importExternalIdentifiers)
        {
            EmployeePaycodeArrearsCollection arrearsCollection = new EmployeePaycodeArrearsCollection();

            //get the newly inserted positions from the db as we need the employee_position_id
            WorkLinksEmployeePositionCollection newlyInsertedPositionsWithIds = EmployeeAccess.GetBatchLatestEmployeePosition(user, importExternalIdentifiers);

            foreach (WorkLinksEmployeePosition position in existingEmployeePositions)
            {
                //foreach newly inserted position, update the "position" variable to have the new employee_position_id
                AssignPositionIdFromDb(newlyInsertedPositionsWithIds, position);

                //for a given employee who has been rehired (RH, RHRESET)- update all former records that are terminations and have pending roe's (PEN) to have an ROE status of not processed (NOTPROC).
                if (position.EmployeePositionActionCode == "RH" || position.EmployeePositionActionCode == "RHRESET")
                {
                    string payrollProcessGroup = payrollProcessGroupCache[position.ImportExternalIdentifier];
                    DateTime? cutoffDate = cutoffDateCache[position.ImportExternalIdentifier];

                    //assign the new position id to the CreateEmployeePositionId field
                    AssignCreateEmployeePositionId(negateAccumulationHoursCollection, position);

                    //check if this rehire (RH, RHRESET) is happening in a current/past period
                    if (cutoffDate != null && cutoffDate >= position.EffectiveDate)
                    {
                        foreach (EmployeePaycodeArrears paycodeArrears in employeePaycodeArrears)
                        {
                            if (paycodeArrears.EmployeeId == position.EmployeeId)
                                arrearsCollection.Add(paycodeArrears);
                        }

                        //check if employee has arrears records
                        if (arrearsCollection.Count > 0)
                        {
                            //insert a batch
                            long tempBatchId = InsertPayrollBatch(user, cutoffDate, payrollProcessGroup);

                            if (tempBatchId > 0)
                            {
                                //add employee's arrears tranactions
                                InsertPayrollBatchTrans(user, arrearsCollection, tempBatchId, payrollProcessGroup, cutoffDate);

                                //update the arrears records to be processed (tag the positionid and transaction id too)
                                foreach (EmployeePaycodeArrears empArrears in arrearsCollection)
                                {
                                    empArrears.ProcessingEmployeePositionId = position.EmployeePositionId;
                                    empArrears.CodeEmployeePaycodeArrearsStatusCd = "PROC";
                                    PayrollAccess.UpdateEmployeeArrears(user, empArrears);
                                }
                            }
                        }
                    }
                }

                if (position.EmployeePositionActionCode == "TERM" || position.EmployeePositionActionCode == "LEAV" || position.EmployeePositionActionCode == "BENLEAV" || position.EmployeePositionActionCode == "LD0") /* HACK */
                {
                    //position changed to "termination", therefore insert an ROE record with minimal data
                    EmployeeTerminationRoe tempRoe = CopyPositionDataToRoe(user, position, null, null, null, null);
                    EmployeeAccess.InsertEmployeeTerminationRoe(user, tempRoe);
                }

                //call the Employee_resetDataAttributes proc
                EmployeeAccess.ResetDataAttributes(user, position.EmployeeId);
            }

            //if there's accululation hours to negate, do the insert here
            if (negateAccumulationHoursCollection != null && negateAccumulationHoursCollection.Count > 0)
                PayrollAccess.InsertPayrollTransaction(user, negateAccumulationHoursCollection);
        }

        private void AssignCreateEmployeePositionId(SalaryPlanGradeAccumulationCollection negateAccumulationHoursCollection, WorkLinksEmployeePosition position)
        {
            //get the accumulations for this employee and assign the CreateEmployeePositionId
            IEnumerable<SalaryPlanGradeAccumulation> rows = from hoursCollection
                         in negateAccumulationHoursCollection
                                                            where hoursCollection.EmployeeId == position.EmployeeId
                                                            select hoursCollection;

            foreach (SalaryPlanGradeAccumulation item in rows)
            {
                item.CreateEmployeePositionId = position.EmployeePositionId;
            }
        }

        private void AssignPositionIdFromDb(WorkLinksEmployeePositionCollection newlyInsertedPositionsWithIds, WorkLinksEmployeePosition position)
        {
            IEnumerable<WorkLinksEmployeePosition> rows = from newPositions
                                                           in newlyInsertedPositionsWithIds
                                                          where newPositions.EmployeeId == position.EmployeeId
                                                          select newPositions;

            foreach (WorkLinksEmployeePosition item in rows)
            {
                position.EmployeePositionId = item.EmployeePositionId;
            }
        }
        #endregion

        #region employee position workday
        public EmployeePositionWorkdayCollection GetEmployeePositionWorkday(DatabaseUser user, long employeePositionId)
        {
            try
            {
                return EmployeeAccess.GetEmployeePositionWorkday(user, employeePositionId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeePositionWorkday", ex);
                throw;
            }
        }
        public EmployeePositionWorkday InsertEmployeePositionWorkday(DatabaseUser user, EmployeePositionWorkday workday)
        {
            try
            {
                return EmployeeAccess.InsertEmployeePositionWorkday(user, workday);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeePositionWorkday", ex);
                throw;
            }
        }
        public void UpdateEmployeePositionWorkday(DatabaseUser user, EmployeePositionWorkday workday)
        {
            try
            {
                EmployeeAccess.UpdateEmployeePositionWorkday(user, workday);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeePositionWorkday", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeePositionWorkday(DatabaseUser user, EmployeePositionWorkday workday)
        {
            try
            {
                EmployeeAccess.DeleteEmployeePositionWorkday(user, workday);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeePositionWorkday", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region employee position organization unit level report
        public EmployeePositionOrganizationUnitCollection GetEmployeePositionOrganizationUnitLevelSummary(DatabaseUser user, long employeePositionId)
        {
            try
            {
                return EmployeeAccess.GetEmployeePositionOrganizationUnitLevelSummary(user, employeePositionId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeePositionOrganizationUnitLevelSummary", ex);
                throw;
            }
        }
        public String SelectEmployeePositionOrganizationUnitLevelHrxmlMatch(DatabaseUser user, String costCenter)
        {
            try
            {
                return EmployeeAccess.SelectEmployeePositionOrganizationUnitLevelHrxmlMatch(user, costCenter);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - SelectEmployeePositionOrganizationUnitLevelHrxmlMatch", ex);
                throw;
            }
        }
        #endregion~

        #region employee position secondary
        public EmployeePositionSecondaryCollection GetEmployeePositionSecondary(DatabaseUser user, long? employeePositionSecondaryId, long? employeePositionId, string languageCode, string organizationUnit = null, bool includeOrganizatonUnit = true)
        {
            try
            {
                EmployeePositionSecondaryCollection collection = EmployeeAccess.GetEmployeePositionSecondary(user, employeePositionSecondaryId, employeePositionId, languageCode, organizationUnit);

                if (includeOrganizatonUnit)
                {
                    foreach (EmployeePositionSecondary position in collection)
                        position.OrganizationUnits = GetEmployeePositionSecondaryOrganizationUnit(user, position.EmployeePositionSecondaryId);
                }

                return collection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeePositionSecondary", ex);
                throw;
            }
        }
        public void InsertEmployeePositionSecondary(DatabaseUser user, EmployeePositionSecondary item)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (EmployeePositionSecondaryOrganizationUnit unit in item.OrganizationUnits)
                    {
                        if (!unit.UsedOnSecondaryEmployeePositionFlag)
                        {
                            unit.OrganizationUnitId = null;
                            unit.OrganizationUnitStartDate = null;
                        }
                    }

                    EmployeeAccess.InsertEmployeePositionSecondary(user, item);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeePositionSecondary", ex);
                throw;
            }
        }
        public EmployeePositionSecondary UpdateEmployeePositionSecondary(DatabaseUser user, EmployeePositionSecondary item)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (EmployeePositionSecondaryOrganizationUnit unit in item.OrganizationUnits)
                    {
                        if (!unit.UsedOnSecondaryEmployeePositionFlag)
                        {
                            unit.OrganizationUnitId = null;
                            unit.OrganizationUnitStartDate = null;
                        }
                    }

                    EmployeeAccess.UpdateEmployeePositionSecondary(user, item);

                    scope.Complete();

                    return item;
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeePositionSecondary", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }

        public void DeleteSalaryPlanGradeAccumulationByCreateEmployeePositionId(DatabaseUser user, long createEmployeePositionId)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    EmployeeAccess.DeleteSalaryPlanGradeAccumulationByCreateEmployeePositionId(user, createEmployeePositionId);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteSalaryPlanGradeAccumulationByCreateEmployeePositionId", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }

        public void DeleteEmployeePositionSecondary(DatabaseUser user, EmployeePositionSecondary item)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (EmployeePositionSecondaryOrganizationUnit unit in item.OrganizationUnits)
                        DeleteEmployeePositionSecondaryOrganizationUnit(user, unit);

                    EmployeeAccess.DeleteEmployeePositionSecondary(user, item);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeePositionSecondary", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region employee position secondary organization unit
        public EmployeePositionSecondaryOrganizationUnitCollection GetEmployeePositionSecondaryOrganizationUnit(DatabaseUser user, long? employeePositionSecondaryId)
        {
            try
            {
                return EmployeeAccess.GetEmployeePositionSecondaryOrganizationUnit(user, employeePositionSecondaryId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeePositionSecondaryOrganizationUnit", ex);
                throw;
            }
        }
        //public void InsertEmployeePositionSecondaryOrganizationUnit(DatabaseUser user, EmployeePositionSecondaryOrganizationUnit item)
        //{
        //    try
        //    {
        //        EmployeeAccess.InsertEmployeePositionSecondaryOrganizationUnit(user, item);
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeePositionSecondaryOrganizationUnit", ex);
        //        throw;
        //    }
        //}
        //public void UpdateEmployeePositionSecondaryOrganizationUnit(DatabaseUser user, EmployeePositionSecondaryOrganizationUnit item)
        //{
        //    try
        //    {
        //        EmployeeAccess.UpdateEmployeePositionSecondaryOrganizationUnit(user, item);
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeePositionSecondaryOrganizationUnit", ex);

        //        if (ex is SqlServerException)
        //        {
        //            if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
        //            {
        //                EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
        //                throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
        //            }
        //            else
        //                throw;
        //        }
        //        else
        //            throw;
        //    }
        //}
        public void DeleteEmployeePositionSecondaryOrganizationUnit(DatabaseUser user, EmployeePositionSecondaryOrganizationUnit item)
        {
            try
            {
                EmployeeAccess.DeleteEmployeePositionSecondaryOrganizationUnit(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeePositionSecondaryOrganizationUnit", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region employee wsib health and safety report
        public EmployeeWsibHealthAndSafetyReportCollection GetEmployeeWsibHealthAndSafetyReport(DatabaseUser user, long employeeId)
        {
            try
            {
                return EmployeeAccess.GetEmployeeWsibHealthAndSafetyReport(user, employeeId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeWsibHealthAndSafetyReport", ex);
                throw;
            }
        }
        public EmployeeWsibHealthAndSafetyReportPersonCollection GetEmployeeWsibHealthAndSafetyReportPerson(DatabaseUser user, long employeeId)
        {
            try
            {
                return EmployeeAccess.GetEmployeeWsibHealthAndSafetyReportPerson(user, employeeId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeWsibHealthAndSafetyReportPerson", ex);
                throw;
            }
        }
        public void InsertEmployeeWsibHealthAndSafetyReport(DatabaseUser user, EmployeeWsibHealthAndSafetyReport item)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    AddressAccess.InsertAddress(user, item);
                    EmployeeAccess.InsertEmployeeWsibHealthAndSafetyReport(user, item);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeWsibHealthAndSafetyReport", ex);
                throw;
            }
        }
        public void UpdateEmployeeWsibHealthAndSafetyReport(DatabaseUser user, EmployeeWsibHealthAndSafetyReport item)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    item.OverrideConcurrencyCheckFlag = true;

                    AddressAccess.UpdateAddress(user, item);
                    EmployeeAccess.UpdateEmployeeWsibHealthAndSafetyReport(user, item);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeeWsibHealthAndSafetyReport", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeeWsibHealthAndSafetyReport(DatabaseUser user, EmployeeWsibHealthAndSafetyReport item)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    EmployeeAccess.DeleteEmployeeWsibHealthAndSafetyReport(user, item);
                    AddressAccess.DeleteAddress(user, item);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeWsibHealthAndSafetyReport", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region statutory deduction
        public StatutoryDeductionCollection GetStatutoryDeduction(DatabaseUser user, long id, bool securityOverrideFlag)
        {
            try
            {
                return EmployeeAccess.GetStatutoryDeduction(user, id, securityOverrideFlag);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetStatutoryDeduction", ex);
                throw;
            }
        }
        public void InsertStatutoryDeductionByType(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                if (item.StatutoryDeductionTypeCode.Equals("BB"))
                    InsertStatutoryDeductionBarbados(user, item);
                else if (item.StatutoryDeductionTypeCode.Equals("JM"))
                    InsertStatutoryDeductionJamaica(user, item);
                else if (item.StatutoryDeductionTypeCode.Equals("STLU"))
                    InsertStatutoryDeductionSaintLucia(user, item);
                else if (item.StatutoryDeductionTypeCode.Equals("TT"))
                    InsertStatutoryDeductionTrinidad(user, item);
                else
                    InsertStatutoryDeduction(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertStatutoryDeductionByType", ex);
                throw;
            }
        }
        public void InsertStatutoryDeduction(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                EmployeeAccess.InsertStatutoryDeduction(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertStatutoryDeduction", ex);
                throw;
            }
        }
        public void UpdateStatutoryDeduction(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                EmployeeAccess.UpdateStatutoryDeduction(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateStatutoryDeduction", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteStatutoryDeduction(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                EmployeeAccess.DeleteStatutoryDeduction(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteStatutoryDeduction", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region statutory deduction barbados
        public void InsertStatutoryDeductionBarbados(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    InsertStatutoryDeduction(user, item);
                    EmployeeAccess.InsertStatutoryDeductionBarbados(user, item);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertStatutoryDeductionBarbados", ex);
                throw;
            }
        }
        public void UpdateStatutoryDeductionBarbados(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    UpdateStatutoryDeduction(user, item);
                    EmployeeAccess.UpdateStatutoryDeductionBarbados(user, item);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateStatutoryDeductionBarbados", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteStatutoryDeductionBarbados(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    EmployeeAccess.DeleteStatutoryDeductionBarbados(user, item);
                    DeleteStatutoryDeduction(user, item);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteStatutoryDeductionBarbados", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region statutory deduction saint lucia
        public void InsertStatutoryDeductionSaintLucia(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    InsertStatutoryDeduction(user, item);
                    EmployeeAccess.InsertStatutoryDeductionSaintLucia(user, item);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertStatutoryDeductionSaintLucia", ex);
                throw;
            }
        }
        public void UpdateStatutoryDeductionSaintLucia(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    UpdateStatutoryDeduction(user, item);
                    EmployeeAccess.UpdateStatutoryDeductionSaintLucia(user, item);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateStatutoryDeductionSaintLucia", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteStatutoryDeductionSaintLucia(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    EmployeeAccess.DeleteStatutoryDeductionSaintLucia(user, item);
                    DeleteStatutoryDeduction(user, item);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteStatutoryDeductionSaintLucia", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region statutory deduction trinidad
        public void InsertStatutoryDeductionTrinidad(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    InsertStatutoryDeduction(user, item);
                    EmployeeAccess.InsertStatutoryDeductionTrinidad(user, item);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertStatutoryDeductionTrinidad", ex);
                throw;
            }
        }
        public void UpdateStatutoryDeductionTrinidad(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    UpdateStatutoryDeduction(user, item);
                    EmployeeAccess.UpdateStatutoryDeductionTrinidad(user, item);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateStatutoryDeductionTrinidad", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteStatutoryDeductionTrinidad(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    EmployeeAccess.DeleteStatutoryDeductionTrinidad(user, item);
                    DeleteStatutoryDeduction(user, item);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteStatutoryDeductionTrinidad", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region statutory deduction jamaica
        public void InsertStatutoryDeductionJamaica(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    InsertStatutoryDeduction(user, item);
                    EmployeeAccess.InsertStatutoryDeductionJamaica(user, item);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertStatutoryDeductionJamaica", ex);
                throw;
            }
        }
        public void UpdateStatutoryDeductionJamaica(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    UpdateStatutoryDeduction(user, item);
                    EmployeeAccess.UpdateStatutoryDeductionJamaica(user, item);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateStatutoryDeductionJamaica", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteStatutoryDeductionJamaica(DatabaseUser user, StatutoryDeduction item)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    EmployeeAccess.DeleteStatutoryDeductionJamaica(user, item);
                    DeleteStatutoryDeduction(user, item);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteStatutoryDeductionJamaica", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region stat deduction import
        public void ImportStatDeduction(String database, String username, StatDeductionImport[] array)
        {
            //use trans scope here?????  stacy test

            List<StatDeductionImport> partial = new List<StatDeductionImport>();

            for (int i = 0; i < array.Length; i++)
            {
                //add to list
                partial.Add(array[i]);

                if ((i + 1) % 5000 == 0 || i == array.Length - 1)
                {
                    //store in db 
                    DynamicsAccess.ImportStatDeduction(database, username, partial);

                    //clear the list
                    partial.Clear();
                }
            }
        }
        private String StatutoryDeductionDecodeCode(DatabaseUser user, WorklinksStatutoryDeduction statDeduction, CodeCollection businessNumbers)
        {
            String validationMessage = null;
            StringBuilder output = new StringBuilder();

            try
            {
                statDeduction.ProvinceStateCode = CodeManagement.DecodeCode(user, CodeTableType.ProvinceStateCode, statDeduction.ProvinceStateCodeExternalIdentifier);
            }
            catch (FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.CodeMismatch)
                    output.AppendLine(ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            try
            {
                //this is not a code table value, but we have to check if it's in our system
                String value = null;

                foreach (CodeObject business in businessNumbers)
                {
                    if (business.Description == statDeduction.EmployerExternalIdentifier)
                    {
                        value = business.Code;
                        break;
                    }
                }

                if (value != null)
                    statDeduction.BusinessNumberId = Convert.ToInt64(value);
                else
                    output.AppendLine(String.Format("Cannot match code:{0} type:{1} in WorkLinks.", statDeduction.EmployerExternalIdentifier, "EmployerNumber"));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (output.Length > 0)
                validationMessage += output.ToString();

            return validationMessage;
        }
        private Dictionary<String, WizardCache> GetWizardCacheFromStatutoryDeduction(DatabaseUser user, WorklinksStatutoryDeduction[] xsdObject)
        {
            List<String> importExternalIdentifiers = new List<String>();

            foreach (WorklinksStatutoryDeduction statDeduction in xsdObject)
                importExternalIdentifiers.Add(statDeduction.ImportExternalIdentifier);

            return WizardManagement.GetWizardCache(user, importExternalIdentifiers);
        }
        private Dictionary<String, StatutoryDeductionDefault> GetStatutoryDeductionDefaultCache(DatabaseUser user)
        {
            Dictionary<String, StatutoryDeductionDefault> statDeductionDefaultCache = new Dictionary<String, StatutoryDeductionDefault>();
            StatutoryDeductionDefaultCollection statutoryDeductionDefaults = GetStatutoryDeductionDefaults(user, null);

            foreach (StatutoryDeductionDefault statutoryDeduction in statutoryDeductionDefaults)
                statDeductionDefaultCache.Add(statutoryDeduction.ProvinceStateCode, statutoryDeduction);

            return statDeductionDefaultCache;
        }
        public void ProcessStatutoryDeduction(DatabaseUser user, WorklinksStatutoryDeduction[] xsdObject, ImportExportLog log, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions)
        {
            Dictionary<String, Employee> employeeCache = GetEmployeeCache(user, xsdObject);
            ProcessStatutoryDeduction(user, employeeCache, xsdObject, log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);
        }
        public void ProcessStatutoryDeduction(DatabaseUser user, Dictionary<String, Employee> employeeCache, WorklinksStatutoryDeduction[] xsdObject, ImportExportLog log, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions)
        {
            try
            {
                StringBuilder output = new StringBuilder();
                int failedRows = 0;
                int successfulRows = 0;

                CodeCollection businessNumbers = CodeManagement.GetBusinessAndEmployerNumber(user, null);
                Dictionary<String, WizardCache> wizardCache = GetWizardCacheFromStatutoryDeduction(user, xsdObject);
                Dictionary<String, StatutoryDeductionDefault> statDeductionDefaultCache = GetStatutoryDeductionDefaultCache(user);

                using (TransactionScope scope = new TransactionScope())
                {
                    int i = 0;

                    WorklinksStatutoryDeductionCollection existingEmployeeStatDeductions = new WorklinksStatutoryDeductionCollection();
                    WorklinksStatutoryDeductionCollection pendingEmployeeStatDeductions = new WorklinksStatutoryDeductionCollection();

                    foreach (WorklinksStatutoryDeduction statDeduction in xsdObject)
                    {
                        String decodeCodeValidationMessage = StatutoryDeductionDecodeCode(user, statDeduction, businessNumbers);

                        if (statDeduction.MeetsStatDeductionRequirements(decodeCodeValidationMessage))
                        {
                            //default to Canada if the type isn't supplied
                            if (statDeduction.StatutoryDeductionTypeCodeImportExternalIdentifier == null)
                                statDeduction.StatutoryDeductionTypeCode = "CAN";
                            else
                                statDeduction.StatutoryDeductionTypeCode = statDeduction.StatutoryDeductionTypeCodeImportExternalIdentifier;

                            //if province is not QC, ParentalInsurancePlanFlag is always false
                            if (statDeduction.ProvinceStateCode != "QC")
                                statDeduction.ParentalInsurancePlanFlag = false;

                            SetStatutoryDeductionDefaults(statDeduction, statDeductionDefaultCache);

                            if (employeeCache.ContainsKey(statDeduction.ImportExternalIdentifier)) //employee was found in the database
                            {
                                successfulRows++;

                                statDeduction.StatutoryDeductionId = i--;
                                statDeduction.EmployeeId = employeeCache[statDeduction.ImportExternalIdentifier].EmployeeId;
                                existingEmployeeStatDeductions.Add(statDeduction);
                            }
                            else
                            {
                                if (wizardCache.ContainsKey(statDeduction.ImportExternalIdentifier)) //employee is pending
                                {
                                    successfulRows++;

                                    statDeduction.StatutoryDeductionId = i--;
                                    pendingEmployeeStatDeductions.Add(statDeduction);
                                }
                                else //employee is missing
                                {
                                    failedRows++;
                                    output.Append(String.Format(GuiManagement.GetResourceByLanguageAndKey(user, "ErrorMessages", "MissingEmployee"), statDeduction.ImportExternalIdentifier) + "\r\n");
                                }
                            }
                        }
                        else
                        {
                            failedRows++;
                            output.Append(statDeduction.GetValidationMessage(decodeCodeValidationMessage));
                        }
                    }

                    ImportStatutoryDeduction(user, existingEmployeeStatDeductions, pendingEmployeeStatDeductions, autoGenerateEmployeeNumber, employeeNumberFormat, autoAddSecondaryPositions);

                    if (importDirtySave || failedRows == 0)
                        scope.Complete();
                }

                output.AppendLine("----------");
                output.AppendLine("# rows total: " + (successfulRows + failedRows));
                output.AppendLine("# rows succeeded: " + successfulRows);
                output.AppendLine("# rows failed: " + failedRows);

                if (failedRows > 0)
                {
                    if (importDirtySave)
                        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n" + "----------\r\n");
                    else
                        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportCompleteProcessFailed") + "\r\n" + "----------\r\n");
                }
                else
                    output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n");

                log.SuccessFlag = true;
                log.ProcessingOutput += output.ToString();
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - ProcessStatutoryDeduction", ex);
                throw;
            }
        }
        private void SetStatutoryDeductionDefaults(WorklinksStatutoryDeduction statDeduction, Dictionary<String, StatutoryDeductionDefault> cache)
        {
            if (cache.ContainsKey(statDeduction.ProvinceStateCode))
            {
                if (!statDeduction.PayEmploymentInsuranceFlagExistsFlag) statDeduction.PayEmploymentInsuranceFlag = cache[statDeduction.ProvinceStateCode].PayEmploymentInsuranceFlag;
                if (!statDeduction.PayCanadaPensionPlanFlagExistsFlag) statDeduction.PayCanadaPensionPlanFlag = cache[statDeduction.ProvinceStateCode].PayCanadaPensionPlanFlag;
                if (!statDeduction.ParentalInsurancePlanFlagExistsFlag) statDeduction.ParentalInsurancePlanFlag = cache[statDeduction.ProvinceStateCode].ParentalInsurancePlanFlag;
                if (!statDeduction.FederalTaxClaimExistsFlag) statDeduction.FederalTaxClaim = cache[statDeduction.ProvinceStateCode].FederalTaxClaim;
                if (!statDeduction.FederalAdditionalTaxExistsFlag) statDeduction.FederalAdditionalTax = cache[statDeduction.ProvinceStateCode].FederalAdditionalTax;
                if (!statDeduction.FederalDesignatedAreaDeductionExistsFlag) statDeduction.FederalDesignatedAreaDeduction = cache[statDeduction.ProvinceStateCode].FederalDesignatedAreaDeduction;
                if (!statDeduction.FederalAuthorizedAnnualDeductionExistsFlag) statDeduction.FederalAuthorizedAnnualDeduction = cache[statDeduction.ProvinceStateCode].FederalAuthorizedAnnualDeduction;
                if (!statDeduction.FederalAuthorizedAnnualTaxCreditExistsFlag) statDeduction.FederalAuthorizedAnnualTaxCredit = cache[statDeduction.ProvinceStateCode].FederalAuthorizedAnnualTaxCredit;
                if (!statDeduction.FederalLabourTaxCreditExistsFlag) statDeduction.FederalLabourTaxCredit = cache[statDeduction.ProvinceStateCode].FederalLabourTaxCredit;
                if (!statDeduction.FederalPayTaxFlagExistsFlag) statDeduction.FederalPayTaxFlag = cache[statDeduction.ProvinceStateCode].FederalPayTaxFlag;
                if (!statDeduction.ProvincialTaxClaimExistsFlag) statDeduction.ProvincialTaxClaim = cache[statDeduction.ProvinceStateCode].ProvincialTaxClaim;
                if (!statDeduction.ProvincialAdditionalTaxExistsFlag) statDeduction.ProvincialAdditionalTax = cache[statDeduction.ProvinceStateCode].ProvincialAdditionalTax;
                if (!statDeduction.ProvincialDesignatedAreaDeductionExistsFlag) statDeduction.ProvincialDesignatedAreaDeduction = cache[statDeduction.ProvinceStateCode].ProvincialDesignatedAreaDeduction;
                if (!statDeduction.ProvincialAuthorizedAnnualDeductionExistsFlag) statDeduction.ProvincialAuthorizedAnnualDeduction = cache[statDeduction.ProvinceStateCode].ProvincialAuthorizedAnnualDeduction;
                if (!statDeduction.ProvincialAuthorizedAnnualTaxCreditExistsFlag) statDeduction.ProvincialAuthorizedAnnualTaxCredit = cache[statDeduction.ProvinceStateCode].ProvincialAuthorizedAnnualTaxCredit;
                if (!statDeduction.ProvincialLabourTaxCreditExistsFlag) statDeduction.ProvincialLabourTaxCredit = cache[statDeduction.ProvinceStateCode].ProvincialLabourTaxCredit;
                if (!statDeduction.ProvincialPayTaxFlagExistsFlag) statDeduction.ProvincialPayTaxFlag = cache[statDeduction.ProvinceStateCode].ProvincialPayTaxFlag;
                if (!statDeduction.EstimatedAnnualIncomeExistsFlag) statDeduction.EstimatedAnnualIncome = cache[statDeduction.ProvinceStateCode].EstimatedAnnualIncome;
                if (!statDeduction.EstimatedAnnualExpenseExistsFlag) statDeduction.EstimatedAnnualExpense = cache[statDeduction.ProvinceStateCode].EstimatedAnnualExpense;
                if (!statDeduction.EstimatedNetIncomeExistsFlag) statDeduction.EstimatedNetIncome = cache[statDeduction.ProvinceStateCode].EstimatedNetIncome;
                if (!statDeduction.CommissionPercentageExistsFlag) statDeduction.CommissionPercentage = cache[statDeduction.ProvinceStateCode].CommissionPercentage;
            }
        }
        private void ImportStatutoryDeduction(DatabaseUser user, WorklinksStatutoryDeductionCollection existingEmployeeStatDeductions, WorklinksStatutoryDeductionCollection pendingEmployeeStatDeductions, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool autoAddSecondaryPositions)
        {
            //insert or update stat deductions for existing employees
            if (existingEmployeeStatDeductions.Count > 0)
                EmployeeAccess.ImportStatutoryDeduction(user, existingEmployeeStatDeductions);

            //update pending employee stat deductions or insert new stat deductions
            if (pendingEmployeeStatDeductions.Count > 0)
            {
                WizardCacheImportCollection collection = new WizardCacheImportCollection();
                int i = 0;

                foreach (WorklinksStatutoryDeduction statDeduction in pendingEmployeeStatDeductions)
                {
                    collection.Add(new WizardCacheImport
                    {
                        WizardCacheId = i--,
                        WizardId = 1, //1 = Add Employee Wizard
                        ImportExternalIdentifier = statDeduction.ImportExternalIdentifier,
                        WizardItemId = 7, //7 = StatutoryDeductionViewControl
                        Data = statDeduction.StatutoryDeductionCollection
                    });
                }

                WizardManagement.ImportWizardCache(user, collection);
                WizardManagement.ValidateAndHireEmployee(user, collection, autoGenerateEmployeeNumber, employeeNumberFormat, autoAddSecondaryPositions);
            }
        }
        #endregion

        #region employee statutory deduction defaults
        public StatutoryDeductionDefaultCollection GetStatutoryDeductionDefaults(DatabaseUser user, String codeProvinceStateCd)
        {
            try
            {
                return EmployeeAccess.GetStatutoryDeductionDefaults(user, codeProvinceStateCd);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetStatutoryDeductionDefaults", ex);
                throw;
            }
        }
        #endregion

        #region business number
        public long GetBusinessNumberIdFromEmployerNumber(DatabaseUser user, string employerNumber)
        {
            try
            {
                var businessNumbers = EmployeeAccess.GetBusinessNumber(user, null);
                var busNum = businessNumbers.FirstOrDefault(bn => bn.EmployerNumber.Equals(employerNumber, StringComparison.InvariantCultureIgnoreCase))?.BusinessNumberId;

                if (busNum == null)
                {
                    throw new Exception($"No business number found with tax number {employerNumber}");
                }

                return busNum.Value;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetBusinessNumber", ex);
                throw;
            }
        }

        public BusinessNumberCollection GetBusinessNumber(DatabaseUser user, long? businessNumberId)
        {
            try
            {
                return EmployeeAccess.GetBusinessNumber(user, businessNumberId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetBusinessNumber", ex);
                throw;
            }
        }
        public BusinessNumber InsertBusinessNumber(DatabaseUser user, BusinessNumber businessNumber)
        {
            try
            {
                return EmployeeAccess.InsertBusinessNumber(user, businessNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertBusinessNumber", ex);
                throw;
            }
        }
        public void UpdateBusinessNumber(DatabaseUser user, BusinessNumber businessNumber)
        {
            try
            {
                EmployeeAccess.UpdateBusinessNumber(user, businessNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateBusinessNumber", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteBusinessNumber(DatabaseUser user, BusinessNumber businessNumber)
        {
            try
            {
                EmployeeAccess.DeleteBusinessNumber(user, businessNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteBusinessNumber", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region employee employment information
        //public EmployeeEmploymentInformationCollection GetEmployeeEmploymentInformation(long id)
        //{
        //    try
        //    {
        //        return GetEmployeeEmploymentInformation(id);
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeEmploymentInformationNoOverrideArg", ex);
        //        throw;
        //    }
        //}
        public EmployeeEmploymentInformationCollection GetEmployeeEmploymentInformation(DatabaseUser user, long id)
        {
            try
            {
                return GetEmployeeEmploymentInformation(user, id, false);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeEmploymentInformationNoOverrideArg", ex);
                throw;
            }
        }
        public EmployeeEmploymentInformationCollection GetEmployeeEmploymentInformation(DatabaseUser user, long id, bool securityOverrideFlag)
        {
            try
            {
                EmployeeEmploymentInformationCollection collection = EmployeeAccess.GetEmployeeEmploymentInformation(user, id, securityOverrideFlag);

                foreach (EmployeeEmploymentInformation item in collection)
                {
                    //only load the EmployeeCustomFields collection if custom fields exist
                    EmployeeCustomFieldConfigCollection customFieldConfigCollection = EmployeeAccess.GetEmployeeCustomFieldConfig(user, null);

                    if (customFieldConfigCollection != null && customFieldConfigCollection.Count > 0)
                        item.EmployeeCustomFields = GetEmployeeCustomField(user, item.EmployeeId);
                }

                return collection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeEmploymentInformationSecurityOverrideArg", ex);
                throw;
            }
        }
        public WorkLinksEmployeeEmploymentInformationCollection GetEmployeeEmploymentInformationBatch(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            try
            {
                return EmployeeAccess.GetEmployeeEmploymentInformationBatch(user, importExternalIdentifiers);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WizardManagement - GetEmployeeEmploymentInformationBatch", ex);
                throw;
            }
        }
        public Dictionary<String, EmployeeEmploymentInformation> GetEmploymentInformationCache(DatabaseUser user, List<String> importExternalIdentifiers)
        {
            Dictionary<String, EmployeeEmploymentInformation> employmentCache = new Dictionary<String, EmployeeEmploymentInformation>();

            //get the existing employee employment information from the database and add them to the dictionary
            foreach (WorkLinksEmployeeEmploymentInformation employment in GetEmployeeEmploymentInformationBatch(user, importExternalIdentifiers))
                employmentCache.Add(employment.ImportExternalIdentifier, employment);

            return employmentCache;
        }
        public void UpdateEmployeeEmploymentInformation(DatabaseUser user, EmployeeEmploymentInformation item)
        {
            try
            {
                if (item.EmployeeEmploymentInformationId < 0)
                    EmployeeAccess.InsertEmployeeEmploymentInformation(user, item);
                else
                    EmployeeAccess.UpdateEmployeeEmploymentInformation(user, item);

                if (item.EmployeeCustomFields != null && item.EmployeeCustomFields.Count > 0)
                {
                    if (item.EmployeeCustomFields[0].EmployeeCustomFieldId < 0)
                        InsertEmployeeCustomField(user, item.EmployeeCustomFields[0]);
                    else
                        UpdateEmployeeCustomField(user, item.EmployeeCustomFields[0]);
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeeEmploymentInformation", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeeEmploymentInformation(DatabaseUser user, EmployeeEmploymentInformation item)
        {
            try
            {
                EmployeeAccess.DeleteEmployeeEmploymentInformation(user, item);

                if (item.EmployeeCustomFields != null && item.EmployeeCustomFields.Count > 0)
                    DeleteEmployeeCustomField(user, item.EmployeeCustomFields[0]);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeEmploymentInformation", ex);
                throw;
            }
        }
        public void InsertEmployeeEmploymentInformation(DatabaseUser user, EmployeeEmploymentInformation item)
        {
            try
            {
                EmployeeAccess.InsertEmployeeEmploymentInformation(user, item);

                if (item.EmployeeCustomFields != null && item.EmployeeCustomFields.Count > 0)
                {
                    item.EmployeeCustomFields[0].EmployeeId = item.EmployeeId;
                    InsertEmployeeCustomField(user, item.EmployeeCustomFields[0]);
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeEmploymentInformation", ex);
                throw;
            }
        }
        #endregion

        #region employment import
        private List<String> GetImportExternalIdentifiersFromEmployment(DatabaseUser user, WorkLinksEmployeeEmploymentInformation[] xsdObject)
        {
            List<String> importExternalIdentifiers = new List<String>();

            foreach (WorkLinksEmployeeEmploymentInformation employment in xsdObject)
                importExternalIdentifiers.Add(employment.ImportExternalIdentifier);

            return importExternalIdentifiers;
        }
        private String EmploymentDecodeCode(DatabaseUser user, WorkLinksEmployeeEmploymentInformation employment)
        {
            String validationMessage = null;
            StringBuilder output = new StringBuilder();

            try
            {
                employment.PayrollProcessGroupCode = CodeManagement.DecodeCode(user, CodeTableType.PayrollProcessGroupCode, employment.PayrollProcessGroupCodeExternalIdentifier);
            }
            catch (FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.CodeMismatch)
                    output.AppendLine(ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            try
            {
                employment.WSIBCode = CodeManagement.DecodeCode(user, CodeTableType.WSIBCode, employment.WsibCodeExternalIdentifier);
            }
            catch (FaultException<EmployeeServiceException> ex)
            {
                if (ex.Detail.ExceptionCode == EmployeeServiceException.ExceptionCodes.CodeMismatch)
                    output.AppendLine(ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (output.Length > 0)
                validationMessage += output.ToString();

            return validationMessage;
        }
        public void ProcessEmployment(DatabaseUser user, WorkLinksEmployeeEmploymentInformation[] xsdObject, ImportExportLog log, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions)
        {
            Dictionary<String, Employee> employeeCache = GetEmployeeCache(user, xsdObject);
            ProcessEmployment(user, employeeCache, xsdObject, log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);
        }
        public void ProcessEmployment(DatabaseUser user, Dictionary<String, Employee> employeeCache, WorkLinksEmployeeEmploymentInformation[] xsdObject, ImportExportLog log, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions)
        {
            try
            {
                StringBuilder output = new StringBuilder();
                int failedRows = 0;
                int successfulRows = 0;

                List<String> importExternalIdentifiers = GetImportExternalIdentifiersFromEmployment(user, xsdObject);
                Dictionary<String, WizardCache> wizardCache = WizardManagement.GetWizardCache(user, importExternalIdentifiers);

                //we only need this for existing employees
                Dictionary<String, EmployeeEmploymentInformation> employmentCache = null;

                if (employeeCache.Count > 0)
                    employmentCache = GetEmploymentInformationCache(user, importExternalIdentifiers);

                using (TransactionScope scope = new TransactionScope())
                {
                    int i = 0;

                    WorkLinksEmployeeEmploymentInformationCollection existingEmployeeEmployment = new WorkLinksEmployeeEmploymentInformationCollection();
                    WorkLinksEmployeeEmploymentInformationCollection pendingEmployeeEmployment = new WorkLinksEmployeeEmploymentInformationCollection();

                    foreach (WorkLinksEmployeeEmploymentInformation employment in xsdObject)
                    {
                        String decodeCodeValidationMessage = EmploymentDecodeCode(user, employment);

                        if (employment.WSIBCode == null) //if the WSIB code is missing, default it to "EXEMPT"
                            employment.WSIBCode = "EXEMPT";

                        if (employeeCache.ContainsKey(employment.ImportExternalIdentifier)) //employee was found in the database
                        {
                            SetEmployeeEmploymentInformation(employment, employmentCache);

                            if (employment.MeetsEmploymentRequirements(decodeCodeValidationMessage))
                            {
                                successfulRows++;

                                employment.EmployeeEmploymentInformationId = i--;
                                existingEmployeeEmployment.Add(employment);
                            }
                            else
                            {
                                failedRows++;
                                output.Append(employment.GetValidationMessage(decodeCodeValidationMessage));
                            }
                        }
                        else
                        {
                            if (wizardCache.ContainsKey(employment.ImportExternalIdentifier)) //employee is pending
                            {
                                if (employment.MeetsEmploymentRequirements(decodeCodeValidationMessage, false))
                                {
                                    successfulRows++;

                                    WizardCache positionCache = wizardCache[employment.ImportExternalIdentifier];
                                    EmployeePositionCollection positionCollection = null;

                                    if (positionCache.Items[positionCache.WizardCacheId.ToString() + '_' + WizardCache.ItemType.EmployeePositionViewControl] != null)
                                        positionCollection = ((EmployeePositionCollection)positionCache.Items[positionCache.WizardCacheId.ToString() + '_' + WizardCache.ItemType.EmployeePositionViewControl].Data);

                                    //set the HireDate to the EffectiveDate from the employee position record
                                    if (positionCollection != null)
                                        employment.HireDate = positionCollection[0].EffectiveDate;

                                    employment.EmployeeEmploymentInformationId = i--;
                                    pendingEmployeeEmployment.Add(employment);
                                }
                                else
                                {
                                    failedRows++;
                                    output.Append(employment.GetValidationMessage(decodeCodeValidationMessage, false));
                                }
                            }
                            else //employee is missing
                            {
                                failedRows++;
                                output.Append(String.Format(GuiManagement.GetResourceByLanguageAndKey(user, "ErrorMessages", "MissingEmployee"), employment.ImportExternalIdentifier) + "\r\n");
                            }
                        }
                    }

                    ImportEmployeeEmploymentInformation(user, existingEmployeeEmployment, pendingEmployeeEmployment, autoGenerateEmployeeNumber, employeeNumberFormat, autoAddSecondaryPositions);

                    if (importDirtySave || failedRows == 0)
                        scope.Complete();
                }

                output.AppendLine("----------");
                output.AppendLine("# rows total: " + (successfulRows + failedRows));
                output.AppendLine("# rows succeeded: " + successfulRows);
                output.AppendLine("# rows failed: " + failedRows);

                if (failedRows > 0)
                {
                    if (importDirtySave)
                        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n" + "----------\r\n");
                    else
                        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportCompleteProcessFailed") + "\r\n" + "----------\r\n");
                }
                else
                    output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n");

                log.SuccessFlag = true;
                log.ProcessingOutput += output.ToString();
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - ProcessEmployment", ex);
                throw;
            }
        }
        private void SetEmployeeEmploymentInformation(WorkLinksEmployeeEmploymentInformation employment, Dictionary<String, EmployeeEmploymentInformation> employmentCache)
        {
            if (employmentCache.ContainsKey(employment.ImportExternalIdentifier))
            {
                employment.EmployeeId = employmentCache[employment.ImportExternalIdentifier].EmployeeId;
                employment.EmployeeEmploymentInformationId = employmentCache[employment.ImportExternalIdentifier].EmployeeEmploymentInformationId;
                employment.HireDate = employmentCache[employment.ImportExternalIdentifier].HireDate;
                employment.TerminationDate = employmentCache[employment.ImportExternalIdentifier].TerminationDate;
                employment.RehireDate = employmentCache[employment.ImportExternalIdentifier].RehireDate;
                employment.LastReviewDate = employmentCache[employment.ImportExternalIdentifier].LastReviewDate;
                employment.NextReviewDate = employmentCache[employment.ImportExternalIdentifier].NextReviewDate;
                employment.IncreaseDate = employmentCache[employment.ImportExternalIdentifier].IncreaseDate;
                employment.LunchDuration = employmentCache[employment.ImportExternalIdentifier].LunchDuration;
                employment.DaysPerWeek = employmentCache[employment.ImportExternalIdentifier].DaysPerWeek;
                employment.WSIBCode = employmentCache[employment.ImportExternalIdentifier].WSIBCode;
            }
        }
        private void ImportEmployeeEmploymentInformation(DatabaseUser user, WorkLinksEmployeeEmploymentInformationCollection existingEmployeeEmployment, WorkLinksEmployeeEmploymentInformationCollection pendingEmployeeEmployment, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool autoAddSecondaryPositions)
        {
            //update existing employee employment information
            if (existingEmployeeEmployment.Count > 0)
                EmployeeAccess.ImportEmployeeEmploymentInformation(user, existingEmployeeEmployment);

            //update pending employee employment information or insert new employee employment information
            if (pendingEmployeeEmployment.Count > 0)
            {
                WizardCacheImportCollection collection = new WizardCacheImportCollection();
                int i = 0;

                foreach (WorkLinksEmployeeEmploymentInformation employment in pendingEmployeeEmployment)
                {
                    collection.Add(new WizardCacheImport
                    {
                        WizardCacheId = i--,
                        WizardId = 1, //1 = Add Employee Wizard
                        ImportExternalIdentifier = employment.ImportExternalIdentifier,
                        WizardItemId = 5, //5 = EmployeeEmploymentInformationControl
                        Data = employment.EmployeeEmploymentInformationCollection
                    });
                }

                WizardManagement.ImportWizardCache(user, collection);
                WizardManagement.ValidateAndHireEmployee(user, collection, autoGenerateEmployeeNumber, employeeNumberFormat, autoAddSecondaryPositions);
            }
        }
        #endregion

        #region vendor
        public VendorCollection GetVendor(DatabaseUser user, long VendorId)
        {
            try
            {
                VendorCollection tempCollection = new VendorCollection();
                tempCollection = EmployeeAccess.GetVendor(user, VendorId);

                //gather the addresses into our vendor collection
                foreach (Vendor venderObj in tempCollection)
                    venderObj.Address = AddressAccess.GetAddress(user, venderObj.AddressId)[0];

                return tempCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetVendor", ex);
                throw;
            }
        }
        public void UpdateVendor(DatabaseUser user, Vendor vendor)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //update the vendor objects and then the address objects
                    EmployeeAccess.UpdateVendor(user, vendor);
                    AddressAccess.UpdateAddress(user, vendor.Address);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateVendor", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public Vendor InsertVendor(DatabaseUser user, Vendor vendor)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //copy the update user from the vendor object to the address object inside it
                    vendor.Address.UpdateUser = vendor.UpdateUser;

                    AddressAccess.InsertAddress(user, vendor.Address);
                    EmployeeAccess.InsertVendor(user, vendor);

                    scope.Complete();
                }

                return vendor;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertVendor", ex);
                throw;
            }
        }
        public void DeleteVendor(DatabaseUser user, Vendor vendor)
        {
            try
            {
                EmployeeAccess.DeleteVendor(user, vendor);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteVendor", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region year end adjustments search
        public YearEndAdjustmentsSearchReportCollection GetYearEndAdjustmentsSearchReport(DatabaseUser user, YearEndAdjustmentsSearchCriteria criteria)
        {
            try
            {
                return YearEndAccess.GetYearEndAdjustmentsSearchReport(user, criteria);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetYearEndAdjustmentsSearchReport", ex);
                throw;
            }
        }
        #endregion

        #region year end
        public YearEndCollection GetYearEnd(DatabaseUser user, long? year)
        {
            try
            {
                return YearEndAccess.GetYearEnd(user, year);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetYearEnd", ex);
                throw;
            }
        }
        public void CreateCurrentYE(DatabaseUser user)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                //1. insert row into year_end with status='OPEN'.  Proc will determine current year
                YearEnd yearEndObj = new YearEnd() { CodeYearEndStatusCd = "OPEN", UpdateUser = user.UserName };
                YearEndAccess.InsertYearEndRecord(user, yearEndObj);

                //2. all open (unprocessed) payroll period records must have their year/periods reset for next year
                foreach (PayrollPeriod period in PayrollAccess.GetPayrollPeriod(user, new PayrollPeriodCriteria() { GetMinOpenPeriodFlag = true }))
                {
                    period.UpdateDatetime = DateTime.Now;
                    period.UpdateUser = user.UserName;
                    period.PeriodYear = yearEndObj.Year + 1;
                    period.Period = 1;

                    PayrollAccess.UpdatePayrollPeriod(user, period);
                }

                //populate year end tables
                YearEndAccess.PopulateYearEndTables(user, yearEndObj.Year);

                scope.Complete();
            }
        }
        public void CloseVersionYE(DatabaseUser user, YearEnd yearEnd)
        {
            try
            {
                YearEndAccess.UpdateYearEndRecord(user, yearEnd);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - CloseVersionYE", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void CloseCurrentYE(DatabaseUser user, YearEnd yearEnd)
        {
            try
            {
                yearEnd.CodeYearEndStatusCd = "CMP";
                YearEndAccess.UpdateYearEndRecord(user, yearEnd);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - CloseCurrentYE", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region get year end negative amount employee
        public Employee GetYearEndNegativeAmountEmployee(DatabaseUser user, Decimal year, int revision)
        {
            long? employeeId = EmployeeAccess.GetYearEndNegativeAmountEmployee(user, year, revision);

            if (employeeId != null && employeeId > 0)
            {
                EmployeeCollection employees = GetEmployee(user, new EmployeeCriteria() { EmployeeId = employeeId, SecurityOverrideFlag = true });
                Employee employee = employees[0];

                return employee;
            }
            else
                return null;
        }
        #endregion

        #region employee photo
        public EmployeePhotoCollection GetEmployeePhoto(DatabaseUser user, long employeeId)
        {
            try
            {
                return EmployeeAccess.GetEmployeePhoto(user, employeeId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeePhoto", ex);
                throw;
            }
        }
        public EmployeePhoto InsertEmployeePhoto(DatabaseUser user, EmployeePhoto photo)
        {
            try
            {
                return EmployeeAccess.InsertEmployeePhoto(user, photo);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeePhoto", ex);
                throw;
            }
        }
        public void UpdateEmployeePhoto(DatabaseUser user, EmployeePhoto photo)
        {
            try
            {
                if (photo.EmployeePhotoId < 0)
                    EmployeeAccess.InsertEmployeePhoto(user, photo);
                else
                    EmployeeAccess.UpdateEmployeePhoto(user, photo);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeePhoto", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeePhoto(DatabaseUser user, EmployeePhoto photo)
        {
            try
            {
                EmployeeAccess.DeleteEmployeePhoto(user, photo);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeePhoto", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region employee employment equity
        public EmployeeEmploymentEquityCollection GetEmployeeEmploymentEquity(DatabaseUser user, long employeeId)
        {
            try
            {
                return EmployeeAccess.GetEmployeeEmploymentEquity(user, employeeId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeEmploymentEquity", ex);
                throw;
            }
        }
        public EmployeeEmploymentEquity InsertEmployeeEmploymentEquity(DatabaseUser user, EmployeeEmploymentEquity employmentEquity)
        {
            try
            {
                return EmployeeAccess.InsertEmployeeEmploymentEquity(user, employmentEquity);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeEmploymentEquity", ex);
                throw;
            }
        }
        public void UpdateEmployeeEmploymentEquity(DatabaseUser user, EmployeeEmploymentEquity employmentEquity)
        {
            try
            {
                EmployeeAccess.UpdateEmployeeEmploymentEquity(user, employmentEquity);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeeEmploymentEquity", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeeEmploymentEquity(DatabaseUser user, long employeeId)
        {
            try
            {
                EmployeeAccess.DeleteEmployeeEmploymentEquity(user, employeeId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeEmploymentEquity", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region attachment
        public AttachmentCollection GetAttachment(DatabaseUser user, long attachmentId)
        {
            try
            {
                return EmployeeAccess.GetAttachment(user, attachmentId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetAttachment", ex);
                throw;
            }
        }
        public Attachment InsertAttachment(DatabaseUser user, Attachment attachment)
        {
            try
            {
                return EmployeeAccess.InsertAttachment(user, attachment);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertAttachment", ex);
                throw;
            }
        }
        public void UpdateAttachment(DatabaseUser user, Attachment attachment)
        {
            try
            {
                if (attachment.AttachmentId < 0)
                    InsertAttachment(user, attachment);
                else
                    EmployeeAccess.UpdateAttachment(user, attachment);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateAttachment", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteAttachment(DatabaseUser user, Attachment attachment)
        {
            try
            {
                EmployeeAccess.DeleteAttachment(user, attachment);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteAttachment", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region employee remittance stub
        public EmployeeRemittanceStubCollection GetEmployeeRemittanceStub(DatabaseUser user, long employeePaycodeId)
        {
            try
            {
                return EmployeeAccess.GetEmployeeRemittanceStub(user, employeePaycodeId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeRemittanceStub", ex);
                throw;
            }
        }
        public EmployeeRemittanceStub InsertEmployeeRemittanceStub(DatabaseUser user, EmployeeRemittanceStub stub)
        {
            try
            {
                return EmployeeAccess.InsertEmployeeRemittanceStub(user, stub);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeRemittanceStub", ex);
                throw;
            }
        }
        public void UpdateEmployeeRemittanceStub(DatabaseUser user, EmployeeRemittanceStub stub)
        {
            try
            {
                EmployeeAccess.UpdateEmployeeRemittanceStub(user, stub);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeeRemittanceStub", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeeRemittanceStub(DatabaseUser user, EmployeeRemittanceStub stub)
        {
            try
            {
                EmployeeAccess.DeleteEmployeeRemittanceStub(user, stub);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeRemittanceStub", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region global employee paycode
        public EmployeePaycodeCollection GetGlobalEmployeePaycode(DatabaseUser user, long? globalEmployeePaycodeId)
        {
            try
            {
                return EmployeeAccess.GetGlobalEmployeePaycode(user, globalEmployeePaycodeId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetGlobalEmployeePaycode", ex);
                throw;
            }
        }
        public EmployeePaycode InsertGlobalEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            try
            {
                return EmployeeAccess.InsertGlobalEmployeePaycode(user, employeePaycode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertGlobalEmployeePaycode", ex);
                throw;
            }
        }
        public void UpdateGlobalEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            try
            {
                EmployeeAccess.UpdateGlobalEmployeePaycode(user, employeePaycode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateGlobalEmployeePaycode", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteGlobalEmployeePaycode(DatabaseUser user, EmployeePaycode employeePaycode)
        {
            try
            {
                EmployeeAccess.DeleteGlobalEmployeePaycode(user, employeePaycode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteGlobalEmployeePaycode", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region statutory holiday date
        public StatutoryHolidayDateCollection GetStatutoryHolidayDateReport(DatabaseUser user, StatutoryDeductionCriteria criteria)
        {
            try
            {
                return EmployeeAccess.GetStatutoryHolidayDateReport(user, criteria);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetStatutoryHolidayDateReport", ex);
                throw;
            }
        }
        public StatutoryHolidayDateCollection GetStatutoryHolidayDate(DatabaseUser user, long? statutoryHolidayDateId)
        {
            try
            {
                return EmployeeAccess.GetStatutoryHolidayDate(user, statutoryHolidayDateId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetStatutoryHolidayDate", ex);
                throw;
            }
        }
        public StatutoryHolidayDate InsertStatutoryHolidayDate(DatabaseUser user, StatutoryHolidayDate statutoryHolidayDate)
        {
            try
            {
                return EmployeeAccess.InsertStatutoryHolidayDate(user, statutoryHolidayDate);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertStatutoryHolidayDate", ex);
                throw;
            }
        }
        public void UpdateStatutoryHolidayDate(DatabaseUser user, StatutoryHolidayDate statutoryHolidayDate)
        {
            try
            {
                EmployeeAccess.UpdateStatutoryHolidayDate(user, statutoryHolidayDate);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateStatutoryHolidayDate", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteStatutoryHolidayDate(DatabaseUser user, StatutoryHolidayDate statutoryHolidayDate)
        {
            try
            {
                EmployeeAccess.DeleteStatutoryHolidayDate(user, statutoryHolidayDate);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteStatutoryHolidayDate", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region employee custom field config
        public EmployeeCustomFieldConfigCollection GetEmployeeCustomFieldConfig(DatabaseUser user, long? employeeCustomFieldConfigId)
        {
            try
            {
                return EmployeeAccess.GetEmployeeCustomFieldConfig(user, employeeCustomFieldConfigId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeCustomFieldConfig", ex);
                throw;
            }
        }
        public EmployeeCustomFieldConfig InsertEmployeeCustomFieldConfig(DatabaseUser user, EmployeeCustomFieldConfig item)
        {
            try
            {
                return EmployeeAccess.InsertEmployeeCustomFieldConfig(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeCustomFieldConfig", ex);
                throw;
            }
        }
        public void UpdateEmployeeCustomFieldConfig(DatabaseUser user, EmployeeCustomFieldConfig item)
        {
            try
            {
                EmployeeAccess.UpdateEmployeeCustomFieldConfig(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeeCustomFieldConfig", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeeCustomFieldConfig(DatabaseUser user, EmployeeCustomFieldConfig item)
        {
            try
            {
                EmployeeAccess.DeleteEmployeeCustomFieldConfig(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeCustomFieldConfig", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region employee custom field
        public EmployeeCustomFieldCollection GetEmployeeCustomField(DatabaseUser user, long? employeeId)
        {
            try
            {
                return EmployeeAccess.GetEmployeeCustomField(user, employeeId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeCustomField", ex);
                throw;
            }
        }
        public EmployeeCustomField InsertEmployeeCustomField(DatabaseUser user, EmployeeCustomField item)
        {
            try
            {
                return EmployeeAccess.InsertEmployeeCustomField(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeCustomField", ex);
                throw;
            }
        }
        public void UpdateEmployeeCustomField(DatabaseUser user, EmployeeCustomField item)
        {
            try
            {
                EmployeeAccess.UpdateEmployeeCustomField(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeeCustomField", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteEmployeeCustomField(DatabaseUser user, EmployeeCustomField item)
        {
            try
            {
                EmployeeAccess.DeleteEmployeeCustomField(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeCustomField", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion


        #region salary plan grade and effective date detail
        public SalaryPlanGradeAndEffectiveDetailCollection GetSalaryPlanGradeAndEffectiveDetail(DatabaseUser user, long salaryPlanId)
        {
            try
            {
                return EmployeeAccess.GetSalaryPlanGradeAndEffectiveDetail(user, salaryPlanId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetSalaryPlanGradeAndEffectiveDetail", ex);
                throw;
            }
        }

        public SalaryPlanGradeMinEffectiveDateCollection SelectMinEffectiveDate(DatabaseUser user, long organizationUnitId)
        {
            try
            {
                return EmployeeAccess.SelectMinEffectiveDate(user, organizationUnitId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - SelectMinEffectiveDate", ex);
                throw;
            }
        }

        public void AddNewSalaryPlanGradeAndSteps(DatabaseUser user, SalaryPlanGradeAndEffectiveDetail item)
        {
            try
            {
                EmployeeAccess.AddNewSalaryPlanGradeAndSteps(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - AddNewSalaryPlanGradeAndSteps", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }

        #endregion


        #region salary plan
        public SalaryPlanCollection GetSalaryPlan(DatabaseUser user, long? salaryPlanId)
        {
            try
            {
                return EmployeeAccess.GetSalaryPlan(user, salaryPlanId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetSalaryPlan", ex);
                throw;
            }
        }
        public SalaryPlanOrganizationUnitCollection GetSalaryPlanOrganizationUnit(DatabaseUser user, string organizationUnit)
        {
            try
            {
                return EmployeeAccess.GetSalaryPlanOrganizationUnit(user, organizationUnit);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetSalaryPlanOrganizationUnit", ex);
                throw;
            }
        }
        #endregion

        #region salary plan grade
        public SalaryPlanGradeCollection GetSalaryPlanGrade(DatabaseUser user, long? salaryPlanGradeId, long? salaryPlanId, string languageCode)
        {
            try
            {
                return EmployeeAccess.GetSalaryPlanGrade(user, salaryPlanGradeId, salaryPlanId, languageCode);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetSalaryPlanGrade", ex);
                throw;
            }
        }

        public SalaryPlanGradeOrgUnitDescriptionComboCollection PopulateComboBoxWithSalaryPlanGrades(DatabaseUser user, long salaryPlanId, long organizationUnitLevelId)
        {
            try
            {
                return EmployeeAccess.PopulateComboBoxWithSalaryPlanGrades(user, salaryPlanId, organizationUnitLevelId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - SelectSalaryPlanGradePopulateNameComboRemovingOrgsInUse", ex);
                throw;
            }
        }


        #endregion

        #region salary plan grade step
        public SalaryPlanGradeStepCollection GetSalaryPlanGradeStep(DatabaseUser user, long? salaryPlanGradeStepId, long? salaryPlanGradeId)
        {
            try
            {
                return EmployeeAccess.GetSalaryPlanGradeStep(user, salaryPlanGradeStepId, salaryPlanGradeId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetSalaryPlanGradeStep", ex);
                throw;
            }
        }
        public SalaryPlanGradeStepDetailCollection GetSalaryPlanGradeStepDetail(DatabaseUser user, long? salaryPlanGradeStepDetailId, long? salaryPlanGradeStepId)
        {
            try
            {
                return EmployeeAccess.GetSalaryPlanGradeStepDetail(user, salaryPlanGradeStepDetailId, salaryPlanGradeStepId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetSalaryPlanGradeStepDetail", ex);
                throw;
            }
        }
        #endregion

        #region salary plan grade accumulation total
        public SalaryPlanGradeAccumulationTotalsCollection GetSalaryPlanGradeAccumulationTotals(DatabaseUser user, long employeeId)
        {
            try
            {
                return EmployeeAccess.GetSalaryPlanGradeAccumulationTotals(user, employeeId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetSalaryPlanGradeAccumulationTotals", ex);
                throw;
            }
        }
        #endregion

        #region employee benefit
        public EmployeeBenefitCollection GetEmployeeBenefit(DatabaseUser user, long employeeId)
        {
            try
            {
                return EmployeeAccess.GetEmployeeBenefit(user, employeeId, false);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeBenefit-NoOverrideArg", ex);
                throw;
            }
        }

        public EmployeeBenefitCollection GetEmployeeBenefit(DatabaseUser user, long employeeId, bool securityOverrideFlag)
        {
            try
            {
                return EmployeeAccess.GetEmployeeBenefit(user, employeeId, securityOverrideFlag);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeBenefit-OverrideArg", ex);
                throw;
            }
        }

        public EmployeeBenefitCollection GetEmployeeBenefit(DatabaseUser user, long employeeId, long? employeeBenefitId)
        {
            try
            {
                return EmployeeAccess.GetEmployeeBenefit(user, employeeId, employeeBenefitId, false);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeBenefit-EmployeeBenefitId-NoOverrideArg", ex);
                throw;
            }
        }

        public EmployeeBenefitCollection GetEmployeeBenefit(DatabaseUser user, long employeeId, long? employeeBenefitId, bool securityOverrideFlag)
        {
            try
            {
                return EmployeeAccess.GetEmployeeBenefit(user, employeeId, employeeBenefitId, securityOverrideFlag);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetEmployeeBenefit-EmployeeBenefitId-OverrideArg", ex);
                throw;
            }
        }

        public void UpdateEmployeeBenefit(DatabaseUser user, EmployeeBenefit employeeBenefit)
        {
            try
            {
                EmployeeAccess.UpdateEmployeeBenefit(user, employeeBenefit);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateEmployeeBenefit", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }

        public void DeleteEmployeeBenefit(DatabaseUser user, EmployeeBenefit employeeBenefit)
        {
            try
            {
                EmployeeAccess.DeleteEmployeeBenefit(user, employeeBenefit);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteEmployeeBenefit", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }

        public EmployeeBenefit InsertEmployeeBenefit(DatabaseUser user, EmployeeBenefit employeeBenefit)
        {
            try
            {
                return EmployeeAccess.InsertEmployeeBenefit(user, employeeBenefit);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertEmployeeBenefit", ex);
                throw;
            }
        }
        #endregion

        public SalaryPlanGradeRuleCollection GetSalaryPlanGradeRules(DatabaseUser user)
        {
            try
            {
                return SalaryPlanAccess.GetSalaryPlanGradeRules(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetSalaryPlanGradeRules", ex);
                throw;
            }
        }

        public void UpdatePlanGradeRule(DatabaseUser user, SalaryPlanGradeRule item)
        {
            try
            {
                SalaryPlanAccess.UpdatePlanGradeRule(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdatePlanGradeRule", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 2601)
                    {
                        EmployeeServiceException uniqueIndexFault = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.UniqueIndexFault);
                        throw new FaultException<EmployeeServiceException>(uniqueIndexFault, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }

        public SalaryPlanGradeRule InsertPlanGradeRule(DatabaseUser user, SalaryPlanGradeRule item)
        {
            try
            {
                return SalaryPlanAccess.InsertPlanGradeRule(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertPlanGradeRule", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 2601)
                    {
                        EmployeeServiceException uniqueIndexFault = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.UniqueIndexFault);
                        throw new FaultException<EmployeeServiceException>(uniqueIndexFault, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }

        public SalaryPlanGradeRuleCodeCollection GetSalaryPlanGradeRuleCodes(DatabaseUser user)
        {
            try
            {
                return SalaryPlanAccess.GetSalaryPlanGradeRuleCodes(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - GetSalaryPlanGradeRuleCodes", ex);
                throw;
            }
        }

        public SalaryPlanGradeRuleCode InsertGradeRuleCode(DatabaseUser user, SalaryPlanGradeRuleCode code)
        {
            try
            {
                return SalaryPlanAccess.InsertGradeRuleCode(user, code);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - InsertGradeRuleCode", ex);
                throw;
            }
        }

        public SalaryPlanGradeRuleCode UpdateGradeRuleCode(DatabaseUser user, SalaryPlanGradeRuleCode code)
        {
            try
            {
                return SalaryPlanAccess.UpdateGradeRuleCode(user, code);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - UpdateGradeRuleCode", ex);
                throw;
            }
        }

        public void DeleteGradeRuleCode(DatabaseUser user, SalaryPlanGradeRuleCode code)
        {
            try
            {
                SalaryPlanAccess.DeleteGradeRuleCode(user, code);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "EmployeeManagement - DeleteGradeRuleCode", ex);
                throw;
            }
        }
    }
}
 