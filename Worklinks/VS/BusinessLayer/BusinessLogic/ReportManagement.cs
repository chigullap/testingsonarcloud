﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel;
using System.Web.Script.Serialization;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.DataLayer.DataAccess;
using WorkLinks.DataLayer.DataAccess.SqlServer;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class ReportManagement
    {
        #region fields
        ReportAccess _reportAccess = null;
        PayrollAccess _payrollAccess = null;
        EmployeeAccess _employeeAccess = null;
        DataLayer.DataAccess.Dynamics.DynamicsAccess _dynamicsAccess = null;
        //DataLayer.DataAccess.Test.TestReportAccess _testReportAccess = null;
        WorkLinksDynamicsManagement _worklinksDynamicsManagement = null;
        YearEndManagement _yearEndManagement = null;
        #endregion

        #region properties
        private ReportAccess ReportAccess
        {
            get
            {
                if (_reportAccess == null)
                    _reportAccess = new ReportAccess();

                return _reportAccess;
            }
        }
        private PayrollAccess PayrollAccess
        {
            get
            {
                if (_payrollAccess == null)
                    _payrollAccess = new PayrollAccess();

                return _payrollAccess;
            }
        }
        private EmployeeAccess EmployeeAccess
        {
            get
            {
                if (_employeeAccess == null)
                    _employeeAccess = new EmployeeAccess();

                return _employeeAccess;
            }
        }
        //private DataLayer.DataAccess.Test.TestReportAccess TestReportAccess
        //{
        //    get
        //    {
        //        if (_testReportAccess == null)
        //            _testReportAccess = new DataLayer.DataAccess.Test.TestReportAccess();

        //        return _testReportAccess;
        //    }
        //}
        private DataLayer.DataAccess.Dynamics.DynamicsAccess DynamicsAccess
        {
            get
            {
                if (_dynamicsAccess == null)
                    _dynamicsAccess = new DataLayer.DataAccess.Dynamics.DynamicsAccess();

                return _dynamicsAccess;
            }
        }
        public WorkLinksDynamicsManagement WorklinksDynamicsManagement
        {
            get
            {
                if (_worklinksDynamicsManagement == null)
                    _worklinksDynamicsManagement = new WorkLinksDynamicsManagement();

                return _worklinksDynamicsManagement;
            }
        }
        private YearEndManagement YearEndManagement
        {
            get
            {
                if (_yearEndManagement == null)
                    _yearEndManagement = new YearEndManagement();

                return _yearEndManagement;
            }
        }
        #endregion

        #region reports
        public byte[] GetReport(DatabaseUser user, String reportName, String[] parms, String reportType)
        {
            try
            {
                //get report template
                Report report = ReportAccess.GetReport(user, reportName)[0];
                report.Parameters = ReportAccess.GetReportParameter(user, report.ReportId);

                //build report
                Microsoft.Reporting.WebForms.ReportViewer viewer = new Microsoft.Reporting.WebForms.ReportViewer();
                viewer.ServerReport.ReportServerUrl = new Uri(report.ReportServerUrl);
                viewer.ServerReport.ReportPath = report.ReportPath;
                List<Microsoft.Reporting.WebForms.ReportParameter> reportServerParms = new List<Microsoft.Reporting.WebForms.ReportParameter>();

                //build parms for saving
                int i = 0;
                int key = 0;
                ReportInstance reportInstance = new ReportInstance() { UpdateUser = user.UserName, UpdateDatetime = DateTime.Now, ReportId = report.ReportId };

                foreach (ReportParameter parameter in report.Parameters)
                {
                    ReportInstanceParameter processParm = new ReportInstanceParameter(parameter) { ReportInstanceParameterId = --key, UpdateUser = user.UserName, UpdateDatetime = DateTime.Now };
                    processParm.ParameterValue = parms[i++];
                    reportInstance.Parameters.Add(processParm);
                }

                //build report server parms
                foreach (ReportInstanceParameter parameter in reportInstance.Parameters)
                    reportServerParms.Add(new Microsoft.Reporting.WebForms.ReportParameter(parameter.ParameterName, parameter.ParameterValue));

                viewer.ServerReport.SetParameters(reportServerParms);

                if (user.LanguageCode == "FR")
                    System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("fr-CA");

                reportInstance.Data = viewer.ServerReport.Render(reportType);

                if (user.LanguageCode == "FR")
                    System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");

                return reportInstance.Data;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GetReport", ex);
                throw;
            }
        }
        //this is for reports driven off the Remittance screen.  These dont have the usual payroll variables so they have their own method
        public byte[] GetRemittanceReport(DatabaseUser user, string reportName, string detailType, string remitCode, string exportId, string reportType, string remitDate)
        {
            try
            {
                string[] parms = null;

                if (reportName.Equals("CraGarnishmentDetailsReport"))
                    parms = new string[] { user.DatabaseName, detailType, exportId, user.LanguageCode };
                else if (reportName.Equals("RqSourceDeductions"))
                    parms = new string[] { user.DatabaseName, exportId, user.LanguageCode };
                else if (reportName.Equals("ChequeHealthTaxRemit"))
                    parms = new string[] { user.DatabaseName, remitCode, exportId, user.LanguageCode };
                else if (reportName.Equals("CraWcbRemitDetailsReport"))
                    parms = new string[] { user.DatabaseName, remitCode, exportId, remitDate, user.LanguageCode };
                else if (reportName.Equals("ChequeWcbRemitDetailsReport"))
                    parms = new string[] { user.DatabaseName, detailType, remitCode, exportId, user.LanguageCode };
                else if (reportName.Equals("CraSourceDeductions"))
                    parms = new string[] { user.DatabaseName, remitCode, exportId, remitDate, user.LanguageCode };

                return GetReport(user, reportName, parms, reportType);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GetRemittanceReport", ex);
                throw;
            }
        }
        /// <summary>
        /// This method generates year end reports.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        public byte[] GetYearEndReport(DatabaseUser user, YearEndReportsCriteria criteria)
        {
            try
            {
                string[] parms = null;

                if (criteria.ReportName.Equals("T4") || criteria.ReportName.Equals("T4MassPrint") || criteria.ReportName.Equals("T4MassFile"))
                    parms = new string[] { user.DatabaseName, criteria.YearEndT4Ids };

                if (criteria.ReportName == "T4MassPrint") //update the year_end table with a timestamp of when the file was created
                    UpdateYearEndTable(user, Convert.ToInt64(criteria.Year), criteria.ReportName);

                return GetReport(user, criteria.ReportName.Replace("MassPrint", "").Replace("MassFile", ""), parms, criteria.ReportType);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GetYearEndReport", ex);
                throw;
            }
        }
        //this is for reports driven off the HR-Reports menu.  These dont have the usual payroll variables so they have their own method
        public byte[] GetHRMenuReport(DatabaseUser user, HrReportsCriteria criteria)
        {
            try
            {
                string[] parms = null;

                if (criteria.ReportName.Equals("EmployeeDetailAuditReport"))
                    parms = new string[] { user.DatabaseName, criteria.EmployeeNumber, criteria.OrganizationUnit, criteria.StartDate, criteria.EndDate, criteria.PaymentMethodCode, user.LanguageCode, user.SecurityRoleId.ToString(), user.SecurityUserId.ToString(), "false" };

                return GetReport(user, criteria.ReportName, parms, criteria.ReportType);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GetHRMenuReport", ex);
                throw;
            }
        }
        public byte[] GetReport(DatabaseUser user, String reportName, long payrollProcessId, String employeeNumber, String reportType, String year, String employerNumber,
            String provinceCode, String reportShowDescriptionField, String revisionNumber, String reportPayRegisterSortOrder,
            DateTime? remittanceDate = null, DateTime? remittancePeriodEndDate = null, String codeWcbCode = null, String codeProvinceStateCd = null, long employeeId = 0)
        {
            try
            {
                String[] parms = null;

                if (reportName.Equals("EmployeePaySlip"))
                {
                    EmployeeCriteria criteria = new EmployeeCriteria() { EmployeeId = employeeId, UseLikeStatementInProcSelect = false };
                    EmployeeSummaryCollection employee = EmployeeAccess.GetEmployeeSummary(user, criteria);

                    if (employee.Count == 0)
                        throw new Exception("You are not authorized to view this content.");
                    else
                        parms = new String[] { Convert.ToString(employeeId), payrollProcessId.ToString(), user.DatabaseName, reportShowDescriptionField };
                }
                else if (reportName.Equals("YearEndAdjustmentReport"))
                {
                    if (employerNumber != "x") //arrived here from the year end adjustments search screen
                    {
                        String key = payrollProcessId.ToString().Substring(0, payrollProcessId.ToString().Length - 1); //payrollProcessId here is holding the "keyId", remove the last digit
                        String reportTypeId = payrollProcessId.ToString().Substring(payrollProcessId.ToString().Length - 1); //the report type is the last digit from above (1="T4", 2="T4a", 3="R1")

                        //because R1/R2 no longer contains employer number in the table, we are routed here from year end adjustments search screen with a value in employerNumber equal to "R1R1".  Replace with null for the proc call.
                        if (employerNumber == "R1R1")
                            employerNumber = null;

                        parms = new String[] { user.DatabaseName, year, employerNumber, key, reportTypeId, revisionNumber, user.LanguageCode };
                    }
                    else //arrived here from the year end screen, only "YearEndAdjustmentReport" will get here
                        parms = new String[] { user.DatabaseName, year, null, null, null, revisionNumber, user.LanguageCode };
                }
                else if (reportName.Equals("YeAdjustAudit"))
                {
                    if (employerNumber != "x") //arrived here from the year end adjustments search screen
                    {
                        String key = payrollProcessId.ToString().Substring(0, payrollProcessId.ToString().Length - 1); //payrollProcessId here is holding the "keyId", remove the last digit
                        String reportTypeId = payrollProcessId.ToString().Substring(payrollProcessId.ToString().Length - 1); //the report type is the last digit from above (1="T4", 2="T4a", 3="R1")

                        //because R1/R2 no longer contains employer number in the table, we are routed here from year end adjustments search screen with a value in employerNumber equal to "R1R1".  Replace with null for the proc call.
                        if (employerNumber == "R1R1")
                            employerNumber = null;

                        parms = new String[] { user.DatabaseName, year, employerNumber, key, reportTypeId, revisionNumber, user.LanguageCode };
                    }
                    else //arrived here from the year end screen, only "YearEndAdjustmentReport" will get here
                        parms = new String[] { user.DatabaseName, year, null, null, null, revisionNumber, user.LanguageCode };
                }
                else if (reportName.Equals("PaycodeReport"))
                    parms = new String[] { user.DatabaseName, year.Equals("x") ? null : year, employerNumber.Equals("x") ? "" : employerNumber /*uses paycodeType*/, user.LanguageCode };
                else if (reportName.Equals("PaySlipMailingLabel"))
                    parms = new String[] { user.DatabaseName, employeeNumber };
                else if (reportName.Equals("PayRegister"))
                    parms = new String[] { payrollProcessId.ToString(), user.LanguageCode, reportPayRegisterSortOrder, user.DatabaseName };
                else if (reportName.Equals("MassPaySlipPrint"))
                    parms = new String[] { payrollProcessId.ToString(), user.DatabaseName, reportShowDescriptionField };
                else if (reportName.Equals("RegisterSummary"))
                    parms = new String[] { payrollProcessId.ToString(), user.LanguageCode, user.DatabaseName };
                else if (reportName.Equals("T4A") || reportName.Equals("NR4"))
                    parms = new String[] { user.DatabaseName, employeeNumber /*uses employeeId*/, year, employerNumber, revisionNumber };
                else if (reportName.Equals("T4RSP") || reportName.Equals("T4ARCA") || reportName.Equals("R1") || reportName.Equals("R2"))
                    parms = new String[] { user.DatabaseName, employeeNumber /*uses employeeId*/, year, revisionNumber };
                else if (reportName.Equals("YearEndT4Detail") || reportName.Equals("YearEndT4ADetail"))
                    parms = new String[] { user.DatabaseName, year, employerNumber };
                else if (reportName.Equals("YearEndR1Detail"))
                    parms = new String[] { user.DatabaseName, year, user.LanguageCode };
                else if (reportName.Equals("YearEndR2Detail"))
                    parms = new String[] { user.DatabaseName, year, user.LanguageCode };
                else if (reportName.Equals("R1SummaryOfSourceDeductions") || reportName.Equals("T4ASummaryMassPrint") || reportName.Equals("NR4SummaryMassPrint") || reportName.Equals("T4SummaryMassPrint") || reportName.Equals("R2Summary"))
                    parms = new String[] { user.DatabaseName, year, revisionNumber };
                else if (reportName.Equals("T4RSPMassPrint") || reportName.Equals("T4AMassPrint") || reportName.Equals("T4ARCAMassPrint") || reportName.Equals("NR4MassPrint") || reportName.Equals("R1MassPrint") || reportName.Equals("R2MassPrint") || reportName.Equals("T4RSPMassFile") || reportName.Equals("T4AMassFile") || reportName.Equals("T4ARCAMassFile") || reportName.Equals("NR4MassFile") || reportName.Equals("R1MassFile") || reportName.Equals("R2MassFile"))
                    parms = new String[] { user.DatabaseName, year, employeeNumber /*uses employeeId*/ };
                else if (reportName.Equals("StatDeductionExceptionReport") || reportName.Equals("NameAddressException") || reportName.Equals("YearEndPaycodeSummaryReport") || reportName.Equals("YearEndHealthTaxSummary"))
                    parms = new String[] { user.DatabaseName, year };
                else if (reportName.Equals("UserAccessInformation") || reportName.Equals("EmployeeEmergencyContacts") || reportName.Equals("SkillsCertificateLicenseSample"))
                    parms = new String[] { user.DatabaseName };
                else if (reportName.Equals("SalaryHourlyReport") || reportName.Equals("ProbationReport"))
                {
                    string employeePositionStatusCode = null;
                    string organizationUnitId = null;

                    employeePositionStatusCode = year.Equals("x") ? null : year;
                    organizationUnitId = employerNumber.Equals("x") ? null : employerNumber;

                    parms = new String[] { user.DatabaseName, user.SecurityRoleId.ToString(), user.SecurityUserId.ToString(), user.LanguageCode, employeePositionStatusCode, organizationUnitId };
                }
                else if (reportName.Equals("EmployeePhoneList") || reportName.Equals("EmployeeAnniversaryListing") || reportName.Equals("EmployeeListingReport"))
                    parms = new String[] { user.DatabaseName, user.SecurityRoleId.ToString(), user.SecurityUserId.ToString(), user.LanguageCode };
                else if (reportName.Equals("EmployeeBirthdayList") || reportName.Equals("EmployeeNameAndAddressListing") || reportName.Equals("TerminatedEmployees") || reportName.Equals("StatusCodeReport"))
                    parms = new String[] { user.DatabaseName, user.SecurityRoleId.ToString(), user.SecurityUserId.ToString() };
                else if (reportName.Equals("GL") || reportName.Equals("SrpReport") || reportName.Equals("PreYearEndDetail") || reportName.Equals("PreYearEndIncome") || reportName.Equals("CustomerInvoice") || reportName.Equals("EftCustomerInvoiceJamaica"))
                    parms = new String[] { user.DatabaseName, payrollProcessId.ToString() };
                else if (reportName.Equals("FedProvRemitSummary"))
                    parms = new String[] { user.DatabaseName, year, payrollProcessId.ToString(), user.LanguageCode };
                else if (reportName.Equals("ProvincialHealthSummary") || reportName.Equals("QuebecRemitSummary") || reportName.Equals("PreYET4ADetail") || reportName.Equals("PreYER1Detail") || reportName.Equals("PreYER2DetailReport") || reportName.Equals("PreYET4Detail"))
                    parms = new String[] { user.DatabaseName, payrollProcessId.ToString(), year, user.LanguageCode };
                else if (reportName.Equals("YearEndWCBReport"))
                    parms = new String[] { user.DatabaseName, year, user.LanguageCode };
                else if (reportName.Equals("MassYeAdjustAudit"))
                    parms = new String[] { user.DatabaseName, year, revisionNumber, user.LanguageCode };
                else if (reportName.Equals("StatHoliday"))
                    parms = new String[] { user.DatabaseName, employeeNumber /*uses holidayDate*/ };
                else if (reportName.Equals("ChequeWcbInvoice"))
                    parms = new String[] { user.DatabaseName, (remittanceDate.HasValue) ? Convert.ToDateTime(remittanceDate).ToString() : null, (remittancePeriodEndDate.HasValue) ? Convert.ToDateTime(remittancePeriodEndDate).ToString() : null, codeWcbCode };
                else if (reportName.Equals("ChequeHealthTaxInvoice"))
                    parms = new String[] { user.DatabaseName, (remittanceDate.HasValue) ? Convert.ToDateTime(remittanceDate).ToString() : null, (remittancePeriodEndDate.HasValue) ? Convert.ToDateTime(remittancePeriodEndDate).ToString() : null, codeProvinceStateCd };
                else if (reportName.Equals("AccidentDangerousOccurrence"))
                    parms = new String[] { user.DatabaseName, payrollProcessId.ToString() /*uses employeeWsibHealthAndSafetyReportId*/ };
                else if (reportName.Equals("PD7AReport"))
                    parms = new String[] { user.DatabaseName, user.LanguageCode, payrollProcessId.ToString() };
                else if (reportName.Equals("AttritionReport"))
                    parms = new String[] { user.DatabaseName, employeeNumber /*uses monthCode*/, year, user.LanguageCode, user.SecurityRoleId.ToString(), user.SecurityUserId.ToString() };
                else if (reportName.Equals("WageTypeCatalog"))
                    parms = new String[] { user.DatabaseName, user.LanguageCode };
                else if (reportName.Equals("PaycodeYearEndMapping"))
                    parms = new String[] { user.DatabaseName, year, user.LanguageCode };
                else if (reportName.Equals("P45"))
                    parms = new String[] { user.DatabaseName, employeeId.ToString(), user.LanguageCode, user.SecurityRoleId.ToString(), user.SecurityUserId.ToString() };
                else if (reportName.Equals("NationalHousingTrust"))
                    parms = new String[] { user.DatabaseName, employeeId.ToString(), user.SecurityUserId.ToString(), user.LanguageCode, null };
                else if (reportName.Equals("NonStatDeductions"))
                    parms = new String[] { user.DatabaseName, payrollProcessId.ToString() /* uses vendorId */, employeeNumber /* uses selectedDate */, user.LanguageCode };
                else if (reportName.Equals("HrChangesAuditReport"))
                {
                    string startDate = null;
                    string endDate = null;
                    string organizationUnitId = null;

                    startDate = year;
                    endDate = employerNumber;
                    organizationUnitId = provinceCode.Equals("x") ? null : provinceCode;

                    parms = new String[] { user.DatabaseName, user.SecurityRoleId.ToString(), user.SecurityUserId.ToString(), user.LanguageCode, startDate, endDate, organizationUnitId };
                }
                else if (reportName.Equals("SkillReport"))
                {
                    string employeeSkillCode = null;
                    string organizationUnitId = null;

                    employeeSkillCode = year.Equals("x") ? null : year;
                    organizationUnitId = employerNumber.Equals("x") ? null : employerNumber;

                    parms = new String[] { user.DatabaseName, user.SecurityRoleId.ToString(), user.SecurityUserId.ToString(), user.LanguageCode, employeeSkillCode, organizationUnitId };
                }
                else if(reportName.Equals("AccumHoursSummaryRateProgression"))
                    parms = new string[] { user.DatabaseName, payrollProcessId.ToString(), user.LanguageCode };
                else
                    parms = new String[] { user.DatabaseName, payrollProcessId.ToString(), user.LanguageCode };

                byte[] report = GetReport(user, reportName, parms, reportType);

                if (reportName == "T4AMassPrint" || reportName == "R1MassPrint" || reportName == "R2MassPrint" || reportName == "NR4MassPrint")
                    UpdateYearEndTable(user, Convert.ToInt64(year), reportName); //update the year_end table with a timestamp of when the file was created

                return report;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GetReport", ex);
                throw;
            }
        }
        public byte[] MergePDF(DatabaseUser user, String reportName, long payrollProcessId, String employeeNumber, String reportType, String year, String employerNumber, String provinceCode, String reportShowDescriptionField, String reportPayRegisterSortOrder)
        {
            byte[] report = null;

            if (reportName == "MailingPaySlip")
            {
                //load pdf documents
                byte[] label = GetReport(user, "PaySlipMailingLabel", payrollProcessId, employeeNumber, reportType, year, employerNumber, provinceCode, reportShowDescriptionField, null, reportPayRegisterSortOrder);
                byte[] payslip = GetReport(user, "EmployeePaySlip", payrollProcessId, employeeNumber, reportType, year, employerNumber, provinceCode, reportShowDescriptionField, null, reportPayRegisterSortOrder);

                iTextSharp.text.Document doc = new iTextSharp.text.Document();
                System.IO.MemoryStream stream = new System.IO.MemoryStream();
                iTextSharp.text.pdf.PdfWriter writer = iTextSharp.text.pdf.PdfWriter.GetInstance(doc, stream);

                doc.Open();

                iTextSharp.text.pdf.PdfContentByte content = writer.DirectContent;
                iTextSharp.text.pdf.PdfReader payslipReader = new iTextSharp.text.pdf.PdfReader(label);
                RenderToMemoryStream(payslipReader, doc, writer, content);

                iTextSharp.text.pdf.PdfReader labelReader = new iTextSharp.text.pdf.PdfReader(payslip);
                RenderToMemoryStream(labelReader, doc, writer, content);

                doc.Close();

                report = stream.ToArray();
            }

            return report;
        }
        private void RenderToMemoryStream(iTextSharp.text.pdf.PdfReader reader, iTextSharp.text.Document doc, iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.pdf.PdfContentByte content)
        {
            for (int pageNumber = 1; pageNumber <= reader.NumberOfPages; pageNumber++)
            {
                doc.SetPageSize(reader.GetPageSizeWithRotation(pageNumber));
                doc.NewPage();

                iTextSharp.text.pdf.PdfImportedPage importedPage = writer.GetImportedPage(reader, pageNumber);

                content.AddTemplate(importedPage, 1f, 0, 0, 1f, 0, 0);

                int pageOrientation = reader.GetPageRotation(pageNumber);

                if ((pageOrientation == 90) || (pageOrientation == 270))
                    content.AddTemplate(importedPage, 0, -1f, 1f, 0, 0, reader.GetPageSizeWithRotation(pageNumber).Height);
                else
                    content.AddTemplate(importedPage, 1f, 0, 0, 1f, 0, 0);
            }
        }
        /// <summary>
        /// This method generates and zips year end reports for the Mass File All export for a specified year.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="massPayslipInternalFileFormats"></param>
        /// <param name="year"></param>
        /// <param name="reportTypes"></param>
        /// <param name="reportShowDescriptionField"></param>
        /// <param name="reportPayRegisterSortOrder"></param>
        /// <param name="revision"></param>
        public byte[] GetMassFileForAllReportsForYear(DatabaseUser user, string[] massPayslipInternalFileFormats, long year, string[] reportTypes, string reportShowDescriptionField, string reportPayRegisterSortOrder, string revision)
        {
            try
            {
                T4T4aR1MassPrintCollection massObject = new T4T4aR1MassPrintCollection();
                Dictionary<string, byte[]> employeeReports = new Dictionary<string, byte[]>();
                int i = 0;

                foreach (string reportName in reportTypes)
                {
                    YearEndT4Collection t4Collection = new YearEndT4Collection();
                    string prevEmployeeNumber = "";
                    string currentEmployeeNumber = "";

                    if (reportName == "T4MassFile")
                        t4Collection = YearEndManagement.GetYearEndT4(user, Convert.ToInt32(year), null);
                    else
                        massObject = GetMassObject(user, year, reportName);

                    if (t4Collection != null && t4Collection.Count > 0)
                    {
                        foreach (YearEndT4 t4 in t4Collection)
                        {
                            currentEmployeeNumber = t4.EmployeeNumber;

                            if (prevEmployeeNumber != currentEmployeeNumber) //assign to our jagged array
                                employeeReports.Add(XmlManagement.GenerateFileName(massPayslipInternalFileFormats[i], new EmployeeSummary() { EmployeeNumber = t4.EmployeeNumber }, new PayrollProcess() { PeriodYear = year }, 1), GetMassFileReport(user, "PDF", reportName, year.ToString(), null, null, null, null, null, null, t4.YearEndT4Id));

                            prevEmployeeNumber = currentEmployeeNumber;
                        }
                    }
                    else if (massObject != null && massObject.Count > 0)
                    {
                        foreach (T4T4aR1MassPrint employee in massObject)
                        {
                            currentEmployeeNumber = employee.EmployeeNumber;

                            if (prevEmployeeNumber != currentEmployeeNumber) //assign to our jagged array
                                employeeReports.Add(XmlManagement.GenerateFileName(massPayslipInternalFileFormats[i], new EmployeeSummary() { EmployeeNumber = employee.EmployeeNumber }, new PayrollProcess() { PeriodYear = year }, 1), GetMassFileReport(user, "PDF", reportName, year.ToString(), employee.EmployeeId.ToString(), employee.EmployerNumber, employee.TaxableCodeProvinceStateCd, reportShowDescriptionField, revision, reportPayRegisterSortOrder));

                            prevEmployeeNumber = currentEmployeeNumber;
                        }
                    }

                    //update the year_end table with a timestamp of when the file was created
                    UpdateYearEndTable(user, year, reportName);
                    i++;
                }

                return CreateZip(user, employeeReports);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GetMassFileForAllReportsForYear", ex);
                throw;
            }
        }
        /// <summary>
        /// This method retrieves data for the T4A/T4RSP/T4ARCA/NR4/R1/R2 reports for the Mass File All export for a specified year.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="year"></param>
        /// <param name="reportName"></param>
        private T4T4aR1MassPrintCollection GetMassObject(DatabaseUser user, long year, string reportName)
        {
            if (reportName == "T4RSPMassFile")
                return ReportAccess.GetMassT4RSPReportValues(user, year);
            else if (reportName == "T4AMassFile")
                return ReportAccess.GetMassT4AReportValues(user, year);
            else if (reportName == "T4ARCAMassFile")
                return ReportAccess.GetMassT4ARCAReportValues(user, year);
            else if (reportName == "NR4MassFile")
                return ReportAccess.GetMassNR4ReportValues(user, year);
            else if (reportName == "R1MassFile")
                return ReportAccess.GetMassR1ReportValues(user, year);
            else if (reportName == "R2MassFile")
                return ReportAccess.GetMassR2ReportValues(user, year);

            return null;
        }
        /// <summary>
        /// This method generates the T4/T4A/T4RSP/T4ARCA/NR4/R1/R2 reports for the Mass File All export for a specified year.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportType"></param>
        /// <param name="reportName"></param>
        /// <param name="year"></param>
        /// <param name="employeeId"></param>
        /// <param name="employerNumber"></param>
        /// <param name="taxableProvinceStateCode"></param>
        /// <param name="reportShowDescriptionField"></param>
        /// <param name="reportPayRegisterSortOrder"></param>
        /// <param name="revision"></param>
        /// <param name="yearEndT4Id"></param>
        private byte[] GetMassFileReport(DatabaseUser user, string reportType, string reportName, string year, string employeeId, string employerNumber, string taxableProvinceStateCode, string reportShowDescriptionField, string reportPayRegisterSortOrder, string revision, long? yearEndT4Id = null)
        {
            if (reportName == "T4MassFile")
            {
                List<YearEndT4> t4s = new List<YearEndT4> { new YearEndT4() { YearEndT4Id = Convert.ToInt64(yearEndT4Id) } };
                YearEndReportsCriteria criteria = new YearEndReportsCriteria()
                {
                    ReportName = reportName,
                    ReportType = reportType,
                    Year = year,
                    YearEndT4Ids = new JavaScriptSerializer().Serialize(t4s.Select(x => new { year_end_t4_id = x.YearEndT4Id }))
                };

                return GetYearEndReport(user, criteria);
            }

            return GetReport(user, reportName, -1, employeeId, reportType, year, employerNumber, taxableProvinceStateCode, reportShowDescriptionField, revision, reportPayRegisterSortOrder);
        }
        public byte[] GetReportForAllEmployeesOfThisYear(DatabaseUser user, String massPayslipInternalFileFormat, long year, String reportType, String reportShowDescriptionField, String reportPayRegisterSortOrder)
        {
            try
            {
                T4T4aR1MassPrintCollection massObject = new T4T4aR1MassPrintCollection();

                //change the report type so the mass file can call the regular report and build itself
                if (reportType == "T4MassFile")
                    massObject = ReportAccess.GetMassT4ReportValues(user, year);
                else if (reportType == "T4AMassFile")
                    massObject = ReportAccess.GetMassT4AReportValues(user, year);
                else if (reportType == "R1MassFile")
                    massObject = ReportAccess.GetMassR1ReportValues(user, year);

                Dictionary<String, byte[]> employeeReports = null;

                if (massObject.Count > 0)
                {
                    employeeReports = new Dictionary<string, byte[]>();

                    String prevEmployeeNumber = "";
                    String currentEmployeeNumber = "";

                    foreach (T4T4aR1MassPrint employee in massObject)
                    {
                        currentEmployeeNumber = employee.EmployeeNumber;

                        //assign to our jagged array
                        if (prevEmployeeNumber != currentEmployeeNumber)
                            employeeReports.Add(XmlManagement.GenerateFileName(massPayslipInternalFileFormat, new EmployeeSummary() { EmployeeNumber = employee.EmployeeNumber }, new PayrollProcess() { PeriodYear = year }, 1), GetReport(user, reportType, -1, employee.EmployeeNumber, "", "PDF", employee.Year, employee.EmployerNumber, employee.TaxableCodeProvinceStateCd, reportShowDescriptionField, reportPayRegisterSortOrder));

                        prevEmployeeNumber = currentEmployeeNumber;
                    }
                }

                byte[] zipFile = CreateZip(user, employeeReports);

                //update the year_end table with a timestamp of when the file was created
                UpdateYearEndTable(user, year, reportType);

                return zipFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GetReportForAllEmployeesOfThisYear", ex);
                throw;
            }
        }
        public void UpdateYearEndTable(DatabaseUser user, long year, String reportType)
        {
            YearEndAccess yearEnd = new YearEndAccess();
            YearEndCollection yearEndColl = new YearEndCollection();
            yearEndColl = yearEnd.GetYearEnd(user, year);

            if (reportType == "T4" || reportType == "T4MassPrint")
                yearEndColl[0].T4PrintDatetime = DateTime.Now;
            else if (reportType == "T4A" || reportType == "T4AMassPrint")
                yearEndColl[0].T4aPrintDatetime = DateTime.Now;
            else if (reportType == "R1" || reportType == "R1MassPrint")
                yearEndColl[0].R1PrintDatetime = DateTime.Now;
            else if (reportType == "R2" || reportType == "R2MassPrint")
                yearEndColl[0].R2PrintDatetime = DateTime.Now;
            else if (reportType == "NR4" || reportType == "NR4MassPrint")
                yearEndColl[0].NR4PrintDatetime = DateTime.Now;
            else if (reportType == "T4MassFile")
                yearEndColl[0].T4MassFileDatetime = DateTime.Now;
            else if (reportType == "T4AMassFile")
                yearEndColl[0].T4aMassFileDatetime = DateTime.Now;
            else if (reportType == "R1MassFile")
                yearEndColl[0].R1MassFileDatetime = DateTime.Now;
            else if (reportType == "R2MassFile")
                yearEndColl[0].R2MassFileDatetime = DateTime.Now;
            else if (reportType == "NR4MassFile")
                yearEndColl[0].NR4MassFileDatetime = DateTime.Now;
            else if (reportType == "T4Export")
                yearEndColl[0].T4ExportDatetime = DateTime.Now;
            else if (reportType == "T4aExport")
                yearEndColl[0].T4aExportDatetime = DateTime.Now;
            else if (reportType == "R1Export")
                yearEndColl[0].R1ExportDatetime = DateTime.Now;
            else if (reportType == "R2Export")
                yearEndColl[0].R2ExportDatetime = DateTime.Now;
            else if (reportType == "NR4Export")
                yearEndColl[0].NR4ExportDatetime = DateTime.Now;

            if (yearEndColl.Count != 0)
                yearEnd.UpdateYearEndRecord(user, yearEndColl[0]);
        }
        public byte[] CreateMercerPensionExportFile(DatabaseUser user, long payrollProcessId, String reportShowDescriptionField, String reportPayRegisterSortOrder)
        {
            try
            {
                if (user.DatabaseName.ToUpper() == "A1225")
                    return GetReport(user, "MercerPensionExport", payrollProcessId, "", "EXCELOPENXML", "x", "x", "x", reportShowDescriptionField, null, reportPayRegisterSortOrder);
                else
                    return GetReport(user, "MercerPensionExport", payrollProcessId, "", "EXCEL", "x", "x", "x", reportShowDescriptionField, null, reportPayRegisterSortOrder);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - CreateMercerPensionExportFile", ex);
                throw;
            }
        }
        public byte[] CreateGLExportFile(DatabaseUser user, long payrollProcessId, String glType, String reportShowDescriptionField, String reportPayRegisterSortOrder)
        {
            try
            {
                Dictionary<String, byte[]> glReport = new Dictionary<String, byte[]>();

                //only one report will be added for now
                if (glType.ToLower().StartsWith("gl_csv"))
                    glReport.Add(String.Format("GLExport{0}.csv", payrollProcessId.ToString()), GetCsvExport(user, payrollProcessId, glType.ToLower().Equals("gl_csv_header")));
                else if (glType.ToLower().StartsWith("gl_sage"))
                    glReport.Add(String.Format("GLExport{0}.csv", payrollProcessId.ToString()), GetSageCsvExport(user, payrollProcessId));
                else if (glType.ToLower().StartsWith("gl_pex"))
                    glReport.Add(String.Format("GLExport{0}.csv", payrollProcessId.ToString()), GetPexCsvExport(user, payrollProcessId));
                else if (glType.ToLower().StartsWith("gl_text"))
                {
                    //store parameters and values in string arrays for re-useable function
                    String[] parmNames = new String[2] { "@databaseName", "@payrollProcessId" };
                    Object[] parmValues = new Object[2] { user.DatabaseName, payrollProcessId };

                    glReport.Add(String.Format("GLExport{0}.txt", payrollProcessId.ToString()), GetCsvFlatFile(user, "GLReport_select", parmNames, parmValues));
                }
                else if (glType.ToLower().StartsWith("gl_sap"))
                    glReport.Add("key", GetGlSapFlatFile(user, payrollProcessId));
                else
                    glReport.Add(String.Format("GLExport{0}.xls", payrollProcessId.ToString()), GetReport(user, "GL", payrollProcessId, "", "EXCEL", "x", "x", "x", reportShowDescriptionField, null, reportPayRegisterSortOrder));

                //depending on company, do not zip file
                if (user.DatabaseName.ToLower().Equals("a1225") || user.DatabaseName.ToLower().Equals("a1226") || (user.DatabaseName.ToLower().Equals("a1215") && glType.ToLower().StartsWith("gl_sap")))
                    return glReport.Values.First();
                else
                    return CreateZip(user, glReport);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - CreateGLExportFile", ex);
                throw;
            }
        }
        public byte[] GetCsvFlatFile(DatabaseUser user, String procName, String[] parmNames, Object[] parmValues)
        {
            byte[] glFlatFile = null;

            try
            {
                //get the needed data via sql and return a byte array
                glFlatFile = DynamicsAccess.GetGlFlatFileData(user, procName, parmNames, parmValues);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GetCsvFlatFile", ex);
                throw;
            }

            return glFlatFile;
        }

        public byte[] GetPexCsvExport(DatabaseUser user, long payrollProcessId)
        {
            byte[] glCsvFile = null;

            //get the needed data via sql and put in business object and use the biz object data to fill the record sections of the file and return the file into the byte array
            glCsvFile = DynamicsAccess.CreateGlPexCsvExport(GetGlPexData(user, payrollProcessId));

            return glCsvFile;
        }
        public GlPexCollection GetGlPexData(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                return ReportAccess.GetGlPexData(user, payrollProcessId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GetGlPexData", ex);
                throw;
            }
        }

        public byte[] GetSageCsvExport(DatabaseUser user, long payrollProcessId)
        {
            byte[] glCsvFile = null;
            //get the needed data via sql and put in business object and use the biz object data to fill the record sections of the file and return the file into the byte array
            glCsvFile = DynamicsAccess.CreateGlSageCsvExport(GetGlSageData(user, payrollProcessId));

            return glCsvFile;
        }
        public GlSageCollection GetGlSageData(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                return ReportAccess.GetGlSageData(user, payrollProcessId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GetGlSageData", ex);
                throw;
            }
        }
        public byte[] GetCsvExport(DatabaseUser user, long payrollProcessId, bool includeHeader)
        {
            byte[] glCsvFile = null;

            //get the needed data via sql and put in business object and use the biz object data to fill the record sections of the file and return the file into the byte array
            glCsvFile = CreateGlCsvExport(GetGlData(user, payrollProcessId), includeHeader);

            return glCsvFile;
        }
        public byte[] GetGlSapFlatFile(DatabaseUser user, long payrollProcessId)
        {
            byte[] glCsvFile = null;

            //get the needed data via sql and put in business object and use the biz object data to fill the record sections of the file and return the file into the byte array
            glCsvFile = DynamicsAccess.CreateGlSapExport(GetGlSapData(user, payrollProcessId));

            return glCsvFile;
        }
        public GlSapCollection GetGlSapData(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                return ReportAccess.GetGlSapData(user, payrollProcessId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GetGlSapData", ex);
                throw;
            }
        }
        public GlCsvCollection GetGlData(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                return ReportAccess.GetGlData(user, payrollProcessId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GetGlData", ex);
                throw;
            }
        }
        public byte[] CreateGlCsvExport(GlCsvCollection collection, bool includeHeader)
        {
            return DynamicsAccess.CreateGlCsvExport(collection, includeHeader);
        }
        public byte[] CreateStockExportFile(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                return GetReport(user, "STOCK", payrollProcessId, "", "EXCEL", "x", "x", "x", null, null, null);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - CreateStockExportFile", ex);
                throw;
            }
        }
        public byte[] GetReportForAllEmployeesOfThisPayProcess(DatabaseUser user, String massPayslipInternalFileFormat, long payrollProcessId, String reportShowDescriptionField, String reportPayRegisterSortOrder, int maxDegreeOfParallelism)
        {
            try
            {
                PayrollProcessSummary payrollProcess = PayrollAccess.GetPayrollProcessSummary(user, new PayrollProcessCriteria() { PayrollProcessId = payrollProcessId })[0];
                EmployeeSummaryCollection employees = EmployeeAccess.GetEmployeeSummary(user, new EmployeeCriteria() { PayrollProcessId = payrollProcessId, SecurityOverrideFlag = true });
                Dictionary<String, byte[]> employeeReports = null;

                if (employees.Count > 0)
                {
                    employeeReports = new Dictionary<String, byte[]>();

                    //use a parallel foreach to speed things up
                    System.Threading.Tasks.Parallel.ForEach(employees, new System.Threading.Tasks.ParallelOptions { MaxDegreeOfParallelism = maxDegreeOfParallelism }, employee =>
                   {
                       if (employee.ExcludeMassPayslipFileFlag == false)
                       {
                           //check for null values in employee.ImportExternalIdentifier, if they exist, assign a default
                           if (employee.ImportExternalIdentifier == null)
                               employee.ImportExternalIdentifier = IdentifierCheck(employee.ImportExternalIdentifier, employee.EmployeeId, user.DatabaseName);

                           String fileName = XmlManagement.GenerateFileName(massPayslipInternalFileFormat, employee, payrollProcess, (int)payrollProcess.ExportSequenceNumber);
                           employeeReports.Add(fileName, GetReport(user, "EmployeePaySlip", payrollProcessId, employee.EmployeeNumber, "PDF", "x", "x", "x", reportShowDescriptionField, null, reportPayRegisterSortOrder, null, null, null, null, employee.EmployeeId));
                       }
                   });
                }

                return CreateZip(user, employeeReports);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GetReportForAllEmployeesOfThisPayProcess", ex);
                throw;
            }
        }
        private String IdentifierCheck(String identifier, long employeeId, String companyCode)
        {
            //generate an id in format AAAAAXXXX where AAAAA = company code (A1210, A1211, etc.) and XXXX = the internal employee_id 
            if (identifier == null)
            {
                identifier = "AAAAAXXXX";

                //format the identifier
                identifier = identifier.Replace("AAAAA", companyCode);
                identifier = identifier.Replace("XXXX", employeeId.ToString().PadLeft(4, 'X'));
            }

            return identifier;
        }
        public static byte[] CreateZip(DatabaseUser user, Dictionary<String, byte[]> reports)
        {
            try
            {
                System.IO.MemoryStream stream = new System.IO.MemoryStream();

                using (System.IO.Compression.ZipArchive archive = new System.IO.Compression.ZipArchive(stream, System.IO.Compression.ZipArchiveMode.Create))
                {
                    System.IO.Compression.ZipArchiveEntry entry = null;

                    if (reports != null)
                    {
                        foreach (KeyValuePair<String, byte[]> report in reports)
                        {
                            entry = archive.CreateEntry(report.Key);

                            using (System.IO.BinaryWriter bWriter = new System.IO.BinaryWriter(entry.Open()))
                            {
                                if (report.Value != null)
                                    bWriter.Write(report.Value);
                            }
                        }
                    }
                }

                stream.Close();
                stream.Dispose();

                byte[] output = stream.ToArray();

                return output;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - CreateZip", ex);
                throw;
            }
        }
        public String GetYearEndZipFileNameFormat(String zipFileNameFormat)
        {
            return XmlManagement.GenerateFileName(zipFileNameFormat, new EmployeeSummary(), new PayrollProcess(), 1);
        }
        //used when naming the sap gl export
        public String GenerateSapFileName(DatabaseUser user, String fileNameFormat, long payrollProcessId)
        {
            PayrollProcessSummary payrollProcess = PayrollAccess.GetPayrollProcessSummary(user, new PayrollProcessCriteria() { PayrollProcessId = payrollProcessId })[0];
            int sequenceNumber = 0;

            if (payrollProcess.PayrollProcessId.ToString().Length > 3)
                sequenceNumber = Convert.ToInt32(payrollProcess.PayrollProcessId.ToString().Substring(payrollProcess.PayrollProcessId.ToString().Length - 3));
            else
                sequenceNumber = Convert.ToInt32(payrollProcess.PayrollProcessId);

            return XmlManagement.GenerateFileName(fileNameFormat, new EmployeeSummary(), payrollProcess, sequenceNumber);
        }
        public String GenerateFileName(DatabaseUser user, String fileNameFormat, long payrollProcessId)
        {
            PayrollProcessSummary payrollProcess = PayrollAccess.GetPayrollProcessSummary(user, new PayrollProcessCriteria() { PayrollProcessId = payrollProcessId })[0];
            return XmlManagement.GenerateFileName(fileNameFormat, new EmployeeSummary(), payrollProcess, (int)payrollProcess.ExportSequenceNumber);
        }
        public String GetEmpowerGarnishmentExportFileNameFormat(DatabaseUser user, String fileNameFormat, long payrollProcessId)
        {
            PayrollProcessSummary payrollProcess = PayrollAccess.GetPayrollProcessSummary(user, new PayrollProcessCriteria() { PayrollProcessId = payrollProcessId })[0];
            return XmlManagement.GenerateFileName(fileNameFormat, new EmployeeSummary(), payrollProcess, (int)payrollProcess.ExportSequenceNumber);
        }
        #endregion

        #region CSB Export
        public CanadaRevenueAgencyBondExportCollection GetCanadaRevenueAgencyBondExport(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                return ReportAccess.GetCanadaRevenueAgencyBondExport(user, payrollProcessId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GetCanadaRevenueAgencyBondExport", ex);
                throw;
            }
        }
        public CanadaRevenueAgencyBondExport InsertCanadaRevenueAgencySavingsBondExportData(DatabaseUser user, CanadaRevenueAgencyBondExport export)
        {
            try
            {
                return ReportAccess.InsertCanadaRevenueAgencySavingsBondExportData(user, export);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - InsertCanadaRevenueAgencySavingsBondExportData", ex);
                throw;
            }
        }
        public void UpdateCanadaRevenueAgencySavingsBondExportData(DatabaseUser user, CanadaRevenueAgencyBondExport export)
        {
            try
            {
                ReportAccess.UpdateCanadaRevenueAgencySavingsBondExportData(user, export);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - UpdateCanadaRevenueAgencySavingsBondExportData", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public byte[] CreateCSBExportFile(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                byte[] csbFile = null;

                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    CanadaRevenueAgencyBondExportCollection bondExport = ReportAccess.GetCanadaRevenueAgencyBondExport(user, payrollProcessId);

                    if (bondExport != null && bondExport.Count > 0)
                    {
                        bondExport[0].TransmissionDate = DateTime.Now;
                        ReportAccess.UpdateCanadaRevenueAgencySavingsBondExportData(user, bondExport[0]);
                    }

                    //get the needed data via sql and put in business object and use the biz object data to fill the record sections of the file and return the file into the byte array
                    csbFile = CreateCSBExport(user, GetExportFileData(user, payrollProcessId));

                    scope.Complete();
                }

                return csbFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - CreateCSBExportFile", ex);
                throw;
            }
        }
        public CanadaRevenueAgencyBondExportFileCollection GetExportFileData(DatabaseUser user, long payrollProcessId)
        {
            try
            {
                return ReportAccess.GetExportFileData(user, payrollProcessId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GetExportFileData", ex);
                throw;
            }
        }
        public byte[] CreateCSBExport(DatabaseUser user, CanadaRevenueAgencyBondExportFileCollection collection)
        {
            try
            {
                return WorklinksDynamicsManagement.CreateCSBExport(collection);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - CreateCSBExport", ex);
                throw;
            }
        }
        #endregion

        //#region report testing
        //public BusinessObjects.Test.TestReportCollection GetTestReports(DatabaseUser user)
        //{
        //    return TestReportAccess.GetTestReports(user);
        //}
        //public BusinessObjects.Test.TestReportParameterCollection GetTestReportParameters(DatabaseUser user, long testReportId)
        //{
        //    return TestReportAccess.GetTestReportParameters(user, testReportId);
        //}
        //public BusinessObjects.Test.TestReportExpectedValueCollection GetTestReportExpectedValues(DatabaseUser user, long testReportId)
        //{
        //    return TestReportAccess.GetTestReportExpectedValues(user, testReportId);
        //}
        //#endregion

        public static Dictionary<String, byte[]> UnzipFile(DatabaseUser user, byte[] zippedFile)
        {
            try
            {
                Dictionary<String, byte[]> exportFiles = new Dictionary<String, byte[]>();
                System.IO.MemoryStream stream = new System.IO.MemoryStream(zippedFile); //put byte array into memory stream

                using (System.IO.Compression.ZipArchive archive = new System.IO.Compression.ZipArchive(stream, System.IO.Compression.ZipArchiveMode.Update))
                {
                    if (zippedFile != null)
                    {
                        foreach (System.IO.Compression.ZipArchiveEntry entry in archive.Entries)
                        {
                            System.IO.Stream file = entry.Open();
                            byte[] fileContents;

                            using (System.IO.BinaryReader bReader = new System.IO.BinaryReader(file))
                                fileContents = bReader.ReadBytes((int)file.Length);

                            exportFiles.Add(entry.Name, fileContents);

                            file.Close();
                            file.Dispose();
                        }
                    }
                }

                stream.Close();
                stream.Dispose();

                return exportFiles;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - UnzipZip", ex);
                throw;
            }
        }

        #region sftp report and export section
        public void CreateReportsAndExports(DatabaseUser user, long payrollProcessId, String periodYear, PayRegisterExportCollection collection, string pexFtp, String reportShowDescriptionField, String reportPayRegisterSortOrder, String revenueQuebecTaxId, bool useQuebecTaxId, String citiEftFileName, String citiChequeFileName, String eftClientNumber, String citiChequeAccountNumber, String companyName, String companyShortName, String massPayslipInternalFileFormat, string originatorId, string eftType, int maxDegreeOfParallelism)
        {
            byte[] exportReport = null;
            String fileName = "";

            foreach (PayRegisterExport payRegisterExport in collection)
            {
                //get ftp protocol details
                ExportFtp exportFtp = PayrollAccess.GetExportFtp(user, payRegisterExport.ExportFtpId, null, pexFtp)[0]; //only will be 1 row

                //This code is meant for SFTP being used by BMS
                if (exportFtp.ExportFtpTypeCode == "SFTP")
                {
                    //get report details
                    ReportExport reportExport = GetReportExport(user, payRegisterExport.ExportId)[0]; //only will be 1 row

                    if (reportExport.ReportId != null) //this is a report
                        exportReport = CreateReport(user, reportExport.ReportId, reportExport.ExportTypeCode, periodYear, reportShowDescriptionField, reportPayRegisterSortOrder, payrollProcessId, reportExport.FileFormat, out fileName);
                    else //this is an export
                        exportReport = CreateExport(user, reportExport.Name, revenueQuebecTaxId, useQuebecTaxId, reportShowDescriptionField, reportPayRegisterSortOrder, reportExport.FileFormat, citiEftFileName, citiChequeFileName, eftClientNumber, citiChequeAccountNumber, companyName, companyShortName, massPayslipInternalFileFormat, payrollProcessId, out fileName, originatorId, eftType, maxDegreeOfParallelism);

                    FTP.SftpManagement ftpClass = new FTP.SftpManagement();

                    if (payRegisterExport.UnzipFlag && reportExport.ExportTypeCode.ToUpper() == "ZIP")
                    {
                        //unzip files
                        Dictionary<String, byte[]> unzippedFiles = UnzipFile(user, exportReport);

                        foreach (KeyValuePair<String, byte[]> files in unzippedFiles) //send files out one at a time
                            ftpClass.UploadSftpFile(exportFtp.UriName, exportFtp.Port, exportFtp.Directory, exportFtp.UnixPathFlag, exportFtp.Login, exportFtp.Password, files.Value, files.Key);
                    }
                    else
                        ftpClass.UploadSftpFile(exportFtp.UriName, exportFtp.Port, exportFtp.Directory, exportFtp.UnixPathFlag, exportFtp.Login, exportFtp.Password, exportReport, fileName);

                    //clear out the objects
                    exportReport = null;
                    fileName = "";
                }
                else
                {
                    //only SFTP implemented, for now throw exception as per request
                    throw new NotImplementedException();
                }
            }

            //update payroll_process.file_transmission_date
            PayrollProcess process = PayrollAccess.GetPayrollProcess(user, new PayrollProcessCriteria() { PayrollProcessId = payrollProcessId })[0];
            process.FileTransmissionDate = DateTime.Now;
            PayrollAccess.UpdatePayrollProcess(user, process);
        }
        #endregion

        #region direct deposit / cra garnishment remittance
        public void ExportDirectDepositCraGarnishmentFtps(DatabaseUser user, DateTime date, bool usingRbcEdiForDirectDepositsFlag, bool usingCraGarnishmentFlag, bool usingScotiaBankEdiForDirectDepositsFlag, string rbcFtpname, string scotiaFtpname)
        {
            try
            {
                //check if any direct deposit files and/or cra garnishment need to be sent
                PayrollExportCollection coll = DynamicsAccess.GetDirectDepositCraGarnishmentExportData(user, date, usingRbcEdiForDirectDepositsFlag, usingCraGarnishmentFlag, usingScotiaBankEdiForDirectDepositsFlag);

                if (coll != null && coll.Count > 0)
                {
                    //get ftp protocol details
                    ExportFtp rbcExportFtp = null;
                    ExportFtp scotiaExportFtp = null;

                    //if using RBC Canada
                    if (usingRbcEdiForDirectDepositsFlag || usingCraGarnishmentFlag)
                        rbcExportFtp = PayrollAccess.GetExportFtp(user, null, "FTPS", rbcFtpname)[0];       //only will be 1 row

                    //if using ScotiaBank Jamaica
                    if (usingScotiaBankEdiForDirectDepositsFlag)
                        scotiaExportFtp = PayrollAccess.GetExportFtp(user, null, "FTPS", scotiaFtpname)[0]; //only will be 1 row

                    //send the files
                    foreach (PayrollExport payExport in coll)
                    {
                        RbcFptFiles(user, payExport, date, (payExport.EftType == "SCOT_EDI" ? scotiaExportFtp : rbcExportFtp));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void RbcFptFiles(DatabaseUser user, PayrollExport payExport, DateTime date, ExportFtp exportFtp)
        {
            if (payExport.Data != null)
            {
                FTP.SftpManagement ftpClass = new FTP.SftpManagement();

                if (payExport.EftType == "R820DD")
                {
                    //get filename
                    String eftFileName = String.Format("{0:yyyyMMddHHmmssfff}_{1}_{2}_EFT.txt", DateTime.Now, user.DatabaseName, payExport.PayrollProcessId);
                    ftpClass.UploadFtpsFile(eftFileName, payExport.Data, exportFtp.UriName, exportFtp.Login, exportFtp.Password, exportFtp.EnableSslFlag, exportFtp.BinaryModeFlag, exportFtp.PassiveModeFlag);
                }
                else if (payExport.EftType == "SCOT_EDI")
                {
                    //get filename
                    String eftFileName = String.Format("{0:yyyyMMddHHmmssfff}_{1}_{2}_EFT.txt", DateTime.Now, user.DatabaseName, payExport.PayrollProcessId);
                    ftpClass.UploadFtpsFile(eftFileName, payExport.Data, exportFtp.UriName, exportFtp.Login, exportFtp.Password, exportFtp.EnableSslFlag, exportFtp.BinaryModeFlag, exportFtp.PassiveModeFlag);
                }
                else if (payExport.EftType == "R820GARN")
                {
                    //get filename
                    String garnishmentsEftFileName = String.Format("{0:yyyyMMddHHmmssfff}_{1}_{2}_GARNISH_EFT.txt", DateTime.Now, user.DatabaseName, payExport.PayrollProcessId);
                    ftpClass.UploadFtpsFile(garnishmentsEftFileName, payExport.Data, exportFtp.UriName, exportFtp.Login, exportFtp.Password, exportFtp.EnableSslFlag, exportFtp.BinaryModeFlag, exportFtp.PassiveModeFlag);
                }

                //update the file sent flag
                payExport.FileSentFlag = true;
                DynamicsAccess.UpdatePayrollExportFileSentFlag(user, payExport);

                //update payroll_process.eft_file_transmission_date
                PayrollProcess process = PayrollAccess.GetPayrollProcess(user, new PayrollProcessCriteria() { PayrollProcessId = payExport.PayrollProcessId })[0];
                process.EftFileTransmissionDate = DateTime.Now;
                PayrollAccess.UpdatePayrollProcess(user, process);

                SendEmailParameters emailParms = GetEmailParms(user, null, null, null, date, payExport.ProcessGroupDescription, false, false, false, false, (payExport.EftType == "R820DD" || payExport.EftType == "SCOT_EDI"), (payExport.EftType == "R820GARN"), false, false, false, false);

                //Send notification once file transfer is complete
                EmailManagement.SendMessage(emailParms);
            }
        }

        #endregion

        #region ftps section for eft, cra export section

        public void FtpRbcEdiExport(DatabaseUser user, long payrollProcessId, RbcEftEdiParameters ediParms, RbcEftSourceDeductionParameters garnishmentParms, string rbcFtpName)
        {
            try
            {
                byte[] eftFile = null;
                byte[] garnishmentEftFile = null;
                ExportFtp exportFtp = null;

                //if client is using RBC EFT DIRECT DEPOSIT FILE, ediParms will not be null
                if (ediParms != null)
                {
                    //get direct deposit eft from database
                    eftFile = WorklinksDynamicsManagement.GenerateRBCEftEdiFile(user, payrollProcessId, ediParms, null);
                }
                if (garnishmentParms != null)  //if client is using CRA GARNISHMENT FILE, ediParms will not be null
                {
                    //get garnishments eft from database
                    garnishmentEftFile = WorklinksDynamicsManagement.GenerateCraEftGarnishmentFile(user, payrollProcessId, garnishmentParms, null);
                }

                if (eftFile != null || garnishmentEftFile != null)
                {
                    //get ftp protocol details
                    exportFtp = PayrollAccess.GetExportFtp(user, null, "FTPS", rbcFtpName)[0]; //only will be 1 row
                    FTP.SftpManagement ftpClass = new FTP.SftpManagement();

                    if (eftFile != null)
                    {
                        //get filename
                        String eftFileName = String.Format("{0:yyyyMMddHHmmssfff}_{1}_{2}_EFT.txt", DateTime.Now, user.DatabaseName, payrollProcessId);
                        ftpClass.UploadFtpsFile(eftFileName, eftFile, exportFtp.UriName, exportFtp.Login, exportFtp.Password, exportFtp.EnableSslFlag, exportFtp.BinaryModeFlag, exportFtp.PassiveModeFlag);
                    }
                    if (garnishmentEftFile != null)
                    {
                        //get filename
                        String garnishmentsEftFileName = String.Format("{0:yyyyMMddHHmmssfff}_{1}_{2}_GARNISH_EFT.txt", DateTime.Now, user.DatabaseName, payrollProcessId);
                        ftpClass.UploadFtpsFile(garnishmentsEftFileName, garnishmentEftFile, exportFtp.UriName, exportFtp.Login, exportFtp.Password, exportFtp.EnableSslFlag, exportFtp.BinaryModeFlag, exportFtp.PassiveModeFlag);
                    }

                    //update payroll_process.file_transmission_date
                    PayrollProcess process = PayrollAccess.GetPayrollProcess(user, new PayrollProcessCriteria() { PayrollProcessId = payrollProcessId })[0];
                    process.EftFileTransmissionDate = DateTime.Now;
                    PayrollAccess.UpdatePayrollProcess(user, process);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void CheckForUnprocessedExports(DatabaseUser user, string rbcFtpname)
        {
            //look for unprocessed exports
            CraExportCollection craExportColl = DynamicsAccess.CheckForUnprocessedCraRemittanceOnDatabase(user);
            CraWcbExportCollection craWcbExportColl = DynamicsAccess.CheckForUnprocessedCraWcbRemittanceOnDatabase(user);

            //if either has data, try to resend the file(s)
            if ((craExportColl != null && craExportColl.Count > 0) || (craWcbExportColl != null && craWcbExportColl.Count > 0))
            {
                //get ftp protocol details
                ExportFtp exportFtp = PayrollAccess.GetExportFtp(user, null, "FTPS", rbcFtpname)[0]; //only will be 1 row

                //cra source deduction
                if (craExportColl != null && craExportColl.Count > 0)
                {
                    foreach (CraExport export in craExportColl)
                    {
                        FtpFilesAndSentNotification(user, export.CodeCraRemittancePeriodCd, export.Data, exportFtp, export.CraExportId, DateTime.Now, "CRA_EXPORT");
                    }
                }
                //cra wcb (nova scotia)
                if (craWcbExportColl != null && craWcbExportColl.Count > 0)
                {
                    foreach (CraWcbExport export in craWcbExportColl)
                    {
                        FtpFilesAndSentNotification(user, export.CodeCraRemittancePeriodCd, export.Data, exportFtp, export.CraWcbExportId, DateTime.Now, "CRA_WCB");
                    }
                }
            }
        }
        private void FtpFilesAndSentNotification(DatabaseUser user, string craRemittancePeriodCode, byte[] exportReport, ExportFtp exportFtp, long exportId, DateTime date, string exportType)
        {
            String fileName = "";

            if (exportType == "CRA_EXPORT")
                fileName = String.Format("{0:yyyyMMddHHmmssfff}_{1}_CRA_{2}.txt", DateTime.Now, user.DatabaseName, craRemittancePeriodCode);
            else //it is a cra wcb file
                fileName = String.Format("{0:yyyyMMddHHmmssfff}_{1}_CRA_WCB_{2}.txt", DateTime.Now, user.DatabaseName, craRemittancePeriodCode);

            //upload to rbc
            FTP.SftpManagement ftpClass = new FTP.SftpManagement();
            ftpClass.UploadFtpsFile(fileName, exportReport, exportFtp.UriName, exportFtp.Login, exportFtp.Password, exportFtp.EnableSslFlag, exportFtp.BinaryModeFlag, exportFtp.PassiveModeFlag);

            if (exportType == "CRA_EXPORT")
            {
                //update cra_export.cra_file_transmission_date
                DynamicsAccess.UpdateCraExportFileTransmissionDate(user, exportId, DateTime.Now);
            }
            else
            {
                //update cra_wcb_export.cra_file_transmission_date
                DynamicsAccess.UpdateCraWcbExportFileTransmissionDate(user, exportId, DateTime.Now);
            }

            SendEmailParameters emailParms = GetEmailParms(user, craRemittancePeriodCode, null, null, date, null, true, false, false, false, false, false, false, false, false, false);

            //Send notification once file transfer is complete
            EmailManagement.SendMessage(emailParms);
        }

        public void ExportCraFtps(DatabaseUser user, DateTime date, String craRemittancePeriodCode, RbcEftSourceDeductionParameters craParms, string rbcFtpname)
        {
            try
            {
                //check if today is a remit day
                if (RemitScheduleActivation(date, craRemittancePeriodCode))
                {
                    long insertedCraExportId = -1;
                    byte[] exportReport = null;
                    string[] exportTypes = new string[2] { "CRA_EXPORT", "CRA_WCB" }; //currently we send CRA source deductions, and CRA WCB (Nova Scotia) payments

                    //get ftp protocol details
                    ExportFtp exportFtp = PayrollAccess.GetExportFtp(user, null, "FTPS", rbcFtpname)[0]; //only will be 1 row

                    for (int i = 0; i < exportTypes.Length; i++)
                    {
                        exportReport = CreateCraExport(user, exportTypes[i], craParms, date, craRemittancePeriodCode, out insertedCraExportId);

                        if (exportReport != null)
                            FtpFilesAndSentNotification(user, craRemittancePeriodCode, exportReport, exportFtp, insertedCraExportId, date, exportTypes[i]);

                        //reset re-used vars
                        exportReport = null;
                        insertedCraExportId = -1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Schedule to remit for RQ.  The bank doesnt allow us to send early so we have to use diff logic than CRA even though it should be the same.
        //We check if today matches the effective_entry_date for records in the revenu_quebec_remittance table.
        //If there are records to be remitted, we return the data in the "coll" object to save another db query hit.
        private bool RqRemitScheduleActivation(DatabaseUser user, DateTime date, string rqRemittancePeriodCode, out RevenuQuebecRemittanceSummaryCollection coll)
        {
            bool processData = false;
            coll = null;

            //query db to see if today is a remittance date by checking revenu_quebec_remittance.effective_entry_date.
            RevenuQuebecRemittanceSummaryCollection collection = WorklinksDynamicsManagement.GetRevenueQuebecExportData(user, date, rqRemittancePeriodCode);
            if (collection != null && collection.Count > 0)
            {
                //if there are negative amounts then email support and set bucket to 0 so file can process
                CheckForNegativeAmounts(user, date, collection);

                processData = true;
                coll = collection;
            }

            return processData;
        }
        
        private void CheckForNegativeAmounts(DatabaseUser user, DateTime date, RevenuQuebecRemittanceSummaryCollection collection)
        {
            //if there are negative amounts then email support and set bucket to 0 so file can process
            System.Text.StringBuilder message = new System.Text.StringBuilder();
            bool negativeFound = false;

            foreach (RevenuQuebecRemittanceSummary remit in collection)
            {
                if (remit.QuebecTax < 0)
                {
                    message.AppendLine($"RS#: {remit.RevenuQuebecTaxId}").AppendLine("Bucket: IMPO").AppendLine($"Original Value: {Math.Round(remit.QuebecTax, 2, MidpointRounding.AwayFromZero)}").AppendLine();
                    remit.QuebecTax = 0.00M;
                    negativeFound = true;
                }
                if (remit.TotalQuebecPensionPlanContribution < 0)
                {
                    message.AppendLine($"RS#: {remit.RevenuQuebecTaxId}").AppendLine("Bucket: RRQ").AppendLine($"Original Value: {Math.Round(remit.TotalQuebecPensionPlanContribution,2, MidpointRounding.AwayFromZero)}").AppendLine();
                    remit.TotalQuebecPensionPlanContribution = 0.00M;
                    negativeFound = true;
                }
                if (remit.TotalHealthTaxLevy < 0)
                {
                    message.AppendLine($"RS#: {remit.RevenuQuebecTaxId}").AppendLine("Bucket: RAMQ").AppendLine($"Original Value: {Math.Round(remit.TotalHealthTaxLevy,2, MidpointRounding.AwayFromZero)}").AppendLine();
                    remit.TotalHealthTaxLevy = 0.00M;
                    negativeFound = true;
                }
                if (remit.TotalQuebecParentalInsurancePlanContribution < 0)
                {
                    message.AppendLine($"RS#: {remit.RevenuQuebecTaxId}").AppendLine("Bucket: RQAP").AppendLine($"Original Value: {Math.Round(remit.TotalQuebecParentalInsurancePlanContribution,2, MidpointRounding.AwayFromZero)}").AppendLine();
                    remit.TotalQuebecParentalInsurancePlanContribution = 0.00M;
                    negativeFound = true;
                }
                if (remit.EmployerWorkersCompensationPremium < 0)
                {
                    message.AppendLine($"RS#: {remit.RevenuQuebecTaxId}").AppendLine("Bucket: CSST").AppendLine($"Original Value: {Math.Round(remit.EmployerWorkersCompensationPremium,2, MidpointRounding.AwayFromZero)}").AppendLine();
                    remit.EmployerWorkersCompensationPremium = 0.00M;
                    negativeFound = true;
                }
                message.AppendLine("----------------------------------------------------");
            }
            if (negativeFound)
            {
                SendEmailParameters emailParms = GetEmailParms(user, null, null, null, date, null, false, false, false, false, false, false, false, false, false, true);

                if (emailParms != null)
                {
                    emailParms.Body += Environment.NewLine + Environment.NewLine + message.ToString();
                    EmailManagement.SendMessage(emailParms);
                }
            }
        }

        //schedule to remit for CRA
        private bool RemitScheduleActivation(DateTime date, String remittancePeriodCode)
        {
            bool processData = false;

            //logic to see if we should perform an action or not
            if (remittancePeriodCode == "REG")       //the remit period for REG is 1-EOM, so check on the 1st for the prev month
            {
                if (date.Day == 1)
                    processData = true;
            }
            else if (remittancePeriodCode == "A1")   //the remit period for A1 is 1-15, 16-EOM, so check on the 16th for current month, and 1st for the last period of the month
            {
                if (date.Day == 16 || date.Day == 1)
                    processData = true;
            }
            else if (remittancePeriodCode == "A2")   //the remit period for A2 is 1-7, 8-14, 15-21, 22-EOM, so check on the 8th, 15th, 22nd for current month, and 1st for the last period of month
            {
                if (date.Day == 8 || date.Day == 15 || date.Day == 22 || date.Day == 1)
                    processData = true;
            }

            return processData;
        }
        public SendEmailParameters GetEmailParms(DatabaseUser user, string remitPeriodCode, string codeWcbCode, string codeHealthTaxCode, DateTime date, string processGroupDescription, bool isCraRemit, bool isRqRemit, bool isWcbChequeRemit, bool isHealthChequeRemit, bool isDirectDeposit, bool isCraGarnishment, bool isPreAuthorizedDebit, bool isWcbEstimatePreAuthorizedDebit, bool isAutoPayroll, bool isNegativeRqRemit)
        {
            CodeManagement _codeMgmt = new CodeManagement();

            //get code sys values for email
            CodeCollection codeSysCollection = _codeMgmt.GetCodeTable(user.DatabaseName, "EN", CodeTableType.System);

            SendEmailParameters emailParms = new SendEmailParameters
            {
                //not sure if customers will get this so use the BCC for now (should be worklinks support)
                ToAddress = codeSysCollection["MAILBCC"].Description,

                FromAddress = codeSysCollection["MAILFROM"].Description,
                CcAddress = codeSysCollection["MAILCC"].Description,
                BccAddress = codeSysCollection["MAILBCC"].Description,

                SmtpHostName = codeSysCollection["MAILSMTP"].Description,
                SmtpPortNumber = Convert.ToInt32(codeSysCollection["MAILPORT"].Description),
                EnableSSL = Convert.ToBoolean(codeSysCollection["MAILSSL"].Description),
                SmtpLogin = codeSysCollection["MAILLOGN"].Description,
                SmtpPassword = codeSysCollection["MAILPASS"].Description
            };

            if (isCraRemit)
            {
                //get code sys values for threshold description
                CodeCollection codeCraRemittancePeriod = _codeMgmt.GetCodeTable(user.DatabaseName, "EN", CodeTableType.CodeCraRemittancePeriod);
                emailParms.Subject = "CRA REMITTANCE FILE SUBMISSION ON " + date.ToString() + " FOR CUSTOMER:  " + codeSysCollection["CMPNAME"].Description;
                emailParms.Body = "CRA REMITTANCE FILE SUBMISSION FOR:  " + codeSysCollection["CMPNAME"].Description + " WITH REMITTANCE PERIOD " + codeCraRemittancePeriod[remitPeriodCode].Description + " HAS COMPLETED.";
            }
            else if (isRqRemit)
            {
                //get code sys values for threshold description
                CodeCollection codeRqRemittancePeriod = _codeMgmt.GetCodeTable(user.DatabaseName, "EN", CodeTableType.CodeRevenuQuebecRemittancePeriod);
                emailParms.Subject = "REVENU QUEBEC REMITTANCE FILE SUBMISSION ON " + date.ToString() + " FOR CUSTOMER:  " + codeSysCollection["CMPNAME"].Description;
                emailParms.Body = "REVENU QUEBEC REMITTANCE FILE SUBMISSION FOR:  " + codeSysCollection["CMPNAME"].Description + " WITH REMITTANCE PERIOD " + codeRqRemittancePeriod[remitPeriodCode].Description + " HAS COMPLETED.";
            }
            else if (isWcbChequeRemit)
            {
                //get code sys values for remit frequency description
                CodeCollection codeChequeWcbRemittanceFrequency = _codeMgmt.GetCodeTable(user.DatabaseName, "EN", CodeTableType.WcbChequeRemittanceFrequencyCode);
                emailParms.Subject = "CHEQUE WCB REMITTANCE FILE SUBMISSION ON " + date.ToString() + " FOR CUSTOMER:  " + codeSysCollection["CMPNAME"].Description;
                emailParms.Body = "CHEQUE WCB REMITTANCE FILE SUBMISSION FOR:  " + codeSysCollection["CMPNAME"].Description + " WITH WCB CODE: " + codeWcbCode + " WITH REMITTANCE PERIOD " + codeChequeWcbRemittanceFrequency[remitPeriodCode].Description + " HAS COMPLETED.";
            }
            else if (isHealthChequeRemit)
            {
                //get code sys values for remit frequency description
                CodeCollection codeChequeWcbRemittanceFrequency = _codeMgmt.GetCodeTable(user.DatabaseName, "EN", CodeTableType.WcbChequeRemittanceFrequencyCode);
                emailParms.Subject = "CHEQUE HEALTH TAX REMITTANCE FILE SUBMISSION ON " + date.ToString() + " FOR CUSTOMER:  " + codeSysCollection["CMPNAME"].Description;
                emailParms.Body = "CHEQUE HEALTH REMITTANCE FILE SUBMISSION FOR:  " + codeSysCollection["CMPNAME"].Description + " WITH HEALTH TAX CODE: " + codeHealthTaxCode + " WITH REMITTANCE PERIOD " + codeChequeWcbRemittanceFrequency[remitPeriodCode].Description + " HAS COMPLETED.";
            }
            else if (isDirectDeposit)
            {
                emailParms.Subject = "DIRECT DEPOSIT SUBMISSION ON " + date.ToString() + " FOR CUSTOMER:  " + codeSysCollection["CMPNAME"].Description;
                emailParms.Body = "DIRECT DEPOSIT FILE SUBMISSION FOR:  " + codeSysCollection["CMPNAME"].Description + " WITH PROCESS GROUP OF: " + processGroupDescription + " HAS COMPLETED.";
            }
            else if (isPreAuthorizedDebit)
            {
                emailParms.Subject = "PRE-AUTHORIZED DEBIT SUBMISSION ON " + date.ToString() + " FOR CUSTOMER:  " + codeSysCollection["CMPNAME"].Description;
                emailParms.Body = "PRE-AUTHORIZED DEBIT FILE SUBMISSION FOR:  " + codeSysCollection["CMPNAME"].Description + " WITH PROCESS GROUP OF: " + processGroupDescription + " HAS COMPLETED.";
            }
            else if (isWcbEstimatePreAuthorizedDebit)
            {
                emailParms.Subject = "WCB ESTIMATE PRE-AUTHORIZED DEBIT SUBMISSION ON " + date.ToString() + " FOR CUSTOMER:  " + codeSysCollection["CMPNAME"].Description;
                emailParms.Body = "WCB ESTIMATE PRE-AUTHORIZED DEBIT FILE SUBMISSION FOR:  " + codeSysCollection["CMPNAME"].Description + " WITH WCB ESTIMATE CODE OF: " + codeWcbCode + " HAS COMPLETED.";
            }
            else if (isCraGarnishment)
            {
                emailParms.Subject = "CRA GARNISHMENT SUBMISSION ON " + date.ToString() + " FOR CUSTOMER:  " + codeSysCollection["CMPNAME"].Description;
                emailParms.Body = "CRA GARNISHMENT FILE SUBMISSION FOR:  " + codeSysCollection["CMPNAME"].Description + " WITH PROCESS GROUP OF: " + processGroupDescription + " HAS COMPLETED.";
            }
            else if (isAutoPayroll)
            {
                emailParms.Subject = "AUTO PAYROLL CALC AND POST COMPLETED ON " + date.ToString() + " FOR CUSTOMER:  " + codeSysCollection["CMPNAME"].Description;
                emailParms.Body = "AUTO PAYROLL CALC AND POST FOR:  " + codeSysCollection["CMPNAME"].Description + " WITH PROCESS GROUP OF: " + processGroupDescription + " HAS COMPLETED.";
            }
            else if (isNegativeRqRemit)
            {
                emailParms.Subject = "** RQ Remittance File had Negative Total ** ON " + date.ToString() + " FOR CUSTOMER:  " + codeSysCollection["CMPNAME"].Description;
                emailParms.Body = "** RQ Remittance File had Negative Total ** ON " + date.ToString() + " FOR CUSTOMER:  " + codeSysCollection["CMPNAME"].Description;
            }            

            return emailParms;
        }
        private byte[] CreateCraExport(DatabaseUser user, String reportExportName, RbcEftSourceDeductionParameters craParms, DateTime date, String craRemittancePeriodCode, out long insertedCraExportId)
        {
            insertedCraExportId = -1;

            if (reportExportName == "CRA_EXPORT")
            {
                return WorklinksDynamicsManagement.GenerateRBCEftSourceDeductionsFile(user, craParms, date, craRemittancePeriodCode, null, out insertedCraExportId);
            }
            else
            {
                return WorklinksDynamicsManagement.GenerateRBCEftWcbFile(user, craParms, date, craRemittancePeriodCode, null, out insertedCraExportId);
            }
        }
        #endregion

        #region health tax remittance
        public void CheckForUnprocessedChequeHealthTaxExports(DatabaseUser user, string rbcFtpname)
        {
            //look for unprocessed exports
            ChequeHealthTaxExportCollection chequeHealthTaxExportColl = DynamicsAccess.CheckForUnprocessedChequeHealthTaxExports(user);

            //try to resend the file(s) if unprocessed exports exist
            if (chequeHealthTaxExportColl != null && chequeHealthTaxExportColl.Count > 0)
            {
                //get ftp protocol details
                ExportFtp exportFtp = PayrollAccess.GetExportFtp(user, null, "FTPS", rbcFtpname)[0]; //only will be 1 row

                foreach (ChequeHealthTaxExport export in chequeHealthTaxExportColl)
                {
                    FtpChequeHealthTaxFilesAndSendNotification(user, export.CodeProvinceStateCd, export.CodeWcbChequeRemittanceFrequencyCd, export.Data, exportFtp, export.ChequeHealthTaxExportId, DateTime.Now);
                }
            }
        }

        private void FtpChequeHealthTaxFilesAndSendNotification(DatabaseUser user, string CodeHealthTaxCd, string codeWcbChequeRemittanceFrequencyCd, byte[] exportReport, ExportFtp exportFtp, long chequeHealthTaxExportId, DateTime date)
        {
            String fileName = String.Format("{0:yyyyMMddHHmmssfff}_{1}_CHEQUE_HEALTH_TAX_{2}.txt", DateTime.Now, user.DatabaseName, CodeHealthTaxCd);

            //upload to rbc
            FTP.SftpManagement ftpClass = new FTP.SftpManagement();
            ftpClass.UploadFtpsFile(fileName, exportReport, exportFtp.UriName, exportFtp.Login, exportFtp.Password, exportFtp.EnableSslFlag, exportFtp.BinaryModeFlag, exportFtp.PassiveModeFlag);

            //update cheque_health_tax_export.cheque_file_transmission_date
            DynamicsAccess.UpdateChequeHealthTaxExportFileTransmissionDate(user, chequeHealthTaxExportId, DateTime.Now);

            SendEmailParameters emailParms = GetEmailParms(user, codeWcbChequeRemittanceFrequencyCd, null, CodeHealthTaxCd, date, null, false, false, false, true, false, false, false, false, false, false);

            //Send notification once file transfer is complete
            EmailManagement.SendMessage(emailParms);
        }

        public void ExportChequeHealthTaxFtps(DatabaseUser user, DateTime date, string rbcFtpname)
        {
            try
            {
                //check if we have to send the EFT file
                ChequeHealthTaxRemittanceScheduleCollection invoiceScheduleCollection = WorklinksDynamicsManagement.GetChequeHealthTaxRemittanceSchedule(user, date);

                if (invoiceScheduleCollection != null && invoiceScheduleCollection.Count > 0)
                {
                    //get ftp protocol details
                    ExportFtp exportFtp = PayrollAccess.GetExportFtp(user, null, "FTPS", rbcFtpname)[0]; //only will be 1 row
                    RbcEftEdiParameters parms = GetRbcEftParmsForChequeRemittance(user);

                    //we have to send an EFT file for each item
                    foreach (ChequeHealthTaxRemittanceSchedule remit in invoiceScheduleCollection)
                    {
                        CreateChequeHealthTaxRemittanceFile(user, remit, exportFtp, date, parms);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void CreateChequeHealthTaxRemittanceFile(DatabaseUser user, ChequeHealthTaxRemittanceSchedule remit, ExportFtp exportFtp, DateTime date, RbcEftEdiParameters parms)
        {
            try
            {
                byte[] exportReport = null;
                long insertedChequeHealthTaxExportId = -1;

                exportReport = GenerateRbcChequeHealthTaxRemittanceFile(user, parms, remit, date, null, out insertedChequeHealthTaxExportId);

                if (exportReport != null)
                    FtpChequeHealthTaxFilesAndSendNotification(user, remit.CodeHealthTaxCd, remit.CodeWcbChequeRemittanceFrequencyCd, exportReport, exportFtp, insertedChequeHealthTaxExportId, DateTime.Now);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public byte[] GenerateRbcChequeHealthTaxRemittanceFile(DatabaseUser user, RbcEftEdiParameters parms, ChequeHealthTaxRemittanceSchedule remit, DateTime date, long? chequeHealthTaxExportId, out long insertedChequeHealthTaxExportId)
        {
            try
            {
                //this will keep track of the inserted cheque_health_tax_export record
                insertedChequeHealthTaxExportId = -1;

                byte[] eftFile = null;
                ChequeHealthTaxExportCollection chequeHealthTaxExportColl = null;

                //check if file exists on the database, maybe used by future screen that may show what was stored in the db
                if (chequeHealthTaxExportId != null)
                    chequeHealthTaxExportColl = DynamicsAccess.CheckForChequeHealthTaxRemittanceOnDatabase(user, chequeHealthTaxExportId);

                //if found on db, return it, otherwise create it
                if (chequeHealthTaxExportColl != null && chequeHealthTaxExportColl.Count > 0)
                    eftFile = chequeHealthTaxExportColl[0].Data;
                else
                {
                    ChequeHealthTaxRemittanceSummaryCollection collection = GetRbcChequeHealthTaxRemittanceFileData(user, remit);

                    if (collection != null && collection.Count > 0)
                    {
                        //get sequence number from db
                        long headerSequenceNumber = DynamicsAccess.GetSequenceNumber(user, "GLOBAL_RBC_HEADER_SEQUENCE");
                        //there will only be one health tax payment in this cheque file so we will use the GLOBAL_RBC_TRANSACTION_SEQUENCE in creating the file.
                        long transactionSequenceNumber = DynamicsAccess.GetSequenceNumber(user, "GLOBAL_RBC_TRANSACTION_SEQUENCE");

                        eftFile = CreateRbcChequeHealthTaxRemittanceFile(user, collection, parms, remit, headerSequenceNumber, transactionSequenceNumber);

                        if (eftFile != null)
                        {
                            ChequeHealthTaxExport chequeHealthTaxExport = new ChequeHealthTaxExport()
                            {
                                Data = eftFile,
                                RbcSequenceNumber = headerSequenceNumber,
                                RbcChequeNumber = transactionSequenceNumber,
                                CodeProvinceStateCd = remit.CodeProvinceStateCd,
                                RemittanceDate = remit.RemittanceDate,
                                CodeWcbChequeRemittanceFrequencyCd = remit.CodeWcbChequeRemittanceFrequencyCd
                            };

                            DynamicsAccess.InsertChequeHealthTaxExportToDatabase(user, chequeHealthTaxExport);

                            //assign the id of the cheque_health_tax_export just inserted
                            insertedChequeHealthTaxExportId = chequeHealthTaxExport.ChequeHealthTaxExportId;

                            //update cheque_health_tax_remittance with processed flag and key from cheque_health_tax_export table
                            DynamicsAccess.UpdateChequeHealthTaxRemittanceChequeHealthTaxExportIdColumn(user, remit.RemittanceDate, insertedChequeHealthTaxExportId, remit.HealthTaxId);
                        }
                    }
                }

                return eftFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GenerateRbcChequeWcbRemittanceFile", ex);
                throw;
            }
        }
        public ChequeHealthTaxRemittanceSummaryCollection GetRbcChequeHealthTaxRemittanceFileData(DatabaseUser user, ChequeHealthTaxRemittanceSchedule remit)
        {
            try
            {
                return DynamicsAccess.GetRbcChequeHealthTaxRemittanceFileData(user, remit);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GetRbcChequeHealthTaxRemittanceFileData", ex);
                throw;
            }
        }
        public byte[] CreateRbcChequeHealthTaxRemittanceFile(DatabaseUser user, ChequeHealthTaxRemittanceSummaryCollection coll, RbcEftEdiParameters parms, ChequeHealthTaxRemittanceSchedule remit, long headerSequenceNumber, long transactionSequenceNumber)
        {
            try
            {
                return DynamicsAccess.CreateRbcChequeHealthTaxRemittanceFile(user, coll, parms, remit, headerSequenceNumber, transactionSequenceNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - CreateRbcChequeHealthTaxRemittanceFile", ex);
                throw;
            }
        }

        //The invoice is no longer being send separately but being added to the invoice created during the POST of a payoll.
        //Commenting out the logic instead of deleting it incase this needs to be undone in the future.

        //public void SendHealthTaxInvoiceFileViaEmail(DatabaseUser user, ChequeHealthTaxRemittanceSchedule remit, DateTime date, SendEmailParameters emailParms, string fileMask)
        //{
        //    emailParms.FileName = String.Format(fileMask, DateTime.Now, user.DatabaseName, remit.CodeProvinceStateCd);

        //    //attach the invoice to the email
        //    emailParms.Attachment = new ReportManagement().GetReport(user, "ChequeHealthTaxInvoice", -1, null, "PDF", null, null, null, null, null, null, null, remit.RemittanceDate, remit.ReportingPeriodEndDate, null, remit.CodeProvinceStateCd);

        //    //Send invoice
        //    EmailManagement.SendMessage(emailParms.ToAddress, emailParms.FromAddress, emailParms.CcAddress, emailParms.BccAddress, emailParms.Subject, emailParms.Body, emailParms.SmtpHostName, emailParms.SmtpPortNumber, emailParms.EnableSSL, emailParms.SmtpLogin, emailParms.SmtpPassword, emailParms.Attachment, emailParms.FileName);

        //    //insert into the cheque_health_tax_invoice_export, email_sent_by_system tables.
        //    StoreInvoice(user, emailParms, null, remit);

        //    //clear filename and attachment
        //    emailParms.FileName = null;
        //    emailParms.Attachment = null;
        //}
        #endregion

        #region cheque wcb remittance
        public void CheckForUnprocessedChequeWcbExports(DatabaseUser user, string rbcFtpname)
        {
            //look for unprocessed exports
            ChequeWcbExportCollection chequeWcbExportColl = DynamicsAccess.CheckForUnprocessedChequeWcbExports(user);

            //try to resend the file(s) if unprocessed exports exist
            if (chequeWcbExportColl != null && chequeWcbExportColl.Count > 0)
            {
                //get ftp protocol details
                ExportFtp exportFtp = PayrollAccess.GetExportFtp(user, null, "FTPS", rbcFtpname)[0]; //only will be 1 row

                foreach (ChequeWcbExport export in chequeWcbExportColl)
                {
                    FtpChequeWcbFilesAndSendNotification(user, export.CodeWsibCd, export.CodeWcbChequeRemittanceFrequencyCd, export.Data, exportFtp, export.ChequeWcbExportId, DateTime.Now);
                }
            }
        }

        private void FtpChequeWcbFilesAndSendNotification(DatabaseUser user, string CodeWsibCd, string codeWcbChequeRemittanceFrequencyCd, byte[] exportReport, ExportFtp exportFtp, long chequeWcbExportId, DateTime date)
        {
            String fileName = String.Format("{0:yyyyMMddHHmmssfff}_{1}_CHEQUE_WCB_{2}.txt", DateTime.Now, user.DatabaseName, CodeWsibCd);

            //upload to rbc
            FTP.SftpManagement ftpClass = new FTP.SftpManagement();
            ftpClass.UploadFtpsFile(fileName, exportReport, exportFtp.UriName, exportFtp.Login, exportFtp.Password, exportFtp.EnableSslFlag, exportFtp.BinaryModeFlag, exportFtp.PassiveModeFlag);

            //update cheque_wcb_export.cheque_file_transmission_date
            DynamicsAccess.UpdateChequeWcbExportFileTransmissionDate(user, chequeWcbExportId, DateTime.Now);

            SendEmailParameters emailParms = GetEmailParms(user, codeWcbChequeRemittanceFrequencyCd, CodeWsibCd, null, date, null, false, false, true, false, false, false, false, false, false, false);

            //Send notification once file transfer is complete
            EmailManagement.SendMessage(emailParms);
        }

        public void ExportChequeWcbFtps(DatabaseUser user, DateTime date, string rbcFtpname)
        {
            try
            {
                //This wcb invoice is now being used to only send wcb estimates.  The wcb "actuals" will be on the direct deposit invoice during the POST of a payroll.
                ChequeWcbRemittanceScheduleCollection invoiceScheduleCollection = WorklinksDynamicsManagement.GetChequeWcbRemittanceSchedule(user, date, true);

                if (invoiceScheduleCollection != null && invoiceScheduleCollection.Count > 0)
                {
                    //get email parms for cheque invoices
                    String fileMask = "";
                    SendEmailParameters emailParms = GetChequeInvoiceEmailParms(user, "WCB", out fileMask);

                    //we have to submit an invoice for each item
                    foreach (ChequeWcbRemittanceSchedule remit in invoiceScheduleCollection)
                    {
                        SendWcbInvoiceFileViaEmail(user, remit, date, emailParms, fileMask);
                    }
                }

                invoiceScheduleCollection = null;

                //check if we have to send the EFT file
                invoiceScheduleCollection = WorklinksDynamicsManagement.GetChequeWcbRemittanceSchedule(user, date, false);

                if (invoiceScheduleCollection != null && invoiceScheduleCollection.Count > 0)
                {
                    //get ftp protocol details
                    ExportFtp exportFtp = PayrollAccess.GetExportFtp(user, null, "FTPS", rbcFtpname)[0]; //only will be 1 row
                    RbcEftEdiParameters parms = GetRbcEftParmsForChequeRemittance(user);
                    long? insertedPreAuthorizedDebitKey = null;

                    //we have to send an EFT file for each item
                    foreach (ChequeWcbRemittanceSchedule remit in invoiceScheduleCollection)
                    {
                        //create the pre-authorized debit file FOR WCB ESTIMATES ONLY if customer is using pre-authorized debit
                        if (parms.PreAuthorizedDebitFlag && parms.PreAuthorizedDebitFileType == "RBC_CANADA")
                            insertedPreAuthorizedDebitKey = GenerateWcbEstimatePreAuthorizedDebitFile(user, remit, exportFtp, date, parms);

                        //create the cheque wcb file.
                        CreateChequeWcbRemittanceFile(user, remit, exportFtp, date, parms, insertedPreAuthorizedDebitKey);

                        //reset variable
                        insertedPreAuthorizedDebitKey = null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private long? GenerateWcbEstimatePreAuthorizedDebitFile(DatabaseUser user, ChequeWcbRemittanceSchedule remit, ExportFtp exportFtp, DateTime date, RbcEftEdiParameters parms)
        {
            try
            {
                long? insertedExportId = null;
                byte[] eftFile = null;

                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    ChequeWcbRemittanceSummaryCollection collection = GetRbcChequeWcbRemittanceFileData(user, remit, true);

                    //if PAD file was already created then ChequeWcbPreAuthorizedDebitExportId would have a value
                    if (collection != null && collection.Count > 0 && collection[0].ChequeWcbPreAuthorizedDebitExportId == null)
                    {
                        //store results in existing PreAuthorizedDebitTotals to use re-existing method
                        PreAuthorizedDebitTotals preAuthDets = new PreAuthorizedDebitTotals()
                        {
                            Amount = collection[0].PaymentAmount,
                            BusinessTaxNumber = collection[0].BusinessTaxNumber,
                            CodePreAuthorizedBankCd = collection[0].CodePreAuthorizedBankCd,
                            PreAuthorizedTransitNumber = collection[0].PreAuthorizedTransitNumber,
                            PreAuthorizedAccountNumber = collection[0].PreAuthorizedAccountNumber,
                            PaymentDueDate = Convert.ToDateTime(collection[0].PaymentDueDate),
                            CodeWcbCd = collection[0].CodeWcbCd
                        };
                        //convert to PreAuthorizedDebitTotalsCollection object so we can re-use the existing method
                        PreAuthorizedDebitTotalsCollection preAuthCollection = new PreAuthorizedDebitTotalsCollection();
                        preAuthCollection.Add(preAuthDets);

                        //get sequence number from db
                        long headerSequenceNumber = _dynamicsAccess.GetSequenceNumber(user, "GLOBAL_RBC_HEADER_SEQUENCE");

                        //create file
                        eftFile = _dynamicsAccess.CreatePreAuthorizedDebitFile(user, preAuthCollection, parms, headerSequenceNumber);

                        if (eftFile != null)
                        {
                            ChequeWcbPreAuthorizedDebitExport padExport = new ChequeWcbPreAuthorizedDebitExport()
                            {
                                RbcSequenceNumber = headerSequenceNumber,
                                Data = eftFile,
                                CodeWsibCd = preAuthDets.CodeWcbCd,
                                RemittanceDate = remit.RemittanceDate,
                                CodeWcbChequeRemittanceFrequencyCd = remit.CodeWcbChequeRemittanceFrequencyCd,
                                TotalAmount = preAuthCollection.TotalAmount
                            };

                            //store file in the database
                            _dynamicsAccess.InsertWcbEstimatePreAuthorizedDebitExportToDatabase(user, padExport);

                            //assign the id of the cheque_wcb_pre_authorized_debit_export just inserted
                            insertedExportId = padExport.ChequeWcbPreAuthorizedDebitExportId;

                            //this will FTP the file, update the cheque_wcb_pre_authorized_debit_export.cheque_file_transmission_date, and send an email to support
                            WorklinksDynamicsManagement.FtpPreAuthorizedDebitFileAndSendNotification(user, parms.RbcFtpName, null, null, padExport);
                        }
                    }
                    //if PAD file was already created
                    else if (collection != null && collection.Count > 0 && collection[0].ChequeWcbPreAuthorizedDebitExportId != null)
                    {
                        insertedExportId = collection[0].ChequeWcbPreAuthorizedDebitExportId;
                    }
                    scope.Complete();
                }

                return insertedExportId;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "WorkLinksDynamicsManagement - CreatePreAuthorizedDebitFileForChequeWcbFile", ex);
                throw;
            }
        }

        //This wcb invoice is now being used to only send wcb estimates.  The wcb "actuals" will be on the direct deposit invoice during the POST of a payroll.
        private void SendWcbInvoiceFileViaEmail(DatabaseUser user, ChequeWcbRemittanceSchedule remit, DateTime date, SendEmailParameters emailParms, string fileMask)
        {
            emailParms.FileName = String.Format(fileMask, DateTime.Now, user.DatabaseName, remit.CodeWcbCd);

            //attach the invoice to the email
            emailParms.Attachment = new ReportManagement().GetReport(user, "ChequeWcbInvoice", -1, null, "PDF", null, null, null, null, null, null, remit.RemittanceDate, remit.ReportingPeriodEndDate, remit.CodeWcbCd);

            //Send invoice
            EmailManagement.SendMessage(emailParms);

            //insert into the cheque_wcb_invoice_export, email_sent_by_system tables.
            StoreInvoice(user, emailParms, remit, null);

            //clear filename and attachment
            emailParms.FileName = null;
            emailParms.Attachment = null;
        }

        private void StoreInvoice(DatabaseUser user, SendEmailParameters emailParms, ChequeWcbRemittanceSchedule wcbRemit, ChequeHealthTaxRemittanceSchedule healthRemit)
        {
            try
            {
                if (wcbRemit != null)
                {
                    ChequeWcbInvoiceExport invoice = new ChequeWcbInvoiceExport();
                    invoice.CodeWsibCd = wcbRemit.CodeWcbCd;
                    invoice.RemittanceDate = wcbRemit.RemittanceDate;

                    invoice.EmailSentBySystem.EmailTo = emailParms.ToAddress;
                    invoice.EmailSentBySystem.EmailFrom = emailParms.FromAddress;
                    invoice.EmailSentBySystem.EmailCC = emailParms.CcAddress;
                    invoice.EmailSentBySystem.EmailBCC = emailParms.BccAddress;
                    invoice.EmailSentBySystem.EmailSubject = emailParms.Subject;
                    invoice.EmailSentBySystem.EmailBody = emailParms.Body;
                    invoice.EmailSentBySystem.EmailSmtp = emailParms.SmtpHostName;
                    invoice.EmailSentBySystem.EmailEnableSSL = emailParms.EnableSSL;
                    invoice.EmailSentBySystem.EmailPort = emailParms.SmtpPortNumber;
                    invoice.EmailSentBySystem.EmailFilename = emailParms.FileName;
                    invoice.EmailSentBySystem.EmailData = emailParms.Attachment;

                    InsertInvoice(user, invoice, null);
                }
                else
                {
                    ChequeHealthTaxInvoiceExport invoice = new ChequeHealthTaxInvoiceExport();
                    invoice.CodeProvinceStateCd = healthRemit.CodeProvinceStateCd;
                    invoice.RemittanceDate = healthRemit.RemittanceDate;

                    invoice.EmailSentBySystem.EmailTo = emailParms.ToAddress;
                    invoice.EmailSentBySystem.EmailFrom = emailParms.FromAddress;
                    invoice.EmailSentBySystem.EmailCC = emailParms.CcAddress;
                    invoice.EmailSentBySystem.EmailBCC = emailParms.BccAddress;
                    invoice.EmailSentBySystem.EmailSubject = emailParms.Subject;
                    invoice.EmailSentBySystem.EmailBody = emailParms.Body;
                    invoice.EmailSentBySystem.EmailSmtp = emailParms.SmtpHostName;
                    invoice.EmailSentBySystem.EmailEnableSSL = emailParms.EnableSSL;
                    invoice.EmailSentBySystem.EmailPort = emailParms.SmtpPortNumber;
                    invoice.EmailSentBySystem.EmailFilename = emailParms.FileName;
                    invoice.EmailSentBySystem.EmailData = emailParms.Attachment;

                    InsertInvoice(user, null, invoice);
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - StoreInvoice", ex);
                throw;
            }
        }

        //not used by worklinks at the moment but can be wired in for the future.  This is a way to get the invoice stored in the "email_sent_by_system" table
        public EmailSentBySystemCollection GetInvoiceByEmailSentBySystemId(DatabaseUser user, long emailSentBySystemId)
        {
            return DynamicsAccess.GetInvoiceByEmailSentBySystemId(user, emailSentBySystemId);
        }

        public void InsertInvoice(DatabaseUser user, ChequeWcbInvoiceExport wcbExport, ChequeHealthTaxInvoiceExport healthExport)
        {
            try
            {
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    if (wcbExport != null)
                    {
                        DynamicsAccess.InsertEmailSentBySystem(user, wcbExport.EmailSentBySystem);
                        DynamicsAccess.InsertChequeWcbInvoiceExport(user, wcbExport);
                    }

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - InsertInvoice", ex);
                throw;
            }
        }

        private SendEmailParameters GetChequeInvoiceEmailParms(DatabaseUser user, string chequeType, out string fileMask)
        {
            CodeManagement _codeMgmt = new CodeManagement();

            //get code sys values for email
            CodeCollection codeSysCollection = _codeMgmt.GetCodeTable(user.DatabaseName, "EN", CodeTableType.System);

            SendEmailParameters emailParms = new SendEmailParameters();

            emailParms.ToAddress = codeSysCollection["MAILTO"].Description;
            emailParms.FromAddress = codeSysCollection["MAILFROM"].Description;
            emailParms.CcAddress = codeSysCollection["MAILCC"].Description;
            emailParms.BccAddress = codeSysCollection["MAILBCC"].Description;
            emailParms.SmtpHostName = codeSysCollection["MAILSMTP"].Description;
            emailParms.SmtpPortNumber = Convert.ToInt32(codeSysCollection["MAILPORT"].Description);
            emailParms.EnableSSL = Convert.ToBoolean(codeSysCollection["MAILSSL"].Description);
            emailParms.SmtpLogin = codeSysCollection["MAILLOGN"].Description;
            emailParms.SmtpPassword = codeSysCollection["MAILPASS"].Description;

            //values below are for either WCB or HEALTH, stored in the code system table

            //body
            emailParms.Subject = (chequeType == "WCB") ? codeSysCollection["CWCBSUBJ"].Description : codeSysCollection["CHTXSUBJ"].Description;
            //subject
            emailParms.Body = (chequeType == "WCB") ? codeSysCollection["CWCBBODY"].Description : codeSysCollection["CHTXBODY"].Description;
            //filename mask
            fileMask = (chequeType == "WCB") ? codeSysCollection["CWCBFILE"].Description : codeSysCollection["CHTXFILE"].Description;

            return emailParms;
        }

        private void CreateChequeWcbRemittanceFile(DatabaseUser user, ChequeWcbRemittanceSchedule remit, ExportFtp exportFtp, DateTime date, RbcEftEdiParameters parms, long? insertedPreAuthorizedDebitKey)
        {
            try
            {
                byte[] exportReport = null;
                long insertedChequeWcbExportId = -1;

                exportReport = GenerateRbcChequeWcbRemittanceFile(user, parms, remit, date, null, out insertedChequeWcbExportId, insertedPreAuthorizedDebitKey);

                if (exportReport != null)
                    FtpChequeWcbFilesAndSendNotification(user, remit.CodeWcbCd, remit.CodeWcbChequeRemittanceFrequencyCd, exportReport, exportFtp, insertedChequeWcbExportId, DateTime.Now);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public byte[] GenerateRbcChequeWcbRemittanceFile(DatabaseUser user, RbcEftEdiParameters parms, ChequeWcbRemittanceSchedule remit, DateTime date, long? chequeWcbExportId, out long insertedChequeWcbExportId, long? insertedPreAuthorizedDebitKey)
        {
            try
            {
                //this will keep track of the inserted cheque_wcb_export record
                insertedChequeWcbExportId = -1;

                byte[] eftFile = null;
                ChequeWcbExportCollection chequeWcbExportColl = null;

                //check if file exists on the database, maybe used by future screen that may show what was stored in the db
                if (chequeWcbExportId != null)
                    chequeWcbExportColl = DynamicsAccess.CheckForChequeWcbRemittanceOnDatabase(user, chequeWcbExportId);

                //if found on db, return it, otherwise create it
                if (chequeWcbExportColl != null && chequeWcbExportColl.Count > 0)
                    eftFile = chequeWcbExportColl[0].Data;
                else
                {
                    ChequeWcbRemittanceSummaryCollection collection = GetRbcChequeWcbRemittanceFileData(user, remit);

                    if (collection != null && collection.Count > 0)
                    {
                        //get sequence number from db
                        long headerSequenceNumber = DynamicsAccess.GetSequenceNumber(user, "GLOBAL_RBC_HEADER_SEQUENCE");
                        //there will only be one wcb payment in this cheque file so we will use the GLOBAL_RBC_TRANSACTION_SEQUENCE in creating the file.
                        long transactionSequenceNumber = DynamicsAccess.GetSequenceNumber(user, "GLOBAL_RBC_TRANSACTION_SEQUENCE");

                        eftFile = CreateRbcChequeWcbRemittanceFile(user, collection, parms, remit, headerSequenceNumber, transactionSequenceNumber);

                        if (eftFile != null)
                        {
                            ChequeWcbExport chequeWcbExport = new ChequeWcbExport()
                            {
                                Data = eftFile,
                                RbcSequenceNumber = headerSequenceNumber,
                                RbcChequeNumber = transactionSequenceNumber,
                                CodeWsibCd = remit.CodeWcbCd,
                                RemittanceDate = remit.RemittanceDate,
                                ReportingPeriodStartDate = remit.ReportingPeriodStartDate,
                                ReportingPeriodEndDate = remit.ReportingPeriodEndDate,
                                CodeWcbChequeRemittanceFrequencyCd = remit.CodeWcbChequeRemittanceFrequencyCd,
                                ChequeWcbPreAuthorizedDebitExportId = insertedPreAuthorizedDebitKey
                            };

                            DynamicsAccess.InsertChequeWcbExportToDatabase(user, chequeWcbExport);

                            //assign the id of the cheque_wcb_export just inserted
                            insertedChequeWcbExportId = chequeWcbExport.ChequeWcbExportId;

                            //update cheque_wcb_remittance with processed flag and key from cheque_wcb_export table
                            DynamicsAccess.UpdateChequeWcbRemittanceChequeWcbExportIdColumn(user, remit.ReportingPeriodEndDate, insertedChequeWcbExportId, remit.CodeWcbCd);
                        }
                    }
                }

                return eftFile;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GenerateRbcChequeWcbRemittanceFile", ex);
                throw;
            }
        }

        public ChequeWcbRemittanceSummaryCollection GetRbcChequeWcbRemittanceFileData(DatabaseUser user, ChequeWcbRemittanceSchedule remit, bool estimatesOnly = false)
        {
            try
            {
                return DynamicsAccess.GetRbcChequeWcbRemittanceFileData(user, remit, estimatesOnly); //"estimatesOnly" used only in creating pre-authorized debit file for wcb estimates to only grab the estimate amounts
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - GetRbcChequeWcbRemittanceFileData", ex);
                throw;
            }
        }

        public byte[] CreateRbcChequeWcbRemittanceFile(DatabaseUser user, ChequeWcbRemittanceSummaryCollection coll, RbcEftEdiParameters parms, ChequeWcbRemittanceSchedule remit, long headerSequenceNumber, long transactionSequenceNumber)
        {
            try
            {
                return DynamicsAccess.CreateRbcChequeWcbRemittanceFile(user, coll, parms, remit, headerSequenceNumber, transactionSequenceNumber);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - CreateRbcChequeWcbRemittanceFile", ex);
                throw;
            }
        }
        private RbcEftEdiParameters GetRbcEftParmsForChequeRemittance(DatabaseUser user)
        {
            CodeManagement _codeMgmt = new CodeManagement();

            //get code sys values for rbc edi file
            CodeCollection codeSysCollection = _codeMgmt.GetCodeTable(user.DatabaseName, "EN", CodeTableType.System);

            RbcEftEdiParameters rbcparms = new RbcEftEdiParameters();

            rbcparms.IsaSenderInterchangeID = codeSysCollection["RBC_ID1"].Description;
            rbcparms.IsaReceiverInterchangeID = codeSysCollection["RBC_ID2"].Description;
            rbcparms.IsaTestIndicator = codeSysCollection["RBC_IND"].Description;
            rbcparms.DirectDepositBankCodeTransitNumber = codeSysCollection["RBC_FI_T"].Description;
            rbcparms.DirectDepositAccountNumber = codeSysCollection["RBC_FI_A"].Description;
            rbcparms.CompanyName = codeSysCollection["CMPNAME"].Description;
            rbcparms.CompanyShortName = codeSysCollection["CMPSHORT"].Description;
            rbcparms.ChequesChequeIssuance = codeSysCollection["RBC_ROUT"].Description;
            rbcparms.DescriptionPayeeMatch = codeSysCollection["RBC_REF4"].Description;
            rbcparms.PreAuthorizedDebitClientNumber = codeSysCollection["PACLIENT"].Description;            //only used for pre authorized debit file creation
            rbcparms.PreAuthorizedDebitFlag = Convert.ToBoolean(codeSysCollection["PA_DEBIT"].Description); //only used for pre authorized debit file creation
            rbcparms.PreAuthorizedDebitFileType = codeSysCollection["PAD_TYPE"].Description;                //only used for pre authorized debit file creation
            rbcparms.RbcFtpName = codeSysCollection["RBC_FTP"].Description;

            return rbcparms;
        }
        #endregion

        #region revenu quebec export

        public void ExportRqFtps(DatabaseUser user, DateTime date, string rqRemittancePeriodCode, RbcRqSourceDeductionParameters rqParms, string rbcFtpname)
        {
            RevenuQuebecRemittanceSummaryCollection rqRemitSummaryColl = null;
            try
            {
                //check if today is a remit day
                if (RqRemitScheduleActivation(user, date, rqRemittancePeriodCode, out rqRemitSummaryColl))
                {
                    long insertedRqExportId = -1;
                    byte[] exportReport = null;

                    //get ftp protocol details
                    ExportFtp exportFtp = PayrollAccess.GetExportFtp(user, null, "FTPS", rbcFtpname)[0]; //only will be 1 row

                    exportReport = WorklinksDynamicsManagement.GenerateRevenuQuebecSourceDeductionsFileFile(user, rqParms, date, rqRemittancePeriodCode, rqRemitSummaryColl, null, out insertedRqExportId);

                    if (exportReport != null)
                        FtpRqFilesAndSentNotification(user, rqRemittancePeriodCode, exportReport, exportFtp, insertedRqExportId, date);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FtpRqFilesAndSentNotification(DatabaseUser user, string rqRemittancePeriodCode, byte[] exportReport, ExportFtp exportFtp, long rqExportId, DateTime date)
        {
            String fileName = String.Format("{0:yyyyMMddHHmmssfff}_{1}_RQ_{2}.txt", DateTime.Now, user.DatabaseName, rqRemittancePeriodCode);

            //upload to rbc
            FTP.SftpManagement ftpClass = new FTP.SftpManagement();
            ftpClass.UploadFtpsFile(fileName, exportReport, exportFtp.UriName, exportFtp.Login, exportFtp.Password, exportFtp.EnableSslFlag, exportFtp.BinaryModeFlag, exportFtp.PassiveModeFlag);

            //update revenu_quebec_export.revenu_quebec_file_transmission_date
            DynamicsAccess.UpdateRevenuQuebecExportFileTransmissionDate(user, rqExportId, DateTime.Now);

            SendEmailParameters emailParms = GetEmailParms(user, rqRemittancePeriodCode, null, null, date, null, false, true, false, false, false, false, false, false, false, false);

            //Send notification once file transfer is complete
            EmailManagement.SendMessage(emailParms);
        }

        public int CheckForUnprocessedRevenuQuebecExports(DatabaseUser user, string rbcFtpname)
        {
            int numberOfUnprocessedFiles = 0;

            //look for unprocessed exports
            RevenuQuebecExportCollection rqExportColl = DynamicsAccess.CheckForUnprocessedRevenuQuebecRemittanceOnDatabase(user);

            if (rqExportColl != null && rqExportColl.Count > 0)
            {
                numberOfUnprocessedFiles = rqExportColl.Count;

                //get ftp protocol details
                ExportFtp exportFtp = PayrollAccess.GetExportFtp(user, null, "FTPS", rbcFtpname)[0]; //only will be 1 row

                foreach (RevenuQuebecExport export in rqExportColl)
                    FtpRqFilesAndSentNotification(user, export.CodeRevenuQuebecRemittancePeriodCd, export.Data, exportFtp, export.RevenuQuebecExportId, DateTime.Now);
            }

            return numberOfUnprocessedFiles;
        }
        #endregion

        private byte[] CreateReport(DatabaseUser user, long? reportId, String exportCode, String year, String showDesc, String sortOrder, long payrollProcessId, String fileMaskFormat, out String fileName)
        {
            Report report = GetReportById(user, Convert.ToInt64(reportId))[0];
            fileName = String.Format(fileMaskFormat, payrollProcessId);

            return GetReport(user, report.Name, payrollProcessId, "", exportCode, year, "", "", showDesc, "", sortOrder);
        }
        private byte[] CreateExport(DatabaseUser user, String reportExportName, String revenueQuebecTaxId, bool useQuebecTaxId, String showDesc, String sortOrder, String fileMaskFormat, String citiEftFileName, String citiChequeFileName, String eftClientNumber, String citiChequeAccountNumber, String companyName, String companyShortName, String massPayslipInternalFileFormat, long payrollProcessId, out String fileName, string originatorId, string eftType, int maxDegreeOfParallelism)
        {
            if (reportExportName == "GL_TEXT")
            {
                fileName = GenerateFileName(user, fileMaskFormat, payrollProcessId);
                return CreateGLExportFile(user, payrollProcessId, reportExportName.ToLower(), "", "");
            }
            else if (reportExportName == "EMPOWER_TAX")
            {
                fileName = GenerateFileName(user, fileMaskFormat, payrollProcessId);
                return WorklinksDynamicsManagement.CreateEmpowerRemittanceExport(user, payrollProcessId, revenueQuebecTaxId, useQuebecTaxId);
            }
            else if (reportExportName == "PENSION_EXPORT")
            {
                fileName = GenerateFileName(user, fileMaskFormat, payrollProcessId);
                return CreateMercerPensionExportFile(user, payrollProcessId, showDesc, sortOrder);
            }
            else if (reportExportName == "CITI_EFT_CHEQ")
            {
                fileName = WorklinksDynamicsManagement.GetCitiNamingFormat(user, fileMaskFormat, payrollProcessId);
                return WorklinksDynamicsManagement.CreateCitiEftAndChequeFile(user, payrollProcessId, citiEftFileName, citiChequeFileName, eftClientNumber, citiChequeAccountNumber, companyName, companyShortName, null, originatorId, eftType);
            }
            else if (reportExportName == "MASS_PAY_SLIP_FILE")
            {
                fileName = GenerateFileName(user, fileMaskFormat, payrollProcessId);
                return GetReportForAllEmployeesOfThisPayProcess(user, massPayslipInternalFileFormat, payrollProcessId, showDesc, sortOrder, maxDegreeOfParallelism);
            }
            else
            {
                fileName = "";
                return null;
            }
        }
        public ReportCollection GetReportById(DatabaseUser user, long reportId)
        {
            return ReportAccess.GetReportById(user, reportId);
        }
        public ReportCollection GetReportRecord(DatabaseUser user, String reportName)
        {
            return ReportAccess.GetReport(user, reportName);
        }

        #region report export
        public ReportExportCollection GetReportExport(DatabaseUser user, long? exportId)
        {
            return ReportAccess.GetReportExport(user, exportId);
        }
        public void UpdateReportExport(DatabaseUser user, ReportExport export)
        {
            try
            {
                ReportAccess.UpdateReportExport(user, export);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - UpdateReportExport", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                {
                    EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                    throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                }
                else
                    throw;
            }
        }
        public ReportExport InsertReportExport(DatabaseUser user, ReportExport export)
        {
            return ReportAccess.InsertReportExport(user, export);
        }
        public void DeleteReportExport(DatabaseUser user, ReportExport export)
        {
            try
            {
                ReportAccess.DeleteReportExport(user, export);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ReportManagement - DeleteReportExport", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                {
                    EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                    throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                }
                else
                    throw;
            }
        }
        #endregion

        /*
        #region report language
        public ReportLanguageCollection GetReportLanguage(DatabaseUser user, long reportId)
        {
            return ReportAccess.GetReportLanguage(user, reportId);
        }
        #endregion
    */

        #region STE ENGINE TESTING
        public void InsertPayrollEngineOutput(String databaseName, PayrollEngineOutput payEngineOutput)
        {
            ReportAccess.InsertPayrollEngineOutput(databaseName, payEngineOutput);
        }
        #endregion
    }
}