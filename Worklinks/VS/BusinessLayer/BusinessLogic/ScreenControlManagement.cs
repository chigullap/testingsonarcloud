﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.ComponentModel;
using System.Transactions;

using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.DataLayer.DataAccess;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class ScreenControlManagement
    {
        #region fields
        private ScreenControlAccess _screenControlAccess = null;
        #endregion

        #region properties
        private ScreenControlAccess ScreenControlAccess
        {
            get
            {
                if (_screenControlAccess == null)
                    _screenControlAccess = new ScreenControlAccess();
                return _screenControlAccess;
            }
        }
        #endregion

        #region ScreenControl
        public void MergeScreenControls(DatabaseUser user, ScreenControl[] screenControlArray)
        {
            try
            {
                ScreenControlAccess.Merge(user, screenControlArray);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ScreenControlManagement - MergeScreenControls", ex);
                throw;
            }
        }
        #endregion

    }
}
