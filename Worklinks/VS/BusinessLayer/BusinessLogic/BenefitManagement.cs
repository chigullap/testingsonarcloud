﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.DataLayer.DataAccess;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class BenefitManagement
    {
        #region fields
        BenefitAccess _benefitAccess = null;
        #endregion

        #region properties
        private BenefitAccess BenefitAccess
        {
            get
            {
                if (_benefitAccess == null)
                    _benefitAccess = new BenefitAccess();

                return _benefitAccess;
            }
        }
        #endregion


        #region benefit policy
        public BenefitPolicyCollection GetBenefitPolicy(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? benefitPolicyId)
        {
            try
            {
                return BenefitAccess.GetBenefitPolicy(user, benefitPolicyId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - GetBenefitPolicy", ex);
                throw;
            }
        }

        public BenefitPolicy InsertBenefitPolicy(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPolicy benefitPolicy, BenefitPolicyPlanCollection benefitPolicyPlanCollection)
        {
            try
            {
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    //insert into the benefit_policy table
                    BenefitAccess.InsertBenefitPolicy(user, benefitPolicy);

                    //insert into the benefit_policy_plan table
                    InsertBenefitPolicyPlan(user, benefitPolicy.BenefitPolicyId, benefitPolicyPlanCollection);

                    scope.Complete();
                }

                return benefitPolicy;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - InsertBenefitPolicy", ex);
                throw;
            }
        }

        public void UpdateBenefitPolicy(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPolicy benefitPolicy, BenefitPolicyPlanCollection benefitPolicyPlanCollection)
        {
            try
            {
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    //update the benefit_policy table
                    BenefitAccess.UpdateBenefitPolicy(user, benefitPolicy);

                    //insert into the benefit_policy_plan table
                    InsertBenefitPolicyPlan(user, benefitPolicy.BenefitPolicyId, benefitPolicyPlanCollection);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - UpdateBenefitPolicy", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }

        public void DeleteBenefitPolicy(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPolicy benefitPolicy)
        {
            try
            {
                BenefitAccess.DeleteBenefitPolicy(user, benefitPolicy);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - DeleteBenefitPolicy", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion


        #region benefit plan
        public BenefitPlanCollection GetBenefitPlan(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long? benefitPlanId)
        {
            try
            {
                return BenefitAccess.GetBenefitPlan(user, benefitPlanId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - GetBenefitPlan", ex);
                throw;
            }
        }

        public BenefitPlan InsertBenefitPlan(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPlan benefitPlan)
        {
            try
            {
                return BenefitAccess.InsertBenefitPlan(user, benefitPlan);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - InsertBenefitPlan", ex);
                throw;
            }
        }

        public void UpdateBenefitPlan(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPlan benefitPlan)
        {
            try
            {
                BenefitAccess.UpdateBenefitPlan(user, benefitPlan);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - UpdateBenefitPlan", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }

        public void DeleteBenefitPlan(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPlan benefitPlan)
        {
            try
            {
                BenefitAccess.DeleteBenefitPlan(user, benefitPlan);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - DeleteBenefitPlan", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion


        #region benefit plan detail
        public BenefitPlanDetailCollection GetBenefitPlanDetail(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long benefitPlanId, long benefitPlanDetailId)
        {
            try
            {
                return BenefitAccess.GetBenefitPlanDetail(user, benefitPlanId, benefitPlanDetailId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - GetBenefitPlanDetail", ex);
                throw;
            }
        }

        public BenefitPlanDetail InsertBenefitPlanDetail(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPlanDetail benefitPlanDetail)
        {
            try
            {
                return BenefitAccess.InsertBenefitPlanDetail(user, benefitPlanDetail);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - InsertBenefitPlanDetail", ex);
                throw;
            }
        }

        public void UpdateBenefitPlanDetail(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPlanDetail benefitPlanDetail)
        {
            try
            {
                //insert into or update the benefit_plan_detail table
                if (benefitPlanDetail.BenefitPlanDetailId > 0)
                    BenefitAccess.UpdateBenefitPlanDetail(user, benefitPlanDetail);
                else
                    InsertBenefitPlanDetail(user, benefitPlanDetail);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - UpdateBenefitPlanDetail", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }

        public void DeleteBenefitPlanDetail(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, BenefitPlanDetail benefitPlanDetail)
        {
            try
            {
                //delete from the benefit_plan_detail table
                BenefitAccess.DeleteBenefitPlanDetail(user, benefitPlanDetail);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - DeleteBenefitPlanDetail", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion


        #region benefit policy plan
        public BenefitPolicyPlanCollection GetBenefitPolicyPlan(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long benefitPolicyId)
        {
            try
            {
                return BenefitAccess.GetBenefitPolicyPlan(user, benefitPolicyId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - GetBenefitPolicyPlan", ex);
                throw;
            }
        }

        public BenefitPolicyPlanCollection InsertBenefitPolicyPlan(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long benefitPolicyId, BenefitPolicyPlanCollection benefitPolicyPlanCollection)
        {
            try
            {
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    //delete from the benefit_policy_plan table
                    DeleteBenefitPolicyPlan(user, benefitPolicyId);

                    //insert into the benefit_policy_plan table
                    foreach (BenefitPolicyPlan policyPlan in benefitPolicyPlanCollection)
                    {
                        //set the policy id
                        if (policyPlan.BenefitPolicyId == -1)
                            policyPlan.BenefitPolicyId = benefitPolicyId;

                        BenefitAccess.InsertBenefitPolicyPlan(user, policyPlan);
                    }

                    scope.Complete();
                }

                return benefitPolicyPlanCollection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - InsertBenefitPolicyPlan", ex);
                throw;
            }
        }

        public void DeleteBenefitPolicyPlan(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long benefitPolicyId)
        {
            try
            {
                BenefitAccess.DeleteBenefitPolicyPlan(user, benefitPolicyId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - DeleteBenefitPolicyPlan", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion


        #region benefit policy search
        public BenefitPolicySearchCollection GetBenefitPolicyReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String criteria)
        {
            try
            {
                return BenefitAccess.GetBenefitPolicyReport(user, criteria);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - GetBenefitPolicyReport", ex);
                throw;
            }
        }

        public void DeleteBenefitPolicyReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long benefitPolicyId)
        {
            try
            {
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    //delete from the benefit_policy_plan table
                    DeleteBenefitPolicyPlan(user, benefitPolicyId);

                    //delete from the benefit_policy table
                    BenefitPolicyCollection benefitPolicyCollection = GetBenefitPolicy(user, benefitPolicyId);
                    if (benefitPolicyCollection.Count > 0)
                        DeleteBenefitPolicy(user, benefitPolicyCollection[0]);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - DeleteBenefitPolicyReport", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion


        #region benefit plan search
        public BenefitPlanSearchCollection GetBenefitPlanReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, String criteria)
        {
            try
            {
                return BenefitAccess.GetBenefitPlanReport(user, criteria);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - GetBenefitPlanReport", ex);
                throw;
            }
        }

        public void DeleteBenefitPlanReport(WLP.BusinessLayer.BusinessObjects.DatabaseUser user, long benefitPlanId)
        {
            try
            {
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    //delete from the benefit_plan_detail table
                    BenefitPlanDetailCollection entitlementDetailCollection = GetBenefitPlanDetail(user, benefitPlanId, -1); //-1 gets everything
                    foreach (BenefitPlanDetail detail in entitlementDetailCollection)
                        DeleteBenefitPlanDetail(user, detail);

                    //delete from the benefit_plan table
                    BenefitPlanCollection entitlementCollection = GetBenefitPlan(user, benefitPlanId);
                    if (entitlementCollection.Count > 0)
                        DeleteBenefitPlan(user, entitlementCollection[0]);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "BenefitManagement - DeleteBenefitPlanReport", ex);

                if (ex is DataLayer.DataAccess.SqlServer.SqlServerException)
                {
                    if (((System.Data.SqlClient.SqlException)((DataLayer.DataAccess.SqlServer.SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion
    }
}
