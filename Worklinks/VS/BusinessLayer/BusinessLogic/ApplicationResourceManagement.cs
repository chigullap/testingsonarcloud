﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.ComponentModel;
using System.Collections.Specialized;

using WorkLinks.DataLayer.DataAccess.SqlServer;
using WorkLinks.BusinessLayer.BusinessObjects;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.DataLayer.DataAccess;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class ApplicationResourceManagement
    {
        //add data access
        SqlServerApplicationResource _guiAccess = new SqlServerApplicationResource();


        public String GetResourceByLanguageAndKey(DatabaseUser user, String virtualPath, String resourceKey)
        {
            return GetResourceByLanguageAndKey(user.DatabaseName, user.SecurityRoleId, user.LanguageCode, virtualPath, resourceKey);
        }

        public String GetResourceByLanguageAndKey(String databaseName, long securityRoleId, String languageCode, String virtualPath, String resourceKey)
        {
            ApplicationResourceCollection resources = _guiAccess.GetResourceByCultureAndKey(databaseName, securityRoleId, languageCode, virtualPath, resourceKey);
            return resources.Count>0?(String)resources[resourceKey].Value:null;
        }

    }
}
