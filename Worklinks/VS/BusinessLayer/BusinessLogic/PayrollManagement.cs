﻿using met;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Transactions;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.CalcModel;
using WorkLinks.BusinessLayer.BusinessObjects.Import;
using WorkLinks.BusinessLayer.BusinessObjects.Payroll;
using WorkLinks.DataLayer.DataAccess;
using WorkLinks.DataLayer.DataAccess.SqlServer;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class PayrollManagement
    {
        #region fields
        //add data access
        PayrollAccess _payrollAccess = new PayrollAccess();
        CodeAccess _codeAccess = null;
        EmployeeAccess _employeeAccess = null;
        ApplicationResourceManagement _guiManagement = null;
        EmployeeManagement _employeeManagement = null;
        #endregion

        #region properties
        public ApplicationResourceManagement GuiManagement
        {
            get
            {
                if (_guiManagement == null)
                    _guiManagement = new ApplicationResourceManagement();

                return _guiManagement;
            }
        }
        public EmployeeManagement EmployeeManagement
        {
            get
            {
                if (_employeeManagement == null)
                    _employeeManagement = new EmployeeManagement();

                return _employeeManagement;
            }
        }
        private EmployeeAccess EmployeeAccess
        {
            get
            {
                if (_employeeAccess == null)
                    _employeeAccess = new EmployeeAccess();

                return _employeeAccess;
            }
        }
        private CodeAccess CodeAccess
        {
            get
            {
                if (_codeAccess == null)
                    _codeAccess = new CodeAccess();

                return _codeAccess;
            }
        }
        #endregion

        #region payroll period
        public PayrollPeriodCollection GetPayrollPeriod(DatabaseUser user, long payrollPeriodId)
        {
            try
            {
                PayrollPeriodCriteria criteria = new PayrollPeriodCriteria() { PayrollPeriodId = payrollPeriodId };
                return _payrollAccess.GetPayrollPeriod(user, criteria);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - GetPayrollPeriodPeriodIdArg", ex);
                throw;
            }
        }
        public PayrollPeriodCollection GetPayrollPeriod(DatabaseUser user, PayrollPeriodCriteria criteria)
        {
            try
            {
                return _payrollAccess.GetPayrollPeriod(user, criteria);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - GetPayrollPeriodPPCriteriaArg", ex);
                throw;
            }
        }
        public void ClosePeriod(DatabaseUser user, PayrollPeriod item, long payrollProcessId)
        {
            try
            {
                _payrollAccess.ClosePeriod(user, item, payrollProcessId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - ClosePeriod", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void UpdatePayrollPeriod(DatabaseUser user, PayrollPeriod item)
        {
            try
            {
                _payrollAccess.UpdatePayrollPeriod(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - UpdatePayrollPeriod", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeletePayrollPeriod(DatabaseUser user, PayrollPeriod item)
        {
            try
            {
                _payrollAccess.DeletePayrollPeriod(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - DeletePayrollPeriod", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void InsertPayrollPeriod(DatabaseUser user, PayrollPeriod item)
        {
            try
            {
                _payrollAccess.InsertPayrollPeriod(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - InsertPayrollPeriod", ex);
                throw;
            }
        }
        #endregion

        #region payroll process
        public PayrollProcessSummaryCollection GetPayrollProcessSummary(DatabaseUser user, PayrollProcessCriteria criteria)
        {
            try
            {
                //get payroll process
                PayrollProcessSummaryCollection payrollProcesses = _payrollAccess.GetPayrollProcessSummary(user, criteria); //just one
                return payrollProcesses;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - GetPayrollProcessSummary", ex);
                throw;
            }
        }
        public PayrollProcessCollection GetPayrollProcess(DatabaseUser user, PayrollProcessCriteria criteria)
        {
            try
            {
                //get payroll process
                PayrollProcessCollection payrollProcesses = _payrollAccess.GetPayrollProcess(user, criteria); //just one
                return payrollProcesses;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - GetPayrollProcess", ex);
                throw;
            }
        }
        private CodeCollection CopyUpdateUserInfo(DatabaseUser user, CodeCollection collection, PayrollProcess item)
        {
            foreach (CodeObject obj in collection)
            {
                obj.CreateUser = item.UpdateUser;
                obj.UpdateUser = item.UpdateUser;
            }

            return collection;
        }

        public PayrollTransactionPayrollMasterCollection GetPayrollTransactionPayrollMaster(DatabaseUser user, long[] batchIds)
        {
            return _payrollAccess.GetPayrollTransactionPayrollMaster(user, batchIds);
        }

        public List<METEmployeeData> MapToMET(PayrollMasterCollection payrollMasterColl)
        {
            //create a list to hold all items
            List<METEmployeeData> tableData = new List<METEmployeeData>();

            //for each employee row in the db, assign data to MET object and add to the list
            foreach (PayrollMaster payMaster in payrollMasterColl)
            {
                if (payMaster.HasCurrentFlag)
                {
                    METEmployeeData temp = new METEmployeeData();
                    temp.PayrollMasterId = payMaster.PayrollMasterId;       //foreign key
                    temp.EmployeeId = payMaster.EmployeeId;
                    temp.PayrollProcessId = payMaster.PayrollProcessId;

                    //assign province
                    Province employeeProvince;

                    if (payMaster.CodeProvinceStateCd == "ZZ") //for now, ZZ gets put into NA for MET processing
                        employeeProvince = Province.NA;
                    else
                    {
                        if (!Enum.TryParse<Province>(payMaster.CodeProvinceStateCd, out employeeProvince))
                            employeeProvince = Province.NA;
                    }

                    //set province for canada object
                    temp.canada.SetProvince(employeeProvince);

                    //Determine our calc type.  
                    //It is ok if we have TaxableIncome > 0 and the calc type isnt set to RegularSalary here because the calc method will check to see if there are Regular Salary earnings 
                    //when we are using a non-regular CalculationType 

                    if (employeeProvince != Province.QC)  //non quebec employees
                    {
                        if (payMaster.BonusTaxableIncome != 0 || payMaster.LumpsumTaxableIncome != 0)
                            temp.canada.CalculationType = CalculationType.Bonus;
                        else if (payMaster.CommissionTaxableIncome != 0)
                            temp.canada.CalculationType = CalculationType.Commission;
                        else if (payMaster.PensionTaxableIncome != 0)
                            temp.canada.CalculationType = CalculationType.Pension;
                        else
                            temp.canada.CalculationType = CalculationType.RegularSalary;
                        //no retro inputs yet
                    }
                    else //quebec employees
                    {
                        //instantiate a quebec object
                        temp.quebec = new EmployeeDataQC();

                        if (payMaster.QuebecBonusTaxableIncome != 0 || payMaster.QuebecLumpsumTaxableIncome != 0)
                        {
                            temp.canada.CalculationType = CalculationType.Bonus;
                            temp.quebec.CalculationType = CalculationTypeQC.Bonus;
                        }
                        else if (payMaster.QuebecCommissionTaxableIncome != 0)
                        {
                            temp.canada.CalculationType = CalculationType.Commission;
                            temp.quebec.CalculationType = CalculationTypeQC.Commission;
                        }
                        else if (payMaster.QuebecPensionTaxableIncome != 0)
                        {
                            temp.canada.CalculationType = CalculationType.Pension;
                            temp.quebec.CalculationType = CalculationTypeQC.Pension;
                        }
                        else
                        {
                            temp.canada.CalculationType = CalculationType.RegularSalary;
                            temp.quebec.CalculationType = CalculationTypeQC.RegularSalary;
                        }
                        //no retro inputs yet
                    }

                    #region canadian assignments

                    //annual pay periods  NOTE:  For CRA (CPTL) commission calculations, the value is always = 0.
                    if (temp.canada.CalculationType == CalculationType.Commission)
                        temp.canada.annualPayPeriods = 0;
                    else
                        temp.canada.annualPayPeriods = (ushort)payMaster.PeriodsPerYear;

                    //The birth date is used only to determine if the employee is eligible for an age-related exemption from CPP
                    temp.canada.birthDate = (payMaster.BirthDate == null) ? null : Convert.ToDateTime(payMaster.BirthDate).ToString("yyyy-MM-dd");

                    //Enter the total claim amount from the employee’s federal TD1 form. 
                    //If the employee has not completed a federal TD1 form, enter the value 1, which will result in the default Basic Claim Amount being used for payroll calculations. 
                    temp.canada.TD1_Claim = (int)Math.Round(Convert.ToDecimal(payMaster.FederalTaxClaim) * 100, MidpointRounding.AwayFromZero); //federal total claim amount

                    //CPTL can be configured to not calculate a federal income tax deduction by entering a value of 1 in this field.  Otherwise, the default value of 0 should be used.
                    temp.canada.TD1_exemptITD = Convert.ToUInt16(payMaster.FederalTaxExemptFlag);

                    //Enter the amount of the annual deduction for living in a prescribed zone as indicated on the federal TD1 form.  If no amount is claimed, enter 0.
                    temp.canada.TD1_HD = (int)Math.Round(Convert.ToDecimal(payMaster.FederalDesignatedAreaDeduction) * 100, MidpointRounding.AwayFromZero);

                    //Enter the amount of additional income tax deductions per pay period requested by the employee on his / her federal TD1 form.  If no additional amount is requested, enter 0.
                    if (employeeProvince != Province.QC)
                        //if non quebec, all additional tax is taken off the federal side.  (fed additional tax + prov additional tax = SumFederalProvincialAdditionalTax)
                        temp.canada.TD1_L = (int)Math.Round(Convert.ToDecimal(payMaster.SumFederalProvincialAdditionalTax) * 100, MidpointRounding.AwayFromZero);
                    else
                        //if quebec, fed additional tax is just for cra.  revenu quebec has it's own field for additional provincial tax.
                        temp.canada.TD1_L = (int)Math.Round(Convert.ToDecimal(payMaster.FederalAdditionalTax) * 100, MidpointRounding.AwayFromZero);

                    //Enter the amount of annual taxable deductions, such as childcare expenses and support payments, etc., authorized by a tax services office or tax centre.
                    //The employee must have a letter from a tax office authorizing this deduction amount on file with the employer.  If no amount is claimed, enter 0.
                    temp.canada.CRA_F1 = (int)Math.Round(Convert.ToDecimal(payMaster.FederalAuthorizedAnnualDeduction) * 100, MidpointRounding.AwayFromZero);

                    //Enter the amount of other federal tax credits, such as medical expenses and charitable donations, authorized by a tax services office or tax centre.
                    temp.canada.CRA_K3 = (int)Math.Round(Convert.ToDecimal(payMaster.FederalAuthorizedAnnualTaxCredit) * 100, MidpointRounding.AwayFromZero);

                    //For commissioned employees, enter the amount of the total annual remuneration as reported on the employee’s federal TD1(X) form.
                    //If a commission income deduction calculation (calcType = 5) is being performed, the amount in this field must be greater than 0.
                    //If the employee is not remunerated by commission income, enter 0.
                    temp.canada.TD1X_I1 = (int)Math.Round(Convert.ToDecimal(payMaster.EstimatedAnnualIncome) * 100, MidpointRounding.AwayFromZero);

                    //For commissioned employees, enter the amount of the total annual commission expenses deduction as reported on the employee’s federal TD1(X) form.
                    //If the employee is not remunerated by commission income, enter 0.
                    temp.canada.TD1X_E = (int)Math.Round(Convert.ToDecimal(payMaster.EstimatedAnnualExpense) * 100, MidpointRounding.AwayFromZero);

                    //Enter the total claim amount from the employee’s provincial TD1P form. 
                    //If the employee has not completed a provincial TD1P form, enter the value 1, which will result in the default Basic Claim Amount being used for payroll calculations.
                    temp.canada.TD1P_Claim = (int)Math.Round((Decimal)payMaster.ProvincialTaxClaim * 100, MidpointRounding.AwayFromZero); //prov total claim amount

                    //CPTL can be configured to not calculate a provincial/ territorial income tax deduction by entering a value of 1 in this field.  Otherwise, the default value of 0 should be used.
                    temp.canada.TD1P_exemptITD = Convert.ToUInt16(payMaster.ProvincialTaxExemptFlag);

                    //If no amount is claimed, enter 0.
                    temp.canada.CRA_K3P = (int)Math.Round((Decimal)payMaster.ProvincialAuthorizedAnnualTaxCredit * 100, MidpointRounding.AwayFromZero); //prov tax credit

                    //Enter the amount of additional provincial or territorial tax reduction based on applicable amounts reported on the provincial/ territorial Form TD1P. 
                    //If no amount is claimed, enter 0.
                    temp.canada.CRA_Y = 0;

                    //If the employee is exempt from contributing to Canada Pension Plan (CPP), enter 1 (true), otherwise enter 0 (false).
                    //If unknown, enter 0 (false).  The CPTL program can determine age related exemptions to CPP automatically if the birth date for the employee is provided in the birthDate field.
                    temp.canada.isCPPexempt = Convert.ToUInt16(payMaster.CanadaPensionPlanExemptFlag);

                    //If the employee is exempt from paying Employment Insurance (EI) premiums, enter 1 (true), otherwise enter 0 (false).If unknown, enter 0 (false).
                    temp.canada.isEIexempt = Convert.ToUInt16(payMaster.EmploymentInsuranceExemptFlag);

                    //If a Quebec employee is exempt from paying Quebec Parental Insurance Plan (PPIP) premiums, enter 1(true), otherwise enter 0 (false).  If unknown, enter 0 (false).
                    //Note: Currently, this factor applies only in the province of Quebec, therefore for all other provinces, the value should be 0 (false).
                    temp.canada.isPPIPexempt = Convert.ToUInt16(payMaster.ParentalInsurancePlanExemptFlag);

                    //If an employer has qualified for a reduced EI contribution rate factor, enter the reduced employer EI rate.
                    //This amount will be used to determine the employer’s EI contribution cost and the amount of EI deduction that is returnable to the employee.
                    //If unknown, enter a Null value, and the default EI employer contribution factor (1.4) will be assumed.
                    //Enter the amount WITH a decimal point – e.g. 1.4 should be entered as 1.4.
                    temp.canada.employer_EI_ratefactor = (double)payMaster.EmployerEmploymentInsuranceRate;

                    //Enter the total amount of year-to-date deductions of Canada Pension Plan (CPP) contributions for the employee. 
                    //This amount will be used to determine if the employee has reached the maximum contribution limit for the year. If unknown, enter 0.
                    temp.canada.ytdCPP = (int)Math.Round((Decimal)payMaster.YtdPreviousCanadaPensionPlan * 100, MidpointRounding.AwayFromZero);

                    //Enter the total amount of year-to-date deductions of Employment Insurance (EI) premiums for the employee. 
                    //This amount will be used to determine if the employee has reached the maximum deduction limit for the year. If unknown, enter 0.
                    temp.canada.ytdEI = (int)Math.Round((Decimal)payMaster.YtdPreviousEmploymentInsurance * 100, MidpointRounding.AwayFromZero);

                    //Enter the total amount of year-to-date deductions of Quebec Parental Insurance Plan (QPIP) premiums for the employee (Quebec employees only).
                    //This amount will be used to determine if the employee has reached the maximum premium deduction limit for the year. 
                    //If unknown, enter 0.  Enter the amount WITHOUT a decimal point - 2 decimal points are assumed – e.g. 200.00 should be entered as 20000.
                    //Note: Only the province of Quebec administers this program, therefore the value in this field for employees of all other provinces should be 0.
                    if (employeeProvince != Province.QC)
                        temp.canada.ytdPPIP = 0;
                    else
                        temp.canada.ytdPPIP = (int)Math.Round((Decimal)payMaster.YtdPreviousQuebecParentalInsurancePlan * 100, MidpointRounding.AwayFromZero);

                    //Enter the total amount of year-to-date employer contributions to Quebec Parental Insurance Plan(QPIP) premiums for the employee (Quebec employees only).
                    //This amount will be used to determine if the employer has reached the maximum premium contribution limit for the year. 
                    //If unknown, enter 0.Enter the amount WITHOUT a decimal point - 2 decimal points are assumed – e.g. 200.00 should be entered as 20000.
                    //Note: Only the province of Quebec administers this program, therefore the value in this field for employees of all other provinces should be 0
                    if (employeeProvince != Province.QC)
                        temp.canada.ytdPPIP_employer = 0;
                    else
                        temp.canada.ytdPPIP_employer = (int)Math.Round((Decimal)payMaster.YtdPreviousEmployerQuebecParentalInsurancePlan * 100, MidpointRounding.AwayFromZero);

                    //Enter the integer value of the current Pay Period number – i.e. the number corresponding to the position of the current pay period within the total number of pay periods in the year. 
                    //This is an optional field and is used to calculate the number of remaining pay periods in the current year.
                    //If the current pay period number is not known or not used, enter a value of 0.
                    //If ytd deduction values have been provided and you wish CPTL to base income tax deductions on values calculated from the ytd deductions of CPP, EI, PPIP and Income taxes, then you
                    //must provide a non-zero value in this field, otherwise the ytd values will be ignored.
                    temp.canada.payPeriod = (ushort)payMaster.Period;

                    //Number of pay periods covered by current payroll. Default = 0.
                    temp.canada.cntPP = (ushort)payMaster.NumberOfPayPeriodsCovered;

                    //Enter the date of the current payroll payment in the format YYYY-MM-DD. 
                    //If an empty string is entered, the CPTL program will assume the current system date is the payroll payment date
                    temp.canada.payDate = payMaster.ChequeDate.ToString("yyyy-MM-dd");

                    if (temp.canada.CalculationType == CalculationType.RegularSalary || temp.canada.CalculationType == CalculationType.Bonus || temp.canada.CalculationType == CalculationType.RetroPay)
                        //Enter the amount of regular salary/wages paid to the employee for the defined pay period.  Data entered in this field is used only when the calcType value is 1, 3, or 4 – for other calcTypes enter 0 in this field
                        temp.canada.wagesAmount = (int)Math.Round(payMaster.TaxableIncome * 100, MidpointRounding.AwayFromZero);
                    else if (temp.canada.CalculationType == CalculationType.Pension)
                        //Enter the amount of pension income paid to the employee for the defined pay period.  Data entered in this field is only used when the calcType value is 2 – for other calcTypes enter 0 in this field.
                        temp.canada.pensionAmount = (int)Math.Round(payMaster.PensionTaxableIncome * 100, MidpointRounding.AwayFromZero);
                    else
                    {
                        temp.canada.wagesAmount = 0;
                        temp.canada.pensionAmount = 0;
                    }

                    if (temp.canada.CalculationType == CalculationType.Bonus)
                        temp.BonusLumpSumTaxableIncome = (int)Math.Round(payMaster.BonusLumpSumTaxableIncome * 100, MidpointRounding.AwayFromZero);

                    //Enter the amount of the current bonus to be paid to the employee for the defined pay period.
                    //Data entered in this field is only used when the calcType value is 3 – for other calcTypes enter 0 in this field.
                    //Note that a bonus deduction calculation(calcType = 3) also requires that amounts be entered for wagesAmount, annualPayPeriods and ytdPrevBonusAmount.
                    //The wagesAmount and annualPayPeriods data will only be used to estimate annual taxable income for purposes of determining tax rates applicable to the bonus income.
                    //The calculated payroll deductions will apply ONLY to the bonus amount entered in this field
                    if (temp.canada.CalculationType == CalculationType.Bonus)
                        if (employeeProvince != Province.QC)
                        {
                            temp.canada.bonusAmount = (int)Math.Round(payMaster.BonusTaxableIncome * 100, MidpointRounding.AwayFromZero);
                            temp.canada.ytdPrevBonusAmount = (int)Math.Round((Decimal)payMaster.YtdPreviousBonusTaxableIncome * 100, MidpointRounding.AwayFromZero);

                            if (temp.canada.bonusAmount == 0) //this is not a bonus but a lump sum run
                            {
                                temp.canada.bonusAmount = (int)Math.Round(payMaster.LumpsumTaxableIncome * 100, MidpointRounding.AwayFromZero);
                                temp.canada.ytdPrevBonusAmount = (int)Math.Round((Decimal)payMaster.YtdPreviousQuebecLumpsumTaxableIncome * 100, MidpointRounding.AwayFromZero);
                            }
                        }
                        else
                        {
                            temp.canada.bonusAmount = (int)Math.Round(payMaster.QuebecBonusTaxableIncome * 100, MidpointRounding.AwayFromZero);
                            temp.canada.ytdPrevBonusAmount = (int)Math.Round((Decimal)payMaster.YtdPreviousQuebecBonusTaxableIncome * 100, MidpointRounding.AwayFromZero);

                            if (temp.canada.bonusAmount == 0) //this is not a bonus but a lump sum run
                            {
                                temp.canada.bonusAmount = (int)Math.Round(payMaster.QuebecLumpsumTaxableIncome * 100, MidpointRounding.AwayFromZero);
                                temp.canada.ytdPrevBonusAmount = (int)Math.Round((Decimal)payMaster.YtdPreviousQuebecLumpsumTaxableIncome * 100, MidpointRounding.AwayFromZero);
                            }
                        }
                    else
                    {
                        temp.canada.bonusAmount = 0;
                        temp.canada.ytdPrevBonusAmount = 0;
                    }

                    //Enter the amount of taxable cash benefits paid to the employee for the defined current pay period.  Data entered in this field can be used with any calcType value
                    temp.canada.txCashBenefitsAmount = (int)Math.Round(payMaster.TaxableCashBenefit * 100, MidpointRounding.AwayFromZero);

                    //Enter the amount of taxable non-cash benefits provided to the employee for the defined current pay period.  Data entered in this field can be used with any calcType value
                    temp.canada.txNonCashBenefitsAmount = (int)Math.Round(payMaster.TaxableNonCashBenefit * 100, MidpointRounding.AwayFromZero);

                    //Enter the amount of the current payroll deduction(s) for employee contributions to a registered pension plan (RPP), a registered retirement savings plan(RRSP), 
                    //or a retirement compensation arrangement(RCA).  Data entered in this field can be used with any calcType value.
                    temp.canada.dedF = (int)Math.Round(payMaster.BeforeIncomeTaxDeduction * 100, MidpointRounding.AwayFromZero);

                    //Enter the amount of the current payroll deduction(s)(e.g.RRSP / RPP, union dues, alimony or family maintenance) applicable only to the current bonus payment.
                    //Data entered in this field is only used when the calcType value is 3 (Bonus payments) – for other calcTypes enter 0 in this field.
                    if (temp.canada.CalculationType == CalculationType.Bonus)
                        temp.canada.bonusDeductions = (int)Math.Round(payMaster.BeforeBonusTaxDeduction * 100, MidpointRounding.AwayFromZero);
                    else
                        temp.canada.bonusDeductions = 0;

                    //Enter the amount of the current commission payment to the employee. 
                    //Data entered in this field is only used when the calcType value is 5 – for other calcTypes enter 0 in this field.
                    //Note that a commission income deduction calculation (calcType=5) also requires that amounts be entered for TD1X_I1, TD1X_E, and daysSincePrevCommPmt
                    if (temp.canada.CalculationType == CalculationType.Commission)
                        temp.canada.commAmount = (int)Math.Round(payMaster.CommissionTaxableIncome * 100, MidpointRounding.AwayFromZero);
                    else
                        temp.canada.commAmount = 0;

                    //Enter the number of days since the previous commission payment during the current calendar year.
                    //Data entered in this field is only used when the calcType value is 5 – for other calcTypes enter 0 in this field.
                    //If this is the first commission payment of the calendar year, enter 0.
                    //Note that a commission income deduction calculation(calcType = 5) also requires that amounts be entered for TD1X_I1, TD1X_E, and commAmount.
                    if (temp.canada.CalculationType == CalculationType.Commission)
                        temp.canada.daysSincePrevCommPmt = (ushort)payMaster.DaysSinceLastCommissionPayment;
                    else
                        temp.canada.daysSincePrevCommPmt = 0;

                    #region other run types and zeroed values

                    //Enter the amount of vacation pay paid to the employee for the defined pay period. 
                    //Data entered in this field is only used when the calcType value is 1,3 or 4 – for other calcTypes enter 0 in this field.
                    if (temp.canada.CalculationType == CalculationType.RegularSalary || temp.canada.CalculationType == CalculationType.Bonus || temp.canada.CalculationType == CalculationType.RetroPay)
                        temp.canada.vacationPayAmount = 0;
                    else
                        temp.canada.vacationPayAmount = 0;

                    //Enter the total amount of year-to-date deductions of Income Taxes withheld for the employee.  
                    //This amount will be used to determine if the employee’s current payroll income tax deduction can be reduced by an over-deduction on previous payrolls.  If unknown, enter 0.
                    //Stacy note:  our input data doesnt have this
                    temp.canada.ytdITD = 0;

                    //Enter the total amount of year-to-date purchases of units of eligible labour-sponsored funds for the employee. 
                    //This amount will be used to determine the applicable tax credit that can be used to reduce income taxes payable for the year.  If unknown, enter 0.
                    temp.canada.ytdLSFp = 0;

                    //Enter the total number of pay periods for allocation of the calculated annual Labour - Sponsored Funds(LSF) credit using the accelerated method approved by CRA.
                    //If unknown, or if the accelerated method is not applicable, enter 0.
                    temp.canada.LSFp_P = 0;

                    //Enter the amount of the current payroll deduction(s) for union dues. Data entered in this field can be used with any calcType value.
                    temp.canada.dedU1 = 0;

                    //Enter the amount of the current payroll deduction(s) for alimony or family maintenance payments required by a legal document to be payroll - deducted.
                    //Data entered in this field can be used with any calcType value.
                    temp.canada.dedF2 = 0;


                    #region retro pay section TODO
                    //stacy test this is for RETRO*******************************************************************************************************************

                    if (temp.canada.CalculationType == CalculationType.RetroPay)
                    {
                        //Note that a retroactive pay deduction calculation(calcType = 4) also requires that amounts be entered for wagesAmount, annualPayPeriods and retroPayPeriods.

                        //retroPayAmount = Enter the total amount of retroactive pay paid to the employee. 
                        //The wagesAmount and annualPayPeriods data will only be used to estimate annual taxable income for purposes of determining tax rates applicable to the retroactive pay income.
                        //The calculated payroll deductions will apply ONLY to the retroactive pay amount entered in this field

                        //Data entered in these fields below are only used when the calcType value is 4 – for other calcTypes enter 0 in this field.
                        temp.canada.retroPayAmount = 0;
                        temp.canada.retroPayPeriods = 0;    //Enter the number of pay periods to which the total retroactive pay amount is applicable for the employee.
                        temp.canada.retroDeductions = 0;    //Enter the amount of the current payroll deduction(s) (e.g. RRSP/RPP, union dues, alimony or family maintenance) applicable only to the current retroactive pay payment. 
                    }
                    else
                    {
                        temp.canada.retroPayAmount = 0;
                        temp.canada.retroPayPeriods = 0;
                        temp.canada.retroDeductions = 0;
                    }

                    //end stacy test this is for RETRO*******************************************************************************************************************
                    #endregion

                    #endregion

                    #endregion

                    #region quebec
                    if (temp.quebec != null)
                    {
                        //annual pay periods.  NOTEP:  for Quebec commission calculations, the value must be > 0.
                        temp.quebec.annualPayPeriods = (ushort)payMaster.PeriodsPerYear;

                        //The birth date is used only to determine if the employee is eligible for an age-related exemption from CPP
                        temp.quebec.birthDate = (payMaster.BirthDate == null) ? null : Convert.ToDateTime(payMaster.BirthDate).ToString("yyyy-MM-dd");

                        //Enter the integer value of the current Pay Period number – i.e. the number corresponding to the position of the current pay period within the total number of pay periods in the year. 
                        //This is an optional field and is used to calculate the number of remaining pay periods in the current year.
                        //If the current pay period number is not known or not used, enter a value of 0.
                        //If ytd deduction values have been provided and you wish QPTL to base income tax deductions on values calculated from the ytd deductions of CPP, EI, PPIP and Income taxes, then you
                        //must provide a non-zero value in this field, otherwise the ytd values will be ignored.
                        temp.quebec.payPeriod = (ushort)payMaster.Period;

                        //Number of pay periods covered by current payroll. Default = 0.
                        temp.quebec.cntPP = (ushort)payMaster.NumberOfPayPeriodsCovered;

                        //Enter the date of the current payroll payment in the format YYYY-MM-DD. 
                        //If an empty string is entered, the QPTL program will assume the current system date is the payroll payment date
                        temp.quebec.payDate = payMaster.ChequeDate.ToString("yyyy-MM-dd");

                        //Enter the amount of the current bonus to be paid to the employee for the defined pay period.
                        //Data entered in this field is only used when the calcType value is 3 – for other calcTypes enter 0 in this field.
                        //Note that a bonus deduction calculation(calcType = 3) also requires that amounts be entered for wagesAmount, annualPayPeriods and ytdPrevBonusAmount.
                        //The wagesAmount and annualPayPeriods data will only be used to estimate annual taxable income for purposes of determining tax rates applicable to the bonus income.
                        //The calculated payroll deductions will apply ONLY to the bonus amount entered in this field
                        if (temp.quebec.CalculationType == CalculationTypeQC.Bonus)
                            temp.quebec.bonusAmount = (int)Math.Round(payMaster.QuebecBonusTaxableIncome * 100, MidpointRounding.AwayFromZero);

                        //Year-to-date prior cumulative bonuses paid (currently used only in bonus and commission income calculations
                        //(CalculationType = CalculationType.Bonus or CalculationType = CalculationType.Commission)). Default = 0.
                        if (temp.quebec.CalculationType == CalculationTypeQC.Bonus || temp.quebec.CalculationType == CalculationTypeQC.Commission)
                            temp.quebec.ytdBonus = (int)Math.Round((Decimal)payMaster.YtdPreviousQuebecBonusTaxableIncome * 100, MidpointRounding.AwayFromZero);
                        else
                            temp.quebec.ytdBonus = 0;

                        //Enter the amount of the current payroll deduction(s) for employee contributions to a registered pension plan (RPP), a registered retirement savings plan(RRSP), 
                        //or a retirement compensation arrangement(RCA).  Data entered in this field can be used with any calcType value.
                        temp.quebec.dedRRSP = (int)Math.Round(payMaster.BeforeIncomeTaxDeduction * 100, MidpointRounding.AwayFromZero);

                        /*  Current pay period portion of remuneration that gives entitlement to one of the following deductions: 
                            - The deduction for employment income situated on a reserve or premises,
                            - The deduction for employment income earned on a vessel,
                            - The deduction for employees of an international financial centre (IFC),
                            - The deduction for foreign specialists, foreign researchers, foreign researchers on a post-doctoral internship, foreign experts, foreign professors, foreign producers, foreign individuals holding a key position in a foreign production filmed in Quebec, foreign farm workers, members of the Canadian forces or members of a Canadian police force.
                        */
                        //stacy test**** -- not sure this is right, also what about ProvincialDesignatedAreaDeduction?  these were both 0's in test input
                        temp.quebec.dedOther = (int)Math.Round(Convert.ToDecimal(payMaster.FederalDesignatedAreaDeduction) * 100, MidpointRounding.AwayFromZero);

                        //Quebec Claim Amount (from line 10 of the employees Quebec TP-1015.3-V form for the tax year in which the payroll is being paid.
                        temp.quebec.E = (int)Math.Round((Decimal)payMaster.ProvincialTaxClaim * 100, MidpointRounding.AwayFromZero);

                        //Exempt from Quebec income tax - total estimated income less than E. 
                        //Value         Description 
                        //  0(default)  No exemption
                        //  1           Exempt from Quebec income tax
                        temp.quebec.exemptQITD = Convert.ToUInt16(payMaster.ProvincialTaxExemptFlag);

                        //Employee - requested additional deduction of income tax (per pay period) – as requested by the individual on form TP-­-1017 -­-V; 
                        //or source deduction of income tax requested by a fisher on form TP - 1015.N - V; 
                        //or amount indicated on line 11 of form TP - 1015.3 - V, for the pay period.Default = 0.
                        temp.quebec.L = (int)Math.Round((Decimal)payMaster.ProvincialAdditionalTax * 100, MidpointRounding.AwayFromZero);

                        //Current payroll taxable deductions related to bonus payment, rounded to the nearest penny (e.g. 100.00 should be entered as 10000). 
                        ////Is used only for CalculationType = CalculationType.Bonus(calcType = 3).
                        if (temp.quebec.CalculationType == CalculationTypeQC.Bonus)
                            temp.quebec.bonusDeductions = (int)Math.Round(payMaster.BeforeBonusTaxDeduction * 100, MidpointRounding.AwayFromZero);
                        else
                            temp.quebec.bonusDeductions = 0;

                        //Quebec Parental Insurance Program (QPIP)exempt indicator.
                        //Value         Description 
                        //  0(default)  No exemption
                        //  1           Exempt from QPIP contributions.
                        temp.quebec.isQPIPexempt = Convert.ToUInt16(payMaster.ParentalInsurancePlanExemptFlag);

                        //Quebec Pension Plan(QPP) exempt indicator. 
                        //Value         Description
                        //  0(default)  No exemption
                        //  1           Exempt from QPP contributions.
                        temp.quebec.isQPPexempt = Convert.ToUInt16(payMaster.CanadaPensionPlanExemptFlag);

                        //Current payroll taxable cash benefits amount, rounded to the nearest penny (e.g. 100.00 should be entered as 10000). 
                        //Is used for CalculationType = CalculationType.RegularSalary, CalculationType.Bonus and CalculationType.RetroPay (calcType = 1, 3 and 4).
                        if (temp.quebec.CalculationType == CalculationTypeQC.RegularSalary || temp.quebec.CalculationType == CalculationTypeQC.Bonus || temp.quebec.CalculationType == CalculationTypeQC.RetroPay)
                            temp.quebec.txCashBenefitsAmount = (int)Math.Round(payMaster.QuebecTaxableCashBenefit * 100, MidpointRounding.AwayFromZero);
                        else
                            temp.quebec.txCashBenefitsAmount = 0;

                        //Current payroll taxable non-cash benefits amount, rounded to the nearest penny (e.g. 100.00 should be entered as 10000). 
                        //Is used for CalculationType = CalculationType.RegularSalary, CalculationType.Bonus and CalculationType.RetroPay(calcType = 1, 3 and 4).
                        if (temp.quebec.CalculationType == CalculationTypeQC.RegularSalary || temp.quebec.CalculationType == CalculationTypeQC.Bonus || temp.quebec.CalculationType == CalculationTypeQC.RetroPay)
                            temp.quebec.txNonCashBenefitsAmount = (int)Math.Round(payMaster.QuebecTaxableNonCashBenefit * 100, MidpointRounding.AwayFromZero);
                        else
                            temp.quebec.txNonCashBenefitsAmount = 0;

                        if (temp.quebec.CalculationType == CalculationTypeQC.RegularSalary || temp.quebec.CalculationType == CalculationTypeQC.Bonus || temp.quebec.CalculationType == CalculationTypeQC.RetroPay)
                            //Current payroll regular salary/wages amount, rounded to the nearest penny (e.g. 5,000.00 should be entered as 500000). Is used for CalculationType = CalculationType.RegularSalary, CalculationType.Bonus and CalculationType.RetroPay (calcType = 1, 3 and 4).
                            temp.quebec.wagesAmount = (int)Math.Round(payMaster.QuebecTaxableIncome * 100, MidpointRounding.AwayFromZero);
                        else if (temp.quebec.CalculationType == CalculationTypeQC.Pension)
                            //Current payroll pension income amount, rounded to the nearest penny(e.g. 2, 000.00 should be entered as 200000).  Is used only for CalculationType = CalculationType.Pension(calcType = 2).
                            temp.quebec.pensionAmount = (int)Math.Round(payMaster.QuebecPensionTaxableIncome * 100, MidpointRounding.AwayFromZero);
                        else
                        {
                            temp.quebec.wagesAmount = temp.canada.wagesAmount = 0;
                            temp.quebec.pensionAmount = temp.canada.pensionAmount = 0;
                        }

                        if (temp.quebec.CalculationType == CalculationTypeQC.Bonus)
                            temp.BonusLumpSumTaxableIncome = (int)Math.Round(payMaster.BonusLumpSumTaxableIncome * 100, MidpointRounding.AwayFromZero);

                        //Year-to-date prior Quebec Parental Insurance Program(QPIP) employee payroll deductions.  Default = 0.
                        temp.quebec.ytdQPIP = (int)Math.Round((Decimal)payMaster.YtdPreviousQuebecParentalInsurancePlan * 100, MidpointRounding.AwayFromZero); //Ytd Ppip Deducted

                        //Year-to-date prior Quebec Pension Plan (QPP) employee payroll deductions. Default = 0.
                        temp.quebec.ytdQPP = (int)Math.Round((Decimal)payMaster.YtdPreviousCanadaPensionPlan * 100, MidpointRounding.AwayFromZero); //Ytd Cpp/Qpp Deducted

                        //Exempt from Quebec Health contribution - requested by employee on TP-1015.3-V.
                        //Value         Description 
                        //  0(default)  No exemption
                        //  1           Exempt from Quebec Health contribution
                        temp.quebec.exemptQITD_Z = 0;

                        //Health contribution portion of year-to-date prior Quebec Income Tax employee payroll deductions (QITD). Default = 0.
                        temp.quebec.ytdQITD_Z = (int)Math.Round((Decimal)payMaster.YtdPreviousQuebecHealthContribution * 100, MidpointRounding.AwayFromZero);

                        //Non-refundable tax credits authorized by Revenu Quebec for the year after the employee completed form TP-1016-V (for example, the tax credit for charitable donations).
                        temp.quebec.K1 = (int)Math.Round((Decimal)payMaster.ProvincialAuthorizedAnnualTaxCredit * 100, MidpointRounding.AwayFromZero);

                        //Deductions indicated on line 19 of form TP-1015.3-V. Default = 0.
                        temp.quebec.J = (int)Math.Round((Decimal)payMaster.ProvincialAuthorizedAnnualDeduction * 100, MidpointRounding.AwayFromZero);

                        //Year-to-date prior Quebec Parental Insurance Program (QPIP) employer contributions.  Default = 0.
                        temp.quebec.ytdQPIP_employer = (int)Math.Round((Decimal)payMaster.YtdPreviousEmployerQuebecParentalInsurancePlan * 100, MidpointRounding.AwayFromZero);

                        //currently used only in commission income calculations
                        if (temp.quebec.CalculationType == CalculationTypeQC.Commission)
                        {
                            //Current payroll commission pay amount, rounded to the nearest penny(e.g. 100.00 should be entered as 10000).
                            temp.quebec.commAmount = (int)Math.Round(payMaster.QuebecCommissionTaxableIncome * 100, MidpointRounding.AwayFromZero);

                            //temp.quebec.daysSincePrevCommPmt This is a misprint in the documentation.  It is not required for calculation of Quebec deductions on commission payments, only CRA

                            //Year-to-date prior cumulative pension income paid.            Default = 0.
                            temp.quebec.ytdPension = 0;                     //Year-to-date prior cumulative pension income paid (currently used only in commission income calculations (CalculationType = CalculationType.Commission)). Default = 0.
                            temp.quebec.ytdtxCashBenefitsAmount = 0;        //Year-to-date prior cumulative taxable cash benefits paid.     Default = 0.
                            temp.quebec.ytdtxNonCashBenefitsAmount = 0;     //Year-to-date prior cumulative taxable non-­cash benefits paid. Default = 0.
                            temp.quebec.ytdVacationPay = 0;                 //Year-to-date prior cumulative vacation pay paid.              Default = 0.
                            temp.quebec.ytdWages = 0;                       //Year-to-date prior cumulative salary and/ or wages paid.      Default = 0.       
                            temp.quebec.ytdComm = 0;                        //Year-to-date prior cumulative commissions paid (currently used only in commission income calculations(CalculationType = CalculationType.Commission)). Default = 0.
                        }
                        else
                        {
                            temp.quebec.commAmount = 0;
                            temp.quebec.daysSincePrevCommPmt = 0;
                        }

                        #region other calc types and zeroed values

                        //Current payroll vacation pay amount, rounded to the nearest penny (e.g. 100.00 should be entered as 10000). 
                        //Is used for CalculationType = CalculationType.RegularSalary, CalculationType.Bonus and CalculationType.RetroPay (calcType = 1, 3 and 4).
                        if (temp.quebec.CalculationType == CalculationTypeQC.RegularSalary || temp.quebec.CalculationType == CalculationTypeQC.Bonus || temp.quebec.CalculationType == CalculationTypeQC.RetroPay)
                            temp.quebec.vacationPayAmount = 0;
                        else
                            temp.quebec.vacationPayAmount = 0;

                        #region retro not done //stacy test this is for RETRO*******************************************************************************************************************

                        //Used only for CalculationType = CalculationType.RetroPay (calcType = 4).
                        if (temp.quebec.CalculationType == CalculationTypeQC.RetroPay)
                        {
                            temp.quebec.retroDeductions = 0;                //Current payroll taxable deductions related to retroactive pay payment, rounded to the nearest penny (e.g. 100.00 should be entered as 10000). 
                            temp.quebec.retroPayAmount = 0;                 //Current payroll retroactive pay amount, rounded to the nearest penny(e.g. 1, 000.00 should be entered as 100000).
                            temp.quebec.retroPayPeriods = 0;                //Current payroll retroactive pay payroll periods – i.e. the number of payroll periods that the retroactive pay amount covers. 
                        }
                        else
                        {
                            temp.quebec.retroDeductions = 0;
                            temp.quebec.retroPayAmount = 0;
                            temp.quebec.retroPayPeriods = 0;
                        }

                        #endregion //end stacy test this is for RETRO*******************************************************************************************************************

                        //Current pay period deduction respecting the CIP (Cooperative Investment Plan), that is, 125% of the amount withheld from the employee's remuneration for the 
                        //purchase of preferred shares qualifying under the CIP.
                        temp.quebec.dedCIP = 0;

                        //Current payroll deduction for amount withheld for the pay period for the purchase of class A shares in the Fonds de solidarite des travailleurs du Quebec(FTQ).
                        temp.quebec.dedQ = 0;

                        //Current payroll deduction for amount withheld for the pay period for the purchase of class A or class B shares in Fondaction, the Fonds de developpement de la Confederation des syndicates nationaux pour la cooperation et l'emploi.
                        temp.quebec.dedQ1 = 0;

                        //Amount withheld from current pay period for contribution to an RCA (retirement compensation arrangement).
                        temp.quebec.dedRCA = 0;

                        //Amount withheld from current pay period for contribution to an RPP.
                        temp.quebec.dedRPP = 0;

                        //Current pay period security option deduction.
                        temp.quebec.dedSecurity = 0;

                        //Current pay period travel deduction for residents of designated remote areas.
                        temp.quebec.dedTravel = 0;

                        //Annual taxable deductions authorized by Revenu Quebec after the employee completes and submits form TP - 1016 - V.Default = 0.
                        temp.quebec.J1 = 0;

                        //Year-­to-date prior cumulative deduction respecting the CIP, that is, 125% of the amount withheld from the employee’s remuneration for the purchase of 
                        //preferred shares qualifying under the CIP. Default = 0.
                        temp.quebec.ytdCIP = 0;

                        //Year-to-date prior cumulative additional source deductions of income tax requested by the employee on form TP-1017-V, TP-1015.N-V, or TP-1015.3-V. Default = 0.
                        temp.quebec.ytdL = 0;

                        /* The portion of year-­to-date prior cumulative remuneration that gives entitlement to one of the following deductions: 
                            - The deduction for employment income situated on a reserve or premises,
                            - The deduction for employment income earned on a vessel,
                            - The deduction for employees of an international financial centre (IFC),
                            - The deduction for foreign specialists, foreign researchers, foreign researchers on a post-doctoral internship, foreign experts, foreign professors, foreign producers, 
                                foreign individuals holding a key position in a foreign production filmed in Quebec, foreign farm workers, members of the Canadian forces or members of a 
                                Canadian police force.
                            Default = 0;
                        */
                        temp.quebec.ytdOther = 0;

                        //Year-­to-­date prior cumulative amounts withheld for the purchase of class A shares in the Fonds de solidarite des travailleurs du Quebec(FTQ). Default = 0.
                        temp.quebec.ytdQ = 0;

                        //Year-­to-­date prior cumulative amounts withheld for the purchase of class A or class B shares in the Fonds de developpement de la Confederation des syndicates 
                        //nationaux pour la cooperation et l'emploi (called "Fondaction"). Default = 0.
                        temp.quebec.ytdQ1 = 0;

                        //Year-to-date prior Quebec Income Tax employee payroll deductions.Default = 0.
                        temp.quebec.ytdQITD = 0;

                        //Year-­to-­date prior cumulative amounts withheld as contributions paid under a retirement compensation arrangement. Default = 0.
                        temp.quebec.ytdRCA = 0;

                        //Year-­to-­date prior cumulative amounts withheld as contributions to an RPP.Default = 0.
                        temp.quebec.ytdRPP = 0;

                        //Year-­to-­date prior cumulative amounts withheld as contributions to an RRSP.Default = 0.
                        temp.quebec.ytdRRSP = 0;

                        //Year-­to-date prior cumulative security option deduction. Default = 0.
                        temp.quebec.ytdSecurity = 0;

                        //Year-­to-date prior cumulative travel deduction for residents of designated remote areas. Default = 0.
                        temp.quebec.ytdTravel = 0;

                        #endregion
                    }
                    #endregion

                    //add to list
                    tableData.Add(temp);
                }

            }
            return tableData;
        }
        public List<PayrollMasterTax> CalcData(List<METEmployeeData> metData)
        {
            List<PayrollMasterTax> PayrollOutput = new List<PayrollMasterTax>();

            //pk counter
            int i = -1;

            foreach (METEmployeeData inputs in metData)
            {
                //store original ytd values.  will always have a canada object, use this ytd if no quebec object exists
                inputs.StartingYtdCppQppDeducted = (inputs.quebec != null) ? inputs.quebec.ytdQPP : inputs.canada.ytdCPP;
                inputs.StartingYtdPpipDeducted = (inputs.quebec != null) ? inputs.quebec.ytdQPIP : inputs.canada.ytdPPIP;
                inputs.StartingYtdPpipEmployerDeducted = (inputs.quebec != null) ? inputs.quebec.ytdQPIP_employer : inputs.canada.ytdPPIP_employer;
                //canada use only
                inputs.StartingYtdEiDeducted = inputs.canada.ytdEI;
                //quebec only
                inputs.StartingQuebecHealthDeducted = (inputs.quebec != null) ? inputs.quebec.ytdQITD_Z : 0;

                #region CANADA BONUS RUN CODE

                if (inputs.canada.CalculationType == CalculationType.Bonus)
                {
                    PayrollMasterTax payrollMasterTaxBonus = new PayrollMasterTax();
                    payrollMasterTaxBonus.EmployeeId = inputs.EmployeeId;
                    payrollMasterTaxBonus.PayrollProcessId = inputs.PayrollProcessId;
                    payrollMasterTaxBonus.PayrollMasterTaxId = i--;
                    payrollMasterTaxBonus.PayrollMasterId = inputs.PayrollMasterId;
                    payrollMasterTaxBonus.CalculationType = (int)inputs.canada.CalculationType;

                    //if no wages, use BonusLumpSumTaxableIncome as bonus needs wages to determine ytd salary for taxes
                    bool changedWages = false;

                    if (inputs.canada.wagesAmount == 0)
                    {
                        changedWages = true;
                        inputs.canada.wagesAmount = inputs.BonusLumpSumTaxableIncome;
                    }

                    CPTL tempEngine = new CPTL();
                    tempEngine.calcDeductions(inputs.canada);

                    //if wages were changed to use BonusLumpSumTaxableIncome, reset back to 0
                    if (changedWages)
                        inputs.canada.wagesAmount = 0;

                    //calc results
                    payrollMasterTaxBonus.EmployeeEi = (inputs.canada.dedEI != 0) ? Convert.ToDecimal(inputs.canada.dedEI) / 100 : inputs.canada.dedEI;
                    payrollMasterTaxBonus.FederalTax = (inputs.canada.dedITD_Fed != 0) ? Convert.ToDecimal(inputs.canada.dedITD_Fed) / 100 : inputs.canada.dedITD_Fed;
                    payrollMasterTaxBonus.CombinedTaxes = (inputs.canada.dedITD != 0) ? Convert.ToDecimal(inputs.canada.dedITD) / 100 : inputs.canada.dedITD;
                    payrollMasterTaxBonus.EmployerEi = (inputs.canada.employerEIcost != 0) ? Convert.ToDecimal(inputs.canada.employerEIcost) / 100 : inputs.canada.employerEIcost;
                    payrollMasterTaxBonus.EiEarnings = (inputs.canada.EI_Earnings != 0) ? Convert.ToDecimal(inputs.canada.EI_Earnings) / 100 : inputs.canada.EI_Earnings;
                    payrollMasterTaxBonus.EmployeeEiReturnable = (inputs.canada.employeeEIreturnable != 0) ? Convert.ToDecimal(inputs.canada.employeeEIreturnable) / 100 : inputs.canada.employeeEIreturnable;
                    payrollMasterTaxBonus.CreateUser = payrollMasterTaxBonus.UpdateUser = "metThreadingEngine";

                    //add to EI deduction to YTD
                    inputs.canada.ytdEI += inputs.canada.dedEI;

                    if (inputs.quebec == null) //this is calculating a bonus on a canadian run, not a quebec run, so gather these fields.
                    {
                        payrollMasterTaxBonus.ProvincialTax = (inputs.canada.dedITD_Prov != 0) ? Convert.ToDecimal(inputs.canada.dedITD_Prov) / 100 : inputs.canada.dedITD_Prov;
                        payrollMasterTaxBonus.EmployeeCpp = (inputs.canada.dedCPP != 0) ? Convert.ToDecimal(inputs.canada.dedCPP) / 100 : inputs.canada.dedCPP;
                        payrollMasterTaxBonus.EmployerCpp = (inputs.canada.employerCPPcost != 0) ? Convert.ToDecimal(inputs.canada.employerCPPcost) / 100 : inputs.canada.employerCPPcost;
                        payrollMasterTaxBonus.CppEarnings = (inputs.canada.CPP_Earnings != 0) ? Convert.ToDecimal(inputs.canada.CPP_Earnings) / 100 : inputs.canada.CPP_Earnings;

                        //add CPP deduction to YTD
                        inputs.canada.ytdCPP += inputs.canada.dedCPP;
                    }

                    PayrollOutput.Add(payrollMasterTaxBonus);


                    //stacy test have to check for RETRO, if not then REG.  in RETRO add this
                    //switch back to regular for run below if wagesAmount > 0
                    if (inputs.canada.wagesAmount > 0)
                        inputs.canada.CalculationType = CalculationType.RegularSalary;
                }

                #endregion

                #region QUEBEC BONUS RUN CODE

                if (inputs.quebec != null && inputs.quebec.CalculationType == CalculationTypeQC.Bonus)
                {
                    PayrollMasterTax payrollMasterTaxBonus = new PayrollMasterTax();
                    payrollMasterTaxBonus.EmployeeId = inputs.EmployeeId;
                    payrollMasterTaxBonus.PayrollProcessId = inputs.PayrollProcessId;
                    payrollMasterTaxBonus.PayrollMasterTaxId = i--;
                    payrollMasterTaxBonus.PayrollMasterId = inputs.PayrollMasterId;
                    payrollMasterTaxBonus.CalculationType = (int)inputs.quebec.CalculationType;
                    payrollMasterTaxBonus.QuebecCalculationFlag = true;

                    //if no wages, use BonusLumpSumTaxableIncome as bonus needs wages to determine ytd salary for taxes
                    bool changedWages = false;

                    if (inputs.quebec.wagesAmount == 0)
                    {
                        changedWages = true;
                        inputs.quebec.wagesAmount = inputs.BonusLumpSumTaxableIncome;
                    }

                    QPTL tempEngine = new QPTL();
                    tempEngine.calcDeductions(inputs.quebec);

                    //if wages were changed to use BonusLumpSumTaxableIncome, reset back to 0
                    if (changedWages)
                        inputs.quebec.wagesAmount = 0;

                    //these two are the same for bonus
                    payrollMasterTaxBonus.QuebecCombinedTaxes = (inputs.quebec.dedQITD) != 0 ? Convert.ToDecimal(inputs.quebec.dedQITD) / 100 : inputs.quebec.dedQITD;
                    payrollMasterTaxBonus.QuebecProvincialTax = (inputs.quebec.dedQITD) != 0 ? Convert.ToDecimal(inputs.quebec.dedQITD) / 100 : inputs.quebec.dedQITD;

                    payrollMasterTaxBonus.QuebecHeathContribution = (inputs.quebec.dedQITD_Z != 0) ? Convert.ToDecimal(inputs.quebec.dedQITD_Z) / 100 : inputs.quebec.dedQITD_Z;
                    payrollMasterTaxBonus.EmployeeQpip = (inputs.quebec.dedQPIP != 0) ? Convert.ToDecimal(inputs.quebec.dedQPIP) / 100 : inputs.quebec.dedQPIP;
                    payrollMasterTaxBonus.EmployeeQpp = (inputs.quebec.dedQPP != 0) ? Convert.ToDecimal(inputs.quebec.dedQPP) / 100 : inputs.quebec.dedQPP;
                    payrollMasterTaxBonus.EmployerQpip = (inputs.quebec.employerQPIPcost != 0) ? Convert.ToDecimal(inputs.quebec.employerQPIPcost) / 100 : inputs.quebec.employerQPIPcost;
                    payrollMasterTaxBonus.EmployerQpp = (inputs.quebec.employerQPPcost != 0) ? Convert.ToDecimal(inputs.quebec.employerQPPcost) / 100 : inputs.quebec.employerQPPcost;
                    payrollMasterTaxBonus.QpipEarnings = (inputs.quebec.QPIP_Earnings != 0) ? Convert.ToDecimal(inputs.quebec.QPIP_Earnings) / 100 : inputs.quebec.QPIP_Earnings;
                    payrollMasterTaxBonus.QppEarnings = (inputs.quebec.QPP_Earnings != 0) ? Convert.ToDecimal(inputs.quebec.QPP_Earnings) / 100 : inputs.quebec.QPP_Earnings;

                    //add QPP deduction to YTD
                    inputs.quebec.ytdQPP += inputs.quebec.dedQPP;
                    inputs.quebec.ytdQPIP += inputs.quebec.dedQPIP;
                    inputs.quebec.ytdQPIP_employer += inputs.quebec.employerQPIPcost;
                    inputs.quebec.ytdQITD_Z += inputs.quebec.dedQITD_Z;

                    payrollMasterTaxBonus.CreateUser = payrollMasterTaxBonus.UpdateUser = "metThreadingEngine";

                    PayrollOutput.Add(payrollMasterTaxBonus);

                    //stacy test have to check for RETRO, if not then REG.  in RETRO add this
                    //switch back to regular for run below if wagesAmount > 0
                    if (inputs.quebec.wagesAmount > 0)
                        inputs.quebec.CalculationType = CalculationTypeQC.RegularSalary;
                }

                #endregion


                //stacy test this is for RETRO*******************************************************************************************************************

                //last part of RETRO
                //if (inputs.canada.wagesAmount > 0)
                //    inputs.canada.CalculationType = CalculationType.RegularSalary;

                //end stacy test this is for RETRO*******************************************************************************************************************  

                #region canada and quebec regular/pension

                if (inputs.canada.CalculationType == CalculationType.RegularSalary || inputs.canada.CalculationType == CalculationType.Pension)
                {
                    PayrollMasterTax payrollMasterTax = new PayrollMasterTax();
                    payrollMasterTax.EmployeeId = inputs.EmployeeId;
                    payrollMasterTax.PayrollProcessId = inputs.PayrollProcessId;
                    payrollMasterTax.PayrollMasterTaxId = i--;
                    payrollMasterTax.PayrollMasterId = inputs.PayrollMasterId;
                    payrollMasterTax.CalculationType = (int)inputs.canada.CalculationType;

                    //reset these as a non-zero value is treated like an override (if we ran a bonus, then a reg, this could be populated from the bonus calculation so reset it for this one)
                    inputs.canada.CPP_Earnings = 0;
                    inputs.canada.EI_Earnings = 0;

                    //calc canadian taxes
                    CPTL engineCanada = new CPTL();
                    engineCanada.calcDeductions(inputs.canada);

                    //This is the return value for the amount of the calculated Employment Insurance (IE) deduction
                    payrollMasterTax.EmployeeEi = (inputs.canada.dedEI != 0) ? Convert.ToDecimal(inputs.canada.dedEI) / 100 : inputs.canada.dedEI;

                    //This is the return value for the amount of the federal portion of the total calculated Income Tax deduction.
                    payrollMasterTax.FederalTax = (inputs.canada.dedITD_Fed != 0) ? Convert.ToDecimal(inputs.canada.dedITD_Fed) / 100 : inputs.canada.dedITD_Fed;

                    //This is the return value for the amount of the calculated Income Tax deduction.
                    payrollMasterTax.CombinedTaxes = (inputs.canada.dedITD != 0) ? Convert.ToDecimal(inputs.canada.dedITD) / 100 : inputs.canada.dedITD;

                    //This is the return value for the calculated amount of the employer portion of Employment Insurance(EI) premiums based on the employee’s premium amount.
                    //As of January 1, 2014, the employer portion remains as an amount equal to 1.4 times the employee payroll deducted premium.
                    payrollMasterTax.EmployerEi = (inputs.canada.employerEIcost != 0) ? Convert.ToDecimal(inputs.canada.employerEIcost) / 100 : inputs.canada.employerEIcost;

                    //This is the return value for the total amount of EI insurable earnings calculated by CPTL, on which the current calculated deduction for EI premiums is based.
                    //This amount should be used to accumulate year - to - date totals of EI insurable earnings for each employee, which will be required for year-end employee tax reporting(T4).
                    payrollMasterTax.EiEarnings = (inputs.canada.EI_Earnings != 0) ? Convert.ToDecimal(inputs.canada.EI_Earnings) / 100 : inputs.canada.EI_Earnings;

                    //This is the return value for the calculated amount of the employer EI cost savings that are returnable to an employee when an employer is eligible for a reduced EI rate
                    payrollMasterTax.EmployeeEiReturnable = (inputs.canada.employeeEIreturnable != 0) ? Convert.ToDecimal(inputs.canada.employeeEIreturnable) / 100 : inputs.canada.employeeEIreturnable;

                    //restore ytd original value for db insert
                    inputs.canada.ytdEI = inputs.StartingYtdEiDeducted;

                    if (inputs.quebec == null) //this is calculating a canadian run, not a quebec run, so gather these fields.
                    {
                        payrollMasterTax.ProvincialTax = (inputs.canada.dedITD_Prov != 0) ? Convert.ToDecimal(inputs.canada.dedITD_Prov) / 100 : inputs.canada.dedITD_Prov;
                        payrollMasterTax.EmployeeCpp = (inputs.canada.dedCPP != 0) ? Convert.ToDecimal(inputs.canada.dedCPP) / 100 : inputs.canada.dedCPP;
                        payrollMasterTax.EmployerCpp = (inputs.canada.employerCPPcost != 0) ? Convert.ToDecimal(inputs.canada.employerCPPcost) / 100 : inputs.canada.employerCPPcost;
                        payrollMasterTax.CppEarnings = (inputs.canada.CPP_Earnings != 0) ? Convert.ToDecimal(inputs.canada.CPP_Earnings) / 100 : inputs.canada.CPP_Earnings;

                        //restore ytd original value for db insert
                        inputs.canada.ytdCPP = inputs.StartingYtdCppQppDeducted;
                    }

                    #region quebec

                    if (inputs.quebec != null && (inputs.quebec.CalculationType == CalculationTypeQC.RegularSalary || inputs.quebec.CalculationType == CalculationTypeQC.Pension))
                    {
                        //reset these as a non-zero value is treated like an override (if we ran a bonus, then a reg, this could be populated from the bonus calculation so reset it for this one)
                        inputs.quebec.QPP_Earnings = 0;
                        inputs.quebec.QPIP_Earnings = 0;

                        //calc quebec taxes
                        QPTL engine = new QPTL();
                        engine.calcDeductions(inputs.quebec);

                        //Calculated Quebec Income Tax deduction (including Requested Additional Deduction) for current payroll. The field is filled by the QPTL.calcDeductions(..) method.
                        payrollMasterTax.QuebecCombinedTaxes = (inputs.quebec.dedQITD) != 0 ? Convert.ToDecimal(inputs.quebec.dedQITD) / 100 : inputs.quebec.dedQITD;

                        //Calculated Quebec Income Tax deduction (excluding Requested Additional Deduction) for current payroll. The field is filled by the QPTL.calcDeductions(..) method.
                        payrollMasterTax.QuebecProvincialTax = (inputs.quebec.dedQITD_YP) != 0 ? Convert.ToDecimal(inputs.quebec.dedQITD_YP) / 100 : inputs.quebec.dedQITD_YP;

                        //Calculated Quebec Health contribution portion of income tax deduction amount (QITD).The field is filled by the QPTL.calcDeductions(..) method.
                        payrollMasterTax.QuebecHeathContribution = (inputs.quebec.dedQITD_Z != 0) ? Convert.ToDecimal(inputs.quebec.dedQITD_Z) / 100 : inputs.quebec.dedQITD_Z;

                        //Calculated Quebec Parental Insurance Program (QPIP) deduction for current payroll. The field is filled by the QPTL.calcDeductions(..) method.
                        payrollMasterTax.EmployeeQpip = (inputs.quebec.dedQPIP != 0) ? Convert.ToDecimal(inputs.quebec.dedQPIP) / 100 : inputs.quebec.dedQPIP;

                        //Calculated Quebec Pension Plan (QPP) deduction for current payroll. The field is filled by the QPTL.calcDeductions(..) method.
                        payrollMasterTax.EmployeeQpp = (inputs.quebec.dedQPP != 0) ? Convert.ToDecimal(inputs.quebec.dedQPP) / 100 : inputs.quebec.dedQPP;

                        //Calculated employer QPIP contribution (cost) for the current payroll calculation. The field is filled by the QPTL.calcDeductions(..) method.
                        payrollMasterTax.EmployerQpip = (inputs.quebec.employerQPIPcost != 0) ? Convert.ToDecimal(inputs.quebec.employerQPIPcost) / 100 : inputs.quebec.employerQPIPcost;

                        //Calculated employer QPP contribution (cost) for the current payroll calculation. The field is filled by the QPTL.calcDeductions(..) method.
                        payrollMasterTax.EmployerQpp = (inputs.quebec.employerQPPcost != 0) ? Convert.ToDecimal(inputs.quebec.employerQPPcost) / 100 : inputs.quebec.employerQPPcost;
                        //Calculated QPIP Insurable Earnings amount. The field is filled by the QPTL.calcDeductions(..) method.
                        payrollMasterTax.QpipEarnings = (inputs.quebec.QPIP_Earnings != 0) ? Convert.ToDecimal(inputs.quebec.QPIP_Earnings) / 100 : inputs.quebec.QPIP_Earnings;

                        //Calculated QPP Pensionable Earnings amount. The field is filled by the QPTL.calcDeductions(..) method
                        payrollMasterTax.QppEarnings = (inputs.quebec.QPP_Earnings != 0) ? Convert.ToDecimal(inputs.quebec.QPP_Earnings) / 100 : inputs.quebec.QPP_Earnings;

                        payrollMasterTax.CreateUser = payrollMasterTax.UpdateUser = "metThreadingEngine";

                        //restore ytd original value for db insert
                        inputs.quebec.ytdQPP = inputs.StartingYtdCppQppDeducted;
                        inputs.quebec.ytdQPIP = inputs.StartingYtdPpipDeducted;
                        inputs.quebec.ytdQPIP_employer = inputs.StartingYtdPpipEmployerDeducted;
                        inputs.quebec.ytdQITD_Z = inputs.StartingQuebecHealthDeducted;
                    }
                    #endregion

                    PayrollOutput.Add(payrollMasterTax);
                }
                #endregion
            }

            //PayrollOutput is object full of calc'd objects.
            return PayrollOutput;
        }
        public PayrollMasterCollection LoadPayrollMaster(DatabaseUser user, PayrollProcess payrollProcess)
        {
            return _payrollAccess.LoadPayrollMaster(user, payrollProcess);
        }

        public void PerformCalc(DatabaseUser user, PayrollProcess payrollProcess)
        {
            //get data from db
            PayrollMasterCollection payrollMasterColl = _payrollAccess.LoadPayrollMaster(user, payrollProcess);

            payrollMasterColl.LoadTransactions(_payrollAccess.GetPayrollTransactionPayrollMaster(user, payrollProcess.Batches.PayrollBatchIds));

            //store the employee paycode records in the employee paycode processed table for use with ceridian export
            long[] employeeIds = CreateEmployeeIdArray(payrollMasterColl);
            EmployeeAccess.InsertEmployeePaycodeProcessed(user, employeeIds, payrollProcess.PayrollProcessId);

            foreach (PayrollMasterCollection item in payrollMasterColl.Chunks)
            {
                //assign data to list of MET objects
                List<METEmployeeData> metData = MapToMET(item);

                //perform calcs
                List<PayrollMasterTax> payrollOutput = CalcData(metData);

                //store in db 
                _payrollAccess.InsertPayrollMasterAndTax(user, item, payrollOutput, payrollProcess.PayrollProcessRunTypeCode);
            }

        }
        private long[] CreateEmployeeIdArray(PayrollMasterCollection employees)
        {
            long[] employeeIds = new long[employees.Count];
            int i = 0;

            foreach (PayrollMaster emp in employees)
                employeeIds[i++] = emp.EmployeeId;

            return employeeIds;
        }
        public void ProcessPayrollBatchImportViaService(String database, String username, PayrollTransactionImportUsedInService[] array)
        {
            List<PayrollTransactionImportUsedInService> partial = new List<PayrollTransactionImportUsedInService>();

            for (int i = 0; i < array.Length; i++)
            {
                //add to list
                partial.Add(array[i]);

                if ((i + 1) % 5000 == 0 || i == array.Length - 1)
                {
                    //store in db 
                    _payrollAccess.ImportPayrollTransaction(database, username, partial);

                    //clear the list
                    partial.Clear();
                }
            }
        }
        //this is used by the WorklinksAutoCalcPostPayroll service
        public void AutoCalcPayroll(DatabaseUser user, DateTime todaysDate)
        {
            //call method to see if we should auto calc/post payroll today, used by WorklinksAutoCalcPostPayroll service
            AutoPayrollScheduleCollection scheduleColl = _payrollAccess.GetAutoPayrollSchedule(user, todaysDate);

            foreach (AutoPayrollSchedule schedule in scheduleColl)
            {
                AutoCalcPayroll(user, schedule);

                SendEmailParameters emailParms = new ReportManagement().GetEmailParms(user, null, null, null, DateTime.Now, schedule.CodePayrollProcessGroupCd, false, false, false, false, false, false, false, false, true, false);

                //Send notification once file transfer is complete
                EmailManagement.SendMessage(emailParms);
            }
        }
        public void AutoCalcPayroll(DatabaseUser user, AutoPayrollSchedule schedule)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //populate objects
                    PayrollProcess process = new PayrollProcess()
                    {
                        PayrollProcessStatusCode = "NW",
                        ChequeDate = schedule.ChequeDate,
                        PayrollProcessGroupCode = schedule.CodePayrollProcessGroupCd
                    };

                    PayrollPeriodCriteria criteria = new PayrollPeriodCriteria();
                    criteria.PayrollProcessGroupCode = process.PayrollProcessGroupCode;
                    criteria.GetMinOpenPeriodFlag = true;

                    PayrollPeriodCollection periods = GetPayrollPeriod(user, criteria);

                    if (periods.Count > 0)
                    {
                        periods[0].CopyTo(process);
                        process.PayrollProcessRunTypeCode = "REGULAR";
                        process.CurrentPayrollYear = process.PPCurrentPayrollYear;
                        process.LockedFlag = true;

                        //perform calcluation
                        CalculatePayroll(user, process, false, false, false, false);

                        //perform post
                        new WorkLinksDynamicsManagement().PostTransaction(user, process, "PT", null, null, null, null, false, false, false, false, false, false, false);

                        //update the schedule
                        schedule.ProcessedFlag = true;
                        schedule.PayrollProcessId = process.PayrollProcessId;
                        _payrollAccess.UpdateAutoPayrollScheduleProcessed(user, schedule);
                    }

                    //complete the scope
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void CalculatePayroll(DatabaseUser user, PayrollProcess item, bool autoGenSalaryEmployee, bool calculateBmsPension, bool prorationEnabledFlag, bool newArrearsProcessingFlag)
        {

            long autoGenBatchId = -1;

            item.PayrollProcessStatusCode = "CP"; //hardcode

            //set status of payroll
            UpdatePayrollProcess(user, item);

            PayrollProcessSummary process = GetPayrollProcessSummary(user, new PayrollProcessCriteria() { PayrollProcessId = item.PayrollProcessId, PayrollProcessStatusCode = null })[0];

            //copy needed information from "item" to "process" variables
            CopyItemData(user, process, item);

            //if "auto generate salary employee batch/transactions" is turned on and we are not handling an adjustment
            if (autoGenSalaryEmployee && item.PayrollProcessRunTypeCode.ToLower() != "adjust" && item.PayrollProcessRunTypeCode.ToLower() != "offcycle")
            {
                //insert a batch with SystemGeneratedFlag=true
                autoGenBatchId = InsertPayrollBatch(user, process);

                //add salary employee tranactions, if none exist, delete the batch
                if (!InsertPayrollBatchSalaryTransaction(user, autoGenBatchId, item.PayrollProcessRunTypeCode, item.PayrollProcessGroupCode, item.CutoffDate, prorationEnabledFlag)) //batch was deleted as it had no transactions, reset the ID variable
                    autoGenBatchId = -1;
            }

            //run rate progression
            if (item.PayrollProcessRunTypeCode.ToLower() != "adjust")
            {
                BonusRuleCollection rules = _payrollAccess.GetBonusRule(user, process.CutoffDate);
                PayrollTransactionRateProgressionBonusCollection bonus = _payrollAccess.GetPayrollTransactionRateProgressionBonus(user, process.PayrollProcessId, rules);
                PayrollTransactionRateProgressionCollection trans = _payrollAccess.GetPayrollTransactionRateProgression(user, process.PayrollProcessId);

                //if rate changes, save them
                if (trans.Count > 0)
                {
                    //create rate progression batch
                    long rateProgressionBatchId = InsertPayrollBatch(user, process, "Rate Progression");
                    PayrollTransactionCollection transactions = trans.GetRateAdjustedTransactions(rateProgressionBatchId);
                    _payrollAccess.InsertPayrollTransactionBatch(user, transactions);
                }
                if (bonus.Count > 0)
                {
                    //create rate progression batch
                    long rateProgressionBatchId = InsertPayrollBatch(user, process, "Service Premium");
                    PayrollTransactionCollection transactions = bonus.GetBonusTransactions(rateProgressionBatchId);
                    _payrollAccess.InsertPayrollTransactionBatch(user, transactions);
                }

                //save accumulations
                _payrollAccess.InsertPayrollTransaction(user, trans.GetCurrentAccumulations());
                _payrollAccess.InsertPayrollTransaction(user, bonus.GetCurrentAccumulations());
            }

            //load batches
            process.Batches = GetPayrollBatch(user, new PayrollBatchCriteria()
            {
                IncludePayrollTransaction = true,
                PayrollProcessRunTypeCode = item.PayrollProcessRunTypeCode,
                PayrollProcessGroupCode = item.PayrollProcessGroupCode,
                PerformingCalc = true
            });


            //this section will process against payroll_transaction_offset_association paycode matching against existing paycodes to this point
            if (process.Batches != null)
                GeneratePaycodeOffsets(user, autoGenBatchId, process.Batches.PayrollBatchIds, process);

            //bms propriatary employee paycode adjustments
            if (calculateBmsPension)
                CalculateBmsPension(user, process);

            //insert a batch with SystemGeneratedFlag=true
            long newBatchId = InsertPayrollBatch(user, process, "AUTO GENERATED EMPLOYEE PAYCODE BATCH");

            EmployeePaycodeCalculationCollection employeePaycodeCalculations = _payrollAccess.GetEmployeePaycodeAmount(user, process.PayrollProcessId, newArrearsProcessingFlag);

            //calc employee paycodes
            PaycodeAttachedPaycodeProvisionCollection provisions = CodeAccess.GetPaycodeAttachedPaycodeProvision(user, null);
            PayrollTransactionEmployeePaycodeCollection paycodeTransactions = _payrollAccess.GetPayrollTransactionEmployeePaycode(user, process.PayrollProcessId);
            employeePaycodeCalculations.LoadProvisions(provisions);
            employeePaycodeCalculations.LoadTransactions(paycodeTransactions, false, null);

            int id = -1;
            PaycodePayrollTransactionOffsetAssociationCollection offsets = _payrollAccess.GetPaycodePayrollTransactionOffsetAssociation(user, null);
            PayrollTransactionEmployeePaycodeCollection employeePayrollTransactions = employeePaycodeCalculations.GetTransactions(ref id, newBatchId, process.CutoffDate, false);
            employeePayrollTransactions.Add(paycodeTransactions.GetTaxTransactions(ref id, newBatchId, process.CutoffDate));
            employeePayrollTransactions.Add(offsets.GetOffsetTransactions(employeePayrollTransactions, ref id));

            //
            employeePaycodeCalculations.LoadTransactions(employeePayrollTransactions, true, employeePayrollTransactions);
            PayrollTransactionEmployeePaycodeCollection recurseCalc = employeePaycodeCalculations.GetTransactions(ref id, newBatchId, process.CutoffDate, true);
            recurseCalc.Add(offsets.GetOffsetTransactions(recurseCalc, ref id));
            employeePayrollTransactions.Add(recurseCalc);

            employeePaycodeCalculations.LoadTransactions(recurseCalc, true, employeePayrollTransactions);
            PayrollTransactionEmployeePaycodeCollection recurseCalc2 = employeePaycodeCalculations.GetTransactions(ref id, newBatchId, process.CutoffDate, true);
            recurseCalc2.Add(offsets.GetOffsetTransactions(recurseCalc2, ref id));
            employeePayrollTransactions.Add(recurseCalc2);


            //if transaction rows were inserted by the method below...
            if (employeePayrollTransactions.Count > 0)
            {
                _payrollAccess.InsertPayrollTransactionBatch(user, employeePayrollTransactions.PayrollTransactions);

                //this section will process against payroll_transaction_offset_association paycode matching against employee paycode calcs
                //GeneratePaycodeOffsets(user, autoGenBatchId, new long[] { newBatchId }, process);


            }
            else
            {
                //no transactions were inserted so delete the batch, reset the ID variable, do not reload batches.
                _payrollAccess.DeletePayrollBatch(user, new PayrollBatch() { PayrollBatchId = newBatchId });
                newBatchId = -1;
            }

            //reload batches
            process.Batches = GetPayrollBatch(user, new PayrollBatchCriteria()
            {
                IncludePayrollTransaction = true,
                PayrollProcessRunTypeCode = item.PayrollProcessRunTypeCode,
                PayrollProcessGroupCode = item.PayrollProcessGroupCode,
                PerformingCalc = true
            });

            //check for transactions and if none are found, return...
            if (process.Transactions != null && process.Transactions.Count > 0)
            {
                //stamp batches with payroll_process_id.  If called from Pay Process, do not update ADJUSTments.  If called from Adjustments Process, only update adjustments with manual run type
                if (item.PayrollProcessRunTypeCode.ToLower() != "adjust")
                    BatchUpdatePayrollBatch(user, process.Batches, process.PayrollProcessId, true);
                else if (item.PayrollProcessRunTypeCode.ToLower() == "adjust")
                    BatchUpdatePayrollBatch(user, process.Batches, process.PayrollProcessId, false);

                //update employee position ids
                _payrollAccess.BatchUpdateEmployeePositionPayrollProcessField(user, process.PayrollProcessId);

                //reload item as it's being sent back to the web layer
                item.Batches = process.Batches;

                //Clear transactions before going back to GUI, as they are fetched on that screen.
                item.Batches.ClearTransactions();

                //perform calcs using MET engine
                PerformCalc(user, item);

                //perform PayrollMasterPaycode/PayrollMasterPayment data
                PayrollMasterPaymentErrorCollection arrearsCollection = _payrollAccess.GeneratePayrollMasterData(user, item.PayrollProcessId);

                //only check for arrears if we aren't handling an adjustment
                if (process.PayrollProcessRunTypeCode.ToLower() != "adjust")
                {
                    if (arrearsCollection.Count > 0) //we have arrears
                    {
                        String arrearsMsg = GetArrearsMessage(arrearsCollection);
                        _payrollAccess.UpdatePayrollProcessStatusCode(user, item.PayrollProcessId, "ARR", arrearsMsg);
                    }
                }
            }
            else
                return;
        }
        private void ProcessEmployeePaycode()
        {
            //get current transactions

            //get employee paycodes

            //get year to date
        }
        public ImportExportLog ImportPayroll(DatabaseUser user, PayrollProcess item, byte[] uploadedFile, String uploadedFileName, bool autoAddSecondaryPositions, long? payrollProcessId = null)
        {
            item.PayrollProcessStatusCode = "CP"; //hardcode
            //set status of payroll
            UpdatePayrollProcess(user, item);

            //upload file
            XmlManagement xml = new XmlManagement();
            long importExportId = xml.GetImportExport(user, null, "RemittanceImp")[0].ImportExportId;
            String importExportDirectory = CodeAccess.GetCodeSystem(user)["IMPEXDIR"].Description;
            return xml.ImportFile(user, importExportId, uploadedFile, uploadedFileName, importExportDirectory, null, null, autoAddSecondaryPositions, false, item.PayrollProcessId);
        }
        public void UpdatePayrollProcess(DatabaseUser user, PayrollProcess item)
        {
            if (item.PayrollProcessId < 0)
                _payrollAccess.InsertPayrollProcess(user, item);
            else
            {
                item.ProcessException = null;
                _payrollAccess.UpdatePayrollProcess(user, item);
            }
        }
        public void UpdatePayrollProcess(DatabaseUser user, PayrollProcess item, PayrollProcess.CalculationProcess processType, String version, bool autoGenSalaryEmployee, bool calculateBmsPension, bool lockUnlockButtonPressed, bool prorationEnabledFlag, byte[] uploadedFile, String uploadedFileName, bool autoAddSecondaryPositions, bool newArrearsProcessingFlag)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    if (processType.Equals(PayrollProcess.CalculationProcess.Calculate))
                    {
                        CalculatePayroll(user, item, autoGenSalaryEmployee, calculateBmsPension, prorationEnabledFlag, newArrearsProcessingFlag);
                    }
                    else if (processType.Equals(PayrollProcess.CalculationProcess.Import))
                    {
                        ImportExportLog log = ImportPayroll(user, item, uploadedFile, uploadedFileName, autoAddSecondaryPositions, item.PayrollProcessId);

                        if (!log.SuccessFlag)
                            _payrollAccess.UpdatePayrollProcessStatusCode(user, item.PayrollProcessId, "IMP_ERR", log.ProcessingOutput);
                    }
                    else
                    {
                        if (item.PayrollProcessId < 0)
                            _payrollAccess.InsertPayrollProcess(user, item);
                        else
                        {
                            item.ProcessException = null;
                            _payrollAccess.UpdatePayrollProcess(user, item);
                        }

                        //are we resetting to new?
                        if (item.PayrollProcessStatusCode.ToLower().Equals("nw") && !lockUnlockButtonPressed) //do not perform a recalc if the lock/unlock buttons were pressed
                        {
                            //set status of payroll
                            UpdatePayrollProcess(user, item);

                            PayrollProcessSummary process = GetPayrollProcessSummary(user, new PayrollProcessCriteria() { PayrollProcessId = item.PayrollProcessId, PayrollProcessStatusCode = null })[0];

                            //reload batches
                            process.Batches = GetPayrollBatch(user, new PayrollBatchCriteria()
                            {
                                PayrollProcessId = process.PayrollProcessId,
                                IncludePayrollTransaction = true,
                                PayrollProcessRunTypeCode = process.PayrollProcessRunTypeCode,
                                PayrollProcessGroupCode = item.PayrollProcessGroupCode
                            });

                            //clean up any auto generated batches/transactions
                            bool reloadBatches = false;

                            foreach (PayrollBatch pBatch in process.Batches)
                            {
                                if (pBatch.SystemGeneratedFlag == true)
                                {
                                    reloadBatches = true;
                                    DeletePayrollBatchCascadeTransactions(user, pBatch);
                                }
                            }

                            //reload batches if auto generated batches were cleaned up
                            if (reloadBatches)
                            {
                                process.Batches = GetPayrollBatch(user, new PayrollBatchCriteria()
                                {
                                    PayrollProcessId = process.PayrollProcessId,
                                    IncludePayrollTransaction = true,
                                    PayrollProcessRunTypeCode = process.PayrollProcessRunTypeCode,
                                    PayrollProcessGroupCode = item.PayrollProcessGroupCode
                                });
                            }

                            EmployeeAccess.DeleteEmployeePaycodeProcessed(user, process.PayrollProcessId);

                            /* Process Id Update Section  */
                            //remove process id's.  If called from Pay Process, do not update ADJUSTments.  If called from Adjustments Process, only update adjustments with manual run type
                            if (process.PayrollProcessRunTypeCode.ToLower() != "adjust")
                                BatchUpdatePayrollBatch(user, process.Batches, null, true);
                            else if (process.PayrollProcessRunTypeCode.ToLower() == "adjust")
                                BatchUpdatePayrollBatch(user, process.Batches, null, false);

                            //delete from new payroll_master family of tables.  Above code will be deprecated once we move away from GP
                            _payrollAccess.PerformTableCleanups(user, item.PayrollProcessId);

                            WorkLinksDynamicsManagement management = new WorkLinksDynamicsManagement();

                            //revert the update to employee_position.payroll_process_id column
                            management.RevertUpdateTerminatedEmployees(user, item.PayrollProcessId);
                        }
                    }
                    scope.Complete();
                }
            }
            catch (PayrollValidationException ex)
            {
                PayrollValidationFault validationFault = new PayrollValidationFault(ex.Message);
                throw new FaultException<PayrollValidationFault>(validationFault, new FaultReason(ex.Message));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void CalculateBmsPension(DatabaseUser user, PayrollProcess process)
        {
            EmployeeAccess.EmployeePaycodeSetMaximumPensionAmount(user, process.PayrollProcessId);
        }
        private DateTime GetFirstDayOfMonth(DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }
        private long[] CreateEmployeeIdArray(EmployeeCollection employees)
        {
            long[] employeeIds = new long[employees.Count];
            int i = 0;

            foreach (Employee emp in employees)
                employeeIds[i++] = emp.EmployeeId;

            return employeeIds;
        }
        private void GeneratePaycodeOffsets(DatabaseUser user, long autoGenBatchId, long[] batchIds, PayrollProcessSummary process)
        {
            PayrollTransactionCollection bonusTransactionWithDeductionColl = _payrollAccess.CheckForBonusWithDeduction(user, batchIds);

            if (bonusTransactionWithDeductionColl.Count > 0)
            {
                if (autoGenBatchId == -1)
                    autoGenBatchId = InsertPayrollBatch(user, process);

                //need to append transactions to the existing auto generated batch
                InsertBonusWithDeductionBatchTrans(user, bonusTransactionWithDeductionColl, autoGenBatchId, process.PayrollProcessGroupCode, process.CutoffDate);
            }
        }
        private string GetArrearsMessage(PayrollMasterPaymentErrorCollection coll)
        {
            String arrearsMsg = "";

            foreach (PayrollMasterPaymentError error in coll)
                arrearsMsg += "Employee " + error.EmployeeNumber + " has arrears of:  " + String.Format("{0:#.00}", error.Amount) + "<br />";

            return arrearsMsg;
        }
        private long InsertPayrollBatch(DatabaseUser user, PayrollProcessSummary processVar, String description = "AUTO GENERATED BATCH")
        {
            PayrollBatch newBatch = new PayrollBatch();
            newBatch.PayrollProcessGroupCode = processVar.PayrollProcessGroupCode;
            newBatch.BatchSource = processVar.PayrollProcessGroupCode + "-" + processVar.CutoffDate;
            newBatch.PayrollBatchStatusCode = "AP";
            newBatch.PayrollProcessRunTypeCode = processVar.PayrollProcessRunTypeCode;
            newBatch.Description = description;
            newBatch.SystemGeneratedFlag = true;
            newBatch.CreateUser = processVar.UpdateUser;
            newBatch.CreateDatetime = DateTime.Now;
            newBatch.UpdateUser = processVar.UpdateUser;
            newBatch.UpdateDatetime = DateTime.Now;

            return _payrollAccess.InsertPayrollBatch(user, newBatch).PayrollBatchId;
        }
        private void CopyItemData(DatabaseUser user, PayrollProcessSummary process, PayrollProcess item)
        {
            //PayrollProcessSummary inherits from PayrollProcess so just assign from "item" to new object "process".
            process.StartDate = item.StartDate;
            process.PeriodYear = item.PeriodYear;
            process.Period = item.Period;
            process.UpdateUser = item.UpdateUser;
            process.PayrollProcessRunTypeCodeDescription = item.PayrollProcessRunTypeCodeDescription;
        }
        private PayrollPeriodOverrideCollection CheckForPayrollPeriodOverride(DatabaseUser user, String payrollProcessGroupCode, decimal periodYear, decimal period)
        {
            try
            {
                return _payrollAccess.CheckForPayrollPeriodOverride(user, payrollProcessGroupCode, periodYear, period);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - CheckForPayrollPeriodOverride", ex);
                throw;
            }
        }
        public long ClosePeriod(DatabaseUser user, PayrollProcess item)
        {
            try
            {
                long newPeriodId = -1;

                using (TransactionScope scope = new TransactionScope())
                {
                    if (item.PayrollProcessRunTypeCode.ToLower().Equals("regular")) /**HACK**hard code*/
                    {
                        //close current processs period
                        ClosePeriod(user, item, item.PayrollProcessId);

                        //create new process period
                        PayrollPeriod newPeriod = new PayrollPeriod();

                        //check the payroll_period_override table to see if the next period exists, if so use it, otherwise use the usual logic.
                        //this is used in the case of a SemiMonthly payroll where they are not using a start_day of 1, 16 and have non-uniform period lengths.
                        PayrollPeriodOverrideCollection payrollPeriodOverrideCollection = CheckForPayrollPeriodOverride(user, item.PayrollProcessGroupCode, item.PeriodYear, item.Period + 1);
                        if (payrollPeriodOverrideCollection != null && payrollPeriodOverrideCollection.Count > 0)
                        {
                            newPeriod.StartDate = payrollPeriodOverrideCollection[0].StartDate;
                            newPeriod.CutoffDate = payrollPeriodOverrideCollection[0].CutoffDate;
                            newPeriod.PeriodYear = payrollPeriodOverrideCollection[0].PeriodYear;
                            newPeriod.Period = payrollPeriodOverrideCollection[0].Period;
                        }
                        else
                        {
                            newPeriod.StartDate = item.CutoffDate.AddDays(1);
                            newPeriod.CutoffDate = CalculateCutoffDate(user, item, newPeriod.StartDate);
                            newPeriod.PeriodYear = item.PeriodYear;
                            newPeriod.Period = item.Period + 1;
                        }

                        newPeriod.PayrollProcessGroupCode = item.PayrollProcessGroupCode;
                        newPeriod.UpdateUser = user.UserName;
                        newPeriod.UpdateDatetime = DateTime.Now;

                        InsertPayrollPeriod(user, newPeriod);
                        newPeriodId = newPeriod.PayrollPeriodId;
                    }

                    scope.Complete();
                }

                return newPeriodId;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - ClosePeriod", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        private DateTime CalculateCutoffDate(DatabaseUser user, PayrollPeriod lastPeriod, DateTime startDate)
        {
            DateTime cutoffDate = DateTime.MinValue;
            String frequencyCode = CodeAccess.GetPayrollProcessingGroup(user, lastPeriod.PayrollProcessGroupCode)[0].PaymentFrequencyCode; //get frequency

            switch (frequencyCode)
            {
                case "1": //Yearly
                    if (DateTime.IsLeapYear(startDate.Year))
                        cutoffDate = startDate.AddDays(365);
                    else
                        cutoffDate = startDate.AddDays(364);
                    break;
                case "2": //Half-Yearly
                    cutoffDate = startDate.AddMonths(6).AddDays(-1);
                    break;
                case "4": //Quarterly
                    cutoffDate = startDate.AddMonths(3).AddDays(-1);
                    break;
                case "6": //Bi-Monthly
                    cutoffDate = startDate.AddMonths(2).AddDays(-1);
                    break;
                case "12": //Monthly
                    cutoffDate = startDate.AddMonths(1).AddDays(-1);
                    break;
                case "24": //Semi-Monthly
                    cutoffDate = lastPeriod.StartDate.AddMonths(1).AddDays(-1);
                    break;
                case "26/27": //Bi-Weekly
                    cutoffDate = startDate.AddDays(14 - 1);
                    break;
                case "52/53": //Weekly
                    cutoffDate = startDate.AddDays(7 - 1);
                    break;
            }

            return cutoffDate;
        }
        #endregion

        #region Pending ROE
        public RoeCreationSearchResultsCollection GetPendingRoeEmployeeSummary(DatabaseUser user, String criteria)
        {
            try
            {
                //get pending ROEs
                RoeCreationSearchResultsCollection pendingRoes = _payrollAccess.GetPendingRoeEmployeeSummary(user, criteria);
                return pendingRoes;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - GetPendingRoeEmployeeSummary", ex);
                throw;
            }
        }
        public void SetPendingRoeStatusForEmployeeId(DatabaseUser user, long selectedEmployeeId)
        {
            try
            {
                EmployeePositionCollection coll = new EmployeePositionCollection(); //get employee position record
                EmployeePositionCriteria criteria = new EmployeePositionCriteria() { EmployeeId = selectedEmployeeId, GetCurrentPositionFlag = true };

                coll = EmployeeManagement.GetEmployeePosition(user, criteria);
                coll[0].CreateUser = user.UserName; coll[0].UpdateUser = user.UserName; coll[0].CodeRoeCreationStatusCd = "PEN";

                _payrollAccess.SetPendingRoeStatusForEmployeeId(user, coll[0]);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - SetPendingRoeStatusForEmployeeId", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50001)
                    {
                        EmployeeServiceException otherException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.Other);
                        throw new FaultException<EmployeeServiceException>(otherException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public EmployeeRoeAmountCollection GetRoeData(DatabaseUser user, long employeePositionId, long employeeId)
        {
            try
            {
                EmployeeRoeAmountCollection coll = new EmployeeRoeAmountCollection();
                coll = _payrollAccess.GetRoeData(user, employeePositionId, employeeId);
                coll[0].AmountDetailSummaryCollection = _payrollAccess.GetRoeDataDetailsSummary(user, employeePositionId, employeeId);

                return coll;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - GetRoeData", ex);
                throw;
            }
        }
        public void UpdateEmployeeRoeAmount(DatabaseUser user, EmployeeRoeAmount roeObj)
        {
            try
            {
                _payrollAccess.UpdateEmployeeRoeAmount(user, roeObj);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - UpdateEmployeeRoeAmount", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public EmployeeRoeAmountDetailCollection UpdateEmployeeRoeDetail(DatabaseUser user, EmployeeRoeAmountDetailCollection collection)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    for (int i = 0; i < collection.Count; i++)
                        _payrollAccess.UpdateEmployeeRoeDetail(user, collection[i]);

                    scope.Complete();
                }

                return collection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - UpdateEmployeeRoeDetail", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public EmployeeRoeAmountDetailSummaryCollection GetRoeDataDetailsSummary(DatabaseUser user, long employeePositionId, long employeeId)
        {
            try
            {
                return _payrollAccess.GetRoeDataDetailsSummary(user, employeePositionId, employeeId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - GetRoeDataDetailsSummary", ex);
                throw;
            }
        }
        #endregion

        #region payroll batch
        public PayrollBatchReportCollection GetPayrollBatchReport(DatabaseUser user, PayrollBatchCriteria criteria)
        {
            try
            {
                return _payrollAccess.GetPayrollBatchReport(user, criteria);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - GetPayrollBatchReport", ex);
                throw;
            }
        }
        public PayrollBatchCollection GetPayrollBatch(DatabaseUser user, long payrollBatchId)
        {
            try
            {
                return GetPayrollBatch(user, new PayrollBatchCriteria() { PayrollBatchId = payrollBatchId });
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - GetPayrollBatchPBatchIdArg", ex);
                throw;
            }
        }
        public PayrollBatchCollection GetPayrollBatch(DatabaseUser user, PayrollBatchCriteria criteria)
        {
            try
            {
                PayrollBatchCollection collection = _payrollAccess.GetPayrollBatch(user, criteria);

                if (criteria.IncludePayrollTransaction)
                {
                    foreach (PayrollBatch batch in collection)
                    {
                        if (criteria.IncludeOnlyApprovedTransactions && !batch.PayrollBatchStatusCode.Equals("AP")) /**HACK*** need application constant*/
                        {
                            // do nothing
                        }
                        else
                            batch.Transactions = _payrollAccess.GetPayrollTransactionSummary(user, batch.PayrollBatchId);
                    }
                }

                return collection;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - GetPayrollBatchCriteriaArg", ex);
                throw;
            }
        }
        public PayrollBatch InsertPayrollBatch(DatabaseUser user, PayrollBatch item)
        {
            try
            {
                return _payrollAccess.InsertPayrollBatch(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - InsertPayrollBatch", ex);
                throw;
            }
        }
        public void UpdatePayrollBatch(DatabaseUser user, PayrollBatch item)
        {
            try
            {
                _payrollAccess.UpdatePayrollBatch(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - UpdatePayrollBatch", ex);
                throw;
            }
        }
        public void BatchUpdatePayrollBatch(DatabaseUser user, PayrollBatchCollection items, long? payrollProcessId, bool filterAdjustments)
        {
            try
            {
                PayrollBatchCollection tempCollection = new PayrollBatchCollection();

                //gather list of batchids
                if (filterAdjustments)
                {
                    foreach (PayrollBatch batch in items)
                    {
                        if (batch.PayrollProcessRunTypeCode.ToLower() != "adjust")
                        {
                            batch.PayrollProcessId = payrollProcessId;
                            tempCollection.Add(batch);
                        }
                    }
                }
                else
                {
                    foreach (PayrollBatch batch in items)
                    {
                        if (batch.PayrollProcessRunTypeCode.ToLower() == "adjust")
                        {
                            batch.PayrollProcessId = payrollProcessId;
                            tempCollection.Add(batch);
                        }
                    }
                }

                //cleanup and call database
                if (tempCollection != null && tempCollection.Count > 0)
                    _payrollAccess.BatchUpdatePayrollBatch(user, tempCollection);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - UpdatePayrollBatch", ex);
                throw;
            }
        }
        public void DeletePayrollBatch(DatabaseUser user, PayrollBatchReport item)
        {
            try
            {
                _payrollAccess.DeletePayrollBatch(user, item);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - DeletePayrollBatch", ex);
                throw;
            }
        }
        public PayrollPeriodCollection GetPayrollPeriodIdFromPayrollProcessGroupCode(DatabaseUser user, PayrollBatchCriteria payrollBatchCriteria)
        {
            try
            {
                return _payrollAccess.GetPayrollPeriodIdFromPayrollProcessGroupCode(user, payrollBatchCriteria);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - GetPayrollPeriodIdFromPayrollProcessGroupCode", ex);
                throw;
            }
        }
        public void InsertBonusWithDeductionBatchTrans(DatabaseUser user, PayrollTransactionCollection transCollection, long payrollBatchId, String payrollProcessGroupCode, DateTime transactionDate)
        {
            try
            {
                //insert these records into the current batch
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (PayrollTransaction trans in transCollection)
                        trans.PayrollBatchId = payrollBatchId;

                    _payrollAccess.InsertPayrollTransactionBatch(user, transCollection);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - InsertBonusWithDeductionBatchTrans", ex);
                throw;
            }
        }
        public SalaryEmployeeCollection GetSalaryEmployees(DatabaseUser user, String payrollProcessGroupCode, bool prorationEnabledFlag)
        {
            return _payrollAccess.GetSalaryEmployees(user, payrollProcessGroupCode, prorationEnabledFlag);
        }
        public bool InsertPayrollBatchSalaryTransaction(DatabaseUser user, long payrollBatchId, String payrollProcessRunTypeCode, String payrollProcessGroupCode, DateTime transactionDate, bool prorationEnabledFlag)
        {
            bool reload = false;

            try
            {
                //get the salaried employees for this Process Group
                SalaryEmployeeCollection tempEmployees = _payrollAccess.GetSalaryEmployees(user, payrollProcessGroupCode, prorationEnabledFlag);

                //for each record, insert the needed data and such into a PayrollTransactionByBatchReport record collection
                PayrollTransactionSummaryCollection collection = CreatePayrollTransactionSalaryCollection(user, tempEmployees, transactionDate, payrollProcessRunTypeCode, payrollProcessGroupCode, payrollBatchId, prorationEnabledFlag);

                using (TransactionScope scope = new TransactionScope())
                {
                    if (collection.Count > 0)//insert these records into the current batch
                    {
                        _payrollAccess.InsertPayrollTransactionBatch(user, collection.PayrollTransactionCollection);
                        reload = true;
                    }
                    else
                        _payrollAccess.DeletePayrollBatch(user, new PayrollBatch() { PayrollBatchId = payrollBatchId });

                    scope.Complete();
                }

                return reload;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - InsertPayrollBatchTrans", ex);
                throw;
            }
        }
        private void GetPositions(DatabaseUser user, SalaryEmployeeCollection tempEmployees)
        {
            //call db
            EmployeePositionCollection positions = EmployeeManagement.GetBatchEmployeePosition(user, tempEmployees, true);

            //assign positions to employee object
            foreach (SalaryEmployee employee in tempEmployees)
                employee.Position = positions[Math.Abs(employee.EmployeePositionId).ToString()];
        }
        public PayrollTransactionSummaryCollection CreatePayrollTransactionSalaryCollection(DatabaseUser user, SalaryEmployeeCollection tempEmployees, DateTime transactionDate, String payrollProcessRunTypeCode, String payrollProcessGroupCode, long payrollBatchId, bool prorationEnabledFlag)
        {
            //step one, get all the EmployeePositions for the salary employee
            GetPositions(user, tempEmployees);

            int payrollTransactionId = -1;
            PayrollTransactionSummaryCollection returnCollection = new PayrollTransactionSummaryCollection();
            PayrollTransactionSummaryCollection prorationTransactions = null;

            if (prorationEnabledFlag) //cache out calculate out proration collection
            {
                SalaryEmployeeCollection prorationCollection = tempEmployees.ProrationCollection;

                foreach (SalaryEmployee prorationEmployee in prorationCollection)
                {
                    if (prorationTransactions == null)
                        prorationTransactions = new PayrollTransactionSummaryCollection();

                    PayrollTransactionSummary proratedTransaction = CreateNewPayrollTransactionSummaryRecord(user, payrollTransactionId--, prorationEmployee, prorationEmployee.EndDateRange, payrollBatchId, prorationEmployee.OffsetHour, 0);
                    prorationTransactions.Add(proratedTransaction);
                    AddPayrollTransactionSummary(returnCollection, proratedTransaction);
                }
            }

            //fill collection with data
            foreach (SalaryEmployee salEmployee in tempEmployees.PrimaryCollection)
            {
                decimal salaryHoursDeduction = 0;
                decimal salaryAmountDeduction = 0;

                //subtract off proration transactions
                if (prorationTransactions != null)
                {
                    //are there any terms?
                    PayrollTransactionSummaryCollection employeeProrations = prorationTransactions.GetEmployeeTransactions(salEmployee.EmployeeId);

                    if (employeeProrations.Count > 0)
                    {
                        if (tempEmployees.GetEmployeeSalaryCollection(salEmployee.EmployeeId).IsDailyCalculationOnly) //terminations/hires prorated by day
                            salEmployee.PrimaryRecordFlag = false;
                        else
                        {
                            foreach (PayrollTransaction transaction in employeeProrations)
                            {
                                //reduce salary by prorated hours
                                salaryHoursDeduction += transaction.Units;
                            }
                        }

                    }

                }
                //subtract off offset paycode units
                salaryHoursDeduction += salEmployee.OffsetHour; // + salEmployee.OffsetHour;

                if (salEmployee.CodePaymentMethodCd != "SH" && salaryHoursDeduction != 0) //salaried hourly employees do not need to go thru this process to calc "salaryAmountDeduction"
                    salaryAmountDeduction = Math.Round(salEmployee.Position.RatePerHour * salaryHoursDeduction, 2, MidpointRounding.AwayFromZero);

                if (salEmployee.PaycodeCode != null)
                {
                    PayrollTransactionSummary payrollTransaction = CreateNewPayrollTransactionSummaryRecord(user, payrollTransactionId--, salEmployee, transactionDate, payrollBatchId, salaryHoursDeduction, salaryAmountDeduction);
                    AddPayrollTransactionSummary(returnCollection, payrollTransaction);
                }
            }

            return returnCollection;
        }
        private void AddPayrollTransactionSummary(PayrollTransactionSummaryCollection collection, PayrollTransactionSummary trans)
        {
            //do not add transactions that have $0 rate
            if (trans.Rate != 0)
                collection.Add(trans);
        }
        private PayrollTransactionSummary CreateNewPayrollTransactionSummaryRecord(DatabaseUser user, long payrollTransactionId, SalaryEmployee salEmployee, DateTime transactionDate, long payrollBatchId, decimal salaryHoursDeduction, decimal salaryAmountDeduction)
        {
            PayrollTransactionSummary payrollTransaction = new PayrollTransactionSummary();
            payrollTransaction.EmployeeId = salEmployee.EmployeeId;
            payrollTransaction.PayrollTransactionId = payrollTransactionId;
            payrollTransaction.PaycodeTypeCode = "1"; //Income
            payrollTransaction.PaycodeCode = salEmployee.PaycodeCode;
            payrollTransaction.EntryTypeCode = "NORM"; //Normal
            payrollTransaction.TransactionDate = transactionDate;
            payrollTransaction.PayrollBatchId = payrollBatchId;
            payrollTransaction.OffsetHour = salEmployee.OffsetHour;

            //modify salary rate calculation to use locked_compensation_rate if not null
            decimal? compAmt = (salEmployee.LockedCompensationAmount != null) ? salEmployee.LockedCompensationAmount : salEmployee.CompensationAmount;
            decimal? stdHours = (salEmployee.LockedStandardHours != null) ? salEmployee.LockedStandardHours : salEmployee.StandardHours;

            if (salEmployee.PrimaryRecordFlag) //regular salary calc
            {
                if (salEmployee.CodePaymentMethodCd == "SH") //just get hourly rate 
                {
                    payrollTransaction.Units = Convert.ToDecimal(stdHours) - salaryHoursDeduction;
                    payrollTransaction.Rate = Math.Round(Convert.ToDecimal(compAmt) * salEmployee.AmountRateFactor, 2, MidpointRounding.AwayFromZero);
                }
                else //rate is total
                {
                    payrollTransaction.Units = 1;
                    payrollTransaction.Rate = Math.Round(Convert.ToDecimal(compAmt) * salEmployee.AmountRateFactor, 2, MidpointRounding.AwayFromZero) - Math.Round(salaryAmountDeduction * salEmployee.AmountRateFactor, 2, MidpointRounding.AwayFromZero);
                }
            }
            else //proration calculation
            {
                payrollTransaction.Units = (int)salEmployee.WorkdayCount * GetRegularHoursPerDay(salEmployee.HoursPerWeek, salEmployee.WorkDaysPerWeek) - salaryHoursDeduction;
                payrollTransaction.Rate = salEmployee.PaycodeCode == null ? 0 : Math.Round(salEmployee.Position.RatePerHour * salEmployee.AmountRateFactor, 4, MidpointRounding.AwayFromZero); //no salary paycode, ignore salary
            }

            if (salEmployee != null && salEmployee.AutoPopulateRateFlag)
                payrollTransaction.BaseSalaryRate = salEmployee.Position.RatePerHour;

            payrollTransaction.UpdateUser = user.UserName;
            payrollTransaction.SetCalculateEmploymentInsuranceHourAndUnits(payrollTransaction.Units, Convert.ToDecimal(stdHours), salaryHoursDeduction, salEmployee.IncludeEmploymentInsuranceHoursFlag, salEmployee.PrimaryRecordFlag ? salEmployee.UseSalaryStandardHourFlag : false);

            return payrollTransaction;
        }
        private decimal GetRegularHoursPerDay(Decimal regularPayHourPerWeek, int workDaysPerWeek)
        {
            return Math.Round((regularPayHourPerWeek / workDaysPerWeek), 2, MidpointRounding.AwayFromZero);
        }
        #endregion

        #region payroll batch import

        private Dictionary<string, PayrollTransaction> GetUniqueEmployeeIdsAndTransactionDates(PayrollTransactionSummaryCollection payrollTransactions)
        {
            Dictionary<string, PayrollTransaction> data = new Dictionary<string, PayrollTransaction>();

            foreach (PayrollTransaction payTrans in payrollTransactions)
            {
                if (!data.ContainsKey(payTrans.EmployeeId.ToString() + payTrans.TransactionDate.ToString("yyyy-MM-dd")))
                    data.Add(payTrans.EmployeeId.ToString() + payTrans.TransactionDate.Date.ToString("yyyy-MM-dd"),
                        new PayrollTransaction() { EmployeeId = payTrans.EmployeeId, TransactionDate = payTrans.TransactionDate });
            }

            return data;
        }

        private Dictionary<string, EmployeeRate> GetPositionsByEmployeeIdAndTransactionDate(DatabaseUser user, PayrollTransactionSummaryCollection trans)
        {
            //get a list of unique "employeeids + dates" to query the db with.
            Dictionary<string, PayrollTransaction> data = GetUniqueEmployeeIdsAndTransactionDates(trans);

            //get positions by employeeid and trans date
            EmployeePositionEmployeeIdTransactionDateCollection coll = GetBatchPositionsByEmployeeIdAndTransactionDate(user, data);

            //batch select to fill the collection
            BatchSelectPositionWorkdayOrgAndSecondary(user, coll, true);

            //add positions to dictionary
            Dictionary<string, EmployeeRate> dictEmployeePosition = new Dictionary<string, EmployeeRate>();
            foreach (EmployeePositionEmployeeIdTransactionDate pos in coll)
            {
                EmployeeRate rate = new EmployeeRate();
                pos.CopyTo(rate);
                dictEmployeePosition.Add(pos.DummyKey, rate);
            }

            return dictEmployeePosition;
        }

        private EmployeePositionEmployeeIdTransactionDateCollection BatchSelectPositionWorkdayOrgAndSecondary(DatabaseUser user, EmployeePositionEmployeeIdTransactionDateCollection positionTrans, bool securityOverrideFlag)
        {
            try
            {
                return EmployeeManagement.BatchSelectPositionWorkdayOrgAndSecondary(user, positionTrans, securityOverrideFlag);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - BatchSelectPositionWorkdayOrgAndSecondary", ex);
                throw;
            }
        }

        private EmployeePositionEmployeeIdTransactionDateCollection GetBatchPositionsByEmployeeIdAndTransactionDate(DatabaseUser user, Dictionary<string, PayrollTransaction> data)
        {
            try
            {
                return EmployeeAccess.GetBatchPositionsByEmployeeIdAndTransactionDate(user, data);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - GetBatchPositionsByEmployeeIdAndTransactionDate", ex);
                throw;
            }
        }

        public void ProcessPayrollBatchImport(DatabaseUser user, BusinessObjects.Xsd.WorkLinksPayrollBatch xsdObject, ImportExportLog log, bool automaticImportFlag, string codePayrollBatchTypeCd = null)
        {
            try
            {
                StringBuilder output = new StringBuilder();
                PayrollTransactionImportCollection transactions = new PayrollTransactionImportCollection(xsdObject);

                //sync transactions
                Dictionary<String, EmployeeSummary> employeeCache = new Dictionary<String, EmployeeSummary>();
                Dictionary<String, CodePaycode> paycodeCache = new Dictionary<String, CodePaycode>();
                Dictionary<String, String> orgUnitCache = new Dictionary<String, String>();

                foreach (PayrollTransactionImport transaction in transactions)
                {
                    output.Append(EmployeeManagement.SetEmployeeIdFromIdentifier(user, transaction, employeeCache));
                    output.Append(EmployeeManagement.SetPaycodeCodeFromIdentifier(user, transaction, paycodeCache));
                    output.Append(EmployeeManagement.SetOrganizationUnitFromIdentifier(user, transaction, orgUnitCache));
                }

                //validate some records can process
                if (transactions.HasAllErrors)
                {
                    output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportValidationFail") + "\r\n" + "----------\r\n");
                    log.ProcessingOutput += output.ToString();
                    log.SuccessFlag = false;
                }
                else
                {
                    PayrollTransactionErrorCollection payrollTransactionErrors = new PayrollTransactionErrorCollection();

                    foreach (String payrollProcessGroupCode in transactions.GetAllPayrollProcessGroupCodes())
                    {
                        PayrollBatchCollection openBatches = null; //insert batch if needed
                        PayrollBatchImport batch = new PayrollBatchImport(xsdObject, payrollProcessGroupCode);
                        batch.PayrollProcessRunTypeCode = batch.PayrollBatchEntryTypeCodeExternalIdentifier;

                        //this will be null unless this batch is being imported via FTP on the process screen
                        batch.PayrollBatchTypeCode = codePayrollBatchTypeCd;

                        if (automaticImportFlag && batch.BatchSource != null && !batch.BatchSource.Equals(String.Empty))
                            openBatches = GetPayrollBatch(user, new PayrollBatchCriteria() { PayrollProcessGroupCode = batch.PayrollProcessGroupCode, BatchSource = batch.BatchSource });

                        if (openBatches != null && openBatches.Count > 0)
                            openBatches[0].CopyTo(batch);
                        else
                            InsertPayrollBatch(user, batch);

                        PayrollTransactionCollection save = new PayrollTransactionCollection();

                        //these are the transactions the foreach will go thru
                        PayrollTransactionSummaryCollection trans = transactions.GetValidPayrollTransactionSummaryCollection(payrollProcessGroupCode);

                        //get cutoff date
                        PayrollBatchCriteria payrollBatchCriteria = new PayrollBatchCriteria()
                        {
                            PayrollProcessGroupCode = batch.PayrollProcessGroupCode,
                            PayrollProcessRunTypeCode = batch.PayrollProcessRunTypeCode
                        };

                        PayrollPeriod period = GetPayrollPeriodIdFromPayrollProcessGroupCode(user, payrollBatchCriteria)[0];
                        DateTime cutoffDate = period.CutoffDate;

                        trans.DefaultUnsetTransactionDates(cutoffDate);

                        //get collection of employee positions based on employeeid and transactiondate
                        Dictionary<string, EmployeeRate> positions = GetPositionsByEmployeeIdAndTransactionDate(user, trans);

                        foreach (PayrollTransactionImport transaction in trans) //sync batch number/insert
                        {
                            if (transaction.EmployeeId > 0 && transaction.PaycodeCode != null) //do checking here for transaction Fed/Prov Tax Amounts
                            {
                                if (transaction.FederalTaxAmountContainsMoreThanTwoDecimalPlaces || transaction.ProvincialTaxAmountContainsMoreThanTwoDecimalPlaces)
                                {
                                    output.Append(String.Format(GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "TooManyDecimalPlaces"), transaction.EmployeeImportExternalIdentifier, transaction.PaycodeCode) + "\r\n");
                                    transaction.HasErrors = true;
                                }
                                else
                                {


                                    if (transaction.Rate == null)
                                    {
                                        EmployeeRate position = null;

                                        if (positions.ContainsKey(transaction.EmployeeId.ToString() + transaction.TransactionDate.ToString("yyyy-MM-dd")))
                                            position = positions[transaction.EmployeeId.ToString() + transaction.TransactionDate.ToString("yyyy-MM-dd")];

                                        CodePaycode code = paycodeCache[transaction.PaycodeCodeImportExternalIdentifier];//CodeAccess.GetCodePaycode(user, transaction.PaycodeCode, false)[0];

                                        if (transaction.OrganizationUnit == null)
                                        {
                                            transaction.OrganizationUnit = position.OrganizationUnit;
                                            if (transaction.Rate == null)
                                                SetRate(user, transaction, code, position.RatePerHour);
                                        }
                                        else
                                        {
                                            //check if the org unit in the transaction is the same as the position
                                            if (position.SecondaryPositions.Count == 0)
                                            {
                                                if (transaction.Rate == null)
                                                    SetRate(user, transaction, code, position.RatePerHour);
                                            }
                                            else
                                            {
                                                IEmployeePosition matchedPosition = position.GetMatchedEmployeePosition(transaction.OrganizationUnit);

                                                if (matchedPosition != null)
                                                {
                                                    if (transaction.Rate == null)
                                                        SetRate(user, transaction, code, matchedPosition.RatePerHour);
                                                }
                                                else
                                                {
                                                    output.Append(String.Format(GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "EmployeeUnassignedOrgUnit"), transaction.EmployeeImportExternalIdentifier, transaction.OrganizationUnitImportExternalIdentifier) + "\r\n");
                                                    transaction.HasErrors = true;
                                                }
                                            }
                                        }
                                    }

                                    transaction.SetCalculateEmploymentInsuranceHourAndUnits(transaction.Units, 0, 0, transaction.IncludeEmploymentInsuranceHoursFlag, false);
                                    transaction.PayrollBatchId = batch.PayrollBatchId;
                                    transaction.UpdateUser = user.UserName;
                                    transaction.UpdateDatetime = DateTime.Now;

                                    if (!transaction.HasErrors)
                                        save.Add(transaction);
                                }
                            }
                            else
                            {
                                transaction.HasErrors = true;
                            }
                        }

                        PayrollTransactionErrorCollection payrollTransactionErrorCollection = _payrollAccess.InsertPayrollTransactionBatch(user, save);

                        foreach (PayrollTransactionError error in payrollTransactionErrorCollection)
                        {
                            payrollTransactionErrors.Add(new PayrollTransactionError()
                            {
                                PayrollTransactionId = payrollTransactionErrors.Count + 1,
                                EmployeeNumber = error.EmployeeNumber,
                                YearlyMaximum = error.YearlyMaximum,
                                PaycodeCode = error.PaycodeCode,
                                TotalAmount = error.TotalAmount
                            });
                        }
                    }

                    if (payrollTransactionErrors.Count > 0)
                    {
                        String maximumMessage = GetMaximumMessage(payrollTransactionErrors);
                        output.AppendLine(maximumMessage);
                    }

                    //count error/success /**HACK TEMP**/
                    int successfulRows = 0;
                    int failedRows = 0;
                    foreach (PayrollTransactionImport tran in transactions)
                    {
                        if (tran.HasErrors)
                            failedRows++;
                        else
                            successfulRows++;
                    }

                    output.AppendLine("----------");
                    output.AppendLine("# rows total: " + (successfulRows + failedRows));
                    output.AppendLine("# rows succeeded: " + successfulRows);
                    output.AppendLine("# rows failed: " + failedRows);

                    if (failedRows > 0 || payrollTransactionErrors.Count > 0)
                        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n" + "----------\r\n");
                    else
                        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n");

                    log.SuccessFlag = !(automaticImportFlag && transactions.HasErrors);
                    log.WarningFlag = transactions.HasErrors;
                    log.ProcessingOutput += output.ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - ProcessPayrollBatchImport", ex);
                throw;
            }
        }
        private bool OrganizationUnitMatches(String organizationUnit1, String organizationUnit2)
        {
            String[] orgArray1 = organizationUnit1.Substring(1).Split('/');
            String[] orgArray2 = organizationUnit2.Substring(1).Split('/');

            if (orgArray1.Length != orgArray2.Length)
                return false;
            else
            {
                for (int i = 0; i < orgArray1.Length; i++)
                {
                    if (orgArray1[i] == String.Empty) { }
                    else if (orgArray2[i] == String.Empty) { }
                    else if (orgArray1[i] != orgArray2[i]) { return false; }
                }

                return true;
            }
        }
        private void SetRate(DatabaseUser user, PayrollTransactionImport transaction, CodePaycode code, decimal ratePerHour)
        {
            if (code.AutoPopulateRateFlag)
            {
                transaction.BaseSalaryRate = ratePerHour;
                transaction.Rate = EmployeeManagement.CalculateRate(user, transaction.EmployeeId, ratePerHour, transaction.AmountRateFactor, code.VacationCalculationOverrideFlag);
            }
            else
                transaction.Rate = 0;
        }
        private string GetMaximumMessage(PayrollTransactionErrorCollection collection)
        {
            String maximumMessage = "";

            foreach (PayrollTransactionError error in collection)
                maximumMessage += "Global yearly maximum exceeded for " + error.EmployeeNumber + " for paycode " + error.PaycodeCode + ".\r\n";

            return maximumMessage;
        }
        #endregion

        #region payroll transaction
        public PayrollTransactionSummaryCollection GetPayrollTransactionSummary(DatabaseUser user, long payrollBatchId)
        {
            try
            {
                return _payrollAccess.GetPayrollTransactionSummary(user, payrollBatchId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - GetPayrollTransactionSummary", ex);
                throw;
            }
        }
        public void UpdatePayrollTransaction(DatabaseUser user, PayrollTransaction payrollTransaction)
        {
            try
            {
                CodePaycodeCollection paycodes = CodeAccess.GetCodePaycode(user, payrollTransaction.PaycodeCode, false);
                if (paycodes.Count > 0)
                    payrollTransaction.SetCalculateEmploymentInsuranceHourAndUnits(payrollTransaction.Units, 0, 0, paycodes[0].IncludeEmploymentInsuranceHoursFlag, false, payrollTransaction.OverrideEiHours);


                _payrollAccess.UpdatePayrollTransaction(user, payrollTransaction);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - GetPayrollTransactionSummary", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void InsertPayrollTransaction(DatabaseUser user, PayrollTransaction payrollTransaction)
        {
            try
            {
                CodePaycodeCollection paycodes = CodeAccess.GetCodePaycode(user, payrollTransaction.PaycodeCode, false);
                if (paycodes.Count > 0)
                    payrollTransaction.SetCalculateEmploymentInsuranceHourAndUnits(payrollTransaction.Units, 0, 0, paycodes[0].IncludeEmploymentInsuranceHoursFlag, false, payrollTransaction.OverrideEiHours);


                _payrollAccess.InsertPayrollTransaction(user, payrollTransaction);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - InsertPayrollTransaction", ex);
                throw;
            }
        }
        public void DeletePayrollTransaction(DatabaseUser user, PayrollTransaction payrollTransaction)
        {
            try
            {
                _payrollAccess.DeletePayrollTransaction(user, payrollTransaction);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - DeletePayrollTransaction", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }

        public void DeleteFtpImportBatchCascade(DatabaseUser user, string codePayrollProcessRunTypeCode, string codePayrollProcessGroupCd)
        {
            _payrollAccess.DeleteFtpImportBatchCascade(user, codePayrollProcessRunTypeCode, codePayrollProcessGroupCd);
        }

        public void DeletePayrollBatchCascadeTransactions(DatabaseUser user, PayrollBatch payrollBatch)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _payrollAccess.DeletePayrollTransactionByBatchId(user, payrollBatch.PayrollBatchId);
                    _payrollAccess.DeletePayrollBatch(user, payrollBatch);
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - DeletePayrollBatchCascadeTransactions", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        public CitiEftPayrollMasterPaymentCollection GetCitiEftPayrollMasterPayment(DatabaseUser user, long payrollProcessId, String employeeNumber)
        {
            return _payrollAccess.GetCitiEftPayrollMasterPayment(user, payrollProcessId, employeeNumber);
        }

        #region export ftp
        public ExportFtpCollection GetExportFtp(DatabaseUser user, long? exportFtpId, string codeExportEftTypeCd, string ftpName)
        {
            return _payrollAccess.GetExportFtp(user, exportFtpId, codeExportEftTypeCd, ftpName);
        }
        public void UpdateExportFtp(DatabaseUser user, ExportFtp export)
        {
            try
            {
                _payrollAccess.UpdateExportFtp(user, export);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - UpdateExportFtp", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public ExportFtp InsertExportFtp(DatabaseUser user, ExportFtp export)
        {
            return _payrollAccess.InsertExportFtp(user, export);
        }
        public void DeleteExportFtp(DatabaseUser user, ExportFtp export)
        {
            try
            {
                _payrollAccess.DeleteExportFtp(user, export);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - DeleteExportFtp", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region pay register export
        public PayRegisterExportCollection GetPayRegisterExport(DatabaseUser user, long? exportQueueId, string codeExportFtpTypeCd)
        {
            return _payrollAccess.GetPayRegisterExport(user, exportQueueId, codeExportFtpTypeCd);
        }
        public void UpdatePayRegisterExport(DatabaseUser user, PayRegisterExport export)
        {
            try
            {
                _payrollAccess.UpdatePayRegisterExport(user, export);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - UpdatePayRegisterExport", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public PayRegisterExport InsertPayRegisterExport(DatabaseUser user, PayRegisterExport export)
        {
            return _payrollAccess.InsertPayRegisterExport(user, export);
        }
        public void DeletePayRegisterExport(DatabaseUser user, PayRegisterExport export)
        {
            try
            {
                _payrollAccess.DeletePayRegisterExport(user, export);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - DeletePayRegisterExport", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region paycode year end report map
        public PaycodeYearEndReportMapCollection GetPaycodeYearEndReportMap(DatabaseUser user, String paycodeCode)
        {
            return _payrollAccess.GetPaycodeYearEndReportMap(user, paycodeCode);
        }
        public void UpdatePaycodeYearEndReportMap(DatabaseUser user, PaycodeYearEndReportMap paycodeYearEndReportMap)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _payrollAccess.UpdatePaycodeYearEndReportMap(user, paycodeYearEndReportMap);
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - UpdatePaycodeYearEndReportMap", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public PaycodeYearEndReportMap InsertPaycodeYearEndReportMap(DatabaseUser user, PaycodeYearEndReportMap paycodeYearEndReportMap)
        {
            try
            {
                PaycodeYearEndReportMap yearEndReportMap = new PaycodeYearEndReportMap();

                using (TransactionScope scope = new TransactionScope())
                {
                    yearEndReportMap = _payrollAccess.InsertPaycodeYearEndReportMap(user, paycodeYearEndReportMap);
                    scope.Complete();
                }

                return yearEndReportMap;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - InsertPaycodeYearEndReportMap", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 2601)
                    {
                        EmployeeServiceException uniqueIndexFault = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.UniqueIndexFault);
                        throw new FaultException<EmployeeServiceException>(uniqueIndexFault, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeletePaycodeYearEndReportMap(DatabaseUser user, PaycodeYearEndReportMap paycodeYearEndReportMap)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _payrollAccess.DeletePaycodeYearEndReportMap(user, paycodeYearEndReportMap);
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - DeletePaycodeYearEndReportMap", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region paycode payroll transaction offset association
        public PaycodePayrollTransactionOffsetAssociationCollection GetPaycodePayrollTransactionOffsetAssociation(DatabaseUser user, String paycodeCode)
        {
            return _payrollAccess.GetPaycodePayrollTransactionOffsetAssociation(user, paycodeCode);
        }
        public void UpdatePaycodePayrollTransactionOffsetAssociation(DatabaseUser user, PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _payrollAccess.UpdatePaycodePayrollTransactionOffsetAssociation(user, paycodePayrollTransactionOffsetAssociation);
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - UpdatePaycodePayrollTransactionOffsetAssociation", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public PaycodePayrollTransactionOffsetAssociation InsertPaycodePayrollTransactionOffsetAssociation(DatabaseUser user, PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation)
        {
            try
            {
                PaycodePayrollTransactionOffsetAssociation payrollTransactionOffsetAssociation = new PaycodePayrollTransactionOffsetAssociation();

                using (TransactionScope scope = new TransactionScope())
                {
                    payrollTransactionOffsetAssociation = _payrollAccess.InsertPaycodePayrollTransactionOffsetAssociation(user, paycodePayrollTransactionOffsetAssociation);
                    scope.Complete();
                }

                return payrollTransactionOffsetAssociation;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CodeManagement - InsertPaycodePayrollTransactionOffsetAssociation", ex);
                throw;
            }
        }
        public void DeletePaycodePayrollTransactionOffsetAssociation(DatabaseUser user, PaycodePayrollTransactionOffsetAssociation paycodePayrollTransactionOffsetAssociation)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _payrollAccess.DeletePaycodePayrollTransactionOffsetAssociation(user, paycodePayrollTransactionOffsetAssociation);
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - DeletePaycodePayrollTransactionOffsetAssociation", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        public TempBulkPayrollProcessCollection GetTempBulkPayrollProcess(DatabaseUser user)
        {
            return _payrollAccess.GetTempBulkPayrollProcess(user);
        }

        #region MET wrapped classes
        public class METEmployeeData
        {
            public long PayrollMasterId = -1;
            public long EmployeeId = -1;
            public long PayrollProcessId = -1;
            public int BonusLumpSumTaxableIncome = 0; //used when a bonus only run has occured, we map the wages here for MET to estimate yearly wages for taxes on bonus

            /*  We get YtdPrevious values from the db.  If we go thru a bonus and something is deducted, we have to add it to the ytd if we go into the regular calc section after the bonus.
                These fields keep track of what came from the db originally so we can store it in the payroll_master table (original inputs)
            */
            //used in fed calc
            public int StartingYtdCppQppDeducted = 0;
            public int StartingYtdEiDeducted = 0;
            //used in quebec calc
            public int StartingYtdPpipDeducted = 0;
            public int StartingYtdPpipEmployerDeducted = 0;
            public int StartingQuebecHealthDeducted = 0;

            public EmployeeData canada;
            public EmployeeDataQC quebec;

            public METEmployeeData()
            {
                canada = new EmployeeData(); //we will always instantiate a canada object.
            }
        }
        #endregion

        #region remittance
        public RemittanceCollection GetRemittance(DatabaseUser user)
        {
            return _payrollAccess.GetRemittance(user);
        }
        #endregion

        #region remittance detail
        public RemittanceDetailCraSourceDeductionsCollection GetRemittanceDetailCraSourceDeductions(DatabaseUser user, long? craExportId, string remitCode, DateTime remitDate)
        {
            return _payrollAccess.GetRemittanceDetailCraSourceDeductions(user, craExportId, remitCode, remitDate);
        }
        public RemittanceDetailWcbDeductionsCollection GetRemittanceDetailWcbDeductions(DatabaseUser user, string detailType, long? wcbExportId, string remitCode, DateTime remitDate)
        {
            return _payrollAccess.GetRemittanceDetailWcbDeductions(user, detailType, wcbExportId, remitCode, remitDate);
        }
        public RemittanceDetailChequeHealthTaxDeductionsCollection GetRemittanceDetailChequeHealthTaxDeductions(DatabaseUser user, long? chequeHealthTaxExportId, string codeHealthTaxCode)
        {
            return _payrollAccess.GetRemittanceDetailChequeHealthTaxDeductions(user, chequeHealthTaxExportId, codeHealthTaxCode);
        }
        public RemittanceDetailGarnishmentDeductionsCollection GetRemittanceDetailGarnishmentDeductions(DatabaseUser user, string detailType, long? exportId)
        {
            return _payrollAccess.GetRemittanceDetailGarnishmentDeductions(user, detailType, exportId);
        }
        public RemittanceDetailRqSourceDeductionsCollection GetRemittanceDetailRqSourceDeductions(DatabaseUser user, long? rqExportId)
        {
            return _payrollAccess.GetRemittanceDetailRqSourceDeductions(user, rqExportId);
        }
        #endregion

        #region remittance import
        public RemittanceImportCollection GetRemittanceImport(DatabaseUser user, long? remittanceImportId)
        {
            return _payrollAccess.GetRemittanceImport(user, remittanceImportId);
        }
        //public RemittanceImportDetailCollection GetRemittanceImportDetail(DatabaseUser user, long remittanceImportId)
        //{
        //    return _payrollAccess.GetRemittanceImportDetail(user, remittanceImportId);
        //}
        //public void ProcessRemittanceImport(DatabaseUser user, long remittanceImportId, String remittanceImportStatusCode)
        //{
        //    _payrollAccess.ProcessRemittanceImport(user, remittanceImportId, remittanceImportStatusCode);
        //}
        public void ProcessRemittance(DatabaseUser user, RemittanceImport xsdObject, ImportExportLog log, bool importDirtySave)
        {
            try
            {
                StringBuilder output = new StringBuilder();
                int failedRows = 0;
                int successfulRows = 0;
                String validationMessage = null;

                using (TransactionScope scope = new TransactionScope())
                {
                    RemittanceImportDetailCollection remittanceImportDetails = new RemittanceImportDetailCollection();

                    foreach (RemittanceImportDetail remittance in xsdObject.RemittanceImportDetail)
                    {
                        if (remittance.IsValid)
                        {
                            if (remittance.ChequeDataInValidMsg == null)
                            {
                                //check if employee number already exists in collection
                                if (remittanceImportDetails.Any(e => e.EmployeeImportExternalIdentifier == remittance.EmployeeImportExternalIdentifier))
                                {
                                    failedRows++;
                                    output.Append("Duplicate Employee found: " + remittance.EmployeeImportExternalIdentifier + ".  Please fix the file and re-import.  <br/>");
                                }
                                else
                                {
                                    successfulRows++;
                                    remittanceImportDetails.Add(remittance);
                                }
                            }
                            else
                            {
                                failedRows++;
                                output.Append(remittance.ChequeDataInValidMsg + "<br/>");
                            }
                        }
                        else
                        {
                            failedRows++;
                            output.Append(remittance.GetValidationInformation().Replace(":", ":  ") + "<br/>");
                        }
                    }

                    xsdObject.RemittanceImportStatusCode = "NEW";
                    xsdObject.RemittanceImportDetail = remittanceImportDetails;

                    _payrollAccess.InsertRemittanceImportBatch(user, xsdObject);
                    if (output.Length != 0)
                    {
                        output.Insert(0, "FAILURES <br/>");
                    }
                    String warnings = _payrollAccess.ProcessRemittanceImport(user, (long)xsdObject.PayrollProcessId);
                    if (warnings.Length > 0)
                    {
                        output.Append("<br/> WARNINGS <br/>");
                        output.Append(warnings.Replace(":", ":  "));
                    }

                    // if (importDirtySave || failedRows == 0)
                    scope.Complete();
                }

                if (output.Length == 0)
                    log.SuccessFlag = true;
                else
                    log.SuccessFlag = false;

                output.Insert(0, "# Failed Rows: " + Convert.ToString(failedRows) + "<br/><br/>");
                output.Insert(0, "# Successful Rows: " + Convert.ToString(successfulRows) + "<br/>");
                output.Insert(0, "# Total Rows: " + Convert.ToString(successfulRows + failedRows) + "<br/>");

                //if (failedRows > 0)
                //{
                //    if (importDirtySave)
                //        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n" + "----------\r\n");
                //    else
                //        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportCompleteProcessFailed") + "\r\n" + "----------\r\n");
                //}
                //else
                //    output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n");
                validationMessage = output.ToString();

                log.ProcessingOutput += validationMessage;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - ProcessRemittance", ex);
                throw;
            }
        }
        #endregion

        #region health tax
        public HealthTaxCollection GetHealthTax(DatabaseUser user, long? healthTaxId)
        {
            try
            {
                return _payrollAccess.GetHealthTax(user, healthTaxId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - GetHealthTax", ex);
                throw;
            }
        }
        public HealthTax InsertHealthTax(DatabaseUser user, HealthTax healthTax)
        {
            try
            {
                return _payrollAccess.InsertHealthTax(user, healthTax);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - InsertHealthTax", ex);
                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 2601)
                    {
                        EmployeeServiceException uniqueIndexFault = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.UniqueIndexFault);
                        throw new FaultException<EmployeeServiceException>(uniqueIndexFault, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void UpdateHealthTax(DatabaseUser user, HealthTax healthTax)
        {
            try
            {
                _payrollAccess.UpdateHealthTax(user, healthTax);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - UpdateHealthTax", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 2601)
                    {
                        EmployeeServiceException uniqueIndexFault = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.UniqueIndexFault);
                        throw new FaultException<EmployeeServiceException>(uniqueIndexFault, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteHealthTax(DatabaseUser user, HealthTax healthTax)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _payrollAccess.DeleteHealthTaxDetail(user, new HealthTaxDetail() { HealthTaxId = healthTax.HealthTaxId });
                    _payrollAccess.DeleteHealthTax(user, healthTax);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - DeleteHealthTax", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region health tax detail
        public HealthTaxDetailCollection GetHealthTaxDetail(DatabaseUser user, long healthTaxId)
        {
            try
            {
                return _payrollAccess.GetHealthTaxDetail(user, healthTaxId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - GetHealthTaxDetail", ex);
                throw;
            }
        }
        public HealthTaxDetail InsertHealthTaxDetail(DatabaseUser user, HealthTaxDetail healthTaxDetail)
        {
            try
            {
                return _payrollAccess.InsertHealthTaxDetail(user, healthTaxDetail);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - InsertHealthTaxDetail", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 2601)
                    {
                        EmployeeServiceException uniqueIndexFault = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.UniqueIndexFault);
                        throw new FaultException<EmployeeServiceException>(uniqueIndexFault, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void UpdateHealthTaxDetail(DatabaseUser user, HealthTaxDetail healthTaxDetail)
        {
            try
            {
                _payrollAccess.UpdateHealthTaxDetail(user, healthTaxDetail);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - UpdateHealthTaxDetail", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 2601)
                    {
                        EmployeeServiceException uniqueIndexFault = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.UniqueIndexFault);
                        throw new FaultException<EmployeeServiceException>(uniqueIndexFault, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public void DeleteHealthTaxDetail(DatabaseUser user, HealthTaxDetail healthTaxDetail)
        {
            try
            {
                _payrollAccess.DeleteHealthTaxDetail(user, healthTaxDetail);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - DeleteHealthTaxDetail", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50020)
                    {
                        EmployeeServiceException foreignKeyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.ForeignKeyConstraint);
                        throw new FaultException<EmployeeServiceException>(foreignKeyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        #region payroll process
        public PayrollEmployeePaycodeCollection GetPayrollEmployeePaycode(DatabaseUser user, long payrollProcessId)
        {
            return _payrollAccess.SelectPayrollEmployeePaycode(user, payrollProcessId);
        }
        public PayrollTransactionIncomeCollection SelectPayrollTransactionIncome(DatabaseUser user, long payrollProcessId, String payrollProcessGroupCode, String payrollProcessRunTypeCode)
        {
            return _payrollAccess.SelectPayrollTransactionIncome(user, payrollProcessId, payrollProcessGroupCode, payrollProcessRunTypeCode);
        }
        public PayrollEmployeeYearToDatePaycodeCollection SelectPayrollEmployeeYearToDatePaycode(DatabaseUser user, long payrollProcessId, int periodYear)
        {
            return _payrollAccess.SelectPayrollEmployeeYearToDatePaycode(user, payrollProcessId, periodYear);
        }
        #endregion

        #region Advantage Time Record

        public void ProcessAdvantageTimeRecord(DatabaseUser user, AdvantageTimeRecord xsdObject, ImportExportLog log, bool importDirtySave)
        {
            try
            {
                StringBuilder output = new StringBuilder();
                int failedRows = 0;
                int successfulRows = 0;
                //String validationMessage = null;
                Dictionary<String, CodePaycode> paycodeCache = new Dictionary<String, CodePaycode>();
                PayrollTransactionImportCollection transactions = null;
                Dictionary<String, EmployeeSummary> employeeCache = new Dictionary<String, EmployeeSummary>();
                Dictionary<String, String> orgUnitCache = new Dictionary<String, String>();

                using (TransactionScope scope = new TransactionScope())
                {
                    AdvantageTimeRecordDetailCollection advantageTimeRecordDetails = new AdvantageTimeRecordDetailCollection();

                    foreach (AdvantageTimeRecordDetail advantageTimeRecordDetail in xsdObject.AdvantageTimeRecordDetails)
                    {
                        if (advantageTimeRecordDetail.IsValid)
                        {
                            successfulRows++;
                            advantageTimeRecordDetails.Add(advantageTimeRecordDetail);
                        }
                        else
                        {
                            failedRows++;
                            output.Append(advantageTimeRecordDetail.GetValidationInformation().Replace(":", ":  ") + "<br/>");
                        }
                    }

                    xsdObject.AdvantageTimeRecordDetails = advantageTimeRecordDetails;

                    // Insert 1 Header and 1 or more Detail rows
                    _payrollAccess.ImportAdvantageTimeRecord(user, xsdObject);

                    // get all paycodes to work with
                    paycodeCache = RetrieveAdvantageTimeRecordPaycodes(user);

                    // create all payroll transactions to be inserted
                    transactions = new PayrollTransactionImportCollection(xsdObject, paycodeCache);

                    // set up those payroll transactions
                    foreach (PayrollTransactionImport transaction in transactions)
                    {
                        output.Append(EmployeeManagement.SetEmployeeIdFromIdentifier(user, transaction, employeeCache));
                        output.Append(EmployeeManagement.SetPaycodeCodeFromIdentifier(user, transaction, paycodeCache));
                        output.Append(EmployeeManagement.SetOrganizationUnitFromIdentifier(user, transaction, orgUnitCache));
                    }

                    PayrollTransactionErrorCollection payrollTransactionErrors = new PayrollTransactionErrorCollection();

                    // Loop through all unique Payroll Process Group codes
                    foreach (String payrollProcessGroupCode in transactions.GetAllPayrollProcessGroupCodes())
                    {
                        // Generate 1 Payroll Batch row to insert
                        PayrollBatchImport batch = new PayrollBatchImport(xsdObject, payrollProcessGroupCode);
                        batch.PayrollProcessRunTypeCode = batch.PayrollBatchEntryTypeCodeExternalIdentifier;
                        batch.PayrollBatchTypeCode = null;
                        batch.PayrollBatchTypeCode = "LOCKED"; // Note that the batch type is "LOCKED" for Advantage time records

                        // Insert 1 Payroll Batch row
                        InsertPayrollBatch(user, batch);

                        // these Payroll Transactions will be inserted
                        PayrollTransactionCollection save = new PayrollTransactionCollection();

                        //these are the transactions the foreach will go thru
                        PayrollTransactionSummaryCollection trans = transactions.GetValidPayrollTransactionSummaryCollection(payrollProcessGroupCode);

                        //get cutoff date
                        PayrollBatchCriteria payrollBatchCriteria = new PayrollBatchCriteria()
                        {
                            PayrollProcessGroupCode = batch.PayrollProcessGroupCode,
                            PayrollProcessRunTypeCode = batch.PayrollProcessRunTypeCode
                        };

                        PayrollPeriod period = GetPayrollPeriodIdFromPayrollProcessGroupCode(user, payrollBatchCriteria)[0];
                        DateTime cutoffDate = period.CutoffDate;

                        trans.DefaultUnsetTransactionDates(cutoffDate);

                        //get collection of employee positions based on employeeid and transactiondate
                        Dictionary<string, EmployeeRate> positions = GetPositionsByEmployeeIdAndTransactionDate(user, trans);

                        foreach (PayrollTransactionImport transaction in trans)
                        {
                            if (transaction.EmployeeId > 0 && transaction.PaycodeCode != null)
                            {
                                if (transaction.Rate == null)
                                {
                                    EmployeeRate position = null;

                                    if (positions.ContainsKey(transaction.EmployeeId.ToString() + transaction.TransactionDate.ToString("yyyy-MM-dd")))
                                        position = positions[transaction.EmployeeId.ToString() + transaction.TransactionDate.ToString("yyyy-MM-dd")];

                                    CodePaycode code = paycodeCache[transaction.PaycodeCodeImportExternalIdentifier];//CodeAccess.GetCodePaycode(user, transaction.PaycodeCode, false)[0];

                                    if (transaction.OrganizationUnit == null)
                                    {
                                        transaction.OrganizationUnit = position.OrganizationUnit;
                                        if (transaction.Rate == null)
                                            SetRate(user, transaction, code, position.RatePerHour);
                                    }
                                    else
                                    {
                                        //check if the org unit in the transaction is the same as the position
                                        if (position.SecondaryPositions.Count == 0)
                                        {
                                            if (transaction.Rate == null)
                                                SetRate(user, transaction, code, position.RatePerHour);
                                        }
                                        else
                                        {
                                            IEmployeePosition matchedPosition = position.GetMatchedEmployeePosition(transaction.OrganizationUnit);

                                            if (matchedPosition != null)
                                            {
                                                if (transaction.Rate == null)
                                                    SetRate(user, transaction, code, matchedPosition.RatePerHour);
                                            }
                                            else
                                            {
                                                output.Append(String.Format(GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "EmployeeUnassignedOrgUnit"), transaction.EmployeeImportExternalIdentifier, transaction.OrganizationUnitImportExternalIdentifier) + "\r\n");
                                                transaction.HasErrors = true;
                                            }
                                        }
                                    }
                                }

                                transaction.SetCalculateEmploymentInsuranceHourAndUnits(transaction.Units, 0, 0, transaction.IncludeEmploymentInsuranceHoursFlag, false);
                                transaction.PayrollBatchId = batch.PayrollBatchId;
                                transaction.OriginalImportFlag = true;
                                transaction.UpdateUser = user.UserName;
                                transaction.UpdateDatetime = DateTime.Now;

                                if (!transaction.HasErrors)
                                    save.Add(transaction);
                            }
                            else
                                transaction.HasErrors = true;
                        } // foreach (PayrollTransactionImport transaction in trans)

                        // Insert Payroll Transactions 
                        PayrollTransactionErrorCollection payrollTransactionErrorCollection = _payrollAccess.InsertPayrollTransactionBatch(user, save);

                        foreach (PayrollTransactionError error in payrollTransactionErrorCollection)
                        {
                            payrollTransactionErrors.Add(new PayrollTransactionError()
                            {
                                PayrollTransactionId = payrollTransactionErrors.Count + 1,
                                EmployeeNumber = error.EmployeeNumber,
                                YearlyMaximum = error.YearlyMaximum,
                                PaycodeCode = error.PaycodeCode,
                                TotalAmount = error.TotalAmount
                            });
                        }

                    } // foreach (String payrollProcessGroupCode in transactions.GetAllPayrollProcessGroupCodes())

                    if (payrollTransactionErrors.Count > 0)
                    {
                        String maximumMessage = GetMaximumMessage(payrollTransactionErrors);
                        output.AppendLine(maximumMessage);
                    }

                    if (importDirtySave || failedRows == 0)
                        scope.Complete();
                }

                output.AppendLine("----------");
                output.AppendLine("# rows total: " + (successfulRows + failedRows));
                output.AppendLine("# rows succeeded: " + successfulRows);
                output.AppendLine("# rows failed: " + failedRows);

                if (failedRows > 0)
                {
                    if (importDirtySave)
                        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n" + "----------\r\n");
                    else
                        output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportCompleteProcessFailed") + "\r\n" + "----------\r\n");
                }
                else
                    output.Insert(0, GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n");

                log.SuccessFlag = true;
                log.ProcessingOutput += output.ToString();
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - ProcessAdvantageTimeRecord", ex);
                throw;
            }
        }
        private Dictionary<String, CodePaycode> RetrieveAdvantageTimeRecordPaycodes(DatabaseUser user)
        {
            try
            {
                Dictionary<String, CodePaycode> paycodeCache = new Dictionary<String, CodePaycode>();

                for (int i = 0; i <= 50; i++)
                {
                    string paycodeExternalName = "Timecode_" + i.ToString("00");
                    CodePaycodeCollection paycodes = CodeAccess.GetCodePaycode(user, paycodeExternalName, true);

                    if (paycodes.Count == 1)
                    {
                        paycodeCache.Add(paycodeExternalName, paycodes[0]);
                    }
                }

                return paycodeCache;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "PayrollManagement - RetrieveAdvantageTimeRecordPaycodes", ex);
                throw;
            }
        }
        #endregion
    }
}