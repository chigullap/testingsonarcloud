﻿using System;
using System.Collections.Generic;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Export;
using WorkLinks.DataLayer.DataAccess;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    /// <summary>
    /// deals with worklinks microsoft dynamics management (WorkLinks database)
    /// </summary>
    public class DynamicsManagement
    {
        #region fields
        DynamicsAccess _dynamicsAccess = null;
        AddressAccess _addressAccess = null;
        PayrollAccess _payrollAccess = null;
        EmployeeManagement _employeeManagement = null;
        ReportManagement _reportManagement = null;
        #endregion

        #region properties
        public DynamicsAccess DynamicsAccess
        {
            get
            {
                if (_dynamicsAccess == null)
                    _dynamicsAccess = new DynamicsAccess();

                return _dynamicsAccess;
            }
        }
        public AddressAccess AddressAccess
        {
            get
            {
                if (_addressAccess == null)
                    _addressAccess = new AddressAccess();

                return _addressAccess;
            }
        }
        public PayrollAccess PayrollAccess
        {
            get
            {
                if (_payrollAccess == null)
                    _payrollAccess = new PayrollAccess();

                return _payrollAccess;
            }
        }
        public EmployeeManagement EmployeeManagement
        {
            get
            {
                if (_employeeManagement == null)
                    _employeeManagement = new EmployeeManagement();

                return _employeeManagement;
            }
        }
        public ReportManagement ReportManagement
        {
            get
            {
                if (_reportManagement == null)
                    _reportManagement = new ReportManagement();

                return _reportManagement;
            }
        }
        #endregion

        #region local Dynamics
        public byte[] ExportCeridianPayProcess(DatabaseUser user, long payrollProcessId, long onEFTAmount, String companyNumber, String companyName, CeridianBilling.ProcessType ceridianBillingProcessType, DateTime wcbStartDate)
        {
            try
            {
                Dictionary<String, byte[]> exportFiles = new Dictionary<string, byte[]>();

                CeridianBilling billing = GetCeridianBilling(user, companyNumber, payrollProcessId, onEFTAmount, ceridianBillingProcessType, wcbStartDate);
                CeridianPayCollection cheques = GetCeridianCheque(user, payrollProcessId, companyNumber, companyName);
                CeridianPayCollection statements = GetCeridianStatement(user, payrollProcessId, companyNumber, companyName);

                if (billing != null)
                    exportFiles.Add(String.Format("{0}.bil", companyNumber), convertStringToByteArray(billing.ToString()));

                if (cheques != null & cheques.Count > 0)
                    exportFiles.Add(String.Format("{0}.psc", companyNumber), convertStringToByteArray(cheques.ToString()));

                if (statements != null & statements.Count > 0)
                    exportFiles.Add(String.Format("{0}.sss", companyNumber), convertStringToByteArray(statements.ToString()));

                if (exportFiles.Count > 0)
                    return ReportManagement.CreateZip(user, exportFiles);
                else
                    return null;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "DynamicsManagement - ExportCeridianPayProcess", ex);
                throw;
            }
        }

        private byte[] convertStringToByteArray(String input)
        {
            //****there must be a better way to do this****
            byte[] bytes = new byte[input.Length];
            long i = 0;
            foreach (char val in input.ToCharArray())
            {
                bytes[i++] = (byte)val;
            }

            return bytes;
        }
        public CeridianBilling GetCeridianBilling(DatabaseUser user, String companyNumber, long payrollProcessId, long onEFTAmount, CeridianBilling.ProcessType ceridianBillingProcessType, DateTime wcbStartDate)
        {
            try
            {
                //get businessTaxNumber from business number table
                String businessTaxNumber = EmployeeManagement.GetBusinessNumber(user, null)[0].BusinessTaxNumber;

                //get payroll period
                PayrollProcessSummary payroll = PayrollAccess.GetPayrollProcessSummary(user, new PayrollProcessCriteria() { PayrollProcessId = payrollProcessId })[0];
                short periodRunNumber = Convert.ToInt16(payroll.Period);

                CeridianBilling billing = new CeridianBilling(companyNumber, ceridianBillingProcessType, periodRunNumber, wcbStartDate, businessTaxNumber, onEFTAmount);

                billing.Garnishments = PayrollAccess.GetPayrollMasterPaycode(user, payrollProcessId, true);
                billing.Add(PayrollAccess.GetPayrollMaster(user, payrollProcessId));
                billing.ProvinceRemittances = PayrollAccess.GetHealthTax(user, null, true);
                billing.WSIBSummarys = DynamicsAccess.GetWSIBSummary(user, payroll.CutoffDate);
                billing.EFTCollection = new DataLayer.DataAccess.Dynamics.DynamicsAccess().GetEftData(user, payrollProcessId, null);

                return billing;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "DynamicsManagement - GetCeridianBilling", ex);
                throw;
            }
        }
        public CeridianPayCollection GetCeridianCheque(DatabaseUser user, long payrollProcessId, String companyNumber, String companyName)
        {
            try
            {
                CeridianPayCollection cheques = new CeridianPayCollection();
                //***HACK***  need to break this down by company number later  START//
                BusinessNumberCollection businessNumbers = EmployeeManagement.GetBusinessNumber(user, null);
                Address companyAddress = AddressAccess.GetAddress(user, businessNumbers[0].EmployerAddressId)[0];
                //***HACK***  END//

                PayrollProcessSummary payrollProcess = PayrollAccess.GetPayrollProcessSummary(user, new PayrollProcessCriteria() { PayrollProcessId = payrollProcessId })[0];
                int pageNumber = 1;

                foreach (PayrollMasterPayment cheque in new DataLayer.DataAccess.Dynamics.DynamicsAccess().GetPayrollMasterPaymentCheque(user, payrollProcessId))
                {
                    if (cheque.Amount != 0)
                        cheques.Add(BuildCeridianCheque(user, cheque, companyNumber, companyName, companyAddress, ++pageNumber, payrollProcess));
                }

                foreach (PayrollMasterPaycode paycode in PayrollAccess.GetPayrollMasterPaycode(user, payrollProcessId, true))
                {
                    //build garnishment
                    Garnishment garnishment = new Garnishment();
                    paycode.CopyTo(garnishment);

                    garnishment.EmployeePaycode = EmployeeManagement.GetEmployeePaycodeProcessed(user, payrollProcessId, garnishment.EmployeeId)[garnishment.CodePaycodeCd];

                    if (garnishment.EmployeePaycode != null)
                    {
                        garnishment.Vendor = DynamicsAccess.GetVendor(user, (long)garnishment.EmployeePaycode.GarnishmentVendorId)[0];
                        garnishment.Address = AddressAccess.GetAddress(user, garnishment.Vendor.AddressId)[0];

                        //build cheque
                        PayrollMasterPayment cheque = new PayrollMasterPayment(garnishment, (DateTime)payrollProcess.ChequeDate);
                        cheques.Add(BuildVendorCheque(user, cheque, companyNumber, companyName, companyAddress, ++pageNumber, payrollProcess, garnishment.Vendor, garnishment.Address, garnishment.EmployeePaycode));
                    }
                }

                return cheques;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "DynamicsManagement - GetCeridianCheque", ex);
                throw;
            }
        }
        public CeridianPayCollection GetCeridianStatement(DatabaseUser user, long payrollProcessId, String companyNumber, String companyName)
        {
            try
            {
                CeridianPayCollection cheques = new CeridianPayCollection();
                Dictionary<String, PayrollMasterPaymentCollection> employeeEfts = new Dictionary<String, PayrollMasterPaymentCollection>(); //build statement list by employee

                foreach (PayrollMasterPayment eft in new DataLayer.DataAccess.Dynamics.DynamicsAccess().GetEftData(user, payrollProcessId, null))
                {
                    if (!employeeEfts.ContainsKey(eft.EmployeeNumber))
                        employeeEfts.Add(eft.EmployeeNumber, new PayrollMasterPaymentCollection());

                    employeeEfts[eft.EmployeeNumber].Add(eft);
                }

                //need to break this down by company number later
                BusinessNumberCollection businessNumbers = EmployeeManagement.GetBusinessNumber(user, null);
                Address companyAddress = AddressAccess.GetAddress(user, businessNumbers[0].EmployerAddressId)[0];

                PayrollProcessSummary payrollProcess = PayrollAccess.GetPayrollProcessSummary(user, new PayrollProcessCriteria() { PayrollProcessId = payrollProcessId })[0];
                int pageNumber = 1;

                foreach (KeyValuePair<String, PayrollMasterPaymentCollection> item in employeeEfts)
                {
                    pageNumber++;
                    cheques.Add(BuildCeridianStatment(user, item.Value, companyNumber, companyName, companyAddress, pageNumber, payrollProcess));
                }

                return cheques;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "DynamicsManagement - GetCeridianStatement", ex);
                throw;
            }
        }
        private CeridianPay BuildCeridianStatment(DatabaseUser user, PayrollMasterPaymentCollection item, String companyNumber, String companyName, Address companyAddress, int pageNumber, PayrollProcess payrollProcess)
        {
            CeridianStatement ceridianCheque = new CeridianStatement();

            ceridianCheque.PayrollMasterPaymentId = item[0].PayrollMasterPaymentId;
            ceridianCheque.EmployeeNumber = item[0].EmployeeNumber;
            ceridianCheque.PayrollProcessId = payrollProcess.PayrollProcessId;
            ceridianCheque.ChequeNumber = item[0].ChequeNumber;
            ceridianCheque.Amount = 0;

            if (item.Count > 1)
            {
                short maxSequenceNumber = 0;

                foreach (PayrollMasterPayment deposit in item)
                {
                    if (Convert.ToInt16(deposit.CodeEmployeeBankingSequenceCd) > maxSequenceNumber)
                        maxSequenceNumber = Convert.ToInt16(deposit.CodeEmployeeBankingSequenceCd);
                }

                foreach (PayrollMasterPayment deposit in item)
                {
                    if (Convert.ToInt16(deposit.CodeEmployeeBankingSequenceCd) == maxSequenceNumber)
                        ceridianCheque.Amount = deposit.Amount;
                }
            }
            else
                ceridianCheque.Amount = item[0].Amount;

            ceridianCheque.ChequeDate = item[0].ChequeDate;
            ceridianCheque.CompanyNumber = companyNumber;
            ceridianCheque.CompanyName = companyName;
            ceridianCheque.CompanyAddress = companyAddress;
            ceridianCheque.PeriodNumber = Convert.ToInt16(payrollProcess.Period);
            ceridianCheque.PeriodDate = payrollProcess.CutoffDate;
            ceridianCheque.PageNumber = pageNumber;
            ceridianCheque.EFTS = item;

            //find employee
            EmployeeSummary employeeSummary = EmployeeManagement.GetEmployeeSummary(user, new EmployeeCriteria() { EmployeeNumber = ceridianCheque.EmployeeNumber, SecurityOverrideFlag = true })[0];
            EmployeeCriteria employeeCriteria = new EmployeeCriteria()
            {
                SecurityOverrideFlag = true,
                EmployeeId = employeeSummary.EmployeeId
            };

            ceridianCheque.Employee = EmployeeManagement.GetEmployeeSummary(user, employeeCriteria)[0];

            //ceridianCheque.Earnings = DynamicsAccess.GetDynamicsMasterPayDetail(payrollProcess.PayrollProcessId, ceridianCheque.EmployeeNumber, CodePaycode.PaycodeType.Income);
            //ceridianCheque.Benefits = DynamicsAccess.GetDynamicsMasterPayDetail(payrollProcess.PayrollProcessId, ceridianCheque.EmployeeNumber, CodePaycode.PaycodeType.Benefit);
            //ceridianCheque.Deductions = DynamicsAccess.GetDynamicsMasterPayDetail(payrollProcess.PayrollProcessId, ceridianCheque.EmployeeNumber, CodePaycode.PaycodeType.Deduction);

            return ceridianCheque;
        }
        private CeridianPay BuildCeridianCheque(DatabaseUser user, CeridianPay ceridianCheque, PayrollMasterPayment cheque, String companyNumber, String companyName, Address companyAddress, int pageNumber, PayrollProcess payrollProcess)
        {
            cheque.CopyTo(ceridianCheque);

            ceridianCheque.CompanyNumber = companyNumber;
            ceridianCheque.CompanyName = companyName;
            ceridianCheque.CompanyAddress = companyAddress;
            ceridianCheque.PeriodNumber = Convert.ToInt16(payrollProcess.Period);
            ceridianCheque.PeriodDate = payrollProcess.CutoffDate;
            ceridianCheque.PageNumber = pageNumber;

            //find employee
            EmployeeSummary employeeSummary = EmployeeManagement.GetEmployeeSummary(user, new EmployeeCriteria() { EmployeeNumber = ceridianCheque.EmployeeNumber, SecurityOverrideFlag = true })[0];
            EmployeeCriteria employeeCriteria = new EmployeeCriteria()
            {
                SecurityOverrideFlag = true,
                EmployeeId = employeeSummary.EmployeeId
            };

            ceridianCheque.Employee = EmployeeManagement.GetEmployeeSummary(user, employeeCriteria)[0];

            //ceridianCheque.Earnings = DynamicsAccess.GetDynamicsMasterPayDetail(payrollProcess.PayrollProcessId, ceridianCheque.EmployeeNumber, CodePaycode.PaycodeType.Income);
            //ceridianCheque.Benefits = DynamicsAccess.GetDynamicsMasterPayDetail(payrollProcess.PayrollProcessId, ceridianCheque.EmployeeNumber, CodePaycode.PaycodeType.Benefit);
            //ceridianCheque.Deductions = DynamicsAccess.GetDynamicsMasterPayDetail(payrollProcess.PayrollProcessId, ceridianCheque.EmployeeNumber, CodePaycode.PaycodeType.Deduction);

            return ceridianCheque;
        }
        private CeridianPay BuildCeridianCheque(DatabaseUser user, PayrollMasterPayment cheque, String companyNumber, String companyName, Address companyAddress, int pageNumber, PayrollProcess payrollProcess)
        {
            CeridianPay ceridianCheque = new CeridianCheque();

            BuildCeridianCheque(user, ceridianCheque, cheque, companyNumber, companyName, companyAddress, pageNumber, payrollProcess);

            return ceridianCheque;
        }
        private CeridianPay BuildVendorCheque(DatabaseUser user, PayrollMasterPayment cheque, String companyNumber, String companyName, Address companyAddress, int pageNumber, PayrollProcess payrollProcess, Vendor vendor, Address address, EmployeePaycode paycode)
        {
            VendorCheque ceridianCheque = new VendorCheque();

            BuildCeridianCheque(user, ceridianCheque, cheque, companyNumber, companyName, companyAddress, pageNumber, payrollProcess);

            ceridianCheque.VendorName = vendor.Name;
            ceridianCheque.AddressLine1 = address.AddressLine1;
            ceridianCheque.ProvinceStateCode = address.ProvinceStateCode;
            ceridianCheque.PostalZipCode = address.PostalZipCode;
            ceridianCheque.City = address.City;
            ceridianCheque.OrderNumber = paycode.GarnishmentOrderNumber;

            return ceridianCheque;
        }
        #endregion
    }
}