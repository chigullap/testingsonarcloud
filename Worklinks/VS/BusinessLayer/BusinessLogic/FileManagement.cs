﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class FileManagement
    {
        public static byte[] GetByteArray(FileStream stream)
        {
            byte[] rtn =new byte[stream.Length];
            stream.Read(rtn, 0, (int)stream.Length);
            return rtn;
        }

        public static List<FileStream> LockFiles(String[] fileNames)
        {
            List<FileStream> files = new List<FileStream>();
            foreach (String fileName in fileNames)
            {
                files.Add(LockFile(fileName));
            }

            return files;
        }

        public static FileStream LockFile(String fileName)
        {
           return new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.None);
        }


        public static void UnlockFilesAndMove(List<FileStream> files, string xmlProcessedPath)
        {
            //find the corresponding filename
            foreach (FileStream file in files)
            {
                UnlockFileAndMove(file, xmlProcessedPath);
            }
        }

        public static void UnlockFileAndMove(FileStream file, String xmlProcessedPath)
        {
            file.Close();
            //move the file to the "processed" folder
            File.Move(file.Name, xmlProcessedPath + Path.DirectorySeparatorChar + Path.GetFileName(file.Name));
        }

        public static void UnlockFiles(List<FileStream> files)
        {
            //find the corresponding filename
            foreach (FileStream file in files)
            {
                UnlockFile(file);
            }
        }

        public static void UnlockFile(FileStream file)
        {
            file.Close();
        }
    }
}
