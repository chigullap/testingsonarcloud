﻿using NPOI.HSSF.UserModel;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using Saxon.Api;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Xml.Serialization;
using NLog;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Import;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;
using WorkLinks.DataLayer.DataAccess;
using WorkLinks.DataLayer.DataAccess.SqlServer;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class XmlManagement
    {
        #region fields
        private XmlAccess _xmlAccess = null;
        private EmployeeAccess _employeeAccess = null;
        private PersonAccess _personAccess = null;
        private PayrollAccess _payrollAccess = null;
        private YearEndAccess _yearEndAccess = null;
        private AddressAccess _addressAccess = null;
        private PersonManagement _personManagement = null;
        private ReportManagement _reportManagement = null;
        private static ILogger _logger = LogManager.GetCurrentClassLogger(typeof(XmlManagement));
        #endregion

        #region properties
        private XmlAccess XmlAccess
        {
            get
            {
                if (_xmlAccess == null)
                    _xmlAccess = new XmlAccess();

                return _xmlAccess;
            }
        }
        private EmployeeAccess EmployeeAccess
        {
            get
            {
                if (_employeeAccess == null)
                    _employeeAccess = new EmployeeAccess();

                return _employeeAccess;
            }
        }
        private PersonAccess PersonAccess
        {
            get
            {
                if (_personAccess == null)
                    _personAccess = new PersonAccess();
                return _personAccess;
            }
        }
        private PayrollAccess PayrollAccess
        {
            get
            {
                if (_payrollAccess == null)
                    _payrollAccess = new PayrollAccess();

                return _payrollAccess;
            }
        }
        private YearEndAccess YearEndAccess
        {
            get
            {
                if (_yearEndAccess == null)
                    _yearEndAccess = new YearEndAccess();

                return _yearEndAccess;
            }
        }
        private AddressAccess AddressAccess
        {
            get
            {
                if (_addressAccess == null)
                    _addressAccess = new AddressAccess();

                return _addressAccess;
            }
        }
        private PersonManagement PersonManagement
        {
            get
            {
                if (_personManagement == null)
                    _personManagement = new PersonManagement();

                return _personManagement;
            }
        }
        private ReportManagement ReportManagement
        {
            get
            {
                if (_reportManagement == null)
                    _reportManagement = new ReportManagement();

                return _reportManagement;
            }
        }
        #endregion

        public byte[] ConvertStringToByteArray(String input)
        {
            //****there must be a better way to do this****
            byte[] bytes = new byte[input.Length];
            long i = 0;

            foreach (char val in input.ToCharArray())
                bytes[i++] = (byte)val;

            return bytes;
        }
        public String CreateDirectory(String companyCode, String importExportProcessingDirectory)
        {
            String rtn;

            lock (this)
            {
                rtn = Path.Combine(importExportProcessingDirectory, String.Format("{0:yyyyMMddHHmmssfff}_{1}", DateTime.Now, companyCode));
                System.Threading.Thread.Sleep(1);
            }

            Directory.CreateDirectory(rtn);

            return rtn;
        }
        public void ImportBatchesViaFtp(DatabaseUser user, string importName, string importExportProcessingDirectory, bool importDirtySave, bool autoAddSecondaryPositions, string realFtpName, string filePatternToMatch, string payrollProcessRunTypeCode, string payrollProcessGroupCode)
        {
            //get ftp protocol details
            ExportFtp exportFtp = PayrollAccess.GetExportFtp(user, null, null, realFtpName)[0];

            //get files from FTP
            SortedDictionary<string, byte[]> ftpFiles = new FTP.SftpManagement().DownloadSftpFile(exportFtp.UriName, exportFtp.Port, exportFtp.Directory, exportFtp.UnixPathFlag, exportFtp.Login, exportFtp.Password, filePatternToMatch);

            if (ftpFiles.Count > 0)
            {
                //delete existing FTP auto generated import batches
                PayrollAccess.DeleteFtpImportBatchCascade(user, payrollProcessRunTypeCode, payrollProcessGroupCode);

                //get importExportId of the import
                long importExportId = (XmlAccess.GetImportExport(user, null, importName)[0]).ImportExportId;

                foreach (KeyValuePair<string, byte[]> file in ftpFiles)
                {
                    //import the file
                    ImportFile(user, importExportId, file.Value, file.Key, importExportProcessingDirectory, null, null, importDirtySave, autoAddSecondaryPositions, null, "FTP");

                    //delete the file from the sftp server
                    new FTP.SftpManagement().DeleteSftpFile(exportFtp.UriName, exportFtp.Port, exportFtp.Directory, exportFtp.UnixPathFlag, exportFtp.Login, exportFtp.Password, file.Key);
                }
            }
        }

        public ImportExportLog ImportFile(DatabaseUser user, long importExportId, byte[] data, string importFileName, string importExportProcessingDirectory, string autoGenerateEmployeeNumber, string employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions, long? payrollProcessId = null, string codePayrollBatchTypeCd = null, bool usingGui = true)
        {
            ImportExportLog log = new ImportExportLog(Guid.NewGuid()) { FileProcessingDirectory = string.Empty, ProcessingOutput = string.Empty };

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    // We need to stamp the log now so it has an id to pass down to the import file
                    InsertImportExportLog(user, log);

                    ImportFile(user, log, importExportId, data, importFileName, importExportProcessingDirectory, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions, DateTime.Now, false, payrollProcessId, codePayrollBatchTypeCd);

                    scope.Complete();
                }
            }
            catch (TransactionException exc)
            {
                log.ProcessingOutput += exc.Message + exc.StackTrace;

                //if not using the GUI to do the import, ex.  ftp import, throw an exception
                if (!usingGui)
                    throw exc;
            }
            catch (Exception exc)
            {
                log.ProcessingOutput += exc.Message + exc.StackTrace;

                //if not using the GUI to do the import, ex.  ftp import, throw an exception
                if (!usingGui)
                    throw exc;
            }
            finally
            {
                FinalizeImportExportLog(user, log);
            }

            return log;
        }
        public ImportExportLogCollection GetImportExportLog(DatabaseUser user, Guid uniqueIdentifier)
        {
            return XmlAccess.GetImportExportLog(user, uniqueIdentifier);
        }
        public void InsertImportExportLog(DatabaseUser user, ImportExportLog log)
        {
            XmlAccess.InsertImportExportLog(user, log);
        }

        public void FinalizeImportExportLog(DatabaseUser user, ImportExportLog log)
        {
            XmlAccess.UpdateImportExportLog(user, log);
        }

        //write file
        public void WriteFile(String filePath, byte[] data)
        {
            WriteFile(filePath, data, null);
        }
        public void WriteFile(String filePath, byte[] data, String prependHeader)
        {
            using (FileStream importFileStream = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                if (!string.IsNullOrEmpty(prependHeader))
                {
                    byte[] header = ConvertStringToByteArray(prependHeader + Environment.NewLine);
                    importFileStream.Write(header, 0, header[header.Length - 1] == 26 ? header.Length - 1 : header.Length);
                }

                //write out file for processing (note, remove end of file "1A"=char26 if exists)
                importFileStream.Write(data, 0, data[data.Length - 1] == 26 ? data.Length - 1 : data.Length);
                importFileStream.Close();
            }
        }
        public ImportExportLog ImportFile(DatabaseUser user, ImportExportLog log, long importExportId, byte[] data, String importFileName, String importExportProcessingDirectory, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions, DateTime importFileCreationDate, bool automaticImportFlag, long? payrollProcessId = null, string codePayrollBatchTypeCd = null)
        {
            //get import attributes
            ImportExport importExport = XmlAccess.GetImportExport(user, importExportId, null)[0];
            log.ImportExportId = importExport.ImportExportId;
            log.ImportExportDate = importFileCreationDate;

            //form directory name
            log.FileProcessingDirectory = CreateDirectory(user.DatabaseName, importExportProcessingDirectory);

            //write imported file to temp directory
            String lastXmlFilePath = Path.Combine(log.FileProcessingDirectory, importFileName);
            WriteFile(lastXmlFilePath, data, importExport.PrependHeaderValue);

            StreamConverter converter = new StreamConverter();

            if (importExport.Label.Equals("SunLifeBenImp"))
            {
                //if sunlife benefit need to break into 2 files
                String employeePaycodeImportFileName = $"{importFileName}-employeePaycode.csv";
                String payrollBatchImportFileName = $"{importFileName}-batch.csv";
                String employeePaycodeImportFilePathName = Path.Combine(log.FileProcessingDirectory, employeePaycodeImportFileName);
                String payrollBatchImportFilePathName = Path.Combine(log.FileProcessingDirectory, payrollBatchImportFileName);
                GenerateSunLifeBenefitImportFiles(user, lastXmlFilePath, employeePaycodeImportFilePathName, payrollBatchImportFilePathName);

                if (File.Exists(employeePaycodeImportFilePathName))
                    ImportFile(user, employeePaycodeImportFilePathName, log, "STDPCIMP", automaticImportFlag, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);

                if (File.Exists(payrollBatchImportFilePathName))
                    ImportFile(user, payrollBatchImportFilePathName, log, "STDPAYIMP", automaticImportFlag, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);
            }
            else
            {
                // If this import is flagged for conversion and is an .xls or .xlsx file we will convert it
                if (importExport.CanConvertXlsxToCsv)
                {
                    if (!string.IsNullOrWhiteSpace(importFileName) && Path.GetExtension(importFileName).StartsWith(".xls"))
                    {
                        using (Stream csvStream = converter.ConvertExcelToCsv(File.OpenRead(lastXmlFilePath)))
                        {
                            importFileName = Path.ChangeExtension(lastXmlFilePath, ".csv");

                            using (FileStream file = File.Create(importFileName))
                            {
                                csvStream.CopyTo(file);
                                file.Flush(true);
                                file.Close();
                            }
                        }
                    }
                }

                // Remove BOM?
                if (importExport.RemoveByteOrderMark)
                {
                    string currentFileExt = Path.GetExtension(importFileName);
                    using (FileStream file = File.Create($"{lastXmlFilePath}.NoBOM{currentFileExt}"))
                    {
                        using (Stream ms = converter.RemoveByteOrderMark(File.OpenRead(lastXmlFilePath)))
                        {
                            ms.CopyTo(file);
                            file.Flush(true);
                            file.Close();
                        }

                        importFileName = file.Name;
                    }
                }

                ImportFile(user, importFileName, log, importExport.Label, automaticImportFlag, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions, payrollProcessId, codePayrollBatchTypeCd);
            }

            return log;
        }
        private void ImportFile(DatabaseUser user, String importFileName, ImportExportLog log, String importExportLabel, bool automaticImportFlag, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions, long? payrollProcessId = null, string codePayrollBatchTypeCd = null)
        {
            Object xsdObject = ConvertImportFile(user, importFileName, log, importExportLabel, automaticImportFlag);
            ProcessData(user, xsdObject, log, importExportLabel, automaticImportFlag, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions, payrollProcessId, codePayrollBatchTypeCd);
        }
        public Object ConvertImportFile(DatabaseUser user, String importExportLabel, String importFileName, ImportExportLog log)
        {
            return ConvertImportFile(user, importFileName, log, importExportLabel, true);
        }
        public Object ConvertImportFile(DatabaseUser user, String importFileName, ImportExportLog log, String importExportLabel, bool automaticImportFlag)
        {
            String lastXmlFilePath = Path.Combine(log.FileProcessingDirectory, importFileName);

            //loop through xslt's to get final xml
            //load xslt's
            try
            {
                ImportExportXmlSummaryCollection xsls = XmlAccess.GetImportExportXmlSummary(user, importExportLabel);
                String nextImportFilePath = null;
                foreach (ImportExportXmlSummary xsl in xsls)
                {
                    nextImportFilePath = Path.Combine(log.FileProcessingDirectory, String.Format("{0}-{1}.xml", importFileName, xsl.ProcessingOrder));

                    if (xsl.InputFileParameterRequiredFlag)
                        TransformCsvXml(xsl, lastXmlFilePath, nextImportFilePath);
                    else
                        TransformXml(xsl, lastXmlFilePath, nextImportFilePath);

                    lastXmlFilePath = nextImportFilePath;
                }

            }
            catch (Exception)
            {

                log.ProcessingOutput += "Import file is invalid.";
            }

            //get datatype
            String datatype = XmlAccess.GetImportExport(user, null, importExportLabel)[0].XsdBusinessObject;
            //return GetXsdObject(user, lastXmlFilePath, datatype);
            return GetXsdObject(user, importFileName, lastXmlFilePath, datatype);
        }
        public Object GetXsdObject(DatabaseUser user, String importFileName, String importFilePath, String datatype)
        {
            try
            {
                Object rtn = null;

                if (datatype.Equals("WorkLinksEmployeePaycode"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(WorkLinksEmployeePaycode));
                    TextReader reader = new StreamReader(importFilePath);
                    WorkLinksEmployeePaycode xsdObject = (WorkLinksEmployeePaycode)serializer.Deserialize(reader);
                    reader.Close();

                    rtn = xsdObject;
                }
                else if (datatype.Equals("WorkLinksPayrollBatch"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(WorkLinksPayrollBatch));
                    TextReader reader = new StreamReader(importFilePath);
                    WorkLinksPayrollBatch xsdObject = (WorkLinksPayrollBatch)serializer.Deserialize(reader);
                    reader.Close();

                    rtn = xsdObject;
                }
                else if (datatype.Equals("WorkLinksEmployee"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(WorkLinksEmployee));
                    TextReader reader = new StreamReader(importFilePath);
                    WorkLinksEmployee xsdObject = (WorkLinksEmployee)serializer.Deserialize(reader);
                    reader.Close();

                    rtn = xsdObject;
                }
                else if (datatype.Equals("WorkLinksYearEndAdjustment"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(WorkLinksYearEndAdjustment[]));
                    TextReader reader = new StreamReader(importFilePath);
                    WorkLinksYearEndAdjustment[] xsdObject = (WorkLinksYearEndAdjustment[])serializer.Deserialize(reader);
                    reader.Close();

                    rtn = xsdObject;
                }
                else if (datatype.Equals("WorkLinksAddress"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(WorkLinksPersonAddress[]));
                    TextReader reader = new StreamReader(importFilePath);
                    WorkLinksPersonAddress[] xsdObject = (WorkLinksPersonAddress[])serializer.Deserialize(reader);
                    reader.Close();

                    rtn = xsdObject;
                }
                else if (datatype.Equals("WorkLinksBanking"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(WorkLinksEmployeeBanking[]));
                    TextReader reader = new StreamReader(importFilePath);
                    WorkLinksEmployeeBanking[] xsdObject = (WorkLinksEmployeeBanking[])serializer.Deserialize(reader);
                    reader.Close();

                    rtn = xsdObject;
                }
                else if (datatype.Equals("WorkLinksEmployeeBiographical"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(WorkLinksEmployee[]));
                    TextReader reader = new StreamReader(importFilePath);
                    WorkLinksEmployee[] xsdObject = (WorkLinksEmployee[])serializer.Deserialize(reader);
                    reader.Close();

                    rtn = xsdObject;
                }
                else if (datatype.Equals("WorkLinksEmployment"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(WorkLinksEmployeeEmploymentInformation[]));
                    TextReader reader = new StreamReader(importFilePath);
                    WorkLinksEmployeeEmploymentInformation[] xsdObject = (WorkLinksEmployeeEmploymentInformation[])serializer.Deserialize(reader);
                    reader.Close();

                    rtn = xsdObject;
                }
                else if (datatype.Equals("WorkLinksPosition"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(WorkLinksEmployeePosition[]));
                    TextReader reader = new StreamReader(importFilePath);
                    WorkLinksEmployeePosition[] xsdObject = (WorkLinksEmployeePosition[])serializer.Deserialize(reader);
                    reader.Close();

                    rtn = xsdObject;
                }
                else if (datatype.Equals("WorkLinksStatutoryDeduction"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(WorklinksStatutoryDeduction[]));
                    TextReader reader = new StreamReader(importFilePath);
                    WorklinksStatutoryDeduction[] xsdObject = (WorklinksStatutoryDeduction[])serializer.Deserialize(reader);
                    reader.Close();

                    rtn = xsdObject;
                }
                else if (datatype.Equals("RemittanceImport"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(RemittanceImport));
                    TextReader reader = new StreamReader(importFilePath);
                    RemittanceImport xsdObject = (RemittanceImport)serializer.Deserialize(reader);
                    reader.Close();

                    rtn = xsdObject;
                }
                else if (datatype.Equals("EmployeeOnboarding"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(WorkLinksEmployeeOnboarding[]));
                    TextReader reader = new StreamReader(importFilePath);
                    WorkLinksEmployeeOnboarding[] xsdObject = (WorkLinksEmployeeOnboarding[])serializer.Deserialize(reader);
                    reader.Close();

                    rtn = xsdObject;
                }
                else if (datatype.Equals("WorkLinksYearEndAdjustmentPaycode"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(WorkLinksYearEndAdjustmentPaycode[]));
                    TextReader reader = new StreamReader(importFilePath);
                    WorkLinksYearEndAdjustmentPaycode[] xsdObject = (WorkLinksYearEndAdjustmentPaycode[])serializer.Deserialize(reader);
                    reader.Close();

                    rtn = xsdObject;
                }
                else if (datatype.Equals("AdvantageTimeFileImport"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(AdvantageTimeRecord));
                    TextReader reader = new StreamReader(importFilePath);
                    AdvantageTimeRecord xsdObject = (AdvantageTimeRecord)serializer.Deserialize(reader);
                    reader.Close();
                    xsdObject.ImportFileName = importFileName;

                    rtn = xsdObject;
                }

                return rtn;
            }
            catch (Exception e)
            {
                _logger.Error($"Error while deserializing {datatype}: {e}");
                throw;
            }
        }
        public ImportExportCollection GetImportExport(DatabaseUser user, long? importExportId, String label)
        {
            return XmlAccess.GetImportExport(user, importExportId, label);
        }
        public void ProcessData(DatabaseUser user, Object xsdObject, ImportExportLog log, String importExportLabel, bool automaticImportFlag, String autoGenerateEmployeeNumber, String employeeNumberFormat, bool importDirtySave, bool autoAddSecondaryPositions, long? payrollProcessId = null, string codePayrollBatchTypeCd = null)
        {
            String datatype = XmlAccess.GetImportExport(user, null, importExportLabel)[0].XsdBusinessObject;

            try
            {
                if (datatype.Equals("WorkLinksEmployeePaycode"))
                {
                    EmployeeManagement management = new EmployeeManagement();
                    management.ProcessEmployeePaycodeImport(user, (WorkLinksEmployeePaycode)xsdObject, log);
                }
                else if (datatype.Equals("WorkLinksPayrollBatch"))
                {
                    PayrollManagement management = new PayrollManagement();
                    management.ProcessPayrollBatchImport(user, (WorkLinksPayrollBatch)xsdObject, log, automaticImportFlag, codePayrollBatchTypeCd);
                }
                else if (datatype.Equals("WorkLinksYearEndAdjustment"))
                {
                    YearEndManagement management = new YearEndManagement();
                    management.ProcessYearEndAdjustment(user, (WorkLinksYearEndAdjustment[])xsdObject, log, automaticImportFlag);
                }
                else if (datatype.Equals("WorkLinksAddress"))
                {
                    PersonManagement management = new PersonManagement();
                    management.ProcessAddress(user, (WorkLinksPersonAddress[])xsdObject, log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);
                }
                else if (datatype.Equals("WorkLinksBanking"))
                {
                    EmployeeManagement management = new EmployeeManagement();
                    management.ProcessBanking(user, (WorkLinksEmployeeBanking[])xsdObject, log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);
                }
                else if (datatype.Equals("WorkLinksEmployeeBiographical"))
                {
                    EmployeeManagement management = new EmployeeManagement();
                    management.ProcessEmployee(user, (WorkLinksEmployee[])xsdObject, log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);
                }
                else if (datatype.Equals("WorkLinksEmployment"))
                {
                    EmployeeManagement management = new EmployeeManagement();
                    management.ProcessEmployment(user, (WorkLinksEmployeeEmploymentInformation[])xsdObject, log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);
                }
                else if (datatype.Equals("WorkLinksPosition"))
                {
                    EmployeeManagement management = new EmployeeManagement();
                    management.ProcessPosition(user, (WorkLinksEmployeePosition[])xsdObject, log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);
                }
                else if (datatype.Equals("WorkLinksStatutoryDeduction"))
                {
                    EmployeeManagement management = new EmployeeManagement();
                    management.ProcessStatutoryDeduction(user, (WorklinksStatutoryDeduction[])xsdObject, log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);
                }
                else if (datatype.Equals("RemittanceImport"))
                {
                    PayrollManagement management = new PayrollManagement();
                    ((RemittanceImport)xsdObject).PayrollProcessId = payrollProcessId;
                    management.ProcessRemittance(user, (RemittanceImport)xsdObject, log, importDirtySave);
                }
                else if (datatype.Equals("EmployeeOnboarding"))
                {
                    EmployeeManagement management = new EmployeeManagement();
                    management.ProcessEmployeeOnboarding(user, (WorkLinksEmployeeOnboarding[])xsdObject, log, autoGenerateEmployeeNumber, employeeNumberFormat, importDirtySave, autoAddSecondaryPositions);
                }
                else if (datatype.Equals("WorkLinksYearEndAdjustmentPaycode"))
                {
                    YearEndManagement management = new YearEndManagement();
                    management.ProcessYearEndAdjustmentPaycode(user, (WorkLinksYearEndAdjustmentPaycode[])xsdObject, log, automaticImportFlag);
                }
                else if (datatype.Equals("AdvantageTimeFileImport"))
                {
                    PayrollManagement management = new PayrollManagement();
                    management.ProcessAdvantageTimeRecord(user, (AdvantageTimeRecord)xsdObject, log, importDirtySave);
                }
            }
            catch (Exception ex)
            {
                //return to the GUI
                log.ProcessingOutput = (log.ProcessingOutput + "\r\nError:" + ex.Message + "\r\n" + ex.StackTrace) ?? ex.StackTrace;
                XmlAccess.InsertImportExportLog(user, log);
                throw ex;
            }
        }
        /// <summary>
        /// for transforming text files via xsl
        /// </summary>
        /// <param name="xsl"></param>
        /// <param name="sourceXmlPath"></param>
        /// <param name="outputFilePath"></param>
        private void TransformCsvXml(ImportExportXmlSummary xsl, String sourceXmlPath, String outputFilePath) 
        {
            Processor processor = new Processor();
            XsltCompiler compiler = processor.NewXsltCompiler();

            //convert import file data to stream
            MemoryStream xslStream = new MemoryStream(xsl.Data);

            //build transformer
            XsltExecutable xslExec = compiler.Compile(xslStream);
            XsltTransformer transformer = xslExec.Load();

            //set file parm (specific to csv requirement)
            transformer.InitialTemplate = new QName("main");
            Uri sourceUri = new Uri(sourceXmlPath);
            XdmValue parm = new XdmAtomicValue(sourceUri);
            transformer.SetParameter(new QName(xsl.InputFileParameterName), parm);

            //build file output
            Serializer serializer = new Serializer();
            serializer.SetOutputFile(outputFilePath);
            transformer.Run(serializer);
        }
        public void TransformXml(ImportExportXmlSummary xsl, String sourceXmlPath, String outputFilePath)
        {
            TransformXml(xsl, sourceXmlPath, outputFilePath, null, null);
        }
        /// <summary>
        /// for transforming xml files via xslt
        /// </summary>
        /// <param name="xsl"></param>
        /// <param name="sourceXmlPath"></param>
        /// <param name="outputFilePath"></param>
        public void TransformXml(ImportExportXmlSummary xsl, String sourceXmlPath, String outputFilePath, String[] parmNames, Object[] parms)
        {
            List<StaticError> errors = new List<StaticError>();

            try
            {
                Processor processor = new Processor();
                XsltCompiler compiler = processor.NewXsltCompiler();
                compiler.ErrorList = errors;

                //convert import file data to stream
                MemoryStream xslStream = new MemoryStream(xsl.Data);

                //build transformer
                XsltExecutable xslExec = compiler.Compile(xslStream);
                XsltTransformer transformer = xslExec.Load();

                //inject parms if they exist
                if (parmNames != null && parmNames.Length > 0)
                {
                    int parmIndex = 0;

                    foreach (String parmName in parmNames)
                    {
                        QName name = new QName(parmName);
                        XdmValue value = GetXdmValue(parms[parmIndex++]);
                        transformer.SetParameter(name, value);
                    }
                }

                //load input file
                transformer.InitialContextNode = processor.NewDocumentBuilder().Build(new Uri(sourceXmlPath));

                //build output file
                Serializer serializer = new Serializer();
                serializer.SetOutputFile(outputFilePath);
                transformer.Run(serializer);
            }
            catch (Exception exc)
            {
                System.Text.StringBuilder builder = new System.Text.StringBuilder();

                foreach (StaticError error in errors)
                    builder.AppendLine(error.Message);

                throw new Exception(builder.ToString(), exc);
            }
        }
        private XdmValue GetXdmValue(Object obj)
        {
            XdmValue value = null;

            if (obj is bool)
                value = new XdmAtomicValue((bool)obj);
            if (obj is DateTime)
                value = new XdmAtomicValue(String.Format("{0:yyyy-MM-ddTHH:mm:ss+HH:mm}", obj));
            else if (obj is String)
                value = new XdmAtomicValue((String)obj);

            return value;
        }

        #region import/export
        public ImportExportSummaryCollection GetImportExportSummary(DatabaseUser user)
        {
            try
            {
                return XmlAccess.GetImportExportSummary(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "XmlManagement - GetImportExportSummary", ex);
                throw;
            }
        }
        public byte[] BuildResponse(DatabaseUser user, String inputFilePath, String outputFilePath, String responseExpression, String sourceCreateDateTime, String sourceBodid)
        {
            ImportExportXmlSummaryCollection xslts = XmlAccess.GetImportExportXmlSummary(user, "HrXmlAcknowledge");

            String[] parms = { "createDateTime", "bodid", "responseExpression", "sourceCreateDateTime", "sourceBodid" };
            Object[] values = { DateTime.Now, Guid.NewGuid().ToString(), responseExpression, sourceCreateDateTime, sourceBodid };
            TransformXml(xslts[0], inputFilePath, outputFilePath, parms, values);

            return File.ReadAllBytes(outputFilePath);
        }
        private bool CheckForRoeAndRoeReason(EmployeeTerminationRoeCollection coll)
        {
            bool missingData = false;

            //make sure an roe exists
            if (coll.Count == 0)
                missingData = true;
            else
            {
                //make sure a reason has been set, if not, do not allow export to proceed.  ("reason" is mandatory but hrxml might not send it for some reason)
                foreach (EmployeeTerminationRoe termRoe in coll)
                {
                    if (termRoe.CodeRoeReasonCd == null)
                    {
                        missingData = true;
                        break; //exit this foreach
                    }
                }
            }

            return missingData;
        }
        public byte[] ExportRoe(DatabaseUser user, long[] employeePositionIds, String importExportProcessingDirectory, String roeIssueCode, out bool missingRoeReason)
        {
            bool exitForEach = false;
            EmployeeCollection employees = new EmployeeCollection();

            foreach (long employeePositionId in employeePositionIds)
            {
                EmployeePosition position = new EmployeeManagement().GetEmployeePosition(user, new EmployeePositionCriteria() { EmployeePositionId = employeePositionId, IncludeOrganizationUnit = true, IncludeWorkDays = false })[0];
                Employee employee = EmployeeAccess.GetEmployee(user, position.EmployeeId, true)[0];
                PersonAccess.GetPerson(user, employee.PersonId)[0].CopyTo(employee);
                employee.CurrentEmployeePosition = position;
                employee.PrimaryAddress = PersonAccess.GetPrimaryPersonAddress(user, employee.PersonId);
                EmployeeTerminationRoeCollection terminationRoes = EmployeeAccess.GetEmployeeTerminationRoe(user, employeePositionId);
                if (terminationRoes.Count > 0) employee.CurrentEmployeeTerminationRoe = terminationRoes[0];

                //check that roe exits and reason is populated, if not we exit this foreach
                exitForEach = CheckForRoeAndRoeReason(terminationRoes);

                //if an roe doesnt exist or an roe does not contain an roe reason, this condition will be true and we will exit the parent foreach
                if (exitForEach)
                    break;

                EmployeeRoeAmountSummaryCollection roeAmounts = PayrollAccess.GetEmployeeRoeAmountSummary(user, employeePositionId);
                if (roeAmounts.Count > 0) employee.EmployeeRoeAmountSummary = roeAmounts[0];

                employee.EmployeeRoeAmountSummary.AmountDetailSummaryCollection = PayrollAccess.GetRoeDataDetailsSummary(user, employeePositionId, employee.EmployeeId);
                employees.Add(employee);
            }

            if (exitForEach)
            {
                missingRoeReason = exitForEach;
                return null;
            }

            Roe roe = new Roe() { UpdateUser = user.UserName, UpdateDatetime = DateTime.Now };
            roe.Employees = employees;
            ImportExportXmlSummary xsl = XmlAccess.GetImportExportXmlSummary(user, "Roe")[0]; //**HARDCODE**//

            //create working directory
            String workingDirectory = CreateDirectory(user.DatabaseName, importExportProcessingDirectory);
            String sourceFilePath = Path.Combine(workingDirectory, String.Format("{0}-{1}.xml", "roeExport", 0));
            String outputFilePath = Path.Combine(workingDirectory, String.Format("{0}-{1}.xml", "roeExport", 1));

            //serialize source
            using (FileStream sourceStream = new FileStream(sourceFilePath, FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(roe.GetType());
                serializer.Serialize(sourceStream, roe);
            }

            String[] parms = { "issue" };
            Object[] values = { roeIssueCode };

            TransformXml(xsl, sourceFilePath, outputFilePath, parms, values);

            roe.Data = File.ReadAllBytes(outputFilePath);

            //store roe to database for auditing and stamp roe amount status and roe links
            using (TransactionScope scope = new TransactionScope())
            {
                PayrollAccess.InsertRoe(user, roe);

                foreach (Employee employee in employees)
                {
                    employee.EmployeeRoeAmountSummary.RoeFileProcessedFlag = true;
                    employee.EmployeeRoeAmountSummary.RoeId = roe.RoeId;

                    PayrollAccess.UpdateEmployeeRoeAmount(user, employee.EmployeeRoeAmountSummary);

                    employee.CurrentEmployeePosition.CodeRoeCreationStatusCd = "CMP";//COMPLETE
                    EmployeeAccess.UpdateEmployeePosition(user, employee.CurrentEmployeePosition);
                }

                scope.Complete();
            }

            //should be false if we get here...but must assign "out" parameter a value...
            missingRoeReason = exitForEach;

            return roe.Data;
        }
        private Object GetCellValue(ICell cell)
        {
            if (cell == null)
                return null;
            else if (cell.CellType.Equals(CellType.Boolean))
                return cell.BooleanCellValue;
            else if (cell.CellType.Equals(CellType.Formula))
                return cell.CellFormula;
            else if (cell.CellType.Equals(CellType.Numeric))
                return cell.NumericCellValue;
            else if (cell.CellType.Equals(CellType.String))
                return cell.StringCellValue;
            else
                return null;
        }
        private void GenerateSunLifeBenefitImportFiles(DatabaseUser user, String sunLifeImportFilePath, String sunLifeEmployeePaycodeImportFilePath, String sunLifePayrollBatchImportFilePath)
        {
            const int employeeColumnIndex = 7;
            const int employeeTerminatedColumnIndex = 10;
            const int employeeStartRowIndex = 9;
            const int paycodeIdentifier1StartColumnIndex = 11;
            const int paycodeIdentifier1RowIndex = 6;
            const int paycodeIdentifier2RowIndex = 8;
            const int paycodeIdentifier2NumberOfColumn = 7;

            HSSFWorkbook wb = null;
            XmlAccess _xmlAccess = new XmlAccess();
            ExternalPaycodeImportMapCollection externalPaycodeMapCollection = _xmlAccess.GetExternalPaycodeImportMap(user, "SunLifeBenefit");

            using (FileStream stream = new FileStream(sunLifeImportFilePath, FileMode.Open, FileAccess.Read))
            {
                wb = new HSSFWorkbook(new POIFSFileSystem(stream), true);
                ISheet sheet = wb.GetSheetAt(0);

                int maxRowCount = sheet.LastRowNum + 1;
                int maxColumnCount = sheet.GetRow(paycodeIdentifier2RowIndex).LastCellNum;
                int importIndex = 0;

                PaycodeImportCollection importedPaycodes = new PaycodeImportCollection();

                //loop to get rows (assume starts at row 10)
                for (int rowIndex = employeeStartRowIndex; rowIndex < maxRowCount; rowIndex++)
                {
                    //get employee number
                    if (sheet.GetRow(rowIndex) == null)
                        break;

                    String employeeIdentifier = Convert.ToString(GetCellValue(sheet.GetRow(rowIndex).GetCell(employeeColumnIndex)));

                    if (employeeIdentifier == null || employeeIdentifier.Trim().Equals(String.Empty))
                        break;

                    //get employee terminated
                    String employeeTerminatedValue = Convert.ToString(GetCellValue(sheet.GetRow(rowIndex).GetCell(employeeTerminatedColumnIndex)));
                    bool employeeIsTerminated = (employeeTerminatedValue != null && employeeTerminatedValue.Equals("A"));

                    String paycodeIdentifier1 = null;
                    String paycodeIdentifier2 = null;
                    Decimal paycodeValue = 0;

                    //get paycode info 
                    for (int columnIndex = paycodeIdentifier1StartColumnIndex; columnIndex < maxColumnCount; columnIndex++)
                    {
                        //get paycode identifier #1
                        if ((columnIndex - paycodeIdentifier1StartColumnIndex) % paycodeIdentifier2NumberOfColumn == 0)
                        {
                            paycodeIdentifier1 = Convert.ToString(GetCellValue(sheet.GetRow(paycodeIdentifier1RowIndex).GetCell(columnIndex)));

                            //if no paycode identifier, we are done, exit loop
                            if (paycodeIdentifier1 == null || paycodeIdentifier1.Trim().Equals(String.Empty))
                                break;
                        }

                        //get paycode identifer #2
                        paycodeIdentifier2 = Convert.ToString(GetCellValue(sheet.GetRow(paycodeIdentifier2RowIndex).GetCell(columnIndex)));
                        paycodeValue = Convert.ToDecimal(GetCellValue(sheet.GetRow(rowIndex).GetCell(columnIndex)));

                        PaycodeImport import = new PaycodeImport();
                        import.PaycodeImportId = importIndex--;
                        import.EmployeeIdentifier = employeeIdentifier;
                        import.AmountRate = Math.Round(Convert.ToDecimal(paycodeValue), 2, MidpointRounding.AwayFromZero);
                        import.Units = 1;
                        import.ImportType = employeeIsTerminated ? PaycodeImport.PaycodeImportType.Batch : PaycodeImport.PaycodeImportType.EmployeePaycode;
                        import.ExternalPaycodeImportMap = externalPaycodeMapCollection.GetExternalPaycodeImportMap(paycodeIdentifier1, paycodeIdentifier2);

                        if (import.ExternalPaycodeImportMap == null)
                            throw new Exception(String.Format("Missing paycode mapping for {0} - {1}", paycodeIdentifier1, paycodeIdentifier2));

                        importedPaycodes.Add(import);
                    }
                }

                String employeePaycodes = importedPaycodes.EmployeePaycodeCSV;
                String payrollBatch = importedPaycodes.PayrollBatchCSV;
                String employeePaycodeCsv = importedPaycodes.EmployeePaycodeCSV;

                if (employeePaycodeCsv != null)
                {
                    using (StreamWriter outfile = new StreamWriter(sunLifeEmployeePaycodeImportFilePath))
                        outfile.Write(employeePaycodeCsv);
                }

                String batchCsv = importedPaycodes.PayrollBatchCSV;

                if (batchCsv != null)
                {
                    using (StreamWriter outfile = new StreamWriter(sunLifePayrollBatchImportFilePath))
                        outfile.Write(batchCsv);
                }
            }
        }
        public CanadaRevenueAgencyT4ExportCollection ExportT4Details(DatabaseUser user, String year, String exportType)
        {
            //load year end
            YearEnd yearEnd = YearEndAccess.GetYearEnd(user, Convert.ToDecimal(year))[0];

            //load t4export
            CanadaRevenueAgencyT4ExportCollection exportCollection = YearEndAccess.GetCanadaRevenueAgencyT4Export(user, yearEnd.YearEndId, exportType);

            if (exportCollection.Count > 0)
            {
                exportCollection[0].YearEnd = yearEnd;

                //load t619
                exportCollection[0].CanadaRevenueAgencyT619 = YearEndAccess.GetCanadaRevenueAgencyT619(user, exportCollection[0].CanadaRevenueAgencyT619Id)[0];
                exportCollection[0].CanadaRevenueAgencyT619.Person = PersonAccess.GetPerson(user, exportCollection[0].CanadaRevenueAgencyT619.TransmitterPersonId)[0];
                PersonContactChannelCollection transmitterPhones = PersonManagement.GetPersonContactChannel(user, exportCollection[0].CanadaRevenueAgencyT619.TransmitterPersonId, "Phone");
                exportCollection[0].CanadaRevenueAgencyT619.Phone = transmitterPhones.PrimaryContactChannel;
                PersonContactChannelCollection transmitterEmails = PersonManagement.GetPersonContactChannel(user, exportCollection[0].CanadaRevenueAgencyT619.TransmitterPersonId, "Email");
                exportCollection[0].CanadaRevenueAgencyT619.Email = transmitterEmails.PrimaryContactChannel;
                PersonAddressCollection transmitterAddresses = PersonAccess.GetPersonAddress(user, exportCollection[0].CanadaRevenueAgencyT619.TransmitterPersonId);
                exportCollection[0].CanadaRevenueAgencyT619.PersonAddress = transmitterAddresses.PrimaryAddress;

                //format postal/zip code for GUI viewing
                PersonManagement.FormatPostalZipCode(exportCollection[0].CanadaRevenueAgencyT619.PersonAddress);
            }

            return exportCollection;
        }
        public CanadaRevenueAgencyT4Export InsertExportT4Details(DatabaseUser user, CanadaRevenueAgencyT4Export export, String year)
        {
            try
            {
                //insert into Person, Address, PersonAddress, ContactChannel, PersonContactChannel, CanadaRevenueAgencyT619, CanadaRevenueAgencyT4Export
                using (TransactionScope scope = new TransactionScope())
                {
                    //insert Person
                    PersonAccess.InsertPerson(user, user.DatabaseName, export.CanadaRevenueAgencyT619.Person);

                    //insert Address
                    AddressAccess.InsertAddress(user, export.CanadaRevenueAgencyT619.PersonAddress);

                    //insert PersonAddress
                    export.CanadaRevenueAgencyT619.PersonAddress.PersonId = export.CanadaRevenueAgencyT619.Person.PersonId;

                    //format the postal/zip code
                    export.CanadaRevenueAgencyT619.PersonAddress.UpdateUser = user.UserName;
                    PersonManagement.CleanUpPostalZipCode(export.CanadaRevenueAgencyT619.PersonAddress);
                    PersonAccess.InsertPersonAddress(user, export.CanadaRevenueAgencyT619.PersonAddress);

                    //insert into ContactChannel - Phone info
                    PersonAccess.InsertContactChannel(user, export.CanadaRevenueAgencyT619.Phone);

                    //insert into PersonContactChannel
                    export.CanadaRevenueAgencyT619.Phone.PersonId = export.CanadaRevenueAgencyT619.Person.PersonId;
                    export.CanadaRevenueAgencyT619.Phone.UpdateUser = user.UserName;
                    PersonAccess.InsertPersonContactChannel(user, export.CanadaRevenueAgencyT619.Phone);

                    //insert into ContactChannel - Email info
                    export.CanadaRevenueAgencyT619.Email.CreateUser = user.UserName; export.CanadaRevenueAgencyT619.Email.UpdateUser = user.UserName;
                    PersonAccess.InsertContactChannel(user, export.CanadaRevenueAgencyT619.Email);

                    //insert into PersonContactChannel
                    export.CanadaRevenueAgencyT619.Email.PersonId = export.CanadaRevenueAgencyT619.Person.PersonId;
                    PersonAccess.InsertPersonContactChannel(user, export.CanadaRevenueAgencyT619.Email);

                    //insert into CanadaRevenueAgencyT619
                    export.CanadaRevenueAgencyT619.TransmitterPersonId = export.CanadaRevenueAgencyT619.Person.PersonId;
                    export.CanadaRevenueAgencyT619.CreateUser = user.UserName; export.CanadaRevenueAgencyT619.UpdateUser = user.UserName;
                    YearEndAccess.InsertCanadaRevenueAgencyT619(user, export.CanadaRevenueAgencyT619);

                    //insert into CanadaRevenueAgencyT4Export
                    export.CanadaRevenueAgencyT619Id = export.CanadaRevenueAgencyT619.CanadaRevenueAgencyT619Id;
                    export.CreateUser = user.UserName; export.UpdateUser = user.UserName;

                    YearEnd yearEnd = YearEndAccess.GetYearEnd(user, Convert.ToDecimal(year))[0];
                    export.CurrentRevision = yearEnd.CurrentRevision;

                    YearEndAccess.InsertCanadaRevenueAgencyT4Export(user, export);

                    scope.Complete();
                }

                //format postal/zip code for GUI viewing
                PersonManagement.FormatPostalZipCode(export.CanadaRevenueAgencyT619.PersonAddress);

                return export;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "XmlManagement - InsertExportT4Details", ex);
                throw;
            }
        }
        public CanadaRevenueAgencyT4Export UpdateExportT4Details(DatabaseUser user, CanadaRevenueAgencyT4Export export)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //update CanadaRevenueAgencyT619 data (transmitter_number, transmitter_name)
                    YearEndAccess.UpdateCanadaRevenueAgencyT619(user, export.CanadaRevenueAgencyT619);

                    //update person (contact first name, last name)
                    PersonAccess.UpdatePerson(user, export.CanadaRevenueAgencyT619.Person);

                    //update PersonContactChannel (phone, extension)
                    PersonAccess.UpdateContactChannel(user, export.CanadaRevenueAgencyT619.Phone);

                    //update PersonContactChannel (email)
                    PersonAccess.UpdateContactChannel(user, export.CanadaRevenueAgencyT619.Email);

                    //update PersonAddress (address line 1, address line 2, city, country, province/state, postal/zip)
                    //format the postal/zip code
                    PersonManagement.CleanUpPostalZipCode(export.CanadaRevenueAgencyT619.PersonAddress);
                    AddressAccess.UpdateAddress(user, export.CanadaRevenueAgencyT619.PersonAddress);

                    scope.Complete();
                }

                //format postal/zip code for GUI viewing
                PersonManagement.FormatPostalZipCode(export.CanadaRevenueAgencyT619.PersonAddress);

                return export;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "XmlManagement - UpdateExportT4Details", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                {
                    EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                    throw new System.ServiceModel.FaultException<EmployeeServiceException>(concurrencyException, new System.ServiceModel.FaultReason(ex.Message));
                }
                else
                    throw;
            }
        }
        public byte[] ExportT4XmlFile(DatabaseUser user, int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string exportType)
        {
            //load all the business numbers
            BusinessNumberCollection businessNumbers = EmployeeAccess.GetBusinessNumber(user, null);
            Dictionary<String, byte[]> t4Exports = new Dictionary<String, byte[]>();

            foreach (BusinessNumber businessNumber in businessNumbers)
            {
                byte[] output = null;

                //load original t4's (if any exist)
                CanadaRevenueAgencyT4Collection t4s = new CanadaRevenueAgencyT4Collection(YearEndAccess.GetYearEndT4(user, year, businessNumber.BusinessNumberId, true));

                //load amended t4's (if any exist)
                CanadaRevenueAgencyT4Collection amendedT4s = new CanadaRevenueAgencyT4Collection(YearEndAccess.GetYearEndT4(user, year, businessNumber.BusinessNumberId, false, true));

                if (t4s.Count > 0)
                {
                    output = ExportT4XmlFile(user, year, t4s, businessNumber, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, exportType);

                    if (output != null) t4Exports.Add(String.Format("{0}_{1}_{2}_Originals.xml", year, businessNumber.EmployerNumber, "T4"), output);
                }
                if (amendedT4s.Count > 0)
                {
                    output = ExportT4XmlFile(user, year, amendedT4s, businessNumber, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, exportType);

                    if (output != null) t4Exports.Add(String.Format("{0}_{1}_{2}_Amended.xml", year, businessNumber.EmployerNumber, "T4"), output);
                }
            }

            return ReportManagement.CreateZip(user, t4Exports);
        }
        public byte[] ExportT4XmlFile(DatabaseUser user, int year, CanadaRevenueAgencyT4Collection t4s, BusinessNumber businessNumber, String importExportProcessingDirectory, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2, String exportType)
        {
            byte[] rtn = null;

            //load year end
            YearEnd yearEnd = YearEndAccess.GetYearEnd(user, year)[0];

            //load t4export
            CanadaRevenueAgencyT4Export export = YearEndAccess.GetCanadaRevenueAgencyT4Export(user, yearEnd.YearEndId, exportType)[0];
            export.YearEnd = yearEnd;

            //load employer
            export.Employer = GetEmployer(user, businessNumber, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2);

            //load t619
            export.CanadaRevenueAgencyT619 = YearEndAccess.GetCanadaRevenueAgencyT619(user, export.CanadaRevenueAgencyT619Id)[0];
            export.CanadaRevenueAgencyT619.Person = PersonAccess.GetPerson(user, export.CanadaRevenueAgencyT619.TransmitterPersonId)[0];
            PersonContactChannelCollection transmitterPhones = PersonManagement.GetPersonContactChannel(user, export.CanadaRevenueAgencyT619.TransmitterPersonId, "Phone");
            export.CanadaRevenueAgencyT619.Phone = transmitterPhones.PrimaryContactChannel;
            PersonContactChannelCollection transmitterEmails = PersonManagement.GetPersonContactChannel(user, export.CanadaRevenueAgencyT619.TransmitterPersonId, "Email");
            export.CanadaRevenueAgencyT619.Email = transmitterEmails.PrimaryContactChannel;
            PersonAddressCollection transmitterAddresses = PersonAccess.GetPersonAddress(user, export.CanadaRevenueAgencyT619.TransmitterPersonId);
            export.CanadaRevenueAgencyT619.PersonAddress = transmitterAddresses.PrimaryAddress;

            //assign t4's
            export.T4s = t4s;

            foreach (CanadaRevenueAgencyT4 t4 in export.T4s)
            {
                t4.Employee = EmployeeAccess.GetEmployee(user, t4.EmployeeId, true)[0];
                PersonAccess.GetPerson(user, t4.Employee.PersonId)[0].CopyTo(t4.Employee);
                PersonAddressCollection addresses = PersonAccess.GetPersonAddress(user, t4.Employee.PersonId);
                t4.Address = addresses.PrimaryAddress;
                PersonContactChannelCollection phones = PersonManagement.GetPersonContactChannel(user, t4.Employee.PersonId, "Phone");
                t4.Phone = phones.PrimaryContactChannel;
                t4.BusinessNumber = businessNumber;
            }

            XmlSerializer output = new XmlSerializer(export.GetType());

            //create working directory
            String workingDirectory = CreateDirectory(user.DatabaseName, importExportProcessingDirectory);
            String sourceFilePath = Path.Combine(workingDirectory, String.Format("{0}-{1}.xml", "T4Export", 0));

            using (FileStream exportStream = new FileStream(sourceFilePath, FileMode.Create))
            {
                output.Serialize(exportStream, export);
                exportStream.Close();
            }

            ImportExportXmlSummary xsl = XmlAccess.GetImportExportXmlSummary(user, "T4Export")[0]; //**HARDCODE**//
            String outputFilePath = Path.Combine(workingDirectory, String.Format("{0}-{1}.xml", "T4Export", 1));

            TransformXml(xsl, sourceFilePath, outputFilePath);
            rtn = File.ReadAllBytes(outputFilePath);

            //update current revision field of the canada_revenue_agency_t4 table
            export.CurrentRevision = export.YearEnd.CurrentRevision;
            YearEndAccess.UpdateCanadaRevenueAgencyT4Export(user, export);

            //update the year_end table with a timestamp of when the file was created
            ReportManagement.UpdateYearEndTable(user, Convert.ToInt64(year), "T4Export");

            return rtn;
        }

        public byte[] ExportNR4XmlFile(DatabaseUser user, int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string exportType)
        {
            //load all the business numbers
            BusinessNumberCollection businessNumbers = EmployeeAccess.GetBusinessNumber(user, null);
            Dictionary<String, byte[]> nr4Exports = new Dictionary<String, byte[]>();

            foreach (BusinessNumber businessNumber in businessNumbers)
            {
                byte[] output = null;

                //load original NR4's (if any exist)
                CanadaRevenueAgencyNR4Collection nr4s = new CanadaRevenueAgencyNR4Collection(YearEndAccess.GetYearEndNR4rsp(user, year, businessNumber.BusinessNumberId, true));

                //load amended NR4's (if any exist)
                CanadaRevenueAgencyNR4Collection amendedNR4s = new CanadaRevenueAgencyNR4Collection(YearEndAccess.GetYearEndNR4rsp(user, year, businessNumber.BusinessNumberId, false, true));

                if (nr4s.Count > 0)
                {
                    output = ExportNR4XmlFile(user, year, nr4s, businessNumber, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, exportType);

                    if (output != null) nr4Exports.Add(String.Format("{0}_{1}_{2}_Originals.xml", year, businessNumber.EmployerNumber, "NR4"), output);
                }
                if (amendedNR4s.Count > 0)
                {
                    output = ExportNR4XmlFile(user, year, amendedNR4s, businessNumber, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, exportType);

                    if (output != null) nr4Exports.Add(String.Format("{0}_{1}_{2}_Amended.xml", year, businessNumber.EmployerNumber, "NR4"), output);
                }
            }

            return ReportManagement.CreateZip(user, nr4Exports);
        }
        public byte[] ExportNR4XmlFile(DatabaseUser user, int year, CanadaRevenueAgencyNR4Collection nr4s, BusinessNumber businessNumber, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string exportType)
        {
            byte[] rtn = null;

            //load year end
            YearEnd yearEnd = YearEndAccess.GetYearEnd(user, year)[0];

            //load t4export
            CanadaRevenueAgencyT4Export export = YearEndAccess.GetCanadaRevenueAgencyT4Export(user, yearEnd.YearEndId, exportType)[0];
            export.YearEnd = yearEnd;

            //load employer
            export.Employer = GetEmployer(user, businessNumber, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2);

            //load t619
            export.CanadaRevenueAgencyT619 = YearEndAccess.GetCanadaRevenueAgencyT619(user, export.CanadaRevenueAgencyT619Id)[0];
            export.CanadaRevenueAgencyT619.Person = PersonAccess.GetPerson(user, export.CanadaRevenueAgencyT619.TransmitterPersonId)[0];
            PersonContactChannelCollection transmitterPhones = PersonManagement.GetPersonContactChannel(user, export.CanadaRevenueAgencyT619.TransmitterPersonId, "Phone");
            export.CanadaRevenueAgencyT619.Phone = transmitterPhones.PrimaryContactChannel;
            PersonContactChannelCollection transmitterEmails = PersonManagement.GetPersonContactChannel(user, export.CanadaRevenueAgencyT619.TransmitterPersonId, "Email");
            export.CanadaRevenueAgencyT619.Email = transmitterEmails.PrimaryContactChannel;
            PersonAddressCollection transmitterAddresses = PersonAccess.GetPersonAddress(user, export.CanadaRevenueAgencyT619.TransmitterPersonId);
            export.CanadaRevenueAgencyT619.PersonAddress = transmitterAddresses.PrimaryAddress;

            //assign nr4's
            export.NR4s = nr4s;

            foreach (CanadaRevenueAgencyNR4 nr4 in export.NR4s)
            {
                nr4.Employee = EmployeeAccess.GetEmployee(user, nr4.EmployeeId, true)[0];
                PersonAccess.GetPerson(user, nr4.Employee.PersonId)[0].CopyTo(nr4.Employee);
                PersonAddressCollection addresses = PersonAccess.GetPersonAddress(user, nr4.Employee.PersonId);
                nr4.Address = addresses.PrimaryAddress;
                PersonContactChannelCollection phones = PersonManagement.GetPersonContactChannel(user, nr4.Employee.PersonId, "Phone");
                nr4.Phone = phones.PrimaryContactChannel;
                nr4.BusinessNumber = businessNumber;
            }

            if (export.NR4s != null && export.NR4s.Count > 0)
            {
                if (export.NR4s[0].YearEndNr4PayerAgentId != null)
                {
                    //get payer agent info
                    export.Nr4PayerAgent = YearEndAccess.SelectNr4PayerAgentByKey(user, Convert.ToInt64(export.NR4s[0].YearEndNr4PayerAgentId))[0];
                    //get address info for payer agent
                    export.Nr4PayerAgentAddress = AddressAccess.GetAddress(user, export.Nr4PayerAgent.AddressId)[0];
                }
            }

            XmlSerializer output = new XmlSerializer(export.GetType());

            //create working directory
            String workingDirectory = CreateDirectory(user.DatabaseName, importExportProcessingDirectory);
            String sourceFilePath = Path.Combine(workingDirectory, String.Format("{0}-{1}.xml", "NR4Export", 0));

            using (FileStream exportStream = new FileStream(sourceFilePath, FileMode.Create))
            {
                output.Serialize(exportStream, export);
                exportStream.Close();
            }

            ImportExportXmlSummary xsl = XmlAccess.GetImportExportXmlSummary(user, "T4Export")[0]; //NOTE:  use T4Export here because this object holds the whole CRA schema //**HARDCODE**//   
            String outputFilePath = Path.Combine(workingDirectory, String.Format("{0}-{1}.xml", "NR4Export", 1));

            TransformXml(xsl, sourceFilePath, outputFilePath);
            rtn = File.ReadAllBytes(outputFilePath);

            //update the year_end table with a timestamp of when the file was created
            ReportManagement.UpdateYearEndTable(user, Convert.ToInt64(year), "NR4Export");

            return rtn;
        }

        public byte[] ExportT4aXmlFile(DatabaseUser user, int year, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string exportType)
        {
            //load all the business numbers
            BusinessNumberCollection businessNumbers = EmployeeAccess.GetBusinessNumber(user, null);
            Dictionary<String, byte[]> t4aExports = new Dictionary<String, byte[]>();

            foreach (BusinessNumber businessNumber in businessNumbers)
            {
                byte[] output = null;

                //load original t4a's (if any exist)
                CanadaRevenueAgencyT4aCollection t4as = new CanadaRevenueAgencyT4aCollection(YearEndAccess.GetYearEndT4a(user, year, businessNumber.BusinessNumberId, true));

                //load amended t4a's (if any exist)
                CanadaRevenueAgencyT4aCollection amendedT4as = new CanadaRevenueAgencyT4aCollection(YearEndAccess.GetYearEndT4a(user, year, businessNumber.BusinessNumberId, false, true));

                if (t4as.Count > 0)
                {
                    output = ExportT4aXmlFile(user, year, t4as, businessNumber, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, exportType);

                    if (output != null) t4aExports.Add(String.Format("{0}_{1}_{2}_Originals.xml", year, businessNumber.EmployerNumber, "T4A"), output);
                }
                if (amendedT4as.Count > 0)
                {
                    output = ExportT4aXmlFile(user, year, amendedT4as, businessNumber, importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, exportType);

                    if (output != null) t4aExports.Add(String.Format("{0}_{1}_{2}_Amended.xml", year, businessNumber.EmployerNumber, "T4A"), output);
                }
            }

            return ReportManagement.CreateZip(user, t4aExports);
        }
        public byte[] ExportT4aXmlFile(DatabaseUser user, int year, CanadaRevenueAgencyT4aCollection t4as, BusinessNumber businessNumber, string importExportProcessingDirectory, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string exportType)
        {
            byte[] rtn = null;

            //load year end
            YearEnd yearEnd = YearEndAccess.GetYearEnd(user, year)[0];

            //load t4export
            CanadaRevenueAgencyT4Export export = YearEndAccess.GetCanadaRevenueAgencyT4Export(user, yearEnd.YearEndId, exportType)[0];
            export.YearEnd = yearEnd;

            //load employer
            export.Employer = GetEmployer(user, businessNumber, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2);

            //load t619
            export.CanadaRevenueAgencyT619 = YearEndAccess.GetCanadaRevenueAgencyT619(user, export.CanadaRevenueAgencyT619Id)[0];
            export.CanadaRevenueAgencyT619.Person = PersonAccess.GetPerson(user, export.CanadaRevenueAgencyT619.TransmitterPersonId)[0];
            PersonContactChannelCollection transmitterPhones = PersonManagement.GetPersonContactChannel(user, export.CanadaRevenueAgencyT619.TransmitterPersonId, "Phone");
            export.CanadaRevenueAgencyT619.Phone = transmitterPhones.PrimaryContactChannel;
            PersonContactChannelCollection transmitterEmails = PersonManagement.GetPersonContactChannel(user, export.CanadaRevenueAgencyT619.TransmitterPersonId, "Email");
            export.CanadaRevenueAgencyT619.Email = transmitterEmails.PrimaryContactChannel;
            PersonAddressCollection transmitterAddresses = PersonAccess.GetPersonAddress(user, export.CanadaRevenueAgencyT619.TransmitterPersonId);
            export.CanadaRevenueAgencyT619.PersonAddress = transmitterAddresses.PrimaryAddress;

            //assign t4a's
            export.T4as = t4as;

            foreach (CanadaRevenueAgencyT4a t4a in export.T4as)
            {
                t4a.Employee = EmployeeAccess.GetEmployee(user, t4a.EmployeeId, true)[0];
                PersonAccess.GetPerson(user, t4a.Employee.PersonId)[0].CopyTo(t4a.Employee);
                PersonAddressCollection addresses = PersonAccess.GetPersonAddress(user, t4a.Employee.PersonId);
                t4a.Address = addresses.PrimaryAddress;
                PersonContactChannelCollection phones = PersonManagement.GetPersonContactChannel(user, t4a.Employee.PersonId, "Phone");
                t4a.Phone = phones.PrimaryContactChannel;
                t4a.BusinessNumber = businessNumber;
            }

            XmlSerializer output = new XmlSerializer(export.GetType());

            //create working directory
            String workingDirectory = CreateDirectory(user.DatabaseName, importExportProcessingDirectory);
            String sourceFilePath = Path.Combine(workingDirectory, String.Format("{0}-{1}.xml", "T4aExport", 0));

            using (FileStream exportStream = new FileStream(sourceFilePath, FileMode.Create))
            {
                output.Serialize(exportStream, export);
                exportStream.Close();
            }

            ImportExportXmlSummary xsl = XmlAccess.GetImportExportXmlSummary(user, "T4Export")[0]; //NOTE:  use T4Export here because this object holds the whole CRA schema //**HARDCODE**//
            String outputFilePath = Path.Combine(workingDirectory, String.Format("{0}-{1}.xml", "T4aExport", 1));

            TransformXml(xsl, sourceFilePath, outputFilePath);
            rtn = File.ReadAllBytes(outputFilePath);

            //update the year_end table with a timestamp of when the file was created
            ReportManagement.UpdateYearEndTable(user, Convert.ToInt64(year), "T4aExport");

            return rtn;
        }
        public ImportExportXmlSummary GetImportExportXmlSummary(DatabaseUser user, String label)
        {
            return XmlAccess.GetImportExportXmlSummary(user, label)[0];
        }
        public Employer GetEmployer(DatabaseUser user, BusinessNumber businessNumber, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2)
        {
            Employer employer = new Employer();
            employer.Name = businessNumber.EmployerName;
            employer.Address = AddressAccess.GetAddress(user, businessNumber.EmployerAddressId)[0];
            employer.ProprietorSocialInsuranceNumber1 = proprietorSocialInsuranceNumber1;
            employer.ProprietorSocialInsuranceNumber2 = proprietorSocialInsuranceNumber2;
            employer.RevenueQuebecTaxId = businessNumber.RevenuQuebecBusinessTaxNumber;

            return employer;
        }
        public RevenueQuebecR1ExportCollection ExportR1Details(DatabaseUser user, String year, String exportType)
        {
            //load year end
            YearEnd yearEnd = YearEndAccess.GetYearEnd(user, Convert.ToDecimal(year))[0];

            //load t4export
            RevenueQuebecR1ExportCollection exportCollection = YearEndAccess.GetRevenueQuebecR1Export(user, yearEnd.YearEndId, exportType);

            if (exportCollection.Count > 0)
            {
                exportCollection[0].YearEnd = yearEnd;

                //load RevenueQuebecTransmitterRecord
                exportCollection[0].RevenueQuebecTransmitterRecord = YearEndAccess.GetRevenueQuebecTransmitterRecord(user, exportCollection[0].RevenueQuebecTransmitterRecordId)[0];
                exportCollection[0].RevenueQuebecTransmitterRecord.Person = PersonAccess.GetPerson(user, exportCollection[0].RevenueQuebecTransmitterRecord.TransmitterPersonId)[0];
                PersonContactChannelCollection transmitterPhones = PersonManagement.GetPersonContactChannel(user, exportCollection[0].RevenueQuebecTransmitterRecord.TransmitterPersonId, "Phone");
                exportCollection[0].RevenueQuebecTransmitterRecord.Phone = transmitterPhones.PrimaryContactChannel;
                PersonContactChannelCollection transmitterEmails = PersonManagement.GetPersonContactChannel(user, exportCollection[0].RevenueQuebecTransmitterRecord.TransmitterPersonId, "Email");
                exportCollection[0].RevenueQuebecTransmitterRecord.Email = transmitterEmails.PrimaryContactChannel;
                PersonAddressCollection transmitterAddresses = PersonAccess.GetPersonAddress(user, exportCollection[0].RevenueQuebecTransmitterRecord.TransmitterPersonId);
                exportCollection[0].RevenueQuebecTransmitterRecord.PersonAddress = transmitterAddresses.PrimaryAddress;

                //format postal/zip code for GUI viewing
                PersonManagement.FormatPostalZipCode(exportCollection[0].RevenueQuebecTransmitterRecord.PersonAddress);
            }

            return exportCollection;
        }
        public RevenueQuebecR1Export InsertExportR1Details(DatabaseUser user, RevenueQuebecR1Export export)
        {
            try
            {
                //insert into Person, Address, PersonAddress, ContactChannel, PersonContactChannel, RevenueQuebecTransmitterRecord, RevenueQuebecR1Export
                using (TransactionScope scope = new TransactionScope())
                {
                    //insert Person
                    PersonAccess.InsertPerson(user, user.DatabaseName, export.RevenueQuebecTransmitterRecord.Person);

                    //insert Address
                    AddressAccess.InsertAddress(user, export.RevenueQuebecTransmitterRecord.PersonAddress);

                    //insert PersonAddress
                    export.RevenueQuebecTransmitterRecord.PersonAddress.PersonId = export.RevenueQuebecTransmitterRecord.Person.PersonId;
                    //format the postal/zip code
                    PersonManagement.CleanUpPostalZipCode(export.RevenueQuebecTransmitterRecord.PersonAddress);
                    PersonAccess.InsertPersonAddress(user, export.RevenueQuebecTransmitterRecord.PersonAddress);

                    //insert into ContactChannel - Phone info
                    PersonAccess.InsertContactChannel(user, export.RevenueQuebecTransmitterRecord.Phone);

                    //insert into PersonContactChannel
                    export.RevenueQuebecTransmitterRecord.Phone.PersonId = export.RevenueQuebecTransmitterRecord.Person.PersonId;
                    PersonAccess.InsertPersonContactChannel(user, export.RevenueQuebecTransmitterRecord.Phone);

                    //insert into ContactChannel - Email info
                    PersonAccess.InsertContactChannel(user, export.RevenueQuebecTransmitterRecord.Email);

                    //insert into PersonContactChannel
                    export.RevenueQuebecTransmitterRecord.Email.PersonId = export.RevenueQuebecTransmitterRecord.Person.PersonId;
                    PersonAccess.InsertPersonContactChannel(user, export.RevenueQuebecTransmitterRecord.Email);

                    //insert into CanadaRevenueAgencyT619
                    export.RevenueQuebecTransmitterRecord.TransmitterPersonId = export.RevenueQuebecTransmitterRecord.Person.PersonId;
                    YearEndAccess.InsertRevenueQuebecTransmitterRecord(user, export.RevenueQuebecTransmitterRecord);

                    //insert into CanadaRevenueAgencyT4Export
                    export.RevenueQuebecTransmitterRecordId = export.RevenueQuebecTransmitterRecord.RevenueQuebecTransmitterRecordId;
                    YearEndAccess.InsertRevenueQuebecR1Export(user, export);

                    scope.Complete();
                }

                //format postal/zip code for GUI viewing
                PersonManagement.FormatPostalZipCode(export.RevenueQuebecTransmitterRecord.PersonAddress);

                return export;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "XmlManagement - InsertExportR1Details", ex);
                throw;
            }
        }
        public RevenueQuebecR1Export UpdateExportR1Details(DatabaseUser user, RevenueQuebecR1Export export)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //update RevenueQuebecR1Export data (RevenueQuebecTypeOfPackageCode)
                    YearEndAccess.UpdateRevenueQuebecR1Export(user, export);

                    //update RevenueQuebecTransmitterRecord data (transmitter_number, transmitter_name, certification_number)
                    YearEndAccess.UpdateRevenueQuebecTransmitterRecord(user, export.RevenueQuebecTransmitterRecord);

                    //update person (contact first name, last name)
                    PersonAccess.UpdatePerson(user, export.RevenueQuebecTransmitterRecord.Person);

                    //update PersonContactChannel (phone, extension)
                    PersonAccess.UpdateContactChannel(user, export.RevenueQuebecTransmitterRecord.Phone);

                    //update PersonContactChannel (email)
                    PersonAccess.UpdateContactChannel(user, export.RevenueQuebecTransmitterRecord.Email);

                    //update PersonAddress (address line 1, address line 2, city, country, province/state, postal/zip)
                    //format the postal/zip code
                    PersonManagement.CleanUpPostalZipCode(export.RevenueQuebecTransmitterRecord.PersonAddress);
                    AddressAccess.UpdateAddress(user, export.RevenueQuebecTransmitterRecord.PersonAddress);

                    scope.Complete();
                }

                //format postal/zip code for GUI viewing
                PersonManagement.FormatPostalZipCode(export.RevenueQuebecTransmitterRecord.PersonAddress);

                return export;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "XmlManagement - UpdateExportR1Details", ex);

                if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                {
                    EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                    throw new System.ServiceModel.FaultException<EmployeeServiceException>(concurrencyException, new System.ServiceModel.FaultReason(ex.Message));
                }
                else
                    throw;
            }
        }
        public byte[] ExportR2XmlFile(DatabaseUser user, int year, String importExportProcessingDirectory, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2, String revenueQuebecTaxId)
        {
            //load business numbers so we can use the address
            BusinessNumberCollection businessNumbers = EmployeeAccess.GetBusinessNumber(user, null);

            Dictionary<String, byte[]> r2Exports = new Dictionary<String, byte[]>();

            byte[] output = null;

            //load original r2's (if any exist)
            RevenueQuebecR2Collection r2s = new RevenueQuebecR2Collection(YearEndAccess.GetYearEndR2(user, year, true));

            //load amended r2's (if any exist)
            RevenueQuebecR2Collection amendedR2s = new RevenueQuebecR2Collection(YearEndAccess.GetYearEndR2(user, year, false, true));

            if (r2s.Count > 0)
            {
                output = ExportR2XmlFile(user, true, year, r2s, businessNumbers[0], importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, revenueQuebecTaxId);

                if (output != null) r2Exports.Add(String.Format("{0}_{1}_{2}_Originals.xml", year, revenueQuebecTaxId, "R2"), output);
            }
            if (amendedR2s.Count > 0)
            {
                output = ExportR2XmlFile(user, false, year, amendedR2s, businessNumbers[0], importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, revenueQuebecTaxId);

                if (output != null) r2Exports.Add(String.Format("{0}_{1}_{2}_Amended.xml", year, revenueQuebecTaxId, "R2"), output);
            }

            return ReportManagement.CreateZip(user, r2Exports);
        }
        public byte[] ExportR2XmlFile(DatabaseUser user, bool originals, int year, RevenueQuebecR2Collection r2s, BusinessNumber businessNumber, String importExportProcessingDirectory, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2, String revenueQuebecTaxId)
        {
            byte[] rtn = null;

            //load year end
            YearEnd yearEnd = YearEndAccess.GetYearEnd(user, year)[0];

            //load r1export
            RevenueQuebecR1Export export = YearEndAccess.GetRevenueQuebecR1Export(user, yearEnd.YearEndId, "R2")[0];
            export.YearEnd = yearEnd;

            //if originals, set the package code to 1, otherwise 4 for amendments
            export.RevenueQuebecTypeOfPackageCode = (originals) ? "1" : "4";

            //load employer
            export.Employer = GetEmployer(user, businessNumber, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2);

            //load t619
            export.RevenueQuebecTransmitterRecord = YearEndAccess.GetRevenueQuebecTransmitterRecord(user, export.RevenueQuebecTransmitterRecordId)[0];
            export.RevenueQuebecTransmitterRecord.Person = PersonAccess.GetPerson(user, export.RevenueQuebecTransmitterRecord.TransmitterPersonId)[0];
            PersonContactChannelCollection transmitterPhones = PersonManagement.GetPersonContactChannel(user, export.RevenueQuebecTransmitterRecord.TransmitterPersonId, "Phone");
            export.RevenueQuebecTransmitterRecord.Phone = transmitterPhones.PrimaryContactChannel;
            PersonContactChannelCollection transmitterEmails = PersonManagement.GetPersonContactChannel(user, export.RevenueQuebecTransmitterRecord.TransmitterPersonId, "Email");
            export.RevenueQuebecTransmitterRecord.Email = transmitterEmails.PrimaryContactChannel;
            PersonAddressCollection transmitterAddresses = PersonAccess.GetPersonAddress(user, export.RevenueQuebecTransmitterRecord.TransmitterPersonId);
            export.RevenueQuebecTransmitterRecord.PersonAddress = transmitterAddresses.PrimaryAddress;

            //load r2's
            export.R2s = r2s;

            foreach (RevenueQuebecR2 r2 in export.R2s)
            {
                r2.Employee = EmployeeAccess.GetEmployee(user, r2.EmployeeId, true)[0];
                PersonAccess.GetPerson(user, r2.Employee.PersonId)[0].CopyTo(r2.Employee);
                PersonAddressCollection addresses = PersonAccess.GetPersonAddress(user, r2.Employee.PersonId);
                r2.Address = addresses.PrimaryAddress;
                PersonContactChannelCollection phones = PersonManagement.GetPersonContactChannel(user, r2.Employee.PersonId, "Phone");
                r2.Phone = phones.PrimaryContactChannel;
                r2.BusinessNumber = businessNumber;
            }

            XmlSerializer output = new XmlSerializer(export.GetType());

            //create working directory
            String workingDirectory = CreateDirectory(user.DatabaseName, importExportProcessingDirectory);
            String sourceFilePath = Path.Combine(workingDirectory, String.Format("{0}-{1}.xml", "R2Export", 0));

            using (FileStream exportStream = new FileStream(sourceFilePath, FileMode.Create))
            {
                output.Serialize(exportStream, export);
                exportStream.Close();
            }

            ImportExportXmlSummary xsl = XmlAccess.GetImportExportXmlSummary(user, "R1Export")[0]; //**HARDCODE**//  //the r2 and r1 share the same xml schema from revenu quebec
            String outputFilePath = Path.Combine(workingDirectory, String.Format("{0}-{1}.xml", "R2Export", 1));

            TransformXml(xsl, sourceFilePath, outputFilePath);
            rtn = File.ReadAllBytes(outputFilePath);

            //update the year_end table with a timestamp of when the file was created
            ReportManagement.UpdateYearEndTable(user, Convert.ToInt64(year), "R2Export");

            return rtn;
        }
        public byte[] ExportR1XmlFile(DatabaseUser user, int year, String importExportProcessingDirectory, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2, String revenueQuebecTaxId)
        {
            //load business numbers so we can use the address
            BusinessNumberCollection businessNumbers = EmployeeAccess.GetBusinessNumber(user, null);

            Dictionary<String, byte[]> r1Exports = new Dictionary<String, byte[]>();

            byte[] output = null;

            //load original r1's (if any exist)
            RevenueQuebecR1Collection r1s = new RevenueQuebecR1Collection(YearEndAccess.GetYearEndR1(user, year, true));

            //load amended r1's (if any exist)
            RevenueQuebecR1Collection amendedR1s = new RevenueQuebecR1Collection(YearEndAccess.GetYearEndR1(user, year, false, true));

            if (r1s.Count > 0)
            {
                output = ExportR1XmlFile(user, true, year, r1s, businessNumbers[0], importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, revenueQuebecTaxId);

                if (output != null) r1Exports.Add(String.Format("{0}_{1}_{2}_Originals.xml", year, revenueQuebecTaxId, "R1"), output);
            }
            if (amendedR1s.Count > 0)
            {
                output = ExportR1XmlFile(user, false, year, amendedR1s, businessNumbers[0], importExportProcessingDirectory, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, revenueQuebecTaxId);

                if (output != null) r1Exports.Add(String.Format("{0}_{1}_{2}_Amended.xml", year, revenueQuebecTaxId, "R1"), output);
            }

            return ReportManagement.CreateZip(user, r1Exports);
        }
        public byte[] ExportR1XmlFile(DatabaseUser user, bool originals, int year, RevenueQuebecR1Collection r1s, BusinessNumber businessNumber, String importExportProcessingDirectory, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2, String revenueQuebecTaxId)
        {
            byte[] rtn = null;

            //load year end
            YearEnd yearEnd = YearEndAccess.GetYearEnd(user, year)[0];

            //load r1export
            RevenueQuebecR1Export export = YearEndAccess.GetRevenueQuebecR1Export(user, yearEnd.YearEndId, "R1")[0];
            export.YearEnd = yearEnd;

            //if originals, set the package code to 1, otherwise 4 for amendments
            export.RevenueQuebecTypeOfPackageCode = (originals) ? "1" : "4";

            //load employer
            export.Employer = GetEmployer(user, businessNumber, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2);

            //load t619
            export.RevenueQuebecTransmitterRecord = YearEndAccess.GetRevenueQuebecTransmitterRecord(user, export.RevenueQuebecTransmitterRecordId)[0];
            export.RevenueQuebecTransmitterRecord.Person = PersonAccess.GetPerson(user, export.RevenueQuebecTransmitterRecord.TransmitterPersonId)[0];
            PersonContactChannelCollection transmitterPhones = PersonManagement.GetPersonContactChannel(user, export.RevenueQuebecTransmitterRecord.TransmitterPersonId, "Phone");
            export.RevenueQuebecTransmitterRecord.Phone = transmitterPhones.PrimaryContactChannel;
            PersonContactChannelCollection transmitterEmails = PersonManagement.GetPersonContactChannel(user, export.RevenueQuebecTransmitterRecord.TransmitterPersonId, "Email");
            export.RevenueQuebecTransmitterRecord.Email = transmitterEmails.PrimaryContactChannel;
            PersonAddressCollection transmitterAddresses = PersonAccess.GetPersonAddress(user, export.RevenueQuebecTransmitterRecord.TransmitterPersonId);
            export.RevenueQuebecTransmitterRecord.PersonAddress = transmitterAddresses.PrimaryAddress;

            //load r1's
            export.R1s = r1s;

            foreach (RevenueQuebecR1 r1 in export.R1s)
            {
                r1.Employee = EmployeeAccess.GetEmployee(user, r1.EmployeeId, true)[0];
                PersonAccess.GetPerson(user, r1.Employee.PersonId)[0].CopyTo(r1.Employee);
                PersonAddressCollection addresses = PersonAccess.GetPersonAddress(user, r1.Employee.PersonId);
                r1.Address = addresses.PrimaryAddress;
                PersonContactChannelCollection phones = PersonManagement.GetPersonContactChannel(user, r1.Employee.PersonId, "Phone");
                r1.Phone = phones.PrimaryContactChannel;
                r1.BusinessNumber = businessNumber;
            }

            XmlSerializer output = new XmlSerializer(export.GetType());

            //create working directory
            String workingDirectory = CreateDirectory(user.DatabaseName, importExportProcessingDirectory);
            String sourceFilePath = Path.Combine(workingDirectory, String.Format("{0}-{1}.xml", "R1Export", 0));

            using (FileStream exportStream = new FileStream(sourceFilePath, FileMode.Create))
            {
                output.Serialize(exportStream, export);
                exportStream.Close();
            }

            ImportExportXmlSummary xsl = XmlAccess.GetImportExportXmlSummary(user, "R1Export")[0]; //**HARDCODE**//
            String outputFilePath = Path.Combine(workingDirectory, String.Format("{0}-{1}.xml", "R1Export", 1));

            TransformXml(xsl, sourceFilePath, outputFilePath);
            rtn = File.ReadAllBytes(outputFilePath);

            //update the year_end table with a timestamp of when the file was created
            ReportManagement.UpdateYearEndTable(user, Convert.ToInt64(year), "R1Export");

            return rtn;
        }
        #endregion

        #region CIC T4/T4A/R1 export
        public byte[] ExportCICR1File(DatabaseUser user, int year, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2, string revenueQuebecTaxId)
        {
            BusinessNumberCollection businessNumbers = EmployeeAccess.GetBusinessNumber(user, null);
            Dictionary<String, byte[]> r1Exports = new Dictionary<String, byte[]>();

            byte[] output = null;

            //load original r1's (if any exist)
            RevenueQuebecR1Collection r1s = new RevenueQuebecR1Collection(YearEndAccess.GetYearEndR1(user, year, true));
            //load amended r1's (if any exist)
            RevenueQuebecR1Collection amendedR1s = new RevenueQuebecR1Collection(YearEndAccess.GetYearEndR1(user, year, false, true));

            if (r1s.Count > 0)
            {
                output = GetEmployeeDataForCICR1(user, year, r1s, businessNumbers[0], proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, revenueQuebecTaxId);
                if (output != null)
                    r1Exports.Add(String.Format("{0}_{1}_{2}_Originals.txt", year, revenueQuebecTaxId, "R1"), output);
            }
            if (amendedR1s.Count > 0)
            {
                output = GetEmployeeDataForCICR1(user, year, amendedR1s, businessNumbers[0], proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2, revenueQuebecTaxId);
                if (output != null)
                    r1Exports.Add(String.Format("{0}_{1}_{2}_Amended.txt", year, revenueQuebecTaxId, "R1"), output);
            }

            return ReportManagement.CreateZip(user, r1Exports);
        }
        public byte[] GetEmployeeDataForCICR1(DatabaseUser user, int year, RevenueQuebecR1Collection r1s, BusinessNumber businessNumber, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2, String revenueQuebecTaxId)
        {
            //load year end
            YearEnd yearEnd = YearEndAccess.GetYearEnd(user, year)[0];

            //load r1export
            RevenueQuebecR1Export export = YearEndAccess.GetRevenueQuebecR1Export(user, yearEnd.YearEndId, "R1")[0];
            export.YearEnd = yearEnd;

            //load employer
            export.Employer = GetEmployer(user, businessNumber, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2);

            //load t619
            export.RevenueQuebecTransmitterRecord = YearEndAccess.GetRevenueQuebecTransmitterRecord(user, export.RevenueQuebecTransmitterRecordId)[0];
            export.RevenueQuebecTransmitterRecord.Person = PersonAccess.GetPerson(user, export.RevenueQuebecTransmitterRecord.TransmitterPersonId)[0];
            PersonContactChannelCollection transmitterPhones = PersonManagement.GetPersonContactChannel(user, export.RevenueQuebecTransmitterRecord.TransmitterPersonId, "Phone");
            export.RevenueQuebecTransmitterRecord.Phone = transmitterPhones.PrimaryContactChannel;
            PersonContactChannelCollection transmitterEmails = PersonManagement.GetPersonContactChannel(user, export.RevenueQuebecTransmitterRecord.TransmitterPersonId, "Email");
            export.RevenueQuebecTransmitterRecord.Email = transmitterEmails.PrimaryContactChannel;
            PersonAddressCollection transmitterAddresses = PersonAccess.GetPersonAddress(user, export.RevenueQuebecTransmitterRecord.TransmitterPersonId);
            export.RevenueQuebecTransmitterRecord.PersonAddress = transmitterAddresses.PrimaryAddress;

            //load r1's
            export.R1s = r1s;

            foreach (RevenueQuebecR1 r1 in export.R1s)
            {
                r1.Employee = EmployeeAccess.GetEmployee(user, r1.EmployeeId, true)[0];
                r1.Employee.CurrentEmployeePosition = EmployeeAccess.GetEmployeePosition(user, new EmployeePositionCriteria() { EmployeeId = r1.EmployeeId, GetCurrentPositionFlag = true })[0];
                PersonAccess.GetPerson(user, r1.Employee.PersonId, true)[0].CopyTo(r1.Employee);
                PersonAddressCollection addresses = PersonAccess.GetPersonAddress(user, r1.Employee.PersonId, true);
                r1.Address = addresses.PrimaryAddress;
                PersonContactChannelCollection phones = PersonManagement.GetPersonContactChannel(user, r1.Employee.PersonId, "Phone");
                r1.Phone = phones.PrimaryContactChannel;
                r1.BusinessNumber = businessNumber;
            }

            return new DataLayer.DataAccess.Dynamics.DynamicsAccess().CreateR1CICExportFile(export);
        }
        public byte[] ExportCICT4File(DatabaseUser user, int year, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2)
        {
            //load all the business numbers
            BusinessNumberCollection businessNumbers = EmployeeAccess.GetBusinessNumber(user, null);
            Dictionary<String, byte[]> t4Exports = new Dictionary<String, byte[]>();

            byte[] output = null;

            foreach (BusinessNumber businessNumber in businessNumbers)
            {
                //load original t4a's (if any exist)
                CanadaRevenueAgencyT4Collection t4s = new CanadaRevenueAgencyT4Collection(YearEndAccess.GetYearEndT4(user, year, businessNumber.BusinessNumberId, true));
                //load amended t4a's (if any exist)
                CanadaRevenueAgencyT4Collection amendedT4s = new CanadaRevenueAgencyT4Collection(YearEndAccess.GetYearEndT4(user, year, businessNumber.BusinessNumberId, false, true));

                if (t4s.Count > 0)
                {
                    output = GetEmployeeDataForCICT4(user, year, t4s, businessNumber, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2);
                    if (output != null)
                        t4Exports.Add(String.Format("{0}_{1}_{2}_Originals.txt", year, businessNumber.EmployerNumber, "T4"), output);
                }
                if (amendedT4s.Count > 0)
                {
                    output = GetEmployeeDataForCICT4(user, year, amendedT4s, businessNumber, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2);
                    if (output != null)
                        t4Exports.Add(String.Format("{0}_{1}_{2}_Amended.txt", year, businessNumber.EmployerNumber, "T4"), output);
                }
            }

            return ReportManagement.CreateZip(user, t4Exports);
        }
        private byte[] GetEmployeeDataForCICT4(DatabaseUser user, int year, CanadaRevenueAgencyT4Collection t4s, BusinessNumber businessNumber, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2)
        {
            //load year end
            YearEnd yearEnd = YearEndAccess.GetYearEnd(user, year)[0];

            //load t4export
            CanadaRevenueAgencyT4Export export = YearEndAccess.GetCanadaRevenueAgencyT4Export(user, yearEnd.YearEndId, "T4")[0];
            export.YearEnd = yearEnd;

            //load employer
            export.Employer = GetEmployer(user, businessNumber, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2);

            //load t619
            export.CanadaRevenueAgencyT619 = YearEndAccess.GetCanadaRevenueAgencyT619(user, export.CanadaRevenueAgencyT619Id)[0];
            export.CanadaRevenueAgencyT619.Person = PersonAccess.GetPerson(user, export.CanadaRevenueAgencyT619.TransmitterPersonId)[0];
            PersonContactChannelCollection transmitterPhones = PersonManagement.GetPersonContactChannel(user, export.CanadaRevenueAgencyT619.TransmitterPersonId, "Phone");
            export.CanadaRevenueAgencyT619.Phone = transmitterPhones.PrimaryContactChannel;
            PersonContactChannelCollection transmitterEmails = PersonManagement.GetPersonContactChannel(user, export.CanadaRevenueAgencyT619.TransmitterPersonId, "Email");
            export.CanadaRevenueAgencyT619.Email = transmitterEmails.PrimaryContactChannel;
            PersonAddressCollection transmitterAddresses = PersonAccess.GetPersonAddress(user, export.CanadaRevenueAgencyT619.TransmitterPersonId);
            export.CanadaRevenueAgencyT619.PersonAddress = transmitterAddresses.PrimaryAddress;

            //assign t4's
            export.T4s = t4s;

            foreach (CanadaRevenueAgencyT4 t4 in export.T4s)
            {
                t4.Employee = EmployeeAccess.GetEmployee(user, t4.EmployeeId, true)[0];
                t4.Employee.CurrentEmployeePosition = EmployeeAccess.GetEmployeePosition(user, new EmployeePositionCriteria() { EmployeeId = t4.EmployeeId, GetCurrentPositionFlag = true })[0];
                PersonAccess.GetPerson(user, t4.Employee.PersonId, true)[0].CopyTo(t4.Employee);
                PersonAddressCollection addresses = PersonAccess.GetPersonAddress(user, t4.Employee.PersonId, true);
                t4.Address = addresses.PrimaryAddress;
                PersonContactChannelCollection phones = PersonManagement.GetPersonContactChannel(user, t4.Employee.PersonId, "Phone");
                t4.Phone = phones.PrimaryContactChannel;
                t4.BusinessNumber = businessNumber;
            }

            return new DataLayer.DataAccess.Dynamics.DynamicsAccess().CreateT4CICExportFile(export);
        }
        public byte[] ExportCICT4aFile(DatabaseUser user, int year, string proprietorSocialInsuranceNumber1, string proprietorSocialInsuranceNumber2)
        {
            //load all the business numbers
            BusinessNumberCollection businessNumbers = EmployeeAccess.GetBusinessNumber(user, null);
            Dictionary<String, byte[]> t4aExports = new Dictionary<String, byte[]>();

            byte[] output = null;

            foreach (BusinessNumber businessNumber in businessNumbers)
            {
                //load original t4a's (if any exist)
                CanadaRevenueAgencyT4aCollection t4as = new CanadaRevenueAgencyT4aCollection(YearEndAccess.GetYearEndT4a(user, year, businessNumber.BusinessNumberId, true));
                //load amended t4a's (if any exist)
                CanadaRevenueAgencyT4aCollection amendedT4as = new CanadaRevenueAgencyT4aCollection(YearEndAccess.GetYearEndT4a(user, year, businessNumber.BusinessNumberId, false, true));

                if (t4as.Count > 0)
                {
                    output = GetEmployeeDataForCICT4a(user, year, t4as, businessNumber, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2);
                    if (output != null)
                        t4aExports.Add(String.Format("{0}_{1}_{2}_Originals.txt", year, businessNumber.EmployerNumber, "T4A"), output);
                }
                if (amendedT4as.Count > 0)
                {
                    output = GetEmployeeDataForCICT4a(user, year, amendedT4as, businessNumber, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2);
                    if (output != null)
                        t4aExports.Add(String.Format("{0}_{1}_{2}_Amended.txt", year, businessNumber.EmployerNumber, "T4A"), output);
                }
            }

            return ReportManagement.CreateZip(user, t4aExports);
        }
        private byte[] GetEmployeeDataForCICT4a(DatabaseUser user, int year, CanadaRevenueAgencyT4aCollection t4as, BusinessNumber businessNumber, String proprietorSocialInsuranceNumber1, String proprietorSocialInsuranceNumber2)
        {
            //load year end
            YearEnd yearEnd = YearEndAccess.GetYearEnd(user, year)[0];

            //load t4export
            CanadaRevenueAgencyT4Export export = YearEndAccess.GetCanadaRevenueAgencyT4Export(user, yearEnd.YearEndId, "T4A")[0];
            export.YearEnd = yearEnd;

            //load employer
            export.Employer = GetEmployer(user, businessNumber, proprietorSocialInsuranceNumber1, proprietorSocialInsuranceNumber2);

            //load t619
            export.CanadaRevenueAgencyT619 = YearEndAccess.GetCanadaRevenueAgencyT619(user, export.CanadaRevenueAgencyT619Id)[0];
            export.CanadaRevenueAgencyT619.Person = PersonAccess.GetPerson(user, export.CanadaRevenueAgencyT619.TransmitterPersonId)[0];
            PersonContactChannelCollection transmitterPhones = PersonManagement.GetPersonContactChannel(user, export.CanadaRevenueAgencyT619.TransmitterPersonId, "Phone");
            export.CanadaRevenueAgencyT619.Phone = transmitterPhones.PrimaryContactChannel;
            PersonContactChannelCollection transmitterEmails = PersonManagement.GetPersonContactChannel(user, export.CanadaRevenueAgencyT619.TransmitterPersonId, "Email");
            export.CanadaRevenueAgencyT619.Email = transmitterEmails.PrimaryContactChannel;
            PersonAddressCollection transmitterAddresses = PersonAccess.GetPersonAddress(user, export.CanadaRevenueAgencyT619.TransmitterPersonId);
            export.CanadaRevenueAgencyT619.PersonAddress = transmitterAddresses.PrimaryAddress;

            //assign t4a's
            export.T4as = t4as;

            foreach (CanadaRevenueAgencyT4a t4a in export.T4as)
            {
                t4a.Employee = EmployeeAccess.GetEmployee(user, t4a.EmployeeId, true)[0];
                t4a.Employee.CurrentEmployeePosition = EmployeeAccess.GetEmployeePosition(user, new EmployeePositionCriteria() { EmployeeId = t4a.EmployeeId, GetCurrentPositionFlag = true })[0];
                PersonAccess.GetPerson(user, t4a.Employee.PersonId, true)[0].CopyTo(t4a.Employee);
                PersonAddressCollection addresses = PersonAccess.GetPersonAddress(user, t4a.Employee.PersonId, true);
                t4a.Address = addresses.PrimaryAddress;
                PersonContactChannelCollection phones = PersonManagement.GetPersonContactChannel(user, t4a.Employee.PersonId, "Phone");
                t4a.Phone = phones.PrimaryContactChannel;
                t4a.BusinessNumber = businessNumber;
            }

            return new DataLayer.DataAccess.Dynamics.DynamicsAccess().CreateT4aCICExportFile(export);
        }
        #endregion

        //used to name export files
        public static String GenerateFileName(String fileMask, EmployeeSummary employee, PayrollProcess payrollProcess, int sequenceNumber)
        {
            return String.Format(fileMask, employee.EmployeeId, //{0)
                                           employee.EmployeeNumber, //{1}
                                           employee.ImportExternalIdentifier, //{2}
                                           payrollProcess.PeriodYear, //{3}
                                           payrollProcess.Period, //{4}
                                           payrollProcess.PayrollProcessGroupCode, //{5}
                                           DateTime.Now, //{6}
                                           payrollProcess.CutoffDate, //{7}
                                           payrollProcess.ChequeDate, //{8}
                                           payrollProcess.ProcessedDate, //{9}
                                           sequenceNumber, //{10}
                                           payrollProcess.PayrollProcessId //{11}
                                           );
        }

        #region Advantage Time File FTP import

        public void CopyImportMoveAdvantageTimeFiles(DatabaseUser user, string advantageFtpName, string filePatternToMatch, string filePatternToMatchExtension, string importName, string importExportProcessingDirectory, bool importDirtySave, string folderToMoveProcessedFiles)
        {
            //get ftp protocol details
            ExportFtp exportFtp = new PayrollManagement().GetExportFtp(user, null, null, advantageFtpName)[0]; //only will be 1 row

            FTP.SftpManagement ftpClass = new FTP.SftpManagement();

            //download the time files from FTP if they exist
            SortedDictionary<string, byte[]> ftpFiles = ftpClass.DownloadSftpFile(exportFtp.UriName, exportFtp.Port, exportFtp.Directory, exportFtp.UnixPathFlag, exportFtp.Login, exportFtp.Password, filePatternToMatch, filePatternToMatchExtension);

            if (ftpFiles.Count > 0)
            {
                //get importExportId of the import
                long importExportId = (XmlAccess.GetImportExport(user, null, importName)[0]).ImportExportId;

                foreach (KeyValuePair<string, byte[]> file in ftpFiles)
                {
                    //import the file
                    ImportFile(user, importExportId, file.Value, file.Key, importExportProcessingDirectory, null, null, importDirtySave, false, null, null, false);

                    //move the file on the sftp server to a processed directory
                    ftpClass.MoveSftpFile(exportFtp.UriName, exportFtp.Port, exportFtp.Directory, exportFtp.UnixPathFlag, exportFtp.Login, exportFtp.Password, file.Key, folderToMoveProcessedFiles);
                }
            }
        }

        #endregion
    }

    internal class StreamConverter
    {
        readonly DateTime _excelMinDate = new DateTime(1899, 12, 31);
        private static readonly Regex RxQuote = new Regex("\"", RegexOptions.Compiled);

        /// <summary>
        /// Checks the beginning of the UTF-8 or UTF-16 stream for a BOM
        /// </summary>
        /// <param name="sourceStream">Stream to check</param>
        /// <returns>A new BOM-less stream should the BOM exist, the original stream if not</returns>
        public Stream RemoveByteOrderMark(Stream sourceStream)
        {
            byte[] preamble = Encoding.UTF8.GetPreamble();
            byte[] buffer = new byte[preamble.Length];
            sourceStream.Read(buffer, 0, preamble.Length);

            if (StartsWith(buffer, preamble))
            {
                buffer = new byte[16 * 1024];
                MemoryStream ms = new MemoryStream();

                int read;
                while ((read = sourceStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                ms.Flush();
                ms.Position = 0;

                return ms;
            }

            sourceStream.Position = 0;
            return sourceStream;
        }

        private static bool StartsWith(IReadOnlyList<byte> thisArray, IReadOnlyList<byte> otherArray)
        {
            if (otherArray.Count > thisArray.Count) return false;

            for (int i = 0; i < otherArray.Count; i++)
            {
                if (thisArray[i] != otherArray[i])
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Converts the passed Excel <paramref name="sourceStream"/> to a csv stream
        /// </summary>
        /// <param name="sourceStream">Assumed to be .xls or .xlsx stream</param>
        /// <param name="includeByteOrderMark">Optional parameter to idicated whether to include a BOM or not</param>
        /// <returns>A stream containing csv content, converted from the Excel source</returns>
        public Stream ConvertExcelToCsv(Stream sourceStream, bool includeByteOrderMark = false)
        {
            IWorkbook workbook = WorkbookFactory.Create(sourceStream, ImportOption.All);
            ICreationHelper helper = workbook.GetCreationHelper();
            IFormulaEvaluator eval = helper.CreateFormulaEvaluator();
            DataFormatter formatter = new DataFormatter();
            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = helper.CreateDataFormat().GetFormat("yyyy-MM-dd");

            MemoryStream csv = new MemoryStream();
            StreamWriter writer = new StreamWriter(csv, new UTF8Encoding(includeByteOrderMark), 4098, true); // Do not dispose of this, it disposes the stream as well, let the GC pick it up on .Close()

            ISheet sheet = workbook.GetSheetAt(0);
            int numberOfRows = sheet.LastRowNum;

            for (int i = 0; i <= numberOfRows; i++)
            {
                IRow row = sheet.GetRow(i);

                if (row != null)
                {
                    int lastCellNumber = row.LastCellNum;

                    for (int j = 0; j < lastCellNumber; j++)
                    {
                        ICell cell = row.GetCell(j, MissingCellPolicy.RETURN_BLANK_AS_NULL);

                        // Tack on a comma if this is not the first cell
                        if (j > 0) writer.Write(',');

                        if (cell != null)
                        {
                            // This is Excel remember...and we are talking dates! Read Joel Spolsky's blog on that debacle
                            // So, if this is a numeric cell that has a date format applied...
                            if (cell.CellType == CellType.Numeric && DateUtil.IsCellDateFormatted(cell))
                            {
                                DateTime dateVal = cell.DateCellValue;
                                if (dateVal.Date != _excelMinDate)
                                {
                                    cell.CellStyle = dateStyle;
                                }
                            }
                            string value = formatter.FormatCellValue(cell, eval);
                            writer.Write(EncodeValue(value));
                        }
                    }

                    // Close off the line/row
                    writer.WriteLine();
                }

                writer.Flush();
            }

            workbook.Close();

            if (csv.CanSeek) csv.Seek(0, SeekOrigin.Begin);

            return csv;
        }

        private static string EncodeValue(string value)
        {
            bool needQuotes = value.IndexOf(',') != -1 ||
                              value.IndexOf('"') != -1 ||
                              value.IndexOf('\n') != -1 ||
                              value.IndexOf('\r') != -1;

            needQuotes |= RxQuote.Match(value).Success;
            value = RxQuote.Replace(value, "\"\"");
            return needQuotes ? "\"" + value + "\"" : value;
        }
    }

}