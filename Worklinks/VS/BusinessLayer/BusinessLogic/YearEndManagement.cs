﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Transactions;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;
using WorkLinks.DataLayer.DataAccess;
using WorkLinks.DataLayer.DataAccess.SqlServer;

namespace WorkLinks.BusinessLayer.BusinessLogic
{
    public class YearEndManagement
    {
        #region fields
        private YearEndAccess _yearEndAccess = null;
        private EmployeeManagement _employeeManagement = null;
        private ApplicationResourceManagement _guiManagement = null;
        private CodeManagement _codeManagement;

        #endregion

        #region properties
        private YearEndAccess YearEndAccess
        {
            get
            {
                if (_yearEndAccess == null)
                    _yearEndAccess = new YearEndAccess();

                return _yearEndAccess;
            }
        }
        private EmployeeManagement EmployeeManagement
        {
            get
            {
                if (_employeeManagement == null)
                    _employeeManagement = new EmployeeManagement();

                return _employeeManagement;
            }
        }
        private ApplicationResourceManagement GuiManagement
        {
            get
            {
                if (_guiManagement == null)
                    _guiManagement = new ApplicationResourceManagement();

                return _guiManagement;
            }
        }
        private CodeManagement CodeManagement => _codeManagement ?? (_codeManagement = new CodeManagement());

        #endregion

        #region year end adjustment import
        public void ProcessYearEndAdjustment(DatabaseUser user, WorkLinksYearEndAdjustment[] xsdObject, ImportExportLog log, bool automaticImportFlag)
        {
            //load collection
            WorkLinksYearEndAdjustmentCollection collection = new WorkLinksYearEndAdjustmentCollection();
            Dictionary<String, EmployeeSummary> employeeCache = new Dictionary<String, EmployeeSummary>();
            System.Text.StringBuilder output = new System.Text.StringBuilder();
            YearEndAdjustmentTableCollection tables = GetYearEndAdjustmentTables(user);
            YearEndCollection yearEndCollection = YearEndAccess.GetYearEnd(user, null, true);

            if (yearEndCollection.Count == 0 || yearEndCollection[0].CodeYearEndStatusCd.Equals("CMP"))
            {
                log.SuccessFlag = false;
                log.ProcessingOutput += "Cannot import. Current year end is an invalid state.\r\n";
            }
            else
            {
                YearEnd yearEnd = yearEndCollection[0];

                foreach (WorkLinksYearEndAdjustment adjustment in xsdObject)
                {
                    //validate current year

                    //get employee id
                    output.Append(EmployeeManagement.SetEmployeeIdFromIdentifier(user, adjustment, employeeCache));

                    //get table id
                    adjustment.YearEndTableId = tables.GetYearEndAdjustmentTableIdFromIdentifer(adjustment.FormType);

                    collection.Add(adjustment);
                }

                using (TransactionScope scope = new TransactionScope())
                {
                    //this method determines if we can update or insert or throw an error                    
                    InsertOrUpdateYearEndAdjustment(user, collection, yearEnd.CurrentRevision, yearEnd.Year);

                    //validate collection
                    KeyValuePair<int, String> warning = collection.GetValidationInformationWarning();
                    if (warning.Key > 0)
                    {
                        log.ProcessingOutput += warning;
                        log.WarningFlag = true;
                    }

                    if (collection.GetValidationInformationFail(yearEnd.Year) == null)
                    {
                        log.ProcessingOutput += GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n";
                        log.SuccessFlag = true;
                        scope.Complete();
                    }
                    else
                    {
                        log.SuccessFlag = false;
                        log.ProcessingOutput += GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n" + "----------\r\n";
                        log.ProcessingOutput += collection.GetValidationInformationFail(yearEnd.Year);
                    }
                }
            }
        }
        public void InsertOrUpdateYearEndAdjustment(DatabaseUser user, WorkLinksYearEndAdjustmentCollection yearEndAdjustmentCollection, int yearEndRevision, decimal year)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    int index = -1;

                    foreach (WorkLinksYearEndAdjustment yearEndAdjust in yearEndAdjustmentCollection)
                    {
                        //get existing record for the current year end revision
                        GetCurrentRecord(user, yearEndAdjust, index--, yearEndRevision);

                        //apply min/max rules
                        // Commenting out the min max logic, we will store the data as is, formatting
                        // the raw value when exporting/displaying
                        //yearEndAdjust.FixMinMax();

                        //if found and no errors, update it
                        if (yearEndAdjust.ErrorMessage(yearEndAdjust.Year) == null)
                            YearEndAccess.UpdateYearEndAdjustment(user, yearEndAdjust);
                        //if no record was found and yearEndRevision > 0, then we need to Add a revision before updating unless the record was a T4 with a missing province field, thats an import file error
                        else if (yearEndAdjust.NoExistingRecordFlag && yearEndRevision != 0 && !(yearEndAdjust.FormType == "T4" && String.IsNullOrWhiteSpace(yearEndAdjust.ProvinceStateCode)))
                            CreateNewRowAndUpdate(user, yearEndAdjust, yearEndRevision, year, index);
                        //if yearEndRevision = 0 and NoExistingRecordFlag was true, continue on and show the error to the screen
                    }

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "YearEndManagement - GetYearEndAdjustmentWithListParameter", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }

        private void CreateNewRowAndUpdate(DatabaseUser user, WorkLinksYearEndAdjustment yearEndAdjust, int yearEndRevision, decimal year, int index)
        {
            //call a proc which will get most recent record for employee
            YearEndAdjustmentsSearchCriteria criteria = new YearEndAdjustmentsSearchCriteria()
            {
                Year = year.ToString(),
                EmployeeNumber = yearEndAdjust.EmployeeImportExternalIdentifier,
                FormType = yearEndAdjust.FormType,
                GetMaxRevision = true
            };
            YearEndAdjustmentsSearchReportCollection coll = EmployeeManagement.GetYearEndAdjustmentsSearchReport(user, criteria);

            if (coll.Count == 1)
            {
                //keep the prev max revision
                int prevRevision = coll[0].Revision;

                //assign new revision to the max record and insert it with mostly 0s.
                coll[0].Revision = yearEndRevision;

                //insert new, mostly empty record, 
                coll[0] = InsertYearEndAdjustment(user, coll[0], Convert.ToInt64(coll[0].KeyId.ToString().Substring(0, coll[0].KeyId.ToString().Length - 1)));

                //get new key from new inserted row
                string key = coll[0].KeyId.ToString().Substring(0, coll[0].KeyId.ToString().Length - 1);//remove the last digit as it is corresponding to the form type

                //get all the data for the previous row
                YearEndAdjustmentCollection adjustmentCollection = GetYearEndAdjustment(user, yearEndAdjust.EmployeeId, yearEndAdjust.Year, yearEndAdjust.YearEndTableId ?? -1, yearEndAdjust.BusinessNumber, yearEndAdjust.ProvinceStateCode, prevRevision, null);

                //update old data with key of the new row
                foreach (YearEndAdjustment item in adjustmentCollection)
                    item.Id = Convert.ToInt64(key);

                //update the new row in the db so it's now a copy of the previous one
                UpdateYearEndAdjustment(user, adjustmentCollection);

                //now update the new row with the imported data, by getting the new row and updating it
                GetCurrentRecord(user, yearEndAdjust, index, yearEndRevision);
                YearEndAccess.UpdateYearEndAdjustment(user, yearEndAdjust);

                //reset so no error shows on the import screen
                yearEndAdjust.NoExistingRecordFlag = false;
            }
            else if (coll.Count > 1)
            {
                throw new Exception(String.Format($"More than one record returned for employee:{criteria.EmployeeNumber}, year:{year}, form:{yearEndAdjust.FormType}, revision:{coll[0].Revision}"));
            }
            else
            {
                //we cant find a record
                yearEndAdjust.ColumnName = index.ToString();
                yearEndAdjust.NoExistingRecordFlag = true;
            }
        }

        private void GetCurrentRecord(DatabaseUser user, WorkLinksYearEndAdjustment adjustment, int index, int yearEndRevision)
        {
            //If yearEndRevision = 0 or yearEndRevision = the max revision for the the employee, we will get the data from the db
            YearEndAdjustmentCollection adjustmentCollection = GetYearEndAdjustment(user, adjustment.EmployeeId, adjustment.Year, adjustment.YearEndTableId ?? -1, adjustment.BusinessNumber, adjustment.ProvinceStateCode, yearEndRevision, adjustment.WorkLinksBoxNumber);

            if (adjustmentCollection.Count == 1)
                adjustment.CopyFromAndPreValidate(adjustmentCollection[0]);
            else if (adjustmentCollection.Count > 1)
            {
                throw new Exception(String.Format("More than one record returned for employee:{0}, year:{1}, table:{2}, businessNumber:{3}, province:{4}, box:{5}", adjustment.EmployeeId, adjustment.Year, adjustment.TableName, adjustment.BusinessNumber,
                adjustment.ProvinceStateCode, adjustment.WorkLinksBoxNumber));
            }
            else
            {
                adjustment.ColumnName = index.ToString();
                adjustment.NoExistingRecordFlag = true;
            }
        }
        #endregion

        #region year end paycode import

        public void ProcessYearEndAdjustmentPaycode(DatabaseUser user, WorkLinksYearEndAdjustmentPaycode[] xsdObject, ImportExportLog log, bool importDirtySave)
        {
            WorkLinksYearEndAdjustmentPaycodeCollection collection = new WorkLinksYearEndAdjustmentPaycodeCollection();
            Dictionary<string, EmployeeSummary> employeeCache = new Dictionary<string, EmployeeSummary>();
            Dictionary<string, string> paycodeCache = new Dictionary<string, string>();
            System.Text.StringBuilder output = new System.Text.StringBuilder();
            YearEndCollection yearEndCollection = YearEndAccess.GetYearEnd(user, null, true);

            if (yearEndCollection.Count == 0)
            {
                log.SuccessFlag = false;
                log.ProcessingOutput += $"Cannot import. No current year end.{Environment.NewLine}";
            }

            YearEnd yearEnd = yearEndCollection[0];

            foreach (WorkLinksYearEndAdjustmentPaycode paycode in xsdObject)
            {
                // Set ImportExportLogId
                paycode.ImportExportLogId = log.ImportExportLogId;

                // get employee id
                output.Append(EmployeeManagement.SetEmployeeIdFromIdentifier(user, paycode, employeeCache));

                // get business number
                paycode.BusinessNumberId = EmployeeManagement.GetBusinessNumberIdFromEmployerNumber(user, paycode.EmployerExternalIdentifier);

                // get ProvinceStateCodeExternalIdentifier
                paycode.ProvinceStateCode = CodeManagement.DecodeCode(user, CodeTableType.ProvinceStateCode, paycode.ProvinceStateCodeExternalIdentifier);

                // get PaycodeCodeExternalIdentifier
                paycode.PaycodeCode = EmployeeManagement.GetPaycodeCodeFromIdentifier(user, paycode.PaycodeCodeExternalIdentifier, paycodeCache);

                collection.Add(paycode);
            }

            using (TransactionScope scope = new TransactionScope())
            {
                //this method determines if we can update or insert or throw an error                    
                InsertYearEndAdjustmentCodes(user, collection);

                //validate collection
                if (collection.GetValidationInformationFail(yearEnd.Year) == null)
                {
                    log.ProcessingOutput += GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n";
                    log.SuccessFlag = true;
                    scope.Complete();
                }
                else
                {
                    log.SuccessFlag = false;
                    log.ProcessingOutput += GuiManagement.GetResourceByLanguageAndKey(user, "Messages", "ImportComplete") + "\r\n" + "----------\r\n";
                    log.ProcessingOutput += collection.GetValidationInformationFail(yearEnd.Year);
                }
            }
        }

        private void InsertYearEndAdjustmentCodes(DatabaseUser user, WorkLinksYearEndAdjustmentPaycodeCollection collection)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    YearEndAccess.InsertYearEndAdjustmentCodes(user, collection);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "YearEndManagement - GetYearEndAdjustmentWithListParameter", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }

        #endregion

        #region year end adjustment
        public YearEndAdjustmentCollection GetYearEndAdjustment(DatabaseUser user, long employeeId, Decimal year, long yearEndAdjustmentTableId, String employerNumber, String provinceStateCode, long revision, String columnImportIdentifier)
        {
            try
            {
                return YearEndAccess.GetYearEndAdjustment(user, employeeId, year, yearEndAdjustmentTableId, employerNumber, provinceStateCode, true, revision, columnImportIdentifier);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "YearEndManagement - GetYearEndAdjustment", ex);
                throw;
            }
        }

        public void UpdateYearEndAdjustment(DatabaseUser user, YearEndAdjustmentCollection yearEndAdjustmentCollection)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (YearEndAdjustment yearEndAdjust in yearEndAdjustmentCollection)
                        YearEndAccess.UpdateYearEndAdjustment(user, yearEndAdjust);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "YearEndManagement - GetYearEndAdjustment", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        public YearEndAdjustmentsSearchReport InsertYearEndAdjustment(DatabaseUser user, YearEndAdjustmentsSearchReport report, long? previousKeyId = null)
        {
            try
            {
                long tableKey = -1;

                using (TransactionScope scope = new TransactionScope())
                {
                    if (report.YearEndAdjustmentTableId == 1) //insert into T4 table
                    {
                        YearEndT4 yearEndT4Obj = new YearEndT4
                        {
                            Year = report.Year,
                            EmployeeId = report.EmployeeId,
                            TaxableCodeProvinceStateCd = report.TaxableCodeProvinceStateCd,
                            EmployerNumber = report.EmployerNumber,
                            Revision = report.Revision,
                            PreviousRevisionYearEndT4Id = previousKeyId,
                            CreateUser = report.CreateUser
                        };

                        tableKey = YearEndAccess.InsertYearEndT4(user, yearEndT4Obj).YearEndT4Id;
                    }
                    else if (report.YearEndAdjustmentTableId == 2) //insert into T4A table
                    {
                        YearEndT4a yearEndT4aObj = new YearEndT4a
                        {
                            Year = report.Year,
                            EmployeeId = report.EmployeeId,
                            EmployerNumber = report.EmployerNumber,
                            Revision = report.Revision,
                            PreviousRevisionYearEndT4aId = previousKeyId,
                            CreateUser = report.CreateUser
                        };

                        tableKey = YearEndAccess.InsertYearEndT4a(user, yearEndT4aObj).YearEndT4aId;
                    }
                    else if (report.YearEndAdjustmentTableId == 3) //insert into R1 table
                    {
                        YearEndR1 yearEndR1Obj = new YearEndR1
                        {
                            Year = report.Year,
                            EmployeeId = report.EmployeeId,
                            Revision = report.Revision,
                            PreviousRevisionYearEndR1Id = previousKeyId,
                            CreateUser = report.CreateUser
                        };

                        tableKey = YearEndAccess.InsertYearEndR1(user, yearEndR1Obj).YearEndR1Id;
                    }
                    else if (report.YearEndAdjustmentTableId == 4) //insert into R2 table
                    {
                        YearEndR2 yearEndR2Obj = new YearEndR2
                        {
                            Year = report.Year,
                            EmployeeId = report.EmployeeId,
                            Revision = report.Revision,
                            PreviousRevisionYearEndR2Id = previousKeyId,
                            CreateUser = report.CreateUser
                        };

                        tableKey = YearEndAccess.InsertYearEndR2(user, yearEndR2Obj).YearEndR2Id;
                    }
                    else if (report.YearEndAdjustmentTableId == 5) //insert into T4RSP table
                    {
                        YearEndT4rsp yearEndT4rspObj = new YearEndT4rsp
                        {
                            Year = report.Year,
                            EmployeeId = report.EmployeeId,
                            EmployerNumber = report.EmployerNumber,
                            Revision = report.Revision,
                            PreviousRevisionYearEndT4rspId = previousKeyId,
                            CreateUser = report.CreateUser
                        };

                        tableKey = YearEndAccess.InsertYearEndT4rsp(user, yearEndT4rspObj).YearEndT4rspId;
                    }
                    else if (report.YearEndAdjustmentTableId == 6) //insert into NR4 table
                    {
                        YearEndNR4rsp yearEndNR4rspObj = new YearEndNR4rsp
                        {
                            Year = report.Year,
                            EmployeeId = report.EmployeeId,
                            EmployerNumber = report.EmployerNumber,
                            Revision = report.Revision,
                            PreviousRevisionYearEndNR4rspId = previousKeyId,
                            CreateUser = report.CreateUser
                        };

                        tableKey = YearEndAccess.InsertYearEndNR4rsp(user, yearEndNR4rspObj).YearEndNR4rspId;
                    }
                    else if (report.YearEndAdjustmentTableId == 7) //insert into T4ARCA table
                    {
                        YearEndT4arca yearEndT4arcaObj = new YearEndT4arca
                        {
                            Year = report.Year,
                            EmployeeId = report.EmployeeId,
                            EmployerNumber = report.EmployerNumber,
                            Revision = report.Revision,
                            PreviousRevisionYearEndT4arcaId = previousKeyId,
                            CreateUser = report.CreateUser
                        };

                        tableKey = YearEndAccess.InsertYearEndT4arca(user, yearEndT4arcaObj).YearEndT4arcaId;
                    }

                    scope.Complete();
                }

                if (report.YearEndAdjustmentTableId == 1) //insert was into T4 table
                {
                    report.YearEndTypeForm = "T4";
                    report.KeyId = Convert.ToInt64(Convert.ToString(tableKey + "1"));
                }
                else if (report.YearEndAdjustmentTableId == 2) //insert was into T4A table
                {
                    report.YearEndTypeForm = "T4A";
                    report.KeyId = Convert.ToInt64(Convert.ToString(tableKey + "2"));
                }
                else if (report.YearEndAdjustmentTableId == 3) //insert was into R1 table
                {
                    report.YearEndTypeForm = "R1";
                    report.KeyId = Convert.ToInt64(Convert.ToString(tableKey + "3"));
                }
                else if (report.YearEndAdjustmentTableId == 4) //insert was into R2 table
                {
                    report.YearEndTypeForm = "R2";
                    report.KeyId = Convert.ToInt64(Convert.ToString(tableKey + "4"));
                }
                else if (report.YearEndAdjustmentTableId == 5) //insert was into T4RSP table
                {
                    report.YearEndTypeForm = "T4RSP";
                    report.KeyId = Convert.ToInt64(Convert.ToString(tableKey + "5"));
                }
                else if (report.YearEndAdjustmentTableId == 6) //insert was into NR4 table
                {
                    report.YearEndTypeForm = "NR4";
                    report.KeyId = Convert.ToInt64(Convert.ToString(tableKey + "6"));
                }
                else if (report.YearEndAdjustmentTableId == 7) //insert was into T4RCA table
                {
                    report.YearEndTypeForm = "T4ARCA";
                    report.KeyId = Convert.ToInt64(Convert.ToString(tableKey + "7"));
                }

                return report;
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "YearEndManagement - InsertYearEndAdjustment", ex);
                throw;
            }
        }
        public YearEndAdjustmentTableCollection GetYearEndAdjustmentTables(DatabaseUser user)
        {
            try
            {
                return YearEndAccess.GetYearEndAdjustmentTables(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "YearEndManagement - GetYearEndAdjustmentTables", ex);
                throw;
            }
        }
        public void DeleteYearEndReport(DatabaseUser user, YearEndAdjustmentsSearchReport reportObj)
        {
            try
            {
                String key = reportObj.Key.Substring(0, reportObj.Key.Length - 1);
                String reportType = reportObj.Key.Substring(reportObj.Key.Length - 1); //the report type is appended to the key in the REPORT proc (1="T4", 2="T4a", 3="R1") to keep keys unique in the collection

                using (TransactionScope scope = new TransactionScope())
                {
                    //get the year end adjustment data for this record
                    YearEndAdjustmentCollection yearEndAdjustment = new YearEndAdjustmentCollection();
                    yearEndAdjustment = YearEndAccess.GetYearEndAdjustment(user, reportObj.EmployeeId, reportObj.Year, Convert.ToInt64(reportType), reportObj.EmployerNumber, reportObj.TaxableCodeProvinceStateCd, null, reportObj.Revision, null);

                    //insert a copy of this data into the audit table
                    foreach (YearEndAdjustment obj in yearEndAdjustment)
                    {
                        if (obj.ColumnName != "row_version")
                            _yearEndAccess.InsertIntoYearEndAdjustmentAudit(user, obj, true);
                    }

                    //this is where we will now call an update and set active_flag to be 0 rather than delete by id...
                    if (reportType == "1")
                        YearEndAccess.UpdateYearEndT4(user, Convert.ToInt64(key), false);
                    else if (reportType == "2")
                        YearEndAccess.UpdateYearEndT4a(user, Convert.ToInt64(key), false);
                    else if (reportType == "3")
                        YearEndAccess.UpdateYearEndR1(user, Convert.ToInt64(key), false);
                    else if (reportType == "4")
                        YearEndAccess.UpdateYearEndR2(user, Convert.ToInt64(key), false);
                    else if (reportType == "5")
                        YearEndAccess.UpdateYearEndT4rsp(user, Convert.ToInt64(key), false);
                    else if (reportType == "6")
                        YearEndAccess.UpdateYearEndNR4rsp(user, Convert.ToInt64(key), false);
                    else if (reportType == "7")
                        YearEndAccess.UpdateYearEndT4arca(user, Convert.ToInt64(key), false);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "YearEndManagement - DeleteYearEndReport", ex);

                if (ex is SqlServerException)
                {
                    if (((SqlException)((SqlServerException)ex).GetBaseException()).Number == 50010)
                    {
                        EmployeeServiceException concurrencyException = new EmployeeServiceException(EmployeeServiceException.ExceptionCodes.DataConcurrency);
                        throw new FaultException<EmployeeServiceException>(concurrencyException, new FaultReason(ex.Message));
                    }
                    else
                        throw;
                }
                else
                    throw;
            }
        }
        #endregion

        public YearEndT4Collection GetYearEndT4(DatabaseUser user, int year, long? businessNumberId)
        {
            try
            {
                return YearEndAccess.GetYearEndT4(user, year, businessNumberId);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "YearEndManagement - GetBatchYearEndT4", ex);
                throw;
            }
        }
        public YearEndT4Collection GetBatchYearEndT4(DatabaseUser user, string[] yearEndT4Ids)
        {
            try
            {
                return YearEndAccess.GetBatchYearEndT4(user, yearEndT4Ids);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "YearEndManagement - GetBatchYearEndT4", ex);
                throw;
            }
        }

        #region year end clone
        public YearEndT4 SelectSingleT4ByKey(DatabaseUser user, long key)
        {
            return YearEndAccess.SelectSingleT4ByKey(user, key)[0];
        }
        public YearEndT4a SelectSingleT4aByKey(DatabaseUser user, long key)
        {
            return YearEndAccess.SelectSingleT4aByKey(user, key)[0];
        }
        public YearEndR1 SelectSingleR1ByKey(DatabaseUser user, long key)
        {
            return YearEndAccess.SelectSingleR1ByKey(user, key)[0];
        }
        public YearEndR2 SelectSingleR2ByKey(DatabaseUser user, long key)
        {
            return YearEndAccess.SelectSingleR2ByKey(user, key)[0];
        }
        public YearEndT4rsp SelectSingleT4rspByKey(DatabaseUser user, long key)
        {
            return YearEndAccess.SelectSingleT4rspByKey(user, key)[0];
        }
        public YearEndNR4rsp SelectSingleNR4rspByKey(DatabaseUser user, long key)
        {
            return YearEndAccess.SelectSingleNR4rspByKey(user, key)[0];
        }
        public YearEndT4arca SelectSingleT4arcaByKey(DatabaseUser user, long key)
        {
            return YearEndAccess.SelectSingleT4arcaByKey(user, key)[0];
        }
        #endregion
    }
}