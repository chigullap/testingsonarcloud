﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CodeTableDescription: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {

        #region properties
        [DataMember]
        public string LanguageCode 
        {
            get { return (String)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string CodeTableDescCd { get; set; }
        [DataMember]
        public string TableDescription { get; set; }
        

        #endregion

        #region construct/destruct
        public CodeTableDescription()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            CodeTableDescription ctDesc = new CodeTableDescription();
            this.CopyTo(ctDesc);
            return ctDesc;
        }
        public void CopyTo(CodeTableDescription data)
        {
            base.CopyTo(data);

            data.Id = Id;
            data.CodeTableDescCd = CodeTableDescCd;
            data.LanguageCode = LanguageCode;
            data.TableDescription = TableDescription;
        }
        public new void Clear()
        {
            base.Clear();

            Id = -1;
            CodeTableDescCd = "";
            LanguageCode = "";
            TableDescription = "";
        }

        #endregion

    }
}
