﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SummaryDataForRoeInsert : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
         #region properties
        [DataMember]
        public DateTime? EffectiveDate 
        {
            get { return (DateTime?)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String CodeTerminationReasonCd { get; set; }
        [DataMember]
        public DateTime? rehireDate { get; set; }
        [DataMember]
        public DateTime? hireDate { get; set; }
        [DataMember]
        public DateTime? cutoffDate { get; set; }

        #endregion

        #region construct/destruct
        public SummaryDataForRoeInsert()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(SummaryDataForRoeInsert data)
        {
            base.CopyTo(data);

            data.EffectiveDate = EffectiveDate;
            data.CodeTerminationReasonCd = CodeTerminationReasonCd;
            data.rehireDate = rehireDate;
            data.hireDate = hireDate;
            data.cutoffDate = cutoffDate;
        }
        public new void Clear()
        {
            base.Clear();

            EffectiveDate = null;
            CodeTerminationReasonCd = null;
            rehireDate = null;
            hireDate = null;
            cutoffDate = null;
        }

        #endregion
    }
}
