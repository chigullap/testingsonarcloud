﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ChequeWcbRemittanceSummary : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public String CodeWcbCd
        {
            get { return (String)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public Decimal PaymentAmount { get; set; }

        [DataMember]
        public Decimal AssessableEarnings { get; set; }

        [DataMember]
        public DateTime RemittanceDate { get; set; }

        [DataMember]
        public string VendorName { get; set; }

        [DataMember]
        public string WorkersCompensationAccountNumber { get; set; }

        [DataMember]
        public string AddressLine1 { get; set; }

        [DataMember]
        public string AddressLine2 { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string CodeProvinceStateCd { get; set; }

        [DataMember]
        public string PostalZipCode { get; set; }

        [DataMember]
        public string CodeCountryCd { get; set; }

        [DataMember]
        public long? BusinessNumberId { get; set; }

        [DataMember]
        public string BusinessTaxNumber { get; set; }

        [DataMember]
        public string CodePreAuthorizedBankCd { get; set; }

        [DataMember]
        public string PreAuthorizedTransitNumber { get; set; }

        [DataMember]
        public string PreAuthorizedAccountNumber { get; set; }

        [DataMember]
        public DateTime? PaymentDueDate { get; set; }

        [DataMember]
        public long? ChequeWcbPreAuthorizedDebitExportId { get; set; }
        #endregion

        #region construct/destruct
        public ChequeWcbRemittanceSummary()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(ChequeWcbRemittanceSummary data)
        {
            base.CopyTo(data);

            data.CodeWcbCd = CodeWcbCd;
            data.PaymentAmount = PaymentAmount;
            data.AssessableEarnings = AssessableEarnings;
            data.RemittanceDate = RemittanceDate;
            data.VendorName = VendorName;
            data.WorkersCompensationAccountNumber = WorkersCompensationAccountNumber;
            data.AddressLine1 = AddressLine1;
            data.AddressLine2 = AddressLine2;
            data.City = City;
            data.CodeProvinceStateCd = CodeProvinceStateCd;
            data.PostalZipCode = PostalZipCode;
            data.CodeCountryCd = CodeCountryCd;
            data.BusinessNumberId = BusinessNumberId;
            data.BusinessTaxNumber = BusinessTaxNumber;
            data.CodePreAuthorizedBankCd = CodePreAuthorizedBankCd;
            data.PreAuthorizedTransitNumber = PreAuthorizedTransitNumber;
            data.PreAuthorizedAccountNumber = PreAuthorizedAccountNumber;
            data.PaymentDueDate = PaymentDueDate;
            data.ChequeWcbPreAuthorizedDebitExportId = ChequeWcbPreAuthorizedDebitExportId;
        }

        public new void Clear()
        {
            base.Clear();

            CodeWcbCd = null;
            PaymentAmount = 0;
            AssessableEarnings = 0;
            RemittanceDate = DateTime.Now;
            VendorName = null;
            WorkersCompensationAccountNumber = null;
            AddressLine1 = null;
            AddressLine2 = null;
            City = null;
            CodeProvinceStateCd = null;
            PostalZipCode = null;
            CodeCountryCd = null;
            BusinessNumberId = null;
            BusinessTaxNumber = null;
            CodePreAuthorizedBankCd = null;
            PreAuthorizedTransitNumber = null;
            PreAuthorizedAccountNumber = null;
            PaymentDueDate = null;
            ChequeWcbPreAuthorizedDebitExportId = null;
        }
        #endregion
    }
}
