﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PayrollMasterDetailCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollMasterDetail>
    {
        public PayrollMasterDetail GetPayrollMasterDetailByOrganizationUnit (String organizationUnit)
        {
            foreach (PayrollMasterDetail detail in this)
            {
                if (detail.OrganizationUnit== organizationUnit)
                {
                    return detail;
                }
            }

            return null;
        }
    }
}