﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollTransactionSummary : PayrollTransaction
    {
        #region properties
        [DataMember]
        public String EmployeeNumber { get; set; }

        [DataMember]
        public String PaycodeTypeCode { get; set; }

        [DataMember]
        public bool SubtractHourFromSalaryFlag { get; set; }

        [DataMember]
        public String EmployeeName { get; set; }
        
        [DataMember]
        public String OrganizationUnitDescription { get; set; }

        public Decimal OffsetHour { get; set; }

        [DataMember]
        public string PaycodeDescription { get; set; }

        [DataMember]
        public string PaycodeTypeDescription { get; set; }

        #endregion



        #region construct/destruct
        public PayrollTransactionSummary()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            PayrollTransactionSummary item = new PayrollTransactionSummary();
            this.CopyTo(item);

            return item;
        }
        public void CopyTo(PayrollTransactionSummary data)
        {
            base.CopyTo(data);

            data.EmployeeNumber = EmployeeNumber;
            data.PaycodeTypeCode = PaycodeTypeCode;
            data.SubtractHourFromSalaryFlag = SubtractHourFromSalaryFlag;
            data.EmployeeName = EmployeeName;
            data.OrganizationUnitDescription = OrganizationUnitDescription;
            data.OffsetHour = OffsetHour;
            data.PaycodeDescription = PaycodeDescription;
            data.PaycodeTypeDescription = PaycodeTypeDescription;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeeNumber = null;
            PaycodeTypeCode = null;
            SubtractHourFromSalaryFlag = false;
            EmployeeName = null;
            OrganizationUnitDescription = null;
            OffsetHour = 0;
            PaycodeDescription = null;
            PaycodeTypeDescription = null;
        }
        #endregion
    }
}