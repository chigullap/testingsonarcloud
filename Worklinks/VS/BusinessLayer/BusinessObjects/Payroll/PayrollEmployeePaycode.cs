﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects.Payroll
{
    public class PayrollEmployeePaycode : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {

        public override string Key
        {
            get
            {
                return EmployeePaycodeId.ToString();
            }
        }
        public long EmployeePaycodeId { get; set; }
        public long EmployeeId { get; set; }
        public String PaycodeCode { get; set; }
        public Decimal? AmountRate { get; set; }
        public Decimal? YearlyMaximum { get; set; }
        public Decimal? YearlyMinimumCalculationOffset { get; set; }
        public Decimal? AmountUnits { get; set; }
        public Decimal? AmountPercentage { get; set; }

        public override object Clone()
        {
            throw new NotImplementedException();
        }
    }
}
