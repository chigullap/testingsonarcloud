﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects.Payroll
{
    public class PayrollTransactionIncome : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {

        public override String Key { get { return PayrollTransactionId.ToString(); } }

        public long PayrollTransactionId { get; set; }
        public long EmployeeId { get; set; }
        public String PaycodeCode { get; set; }
        public Decimal? Units { get; set; }
        public Decimal? Rate { get; set; }
        public Decimal? FederalTaxAmount { get; set; }
        public Decimal? ProvincialTaxAmount { get; set; }

        public override object Clone()
        {
            throw new NotImplementedException();
        }
    }
}
