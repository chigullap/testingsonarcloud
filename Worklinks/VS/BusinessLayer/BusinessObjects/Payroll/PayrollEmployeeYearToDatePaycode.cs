﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects.Payroll
{
    public class PayrollEmployeeYearToDatePaycode : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {

        public override string Key
        {
            get
            {
                return String.Format("{0}|{1}",EmployeeId, PaycodeCode);
            }
        }
        public long EmployeeId { get; set; }
        public String PaycodeCode { get; set; }
        public Decimal Amount { get; set; }

        public override object Clone()
        {
            throw new NotImplementedException();
        }
    }
}
