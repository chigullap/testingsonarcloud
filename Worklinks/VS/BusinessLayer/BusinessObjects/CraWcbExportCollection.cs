﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class CraWcbExportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<CraWcbExport>
    {
    }
}
