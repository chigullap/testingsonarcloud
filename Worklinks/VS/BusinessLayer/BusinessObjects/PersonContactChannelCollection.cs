﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PersonContactChannelCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PersonContactChannel>
    {
        public PersonContactChannel PrimaryContactChannel
        {
            get
            {
                foreach (PersonContactChannel channel in this)
                    if (channel.PrimaryFlag)
                        return channel;

                return null;
            }
        }

        public PersonContactChannel GetPersonContactChannelByCode(String code)
        {
            foreach (PersonContactChannel channel in this)
                if (channel.ContactChannelTypeCode==code)
                    return channel;

            return null;
        }
    }

   

}
