﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PreAuthorizedDebitTotalsCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PreAuthorizedDebitTotals>
    {
        public PreAuthorizedDebitTotalsCollection GroupedAccounts
        {
            get
            {
                PreAuthorizedDebitTotalsCollection grouped = new PreAuthorizedDebitTotalsCollection();
                bool found = false;

                foreach (PreAuthorizedDebitTotals record in this)
                {
                    if (grouped.Count == 0) //add first record
                        grouped.Add((PreAuthorizedDebitTotals)record.Clone());
                    else //check if bank account already exists, if so add totals to prev record and shorten the bix tax num, otherwise add new record
                    {
                        for (int i = grouped.Count; i > 0; i--)
                        {
                            if (grouped[i - 1].CodePreAuthorizedBankCd == record.CodePreAuthorizedBankCd && grouped[i - 1].PreAuthorizedTransitNumber == record.PreAuthorizedTransitNumber && grouped[i - 1].PreAuthorizedAccountNumber == record.PreAuthorizedAccountNumber)
                            {
                                grouped[i - 1].BusinessTaxNumber = grouped[i - 1].BusinessTaxNumber.Substring(0, 9);    //strip the RPxxx as they are using the same bank account
                                grouped[i - 1].Amount += record.Amount;
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                            grouped.Add((PreAuthorizedDebitTotals)record.Clone()); //add to the end of the collection

                        //reset variable
                        found = false;
                    }
                }
                return grouped;
            }
        }

        public decimal TotalAmount
        {
            get
            {
                decimal rtn = 0;

                foreach (PreAuthorizedDebitTotals record in this)
                {
                    rtn += record.Amount;
                }

                return rtn;
            }
        }
    }
}