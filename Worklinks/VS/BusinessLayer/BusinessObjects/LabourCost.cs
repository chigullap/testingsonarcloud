﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class LabourCost : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long LabourCostId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public Int16 Month { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public Decimal Amount { get; set; }
        #endregion

        #region construct/destruct
        public LabourCost()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(LabourCost data)
        {
            base.CopyTo(data);
            data.LabourCostId = LabourCostId;
            data.Month = Month;
            data.Description = Description;
            data.Amount = Amount;
        }
        public new void Clear()
        {
            base.Clear();
            LabourCostId = -1;
            Month = 0;
            Description = null;
            Amount = 0;
        }
        #endregion
    }
}