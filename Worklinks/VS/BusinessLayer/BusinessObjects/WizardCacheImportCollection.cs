﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class WizardCacheImportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<WizardCacheImport>
    {
    }
}