﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public interface IEmployeeIdentifier
    {
        String ImportExternalIdentifier { get; set; }
        long EmployeeId { get; set; }
        String PayrollProcessGroupCode { get; set; }
        bool HasErrors { get; set; }
    }
}
