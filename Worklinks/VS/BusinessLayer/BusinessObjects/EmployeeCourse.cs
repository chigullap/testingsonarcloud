﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeCourse : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeCourseId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public String EmployeeCourseNameCode { get; set; }

        [DataMember]
        public DateTime? RegistrationDate { get; set; }

        [DataMember]
        public bool AttendedFlag { get; set; }

        [DataMember]
        public bool CertificateIssued { get; set; }

        [DataMember]
        public String Note { get; set; }

        [DataMember]
        public String EmployeeCourseTypeCode { get; set; }

        [DataMember]
        public Decimal Duration { get; set; }

        [DataMember]
        public Decimal Mark { get; set; }

        [DataMember]
        public Decimal EmployeeTuitionAmount { get; set; }

        [DataMember]
        public Decimal EmployerTuitionAmount { get; set; }

        [DataMember]
        public Decimal EmployeeOtherAmount { get; set; }

        [DataMember]
        public Decimal EmployerOtherAmount { get; set; }

        [DataMember]
        public DateTime? CourseExpirationDate { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? CompletionDate { get; set; }

        [DataMember]
        public bool CompletedFlag { get; set; }

        [DataMember]
        public bool JobRequiredFlag { get; set; }

        [DataMember]
        public String AlternateAttendant { get; set; }

        [DataMember]
        public String EmployeeCourseResultCode { get; set; }

        [DataMember]
        public String EmployeeCoursePenaltyCode { get; set; }

        [DataMember]
        public long? AttachmentId { get; set; }

        [DataMember]
        public AttachmentCollection AttachmentObjectCollection { get; set; }

        //public property
        public String Description
        {
            get
            {
                if (AttachmentObjectCollection != null && AttachmentObjectCollection.Count > 0)
                    return AttachmentObjectCollection[0].Description;
                else
                    return null;
            }
        }
        #endregion

        #region construct/destruct
        public EmployeeCourse()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeeCourse data)
        {
            base.CopyTo(data);
            data.EmployeeCourseId = EmployeeCourseId;
            data.EmployeeId = EmployeeId;
            data.EmployeeCourseNameCode = EmployeeCourseNameCode;
            data.RegistrationDate = RegistrationDate;
            data.AttendedFlag = AttendedFlag;
            data.CertificateIssued = CertificateIssued;
            data.Note = Note;
            data.EmployeeCourseTypeCode = EmployeeCourseTypeCode;
            data.Duration = Duration;
            data.Mark = Mark;
            data.EmployeeTuitionAmount = EmployeeTuitionAmount;
            data.EmployerTuitionAmount = EmployerTuitionAmount;
            data.EmployeeOtherAmount = EmployeeOtherAmount;
            data.EmployerOtherAmount = EmployerOtherAmount;
            data.CourseExpirationDate = CourseExpirationDate;
            data.StartDate = StartDate;
            data.CompletionDate = CompletionDate;
            data.CompletedFlag = CompletedFlag;
            data.JobRequiredFlag = JobRequiredFlag;
            data.AlternateAttendant = AlternateAttendant;
            data.EmployeeCourseResultCode = EmployeeCourseResultCode;
            data.EmployeeCoursePenaltyCode = EmployeeCoursePenaltyCode;
            data.AttachmentId = AttachmentId;

            //attachments
            if (AttachmentObjectCollection != null)
            {
                data.AttachmentObjectCollection = new AttachmentCollection();
                AttachmentObjectCollection.CopyTo(data.AttachmentObjectCollection);
            }
            else
                data.AttachmentObjectCollection = AttachmentObjectCollection;
        }
        public new void Clear()
        {
            base.Clear();
            EmployeeCourseId = -1;
            EmployeeId = -1;
            EmployeeCourseNameCode = null;
            RegistrationDate = null;
            AttendedFlag = false;
            CertificateIssued = false;
            Note = null;
            EmployeeCourseTypeCode = null;
            Duration = 0;
            Mark = 0;
            EmployeeTuitionAmount = 0;
            EmployerTuitionAmount = 0;
            EmployeeOtherAmount = 0;
            EmployerOtherAmount = 0;
            CourseExpirationDate = null;
            StartDate = null;
            CompletionDate = null;
            CompletedFlag = false;
            JobRequiredFlag = false;
            AlternateAttendant = null;
            EmployeeCourseResultCode = null;
            EmployeeCoursePenaltyCode = null;
            AttachmentId = -1;
            AttachmentObjectCollection = null;
        }
        #endregion
    }
}