﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CodeWsib : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public string WsibCode
        {
            get { return (string)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public string EnglishDescription { get; set; }

        [DataMember]
        public string FrenchDescription { get; set; }

        [DataMember]
        public string ImportExternalIdentifier { get; set; }

        [DataMember]
        public string WorkersCompensationAccountNumber { get; set; }

        [DataMember]
        public string ProvinceStateCode { get; set; }

        [DataMember]
        public string PaycodeCode { get; set; }

        [DataMember]
        public decimal SortOrder { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public bool SystemFlag { get; set; }

        [DataMember]
        public bool ExcludeFromRemittanceExportFlag { get; set; }

        [DataMember]
        public bool GenerateChequeForRemittanceFlag { get; set; }

        [DataMember]
        public string CodeWcbChequeRemittanceFrequencyCd { get; set; }

        [DataMember]
        public long? VendorId { get; set; }

        [DataMember]
        public long? BusinessNumberId { get; set; }

        [DataMember]
        public bool HideInEmploymentScreenFlag { get; set; }        

        #endregion

        #region construct/destruct
        public CodeWsib()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(CodeWsib data)
        {
            base.CopyTo(data);

            data.WsibCode = WsibCode;
            data.EnglishDescription = EnglishDescription;
            data.FrenchDescription = FrenchDescription;
            data.ImportExternalIdentifier = ImportExternalIdentifier;
            data.WorkersCompensationAccountNumber = WorkersCompensationAccountNumber;
            data.ProvinceStateCode = ProvinceStateCode;
            data.PaycodeCode = PaycodeCode;
            data.SortOrder = SortOrder;
            data.ActiveFlag = ActiveFlag;
            data.SystemFlag = SystemFlag;
            data.ExcludeFromRemittanceExportFlag = ExcludeFromRemittanceExportFlag;
            data.GenerateChequeForRemittanceFlag = GenerateChequeForRemittanceFlag;
            data.CodeWcbChequeRemittanceFrequencyCd = CodeWcbChequeRemittanceFrequencyCd;
            data.VendorId = VendorId;
            data.BusinessNumberId = BusinessNumberId;
            data.HideInEmploymentScreenFlag = HideInEmploymentScreenFlag;
        }
        public new void Clear()
        {
            base.Clear();

            WsibCode = "";
            EnglishDescription = null;
            FrenchDescription = null;
            ImportExternalIdentifier = null;
            WorkersCompensationAccountNumber = null;
            ProvinceStateCode = null;
            PaycodeCode = null;
            SortOrder = 1;
            ActiveFlag = false;
            SystemFlag = false;
            ExcludeFromRemittanceExportFlag = false;
            GenerateChequeForRemittanceFlag = false;
            CodeWcbChequeRemittanceFrequencyCd = null;
            VendorId = null;
            BusinessNumberId = null;
            HideInEmploymentScreenFlag = false;
        }
        #endregion
    }
}