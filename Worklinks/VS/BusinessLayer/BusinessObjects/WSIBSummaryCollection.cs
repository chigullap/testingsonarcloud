﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class WSIBSummaryCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<WSIBSummary>
    {

        public WSIBSummaryCollection GetWSIBSummaryByProvince(String provinceCode)
        {
            WSIBSummaryCollection rtn = new WSIBSummaryCollection();
            foreach (WSIBSummary summary in this)
            {
                if (summary.ProvinceStateCode.ToLower().Equals(provinceCode.ToLower()))
                {
                    rtn.Add(summary);
                }
            }

            return rtn;
        }
    }

}
