﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SalaryPlanGradeAndEffectiveDetail : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long SalaryPlanGradeEffectiveId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public DateTime EffectiveDate { get; set; }

        [DataMember]
        public DateTime? RetroactiveDate { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string EnglishDescription { get; set; }

        [DataMember]
        public string FrenchDescription { get; set; }

        [DataMember]
        public decimal? Step1 { get; set; }

        [DataMember]
        public decimal? Step2 { get; set; }

        [DataMember]
        public decimal? Step3 { get; set; }

        [DataMember]
        public long SalaryPlanId { get; set; }

        #endregion

        #region construct/destruct
        public SalaryPlanGradeAndEffectiveDetail()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(SalaryPlanGradeAndEffectiveDetail data)
        {
            base.CopyTo(data);

            data.SalaryPlanGradeEffectiveId = SalaryPlanGradeEffectiveId;
            data.EffectiveDate = EffectiveDate;
            data.RetroactiveDate = RetroactiveDate;
            data.Name = Name;
            data.EnglishDescription = EnglishDescription;
            data.FrenchDescription = FrenchDescription;
            data.Step1 = Step1;
            data.Step2 = Step2;
            data.Step3 = Step3;
            data.SalaryPlanId = SalaryPlanId;
        }
        public new void Clear()
        {
            base.Clear();

            SalaryPlanGradeEffectiveId = -1;
            EffectiveDate = DateTime.Now;
            RetroactiveDate = null;
            Name = null;
            EnglishDescription = null;
            FrenchDescription = null;
            Step1 = null;
            Step2 = null;
            Step3 = null;
            SalaryPlanId = -1;
    }
    #endregion
}
}