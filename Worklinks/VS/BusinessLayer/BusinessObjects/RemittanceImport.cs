﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class RemittanceImport : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long RemittanceImportId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String RemittanceImportStatusCode { get; set; }

        [DataMember]
        public long? ImportExportLogId { get; set; }

        [DataMember]
        public long? PayrollProcessId { get; set; }

        [DataMember]
        public RemittanceImportDetailCollection RemittanceImportDetail { get; set; }

        //for remittance import screen
        [DataMember]
        public DateTime ImportDate { get; set; }

        [DataMember]
        public String Notes { get; set; }

        [DataMember]
        public bool WarningFlag { get; set; }
        #endregion

        #region construct/destruct
        public RemittanceImport()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(RemittanceImport data)
        {
            base.CopyTo(data);

            data.RemittanceImportId = RemittanceImportId;
            data.RemittanceImportStatusCode = RemittanceImportStatusCode;
            data.ImportExportLogId = ImportExportLogId;
            data.PayrollProcessId = PayrollProcessId;
            data.RemittanceImportDetail = RemittanceImportDetail;

            //for remittance import screen
            data.ImportDate = ImportDate;
            data.Notes = Notes;
            data.WarningFlag = WarningFlag;
        }
        public new void Clear()
        {
            base.Clear();

            RemittanceImportId = -1;
            RemittanceImportStatusCode = null;
            ImportExportLogId = null;
            PayrollProcessId = null;
            RemittanceImportDetail = null;

            //for remittance import screen
            ImportDate = DateTime.Now;
            Notes = null;
            WarningFlag = false;
        }
        #endregion
    }
}