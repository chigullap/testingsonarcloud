﻿using System;
using System.Runtime.Serialization;
using WorkLinks.BusinessLayer.BusinessObjects.Import;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollBatchImport : PayrollBatch
    {
        #region properties
        public String PayrollBatchExternalIdentifier { get; set; }
        public String PayrollBatchEntryTypeCodeExternalIdentifier { get; set; }
        #endregion

        #region construct/destruct
        public PayrollBatchImport(Xsd.WorkLinksPayrollBatch item, String payrollProcessGroupCode)
            : this()
        {
            PayrollBatchExternalIdentifier = item.Body.PayrollBatch.PayrollBatchIdentifier;

            //PayrollBatchId
            //PayrollBatchTypeCode
            BatchSource = item.Body.PayrollBatch.BatchNumber ?? "AUTO GENERATED IMPORT BATCH";
            PayrollBatchEntryTypeCodeExternalIdentifier = item.Body.PayrollBatch.PayrollBatchEntryTypeCode.CodeIdentifier;
            //PayrollPeriodId =
            Description = item.Body.PayrollBatch.Description;
            //PayrollBatchStatusCode 
            PayrollProcessGroupCode = payrollProcessGroupCode;
            //PayrollProcessId =
            SystemGeneratedFlag = SystemGeneratedFlag;

            //defaults
            //approve batch
            PayrollBatchStatusCode = "AP";
        }

        public PayrollBatchImport(AdvantageTimeRecord item, String payrollProcessGroupCode)
            : this()
        {
            PayrollBatchExternalIdentifier = null;
            BatchSource = (item.ImportFileName.Length > 32) ? item.ImportFileName.Substring(0,32) : item.ImportFileName;
            PayrollBatchEntryTypeCodeExternalIdentifier = "REGULAR";
            Description = "Advantage Time File Import";
            PayrollProcessGroupCode = payrollProcessGroupCode;
            SystemGeneratedFlag = SystemGeneratedFlag;

            // John asked that the Status of Payroll Batch for Advantage Time File Import should be "Not Approved"
            PayrollBatchStatusCode = "UA";
        }

        public PayrollBatchImport()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PayrollBatchImport data)
        {
            base.CopyTo(data);
            data.PayrollBatchExternalIdentifier = PayrollBatchExternalIdentifier;
        }
        public new void Clear()
        {
            base.Clear();
            PayrollBatchExternalIdentifier = null;
        }
        #endregion
    }
}