﻿using System;
using System.Runtime.Serialization;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SalaryPlanGradeRuleCode : BusinessObject
    {
        public SalaryPlanGradeRuleCode()
        {
            SalaryPlanGradeRuleCodeId = -1;
        }

        [DataMember]
        public long SalaryPlanGradeRuleCodeId
        {
            get => (long) _Key;
            set => _Key = value;
        } 

        [DataMember]
        public long SalaryPlanGradeRuleId { get; set; }
        [DataMember]
        public string InputPayCode { get; set; }
        [DataMember]
        public string OutputPayCode { get; set; }
        
        #region Overrides of BusinessObject

        /// <summary>Creates a new object that is a copy of the current instance.</summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        protected override void Clear()
        {
            base.Clear();

            SalaryPlanGradeRuleCodeId = -1;
            SalaryPlanGradeRuleId = -1;
            InputPayCode = null;
            OutputPayCode = null;
        }

        #endregion
    }
}
