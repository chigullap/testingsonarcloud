﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class YearEndAdjustmentsSearchReport : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long KeyId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long YearEndAdjustmentTableId { get; set; }

        [DataMember]
        public String EmployeeNumber { get; set; }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public String LastName { get; set; }

        [DataMember]
        public String FirstName { get; set; }

        [DataMember]
        public Decimal Year { get; set; }

        [DataMember]
        public String YearEndTypeForm { get; set; }

        [DataMember]
        public String CodePayrollProcessGroupCd { get; set; }

        [DataMember]
        public String EmployerNumber { get; set; }

        [DataMember]
        public String TaxableCodeProvinceStateCd { get; set; }

        [DataMember]
        public int Revision { get; set; }

        [DataMember]
        public int CurrentRevision { get; set; }

        [DataMember]
        public int MaxRevision { get; set; }
        #endregion

        #region construct/destruct
        public YearEndAdjustmentsSearchReport()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(YearEndAdjustmentsSearchReport data)
        {
            base.CopyTo(data);

            data.KeyId = KeyId;
            data.YearEndAdjustmentTableId = YearEndAdjustmentTableId;
            data.EmployeeNumber = EmployeeNumber;
            data.EmployeeId = EmployeeId;
            data.LastName = LastName;
            data.FirstName = FirstName;
            data.Year = Year;
            data.YearEndTypeForm = YearEndTypeForm;
            data.CodePayrollProcessGroupCd = CodePayrollProcessGroupCd;
            data.EmployerNumber = EmployerNumber;
            data.TaxableCodeProvinceStateCd = TaxableCodeProvinceStateCd;
            data.Revision = Revision;
            data.CurrentRevision = CurrentRevision;
            data.MaxRevision = MaxRevision;
        }
        public new void Clear()
        {
            base.Clear();

            KeyId = -1;
            YearEndAdjustmentTableId = -1;
            EmployeeNumber = null;
            EmployeeId = -1;
            LastName = null;
            FirstName = null;
            Year = 0;
            YearEndTypeForm = null;
            CodePayrollProcessGroupCd = null;
            EmployerNumber = null;
            TaxableCodeProvinceStateCd = null;
            Revision = 0;
            CurrentRevision = 0;
            MaxRevision = 0;
        }
        #endregion
    }
}