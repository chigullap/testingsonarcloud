﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class TempBulkPayrollProcessCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<TempBulkPayrollProcess>
    {
    }
}