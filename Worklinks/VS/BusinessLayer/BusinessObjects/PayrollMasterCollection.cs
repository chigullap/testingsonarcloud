﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using WorkLinks.BusinessLayer.BusinessObjects.CalcModel;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PayrollMasterCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollMaster>
    {
        public List<PayrollMasterCollection> Chunks
        {
            get
            {
                List<PayrollMasterCollection> rtn = new List<PayrollMasterCollection>();
                int i = 0;
                foreach (PayrollMaster master in this)
                {
                    if (i++ % 20000 == 0)
                        rtn.Add(new PayrollMasterCollection());
                    rtn[rtn.Count - 1].Add(master);
                }

                return rtn;
            }
        }
        public PayrollMaster GetPayrollMasterByEmployeeId(long employeeId)
        {
            foreach (PayrollMaster master in this)
            {
                if (master.EmployeeId == employeeId)
                    return master;
            }
            return null;

        }

        public void LoadTransactions(PayrollTransactionPayrollMasterCollection transactions)
        {

            //associate transactions
            PayrollMaster currentMaster = null;
            foreach (PayrollTransactionPayrollMaster transaction in transactions)
            {
                if (currentMaster == null || currentMaster.EmployeeId != transaction.EmployeeId)
                    currentMaster = this.GetPayrollMasterByEmployeeId(transaction.EmployeeId);

                currentMaster.Transactions.Add(transaction);

            }

            int payrollMasterDetail = -1;
            //build detail
            foreach (PayrollMaster master in this)
            {
                master.CalculatePayrollMasterDetail(ref payrollMasterDetail);
            }

        }

        public PayrollMasterDetailCollection PayrollMasterDetails
        {
            get
            {
                PayrollMasterDetailCollection rtn = new PayrollMasterDetailCollection();
                foreach (PayrollMaster master in this)
                {
                    rtn.Add(master.Details);
                }

                return rtn;
            }
            
        }
    }
}