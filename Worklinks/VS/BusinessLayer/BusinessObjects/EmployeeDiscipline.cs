﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeDiscipline : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeDisciplineId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public long? ReportedByEmployeeId { get; set; }

        [DataMember]
        public string DisciplineCode { get; set; }

        [DataMember]
        public string DisciplineTypeCode { get; set; }

        [DataMember]
        public DateTime? DisciplineDate { get; set; }

        [DataMember]
        public DateTime? ReportedDate { get; set; }

        [DataMember]
        public DateTime? ExpiryDate { get; set; }

        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public string DisciplineResolutionCode { get; set; }

        [DataMember]
        public DateTime? ResolutionDate { get; set; }

        [DataMember]
        public string ResolutionNote { get; set; }
        #endregion

        #region construct/destruct
        public EmployeeDiscipline()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeeDiscipline data)
        {
            base.CopyTo(data);

            data.EmployeeDisciplineId = EmployeeDisciplineId;
            data.EmployeeId = EmployeeId;
            data.ReportedByEmployeeId = ReportedByEmployeeId;
            data.DisciplineCode = DisciplineCode;
            data.DisciplineTypeCode = DisciplineTypeCode;
            data.DisciplineDate = DisciplineDate;
            data.ReportedDate = ReportedDate;
            data.ExpiryDate = ExpiryDate;
            data.Note = Note;
            data.DisciplineResolutionCode = DisciplineResolutionCode;
            data.ResolutionDate = ResolutionDate;
            data.ResolutionNote = ResolutionNote;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeeDisciplineId = -1;
            EmployeeId = -1;
            ReportedByEmployeeId = null;
            DisciplineCode = null;
            DisciplineTypeCode = null;
            DisciplineDate = null;
            ReportedDate = null;
            ExpiryDate = null;
            Note = null;
            DisciplineResolutionCode = null;
            ResolutionDate = null;
            ResolutionNote = null;
        }
        #endregion
    }
}