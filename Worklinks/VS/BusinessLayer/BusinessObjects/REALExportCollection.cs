﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class REALExportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<REALExport>
    {
    }
}
