﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class RemittanceImportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<RemittanceImport>
    {
    }
}