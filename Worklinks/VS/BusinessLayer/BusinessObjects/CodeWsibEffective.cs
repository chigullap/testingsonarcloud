﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CodeWsibEffective : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long CodeWsibEffectiveId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String WsibCode { get; set; }

        [DataMember]
        public DateTime EffectiveDate { get; set; }

        [DataMember]
        public Decimal WorkersCompensationRate { get; set; }

        [DataMember]
        public Decimal? EstimatedValue { get; set; }

        [DataMember]
        public Decimal? MaximumAssessableEarning { get; set; }
        #endregion

        #region construct/destruct
        public CodeWsibEffective()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(CodeWsibEffective data)
        {
            base.CopyTo(data);

            data.CodeWsibEffectiveId = CodeWsibEffectiveId;
            data.WsibCode = WsibCode;
            data.EffectiveDate = EffectiveDate;
            data.WorkersCompensationRate = WorkersCompensationRate;
            data.EstimatedValue = EstimatedValue;
            data.MaximumAssessableEarning = MaximumAssessableEarning;
        }

        public new void Clear()
        {
            base.Clear();

            CodeWsibEffectiveId = -1;
            WsibCode = "";
            EffectiveDate = DateTime.Now;
            WorkersCompensationRate = 0;
            EstimatedValue = null;
            MaximumAssessableEarning = null;
        }
        #endregion
    }
}