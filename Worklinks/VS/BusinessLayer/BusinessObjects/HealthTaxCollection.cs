﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class HealthTaxCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<HealthTax>
    {
        public HealthTax GetProvinceRemittance(String province) //stacy test - see if this needs changing for ceridian export...
        {
            foreach (HealthTax prov in this)
            {
                if (prov.ProvinceStateCode.Equals(province))
                    return prov;
            }

            return null;
        }
    }
}