﻿using System;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class HrReportsCriteria
    {
        #region properties
        public string ReportName { get; set; }
        public string EmployeeNumber { get; set; }
        public string ReportType { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string OrganizationUnit { get; set; }
        public string PaymentMethodCode { get; set; }
        #endregion

        public HrReportsCriteria()
        {
            Clear();
        }
        public void Clear()
        {
            ReportName = null;
            EmployeeNumber = null;
            ReportType = null;
            StartDate = null;
            EndDate = null;
            OrganizationUnit = null;
            PaymentMethodCode = null;
        }
    }
}