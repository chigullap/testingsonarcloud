﻿using System;
using System.Collections.Generic;
using System.Text;

using System.ComponentModel.DataAnnotations;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class OrganizationUnitCriteria
    {
        #region properties
        public long OrganizationUnitId { get; set; }
        public long? OrganizationUnitLevelId { get; set; }
        public long? ParentOrganizationUnitId { get; set; }
        #endregion

        public OrganizationUnitCriteria()
        {
            Clear();
        }

        public void Clear()
        {
            OrganizationUnitId = -1;
            OrganizationUnitLevelId = null;
            ParentOrganizationUnitId = null;
        }
    }

}
