﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class AccrualPolicyCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<AccrualPolicy>
    {
    }
}