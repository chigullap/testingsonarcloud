﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeeCustomFieldConfigCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeeCustomFieldConfig>
    {
    }
}