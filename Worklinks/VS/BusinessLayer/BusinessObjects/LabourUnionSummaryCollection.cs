﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class LabourUnionSummaryCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<LabourUnionSummary>
    {
    }
}