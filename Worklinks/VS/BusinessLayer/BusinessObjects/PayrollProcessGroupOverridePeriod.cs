﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollProcessGroupOverridePeriod : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PayrollProcessGroupOverridePeriodId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public String PayrollProcessGroupCode { get; set; }

        [DataMember]
        public int Period { get; set; }
        #endregion

        #region construct/destruct
        public PayrollProcessGroupOverridePeriod()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PayrollProcessGroupOverridePeriod data)
        {
            base.CopyTo(data);

            data.PayrollProcessGroupOverridePeriodId = PayrollProcessGroupOverridePeriodId;
            data.Year = Year;
            data.PayrollProcessGroupCode = PayrollProcessGroupCode;
            data.Period = Period;
        }
        public new void Clear()
        {
            base.Clear();

            PayrollProcessGroupOverridePeriodId = -1;
            Year = DateTime.Now.Year;
            PayrollProcessGroupCode = null;
            Period = 0;
        }
        #endregion
    }
}