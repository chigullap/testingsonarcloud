﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class YearEndAdjustmentCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<YearEndAdjustment>
    {
    }
}