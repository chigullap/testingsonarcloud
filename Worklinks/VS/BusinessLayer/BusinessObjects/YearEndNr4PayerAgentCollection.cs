﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]

    public class YearEndNr4PayerAgentCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<YearEndNr4PayerAgent>
    {
    }
}
