﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class YearEndR2 : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long YearEndR2Id
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public int XmlSlipNumber { get; set; }

        [DataMember]
        public int PrintedSlipNumber { get; set; }

        [DataMember]
        public int Revision { get; set; }

        [DataMember]
        public long? PreviousRevisionYearEndR2Id { get; set; }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public decimal Year { get; set; }

        [DataMember]
        public string R2SourceOfIncomeCode { get; set; }

        [DataMember]
        public decimal BoxAAmount { get; set; }

        [DataMember]
        public decimal BoxBAmount { get; set; }

        [DataMember]
        public decimal BoxCAmount { get; set; }

        [DataMember]
        public decimal BoxDAmount { get; set; }

        [DataMember]
        public decimal BoxEAmount { get; set; }

        [DataMember]
        public decimal BoxFAmount { get; set; }

        [DataMember]
        public decimal BoxGAmount { get; set; }

        [DataMember]
        public decimal BoxHAmount { get; set; }

        [DataMember]
        public decimal BoxIAmount { get; set; }

        [DataMember]
        public decimal BoxJAmount { get; set; }

        [DataMember]
        public decimal BoxKAmount { get; set; }

        [DataMember]
        public decimal BoxLAmount { get; set; }

        [DataMember]
        public decimal BoxMAmount { get; set; }

        [DataMember]
        public decimal? BoxNBeneficiarySocialInsuranceNumber { get; set; }

        [DataMember]
        public decimal BoxOAmount { get; set; }

        [DataMember]
        public decimal BoxB1Amount { get; set; }

        [DataMember]
        public decimal BoxB2Amount { get; set; }

        [DataMember]
        public decimal BoxB3Amount { get; set; }

        [DataMember]
        public decimal BoxB4Amount { get; set; }

        [DataMember]
        public decimal BoxC1Amount { get; set; }

        [DataMember]
        public decimal BoxC2Amount { get; set; }

        [DataMember]
        public decimal BoxC3Amount { get; set; }

        [DataMember]
        public decimal BoxC4aAmount { get; set; }

        [DataMember]
        public decimal BoxC4bAmount { get; set; }

        [DataMember]
        public decimal BoxC4cAmount { get; set; }

        [DataMember]
        public decimal BoxC4dAmount { get; set; }

        [DataMember]
        public decimal BoxC4eAmount { get; set; }

        [DataMember]
        public decimal BoxC4fAmount { get; set; }

        [DataMember]
        public string BoxC6Amount { get; set; }

        [DataMember]
        public string BoxC7Date { get; set; }

        [DataMember]
        public string BoxC8YearMonth { get; set; }

        [DataMember]
        public decimal BoxC9Amount { get; set; }

        [DataMember]
        public string BoxC10Date { get; set; }

        [DataMember]
        public string Box201Code { get; set; }

        [DataMember]
        public decimal Box210Amount { get; set; }

        [DataMember]
        public decimal Box235Amount { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public string R2BoxNumber1 { get; set; }

        [DataMember]
        public string R2BoxAmount1 { get; set; }

        [DataMember]
        public string R2BoxNumber2 { get; set; }

        [DataMember]
        public string R2BoxAmount2 { get; set; }

        [DataMember]
        public string R2BoxNumber3 { get; set; }

        [DataMember]
        public string R2BoxAmount3 { get; set; }

        [DataMember]
        public string R2BoxNumber4 { get; set; }

        [DataMember]
        public string R2BoxAmount4 { get; set; }

        [DataMember]
        public int? PreviousXmlSlipNumber { get; set; }

        [DataMember]
        public int? PreviousPrintedSlipNumber { get; set; }
        #endregion

        #region construct/destruct
        public YearEndR2()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(YearEndR2 data)
        {
            base.CopyTo(data);

            data.YearEndR2Id = YearEndR2Id;
            data.XmlSlipNumber = XmlSlipNumber;
            data.PrintedSlipNumber = PrintedSlipNumber;
            data.Revision = Revision;
            data.PreviousRevisionYearEndR2Id = PreviousRevisionYearEndR2Id;
            data.EmployeeId = EmployeeId;
            data.Year = Year;
            data.R2SourceOfIncomeCode = R2SourceOfIncomeCode;
            data.BoxAAmount = BoxAAmount;
            data.BoxBAmount = BoxBAmount;
            data.BoxCAmount = BoxCAmount;
            data.BoxDAmount = BoxDAmount;
            data.BoxEAmount = BoxEAmount;
            data.BoxFAmount = BoxFAmount;
            data.BoxGAmount = BoxGAmount;
            data.BoxHAmount = BoxHAmount;
            data.BoxIAmount = BoxIAmount;
            data.BoxJAmount = BoxJAmount;
            data.BoxKAmount = BoxKAmount;
            data.BoxLAmount = BoxLAmount;
            data.BoxMAmount = BoxMAmount;
            data.BoxNBeneficiarySocialInsuranceNumber = BoxNBeneficiarySocialInsuranceNumber;
            data.BoxOAmount = BoxOAmount;
            data.BoxB1Amount = BoxB1Amount;
            data.BoxB2Amount = BoxB2Amount;
            data.BoxB3Amount = BoxB3Amount;
            data.BoxB4Amount = BoxB4Amount;
            data.BoxC1Amount = BoxC1Amount;
            data.BoxC2Amount = BoxC2Amount;
            data.BoxC3Amount = BoxC3Amount;
            data.BoxC4aAmount = BoxC4aAmount;
            data.BoxC4bAmount = BoxC4bAmount;
            data.BoxC4cAmount = BoxC4cAmount;
            data.BoxC4dAmount = BoxC4dAmount;
            data.BoxC4eAmount = BoxC4eAmount;
            data.BoxC4fAmount = BoxC4fAmount;
            data.BoxC6Amount = BoxC6Amount;
            data.BoxC7Date = BoxC7Date;
            data.BoxC8YearMonth = BoxC8YearMonth;
            data.BoxC9Amount = BoxC9Amount;
            data.BoxC10Date = BoxC10Date;
            data.Box201Code = Box201Code;
            data.Box210Amount = Box210Amount;
            data.Box235Amount = Box235Amount;
            data.ActiveFlag = ActiveFlag;
            data.R2BoxNumber1 = R2BoxNumber1;
            data.R2BoxAmount1 = R2BoxAmount1;
            data.R2BoxNumber2 = R2BoxNumber2;
            data.R2BoxAmount2 = R2BoxAmount2;
            data.R2BoxNumber3 = R2BoxNumber3;
            data.R2BoxAmount3 = R2BoxAmount3;
            data.R2BoxNumber4 = R2BoxNumber4;
            data.R2BoxAmount4 = R2BoxAmount4;
            data.PreviousXmlSlipNumber = PreviousXmlSlipNumber;
            data.PreviousPrintedSlipNumber = PreviousPrintedSlipNumber;
        }
        public new void Clear()
        {
            base.Clear();

            YearEndR2Id = -1;
            XmlSlipNumber = 0;
            PrintedSlipNumber = 0;
            Revision = 0;
            PreviousRevisionYearEndR2Id = null;
            EmployeeId = -1;
            Year = 0;
            R2SourceOfIncomeCode = null;
            BoxAAmount = 0;
            BoxBAmount = 0;
            BoxCAmount = 0;
            BoxDAmount = 0;
            BoxEAmount = 0;
            BoxFAmount = 0;
            BoxGAmount = 0;
            BoxHAmount = 0;
            BoxIAmount = 0;
            BoxJAmount = 0;
            BoxKAmount = 0;
            BoxLAmount = 0;
            BoxMAmount = 0;
            BoxNBeneficiarySocialInsuranceNumber = null;
            BoxOAmount = 0;
            BoxB1Amount = 0;
            BoxB2Amount = 0;
            BoxB3Amount = 0;
            BoxB4Amount = 0;
            BoxC1Amount = 0;
            BoxC2Amount = 0;
            BoxC3Amount = 0;
            BoxC4aAmount = 0;
            BoxC4bAmount = 0;
            BoxC4cAmount = 0;
            BoxC4dAmount = 0;
            BoxC4eAmount = 0;
            BoxC4fAmount = 0;
            BoxC6Amount = null;
            BoxC7Date = null;
            BoxC8YearMonth = null;
            BoxC9Amount = 0;
            BoxC10Date = null;
            Box201Code = null;
            Box210Amount = 0;
            Box235Amount = 0;
            ActiveFlag = true;
            R2BoxNumber1 = null;
            R2BoxAmount1 = null;
            R2BoxNumber2 = null;
            R2BoxAmount2 = null;
            R2BoxNumber3 = null;
            R2BoxAmount3 = null;
            R2BoxNumber4 = null;
            R2BoxAmount4 = null;
            PreviousXmlSlipNumber = null;
            PreviousPrintedSlipNumber = null;
        }
        #endregion
    }
}