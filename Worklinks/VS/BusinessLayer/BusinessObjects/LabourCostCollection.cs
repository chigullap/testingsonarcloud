﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class LabourCostCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<LabourCost>
    {
    }
}