﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class SalaryPlanGradeStepCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<SalaryPlanGradeStep>
    {
    }
}