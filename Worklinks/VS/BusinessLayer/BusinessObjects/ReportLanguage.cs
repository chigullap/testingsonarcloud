﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public class ReportLanguage : WLP.BusinessLayer.BusinessObjects.BusinessObject, ICloneable
    {

        #region fields
        #endregion

        #region properties
        [DataMember]
        public long ReportLanguageId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long ReportId { get; set; }
        [DataMember]
        public String LanguageCode { get; set; }
        [DataMember]
        public String Identifier { get; set; }
        [DataMember]
        public String Value { get; set; }

        #endregion


        #region construct/destruct
        public ReportLanguage()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone

        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(ReportLanguage data)
        {
            base.CopyTo(data);
            data.ReportLanguageId = ReportLanguageId;
            data.ReportId = ReportId;
            data.LanguageCode = LanguageCode;
            data.Identifier = Identifier;
            data.Value = Value;

        }
        public new void Clear()
        {
            base.Clear();
            ReportLanguageId = -1;
            ReportId = -1;
            LanguageCode = null;
            Identifier = null;
            Value = null;
        }

        #endregion
    }
}
