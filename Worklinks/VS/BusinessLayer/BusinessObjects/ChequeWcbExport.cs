﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ChequeWcbExport : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long ChequeWcbExportId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long RbcSequenceNumber { get; set; }

        [DataMember]
        public long RbcChequeNumber { get; set; }

        [DataMember]
        public byte[] Data { get; set; }

        [DataMember]
        public string CodeWsibCd { get; set; }

        [DataMember]
        public DateTime RemittanceDate { get; set; }

        [DataMember]
        public string CodeWcbChequeRemittanceFrequencyCd { get; set; }

        [DataMember]
        public DateTime? ChequeFileTransmissionDate { get; set; }

        [DataMember]
        public long? ChequeWcbPreAuthorizedDebitExportId { get; set; }

        [DataMember]
        public DateTime ReportingPeriodStartDate { get; set; }

        [DataMember]
        public DateTime ReportingPeriodEndDate { get; set; }

        #endregion

        #region construct/destruct
        public ChequeWcbExport()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();

            ChequeWcbExportId = -1;
            RbcSequenceNumber = -1;
            RbcChequeNumber = -1;
            Data = null;
            CodeWsibCd = null;
            RemittanceDate = DateTime.Now;
            CodeWcbChequeRemittanceFrequencyCd = null;
            ChequeFileTransmissionDate = null;
            ChequeWcbPreAuthorizedDebitExportId = null;
            ReportingPeriodStartDate = DateTime.Now;
            ReportingPeriodEndDate = DateTime.Now;
        }

        public virtual void CopyTo(ChequeWcbExport data)
        {
            base.CopyTo(data);

            data.ChequeWcbExportId = ChequeWcbExportId;
            data.RbcSequenceNumber = RbcSequenceNumber;
            data.RbcChequeNumber = RbcChequeNumber;
            Data.CopyTo(data.Data, 0);
            data.CodeWsibCd = CodeWsibCd;
            data.RemittanceDate = RemittanceDate;
            data.CodeWcbChequeRemittanceFrequencyCd = CodeWcbChequeRemittanceFrequencyCd;
            data.ChequeFileTransmissionDate = ChequeFileTransmissionDate;
            data.ChequeWcbPreAuthorizedDebitExportId = ChequeWcbPreAuthorizedDebitExportId;
            data.ReportingPeriodStartDate = ReportingPeriodStartDate;
            data.ReportingPeriodEndDate = ReportingPeriodEndDate;
        }
        #endregion
    }
}
