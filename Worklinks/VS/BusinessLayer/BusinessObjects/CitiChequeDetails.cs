﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CitiChequeDetails : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PayrollMasterPaymentId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long PayrollMasterId { get; set; }

        [DataMember]
        public String EmployeeNumber { get; set; }

        [DataMember]
        public Decimal ChequeAmount { get; set; }

        [DataMember]
        public DateTime ChequeDate { get; set; }

        [DataMember]
        public String CompanyCode { get; set; }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public long PayrollProcessId { get; set; }

        [DataMember]
        public DateTime ProcessChequeDate { get; set; }

        [DataMember]
        public String SequenceNumber { get; set; }

        [DataMember]
        public String EmployeeName { get; set; }

        [DataMember]
        public String CodeLanguage { get; set; }

        [DataMember]
        public String AddressLine1 { get; set; }

        [DataMember]
        public String AddressLine2 { get; set; }

        [DataMember]
        public String City { get; set; }

        [DataMember]
        public String CodeProvinceStateCd { get; set; }

        [DataMember]
        public String CodeCountryCd { get; set; }

        [DataMember]
        public String PostalZipCode { get; set; }

        [DataMember]
        public DateTime CutoffDate { get; set; }
        #endregion

        #region construct/destruct
        public CitiChequeDetails()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(CitiChequeDetails data)
        {
            base.CopyTo(data);

            data.PayrollMasterPaymentId = PayrollMasterPaymentId;
            data.PayrollMasterId = PayrollMasterId;
            data.EmployeeNumber = EmployeeNumber;
            data.ChequeAmount = ChequeAmount;
            data.ChequeDate = ChequeDate;
            data.CompanyCode = CompanyCode;
            data.EmployeeId = EmployeeId;
            data.PayrollProcessId = PayrollProcessId;
            data.ProcessChequeDate = ProcessChequeDate;
            data.SequenceNumber = SequenceNumber;
            data.EmployeeName = EmployeeName;
            data.AddressLine1 = AddressLine1;
            data.AddressLine2 = AddressLine2;
            data.City = City;
            data.CodeProvinceStateCd = CodeProvinceStateCd;
            data.CodeCountryCd = CodeCountryCd;
            data.PostalZipCode = PostalZipCode;
            data.CutoffDate = CutoffDate;
        }

        public new void Clear()
        {
            base.Clear();

            PayrollMasterPaymentId = -1;
            PayrollMasterId = -1;
            EmployeeNumber = "";
            ChequeAmount = 0;
            ChequeDate = DateTime.Now;
            CompanyCode = "";
            EmployeeId = -1;
            PayrollProcessId = -1;
            ProcessChequeDate = DateTime.Now;
            SequenceNumber = "";
            EmployeeName = "";
            AddressLine1 = "";
            AddressLine2 = "";
            City = "";
            CodeProvinceStateCd = "";
            CodeCountryCd = "";
            PostalZipCode = "";
            CutoffDate = DateTime.Now;
        }
        #endregion
    }
}