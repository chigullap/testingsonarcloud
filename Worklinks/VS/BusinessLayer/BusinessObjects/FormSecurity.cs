﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]

    public class FormSecurity : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties

        [DataMember]
        public long FormId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String Label { get; set; }
        [DataMember]
        public long? SecurityRoleFormId { get; set; }
        [DataMember]
        public String FormDescription { get; set; }
        [DataMember]
        public long? SecurityRoleId { get; set; }
        [DataMember]
        public bool ViewFlag { get; set; }
        [DataMember]
        public bool AddFlag { get; set; }
        [DataMember]
        public bool UpdateFlag { get; set; }
        [DataMember]
        public bool DeleteFlag { get; set; }

        #endregion

        #region construct/destruct
        public FormSecurity()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(FormSecurity data)
        {
            base.CopyTo(data);

            data.SecurityRoleFormId = SecurityRoleFormId;
            data.Label = Label;
            data.FormId = FormId;
            data.FormDescription = FormDescription;
            data.SecurityRoleId = SecurityRoleId;
            data.ViewFlag = ViewFlag;
            data.AddFlag = AddFlag;
            data.UpdateFlag = UpdateFlag;
            data.DeleteFlag = DeleteFlag;
        }
        public new void Clear()
        {
            base.Clear();

            Label = null;
            SecurityRoleFormId = -1;
            FormId = -1;
            FormDescription = "";
            SecurityRoleId = -1;
            ViewFlag = false;
            AddFlag = false;
            UpdateFlag = false;
            DeleteFlag = false;
        }

        #endregion
    }
}
