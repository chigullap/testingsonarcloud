﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PayrollBatchCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollBatch>
    {
        public Dictionary<long, PayrollTransactionCollection> EmployeeTransactions
        {
            get
            {
                Dictionary<long, PayrollTransactionCollection> list = new Dictionary<long, PayrollTransactionCollection>();

                foreach (PayrollBatch batch in this)
                {
                    foreach (PayrollTransaction transaction in batch.Transactions)
                    {
                        if (!list.ContainsKey(transaction.EmployeeId))
                        {
                            list.Add(transaction.EmployeeId, new PayrollTransactionCollection());
                        }
                        list[transaction.EmployeeId].Add(transaction);
                    }
                }

                return list;
            }
        }

        public void ClearTransactions()
        {
            foreach (PayrollBatch batch in this)
                batch.Transactions = null;
        }

        public PayrollBatchCollection GetPayrollBatchByPayrollProcessGroupCode(String payrollProcessGroupCode)
        {
            PayrollBatchCollection rtn = new PayrollBatchCollection();

            foreach (PayrollBatch batch in this)
            {
                if (batch.PayrollProcessGroupCode.Equals(payrollProcessGroupCode))
                    rtn.Add(batch);
            }

            return rtn;
        }

        public long[] PayrollBatchIds
        {
            get
            {
                long[] rtn = new long[this.Count];
                int i = 0;
                foreach(PayrollBatch batch in this)
                {
                    rtn[i++] = batch.PayrollBatchId;
                }
                return rtn;
            }
        }

    }

}
