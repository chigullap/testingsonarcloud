﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CanadaRevenueAgencyT4Export : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long CanadaRevenueAgencyT4ExportId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String YearEndFormCode { get; set; }
        [DataMember]
        public long YearEndId { get; set; }
        [DataMember]
        public YearEnd YearEnd { get; set; }
        [DataMember]
        public long CanadaRevenueAgencyT619Id { get; set; }
        [DataMember]
        public CanadaRevenueAgencyT619 CanadaRevenueAgencyT619 { get; set; }
        [DataMember]
        public CanadaRevenueAgencyT4Collection T4s { get; set; }
        [DataMember]
        public CanadaRevenueAgencyT4aCollection T4as { get; set; }
        [DataMember]
        public CanadaRevenueAgencyNR4Collection NR4s { get; set; }
        [DataMember]
        public Employer Employer { get; set; }
        [DataMember]
        public int CurrentRevision { get; set; }
        [DataMember]
        public YearEndNr4PayerAgent Nr4PayerAgent { get; set; }
        [DataMember]
        public Address Nr4PayerAgentAddress { get; set; }

        #endregion

        #region construct/destruct
        public CanadaRevenueAgencyT4Export()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(CanadaRevenueAgencyT4Export data)
        {
            base.CopyTo(data);
            data.CanadaRevenueAgencyT4ExportId = CanadaRevenueAgencyT4ExportId;
            data.CanadaRevenueAgencyT619Id = CanadaRevenueAgencyT619Id;
            data.YearEndFormCode = YearEndFormCode;
            data.YearEndId = YearEndId;
            if (YearEnd == null)
                data.YearEnd = null;
            else
                YearEnd.CopyTo(data.YearEnd);

            if (CanadaRevenueAgencyT619 == null)
                data.CanadaRevenueAgencyT619 = null;
            else
                CanadaRevenueAgencyT619.CopyTo(data.CanadaRevenueAgencyT619);

            if (T4s == null)
                data.T4s = null;
            else
                T4s.CopyTo(data.T4s);

            if (T4as == null)
                data.T4as = null;
            else
                T4as.CopyTo(data.T4as);

            if (NR4s == null)
                data.NR4s = null;
            else
                NR4s.CopyTo(data.NR4s);

            if (Employer == null)
                data.Employer = null;
            else
                Employer.CopyTo(data.Employer);

            data.CurrentRevision = CurrentRevision;

            if (Nr4PayerAgent == null)
                data.Nr4PayerAgent = null;
            else
                Nr4PayerAgent.CopyTo(data.Nr4PayerAgent);

            if (Nr4PayerAgentAddress == null)
                data.Nr4PayerAgentAddress = null;
            else
                Nr4PayerAgentAddress.CopyTo(data.Nr4PayerAgentAddress);
        }

        public new void Clear()
        {
            base.Clear();

            CanadaRevenueAgencyT4ExportId = -1;
            CanadaRevenueAgencyT619Id = -1;
            YearEndFormCode = null;
            YearEndId = -1;
            CanadaRevenueAgencyT619 = null;
            T4s = null;
            T4as = null;
            NR4s = null;
            Employer = null;
            CurrentRevision = 0;
            Nr4PayerAgent = null;
            Nr4PayerAgentAddress = null;
        }

        #endregion
    }
}
