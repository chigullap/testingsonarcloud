﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PayrollTransactionErrorCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollTransactionError>
    {
    }
}