﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class YearEndT4aCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<YearEndT4a>
    {
    }
}