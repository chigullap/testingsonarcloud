﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CraGarnishment : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        public long RowNumber
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public DateTime ChequeDate { get; set; }
        [DataMember]
        public string EmployeeName { get; set; }
        [DataMember]
        public string SocialInsuranceNumber { get; set; }
        [DataMember]
        public decimal GarnishmentAmount { get; set; }
        #endregion

        #region construct/destruct
        public CraGarnishment()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(CraGarnishment data)
        {
            base.CopyTo(data);

            data.RowNumber = RowNumber;
            data.ChequeDate = ChequeDate;
            data.EmployeeName = EmployeeName;
            data.SocialInsuranceNumber = SocialInsuranceNumber;
            data.GarnishmentAmount = GarnishmentAmount;
        }

        public new void Clear()
        {
            base.Clear();

            RowNumber = -1;
            ChequeDate = DateTime.Now;
            EmployeeName = null;
            SocialInsuranceNumber = null;
            GarnishmentAmount = 0;
        }

        #endregion
    }
}
