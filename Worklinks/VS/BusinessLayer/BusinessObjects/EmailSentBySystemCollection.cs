﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmailSentBySystemCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmailSentBySystem>
    {
    }
}
