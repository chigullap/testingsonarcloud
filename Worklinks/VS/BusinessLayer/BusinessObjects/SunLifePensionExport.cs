﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SunLifePensionExport : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String EmployeeNumber { get; set; }
        [DataMember]
        public String SocialInsuranceNumber { get; set; }
        [DataMember]
        public String MemberLastName { get; set; }
        [DataMember]
        public String MemberFirstName { get; set; }
        [DataMember]
        public String AddressLine1 { get; set; }
        [DataMember]
        public String AddressLine2 { get; set; }
        [DataMember]
        public String AddressLine3 { get; set; }
        [DataMember]
        public String AddressLine4 { get; set; }
        [DataMember]
        public String MemberPostalCode { get; set; }
        [DataMember]
        public String MemberResidenceCode { get; set; }
        [DataMember]
        public String UserField1 { get; set; }
        [DataMember]
        public String UserField2 { get; set; }
        [DataMember]
        public String UserField5 { get; set; }
        [DataMember]
        public String UserField9 { get; set; }
        [DataMember]
        public String EmployersRequired { get; set; }
        [DataMember]
        public String MemberVoluntaryMatched { get; set; }
        [DataMember]
        public String EmployerVoluntaryMatched { get; set; }
        [DataMember]
        public String MemberVoluntaryUnmatched1 { get; set; }
        [DataMember]
        public String MemberVoluntaryUnmatched2 { get; set; }
        [DataMember]
        public String MemberVoluntarySpousalUnmatched { get; set; }
        [DataMember]
        public String MemberVoluntaryUnmatched3 { get; set; }
        [DataMember]
        public String MemberProvinceOfEmployment1 { get; set; }
        [DataMember]
        public String MemberDateOfBirth1 { get; set; }
        [DataMember]
        public String MemberDateOfHire { get; set; }
        [DataMember]
        public String LanguageCode { get; set; }
        [DataMember]
        public String CurrentYearCompensation { get; set; }
        [DataMember]
        public String CurrentYearSalaryWages { get; set; }
        [DataMember]
        public String EmployeeStatusCode { get; set; }
        [DataMember]
        public String EmployeeStatusEffectiveDate { get; set; }
        [DataMember]
        public String MemberGender { get; set; }
        [DataMember]
        public String PayStartDate { get; set; }
        [DataMember]
        public String PayEndDate { get; set; }
        [DataMember]
        public String MemberProvinceOfEmployment2 { get; set; }
        [DataMember]
        public String DateOfHire { get; set; }
        [DataMember]
        public String MemberLanguageCode { get; set; }
        [DataMember]
        public String MemberDateOfBirth2 { get; set; }
        [DataMember]
        public String EmailFlag { get; set; }
        [DataMember]
        public String EmailAddress { get; set; }
        [DataMember]
        public String PhoneFlag { get; set; }

        #endregion

        #region construct/destruct
        public SunLifePensionExport()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(SunLifePensionExport data)
        {
            base.CopyTo(data);

            data.EmployeeId = EmployeeId;
            data.EmployeeNumber = EmployeeNumber;
            data.SocialInsuranceNumber = SocialInsuranceNumber = "";
            data.MemberLastName = MemberLastName = "";
            data.MemberFirstName = MemberFirstName = "";
            data.AddressLine1 = AddressLine1 = "";
            data.AddressLine2 = AddressLine2 = "";
            data.AddressLine3 = AddressLine3 = "";
            data.AddressLine4 = AddressLine4 = "";
            data.MemberPostalCode = MemberPostalCode = "";
            data.MemberResidenceCode = MemberResidenceCode = "";
            data.UserField1 = UserField1 = "";
            data.UserField2 = UserField2 = "";
            data.UserField5 = UserField5 = "";
            data.UserField9 = UserField9 = "";
            data.EmployersRequired = EmployersRequired = "";
            data.MemberVoluntaryMatched = MemberVoluntaryMatched = "";
            data.EmployerVoluntaryMatched = EmployerVoluntaryMatched = "";
            data.MemberVoluntaryUnmatched1 = MemberVoluntaryUnmatched1 = "";
            data.MemberVoluntaryUnmatched2 = MemberVoluntaryUnmatched2 = "";
            data.MemberVoluntarySpousalUnmatched = MemberVoluntarySpousalUnmatched = "";
            data.MemberVoluntaryUnmatched3 = MemberVoluntaryUnmatched3 = "";
            data.MemberProvinceOfEmployment1 = MemberProvinceOfEmployment1 = "";
            data.MemberDateOfBirth1 = MemberDateOfBirth1 = "";
            data.MemberDateOfHire = MemberDateOfHire = "";
            data.LanguageCode = LanguageCode = "";
            data.CurrentYearCompensation = CurrentYearCompensation = "";
            data.CurrentYearSalaryWages = CurrentYearSalaryWages = "";
            data.EmployeeStatusCode = EmployeeStatusCode = "";
            data.EmployeeStatusEffectiveDate = EmployeeStatusEffectiveDate = "";
            data.MemberGender = MemberGender = "";
            data.PayStartDate = PayStartDate = "";
            data.PayEndDate = PayEndDate = "";
            data.MemberProvinceOfEmployment2 = MemberProvinceOfEmployment2 = "";
            data.DateOfHire = DateOfHire = "";
            data.MemberLanguageCode = MemberLanguageCode = "";
            data.MemberDateOfBirth2 = MemberDateOfBirth2 = "";
            data.EmailFlag = EmailFlag = "";
            data.EmailAddress = EmailAddress = "";
            data.PhoneFlag = PhoneFlag = "";
        }

        public new void Clear()
        {
            base.Clear();

            EmployeeId = -1;
            EmployeeNumber = "";
            SocialInsuranceNumber = "";
            MemberLastName = "";
            MemberFirstName = "";
            AddressLine1 = "";
            AddressLine2 = "";
            AddressLine3 = "";
            AddressLine4 = "";
            MemberPostalCode = "";
            MemberResidenceCode = "";
            UserField1 = "";
            UserField2 = "";
            UserField5 = "";
            UserField9 = "";
            EmployersRequired = "";
            MemberVoluntaryMatched = "";
            EmployerVoluntaryMatched = "";
            MemberVoluntaryUnmatched1 = "";
            MemberVoluntaryUnmatched2 = "";
            MemberVoluntarySpousalUnmatched = "";
            MemberVoluntaryUnmatched3 = "";
            MemberProvinceOfEmployment1 = "";
            MemberDateOfBirth1 = "";
            MemberDateOfHire = "";
            LanguageCode = "";
            CurrentYearCompensation = "";
            CurrentYearSalaryWages = "";
            EmployeeStatusCode = "";
            EmployeeStatusEffectiveDate = "";
            MemberGender = "";
            PayStartDate = "";
            PayEndDate = "";
            MemberProvinceOfEmployment2 = "";
            DateOfHire = "";
            MemberLanguageCode = "";
            MemberDateOfBirth2 = "";
            EmailFlag = "";
            EmailAddress = "";
            PhoneFlag = "";
        }

        #endregion

    }
}
