﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ScotiaBankEdi : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PayrollMasterPaymentId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String CodeCompanyCd { get; set; }

        [DataMember]
        public String EmployeeNumber { get; set; }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public String PayrollProcessId { get; set; }

        [DataMember]
        public String CodeEmployeeBankingSequenceCode { get; set; }

        [DataMember]
        public String EmployeeName { get; set; }

        [DataMember]
        public String BankInstCode { get; set; }

        [DataMember]
        public String BankTransactionNumber { get; set; }

        [DataMember]
        public String EmployeeBankAccountNumber { get; set; }

        [DataMember]
        public String AbaNumber { get; set; }

        [DataMember]
        public Decimal ChequeAmount { get; set; }

        [DataMember]
        public DateTime ChequeDate { get; set; }

        [DataMember]
        public bool DirectDepositFlag { get; set; }

        [DataMember]
        public long? SequenceNumber { get; set; }
       
        [DataMember]
        public string CodeStatusCd { get; set; }

        [DataMember]
        public string CodeBankingCountryCd { get; set; }

        #endregion

        #region construct/destruct
        public ScotiaBankEdi()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(ScotiaBankEdi data)
        {
            base.CopyTo(data);

            data.PayrollMasterPaymentId = PayrollMasterPaymentId;
            data.CodeCompanyCd = CodeCompanyCd;
            data.EmployeeNumber = EmployeeNumber;
            data.EmployeeId = EmployeeId;
            data.PayrollProcessId = PayrollProcessId;
            data.CodeEmployeeBankingSequenceCode = CodeEmployeeBankingSequenceCode;
            data.EmployeeName = EmployeeName;
            data.BankInstCode = BankInstCode;
            data.BankTransactionNumber = BankTransactionNumber;
            data.EmployeeBankAccountNumber = EmployeeBankAccountNumber;
            data.AbaNumber = AbaNumber;
            data.ChequeAmount = ChequeAmount;
            data.ChequeDate = ChequeDate;
            data.DirectDepositFlag = DirectDepositFlag;
            data.SequenceNumber = SequenceNumber;
            data.CodeStatusCd = CodeStatusCd;
            data.CodeBankingCountryCd = CodeBankingCountryCd;
        }

        public new void Clear()
        {
            base.Clear();

            PayrollMasterPaymentId = -1;
            CodeCompanyCd = "";
            EmployeeNumber = "";
            EmployeeId = -1;
            PayrollProcessId = "";
            CodeEmployeeBankingSequenceCode = null;
            EmployeeName = "";
            BankInstCode = "";
            BankTransactionNumber = "";
            EmployeeBankAccountNumber = "";
            AbaNumber = null;
            ChequeAmount = 0;
            ChequeDate = DateTime.Now;
            DirectDepositFlag = true;
            SequenceNumber = null;
            CodeStatusCd = "";
            CodeBankingCountryCd = null;
        }
        #endregion
    }
}
