﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class AdjustmentRuleExportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<AdjustmentRuleExport>
    {
    }
}
