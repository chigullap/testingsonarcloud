﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeeRemittanceStubCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeeRemittanceStub>
    {
    }
}