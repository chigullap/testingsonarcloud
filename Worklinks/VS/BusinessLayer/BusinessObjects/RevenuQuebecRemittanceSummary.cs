﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class RevenuQuebecRemittanceSummary : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        public long DummyKey
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long PayrollProcessId { get; set; }
        [DataMember]
        public string RevenuQuebecTaxId { get; set; }
        [DataMember]
        public DateTime ChequeDate { get; set; }
        [DataMember]
        public Int64 EmployeeCount { get; set; }
        [DataMember]
        public decimal QuebecTax { get; set; }
        [DataMember]
        public decimal TotalQuebecPensionPlanContribution { get; set; }
        [DataMember]
        public decimal TotalHealthTaxLevy { get; set; }
        [DataMember]
        public decimal TotalQuebecParentalInsurancePlanContribution { get; set; }
        [DataMember]
        public decimal EmployerWorkersCompensationPremium { get; set; }
        [DataMember]
        public DateTime? RemittancePeriodStartDate { get; set; }
        [DataMember]
        public DateTime? RemittancePeriodEndDate { get; set; }
        [DataMember]
        public DateTime? EffectiveEntryDate { get; set; }
        [DataMember]
        public string RqSubdivisionCode { get; set; }
        [DataMember]
        public string RqFormCode { get; set; }

        #endregion

        #region construct/destruct
        public RevenuQuebecRemittanceSummary()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(RevenuQuebecRemittanceSummary data)
        {
            base.CopyTo(data);

            data.DummyKey = DummyKey;
            data.PayrollProcessId = PayrollProcessId;
            data.RevenuQuebecTaxId = RevenuQuebecTaxId;
            data.ChequeDate = ChequeDate;
            data.EmployeeCount = EmployeeCount;
            data.QuebecTax = QuebecTax;
            data.TotalQuebecPensionPlanContribution = TotalQuebecPensionPlanContribution;
            data.TotalHealthTaxLevy = TotalHealthTaxLevy;
            data.TotalQuebecParentalInsurancePlanContribution = TotalQuebecParentalInsurancePlanContribution;
            data.EmployerWorkersCompensationPremium = EmployerWorkersCompensationPremium;
            data.RemittancePeriodStartDate = RemittancePeriodStartDate;
            data.RemittancePeriodEndDate = RemittancePeriodEndDate;
            data.EffectiveEntryDate = EffectiveEntryDate;
            data.RqSubdivisionCode = RqSubdivisionCode;
            data.RqFormCode = RqFormCode;
        }

        public new void Clear()
        {
            base.Clear();

            DummyKey = -1;
            PayrollProcessId = -1;
            RevenuQuebecTaxId = null;
            ChequeDate = DateTime.Now;
            EmployeeCount = 0;
            QuebecTax = 0;
            TotalQuebecPensionPlanContribution = 0;
            TotalHealthTaxLevy = 0;
            TotalQuebecParentalInsurancePlanContribution = 0;
            EmployerWorkersCompensationPremium = 0;
            RemittancePeriodStartDate = null;
            RemittancePeriodEndDate = null;
            EffectiveEntryDate = null;
            RqSubdivisionCode = null;
            RqFormCode = null;
        }

        #endregion
    }
}
