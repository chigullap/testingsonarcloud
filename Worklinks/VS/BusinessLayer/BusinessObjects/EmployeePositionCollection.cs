﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.IO;
using System.Linq;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class EmployeePositionCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeePosition>
    {
        //[DataMember]
        //public DateTime? PayrollPeriodCutoffDate { get; set; }

        public EmployeePosition GetFormerPosition(long? employeePositionId)
        {
            foreach (EmployeePosition position in this)
            {
                if (position.NextEmployeePositionId.Equals(employeePositionId))
                    return position;
            }
            return null;
        }

        public EmployeePosition FirstPosition
        {
            get
            {
                EmployeePosition rtn = this[0];
                foreach (EmployeePosition position in this)
                {
                    if (rtn.EffectiveDate >= position.EffectiveDate && rtn.EffectiveSequence >= position.EffectiveSequence)
                        rtn = position;
                }

                return rtn;
            }
        }

        public EmployeePosition GetCurrentEmployeePosition(DateTime cutoffDate)
        {


            EmployeePosition currentPosition = FirstPosition;

            foreach (EmployeePosition position in this)
            {
                if (position.EffectiveDate <= cutoffDate)
                {
                    if (position.EffectiveDate > currentPosition.EffectiveDate)
                        currentPosition = position;
                    else if (position.EffectiveDate == currentPosition.EffectiveDate && position.EffectiveSequence >= currentPosition.EffectiveSequence)
                        currentPosition = position;
                }
            }

            return currentPosition;
        }
    }
}
