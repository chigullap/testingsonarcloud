﻿
using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class AccrualEntitlement : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long AccrualEntitlementId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String EnglishDescription { get; set; }

        [DataMember]
        public String FrenchDescription { get; set; }

        [DataMember]
        public String EntitlementBasedOnCode { get; set; }

        [DataMember]
        public String PaycodeCode { get; set; }
        #endregion

        #region construct/destruct
        public AccrualEntitlement()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(AccrualEntitlement data)
        {
            base.CopyTo(data);

            data.AccrualEntitlementId = AccrualEntitlementId;
            data.EnglishDescription = EnglishDescription;
            data.FrenchDescription = FrenchDescription;
            data.EntitlementBasedOnCode = EntitlementBasedOnCode;
            data.PaycodeCode = PaycodeCode;
        }
        public new void Clear()
        {
            base.Clear();

            AccrualEntitlementId = -1;
            EnglishDescription = null;
            FrenchDescription = null;
            EntitlementBasedOnCode = null;
            PaycodeCode = null;
        }
        #endregion
    }
}