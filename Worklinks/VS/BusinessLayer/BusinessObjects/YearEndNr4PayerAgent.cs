﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class YearEndNr4PayerAgent : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long YearEndNr4PayerAgentId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String Name { get; set; }

        [DataMember]
        public long AddressId { get; set; }

        [DataMember]
        public String NonResidentAccountNumber { get; set; }
        
        #endregion

        #region construct/destruct
        public YearEndNr4PayerAgent()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(YearEndNr4PayerAgent data)
        {
            base.CopyTo(data);

            data.YearEndNr4PayerAgentId = YearEndNr4PayerAgentId;
            data.Name = Name;
            data.AddressId = AddressId;
            data.NonResidentAccountNumber = NonResidentAccountNumber;
        }
        public new void Clear()
        {
            base.Clear();

            YearEndNr4PayerAgentId = -1;
            Name = null;
            AddressId = -1;
            NonResidentAccountNumber = null;
        }
        #endregion
    }
}
