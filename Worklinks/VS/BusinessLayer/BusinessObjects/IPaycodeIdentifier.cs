﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public interface IPaycodeIdentifier
    {
        String PaycodeCodeImportExternalIdentifier { get; set; }
        String PaycodeCode { get; set; }
        decimal AmountRateFactor { get; set; }
        bool IncludeEmploymentInsuranceHoursFlag { get; set; }
        bool UseSalaryStandardHoursFlag { get; set; }
        String PaycodeTypeCode { get; set; }
        bool AutoPopulateRateFlag { get; set; }
        bool HasErrors { get; set; }
    }
}
