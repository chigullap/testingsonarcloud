﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class GlPexCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<GlPex>
    {
        public Decimal TotalDebitAmount
        {
            get
            {
                Decimal totalAmount = 0;
                foreach (GlPex rec in this)
                {
                    totalAmount += rec.DebitAmount;
                }
                return totalAmount;
            }
        }
        public Decimal TotalCreditAmount
        {
            get
            {
                Decimal totalAmount = 0;
                foreach (GlPex rec in this)
                {
                    totalAmount += rec.CreditAmount;
                }
                return totalAmount;
            }
        }
    }
}

