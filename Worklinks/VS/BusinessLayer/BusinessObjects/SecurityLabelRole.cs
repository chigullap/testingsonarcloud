﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SecurityLabelRole: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties

        [DataMember]
        public long SecurityLabelRoleId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long SecurityRoleId { get; set; }
        [DataMember]
        public String LabelPath { get; set; }
        #endregion

        #region construct/destruct
        public SecurityLabelRole()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone

        public override object Clone()
        {
            SecurityLabelRole data = new SecurityLabelRole();
            CopyTo(data);
            return data;
        }
        public void CopyTo(SecurityLabelRole data)
        {
            base.CopyTo(data);
            data.SecurityLabelRoleId = SecurityLabelRoleId;
            data.SecurityRoleId = SecurityRoleId;
            data.LabelPath = LabelPath;
        }
        public new void Clear()
        {
            base.Clear();
            SecurityLabelRoleId = -1;
            SecurityRoleId = -1;
            LabelPath = null;
        }

        #endregion

    }
}
