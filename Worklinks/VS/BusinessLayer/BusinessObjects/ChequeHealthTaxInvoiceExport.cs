﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ChequeHealthTaxInvoiceExport : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long ChequeHealthTaxInvoiceExportId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public string CodeProvinceStateCd { get; set; }
        [DataMember]
        public DateTime RemittanceDate { get; set; }
        [DataMember]
        public EmailSentBySystem EmailSentBySystem { get; set; }

        #endregion

        #region construct/destruct
        public ChequeHealthTaxInvoiceExport()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();

            ChequeHealthTaxInvoiceExportId = -1;
            CodeProvinceStateCd = null;
            RemittanceDate = DateTime.Now;
            EmailSentBySystem = new EmailSentBySystem();
        }

        public virtual void CopyTo(ChequeHealthTaxInvoiceExport data)
        {
            base.CopyTo(data);

            data.ChequeHealthTaxInvoiceExportId = ChequeHealthTaxInvoiceExportId;
            data.CodeProvinceStateCd = CodeProvinceStateCd;
            data.RemittanceDate = RemittanceDate;
            EmailSentBySystem.CopyTo(data.EmailSentBySystem);
        }
        #endregion
    }
}
