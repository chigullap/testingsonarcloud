﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeePositionSecondary : WLP.BusinessLayer.BusinessObjects.BusinessObject, IEmployeePosition
    {
        String _organizationUnit = null;

        #region properties
        [DataMember]
        public long EmployeePositionSecondaryId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeePositionId { get; set; }

        [DataMember]
        public decimal CompensationAmount { get; set; }

        [DataMember]
        public long? SalaryPlanId { get; set; }

        [DataMember]
        public long? SalaryPlanGradeId { get; set; }

        [DataMember]
        public long? SalaryPlanGradeStepId { get; set; }

        [DataMember]
        public string OrganizationUnitDescription { get; set; }

        [DataMember]
        public EmployeePositionSecondaryOrganizationUnitCollection OrganizationUnits { get; set; }

        [DataMember]
        public decimal AccumulatedHours { get; set; }

        public string OrganizationUnit
        {
            get
            {
                if (OrganizationUnits != null)
                {
                    List<string> tempList = new List<string>();

                    foreach (EmployeePositionSecondaryOrganizationUnit unit in OrganizationUnits)
                        tempList.Add(unit.OrganizationUnitId == null ? "" : unit.OrganizationUnitId.ToString());

                    if (tempList.Count > 0)
                        _organizationUnit = "/" + string.Join("/", tempList);
                }

                return _organizationUnit;
            }

            set
            {
                _organizationUnit = value;
            }

        }

        public string OrganizationUnitDates
        {
            get
            {
                string organizationUnitDate = null;

                if (OrganizationUnits != null)
                {
                    List<string> tempList = new List<string>();

                    foreach (EmployeePositionSecondaryOrganizationUnit unit in OrganizationUnits)
                        tempList.Add(unit.OrganizationUnitId == null ? "" : ((DateTime)unit.OrganizationUnitStartDate).ToString("yyyy-MM-dd"));

                    if (tempList.Count > 0)
                        organizationUnitDate = "/" + string.Join("/", tempList);
                }

                return organizationUnitDate;
            }

            set
            {
                throw new NotImplementedException("EmployeePositionSecondary:OrganizationUnit:set");
            }

        }

        public Decimal RatePerHour
        {
            get { return CompensationAmount; }
        }

        #endregion

        #region construct/destruct
        public EmployeePositionSecondary()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            EmployeePositionSecondary item = new EmployeePositionSecondary();
            this.CopyTo(item);

            return item;
        }
        public void CopyTo(EmployeePositionSecondary data)
        {
            base.CopyTo(data);

            data.EmployeePositionSecondaryId = EmployeePositionSecondaryId;
            data.EmployeePositionId = EmployeePositionId;
            data.CompensationAmount = CompensationAmount;
            data.SalaryPlanId = SalaryPlanId;
            data.SalaryPlanGradeId = SalaryPlanGradeId;
            data.SalaryPlanGradeStepId = SalaryPlanGradeStepId;
            data.OrganizationUnitDescription = OrganizationUnitDescription;

            if (OrganizationUnits != null)
            {
                EmployeePositionSecondaryOrganizationUnitCollection newCollection = new EmployeePositionSecondaryOrganizationUnitCollection();
                OrganizationUnits.CopyTo(newCollection);
                data.OrganizationUnits = newCollection;
            }
            else
                data.OrganizationUnits = null;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeePositionSecondaryId = -1;
            EmployeePositionId = -1;
            CompensationAmount = 0;
            SalaryPlanId = null;
            SalaryPlanGradeId = null;
            SalaryPlanGradeStepId = null;
            OrganizationUnits = null;
            OrganizationUnitDescription = null;
        }
        #endregion
    }
}