﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SalaryPlanCodeTable : CodeTable
    {
        #region properties
        [DataMember]
        public bool HasChildrenFlag { get; set; }
        #endregion

        #region construct/destruct
        public SalaryPlanCodeTable()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(SalaryPlanCodeTable data)
        {
            base.CopyTo(data);
            data.HasChildrenFlag = HasChildrenFlag;
        }
        public new void Clear()
        {
            base.Clear();
            HasChildrenFlag = false;
        }
        #endregion
    }
}