﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class RevenueQuebecTransmitterRecord : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long RevenueQuebecTransmitterRecordId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String RevenueQuebecTransmitterTypeCode { get; set; }
        [DataMember]
        public String TransmitterNumber { get; set; }
        [DataMember]
        public String TransmitterName { get; set; }
        [DataMember]
        public String CertificationNumber { get; set; }
        [DataMember]
        public String SoftwareName { get; set; }
        [DataMember]
        public String LanguageCode { get; set; }
        [DataMember]
        public Person Person { get; set; }
        [DataMember]
        public PersonAddress PersonAddress { get; set; }
        [DataMember]
        public PersonContactChannel Email { get; set; }
        [DataMember]
        public PersonContactChannel Phone { get; set; }
        [DataMember]
        public long TransmitterPersonId { get; set; }
        [DataMember]
        public String PartnerIdentifier { get; set; }
        [DataMember]
        public String ProductIdentifier { get; set; }
        [DataMember]
        public String TestCaseNumber { get; set; }


        #endregion


        #region construct/destruct
        public RevenueQuebecTransmitterRecord()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(RevenueQuebecTransmitterRecord data)
        {
            base.CopyTo(data);

            data.RevenueQuebecTransmitterRecordId = RevenueQuebecTransmitterRecordId;
            data.RevenueQuebecTransmitterTypeCode = RevenueQuebecTransmitterTypeCode;
            data.TransmitterNumber = TransmitterNumber;
            data.TransmitterName = TransmitterName;
            data.CertificationNumber = CertificationNumber;
            data.SoftwareName = SoftwareName;
            data.LanguageCode = LanguageCode;
            data.Person = Person;
            data.PersonAddress = PersonAddress;
            data.Email = Email;
            data.Phone = Phone;
            data.TransmitterPersonId = TransmitterPersonId;
            data.PartnerIdentifier = PartnerIdentifier;
            data.ProductIdentifier = ProductIdentifier;
            data.TestCaseNumber = TestCaseNumber;
        }

        public new void Clear()
        {
            base.Clear();

            RevenueQuebecTransmitterRecordId = -1;
            RevenueQuebecTransmitterTypeCode = null;
            TransmitterNumber = null;
            TransmitterName = null;
            CertificationNumber = null;
            SoftwareName = null;
            LanguageCode = null;
            Person = null;
            PersonAddress = null;
            Email = null;
            Phone = null;
            TransmitterPersonId = -1;
            PartnerIdentifier = "";
            ProductIdentifier = "";
            TestCaseNumber = "";
        }

        #endregion
    }
}
