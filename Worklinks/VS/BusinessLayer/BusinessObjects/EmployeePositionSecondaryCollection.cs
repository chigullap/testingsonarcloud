﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeePositionSecondaryCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeePositionSecondary>
    {

    }
}