﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeePositionSecondaryOrganizationUnitCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeePositionSecondaryOrganizationUnit>
    {
        public string OrganizationUnit
        {
            get
            {
                string organizationUnit = null;
                System.Collections.Generic.List<string> tempList = new System.Collections.Generic.List<string>();

                foreach (EmployeePositionSecondaryOrganizationUnit unit in this)
                    tempList.Add(unit.OrganizationUnitId == null ? "" : unit.OrganizationUnitId.ToString());

                if (tempList.Count > 0)
                    organizationUnit = "/" + string.Join("/", tempList);

                return organizationUnit;
            }
        }
    }
}