﻿using System;
using FileHelpers;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    [DelimitedRecord(",")]
    public class PhoneImport
    {
        [DataMember]
        public String EmployeeIdentifier;

        [DataMember]
        public String PrimaryContactValue;

        [DataMember]
        public String SecondaryContactValue;
        
        [DataMember]
        public String contactChanelType;
    }
}