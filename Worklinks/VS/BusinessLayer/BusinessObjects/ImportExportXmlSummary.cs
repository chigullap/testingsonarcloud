﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using  WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ImportExportXmlSummary : BusinessObject
    {
        #region fields
        #endregion

        #region properties

        [DataMember]
        public long ImportExportXmlId 
        {
            get { return (long)_Key; }
            set { _Key = value; }        
        }
        [DataMember]
        public long ImportExportId { get; set; }
        [DataMember]
        public short ProcessingOrder { get; set; }
        [DataMember]
        public byte[] Data { get; set; }
        [DataMember]
        public bool InputFileParameterRequiredFlag { get; set; }
        [DataMember]
        public String InputFileParameterName { get; set; }
        #endregion

        #region construct/destruct
        public ImportExportXmlSummary()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone

        public new void Clear()
        {
            base.Clear();
            ImportExportXmlId = -1;
            ImportExportId = -1;
            ProcessingOrder = -1;
            Data = null;
            InputFileParameterRequiredFlag = false;
            InputFileParameterName = null;
        }

        public void CopyTo(ImportExportXmlSummary data)
        {
            base.CopyTo(data);
            data.ImportExportXmlId = ImportExportXmlId;
            data.ImportExportId = ImportExportId;
            data.ProcessingOrder = ProcessingOrder;
            data.Data = Data;
            data.InputFileParameterRequiredFlag = InputFileParameterRequiredFlag;
            data.InputFileParameterName = InputFileParameterName;
        }

        #endregion

        public override object Clone()
        {
            throw new NotImplementedException();
        }
    }
}
