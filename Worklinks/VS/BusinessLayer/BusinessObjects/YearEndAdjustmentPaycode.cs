﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class YearEndAdjustmentPaycode : BusinessObject
    {
        #region Properties
        [DataMember]
        public long YearEndAdjustmentPaycodeId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long? ImportExportLogId { get; set; }

        [DataMember]
        public Decimal Year { get; set; }

        [DataMember]
        public long BusinessNumberId { get; set; }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public String ProvinceStateCode { get; set; }

        [DataMember]
        public String PaycodeCode { get; set; }

        [DataMember]
        public Decimal Amount { get; set; }
        #endregion


        #region construct/destruct
        public YearEndAdjustmentPaycode()
        {
            Clear();
        }
        #endregion


        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(YearEndAdjustmentPaycode data)
        {
            base.CopyTo(data);

            data.YearEndAdjustmentPaycodeId = YearEndAdjustmentPaycodeId;
            data.ImportExportLogId = ImportExportLogId;
            data.Year = Year;
            data.BusinessNumberId = BusinessNumberId;
            data.EmployeeId = EmployeeId;
            data.ProvinceStateCode = ProvinceStateCode;
            data.PaycodeCode = PaycodeCode;
            data.Amount = Amount;
        }

        public new void Clear()
        {
            base.Clear();

            YearEndAdjustmentPaycodeId = -1;
            ImportExportLogId = null;
            Year = -1;
            BusinessNumberId = -1;
            EmployeeId = -1;
            ProvinceStateCode = null;
            PaycodeCode = null;
            Amount = -1;
        }
        #endregion
    }
}
