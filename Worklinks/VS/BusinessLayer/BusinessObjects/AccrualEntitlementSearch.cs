﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class AccrualEntitlementSearch : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long AccrualEntitlementId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long AccrualEntitlementDescriptionId { get; set; }

        [DataMember]
        public String AccrualEntitlementDescriptionField { get; set; }
        #endregion

        #region construct/destruct
        public AccrualEntitlementSearch()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(AccrualEntitlementSearch data)
        {
            base.CopyTo(data);

            data.AccrualEntitlementId = AccrualEntitlementId;
            data.AccrualEntitlementDescriptionId = AccrualEntitlementDescriptionId;
            data.AccrualEntitlementDescriptionField = AccrualEntitlementDescriptionField;
        }
        public new void Clear()
        {
            base.Clear();

            AccrualEntitlementId = -1;
            AccrualEntitlementDescriptionId = -1;
            AccrualEntitlementDescriptionField = "";
        }
        #endregion
    }
}