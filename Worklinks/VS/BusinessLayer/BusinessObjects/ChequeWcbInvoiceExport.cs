﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ChequeWcbInvoiceExport : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long ChequeWcbInvoiceExportId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public string CodeWsibCd { get; set; }
        [DataMember]
        public DateTime RemittanceDate { get; set; }
        [DataMember]
        public EmailSentBySystem EmailSentBySystem { get; set; }

        #endregion

        #region construct/destruct
        public ChequeWcbInvoiceExport()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();

            ChequeWcbInvoiceExportId = -1;
            CodeWsibCd = null;
            RemittanceDate = DateTime.Now;
            EmailSentBySystem = new EmailSentBySystem();
        }

        public virtual void CopyTo(ChequeWcbInvoiceExport data)
        {
            base.CopyTo(data);

            data.ChequeWcbInvoiceExportId = ChequeWcbInvoiceExportId;
            data.CodeWsibCd = CodeWsibCd;
            data.RemittanceDate = RemittanceDate;
            EmailSentBySystem.CopyTo(data.EmailSentBySystem);
        }
        #endregion
    }
}
