﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class RemittanceDetailRqSourceDeductionsCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<RemittanceDetailRqSourceDeductions>
    {
    }
}
