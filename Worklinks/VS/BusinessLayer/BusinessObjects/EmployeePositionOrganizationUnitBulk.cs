﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeePositionOrganizationUnitBulk: EmployeePositionOrganizationUnit
    {
        [DataMember]
        public override long EmployeePositionOrganizationUnitId {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public override long OrganizationUnitLevelId { get; set; }


    }
}
