﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ReportExport : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long ExportId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public long? ReportId { get; set; }
        [DataMember]
        public string FileFormat { get; set; }
        [DataMember]
        public string ExportTypeCode { get; set; }
        [DataMember]
        public bool ActiveFlag { get; set; }
        [DataMember]
        public bool HasChildrenFlag { get; set; }
        #endregion

        #region construct/destruct
        public ReportExport()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(ReportExport data)
        {
            base.CopyTo(data);

            data.ExportId = ExportId;
            data.Name = Name;
            data.ReportId = ReportId;
            data.FileFormat = FileFormat;
            data.ExportTypeCode = ExportTypeCode;
            data.ActiveFlag = ActiveFlag;
            data.HasChildrenFlag = HasChildrenFlag;
        }
        public new void Clear()
        {
            base.Clear();

            ExportId = -1;
            Name = null;
            ReportId = null;
            FileFormat = null;
            ExportTypeCode = null;
            ActiveFlag = true;
            HasChildrenFlag = false;
        }
        #endregion
    }
}