﻿//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Runtime.Serialization;

//namespace WorkLinks.BusinessLayer.BusinessObjects
//{
//    [Serializable]
//    [DataContract]
//    public class EmployeeSkill: WLP.BusinessLayer.BusinessObjects.BusinessObject
//    {
        
//        #region properties
//        [DataMember]
//        public long EmployeeSkillId 
//        {
//            get { return (long)_Key; }
//            set { _Key = value; }
//        }
//        [DataMember]
//        public Employee ParentEmployee { get; set; }
//        [DataMember]
//        public long EmployeeId { get; set; }
//        [DataMember]
//        public String EmployeeSkillCode { get; set; }
//        [DataMember]
//        public DateTime? DateAcquired { get; set; }
//        [DataMember]
//        public DateTime? ExpiryDate { get; set; }
//        [DataMember]
//        public bool JobRelatedFlag { get; set; }
//        [DataMember]
//        public bool JobRequiredFlag { get; set; }

//        #endregion

//        #region construct/destruct
//        public EmployeeSkill()
//        {
//            Clear();
//        }

//        #endregion

//        #region clear/copy/clone
//        public override object Clone()
//        {
//            throw new NotImplementedException();
//        }
//        public void CopyTo(EmployeeSkill data)
//        {
//            base.CopyTo(data);

//            data.EmployeeSkillId = EmployeeSkillId;
//            data.ParentEmployee = ParentEmployee;
//            data.EmployeeId = EmployeeId;
//            data.EmployeeSkillCode = EmployeeSkillCode;
//            data.DateAcquired = DateAcquired;
//            data.ExpiryDate = ExpiryDate;
//            data.JobRelatedFlag = JobRelatedFlag;
//            data.JobRequiredFlag = JobRequiredFlag;
//        }
//        public new void Clear()
//        {
//            base.Clear();

//            EmployeeSkillId = -1;
//            EmployeeId = -1;
//            ParentEmployee = null;
//            EmployeeSkillCode = null;
//            DateAcquired = null;
//            ExpiryDate = null;
//            JobRelatedFlag = false;
//            JobRequiredFlag = false;
//        }

//        #endregion

//    }
//}
