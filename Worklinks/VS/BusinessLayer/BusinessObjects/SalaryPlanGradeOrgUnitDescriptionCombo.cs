﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SalaryPlanGradeOrgUnitDescriptionCombo : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long OrganizationUnitId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public string ImportExternalIdentifier { get; set; }

        [DataMember]
        public string EnglishDescription { get; set; }

        [DataMember]
        public string FrenchDescription { get; set; }

        #endregion

        #region construct/destruct
        public SalaryPlanGradeOrgUnitDescriptionCombo()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(SalaryPlanGradeOrgUnitDescriptionCombo data)
        {
            base.CopyTo(data);

            data.OrganizationUnitId = OrganizationUnitId;
            data.ImportExternalIdentifier = ImportExternalIdentifier;
            data.EnglishDescription = EnglishDescription;
            data.FrenchDescription = FrenchDescription;
        }
        public new void Clear()
        {
            base.Clear();

            OrganizationUnitId = -1;
            ImportExternalIdentifier = null;
            EnglishDescription = null;
            FrenchDescription = null;
        }
        #endregion
    }
}