﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class RemittanceDetailCraSourceDeductionsCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<RemittanceDetailCraSourceDeductions>
    {
    }
}
