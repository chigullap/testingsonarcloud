﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollExport: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PayrollExportId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long PayrollProcessId { get; set; }
        [DataMember]
        public long? SequenceNumber { get; set; }
        [DataMember]
        public byte[] Data { get; set; }

        [DataMember]
        public string EftType { get; set; }

        [DataMember]
        public bool FileSentFlag { get; set; }

        [DataMember]
        public string ProcessGroupDescription { get; set; }

        #endregion

        #region construct/destruct
        public PayrollExport()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();

            PayrollExportId = -1;
            PayrollProcessId = -1;
            SequenceNumber = null;
            Data = null;
            EftType = null;
            FileSentFlag = false;
            ProcessGroupDescription = null;
        }

        public virtual void CopyTo(PayrollExport data)
        {
            base.CopyTo(data);

            data.PayrollExportId = PayrollExportId;
            data.PayrollProcessId = PayrollProcessId;
            data.SequenceNumber = SequenceNumber;
            Data.CopyTo(data.Data,0);
            data.EftType = EftType;
            data.FileSentFlag = FileSentFlag;
            data.ProcessGroupDescription = ProcessGroupDescription;
        }
        #endregion
    }
}
