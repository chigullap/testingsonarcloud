﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class AccrualEntitlementDetailValueCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<AccrualEntitlementDetailValue>
    {
    }
}