﻿using System;
using FileHelpers;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    [DelimitedRecord(",")]
    public class PayrollTransactionImportUsedInService
    {
        [DataMember]
        public String EmployeeNumber;

        [DataMember]
        public String PaycodeCode;

        [DataMember]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public Decimal Units;

        [DataMember]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public Decimal Rate;

        [DataMember]
        [FieldConverter(ConverterKind.Date, "MM-dd-yyyy")]
        public DateTime TransactionDate;
    }
}
