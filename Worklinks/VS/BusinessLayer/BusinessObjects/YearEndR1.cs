﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class YearEndR1 : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long YearEndR1Id
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public String EmployeeNumber { get; set; }

        [DataMember]
        public Decimal Year { get; set; }

        [DataMember]
        public Decimal BoxAAmount { get; set; }

        [DataMember]
        public Decimal? BoxA1Amount { get; set; }

        [DataMember]
        public Decimal? BoxA2Amount { get; set; }

        [DataMember]
        public Decimal? BoxA3Amount { get; set; }

        [DataMember]
        public Decimal? BoxA4Amount { get; set; }

        [DataMember]
        public Decimal? BoxA5Amount { get; set; }

        [DataMember]
        public Decimal? BoxA6Amount { get; set; }

        [DataMember]
        public Decimal? BoxA7Amount { get; set; }

        [DataMember]
        public Decimal? BoxA8Amount { get; set; }

        [DataMember]
        public Decimal? BoxA9Amount { get; set; }

        [DataMember]
        public Decimal? BoxA10Amount { get; set; }

        [DataMember]
        public Decimal? BoxA11Amount { get; set; }

        [DataMember]
        public Decimal? BoxA12Amount { get; set; }

        [DataMember]
        public Decimal? BoxA13Amount { get; set; }

        [DataMember]
        public Decimal? BoxA14Amount { get; set; }

        [DataMember]
        public Decimal BoxBAmount { get; set; }

        [DataMember]
        public Decimal? BoxB1Amount { get; set; }

        [DataMember]
        public Decimal BoxCAmount { get; set; }

        [DataMember]
        public Decimal BoxDAmount { get; set; }

        [DataMember]
        public Decimal? BoxD1Amount { get; set; }

        [DataMember]
        public Decimal? BoxD2Amount { get; set; }

        [DataMember]
        public Decimal? BoxD3Amount { get; set; }

        [DataMember]
        public Decimal BoxEAmount { get; set; }

        [DataMember]
        public Decimal BoxFAmount { get; set; }

        [DataMember]
        public Decimal BoxGAmount { get; set; }

        [DataMember]
        public Decimal? BoxG1Amount { get; set; }

        [DataMember]
        public Decimal? BoxG2Amount { get; set; }

        [DataMember]
        public Decimal BoxHAmount { get; set; }

        [DataMember]
        public Decimal BoxIAmount { get; set; }

        [DataMember]
        public Decimal BoxJAmount { get; set; }

        [DataMember]
        public Decimal BoxKAmount { get; set; }

        [DataMember]
        public Decimal? BoxK1Amount { get; set; }

        [DataMember]
        public Decimal BoxLAmount { get; set; }

        [DataMember]
        public Decimal? BoxL2Amount { get; set; }

        [DataMember]
        public Decimal? BoxL3Amount { get; set; }

        [DataMember]
        public Decimal? BoxL4Amount { get; set; }

        [DataMember]
        public Decimal? BoxL5Amount { get; set; }

        [DataMember]
        public Decimal? BoxL7Amount { get; set; }

        [DataMember]
        public Decimal? BoxL8Amount { get; set; }

        [DataMember]
        public Decimal? BoxL9Amount { get; set; }

        [DataMember]
        public Decimal? BoxL10Amount { get; set; }

        [DataMember]
        public Decimal BoxMAmount { get; set; }

        [DataMember]
        public Decimal BoxNAmount { get; set; }

        [DataMember]
        public Decimal? BoxO2Amount { get; set; }

        [DataMember]
        public Decimal? BoxO3Amount { get; set; }

        [DataMember]
        public Decimal? BoxO4Amount { get; set; }

        [DataMember]
        public Decimal? BoxORJAmount { get; set; }

        [DataMember]
        public Decimal? BoxORQAmount { get; set; }

        [DataMember]
        public Decimal BoxPAmount { get; set; }

        [DataMember]
        public Decimal BoxQAmount { get; set; }

        [DataMember]
        public Decimal BoxRAmount { get; set; }

        [DataMember]
        public Decimal? BoxR1Amount { get; set; }

        [DataMember]
        public Decimal BoxSAmount { get; set; }

        [DataMember]
        public Decimal BoxTAmount { get; set; }

        [DataMember]
        public Decimal BoxUAmount { get; set; }

        [DataMember]
        public Decimal BoxVAmount { get; set; }

        [DataMember]
        public Decimal? BoxV1Amount { get; set; }

        [DataMember]
        public Decimal BoxWAmount { get; set; }

        [DataMember]
        public Decimal? Box200Amount { get; set; }

        [DataMember]
        public Decimal? Box201Amount { get; set; }

        [DataMember]
        public Decimal? Box211Amount { get; set; }

        [DataMember]
        public Decimal? Box235Amount { get; set; }

        [DataMember]
        public byte? GenerateXml { get; set; }

        [DataMember]
        public Int16? RecordStatus { get; set; }

        [DataMember]
        public int SlipNumber { get; set; }

        [DataMember]
        public Decimal EmployerProvincialPensionPlanAmount { get; set; }

        [DataMember]
        public Decimal EmployerProvincialParentalInsurancePlanAmount { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public int Revision { get; set; }

        [DataMember]
        public long? PreviousRevisionYearEndR1Id { get; set; }

        [DataMember]
        public int? PreviousSlipNumber { get; set; }

        [DataMember]
        public String R1BoxNumberO { get; set; }

        [DataMember]
        public Decimal? R1BoxAmountO { get; set; }

        [DataMember]
        public String R1BoxNumber1 { get; set; }

        [DataMember]
        public Decimal? R1BoxAmount1 { get; set; }

        [DataMember]
        public String R1BoxNumber2 { get; set; }

        [DataMember]
        public Decimal? R1BoxAmount2 { get; set; }

        [DataMember]
        public String R1BoxNumber3 { get; set; }

        [DataMember]
        public Decimal? R1BoxAmount3 { get; set; }

        [DataMember]
        public String R1BoxNumber4 { get; set; }

        [DataMember]
        public Decimal? R1BoxAmount4 { get; set; }
        #endregion

        #region construct/destruct
        public YearEndR1()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(YearEndR1 data)
        {
            base.CopyTo(data);

            data.YearEndR1Id = YearEndR1Id;
            data.EmployeeId = EmployeeId;
            data.EmployeeNumber = EmployeeNumber;
            data.Year = Year;
            data.BoxAAmount = BoxAAmount;
            data.BoxA1Amount = BoxA1Amount;
            data.BoxA2Amount = BoxA2Amount;
            data.BoxA3Amount = BoxA3Amount;
            data.BoxA4Amount = BoxA4Amount;
            data.BoxA5Amount = BoxA5Amount;
            data.BoxA6Amount = BoxA6Amount;
            data.BoxA7Amount = BoxA7Amount;
            data.BoxA8Amount = BoxA8Amount;
            data.BoxA9Amount = BoxA9Amount;
            data.BoxA10Amount = BoxA10Amount;
            data.BoxA11Amount = BoxA11Amount;
            data.BoxA12Amount = BoxA12Amount;
            data.BoxA13Amount = BoxA13Amount;
            data.BoxA14Amount = BoxA14Amount;
            data.BoxBAmount = BoxBAmount;
            data.BoxB1Amount = BoxB1Amount;
            data.BoxCAmount = BoxCAmount;
            data.BoxDAmount = BoxDAmount;
            data.BoxD1Amount = BoxD1Amount;
            data.BoxD2Amount = BoxD2Amount;
            data.BoxD3Amount = BoxD3Amount;
            data.BoxEAmount = BoxEAmount;
            data.BoxFAmount = BoxFAmount;
            data.BoxGAmount = BoxGAmount;
            data.BoxG1Amount = BoxG1Amount;
            data.BoxG2Amount = BoxG2Amount;
            data.BoxHAmount = BoxHAmount;
            data.BoxIAmount = BoxIAmount;
            data.BoxJAmount = BoxJAmount;
            data.BoxKAmount = BoxKAmount;
            data.BoxK1Amount = BoxK1Amount;
            data.BoxLAmount = BoxLAmount;
            data.BoxL2Amount = BoxL2Amount;
            data.BoxL3Amount = BoxL3Amount;
            data.BoxL4Amount = BoxL4Amount;
            data.BoxL5Amount = BoxL5Amount;
            data.BoxL7Amount = BoxL7Amount;
            data.BoxL8Amount = BoxL8Amount;
            data.BoxL9Amount = BoxL9Amount;
            data.BoxL10Amount = BoxL10Amount;
            data.BoxMAmount = BoxMAmount;
            data.BoxNAmount = BoxNAmount;
            data.BoxO2Amount = BoxO2Amount;
            data.BoxO3Amount = BoxO3Amount;
            data.BoxO4Amount = BoxO4Amount;
            data.BoxORJAmount = BoxORJAmount;
            data.BoxORQAmount = BoxORQAmount;
            data.BoxPAmount = BoxPAmount;
            data.BoxQAmount = BoxQAmount;
            data.BoxRAmount = BoxRAmount;
            data.BoxR1Amount = BoxR1Amount;
            data.BoxSAmount = BoxSAmount;
            data.BoxTAmount = BoxTAmount;
            data.BoxUAmount = BoxUAmount;
            data.BoxVAmount = BoxVAmount;
            data.BoxV1Amount = BoxV1Amount;
            data.BoxWAmount = BoxWAmount;
            data.Box200Amount = Box200Amount;
            data.Box201Amount = Box201Amount;
            data.Box211Amount = Box211Amount;
            data.Box235Amount = Box235Amount;
            data.GenerateXml = GenerateXml;
            data.RecordStatus = RecordStatus;
            data.SlipNumber = SlipNumber;
            data.EmployerProvincialPensionPlanAmount = EmployerProvincialPensionPlanAmount;
            data.EmployerProvincialParentalInsurancePlanAmount = EmployerProvincialParentalInsurancePlanAmount;
            data.ActiveFlag = ActiveFlag;
            data.Revision = Revision;
            data.PreviousRevisionYearEndR1Id = PreviousRevisionYearEndR1Id;
            data.PreviousSlipNumber = PreviousSlipNumber;
            data.R1BoxNumberO = R1BoxNumberO;
            data.R1BoxAmountO = R1BoxAmountO;
            data.R1BoxNumber1 = R1BoxNumber1;
            data.R1BoxAmount1 = R1BoxAmount1;
            data.R1BoxNumber2 = R1BoxNumber2;
            data.R1BoxAmount2 = R1BoxAmount2;
            data.R1BoxNumber3 = R1BoxNumber3;
            data.R1BoxAmount3 = R1BoxAmount3;
            data.R1BoxNumber4 = R1BoxNumber4;
            data.R1BoxAmount4 = R1BoxAmount4;
        }
        public new void Clear()
        {
            base.Clear();

            YearEndR1Id = -1;
            EmployeeId = -1;
            EmployeeNumber = null;
            Year = 0;
            BoxAAmount = 0;
            BoxA1Amount = null;
            BoxA2Amount = null;
            BoxA3Amount = null;
            BoxA4Amount = null;
            BoxA5Amount = null;
            BoxA6Amount = null;
            BoxA7Amount = null;
            BoxA8Amount = null;
            BoxA9Amount = null;
            BoxA10Amount = null;
            BoxA11Amount = null;
            BoxA12Amount = null;
            BoxA13Amount = null;
            BoxA14Amount = null;
            BoxBAmount = 0;
            BoxB1Amount = null;
            BoxCAmount = 0;
            BoxDAmount = 0;
            BoxD1Amount = null;
            BoxD2Amount = null;
            BoxD3Amount = null;
            BoxEAmount = 0;
            BoxFAmount = 0;
            BoxGAmount = 0;
            BoxG1Amount = null;
            BoxG2Amount = null;
            BoxHAmount = 0;
            BoxIAmount = 0;
            BoxJAmount = 0;
            BoxKAmount = 0;
            BoxK1Amount = null;
            BoxLAmount = 0;
            BoxL2Amount = null;
            BoxL3Amount = null;
            BoxL4Amount = null;
            BoxL5Amount = null;
            BoxL7Amount = null;
            BoxL8Amount = null;
            BoxL9Amount = null;
            BoxL10Amount = null;
            BoxMAmount = 0;
            BoxNAmount = 0;
            BoxO2Amount = null;
            BoxO3Amount = null;
            BoxO4Amount = null;
            BoxORJAmount = null;
            BoxORQAmount = null;
            BoxPAmount = 0;
            BoxQAmount = 0;
            BoxRAmount = 0;
            BoxR1Amount = null;
            BoxSAmount = 0;
            BoxTAmount = 0;
            BoxUAmount = 0;
            BoxVAmount = 0;
            BoxV1Amount = null;
            BoxWAmount = 0;
            Box200Amount = null;
            Box201Amount = null;
            Box211Amount = null;
            Box235Amount = null;
            GenerateXml = null;
            RecordStatus = null;
            SlipNumber = 0;
            EmployerProvincialPensionPlanAmount = 0;
            EmployerProvincialParentalInsurancePlanAmount = 0;
            ActiveFlag = true;
            Revision = 0;
            PreviousRevisionYearEndR1Id = null;
            PreviousSlipNumber = null;
            R1BoxNumberO = null;
            R1BoxAmountO = null;
            R1BoxNumber1 = null;
            R1BoxAmount1 = null;
            R1BoxNumber2 = null;
            R1BoxAmount2 = null;
            R1BoxNumber3 = null;
            R1BoxAmount3 = null;
            R1BoxNumber4 = null;
            R1BoxAmount4 = null;
        }
        #endregion
    }
}