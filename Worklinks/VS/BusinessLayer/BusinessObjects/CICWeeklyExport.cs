﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CICWeeklyExport : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String EmployeeNumber { get; set; }

        [DataMember]
        public String GovernmentIdentificationNumber1 { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public String Status { get; set; }

        #endregion

        #region construct/destruct
        public CICWeeklyExport()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(CICWeeklyExport data)
        {
            data.EmployeeId = EmployeeId;
            data.EmployeeNumber = EmployeeNumber;
            data.GovernmentIdentificationNumber1 = GovernmentIdentificationNumber1;
            data.BirthDate = BirthDate;
            data.Status = Status;
        }
        public new void Clear()
        {
            EmployeeId = -1;
            EmployeeNumber = null;
            GovernmentIdentificationNumber1 = null;
            BirthDate = null;
            Status = null;
        }
        #endregion

    }
}
