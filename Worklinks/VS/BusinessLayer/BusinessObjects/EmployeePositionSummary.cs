﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeePositionSummary : EmployeePosition
    {

        #region properties
        
        [DataMember]
        public String OrganizationUnitDescription {get;set;}

        #endregion

        #region construct/destruct
        public EmployeePositionSummary()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            EmployeePositionSummary pos = new EmployeePositionSummary();
            this.CopyTo(pos);
            return pos;
        }
        public void CopyTo(EmployeePositionSummary data)
        {
            base.CopyTo(data);
            data.OrganizationUnitDescription = OrganizationUnitDescription;

        }
        public new void Clear()
        {
            base.Clear();
            OrganizationUnitDescription = null;

        }
        #endregion
    }
}
