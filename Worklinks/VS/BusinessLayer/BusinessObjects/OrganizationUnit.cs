﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class OrganizationUnit : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        //these ints represent the index for the English and French descriptions in the "Description" object, which is a "OrganizationUnitLevelDescriptionCollection"
        private static string _englishCd = "EN";
        private static string _frenchCd = "FR";
        private OrganizationUnitDescriptionCollection _descriptions;

        #region properties
        [DataMember]
        public long OrganizationUnitId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public bool ActiveFlag { get; set; }
        [DataMember]
        public bool HasChildrenFlag { get; set; }
        [DataMember]
        public long OrganizationUnitLevelId { get; set; }
        [DataMember]
        public String LanguageCode { get; set; }
        [DataMember]
        public String Description { get; set; }
        [DataMember]
        public String GeneralLedgerSegment { get; set; }

        //Needed here for population of the Grid (passing values to nodes when selecting grid items)
        [DataMember]
        public int IsAssociatedToLevelFlag { get; set; }
        [DataMember]
        public long? ParentOrganizationUnitId { get; set; }  //Reflects organization unit id of the parent for this unit
        [DataMember]
        public String ImportExternalIdentifier { get; set; }
        [DataMember]
        public OrganizationUnitDescriptionCollection Descriptions
        {
            get
            {
                if (_descriptions == null)
                    _descriptions = new OrganizationUnitDescriptionCollection();
                return _descriptions;
            }
            set { _descriptions = value; }
        }
        [DataMember]
        public string EnglishDescription
        {
            get
            {
                return Descriptions[_englishCd] == null ? null : Descriptions[_englishCd].UnitDescription;
            }
            set
            {
                if (EnglishDescription == null)
                {
                    Descriptions.Add(new OrganizationUnitDescription() { LanguageCode = _englishCd, OrganizationUnitId = OrganizationUnitId, CreateUser = UpdateUser, UpdateUser = UpdateUser, UnitDescription = value });
                }
                else
                {
                    Descriptions[_englishCd].UpdateUser = UpdateUser;
                    Descriptions[_englishCd].UnitDescription = value;
                }
            }
        }
        [DataMember]
        public string FrenchDescription
        {
            get
            {
                return Descriptions[_frenchCd] == null ? null : Descriptions[_frenchCd].UnitDescription;
            }
            set
            {
                if (FrenchDescription == null)
                {
                    Descriptions.Add(new OrganizationUnitDescription() { LanguageCode = _frenchCd, OrganizationUnitId = OrganizationUnitId, CreateUser = UpdateUser, UpdateUser = UpdateUser, UnitDescription = value });
                }
                else
                {
                    Descriptions[_frenchCd].UpdateUser = UpdateUser;
                    Descriptions[_frenchCd].UnitDescription = value;
                }
            }
        }
        #endregion

        #region construct/destruct
        public OrganizationUnit()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            OrganizationUnit organizationUnit = new OrganizationUnit();
            this.CopyTo(organizationUnit);
            return organizationUnit;
        }
        public void CopyTo(OrganizationUnit data)
        {
            base.CopyTo(data);
            data.OrganizationUnitId = OrganizationUnitId;
            data.HasChildrenFlag = HasChildrenFlag;
            data.ActiveFlag = ActiveFlag;
            data.OrganizationUnitLevelId = OrganizationUnitLevelId;
            data.LanguageCode = LanguageCode;
            data.Description = Description;
            data.IsAssociatedToLevelFlag = IsAssociatedToLevelFlag;
            data.ParentOrganizationUnitId = ParentOrganizationUnitId;
            data.ImportExternalIdentifier = ImportExternalIdentifier;
            if (Descriptions != null)
            {
                data.Descriptions = new OrganizationUnitDescriptionCollection();
                Descriptions.CopyTo(data.Descriptions);
            }
        }
        public new void Clear()
        {
            base.Clear();
            OrganizationUnitId = -1;
            HasChildrenFlag = false;
            ActiveFlag = false;
            OrganizationUnitLevelId = -1;
            LanguageCode = null;
            Description = null;
            IsAssociatedToLevelFlag = -1;
            ParentOrganizationUnitId = null;
            Descriptions = null;
            ImportExternalIdentifier = null;
        }
        #endregion
    }
}
