﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;

namespace WorkLinks.BusinessLayer.BusinessObjects.Import
{
    public class PaycodeImportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PaycodeImport>
    {
        public PaycodeImport GetFirstPaycodeImport(String paycodeCode, String employeeIdentifier)
        {
            foreach (PaycodeImport import in this)
            {
                if (import.PaycodeCode==paycodeCode && import.EmployeeIdentifier==employeeIdentifier)
                    return import;
            }

            return null;
        }


        public PaycodeImportCollection EmployeePaycodeSummary
        {
            get
            {
                PaycodeImportCollection rtn = new PaycodeImportCollection();

                foreach (PaycodeImport import in this)
                {
                    if (import.ImportType.Equals(PaycodeImport.PaycodeImportType.EmployeePaycode)&&import.PaycodeCode!=null/*&&import.AmountRate!=0*/)
                    {
                        if (rtn.GetFirstPaycodeImport(import.PaycodeCode, import.EmployeeIdentifier)!=null)
                        {
                            rtn.GetFirstPaycodeImport(import.PaycodeCode, import.EmployeeIdentifier).AmountRate += import.AmountRate;
                        }
                        else
                        {
                            rtn.Add((PaycodeImport)import.Clone());
                        }
                    }
                }
                return rtn;
            }
        }

        public PaycodeImportCollection PayrollBatchItems
        {
            get
            {
                PaycodeImportCollection rtn = new PaycodeImportCollection();

                foreach (PaycodeImport import in this)
                {
                    if (import.ImportType.Equals(PaycodeImport.PaycodeImportType.Batch) && import.PaycodeCode != null && import.AmountRate != 0)
                    {
                        rtn.Add((PaycodeImport)import.Clone());
                    }
                }
                return rtn;
            }
        }

        public String EmployeePaycodeCSV
        {
            get
            {
                if (EmployeePaycodeSummary.Count > 0)
                {
                    StringBuilder csvText = new StringBuilder();
                    //add header
                    csvText.AppendLine("EmployeeID,PayCode,Amount");

                    foreach (PaycodeImport import in EmployeePaycodeSummary)
                    {
                        csvText.AppendLine(String.Format("\"{0}\",\"{1}\",{2}", import.EmployeeIdentifier, import.PaycodeCode, import.AmountRate));
                    }

                    return csvText.ToString();
                }
                else
                {
                    return null;
                }
            }

        }
        public String PayrollBatchCSV
        {
            get
            {
                if (PayrollBatchItems.Count > 0)
                {
                    StringBuilder csvText = new StringBuilder();
                    //add header
                    csvText.AppendLine("EmployeeNumber,PayCode,Units,AmountRate");

                    foreach (PaycodeImport import in PayrollBatchItems)
                    {
                        csvText.AppendLine(String.Format("\"{0}\",\"{1}\",{2},{3}", import.EmployeeIdentifier, import.PaycodeCode, import.Units, import.AmountRate));
                    }

                    return csvText.ToString();
                }
                else
                {
                    return null;
                }
            }
        }

    }
}
