﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;

namespace WorkLinks.BusinessLayer.BusinessObjects.Import
{
    public class ExternalPaycodeImportMapCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<ExternalPaycodeImportMap>
    {

        //should have a special lookup for 4 levels, we'll start with 2
        private Dictionary<String,Dictionary<String, ExternalPaycodeImportMap>> _twoKeyLookup = null;

        private Dictionary<String, Dictionary<String, ExternalPaycodeImportMap>> TwoKeyLookup
        {
            get
            {
                if (_twoKeyLookup == null)
                    _twoKeyLookup = new Dictionary<string, Dictionary<string, ExternalPaycodeImportMap>>();
                return _twoKeyLookup;
            }
        }


        public override void Add(ExternalPaycodeImportMap data)
        {
            base.Add(data);

            if (!TwoKeyLookup.ContainsKey(data.ExternalIdentifier1))
                TwoKeyLookup.Add(data.ExternalIdentifier1, new Dictionary<string, ExternalPaycodeImportMap>());
            TwoKeyLookup[data.ExternalIdentifier1].Add(data.ExternalIdentifier2, data);
        }

        public ExternalPaycodeImportMap GetExternalPaycodeImportMap(String identifer1, String identifier2)
        {
            if (TwoKeyLookup.ContainsKey(identifer1) && TwoKeyLookup[identifer1].ContainsKey(identifier2))
                return TwoKeyLookup[identifer1][identifier2];
            else
                return null;
        }
    }
}
