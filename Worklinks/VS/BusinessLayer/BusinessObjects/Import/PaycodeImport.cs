﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects.Import
{
    public class PaycodeImport : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        public enum PaycodeImportType
        {
            EmployeePaycode,
            Batch,
        }

        [DataMember]
        public long PaycodeImportId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String EmployeeIdentifier { get; set; }
        [DataMember]
        public Decimal AmountRate { get; set; }
        [DataMember]
        public Decimal Units { get; set; }
        [DataMember]
        public ExternalPaycodeImportMap ExternalPaycodeImportMap { get; set; }
        [DataMember]
        public PaycodeImportType ImportType { get; set; }

        public String PaycodeCode
        {
            get 
            {
                if (ExternalPaycodeImportMap == null)
                    return null;
                else
                    return ExternalPaycodeImportMap.PaycodeCode;
            }
        }


        #region construct/destruct


        public PaycodeImport()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            PaycodeImport import = new PaycodeImport();
            this.CopyTo(import);
            return import;
        }
        public void CopyTo(PaycodeImport data)
        {
            base.CopyTo(data);
            data.PaycodeImportId = PaycodeImportId;
            data.EmployeeIdentifier = EmployeeIdentifier;
            data.AmountRate = AmountRate;
            data.Units = Units;
            if (ExternalPaycodeImportMap == null)
            {
                data.ExternalPaycodeImportMap = null;
            }
            else
            {
                data.ExternalPaycodeImportMap = new ExternalPaycodeImportMap();
                ExternalPaycodeImportMap.CopyTo(data.ExternalPaycodeImportMap);
            }
            data.ImportType = ImportType;
        }
        public new void Clear()
        {
            base.Clear();
            PaycodeImportId = -1;
            EmployeeIdentifier = null;
            AmountRate = 0;
            Units = 1;
            ExternalPaycodeImportMap = null;
            ImportType = PaycodeImportType.EmployeePaycode;
        }

        #endregion

        
    }
}
