﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects.Import
{
    [Serializable]
    [DataContract]
    public class AdvantageTimeRecord : BusinessObject
    {
        #region properties

        [DataMember]
        public long AdvantageTimeRecordId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long? ImportExportLogId { get; set; }

        [DataMember]
        public AdvantageTimeRecordDetailCollection AdvantageTimeRecordDetails { get; set; }

        [DataMember]
        public String ImportFileName { get; set; }

        #endregion

        #region construct/destruct

        public AdvantageTimeRecord()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone

        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(AdvantageTimeRecord data)
        {
            base.CopyTo(data);

            data.AdvantageTimeRecordId = AdvantageTimeRecordId;
            data.ImportExportLogId = ImportExportLogId;
            data.AdvantageTimeRecordDetails = AdvantageTimeRecordDetails;
            data.ImportFileName = ImportFileName;
        }

        public new void Clear()
        {
            base.Clear();

            AdvantageTimeRecordId = -1;
            ImportExportLogId = null;
            AdvantageTimeRecordDetails = null;
            ImportFileName = null;
        }

        #endregion
    }
}
