﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Import
{
    [Serializable]
    [DataContract]
    public class ExternalPaycodeImportMap: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        
        #region properties
        [DataMember]
        public long ExternalPaycodeImportMapId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long ExternalPaycodeImportId { get; set; }
        [DataMember]
        public String ExternalIdentifier1 { get; set; }
        [DataMember]
        public String ExternalIdentifier2 { get; set; }
        [DataMember]
        public String ExternalIdentifier3 { get; set; }
        [DataMember]
        public String ExternalIdentifier4 { get; set; }
        [DataMember]
        public String PaycodeCode { get; set; }

        #endregion

        #region construct/destruct
        public ExternalPaycodeImportMap()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(ExternalPaycodeImportMap data)
        {
            base.CopyTo(data);

            data.ExternalPaycodeImportMapId = ExternalPaycodeImportMapId;
            data.ExternalPaycodeImportId = ExternalPaycodeImportId;
            data.ExternalIdentifier1 = ExternalIdentifier1;
            data.ExternalIdentifier2 = ExternalIdentifier2;
            data.ExternalIdentifier3 =  ExternalIdentifier3;
            data.ExternalIdentifier4 =  ExternalIdentifier4;
            data.PaycodeCode = PaycodeCode;

        }
        public new void Clear()
        {
            base.Clear();

            ExternalPaycodeImportMapId = -1;
            ExternalPaycodeImportId = -1;
            ExternalIdentifier1 = null;
            ExternalIdentifier2 = null; 
            ExternalIdentifier3 =  null;
            ExternalIdentifier4 =  null;
            PaycodeCode = null;
        }

        #endregion

    }
}
