﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects.Import
{
    [Serializable]
    [DataContract]
    public class AdvantageTimeRecordDetail : BusinessObject
    {
        #region properties

        [DataMember]
        public long AdvantageTimeRecordDetailId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long AdvantageTimeRecordId { get; set; }

        [DataMember]
        public DateTime TransactionDate { get; set; }

        [DataMember]
        public String FullName { get; set; }

        [DataMember]
        public String UniqueIdentifier { get; set; }

        [DataMember]
        [Required]
        public String EmployeeImportExternalIdentifier { get; set; }

        [DataMember]
        public String PayrollPin { get; set; }

        [DataMember]
        public Decimal? LoggedIn { get; set; }

        [DataMember]
        public Decimal? TotalPayable { get; set; }

        [DataMember]
        public Decimal? TotalNonPayable { get; set; }

        [DataMember]
        public Decimal? TotalTraining { get; set; }

        [DataMember]
        public Decimal? TotalNonBillable { get; set; }

        [DataMember]
        public Decimal? TotalBillable { get; set; }

        [DataMember]
        public Decimal? TimeCode00 { get; set; }

        [DataMember]
        public Decimal? TimeCode01 { get; set; }

        [DataMember]
        public Decimal? TimeCode02 { get; set; }

        [DataMember]
        public Decimal? TimeCode03 { get; set; }

        [DataMember]
        public Decimal? TimeCode04 { get; set; }

        [DataMember]
        public Decimal? TimeCode05 { get; set; }

        [DataMember]
        public Decimal? TimeCode06 { get; set; }

        [DataMember]
        public Decimal? TimeCode07 { get; set; }

        [DataMember]
        public Decimal? TimeCode08 { get; set; }

        [DataMember]
        public Decimal? TimeCode09 { get; set; }

        [DataMember]
        public Decimal? TimeCode10 { get; set; }

        [DataMember]
        public Decimal? TimeCode11 { get; set; }

        [DataMember]
        public Decimal? TimeCode12 { get; set; }

        [DataMember]
        public Decimal? TimeCode13 { get; set; }

        [DataMember]
        public Decimal? TimeCode14 { get; set; }

        [DataMember]
        public Decimal? TimeCode15 { get; set; }

        [DataMember]
        public Decimal? TimeCode16 { get; set; }

        [DataMember]
        public Decimal? TimeCode17 { get; set; }

        [DataMember]
        public Decimal? TimeCode18 { get; set; }

        [DataMember]
        public Decimal? TimeCode19 { get; set; }

        [DataMember]
        public Decimal? TimeCode20 { get; set; }

        [DataMember]
        public Decimal? TimeCode21 { get; set; }

        [DataMember]
        public Decimal? TimeCode22 { get; set; }

        [DataMember]
        public Decimal? TimeCode23 { get; set; }

        [DataMember]
        public Decimal? TimeCode24 { get; set; }

        [DataMember]
        public Decimal? TimeCode25 { get; set; }

        [DataMember]
        public Decimal? TimeCode26 { get; set; }

        [DataMember]
        public Decimal? TimeCode27 { get; set; }

        [DataMember]
        public Decimal? TimeCode28 { get; set; }

        [DataMember]
        public Decimal? TimeCode29 { get; set; }

        [DataMember]
        public Decimal? TimeCode30 { get; set; }

        [DataMember]
        public Decimal? TimeCode31 { get; set; }

        [DataMember]
        public Decimal? TimeCode32 { get; set; }

        [DataMember]
        public Decimal? TimeCode33 { get; set; }

        [DataMember]
        public Decimal? TimeCode34 { get; set; }

        [DataMember]
        public Decimal? TimeCode35 { get; set; }

        [DataMember]
        public Decimal? TimeCode36 { get; set; }

        [DataMember]
        public Decimal? TimeCode37 { get; set; }

        [DataMember]
        public Decimal? TimeCode38 { get; set; }

        [DataMember]
        public Decimal? TimeCode39 { get; set; }

        [DataMember]
        public Decimal? TimeCode40 { get; set; }

        [DataMember]
        public Decimal? TimeCode41 { get; set; }

        [DataMember]
        public Decimal? TimeCode42 { get; set; }

        [DataMember]
        public Decimal? TimeCode43 { get; set; }

        [DataMember]
        public Decimal? TimeCode44 { get; set; }

        [DataMember]
        public Decimal? TimeCode45 { get; set; }

        [DataMember]
        public Decimal? TimeCode46 { get; set; }

        [DataMember]
        public Decimal? TimeCode47 { get; set; }

        [DataMember]
        public Decimal? TimeCode48 { get; set; }

        [DataMember]
        public Decimal? TimeCode49 { get; set; }

        [DataMember]
        public Decimal? TimeCode50 { get; set; }

        #endregion

        #region construct/destruct

        public AdvantageTimeRecordDetail()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone

        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(AdvantageTimeRecordDetail data)
        {
            base.CopyTo(data);

            data.AdvantageTimeRecordDetailId = AdvantageTimeRecordDetailId;
            data.AdvantageTimeRecordId = AdvantageTimeRecordId;
            data.TransactionDate = TransactionDate;
            data.FullName = FullName;
            data.UniqueIdentifier = UniqueIdentifier;
            data.EmployeeImportExternalIdentifier = EmployeeImportExternalIdentifier;
            data.PayrollPin = PayrollPin;
            data.LoggedIn = LoggedIn;
            data.TotalPayable = TotalPayable;
            data.TotalNonPayable = TotalNonPayable;
            data.TotalTraining = TotalTraining;
            data.TotalNonBillable = TotalNonBillable;
            data.TotalBillable = TotalBillable;
            data.TimeCode00 = TimeCode00;
            data.TimeCode01 = TimeCode01;
            data.TimeCode02 = TimeCode02;
            data.TimeCode03 = TimeCode03;
            data.TimeCode04 = TimeCode04;
            data.TimeCode05 = TimeCode05;
            data.TimeCode06 = TimeCode06;
            data.TimeCode07 = TimeCode07;
            data.TimeCode08 = TimeCode08;
            data.TimeCode09 = TimeCode09;
            data.TimeCode10 = TimeCode10;
            data.TimeCode11 = TimeCode11;
            data.TimeCode12 = TimeCode12;
            data.TimeCode13 = TimeCode13;
            data.TimeCode14 = TimeCode14;
            data.TimeCode15 = TimeCode15;
            data.TimeCode16 = TimeCode16;
            data.TimeCode17 = TimeCode17;
            data.TimeCode18 = TimeCode18;
            data.TimeCode19 = TimeCode19;
            data.TimeCode20 = TimeCode20;
            data.TimeCode21 = TimeCode21;
            data.TimeCode22 = TimeCode22;
            data.TimeCode23 = TimeCode23;
            data.TimeCode24 = TimeCode24;
            data.TimeCode25 = TimeCode25;
            data.TimeCode26 = TimeCode26;
            data.TimeCode27 = TimeCode27;
            data.TimeCode28 = TimeCode28;
            data.TimeCode29 = TimeCode29;
            data.TimeCode30 = TimeCode30;
            data.TimeCode31 = TimeCode31;
            data.TimeCode32 = TimeCode32;
            data.TimeCode33 = TimeCode33;
            data.TimeCode34 = TimeCode34;
            data.TimeCode35 = TimeCode35;
            data.TimeCode36 = TimeCode36;
            data.TimeCode37 = TimeCode37;
            data.TimeCode38 = TimeCode38;
            data.TimeCode39 = TimeCode39;
            data.TimeCode40 = TimeCode40;
            data.TimeCode41 = TimeCode41;
            data.TimeCode42 = TimeCode42;
            data.TimeCode43 = TimeCode43;
            data.TimeCode44 = TimeCode44;
            data.TimeCode45 = TimeCode45;
            data.TimeCode46 = TimeCode46;
            data.TimeCode47 = TimeCode47;
            data.TimeCode48 = TimeCode48;
            data.TimeCode49 = TimeCode49;
            data.TimeCode50 = TimeCode50;
        }

        public new void Clear()
        {
            base.Clear();

            AdvantageTimeRecordDetailId = -1;
            AdvantageTimeRecordId = -1;
            TransactionDate = DateTime.Now;
            FullName = null;
            UniqueIdentifier = null;
            EmployeeImportExternalIdentifier = null;
            PayrollPin = null;
            LoggedIn = null;
            TotalPayable = null;
            TotalNonPayable = null;
            TotalTraining = null;
            TotalNonBillable = null;
            TotalBillable = null;
            TimeCode00 = null;
            TimeCode01 = null;
            TimeCode02 = null;
            TimeCode03 = null;
            TimeCode04 = null;
            TimeCode05 = null;
            TimeCode06 = null;
            TimeCode07 = null;
            TimeCode08 = null;
            TimeCode09 = null;
            TimeCode10 = null;
            TimeCode11 = null;
            TimeCode12 = null;
            TimeCode13 = null;
            TimeCode14 = null;
            TimeCode15 = null;
            TimeCode16 = null;
            TimeCode17 = null;
            TimeCode18 = null;
            TimeCode19 = null;
            TimeCode20 = null;
            TimeCode21 = null;
            TimeCode22 = null;
            TimeCode23 = null;
            TimeCode24 = null;
            TimeCode25 = null;
            TimeCode26 = null;
            TimeCode27 = null;
            TimeCode28 = null;
            TimeCode29 = null;
            TimeCode30 = null;
            TimeCode31 = null;
            TimeCode32 = null;
            TimeCode33 = null;
            TimeCode34 = null;
            TimeCode35 = null;
            TimeCode36 = null;
            TimeCode37 = null;
            TimeCode38 = null;
            TimeCode39 = null;
            TimeCode40 = null;
            TimeCode41 = null;
            TimeCode42 = null;
            TimeCode43 = null;
            TimeCode44 = null;
            TimeCode45 = null;
            TimeCode46 = null;
            TimeCode47 = null;
            TimeCode48 = null;
            TimeCode49 = null;
            TimeCode50 = null;
        }

        #endregion
    }
}
