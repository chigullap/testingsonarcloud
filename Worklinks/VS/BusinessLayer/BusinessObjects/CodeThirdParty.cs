﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CodeThirdParty : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public string CodeThirdPartyCd
        {
            get { return (string)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public string CodeProvinceStateCode { get; set; }

        [DataMember]
        public string EnglishDescription { get; set; }

        [DataMember]
        public string FrenchDescription { get; set; }

        [DataMember]
        public string AccountNumber { get; set; }

        [DataMember]
        public decimal SortOrder { get; set; }

        [DataMember]
        public string ImportExternalIdentifier { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public string CodePaycodeCode { get; set; }

        [DataMember]
        public bool ExcludeFromRemittanceExportFlag { get; set; }

        [DataMember]
        public long? VendorId { get; set; }

        [DataMember]
        public bool SystemFlag { get; set; }

        [DataMember]
        public string CodeWcbChequeRemittanceFrequencyCd { get; set; }

        [DataMember]
        public string CodeRemittanceMethodCd { get; set; }

        [DataMember]
        public string CodeThirdPartyBankCd { get; set; }

        [DataMember]
        public string CodeBankingCountryCd { get; set; }

        [DataMember]
        public string ThirdPartyTransitNumber { get; set; }

        [DataMember]
        public string ThirdPartyAccountNumber { get; set; }

        [DataMember]
        public long? BusinessNumberId { get; set; }

        #endregion

        #region construct/destruct
        public CodeThirdParty()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(CodeThirdParty data)
        {
            base.CopyTo(data);

            data.CodeThirdPartyCd = CodeThirdPartyCd;
            data.CodeProvinceStateCode = CodeProvinceStateCode;
            data.EnglishDescription = EnglishDescription;
            data.FrenchDescription = FrenchDescription;
            data.AccountNumber = AccountNumber;
            data.SortOrder = SortOrder;
            data.ImportExternalIdentifier = ImportExternalIdentifier;
            data.ActiveFlag = ActiveFlag;
            data.CodePaycodeCode = CodePaycodeCode;
            data.ExcludeFromRemittanceExportFlag = ExcludeFromRemittanceExportFlag;
            data.VendorId = VendorId;
            data.SystemFlag = SystemFlag;
            data.CodeWcbChequeRemittanceFrequencyCd = CodeWcbChequeRemittanceFrequencyCd;
            data.CodeRemittanceMethodCd = CodeRemittanceMethodCd;
            data.CodeThirdPartyBankCd = CodeThirdPartyBankCd;
            data.CodeBankingCountryCd = CodeBankingCountryCd;
            data.ThirdPartyTransitNumber = ThirdPartyTransitNumber;
            data.ThirdPartyAccountNumber = ThirdPartyAccountNumber;
            data.BusinessNumberId = BusinessNumberId;
        }
        public new void Clear()
        {
            base.Clear();

            CodeThirdPartyCd = String.Empty;
            CodeProvinceStateCode = null;
            EnglishDescription = null;
            FrenchDescription = null;
            AccountNumber = null;
            SortOrder = 0;
            ImportExternalIdentifier = null;
            ActiveFlag = true;
            CodePaycodeCode = null;
            ExcludeFromRemittanceExportFlag = false;
            VendorId = null;
            SystemFlag = false;
            CodeWcbChequeRemittanceFrequencyCd = null;
            CodeRemittanceMethodCd = null;
            CodeThirdPartyBankCd = null;
            CodeBankingCountryCd = null;
            ThirdPartyTransitNumber = null;
            ThirdPartyAccountNumber = null;
            BusinessNumberId = null;
        }
        #endregion
    }
}
