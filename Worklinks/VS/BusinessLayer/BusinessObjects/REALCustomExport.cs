﻿using System;
using System.Runtime.Serialization;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class REALCustomExport : BusinessObject
    {
        #region properties

        [DataMember]
        public long RowNumber
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public string EmployeeNumber { get; set; }
        [DataMember]
        public string EmployeeFirstName { get; set; }
        [DataMember]
        public string EmployeeLastName { get; set; }
        [DataMember]
        public DateTime TransactionDate { get; set; }
        [DataMember]
        public string PayCodeType { get; set; }
        [DataMember]
        public string PayCode { get; set; }
        [DataMember]
        public string PayCodeDescription { get; set; }
        [DataMember]
        public string CurrentHours { get; set; }
        [DataMember]
        public string CurrentRate { get; set; }
        [DataMember]
        public string CurrentAmount { get; set; }
        [DataMember]
        public string CostCentreNumber { get; set; }
        [DataMember]
        public string CostCentreDescription { get; set; }
        [DataMember]
        public string PayPeriod { get; set; }

        #endregion

        #region construct/destruct
        public REALCustomExport()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();

            RowNumber = -1;
            EmployeeNumber = null;
            EmployeeFirstName = null;
            EmployeeLastName = null;
            TransactionDate = DateTime.MinValue;
            PayCodeType = null;
            PayCode = null;
            PayCodeDescription = null;
            CurrentHours = null;
            CurrentRate = null;
            CurrentAmount = null;
            CostCentreNumber = null;
            CostCentreDescription = null;
            PayPeriod = null;
        }
        #endregion
    }
}