﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using  WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class Contact : Person
    {
        #region fields
        private long _contactId = -1;
        #endregion

        #region properties
        public override string Key
        {
            get
            {
                return ContactId.ToString();
            }
        }

        [DataMember]
        public long ContactId 
        {
            get { return _contactId; }
            set
            {
                KeyChangedEventArgs args = new KeyChangedEventArgs();
                args.OldKey = _contactId.ToString();
                _contactId = value;
                args.NewKey = Key;
                if (!(args.OldKey ?? String.Empty).Equals(args.NewKey ?? String.Empty) && this.GetType().Equals(typeof(Contact)))
                    OnKeyChanged(args);
            }
        }
        [DataMember]
        public long EmployeeId {get;set;}
        [DataMember]
        public DateTime EffectiveDate {get;set;}
        [DataMember]
        public String ContactRelationshipCode {get;set;}
        /*need this to hold unique rowversion from contact table*/
        [DataMember]
        public new byte[] RowVersion { get; set; }

        #endregion

        #region construct/destruct
        public Contact()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone

        public new void Clear()
        {
            base.Clear();
            ContactId = -1;
            EmployeeId = -1;
            EffectiveDate = DateTime.Now;
            ContactRelationshipCode = null;
            RowVersion = new byte[8];
        }

        public void CopyTo(Contact data)
        {
            base.CopyTo(data);
            data.ContactId = ContactId;
            data.EmployeeId = EmployeeId;
            data.EffectiveDate = EffectiveDate;
            data.ContactRelationshipCode = ContactRelationshipCode;
            data.RowVersion = this.RowVersion;
        }

        #endregion
    }
}
