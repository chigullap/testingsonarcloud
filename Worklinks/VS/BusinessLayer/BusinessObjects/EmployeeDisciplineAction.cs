﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeDisciplineAction: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        
        #region properties
        [DataMember]
        public long EmployeeDisciplineActionId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long EmployeeDisciplineId { get; set; }
        [DataMember]
        public String DisciplineActionStepCode { get; set; }
        [DataMember]
        public DateTime? ActionDate { get; set; }
        [DataMember]
        public String DiscussedWith { get; set; }
        [DataMember]
        public String Note { get; set; }

        #endregion

        #region construct/destruct
        public EmployeeDisciplineAction()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeeDisciplineAction data)
        {
            base.CopyTo(data);

            data.EmployeeDisciplineActionId = EmployeeDisciplineActionId;
            data.EmployeeDisciplineId = EmployeeDisciplineId;
            data.DisciplineActionStepCode = DisciplineActionStepCode;
            data.ActionDate = ActionDate;
            data.DiscussedWith = DiscussedWith;
            data.Note = Note;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeeDisciplineActionId = -1;
            EmployeeDisciplineId = -1;
            DisciplineActionStepCode = null;
            ActionDate = null;
            DiscussedWith = null;
            Note = null;
        }

        #endregion

    }
}
