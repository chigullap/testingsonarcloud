﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class SalaryPlanGradeCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<SalaryPlanGrade>
    {
    }
}