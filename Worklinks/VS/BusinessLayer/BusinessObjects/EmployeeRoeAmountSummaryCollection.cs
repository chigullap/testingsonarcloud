﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeeRoeAmountSummaryCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeeRoeAmountSummary>
    {
    }
}
