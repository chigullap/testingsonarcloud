﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class CraWcbRemittanceSummaryCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<CraWcbRemittanceSummary>
    {
        public decimal SumPaymentAmount
        {
            get
            {
                Decimal rtn = 0;

                foreach (CraWcbRemittanceSummary item in this)
                {
                    rtn += item.CurrentPremium;
                }

                return rtn;
            }
        }
    }
}
