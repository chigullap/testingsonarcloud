﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PersonContactChannel : ContactChannel
    {
        #region fields
        private long _personContactChannelId = -1;
        #endregion

        #region properties
        public override string Key
        {
            get
            {
                return PersonContactChannelId.ToString();
            }
        }

        public long PrimaryContactValueAsLong
        {
            get { return Convert.ToInt64(PrimaryContactValue); }
        }

        [DataMember]
        public long PersonContactChannelId 
        {
            get { return _personContactChannelId; }
            set
            {
                KeyChangedEventArgs args = new KeyChangedEventArgs();
                args.OldKey = _personContactChannelId.ToString();
                _personContactChannelId = value;
                args.NewKey = Key;
                if (!(args.OldKey ?? String.Empty).Equals(args.NewKey ?? String.Empty))
                    OnKeyChanged(args);
            }
        }
        [DataMember]
        public long PersonId {get;set;}
        [DataMember]
        public bool PrimaryFlag {get;set;}
        /*need this to hold unique rowversion from personcontactchannel table*/
        [DataMember]
        public new byte[] RowVersion { get; set; }
        [DataMember]
        public int SharedCount { get; set; }
        public bool IsShared { get { return SharedCount > 0; } }
        public bool IsNotShared { get { return !IsShared; } }

        #endregion

        #region construct/destruct
        public PersonContactChannel()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public void CopyTo(PersonContactChannel data)
        {
            base.CopyTo(data);

            data.PersonContactChannelId = PersonContactChannelId;
            data.PersonId = PersonId;
            data.PrimaryFlag = PrimaryFlag;
            data.RowVersion = RowVersion;
            data.SharedCount = SharedCount;
        }
        public new void Clear()
        {
            base.Clear();
            PersonContactChannelId = -1;
            PersonId = -1;
            PrimaryFlag = false;
            RowVersion = new byte[8];
            SharedCount = 0;
        }
        #endregion
    }
}
