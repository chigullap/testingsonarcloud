﻿using System;
using System.Runtime.Serialization;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class REALExport : BusinessObject
    {
        #region properties
        [DataMember]
        public string EmployeeNumber
        {
            get { return (string)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public DateTime HireDate { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string EmployeeStatus { get; set; }

        [DataMember]
        public DateTime EmployeeStatusDate { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public decimal StandardHoursPerDay { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string Province { get; set; }

        [DataMember]
        public string Zip { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string Phone1 { get; set; }

        [DataMember]
        public string Phone2 { get; set; }

        [DataMember]
        public string Phone3 { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string UnionCode { get; set; }

        [DataMember]
        public decimal PayCycle { get; set; }

        [DataMember]
        public string HourlySalary { get; set; }

        [DataMember]
        public decimal? BaseRateOfPay { get; set; }

        [DataMember]
        public DateTime BaseWage { get; set; }

        [DataMember]
        public DateTime PositionChangeDate { get; set; }

        [DataMember]
        public string LL1 { get; set; }

        [DataMember]
        public string LL2 { get; set; }

        [DataMember]
        public string LL3 { get; set; }

        [DataMember]
        public string LL4 { get; set; }

        [DataMember]
        public string LL5 { get; set; }

        [DataMember]
        public string LL6 { get; set; }

        [DataMember]
        public string LL7 { get; set; }

        [DataMember]
        public string LL1Desc { get; set; }

        [DataMember]
        public string LL2Desc { get; set; }

        [DataMember]
        public string LL3Desc { get; set; }

        [DataMember]
        public string LL4Desc { get; set; }

        [DataMember]
        public string LL5Desc { get; set; }

        [DataMember]
        public string LL6Desc { get; set; }

        [DataMember]
        public string LL7Desc { get; set; }

        [DataMember]
        public string ReportsTo { get; set; }

        [DataMember]
        public string WorkerType { get; set; }

        [DataMember]
        public string BadgeNumber { get; set; }

        [DataMember]
        public string MobileLicense { get; set; }

        [DataMember]
        public string ManagerLicense { get; set; }

        [DataMember]
        public string ScheduleLicense { get; set; }

        [DataMember]
        public string Timezone { get; set; }

        [DataMember]
        public DateTime? SeniorityDate { get; set; }

        [DataMember]
        public string DrawNumber { get; set; }

        [DataMember]
        public string EligibleJobs { get; set; }

        [DataMember]
        public string Posting { get; set; }

        [DataMember]
        public DateTime? PostingEndDate { get; set; }

        [DataMember]
        public string BankVacation { get; set; }

        [DataMember]
        public string BankOvertime { get; set; }

        [DataMember]
        public string SecondaryLabour { get; set; }

        #endregion

        #region construct/destruct
        public REALExport()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();

            EmployeeNumber = null;
            FirstName = null;
            LastName = null;
            HireDate = DateTime.MinValue;
            Username = null;
            EmployeeStatus = null;
            EmployeeStatusDate = DateTime.MinValue;
            BirthDate = null;
            StandardHoursPerDay = 0;
            Address = null;
            City = null;
            Province = null;
            Zip = null;
            Country = null;
            Phone1 = null;
            Phone2 = null;
            Phone3 = null;
            Email = null;
            UnionCode = null;
            PayCycle = 0;
            HourlySalary = null;
            BaseRateOfPay = null;
            BaseWage = DateTime.MinValue;
            PositionChangeDate = DateTime.MinValue;
            LL1 = null;
            LL2 = null;
            LL3 = null;
            LL4 = null;
            LL5 = null;
            LL6 = null;
            LL7 = null;
            LL1Desc = null;
            LL2Desc = null;
            LL3Desc = null;
            LL4Desc = null;
            LL5Desc = null;
            LL6Desc = null;
            LL7Desc = null;
            ReportsTo = null;
            WorkerType = null;
            BadgeNumber = null;
            MobileLicense = null;
            ManagerLicense = null;
            ScheduleLicense = null;
            Timezone = null;
            SeniorityDate = null;
            DrawNumber = null;
            EligibleJobs = null;
            Posting = null;
            PostingEndDate = null;
            BankVacation = null;
            BankOvertime = null;
            SecondaryLabour = null;
        }
        public virtual void CopyTo(REALExport data)
        {
            base.CopyTo(data);

            data.EmployeeNumber = EmployeeNumber;
            data.FirstName = FirstName;
            data.LastName = LastName;
            data.HireDate = HireDate;
            data.Username = Username;
            data.EmployeeStatus = EmployeeStatus;
            data.EmployeeStatusDate = EmployeeStatusDate;
            data.BirthDate = BirthDate;
            data.StandardHoursPerDay = StandardHoursPerDay;
            data.Address = Address;
            data.City = City;
            data.Province = Province;
            data.Zip = Zip;
            data.Country = Country;
            data.Phone1 = Phone1;
            data.Phone2 = Phone2;
            data.Phone3 = Phone3;
            data.Email = Email;
            data.UnionCode = UnionCode;
            data.PayCycle = PayCycle;
            data.HourlySalary = HourlySalary;
            data.BaseRateOfPay = BaseRateOfPay;
            data.BaseWage = BaseWage;
            data.PositionChangeDate = PositionChangeDate;
            data.LL1 = LL1;
            data.LL2 = LL2;
            data.LL3 = LL3;
            data.LL4 = LL4;
            data.LL5 = LL5;
            data.LL6 = LL6;
            data.LL7 = LL7;
            data.LL1Desc = LL1Desc;
            data.LL2Desc = LL2Desc;
            data.LL3Desc = LL3Desc;
            data.LL4Desc = LL4Desc;
            data.LL5Desc = LL5Desc;
            data.LL6Desc = LL6Desc;
            data.LL7Desc = LL7Desc;
            data.ReportsTo = ReportsTo;
            data.WorkerType = WorkerType;
            data.BadgeNumber = BadgeNumber;
            data.MobileLicense = MobileLicense;
            data.ManagerLicense = ManagerLicense;
            data.ScheduleLicense = ScheduleLicense;
            data.Timezone = Timezone;
            data.SeniorityDate = SeniorityDate;
            data.DrawNumber = DrawNumber;
            data.EligibleJobs = EligibleJobs;
            data.Posting = Posting;
            data.PostingEndDate = PostingEndDate;
            data.BankVacation = BankVacation;
            data.BankOvertime = BankOvertime;
            data.SecondaryLabour = SecondaryLabour;
        }
        #endregion
    }
}