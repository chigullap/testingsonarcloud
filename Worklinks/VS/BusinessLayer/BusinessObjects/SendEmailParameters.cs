﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SendEmailParameters
    {
        #region properties
        [DataMember]
        public String ToAddress { get; set; }
        [DataMember]
        public String FromAddress { get; set; }
        [DataMember]
        public String CcAddress { get; set; }
        [DataMember]
        public String BccAddress { get; set; }
        [DataMember]
        public String Subject { get; set; }
        [DataMember]
        public String Body { get; set; }
        [DataMember]
        public String SmtpHostName { get; set; }
        [DataMember]
        public int SmtpPortNumber { get; set; }
        [DataMember]
        public bool EnableSSL { get; set; }
        [DataMember]
        public String SmtpLogin { get; set; }
        [DataMember]
        public String SmtpPassword { get; set; }
        [DataMember]
        public byte[] Attachment { get; set; }
        [DataMember]
        public String FileName { get; set; }

        #endregion

        #region construct/destruct
        public SendEmailParameters()
        {
            Clear();
        }
        #endregion

        #region clear       
        public void Clear()
        {
            ToAddress = null;
            FromAddress = null;
            CcAddress = null;
            BccAddress = null;
            Subject = null;
            Body = null;
            SmtpHostName = null;
            SmtpPortNumber = 0;
            EnableSSL = false;
            SmtpLogin = null;
            SmtpPassword = null;
            Attachment = null;
            FileName = null;
        }
        #endregion
    }
}
