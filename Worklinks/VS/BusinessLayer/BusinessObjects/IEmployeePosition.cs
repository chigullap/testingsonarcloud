﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public interface IEmployeePosition
    {
        String OrganizationUnit { get; set; }
        Decimal RatePerHour { get; }
    }
}
