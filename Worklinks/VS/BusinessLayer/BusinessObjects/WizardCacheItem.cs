﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class WizardCacheItem : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public String ItemName
        {
            get { return (String)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long? WizardCacheId { get; set; }

        [DataMember]
        public long WizardItemId { get; set; }

        [DataMember]
        public long WizardCacheItemId { get; set; }

        [DataMember]
        public String DataType { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public short WizardIndex { get; set; }

        [DataMember]
        public short? NextWizardIndex { get; set; }

        [DataMember]
        public short? PreviousWizardIndex { get; set; }

        [DataMember]
        public bool MandatoryFlag { get; set; }

        [DataMember]
        public bool VisibleFlag { get; set; }

        public Object Data
        {
            get
            {
                if (DataXML == null)
                    return null;
                else
                {
                    Type type = Type.GetType(DataType);
                    XmlSerializer xml = new XmlSerializer(type);
                    System.IO.StringReader reader = new System.IO.StringReader(DataXML);

                    return xml.Deserialize(reader);
                }
            }
            set
            {
                if (value == null)
                    DataXML = null;
                else
                {
                    Type type = value.GetType();
                    DataType = type.ToString();
                    System.IO.StringWriter writer = new System.IO.StringWriter();
                    XmlSerializer xml = new XmlSerializer(value.GetType());
                    xml.Serialize(writer, value);
                    DataXML = writer.ToString();
                }
            }
        }

        [DataMember]
        public String DataXML { get; set; }
        #endregion

        #region construct/destruct
        public WizardCacheItem()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public void CopyTo(WizardCacheItem data)
        {
            base.CopyTo(data);

            data.WizardCacheItemId = WizardCacheItemId;
            data.WizardCacheId = WizardCacheId;
            data.WizardItemId = WizardItemId;
            data.ItemName = ItemName;
            data.DataType = DataType;
            data.DataXML = DataXML;
            data.ActiveFlag = ActiveFlag;
            data.WizardIndex = WizardIndex;
            data.NextWizardIndex = NextWizardIndex;
            data.PreviousWizardIndex = PreviousWizardIndex;
            data.MandatoryFlag = MandatoryFlag;
            data.VisibleFlag = VisibleFlag;
        }
        public new void Clear()
        {
            base.Clear();

            WizardCacheItemId = -1;
            WizardCacheId = null;
            WizardItemId = -1;
            ItemName = null;
            DataType = null;
            Data = null;
            ActiveFlag = true;
            WizardIndex = -1;
            NextWizardIndex = null;
            PreviousWizardIndex = null;
            MandatoryFlag = true;
            VisibleFlag = true;
        }
        public override object Clone()
        {
            WizardCacheItem item = new WizardCacheItem();
            this.CopyTo(item);
            return item;
        }
        #endregion
    }
}