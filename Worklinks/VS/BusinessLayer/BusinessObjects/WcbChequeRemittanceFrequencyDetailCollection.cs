﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class WcbChequeRemittanceFrequencyDetailCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<WcbChequeRemittanceFrequencyDetail>
    {
    }
}