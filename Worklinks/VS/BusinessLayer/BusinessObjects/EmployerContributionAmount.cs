﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployerContributionAmount : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployerContributionAmountId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long BusinessNumberId
        {
            get;
            set;
        }

        [DataMember]
        public int Year
        {
            get;
            set;
        }

        [DataMember]
        public decimal EmploymentInsuranceRate
        {
            get;
            set;
        }
        #endregion


        #region construct/destruct
        public EmployerContributionAmount()
        {
            Clear();
        }
        #endregion


        #region clear/copy/clone
        public new void Clear()
        {
            base.Clear();

            EmployerContributionAmountId = -1;
            BusinessNumberId = -1;
            Year = -1;
            EmploymentInsuranceRate = 0;
        }

        public void CopyTo(EmployerContributionAmount data)
        {
            base.CopyTo(data);

            data.EmployerContributionAmountId = EmployerContributionAmountId;
            data.BusinessNumberId = BusinessNumberId;
            data.Year = Year;
            data.EmploymentInsuranceRate = EmploymentInsuranceRate;
        }

        public override object Clone()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
