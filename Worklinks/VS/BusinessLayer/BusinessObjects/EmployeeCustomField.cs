﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeCustomField : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeCustomFieldId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long EmployeeId { get; set; }
        [DataMember]
        public String CustomField01 { get; set; }
        [DataMember]
        public String CustomField01DatatypeCode { get; set; }
        [DataMember]
        public String CustomField02 { get; set; }
        [DataMember]
        public String CustomField02DatatypeCode { get; set; }
        [DataMember]
        public String CustomField03 { get; set; }
        [DataMember]
        public String CustomField03DatatypeCode { get; set; }
        [DataMember]
        public String CustomField04 { get; set; }
        [DataMember]
        public String CustomField04DatatypeCode { get; set; }
        [DataMember]
        public String CustomField05 { get; set; }
        [DataMember]
        public String CustomField05DatatypeCode { get; set; }
        [DataMember]
        public String CustomField06 { get; set; }
        [DataMember]
        public String CustomField06DatatypeCode { get; set; }
        [DataMember]
        public String CustomField07 { get; set; }
        [DataMember]
        public String CustomField07DatatypeCode { get; set; }
        [DataMember]
        public String CustomField08 { get; set; }
        [DataMember]
        public String CustomField08DatatypeCode { get; set; }
        [DataMember]
        public String CustomField09 { get; set; }
        [DataMember]
        public String CustomField09DatatypeCode { get; set; }
        [DataMember]
        public String CustomField10 { get; set; }
        [DataMember]
        public String CustomField10DatatypeCode { get; set; }
        [DataMember]
        public String CustomField11 { get; set; }
        [DataMember]
        public String CustomField11DatatypeCode { get; set; }
        [DataMember]
        public String CustomField12 { get; set; }
        [DataMember]
        public String CustomField12DatatypeCode { get; set; }
        [DataMember]
        public String CustomField13 { get; set; }
        [DataMember]
        public String CustomField13DatatypeCode { get; set; }
        [DataMember]
        public String CustomField14 { get; set; }
        [DataMember]
        public String CustomField14DatatypeCode { get; set; }
        [DataMember]
        public String CustomField15 { get; set; }
        [DataMember]
        public String CustomField15DatatypeCode { get; set; }
        [DataMember]
        public String CustomField16 { get; set; }
        [DataMember]
        public String CustomField16DatatypeCode { get; set; }
        [DataMember]
        public String CustomField17 { get; set; }
        [DataMember]
        public String CustomField17DatatypeCode { get; set; }
        [DataMember]
        public String CustomField18 { get; set; }
        [DataMember]
        public String CustomField18DatatypeCode { get; set; }
        [DataMember]
        public String CustomField19 { get; set; }
        [DataMember]
        public String CustomField19DatatypeCode { get; set; }
        [DataMember]
        public String CustomField20 { get; set; }
        [DataMember]
        public String CustomField20DatatypeCode { get; set; }
        [DataMember]
        public String CustomField21 { get; set; }
        [DataMember]
        public String CustomField21DatatypeCode { get; set; }
        [DataMember]
        public String CustomField22 { get; set; }
        [DataMember]
        public String CustomField22DatatypeCode { get; set; }
        [DataMember]
        public String CustomField23 { get; set; }
        [DataMember]
        public String CustomField23DatatypeCode { get; set; }
        [DataMember]
        public String CustomField24 { get; set; }
        [DataMember]
        public String CustomField24DatatypeCode { get; set; }
        [DataMember]
        public String CustomField25 { get; set; }
        [DataMember]
        public String CustomField25DatatypeCode { get; set; }
        [DataMember]
        public String CustomField26 { get; set; }
        [DataMember]
        public String CustomField26DatatypeCode { get; set; }
        [DataMember]
        public String CustomField27 { get; set; }
        [DataMember]
        public String CustomField27DatatypeCode { get; set; }
        [DataMember]
        public String CustomField28 { get; set; }
        [DataMember]
        public String CustomField28DatatypeCode { get; set; }
        [DataMember]
        public String CustomField29 { get; set; }
        [DataMember]
        public String CustomField29DatatypeCode { get; set; }
        [DataMember]
        public String CustomField30 { get; set; }
        [DataMember]
        public String CustomField30DatatypeCode { get; set; }
        [DataMember]
        public String CustomField31 { get; set; }
        [DataMember]
        public String CustomField31DatatypeCode { get; set; }
        [DataMember]
        public String CustomField32 { get; set; }
        [DataMember]
        public String CustomField32DatatypeCode { get; set; }

        /// <summary>
        /// This method verifies the employee custom field datatype.
        /// </summary>
        /// <param name="customField"></param>
        /// <param name="dataType"></param>
        public bool CheckCustomFieldDatatype(String customField, String dataType)
        {
            if (customField.Equals("CustomField01")) return CustomField01DatatypeCode != null && CustomField01DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField02")) return CustomField02DatatypeCode != null && CustomField02DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField03")) return CustomField03DatatypeCode != null && CustomField03DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField04")) return CustomField04DatatypeCode != null && CustomField04DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField05")) return CustomField05DatatypeCode != null && CustomField05DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField06")) return CustomField06DatatypeCode != null && CustomField06DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField07")) return CustomField07DatatypeCode != null && CustomField07DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField08")) return CustomField08DatatypeCode != null && CustomField08DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField09")) return CustomField09DatatypeCode != null && CustomField09DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField10")) return CustomField10DatatypeCode != null && CustomField10DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField11")) return CustomField11DatatypeCode != null && CustomField11DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField12")) return CustomField12DatatypeCode != null && CustomField12DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField13")) return CustomField13DatatypeCode != null && CustomField13DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField14")) return CustomField14DatatypeCode != null && CustomField14DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField15")) return CustomField15DatatypeCode != null && CustomField15DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField16")) return CustomField16DatatypeCode != null && CustomField16DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField17")) return CustomField17DatatypeCode != null && CustomField17DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField18")) return CustomField18DatatypeCode != null && CustomField18DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField19")) return CustomField19DatatypeCode != null && CustomField19DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField20")) return CustomField20DatatypeCode != null && CustomField20DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField21")) return CustomField21DatatypeCode != null && CustomField21DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField22")) return CustomField22DatatypeCode != null && CustomField22DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField23")) return CustomField23DatatypeCode != null && CustomField23DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField24")) return CustomField24DatatypeCode != null && CustomField24DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField25")) return CustomField25DatatypeCode != null && CustomField25DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField26")) return CustomField26DatatypeCode != null && CustomField26DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField27")) return CustomField27DatatypeCode != null && CustomField27DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField28")) return CustomField28DatatypeCode != null && CustomField28DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField29")) return CustomField29DatatypeCode != null && CustomField29DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField30")) return CustomField30DatatypeCode != null && CustomField30DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField31")) return CustomField31DatatypeCode != null && CustomField31DatatypeCode.Equals(dataType);
            else if (customField.Equals("CustomField32")) return CustomField32DatatypeCode != null && CustomField32DatatypeCode.Equals(dataType);

            return false;
        }
        #endregion

        #region construct/destruct
        public EmployeeCustomField()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            EmployeeCustomField rtn = new EmployeeCustomField();
            this.CopyTo(rtn);
            return rtn;
        }
        public void CopyTo(EmployeeCustomField data)
        {
            base.CopyTo(data);
            data.EmployeeCustomFieldId = EmployeeCustomFieldId;
            data.EmployeeId = EmployeeId;
            data.CustomField01 = CustomField01;
            data.CustomField01DatatypeCode = CustomField01DatatypeCode;
            data.CustomField02 = CustomField02;
            data.CustomField02DatatypeCode = CustomField02DatatypeCode;
            data.CustomField03 = CustomField03;
            data.CustomField03DatatypeCode = CustomField03DatatypeCode;
            data.CustomField04 = CustomField04;
            data.CustomField04DatatypeCode = CustomField04DatatypeCode;
            data.CustomField05 = CustomField05;
            data.CustomField05DatatypeCode = CustomField05DatatypeCode;
            data.CustomField06 = CustomField06;
            data.CustomField06DatatypeCode = CustomField06DatatypeCode;
            data.CustomField07 = CustomField07;
            data.CustomField07DatatypeCode = CustomField07DatatypeCode;
            data.CustomField08 = CustomField08;
            data.CustomField08DatatypeCode = CustomField08DatatypeCode;
            data.CustomField09 = CustomField09;
            data.CustomField09DatatypeCode = CustomField09DatatypeCode;
            data.CustomField10 = CustomField10;
            data.CustomField10DatatypeCode = CustomField10DatatypeCode;
            data.CustomField11 = CustomField11;
            data.CustomField11DatatypeCode = CustomField11DatatypeCode;
            data.CustomField12 = CustomField12;
            data.CustomField12DatatypeCode = CustomField12DatatypeCode;
            data.CustomField13 = CustomField13;
            data.CustomField13DatatypeCode = CustomField13DatatypeCode;
            data.CustomField14 = CustomField14;
            data.CustomField14DatatypeCode = CustomField14DatatypeCode;
            data.CustomField15 = CustomField15;
            data.CustomField15DatatypeCode = CustomField15DatatypeCode;
            data.CustomField16 = CustomField16;
            data.CustomField16DatatypeCode = CustomField16DatatypeCode;
            data.CustomField17 = CustomField17;
            data.CustomField17DatatypeCode = CustomField17DatatypeCode;
            data.CustomField18 = CustomField18;
            data.CustomField18DatatypeCode = CustomField18DatatypeCode;
            data.CustomField19 = CustomField19;
            data.CustomField19DatatypeCode = CustomField19DatatypeCode;
            data.CustomField20 = CustomField20;
            data.CustomField20DatatypeCode = CustomField20DatatypeCode;
            data.CustomField21 = CustomField21;
            data.CustomField21DatatypeCode = CustomField21DatatypeCode;
            data.CustomField22 = CustomField22;
            data.CustomField22DatatypeCode = CustomField22DatatypeCode;
            data.CustomField23 = CustomField23;
            data.CustomField23DatatypeCode = CustomField23DatatypeCode;
            data.CustomField24 = CustomField24;
            data.CustomField24DatatypeCode = CustomField24DatatypeCode;
            data.CustomField25 = CustomField25;
            data.CustomField25DatatypeCode = CustomField25DatatypeCode;
            data.CustomField26 = CustomField26;
            data.CustomField26DatatypeCode = CustomField26DatatypeCode;
            data.CustomField27 = CustomField27;
            data.CustomField27DatatypeCode = CustomField27DatatypeCode;
            data.CustomField28 = CustomField28;
            data.CustomField28DatatypeCode = CustomField28DatatypeCode;
            data.CustomField29 = CustomField29;
            data.CustomField29DatatypeCode = CustomField29DatatypeCode;
            data.CustomField30 = CustomField30;
            data.CustomField30DatatypeCode = CustomField30DatatypeCode;
            data.CustomField31 = CustomField31;
            data.CustomField31DatatypeCode = CustomField31DatatypeCode;
            data.CustomField32 = CustomField32;
            data.CustomField32DatatypeCode = CustomField32DatatypeCode;
        }
        public new void Clear()
        {
            base.Clear();
            EmployeeCustomFieldId = -1;
            EmployeeId = -1;
            CustomField01 = null;
            CustomField01DatatypeCode = null;
            CustomField02 = null;
            CustomField02DatatypeCode = null;
            CustomField03 = null;
            CustomField03DatatypeCode = null;
            CustomField04 = null;
            CustomField04DatatypeCode = null;
            CustomField05 = null;
            CustomField05DatatypeCode = null;
            CustomField06 = null;
            CustomField06DatatypeCode = null;
            CustomField07 = null;
            CustomField07DatatypeCode = null;
            CustomField08 = null;
            CustomField08DatatypeCode = null;
            CustomField09 = null;
            CustomField09DatatypeCode = null;
            CustomField10 = null;
            CustomField10DatatypeCode = null;
            CustomField11 = null;
            CustomField11DatatypeCode = null;
            CustomField12 = null;
            CustomField12DatatypeCode = null;
            CustomField13 = null;
            CustomField13DatatypeCode = null;
            CustomField14 = null;
            CustomField14DatatypeCode = null;
            CustomField15 = null;
            CustomField15DatatypeCode = null;
            CustomField16 = null;
            CustomField16DatatypeCode = null;
            CustomField17 = null;
            CustomField17DatatypeCode = null;
            CustomField18 = null;
            CustomField18DatatypeCode = null;
            CustomField19 = null;
            CustomField19DatatypeCode = null;
            CustomField20 = null;
            CustomField20DatatypeCode = null;
            CustomField21 = null;
            CustomField21DatatypeCode = null;
            CustomField22 = null;
            CustomField22DatatypeCode = null;
            CustomField23 = null;
            CustomField23DatatypeCode = null;
            CustomField24 = null;
            CustomField24DatatypeCode = null;
            CustomField25 = null;
            CustomField25DatatypeCode = null;
            CustomField26 = null;
            CustomField26DatatypeCode = null;
            CustomField27 = null;
            CustomField27DatatypeCode = null;
            CustomField28 = null;
            CustomField28DatatypeCode = null;
            CustomField29 = null;
            CustomField29DatatypeCode = null;
            CustomField30 = null;
            CustomField30DatatypeCode = null;
            CustomField31 = null;
            CustomField31DatatypeCode = null;
            CustomField32 = null;
            CustomField32DatatypeCode = null;
        }
        #endregion
    }
}