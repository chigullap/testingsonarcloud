﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class YearEndAdjustmentTable : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long YearEndAdjustmentTableId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String TableName { get; set; }

        [DataMember]
        public String CodeLanguageCode { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public String YearEndFormCode { get; set; }
        #endregion

        #region construct/destruct
        public YearEndAdjustmentTable()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(YearEndAdjustmentTable data)
        {
            base.CopyTo(data);

            data.YearEndAdjustmentTableId = YearEndAdjustmentTableId;
            data.TableName = TableName;
            data.CodeLanguageCode = CodeLanguageCode;
            data.Description = Description;
            data.YearEndFormCode = YearEndFormCode;
        }
        public new void Clear()
        {
            base.Clear();

            YearEndAdjustmentTableId = -1;
            TableName = null;
            CodeLanguageCode = null;
            Description = null;
            YearEndFormCode = null;
        }
        #endregion
    }
}