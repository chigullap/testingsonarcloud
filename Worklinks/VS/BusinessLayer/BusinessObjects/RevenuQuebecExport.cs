﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class RevenuQuebecExport : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long RevenuQuebecExportId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long? RbcSequenceNumber { get; set; }
        [DataMember]
        public byte[] Data { get; set; }
        [DataMember]
        public string CodeRevenuQuebecRemittancePeriodCd { get; set; }
        [DataMember]
        public DateTime? RevenuQuebecFileTransmissionDate { get; set; }

        #endregion

        #region construct/destruct
        public RevenuQuebecExport()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();

            RevenuQuebecExportId = -1;
            RbcSequenceNumber = -1;
            Data = null;
            CodeRevenuQuebecRemittancePeriodCd = null;
            RevenuQuebecFileTransmissionDate = null;
        }

        public virtual void CopyTo(RevenuQuebecExport data)
        {
            base.CopyTo(data);

            data.RevenuQuebecExportId = RevenuQuebecExportId;
            data.RbcSequenceNumber = RbcSequenceNumber;
            Data.CopyTo(data.Data, 0);
            data.CodeRevenuQuebecRemittancePeriodCd = CodeRevenuQuebecRemittancePeriodCd;
            data.RevenuQuebecFileTransmissionDate = RevenuQuebecFileTransmissionDate;
        }
        #endregion
    }
}
