﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class AccrualPolicy : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long AccrualPolicyId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String EnglishDescription { get; set; }

        [DataMember]
        public String FrenchDescription { get; set; }
        #endregion

        #region construct/destruct
        public AccrualPolicy()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(AccrualPolicy data)
        {
            base.CopyTo(data);

            data.AccrualPolicyId = AccrualPolicyId;
            data.EnglishDescription = EnglishDescription;
            data.FrenchDescription = FrenchDescription;
        }
        public new void Clear()
        {
            base.Clear();

            AccrualPolicyId = -1;
            EnglishDescription = null;
            FrenchDescription = null;
        }
        #endregion
    }
}