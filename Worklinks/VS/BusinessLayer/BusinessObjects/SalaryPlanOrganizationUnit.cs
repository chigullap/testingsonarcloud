﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SalaryPlanOrganizationUnit : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long SalaryPlanOrganizationUnitId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long SalaryPlanId { get; set; }

        [DataMember]
        public long SalaryPlanGradeId { get; set; }

        [DataMember]
        public string OrganizationUnit { get; set; }
        #endregion

        #region construct/destruct
        public SalaryPlanOrganizationUnit()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(SalaryPlanOrganizationUnit data)
        {
            base.CopyTo(data);

            data.SalaryPlanOrganizationUnitId = SalaryPlanOrganizationUnitId;
            data.SalaryPlanId = SalaryPlanId;
            data.SalaryPlanGradeId = SalaryPlanGradeId;
            data.OrganizationUnit = OrganizationUnit;
        }
        public new void Clear()
        {
            base.Clear();

            SalaryPlanOrganizationUnitId = -1;
            SalaryPlanId = -1;
            SalaryPlanGradeId = -1;
            OrganizationUnit = null;
        }
        #endregion
    }
}