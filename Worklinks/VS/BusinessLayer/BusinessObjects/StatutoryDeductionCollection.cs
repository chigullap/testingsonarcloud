﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class StatutoryDeductionCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<StatutoryDeduction>
    {
        public StatutoryDeduction ActiveStatutoryDeduction
        {
            get
            {
                foreach (StatutoryDeduction deduction in this)
                {
                    if (deduction.ActiveFlag)
                        return deduction;
                }

                return null;
            }
        }
    }
}