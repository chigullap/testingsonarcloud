﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ImportExport : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long ImportExportId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String XsdBusinessObject { get; set; }

        [DataMember]
        public String Label { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public String PrependHeaderValue { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public bool CanConvertXlsxToCsv { get; set; }

        [DataMember]
        public bool RemoveByteOrderMark { get; set; }
        #endregion

        #region construct/destruct
        public ImportExport()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public new void Clear()
        {
            base.Clear();

            ImportExportId = -1;
            XsdBusinessObject = null;
            Label = null;
            Description = null;
            PrependHeaderValue = null;
            ActiveFlag = true;
            CanConvertXlsxToCsv = false;
            RemoveByteOrderMark = false;
        }
        public void CopyTo(ImportExport data)
        {
            base.CopyTo(data);

            data.ImportExportId = ImportExportId;
            data.XsdBusinessObject = XsdBusinessObject;
            data.Label = Label;
            data.Description = Description;
            data.PrependHeaderValue = PrependHeaderValue;
            data.ActiveFlag = ActiveFlag;
            data.CanConvertXlsxToCsv = CanConvertXlsxToCsv;
            data.RemoveByteOrderMark = RemoveByteOrderMark;
        }
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}