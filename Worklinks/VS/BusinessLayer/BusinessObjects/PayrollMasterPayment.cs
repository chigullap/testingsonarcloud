﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollMasterPayment : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PayrollMasterPaymentId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long PayrollMasterId { get; set; }

        [DataMember]
        public long PayrollProcessId { get; set; }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public Decimal Amount { get; set; }

        [DataMember]
        public String ChequeNumber { get; set; }

        [DataMember]
        public String CodeEmployeeBankingSequenceCd { get; set; }

        [DataMember]
        public String CodeEmployeeBankCd { get; set; }

        [DataMember]
        public String BankingCountryCode { get; set; }

        [DataMember]
        public String TransitNumber { get; set; }

        [DataMember]
        public String AccountNumber { get; set; }

        [DataMember]
        public String CodeStatusCd { get; set; }

        [DataMember]
        public bool DirectDepositFlag { get; set; }

        [DataMember]
        public long? SequenceNumber { get; set; }

        //for exports
        [DataMember]
        public String EmployeeNumber { get; set; }

        [DataMember]
        public DateTime ChequeDate { get; set; }

        [DataMember]
        public String CodeCompanyCd { get; set; }

        [DataMember]
        public String EmployeeName { get; set; }
        #endregion

        #region construct/destruct
        public PayrollMasterPayment()
        {
            Clear();
        }
        public PayrollMasterPayment(Garnishment garnishment, DateTime chequeDate)
        {
            Clear();

            PayrollMasterPaymentId = -garnishment.PayrollMasterPaycodeId;
            EmployeeNumber = garnishment.EmployeeNumber;
            EmployeeId = garnishment.EmployeeId;
            PayrollProcessId = garnishment.PayrollProcessId;
            ChequeNumber = "1";
            Amount = garnishment.Amount;
            ChequeDate = chequeDate;
            CodeCompanyCd = garnishment.CodeCompanyCd;
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PayrollMasterPayment data)
        {
            base.CopyTo(data);

            data.PayrollMasterPaymentId = PayrollMasterPaymentId;
            data.PayrollMasterId = PayrollMasterId;
            data.PayrollProcessId = PayrollProcessId;
            data.EmployeeId = EmployeeId;
            data.Amount = Amount;
            data.ChequeNumber = ChequeNumber;
            data.CodeEmployeeBankingSequenceCd = CodeEmployeeBankingSequenceCd;
            data.CodeEmployeeBankCd = CodeEmployeeBankCd;
            data.BankingCountryCode = BankingCountryCode;
            data.TransitNumber = TransitNumber;
            data.AccountNumber = AccountNumber;
            data.CodeStatusCd = CodeStatusCd;
            data.DirectDepositFlag = DirectDepositFlag;
            data.SequenceNumber = SequenceNumber;

            //for exports
            data.EmployeeNumber = EmployeeNumber;
            data.ChequeDate = ChequeDate;
            data.CodeCompanyCd = CodeCompanyCd;
            data.EmployeeName = EmployeeName;
        }
        public new void Clear()
        {
            base.Clear();

            PayrollMasterPaymentId = -1;
            PayrollMasterId = -1;
            PayrollProcessId = -1;
            EmployeeId = -1;
            Amount = 0;
            ChequeNumber = null;
            CodeEmployeeBankingSequenceCd = null;
            CodeEmployeeBankCd = null;
            BankingCountryCode = null;
            TransitNumber = null;
            AccountNumber = null;
            CodeStatusCd = null;
            DirectDepositFlag = true;
            SequenceNumber = null;

            //for exports
            EmployeeNumber = null;
            ChequeDate = DateTime.Now;
            CodeCompanyCd = null;
            EmployeeName = null;
        }
        #endregion
    }
}