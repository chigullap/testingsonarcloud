﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PreAuthorizedDebitExportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PreAuthorizedDebitExport>
    {
    }
}