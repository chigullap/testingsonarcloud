﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeAwardHonour : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeAwardHonourId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public String EmployeeAwardHonourTypeCode { get; set; }

        [DataMember]
        public DateTime? IssueDate { get; set; }

        [DataMember]
        public String Grantor { get; set; }

        [DataMember]
        public long? AttachmentId { get; set; }

        [DataMember]
        public String Notes { get; set; }

        [DataMember]
        public AttachmentCollection AttachmentObjectCollection { get; set; }

        //public property
        public String Description
        {
            get
            {
                if (AttachmentObjectCollection != null && AttachmentObjectCollection.Count > 0)
                    return AttachmentObjectCollection[0].Description;
                else
                    return null;
            }
        }
        #endregion

        #region construct/destruct
        public EmployeeAwardHonour()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(EmployeeAwardHonour data)
        {
            base.CopyTo(data);

            data.EmployeeAwardHonourId = EmployeeAwardHonourId;
            data.EmployeeId = EmployeeId;
            data.EmployeeAwardHonourTypeCode = EmployeeAwardHonourTypeCode;
            data.IssueDate = IssueDate;
            data.Grantor = Grantor;
            data.AttachmentId = AttachmentId;
            data.Notes = Notes;

            //attachments
            if (AttachmentObjectCollection != null)
            {
                data.AttachmentObjectCollection = new AttachmentCollection();
                AttachmentObjectCollection.CopyTo(data.AttachmentObjectCollection);
            }
            else
                data.AttachmentObjectCollection = AttachmentObjectCollection;
        }

        public new void Clear()
        {
            base.Clear();

            EmployeeAwardHonourId = -1;
            EmployeeId = -1;
            EmployeeAwardHonourTypeCode = null;
            IssueDate = null;
            Grantor = null;
            AttachmentId = -1;
            Notes = null;
            AttachmentObjectCollection = null;
        }
        #endregion
    }
}