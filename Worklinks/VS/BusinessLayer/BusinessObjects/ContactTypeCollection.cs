﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class ContactTypeCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<ContactType>
    {
        public ContactTypeCollection SortBySortOrder
        {
            get
            {
                int j;

                for (int i = 0; i < this.Count; i++)
                {
                    j = 0;

                    foreach (ContactType type in this)
                    {
                        if (type.SortOrder == i + 1)
                        {
                            //hold the old object
                            ContactType temp = this[i];

                            //replace with new object
                            this[i] = type;

                            //put old object in its place
                            this[j] = temp;

                            break;
                        }

                        j++;
                    }
                }

                return this;
            }
        }
    }
}