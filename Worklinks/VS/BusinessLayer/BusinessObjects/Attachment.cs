﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class Attachment : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long AttachmentId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String FileName { get; set; }

        [DataMember]
        public String FileTypeCode { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public byte[] Data { get; set; }
        #endregion

        #region construct/destruct
        public Attachment()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            Attachment item = new Attachment();
            this.CopyTo(item);
            return item;
        }

        public virtual void CopyTo(Attachment data)
        {
            base.CopyTo(data);

            data.AttachmentId = AttachmentId;
            data.FileName = FileName;
            data.FileTypeCode = FileTypeCode;
            data.Description = Description;
            data.Data = Data;
        }

        public new void Clear()
        {
            base.Clear();

            AttachmentId = -1;
            FileName = null;
            FileTypeCode = null;
            Description = null;
            Data = null;
        }
        #endregion
    }
}