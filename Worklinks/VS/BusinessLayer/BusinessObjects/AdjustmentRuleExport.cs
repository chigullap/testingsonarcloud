﻿using System;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class AdjustmentRuleExport : BusinessObject
    {
        #region properties
        [DataMember]
        public long DummyKey
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public string PersonNumber { get; set; }
        [DataMember]
        public string PersonName { get; set; }
        [DataMember]
        public DateTime EffectiveDate { get; set; }
        [DataMember]
        public string Job { get; set; }
        [DataMember]
        public decimal Rate { get; set; }

        #endregion

        #region construct/destruct
        public AdjustmentRuleExport()
        {
            Clear();
        }
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public virtual void CopyTo(AdjustmentRuleExport data)
        {
            base.CopyTo(data);

            data.DummyKey = DummyKey;
            data.PersonNumber = PersonNumber;
            data.PersonName = PersonName;
            data.EffectiveDate = EffectiveDate;
            data.Job = Job;
            data.Rate = Rate;
        }

        public new void Clear()
        {
            base.Clear();

            DummyKey = -1;
            PersonNumber = null;
            PersonName = null;
            EffectiveDate = DateTime.Now;
            Job = null;
            Rate = 0;
        }
        #endregion
    }
}
