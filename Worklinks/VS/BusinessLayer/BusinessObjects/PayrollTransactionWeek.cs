﻿//removed by McKayJ for now 

//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Runtime.Serialization;

//using WLP.BusinessLayer.BusinessObjects;

//namespace WorkLinks.BusinessLayer.BusinessObjects
//{
//    [Serializable]
//    [DataContract]
//    public class PayrollTransactionWeek : WLP.BusinessLayer.BusinessObjects.BusinessObject
//    {
//        #region fields
//        private static String _payCodeRegular = "REG";
//        private static String _payCodeOvertime = "OT";
//        private long _employeeId;
//        private long _batchId;
//        private DateTime _day1TransactionDate;
//        private String _updateUser;

//        #endregion

//        #region properties
//        public override String Key
//        {
//            get { return GenerateKey(EmployeeId, Day1TransactionDate); }
//        }
//        [DataMember]
//        public PayrollTransactionQuickEntryCollection Transactions { get; set; }

//        [DataMember]
//        public long EmployeeId
//        {
//            get
//            {
//                return _employeeId;
//            }
//            set
//            {
//                KeyChangedEventArgs args = new KeyChangedEventArgs();
//                args.OldKey = Key;
//                _employeeId = value;
//                args.NewKey = Key;
//                if (Transactions != null)
//                {
//                    foreach (PayrollTransaction transaction in Transactions)
//                    {
//                        transaction.EmployeeId = _employeeId;
//                    }
//                }


//                if (!(args.OldKey ?? String.Empty).Equals(args.NewKey ?? String.Empty) && this.GetType().Equals(typeof(PayrollTransactionWeek)))
//                    OnKeyChanged(args);
//            }
//        }
//        [DataMember]
//        public long PayrollBatchId
//        {
//            get
//            {
//                return _batchId;
//            }
//            set
//            {
//                _batchId = value;
//                if (Transactions != null)
//                {
//                    foreach (PayrollTransaction transaction in Transactions)
//                    {
//                        transaction.PayrollBatchId = _batchId;
//                    }
//                }
//            }
//        }
//        [DataMember]
//        public DateTime Day1TransactionDate
//        {
//            get
//            {
//                return _day1TransactionDate;
//            }
//            set
//            {
//                KeyChangedEventArgs args = new KeyChangedEventArgs();
//                args.OldKey = Key;
//                _day1TransactionDate = value;
//                args.NewKey = Key;
//                if (Transactions != null)
//                {
//                    foreach (PayrollTransactionQuickEntry transaction in Transactions)
//                    {
//                        transaction.TransactionDate = CalcTransactionDate(_day1TransactionDate, transaction.Day);
//                    }
//                }

//                if (!(args.OldKey ?? String.Empty).Equals(args.NewKey ?? String.Empty) && this.GetType().Equals(typeof(PayrollTransactionWeek)))
//                    OnKeyChanged(args);

//            }
//        }
//        [DataMember]
//        public Decimal BaseRate { get; set; }
//        [DataMember]
//        public Decimal RateFactor { get; set; }
//        [DataMember]
//        public override String UpdateUser
//        {
//            get
//            {
//                return _updateUser;
//            }
//            set
//            {
//                _updateUser = value;
//                if (Transactions != null)
//                {
//                    foreach (PayrollTransaction transaction in Transactions)
//                    {
//                        transaction.UpdateUser = value;
//                    }
//                }
//            }
//        }

//        public DayOfWeek Day1 { get { return Day1TransactionDate.DayOfWeek; } }
//        public DayOfWeek Day2 { get { return (DayOfWeek)(((short)Day1TransactionDate.DayOfWeek + 1) % 7); } }
//        public DayOfWeek Day3 { get { return (DayOfWeek)(((short)Day1TransactionDate.DayOfWeek + 2) % 7); } }
//        public DayOfWeek Day4 { get { return (DayOfWeek)(((short)Day1TransactionDate.DayOfWeek + 3) % 7); } }
//        public DayOfWeek Day5 { get { return (DayOfWeek)(((short)Day1TransactionDate.DayOfWeek + 4) % 7); } }
//        public DayOfWeek Day6 { get { return (DayOfWeek)(((short)Day1TransactionDate.DayOfWeek + 5) % 7); } }
//        public DayOfWeek Day7 { get { return (DayOfWeek)(((short)Day1TransactionDate.DayOfWeek + 6) % 7); } }

//        public Decimal? Day1Units
//        {
//            get { return GetRegularHours(1); }
//            set { SetRegularHours(1, value); }
//        }
//        public Decimal? Day1OvertimeUnits
//        {
//            get { return GetOvertimeHours(1); }
//            set { SetOvertimeHours(1, value); }
//        }
//        public Decimal? Day2Units
//        {
//            get { return GetRegularHours(2); }
//            set { SetRegularHours(2, value); }
//        }
//        public Decimal? Day2OvertimeUnits
//        {
//            get { return GetOvertimeHours(2); }
//            set { SetOvertimeHours(2, value); }
//        }
//        public Decimal? Day3Units
//        {
//            get { return GetRegularHours(3); }
//            set { SetRegularHours(3, value); }
//        }
//        public Decimal? Day3OvertimeUnits
//        {
//            get { return GetOvertimeHours(3); }
//            set { SetOvertimeHours(3, value); }
//        }
//        public Decimal? Day4Units
//        {
//            get { return GetRegularHours(4); }
//            set { SetRegularHours(4, value); }
//        }
//        public Decimal? Day4OvertimeUnits
//        {
//            get { return GetOvertimeHours(4); }
//            set { SetOvertimeHours(4, value); }
//        }
//        public Decimal? Day5Units
//        {
//            get { return GetRegularHours(5); }
//            set { SetRegularHours(5, value); }
//        }
//        public Decimal? Day5OvertimeUnits
//        {
//            get { return GetOvertimeHours(5); }
//            set { SetOvertimeHours(5, value); }
//        }
//        public Decimal? Day6Units
//        {
//            get { return GetRegularHours(6); }
//            set { SetRegularHours(6, value); }
//        }
//        public Decimal? Day6OvertimeUnits
//        {
//            get { return GetOvertimeHours(6); }
//            set { SetOvertimeHours(6, value); }
//        }
//        public Decimal? Day7Units
//        {
//            get { return GetRegularHours(7); }
//            set { SetRegularHours(7, value); }
//        }
//        public Decimal? Day7OvertimeUnits
//        {
//            get { return GetOvertimeHours(7); }
//            set { SetOvertimeHours(7, value); }
//        }
//        public Decimal TotalUnits
//        {
//            get
//            {
//                return (Decimal)(Day1Units ?? 0)
//                    + (Decimal)(Day2Units ?? 0)
//                    + (Decimal)(Day3Units ?? 0)
//                    + (Decimal)(Day4Units ?? 0)
//                    + (Decimal)(Day5Units ?? 0)
//                    + (Decimal)(Day6Units ?? 0)
//                    + (Decimal)(Day7Units ?? 0);
//            }
//        }
//        public Decimal TotalOvertimeUnits
//        {
//            get
//            {
//                return (Decimal)(Day1OvertimeUnits ?? 0)
//                    + (Decimal)(Day2OvertimeUnits ?? 0)
//                    + (Decimal)(Day3OvertimeUnits ?? 0)
//                    + (Decimal)(Day4OvertimeUnits ?? 0)
//                    + (Decimal)(Day5OvertimeUnits ?? 0)
//                    + (Decimal)(Day6OvertimeUnits ?? 0)
//                    + (Decimal)(Day7OvertimeUnits ?? 0);
//            }
//        }

//        #endregion

//        #region construct/destruct
//        public PayrollTransactionWeek()
//        {
//            Clear();
//        }

//        #endregion

//        #region clear/copy/clone

//        public override object Clone()
//        {
//            throw new NotImplementedException();
//        }
//        public void CopyTo(PayrollTransactionWeek data)
//        {
//            base.CopyTo(data);
//            Transactions.CopyTo(data.Transactions);
//            data.EmployeeId = EmployeeId;
//            data.PayrollBatchId = PayrollBatchId;
//            data.Day1TransactionDate = Day1TransactionDate;
//            data.Rate = Rate;
//            data.UpdateUser = UpdateUser;
//        }
//        public new void Clear()
//        {
//            base.Clear();
//            Transactions = new PayrollTransactionQuickEntryCollection();
//            EmployeeId = -1;
//            PayrollBatchId = -1;
//            Day1TransactionDate = DateTime.MinValue;
//            Rate = 0;
//            UpdateUser = null;
//        }
//        #endregion

//        #region main
//        private Decimal? GetHours(short day, String payCode)
//        {
//            Decimal? hours = null;
//            PayrollTransaction transaction = GetTransaction(day, payCode);
//            if (transaction != null)
//                hours = transaction.Units;
//            return hours;
//        }
//        private PayrollTransactionQuickEntry GetTransaction(short day, String payCode)
//        {
//            PayrollTransactionQuickEntry foundTransaction = null;
//            foreach (PayrollTransactionQuickEntry transaction in Transactions)
//            {
//                if (transaction.Day.Equals(day) && transaction.PaycodeCode.Equals(payCode))
//                    foundTransaction = transaction;
//            }

//            return foundTransaction;
//        }
//        private void SetHours(short day, Decimal? units, String payCode)
//        {
//            PayrollTransactionQuickEntry transaction = GetTransaction(day, payCode);

//            if (transaction != null)
//            {
//                if (units == null)
//                    Transactions.Remove(transaction);
//                else
//                    transaction.Units = (Decimal)units;
//            }
//            else
//            {
//                if (units != null)
//                {
//                    transaction = CreateTransaction(day, payCode);
//                    transaction.Units = (Decimal)units;
//                    Transactions.Add(transaction);
//                }

//            }
//        }
//        private PayrollTransactionQuickEntry CreateTransaction(short day, String payCode)
//        {
//            PayrollTransactionQuickEntry transaction = new PayrollTransactionQuickEntry();
//            transaction.PayrollTransactionId = ((long)day * -1) + (long)(payCode.Equals(_payCodeRegular) ? 0 : -7);
//            transaction.EmployeeId = EmployeeId;
//            transaction.PayrollBatchId = PayrollBatchId;
//            transaction.TransactionDate = CalcTransactionDate(Day1TransactionDate, day);
//            transaction.PaycodeCode = payCode;
//            if (transaction.PaycodeCode.Equals("OT")) //****HACK*** FOR OT*/
//                ***transaction.Rate = _rate * (decimal)1.5;
//            else
//                transaction.Rate = _rate;
//            transaction.Day = day;
//            return transaction;
//        }
//        private DateTime CalcTransactionDate(DateTime date, short day)
//        {
//            DateTime calcDate = date;
//            calcDate = date.AddDays(day - 1);

//            return calcDate;
//        }
//        private Decimal? GetRegularHours(short day)
//        {
//            return GetHours(day, _payCodeRegular);
//        }
//        private void SetRegularHours(short day, Decimal? units)
//        {
//            SetHours(day, units, _payCodeRegular);
//        }
//        private Decimal? GetOvertimeHours(short day)
//        {
//            return GetHours(day, _payCodeOvertime);
//        }
//        private void SetOvertimeHours(short day, Decimal? units)
//        {
//            SetHours(day, units, _payCodeOvertime);
//        }
//        public static String GenerateKey(long employeeId, DateTime day1Date)
//        {
//            return String.Format("{0}/{1}/{2}", employeeId, day1Date, 1);
//        }
//        #endregion

//    }
//}
