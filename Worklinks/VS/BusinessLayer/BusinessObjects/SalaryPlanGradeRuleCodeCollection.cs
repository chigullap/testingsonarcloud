﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class SalaryPlanGradeRuleCodeCollection : DataItemCollection<SalaryPlanGradeRuleCode>
    {
        public SalaryPlanGradeRuleCodeCollection Filter(long ruleId)
        {
            SalaryPlanGradeRuleCodeCollection filtered = new SalaryPlanGradeRuleCodeCollection();

            foreach (SalaryPlanGradeRuleCode ruleCode in this.Where(r=>r.SalaryPlanGradeRuleId == ruleId))
            {
                filtered.Add(ruleCode);
            }

            return filtered;
        }
    }
}
