﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public class ReportInstanceParameter : WLP.BusinessLayer.BusinessObjects.BusinessObject, ICloneable
    {
        #region properties
        [DataMember]
        public long ReportInstanceParameterId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long ReportInstanceId { get; set; }
        [DataMember]
        public String ParameterName {get;set;}
        [DataMember]
        public int ParameterOrder {get; set;}
        [DataMember]
        public String ParameterType {get;set;}
        [DataMember]
        public String ParameterValue {get; set;}
        #endregion


        #region construct/destruct
        public ReportInstanceParameter()
        {
            Clear();
        }
        public ReportInstanceParameter(ReportParameter parm)
        {
            Clear();
            ParameterName = parm.ParameterName;
            ParameterOrder = parm.ParameterOrder;
            ParameterType = parm.ParameterType;
        }

        #endregion

        #region clear/copy/clone

        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(ReportInstanceParameter data)
        {
            base.CopyTo(data);
            data.ReportInstanceParameterId = ReportInstanceParameterId;
            data.ReportInstanceId = ReportInstanceId;
            data.ParameterName = ParameterName;
            data.ParameterOrder = ParameterOrder;
            data.ParameterType = ParameterType;
            data.ParameterValue = ParameterValue;
        }
        public new void Clear()
        {
            base.Clear();
            ReportInstanceParameterId = -1;
            ReportInstanceId = -1;
            ParameterName = null;
            ParameterOrder = -1;
            ParameterType = null;
            ParameterValue = null;
        }

        #endregion
    }
}
