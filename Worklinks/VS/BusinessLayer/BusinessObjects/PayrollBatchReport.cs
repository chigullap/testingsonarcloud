﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollBatchReport : PayrollBatch
    {
        #region properties
        [DataMember]
        public string PayrollProcessRunTypeCodeDescription { get; set; }

        [DataMember]
        public string PayrollBatchStatusCodeDescription { get; set; }

        [DataMember]
        public string PayrollProcessGroupCodeDescription { get; set; }

        [DataMember]
        public decimal? TotalHours { get; set; }

        [DataMember]
        public decimal? TotalAmount { get; set; }

        [DataMember]
        public string PayrollProcessStatusCode { get; set; }

        [DataMember]
        public bool ProcessedFlag { get; set; }
        #endregion

        #region construct/destruct
        public PayrollBatchReport()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PayrollBatchReport data)
        {
            base.CopyTo(data);

            data.PayrollProcessRunTypeCodeDescription = PayrollProcessRunTypeCodeDescription;
            data.PayrollBatchStatusCodeDescription = PayrollBatchStatusCodeDescription;
            data.PayrollProcessGroupCodeDescription = PayrollProcessGroupCodeDescription;
            data.TotalHours = TotalHours;
            data.TotalAmount = TotalAmount;
            data.PayrollProcessStatusCode = PayrollProcessStatusCode;
            data.ProcessedFlag = ProcessedFlag;
            data.PayrollProcessGroupCode = PayrollProcessGroupCode;
        }
        public new void Clear()
        {
            base.Clear();

            PayrollProcessRunTypeCodeDescription = null;
            PayrollBatchStatusCodeDescription = null;
            PayrollProcessGroupCodeDescription = null;
            TotalHours = -1;
            TotalAmount = -1;
            PayrollProcessStatusCode = null;
            ProcessedFlag = false;
            PayrollProcessGroupCode = "";
        }
        #endregion
    }
}