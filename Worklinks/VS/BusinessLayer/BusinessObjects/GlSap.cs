﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class GlSap : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long GlSapId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String LegalEntity { get; set; }
        [DataMember]
        public String CostCentre { get; set; }
        [DataMember]
        public String Account { get; set; }
        [DataMember]
        public Decimal DebitAmount { get; set; }
        [DataMember]
        public Decimal CreditAmount { get; set; }
        [DataMember]
        public String DebitCreditIndicator { get; set; }
        #endregion

        #region construct/destruct
        public GlSap()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(GlSap data)
        {
            data.GlSapId = GlSapId;
            data.LegalEntity = LegalEntity;
            data.CostCentre = CostCentre;
            data.Account = Account;
            data.DebitAmount = DebitAmount;
            data.CreditAmount = CreditAmount;
            data.DebitCreditIndicator = DebitCreditIndicator;
        }
        #endregion
    }
}
