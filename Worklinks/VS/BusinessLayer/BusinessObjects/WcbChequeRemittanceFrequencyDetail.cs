﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class WcbChequeRemittanceFrequencyDetail : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long WcbChequeRemittanceFrequencyDetailId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String WcbChequeRemittanceFrequencyCode { get; set; }

        [DataMember]
        public String RemittanceDateMonthCode { get; set; }

        [DataMember]
        public String RemittanceDateDayCode { get; set; }

        [DataMember]
        public String ReportingPeriodStartDateMonthCode { get; set; }

        [DataMember]
        public String ReportingPeriodStartDateDayCode { get; set; }

        [DataMember]
        public DateTime RemittanceDate { get; set; }

        [DataMember]
        public DateTime ReportingPeriodStartDate { get; set; }

        [DataMember]
        public DateTime ReportingPeriodEndDate { get; set; }

        [DataMember]
        public bool BasedOnEstimateFlag { get; set; }

        [DataMember]
        public bool RemitInCurrentYearFlag { get; set; }        
        #endregion

        #region construct/destruct
        public WcbChequeRemittanceFrequencyDetail()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(WcbChequeRemittanceFrequencyDetail data)
        {
            base.CopyTo(data);

            data.WcbChequeRemittanceFrequencyDetailId = WcbChequeRemittanceFrequencyDetailId;
            data.WcbChequeRemittanceFrequencyCode = WcbChequeRemittanceFrequencyCode;
            data.RemittanceDateMonthCode = RemittanceDateMonthCode;
            data.RemittanceDateDayCode = RemittanceDateDayCode;
            data.ReportingPeriodStartDateMonthCode = ReportingPeriodStartDateMonthCode;
            data.ReportingPeriodStartDateDayCode = ReportingPeriodStartDateDayCode;
            data.RemittanceDate = RemittanceDate;
            data.ReportingPeriodStartDate = ReportingPeriodStartDate;
            data.ReportingPeriodEndDate = ReportingPeriodEndDate;
            data.BasedOnEstimateFlag = BasedOnEstimateFlag;
            data.RemitInCurrentYearFlag = RemitInCurrentYearFlag ;
        }
        public new void Clear()
        {
            base.Clear();

            WcbChequeRemittanceFrequencyDetailId = -1;
            WcbChequeRemittanceFrequencyCode = null;
            RemittanceDateMonthCode = null;
            RemittanceDateDayCode = null;
            ReportingPeriodStartDateMonthCode = null;
            ReportingPeriodStartDateDayCode = null;
            RemittanceDate = DateTime.Now;
            ReportingPeriodStartDate = DateTime.Now;
            ReportingPeriodEndDate = DateTime.Now;
            BasedOnEstimateFlag = false;
            RemitInCurrentYearFlag = false;
        }
        #endregion
    }
}