﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SalaryPlanGradeAccumulationTotals : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long DummyKey
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public long SalaryPlanGradeId { get; set; }

        [DataMember]
        public decimal? TotalAmount { get; set; }

        [DataMember]
        public decimal? TotalHours { get; set; }
        #endregion

        #region construct/destruct
        public SalaryPlanGradeAccumulationTotals()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(SalaryPlanGradeAccumulationTotals data)
        {
            base.CopyTo(data);

            data.DummyKey = DummyKey;
            data.EmployeeId = EmployeeId;
            data.SalaryPlanGradeId = SalaryPlanGradeId;
            data.TotalAmount = TotalAmount;
            data.TotalHours = TotalHours;
        }
        public new void Clear()
        {
            base.Clear();

            DummyKey = -1;
            EmployeeId = -1;
            SalaryPlanGradeId = -1;
            TotalAmount = null;
            TotalHours = null;
        }
        #endregion
    }
}