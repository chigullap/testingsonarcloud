﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class CodeWsibEffectiveCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<CodeWsibEffective>
    {
    }
}