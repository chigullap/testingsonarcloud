﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Runtime.Serialization;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeePaycodeImportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeePaycodeImport>
    {
        public EmployeePaycodeImportCollection(WorkLinksEmployeePaycode xsdEmployeePaycodeImport)
        {
            int i=0;
            foreach (WorkLinksEmployeePaycodeBodyEmployeePaycode xsdEmployeePaycode in xsdEmployeePaycodeImport.Body.EmployeePaycodes)
            {
                EmployeePaycodeImport employeePaycode = new EmployeePaycodeImport(xsdEmployeePaycode);
                employeePaycode.EmployeePaycodeId = --i;
                this.Add(employeePaycode);
            }
        }
        public EmployeePaycodeImportCollection(EmployeePaycodeCollection employeePaycodes, String employeeImportExternalIdenfier)
        {
            int i = 0;
            foreach (EmployeePaycode paycode in employeePaycodes)
            {
                EmployeePaycodeImport newPaycode = new EmployeePaycodeImport() { ImportExternalIdentifier = employeeImportExternalIdenfier };
                paycode.CopyTo(newPaycode);
                newPaycode.EmployeePaycodeId = --i;
                this.Add(newPaycode);
            }
        }


        public bool HasErrors
        {
            get
            {
                foreach (EmployeePaycodeImport import in this)
                {
                    if (import.HasErrors)
                        return true;
                }
                return false;
            }
        }

        public bool HasAllErrors
        {
            get
            {
                foreach (EmployeePaycodeImport import in this)
                {
                    if (!import.HasErrors)
                        return false;
                }

                if (this.Count == 0) //no rows means no errors
                    return false;

                return true;
            }
        }
    }

   

}
