﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class WizardCacheImport : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long WizardCacheId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long WizardId { get; set; }

        [DataMember]
        public bool TemplateFlag { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public String ImportExternalIdentifier { get; set; }

        [DataMember]
        public String CountryCode { get; set; }

        [DataMember]
        public long WizardCacheItemId { get; set; }

        [DataMember]
        public long WizardItemId { get; set; }

        [DataMember]
        public String DataType { get; set; }

        [DataMember]
        public String DataXML { get; set; }

        [DataMember]
        public bool VisibleFlag { get; set; }

        public Object Data
        {
            get
            {
                if (DataXML == null)
                    return null;
                else
                {
                    Type type = Type.GetType(DataType);
                    XmlSerializer xml = new XmlSerializer(type);
                    System.IO.StringReader reader = new System.IO.StringReader(DataXML);

                    return xml.Deserialize(reader);
                }
            }
            set
            {
                if (value == null)
                    DataXML = null;
                else
                {
                    Type type = value.GetType();
                    DataType = type.ToString();
                    System.IO.StringWriter writer = new System.IO.StringWriter();
                    XmlSerializer xml = new XmlSerializer(value.GetType());
                    xml.Serialize(writer, value);
                    DataXML = writer.ToString();
                }
            }
        }
        #endregion

        #region construct/destruct
        public WizardCacheImport()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public void CopyTo(WizardCacheImport data)
        {
            base.CopyTo(data);

            data.WizardCacheId = WizardCacheId;
            data.WizardId = WizardId;
            data.TemplateFlag = TemplateFlag;
            data.ActiveFlag = ActiveFlag;
            data.Description = Description;
            data.ImportExternalIdentifier = ImportExternalIdentifier;
            data.CountryCode = CountryCode;
            data.WizardCacheItemId = WizardCacheItemId;
            data.WizardItemId = WizardItemId;
            data.DataType = DataType;
            data.Data = Data;
            data.VisibleFlag = VisibleFlag;
        }
        public new void Clear()
        {
            base.Clear();

            WizardCacheId = -1;
            WizardId = -1;
            TemplateFlag = false;
            ActiveFlag = true;
            Description = null;
            ImportExternalIdentifier = null;
            CountryCode = null;
            WizardCacheItemId = -1;
            WizardItemId = -1;
            DataType = null;
            Data = null;
            VisibleFlag = true;
        }
        public override object Clone()
        {
            WizardCacheImport item = new WizardCacheImport();
            this.CopyTo(item);
            return item;
        }
        #endregion
    }
}