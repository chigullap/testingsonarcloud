﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ChequeHealthTaxRemittanceSummary : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long HealthTaxId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public string CodeProvinceStateCd { get; set; }

        [DataMember]
        public string HealthRemittanceAccountNumber { get; set; }

        [DataMember]
        public Decimal Earnings { get; set; }

        [DataMember]
        public Decimal Premium { get; set; }

        [DataMember]
        public DateTime RemittanceDate { get; set; }

        [DataMember]
        public string VendorName { get; set; }

        [DataMember]
        public string VendorAddressLine1 { get; set; }

        [DataMember]
        public string VendorAddressLine2 { get; set; }

        [DataMember]
        public string VendorCity { get; set; }

        [DataMember]
        public string VendorCodeProvinceStateCd { get; set; }

        [DataMember]
        public string VendorPostalZipCode { get; set; }

        [DataMember]
        public string VendorCodeCountryCd { get; set; }
        #endregion

        #region construct/destruct
        public ChequeHealthTaxRemittanceSummary()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(ChequeHealthTaxRemittanceSummary data)
        {
            base.CopyTo(data);

            data.HealthTaxId = HealthTaxId;
            data.CodeProvinceStateCd = CodeProvinceStateCd;
            data.HealthRemittanceAccountNumber = HealthRemittanceAccountNumber;
            data.Earnings = Earnings;
            data.Premium = Premium;
            data.RemittanceDate = RemittanceDate;
            data.VendorName = VendorName;
            data.VendorAddressLine1 = VendorAddressLine1;
            data.VendorAddressLine2 = VendorAddressLine2;
            data.VendorCity = VendorCity;
            data.VendorCodeProvinceStateCd = VendorCodeProvinceStateCd;
            data.VendorPostalZipCode = VendorPostalZipCode;
            data.VendorCodeCountryCd = VendorCodeCountryCd;
        }

        public new void Clear()
        {
            base.Clear();

            HealthTaxId = -1;
            CodeProvinceStateCd = null;
            HealthRemittanceAccountNumber = null;
            Earnings = 0;
            Premium = 0;
            RemittanceDate = DateTime.Now;
            VendorName = null;
            VendorAddressLine1 = null;
            VendorAddressLine2 = null;
            VendorCity = null;
            VendorCodeProvinceStateCd = null;
            VendorPostalZipCode = null;
            VendorCodeCountryCd = null;
        }
        #endregion
    }
}
