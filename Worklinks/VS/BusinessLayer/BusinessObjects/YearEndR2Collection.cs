﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class YearEndR2Collection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<YearEndR2>
    {
    }
}