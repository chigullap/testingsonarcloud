﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class HealthTaxDetail : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long HealthTaxDetailId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long HealthTaxId { get; set; }

        [DataMember]
        public Decimal EffectiveYear { get; set; }

        [DataMember]
        public Decimal ExemptionAmount { get; set; }

        [DataMember]
        public Decimal RatePercentage { get; set; }

        [DataMember]
        public String WcbChequeRemittanceFrequencyCode { get; set; }
        #endregion

        #region construct/destruct
        public HealthTaxDetail()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(HealthTaxDetail data)
        {
            base.CopyTo(data);

            data.HealthTaxDetailId = HealthTaxDetailId;
            data.HealthTaxId = HealthTaxId;
            data.EffectiveYear = EffectiveYear;
            data.ExemptionAmount = ExemptionAmount;
            data.RatePercentage = RatePercentage;
            data.WcbChequeRemittanceFrequencyCode = WcbChequeRemittanceFrequencyCode;
        }
        public new void Clear()
        {
            base.Clear();

            HealthTaxDetailId = -1;
            HealthTaxId = -1;
            EffectiveYear = 0;
            ExemptionAmount = 0;
            RatePercentage = 0;
            WcbChequeRemittanceFrequencyCode = null;
        }
        #endregion
    }
}