﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeePhoto : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeePhotoId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public byte[] Data { get; set; }
        #endregion

        #region construct/destruct
        public EmployeePhoto()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public virtual void CopyTo(EmployeePhoto data)
        {
            base.CopyTo(data);

            data.EmployeePhotoId = EmployeePhotoId;
            data.EmployeeId = EmployeeId;
            data.Data = Data;
        }

        public new void Clear()
        {
            base.Clear();

            EmployeePhotoId = -1;
            EmployeeId = -1;
            Data = null;
        }
        #endregion
    }
}