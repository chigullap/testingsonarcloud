﻿using System;
using System.Collections.Generic;
using System.Text;

using System.ComponentModel.DataAnnotations;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class PaycodeCriteria
    {
        #region properties
        public String PaycodeCode { get; set; }
        public String PaycodeTypeCode { get; set; }

        #endregion

        public PaycodeCriteria()
        {
            Clear();
        }

        public void Clear()
        {
            PaycodeCode = "";
            PaycodeTypeCode = "";
        }
    }

}
