﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class RevenuQuebecRemittanceSummaryCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<RevenuQuebecRemittanceSummary>
    {
        public decimal SumTotalRemits
        {
            get
            {
                return SumQuebecTax 
                    + SumTotalQuebecPensionPlanContribution 
                    + SumTotalHealthTaxLevy
                    + SumTotalQuebecParentalInsurancePlanContribution
                    + SumEmployerWorkersCompensationPremium;
            }
        }

        public decimal SumQuebecTax
        {
            get
            {
                decimal rtn = 0;

                foreach (RevenuQuebecRemittanceSummary item in this)
                {
                    rtn += item.QuebecTax;
                }

                return rtn;
            }
        }
        public decimal SumTotalQuebecPensionPlanContribution
        {
            get
            {
                decimal rtn = 0;

                foreach (RevenuQuebecRemittanceSummary item in this)
                {
                    rtn += item.TotalQuebecPensionPlanContribution;
                }

                return rtn;
            }
        }
        public decimal SumTotalHealthTaxLevy
        {
            get
            {
                decimal rtn = 0;

                foreach (RevenuQuebecRemittanceSummary item in this)
                {
                    rtn += item.TotalHealthTaxLevy;
                }

                return rtn;
            }
        }
        public decimal SumTotalQuebecParentalInsurancePlanContribution
        {
            get
            {
                decimal rtn = 0;

                foreach (RevenuQuebecRemittanceSummary item in this)
                {
                    rtn += item.TotalQuebecParentalInsurancePlanContribution;
                }

                return rtn;
            }
        }
        public decimal SumEmployerWorkersCompensationPremium
        {
            get
            {
                decimal rtn = 0;

                foreach (RevenuQuebecRemittanceSummary item in this)
                {
                    rtn += item.EmployerWorkersCompensationPremium;
                }

                return rtn;
            }
        }
    }
}
