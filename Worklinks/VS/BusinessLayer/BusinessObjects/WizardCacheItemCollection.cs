﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class WizardCacheItemCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<WizardCacheItem>
    {
        public WizardCacheItem this[WizardCache.ItemType type]
        {
            get
            {
                if (type.Equals(WizardCache.ItemType.None))
                    return null;
                else
                    return this[type];
            }
        }

        public new WizardCacheItem this[string key]
        {
            get { return base[key]; }
            set { base[key] = value; }
        }
    }

}
