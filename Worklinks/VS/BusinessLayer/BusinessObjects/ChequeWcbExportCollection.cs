﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class ChequeWcbExportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<ChequeWcbExport>
    {
    }
}
