﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class RbcRqSourceDeductionParameters //this class is used by the RQ source deductions.
    {
        #region properties
        [DataMember]
        public String IsaSenderInterchangeIDSourceDed { get; set; }
        [DataMember]
        public String IsaReceiverInterchangeIDSourceDed { get; set; }
        [DataMember]
        public String IsaTestIndicatorSourceDed { get; set; }
        [DataMember]
        public string GsApplicationReceiversCodeSourceDed { get; set; }
        [DataMember]
        public String DirectDepositBankCodeTransitNumber { get; set; }
        [DataMember]
        public String DirectDepositAccountNumber { get; set; }
        [DataMember]
        public String RqFiTransitNumber { get; set; }
        [DataMember]
        public String RqBankAccount { get; set; }
        [DataMember]
        public String RqPartnerId { get; set; }
        [DataMember]
        public String N1PrNameSourceDed { get; set; }
        
        #endregion

        #region construct/destruct
        public RbcRqSourceDeductionParameters()
        {
            Clear();
        }
        #endregion

        #region clear       
        public void Clear()
        {
            IsaSenderInterchangeIDSourceDed = null;
            IsaReceiverInterchangeIDSourceDed = null;
            IsaTestIndicatorSourceDed = null;
            GsApplicationReceiversCodeSourceDed = null;
            DirectDepositBankCodeTransitNumber = null;
            DirectDepositAccountNumber = null;
            RqFiTransitNumber = null;
            RqBankAccount = null;
            RqPartnerId = null;
            N1PrNameSourceDed = null;
        }

        #endregion
    }
}
