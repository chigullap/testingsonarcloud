﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CodeSystem : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public String CodeSystemCd
        {
            get { return (String)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Notes { get; set; }
        #endregion

        #region construct/destruct
        public CodeSystem()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(CodeSystem data)
        {
            base.CopyTo(data);

            data.CodeSystemCd = CodeSystemCd;
            data.Description = Description;
            data.Notes = Notes;
        }
        public new void Clear()
        {
            base.Clear();

            CodeSystemCd = "";
            Description = null;
            Notes = null;
        }
        #endregion
    }
}