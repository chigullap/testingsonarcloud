﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollTransactionQuickEntry: PayrollTransaction
    {
        #region properties
        [DataMember]
        public short Day { get; set; }

        #endregion

        #region construct/destruct
        public PayrollTransactionQuickEntry()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone

        public override object Clone()
        {
            PayrollTransactionQuickEntry data = new PayrollTransactionQuickEntry();
            CopyTo(data);
            return data;
        }
        public void CopyTo(PayrollTransactionQuickEntry data)
        {
            base.CopyTo(data);
            data.Day = Day;
        }
        public new void Clear()
        {
            base.Clear();
            Day = -1;
        }

        public void RecalcDay(DateTime day1Date)
        {
            Day=(short)((TransactionDate - day1Date).Days+1);
        }

        #endregion

    }
}
