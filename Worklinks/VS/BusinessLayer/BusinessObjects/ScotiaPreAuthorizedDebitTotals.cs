﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ScotiaPreAuthorizedDebitTotals : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public string BusinessTaxNumber
        {
            get { return (string)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public decimal Amount { get; set; }
        [DataMember]
        public string CodePreAuthorizedBankCd { get; set; }
        [DataMember]
        public string PreAuthorizedTransitNumber { get; set; }
        [DataMember]
        public string PreAuthorizedAccountNumber { get; set; }
        [DataMember]
        public string AbaNumber { get; set; }
        [DataMember]
        public DateTime PaymentDueDate { get; set; }
        [DataMember]
        public string ProcessGroupDescription { get; set; }

        #endregion

        #region construct/destruct
        public ScotiaPreAuthorizedDebitTotals()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            ScotiaPreAuthorizedDebitTotals temp = new ScotiaPreAuthorizedDebitTotals();
            this.CopyTo(temp);
            return temp;
        }
        public void CopyTo(ScotiaPreAuthorizedDebitTotals data)
        {
            base.CopyTo(data);

            data.BusinessTaxNumber = BusinessTaxNumber;
            data.Amount = Amount;
            data.CodePreAuthorizedBankCd = CodePreAuthorizedBankCd;
            data.PreAuthorizedTransitNumber = PreAuthorizedTransitNumber;
            data.PreAuthorizedAccountNumber = PreAuthorizedAccountNumber;
            data.AbaNumber = AbaNumber;
            data.PaymentDueDate = PaymentDueDate;
            data.ProcessGroupDescription = ProcessGroupDescription;
        }
        public new void Clear()
        {
            base.Clear();

            BusinessTaxNumber = "";
            Amount = 0;
            CodePreAuthorizedBankCd = "";
            PreAuthorizedTransitNumber = "";
            PreAuthorizedAccountNumber = "";
            AbaNumber = "";
            PaymentDueDate = DateTime.Now;
            ProcessGroupDescription = null;
        }

        #endregion

    }
}
