﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class RoleDescription : WLP.BusinessLayer.BusinessObjects.BusinessObject, ISecurityRoleDescription
    {
        #region properties
        [DataMember]
        public long RoleId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long? SystemRoleId { get; set; }

        [DataMember]
        public String EnglishDesc { get; set; }

        [DataMember]
        public String FrenchDesc { get; set; }

        [DataMember]
        public long? RoleTypeSecurityRoleId { get; set; }

        [DataMember]
        public bool WorklinksAdministrationFlag { get; set; }
        #endregion

        #region construct/destruct
        public RoleDescription()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(RoleDescription data)
        {
            base.CopyTo(data);

            data.RoleId = RoleId;
            data.SystemRoleId = SystemRoleId;
            data.EnglishDesc = EnglishDesc;
            data.FrenchDesc = FrenchDesc;
            data.RoleTypeSecurityRoleId = RoleTypeSecurityRoleId;
            data.WorklinksAdministrationFlag = WorklinksAdministrationFlag;
        }

        public new void Clear()
        {
            base.Clear();

            RoleId = -1;
            SystemRoleId = null;
            EnglishDesc = "";
            FrenchDesc = "";
            RoleTypeSecurityRoleId = null;
            WorklinksAdministrationFlag = false;
        }
        #endregion
    }
}