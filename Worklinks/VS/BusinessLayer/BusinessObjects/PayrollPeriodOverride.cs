﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollPeriodOverride : WLP.BusinessLayer.BusinessObjects.BusinessObject, ICloneable
    {

        #region properties

        [DataMember]
        public long PayrollPeriodOverrideId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public Decimal PeriodYear { get; set; }
        [DataMember]
        public Decimal Period { get; set; }
        [DataMember]
        public String PayrollProcessGroupCode { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public DateTime CutoffDate { get; set; }

        #endregion

        #region construct/destruct
        public PayrollPeriodOverride()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();
            PayrollPeriodOverrideId = -1;
            PeriodYear = -1;
            Period = -1;
            PayrollProcessGroupCode = null;
            StartDate = DateTime.MinValue;
            CutoffDate = DateTime.MinValue;
        }
        public void CopyTo(PayrollPeriodOverride data)
        {
            base.CopyTo(data);
            data.PayrollPeriodOverrideId = PayrollPeriodOverrideId;            
            data.PeriodYear = PeriodYear;
            data.Period = Period;
            data.PayrollProcessGroupCode = PayrollProcessGroupCode;
            data.StartDate = StartDate;
            data.CutoffDate = CutoffDate;
        }
        #endregion
    }
}
