﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class UnionCollectiveAgreementCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<UnionCollectiveAgreement>
    {
    }
}