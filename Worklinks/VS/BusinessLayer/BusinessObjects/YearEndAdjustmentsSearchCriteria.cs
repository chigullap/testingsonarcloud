﻿using System;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class YearEndAdjustmentsSearchCriteria
    {
        #region properties
        public String Year { get; set; }
        public String EmployeeNumber { get; set; }
        public String LastName { get; set; }
        public String SIN { get; set; }
        public String PayrollProcessGroupCode { get; set; }
        public String FormType { get; set; }
        public bool GetMaxRevision { get; set; }
        #endregion

        #region construct/destruct
        public YearEndAdjustmentsSearchCriteria()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public void Clear()
        {
            Year = null;
            EmployeeNumber = null;
            LastName = null;
            SIN = null;
            PayrollProcessGroupCode = null;
            FormType = null;
            GetMaxRevision = false;
        }
        #endregion
    }
}