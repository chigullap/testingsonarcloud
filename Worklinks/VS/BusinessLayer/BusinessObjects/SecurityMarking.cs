﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SecurityMarking: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        
        #region properties
        public override string Key
        {
            get
            {
                return AlternateSecurityMarkingCode??base.Key;
            }
        }
        [DataMember]
        public long SecurityMarkingId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String AlternateSecurityMarkingCode { get; set; }
        [DataMember]
        public bool HasChildrenFlag { get; set; }
        [DataMember]
        public long SecurityCategoryId{get;set;}
        [DataMember]
        public String ReferenceTableIdentifier { get; set; }
        [DataMember]
        public String Description { get; set; }


        #endregion

        #region construct/destruct
        public SecurityMarking()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(SecurityMarking data)
        {
            base.CopyTo(data);

            data.SecurityMarkingId = SecurityMarkingId;
            data.AlternateSecurityMarkingCode = AlternateSecurityMarkingCode;
            data.HasChildrenFlag = HasChildrenFlag;
            data.SecurityCategoryId = SecurityCategoryId;
            data.ReferenceTableIdentifier = ReferenceTableIdentifier;
            data.Description = Description;
        }
        public new void Clear()
        {
            base.Clear();

            SecurityMarkingId = -1;
            AlternateSecurityMarkingCode = null;
            HasChildrenFlag = false;
            SecurityCategoryId = -1;
            ReferenceTableIdentifier = null;
            Description = null;
        }

        #endregion

    }
}
