﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PayrollExportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollExport>
    {
        public bool HasDirectDepositFile
        {
            get
            {
                foreach (PayrollExport payExport in this)
                {
                    if (payExport.EftType == "R820DD")
                        return true;
                }
                return false;
            }
        }
        public bool HasCraGarnishmentFile
        {
            get
            {
                foreach (PayrollExport payExport in this)
                {
                    if (payExport.EftType == "R820GARN")
                        return true;
                }
                return false;
            }
        }
    }
}
