﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PaycodeSalaryMap : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PaycodeSalaryMapId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String CodePaycodeCd { get; set; }
        [DataMember]
        public String CodeEmployeePositionStatusCd { get; set; }
        [DataMember]
        public bool LockSalaryRateFlag { get; set; }
     
        #endregion

        #region construct/destruct
        public PaycodeSalaryMap()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PaycodeSalaryMap data)
        {
            base.CopyTo(data);

            data.PaycodeSalaryMapId = PaycodeSalaryMapId;
            data.CodePaycodeCd = CodePaycodeCd;
            data.CodeEmployeePositionStatusCd = CodeEmployeePositionStatusCd;
            data.LockSalaryRateFlag = LockSalaryRateFlag;
        }
        public new void Clear()
        {
            base.Clear();

            PaycodeSalaryMapId = -1;
            CodePaycodeCd = "";
            CodeEmployeePositionStatusCd = "";
            LockSalaryRateFlag = false;
        }

        #endregion

    }
}
