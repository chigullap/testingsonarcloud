﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeCompanyProperty : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeCompanyPropertyId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public Employee ParentEmployee { get; set; }

        [DataMember]
        public String EmployeeCompanyPropertyCode { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public DateTime? IssueDate { get; set; }

        [DataMember]
        public String IssueCondition { get; set; }

        [DataMember]
        public DateTime? ReturnDate { get; set; }

        [DataMember]
        public String ReturnCondition { get; set; }

        [DataMember]
        public DateTime? ReissueDate { get; set; }

        [DataMember]
        public DateTime? ExpiryDate { get; set; }

        [DataMember]
        public Decimal? Cost { get; set; }

        [DataMember]
        public String Notes { get; set; }
        #endregion

        #region construct/destruct
        public EmployeeCompanyProperty()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();

            EmployeeCompanyPropertyId = -1;
            EmployeeId = -1;
            ParentEmployee = null;
            EmployeeCompanyPropertyCode = null;
            Description = null;
            IssueDate = null;
            IssueCondition = null;
            ReturnDate = null;
            ReturnCondition = null;
            ReissueDate = null;
            ExpiryDate = null;
            Cost = null;
            Notes = null;
        }
        public void CopyTo(EmployeeCompanyProperty data)
        {
            base.CopyTo(data);

            data.EmployeeCompanyPropertyId = EmployeeCompanyPropertyId;
            data.EmployeeId = EmployeeId;
            data.ParentEmployee = ParentEmployee;
            data.EmployeeCompanyPropertyCode = EmployeeCompanyPropertyCode;
            data.Description = Description;
            data.IssueDate = IssueDate;
            data.IssueCondition = IssueCondition;
            data.ReturnDate = ReturnDate;
            data.ReturnCondition = ReturnCondition;
            data.ReissueDate = ReissueDate;
            data.ExpiryDate = ExpiryDate;
            data.Cost = Cost;
            data.Notes = Notes;
        }
        #endregion
    }
}