﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PaycodeYearEndReportMapCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PaycodeYearEndReportMap>
    {
    }
}