﻿using System;
using System.Collections.Generic;
using System.Text;

using System.ComponentModel.DataAnnotations;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class GrievanceCriteria
    {
        #region properties
        public String GrievanceTypeCode { get; set; }
        public String GrievanceStatusCode { get; set; }
        public DateTime? GrievanceStartDate { get; set; }
        public DateTime? GrievanceEndDate { get; set; }
        public String Note { get; set; }
        public int? MaxRecords { get; set; }
        #endregion

        public GrievanceCriteria()
        {
            Clear();
        }

        public void Clear()
        {
            GrievanceTypeCode=null;
            GrievanceStatusCode=null;
            GrievanceStartDate=null;
            GrievanceEndDate=null;
            Note=null;
            MaxRecords = null;
        }
    }

}
