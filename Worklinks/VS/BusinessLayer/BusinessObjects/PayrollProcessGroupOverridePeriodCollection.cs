﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PayrollProcessGroupOverridePeriodCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollProcessGroupOverridePeriod>
    {
    }
}