﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class YearEndNR4rsp : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long YearEndNR4rspId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public int Revision { get; set; }
        [DataMember]
        public long? PreviousRevisionYearEndNR4rspId { get; set; }
        [DataMember]
        public String EmployerNumber { get; set; }
        [DataMember]
        public long EmployeeId { get; set; }
        [DataMember]
        public Decimal Year { get; set; }
        [DataMember]
        public String Box11 { get; set; }
        [DataMember]
        public String Box12 { get; set; }
        [DataMember]
        public String PayerAgentIdentificationNumber { get; set; }
        [DataMember]
        public long? YearEndNr4PayerAgentId { get; set; }
        [DataMember]
        public String Box13 { get; set; }
        [DataMember]
        public String Box14 { get; set; }
        [DataMember]
        public String Box15 { get; set; }
        [DataMember]
        public Decimal? Box16Amount { get; set; }
        [DataMember]
        public Decimal? Box17Amount { get; set; }
        [DataMember]
        public String Box18 { get; set; }
        [DataMember]
        public String Box24 { get; set; }
        [DataMember]
        public String Box25 { get; set; }
        [DataMember]
        public Decimal? Box26Amount { get; set; }
        [DataMember]
        public Decimal? Box27Amount { get; set; }
        [DataMember]
        public String Box28 { get; set; }
        [DataMember]
        public bool ActiveFlag { get; set; }
       
        #endregion

        #region construct/destruct
        public YearEndNR4rsp()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(YearEndNR4rsp data)
        {
            base.CopyTo(data);
            data.YearEndNR4rspId = YearEndNR4rspId;
            data.Revision = Revision;
            data.PreviousRevisionYearEndNR4rspId = PreviousRevisionYearEndNR4rspId;
            data.EmployerNumber = EmployerNumber;
            data.EmployeeId = EmployeeId;
            data.Year = Year;
            data.Box11 = Box11;
            data.Box12 = Box12;
            data.PayerAgentIdentificationNumber = PayerAgentIdentificationNumber;
            data.YearEndNr4PayerAgentId = YearEndNr4PayerAgentId;
            data.Box13 = Box13;
            data.Box14 = Box14;
            data.Box15 = Box15;
            data.Box16Amount = Box16Amount;
            data.Box17Amount = Box17Amount;
            data.Box18 = Box18;
            data.Box24 = Box24;
            data.Box25 = Box25;
            data.Box26Amount = Box26Amount;
            data.Box27Amount = Box27Amount;
            data.Box28 = Box28;
            data.ActiveFlag = ActiveFlag;
        }
        public new void Clear()
        {
            base.Clear();

            YearEndNR4rspId = -1;
            Revision = 0;
            PreviousRevisionYearEndNR4rspId = null;
            EmployerNumber = null;
            EmployeeId = -1;
            Year = 0;
            Box11 = null;
            Box12 = null;
            PayerAgentIdentificationNumber = null;
            YearEndNr4PayerAgentId = -1;
            Box13 = null;
            Box14 = null;
            Box15 = null;
            Box16Amount = null;
            Box17Amount = null;
            Box18 = null;
            Box24 = null;
            Box25 = null;
            Box26Amount = null;
            Box27Amount = null;
            Box28 = null;
            ActiveFlag = true;
        }
        #endregion
    }

}
