﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CanadaRevenueAgencyBondExport : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long CanadaRevenueAgencyBondExportId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long PayrollProcessId { get; set; }
        [DataMember]
        public Decimal TransmitterOrganizationNumber { get; set; }
        [DataMember]
        public Decimal OrganizationNumber { get; set; }
        [DataMember]
        public DateTime? TransmissionDate { get; set; }
        [DataMember]
        public String CodeCanadaRevenueAgencyBondPaymentTypeCd { get; set; }
        [DataMember]
        public Int64? ContactFax { get; set; }
        [DataMember]
		public String ContactEmail { get; set; }
        #endregion

         #region construct/destruct
        public CanadaRevenueAgencyBondExport()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(CanadaRevenueAgencyBondExport data)
        {
            base.CopyTo(data);

            data.CanadaRevenueAgencyBondExportId = CanadaRevenueAgencyBondExportId;
            data.PayrollProcessId = PayrollProcessId;
            data.TransmitterOrganizationNumber = TransmitterOrganizationNumber;
            data.OrganizationNumber = OrganizationNumber;
            data.TransmissionDate = TransmissionDate;
            data.CodeCanadaRevenueAgencyBondPaymentTypeCd = CodeCanadaRevenueAgencyBondPaymentTypeCd;
            data.ContactFax = ContactFax;
            data.ContactEmail = ContactEmail;
        }

        public new void Clear()
        {
            base.Clear();

            CanadaRevenueAgencyBondExportId = -1;
            PayrollProcessId = -1;
            TransmitterOrganizationNumber = 0;
            OrganizationNumber = 0;
            TransmissionDate = null;
            CodeCanadaRevenueAgencyBondPaymentTypeCd = null;
            ContactFax = null;
            ContactEmail = null;            
        }

        #endregion
    }
}
