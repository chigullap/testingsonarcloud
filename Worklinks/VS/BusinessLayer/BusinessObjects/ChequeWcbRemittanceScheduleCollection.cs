﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class ChequeWcbRemittanceScheduleCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<ChequeWcbRemittanceSchedule>
    {
    }
}