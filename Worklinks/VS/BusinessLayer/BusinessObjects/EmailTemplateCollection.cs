﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmailTemplateCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmailTemplate>
    {
    }
}