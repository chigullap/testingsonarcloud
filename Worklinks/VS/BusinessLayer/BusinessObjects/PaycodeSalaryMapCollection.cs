﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PaycodeSalaryMapCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PaycodeSalaryMap>
    {

        public PaycodeSalaryMap GetFirstPaycodeSalaryMapByStatusCode(String statusCode)
        {
           foreach (PaycodeSalaryMap map in this)
            {
                if (map.CodeEmployeePositionStatusCd == statusCode)
                    return map;
            }
            return null;
        }
    }

}
