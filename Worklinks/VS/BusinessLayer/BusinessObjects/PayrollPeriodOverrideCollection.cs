﻿using System;
using System.Runtime.Serialization;


namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PayrollPeriodOverrideCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollPeriodOverride>
    {
    }
}
