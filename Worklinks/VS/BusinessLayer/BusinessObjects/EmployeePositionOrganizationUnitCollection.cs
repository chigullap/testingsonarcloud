﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeePositionOrganizationUnitCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeePositionOrganizationUnit>
    {
        public bool Equals(EmployeePositionOrganizationUnitCollection other)
        {
            foreach (EmployeePositionOrganizationUnit level in this)
            {
                foreach (EmployeePositionOrganizationUnit otherLevel in other)
                {
                    if (otherLevel.OrganizationUnitLevelId.Equals(level.OrganizationUnitLevelId))
                    {
                        if (!level.Equals(otherLevel))
                            return false;
                    }
                }
            }

            return true;

        }

        public string OrganizationUnit
        {
            get
            {
                string organizationUnit = null;
                System.Collections.Generic.List<string> tempList = new System.Collections.Generic.List<string>();

                foreach (EmployeePositionOrganizationUnit unit in this)
                    tempList.Add(unit.OrganizationUnitId == null ? "" : unit.OrganizationUnitId.ToString());

                if (tempList.Count > 0)
                    organizationUnit = "/" + string.Join("/", tempList);

                return organizationUnit;
            }
        }

        //public int GetIndexOfLevel(long organizationUnitLevelId)
        //{
        //    foreach (EmployeePositionOrganizationUnit level in this)
        //    {
        //        if (level.OrganizationUnitLevelId == organizationUnitLevelId)
        //            return IndexOf(level.Key);
        //    }
        //    return -1;

        //}
    }
}