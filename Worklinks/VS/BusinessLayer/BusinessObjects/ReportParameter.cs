﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public class ReportParameter : WLP.BusinessLayer.BusinessObjects.BusinessObject, ICloneable
    {
        #region properties
        [DataMember]
        public long ReportParameterId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long ReportId { get; set; }
        [DataMember]
        public String ParameterName { get; set; }
        [DataMember]
        public int ParameterOrder { get; set; }
        [DataMember]
        public String ParameterType { get; set; }


        #endregion


        #region construct/destruct
        public ReportParameter()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone

        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(ReportParameter data)
        {
            base.CopyTo(data);
            data.ReportParameterId = ReportParameterId;
            data.ReportId = ReportId;
            data.ParameterName = ParameterName;
            data.ParameterOrder = ParameterOrder;
            data.ParameterType = ParameterType;
        }
        public new void Clear()
        {
            base.Clear();
            ReportParameterId = -1;
            ReportId=-1;
            ParameterName = null;
            ParameterOrder = -1;
            ParameterType = null;
        }

        #endregion
    }
}
