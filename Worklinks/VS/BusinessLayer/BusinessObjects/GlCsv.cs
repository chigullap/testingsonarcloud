﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class GlCsv : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long GlCsvId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public Decimal Amount { get; set; }
        [DataMember]
        public String GlAccount { get; set; }
        [DataMember]
        public String Company { get; set; }
        [DataMember]
        public String CostCenter { get; set; }
        [DataMember]
        public String CreditDebit { get; set; }
        #endregion

        #region construct/destruct
        public GlCsv()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(GlCsv data)
        {
            base.CopyTo(data);

            data.GlCsvId = GlCsvId;
            data.Amount = Amount;
            data.GlAccount = GlAccount;
            data.Company = Company;
            data.CostCenter = CostCenter;
            data.CreditDebit = CreditDebit;
        }
        #endregion
    }
}
