﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ChequeHealthTaxRemittanceSchedule : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties

        [DataMember]
        public long HealthTaxId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public string CodeProvinceStateCd { get; set; }

        [DataMember]
        public DateTime RemittanceDate { get; set; }

        [DataMember]
        public DateTime ReportingPeriodStartDate { get; set; }

        [DataMember]
        public DateTime ReportingPeriodEndDate { get; set; }

        [DataMember]
        public string CodeWcbChequeRemittanceFrequencyCd { get; set; }

        [DataMember]
        public string CodeHealthTaxCd { get; set; }
        #endregion

        #region construct/destruct

        public ChequeHealthTaxRemittanceSchedule()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(ChequeHealthTaxRemittanceSchedule data)
        {
            data.HealthTaxId = HealthTaxId;
            data.CodeProvinceStateCd = CodeProvinceStateCd;
            data.RemittanceDate = RemittanceDate;
            data.ReportingPeriodStartDate = ReportingPeriodStartDate;
            data.ReportingPeriodEndDate = ReportingPeriodEndDate;
            data.CodeWcbChequeRemittanceFrequencyCd = CodeWcbChequeRemittanceFrequencyCd;
            data.CodeHealthTaxCd = CodeHealthTaxCd;
        }
        public new void Clear()
        {
            HealthTaxId = -1;
            CodeProvinceStateCd = null;
            RemittanceDate = DateTime.Now;
            ReportingPeriodStartDate = DateTime.Now;
            ReportingPeriodEndDate = DateTime.Now;
            CodeWcbChequeRemittanceFrequencyCd = null;
            CodeHealthTaxCd = null;
        }

        #endregion

    }
}
