﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class YearEndT4arca : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long YearEndT4arcaId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public int Revision { get; set; }

        [DataMember]
        public long? PreviousRevisionYearEndT4arcaId { get; set; }

        [DataMember]
        public String EmployerNumber { get; set; }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public Decimal Year { get; set; }

        [DataMember]
        public Decimal? Box12Amount { get; set; }

        [DataMember]
        public Decimal? Box14Amount { get; set; }

        [DataMember]
        public Decimal? Box16Amount { get; set; }

        [DataMember]
        public Decimal? Box17Amount { get; set; }

        [DataMember]
        public Decimal? Box18Amount { get; set; }

        [DataMember]
        public Decimal? Box20Amount { get; set; }

        [DataMember]
        public Decimal? Box22Amount { get; set; }

        [DataMember]
        public String PersonCustodianName { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }
        #endregion

        #region construct/destruct
        public YearEndT4arca()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(YearEndT4arca data)
        {
            base.CopyTo(data);

            data.YearEndT4arcaId = YearEndT4arcaId;
            data.Revision = Revision;
            data.PreviousRevisionYearEndT4arcaId = PreviousRevisionYearEndT4arcaId;
            data.EmployerNumber = EmployerNumber;
            data.EmployeeId = EmployeeId;
            data.Year = Year;
            data.Box12Amount = Box12Amount;
            data.Box14Amount = Box14Amount;
            data.Box16Amount = Box16Amount;
            data.Box17Amount = Box17Amount;
            data.Box18Amount = Box18Amount;
            data.Box20Amount = Box20Amount;
            data.Box22Amount = Box22Amount;
            data.PersonCustodianName = PersonCustodianName;
            data.ActiveFlag = ActiveFlag;
        }
        public new void Clear()
        {
            base.Clear();

            YearEndT4arcaId = -1;
            Revision = 0;
            PreviousRevisionYearEndT4arcaId = null;
            EmployerNumber = null;
            EmployeeId = -1;
            Year = 0;
            Box12Amount = null;
            Box14Amount = null;
            Box16Amount = null;
            Box17Amount = null;
            Box18Amount = null;
            Box20Amount = null;
            Box22Amount = null;
            PersonCustodianName = null;
            ActiveFlag = true;
        }
        #endregion
    }
}