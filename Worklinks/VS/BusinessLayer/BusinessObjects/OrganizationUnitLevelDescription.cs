﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class OrganizationUnitLevelDescription: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {

        #region properties
        [DataMember]
        public string LanguageCode 
        {
            get { return (String)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long? OrganizationUnitLevelId { get; set; }
        [DataMember]
        public string LevelDescription { get; set; }
        

        #endregion

        #region construct/destruct
        public OrganizationUnitLevelDescription()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            OrganizationUnitLevelDescription ouDesc = new OrganizationUnitLevelDescription();
            this.CopyTo(ouDesc);
            return ouDesc;
        }
        public void CopyTo(OrganizationUnitLevelDescription data)
        {
            base.CopyTo(data);

            data.OrganizationUnitLevelId = OrganizationUnitLevelId;
            data.LanguageCode = LanguageCode;
            data.LevelDescription = LevelDescription;
        }
        public new void Clear()
        {
            base.Clear();

            OrganizationUnitLevelId = null;
            LanguageCode = "";
            LevelDescription = "";
        }

        #endregion

    }
}
