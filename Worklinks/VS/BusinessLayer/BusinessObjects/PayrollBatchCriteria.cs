﻿using System;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class PayrollBatchCriteria
    {
        #region properties
        public long? PayrollBatchId { get; set; }
        public String PayrollBatchStatusCode { get; set; }
        public long? PayrollProcessId { get; set; }
        public String PayrollProcessGroupCode { get; set; }
        public String PayrollProcessRunTypeCode { get; set; }
        public bool IncludePayrollTransaction { get; set; }
        public bool IncludeOnlyApprovedTransactions { get; set; }
        public bool? ProcessedFlag { get; set; }
        public bool PerformingCalc { get; set; }
        public String BatchSource { get; set; }
        public String Description { get; set; }
        #endregion

        #region construct/destruct
        public PayrollBatchCriteria()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public void Clear()
        {
            PayrollBatchId = null;
            PayrollBatchStatusCode = null;
            PayrollProcessId = null;
            PayrollProcessGroupCode = null;
            PayrollProcessRunTypeCode = null;
            IncludePayrollTransaction = false;
            IncludeOnlyApprovedTransactions = true;
            ProcessedFlag = null;
            PerformingCalc = false;
            BatchSource = null;
            Description = null;
        }
        #endregion
    }
}