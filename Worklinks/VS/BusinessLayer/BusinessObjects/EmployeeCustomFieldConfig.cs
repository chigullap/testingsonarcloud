﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeCustomFieldConfig : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeCustomFieldConfigId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String EmployeeCustomFieldNameCode { get; set; }

        [DataMember]
        public String DataTypeCode { get; set; }

        [DataMember]
        public int Length { get; set; }

        [DataMember]
        public int Precision { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public String EnglishDescription { get; set; }

        [DataMember]
        public String FrenchDescription { get; set; }
        #endregion

        #region construct/destruct
        public EmployeeCustomFieldConfig()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeeCustomFieldConfig data)
        {
            base.CopyTo(data);

            data.EmployeeCustomFieldConfigId = EmployeeCustomFieldConfigId;
            data.EmployeeCustomFieldNameCode = EmployeeCustomFieldNameCode;
            data.DataTypeCode = DataTypeCode;
            data.Length = Length;
            data.Precision = Precision;
            data.SortOrder = SortOrder;
            data.ActiveFlag = ActiveFlag;
            data.EnglishDescription = EnglishDescription;
            data.FrenchDescription = FrenchDescription;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeeCustomFieldConfigId = -1;
            EmployeeCustomFieldNameCode = null;
            DataTypeCode = null;
            Length = 0;
            Precision = 0;
            SortOrder = 0;
            ActiveFlag = false;
            EnglishDescription = null;
            FrenchDescription = null;
        }
        #endregion
    }
}