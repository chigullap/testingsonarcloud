﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CanadaRevenueAgencyBondExportFile : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long MasterPayDetailId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long PayrollProcessId { get; set; }
        [DataMember]
        public Decimal TransmitterOrganizationNumber { get; set; }
        [DataMember]
        public Decimal OrganizationNumber { get; set; }
        [DataMember]
        public DateTime? TransmissionDate { get; set; }
        [DataMember]
        public String CodeCanadaRevenueAgencyBondPaymentTypeCd { get; set; }
        [DataMember]
        public Int64? ContactFax { get; set; }
        [DataMember]
		public String ContactEmail { get; set; }
        [DataMember]
        public DateTime ChequeDate { get; set; }
        [DataMember]
        public long SequenceNumber { get; set; }
        [DataMember]
        public String SocialInsuranceNumber { get; set; }
        [DataMember]
        public String EmployeeName { get; set; }
        [DataMember]
        public long CanadaRevenueAgencyBondExportId { get; set; }
        [DataMember]
        public Decimal PPaycodeCurrentDollars { get; set; }
        [DataMember]
        public DateTime? BirthDate { get; set; }
        #endregion

        #region construct/destruct
        public CanadaRevenueAgencyBondExportFile()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(CanadaRevenueAgencyBondExportFile data)
        {
            base.CopyTo(data);

            data.CanadaRevenueAgencyBondExportId = CanadaRevenueAgencyBondExportId;
            data.PayrollProcessId = PayrollProcessId;
            data.TransmitterOrganizationNumber = TransmitterOrganizationNumber;
            data.OrganizationNumber = OrganizationNumber;
            data.TransmissionDate = TransmissionDate;
            data.CodeCanadaRevenueAgencyBondPaymentTypeCd = CodeCanadaRevenueAgencyBondPaymentTypeCd;
            data.ContactFax = ContactFax;
            data.ContactEmail = ContactEmail;
            data.ChequeDate = ChequeDate;
            data.SequenceNumber = SequenceNumber;
            data.SocialInsuranceNumber = SocialInsuranceNumber;
            data.EmployeeName = EmployeeName;
            data.MasterPayDetailId = MasterPayDetailId;
            data.PPaycodeCurrentDollars = PPaycodeCurrentDollars;
            data.BirthDate = BirthDate;
        }

        public new void Clear()
        {
            base.Clear();

            CanadaRevenueAgencyBondExportId = -1;
            PayrollProcessId = -1;
            TransmitterOrganizationNumber = 0;
            OrganizationNumber = 0;
            TransmissionDate = null;
            CodeCanadaRevenueAgencyBondPaymentTypeCd = null;
            ContactFax = null;
            ContactEmail = null;
            ChequeDate = DateTime.Now;
            SequenceNumber = -1;
            SocialInsuranceNumber = null;
            EmployeeName = "";
            MasterPayDetailId = -1;
            PPaycodeCurrentDollars = 0;
            BirthDate = null;
        }

        #endregion
    }
}
