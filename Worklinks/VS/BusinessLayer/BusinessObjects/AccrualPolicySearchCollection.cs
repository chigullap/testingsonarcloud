﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class AccrualPolicySearchCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<AccrualPolicySearch>
    {
    }
}