﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class OrganizationUnitDescription: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {

        #region properties
        [DataMember]
        public string LanguageCode 
        {
            get { return (String)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long? OrganizationUnitId { get; set; }
        [DataMember]
        public string UnitDescription { get; set; }
        #endregion

        #region construct/destruct
        public OrganizationUnitDescription()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            OrganizationUnitDescription organizationUnitDescription = new OrganizationUnitDescription();
            this.CopyTo(organizationUnitDescription);
            return organizationUnitDescription;
        }
        public void CopyTo(OrganizationUnitDescription data)
        {
            base.CopyTo(data);

            data.OrganizationUnitId = OrganizationUnitId;
            data.LanguageCode = LanguageCode;
            data.UnitDescription = UnitDescription;
        }
        public new void Clear()
        {
            base.Clear();

            OrganizationUnitId = null;
            LanguageCode = "";
            UnitDescription = "";
        }
        #endregion

    }
}
