﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ChequeWcbRemittanceSchedule : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties

        [DataMember]
        public String CodeWcbCd
        {
            get { return (String)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public DateTime RemittanceDate { get; set; }
        [DataMember]
        public DateTime ReportingPeriodStartDate { get; set; }
        [DataMember]
        public DateTime ReportingPeriodEndDate { get; set; }
        [DataMember]
        public string CodeWcbChequeRemittanceFrequencyCd { get; set; }

        #endregion

        #region construct/destruct

        public ChequeWcbRemittanceSchedule()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(ChequeWcbRemittanceSchedule data)
        {
            data.CodeWcbCd = CodeWcbCd;
            data.RemittanceDate = RemittanceDate;
            data.ReportingPeriodStartDate = ReportingPeriodStartDate;
            data.ReportingPeriodEndDate = ReportingPeriodEndDate;
            data.CodeWcbChequeRemittanceFrequencyCd = CodeWcbChequeRemittanceFrequencyCd;
        }
        public new void Clear()
        {
            CodeWcbCd = null;
            RemittanceDate = DateTime.Now;
            ReportingPeriodStartDate = DateTime.Now;
            ReportingPeriodEndDate = DateTime.Now;
            CodeWcbChequeRemittanceFrequencyCd = null;
        }

        #endregion

    }
}
