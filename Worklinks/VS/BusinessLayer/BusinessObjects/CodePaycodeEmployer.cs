﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    [XmlRoot("CodePaycode")]
    public class CodePaycodeEmployer : CodePaycode
    {
        #region properties
        [DataMember]
        public long PaycodeEmployerId { get; set; }

        [DataMember]
        public String OffsetGeneralLedgerMask { get; set; }

        [DataMember]
        public new byte[] RowVersion { get; set; }
        #endregion

        #region construct/destruct
        public CodePaycodeEmployer()
        {
            Clear();
            this.PaycodeTypeCode = "6";
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(CodePaycodeEmployer data)
        {
            base.CopyTo(data);
            data.PaycodeEmployerId = PaycodeEmployerId;
            data.OffsetGeneralLedgerMask = OffsetGeneralLedgerMask;
            data.RowVersion = RowVersion;
        }
        public new void Clear()
        {
            base.Clear();
            PaycodeEmployerId = -1;
            OffsetGeneralLedgerMask = null;
            RowVersion = new byte[8];
        }
        #endregion
    }
}