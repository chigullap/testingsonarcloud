﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class VertivSunLifePensionExport : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String EmployeeNumber { get; set; }
        [DataMember]
        public String EmployerRequiredContributionAmount1 { get; set; }
        [DataMember]
        public String MemberVoluntaryMatchedContributionAmount1 { get; set; }
        [DataMember]
        public String MemberVoluntaryUnmatchedContributionAmount1 { get; set; }
        [DataMember]
        public String EmployerVoluntaryMatchingContributionAmount1 { get; set; }
        [DataMember]
        public String MemberVoluntaryMatchedContributionAmount2 { get; set; }
        [DataMember]
        public String MemberVoluntaryUnmatchedContributionAmount2 { get; set; }
        [DataMember]
        public String EmployerRequiredContributionAmount2 { get; set; }
        [DataMember]
        public String MemberVoluntaryMatchedContributionAmount3 { get; set; }
        [DataMember]
        public String MemberVoluntaryUnmatchedContributionAmount3 { get; set; }
        [DataMember]
        public String EmployerVoluntaryMatchingContributionAmount2 { get; set; }
        [DataMember]
        public String EmployerVoluntaryMatchingContributionAmount3 { get; set; }
        [DataMember]
        public String PayStartDate { get; set; }
        [DataMember]
        public String PayEndDate { get; set; }

        #endregion

        #region construct/destruct
        public VertivSunLifePensionExport()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(VertivSunLifePensionExport data)
        {
            base.CopyTo(data);

            data.EmployeeId = EmployeeId;
            data.EmployeeNumber = EmployeeNumber;
            data.EmployerRequiredContributionAmount1 = EmployerRequiredContributionAmount1;
            data.MemberVoluntaryMatchedContributionAmount1 = MemberVoluntaryMatchedContributionAmount1;
            data.MemberVoluntaryUnmatchedContributionAmount1 = MemberVoluntaryUnmatchedContributionAmount1;
            data.EmployerVoluntaryMatchingContributionAmount1 = EmployerVoluntaryMatchingContributionAmount1;
            data.MemberVoluntaryMatchedContributionAmount2 = MemberVoluntaryMatchedContributionAmount2;
            data.MemberVoluntaryUnmatchedContributionAmount2 = MemberVoluntaryUnmatchedContributionAmount2;
            data.EmployerRequiredContributionAmount2 = EmployerRequiredContributionAmount2;
            data.MemberVoluntaryMatchedContributionAmount3 = MemberVoluntaryMatchedContributionAmount3;
            data.MemberVoluntaryUnmatchedContributionAmount3 = MemberVoluntaryUnmatchedContributionAmount3;
            data.EmployerVoluntaryMatchingContributionAmount2 = EmployerVoluntaryMatchingContributionAmount2;
            data.EmployerVoluntaryMatchingContributionAmount3 = EmployerVoluntaryMatchingContributionAmount3;
            data.PayStartDate = PayStartDate = "";
            data.PayEndDate = PayEndDate = "";

        }

        public new void Clear()
        {
            base.Clear();

            EmployeeId = -1;
            EmployeeNumber = "";
            EmployerRequiredContributionAmount1 = "";
            MemberVoluntaryMatchedContributionAmount1 = "";
            MemberVoluntaryUnmatchedContributionAmount1 = "";
            EmployerVoluntaryMatchingContributionAmount1 = "";
            MemberVoluntaryMatchedContributionAmount2 = "";
            MemberVoluntaryUnmatchedContributionAmount2 = "";
            EmployerRequiredContributionAmount2 = "";
            MemberVoluntaryMatchedContributionAmount3 = "";
            MemberVoluntaryUnmatchedContributionAmount3 = "";
            EmployerVoluntaryMatchingContributionAmount2 = "";
            EmployerVoluntaryMatchingContributionAmount3 = "";
            PayStartDate = "";
            PayEndDate = "";
        }

        #endregion

    }
}
