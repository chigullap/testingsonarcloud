﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;


namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PersonAddress : Address
    {
        #region fields
        private long _personAddressId = -1;
        #endregion
        
        #region properties
        public override string Key
        {
            get
            {
                return PersonAddressId.ToString();
            }
        }
        [DataMember]
        public long PersonAddressId
        {
            get { return _personAddressId; }
            set
            {
                KeyChangedEventArgs args = new KeyChangedEventArgs();
                args.OldKey = _personAddressId.ToString();
                _personAddressId = value;
                args.NewKey = Key;
                if (!(args.OldKey ?? String.Empty).Equals(args.NewKey ?? String.Empty) && this.GetType().Equals(typeof(PersonAddress)))
                    OnKeyChanged(args);
            }
        }
        [DataMember]
        public long PersonId { get; set; }
        [DataMember]
        public String PersonAddressTypeCode { get; set; }
        [DataMember]
        public bool PrimaryFlag { get; set; }
        [DataMember]
        public new byte[] RowVersion { get; set; }
        [DataMember]
        public int SharedCount { get; set; }
        public bool IsShared { get { return SharedCount > 0; } }
        #endregion

        #region construct/destruct
        public PersonAddress()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public void CopyTo(PersonAddress data)
        {
            base.CopyTo(data);
            data.PersonAddressId = PersonAddressId;
            data.PersonId = PersonId;
            data.PersonAddressTypeCode = PersonAddressTypeCode;
            data.PrimaryFlag = PrimaryFlag;
            data.RowVersion = RowVersion;
            data.SharedCount = SharedCount;
        }

        public new void Clear()
        {
            base.Clear();
            PersonAddressId=-1;
            PersonId=-1;
            PersonAddressTypeCode=null;
            PrimaryFlag = true;
            RowVersion = new byte[8];
            SharedCount = 0;
        }

        #endregion
    }
}
