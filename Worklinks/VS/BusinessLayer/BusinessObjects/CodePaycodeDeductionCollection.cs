﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class CodePaycodeDeductionCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<CodePaycodeDeduction>
    {
    }
}