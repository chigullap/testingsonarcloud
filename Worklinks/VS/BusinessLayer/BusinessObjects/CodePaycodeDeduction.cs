﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    [XmlRoot("CodePaycode")]
    public class CodePaycodeDeduction : CodePaycode
    {
        #region properties
        [DataMember]
        public long PaycodeDeductionId { get; set; }

        [DataMember]
        public String PaycodeRateBasedOnCode { get; set; }

        [DataMember]
        public bool BeforeIncomeTaxDeductionFlag { get; set; }

        [DataMember]
        public bool BeforeBonusTaxDeductionFlag { get; set; }

        [DataMember]
        public bool BeforeJamaicaTaxDeductionFlag { get; set; }

        [DataMember]
        public bool BeforeRetroactiveTaxDeductionFlag { get; set; }

        [DataMember]
        public new byte[] RowVersion { get; set; }
        #endregion

        #region construct/destruct
        public CodePaycodeDeduction()
        {
            Clear();
            this.PaycodeTypeCode = "3";
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(CodePaycodeDeduction data)
        {
            base.CopyTo(data);

            data.PaycodeDeductionId = PaycodeDeductionId;
            data.PaycodeRateBasedOnCode = PaycodeRateBasedOnCode;
            data.BeforeIncomeTaxDeductionFlag = BeforeIncomeTaxDeductionFlag;
            data.BeforeBonusTaxDeductionFlag = BeforeBonusTaxDeductionFlag;
            data.BeforeJamaicaTaxDeductionFlag = BeforeJamaicaTaxDeductionFlag;
            data.BeforeRetroactiveTaxDeductionFlag = BeforeRetroactiveTaxDeductionFlag;
            data.RowVersion = RowVersion;
        }
        public new void Clear()
        {
            base.Clear();

            PaycodeDeductionId = -1;
            PaycodeRateBasedOnCode = null;
            BeforeIncomeTaxDeductionFlag = false;
            BeforeBonusTaxDeductionFlag = false;
            BeforeJamaicaTaxDeductionFlag = false;
            BeforeRetroactiveTaxDeductionFlag = false;
            RowVersion = new byte[8];
        }
        #endregion
    }
}