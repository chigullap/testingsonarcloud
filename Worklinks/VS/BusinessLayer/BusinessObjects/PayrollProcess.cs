﻿using System;
using System.Runtime.Serialization;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollProcess : PayrollPeriod, ICloneable
    {
        public enum CalculationProcess
        {
            Unknown,
            Calculate,
            Lock,
            Unlock,
            Post,
            ReCalculate,
            Import,
        }

        #region fields
        private long _payrollProcessId = -1;
        #endregion

        #region properties
        public override String Key
        {
            get { return PayrollProcessId.ToString(); }
        }

        [DataMember]
        public long PayrollProcessId
        {
            get { return _payrollProcessId; }
            set
            {
                KeyChangedEventArgs args = new KeyChangedEventArgs();
                args.OldKey = PayrollProcessId.ToString();
                _payrollProcessId = value;
                args.NewKey = Key;
                if (!(args.OldKey ?? String.Empty).Equals(args.NewKey ?? String.Empty) && this.GetType().Equals(typeof(PayrollProcess)))
                    OnKeyChanged(args);
            }
        }

        [DataMember]
        public DateTime? ChequeDate { get; set; }

        [DataMember]
        public String PayrollProcessRunTypeCode { get; set; }

        [DataMember]
        public String PayrollProcessAutoSelectPaycodeModeCode { get; set; }

        [DataMember]
        public bool PreventReclaimsFlag { get; set; }

        [DataMember]
        public bool AutoPayVacationPayThisPeriodFlag { get; set; }

        [DataMember]
        public bool IgnoreCppQppExemptionFlag { get; set; }

        [DataMember]
        public String PayrollProcessStatusCode { get; set; }

        [DataMember]
        public String ProcessException { get; set; }

        [DataMember]
        public Decimal CurrentPayrollYear { get; set; }

        [DataMember]
        public PayrollBatchCollection Batches { get; set; }

        [DataMember]
        public bool LockedFlag { get; set; }

        [DataMember]
        public DateTime? FileTransmissionDate { get; set; }

        [DataMember]
        public DateTime? EftFileTransmissionDate { get; set; }

        [DataMember]
        public String PayslipNote { get; set; }

        [DataMember]
        public String PostedUser { get; set; }

        [DataMember]
        public DateTime? LastCalcDate { get; set; }

        [DataMember]
        public String LastCalcUser { get; set; }

        /// <summary>
        /// contains all batch transactions 
        /// </summary>
        public PayrollTransactionSummaryCollection Transactions
        {
            get
            {
                PayrollTransactionSummaryCollection transactions = new PayrollTransactionSummaryCollection();
                if (Batches != null && Batches.Count > 0)
                {
                    foreach (PayrollBatch batch in Batches)
                    {
                        if (batch.Transactions != null)
                            transactions.Load(batch.Transactions);
                    }
                }

                return transactions;
            }
        }

        /*need this to hold unique rowversion from contact table*/
        [DataMember]
        public new byte[] RowVersion { get; set; }

        //regular property
        [DataMember]
        public String PayrollProcessRunTypeCodeDescription { get; set; }
        #endregion

        #region construct/destruct
        public PayrollProcess()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();
            PayrollProcessId = -1;
            ChequeDate = null;
            PayrollProcessRunTypeCode = null;
            PayrollProcessAutoSelectPaycodeModeCode = null;
            PreventReclaimsFlag = false;
            AutoPayVacationPayThisPeriodFlag = false;
            IgnoreCppQppExemptionFlag = false;
            PayrollProcessStatusCode = null;
            Batches = null;
            ProcessException = null;
            CurrentPayrollYear = 0;
            RowVersion = new byte[8];
            LockedFlag = false;
            FileTransmissionDate = null;
            EftFileTransmissionDate = null;
            PayslipNote = null;
            ProcessedDate = null;
            PostedUser = null;
            LastCalcDate = null;
            LastCalcUser = null;
        }
        public void CopyTo(PayrollProcess data)
        {
            base.CopyTo(data);
            data.PayrollProcessId = PayrollProcessId;
            data.ChequeDate = ChequeDate;
            data.PayrollProcessRunTypeCode = PayrollProcessRunTypeCode;
            data.PayrollProcessAutoSelectPaycodeModeCode = PayrollProcessAutoSelectPaycodeModeCode;
            data.PreventReclaimsFlag = PreventReclaimsFlag;
            data.AutoPayVacationPayThisPeriodFlag = AutoPayVacationPayThisPeriodFlag;
            data.IgnoreCppQppExemptionFlag = IgnoreCppQppExemptionFlag;
            data.PayrollProcessStatusCode = PayrollProcessStatusCode;
            data.ProcessException = ProcessException;
            data.CurrentPayrollYear = CurrentPayrollYear;
            data.ProcessedDate = ProcessedDate;
            data.PostedUser = PostedUser;
            data.LastCalcDate = LastCalcDate;
            data.LastCalcUser = LastCalcUser;

            //batches
            if (Batches != null)
            {
                data.Batches = new PayrollBatchCollection();
                Batches.CopyTo(data.Batches);
            }
            else
                data.Batches = Batches;

            data.RowVersion = this.RowVersion;
            data.LockedFlag = LockedFlag;
            data.FileTransmissionDate = FileTransmissionDate;
            data.EftFileTransmissionDate = EftFileTransmissionDate;
            data.PayslipNote = PayslipNote;
        }
        #endregion
    }
}