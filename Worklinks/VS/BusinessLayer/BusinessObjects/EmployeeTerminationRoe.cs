﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeTerminationRoe : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeTerminationRoeId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long EmployeePositionId { get; set; }
        [DataMember]
        public DateTime? FirstDayWorked { get; set; }
        [DataMember]
        public DateTime? PayrollPeriodCutoffDate { get; set; }
        [DataMember]
        public decimal? Amount { get; set; }
        [DataMember]
        public String CodeEmployeeTerminationRoePaidLeaveCd { get; set; }
        [DataMember]
        public DateTime? ExpectedDateOfRecall { get; set; }
        [DataMember]
        public String CodeEmployeeTerminationRoeDateRecallCd { get; set; }
        [DataMember]
        public String ContactFirstName { get; set; }
        [DataMember]
        public String ContactLastName { get; set; }
        [DataMember]
        public String ContactTelephone { get; set; }
        [DataMember]
        public String ContactTelephoneExtension { get; set; }
        [DataMember]
        public String Comment1 { get; set; }
        [DataMember]
        public String Comment2 { get; set; }
        [DataMember]
        public String Comment3 { get; set; }
        [DataMember]
        public decimal? VacationPay { get; set; }
        [DataMember]
        public String CodeRoeVacationPayTypeCd { get; set; }
        [DataMember]
        public DateTime? StatutoryHolidayPay1Date { get; set; }
        public bool ShouldSerializeStatutoryHolidayPay1Date() { return StatutoryHolidayPay1Date != null; }
        [DataMember]
        public decimal? StatutoryHolidayPay1Amount { get; set; }
        [DataMember]
        public DateTime? StatutoryHolidayPay2Date { get; set; }
        public bool ShouldSerializeStatutoryHolidayPay2Date() { return StatutoryHolidayPay2Date != null; }
        [DataMember]
        public decimal? StatutoryHolidayPay2Amount { get; set; }
        [DataMember]
        public DateTime? StatutoryHolidayPay3Date { get; set; }
        public bool ShouldSerializeStatutoryHolidayPay3Date() { return StatutoryHolidayPay3Date != null; }
        [DataMember]
        public decimal? StatutoryHolidayPay3Amount { get; set; }
        [DataMember]
        public String FirstOtherMoniesCd { get; set; }
        [DataMember]
        public decimal? OtherMoniesFirstAmount { get; set; }
        [DataMember]
        public String SecondOtherMoniesCd { get; set; }
        [DataMember]
        public decimal? OtherMoniesSecondAmount { get; set; }
        [DataMember]
        public String ThirdOtherMoniesCd { get; set; }
        [DataMember]
        public decimal? OtherMoniesThirdAmount { get; set; }
        //used for summary, display only
        [DataMember]
        public DateTime? TerminationDate { get; set; }
        [DataMember]
        public String CodeRoeReasonCd { get; set; }
        [DataMember]
        public DateTime? LastDayPaid { get; set; }


        #endregion

        #region construct/destruct
        public EmployeeTerminationRoe()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeeTerminationRoe data)
        {
            base.CopyTo(data);
            data.EmployeeTerminationRoeId = EmployeeTerminationRoeId;
            data.EmployeePositionId = EmployeePositionId;
            data.FirstDayWorked = FirstDayWorked;
            data.PayrollPeriodCutoffDate = PayrollPeriodCutoffDate;
            data.Amount = Amount;
            data.CodeEmployeeTerminationRoePaidLeaveCd = CodeEmployeeTerminationRoePaidLeaveCd;
            data.ExpectedDateOfRecall = ExpectedDateOfRecall;
            data.CodeEmployeeTerminationRoeDateRecallCd = CodeEmployeeTerminationRoeDateRecallCd;
            data.ContactFirstName = ContactFirstName;
            data.ContactLastName = ContactLastName;
            data.ContactTelephone = ContactTelephone;
            data.ContactTelephoneExtension = ContactTelephoneExtension;
            data.Comment1 = Comment1;
            data.Comment2 = Comment2;
            data.Comment3 = Comment3;
            data.VacationPay = VacationPay;
            data.CodeRoeVacationPayTypeCd = CodeRoeVacationPayTypeCd;
            data.StatutoryHolidayPay1Date = StatutoryHolidayPay1Date;
            data.StatutoryHolidayPay1Amount = StatutoryHolidayPay1Amount;
            data.StatutoryHolidayPay2Date = StatutoryHolidayPay2Date;
            data.StatutoryHolidayPay2Amount = StatutoryHolidayPay2Amount;
            data.StatutoryHolidayPay3Date = StatutoryHolidayPay3Date;
            data.StatutoryHolidayPay3Amount = StatutoryHolidayPay3Amount;
            data.FirstOtherMoniesCd = FirstOtherMoniesCd;
            data.OtherMoniesFirstAmount = OtherMoniesFirstAmount;
            data.SecondOtherMoniesCd = SecondOtherMoniesCd;
            data.OtherMoniesSecondAmount = OtherMoniesSecondAmount;
            data.ThirdOtherMoniesCd = ThirdOtherMoniesCd;
            data.OtherMoniesThirdAmount = OtherMoniesThirdAmount;
            data.TerminationDate = TerminationDate;
            data.CodeRoeReasonCd = CodeRoeReasonCd;
            data.LastDayPaid = LastDayPaid;
        }
        public new void Clear()
        {
            base.Clear();
            EmployeeTerminationRoeId = -1;
            EmployeePositionId = -1;
            FirstDayWorked = null;
            PayrollPeriodCutoffDate = null;
            Amount = null;
            CodeEmployeeTerminationRoePaidLeaveCd = null;
            ExpectedDateOfRecall = null;
            CodeEmployeeTerminationRoeDateRecallCd = null;
            ContactFirstName = null;
            ContactLastName = null;
            ContactTelephone = null;
            ContactTelephoneExtension = null;
            Comment1 = null;
            Comment2 = null;
            Comment3 = null;
            VacationPay = null;
            CodeRoeVacationPayTypeCd = null;
            StatutoryHolidayPay1Date = null;
            StatutoryHolidayPay1Amount = null;
            StatutoryHolidayPay2Date = null;
            StatutoryHolidayPay2Amount = null;
            StatutoryHolidayPay3Date = null;
            StatutoryHolidayPay3Amount = null;
            FirstOtherMoniesCd = null;
            OtherMoniesFirstAmount = null;
            SecondOtherMoniesCd = null;
            OtherMoniesSecondAmount = null;
            ThirdOtherMoniesCd = null;
            OtherMoniesThirdAmount = null;
            TerminationDate = null;
            CodeRoeReasonCd = null;
            LastDayPaid = null;
        }

        #endregion
    }
}
