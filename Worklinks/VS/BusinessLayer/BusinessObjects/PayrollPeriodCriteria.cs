﻿using System;
using System.Collections.Generic;
using System.Text;

using System.ComponentModel.DataAnnotations;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class PayrollPeriodCriteria
    {
        #region properties
        public long? PayrollPeriodId { get; set; }
        public String PayrollProcessGroupCode { get; set; }
        public bool GetMinOpenPeriodFlag { get; set; }
        public bool GetMaxClosedPeriodFlag { get; set; }

        #endregion

        public PayrollPeriodCriteria()
        {
            Clear();
        }

        public void Clear()
        {
            PayrollPeriodId = null;
            PayrollProcessGroupCode = null;
            GetMinOpenPeriodFlag = false;
            GetMaxClosedPeriodFlag = false;
        }
    }

}
