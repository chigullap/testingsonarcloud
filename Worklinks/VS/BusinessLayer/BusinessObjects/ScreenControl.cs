﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;


namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ScreenControl : WLP.BusinessLayer.BusinessObjects.BusinessObject, ICloneable
    {

        #region properties
        [DataMember]
        public String VirtualPath { get; set; }
        [DataMember]
        public String ControlType { get; set; }
        [DataMember]
        public short SortOrder { get; set; }
        [DataMember]
        public String ResourceName { get; set; }

        public override String Key
        {
            get
            {
                return String.Format("{0}|{1}|{2}", VirtualPath, ResourceName, ControlType);
            }
        }

        #endregion

        #region construct/destruct
        public ScreenControl()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();
            VirtualPath = null;
            ControlType = null;
            ResourceName = null;
            SortOrder = 0;
        }

        public void CopyTo(ScreenControl data)
        {
            base.CopyTo(data);
            data.ControlType = ControlType;
            data.ResourceName = ResourceName;
            data.SortOrder = SortOrder;
        }
        #endregion
    }
}
