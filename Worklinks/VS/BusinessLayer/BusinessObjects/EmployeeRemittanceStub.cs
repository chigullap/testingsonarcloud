﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeRemittanceStub : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeRemittanceStubId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeePaycodeId { get; set; }

        [DataMember]
        public String FileName { get; set; }

        [DataMember]
        public String FileTypeCode { get; set; }

        [DataMember]
        public byte[] Data { get; set; }
        #endregion

        #region construct/destruct
        public EmployeeRemittanceStub()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            EmployeeRemittanceStub item = new EmployeeRemittanceStub();
            this.CopyTo(item);
            return item;
        }
        public virtual void CopyTo(EmployeeRemittanceStub data)
        {
            base.CopyTo(data);

            data.EmployeeRemittanceStubId = EmployeeRemittanceStubId;
            data.EmployeePaycodeId = EmployeePaycodeId;
            data.FileName = FileName;
            data.FileTypeCode = FileTypeCode;
            data.Data = Data;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeeRemittanceStubId = -1;
            EmployeePaycodeId = -1;
            FileName = null;
            FileTypeCode = null;
            Data = null;
        }
        #endregion
    }
}