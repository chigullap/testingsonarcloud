﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class AccrualEntitlementDetailPaycode : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long AccrualEntitlementDetailPaycodeId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long AccrualEntitlementDetailId { get; set; }

        [DataMember]
        public String PaycodeCode { get; set; }
        #endregion

        #region construct/destruct
        public AccrualEntitlementDetailPaycode()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(AccrualEntitlementDetailPaycode data)
        {
            base.CopyTo(data);

            data.AccrualEntitlementDetailPaycodeId = AccrualEntitlementDetailPaycodeId;
            data.AccrualEntitlementDetailId = AccrualEntitlementDetailId;
            data.PaycodeCode = PaycodeCode;
        }
        public new void Clear()
        {
            base.Clear();

            AccrualEntitlementDetailPaycodeId = -1;
            AccrualEntitlementDetailId = -1;
            PaycodeCode = "";
        }
        #endregion
    }
}