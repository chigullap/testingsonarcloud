﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class ScotiaBankEdiCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<ScotiaBankEdi>
    {
        public int CountOfRecordsInFile
        {
            get
            {
                int rec = 0;

                foreach (ScotiaBankEdi eft in this)
                {
                    if (eft.DirectDepositFlag)
                        rec++;
                }

                return rec;
            }
        }
    }
}
