﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public class Report : WLP.BusinessLayer.BusinessObjects.BusinessObject, ICloneable
    {

        #region fields
        private ReportParameterCollection _parameters = null;
        #endregion

        #region properties
        [DataMember]
        public long ReportId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String Name { get; set; }
        [DataMember]
        public String ReportServerUrl { get; set; }
        [DataMember]
        public String ReportPath { get; set; }
        [DataMember]
        public ReportParameterCollection Parameters 
        {
            get
            {
                if (_parameters == null)
                    _parameters = new ReportParameterCollection();
                return _parameters;
            }
            set
            {
                _parameters = value;
            }
        }

        #endregion


        #region construct/destruct
        public Report()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone

        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(Report data)
        {
            base.CopyTo(data);
            data.ReportId = ReportId;
            data.Name = Name;
            data.ReportServerUrl = ReportServerUrl;
            data.ReportPath = ReportPath;
            Parameters.CopyTo(data.Parameters);
        }
        public new void Clear()
        {
            base.Clear();
            ReportId = -1;
            Name = null;
            ReportServerUrl = null;
            ReportPath = null;
            Parameters = null;
        }

        #endregion
    }
}
