﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class StatutoryHolidayDate : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long StatutoryHolidayDateId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String EnglishDescription { get; set; }

        [DataMember]
        public String FrenchDescription { get; set; }

        [DataMember]
        public String ProvinceStateCode { get; set; }

        [DataMember]
        public DateTime HolidayDate { get; set; }

        [DataMember]
        public String OrganizationUnit { get; set; }

        [DataMember]
        public String Description { get; set; }
        #endregion

        #region construct/destruct
        public StatutoryHolidayDate()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(StatutoryHolidayDate data)
        {
            base.CopyTo(data);

            data.StatutoryHolidayDateId = StatutoryHolidayDateId;
            data.EnglishDescription = EnglishDescription;
            data.FrenchDescription = FrenchDescription;
            data.ProvinceStateCode = ProvinceStateCode;
            data.HolidayDate = HolidayDate;
            data.OrganizationUnit = OrganizationUnit;
            data.Description = Description;
        }
        public new void Clear()
        {
            base.Clear();

            StatutoryHolidayDateId = -1;
            EnglishDescription = null;
            FrenchDescription = null;
            ProvinceStateCode = null;
            HolidayDate = DateTime.Now;
            OrganizationUnit = null;
            Description = null;
        }
        #endregion
    }
}