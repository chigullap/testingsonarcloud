﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public interface ISecurityRoleDescription
    {
        long RoleId { get; }
        String EnglishDesc { get; set; }
        String FrenchDesc { get; set; }
    }
}
