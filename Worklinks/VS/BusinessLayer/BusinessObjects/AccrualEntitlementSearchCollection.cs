﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class AccrualEntitlementSearchCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<AccrualEntitlementSearch>
    {
    }
}