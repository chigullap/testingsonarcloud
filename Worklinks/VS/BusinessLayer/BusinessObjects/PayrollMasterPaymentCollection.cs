﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PayrollMasterPaymentCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollMasterPayment>
    {
        public Decimal TotalDepositAmount
        {
            get
            {
                Decimal rtn = 0;

                foreach (PayrollMasterPayment item in this)
                {
                    rtn += item.Amount;
                }

                return rtn;
            }
        }
    }
}