﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeSummary : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long PersonId { get; set; }

        [DataMember]
        public string EmployeeNumber { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public string PayrollProcessGroupCode { get; set; }

        [DataMember]
        public string CountryCode { get; set; }

        [DataMember]
        public string OrganizationUnitDescription { get; set; }

        [DataMember]
        public string ImportExternalIdentifier { get; set; }

        [DataMember]
        public string CodeEmployeePositionStatusDescription { get; set; }

        [DataMember]
        public bool ExcludeMassPayslipFileFlag { get; set; }

        [DataMember]
        public Int32? NumberOfRows { get; set; }

        [DataMember]
        public string Description { get; set; }

        public string ChequeName { get { return string.Format("{0}, {1}", LastName, FirstName); } }

        [DataMember]
        public string Email { get; set; }
        #endregion

        #region construct/destruct
        public EmployeeSummary()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeeSummary data)
        {
            base.CopyTo(data);

            data.EmployeeId = EmployeeId;
            data.PersonId = PersonId;
            data.EmployeeNumber = EmployeeNumber;
            data.LastName = LastName;
            data.FirstName = FirstName;
            data.BirthDate = BirthDate;
            data.PayrollProcessGroupCode = PayrollProcessGroupCode;
            data.CountryCode = CountryCode;
            data.OrganizationUnitDescription = OrganizationUnitDescription;
            data.ImportExternalIdentifier = ImportExternalIdentifier;
            data.ExcludeMassPayslipFileFlag = ExcludeMassPayslipFileFlag;
            data.NumberOfRows = NumberOfRows;
            data.Description = Description;
            data.Email = Email;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeeId = -1;
            PersonId = -1;
            EmployeeNumber = null;
            LastName = null;
            FirstName = null;
            BirthDate = null;
            PayrollProcessGroupCode = null;
            CountryCode = null;
            OrganizationUnitDescription = null;
            ImportExternalIdentifier = null;
            ExcludeMassPayslipFileFlag = false;
            NumberOfRows = null;
            Description = null;
            Email = null;
        }
        #endregion
    }
}