﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeRoeAmountSummary:EmployeeRoeAmount
    {
        
        #region properties
        [DataMember]
        public String BusinessNumber { get; set; }
        [DataMember]
        public String PaymentFrequencyCode { get; set; }
        #endregion

        #region construct/destruct
        public EmployeeRoeAmountSummary()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeeRoeAmountSummary data)
        {
            base.CopyTo(data);
            
            data.BusinessNumber = BusinessNumber;
            data.PaymentFrequencyCode = PaymentFrequencyCode;
        }
        public new void Clear()
        {
            base.Clear();

            BusinessNumber = null;
            PaymentFrequencyCode = null;
        }

        #endregion

    }
}
