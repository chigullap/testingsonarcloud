﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeBanking : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeBankingId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        [Required]
        public string EmployeeBankingCode { get; set; }

        [DataMember]
        public string BankingCountryCode { get; set; }

        [DataMember]
        [Required]
        public string CodeEmployeeBankingSequenceCode { get; set; }

        [DataMember]
        [StringLength(9), Required]
        public string Transit_number { get; set; }

        [DataMember]
        [Required]
        public string Account_number { get; set; }

        [DataMember]
        public string AbaNumber { get; set; }

        [DataMember]
        public decimal? Percentage { get; set; }

        [DataMember]
        public decimal? Amount { get; set; }

        [DataMember]
        public string CodeEmployeeBankingRestrictionCode { get; set; }

        [DataMember]
        public string CodeBankAccountTypeCode { get; set; }

        [DataMember]
        public bool OverrideConcurrencyCheckFlag { get; set; }
        #endregion

        #region construct/destruct
        public EmployeeBanking()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeeBanking data)
        {
            base.CopyTo(data);
            data.EmployeeBankingId = EmployeeBankingId;
            data.EmployeeId = EmployeeId;
            data.EmployeeBankingCode = EmployeeBankingCode;
            data.BankingCountryCode = BankingCountryCode;
            data.CodeEmployeeBankingSequenceCode = CodeEmployeeBankingSequenceCode;
            data.Transit_number = Transit_number;
            data.Account_number = Account_number;
            data.AbaNumber = AbaNumber;
            data.Percentage = Percentage;
            data.Amount = Amount;
            data.CodeEmployeeBankingRestrictionCode = CodeEmployeeBankingRestrictionCode;
            data.CodeBankAccountTypeCode = CodeBankAccountTypeCode;
            data.OverrideConcurrencyCheckFlag = OverrideConcurrencyCheckFlag;
        }
        public new void Clear()
        {
            base.Clear();
            EmployeeBankingId = -1;
            EmployeeId = -1;
            EmployeeBankingCode = "";
            BankingCountryCode = "";
            CodeEmployeeBankingSequenceCode = "";
            Transit_number = "";
            Account_number = "";
            AbaNumber = null;
            Percentage = null;
            Amount = null;
            CodeEmployeeBankingRestrictionCode = "";
            OverrideConcurrencyCheckFlag = false;
            CodeBankAccountTypeCode = null;
        }
        #endregion
    }
}