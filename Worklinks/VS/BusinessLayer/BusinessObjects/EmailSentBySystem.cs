﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmailSentBySystem : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmailSentBySystemId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public string EmailTo { get; set; }
        [DataMember]
        public string EmailFrom { get; set; }
        [DataMember]
        public string EmailCC { get; set; }
        [DataMember]
        public string EmailBCC { get; set; }
        [DataMember]
        public string EmailSubject { get; set; }
        [DataMember]
        public string EmailBody { get; set; }
        [DataMember]
        public string EmailSmtp { get; set; }
        [DataMember]
        public bool EmailEnableSSL { get; set; }
        [DataMember]
        public int EmailPort { get; set; }
        [DataMember]
        public string EmailFilename { get; set; }
        [DataMember]
        public byte[] EmailData { get; set; }

        #endregion

        #region construct/destruct
        public EmailSentBySystem()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();

            EmailSentBySystemId = -1;
            EmailTo = null;
            EmailFrom = null;
            EmailCC = null;
            EmailBCC = null;
            EmailSubject = null;
            EmailBody = null;
            EmailSmtp = null;
            EmailEnableSSL = false;
            EmailPort = 0;
            EmailFilename = null;
            EmailData = null;
        }

        public virtual void CopyTo(EmailSentBySystem data)
        {
            base.CopyTo(data);

            data.EmailSentBySystemId = EmailSentBySystemId;
            data.EmailTo = EmailTo;
            data.EmailFrom = EmailFrom;
            data.EmailCC = EmailCC;
            data.EmailBCC = EmailBCC;
            data.EmailSubject = EmailSubject;
            data.EmailBody = EmailBody;
            data.EmailSmtp = EmailSmtp;
            data.EmailEnableSSL = EmailEnableSSL;
            data.EmailPort = EmailPort;
            data.EmailFilename = EmailFilename;
            data.EmailData.CopyTo(data.EmailData, 0);
        }
        #endregion
    }
}
