﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class SalaryPlanGradeAndEffectiveDetailCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<SalaryPlanGradeAndEffectiveDetail>
    {
    }
}