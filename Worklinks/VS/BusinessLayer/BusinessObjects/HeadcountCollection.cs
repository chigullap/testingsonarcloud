﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class HeadcountCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<Headcount>
    {
    }
}