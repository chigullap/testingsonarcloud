﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class CanadaRevenueAgencyT4Collection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<CanadaRevenueAgencyT4>
    {
        public CanadaRevenueAgencyT4Collection()
        {
        }
        public CanadaRevenueAgencyT4Collection(YearEndT4Collection yearEndT4Collection)
        {
            foreach (YearEndT4 t4 in yearEndT4Collection)
            {
                CanadaRevenueAgencyT4 crt4 = new CanadaRevenueAgencyT4();
                t4.CopyTo(crt4);
                this.Add(crt4);
            }
        }
    }
}
