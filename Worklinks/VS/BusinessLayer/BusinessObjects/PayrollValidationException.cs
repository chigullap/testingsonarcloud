﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [DataContract]
    public class PayrollValidationException : Exception
    {
        public PayrollValidationException()
        {
        }

        public PayrollValidationException(string message)
                : base(message)
        {
        }

        public PayrollValidationException(string message, Exception inner)
            : base(message, inner)
        {
        }

    }
}
