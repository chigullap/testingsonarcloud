﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;
using System.ComponentModel.DataAnnotations;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class Address : BusinessObject
    {
        #region fields
        private long _addressId = -1;
        #endregion

        #region properties
        public override string Key
        {
            get
            {
                return AddressId.ToString();
            }
        }

        [DataMember]
        [Required]
        public long AddressId
        {
            get { return _addressId; }
            set
            {
                KeyChangedEventArgs args = new KeyChangedEventArgs();
                args.OldKey = _addressId.ToString();
                _addressId = value;
                args.NewKey = Key;
                if (!(args.OldKey ?? String.Empty).Equals(args.NewKey ?? String.Empty) && this.GetType().Equals(typeof(Address)))
                    OnKeyChanged(args);
            }
        }

        [DataMember]
        [Required]
        public String AddressLine1 { get; set; }

        [DataMember]
        public String AddressLine2 { get; set; }

        [DataMember]
        [Required]
        public String City { get; set; }

        [DataMember]
        public String PostalZipCode { get; set; }

        [DataMember]
        public String ProvinceStateCode { get; set; }

        [DataMember]
        [Required]
        public String CountryCode { get; set; }

        [DataMember]
        public String ThreeCharacterCountryCode { get; set; } //this is used in the T4/T4A/NR4 xml, which requires a 3 char country code, not 2 char.  Not used by the GUI.

        [DataMember]
        public bool OverrideConcurrencyCheckFlag { get; set; }

        public String MailingLabel
        {
            get
            {
                StringBuilder label = new StringBuilder(String.Format("{0}\r\n", AddressLine1));
                if ((AddressLine2 ?? String.Empty).Equals(String.Empty))
                    label.Append(String.Format("{0}\r\n", AddressLine2));
                label.Append(String.Format("{0} {1} {2}", City, ProvinceStateCode, PostalZipCode));

                return label.ToString();
            }
        }
        #endregion

        #region construct/destruct
        public Address()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(Address data)
        {
            base.CopyTo(data);
            data.AddressId = AddressId;
            data.AddressLine1 = AddressLine1;
            data.AddressLine2 = AddressLine2;
            data.City = City;
            data.PostalZipCode = PostalZipCode;
            data.ProvinceStateCode = ProvinceStateCode;
            data.CountryCode = CountryCode;
            data.ThreeCharacterCountryCode = ThreeCharacterCountryCode;
            data.OverrideConcurrencyCheckFlag = OverrideConcurrencyCheckFlag;
        }

        public new void Clear()
        {
            base.Clear();
            AddressId = -1;
            AddressLine1 = null;
            AddressLine2 = null;
            City = null;
            PostalZipCode = null;
            ProvinceStateCode = null;
            CountryCode = null;
            ThreeCharacterCountryCode = null;
            OverrideConcurrencyCheckFlag = false;
        }
        #endregion
    }
}