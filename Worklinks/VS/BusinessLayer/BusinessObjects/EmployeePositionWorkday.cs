﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeePositionWorkday : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeePositionWorkdayId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeePositionId { get; set; }

        [DataMember]
        public bool IsWorkdayFlag { get; set; }

        [DataMember]
        public String DayOfWeek { get; set; }
        #endregion

        #region construct/destruct
        public EmployeePositionWorkday()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            EmployeePositionWorkday item = new EmployeePositionWorkday();
            this.CopyTo(item);

            return item;
        }

        public virtual void CopyTo(EmployeePositionWorkday data)
        {
            base.CopyTo(data);

            data.EmployeePositionWorkdayId = EmployeePositionWorkdayId;
            data.EmployeePositionId = EmployeePositionId;
            data.IsWorkdayFlag = IsWorkdayFlag;
            data.DayOfWeek = DayOfWeek;
        }

        public new void Clear()
        {
            base.Clear();

            EmployeePositionWorkdayId = -1;
            EmployeePositionId = 0;
            IsWorkdayFlag = false;
            DayOfWeek = null;
        }
        #endregion
    }
}