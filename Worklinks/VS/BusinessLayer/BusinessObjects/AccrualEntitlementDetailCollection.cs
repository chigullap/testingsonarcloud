﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class AccrualEntitlementDetailCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<AccrualEntitlementDetail>
    {
    }
}