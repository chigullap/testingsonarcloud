﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class BenefitPlanSearch : BusinessObject
    {
        #region properties
        [DataMember]
        public long BenefitPlanId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long BenefitPlanDescriptionId { get; set; }

        [DataMember]
        public String BenefitPlanDescriptionField { get; set; }
        #endregion

        #region construct/destruct
        public BenefitPlanSearch()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(BenefitPlanSearch data)
        {
            base.CopyTo(data);

            data.BenefitPlanId = BenefitPlanId;
            data.BenefitPlanDescriptionId = BenefitPlanDescriptionId;
            data.BenefitPlanDescriptionField = BenefitPlanDescriptionField;
        }

        public new void Clear()
        {
            base.Clear();

            BenefitPlanId = -1;
            BenefitPlanDescriptionId = -1;
            BenefitPlanDescriptionField = "";
        }
        #endregion
    }
}
