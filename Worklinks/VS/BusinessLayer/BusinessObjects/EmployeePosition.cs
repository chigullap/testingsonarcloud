﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeePosition : WLP.BusinessLayer.BusinessObjects.BusinessObject, IEmployeePosition
    {
        #region properties
        [DataMember]
        public long EmployeePositionId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long? NextEmployeePositionId { get; set; }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        [Required]
        public DateTime? EffectiveDate { get; set; }

        [DataMember]
        public short EffectiveSequence { get; set; }

        [DataMember]
        [Required]
        public String EmployeePositionActionCode { get; set; }

        [DataMember]
        [Required]
        public String EmployeePositionReasonCode { get; set; }

        [DataMember]
        [Required]
        public String EmployeePositionStatusCode { get; set; }

        [DataMember]
        public String PermanencyTypeCode { get; set; }

        [DataMember]
        public String EmploymentTypeCode { get; set; }

        [DataMember]
        public long? ReportsToEmployeeId { get; set; }

        [DataMember]
        public String ShiftCode { get; set; }

        [DataMember]
        public long? SalaryPlanId { get; set; }

        [DataMember]
        public long? SalaryPlanGradeId { get; set; }

        [DataMember]
        public long? SalaryPlanGradeStepId { get; set; }

        [DataMember]
        public String EmployeeTypeCode { get; set; }

        [DataMember]
        [Required]
        public String PaymentMethodCode { get; set; }

        [DataMember]
        [Required]
        public Decimal StandardHours { get; set; }

        [DataMember]
        [Required]
        public Decimal? CompensationAmount { get; set; }

        [DataMember]
        public bool DeleteBankingInfoFlag { get; set; }

        [DataMember]
        public Decimal? LockedStandardHours { get; set; }

        [DataMember]
        public Decimal? LockedCompensationAmount { get; set; }

        [DataMember]
        public long? AccrualPolicyId { get; set; }

        [DataMember]
        public long? LabourUnionId { get; set; }

        [DataMember]
        public DateTime? UnionSeniorityDate { get; set; }

        [DataMember]
        public int? SeniorityRank { get; set; }

        [DataMember]
        public DateTime? CreditedServiceDate { get; set; }

        [DataMember]
        public String EmployeeBankingDeleteAccountCode { get; set; }

        [DataMember]
        public String PayrollProcessGroupCode { get; set; }

        [DataMember]
        public String PaymentFrequencyCode { get; set; }

        [DataMember]
        public Int16 PeriodsPerYear { get; set; }

        //new for terminations
        [DataMember]
        public long? PayrollProcessId { get; set; }

        [DataMember]
        public String CodeTerminationReasonCode { get; set; }

        [DataMember]
        public String Notes { get; set; }

        [DataMember]
        public String ExtraNotes { get; set; }

        [DataMember]
        public String CodeNetPayMethodCode { get; set; }

        [DataMember]
        public String CodeRoeCreationStatusCd { get; set; }

        [DataMember]
        public DateTime? LastDayPaid { get; set; }

        [DataMember]
        public EmployeePositionOrganizationUnitCollection EmployeePositionOrganizationUnits { get; set; }

        [DataMember]
        public EmployeePositionWorkdayCollection Workdays { get; set; }

        [DataMember]
        public EmployeePositionSecondaryCollection SecondaryPositions { get; set; }

        [DataMember]
        public long? BenefitPolicyId { get; set; }

        //regular property
        public String MicrosoftDynamicsStatusActiveCode
        {
            get
            {
                if (EmployeePositionStatusCode == "TER") //HACK - hardcode
                    return "T";
                else
                    return "F";
            }
        }

        #region calcs
        public string OrganizationUnit
        {
            get
            {
                string organizationUnit = null;

                if (EmployeePositionOrganizationUnits != null)
                {
                    System.Collections.Generic.List<string> tempList = new System.Collections.Generic.List<string>();

                    foreach (EmployeePositionOrganizationUnit item in EmployeePositionOrganizationUnits)
                        tempList.Add(item.OrganizationUnitId.ToString());

                    organizationUnit = "/" + string.Join("/", tempList);
                }

                return organizationUnit;
            }
            set
            {
                //throw new NotImplementedException("EmployeePosition:OrganizationUnit:set");
            }
        }
        public Decimal RatePerHour
        {
            get
            {
                if (PaymentMethodCode != null && PaymentMethodCode.ToLower().Equals("s")) //salaried
                {
                    if (StandardHours != 0)
                        return Math.Round((CompensationAmount ?? 0) / StandardHours, 4, MidpointRounding.AwayFromZero);
                    else
                        return 0;
                }
                else
                    return (LockedCompensationAmount != null) ? Convert.ToDecimal(LockedCompensationAmount) : CompensationAmount ?? 0;
            }
        }
        public Decimal AnnualSalary
        {
            get
            {
                if (PaymentMethodCode != null)
                {
                    if (PaymentMethodCode.ToLower().Equals("h"))
                        return Math.Round(((CompensationAmount ?? 0) * StandardHours) * PeriodsPerYear, 4, MidpointRounding.AwayFromZero);
                    else
                        return Math.Round((CompensationAmount ?? 0) * PeriodsPerYear, 4, MidpointRounding.AwayFromZero);
                }
                else
                    return 0;
            }
        }

        //used in termination control to determine if a button is changing the status
        private bool _statusChange = false;

        public bool StatusChange
        {
            get { return _statusChange; }
            set { _statusChange = value; }
        }
        #endregion
        #endregion

        #region construct/destruct
        public EmployeePosition()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            EmployeePosition tempEP = new EmployeePosition();
            this.CopyTo(tempEP);

            return tempEP;
        }
        public EmployeePosition CreateNew()
        {
            EmployeePosition newPosition = (EmployeePosition)this.Clone();
            newPosition.CodeRoeCreationStatusCd = null;
            newPosition.LastDayPaid = null;
            newPosition.CodeTerminationReasonCode = null;
            newPosition.Notes = null;
            newPosition.ExtraNotes = null;
            newPosition.PayrollProcessId = null;
            newPosition.EmployeePositionId = -1;
            newPosition.EffectiveDate = null;
            newPosition.EmployeePositionActionCode = null;
            newPosition.EmployeePositionReasonCode = null;
            newPosition.EffectiveSequence = 1;
            newPosition.EmployeeBankingDeleteAccountCode = null;
            newPosition.BenefitPolicyId = null;

            return newPosition;
        }
        public void CopyTo(EmployeePosition data)
        {
            base.CopyTo(data);
            data.EmployeePositionId = EmployeePositionId;
            data.NextEmployeePositionId = NextEmployeePositionId;
            data.EmployeeId = EmployeeId;
            data.EffectiveDate = EffectiveDate;
            data.EffectiveSequence = EffectiveSequence;
            data.EmployeePositionActionCode = EmployeePositionActionCode;
            data.EmployeePositionReasonCode = EmployeePositionReasonCode;
            data.EmployeePositionStatusCode = EmployeePositionStatusCode;
            data.PermanencyTypeCode = PermanencyTypeCode;
            data.EmploymentTypeCode = EmploymentTypeCode;
            data.ReportsToEmployeeId = ReportsToEmployeeId;
            data.ShiftCode = ShiftCode;
            data.SalaryPlanId = SalaryPlanId;
            data.SalaryPlanGradeId = SalaryPlanGradeId;
            data.SalaryPlanGradeStepId = SalaryPlanGradeStepId;
            data.EmployeeTypeCode = EmployeeTypeCode;
            data.PaymentMethodCode = PaymentMethodCode;
            data.StandardHours = StandardHours;
            data.CompensationAmount = CompensationAmount;
            data.DeleteBankingInfoFlag = DeleteBankingInfoFlag;
            data.PayrollProcessId = PayrollProcessId;
            data.CodeTerminationReasonCode = CodeTerminationReasonCode;
            data.Notes = Notes;
            data.ExtraNotes = ExtraNotes;
            data.CodeNetPayMethodCode = CodeNetPayMethodCode;
            data.CodeRoeCreationStatusCd = CodeRoeCreationStatusCd;

            if (EmployeePositionOrganizationUnits != null)
            {
                EmployeePositionOrganizationUnitCollection newCollection = new EmployeePositionOrganizationUnitCollection();
                EmployeePositionOrganizationUnits.CopyTo(newCollection);
                data.EmployeePositionOrganizationUnits = newCollection;
            }
            else
                data.EmployeePositionOrganizationUnits = null;

            if (Workdays != null)
            {
                EmployeePositionWorkdayCollection newCollection = new EmployeePositionWorkdayCollection();
                Workdays.CopyTo(newCollection);
                data.Workdays = newCollection;
            }
            else
                data.Workdays = null;

            if (SecondaryPositions != null)
            {
                EmployeePositionSecondaryCollection newCollection = new EmployeePositionSecondaryCollection();
                SecondaryPositions.CopyTo(newCollection);
                data.SecondaryPositions = newCollection;
            }
            else
                data.SecondaryPositions = null;

            data.LastDayPaid = LastDayPaid;
            data.LockedStandardHours = LockedStandardHours;
            data.LockedCompensationAmount = LockedCompensationAmount;
            data.AccrualPolicyId = AccrualPolicyId;
            data.LabourUnionId = LabourUnionId;
            data.UnionSeniorityDate = UnionSeniorityDate;
            data.CreditedServiceDate = CreditedServiceDate;
            data.PayrollProcessGroupCode = PayrollProcessGroupCode;
            data.PaymentFrequencyCode = PaymentFrequencyCode;
            data.PeriodsPerYear = PeriodsPerYear;
            data.EmployeeBankingDeleteAccountCode = EmployeeBankingDeleteAccountCode;
            data.SeniorityRank = SeniorityRank;
            data.BenefitPolicyId = BenefitPolicyId;
        }
        public new void Clear()
        {
            base.Clear();
            EmployeePositionId = -1;
            NextEmployeePositionId = null;
            EmployeeId = -1;
            EffectiveDate = null;
            EffectiveSequence = 1;
            EmployeePositionActionCode = null;
            EmployeePositionReasonCode = null;
            EmployeePositionStatusCode = null;
            PermanencyTypeCode = null;
            EmploymentTypeCode = null;
            ReportsToEmployeeId = null;
            ShiftCode = null;
            SalaryPlanId = null;
            SalaryPlanGradeId = null;
            SalaryPlanGradeStepId = null;
            EmployeeTypeCode = null;
            PaymentMethodCode = null;
            StandardHours = 0;
            DeleteBankingInfoFlag = false;
            CompensationAmount = null;
            EmployeePositionOrganizationUnits = null;
            Workdays = null;
            SecondaryPositions = null;
            PayrollProcessId = null;
            CodeTerminationReasonCode = null;
            Notes = null;
            ExtraNotes = null;
            CodeNetPayMethodCode = null;
            CodeRoeCreationStatusCd = null;
            LastDayPaid = null;
            LockedStandardHours = null;
            LockedCompensationAmount = null;
            AccrualPolicyId = null;
            LabourUnionId = null;
            UnionSeniorityDate = null;
            CreditedServiceDate = null;
            PayrollProcessGroupCode = null;
            PaymentFrequencyCode = null;
            PeriodsPerYear = 0;
            EmployeeBankingDeleteAccountCode = null;
            SeniorityRank = null;
            BenefitPolicyId = null;
        }
        #endregion

        public EmployeePosition GetBasePositionRecord()
        {
            EmployeePosition baseTermPosition = new EmployeePosition();
            baseTermPosition.EmployeeId = EmployeeId;
            baseTermPosition.PermanencyTypeCode = PermanencyTypeCode;
            baseTermPosition.EmploymentTypeCode = EmploymentTypeCode;
            baseTermPosition.ReportsToEmployeeId = ReportsToEmployeeId;
            baseTermPosition.EmployeePositionStatusCode = EmployeePositionStatusCode;
            baseTermPosition.ShiftCode = ShiftCode;
            baseTermPosition.SalaryPlanId = SalaryPlanId;
            baseTermPosition.SalaryPlanGradeId = SalaryPlanGradeId;
            baseTermPosition.SalaryPlanGradeStepId = SalaryPlanGradeStepId;
            baseTermPosition.EmployeeTypeCode = EmployeeTypeCode;
            baseTermPosition.PaymentMethodCode = PaymentMethodCode;
            baseTermPosition.StandardHours = StandardHours;
            baseTermPosition.CompensationAmount = CompensationAmount;
            baseTermPosition.DeleteBankingInfoFlag = DeleteBankingInfoFlag;
            baseTermPosition.CodeTerminationReasonCode = CodeTerminationReasonCode;
            baseTermPosition.Notes = Notes;
            baseTermPosition.ExtraNotes = ExtraNotes;
            baseTermPosition.CodeNetPayMethodCode = CodeNetPayMethodCode;
            baseTermPosition.EmployeeBankingDeleteAccountCode = EmployeeBankingDeleteAccountCode;

            if (EmployeePositionOrganizationUnits != null)
            {
                EmployeePositionOrganizationUnitCollection newCollection = new EmployeePositionOrganizationUnitCollection();
                EmployeePositionOrganizationUnits.CopyTo(newCollection);
                baseTermPosition.EmployeePositionOrganizationUnits = newCollection;
            }
            else
                baseTermPosition.EmployeePositionOrganizationUnits = null;

            if (Workdays != null)
            {
                EmployeePositionWorkdayCollection collection = new EmployeePositionWorkdayCollection();
                Workdays.CopyTo(collection);
                baseTermPosition.Workdays = collection;
            }
            else
                Workdays = null;

            if (SecondaryPositions != null)
            {
                EmployeePositionSecondaryCollection collection = new EmployeePositionSecondaryCollection();
                SecondaryPositions.CopyTo(collection);
                baseTermPosition.SecondaryPositions = collection;
            }
            else
                SecondaryPositions = null;

            baseTermPosition.BenefitPolicyId = BenefitPolicyId;

            return baseTermPosition;
        }
        public void SetDefaultEffectiveDates()
        {
            foreach (EmployeePositionOrganizationUnit item in EmployeePositionOrganizationUnits)
                item.OrganizationUnitStartDate = EffectiveDate;
        }
        public bool IsTermRelated
        {
            get { return this.EmployeePositionActionCode != null && (EmployeePositionActionCode.Equals("TERM") || EmployeePositionActionCode.Equals("LEAV") || EmployeePositionActionCode.Equals("BENLEAV")); }
        }
    }
}