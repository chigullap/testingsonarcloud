﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects.Xsd
{
    public class WorkLinksPersonAddressCollection : List<WorkLinksPersonAddress>
    {

        public WorkLinksPersonAddress GetAddressByType(String type)
        {
            foreach (WorkLinksPersonAddress address in this)
            {
                if (address.PersonAddressTypeCode.Equals(type))
                    return address;
            }

            return null;
        }
    }
}
