﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Xsd
{
    public class WorkLinksEmployeePosition : EmployeePosition, IImportEmployeeIdentifier
    {
        #region properties
        [DataMember]
        [Required]
        public String ImportExternalIdentifier { get; set; }

        public String EmployeeExternalIdentifier
        {
            get { return ImportExternalIdentifier; }
            set { ImportExternalIdentifier = value; }
        }

        [DataMember]
        public String EmployeePositionReasonCodeExternalIdentifier { get; set; }

        [DataMember]
        public String PermanencyTypeCodeExternalIdentifier { get; set; }

        [DataMember]
        public String EmploymentTypeCodeExternalIdentifier { get; set; }

        [DataMember]
        public String EmployeeTypeCodeExternalIdentifier { get; set; }

        [DataMember]
        public String PaymentMethodCodeExternalIdentifier { get; set; }

        [DataMember]
        [Required]
        public String OrganationUnitImportExternalIdentifier1 { get; set; }

        public EmployeePositionCollection EmployeePositionCollection
        {
            get
            {
                EmployeePositionCollection collection = new EmployeePositionCollection();
                EmployeePosition employeePosition = new EmployeePosition();
                base.CopyTo(employeePosition);
                collection.Add(employeePosition);

                return collection;
            }
        }
        public bool SkipUpdate { get; set; } = false;
        #endregion

        public bool MeetsPositionRequirements(String decodeCodeValidationMessage, EmployeePosition dbPosition, String orgUnitValidationMessage)
        {
            return GetValidationMessage(decodeCodeValidationMessage, dbPosition, orgUnitValidationMessage) == null;
        }
        public String GetValidationMessage(String decodeCodeValidationMessage, EmployeePosition dbPosition, String orgUnitValidationMessage)
        {
            String validationMessage = null;
            System.Text.StringBuilder output = new System.Text.StringBuilder();

            if (!IsValid)
                output.Append(GetValidationInformation());

            if (decodeCodeValidationMessage != null)
                output.AppendLine(decodeCodeValidationMessage);

            if (dbPosition != null)
            {
                if (dbPosition.EffectiveDate > EffectiveDate)
                    output.AppendLine("Employee Position effective date is prior to last position effective date.");

                if ((EmployeePositionActionCode == "RH" || EmployeePositionActionCode == "RHRESET" ) && dbPosition.EmployeePositionStatusCode == "AC")
                    output.AppendLine("Employee is active, cannot rehire.");
            }

            if (orgUnitValidationMessage != null)
                output.AppendLine(orgUnitValidationMessage);

            if (output.Length > 0)
            {
                //add employee number to message
                validationMessage = String.Format("Employee {0}: ", ImportExternalIdentifier) + Environment.NewLine;
                validationMessage += output.ToString();
            }

            return validationMessage;
        }

        //checks object with respect to import parms for changes
        public bool Equals(EmployeePosition position)
        {
            return this.EffectiveDate == position.EffectiveDate
                 && this.EmployeePositionReasonCode == position.EmployeePositionReasonCode
                 && this.PermanencyTypeCode == position.PermanencyTypeCode
                 && this.EmploymentTypeCode == position.EmploymentTypeCode
                 && this.EmployeeTypeCode == position.EmployeeTypeCode
                 && this.PaymentMethodCode == position.PaymentMethodCode
                 && this.StandardHours == position.StandardHours
                 && this.CompensationAmount == position.CompensationAmount
                 && this.EmployeePositionOrganizationUnits.Equals(position.EmployeePositionOrganizationUnits);

        }


    }


}