﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Xsd
{
    [Serializable]
    [DataContract]
    public class WorklinksStatutoryDeduction : StatutoryDeduction, IImportEmployeeIdentifier
    {
        #region properties
        [DataMember]
        [Required]
        public String ImportExternalIdentifier { get; set; }

        public String EmployeeExternalIdentifier
        {
            get { return ImportExternalIdentifier; }
            set { ImportExternalIdentifier = value; }
        }

        [DataMember]
        public String StatutoryDeductionTypeCodeImportExternalIdentifier { get; set; }

        [DataMember]
        public String ProvinceStateCodeExternalIdentifier { get; set; }

        [DataMember]
        public String EmployerExternalIdentifier { get; set; }

        [DataMember]
        public bool PayEmploymentInsuranceFlagExistsFlag { get; set; }

        [DataMember]
        public bool PayCanadaPensionPlanFlagExistsFlag { get; set; }

        [DataMember]
        public bool ParentalInsurancePlanFlagExistsFlag { get; set; }

        [DataMember]
        public bool FederalTaxClaimExistsFlag { get; set; }

        [DataMember]
        public bool FederalAdditionalTaxExistsFlag { get; set; }

        [DataMember]
        public bool FederalDesignatedAreaDeductionExistsFlag { get; set; }

        [DataMember]
        public bool FederalAuthorizedAnnualDeductionExistsFlag { get; set; }

        [DataMember]
        public bool FederalAuthorizedAnnualTaxCreditExistsFlag { get; set; }

        [DataMember]
        public bool FederalLabourTaxCreditExistsFlag { get; set; }

        [DataMember]
        public bool FederalPayTaxFlagExistsFlag { get; set; }

        [DataMember]
        public bool ProvincialTaxClaimExistsFlag { get; set; }

        [DataMember]
        public bool ProvincialAdditionalTaxExistsFlag { get; set; }

        [DataMember]
        public bool ProvincialDesignatedAreaDeductionExistsFlag { get; set; }

        [DataMember]
        public bool ProvincialAuthorizedAnnualDeductionExistsFlag { get; set; }

        [DataMember]
        public bool ProvincialAuthorizedAnnualTaxCreditExistsFlag { get; set; }

        [DataMember]
        public bool ProvincialLabourTaxCreditExistsFlag { get; set; }

        [DataMember]
        public bool ProvincialPayTaxFlagExistsFlag { get; set; }

        [DataMember]
        public bool EstimatedAnnualIncomeExistsFlag { get; set; }

        [DataMember]
        public bool EstimatedAnnualExpenseExistsFlag { get; set; }

        [DataMember]
        public bool EstimatedNetIncomeExistsFlag { get; set; }

        [DataMember]
        public bool CommissionPercentageExistsFlag { get; set; }

        public StatutoryDeductionCollection StatutoryDeductionCollection
        {
            get
            {
                StatutoryDeductionCollection collection = new StatutoryDeductionCollection();
                StatutoryDeduction statDeduction = new StatutoryDeduction();
                base.CopyTo(statDeduction);
                collection.Add(statDeduction);

                return collection;
            }
        }
        #endregion

        public bool MeetsStatDeductionRequirements(String decodeCodeValidationMessage)
        {
            return GetValidationMessage(decodeCodeValidationMessage) == null;
        }
        public String GetValidationMessage(String decodeCodeValidationMessage)
        {
            String validationMessage = null;
            System.Text.StringBuilder output = new System.Text.StringBuilder();

            if (!IsValid)
                output.Append(GetValidationInformation());

            if (decodeCodeValidationMessage != null)
                output.AppendLine(decodeCodeValidationMessage);

            if (output.Length > 0)
            {
                //add employee number to message
                validationMessage = String.Format("Employee {0}: ", ImportExternalIdentifier) + Environment.NewLine;
                validationMessage += output.ToString();
            }

            return validationMessage;
        }
    }
}