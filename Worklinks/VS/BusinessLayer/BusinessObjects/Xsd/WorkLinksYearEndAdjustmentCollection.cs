﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects.Xsd
{
    [Serializable]
    [CollectionDataContract]
    public class WorkLinksYearEndAdjustmentCollection : List<WorkLinksYearEndAdjustment> //changed to List to fix import issue Jan 27/2016.  Had dupe key issue but we dont need unique keys for this.
    {
        //public List<YearEndAdjustment> YearEndAdjustmentCollection
        //{
        //    get
        //    {
        //        List<YearEndAdjustment> rtn = new List<YearEndAdjustment>();
        //        foreach (YearEndAdjustment adjustment in this)
        //        {
        //            rtn.Add(adjustment);
        //        }
        //        return rtn;
        //    }
        //}
        public KeyValuePair<int, String> GetValidationInformationWarning()
        {
            StringBuilder warnings = new StringBuilder();
            int warningCount = 0;
            foreach (WorkLinksYearEndAdjustment adjustment in this)
            {
                if (adjustment.WarningMessage != null)
                {
                    warnings.AppendLine(adjustment.WarningMessage);
                    warningCount++;
                }
            }

            return new KeyValuePair<int, string>(warningCount, warnings.ToString());
        }

    
        public string GetValidationInformationFail(Decimal year)
        {
            StringBuilder rtn = new StringBuilder();
            
            foreach (WorkLinksYearEndAdjustment adjustment in this)
            {
                if (adjustment.ErrorMessage(year) != null)
                    rtn.AppendLine(adjustment.ErrorMessage(year) ?? "");
            }

            return rtn.Length>0?rtn.ToString():null;
        }

    }
}
