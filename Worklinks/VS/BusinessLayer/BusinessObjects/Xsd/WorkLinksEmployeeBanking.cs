﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Xsd
{
    [Serializable]
    [DataContract]
    public class WorkLinksEmployeeBanking : EmployeeBanking, IImportEmployeeIdentifier
    {
        #region properties
        [DataMember]
        [Required]
        public String ImportExternalIdentifier { get; set; }

        public String EmployeeExternalIdentifier
        {
            get { return ImportExternalIdentifier; }
            set { ImportExternalIdentifier = value; }
        }

        [DataMember]
        public String SequenceImportExternalIdentifier { get; set; }

        [DataMember]
        public String BankingCountryCodeImportExternalIdentifier { get; set; }

        [DataMember]
        public String BankingAccountTypeCodeImportExternalIdentifier { get; set; }

        [DataMember]
        public String BankingCodeImportExternalIdentifier { get; set; }

        [DataMember]
        public String BankingRestrictionExternalIdentifier { get; set; }

        [DataMember]
        public bool DeleteFlag { get; set; }

        public EmployeeBankingCollection EmployeeBankingCollection
        {
            get
            {
                EmployeeBankingCollection collection = new EmployeeBankingCollection();
                EmployeeBanking employeeBanking = new EmployeeBanking();
                base.CopyTo(employeeBanking);
                collection.Add(employeeBanking);

                return collection;
            }
        }
        #endregion
        public String DecodeCodeValidationMessage { get; set; }


        public void SetDecodeCodeValidationMessage(String message)
        {
            DecodeCodeValidationMessage = ((DecodeCodeValidationMessage) ?? "") + message + Environment.NewLine;
        }
        public bool MeetsBankingRequirements()
        {
            return GetValidationMessage() == null;
        }
        public String GetValidationMessage()
        {
            String validationMessage = null;
            System.Text.StringBuilder output = new System.Text.StringBuilder();

            if (DeleteFlag)
            {
                //delete we just check sequence exists
                if (CodeEmployeeBankingSequenceCode==null)
                    output.AppendLine("WorkLinksEmployeeBanking:Cannot find bank sequence to delete.");
            }
            else
            {
                if (!IsValid)
                    output.Append(GetValidationInformation());

                if (Amount != null && Percentage != null)
                    output.AppendLine("WorkLinksEmployeeBanking:Either the amount or the percentage should be provided, but not both.");

                if (CodeEmployeeBankingSequenceCode == "99" && DeleteFlag)
                    output.AppendLine("WorkLinksEmployeeBanking:NET banking sequences cannot be deleted.");

                if (CodeEmployeeBankingSequenceCode == "99" && Percentage != 100)
                    output.AppendLine("WorkLinksEmployeeBanking:NET banking sequences must have a percentage of 100.");

                if (BankingCountryCode == "US" && (CodeBankAccountTypeCode == null || CodeBankAccountTypeCode.Length == 0))
                    output.AppendLine("WorkLinksEmployeeBanking:If country is US, account type must be provided.");

                if (BankingCountryCode == "CA" && Transit_number.Length > 5)
                    output.AppendLine("WorkLinksEmployeeBanking:If country is not US, transit number must be 5 digits.");

                if (BankingCountryCode == "US" && (Transit_number.Length < 5 || Transit_number.Length > 9))
                    output.AppendLine("WorkLinksEmployeeBanking:If country is US, transit number must be between 5-9 digits.");

                if (BankingCountryCode == "JM" && (AbaNumber == null || AbaNumber.Length == 0))
                    output.AppendLine("WorkLinksEmployeeBanking:If country is JM, aba number must be provided.");
                else if (BankingCountryCode == "JM" && AbaNumber.Length != 9)
                    output.AppendLine("WorkLinksEmployeeBanking:If country is JM, aba number must be 9 digits.");

                if (DecodeCodeValidationMessage != null)
                    output.AppendLine(DecodeCodeValidationMessage);
            }

            if (output.Length > 0)
            {
                //add employee number to message
                validationMessage = String.Format("Employee {0}: ", ImportExternalIdentifier) + Environment.NewLine;
                validationMessage += output.ToString();
            }

            return validationMessage;
        }
    }
}