﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace WorkLinks.BusinessLayer.BusinessObjects.Xsd
{
    [Serializable]
    [DataContract]
    public class WorkLinksYearEndAdjustment : YearEndAdjustment, IEmployeeIdentifier
    {
        #region properties
        [DataMember]
        [Required]
        public String ImportExternalIdentifier { get; set; }

        public String EmployeeImportExternalIdentifier
        {
            get { return ImportExternalIdentifier; }
            set { ImportExternalIdentifier = value; }
        }

        [DataMember]
        [Required]
        public String FormType { get; set; }

        [DataMember]
        [Required]
        public String WorkLinksBoxNumber { get; set; }

        [DataMember]
        public String ProvinceStateCode { get; set; }

        [DataMember]
        [Required]
        public String BusinessNumber { get; set; }

        [DataMember]
        public long? YearEndTableId { get; set; }

        public bool MaximumReachedFlag { get; set; } = false;

        public bool NoExistingRecordFlag { get; set; } = false;

        public bool BothAmountsExistFlag { get; set; } = false;

        public bool NeitherAmountsExistFlag { get; set; } = false;

        [DataMember]
        [Required]
        public int Year { get; set; }

        public override String TotalValue
        {
            get
            {
                if (MaximumReachedFlag)
                    return MaximumValue;
                else
                    return base.TotalValue;
            }
        }

        public long EmployeeId { get; set; }

        public String PayrollProcessGroupCode { get; set; }

        public bool HasErrors { get; set; }
        #endregion

        public void CopyFromAndPreValidate(YearEndAdjustment data)
        {
            this.DataType = "numeric";

            if (this.AdjustmentValue == null && this.NumericValue == null)
                NeitherAmountsExistFlag = true;

            if (this.AdjustmentValue != null && this.NumericValue != null)
                BothAmountsExistFlag = true;

            Decimal? numericValue = this.NumericValue;
            Decimal? adjustmentValue = this.AdjustmentValue;

            data.CopyTo(this);

            this.ColumnValue = Convert.ToString(numericValue ?? this.NumericValue);
            this.AdjustmentValue = adjustmentValue ?? this.AdjustmentValue;

            if (MaximumValue != null && base.IsNumericFlag && Convert.ToDecimal(base.TotalValue) > Maximum)
            {
                
                MaximumReachedFlag = true;
            }
        }

        public String WarningMessage
        {
            get
            {
                String message = null;

                if (this.MaximumReachedFlag)
                {
                    message = FormatValidationMessage(true, "Maximum Reached");
                }

                return message;
            }
        }

        public String ErrorMessage(Decimal year)
        {
            StringBuilder rtn = new StringBuilder();

            if (!this.IsValid)
                rtn.AppendLine(FormatValidationMessage(false, this.GetValidationInformation()));
            if (NoExistingRecordFlag)
                rtn.AppendLine(FormatValidationMessage(false, "No existing year end record exists."));
            if (this.Year != year)
                rtn.AppendLine(FormatValidationMessage(false, "Imported year doesn't match current year end."));
            if (this.NeitherAmountsExistFlag)
                rtn.AppendLine(FormatValidationMessage(false, "Adjustment and total amount missing."));
            if (this.BothAmountsExistFlag)
                rtn.AppendLine(FormatValidationMessage(false, "Adjustment and total amount cannot both exist."));
            if ((this.ProvinceStateCode.Trim() ?? String.Empty).Equals(String.Empty) && this.FormType.ToLower().Equals("t4"))
                rtn.AppendLine(FormatValidationMessage(false, "T4 must have a province."));

            return rtn.Length > 0 ? rtn.ToString() : null;
        }

        private String FormatValidationMessage(bool warningFlag, String message)
        {
            return String.Format("{0}:::Year:{1},BusinessNumber:{2},Province:{3},Employee:{4},TotalAmount:{5},FormType:{6},BoxNumber:{7},AdjustmentAmount:{8},Message:{9}",
                warningFlag ? "Warning" : "Error - import failure", this.Year, this.BusinessNumber, this.ProvinceStateCode, this.EmployeeImportExternalIdentifier, this.ColumnValue, this.FormType, this.WorkLinksBoxNumber, this.AdjustmentValue, message);
        }
    }
}