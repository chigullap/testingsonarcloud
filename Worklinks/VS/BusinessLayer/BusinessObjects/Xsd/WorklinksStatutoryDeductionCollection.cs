﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects.Xsd
{
    [Serializable]
    [CollectionDataContract]
    public class WorklinksStatutoryDeductionCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<WorklinksStatutoryDeduction>
    {
    }
}
