﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects.Xsd
{
    [Serializable]
    [DataContract]
    public class WorkLinksContactChannel : PersonContactChannel
    {
        [DataMember]
        public String ImportExternalIdentifier { get; set; }
        [DataMember]
        public bool DeleteFlag { get; set; }
    }
}
