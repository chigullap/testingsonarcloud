﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Xsd
{
    [Serializable]
    [CollectionDataContract]
    public class WorkLinksContactChannelCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<WorkLinksContactChannel>
    {
        #region Overrides of DataItemCollection<WorkLinksContactChannel>

        /// <summary>Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.</summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.</exception>
        public override void Add(WorkLinksContactChannel item)
        {
            if (item != null && Count > 0)
            {
                if (item.PersonContactChannelId < 0)
                {
                    if (MinimumKeyValue <= item.PersonContactChannelId)
                    {
                        item.PersonContactChannelId = MinimumKeyValue.Value - 1;
                    }
                }
            }

            base.Add(item);
        }

        #endregion
    }
}
