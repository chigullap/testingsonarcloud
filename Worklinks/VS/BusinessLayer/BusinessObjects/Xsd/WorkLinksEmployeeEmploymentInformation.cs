﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Xsd
{
    [Serializable]
    [DataContract]
    public class WorkLinksEmployeeEmploymentInformation : EmployeeEmploymentInformation, IImportEmployeeIdentifier
    {
        #region properties
        [DataMember]
        [Required]
        public String ImportExternalIdentifier { get; set; }

        public String EmployeeExternalIdentifier
        {
            get { return ImportExternalIdentifier; }
            set { ImportExternalIdentifier = value; }
        }

        [DataMember]
        public String PayrollProcessGroupCodeExternalIdentifier { get; set; }

        [DataMember]
        public String WsibCodeExternalIdentifier { get; set; }

        public EmployeeEmploymentInformationCollection EmployeeEmploymentInformationCollection
        {
            get
            {
                EmployeeEmploymentInformationCollection collection = new EmployeeEmploymentInformationCollection();
                EmployeeEmploymentInformation employeeEmploymentInformation = new EmployeeEmploymentInformation();
                base.CopyTo(employeeEmploymentInformation);
                collection.Add(employeeEmploymentInformation);

                return collection;
            }
        }
        #endregion

        public bool MeetsEmploymentRequirements(String decodeCodeValidationMessage, bool isHireDateValidationEnabled = true)
        {
            return GetValidationMessage(decodeCodeValidationMessage, isHireDateValidationEnabled) == null;
        }
        public String GetValidationMessage(String decodeCodeValidationMessage, bool isHireDateValidationEnabled = true)
        {
            String validationMessage = null;
            System.Text.StringBuilder output = new System.Text.StringBuilder();

            if (!IsValid)
            {
                String getValidationInformation = GetValidationInformation();

                if (!isHireDateValidationEnabled)
                    getValidationInformation = getValidationInformation.Replace("WorkLinksEmployeeEmploymentInformation:The HireDate field is required.\r\n", null);

                output.Append(getValidationInformation);
            }

            if (decodeCodeValidationMessage != null)
                output.AppendLine(decodeCodeValidationMessage);

            if (output.Length > 0)
            {
                //add employee number to message
                validationMessage = String.Format("Employee {0}: ", ImportExternalIdentifier) + Environment.NewLine;
                validationMessage += output.ToString();
            }

            return validationMessage;
        }
    }
}