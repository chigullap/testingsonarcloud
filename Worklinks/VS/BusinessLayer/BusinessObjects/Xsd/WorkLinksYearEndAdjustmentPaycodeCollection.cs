﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects.Xsd
{
    [Serializable]
    [CollectionDataContract]
    public class WorkLinksYearEndAdjustmentPaycodeCollection : List<WorkLinksYearEndAdjustmentPaycode>
    {
        public string GetValidationInformationFail(Decimal year)
        {
            StringBuilder rtn = new StringBuilder();

            foreach (WorkLinksYearEndAdjustmentPaycode adjustment in this)
            {
                if (adjustment.ErrorMessage(year) != null)
                    rtn.AppendLine(adjustment.ErrorMessage(year) ?? "");
            }

            return rtn.Length > 0 ? rtn.ToString() : null;
        }
    }
}
