﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Xsd
{
    public class WorkLinksPersonAddress : PersonAddress, IImportEmployeeIdentifier
    {
        #region properties
        [DataMember]
        [Required]
        public String ImportExternalIdentifier { get; set; }

        public String EmployeeExternalIdentifier
        {
            get { return ImportExternalIdentifier; }
            set { ImportExternalIdentifier = value; }
        }

        [DataMember]
        public String ProvinceStateCodeExternalIdentifier { get; set; }

        [DataMember]
        public String CountryCodeExternalIdentifier { get; set; }

        [DataMember]
        public String PersonAddressTypeCodeExternalIdentifier { get; set; }

        [DataMember]
        public bool DeleteFlag { get; set; }

        public PersonAddressCollection PersonAddressCollection
        {
            get
            {
                PersonAddressCollection collection = new PersonAddressCollection();
                PersonAddress personAddress = new PersonAddress();
                base.CopyTo(personAddress);
                collection.Add(personAddress);

                return collection;
            }
        }
        #endregion

        public bool MeetsAddressRequirements(String decodeCodeValidationMessage)
        {
            return GetValidationMessage(decodeCodeValidationMessage) == null;
        }
        public String GetValidationMessage(String decodeCodeValidationMessage)
        {
            String validationMessage = null;
            System.Text.StringBuilder output = new System.Text.StringBuilder();

            if (!IsValid)
                output.Append(GetValidationInformation());

            if (PersonAddressTypeCode == "HOME" && DeleteFlag)
                output.AppendLine("WorkLinksPersonAddress:Home address cannot be deleted.");

            if (decodeCodeValidationMessage != null)
                output.AppendLine(decodeCodeValidationMessage);

            if (output.Length > 0)
            {
                //add employee number to message
                validationMessage = String.Format("Employee {0}: ", ImportExternalIdentifier) + Environment.NewLine;
                validationMessage += output.ToString();
            }

            return validationMessage;
        }
    }
}