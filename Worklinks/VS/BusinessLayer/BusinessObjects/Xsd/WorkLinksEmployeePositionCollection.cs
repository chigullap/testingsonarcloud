﻿using System.Collections.Generic;

namespace WorkLinks.BusinessLayer.BusinessObjects.Xsd
{
    public class WorkLinksEmployeePositionCollection : List<WorkLinksEmployeePosition>
    {
        public WorkLinksEmployeePositionCollection UpdateablePositions
        {
            get
            {
                WorkLinksEmployeePositionCollection rtn = new WorkLinksEmployeePositionCollection();
                foreach (WorkLinksEmployeePosition pos in this)
                    if (!pos.SkipUpdate)
                        rtn.Add(pos);
                return rtn;


            }
        }
    }
}