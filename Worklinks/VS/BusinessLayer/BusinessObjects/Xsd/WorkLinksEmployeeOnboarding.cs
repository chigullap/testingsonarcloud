﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Xsd
{
    [Serializable]
    public class WorkLinksEmployeeOnboarding : WorkLinksEmployee, IImportEmployeeIdentifier
    {
        #region fields
        private WorkLinksPersonAddress _personAddress = new WorkLinksPersonAddress();
        private WorkLinksEmployeeBanking _99EmployeeBanking = new WorkLinksEmployeeBanking() { SequenceImportExternalIdentifier = "99", Percentage = 100};
        private WorkLinksEmployeeBanking _1EmployeeBanking = new WorkLinksEmployeeBanking() { SequenceImportExternalIdentifier = "1" };
        private WorkLinksEmployeeBanking _2EmployeeBanking = new WorkLinksEmployeeBanking() { SequenceImportExternalIdentifier = "2"};
        private WorkLinksEmployeeBanking _3EmployeeBanking = new WorkLinksEmployeeBanking() { SequenceImportExternalIdentifier = "3"};
        private WorkLinksEmployeeEmploymentInformation _employment = new WorkLinksEmployeeEmploymentInformation();
        private WorkLinksEmployeePosition _position = new WorkLinksEmployeePosition();
        private WorklinksStatutoryDeduction _statutoryDeduction = new WorklinksStatutoryDeduction();
        #endregion

        #region properties
        public override string ImportExternalIdentifier
        {
            get
            {
                return base.ImportExternalIdentifier;
            }
            set
            {
                base.ImportExternalIdentifier = value;
                _personAddress.EmployeeExternalIdentifier = value;
                _99EmployeeBanking.EmployeeExternalIdentifier = value;
                _1EmployeeBanking.EmployeeExternalIdentifier = value;
                _2EmployeeBanking.EmployeeExternalIdentifier = value;
                _3EmployeeBanking.EmployeeExternalIdentifier = value;
                _employment.EmployeeExternalIdentifier = value;
                _position.EmployeeExternalIdentifier = value;
                _statutoryDeduction.EmployeeExternalIdentifier = value;
            }
        }

        #region employee biographical

        [DataMember]
        [Required]
        public String SocialInsuranceNumber
        {
            get
            {
                return GovernmentIdentificationNumber1;
            }
            set
            {
                GovernmentIdentificationNumber1 = value;
            }
        }
        #endregion

        #region person address

        [DataMember]
        public WorkLinksPersonAddress PersonAddress { get { return _personAddress; } }


        [Required]
        public String AddressLine1
        {
            get { return _personAddress.AddressLine1; }
            set { _personAddress.AddressLine1 = value; }
        }

        public String AddressLine2
        {
            get { return _personAddress.AddressLine2; }
            set { _personAddress.AddressLine2 = value; }
        }

        [Required]
        public String City
        {
            get { return _personAddress.City; }
            set { _personAddress.City = value; }
        }

        [Required]
        public String PostalZipCode
        {
            get { return _personAddress.PostalZipCode; }
            set { _personAddress.PostalZipCode = value; }
        }

        [Required]
        public String ProvinceStateCode
        {
            get { return _personAddress.ProvinceStateCodeExternalIdentifier; }
            set { _personAddress.ProvinceStateCodeExternalIdentifier = value; }
        }

        [Required]
        public String CountryCode
        {
            get { return _personAddress.CountryCodeExternalIdentifier; }
            set { _personAddress.CountryCodeExternalIdentifier = value; }
        }

        [Required]
        public String PersonAddressTypeCode
        {
            get { return _personAddress.PersonAddressTypeCodeExternalIdentifier; }
            set { _personAddress.PersonAddressTypeCodeExternalIdentifier = value; }
        }

        #endregion

        #region banking
        public WorkLinksEmployeeBankingCollection GetWorkLinksEmployeeBanks(ref long startSequence)
        {
            WorkLinksEmployeeBankingCollection accounts = new WorkLinksEmployeeBankingCollection();
            if (_99EmployeeBanking.BankingCodeImportExternalIdentifier == null) _99EmployeeBanking.DeleteFlag = true;
            if (_99EmployeeBanking.EmployeeBankingId < 0) _99EmployeeBanking.EmployeeBankingId = startSequence--;
            accounts.Add(_99EmployeeBanking);
            if (_1EmployeeBanking.BankingCodeImportExternalIdentifier == null) _1EmployeeBanking.DeleteFlag = true;
            {
                if (_1EmployeeBanking.DeleteFlag && !_99EmployeeBanking.DeleteFlag) _1EmployeeBanking.SetDecodeCodeValidationMessage(String.Format("{0} bank account imported without net account", _1EmployeeBanking.SequenceImportExternalIdentifier));
            }
            if (_1EmployeeBanking.EmployeeBankingId < 0) _1EmployeeBanking.EmployeeBankingId = startSequence--;
            accounts.Add(_1EmployeeBanking);

            if (_2EmployeeBanking.BankingCodeImportExternalIdentifier == null) _2EmployeeBanking.DeleteFlag = true;
            if (_2EmployeeBanking.DeleteFlag && !_99EmployeeBanking.DeleteFlag) _2EmployeeBanking.SetDecodeCodeValidationMessage(String.Format("{0} bank account imported without net account", _2EmployeeBanking.SequenceImportExternalIdentifier));
            if (_2EmployeeBanking.EmployeeBankingId < 0) _2EmployeeBanking.EmployeeBankingId = startSequence--;
            accounts.Add(_2EmployeeBanking);

            if (_3EmployeeBanking.BankingCodeImportExternalIdentifier == null) _3EmployeeBanking.DeleteFlag = true;
            if (_3EmployeeBanking.DeleteFlag && !_99EmployeeBanking.DeleteFlag) _3EmployeeBanking.SetDecodeCodeValidationMessage(String.Format("{0} bank account imported without net account", _3EmployeeBanking.SequenceImportExternalIdentifier));
            if (_3EmployeeBanking.EmployeeBankingId < 0) _3EmployeeBanking.EmployeeBankingId = startSequence--;
            accounts.Add(_3EmployeeBanking);

            return accounts;
        }
        public String EmployeeBankCode99
        {
            get { return _99EmployeeBanking.BankingCodeImportExternalIdentifier; }
            set { _99EmployeeBanking.BankingCodeImportExternalIdentifier = value; }
        }
        public String TransitNumber99
        {
            get { return _99EmployeeBanking.Transit_number; }
            set { _99EmployeeBanking.Transit_number = value; }
        }
        public String AccountNumber99
        {
            get { return _99EmployeeBanking.Account_number; }
            set { _99EmployeeBanking.Account_number = value; }
        }
        public String EmployeeBankCode1
        {
            get { return _1EmployeeBanking.BankingCodeImportExternalIdentifier; }
            set { _1EmployeeBanking.BankingCodeImportExternalIdentifier = value; }
        }
        public String TransitNumber1
        {
            get { return _1EmployeeBanking.Transit_number; }
            set { _1EmployeeBanking.Transit_number = value; }
        }
        public String AccountNumber1
        {
            get { return _1EmployeeBanking.Account_number; }
            set { _1EmployeeBanking.Account_number = value; }
        }
        public Decimal? Percentage1
        {
            get { return _1EmployeeBanking.Percentage; }
            set { _1EmployeeBanking.Percentage = value; }
        }
        public Decimal? Amount1
        {
            get { return _1EmployeeBanking.Amount; }
            set { _1EmployeeBanking.Amount = value; }
        }
        public String EmployeeBankCode2
        {
            get { return _2EmployeeBanking.BankingCodeImportExternalIdentifier; }
            set { _2EmployeeBanking.BankingCodeImportExternalIdentifier = value; }
        }
        public String TransitNumber2
        {
            get { return _2EmployeeBanking.Transit_number; }
            set { _2EmployeeBanking.Transit_number = value; }
        }
        public String AccountNumber2
        {
            get { return _2EmployeeBanking.Account_number; }
            set { _2EmployeeBanking.Account_number = value; }
        }
        public Decimal? Percentage2
        {
            get { return _2EmployeeBanking.Percentage; }
            set { _2EmployeeBanking.Percentage = value; }
        }
        public Decimal? Amount2
        {
            get { return _2EmployeeBanking.Amount; }
            set { _2EmployeeBanking.Amount = value; }
        }
        public String EmployeeBankCode3
        {
            get { return _3EmployeeBanking.BankingCodeImportExternalIdentifier; }
            set { _3EmployeeBanking.BankingCodeImportExternalIdentifier = value; }
        }
        public String TransitNumber3
        {
            get { return _3EmployeeBanking.Transit_number; }
            set { _3EmployeeBanking.Transit_number = value; }
        }
        public String AccountNumber3
        {
            get { return _3EmployeeBanking.Account_number; }
            set { _3EmployeeBanking.Account_number = value; }
        }
        public Decimal? Percentage3
        {
            get { return _3EmployeeBanking.Percentage; }
            set { _3EmployeeBanking.Percentage = value; }
        }
        public Decimal? Amount3
        {
            get { return _3EmployeeBanking.Amount; }
            set { _3EmployeeBanking.Amount = value; }
        }
        #endregion

        #region employment
        public WorkLinksEmployeeEmploymentInformation EmployeeEmploymentInformation { get { return _employment; } }

        [Required]
        public String PayrollProcessGroupCode
        {
            get { return _employment.PayrollProcessGroupCodeExternalIdentifier; }
            set { _employment.PayrollProcessGroupCodeExternalIdentifier = value; }
        }

        public DateTime? ProbationDate
        {
            get { return _employment.ProbationDate; }
            set { _employment.ProbationDate = value; }
        }

        public DateTime? SeniorityDate
        {
            get { return _employment.SeniorityDate; }
            set { _employment.SeniorityDate = value; }
        }

        public DateTime? AnniversaryDate
        {
            get { return _employment.AnniversaryDate; }
            set { _employment.AnniversaryDate = value; }
        }

        public DateTime? PensionEffectiveDate
        {
            get { return _employment.PensionEffectiveDate; }
            set { _employment.PensionEffectiveDate = value; }
        }

        [Required]
        public String WsibCode
        {
            get { return _employment.WsibCodeExternalIdentifier; }
            set { _employment.WsibCodeExternalIdentifier = value; }
        }

        public bool ExcludeMassPayslipFileFlag
        {
            get { return _employment.ExcludeMassPayslipFileFlag; }
            set { _employment.ExcludeMassPayslipFileFlag = value; }
        }

        public bool ExcludeMassPayslipPrintFlag
        {
            get { return _employment.ExcludeMassPayslipPrintFlag; }
            set { _employment.ExcludeMassPayslipPrintFlag = value; }
        }
        #endregion


        #region position
        public WorkLinksEmployeePosition EmployeePosition { get { return _position; } }
        [Required]
        public DateTime? EffectiveDate
        {
            get { return _position.EffectiveDate; }
            set { _position.EffectiveDate = value; }
        }

        [Required]
        public String EmployeePositionReasonCode
        {
            get { return _position.EmployeePositionReasonCodeExternalIdentifier; }
            set { _position.EmployeePositionReasonCodeExternalIdentifier = value; }
        }

        public String PermanencyTypeCode
        {
            get { return _position.PermanencyTypeCodeExternalIdentifier; }
            set { _position.PermanencyTypeCodeExternalIdentifier = value; }
        }

        public String EmploymentTypeCode
        {
            get { return _position.EmploymentTypeCodeExternalIdentifier; }
            set { _position.EmploymentTypeCodeExternalIdentifier = value; }
        }

        public String EmployeeTypeCode
        {
            get { return _position.EmployeeTypeCodeExternalIdentifier; }
            set { _position.EmployeeTypeCodeExternalIdentifier = value; }
        }

        [Required]
        public String PaymentMethodCode
        {
            get { return _position.PaymentMethodCodeExternalIdentifier; }
            set { _position.PaymentMethodCodeExternalIdentifier = value; }
        }

        [Required]
        public Decimal StandardHours
        {
            get { return _position.StandardHours; }
            set { _position.StandardHours = value; }
        }

        [Required]
        public Decimal? CompensationAmount
        {
            get { return _position.CompensationAmount; }
            set { _position.CompensationAmount = value; }
        }

        [Required]
        public String CostCenter
        {
            get { return _position.OrganationUnitImportExternalIdentifier1; }
            set { _position.OrganationUnitImportExternalIdentifier1 = value; }
        }
        #endregion


        #region stat deduction
        public WorklinksStatutoryDeduction StatutoryDeduction { get { return _statutoryDeduction; } }

        [Required]
        public String EmployerNumber
        {
            get { return _statutoryDeduction.EmployerExternalIdentifier; }
            set { _statutoryDeduction.EmployerExternalIdentifier = value; }
        }
        [Required]
        public String StatProvinceStateCode
        {
            get { return _statutoryDeduction.ProvinceStateCodeExternalIdentifier; }
            set { _statutoryDeduction.ProvinceStateCodeExternalIdentifier = value; }
        }
        public bool PayEmploymentInsuranceFlag
        {
            get { return _statutoryDeduction.PayEmploymentInsuranceFlag; }
            set { _statutoryDeduction.PayEmploymentInsuranceFlag = value; }
        }
        public bool PayEmploymentInsuranceFlagExistsFlag
        {
            get { return _statutoryDeduction.PayEmploymentInsuranceFlagExistsFlag; }
            set { _statutoryDeduction.PayEmploymentInsuranceFlagExistsFlag = value; }
        }
        public bool PayCanadaPensionPlanFlag
        {
            get { return _statutoryDeduction.PayCanadaPensionPlanFlag; }
            set { _statutoryDeduction.PayCanadaPensionPlanFlag = value; }
        }
        public bool PayCanadaPensionPlanFlagExistsFlag
        {
            get { return _statutoryDeduction.PayCanadaPensionPlanFlagExistsFlag; }
            set { _statutoryDeduction.PayCanadaPensionPlanFlagExistsFlag = value; }
        }
        public bool ParentalInsurancePlanFlag
        {
            get { return _statutoryDeduction.ParentalInsurancePlanFlag; }
            set { _statutoryDeduction.ParentalInsurancePlanFlag = value; }
        }
        public bool ParentalInsurancePlanFlagExistsFlag
        {
            get { return _statutoryDeduction.ParentalInsurancePlanFlagExistsFlag; }
            set { _statutoryDeduction.ParentalInsurancePlanFlagExistsFlag = value; }
        }
        public int? FederalTaxClaim
        {
            get { return _statutoryDeduction.FederalTaxClaim; }
            set { _statutoryDeduction.FederalTaxClaim = value; }
        }
        public bool FederalTaxClaimExistsFlag
        {
            get { return _statutoryDeduction.FederalTaxClaimExistsFlag; }
            set { _statutoryDeduction.FederalTaxClaimExistsFlag = value; }
        }
        public Decimal? FederalAdditionalTax
        {
            get { return _statutoryDeduction.FederalAdditionalTax; }
            set { _statutoryDeduction.FederalAdditionalTax = value; }
        }
        public bool FederalAdditionalTaxExistsFlag
        {
            get { return _statutoryDeduction.FederalAdditionalTaxExistsFlag; }
            set { _statutoryDeduction.FederalAdditionalTaxExistsFlag = value; }
        }
        public int? FederalDesignatedAreaDeduction
        {
            get { return _statutoryDeduction.FederalDesignatedAreaDeduction; }
            set { _statutoryDeduction.FederalDesignatedAreaDeduction = value; }
        }
        public bool FederalDesignatedAreaDeductionExistsFlag
        {
            get { return _statutoryDeduction.FederalDesignatedAreaDeductionExistsFlag; }
            set { _statutoryDeduction.FederalDesignatedAreaDeductionExistsFlag = value; }
        }
        public int? FederalAuthorizedAnnualDeduction
        {
            get { return _statutoryDeduction.FederalAuthorizedAnnualDeduction; }
            set { _statutoryDeduction.FederalAuthorizedAnnualDeduction = value; }
        }
        public bool FederalAuthorizedAnnualDeductionExistsFlag
        {
            get { return _statutoryDeduction.FederalAuthorizedAnnualDeductionExistsFlag; }
            set { _statutoryDeduction.FederalAuthorizedAnnualDeductionExistsFlag = value; }
        }
        public int? FederalAuthorizedAnnualTaxCredit
        {
            get { return _statutoryDeduction.FederalAuthorizedAnnualTaxCredit; }
            set { _statutoryDeduction.FederalAuthorizedAnnualTaxCredit = value; }
        }
        public bool FederalAuthorizedAnnualTaxCreditExistsFlag
        {
            get { return _statutoryDeduction.FederalAuthorizedAnnualTaxCreditExistsFlag; }
            set { _statutoryDeduction.FederalAuthorizedAnnualTaxCreditExistsFlag = value; }
        }
        public int? FederalLabourTaxCredit
        {
            get { return _statutoryDeduction.FederalLabourTaxCredit; }
            set { _statutoryDeduction.FederalLabourTaxCredit = value; }
        }
        public bool FederalLabourTaxCreditExistsFlag
        {
            get { return _statutoryDeduction.FederalLabourTaxCreditExistsFlag; }
            set { _statutoryDeduction.FederalLabourTaxCreditExistsFlag = value; }
        }
        public bool FederalPayTaxFlag
        {
            get { return _statutoryDeduction.FederalPayTaxFlag; }
            set { _statutoryDeduction.FederalPayTaxFlag = value; }
        }
        public bool FederalPayTaxFlagExistsFlag
        {
            get { return _statutoryDeduction.FederalPayTaxFlagExistsFlag; }
            set { _statutoryDeduction.FederalPayTaxFlagExistsFlag = value; }
        }
        public int? ProvincialTaxClaim
        {
            get { return _statutoryDeduction.ProvincialTaxClaim; }
            set { _statutoryDeduction.ProvincialTaxClaim = value; }
        }
        public bool ProvincialTaxClaimExistsFlag
        {
            get { return _statutoryDeduction.ProvincialTaxClaimExistsFlag; }
            set { _statutoryDeduction.ProvincialTaxClaimExistsFlag = value; }
        }
        public Decimal? ProvincialAdditionalTax
        {
            get { return _statutoryDeduction.ProvincialAdditionalTax; }
            set { _statutoryDeduction.ProvincialAdditionalTax = value; }
        }
        public bool ProvincialAdditionalTaxExistsFlag
        {
            get { return _statutoryDeduction.ProvincialAdditionalTaxExistsFlag; }
            set { _statutoryDeduction.ProvincialAdditionalTaxExistsFlag = value; }
        }
        public int? ProvincialDesignatedAreaDeduction
        {
            get { return _statutoryDeduction.ProvincialDesignatedAreaDeduction; }
            set { _statutoryDeduction.ProvincialDesignatedAreaDeduction = value; }
        }
        public bool ProvincialDesignatedAreaDeductionExistsFlag
        {
            get { return _statutoryDeduction.ProvincialDesignatedAreaDeductionExistsFlag; }
            set { _statutoryDeduction.ProvincialDesignatedAreaDeductionExistsFlag = value; }
        }
        public int? ProvincialAuthorizedAnnualDeduction
        {
            get { return _statutoryDeduction.ProvincialAuthorizedAnnualDeduction; }
            set { _statutoryDeduction.ProvincialAuthorizedAnnualDeduction = value; }
        }
        public bool ProvincialAuthorizedAnnualDeductionExistsFlag
        {
            get { return _statutoryDeduction.ProvincialAuthorizedAnnualDeductionExistsFlag; }
            set { _statutoryDeduction.ProvincialAuthorizedAnnualDeductionExistsFlag = value; }
        }
        public int? ProvincialAuthorizedAnnualTaxCredit
        {
            get { return _statutoryDeduction.ProvincialAuthorizedAnnualTaxCredit; }
            set { _statutoryDeduction.ProvincialAuthorizedAnnualTaxCredit = value; }
        }
        public bool ProvincialAuthorizedAnnualTaxCreditExistsFlag
        {
            get { return _statutoryDeduction.ProvincialAuthorizedAnnualTaxCreditExistsFlag; }
            set { _statutoryDeduction.ProvincialAuthorizedAnnualTaxCreditExistsFlag = value; }
        }
        public int? ProvincialLabourTaxCredit
        {
            get { return _statutoryDeduction.ProvincialLabourTaxCredit; }
            set { _statutoryDeduction.ProvincialLabourTaxCredit = value; }
        }
        public bool ProvincialLabourTaxCreditExistsFlag
        {
            get { return _statutoryDeduction.ProvincialLabourTaxCreditExistsFlag; }
            set { _statutoryDeduction.ProvincialLabourTaxCreditExistsFlag = value; }
        }
        public bool ProvincialPayTaxFlag
        {
            get { return _statutoryDeduction.ProvincialPayTaxFlag; }
            set { _statutoryDeduction.ProvincialPayTaxFlag = value; }
        }
        public bool ProvincialPayTaxFlagExistsFlag
        {
            get { return _statutoryDeduction.ProvincialPayTaxFlagExistsFlag; }
            set { _statutoryDeduction.ProvincialPayTaxFlagExistsFlag = value; }
        }
        public int? EstimatedAnnualIncome
        {
            get { return _statutoryDeduction.EstimatedAnnualIncome; }
            set { _statutoryDeduction.EstimatedAnnualIncome = value; }
        }
        public bool EstimatedAnnualIncomeExistsFlag
        {
            get { return _statutoryDeduction.EstimatedAnnualIncomeExistsFlag; }
            set { _statutoryDeduction.EstimatedAnnualIncomeExistsFlag = value; }
        }
        public int? EstimatedAnnualExpense
        {
            get { return _statutoryDeduction.EstimatedAnnualExpense; }
            set { _statutoryDeduction.EstimatedAnnualExpense = value; }
        }
        public bool EstimatedAnnualExpenseExistsFlag
        {
            get { return _statutoryDeduction.EstimatedAnnualExpenseExistsFlag; }
            set { _statutoryDeduction.EstimatedAnnualExpenseExistsFlag = value; }
        }
        public int? EstimatedNetIncome
        {
            get { return _statutoryDeduction.EstimatedNetIncome; }
            set { _statutoryDeduction.EstimatedNetIncome = value; }
        }
        public bool EstimatedNetIncomeExistsFlag
        {
            get { return _statutoryDeduction.EstimatedNetIncomeExistsFlag; }
            set { _statutoryDeduction.EstimatedNetIncomeExistsFlag = value; }
        }
        public Decimal? CommissionPercentage
        {
            get { return _statutoryDeduction.CommissionPercentage; }
            set { _statutoryDeduction.CommissionPercentage = value; }
        }
        public bool CommissionPercentageExistsFlag
        {
            get { return _statutoryDeduction.CommissionPercentageExistsFlag; }
            set { _statutoryDeduction.CommissionPercentageExistsFlag = value; }
        }


        #endregion

        #endregion

        #region clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}