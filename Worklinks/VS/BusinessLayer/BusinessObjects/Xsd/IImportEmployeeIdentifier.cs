﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects.Xsd
{
    public interface IImportEmployeeIdentifier
    {
        String ImportExternalIdentifier { get; set; }
    }
}
