﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Xsd
{
    [Serializable]
    [CollectionDataContract]
    public class WorkLinksEmployeeOnboardingCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<WorkLinksEmployeeOnboarding>
    {
    }
}