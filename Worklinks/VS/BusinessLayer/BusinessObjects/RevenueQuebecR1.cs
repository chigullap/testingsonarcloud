﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class RevenueQuebecR1 : YearEndR1
    {
        public Employee Employee { get; set; }
        public PersonAddress Address { get; set; }
        public PersonContactChannel Phone { get; set; }
        public BusinessNumber BusinessNumber { get; set; }
    }
}
