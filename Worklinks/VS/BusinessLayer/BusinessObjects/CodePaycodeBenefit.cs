﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    [XmlRoot("CodePaycode")]
    public class CodePaycodeBenefit : CodePaycode
    {
        #region properties
        [DataMember]
        public long PaycodeBenefitId { get; set; }

        [DataMember]
        public String PaycodeRateBasedOnCode { get; set; }

        [DataMember]
        public bool IncomeTaxFlag { get; set; }

        [DataMember]
        public bool CanadaQuebecPensionPlanFlag { get; set; }

        [DataMember]
        public bool EmploymentInsuranceFlag { get; set; }

        [DataMember]
        public bool ProvincialParentalInsurancePlanFlag { get; set; }

        [DataMember]
        public bool QuebecTaxFlag { get; set; }

        [DataMember]
        public bool ProvincialHealthTaxFlag { get; set; }

        [DataMember]
        public bool WorkersCompensationBoardFlag { get; set; }

        [DataMember]
        public String FederalTaxPaycodeCode { get; set; }

        [DataMember]
        public String QuebecTaxPaycodeCode { get; set; }

        [DataMember]
        public String OffsetGeneralLedgerMask { get; set; }

        [DataMember]
        public bool IncludeAsIncomeFlag { get; set; }

        [DataMember]
        public new byte[] RowVersion { get; set; }
        #endregion

        #region construct/destruct
        public CodePaycodeBenefit()
        {
            Clear();
            this.PaycodeTypeCode = "2";
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(CodePaycodeBenefit data)
        {
            base.CopyTo(data);

            data.PaycodeBenefitId = PaycodeBenefitId;
            data.PaycodeRateBasedOnCode = PaycodeRateBasedOnCode;
            data.IncomeTaxFlag = IncomeTaxFlag;
            data.CanadaQuebecPensionPlanFlag = CanadaQuebecPensionPlanFlag;
            data.EmploymentInsuranceFlag = EmploymentInsuranceFlag;
            data.ProvincialParentalInsurancePlanFlag = ProvincialParentalInsurancePlanFlag;
            data.QuebecTaxFlag = QuebecTaxFlag;
            data.ProvincialHealthTaxFlag = ProvincialHealthTaxFlag;
            data.WorkersCompensationBoardFlag = WorkersCompensationBoardFlag;
            data.FederalTaxPaycodeCode = FederalTaxPaycodeCode;
            data.QuebecTaxPaycodeCode = QuebecTaxPaycodeCode;
            data.OffsetGeneralLedgerMask = OffsetGeneralLedgerMask;
            data.IncludeAsIncomeFlag = IncludeAsIncomeFlag;
            data.RowVersion = RowVersion;
        }
        public new void Clear()
        {
            base.Clear();

            PaycodeBenefitId = -1;
            PaycodeRateBasedOnCode = null;
            IncomeTaxFlag = false;
            CanadaQuebecPensionPlanFlag = false;
            EmploymentInsuranceFlag = false;
            ProvincialParentalInsurancePlanFlag = false;
            QuebecTaxFlag = false;
            ProvincialHealthTaxFlag = false;
            WorkersCompensationBoardFlag = false;
            FederalTaxPaycodeCode = null;
            QuebecTaxPaycodeCode = null;
            OffsetGeneralLedgerMask = null;
            IncludeAsIncomeFlag = false;
            RowVersion = new byte[8];
        }
        #endregion
    }
}