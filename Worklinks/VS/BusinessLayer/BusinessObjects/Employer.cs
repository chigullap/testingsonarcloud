﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class Employer: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployerId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String Name { get; set; }
        [DataMember]
        public Address Address {get;set;}
        [DataMember]
        public String ProprietorSocialInsuranceNumber1 { get; set; }
        [DataMember]
        public String ProprietorSocialInsuranceNumber2 { get; set; }
        [DataMember]
        public String RevenueQuebecTaxId { get; set; }

        #endregion

        #region construct/destruct
        public Employer()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone

        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(Employer data)
        {
            base.CopyTo(data);
            data.EmployerId = EmployerId;
            data.Name = Name;
            if (Address == null)
                Address = null;
            else
                Address.CopyTo(data.Address);
            data.ProprietorSocialInsuranceNumber1 = ProprietorSocialInsuranceNumber1;
            data.ProprietorSocialInsuranceNumber2 = ProprietorSocialInsuranceNumber2;
            data.RevenueQuebecTaxId = RevenueQuebecTaxId;
        }
        public new void Clear()
        {
            base.Clear();
            EmployerId = -1;
            Name = null;
            Address = null;
            ProprietorSocialInsuranceNumber1 = null;
            ProprietorSocialInsuranceNumber2 = null;
            RevenueQuebecTaxId = null;
        }

        #endregion

    }
}
