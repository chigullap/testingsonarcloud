﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public class SalaryPlanGradeAccumulation : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        public long SalaryPlanGradeAccumulationId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        public long PayrollProcessId { get; set; }
        public long EmployeeId { get; set; }
        public long SalaryPlanGradeId { get; set; }
        public decimal Hours { get; set; }
        public decimal Amount { get; set; }
        public long? CreateEmployeePositionId { get; set; }
        public string KeyMask { get { return String.Format("{0}-{1}", EmployeeId, SalaryPlanGradeId); } }
        #endregion

        #region construct/destruct
        public SalaryPlanGradeAccumulation()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(SalaryPlanGradeAccumulation data)
        {
            base.CopyTo(data);

            data.SalaryPlanGradeAccumulationId = SalaryPlanGradeAccumulationId;
            data.PayrollProcessId = PayrollProcessId;
            data.EmployeeId = EmployeeId;
            data.SalaryPlanGradeId = SalaryPlanGradeId;
            data.Hours = Hours;
            data.Amount = Amount;
            data.CreateEmployeePositionId = CreateEmployeePositionId;
        }
        public new void Clear()
        {
            base.Clear();

            SalaryPlanGradeAccumulationId = -1;
            PayrollProcessId = -1;
            EmployeeId = -1;
            SalaryPlanGradeId = -1;
            Hours = -1;
            Amount = -1;
            CreateEmployeePositionId = null;
        }
        #endregion
    }
}