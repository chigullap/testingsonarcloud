﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeEmploymentEquity : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeEmploymentEquityId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public String EmploymentEquityCode { get; set; }
        #endregion

        #region construct/destruct
        public EmployeeEmploymentEquity()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            EmployeeEmploymentEquity item = new EmployeeEmploymentEquity();
            this.CopyTo(item);

            return item;
        }
        public virtual void CopyTo(EmployeeEmploymentEquity data)
        {
            base.CopyTo(data);

            data.EmployeeEmploymentEquityId = EmployeeEmploymentEquityId;
            data.EmployeeId = EmployeeId;
            data.EmploymentEquityCode = EmploymentEquityCode;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeeEmploymentEquityId = -1;
            EmployeeId = -1;
            EmploymentEquityCode = null;
        }
        #endregion
    }
}