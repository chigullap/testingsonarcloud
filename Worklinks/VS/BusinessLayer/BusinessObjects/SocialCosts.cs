﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SocialCosts : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long DummyKey
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public string EmployeeNumber { get; set; }

        [DataMember]
        public string BenefitName { get; set; }

        [DataMember]
        public decimal BenefitValue { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime CutoffDate { get; set; }

        #endregion

        #region construct/destruct
        public SocialCosts()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(SocialCosts data)
        {
            base.CopyTo(data);

            data.DummyKey = DummyKey;
            data.EmployeeNumber = EmployeeNumber;
            data.BenefitName = BenefitName;
            data.BenefitValue = BenefitValue;
            data.StartDate = StartDate;
            data.CutoffDate = CutoffDate;
        }

        public new void Clear()
        {
            base.Clear();

            DummyKey = -1;
            EmployeeNumber = null;
            BenefitName = null;
            BenefitValue = 0;
            StartDate = DateTime.Now;
            CutoffDate = DateTime.Now;
        }
        #endregion
    }
}
