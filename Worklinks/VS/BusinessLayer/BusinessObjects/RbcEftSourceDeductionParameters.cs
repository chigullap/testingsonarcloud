﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class RbcEftSourceDeductionParameters //this class is used by the CRA garnishment and the CRA source deductions.  Most fields are shared but not all
    {
        #region properties
        [DataMember]
        public String IsaSenderInterchangeIDSourceDed { get; set; }
        [DataMember]
        public String IsaReceiverInterchangeIDSourceDed { get; set; }
        [DataMember]
        public String IsaTestIndicatorSourceDed { get; set; }
        [DataMember]
        public String DirectDepositBankCodeTransitNumber { get; set; }
        [DataMember]
        public String DirectDepositAccountNumber { get; set; }
        [DataMember]
        public String CraBankCodeTransitNumberSourceDed { get; set; }
        [DataMember]
        public String CraAccountNumberSourceDed { get; set; }
        [DataMember]
        public String CompanyShortName { get; set; }
        [DataMember]
        public String GsSenderInterchangeIdSourceDed { get; set; }
        [DataMember]
        public String GsApplicationReceiversCodeSourceDed { get; set; }
        [DataMember]
        public String RefItReferenceNumberSourceDed { get; set; }
        [DataMember]
        public String N1PrNameSourceDed { get; set; }
        [DataMember]
        public String GarnishmentEftType { get; set; } //only used with garnishments

        #endregion

        #region construct/destruct
        public RbcEftSourceDeductionParameters()
        {
            Clear();
        }
        #endregion

        #region clear       
        public void Clear()
        {
            IsaSenderInterchangeIDSourceDed = null;
            IsaReceiverInterchangeIDSourceDed = null;
            IsaTestIndicatorSourceDed = null;
            DirectDepositBankCodeTransitNumber = null;
            DirectDepositAccountNumber = null;
            CraBankCodeTransitNumberSourceDed = null;
            CraAccountNumberSourceDed = null;
            CompanyShortName = null;
            GsSenderInterchangeIdSourceDed = null;
            GsApplicationReceiversCodeSourceDed = null;
            RefItReferenceNumberSourceDed = null;
            N1PrNameSourceDed = null;
            GarnishmentEftType = null;
        }

        #endregion
    }
}
