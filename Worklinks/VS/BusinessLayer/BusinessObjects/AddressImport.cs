﻿using System;
using FileHelpers;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    [DelimitedRecord(",")]
    public class AddressImport
    {
        [DataMember]
        public String EmployeeIdentifier;

        [DataMember]
        public String AddressLine1;

        [DataMember]
        public String AddressLine2;

        [DataMember]
        public String City;

        [DataMember]
        public String PostalZipCode;

        [DataMember]
        public String CodeProvinceStateCd;

        [DataMember]
        public String CodeCountryCd;
    }
}