﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class BenefitPlanDetail : BusinessObject
    {
        #region properties
        [DataMember]
        public long BenefitPlanDetailId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long BenefitPlanId { get; set; }

        [DataMember]
        public DateTime? EffectiveDate { get; set; }

        [DataMember]
        public String BenefitPlanTypeCode { get; set; }

        [DataMember]
        public String BenefitPlanStatusCode { get; set; }

        [DataMember]
        public long? VendorId { get; set; }

        [DataMember]
        public String EmployeePaycodeCode { get; set; }

        [DataMember]
        public String EmployerPaycodeCode { get; set; }
        #endregion

        #region construct/destruct
        public BenefitPlanDetail()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(BenefitPlanDetail data)
        {
            base.CopyTo(data);

            data.BenefitPlanDetailId = BenefitPlanDetailId;
            data.BenefitPlanId = BenefitPlanId;
            data.EffectiveDate = EffectiveDate;
            data.BenefitPlanTypeCode = BenefitPlanTypeCode;
            data.BenefitPlanStatusCode = BenefitPlanStatusCode;
            data.VendorId = VendorId;
            data.EmployeePaycodeCode = EmployeePaycodeCode;
            data.EmployerPaycodeCode = EmployerPaycodeCode;
        }

        public new void Clear()
        {
            base.Clear();

            BenefitPlanDetailId = -1;
            BenefitPlanId = -1;
            EffectiveDate = null;
            BenefitPlanTypeCode = null;
            BenefitPlanStatusCode = null;
            VendorId = null;
            EmployeePaycodeCode = null;
            EmployerPaycodeCode = null;
        }
        #endregion
    }
}
