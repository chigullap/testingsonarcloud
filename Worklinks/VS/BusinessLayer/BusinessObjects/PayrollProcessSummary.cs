﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollProcessSummary: PayrollProcess, ICloneable
    {
        
        #region properties
        [DataMember]
        public String PaymentFrequencyCode { get; set; }
        [DataMember]
        public int? ExportSequenceNumber { get; set; }

        #endregion

        #region construct/destruct
        public PayrollProcessSummary()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();
            PaymentFrequencyCode = null;
            ExportSequenceNumber = null;
        }

        public void CopyTo(PayrollProcessSummary data)
        {
            base.CopyTo(data);
            data.PaymentFrequencyCode = PaymentFrequencyCode;
            data.ExportSequenceNumber = ExportSequenceNumber;
        }

        #endregion

    }
}
