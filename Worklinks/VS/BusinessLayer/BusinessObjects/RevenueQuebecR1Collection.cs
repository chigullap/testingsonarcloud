﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class RevenueQuebecR1Collection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<RevenueQuebecR1>
    {
        public RevenueQuebecR1Collection()
        {
        }
        public RevenueQuebecR1Collection(YearEndR1Collection yearEndR1Collection)
        {
            foreach (YearEndR1 r1 in yearEndR1Collection)
            {
                RevenueQuebecR1 rqr1 = new RevenueQuebecR1();
                r1.CopyTo(rqr1);
                this.Add(rqr1);
            }
        }
    }
}
