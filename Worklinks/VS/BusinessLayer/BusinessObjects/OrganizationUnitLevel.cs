﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class OrganizationUnitLevel : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region fields
        //these ints represent the index for the English and French descriptions in the "Description" object, which is a "OrganizationUnitLevelDescriptionCollection"
        private static String _englishCd = "EN";
        private static String _frenchCd = "FR";
        private OrganizationUnitLevelDescriptionCollection _descriptions;
        #endregion

        #region properties
        [DataMember]
        public long OrganizationUnitLevelId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long? ParentOrganizationUnitLevelId { get; set; }

        [DataMember]
        public String LanguageCode { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public bool UsedOnPayslipFlag { get; set; }

        [DataMember]
        public bool UsedOnSecondaryEmployeePositionFlag { get; set; }

        [DataMember]
        public bool OrgOverrideFlag { get; set; }

        [DataMember]
        public bool EmployeesExistFlag { get; set; }

        [DataMember]
        public int OrgUnitsCount { get; set; }

        [DataMember]
        public OrganizationUnitLevelDescriptionCollection Descriptions
        {
            get
            {
                if (_descriptions == null)
                    _descriptions = new OrganizationUnitLevelDescriptionCollection();

                return _descriptions;
            }
            set
            {
                _descriptions = value;
            }
        }

        [DataMember]
        public String EnglishDescription
        {
            get
            {
                return Descriptions[_englishCd] == null ? null : Descriptions[_englishCd].LevelDescription;
            }
            set
            {
                if (EnglishDescription == null)
                    Descriptions.Add(new OrganizationUnitLevelDescription() { LanguageCode = _englishCd, OrganizationUnitLevelId = OrganizationUnitLevelId, CreateUser = UpdateUser, UpdateUser = UpdateUser, LevelDescription = value });
                else
                {
                    Descriptions[_englishCd].UpdateUser = UpdateUser;
                    Descriptions[_englishCd].LevelDescription = value;
                }
            }
        }

        [DataMember]
        public String FrenchDescription
        {
            get
            {
                return Descriptions[_frenchCd] == null ? null : Descriptions[_frenchCd].LevelDescription;
            }
            set
            {
                if (FrenchDescription == null)
                    Descriptions.Add(new OrganizationUnitLevelDescription() { LanguageCode = _frenchCd, OrganizationUnitLevelId = OrganizationUnitLevelId, CreateUser = UpdateUser, UpdateUser = UpdateUser, LevelDescription = value });
                else
                {
                    Descriptions[_frenchCd].UpdateUser = UpdateUser;
                    Descriptions[_frenchCd].LevelDescription = value;
                }
            }
        }
        #endregion

        #region construct/destruct
        public OrganizationUnitLevel()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            OrganizationUnitLevel organizationUnitLevel = new OrganizationUnitLevel();
            this.CopyTo(organizationUnitLevel);
            return organizationUnitLevel;
        }
        public void CopyTo(OrganizationUnitLevel data)
        {
            base.CopyTo(data);

            data.OrganizationUnitLevelId = OrganizationUnitLevelId;
            data.ParentOrganizationUnitLevelId = ParentOrganizationUnitLevelId;
            data.LanguageCode = LanguageCode;
            data.Description = Description;
            data.UsedOnPayslipFlag = UsedOnPayslipFlag;
            data.UsedOnSecondaryEmployeePositionFlag = UsedOnSecondaryEmployeePositionFlag;
            data.OrgOverrideFlag = OrgOverrideFlag;
            data.EmployeesExistFlag = EmployeesExistFlag;
            data.OrgUnitsCount = OrgUnitsCount;

            if (Descriptions != null)
            {
                data.Descriptions = new OrganizationUnitLevelDescriptionCollection();
                Descriptions.CopyTo(data.Descriptions);
            }
        }
        public new void Clear()
        {
            base.Clear();

            OrganizationUnitLevelId = -1;
            ParentOrganizationUnitLevelId = -1;
            LanguageCode = null;
            Description = null;
            Descriptions = null;
            UsedOnPayslipFlag = false;
            UsedOnSecondaryEmployeePositionFlag = true;
            OrgOverrideFlag = false;
            EmployeesExistFlag = false;
            OrgUnitsCount = 0;
        }
        #endregion
    }
}