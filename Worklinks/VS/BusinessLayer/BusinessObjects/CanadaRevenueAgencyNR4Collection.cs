﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class CanadaRevenueAgencyNR4Collection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<CanadaRevenueAgencyNR4>
    {
        public CanadaRevenueAgencyNR4Collection()
        {
        }
        public CanadaRevenueAgencyNR4Collection(YearEndNR4rspCollecion yearEndNR4Collection)
        {
            foreach (YearEndNR4rsp nr4 in yearEndNR4Collection)
            {
                CanadaRevenueAgencyNR4 crNr4 = new CanadaRevenueAgencyNR4();
                nr4.CopyTo(crNr4);
                this.Add(crNr4);
            }
        }
    }
}
