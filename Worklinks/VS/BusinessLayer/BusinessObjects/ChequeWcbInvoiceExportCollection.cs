﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class ChequeWcbInvoiceExportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<ChequeWcbInvoiceExport>
    {
    }
}
