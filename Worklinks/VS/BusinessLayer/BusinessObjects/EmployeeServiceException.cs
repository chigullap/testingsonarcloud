﻿using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [DataContract]
    public class EmployeeServiceException
    {
        public enum ExceptionCodes
        {
            CodeMismatch,
            DataConcurrency,
            ForeignKeyConstraint,
            UniqueIndexFault,
            Other,
        }

        public EmployeeServiceException(ExceptionCodes code)
        {
            ExceptionCode = (ExceptionCodes)code;
        }

        #region fields
        private ExceptionCodes _exceptionCode = 0;
        #endregion

        #region properties
        [DataMember]
        public ExceptionCodes ExceptionCode
        {
            get { return _exceptionCode; }
            set { _exceptionCode = value; }
        }
        #endregion
    }
}