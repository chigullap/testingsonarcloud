﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CraWcbExport : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long CraWcbExportId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long? RbcSequenceNumber { get; set; }
        [DataMember]
        public byte[] Data { get; set; }
        [DataMember]
        public string CodeCraRemittancePeriodCd { get; set; }

        [DataMember]
        public DateTime? CraFileTransmissionDate { get; set; }

        #endregion

        #region construct/destruct
        public CraWcbExport()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();

            CraWcbExportId = -1;
            RbcSequenceNumber = -1;
            Data = null;
            CodeCraRemittancePeriodCd = null;
            CraFileTransmissionDate = null;
        }

        public virtual void CopyTo(CraWcbExport data)
        {
            base.CopyTo(data);

            data.CraWcbExportId = CraWcbExportId;
            data.RbcSequenceNumber = RbcSequenceNumber;
            Data.CopyTo(data.Data, 0);
            data.CodeCraRemittancePeriodCd = CodeCraRemittancePeriodCd;
            data.CraFileTransmissionDate = CraFileTransmissionDate;
        }
        #endregion
    }
}
