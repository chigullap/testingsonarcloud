﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class UserSummary : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PersonId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String LastName { get; set; }

        [DataMember]
        public String FirstName { get; set; }

        [DataMember]
        public String UserName { get; set; }

        [DataMember]
        public DateTime? LastLoginDatetime { get; set; }

        [DataMember]
        public bool IsLockedOutFlag { get; set; }

        [DataMember]
        public DateTime? LastLockoutDatetime { get; set; }

        [DataMember]
        public DateTime? LastPasswordChangedDatetime { get; set; }

        [DataMember]
        public DateTime? PasswordExpiryDatetime { get; set; }

        [DataMember]
        public DateTime? LastLogoutDatetime { get; set; }

        [DataMember]
        public DateTime? LoginExpiryDatetime { get; set; }

        [DataMember]
        public bool WorklinksAdministrationFlag { get; set; }
        #endregion

        #region construct/destruct
        public UserSummary()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();
            PersonId = -1;
            LastName = null;
            FirstName = null;
            UserName = null;
            LastLoginDatetime = null;
            IsLockedOutFlag = false;
            LastLockoutDatetime = null;
            LastPasswordChangedDatetime = null;
            PasswordExpiryDatetime = null;
            LastLogoutDatetime = null;
            LoginExpiryDatetime = null;
            WorklinksAdministrationFlag = false;
        }
        #endregion
    }
}