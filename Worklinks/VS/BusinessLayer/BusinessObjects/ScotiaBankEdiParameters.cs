﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ScotiaBankEdiParameters
    {
        #region properties

        [DataMember]
        public string ScotiaBankIsa06 { get; set; }
        [DataMember]
        public string ScotiaBankIsa08 { get; set; }
        [DataMember]
        public string ScotiaBankIsa15 { get; set; }
        [DataMember]
        public string ScotiaBankBpr07 { get; set; }
        [DataMember]
        public string ScotiaBankBpr09 { get; set; }
        [DataMember]
        public string CompanyShortName { get; set; }
        [DataMember]
        public bool PreAuthorizedDebitFlag { get; set; }
        [DataMember]
        public string PreAuthorizedDebitFileType { get; set; }
        [DataMember]
        public string EftType { get; set; }
        [DataMember]
        public string ScotiaFtpName { get; set; }

        #endregion

        #region construct/destruct
        public ScotiaBankEdiParameters()
        {
            Clear();
        }
        #endregion

        #region clear       
        public void Clear()
        {
            ScotiaBankIsa06 = null;
            ScotiaBankIsa08 = null;
            ScotiaBankIsa15 = null;
            ScotiaBankBpr07 = null;
            ScotiaBankBpr09 = null;            
            CompanyShortName = null;
            PreAuthorizedDebitFlag = false;
            PreAuthorizedDebitFileType = null;
            ScotiaFtpName = null;
        }

        #endregion
    }
}
