﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class AutoPayrollScheduleCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<AutoPayrollSchedule>
    {
    }
}