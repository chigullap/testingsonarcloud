﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PayrollMasterPaymentErrorCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollMasterPaymentError>
    {
    }
}
