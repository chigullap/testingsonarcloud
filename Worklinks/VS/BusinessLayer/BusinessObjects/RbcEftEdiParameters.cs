﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class RbcEftEdiParameters
    {
        #region properties
        [DataMember]
        public String IsaSenderInterchangeID { get; set; }
        [DataMember]
        public String IsaReceiverInterchangeID { get; set; }
        [DataMember]
        public String IsaTestIndicator { get; set; }
        [DataMember]
        public String DirectDepositBankCodeTransitNumber { get; set; }
        [DataMember]
        public String DirectDepositAccountNumber { get; set; }
        [DataMember]
        public String DirectDepositGsan { get; set; }
        [DataMember]
        public String CompanyName { get; set; }
        [DataMember]
        public String CompanyShortName { get; set; }
        [DataMember]
        public String ChequesChequeIssuance { get; set; }
        [DataMember]
        public String EftType { get; set; }
        [DataMember]
        public String CustomLine1 { get; set; }
        [DataMember]
        public String CustomLine2 { get; set; }
        [DataMember]
        public String CustomLine3 { get; set; }
        [DataMember]
        public string DescriptionPayeeMatch { get; set; }
        [DataMember]
        public string PreAuthorizedDebitClientNumber { get; set; }
        [DataMember]
        public bool PreAuthorizedDebitFlag { get; set; }
        [DataMember]
        public string PreAuthorizedDebitFileType { get; set; }
        [DataMember]
        public string UsTrn { get; set; }
        [DataMember]
        public string RbcFtpName { get; set; }
        


        #endregion

        #region construct/destruct
        public RbcEftEdiParameters()
        {
            Clear();
        }
        #endregion

        #region clear       
        public void Clear()
        {
            IsaSenderInterchangeID = null;
            IsaReceiverInterchangeID = null;
            IsaTestIndicator = null;
            DirectDepositBankCodeTransitNumber = null;
            DirectDepositAccountNumber = null;
            DirectDepositGsan = null;
            CompanyName = null;
            CompanyShortName = null;
            ChequesChequeIssuance = null;
            EftType = null;
            CustomLine1 = null;
            CustomLine2 = null;
            CustomLine3 = null;
            DescriptionPayeeMatch = null;
            PreAuthorizedDebitClientNumber = null;
            PreAuthorizedDebitFlag = false;
            PreAuthorizedDebitFileType = null;
            UsTrn = null;
            RbcFtpName = null;
        }

        #endregion
    }
}
