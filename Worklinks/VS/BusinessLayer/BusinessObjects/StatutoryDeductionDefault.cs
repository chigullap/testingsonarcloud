﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class StatutoryDeductionDefault : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long StatutoryDeductionDefaultId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public DateTime? EffectiveDate { get; set; }

        [DataMember]
        public String ProvinceStateCode { get; set; }

        [DataMember]
        public long? BusinessNumberId { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public bool PayEmploymentInsuranceFlag { get; set; }

        [DataMember]
        public bool PayCanadaPensionPlanFlag { get; set; }

        [DataMember]
        public bool ParentalInsurancePlanFlag { get; set; }

        [DataMember]
        public int? FederalTaxClaim { get; set; }

        [DataMember]
        public Decimal? FederalAdditionalTax { get; set; }

        [DataMember]
        public int? FederalDesignatedAreaDeduction { get; set; }

        [DataMember]
        public int? FederalAuthorizedAnnualDeduction { get; set; }

        [DataMember]
        public int? FederalAuthorizedAnnualTaxCredit { get; set; }

        [DataMember]
        public int? FederalLabourTaxCredit { get; set; }

        [DataMember]
        public bool FederalPayTaxFlag { get; set; }

        [DataMember]
        public int? ProvincialTaxClaim { get; set; }

        [DataMember]
        public Decimal? ProvincialAdditionalTax { get; set; }

        [DataMember]
        public int? ProvincialDesignatedAreaDeduction { get; set; }

        [DataMember]
        public int? ProvincialAuthorizedAnnualDeduction { get; set; }

        [DataMember]
        public int? ProvincialAuthorizedAnnualTaxCredit { get; set; }

        [DataMember]
        public int? ProvincialLabourTaxCredit { get; set; }

        [DataMember]
        public bool ProvincialPayTaxFlag { get; set; }

        [DataMember]
        public int? EstimatedAnnualIncome { get; set; }

        [DataMember]
        public int? EstimatedAnnualExpense { get; set; }

        [DataMember]
        public int? EstimatedNetIncome { get; set; }

        [DataMember]
        public Decimal? CommissionPercentage { get; set; }
        #endregion

        #region construct/destruct
        public StatutoryDeductionDefault()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(StatutoryDeductionDefault data)
        {
            base.CopyTo(data);
            data.StatutoryDeductionDefaultId = StatutoryDeductionDefaultId;
            data.EffectiveDate = EffectiveDate;
            data.ProvinceStateCode = ProvinceStateCode;
            data.BusinessNumberId = BusinessNumberId;
            data.ActiveFlag = ActiveFlag;
            data.PayEmploymentInsuranceFlag = PayEmploymentInsuranceFlag;
            data.PayCanadaPensionPlanFlag = PayCanadaPensionPlanFlag;
            data.ParentalInsurancePlanFlag = ParentalInsurancePlanFlag;
            data.FederalTaxClaim = FederalTaxClaim;
            data.FederalAdditionalTax = FederalAdditionalTax;
            data.FederalDesignatedAreaDeduction = FederalDesignatedAreaDeduction;
            data.FederalAuthorizedAnnualDeduction = FederalAuthorizedAnnualDeduction;
            data.FederalAuthorizedAnnualTaxCredit = FederalAuthorizedAnnualTaxCredit;
            data.FederalLabourTaxCredit = FederalLabourTaxCredit;
            data.FederalPayTaxFlag = FederalPayTaxFlag;
            data.ProvincialTaxClaim = ProvincialTaxClaim;
            data.ProvincialAdditionalTax = ProvincialAdditionalTax;
            data.ProvincialDesignatedAreaDeduction = ProvincialDesignatedAreaDeduction;
            data.ProvincialAuthorizedAnnualDeduction = ProvincialAuthorizedAnnualDeduction;
            data.ProvincialAuthorizedAnnualTaxCredit = ProvincialAuthorizedAnnualTaxCredit;
            data.ProvincialLabourTaxCredit = ProvincialLabourTaxCredit;
            data.ProvincialPayTaxFlag = ProvincialPayTaxFlag;
            data.EstimatedAnnualIncome = EstimatedAnnualIncome;
            data.EstimatedAnnualExpense = EstimatedAnnualExpense;
            data.EstimatedNetIncome = EstimatedNetIncome;
            data.CommissionPercentage = CommissionPercentage;
        }
        public new void Clear()
        {
            base.Clear();
            StatutoryDeductionDefaultId = -1;
            EffectiveDate = null;
            ProvinceStateCode = null;
            BusinessNumberId = null;
            ActiveFlag = true;
            PayEmploymentInsuranceFlag = false;
            PayCanadaPensionPlanFlag = false;
            ParentalInsurancePlanFlag = false;
            FederalTaxClaim = null;
            FederalAdditionalTax = null;
            FederalDesignatedAreaDeduction = null;
            FederalAuthorizedAnnualDeduction = null;
            FederalAuthorizedAnnualTaxCredit = null;
            FederalLabourTaxCredit = null;
            FederalPayTaxFlag = false;
            ProvincialTaxClaim = null;
            ProvincialAdditionalTax = null;
            ProvincialDesignatedAreaDeduction = null;
            ProvincialAuthorizedAnnualDeduction = null;
            ProvincialAuthorizedAnnualTaxCredit = null;
            ProvincialLabourTaxCredit = null;
            ProvincialPayTaxFlag = false;
            EstimatedAnnualIncome = null;
            EstimatedAnnualExpense = null;
            EstimatedNetIncome = null;
            CommissionPercentage = null;
        }
        #endregion
    }
}