﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class Grievance: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {

        #region properties
        [DataMember]
        public long GrievanceId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String GrievanceTypeCode { get; set; }
        [DataMember]
        public String GrievanceFiledByCode { get; set; }
        [DataMember]
        public String Reference { get; set; }
        [DataMember]
        public long? ReferenceEmployeeId { get; set; }
        [DataMember]
        public long? ReferenceLabourUnionId { get; set; }
        [DataMember]
        public DateTime GrievanceDate { get; set; }
        [DataMember]
        public String Note { get; set; }
        [DataMember]
        public String ContactReference { get; set; }
        [DataMember]
        public String GrievanceStatusCode { get; set; }
        [DataMember]
        public DateTime? LastStatusDatetime { get; set; }
        [DataMember]
        public String GrievanceResolutionCode { get; set; }
        [DataMember]
        public DateTime? ResolutionDate { get; set; }
        [DataMember]
        public String ResolutionNote { get; set; }
        [DataMember]
        public GrievanceActionCollection Actions { get; set; }


        #endregion

        #region construct/destruct
        public Grievance()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(Grievance data)
        {
            base.CopyTo(data);
            data.GrievanceId=GrievanceId;
            data.GrievanceTypeCode=GrievanceTypeCode;
            data.GrievanceFiledByCode=GrievanceFiledByCode;
            data.Reference=Reference;
            data.ReferenceEmployeeId=ReferenceEmployeeId;
            data.ReferenceLabourUnionId = ReferenceLabourUnionId;
            data.GrievanceDate=GrievanceDate;
            data.Note=Note;
            data.ContactReference=ContactReference;
            data.GrievanceStatusCode=GrievanceStatusCode;
            data.LastStatusDatetime=LastStatusDatetime;
            data.GrievanceResolutionCode=GrievanceResolutionCode;
            data.ResolutionDate=ResolutionDate;
            data.ResolutionNote = ResolutionNote;
            if (Actions != null)
            {
                data.Actions = new GrievanceActionCollection();
                Actions.CopyTo(data.Actions);
            }

        }
        public new void Clear()
        {
            base.Clear();
            GrievanceId=-1;
            GrievanceTypeCode=null;
            GrievanceFiledByCode=null;
            Reference=null;
            ReferenceEmployeeId=null;
            ReferenceLabourUnionId = null;
            GrievanceDate=DateTime.Now;
            Note=null;
            ContactReference=null;
            GrievanceStatusCode=null;
            LastStatusDatetime=null;
            GrievanceResolutionCode=null;
            ResolutionDate=null;
            ResolutionNote = null;
            Actions = null;
        }

        #endregion
    }
}
