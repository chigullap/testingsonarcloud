﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeVacationHourlyRate : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties

        [DataMember]
        public long EmployeeVacationHourlyRateId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public DateTime AccrualCalculationPeriodStartDate { get; set; }
        [DataMember]
        public DateTime AccrualCalculationPeriodEndDate { get; set; }
        [DataMember]
        public long EmployeeId { get; set; }
        [DataMember]
        public decimal Rate { get; set; }

        #endregion

        #region construct/destruct

        public EmployeeVacationHourlyRate()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone

        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public new void Clear()
        {
            base.Clear();
            EmployeeVacationHourlyRateId = -1;
            AccrualCalculationPeriodStartDate = DateTime.Now;
            AccrualCalculationPeriodEndDate = DateTime.Now;
            EmployeeId = -1;
            Rate = 0;
        }

        #endregion
    }
}
