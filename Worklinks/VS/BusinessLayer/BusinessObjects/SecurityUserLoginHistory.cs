﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SecurityUserLoginHistory: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region fields
        #endregion
        
        #region properties
        [DataMember]
        public long SecurityUserLoginHistoryid
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String UserName {get; set;}
        [DataMember]
        public DateTime? LoginAttemptDatetime {get; set;}
        [DataMember]
        public String Browser {get; set;}
        [DataMember]
        public String BrowserVersion {get; set;}
        [DataMember]
        public String IpAddress {get; set;}
        [DataMember]
        public bool AuthenticatedFlag {get; set;}
        [DataMember]
        public String Platform {get; set;}
        [DataMember]
        public String Version { get; set; }
        [DataMember]
        public String Resolution { get; set; }

        #endregion

        #region construct/destruct
        public SecurityUserLoginHistory()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(SecurityUserLoginHistory data)
        {
            base.CopyTo(data);
            data.SecurityUserLoginHistoryid = SecurityUserLoginHistoryid;
            data.UserName = UserName;
            data.LoginAttemptDatetime = LoginAttemptDatetime;
            data.Browser = Browser;
            data.BrowserVersion = BrowserVersion;
            data.IpAddress = IpAddress;
            data.AuthenticatedFlag = AuthenticatedFlag;
            data.Platform = Platform;
            data.Version = Version;
            data.Resolution = Resolution;
        }

        public new void Clear()
        {
            base.Clear();
            SecurityUserLoginHistoryid = -1;
            UserName = null;
            LoginAttemptDatetime = null;
            Browser = null;
            BrowserVersion = null;
            IpAddress = null;
            AuthenticatedFlag = false;
            Platform = null;
            Version = null;
            Resolution = null;
        }

        #endregion


    }
}
