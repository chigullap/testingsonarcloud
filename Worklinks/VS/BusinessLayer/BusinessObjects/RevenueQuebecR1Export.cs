﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class RevenueQuebecR1Export : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long RevenueQuebecR1ExportId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long RevenueQuebecTransmitterRecordId { get; set; }

        [DataMember]
        public RevenueQuebecTransmitterRecord RevenueQuebecTransmitterRecord { get; set; } 

        [DataMember]
        public long YearEndId { get; set; }

        [DataMember]
        public YearEnd YearEnd { get; set; }

        [DataMember]
        public String RevenueQuebecTypeOfPackageCode { get; set; }

        [DataMember]
        public String YearEndFormCode { get; set; }

        [DataMember]
        public RevenueQuebecR1Collection R1s { get; set; }
        [DataMember]
        public RevenueQuebecR2Collection R2s { get; set; }

        [DataMember]
        public Employer Employer { get; set; }
        #endregion

        #region construct/destruct
        public RevenueQuebecR1Export()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(RevenueQuebecR1Export data)
        {
            base.CopyTo(data);

            data.RevenueQuebecR1ExportId = RevenueQuebecR1ExportId;
            data.RevenueQuebecTransmitterRecordId = RevenueQuebecTransmitterRecordId;

            if (RevenueQuebecTransmitterRecord == null)
                data.RevenueQuebecTransmitterRecord = null;
            else
                RevenueQuebecTransmitterRecord.CopyTo(data.RevenueQuebecTransmitterRecord);

            data.YearEndId = YearEndId;

            if (YearEnd == null)
                data.YearEnd = null;
            else
                YearEnd.CopyTo(data.YearEnd);

            data.RevenueQuebecTypeOfPackageCode = RevenueQuebecTypeOfPackageCode;
            data.YearEndFormCode = YearEndFormCode;

            if (R1s == null)
                data.R1s = null;
            else
                R1s.CopyTo(data.R1s);

            if (R2s == null)
                data.R2s = null;
            else
                R2s.CopyTo(data.R2s);

            if (Employer == null)
                data.Employer = null;
            else
                Employer.CopyTo(data.Employer);
        }
        public new void Clear()
        {
            base.Clear();

            RevenueQuebecR1ExportId = -1;
            RevenueQuebecTransmitterRecordId = -1;
            RevenueQuebecTransmitterRecord = null;
            RevenueQuebecTypeOfPackageCode = null;
            YearEndFormCode = null;
            YearEndId = -1;
            R1s = null;
            R2s = null;
            Employer = null;
        }
        #endregion
    }
}