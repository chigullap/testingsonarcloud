﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class ChequeHealthTaxExportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<ChequeHealthTaxExport>
    {
    }
}

