﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PaycodePayrollTransactionOffsetAssociation : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PayrollTransactionOffsetAssociationId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String PaycodeCode { get; set; }

        [DataMember]
        public String OffsetCodePaycodeCode { get; set; }

        [DataMember]
        public Decimal OffsetMultiplier { get; set; }

        [DataMember]
        public bool UseBaseSalaryRateFlag { get; set; }

        [DataMember]
        public String PaycodeTypeCode { get; set; }

        [DataMember]
        public String PaycodeIncomePayTypeCode { get; set; }
        #endregion



        #region construct/destruct
        public PaycodePayrollTransactionOffsetAssociation()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            PaycodePayrollTransactionOffsetAssociation item = new PaycodePayrollTransactionOffsetAssociation();
            this.CopyTo(item);

            return item;
        }
        public virtual void CopyTo(PaycodePayrollTransactionOffsetAssociation data)
        {
            base.CopyTo(data);

            data.PayrollTransactionOffsetAssociationId = PayrollTransactionOffsetAssociationId;
            data.PaycodeCode = PaycodeCode;
            data.OffsetCodePaycodeCode = OffsetCodePaycodeCode;
            data.OffsetMultiplier = OffsetMultiplier;
            data.UseBaseSalaryRateFlag = UseBaseSalaryRateFlag;
            data.PaycodeTypeCode = PaycodeTypeCode;
            data.PaycodeIncomePayTypeCode = PaycodeIncomePayTypeCode;
        }
        public new void Clear()
        {
            base.Clear();

            PayrollTransactionOffsetAssociationId = -1;
            PaycodeCode = null;
            OffsetCodePaycodeCode = null;
            OffsetMultiplier = 0;
            UseBaseSalaryRateFlag = false;
            PaycodeTypeCode = null;
            PaycodeIncomePayTypeCode = null;
        }


        #endregion
    }
}