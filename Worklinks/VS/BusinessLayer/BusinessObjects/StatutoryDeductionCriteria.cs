﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class StatutoryDeductionCriteria
    {
        #region properties
        public String Year { get; set; }

        public String ProvinceStateCode { get; set; }

        public String Description { get; set; }
        #endregion

        public StatutoryDeductionCriteria()
        {
            Clear();
        }

        public void Clear()
        {
            Year = null;
            ProvinceStateCode = null;
            Description = null;
        }
    }
}