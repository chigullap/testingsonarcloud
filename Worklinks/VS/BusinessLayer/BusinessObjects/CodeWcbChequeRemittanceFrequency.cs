﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CodeWcbChequeRemittanceFrequency : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public String WcbChequeRemittanceFrequencyCode
        {
            get { return (String)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String EnglishDescription { get; set; }

        [DataMember]
        public String FrenchDescription { get; set; }

        [DataMember]
        public String ImportExternalIdentifier { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public bool HasChildrenFlag { get; set; }

        [DataMember]
        public string ScheduleType { get; set; }
        #endregion

        #region construct/destruct
        public CodeWcbChequeRemittanceFrequency()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(CodeWcbChequeRemittanceFrequency data)
        {
            base.CopyTo(data);

            data.WcbChequeRemittanceFrequencyCode = WcbChequeRemittanceFrequencyCode;
            data.EnglishDescription = EnglishDescription;
            data.FrenchDescription = FrenchDescription;
            data.ImportExternalIdentifier = ImportExternalIdentifier;
            data.ActiveFlag = ActiveFlag;
            data.HasChildrenFlag = HasChildrenFlag;
        }
        public new void Clear()
        {
            base.Clear();

            WcbChequeRemittanceFrequencyCode = "";
            EnglishDescription = null;
            FrenchDescription = null;
            ImportExternalIdentifier = null;
            ActiveFlag = false;
            HasChildrenFlag = false;
        }
        #endregion
    }
}