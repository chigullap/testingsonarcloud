﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class YearEndT4Collection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<YearEndT4>
    {
    }
}