﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Runtime.Serialization;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;
using WorkLinks.BusinessLayer.BusinessObjects.Import;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PayrollTransactionImportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollTransactionImport>
    {
        public PayrollTransactionImportCollection(WorkLinksPayrollBatch xsdPayrollTransactionImport)
        {
            int i=0;
            foreach (Xsd.PayrollTransaction xsdTranscation in xsdPayrollTransactionImport.Body.PayrollBatch.PayrollTransaction)
            {
                PayrollTransactionImport transaction = new PayrollTransactionImport(xsdTranscation);
                transaction.PayrollTransactionId = --i;
                this.Add(transaction);
            }
        }

        public PayrollTransactionImportCollection(AdvantageTimeRecord advantageTimeRecord, Dictionary<String, CodePaycode> paycodesDictionary)
        {
            int i = 0;

            foreach (AdvantageTimeRecordDetail advantageTimeRecordDetail in advantageTimeRecord.AdvantageTimeRecordDetails)
            {
                for (int j = 0; j <= 50; j++)
                {
                    string paycodeExternalName = "Timecode_" + j.ToString("00");

                    if (paycodesDictionary.ContainsKey(paycodeExternalName))
                    {
                        PayrollTransactionImport transaction = new PayrollTransactionImport(advantageTimeRecordDetail, paycodeExternalName);
                        transaction.PayrollTransactionId = --i;
                        this.Add(transaction);
                    }
                }
            }
        }

        public PayrollTransactionSummaryCollection GetValidPayrollTransactionSummaryCollection(String payrollProcessGroupCode)
        {
                PayrollTransactionSummaryCollection collection = new PayrollTransactionSummaryCollection();
                foreach (PayrollTransactionImport import in this)
                {
                    if (import.PayrollProcessGroupCode==payrollProcessGroupCode)
                        collection.Add(import);
                }
                return collection;
        }

        public bool HasErrors
        {
            get
            {
                foreach (PayrollTransactionImport import in this)
                {
                    if (import.HasErrors)
                        return true;
                }
                return false;
            }
        }

        public bool HasAllErrors
        {
            get
            {
                foreach (PayrollTransactionImport import in this)
                {
                    if (!import.HasErrors)
                        return false;
                }
                return true;
            }
        }

        public List<String> GetAllPayrollProcessGroupCodes()
        {
            List<String> rtn = new List<string>();
            foreach (PayrollTransactionImport import in this)
            {
                if (import.PayrollProcessGroupCode != null && !rtn.Contains(import.PayrollProcessGroupCode))
                    rtn.Add(import.PayrollProcessGroupCode);
            }

            return rtn;
        }
    }

   

}
