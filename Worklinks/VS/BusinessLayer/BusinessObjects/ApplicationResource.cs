﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ApplicationResource: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        
        #region properties
        [DataMember]
        public new String Key 
        {
            get { return (String)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String Value{get;set;}

        #endregion

        #region construct/destruct
        public ApplicationResource()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();
            Key = null;
            Value = null;
        }

        public void CopyTo(ApplicationResource data)
        {
            base.CopyTo(data);
            data.Key = Key;
            data.Value = this.Value;
        }

        #endregion
    }
}
