﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PayrollTransactionSummaryCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollTransactionSummary>
    {
        public void DefaultUnsetTransactionDates(DateTime cutoffDate)
        {
            foreach (PayrollTransactionSummary transaction in this)
            {
                if (transaction.TransactionDate == DateTime.MinValue)
                {
                    transaction.TransactionDate = cutoffDate;
                }
            }
        }


        #region employee specific methods
        public PayrollTransactionSummaryCollection GetEmployeeTransactions(long employeeId)
        {
            PayrollTransactionSummaryCollection rtn = new PayrollTransactionSummaryCollection();

            foreach (PayrollTransactionSummary item in this)
            {
                if (item.EmployeeId.Equals(employeeId))
                    rtn.Add(item);
            }

            return rtn;
        }
        public Decimal EmployeeTotalFederalTaxAmount(String employeeNumber)
        {
            Decimal rtn = 0;

            foreach (PayrollTransactionSummary item in this)
            {
                if (item.EmployeeNumber == employeeNumber)
                    rtn += item.FederalTaxAmount ?? 0;
            }

            return rtn;
        }

        public Decimal EmployeeTotalProvincialTaxAmount(String employeeNumber)
        {
            Decimal rtn = 0;

            foreach (PayrollTransactionSummary item in this)
            {
                if (item.EmployeeNumber == employeeNumber)
                    rtn += item.ProvincialTaxAmount ?? 0;
            }

            return rtn;
        }
        #endregion

        #region collection property
        public Decimal TotalFederalTaxAmount
        {
            get
            {
                Decimal rtn = 0;
                foreach (PayrollTransactionSummary item in this)
                {
                    rtn += item.FederalTaxAmount ?? 0;
                }

                return rtn;
            }
        }

        public Decimal TotalProvincialTaxAmount
        {
            get
            {
                Decimal rtn = 0;
                foreach (PayrollTransactionSummary item in this)
                {
                    rtn += item.ProvincialTaxAmount ?? 0;
                }

                return rtn;
            }
        }
        public PayrollTransactionCollection PayrollTransactionCollection
        {
            get
            {
                PayrollTransactionCollection items = new PayrollTransactionCollection();

                foreach (PayrollTransaction trans in this)
                    items.Add(trans);

                return items;
            }
        }
        #endregion
    }
}