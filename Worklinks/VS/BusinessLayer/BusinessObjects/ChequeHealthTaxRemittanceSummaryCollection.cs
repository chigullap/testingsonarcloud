﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class ChequeHealthTaxRemittanceSummaryCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<ChequeHealthTaxRemittanceSummary>
    {
    }
}
