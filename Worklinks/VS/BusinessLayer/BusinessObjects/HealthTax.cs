﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class HealthTax : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long HealthTaxId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public string CodeHealthTaxCd { get; set; }

        [DataMember]
        public string EnglishDescription { get; set; }

        [DataMember]
        public string FrenchDescription { get; set; }

        [DataMember]
        public string ProvinceStateCode { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public string HealthRemittanceAccountNumber { get; set; }

        [DataMember]
        public bool GenerateChequeForRemittanceFlag { get; set; }

        [DataMember]
        public long? VendorId { get; set; }

        [DataMember]
        public string ImportExternalIdentifier { get; set; }

        #endregion

        #region construct/destruct
        public HealthTax()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(HealthTax data)
        {
            base.CopyTo(data);

            data.HealthTaxId = HealthTaxId;
            data.CodeHealthTaxCd = CodeHealthTaxCd;
            data.EnglishDescription = EnglishDescription;
            data.FrenchDescription = FrenchDescription;
            data.ProvinceStateCode = ProvinceStateCode;
            data.ActiveFlag = ActiveFlag;
            data.HealthRemittanceAccountNumber = HealthRemittanceAccountNumber;
            data.GenerateChequeForRemittanceFlag = GenerateChequeForRemittanceFlag;
            data.VendorId = VendorId;
            data.ImportExternalIdentifier = ImportExternalIdentifier;
        }
        public new void Clear()
        {
            base.Clear();

            HealthTaxId = -1;
            CodeHealthTaxCd = null;
            EnglishDescription = null;
            FrenchDescription = null;
            ProvinceStateCode = null;
            ActiveFlag = true;
            HealthRemittanceAccountNumber = null;
            GenerateChequeForRemittanceFlag = false;
            VendorId = null;
            ImportExternalIdentifier = null;
        }
        #endregion
    }
}