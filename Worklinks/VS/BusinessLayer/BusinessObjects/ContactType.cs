﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{

    [Serializable]
    [DataContract]
    public class ContactType : Contact
    {
        public override event KeyChangedEventHandler KeyChanged;

        #region fields
        private long _contactTypeId = -1;
        #endregion

        
        #region properties
        public override string Key
        {
            get
            {
                return ContactTypeId.ToString();
            }
        }
        [DataMember]
        public long ContactTypeId 
        {
            get { return _contactTypeId; }
            set
            {
                KeyChangedEventArgs args = new KeyChangedEventArgs();
                args.OldKey = _contactTypeId.ToString();
                _contactTypeId = value;
                args.NewKey = Key;
                if (!(args.OldKey ?? String.Empty).Equals(args.NewKey ?? String.Empty) && this.GetType().Equals(typeof(ContactType)))
                    OnKeyChanged(args);
            }
        }
        [DataMember]
        public short SortOrder {get;set;}
        [DataMember]
        public String ContactTypeCode {get;set;}

        #endregion

        protected override void OnKeyChanged(KeyChangedEventArgs args)
        {
            if (KeyChanged != null)
            {
                KeyChanged(this, args);
            }
        }


        #region construct/destruct
        public ContactType()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone

        public new void Clear()
        {
            base.Clear();
            ContactTypeId = -1;
            SortOrder = 0;
            ContactTypeCode =null;
        }

        #endregion


    }
}
