﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class CodeWcbChequeRemittanceFrequencyCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<CodeWcbChequeRemittanceFrequency>
    {
    }
}