﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PayrollMasterTaxCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollMasterTax>
    {
    }
}