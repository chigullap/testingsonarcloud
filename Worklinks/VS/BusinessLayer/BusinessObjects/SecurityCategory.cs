﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SecurityCategory: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        
        #region properties
        [DataMember]
        public long SecurityCategoryId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public bool SelectedFlag { get; set; }
        [DataMember]
        public int HierarchicalSortOrder { get; set; }
        [DataMember]
        public String ReferenceTableName { get; set; }
        [DataMember]
        public String ParentIdentifier { get; set; }
        [DataMember]
        public String SecurityCategoryDescription { get; set; }

        #endregion

        #region construct/destruct
        public SecurityCategory()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(SecurityCategory data)
        {
            base.CopyTo(data);

            data.SecurityCategoryId = SecurityCategoryId;
            data.SelectedFlag = SelectedFlag;
            data.HierarchicalSortOrder = HierarchicalSortOrder;
            data.ParentIdentifier = ParentIdentifier;
            data.SecurityCategoryDescription = SecurityCategoryDescription;
        }
        public new void Clear()
        {
            base.Clear();

            SecurityCategoryId = -1;
            SelectedFlag = false;
            HierarchicalSortOrder = 0;
            ParentIdentifier = null;
            SecurityCategoryDescription = null;
        }

        #endregion

    }
}
