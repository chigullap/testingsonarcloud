﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class AutoPayrollSchedule : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long AutoPayrollScheduleId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String CodePayrollProcessGroupCd { get; set; }
        [DataMember]
        public DateTime DateToExecute { get; set; }
        [DataMember]
        public DateTime ChequeDate { get; set; }
        [DataMember]
        public bool ProcessedFlag { get; set; }
        [DataMember]
        public long? PayrollProcessId { get; set; }
        #endregion

        #region construct/destruct
        public AutoPayrollSchedule()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(AutoPayrollSchedule data)
        {
            base.CopyTo(data);

            data.AutoPayrollScheduleId = AutoPayrollScheduleId;
            data.CodePayrollProcessGroupCd = CodePayrollProcessGroupCd;
            data.DateToExecute = DateToExecute;
            data.ChequeDate = ChequeDate;
            data.ProcessedFlag = ProcessedFlag;
            data.PayrollProcessId = PayrollProcessId;
        }
        public new void Clear()
        {
            base.Clear();

            AutoPayrollScheduleId = -1;
            CodePayrollProcessGroupCd = null;
            DateToExecute = DateTime.MinValue;
            ChequeDate = DateTime.MinValue;
            ProcessedFlag = false;
            PayrollProcessId = null;
        }
        #endregion
    }
}