﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PayrollMasterPaycodeCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollMasterPaycode>
    {
    }
}