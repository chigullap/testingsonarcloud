﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollProcessingGroup : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public String PayrollProcessGroupCode
        {
            get { return (String)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String PayrollProcessGroupCountryCode { get; set; }

        [DataMember]
        public Int64 SortOrder { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public bool SystemFlag { get; set; }

        [DataMember]
        public String PaymentFrequencyCode { get; set; }

        [DataMember]
        public String PayrollProcessGroupEnglishDesc { get; set; }

        [DataMember]
        public String PayrollProcessGroupFrenchDesc { get; set; }

        [DataMember]
        public long PayrollProcessGroupCodeEnglishDescId { get; set; }

        [DataMember]
        public long PayrollProcessGroupCodeFrenchDescId { get; set; }

        [DataMember]
        public byte[] EngDescRowVersion { get; set; }

        [DataMember]
        public byte[] FrDescRowVersion { get; set; }

        [DataMember]
        public String EnglishLanguageCode { get; set; }

        [DataMember]
        public String FrenchLanguageCode { get; set; }

        [DataMember]
        public Decimal PeriodYear { get; set; }

        [DataMember]
        public Decimal Period { get; set; }

        [DataMember]
        public DateTime PeriodStartDate { get; set; }

        [DataMember]
        public DateTime PeriodCutoffDate { get; set; }

        [DataMember]
        public bool ManualChequeFlag { get; set; }

        #endregion

        #region construct/destruct
        public PayrollProcessingGroup()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PayrollProcessingGroup data)
        {
            base.CopyTo(data);
            data.PayrollProcessGroupCode = PayrollProcessGroupCode;
            data.PayrollProcessGroupCountryCode = PayrollProcessGroupCountryCode;
            data.SortOrder = SortOrder;
            data.StartDate = StartDate;
            data.EndDate = EndDate;
            data.ActiveFlag = ActiveFlag;
            data.SystemFlag = SystemFlag;
            data.PaymentFrequencyCode = PaymentFrequencyCode;
            data.PayrollProcessGroupEnglishDesc = PayrollProcessGroupEnglishDesc;
            data.PayrollProcessGroupCodeEnglishDescId = PayrollProcessGroupCodeEnglishDescId;
            data.PayrollProcessGroupCodeFrenchDescId = PayrollProcessGroupCodeFrenchDescId;
            data.EngDescRowVersion = EngDescRowVersion;
            data.FrDescRowVersion = FrDescRowVersion;
            data.EnglishLanguageCode = EnglishLanguageCode;
            data.FrenchLanguageCode = FrenchLanguageCode;
            data.PayrollProcessGroupFrenchDesc = PayrollProcessGroupFrenchDesc;
            data.PeriodYear = PeriodYear;
            data.Period = Period;
            data.PeriodStartDate = PeriodStartDate;
            data.PeriodCutoffDate = PeriodCutoffDate;
            data.ManualChequeFlag = ManualChequeFlag;
        }
        public new void Clear()
        {
            base.Clear();
            PayrollProcessGroupCode = "";
            PayrollProcessGroupCountryCode = "";
            SortOrder = 0;
            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
            ActiveFlag = true;
            SystemFlag = false;
            PaymentFrequencyCode = "";
            PayrollProcessGroupEnglishDesc = "";
            PayrollProcessGroupFrenchDesc = "";
            PayrollProcessGroupCodeEnglishDescId = -1;
            PayrollProcessGroupCodeFrenchDescId = -1;
            EngDescRowVersion = new byte[8];
            FrDescRowVersion = new byte[8];
            EnglishLanguageCode = "EN";
            FrenchLanguageCode = "FR";
            PeriodYear = 0;
            Period = 0;
            PeriodStartDate = DateTime.MinValue;
            PeriodCutoffDate = DateTime.MinValue;
            ManualChequeFlag = false;
        }
        #endregion
    }
}