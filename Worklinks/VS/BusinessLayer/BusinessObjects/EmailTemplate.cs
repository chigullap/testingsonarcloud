﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmailTemplate : Email, ICloneable
    {
        #region properties
        [DataMember]
        public long EmailTemplateId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        //primary
        [DataMember]
        public String PrimaryLanguageCode { get; set; }

        [DataMember]
        public String PrimarySubject { get; set; }

        [DataMember]
        public String PrimaryBodyHeader { get; set; }

        [DataMember]
        public String PrimaryBodyDetail { get; set; }

        [DataMember]
        public String PrimaryBodyFooter { get; set; }

        //secondary
        [DataMember]
        public String SecondaryLanguageCode { get; set; }

        [DataMember]
        public String SecondarySubject { get; set; }

        [DataMember]
        public String SecondaryBodyHeader { get; set; }

        [DataMember]
        public String SecondaryBodyDetail { get; set; }

        [DataMember]
        public String SecondaryBodyFooter { get; set; }
        #endregion

        #region construct/destruct
        public EmailTemplate()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public virtual void CopyTo(EmailTemplate data)
        {
            base.CopyTo(data);

            data.EmailTemplateId = EmailTemplateId;

            //primary
            data.PrimaryLanguageCode = PrimaryLanguageCode;
            data.PrimarySubject = PrimarySubject;
            data.PrimaryBodyHeader = PrimaryBodyHeader;
            data.PrimaryBodyDetail = PrimaryBodyDetail;
            data.PrimaryBodyFooter = PrimaryBodyFooter;

            //secondary
            data.SecondaryLanguageCode = SecondaryLanguageCode;
            data.SecondarySubject = SecondarySubject;
            data.SecondaryBodyHeader = SecondaryBodyHeader;
            data.SecondaryBodyDetail = SecondaryBodyDetail;
            data.SecondaryBodyFooter = SecondaryBodyFooter;
        }
        public new void Clear()
        {
            base.Clear();

            EmailTemplateId = -1;

            //primary
            PrimaryLanguageCode = "EN";
            PrimarySubject = null;
            PrimaryBodyHeader = null;
            PrimaryBodyDetail = null;
            PrimaryBodyFooter = null;

            //secondary
            SecondaryLanguageCode = "FR";
            SecondarySubject = null;
            SecondaryBodyHeader = null;
            SecondaryBodyDetail = null;
            SecondaryBodyFooter = null;
        }
        #endregion
    }
}