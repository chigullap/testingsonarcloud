﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeTerminationOther : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeTerminationOtherId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long EmployeePositionId { get; set; }
        [DataMember]
        public bool RecommendForRehire { get; set; }
        [DataMember]
        public bool? VoluntaryTermination { get; set; }
        [DataMember]
        public bool? TerminationLetterReceived { get; set; }
        [DataMember]
        public short? WeeksOfTerminationPay { get; set; }
        [DataMember]
        public DateTime? SalaryContinuanceFrom { get; set; }
        [DataMember]
        public short? WeeksOfSeverancePay { get; set; }
        [DataMember]
        public DateTime? SalaryContinuanceTo { get; set; }
        [DataMember]
        public decimal? VacationPay { get; set; }
        [DataMember]
        public short? VacationDays { get; set; }
        [DataMember]
        public decimal? AnticipatedCommission { get; set; }
        [DataMember]
        public decimal? VacationPercent { get; set; }
        [DataMember]
        public decimal? AnticipatedBonus { get; set; }
        [DataMember]
        public bool? MedicalFlag { get; set; }
        [DataMember]
        public bool? DentalFlag { get; set; }
        [DataMember]
        public bool? LifeFlag { get; set; }
        [DataMember]
        public bool? OtherFlag { get; set; }
        [DataMember]
        public String OtherFlagText { get; set; }
        [DataMember]
        public String Comments { get; set; }
       
        #endregion

        #region construct/destruct
        public EmployeeTerminationOther()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeeTerminationOther data)
        {
            base.CopyTo(data);
            data.EmployeeTerminationOtherId = EmployeeTerminationOtherId;
            data.EmployeePositionId = EmployeePositionId;
            data.RecommendForRehire = RecommendForRehire;
            data.VoluntaryTermination = VoluntaryTermination;
            data.TerminationLetterReceived = TerminationLetterReceived;
            data.WeeksOfTerminationPay = WeeksOfTerminationPay;
            data.SalaryContinuanceFrom = SalaryContinuanceFrom;
            data.WeeksOfSeverancePay = WeeksOfSeverancePay;
            data.SalaryContinuanceTo = SalaryContinuanceTo;
            data.VacationPay = VacationPay;
            data.VacationDays = VacationDays;
            data.AnticipatedCommission = AnticipatedCommission;
            data.VacationPercent = VacationPercent;
            data.AnticipatedBonus = AnticipatedBonus;
            data.MedicalFlag = MedicalFlag;
            data.DentalFlag = DentalFlag;
            data.LifeFlag = LifeFlag;
            data.OtherFlag = OtherFlag;
            data.OtherFlagText = OtherFlagText;
            data.Comments = Comments;
        }
        public new void Clear()
        {
            base.Clear();
            EmployeeTerminationOtherId = -1;
            EmployeePositionId = -1;
            RecommendForRehire = true;
            VoluntaryTermination = null;
            TerminationLetterReceived = null;
            WeeksOfTerminationPay = null;
            SalaryContinuanceFrom = null;
            WeeksOfSeverancePay = null;
            SalaryContinuanceTo = null;
            VacationPay = null;
            VacationDays = null;
            AnticipatedCommission = null;
            VacationPercent = null;
            AnticipatedBonus = null;
            MedicalFlag = null;
            DentalFlag = null;
            LifeFlag = null;
            OtherFlag = null;
            OtherFlagText = null;
            Comments = null;
        }

        #endregion
    }
}