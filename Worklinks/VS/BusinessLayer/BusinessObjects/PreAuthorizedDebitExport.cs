﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PreAuthorizedDebitExport : BusinessObject
    {
        #region properties
        [DataMember]
        public long PreAuthorizedDebitExportId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long PayrollProcessId { get; set; }
        [DataMember]
        public long? SequenceNumber { get; set; }
        [DataMember]
        public byte[] Data { get; set; }
        [DataMember]
        public DateTime? ChequeFileTransmissionDate { get; set; }
        [DataMember]
        public decimal TotalAmount { get; set; }
        [DataMember]
        public string PadFileType { get; set; }

        #endregion

        #region construct/destruct
        public PreAuthorizedDebitExport()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();

            PreAuthorizedDebitExportId = -1;
            PayrollProcessId = -1;
            SequenceNumber = null;
            Data = null;
            ChequeFileTransmissionDate = null;
            TotalAmount = 0;
            PadFileType = null;
        }

        public virtual void CopyTo(PreAuthorizedDebitExport data)
        {
            base.CopyTo(data);

            data.PreAuthorizedDebitExportId = PreAuthorizedDebitExportId;
            data.PayrollProcessId = PayrollProcessId;
            data.SequenceNumber = SequenceNumber;
            Data.CopyTo(data.Data, 0);
            data.ChequeFileTransmissionDate = ChequeFileTransmissionDate;
            data.TotalAmount = TotalAmount;
            data.PadFileType = PadFileType;
        }
        #endregion
    }
}
