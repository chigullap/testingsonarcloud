﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class YearEndT4a : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long YearEndT4aId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public decimal Year { get; set; }
        [DataMember]
        public long EmployeeId { get; set; }
        [DataMember]
        public string EmployerNumber { get; set; }
        [DataMember]
        public string EmployeeNumber { get; set; }
        [DataMember]
        public decimal PensionSuperannuation { get; set; }
        [DataMember]
        public decimal LumpSumPay18 { get; set; }
        [DataMember]
        public decimal SelfEmployedCommission { get; set; }
        [DataMember]
        public decimal IncomeTaxDeduction22 { get; set; }
        [DataMember]
        public decimal Annuities { get; set; }
        [DataMember]
        public decimal EligibleRetiringAllow { get; set; }
        [DataMember]
        public decimal NonEligibleRetiringAllow { get; set; }
        [DataMember]
        public decimal OtherIncome { get; set; }
        [DataMember]
        public decimal PatronageAllocation { get; set; }
        [DataMember]
        public decimal RegisteredPensionPlanContributions { get; set; }
        [DataMember]
        public decimal PensionAdjustment { get; set; }
        [DataMember]
        public string RegistrationNumber { get; set; }
        [DataMember]
        public short FootNotesCode { get; set; }
        [DataMember]
        public decimal RespIncome { get; set; }
        [DataMember]
        public decimal RespAssistance { get; set; }
        [DataMember]
        public decimal CharitableDonations46 { get; set; }
        [DataMember]
        public string Footnotes { get; set; }
        [DataMember]
        public decimal ProvincialTax { get; set; }
        [DataMember]
        public short? RecordStatus { get; set; }
        [DataMember]
        public byte? GenerateXml { get; set; }
        [DataMember]
        public byte? XmlCreated { get; set; }
        [DataMember]
        public decimal FeesForServices { get; set; }
        [DataMember]
        public decimal UnregisteredPlanAmount109 { get; set; }
        [DataMember]
        public decimal MedicalPremiumBenefitsAmount118 { get; set; }
        [DataMember]
        public decimal GroupInsurancePlanAmount119 { get; set; }
        [DataMember]
        public decimal LumpSumPaymentsFromUnregisteredPlan102 { get; set; }
        [DataMember]
        public decimal LumpSumPaymentsFromUnregisteredPlan108 { get; set; }
        [DataMember]
        public decimal LumpSumPaymentsFromUnregisteredPlan110 { get; set; }
        [DataMember]
        public decimal LumpSumPaymentsFromUnregisteredPlan158 { get; set; }
        [DataMember]
        public decimal LumpSumPaymentsFromUnregisteredPlan180 { get; set; }
        [DataMember]
        public decimal LumpSumPaymentsFromUnregisteredPlan190 { get; set; }
        [DataMember]
        public decimal TaxDeferredCooperativeShare129 { get; set; }
        [DataMember]
        public decimal VariablePensionBenefits133 { get; set; }
        [DataMember]
        public decimal RecipientPaidPreminumsPrivateHealthServices135 { get; set; }
        [DataMember]
        public decimal TuitionAssistanceAdultBasicEducation196 { get; set; }
        [DataMember]
        public decimal ResearchGrants104 { get; set; }
        [DataMember]
        public decimal PaymentsWageLossReplacementPlan107 { get; set; }
        [DataMember]
        public decimal VeteransBenefits127 { get; set; }
        [DataMember]
        public decimal WageEarnerProtectionProgram132 { get; set; }
        [DataMember]
        public decimal SubpQualitifed152 { get; set; }
        [DataMember]
        public decimal BankruptcySettlement156 { get; set; }
        [DataMember]
        public decimal RprrPayments194 { get; set; }
        [DataMember]
        public decimal RegisteredDisabilitySavingsPlan131 { get; set; }
        [DataMember]
        public decimal ScholarshipsPrizes105 { get; set; }
        [DataMember]
        public decimal DeathBenefits106 { get; set; }
        [DataMember]
        public decimal LoanBenefits117 { get; set; }
        [DataMember]
        public decimal RevokedDpspPayments123 { get; set; }
        [DataMember]
        public decimal DisabilityBenefitsPaid125 { get; set; }
        [DataMember]
        public decimal ApprenticeshipIncentiveGrant130 { get; set; }
        [DataMember]
        public decimal TfsaTaxableAmount134 { get; set; }
        [DataMember]
        public decimal CanadianBenefitPyvc136 { get; set; }
        [DataMember]
        public decimal LabourAdjustmentBenefitsAct150 { get; set; }
        [DataMember]
        public decimal CashAwardPrize154 { get; set; }
        [DataMember]
        public decimal VeteransBenefitsEligibleForPensionSplitting128 { get; set; }
        [DataMember]
        public short BoxNumber1 { get; set; }
        [DataMember]
        public short BoxNumber2 { get; set; }
        [DataMember]
        public short BoxNumber3 { get; set; }
        [DataMember]
        public short BoxNumber4 { get; set; }
        [DataMember]
        public short BoxNumber5 { get; set; }
        [DataMember]
        public short BoxNumber6 { get; set; }
        [DataMember]
        public short BoxNumber7 { get; set; }
        [DataMember]
        public short BoxNumber8 { get; set; }
        [DataMember]
        public short BoxNumber9 { get; set; }
        [DataMember]
        public short BoxNumber10 { get; set; }
        [DataMember]
        public short BoxNumber11 { get; set; }
        [DataMember]
        public short BoxNumber12 { get; set; }
        [DataMember]
        public decimal BoxAmount1 { get; set; }
        [DataMember]
        public decimal BoxAmount2 { get; set; }
        [DataMember]
        public decimal BoxAmount3 { get; set; }
        [DataMember]
        public decimal BoxAmount4 { get; set; }
        [DataMember]
        public decimal BoxAmount5 { get; set; }
        [DataMember]
        public decimal BoxAmount6 { get; set; }
        [DataMember]
        public decimal BoxAmount7 { get; set; }
        [DataMember]
        public decimal BoxAmount8 { get; set; }
        [DataMember]
        public decimal BoxAmount9 { get; set; }
        [DataMember]
        public decimal BoxAmount10 { get; set; }
        [DataMember]
        public decimal BoxAmount11 { get; set; }
        [DataMember]
        public decimal BoxAmount12 { get; set; }
        [DataMember]
        public bool ActiveFlag { get; set; }
        [DataMember]
        public int Revision { get; set; }
        [DataMember]
        public long? PreviousRevisionYearEndT4aId { get; set; }
        #endregion

        #region construct/destruct
        public YearEndT4a()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(YearEndT4a data)
        {
            base.CopyTo(data);

            data.YearEndT4aId = YearEndT4aId;
            data.Year = Year;
            data.EmployeeId = EmployeeId;
            data.EmployeeNumber = EmployeeNumber;
            data.EmployerNumber = EmployerNumber;
            data.PensionSuperannuation = PensionSuperannuation;
            data.LumpSumPay18 = LumpSumPay18;
            data.SelfEmployedCommission = SelfEmployedCommission;
            data.IncomeTaxDeduction22 = IncomeTaxDeduction22;
            data.Annuities = Annuities;
            data.EligibleRetiringAllow = EligibleRetiringAllow;
            data.NonEligibleRetiringAllow = NonEligibleRetiringAllow;
            data.OtherIncome = OtherIncome;
            data.PatronageAllocation = PatronageAllocation;
            data.RegisteredPensionPlanContributions = RegisteredPensionPlanContributions;
            data.PensionAdjustment = PensionAdjustment;
            data.RegistrationNumber = RegistrationNumber;
            data.FootNotesCode = FootNotesCode;
            data.RespIncome = RespIncome;
            data.RespAssistance = RespAssistance;
            data.CharitableDonations46 = CharitableDonations46;
            data.Footnotes = Footnotes;
            data.ProvincialTax = ProvincialTax;
            data.RecordStatus = RecordStatus;
            data.GenerateXml = GenerateXml;
            data.XmlCreated = XmlCreated;
            data.FeesForServices = FeesForServices;
            data.UnregisteredPlanAmount109 = UnregisteredPlanAmount109;
            data.MedicalPremiumBenefitsAmount118 = MedicalPremiumBenefitsAmount118;
            data.GroupInsurancePlanAmount119 = GroupInsurancePlanAmount119;
            data.LumpSumPaymentsFromUnregisteredPlan102 = LumpSumPaymentsFromUnregisteredPlan102;
            data.LumpSumPaymentsFromUnregisteredPlan108 = LumpSumPaymentsFromUnregisteredPlan108;
            data.LumpSumPaymentsFromUnregisteredPlan110 = LumpSumPaymentsFromUnregisteredPlan110;
            data.LumpSumPaymentsFromUnregisteredPlan158 = LumpSumPaymentsFromUnregisteredPlan158;
            data.LumpSumPaymentsFromUnregisteredPlan180 = LumpSumPaymentsFromUnregisteredPlan180;
            data.LumpSumPaymentsFromUnregisteredPlan190 = LumpSumPaymentsFromUnregisteredPlan190;
            data.TaxDeferredCooperativeShare129 = TaxDeferredCooperativeShare129;
            data.VariablePensionBenefits133 = VariablePensionBenefits133;
            data.RecipientPaidPreminumsPrivateHealthServices135 = RecipientPaidPreminumsPrivateHealthServices135;
            data.TuitionAssistanceAdultBasicEducation196 = TuitionAssistanceAdultBasicEducation196;
            data.ResearchGrants104 = ResearchGrants104;
            data.PaymentsWageLossReplacementPlan107 = PaymentsWageLossReplacementPlan107;
            data.VeteransBenefits127 = VeteransBenefits127;
            data.WageEarnerProtectionProgram132 = WageEarnerProtectionProgram132;
            data.SubpQualitifed152 = SubpQualitifed152;
            data.BankruptcySettlement156 = BankruptcySettlement156;
            data.RprrPayments194 = RprrPayments194;
            data.RegisteredDisabilitySavingsPlan131 = RegisteredDisabilitySavingsPlan131;
            data.ScholarshipsPrizes105 = ScholarshipsPrizes105;
            data.DeathBenefits106 = DeathBenefits106;
            data.LoanBenefits117 = LoanBenefits117;
            data.RevokedDpspPayments123 = RevokedDpspPayments123;
            data.DisabilityBenefitsPaid125 = DisabilityBenefitsPaid125;
            data.ApprenticeshipIncentiveGrant130 = ApprenticeshipIncentiveGrant130;
            data.TfsaTaxableAmount134 = TfsaTaxableAmount134;
            data.CanadianBenefitPyvc136 = CanadianBenefitPyvc136;
            data.LabourAdjustmentBenefitsAct150 = LabourAdjustmentBenefitsAct150;
            data.CashAwardPrize154 = CashAwardPrize154;
            data.VeteransBenefitsEligibleForPensionSplitting128 = VeteransBenefitsEligibleForPensionSplitting128;
            data.BoxNumber1 = BoxNumber1;
            data.BoxNumber2 = BoxNumber2;
            data.BoxNumber3 = BoxNumber3;
            data.BoxNumber4 = BoxNumber4;
            data.BoxNumber5 = BoxNumber5;
            data.BoxNumber6 = BoxNumber6;
            data.BoxNumber7 = BoxNumber7;
            data.BoxNumber8 = BoxNumber8;
            data.BoxNumber9 = BoxNumber9;
            data.BoxNumber10 = BoxNumber10;
            data.BoxNumber11 = BoxNumber11;
            data.BoxNumber12 = BoxNumber12;
            data.BoxAmount1 = BoxAmount1;
            data.BoxAmount2 = BoxAmount2;
            data.BoxAmount3 = BoxAmount3;
            data.BoxAmount4 = BoxAmount4;
            data.BoxAmount5 = BoxAmount5;
            data.BoxAmount6 = BoxAmount6;
            data.BoxAmount7 = BoxAmount7;
            data.BoxAmount8 = BoxAmount8;
            data.BoxAmount9 = BoxAmount9;
            data.BoxAmount10 = BoxAmount10;
            data.BoxAmount11 = BoxAmount11;
            data.BoxAmount12 = BoxAmount12;
            data.ActiveFlag = ActiveFlag;
            data.Revision = Revision;
            data.PreviousRevisionYearEndT4aId = PreviousRevisionYearEndT4aId;
        }
        public new void Clear()
        {
            base.Clear();

            YearEndT4aId = -1;
            Year = 0;
            EmployeeId = -1;
            EmployeeNumber = null;
            EmployerNumber = null;
            PensionSuperannuation = 0;
            LumpSumPay18 = 0;
            SelfEmployedCommission = 0;
            IncomeTaxDeduction22 = 0;
            Annuities = 0;
            EligibleRetiringAllow = 0;
            NonEligibleRetiringAllow = 0;
            OtherIncome = 0;
            PatronageAllocation = 0;
            RegisteredPensionPlanContributions = 0;
            PensionAdjustment = 0;
            RegistrationNumber = null;
            FootNotesCode = 0;
            RespIncome = 0;
            RespAssistance = 0;
            CharitableDonations46 = 0;
            Footnotes = null;
            ProvincialTax = 0;
            RecordStatus = null;
            GenerateXml = null;
            XmlCreated = null;
            FeesForServices = 0;
            UnregisteredPlanAmount109 = 0;
            MedicalPremiumBenefitsAmount118 = 0;
            GroupInsurancePlanAmount119 = 0;
            LumpSumPaymentsFromUnregisteredPlan102 = 0;
            LumpSumPaymentsFromUnregisteredPlan108 = 0;
            LumpSumPaymentsFromUnregisteredPlan110 = 0;
            LumpSumPaymentsFromUnregisteredPlan158 = 0;
            LumpSumPaymentsFromUnregisteredPlan180 = 0;
            LumpSumPaymentsFromUnregisteredPlan190 = 0;
            TaxDeferredCooperativeShare129 = 0;
            VariablePensionBenefits133 = 0;
            RecipientPaidPreminumsPrivateHealthServices135 = 0;
            TuitionAssistanceAdultBasicEducation196 = 0;
            ResearchGrants104 = 0;
            PaymentsWageLossReplacementPlan107 = 0;
            VeteransBenefits127 = 0;
            WageEarnerProtectionProgram132 = 0;
            SubpQualitifed152 = 0;
            BankruptcySettlement156 = 0;
            RprrPayments194 = 0;
            RegisteredDisabilitySavingsPlan131 = 0;
            ScholarshipsPrizes105 = 0;
            DeathBenefits106 = 0;
            LoanBenefits117 = 0;
            RevokedDpspPayments123 = 0;
            DisabilityBenefitsPaid125 = 0;
            ApprenticeshipIncentiveGrant130 = 0;
            TfsaTaxableAmount134 = 0;
            CanadianBenefitPyvc136 = 0;
            LabourAdjustmentBenefitsAct150 = 0;
            CashAwardPrize154 = 0;
            VeteransBenefitsEligibleForPensionSplitting128 = 0;
            BoxNumber1 = 0;
            BoxNumber2 = 0;
            BoxNumber3 = 0;
            BoxNumber4 = 0;
            BoxNumber5 = 0;
            BoxNumber6 = 0;
            BoxNumber7 = 0;
            BoxNumber8 = 0;
            BoxNumber9 = 0;
            BoxNumber10 = 0;
            BoxNumber11 = 0;
            BoxNumber12 = 0;
            BoxAmount1 = 0;
            BoxAmount2 = 0;
            BoxAmount3 = 0;
            BoxAmount4 = 0;
            BoxAmount5 = 0;
            BoxAmount6 = 0;
            BoxAmount7 = 0;
            BoxAmount8 = 0;
            BoxAmount9 = 0;
            BoxAmount10 = 0;
            BoxAmount11 = 0;
            BoxAmount12 = 0;
            ActiveFlag = true;
            Revision = 0;
            PreviousRevisionYearEndT4aId = null;
        }
        #endregion
    }
}