﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollMasterTax : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PayrollMasterTaxId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long PayrollMasterId { get; set; }
        [DataMember]
        public bool QuebecCalculationFlag { get; set; }
        [DataMember]
        public int CalculationType { get; set; }
        [DataMember]
        public Decimal FederalTax { get; set; }
        [DataMember]
        public Decimal ProvincialTax { get; set; }
        [DataMember]
        public Decimal QuebecProvincialTax { get; set; }
        [DataMember]
        public Decimal CombinedTaxes { get; set; }
        [DataMember]
        public Decimal QuebecCombinedTaxes { get; set; }
        [DataMember]
        public Decimal EmployeeCpp { get; set; }
        [DataMember]
        public Decimal EmployerCpp { get; set; }
        [DataMember]
        public Decimal CppEarnings { get; set; }
        [DataMember]
        public Decimal EmployeeQpp { get; set; }
        [DataMember]
        public Decimal EmployerQpp { get; set; }
        [DataMember]
        public Decimal QppEarnings { get; set; }
        [DataMember]
        public Decimal EmployeeEi { get; set; }
        [DataMember]
        public Decimal EmployerEi { get; set; }
        [DataMember]
        public Decimal EiEarnings { get; set; }
        [DataMember]
        public Decimal EmployeeEiReturnable { get; set; }
        [DataMember]
        public Decimal EmployeeQpip { get; set; }
        [DataMember]
        public Decimal EmployerQpip { get; set; }
        [DataMember]
        public Decimal QpipEarnings { get; set; }
        [DataMember]
        public Decimal QuebecHeathContribution { get; set; }
        [DataMember]
        public Decimal LumpSumTax { get; set; }
        [DataMember]
        public long PayrollProcessId { get; set; }
        [DataMember]
        public long EmployeeId { get; set; }

        #endregion

        #region construct/destruct
        public PayrollMasterTax()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PayrollMasterTax data)
        {
            base.CopyTo(data);

            data.PayrollMasterTaxId = PayrollMasterTaxId;
            data.PayrollMasterId = PayrollMasterId;
            data.QuebecCalculationFlag = QuebecCalculationFlag;
            data.CalculationType = CalculationType;
            data.FederalTax = FederalTax;
            data.ProvincialTax = ProvincialTax;
            data.QuebecProvincialTax = QuebecProvincialTax;
            data.CombinedTaxes = CombinedTaxes;
            data.QuebecCombinedTaxes = QuebecCombinedTaxes;
            data.EmployeeCpp = EmployeeCpp;
            data.EmployerCpp = EmployerCpp;
            data.CppEarnings = CppEarnings;
            data.EmployeeQpp = EmployeeQpp;
            data.EmployerQpp = EmployerQpp;
            data.QppEarnings = QppEarnings;
            data.EmployeeEi = EmployeeEi;
            data.EmployerEi = EmployerEi;
            data.EiEarnings = EiEarnings;
            data.EmployeeEiReturnable = EmployeeEiReturnable;
            data.EmployeeQpip = EmployeeQpip;
            data.EmployerQpip = EmployerQpip;
            data.QpipEarnings = QpipEarnings;
            data.QuebecHeathContribution = QuebecHeathContribution;
            data.LumpSumTax = LumpSumTax;
            data.PayrollProcessId = PayrollProcessId;
            data.EmployeeId = EmployeeId;
        }
        public new void Clear()
        {
            base.Clear();

            PayrollMasterTaxId = -1;
            PayrollMasterId = -1;
            QuebecCalculationFlag = false;
            CalculationType = 0;
            FederalTax = 0;
            ProvincialTax = 0;
            QuebecProvincialTax = 0;
            CombinedTaxes = 0;
            QuebecCombinedTaxes = 0;
            EmployeeCpp = 0;
            EmployerCpp = 0;
            CppEarnings = 0;
            EmployeeQpp = 0;
            EmployerQpp = 0;
            QppEarnings = 0;
            EmployeeEi = 0;
            EmployerEi = 0;
            EiEarnings = 0;
            EmployeeEiReturnable = 0;
            EmployeeQpip = 0;
            EmployerQpip = 0;
            QpipEarnings = 0;
            QuebecHeathContribution = 0;
            LumpSumTax = 0;
            PayrollProcessId = -1;
            EmployeeId = -1;
        }
        #endregion
    }
}
