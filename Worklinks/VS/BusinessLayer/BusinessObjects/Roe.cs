﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;


namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class Roe: WLP.BusinessLayer.BusinessObjects.BusinessObject, ICloneable
    {
        #region fields
        #endregion

        #region properties
        [DataMember]
        public long RoeId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public EmployeeCollection Employees { get; set; }
        [DataMember]
        public byte[] Data { get; set; }
        #endregion

        #region construct/destruct
        public Roe()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            Person item = new Person();
            this.CopyTo(item);
            return item;
        }
        public new void Clear()
        {
            base.Clear();
            RoeId = -1;
            Employees = null;
            Data = null;
        }

        public virtual void CopyTo(Roe data)
        {
            base.CopyTo(data);
            data.RoeId = RoeId;
            if (Employees == null)
                data.Employees = null;
            else
            {
                data.Employees = new EmployeeCollection();
                Employees.CopyTo(data.Employees);
            }
            Data.CopyTo(data.Data,0);

        }


        
        #endregion

    }
}
