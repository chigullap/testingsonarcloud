﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class StatutoryDeduction : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long StatutoryDeductionId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public string StatutoryDeductionTypeCode { get; set; }

        [DataMember]
        [Required]
        public string ProvinceStateCode { get; set; }

        [DataMember]
        [Required]
        public long? BusinessNumberId { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public bool PayEmploymentInsuranceFlag { get; set; }

        [DataMember]
        public bool PayCanadaPensionPlanFlag { get; set; }

        [DataMember]
        public bool ParentalInsurancePlanFlag { get; set; }

        [DataMember]
        public long? HealthTaxId { get; set; }

        [DataMember]
        public int? FederalTaxClaim { get; set; }

        [DataMember]
        public decimal? FederalAdditionalTax { get; set; }

        [DataMember]
        public int? FederalDesignatedAreaDeduction { get; set; }

        [DataMember]
        public int? FederalAuthorizedAnnualDeduction { get; set; }

        [DataMember]
        public int? FederalAuthorizedAnnualTaxCredit { get; set; }

        [DataMember]
        public int? FederalLabourTaxCredit { get; set; }

        [DataMember]
        public bool FederalPayTaxFlag { get; set; }

        [DataMember]
        public int? ProvincialTaxClaim { get; set; }

        [DataMember]
        public decimal? ProvincialAdditionalTax { get; set; }

        [DataMember]
        public int? ProvincialDesignatedAreaDeduction { get; set; }

        [DataMember]
        public int? ProvincialAuthorizedAnnualDeduction { get; set; }

        [DataMember]
        public int? ProvincialAuthorizedAnnualTaxCredit { get; set; }

        [DataMember]
        public int? ProvincialLabourTaxCredit { get; set; }

        [DataMember]
        public bool ProvincialPayTaxFlag { get; set; }

        [DataMember]
        public int? EstimatedAnnualIncome { get; set; }

        [DataMember]
        public int? EstimatedAnnualExpense { get; set; }

        [DataMember]
        public int? EstimatedNetIncome { get; set; }

        [DataMember]
        public decimal? CommissionPercentage { get; set; }

        [DataMember]
        public decimal? OverridePercentage { get; set; }

        [DataMember]
        public bool PayNationalInsuranceServiceFlag { get; set; }

        [DataMember]
        public decimal? TotalExemption { get; set; }

        [DataMember]
        public bool PayNationalInsuranceCorporationFlag { get; set; }

        [DataMember]
        public decimal? TDAU1Amount { get; set; }

        [DataMember]
        public bool PayNationalInsuranceBoardFlag { get; set; }

        [DataMember]
        public decimal? TD1Amount { get; set; }

        [DataMember]
        public bool PayNationalInsuranceSchemeFlag { get; set; }

        [DataMember]
        public bool PayNationalHealthTaxFlag { get; set; }

        [DataMember]
        public bool PayEducationTaxFlag { get; set; }

        [DataMember]
        public bool OverrideConcurrencyCheckFlag { get; set; }

        [DataMember]
        public bool IsWeek1Month1EmployeeForCurrentYearFlag { get; set; }

        [DataMember]
        public int? WeekNumber { get; set; }

        [DataMember]
        public int? MonthNumber { get; set; }

        [DataMember]
        public decimal? TotalGrossPayToDate { get; set; }

        [DataMember]
        public decimal? Prerequisites { get; set; }

        [DataMember]
        public decimal? NiToDate { get; set; }

        [DataMember]
        public decimal? GrossPayLessDeductions { get; set; }

        [DataMember]
        public decimal? Superannuation { get; set; }

        [DataMember]
        public decimal? ApprovedExp { get; set; }

        [DataMember]
        public decimal? TotalTaxToDateDeterminedByTaxTable { get; set; }
        #endregion

        #region construct/destruct
        public StatutoryDeduction()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(StatutoryDeduction data)
        {
            base.CopyTo(data);
            data.StatutoryDeductionId = StatutoryDeductionId;
            data.EmployeeId = EmployeeId;
            data.StatutoryDeductionTypeCode = StatutoryDeductionTypeCode;
            data.ProvinceStateCode = ProvinceStateCode;
            data.BusinessNumberId = BusinessNumberId;
            data.ActiveFlag = ActiveFlag;
            data.PayEmploymentInsuranceFlag = PayEmploymentInsuranceFlag;
            data.PayCanadaPensionPlanFlag = PayCanadaPensionPlanFlag;
            data.ParentalInsurancePlanFlag = ParentalInsurancePlanFlag;
            data.HealthTaxId = HealthTaxId;
            data.FederalTaxClaim = FederalTaxClaim;
            data.FederalAdditionalTax = FederalAdditionalTax;
            data.FederalDesignatedAreaDeduction = FederalDesignatedAreaDeduction;
            data.FederalAuthorizedAnnualDeduction = FederalAuthorizedAnnualDeduction;
            data.FederalAuthorizedAnnualTaxCredit = FederalAuthorizedAnnualTaxCredit;
            data.FederalLabourTaxCredit = FederalLabourTaxCredit;
            data.FederalPayTaxFlag = FederalPayTaxFlag;
            data.ProvincialTaxClaim = ProvincialTaxClaim;
            data.ProvincialAdditionalTax = ProvincialAdditionalTax;
            data.ProvincialDesignatedAreaDeduction = ProvincialDesignatedAreaDeduction;
            data.ProvincialAuthorizedAnnualDeduction = ProvincialAuthorizedAnnualDeduction;
            data.ProvincialAuthorizedAnnualTaxCredit = ProvincialAuthorizedAnnualTaxCredit;
            data.ProvincialLabourTaxCredit = ProvincialLabourTaxCredit;
            data.ProvincialPayTaxFlag = ProvincialPayTaxFlag;
            data.EstimatedAnnualIncome = EstimatedAnnualIncome;
            data.EstimatedAnnualExpense = EstimatedAnnualExpense;
            data.EstimatedNetIncome = EstimatedNetIncome;
            data.CommissionPercentage = CommissionPercentage;
            data.OverridePercentage = OverridePercentage;
            data.PayNationalInsuranceServiceFlag = PayNationalInsuranceServiceFlag;
            data.TotalExemption = TotalExemption;
            data.PayNationalInsuranceCorporationFlag = PayNationalInsuranceCorporationFlag;
            data.TDAU1Amount = TDAU1Amount;
            data.PayNationalInsuranceBoardFlag = PayNationalInsuranceBoardFlag;
            data.TD1Amount = TD1Amount;
            data.PayNationalInsuranceSchemeFlag = PayNationalInsuranceSchemeFlag;
            data.PayNationalHealthTaxFlag = PayNationalHealthTaxFlag;
            data.PayEducationTaxFlag = PayEducationTaxFlag;
            data.OverrideConcurrencyCheckFlag = OverrideConcurrencyCheckFlag;
            data.IsWeek1Month1EmployeeForCurrentYearFlag = IsWeek1Month1EmployeeForCurrentYearFlag;
            data.WeekNumber = WeekNumber;
            data.MonthNumber = MonthNumber;
            data.TotalGrossPayToDate = TotalGrossPayToDate;
            data.Prerequisites = Prerequisites;
            data.NiToDate = NiToDate;
            data.GrossPayLessDeductions = GrossPayLessDeductions;
            data.Superannuation = Superannuation;
            data.ApprovedExp = ApprovedExp;
            data.TotalTaxToDateDeterminedByTaxTable = TotalTaxToDateDeterminedByTaxTable;
        }
        public new void Clear()
        {
            base.Clear();
            StatutoryDeductionId = -1;
            EmployeeId = -1;
            StatutoryDeductionTypeCode = null;
            ProvinceStateCode = null;
            BusinessNumberId = null;
            ActiveFlag = true;
            PayEmploymentInsuranceFlag = false;
            PayCanadaPensionPlanFlag = false;
            HealthTaxId = null;
            ParentalInsurancePlanFlag = false;
            FederalTaxClaim = null;
            FederalAdditionalTax = null;
            FederalDesignatedAreaDeduction = null;
            FederalAuthorizedAnnualDeduction = null;
            FederalAuthorizedAnnualTaxCredit = null;
            FederalLabourTaxCredit = null;
            FederalPayTaxFlag = false;
            ProvincialTaxClaim = null;
            ProvincialAdditionalTax = null;
            ProvincialDesignatedAreaDeduction = null;
            ProvincialAuthorizedAnnualDeduction = null;
            ProvincialAuthorizedAnnualTaxCredit = null;
            ProvincialLabourTaxCredit = null;
            ProvincialPayTaxFlag = false;
            EstimatedAnnualIncome = null;
            EstimatedAnnualExpense = null;
            EstimatedNetIncome = null;
            CommissionPercentage = null;
            OverridePercentage = null;
            PayNationalInsuranceServiceFlag = false;
            TotalExemption = null;
            PayNationalInsuranceCorporationFlag = false;
            TDAU1Amount = null;
            PayNationalInsuranceBoardFlag = false;
            TD1Amount = null;
            PayNationalInsuranceSchemeFlag = false;
            PayNationalHealthTaxFlag = false;
            PayEducationTaxFlag = false;
            OverrideConcurrencyCheckFlag = false;
            IsWeek1Month1EmployeeForCurrentYearFlag = false;
            WeekNumber = null;
            MonthNumber = null;
            TotalGrossPayToDate = null;
            Prerequisites = null;
            NiToDate = null;
            GrossPayLessDeductions = null;
            Superannuation = null;
            ApprovedExp = null;
            TotalTaxToDateDeterminedByTaxTable = null;
        }
        #endregion
    }
}