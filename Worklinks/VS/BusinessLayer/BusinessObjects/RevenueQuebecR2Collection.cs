﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class RevenueQuebecR2Collection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<RevenueQuebecR2>
    {
        public RevenueQuebecR2Collection()
        {
        }
        public RevenueQuebecR2Collection(YearEndR2Collection yearEndR2Collection)
        {
            foreach (YearEndR2 r2 in yearEndR2Collection)
            {
                RevenueQuebecR2 rqr2 = new RevenueQuebecR2();
                r2.CopyTo(rqr2);
                this.Add(rqr2);
            }
        }
    }
}
