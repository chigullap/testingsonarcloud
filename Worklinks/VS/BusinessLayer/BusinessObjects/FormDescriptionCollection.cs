﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class FormDescriptionCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<FormDescription>
    {
    }

}
