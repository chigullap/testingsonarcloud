﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeeBenefitCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeeBenefit>
    {
    }
}
