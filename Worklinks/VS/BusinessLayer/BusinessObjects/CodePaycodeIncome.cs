﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    [XmlRoot("CodePaycode")]
    public class CodePaycodeIncome : CodePaycode
    {
        #region properties
        [DataMember]
        public long PaycodeIncomeId { get; set; }

        [DataMember]
        public String PaycodeIncomePayTypeCode { get; set; }

        [DataMember]
        public bool IncomeTaxFlag { get; set; }

        [DataMember]
        public bool CanadaQuebecPensionPlanFlag { get; set; }

        [DataMember]
        public bool EmploymentInsuranceFlag { get; set; }

        [DataMember]
        public bool ProvincialParentalInsurancePlanFlag { get; set; }

        [DataMember]
        public bool QuebecTaxFlag { get; set; }

        [DataMember]
        public bool ProvincialHealthTaxFlag { get; set; }

        [DataMember]
        public bool WorkersCompensationBoardFlag { get; set; }

        [DataMember]
        public String FederalTaxPaycodeCode { get; set; }

        [DataMember]
        public String QuebecTaxPaycodeCode { get; set; }

        [DataMember]
        public bool BarbadosNationalInsuranceSchemeFlag { get; set; }

        [DataMember]
        public bool BarbadosIncomeTaxFlag { get; set; }

        [DataMember]
        public bool SaintLuciaNationalInsuranceCorporationFlag { get; set; }

        [DataMember]
        public bool SaintLuciaIncomeTaxFlag { get; set; }

        [DataMember]
        public bool TrinidadNationalInsuranceBoardFlag { get; set; }

        [DataMember]
        public bool TrinidadIncomeTaxFlag { get; set; }

        [DataMember]
        public bool JamaicaNationalInsuranceSchemeFlag { get; set; }

        [DataMember]
        public bool JamaicaNationalHealthTaxFlag { get; set; }

        [DataMember]
        public bool JamaicaIncomeTaxFlag { get; set; }

        [DataMember]
        public new byte[] RowVersion { get; set; }
        #endregion

        #region construct/destruct
        public CodePaycodeIncome()
        {
            Clear();
            this.PaycodeTypeCode = "1";
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(CodePaycodeIncome data)
        {
            base.CopyTo(data);
            data.PaycodeIncomeId = PaycodeIncomeId;
            data.PaycodeIncomePayTypeCode = PaycodeIncomePayTypeCode;
            data.IncomeTaxFlag = IncomeTaxFlag;
            data.CanadaQuebecPensionPlanFlag = CanadaQuebecPensionPlanFlag;
            data.EmploymentInsuranceFlag = EmploymentInsuranceFlag;
            data.ProvincialParentalInsurancePlanFlag = ProvincialParentalInsurancePlanFlag;
            data.QuebecTaxFlag = QuebecTaxFlag;
            data.ProvincialHealthTaxFlag = ProvincialHealthTaxFlag;
            data.WorkersCompensationBoardFlag = WorkersCompensationBoardFlag;
            data.FederalTaxPaycodeCode = FederalTaxPaycodeCode;
            data.QuebecTaxPaycodeCode = QuebecTaxPaycodeCode;
            data.BarbadosNationalInsuranceSchemeFlag = BarbadosNationalInsuranceSchemeFlag;
            data.BarbadosIncomeTaxFlag = BarbadosIncomeTaxFlag;
            data.SaintLuciaNationalInsuranceCorporationFlag = SaintLuciaNationalInsuranceCorporationFlag;
            data.SaintLuciaIncomeTaxFlag = SaintLuciaIncomeTaxFlag;
            data.TrinidadNationalInsuranceBoardFlag = TrinidadNationalInsuranceBoardFlag;
            data.TrinidadIncomeTaxFlag = TrinidadIncomeTaxFlag;
            data.JamaicaNationalInsuranceSchemeFlag = JamaicaNationalInsuranceSchemeFlag;
            data.JamaicaNationalHealthTaxFlag = JamaicaNationalHealthTaxFlag;
            data.JamaicaIncomeTaxFlag = JamaicaIncomeTaxFlag;
            data.RowVersion = RowVersion;
        }
        public new void Clear()
        {
            base.Clear();
            PaycodeIncomeId = -1;
            PaycodeIncomePayTypeCode = null;
            IncomeTaxFlag = false;
            CanadaQuebecPensionPlanFlag = false;
            EmploymentInsuranceFlag = false;
            ProvincialParentalInsurancePlanFlag = false;
            QuebecTaxFlag = false;
            ProvincialHealthTaxFlag = false;
            WorkersCompensationBoardFlag = false;
            FederalTaxPaycodeCode = null;
            QuebecTaxPaycodeCode = null;
            BarbadosNationalInsuranceSchemeFlag = false;
            BarbadosIncomeTaxFlag = false;
            SaintLuciaNationalInsuranceCorporationFlag = false;
            SaintLuciaIncomeTaxFlag = false;
            TrinidadNationalInsuranceBoardFlag = false;
            TrinidadIncomeTaxFlag = false;
            JamaicaNationalInsuranceSchemeFlag = false;
            JamaicaNationalHealthTaxFlag = false;
            JamaicaIncomeTaxFlag = false;
            RowVersion = new byte[8];
        }
        #endregion
    }
}