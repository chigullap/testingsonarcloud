﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class AccrualPolicyEntitlement : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long AccrualPolicyEntitlementId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long AccrualPolicyId { get; set; }

        [DataMember]
        public long AccrualEntitlementId { get; set; }
        #endregion

        #region construct/destruct
        public AccrualPolicyEntitlement()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(AccrualPolicyEntitlement data)
        {
            base.CopyTo(data);

            data.AccrualPolicyEntitlementId = AccrualPolicyEntitlementId;
            data.AccrualPolicyId = AccrualPolicyId;
            data.AccrualEntitlementId = AccrualEntitlementId;
        }
        public new void Clear()
        {
            base.Clear();

            AccrualPolicyEntitlementId = -1;
            AccrualPolicyId = -1;
            AccrualEntitlementId = -1;
        }
        #endregion
    }
}