﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ChequeHealthTaxExport : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long ChequeHealthTaxExportId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long RbcSequenceNumber { get; set; }

        [DataMember]
        public long RbcChequeNumber { get; set; }

        [DataMember]
        public byte[] Data { get; set; }

        [DataMember]
        public string CodeProvinceStateCd { get; set; }

        [DataMember]
        public DateTime RemittanceDate { get; set; }

        [DataMember]
        public string CodeWcbChequeRemittanceFrequencyCd { get; set; }

        [DataMember]
        public DateTime? ChequeFileTransmissionDate { get; set; }

        #endregion

        #region construct/destruct
        public ChequeHealthTaxExport()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();

            ChequeHealthTaxExportId = -1;
            RbcSequenceNumber = -1;
            RbcChequeNumber = -1;
            Data = null;
            CodeProvinceStateCd = null;
            RemittanceDate = DateTime.Now;
            CodeWcbChequeRemittanceFrequencyCd = null;
            ChequeFileTransmissionDate = null;
        }

        public virtual void CopyTo(ChequeHealthTaxExport data)
        {
            base.CopyTo(data);

            data.ChequeHealthTaxExportId = ChequeHealthTaxExportId;
            data.RbcSequenceNumber = RbcSequenceNumber;
            data.RbcChequeNumber = RbcChequeNumber;
            Data.CopyTo(data.Data, 0);
            data.CodeProvinceStateCd = CodeProvinceStateCd;
            data.RemittanceDate = RemittanceDate;
            data.CodeWcbChequeRemittanceFrequencyCd = CodeWcbChequeRemittanceFrequencyCd;
            data.ChequeFileTransmissionDate = ChequeFileTransmissionDate;
        }
        #endregion
    }
}
