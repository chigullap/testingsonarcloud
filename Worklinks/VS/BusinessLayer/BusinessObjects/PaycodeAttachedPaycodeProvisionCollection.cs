﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PaycodeAttachedPaycodeProvisionCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PaycodeAttachedPaycodeProvision>
    {
    }
}