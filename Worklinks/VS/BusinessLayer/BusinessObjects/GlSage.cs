﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class GlSage : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long GlSageId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String Header1RecType { get; set; }
        [DataMember]
        public String Header1BatchId { get; set; }
        [DataMember]
        public String Header1BatchEntry { get; set; }
        [DataMember]
        public String Header1SourceLedger { get; set; }
        [DataMember]
        public String Header1SourceType { get; set; }
        [DataMember]
        public String Header1JournalDescription { get; set; }
        [DataMember]
        public String Header1FiscalYear { get; set; }
        [DataMember]
        public String Header1FiscalMonth { get; set; }
        [DataMember]
        public DateTime Header1DateEntry { get; set; }
        [DataMember]
        public String Header2RecType { get; set; }
        [DataMember]
        public String Header2BatchId { get; set; }
        [DataMember]
        public String Header2JournalEntry { get; set; }
        [DataMember]
        public String Header2TransactionNumber { get; set; }
        [DataMember]
        public String Header2AccountNumber { get; set; }
        [DataMember]
        public Decimal Header2TransactionAmount { get; set; }
        [DataMember]
        public String Header2TransactionDesc { get; set; }
        [DataMember]
        public String Header2TransactionRef { get; set; }
        [DataMember]
        public DateTime Header2TransactionDate { get; set; }

        #endregion

        #region construct/destruct
        public GlSage()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(GlSage data)
        {
            base.CopyTo(data);

            data.GlSageId = GlSageId;
            data.Header1RecType = Header1RecType;
            data.Header1BatchId = Header1BatchId;
            data.Header1BatchEntry = Header1BatchEntry;
            data.Header1SourceLedger = Header1SourceLedger;
            data.Header1SourceType = Header1SourceType;
            data.Header1JournalDescription = Header1JournalDescription;
            data.Header1FiscalYear = Header1FiscalYear;
            data.Header1FiscalMonth = Header1FiscalMonth;
            data.Header1DateEntry = Header1DateEntry;
            data.Header2RecType = Header2RecType;
            data.Header2BatchId = Header2BatchId;
            data.Header2JournalEntry = Header2JournalEntry;
            data.Header2TransactionNumber = Header2TransactionNumber;
            data.Header2AccountNumber = Header2AccountNumber;
            data.Header2TransactionAmount = Header2TransactionAmount;
            data.Header2TransactionDesc = Header2TransactionDesc;
            data.Header2TransactionRef = Header2TransactionRef;
            data.Header2TransactionDate = Header2TransactionDate;
        }
        #endregion
    }
}
