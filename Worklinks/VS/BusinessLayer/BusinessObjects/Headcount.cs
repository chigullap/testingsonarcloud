﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class Headcount : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public String HeadcountCode
        {
            get { return (String)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public Decimal Count { get; set; }

        [DataMember]
        public Decimal Percentage { get; set; }

        [DataMember]
        public String ChartTitle { get; set; }
        #endregion

        #region construct/destruct
        public Headcount()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(Headcount data)
        {
            base.CopyTo(data);
            data.HeadcountCode = HeadcountCode;
            data.Description = Description;
            data.Count = Count;
            data.Percentage = Percentage;
            data.ChartTitle = ChartTitle;
        }
        public new void Clear()
        {
            base.Clear();
            HeadcountCode = null;
            Description = null;
            Count = 0;
            Percentage = 0;
            ChartTitle = null;
        }
        #endregion
    }
}