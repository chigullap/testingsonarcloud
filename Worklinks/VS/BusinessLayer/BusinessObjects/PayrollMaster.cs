﻿using System;
using System.Runtime.Serialization;
using WorkLinks.BusinessLayer.BusinessObjects.CalcModel;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollMaster : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region fields
        private Decimal _federalDesignatedAreaDeduction;
        private Decimal _federalAdditionalTax;
        private Decimal _federalAuthorizedAnnualDeduction;
        private Decimal _federalAuthorizedAnnualTaxCredit;
        private Decimal _estimatedAnnualIncome;
        private Decimal _estimatedAnnualExpense;
        private Decimal _provincialAdditionalTax;
        private Decimal _provincialAuthorizedAnnualDeduction;
        private Decimal _provincialAuthorizedAnnualTaxCredit;
        private Decimal _employerEmploymentInsuranceRate;
        private Decimal _federalTaxClaim;
        private Decimal _provincialTaxClaim;

        public Decimal _ytdPreviousCanadaPensionPlan;
        public Decimal _ytdPreviousCanadaPensionPlanEarnings;
        public Decimal _ytdPreviousEmploymentInsurance;
        public Decimal _ytdPreviousEmployerEmploymentInsurance;
        public Decimal _ytdPreviousQuebecParentalInsurancePlan;
        public Decimal _ytdPreviousEmployerQuebecParentalInsurancePlan;
        public Decimal _ytdPreviousBonusTaxableIncome;
        public Decimal _ytdPreviousQuebecBonusTaxableIncome;
        public Decimal _ytdPreviousLumpsumTaxableIncome;
        public Decimal _ytdPreviousQuebecLumpsumTaxableIncome;
        public Decimal _ytdPreviousQuebecHealthContribution;
        public Decimal _ytdPreviousWorkersCompensationAssessable;
        public Decimal _workersCompensationMaximumAssessableEarning;
        #endregion

        #region properties
        [DataMember]
        public long PayrollMasterId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public PayrollTransactionPayrollMasterCollection Transactions { get; set; }

        [DataMember]
        public PayrollProcess PayrollProcess { get; set; }

        [DataMember]
        public bool HasCurrentFlag { get; set; }

        [DataMember]
        public long PayrollProcessId { get; set; }

        [DataMember]
        public long BusinessNumberId { get; set; }

        [DataMember]
        public String CodeWsibCd { get; set; }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public long EmployeePositionId { get; set; }

        [DataMember]
        public String CodeProvinceStateCd { get; set; }

        [DataMember]
        public Int16 PeriodsPerYear { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public Decimal? FederalTaxClaim { get { return _federalTaxClaim; } set { _federalTaxClaim = value ?? 0; } }

        [DataMember]
        public bool FederalPayTaxFlag { get; set; }

        [DataMember]
        public Decimal? FederalDesignatedAreaDeduction { get { return _federalDesignatedAreaDeduction; } set { _federalDesignatedAreaDeduction = value ?? 0; } }

        [DataMember]
        public Decimal? FederalAdditionalTax { get { return _federalAdditionalTax; } set { _federalAdditionalTax = value ?? 0; } }

        [DataMember]
        public Decimal? ProvincialAdditionalTax { get { return _provincialAdditionalTax; } set { _provincialAdditionalTax = value ?? 0; } }

        [DataMember]
        public Decimal? FederalAuthorizedAnnualDeduction { get { return _federalAuthorizedAnnualDeduction; } set { _federalAuthorizedAnnualDeduction = value ?? 0; } }

        [DataMember]
        public Decimal? FederalAuthorizedAnnualTaxCredit { get { return _federalAuthorizedAnnualTaxCredit; } set { _federalAuthorizedAnnualTaxCredit = value ?? 0; } }

        [DataMember]
        public Decimal? EstimatedAnnualIncome { get { return _estimatedAnnualIncome; } set { _estimatedAnnualIncome = value ?? 0; } }

        [DataMember]
        public Decimal? EstimatedAnnualExpense { get { return _estimatedAnnualExpense; } set { _estimatedAnnualExpense = value ?? 0; } }

        [DataMember]
        public Decimal? ProvincialTaxClaim { get { return _provincialTaxClaim; } set { _provincialTaxClaim = value ?? 0; } }

        [DataMember]
        public bool ProvincialPayTaxFlag { get; set; }

        [DataMember]
        public Decimal? ProvincialAuthorizedAnnualDeduction { get { return _provincialAuthorizedAnnualDeduction; } set { _provincialAuthorizedAnnualDeduction = value ?? 0; } }
        [DataMember]
        public Decimal? ProvincialAuthorizedAnnualTaxCredit { get { return _provincialAuthorizedAnnualTaxCredit; } set { _provincialAuthorizedAnnualTaxCredit = value ?? 0; } }

        [DataMember]
        public bool PayCanadaPensionPlanFlag { get; set; }

        [DataMember]
        public bool PayEmploymentInsuranceFlag { get; set; }

        [DataMember]
        public bool ParentalInsurancePlanFlag { get; set; }

        [DataMember]
        public long? HealthTaxId { get; set; }

        [DataMember]
        public Decimal? EmployerEmploymentInsuranceRate { get { return _employerEmploymentInsuranceRate; } set { _employerEmploymentInsuranceRate = value ?? 0; } }

        [DataMember]
        public Decimal? YtdPreviousCanadaPensionPlan { get { return _ytdPreviousCanadaPensionPlan; } set { _ytdPreviousCanadaPensionPlan = value ?? 0; } }
        [DataMember]
        public Decimal? YtdPreviousCanadaPensionPlanEarnings { get { return _ytdPreviousCanadaPensionPlanEarnings; } set { _ytdPreviousCanadaPensionPlanEarnings = value ?? 0; } }

        [DataMember]
        public Decimal? YtdPreviousEmploymentInsurance { get { return _ytdPreviousEmploymentInsurance; } set { _ytdPreviousEmploymentInsurance = value ?? 0; } }

        [DataMember]
        public Decimal? YtdPreviousEmployerEmploymentInsurance { get { return _ytdPreviousEmployerEmploymentInsurance; } set { _ytdPreviousEmployerEmploymentInsurance = value ?? 0; } }

        [DataMember]
        public Decimal? YtdPreviousQuebecParentalInsurancePlan { get { return _ytdPreviousQuebecParentalInsurancePlan; } set { _ytdPreviousQuebecParentalInsurancePlan = value ?? 0; } }

        [DataMember]
        public Decimal? YtdPreviousEmployerQuebecParentalInsurancePlan { get { return _ytdPreviousEmployerQuebecParentalInsurancePlan; } set { _ytdPreviousEmployerQuebecParentalInsurancePlan = value ?? 0; } }

        [DataMember]
        public Decimal? YtdPreviousBonusTaxableIncome { get { return _ytdPreviousBonusTaxableIncome; } set { _ytdPreviousBonusTaxableIncome = value ?? 0; } }

        [DataMember]
        public Decimal? YtdPreviousQuebecBonusTaxableIncome { get { return _ytdPreviousQuebecBonusTaxableIncome; } set { _ytdPreviousQuebecBonusTaxableIncome = value ?? 0; } }

        [DataMember]
        public Decimal? YtdPreviousLumpsumTaxableIncome { get { return _ytdPreviousLumpsumTaxableIncome; } set { _ytdPreviousLumpsumTaxableIncome = value ?? 0; } }

        [DataMember]
        public Decimal? YtdPreviousQuebecLumpsumTaxableIncome { get { return _ytdPreviousQuebecLumpsumTaxableIncome; } set { _ytdPreviousQuebecLumpsumTaxableIncome = value ?? 0; } }

        [DataMember]
        public Decimal? YtdPreviousQuebecHealthContribution { get { return _ytdPreviousQuebecHealthContribution; } set { _ytdPreviousQuebecHealthContribution = value ?? 0; } }

        [DataMember]
        public Decimal? YtdPreviousWorkersCompensationAssessable { get { return _ytdPreviousWorkersCompensationAssessable; } set { _ytdPreviousWorkersCompensationAssessable = value ?? 0; } }

        [DataMember]
        public DateTime? YtdPreviousCommissionPayDate { get; set; }

        [DataMember]
        public Int16 Period { get; set; }

        [DataMember]
        public Int16 NumberOfPayPeriodsCovered { get; set; }

        //used to non-quebec federal tax calcuation
        public Decimal SumFederalProvincialAdditionalTax { get { return _federalAdditionalTax + _provincialAdditionalTax; } }

        //maxes
        public Decimal MaxQuebecPensionPlanEarnings { get; set; }
        public Decimal MaxCanadaPensionPlanEarnings { get; set; }
        public Decimal MaxQuebecParentalInsuranceEarnings { get; set; }
        public Decimal CompensationAmount { get; set; }
        public Decimal? WorkersCompensationMaximumAssessableEarnings { get { return _workersCompensationMaximumAssessableEarning; } set { _workersCompensationMaximumAssessableEarning = value ?? 0; } }

        public Decimal WorkersCompensationExcessEarnings { get { return WorkersCompensationEarnings - WorkersCompensationEarningsAssessable; } }

        [DataMember]
        public PayrollMasterDetailCollection Details { get; set; }

        #region calc props
        public bool FederalTaxExemptFlag
        {
            get
            {
                if (PayrollProcess.PayrollProcessRunTypeCode == "ADJUST")
                    return false;
                else
                    return !FederalPayTaxFlag;
            }
        }
        public bool ParentalInsurancePlanExemptFlag
        {
            get
            {
                if (PayrollProcess.PayrollProcessRunTypeCode == "ADJUST")
                    return false;
                else
                    return !ParentalInsurancePlanFlag;
            }
        }
        public Int16 DaysSinceLastCommissionPayment { get { if (YtdPreviousCommissionPayDate == null) return 0; else return Convert.ToInt16((ChequeDate - (DateTime)YtdPreviousCommissionPayDate).TotalDays); } }

        public bool EmploymentInsuranceExemptFlag
        {
            get
            {
                if (PayrollProcess.PayrollProcessRunTypeCode == "ADJUST")
                    return false;
                else
                    return !PayEmploymentInsuranceFlag;
            }
        }
        public bool ProvincialTaxExemptFlag
        {
            get
            {
                if (PayrollProcess.PayrollProcessRunTypeCode == "ADJUST")
                    return false;
                else
                    return !ProvincialPayTaxFlag;
            }
        }
        public bool CanadaPensionPlanExemptFlag
        {
            get
            {
                if (PayrollProcess.PayrollProcessRunTypeCode == "ADJUST")
                    return false;
                else
                    return !PayCanadaPensionPlanFlag;
            }
        }
        public Decimal BonusLumpSumTaxableIncome { get { return this.TaxableIncome == 0 ? this.CompensationAmount : this.TaxableIncome; } }

        public Decimal CanadaPensionPlanEarningsAssessable { get { return GetValueBasedOnMax(CanadaPensionPlanEarnings, YtdPreviousCanadaPensionPlanEarnings, MaxCanadaPensionPlanEarnings); } }
        public Decimal QuebecPensionPlanEarningsAssessable { get { return GetValueBasedOnMax(QuebecPensionPlanEarnings, YtdPreviousCanadaPensionPlanEarnings, MaxQuebecPensionPlanEarnings); } }
        public Decimal QuebecParentalInsurancePlanEarningsAssessable { get { return GetValueBasedOnMax(QuebecParentalInsurancePlanEarnings, YtdPreviousQuebecParentalInsurancePlan, MaxQuebecParentalInsuranceEarnings); } }
        public Decimal EmploymentInsuranceEarningsAssessable { get { return EmploymentInsuranceEarnings; } }
        public Decimal WorkersCompensationEarningsAssessable { get { return GetValueBasedOnMax(WorkersCompensationEarnings, YtdPreviousWorkersCompensationAssessable, WorkersCompensationMaximumAssessableEarnings); } }
        public Decimal EmployerHealthTaxEarningsAssessable { get { return EmployerHealthTaxEarnings; } }
        #endregion


        #region summaries

        public Decimal TaxableIncome { get; set; }
        public Decimal OverrideTaxIncome { get; set; }
        public Decimal QuebecTaxableIncome { get; set; }
        public Decimal BonusTaxableIncome { get; set; }
        public Decimal QuebecBonusTaxableIncome { get; set; }
        public Decimal LumpsumTaxableIncome { get; set; }
        public Decimal QuebecLumpsumTaxableIncome { get; set; }
        public Decimal PensionTaxableIncome { get; set; }
        public Decimal QuebecPensionTaxableIncome { get; set; }
        public Decimal TaxableBenefit { get; set; }
        public Decimal QuebecTaxableBenefit { get; set; }
        public Decimal TaxableCashBenefit { get; set; }
        public Decimal QuebecTaxableCashBenefit { get; set; }
        public Decimal TaxableNonCashBenefit { get; set; }
        public Decimal QuebecTaxableNonCashBenefit { get; set; }
        public Decimal SeveranceTaxableIncome { get; set; }
        public Decimal QuebecSeveranceTaxableIncome { get; set; }
        public Decimal CommissionTaxableIncome { get; set; }
        public Decimal QuebecCommissionTaxableIncome { get; set; }
        //public Decimal IncomeOnlyDeductionBeforeTax { get; set; }
        //public Decimal BonusOnlyDeductionBeforeTax { get; set; }
        //public Decimal BonusIncomeBeforeTaxDeduction { get; set; }
        public Decimal WorkersCompensationEarnings { get; set; }
        public Decimal CanadaPensionPlanEarnings { get; set; }
        public Decimal QuebecPensionPlanEarnings { get; set; }
        public Decimal QuebecParentalInsurancePlanEarnings { get; set; }
        public Decimal EmploymentInsuranceEarnings { get; set; }
        public Decimal EmployerHealthTaxEarnings { get; set; }

        #endregion

        //for exports
        [DataMember]
        public String EmployeeNumber { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime CutoffDate { get; set; }

        [DataMember]
        public Decimal ChequeAmount { get; set; }

        [DataMember]
        public Decimal EmploymentIncome14 { get; set; }

        [DataMember]
        public Decimal CanadaPensionPlan { get; set; }

        [DataMember]
        public Decimal EmploymentInsurance { get; set; }

        [DataMember]
        public Decimal FederalTax { get; set; }

        [DataMember]
        public Decimal QuebecTax { get; set; }

        [DataMember]
        public Decimal QuebecParentalInsurancePlan { get; set; }

        [DataMember]
        public Decimal EmployerWorkersCompensationPremium { get; set; }

        [DataMember]
        public Int16 HowPaid { get; set; }

        [DataMember]
        public String CodeCompanyCd { get; set; }

        [DataMember]
        public Decimal EmployerHealthTaxLevy { get; set; }

        [DataMember]
        public Decimal EmployerEmploymentInsurance { get; set; }

        [DataMember]
        public Decimal EmployerCanadaPensionPlan { get; set; }

        [DataMember]
        public String EmployerNumber { get; set; }

        public Decimal GrossAmount { get { return EmploymentIncome14 + OverrideTaxIncome; } }

        [DataMember]
        public DateTime ChequeDate { get; set; }
        #endregion

        public Decimal BeforeIncomeTaxDeduction
        {
            get
            {
                Decimal rtn = 0;

                foreach (PayrollMasterDetail detail in this.Details)
                {
                    rtn += detail.BeforeIncomeTaxDeduction;
                }

                return rtn;
            }
        }
        public Decimal BeforeBonusTaxDeduction
        {
            get
            {
                Decimal rtn = 0;

                foreach (PayrollMasterDetail detail in this.Details)
                {
                    rtn += detail.BeforeBonusTaxDeduction;
                }

                return rtn;
            }
        }

        #region constructor/destructor
        public PayrollMaster()
        {
            Clear();
        }
        public PayrollMaster(PayrollProcess process)
        {
            Clear();
            PayrollProcess = process;
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PayrollMaster data)
        {
            base.CopyTo(data);

            data.PayrollMasterId = PayrollMasterId;
            data.HasCurrentFlag = HasCurrentFlag;
            data.PayrollProcessId = PayrollProcessId;
            data.BusinessNumberId = BusinessNumberId;
            data.CodeWsibCd = CodeWsibCd;
            data.EmployeeId = EmployeeId;
            data.EmployeePositionId = EmployeePositionId;
            data.CodeProvinceStateCd = CodeProvinceStateCd;
            data.PeriodsPerYear = PeriodsPerYear;
            data.BirthDate = BirthDate;
            data.FederalTaxClaim = FederalTaxClaim;
            data.FederalDesignatedAreaDeduction = FederalDesignatedAreaDeduction;
            data.FederalAdditionalTax = FederalAdditionalTax;
            data.FederalAuthorizedAnnualDeduction = FederalAuthorizedAnnualDeduction;
            data.FederalAuthorizedAnnualTaxCredit = FederalAuthorizedAnnualTaxCredit;
            data.EstimatedAnnualIncome = EstimatedAnnualIncome;
            data.EstimatedAnnualExpense = EstimatedAnnualExpense;
            data.ProvincialTaxClaim = ProvincialTaxClaim;
            data.ProvincialAuthorizedAnnualDeduction = ProvincialAuthorizedAnnualDeduction;
            data.ProvincialAuthorizedAnnualTaxCredit = ProvincialAuthorizedAnnualTaxCredit;

            data.EmployerEmploymentInsuranceRate = EmployerEmploymentInsuranceRate;
            data.YtdPreviousCanadaPensionPlan = YtdPreviousCanadaPensionPlan;
            data.YtdPreviousCanadaPensionPlanEarnings = YtdPreviousCanadaPensionPlanEarnings;
            data.YtdPreviousEmploymentInsurance = YtdPreviousEmploymentInsurance;
            data.YtdPreviousEmployerEmploymentInsurance = YtdPreviousEmployerEmploymentInsurance;
            data.YtdPreviousQuebecParentalInsurancePlan = YtdPreviousQuebecParentalInsurancePlan;
            data.YtdPreviousEmployerQuebecParentalInsurancePlan = YtdPreviousEmployerQuebecParentalInsurancePlan;
            data.YtdPreviousBonusTaxableIncome = YtdPreviousBonusTaxableIncome;
            data.YtdPreviousQuebecBonusTaxableIncome = YtdPreviousQuebecBonusTaxableIncome;
            data.YtdPreviousLumpsumTaxableIncome = YtdPreviousLumpsumTaxableIncome;
            data.YtdPreviousWorkersCompensationAssessable = YtdPreviousWorkersCompensationAssessable;
            data.YtdPreviousQuebecLumpsumTaxableIncome = YtdPreviousQuebecLumpsumTaxableIncome;
            data.YtdPreviousQuebecHealthContribution = YtdPreviousQuebecHealthContribution;
            data.Period = Period;
            data.NumberOfPayPeriodsCovered = NumberOfPayPeriodsCovered;
            data.ChequeDate = ChequeDate;
            data.MaxCanadaPensionPlanEarnings = MaxCanadaPensionPlanEarnings;
            data.MaxQuebecParentalInsuranceEarnings = MaxQuebecParentalInsuranceEarnings;
            data.MaxQuebecPensionPlanEarnings = MaxQuebecPensionPlanEarnings;
            data.CompensationAmount = CompensationAmount;
            data.WorkersCompensationMaximumAssessableEarnings = WorkersCompensationMaximumAssessableEarnings;
            data.WorkersCompensationEarnings = WorkersCompensationEarnings;


            if (Details == null)
                Details = null;
            else
                Details.CopyTo(data.Details);

            if (Transactions == null)
                Transactions = null;
            else
                Transactions.CopyTo(data.Transactions);

            //for exports
            data.EmployeeNumber = EmployeeNumber;
            data.StartDate = StartDate;
            data.CutoffDate = CutoffDate;
            data.ChequeAmount = ChequeAmount;
            data.EmploymentIncome14 = EmploymentIncome14;
            data.CanadaPensionPlan = CanadaPensionPlan;
            data.EmploymentInsurance = EmploymentInsurance;
            data.FederalTax = FederalTax;
            data.QuebecTax = QuebecTax;
            data.QuebecParentalInsurancePlan = QuebecParentalInsurancePlan;
            data.EmployerWorkersCompensationPremium = EmployerWorkersCompensationPremium;
            data.HowPaid = HowPaid;
            data.CodeCompanyCd = CodeCompanyCd;
            data.EmployerHealthTaxLevy = EmployerHealthTaxLevy;
            data.EmployerEmploymentInsurance = EmployerEmploymentInsurance;
            data.EmployerCanadaPensionPlan = EmployerCanadaPensionPlan;
            data.EmployerNumber = EmployerNumber;



        }
        public new void Clear()
        {
            base.Clear();

            PayrollMasterId = -1;
            HasCurrentFlag = true;
            PayrollProcessId = -1;
            BusinessNumberId = -1;
            CodeWsibCd = null;
            EmployeeId = -1;
            EmployeePositionId = -1;
            CodeProvinceStateCd = null;
            PeriodsPerYear = 0;
            BirthDate = null;
            FederalTaxClaim = 0;
            FederalDesignatedAreaDeduction = 0;
            FederalAdditionalTax = 0;
            FederalAuthorizedAnnualDeduction = 0;
            FederalAuthorizedAnnualTaxCredit = 0;
            EstimatedAnnualIncome = 0;
            EstimatedAnnualExpense = 0;
            ProvincialTaxClaim = 0;
            ProvincialAuthorizedAnnualDeduction = 0;
            ProvincialAuthorizedAnnualTaxCredit = 0;
            EmployerEmploymentInsuranceRate = 0;
            YtdPreviousCanadaPensionPlan = 0;
            YtdPreviousCanadaPensionPlanEarnings = 0;
            YtdPreviousEmploymentInsurance = 0;
            YtdPreviousEmployerEmploymentInsurance = 0;
            YtdPreviousQuebecParentalInsurancePlan = 0;
            YtdPreviousEmployerQuebecParentalInsurancePlan = 0;
            YtdPreviousBonusTaxableIncome = 0;
            YtdPreviousQuebecBonusTaxableIncome = 0;
            YtdPreviousLumpsumTaxableIncome = 0;
            YtdPreviousQuebecLumpsumTaxableIncome = 0;
            YtdPreviousQuebecHealthContribution = 0;
            YtdPreviousWorkersCompensationAssessable = 0;
            Period = 0;
            NumberOfPayPeriodsCovered = 1;
            ChequeDate = DateTime.Now;
            MaxQuebecPensionPlanEarnings = 0;
            MaxCanadaPensionPlanEarnings = 0;
            MaxQuebecParentalInsuranceEarnings = 0;
            CompensationAmount = 0;
            WorkersCompensationMaximumAssessableEarnings = 0;
            WorkersCompensationEarnings = 0;

            Transactions = new PayrollTransactionPayrollMasterCollection();

            Details = null;

            //for exports
            EmployeeNumber = null;
            StartDate = DateTime.Now;
            CutoffDate = DateTime.Now;
            ChequeAmount = 0;
            EmploymentIncome14 = 0;
            CanadaPensionPlan = 0;
            EmploymentInsurance = 0;
            FederalTax = 0;
            QuebecTax = 0;
            QuebecParentalInsurancePlan = 0;
            EmployerWorkersCompensationPremium = 0;
            HowPaid = 0;
            CodeCompanyCd = null;
            EmployerHealthTaxLevy = 0;
            EmployerEmploymentInsurance = 0;
            EmployerCanadaPensionPlan = 0;
            EmployerNumber = null;


        }
        #endregion


        #region main

        private Decimal GetEligibleAmount(Decimal amount, bool eligible)
        {
            return eligible ? amount : 0;
        }

        private Decimal GetValueBasedOnMax(Decimal currentAmount, Decimal? previousYTD, Decimal? max)
        {
            if (previousYTD > max)
                return 0;
            else if ((previousYTD + currentAmount) > max)
                return (Decimal)max - (Decimal)previousYTD;
            else
                return currentAmount;
        }

        /// <summary>
        /// build detail 
        /// </summary>
        public void CalculatePayrollMasterDetail(ref int id)
        {
            this.Details = new PayrollMasterDetailCollection();

            foreach (PayrollTransactionPayrollMaster transaction in this.Transactions)
            {
                PayrollMasterDetail detail = this.Details.GetPayrollMasterDetailByOrganizationUnit(transaction.OrganizationUnit);
                if (detail == null)
                {
                    detail = new PayrollMasterDetail(id--, this.PayrollMasterId, transaction.OrganizationUnit, this);
                    this.Details.Add(detail);
                }

                detail.TaxableIncome += transaction.TaxableIncome;
                this.TaxableIncome += transaction.TaxableIncome;
                detail.OverrideTaxIncome += transaction.OverrideTaxIncome;
                this.OverrideTaxIncome += transaction.OverrideTaxIncome;
                detail.QuebecTaxableIncome += transaction.QuebecTaxableIncome;
                this.QuebecTaxableIncome += transaction.QuebecTaxableIncome;
                detail.BonusTaxableIncome += transaction.BonusTaxableIncome;
                this.BonusTaxableIncome += transaction.BonusTaxableIncome;
                detail.QuebecBonusTaxableIncome += transaction.QuebecBonusTaxableIncome;
                this.QuebecBonusTaxableIncome += transaction.QuebecBonusTaxableIncome;
                detail.LumpsumTaxableIncome += transaction.LumpsumTaxableIncome;
                this.LumpsumTaxableIncome += transaction.LumpsumTaxableIncome;
                detail.QuebecLumpsumTaxableIncome += transaction.QuebecLumpsumTaxableIncome;
                this.QuebecLumpsumTaxableIncome += transaction.QuebecLumpsumTaxableIncome;
                detail.PensionTaxableIncome += transaction.PensionTaxableIncome;
                this.PensionTaxableIncome += transaction.PensionTaxableIncome;
                detail.QuebecPensionTaxableIncome += transaction.QuebecPensionTaxableIncome;
                this.QuebecPensionTaxableIncome += transaction.QuebecPensionTaxableIncome;
                detail.TaxableBenefit += transaction.TaxableBenefit;
                this.TaxableBenefit += transaction.TaxableBenefit;
                detail.QuebecTaxableBenefit += transaction.QuebecTaxableBenefit;
                this.QuebecTaxableBenefit += transaction.QuebecTaxableBenefit;
                detail.TaxableCashBenefit += transaction.TaxableCashBenefit;
                this.TaxableCashBenefit += transaction.TaxableCashBenefit;
                detail.QuebecTaxableCashBenefit += transaction.QuebecTaxableCashBenefit;
                this.QuebecTaxableCashBenefit += transaction.QuebecTaxableCashBenefit;
                detail.TaxableNonCashBenefit += transaction.TaxableNonCashBenefit;
                this.TaxableNonCashBenefit += transaction.TaxableNonCashBenefit;
                detail.QuebecTaxableNonCashBenefit += transaction.QuebecTaxableNonCashBenefit;
                this.QuebecTaxableNonCashBenefit += transaction.QuebecTaxableNonCashBenefit;
                detail.SeveranceTaxableIncome += transaction.SeveranceTaxableIncome;
                this.SeveranceTaxableIncome += transaction.SeveranceTaxableIncome;
                detail.QuebecSeveranceTaxableIncome += transaction.QuebecSeveranceTaxableIncome;
                this.QuebecSeveranceTaxableIncome += transaction.QuebecSeveranceTaxableIncome;
                detail.CommissionTaxableIncome += transaction.CommissionTaxableIncome;
                this.CommissionTaxableIncome += transaction.CommissionTaxableIncome;
                detail.QuebecCommissionTaxableIncome += transaction.QuebecCommissionTaxableIncome;
                this.QuebecCommissionTaxableIncome += transaction.QuebecCommissionTaxableIncome;

                detail.IncomeOnlyDeductionBeforeTax += transaction.IncomeOnlyDeductionBeforeTax;
                //                this.IncomeOnlyDeductionBeforeTax += transaction.IncomeOnlyDeductionBeforeTax;
                detail.BonusOnlyDeductionBeforeTax += transaction.BonusOnlyDeductionBeforeTax;
                //                this.BonusOnlyDeductionBeforeTax += transaction.BonusOnlyDeductionBeforeTax;
                detail.BonusIncomeBeforeTaxDeduction += transaction.BonusIncomeBeforeTaxDeduction;
                //                this.BonusIncomeBeforeTaxDeduction += transaction.BonusIncomeBeforeTaxDeduction;

                detail.CanadaPensionPlanEarnings += this.CodeProvinceStateCd == "QC" ? 0 : transaction.CanadaQuebecPensionPlanEarnings;
                this.CanadaPensionPlanEarnings += this.CodeProvinceStateCd == "QC" ? 0 : transaction.CanadaQuebecPensionPlanEarnings;
                detail.QuebecPensionPlanEarnings += this.CodeProvinceStateCd == "QC" ? transaction.CanadaQuebecPensionPlanEarnings : 0;
                this.QuebecPensionPlanEarnings += this.CodeProvinceStateCd == "QC" ? transaction.CanadaQuebecPensionPlanEarnings : 0;
                detail.QuebecParentalInsurancePlanEarnings += transaction.ProvincialParentalInsurancePlanEarnings;
                this.QuebecParentalInsurancePlanEarnings += transaction.ProvincialParentalInsurancePlanEarnings;
                detail.EmploymentInsuranceEarnings += transaction.EmploymentInsuranceEarnings;
                this.EmploymentInsuranceEarnings += transaction.EmploymentInsuranceEarnings;
                detail.WorkersCompensationEarnings += transaction.WorkersCompensationEarnings;
                this.WorkersCompensationEarnings += transaction.WorkersCompensationEarnings;
                detail.EmployerHealthTaxEarnings += transaction.EmployerHealthTaxEarnings;
                this.EmployerHealthTaxEarnings += transaction.EmployerHealthTaxEarnings;
            }
        }

        /*****KEEP THIS FOR LATER******
        private decimal _i =  long.MinValue;

        #region move to database
        const decimal DeductionForEmploymentIncomeFactor = 0.06M;  //MOVE TO DATABASE
        const decimal DeductionForEmploymentIncomeMaxYear = 1140; //MOVE TO DATABASE
        private decimal T
        {
            get
            {
                if (I < 42705)
                    return .16M;
                else if (I < 85405)
                    return .20M;
                else if (I < 103915)
                    return .24M;
                else
                    return 0.2575M;
            }
        }
        private decimal K
        {
            get
            {
                if (I < 42705)
                    return 0;
                else if (I < 85405)
                    return 1708;
                else if (I < 103915)
                    return 5124;
                else
                    return 6942;
            }
        }
        #endregion

        private decimal P {get { return PeriodsPerYear; } }//periods per year
        private decimal G {get { return QuebecTaxableIncome+QuebecTaxableBenefit; }}//gross remuneration
        private decimal F {get { return BeforeIncomeTaxDeduction; }}//contributions/deductions
        private decimal H //deduction of employment income
        {
            get
            {
                decimal calc = QuebecTaxableIncome * DeductionForEmploymentIncomeFactor;
                decimal max = Math.Round(DeductionForEmploymentIncomeMaxYear / PeriodsPerYear, 2,MidpointRounding.AwayFromZero);
                return calc > max ? max : calc;
            }
        }
        private decimal J { get { return 0; } } //need AARON
        private decimal J1 { get { return 0; } } //need AARON
        private decimal K1 { get { return 0; } } //Non-refundable tax credits need AARON

        private decimal E1 { get { return ProvincialTaxClaim; } } //need AARON
        private decimal E2 { get { return 0; } } //need AARON
        public decimal E { get { return Math.Round((E1+E2)*2,0, MidpointRounding.AwayFromZero)/2; } } //*2/2 is for multiple of 5
        public decimal Q { get { return 0; } } //need AARON
        public decimal Q1 { get { return 0; } } //need AARON
        public decimal L { get { return ProvincialAdditionalTax; } }

        public decimal I //annual income 
        {
            //P × (G – F – H) – J – J1
            get
            {
                if (_i == long.MinValue)
                    _i=P * (G - F - H) - J - J1; 
                return _i;
            }
        }

        public decimal Y //income tax for year
        {
            get
            {
                //(T × I) – K – K1 – (0.20 × E) – (0.15 × P × Q) – (0.20 × P × Q1)
                return (T * I) - K - K1 - (E * 0.20M) - (0.15M*P*Q) - (0.20M*P*Q1) ;
            }
        }

        public decimal A //income tax result
        {
            get
            {
                //(Y / P) + L

                decimal calc = Math.Round(Y / P, 2, MidpointRounding.AwayFromZero) + L;
                return calc < 0 ? 0 : calc;
            }
        }
        */
        #endregion
    }
}