﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class RemittanceDetailGarnishmentDeductionsCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<RemittanceDetailGarnishmentDeductions>
    {
    }
}
