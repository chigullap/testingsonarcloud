﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeeProfileCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeeProfile>
    {
    }
}