﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeWsibHealthAndSafetyReport : Address
    {
        #region properties
        [DataMember]
        public long EmployeeWsibHealthAndSafetyReportId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public long NameOfEmployer { get; set; }

        [DataMember]
        public String NatureOfIndustry { get; set; }

        [DataMember]
        public String Branch { get; set; }

        [DataMember]
        public DateTime IncidentDate { get; set; }

        [DataMember]
        public TimeSpan HourStartedWork { get; set; }

        [DataMember]
        public String CauseOfIncident { get; set; }

        [DataMember]
        public String CausedByMachinery { get; set; }

        [DataMember]
        public String MachineAndPartName { get; set; }

        [DataMember]
        public String MachineMovedByMechanicalPower { get; set; }

        [DataMember]
        public String Injuries { get; set; }

        [DataMember]
        public String DisabledFromEarningFullWages { get; set; }

        [DataMember]
        public DateTime DateSigned { get; set; }
        #endregion

        #region construct/destruct
        public EmployeeWsibHealthAndSafetyReport()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeeWsibHealthAndSafetyReport data)
        {
            base.CopyTo(data);

            data.EmployeeWsibHealthAndSafetyReportId = EmployeeWsibHealthAndSafetyReportId;
            data.EmployeeId = EmployeeId;
            data.NameOfEmployer = NameOfEmployer;
            data.NatureOfIndustry = NatureOfIndustry;
            data.Branch = Branch;
            data.IncidentDate = IncidentDate;
            data.HourStartedWork = HourStartedWork;
            data.CauseOfIncident = CauseOfIncident;
            data.CausedByMachinery = CausedByMachinery;
            data.MachineAndPartName = MachineAndPartName;
            data.MachineMovedByMechanicalPower = MachineMovedByMechanicalPower;
            data.Injuries = Injuries;
            data.DisabledFromEarningFullWages = DisabledFromEarningFullWages;
            data.DateSigned = DateSigned;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeeWsibHealthAndSafetyReportId = -1;
            EmployeeId = -1;
            NameOfEmployer = -1;
            NatureOfIndustry = null;
            Branch = null;
            IncidentDate = DateTime.Now;
            HourStartedWork = TimeSpan.Zero;
            CauseOfIncident = null;
            CausedByMachinery = null;
            MachineAndPartName = null;
            MachineMovedByMechanicalPower = null;
            Injuries = null;
            DisabledFromEarningFullWages = null;
            DateSigned = DateTime.Now;
        }
        #endregion
    }
}