﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PensionBasicContribution : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PensionBasicContributionId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public Decimal EffectiveYear { get; set; }
        [DataMember]
        public Decimal BasicAmountPercentage { get; set; }
       
        #endregion

        #region construct/destruct
        public PensionBasicContribution()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PensionBasicContribution data)
        {
            base.CopyTo(data);

            data.PensionBasicContributionId = PensionBasicContributionId;
            data.EffectiveYear = EffectiveYear;
            data.BasicAmountPercentage = BasicAmountPercentage;
        }
        public new void Clear()
        {
            base.Clear();

            PensionBasicContributionId = -1;
            EffectiveYear = 0;
            BasicAmountPercentage = 0;
        }

        #endregion

    }
}
