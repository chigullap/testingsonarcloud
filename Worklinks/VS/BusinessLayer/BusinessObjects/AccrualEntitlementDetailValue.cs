﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class AccrualEntitlementDetailValue : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long AccrualEntitlementDetailValueId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long AccrualEntitlementDetailId { get; set; }

        [DataMember]
        public int? EffectiveMonth { get; set; }

        [DataMember]
        public Decimal? AmountPercentage { get; set; }
        #endregion

        #region construct/destruct
        public AccrualEntitlementDetailValue()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(AccrualEntitlementDetailValue data)
        {
            base.CopyTo(data);

            data.AccrualEntitlementDetailValueId = AccrualEntitlementDetailValueId;
            data.AccrualEntitlementDetailId = AccrualEntitlementDetailId;
            data.EffectiveMonth = EffectiveMonth;
            data.AmountPercentage = AmountPercentage;
        }
        public new void Clear()
        {
            base.Clear();

            AccrualEntitlementDetailValueId = -1;
            AccrualEntitlementDetailId = -1;
            EffectiveMonth = null;
            AmountPercentage = null;
        }
        #endregion
    }
}