﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeWsibHealthAndSafetyReportPerson : Person
    {
        #region properties
        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public String AddressLine1 { get; set; }

        [DataMember]
        public String AddressLine2 { get; set; }

        [DataMember]
        public String City { get; set; }

        [DataMember]
        public String PostalZipCode { get; set; }

        [DataMember]
        public String ProvinceStateCode { get; set; }

        [DataMember]
        public String CountryCode { get; set; }

        [DataMember]
        public String Occupation { get; set; }

        public String OtherNames
        {
            get
            {
                String otherNames = null;

                if (MiddleName != null)
                    otherNames = String.Format("{0}, {1}", FirstName, MiddleName);
                else
                    otherNames = FirstName;

                return otherNames;
            }
        }
        #endregion

        #region construct/destruct
        public EmployeeWsibHealthAndSafetyReportPerson()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public virtual void CopyTo(EmployeeWsibHealthAndSafetyReportPerson data)
        {
            base.CopyTo(data);

            data.EmployeeId = EmployeeId;
            data.LastName = LastName;
            data.KnownAsName = KnownAsName;
            data.GenderCode = GenderCode;
            data.BirthDate = BirthDate;
            data.AddressLine1 = AddressLine1;
            data.AddressLine2 = AddressLine2;
            data.City = City;
            data.PostalZipCode = PostalZipCode;
            data.ProvinceStateCode = ProvinceStateCode;
            data.CountryCode = CountryCode;
            data.Occupation = Occupation;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeeId = -1;
            LastName = null;
            KnownAsName = null;
            GenderCode = null;
            BirthDate = DateTime.Now;
            AddressLine1 = null;
            AddressLine2 = null;
            City = null;
            PostalZipCode = null;
            ProvinceStateCode = null;
            CountryCode = null;
            Occupation = null;
        }
        #endregion
    }
}