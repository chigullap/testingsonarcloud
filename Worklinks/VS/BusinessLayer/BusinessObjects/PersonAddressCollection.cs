﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PersonAddressCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PersonAddress>
    {
        #region properties
        /// <summary>
        /// This property iterates through the collection and returns the primary address.
        /// If no addres is found, the method returns null.
        /// </summary>
        public PersonAddress PrimaryAddress
        {
            get
            {
                foreach (PersonAddress address in this)
                {
                    if (address.PrimaryFlag)
                        return address;
                }

                return null;
            }
        }
        /// <summary>
        /// This property determines whether or not a "HOME" address type exists in the collection.
        /// </summary>
        public bool HomeAddressExists
        {
            get
            {
                return GetAddressByType("HOME") != null;
            }
        }
        #endregion

        /// <summary>
        /// This method iterates through the collection and returns an address with the specified type.
        /// If no addres is found, the method returns null.
        /// </summary>
        /// <param name="type"></param>
        public PersonAddress GetAddressByType(String type)
        {
            foreach (PersonAddress address in this)
            {
                if (address.PersonAddressTypeCode.Equals(type))
                    return address;
            }

            return null;
        }
    }
}