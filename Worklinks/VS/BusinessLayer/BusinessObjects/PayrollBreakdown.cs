﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollBreakdown : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PayrollBreakdownId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public Decimal GrossPay { get; set; }

        [DataMember]
        public Decimal NetPay { get; set; }

        [DataMember]
        public Decimal Taxes { get; set; }

        [DataMember]
        public Decimal Deductions { get; set; }

        #region calcs
        public Decimal NetPayPercentage
        {
            get { return GrossPay==0?0:(NetPay / GrossPay * 100); }
        }
        public Decimal TaxesPercentage
        {
            get { return GrossPay==0?0:(Taxes / GrossPay * 100); }
        }
        public Decimal DeductionsPercentage
        {
            get { return GrossPay == 0 ? 0 : (Deductions / GrossPay * 100); }
        }
        #endregion
        #endregion

        #region construct/destruct
        public PayrollBreakdown()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PayrollBreakdown data)
        {
            base.CopyTo(data);

            data.PayrollBreakdownId = PayrollBreakdownId;
            data.Description = Description;
            data.GrossPay = GrossPay;
            data.NetPay = NetPay;
            data.Taxes = Taxes;
            data.Deductions = Deductions;
        }
        public new void Clear()
        {
            base.Clear();

            PayrollBreakdownId = -1;
            Description = null;
            GrossPay = 0;
            NetPay = 0;
            Taxes = 0;
            Deductions = 0;
        }
        #endregion
    }
}