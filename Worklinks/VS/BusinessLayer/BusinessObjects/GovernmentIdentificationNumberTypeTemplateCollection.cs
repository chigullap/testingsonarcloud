﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class GovernmentIdentificationNumberTypeTemplateCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<GovernmentIdentificationNumberTypeTemplate>
    {
    }
}