﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeePaycodeProcessed : EmployeePaycode
    {
        #region properties
        [DataMember]
        public long EmployeePaycodeProcessedId { get; set; }

        [DataMember]
        public long PayrollProcessId { get; set; }

        [DataMember]
        public String OriginalCreateUser { get; set; }

        [DataMember]
        public DateTime? OriginalCreateDatetime { get; set; }

        [DataMember]
        public String OriginalUpdateUser { get; set; }

        [DataMember]
        public DateTime? OriginalUpdateDatetime { get; set; }
        #endregion

        #region construct/destruct
        public EmployeePaycodeProcessed()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeePaycodeProcessed data)
        {
            base.CopyTo(data);

            data.EmployeePaycodeProcessedId = EmployeePaycodeProcessedId;
            data.PayrollProcessId = PayrollProcessId;
            data.OriginalCreateUser = OriginalCreateUser;
            data.OriginalCreateDatetime = OriginalCreateDatetime;
            data.OriginalUpdateUser = OriginalUpdateUser;
            data.OriginalUpdateDatetime = OriginalUpdateDatetime;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeePaycodeProcessedId = -1;
            PayrollProcessId = -1;
            OriginalCreateUser = "";
            OriginalCreateDatetime = null;
            OriginalUpdateUser = "";
            OriginalUpdateDatetime = null;
        }
        #endregion
    }
}