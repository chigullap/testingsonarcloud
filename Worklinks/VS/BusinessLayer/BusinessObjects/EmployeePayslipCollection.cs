﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeePayslipCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeePayslip>
    {
    }
}