﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SalaryPlanGradeStep : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long SalaryPlanGradeStepId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long SalaryPlanGradeId { get; set; }

        [DataMember]
        public String Name { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public String EnglishDescription { get; set; }

        [DataMember]
        public String FrenchDescription { get; set; }
        #endregion

        #region construct/destruct
        public SalaryPlanGradeStep()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(SalaryPlanGradeStep data)
        {
            base.CopyTo(data);

            data.SalaryPlanGradeStepId = SalaryPlanGradeStepId;
            data.SalaryPlanGradeId = SalaryPlanGradeId;
            data.Name = Name;
            data.ActiveFlag = ActiveFlag;
            data.EnglishDescription = EnglishDescription;
            data.FrenchDescription = FrenchDescription;
        }
        public new void Clear()
        {
            base.Clear();

            SalaryPlanGradeStepId = -1;
            SalaryPlanGradeId = -1;
            Name = null;
            ActiveFlag = false;
            EnglishDescription = null;
            FrenchDescription = null;
        }
        #endregion
    }
}