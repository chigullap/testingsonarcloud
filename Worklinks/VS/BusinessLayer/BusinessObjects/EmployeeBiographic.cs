﻿using System;
using System.Runtime.Serialization;
using FileHelpers;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeBiographic : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        [DataMember]
        public string EmployeeNumber { get; set; }

        [DataMember]
        public string TitleCode { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string KnownAsName { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public int? Age
        {
            get
            {
                if (BirthDate == null) return null;
                int now = int.Parse(DateTime.Now.ToString("yyyyMMdd"));
                int dob = int.Parse(BirthDate.Value.ToString("yyyyMMdd"));
                return  (now - dob) / 10000;
            }
        }

        [DataMember]
        public string BadgeNumber { get; set; }

        [DataMember]
        public string AlternateEmployeeNumber { get; set; }

        [DataMember]
        public string LanguageCode { get; set; }

        [DataMember]
        public string GenderCode { get; set; }

        [DataMember]
        public string SocialInsuranceNumber { get; set; }

        [DataMember]
        public string MaritalStatusCode { get; set; }

        [DataMember]
        public string BilingualismCode { get; set; }

        [DataMember]
        public string HealthCareNumber { get; set; }

        [DataMember]
        public string HealthCareProvinceStateCode { get; set; }

        [DataMember]
        public string CitizenshipCode { get; set; }

        [DataMember]
        public string GovernmentIdentificationNumberTypeCode { get; set; }

        [DataMember]
        public string GovernmentIdentificationNumber1 { get; set; }

        [DataMember]
        public string GovernmentIdentificationNumber2 { get; set; }

        [DataMember]
        public string GovernmentIdentificationNumber3 { get; set; }

        /// <summary>Creates a new object that is a copy of the current instance.</summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public override object Clone()
        {
            return new EmployeeBiographic
            {
                EmployeeNumber = EmployeeNumber,
                TitleCode = TitleCode,
                LastName = LastName,
                MiddleName = MiddleName,
                FirstName = FirstName,
                KnownAsName = KnownAsName,
                BirthDate = BirthDate,
                LanguageCode = LanguageCode,
                GenderCode = GenderCode,
                SocialInsuranceNumber = SocialInsuranceNumber,
                MaritalStatusCode = MaritalStatusCode,
                BilingualismCode = BilingualismCode,
                HealthCareNumber = HealthCareNumber,
                HealthCareProvinceStateCode = HealthCareProvinceStateCode,
                CitizenshipCode = CitizenshipCode,
                GovernmentIdentificationNumberTypeCode = GovernmentIdentificationNumberTypeCode,
                GovernmentIdentificationNumber1 = GovernmentIdentificationNumber1,
                GovernmentIdentificationNumber2 = GovernmentIdentificationNumber2,
                GovernmentIdentificationNumber3 = GovernmentIdentificationNumber3,
                AlternateEmployeeNumber = AlternateEmployeeNumber,
                BadgeNumber = BadgeNumber
            };
        }

        public EmployeeBiographicFileItem ToFileItem()
        {
            return new EmployeeBiographicFileItem
            {
                EmployeeNumber = EmployeeNumber,
                TitleCode = TitleCode,
                LastName = LastName,
                MiddleName = MiddleName,
                FirstName = FirstName,
                KnownAsName = KnownAsName,
                BirthDate = BirthDate,
                LanguageCode = LanguageCode,
                GenderCode = GenderCode,
                SocialInsuranceNumber = SocialInsuranceNumber,
                MaritalStatusCode = MaritalStatusCode,
                BilingualismCode = BilingualismCode,
                HealthCareNumber = HealthCareNumber,
                HealthCareProvinceStateCode = HealthCareProvinceStateCode,
                CitizenshipCode = CitizenshipCode,
                IdentificationNumberType = GovernmentIdentificationNumberTypeCode,
                GovernmentIdentificationNumber1 = GovernmentIdentificationNumber1,
                GovernmentIdentificationNumber2 = GovernmentIdentificationNumber2,
                GovernmentIdentificationNumber3 = GovernmentIdentificationNumber3,
                BadgeNumber = BadgeNumber,
                AltEmployeeNumber = AlternateEmployeeNumber,
                Age = Age.GetValueOrDefault()
            };
        }
    }

    [DelimitedRecord(",")]
    public class EmployeeBiographicsFileHeader
    {
        public EmployeeBiographicsFileHeader()
        {
            EmployeeNumber = "EmployeeNumber";
            TitleCode = "TitleCode";
            LastName = "LastName";
            MiddleName = "MiddleName";
            FirstName = "FirstName";
            KnownAsName = "KnownAsName";
            BirthDate = "BirthDate";
            LanguageCode = "LanguageCode";
            GenderCode = "GenderCode";
            SocialInsuranceNumber = "SocialInsuranceNumber";
            MaritalStatusCode = "MaritalStatusCode";
            BilingualismCode = "BilingualismCode";
            HealthCareNumber = "HealthCareNumber";
            HealthCareProvinceStateCode = "HealthCareProvinceStateCode";
            CitizenshipCode = "CitizenshipCode";
            IdentificationNumberType = "GovernmentIdentificationNumberTypeCode";
            GovernmentIdentificationNumber1 = "GovernmentIdentificationNumber1";
            GovernmentIdentificationNumber2 = "GovernmentIdentificationNumber2";
            GovernmentIdentificationNumber3 = "GovernmentIdentificationNumber3";
            Age = "Age";
            BadgeNumber = "BadgeNumber";
            AlternateEmployeeNumber = "AltEmployeeNumber";
        }

        public string EmployeeNumber;
        public string AlternateEmployeeNumber;
        public string TitleCode;
        public string LastName;
        public string MiddleName;
        public string FirstName;
        public string KnownAsName;
        public string BirthDate;
        public string Age;
        public string LanguageCode;
        public string GenderCode;
        public string SocialInsuranceNumber;
        public string MaritalStatusCode;
        public string BilingualismCode;
        public string HealthCareNumber;
        public string HealthCareProvinceStateCode;
        public string CitizenshipCode;
        public string IdentificationNumberType;
        public string GovernmentIdentificationNumber1;
        public string GovernmentIdentificationNumber2;
        public string GovernmentIdentificationNumber3;
        public string BadgeNumber;
    }

    [DelimitedRecord(",")]
    public class EmployeeBiographicFileItem
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string EmployeeNumber { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string AltEmployeeNumber { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string TitleCode { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LastName { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string MiddleName { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstName { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string KnownAsName { get; set; }

        [FieldConverter(ConverterKind.Date, "MM/dd/yyyy")]
        public DateTime? BirthDate { get; set; }

        public int Age { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LanguageCode { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string GenderCode { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SocialInsuranceNumber { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string MaritalStatusCode { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string BilingualismCode { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string HealthCareNumber { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string HealthCareProvinceStateCode { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CitizenshipCode { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string IdentificationNumberType { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string GovernmentIdentificationNumber1 { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string GovernmentIdentificationNumber2 { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string GovernmentIdentificationNumber3 { get; set; }

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string BadgeNumber { get; set; }

    }
}