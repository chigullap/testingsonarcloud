﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class RbcEftEdi : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PayrollMasterPaymentId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String CodeCompanyCd { get; set; }

        [DataMember]
        public String EmployeeNumber { get; set; }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public String PayrollProcessId { get; set; } 

        [DataMember]
        public String CodeEmployeeBankingSequenceCode { get; set; }

        [DataMember]
        public String EmployeeName { get; set; }

        [DataMember]
        public String BankInstCode { get; set; }

        [DataMember]
        public String BankTransactionNumber { get; set; }

        [DataMember]
        public String EmployeeBankAccountNumber { get; set; }

        [DataMember]
        public Decimal ChequeAmount { get; set; }

        [DataMember]
        public DateTime ChequeDate { get; set; }

        [DataMember]
        public bool DirectDepositFlag { get; set; }

        [DataMember]
        public long? RbcSequenceNumber { get; set; }

        [DataMember]
        public String EmployeeAddressLine1 { get; set; }

        [DataMember]
        public String EmployeeAddressLine2 { get; set; }

        [DataMember]
        public String EmployeeCity { get; set; }

        [DataMember]
        public String EmployeePostalZipCode { get; set; }

        [DataMember]
        public String EmployeeProvinceStateCode { get; set; }

        [DataMember]
        public String EmployeeCountryCode { get; set; }

        [DataMember]
        public String EmployerAddressLine1 { get; set; }

        [DataMember]
        public String EmployerAddressLine2 { get; set; }

        [DataMember]
        public String EmployerCity { get; set; }

        [DataMember]
        public String EmployerPostalZipCode { get; set; }

        [DataMember]
        public String EmployerProvinceStateCode { get; set; }

        [DataMember]
        public String EmployerCountryCode { get; set; }

        [DataMember]
        public string CodeStatusCd { get; set; }

        [DataMember]
        public string CodeBankAccountTypeCd { get; set; }

        [DataMember]
        public string CodeBankingCountryCd { get; set; }

        [DataMember]
        public bool ManualChequeFlag { get; set; }

        #endregion

        #region construct/destruct
        public RbcEftEdi()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(RbcEftEdi data)
        {
            base.CopyTo(data);

            data.PayrollMasterPaymentId = PayrollMasterPaymentId;
            data.CodeCompanyCd = CodeCompanyCd;
            data.EmployeeNumber = EmployeeNumber;
            data.EmployeeId = EmployeeId;
            data.PayrollProcessId = PayrollProcessId;
            data.CodeEmployeeBankingSequenceCode = CodeEmployeeBankingSequenceCode;
            data.EmployeeName = EmployeeName;
            data.BankInstCode = BankInstCode;
            data.BankTransactionNumber = BankTransactionNumber;
            data.EmployeeBankAccountNumber = EmployeeBankAccountNumber;
            data.ChequeAmount = ChequeAmount;
            data.ChequeDate = ChequeDate;
            data.DirectDepositFlag = DirectDepositFlag;
            data.RbcSequenceNumber = RbcSequenceNumber;
            data.EmployeeAddressLine1 = EmployeeAddressLine1;
            data.EmployeeAddressLine2 = EmployeeAddressLine2;
            data.EmployeeCity = EmployeeCity;
            data.EmployeePostalZipCode = EmployeePostalZipCode;
            data.EmployeeProvinceStateCode = EmployeeProvinceStateCode;
            data.EmployeeCountryCode = EmployeeCountryCode;
            data.EmployerAddressLine1 = EmployerAddressLine1;
            data.EmployerAddressLine2 = EmployerAddressLine2;
            data.EmployerCity = EmployerCity;
            data.EmployerPostalZipCode = EmployerPostalZipCode;
            data.EmployerProvinceStateCode = EmployerProvinceStateCode;
            data.EmployerCountryCode = EmployerCountryCode;
            data.CodeStatusCd = CodeStatusCd;
            data.CodeBankAccountTypeCd = CodeBankAccountTypeCd;
            data.CodeBankingCountryCd = CodeBankingCountryCd;
            data.ManualChequeFlag = ManualChequeFlag;
        }

        public new void Clear()
        {
            base.Clear();

            PayrollMasterPaymentId = -1;
            CodeCompanyCd = "";
            EmployeeNumber = "";
            EmployeeId = -1;
            PayrollProcessId = "";
            CodeEmployeeBankingSequenceCode = null;
            EmployeeName = "";
            BankInstCode = "";
            BankTransactionNumber = "";
            EmployeeBankAccountNumber = "";
            ChequeAmount = 0;
            ChequeDate = DateTime.Now;
            DirectDepositFlag = true;
            RbcSequenceNumber = null;
            EmployeeAddressLine1 = null;
            EmployeeAddressLine2 = null;
            EmployeeCity = null;
            EmployeePostalZipCode = null;
            EmployeeProvinceStateCode = null;
            EmployeeCountryCode = null;
            EmployerAddressLine1 = null;
            EmployerAddressLine2 = null;
            EmployerCity = null;
            EmployerPostalZipCode = null;
            EmployerProvinceStateCode = null;
            EmployerCountryCode = null;
            CodeStatusCd = "";
            CodeBankAccountTypeCd = null;
            CodeBankingCountryCd = null;
            ManualChequeFlag = false;
        }
        #endregion
    }
}
