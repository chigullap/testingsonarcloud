﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class BusinessNumber : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long BusinessNumberId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string EmployerNumber { get; set; }

        [DataMember]
        public string EmployerName { get; set; }

        [DataMember]
        public Decimal Rate { get; set; }

        [DataMember]
        public long EmployerAddressId { get; set; }

        [DataMember]
        public string BusinessTaxNumber { get; set; }

        [DataMember]
        public string RevenuQuebecBusinessTaxNumber { get; set; }

        [DataMember]
        public string CodeCraRemittancePeriodCd { get; set; }

        [DataMember]
        public string CodePreAuthorizedBankCd { get; set; }

        [DataMember]
        public string PreAuthorizedTransitNumber { get; set; }

        [DataMember]
        public string PreAuthorizedAccountNumber { get; set; }

        [DataMember]
        public string AbaNumber { get; set; }

        [DataMember]
        public string CodeRevenuQuebecRemittancePeriodCd { get; set; }

        public string LongDescription
        {
            get { return string.Format("{0}/{1}", Description, Rate); }
        }
        #endregion

        #region construct/destruct
        public BusinessNumber()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(BusinessNumber data)
        {
            base.CopyTo(data);

            data.BusinessNumberId = BusinessNumberId;
            data.Description = Description;
            data.EmployerNumber = EmployerNumber;
            data.EmployerName = EmployerName;
            data.Rate = Rate;
            data.EmployerAddressId = EmployerAddressId;
            data.BusinessTaxNumber = BusinessTaxNumber;
            data.RevenuQuebecBusinessTaxNumber = RevenuQuebecBusinessTaxNumber;
            data.CodeCraRemittancePeriodCd = CodeCraRemittancePeriodCd;
            data.CodePreAuthorizedBankCd = CodePreAuthorizedBankCd;
            data.PreAuthorizedTransitNumber = PreAuthorizedTransitNumber;
            data.PreAuthorizedAccountNumber = PreAuthorizedAccountNumber;
            data.AbaNumber = AbaNumber;
            data.CodeRevenuQuebecRemittancePeriodCd = CodeRevenuQuebecRemittancePeriodCd;
        }
        public new void Clear()
        {
            base.Clear();

            BusinessNumberId = -1;
            Description = null;
            EmployerNumber = null;
            EmployerName = null;
            Rate = 0;
            EmployerAddressId = -1;
            BusinessTaxNumber = null;
            RevenuQuebecBusinessTaxNumber = null;
            CodeCraRemittancePeriodCd = null;
            CodePreAuthorizedBankCd = null;
            PreAuthorizedTransitNumber = null;
            PreAuthorizedAccountNumber = null;
            AbaNumber = null;
            CodeRevenuQuebecRemittancePeriodCd = null;
        }
        #endregion
    }
}