﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EngineTestInput : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EngineTestInputId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long EmployeeId { get; set; }
        [DataMember]
        public String TaxableCodeProvinceStateCode { get; set; }
        [DataMember]
        public String ProvinceNumber { get; set; }
        [DataMember]
        public Decimal Regular { get; set; }
        [DataMember]
        public Decimal CppQppWages { get; set; }
        [DataMember]
        public Decimal CppQppDeducted { get; set; }
        [DataMember]
        public Decimal EiWages { get; set; }
        [DataMember]
        public Decimal EiDeducted { get; set; }
        [DataMember]
        public Decimal PpipWages { get; set; }
        [DataMember]
        public Decimal PpipDeducted { get; set; }
        [DataMember]
        public Decimal RegularQc { get; set; }
        [DataMember]
        public Decimal TaxableBenefits { get; set; }
        [DataMember]
        public Decimal BeforeTaxDeductions { get; set; }
        [DataMember]
        public Decimal CanBonus { get; set; }
        [DataMember]
        public Decimal RetroactiveIncome { get; set; }
        [DataMember]
        public Decimal CanLumpsum { get; set; }
        [DataMember]
        public Decimal CanBonusQc { get; set; }
        [DataMember]
        public Decimal CanLumpsumQc { get; set; }
        [DataMember]
        public Decimal TaxDeducted { get; set; }
        [DataMember]
        public Decimal TaxQcDeducted { get; set; }
        [DataMember]
        public Decimal TotalClaimAmountFederal { get; set; }
        [DataMember]
        public Decimal DesignatedAreaDeduction { get; set; }
        [DataMember]
        public Decimal AuthorizedAnnualDeduction { get; set; }
        [DataMember]
        public Decimal FederalAdditionalTax { get; set; }
        [DataMember]
        public int TotalClaimAmountProvincial { get; set; }
        [DataMember]
        public Decimal ProvincialAdditionalTax { get; set; }
        [DataMember]
        public int ProvincialAuthorizedAnnualDeduction { get; set; }
        [DataMember]
        public int TaxCreditProvincial { get; set; }
        [DataMember]
        public int? ProvincialDesignatedAreaDeduction { get; set; }
        [DataMember]
        public Decimal Lcf { get; set; }
        [DataMember]
        public Decimal Lcp { get; set; }
        [DataMember]
        public Decimal NorthwestTerritoriesTaxCalculationBase { get; set; }
        [DataMember]
        public Decimal NorthwestTerritoriesTaxAssessed { get; set; }
        [DataMember]
        public Decimal NunavutTaxCalculationBase { get; set; }
        [DataMember]
        public Decimal NunavutTaxAssessed { get; set; }
        [DataMember]
        public Decimal EmployerEmploymentInsuranceAmount { get; set; }
        [DataMember]
        public Decimal EmployerCanadaPensionPlanAmount { get; set; }
        [DataMember]
        public Decimal EmployerProvincialParentalAmount { get; set; }
        [DataMember]
        public Decimal YtdCppQppDeducted { get; set; }
        [DataMember]
        public Decimal YtdEiDeducted { get; set; }
        [DataMember]
        public Decimal YtdPpipDeducted { get; set; }
        [DataMember]
        public Decimal CppQppWagesCalculated { get; set;}
        [DataMember]
        public Decimal EiWagesCalculated { get; set; }
        [DataMember]
        public Decimal PpipWagesCalculated { get; set; }

        #endregion

        #region construct/destruct
        public EngineTestInput()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(EngineTestInput data)
        {
            base.CopyTo(data);

            data.EngineTestInputId = EngineTestInputId;
            data.EmployeeId = EmployeeId;
            data.TaxableCodeProvinceStateCode = TaxableCodeProvinceStateCode;
            data.ProvinceNumber = ProvinceNumber;
            data.Regular = Regular;
            data.CppQppWages = CppQppWages;
            data.CppQppDeducted = CppQppDeducted;
            data.EiWages = EiWages;
            data.EiDeducted = EiDeducted;
            data.PpipWages = PpipWages;
            data.PpipDeducted = PpipDeducted;
            data.RegularQc = RegularQc;
            data.TaxableBenefits = TaxableBenefits;
            data.BeforeTaxDeductions = BeforeTaxDeductions;
            data.CanBonus = CanBonus;
            data.RetroactiveIncome = RetroactiveIncome;
            data.CanLumpsum = CanLumpsum;
            data.CanBonusQc = CanBonusQc;
            data.CanLumpsumQc = CanLumpsumQc;
            data.TaxDeducted = TaxDeducted;
            data.TaxQcDeducted = TaxQcDeducted;
            data.TotalClaimAmountFederal = TotalClaimAmountFederal;
            data.DesignatedAreaDeduction = DesignatedAreaDeduction;
            data.AuthorizedAnnualDeduction = AuthorizedAnnualDeduction;
            data.FederalAdditionalTax = FederalAdditionalTax;
            data.TotalClaimAmountProvincial = TotalClaimAmountProvincial;
            data.ProvincialAdditionalTax = ProvincialAdditionalTax;
            data.ProvincialAuthorizedAnnualDeduction = ProvincialAuthorizedAnnualDeduction;
            data.TaxCreditProvincial = TaxCreditProvincial;
            data.ProvincialDesignatedAreaDeduction = ProvincialDesignatedAreaDeduction;
            data.Lcf = Lcf;
            data.Lcp = Lcp;
            data.NorthwestTerritoriesTaxCalculationBase = NorthwestTerritoriesTaxCalculationBase;
            data.NorthwestTerritoriesTaxAssessed = NorthwestTerritoriesTaxAssessed;
            data.NunavutTaxCalculationBase = NunavutTaxCalculationBase;
            data.NunavutTaxAssessed = NunavutTaxAssessed;
            data.EmployerEmploymentInsuranceAmount = EmployerEmploymentInsuranceAmount;
            data.EmployerCanadaPensionPlanAmount = EmployerCanadaPensionPlanAmount;
            data.EmployerProvincialParentalAmount = EmployerProvincialParentalAmount;
            data.YtdCppQppDeducted = YtdCppQppDeducted;
            data.YtdEiDeducted = YtdEiDeducted;
            data.YtdPpipDeducted = YtdPpipDeducted;
            data.CppQppWagesCalculated = CppQppWagesCalculated;
            data.EiWagesCalculated = EiWagesCalculated;
            data.PpipWagesCalculated = PpipWagesCalculated;
        }

        public new void Clear()
        {
            base.Clear();

            EngineTestInputId = -1;
            EmployeeId = -1;
            TaxableCodeProvinceStateCode = null;
            ProvinceNumber = null;
            Regular = 0;
            CppQppWages = 0;
            CppQppDeducted = 0;
            EiWages = 0;
            EiDeducted = 0;
            PpipWages = 0;
            PpipDeducted = 0;
            RegularQc = 0;
            TaxableBenefits = 0;
            BeforeTaxDeductions = 0;
            CanBonus = 0;
            RetroactiveIncome = 0;
            CanLumpsum = 0;
            CanBonusQc = 0;
            CanLumpsumQc = 0;
            TaxDeducted = 0;
            TaxQcDeducted = 0;
            TotalClaimAmountFederal = 0;
            DesignatedAreaDeduction = 0;
            AuthorizedAnnualDeduction = 0;
            FederalAdditionalTax = 0;
            TotalClaimAmountProvincial = 0;
            ProvincialAdditionalTax = 0;
            ProvincialAuthorizedAnnualDeduction = 0;
            TaxCreditProvincial = 0;
            ProvincialDesignatedAreaDeduction = null;
            Lcf = 0;
            Lcp = 0;
            NorthwestTerritoriesTaxCalculationBase = 0;
            NorthwestTerritoriesTaxAssessed = 0;
            NunavutTaxCalculationBase = 0;
            NunavutTaxAssessed = 0;
            EmployerEmploymentInsuranceAmount = 0;
            EmployerCanadaPensionPlanAmount = 0;
            EmployerProvincialParentalAmount = 0;
            YtdCppQppDeducted = 0;
            YtdEiDeducted = 0;
            YtdPpipDeducted = 0;
            CppQppWagesCalculated = 0;
            EiWagesCalculated = 0;
            PpipWagesCalculated = 0;
        }
        #endregion
    }
}