﻿using System;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class LaborLevelExport : BusinessObject
    {
        #region properties
        [DataMember]
        public long DummyKey
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public string Level { get; set; }
        [DataMember]
        public string EntryName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Status { get; set; }

        #endregion

        #region construct/destruct
        public LaborLevelExport()
        {
            Clear();
        }
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public virtual void CopyTo(LaborLevelExport data)
        {
            base.CopyTo(data);

            data.DummyKey = DummyKey;
            data.Level = Level;
            data.EntryName = EntryName;
            data.Description = Description;
            data.Status = Status;
        }

        public new void Clear()
        {
            base.Clear();

            DummyKey = -1;
            Level = null;
            EntryName = null;
            Description = null;
            Status = null;
        }
        #endregion
    }
}
