﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public class PayrollMasterPaymentError : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties

        [DataMember]
        public long PayrollMasterPaymentId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String Reason { get; set; }
        [DataMember]
        public long EmployeeId { get; set; }
        [DataMember]
        public String EmployeeNumber { get; set; }
        [DataMember]
        public Decimal Amount { get; set; }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PayrollMasterPaymentError data)
        {
            base.CopyTo(data);

            data.PayrollMasterPaymentId = PayrollMasterPaymentId;
            data.Reason = Reason;
            data.EmployeeId = EmployeeId;
            data.EmployeeNumber = EmployeeNumber;
            data.Amount = Amount;
        }

        public new void Clear()
        {
            base.Clear();

            PayrollMasterPaymentId = -1;
            Reason = null;
            EmployeeId = -1;
            EmployeeNumber = null;
            Amount = 0;
        }
        #endregion
    }
}
