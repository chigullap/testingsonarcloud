﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeePaycodeImport : EmployeePaycode, IEmployeeIdentifier, IPaycodeIdentifier
    {

        private long _employeePaycodeId = -1;

        
        #region properties
        public String ImportExternalIdentifier { get; set; }
        public bool UseSalaryStandardHoursFlag {get;set;}
        public bool HasErrors { get; set; }
        public String PayrollProcessGroupCode { get; set; }

        [DataMember]
        public override long EmployeePaycodeId
        {
            get { return _employeePaycodeId; }
            set
            {
                KeyChangedEventArgs args = new KeyChangedEventArgs();
                args.OldKey = _employeePaycodeId.ToString();
                _employeePaycodeId = value;
                args.NewKey = Key;
                if (!(args.OldKey ?? String.Empty).Equals(args.NewKey ?? String.Empty) && this.GetType().Equals(typeof(EmployeePaycodeImport)))
                    OnKeyChanged(args);
            }
        }
        public override string Key
        {
            get
            {
                return EmployeePaycodeId.ToString();
            }
        }

        #endregion

        #region construct/destruct
        public EmployeePaycodeImport(Xsd.WorkLinksEmployeePaycodeBodyEmployeePaycode employeePayCode)
            : this()
        {
            ImportExternalIdentifier = employeePayCode.EmployeeIdentifier;

            PaycodeCodeImportExternalIdentifier = employeePayCode.PayCode.CodeIdentifier??String.Empty;
            AmountRate = employeePayCode.AmountRate;
            EmploymentInsuranceInsurableHoursPerUnit = employeePayCode.EmploymentInsuranceInsurableHoursPerUnit;
            StartDate = employeePayCode.StartDate;
            CutoffDate = employeePayCode.CutoffDate;
            PayPeriodMinimum = employeePayCode.PayPeriodMinimum;
            PayPeriodMaximum = employeePayCode.PayPeriodMaximum;
            MonthlyMaximum = employeePayCode.MonthlyMaximum;
            YearlyMaximum = employeePayCode.YearlyMaximum;
            AmountUnits = employeePayCode.AmountUnits;
            AmountPercentage = employeePayCode.AmountPercentage;
            ExcludeAmount = employeePayCode.ExcludeAmount;
            ExcludePercentage = employeePayCode.ExcludePercentage;
            //GroupIncomeFactor = employeePayCode.group;
            RoundUpTo = employeePayCode.RoundUpTo;
            RatePerRoundUpTo = employeePayCode.RatePerRoundUpTo;
            ExemptAmount = employeePayCode.ExemptAmount;
            ExemptAmountCodeNetGrossCd = employeePayCode.ExemptAmountCodeNetGrossCode;
            ExemptAmountCodeBeforeAfterCd = employeePayCode.ExemptAmountCodeBeforeAfterCode;
            ExemptPercentage = employeePayCode.ExemptPercentage;
            ExemptPercentageCodeNetGrossCd = employeePayCode.ExemptPercentageCodeNetGrossCode;
            ExemptPercentageCodeBeforeAfterCd = employeePayCode.ExemptPercentageCodeBeforeAfterCode;
            OverrideConcurrencyCheck = true;
        }

        public EmployeePaycodeImport()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeePaycodeImport data)
        {
            base.CopyTo(data);
            data.ImportExternalIdentifier = ImportExternalIdentifier;
            data.UseSalaryStandardHoursFlag = UseSalaryStandardHoursFlag;
            data.HasErrors = HasErrors;
            data.PayrollProcessGroupCode = PayrollProcessGroupCode;
        }
        public new void Clear()
        {
            base.Clear();
            ImportExternalIdentifier = null;
            UseSalaryStandardHoursFlag = false;
            HasErrors = false;
            PayrollProcessGroupCode = null;
        }

        #endregion
    }
}
