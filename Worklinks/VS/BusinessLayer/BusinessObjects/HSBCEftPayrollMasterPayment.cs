﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class HSBCEftPayrollMasterPayment : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PayrollMasterPaymentId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public string EftOriginatorId { get; set; }

        [DataMember]
        public decimal ChequeAmount { get; set; }

        [DataMember]
        public DateTime ChequeDate { get; set; }

        [DataMember]
        public string BankCode { get; set; }

        [DataMember]
        public string BankTransitNumber { get; set; }

        [DataMember]
        public string BankAccountNumber { get; set; }

        [DataMember]
        public string CompanyShortName { get; set; }

        [DataMember]
        public string EmployeeName { get; set; }

        [DataMember]
        public string CompanyLongName { get; set; }

        #endregion

        #region construct/destruct
        public HSBCEftPayrollMasterPayment()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(HSBCEftPayrollMasterPayment data)
        {
            base.CopyTo(data);

            data.PayrollMasterPaymentId = PayrollMasterPaymentId;
            data.EftOriginatorId = EftOriginatorId;
            data.ChequeAmount = ChequeAmount;
            data.ChequeDate = ChequeDate;
            data.BankCode = BankCode;
            data.BankTransitNumber = BankTransitNumber;
            data.BankAccountNumber = BankAccountNumber;
            data.CompanyShortName = CompanyShortName;
            data.EmployeeName = EmployeeName;
            data.CompanyLongName = CompanyLongName;            
        }
        public new void Clear()
        {
            base.Clear();

            PayrollMasterPaymentId = -1;
            EftOriginatorId = null;
            ChequeAmount = 0;
            ChequeDate = DateTime.Now;
            BankCode = null;
            BankTransitNumber = null;
            BankAccountNumber = null;
            CompanyShortName = null;
            EmployeeName = null;
            CompanyLongName = null;
        }
        #endregion
    }
}