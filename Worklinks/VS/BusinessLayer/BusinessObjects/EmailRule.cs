﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmailRule : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmailRuleId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmailId { get; set; }

        [DataMember]
        public String EmailTriggerFieldCode { get; set; }

        [DataMember]
        public int TriggerFieldOffsetDays { get; set; }

        [DataMember]
        public String OrganizationUnit { get; set; }

        [DataMember]
        public String OrganizationUnitDescription { get; set; }

        [DataMember]
        public String EmailTo { get; set; }
        #endregion

        #region construct/destruct
        public EmailRule()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public virtual void CopyTo(EmailRule data)
        {
            base.CopyTo(data);
            data.EmailRuleId = EmailRuleId;
            data.EmailId = EmailId;
            data.EmailTriggerFieldCode = EmailTriggerFieldCode;
            data.TriggerFieldOffsetDays = TriggerFieldOffsetDays;
            data.OrganizationUnit = OrganizationUnit;
            data.OrganizationUnitDescription = OrganizationUnitDescription;
            data.EmailTo = EmailTo;
        }
        public new void Clear()
        {
            base.Clear();
            EmailRuleId = -1;
            EmailId = -1;
            EmailTriggerFieldCode = null;
            TriggerFieldOffsetDays = 0;
            OrganizationUnit = null;
            OrganizationUnitDescription = null;
            EmailTo = null;
        }
        #endregion
    }
}