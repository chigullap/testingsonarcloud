﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Runtime.Serialization;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeePaycodeProcessedCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeePaycodeProcessed>
    {
    }
}
