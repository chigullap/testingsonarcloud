﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeePositionSecondaryOrganizationUnitBulk : EmployeePositionSecondaryOrganizationUnit
    {
        [DataMember]
        public override long EmployeePositionSecondaryOrganizationUnitId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public override long OrganizationUnitLevelId { get; set; }

    }
}