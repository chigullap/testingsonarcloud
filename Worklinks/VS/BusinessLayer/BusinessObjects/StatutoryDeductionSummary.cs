﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class StatutoryDeductionSummary: StatutoryDeduction
    {
        
        #region properties
        public String EmployerNumber { get; set; }
        public int? DefaultProvincialTaxClaim { get; set; }
        #endregion

        #region construct/destruct
        public StatutoryDeductionSummary()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(StatutoryDeductionSummary data)
        {
            base.CopyTo(data);
            data.EmployerNumber = EmployerNumber;
            data.DefaultProvincialTaxClaim = DefaultProvincialTaxClaim;
        }


        public new void Clear()
        {
            base.Clear();
            EmployerNumber = null;
            DefaultProvincialTaxClaim = null;
        }

        #endregion
    }
}
