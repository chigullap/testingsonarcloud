﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class CitiEftPayrollMasterPaymentCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<CitiEftPayrollMasterPayment>
    {
    }
}