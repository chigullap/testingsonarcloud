﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class RevenueQuebecR1ExportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<RevenueQuebecR1Export>
    {
    }
}
