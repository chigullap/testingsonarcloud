﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeRoeAmountDetailSummary : EmployeeRoeAmountDetail
    {
        #region properties
        [DataMember]
        public Decimal PeriodYear { get; set; }

        [DataMember]
        public Decimal Period { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime CutoffDate { get; set; }

        [DataMember]
        public DateTime ChequeDate { get; set; }
        #endregion

        #region construct/destruct
        public EmployeeRoeAmountDetailSummary()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeeRoeAmountDetailSummary data)
        {
            base.CopyTo(data);

            data.PeriodYear = PeriodYear;
            data.Period = Period;
            data.StartDate = StartDate;
            data.CutoffDate = CutoffDate;
            data.ChequeDate = ChequeDate;
        }
        public new void Clear()
        {
            base.Clear();

            PeriodYear = -1;
            Period = -1;
            StartDate = DateTime.Now;
            CutoffDate = DateTime.Now;
            ChequeDate = DateTime.Now;
        }
        #endregion
    }
}