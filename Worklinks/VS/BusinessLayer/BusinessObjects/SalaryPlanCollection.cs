﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class SalaryPlanCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<SalaryPlan>
    {
    }
}