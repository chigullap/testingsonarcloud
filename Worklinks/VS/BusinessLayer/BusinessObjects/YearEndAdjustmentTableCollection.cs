﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class YearEndAdjustmentTableCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<YearEndAdjustmentTable>
    {
        public long? GetYearEndAdjustmentTableIdFromIdentifer(String identifier)
        {
            long? rtn = null;

            foreach (YearEndAdjustmentTable item in this)
            {
                if (item.YearEndFormCode.ToLower().Equals(identifier.ToLower()))
                    return item.YearEndAdjustmentTableId;
            }

            return rtn;
        }
    }
}