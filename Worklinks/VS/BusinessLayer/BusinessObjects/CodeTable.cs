﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CodeTable : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        //these ints represent the index for the English and French descriptions in the "Description" object, which is a "CodeTableDescriptionCollection"
        private static string _englishCd = "EN";
        private static string _frenchCd = "FR";
        private CodeTableDescriptionCollection _descriptions;

        #region properties
        [DataMember]
        public String CodeTableId
        {
            get { return (String)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String ParentCodeTableId { get; set; }
        [DataMember]
        public bool ActiveFlag { get; set; }
        [DataMember]
        public bool SystemFlag { get; set; }
        [DataMember]
        public Decimal? SortOrder { get; set; }
        [DataMember]
        public DateTime? StartDate { get; set; }
        [DataMember]
        public DateTime? EndDate { get; set; }
        [DataMember]
        public CodeTableDescriptionCollection Descriptions
        {
            get
            {
                if (_descriptions == null)
                    _descriptions = new CodeTableDescriptionCollection();
                return _descriptions;
            }
            set { _descriptions = value; }
        }
        [DataMember]
        public string EnglishDesc
        {
            get
            {
                return Descriptions[_englishCd] == null ? null : Descriptions[_englishCd].TableDescription;
            }
            set
            {
                if (EnglishDesc == null)
                {
                    Descriptions.Add(new CodeTableDescription() { LanguageCode = _englishCd, CodeTableDescCd = CodeTableId, CreateUser = UpdateUser, UpdateUser = UpdateUser, TableDescription = value });
                }
                else
                {
                    Descriptions[_englishCd].UpdateUser = UpdateUser;
                    Descriptions[_englishCd].TableDescription = value;
                }
            }
        }
        [DataMember]
        public string FrenchDesc
        {
            get
            {
                return Descriptions[_frenchCd] == null ? null : Descriptions[_frenchCd].TableDescription;
            }
            set
            {
                if (FrenchDesc == null)
                {
                    Descriptions.Add(new CodeTableDescription() { LanguageCode = _frenchCd, CodeTableDescCd = CodeTableId, CreateUser = UpdateUser, UpdateUser = UpdateUser, TableDescription = value });
                }
                else
                {
                    Descriptions[_frenchCd].UpdateUser = UpdateUser;
                    Descriptions[_frenchCd].TableDescription = value;
                }
            }
        }
        [DataMember]
        public String ImportExternalIdentifier { get; set; }
        #endregion

        #region construct/destruct
        public CodeTable()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(CodeTable data)
        {
            base.CopyTo(data);

            data.CodeTableId = CodeTableId;
            data.ParentCodeTableId = ParentCodeTableId;
            data.ActiveFlag = ActiveFlag;
            data.SystemFlag = SystemFlag;
            data.SortOrder = SortOrder;
            data.StartDate = StartDate;
            data.EndDate = EndDate;
            if (Descriptions != null)
            {
                data.Descriptions = new CodeTableDescriptionCollection();
                Descriptions.CopyTo(data.Descriptions);
            }
            data.ImportExternalIdentifier = ImportExternalIdentifier;
        }

        public new void Clear()
        {
            base.Clear();

            CodeTableId = "";
            ParentCodeTableId = "";
            ActiveFlag = false;
            SystemFlag = false;
            SortOrder = 1;
            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
            Descriptions = null;
            ImportExternalIdentifier = null;
        }
        #endregion
    }
}