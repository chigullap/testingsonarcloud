﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PreAuthorizedDebitTotals : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public string BusinessTaxNumber
        {
            get { return (string)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public Decimal Amount { get; set; }
        [DataMember]
        public String CodePreAuthorizedBankCd { get; set; }
        [DataMember]
        public String PreAuthorizedTransitNumber { get; set; }
        [DataMember]
        public String PreAuthorizedAccountNumber { get; set; }
        [DataMember]
        public DateTime PaymentDueDate { get; set; }
        [DataMember]
        public String ProcessGroupDescription { get; set; }
        [DataMember]
        public String CodeWcbCd { get; set; }

        #endregion

        #region construct/destruct
        public PreAuthorizedDebitTotals()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            PreAuthorizedDebitTotals temp = new PreAuthorizedDebitTotals();
            this.CopyTo(temp);
            return temp;
        }
        public void CopyTo(PreAuthorizedDebitTotals data)
        {
            base.CopyTo(data);

            data.BusinessTaxNumber = BusinessTaxNumber;
            data.Amount = Amount;
            data.CodePreAuthorizedBankCd = CodePreAuthorizedBankCd;
            data.PreAuthorizedTransitNumber = PreAuthorizedTransitNumber;
            data.PreAuthorizedAccountNumber = PreAuthorizedAccountNumber;
            data.PaymentDueDate = PaymentDueDate;
            data.ProcessGroupDescription = ProcessGroupDescription;
            data.CodeWcbCd = CodeWcbCd;
        }
        public new void Clear()
        {
            base.Clear();

            BusinessTaxNumber = "";
            Amount = 0;
            CodePreAuthorizedBankCd = "";
            PreAuthorizedTransitNumber = "";
            PreAuthorizedAccountNumber = "";
            PaymentDueDate = DateTime.Now;
            ProcessGroupDescription = null;
            CodeWcbCd = null;
        }

        #endregion

    }
}
