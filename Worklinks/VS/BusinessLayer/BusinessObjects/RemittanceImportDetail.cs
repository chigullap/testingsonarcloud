﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class RemittanceImportDetail : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long RemittanceImportDetailId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long RemittanceImportId { get; set; }

        [DataMember]
        [Required]
        public string EmployeeImportExternalIdentifier { get; set; }

        [DataMember]
        [Required]
        public string LastName { get; set; }

        [DataMember]
        [Required]
        public string FirstName { get; set; }

        [DataMember]
        public string FederalBusinessNumber { get; set; }

        [DataMember]
        public string QuebecBusinessNumber { get; set; }

        [DataMember]
        public string WorkersCompensationImportExternalIdentifier { get; set; }

        [DataMember]
        public decimal? WorkersCompensationAssessable { get; set; }

        [DataMember]
        public decimal? EmployerWorkersCompensationAmount { get; set; }

        [DataMember]
        public string EmployerHealthTaxProvinceStateCode { get; set; }

        [DataMember]
        public string TaxProvince { get; set; }

        [DataMember]
        public string EHTCode { get; set; }

        [DataMember]
        public decimal? EmployerHealthTaxAssessable { get; set; }

        [DataMember]
        public decimal? EmployerHealthTax { get; set; }

        [DataMember]
        public decimal? GrossTaxableIncome { get; set; }

        [DataMember]
        public decimal? FederalTax { get; set; }

        [DataMember]
        public decimal? CanadaPensionPlan { get; set; }

        [DataMember]
        public decimal? EmployerCanadaPensionPlan { get; set; }

        [DataMember]
        public decimal? EmploymentInsurance { get; set; }

        [DataMember]
        public decimal? EmployerEmploymentInsurance { get; set; }

        [DataMember]
        public decimal? QuebecTax { get; set; }

        [DataMember]
        public decimal? QuebecPensionPlan { get; set; }

        [DataMember]
        public decimal? EmployerQuebecPensionPlan { get; set; }

        [DataMember]
        public decimal? QuebecParentalInsurancePlan { get; set; }

        [DataMember]
        public decimal? EmployerQuebecParentalInsurancePlan { get; set; }

        [DataMember]
        public decimal? NunavutTax { get; set; }

        [DataMember]
        public decimal? NorthwestTerritoriesTax { get; set; }


        [DataMember]
        public decimal? CanadaRevenueAgencyGarnishment { get; set; }

        [DataMember]
        public string SocialInsuranceNumber { get; set; }

        [DataMember]
        public decimal? RevenueQuebecGarnishement { get; set; }
        #endregion

        [DataMember]
        public string ThirdPartyPaycode1 { get; set; }
        [DataMember]
        public decimal? ThirdPartyAmount1 { get; set; }
        [DataMember]
        public string ThirdPartyPaycode2 { get; set; }
        [DataMember]
        public decimal? ThirdPartyAmount2 { get; set; }
        [DataMember]
        public string ThirdPartyPaycode3 { get; set; }
        [DataMember]
        public decimal? ThirdPartyAmount3 { get; set; }
        [DataMember]
        public string ThirdPartyPaycode4 { get; set; }
        [DataMember]
        public decimal? ThirdPartyAmount4 { get; set; }
        [DataMember]
        public string ThirdPartyPaycode5 { get; set; }
        [DataMember]
        public decimal? ThirdPartyAmount5 { get; set; }
        [DataMember]
        public string ThirdPartyPaycode6 { get; set; }
        [DataMember]
        public decimal? ThirdPartyAmount6 { get; set; }
        [DataMember]
        public string ThirdPartyPaycode7 { get; set; }
        [DataMember]
        public decimal? ThirdPartyAmount7 { get; set; }
        [DataMember]
        public string ThirdPartyPaycode8 { get; set; }
        [DataMember]
        public decimal? ThirdPartyAmount8 { get; set; }
        [DataMember]
        public string ThirdPartyPaycode9 { get; set; }
        [DataMember]
        public decimal? ThirdPartyAmount9 { get; set; }
        [DataMember]
        public string ThirdPartyPaycode10 { get; set; }
        [DataMember]
        public decimal? ThirdPartyAmount10 { get; set; }

        //for remittance clients who want to cut a payroll cheque
        [DataMember]
        public decimal? PayrollChqAmount { get; set; }

        [DataMember]
        public string AddressLine1 { get; set; }

        [DataMember]
        public string AddressLine2 { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string PostalZipCode { get; set; }

        [DataMember]
        public string ProvinceStateCode { get; set; }

        [DataMember]
        public string CountryCode { get; set; }


        public string ChequeDataInValidMsg
        {
            get
            {
                if (PayrollChqAmount != null && (string.IsNullOrWhiteSpace(AddressLine1) || string.IsNullOrWhiteSpace(City) || string.IsNullOrWhiteSpace(PostalZipCode) || string.IsNullOrWhiteSpace(ProvinceStateCode) || string.IsNullOrWhiteSpace(CountryCode)))
                    return $"Employee: {EmployeeImportExternalIdentifier} - cheques require complete address information";
                else if (PayrollChqAmount == null && !(string.IsNullOrWhiteSpace(AddressLine1) || string.IsNullOrWhiteSpace(City) || string.IsNullOrWhiteSpace(PostalZipCode) || string.IsNullOrWhiteSpace(ProvinceStateCode) || string.IsNullOrWhiteSpace(CountryCode)))
                    return $"Employee: {EmployeeImportExternalIdentifier} - cheque amount empty but address information provided";
                else if (PayrollChqAmount != null && Convert.ToDecimal(PayrollChqAmount) <= 0)
                    return $"Employee: {EmployeeImportExternalIdentifier} - cheque value must be greater than 0";
                else
                    return null;
            }
        }

        #region construct/destruct
        public RemittanceImportDetail()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(RemittanceImportDetail data)
        {
            base.CopyTo(data);

            data.RemittanceImportDetailId = RemittanceImportDetailId;
            data.RemittanceImportId = RemittanceImportId;
            data.EmployeeImportExternalIdentifier = EmployeeImportExternalIdentifier;
            data.LastName = LastName;
            data.FirstName = FirstName;
            data.FederalBusinessNumber = FederalBusinessNumber;
            data.QuebecBusinessNumber = QuebecBusinessNumber;
            data.WorkersCompensationAssessable = WorkersCompensationAssessable;
            data.WorkersCompensationImportExternalIdentifier = WorkersCompensationImportExternalIdentifier;
            data.EmployerWorkersCompensationAmount = EmployerWorkersCompensationAmount;
            data.EmployerHealthTaxProvinceStateCode = EmployerHealthTaxProvinceStateCode;
            data.TaxProvince = TaxProvince;
            data.EHTCode = EHTCode;
            data.EmployerHealthTax = EmployerHealthTax;
            data.EmployerHealthTaxAssessable = EmployerHealthTaxAssessable;
            data.GrossTaxableIncome = GrossTaxableIncome;
            data.FederalTax = FederalTax;
            data.CanadaPensionPlan = CanadaPensionPlan;
            data.EmployerCanadaPensionPlan = EmployerCanadaPensionPlan;
            data.EmploymentInsurance = EmploymentInsurance;
            data.EmployerEmploymentInsurance = EmployerEmploymentInsurance;
            data.QuebecTax = QuebecTax;
            data.QuebecPensionPlan = QuebecPensionPlan;
            data.EmployerQuebecPensionPlan = EmployerQuebecPensionPlan;
            data.QuebecParentalInsurancePlan = QuebecParentalInsurancePlan;
            data.EmployerQuebecParentalInsurancePlan = EmployerQuebecParentalInsurancePlan;
            data.NunavutTax = NunavutTax;
            data.NorthwestTerritoriesTax = NorthwestTerritoriesTax;
            data.CanadaRevenueAgencyGarnishment = CanadaRevenueAgencyGarnishment;
            data.SocialInsuranceNumber = SocialInsuranceNumber;
            data.RevenueQuebecGarnishement = RevenueQuebecGarnishement;
            data.ThirdPartyPaycode1 = ThirdPartyPaycode1;
            data.ThirdPartyAmount1 = ThirdPartyAmount1;
            data.ThirdPartyPaycode2 = ThirdPartyPaycode2;
            data.ThirdPartyAmount2 = ThirdPartyAmount2;
            data.ThirdPartyPaycode3 = ThirdPartyPaycode3;
            data.ThirdPartyAmount3 = ThirdPartyAmount3;
            data.ThirdPartyPaycode4 = ThirdPartyPaycode4;
            data.ThirdPartyAmount4 = ThirdPartyAmount4;
            data.ThirdPartyPaycode5 = ThirdPartyPaycode5;
            data.ThirdPartyAmount5 = ThirdPartyAmount5;
            data.ThirdPartyPaycode6 = ThirdPartyPaycode6;
            data.ThirdPartyAmount6 = ThirdPartyAmount6;
            data.ThirdPartyPaycode7 = ThirdPartyPaycode7;
            data.ThirdPartyAmount7 = ThirdPartyAmount7;
            data.ThirdPartyPaycode8 = ThirdPartyPaycode8;
            data.ThirdPartyAmount8 = ThirdPartyAmount8;
            data.ThirdPartyPaycode9 = ThirdPartyPaycode9;
            data.ThirdPartyAmount9 = ThirdPartyAmount9;
            data.ThirdPartyPaycode10 = ThirdPartyPaycode10;
            data.ThirdPartyAmount10 = ThirdPartyAmount10;

            data.PayrollChqAmount = PayrollChqAmount;
            data.AddressLine1 = AddressLine1;
            data.AddressLine2 = AddressLine2;
            data.City = City;
            data.PostalZipCode = PostalZipCode;
            data.ProvinceStateCode = ProvinceStateCode;
            data.CountryCode = CountryCode;
        }
        public new void Clear()
        {
            base.Clear();

            RemittanceImportDetailId = -1;
            RemittanceImportId = -1;
            EmployeeImportExternalIdentifier = null;
            LastName = null;
            FirstName = null;
            FederalBusinessNumber = null;
            QuebecBusinessNumber = null;
            WorkersCompensationImportExternalIdentifier = null;
            WorkersCompensationAssessable = null;
            EmployerWorkersCompensationAmount = null;
            EmployerHealthTaxProvinceStateCode = null;
            TaxProvince = null;
            EHTCode = null;
            EmployerHealthTax = null;
            EmployerHealthTaxAssessable = null;
            GrossTaxableIncome = null;
            FederalTax = null;
            CanadaPensionPlan = null;
            EmployerCanadaPensionPlan = null;
            EmploymentInsurance = null;
            EmployerEmploymentInsurance = null;
            QuebecTax = null;
            QuebecPensionPlan = null;
            EmployerQuebecPensionPlan = null;
            QuebecParentalInsurancePlan = null;
            EmployerQuebecParentalInsurancePlan = null;
            NunavutTax = null;
            NorthwestTerritoriesTax = null;
            CanadaRevenueAgencyGarnishment = null;
            SocialInsuranceNumber = null;
            RevenueQuebecGarnishement = null;
            ThirdPartyPaycode1 = null;
            ThirdPartyAmount1 = null;
            ThirdPartyPaycode2 = null;
            ThirdPartyAmount2 = null;
            ThirdPartyPaycode3 = null;
            ThirdPartyAmount3 = null;
            ThirdPartyPaycode4 = null;
            ThirdPartyAmount4 = null;
            ThirdPartyPaycode5 = null;
            ThirdPartyAmount5 = null;
            ThirdPartyPaycode6 = null;
            ThirdPartyAmount6 = null;
            ThirdPartyPaycode7 = null;
            ThirdPartyAmount7 = null;
            ThirdPartyPaycode8 = null;
            ThirdPartyAmount8 = null;
            ThirdPartyPaycode9 = null;
            ThirdPartyAmount9 = null;
            ThirdPartyPaycode10 = null;
            ThirdPartyAmount10 = null;

            PayrollChqAmount = null;
            AddressLine1 = null;
            AddressLine2 = null;
            City = null;
            PostalZipCode = null;
            ProvinceStateCode = null;
            CountryCode = null;
        }
        #endregion
    }
}