﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeBenefit : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeBenefitId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        [Required]
        public DateTime? EffectiveDate { get; set; }

        [DataMember]
        public long BenefitPolicyId { get; set; }

        [DataMember]
        public String BenefitStatusCode { get; set; }

        [DataMember]
        public String BenefitCoverageCode { get; set; }

        [DataMember]
        public String BenefitSmokerCode { get; set; }

        [DataMember]
        public DateTime? EligibilityDate { get; set; }

        [DataMember]
        public DateTime? EnrolmentDate { get; set; }
        #endregion

        #region construct/destruct
        public EmployeeBenefit()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(EmployeeBenefit data)
        {
            base.CopyTo(data);
            data.EmployeeBenefitId = EmployeeBenefitId;
            data.EmployeeId = EmployeeId;
            data.EffectiveDate = EffectiveDate;
            data.BenefitPolicyId = BenefitPolicyId;
            data.BenefitStatusCode = BenefitStatusCode;
            data.BenefitCoverageCode = BenefitCoverageCode;
            data.BenefitSmokerCode = BenefitSmokerCode;
            data.EligibilityDate = EligibilityDate;
            data.EnrolmentDate = EnrolmentDate;
        }

        public new void Clear()
        {
            base.Clear();
            EmployeeBenefitId = -1;
            EmployeeId = -1;
            EffectiveDate = null;
            BenefitPolicyId = -1;
            BenefitStatusCode = null;
            BenefitCoverageCode = null;
            BenefitSmokerCode = null;
            EligibilityDate = null;
            EnrolmentDate = null;
        }
        #endregion
    }
}
