﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CanadaRevenueAgencyMaximumPensionContributionLimit : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long CanadaRevenueAgencyMaximumPensionContributionLimitId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public Decimal Year { get; set; }
        [DataMember]
        public Decimal Amount { get; set; }

        #endregion


        #region construct/destruct
        public CanadaRevenueAgencyMaximumPensionContributionLimit()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(CanadaRevenueAgencyMaximumPensionContributionLimit data)
        {
            base.CopyTo(data);

            data.CanadaRevenueAgencyMaximumPensionContributionLimitId = CanadaRevenueAgencyMaximumPensionContributionLimitId;
            data.Year = Year;
            data.Amount = Amount;
        }

        public new void Clear()
        {
            base.Clear();

            CanadaRevenueAgencyMaximumPensionContributionLimitId = -1;
            Year = DateTime.Now.Year;
            Amount = 0;           
        }

        #endregion
    }
}
