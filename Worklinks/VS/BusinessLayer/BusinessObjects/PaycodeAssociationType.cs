﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;
using System.ComponentModel.DataAnnotations;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PaycodeAssociationType : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public String PaycodeAssociationTypeCode
        {
            get { return (String)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public bool HasChildrenFlag { get; set; }
        #endregion

        #region construct/destruct
        public PaycodeAssociationType()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(PaycodeAssociationType data)
        {
            base.CopyTo(data);

            data.PaycodeAssociationTypeCode = PaycodeAssociationTypeCode;
            data.Description = Description;
            data.HasChildrenFlag = HasChildrenFlag;
        }

        public new void Clear()
        {
            base.Clear();

            PaycodeAssociationTypeCode = "";
            Description = null;
            HasChildrenFlag = false;
        }
        #endregion
    }
}