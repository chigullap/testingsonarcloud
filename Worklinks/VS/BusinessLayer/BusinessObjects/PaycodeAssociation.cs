﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;
using System.ComponentModel.DataAnnotations;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PaycodeAssociation : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PaycodeAssociationId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String PaycodeCode { get; set; } 

        [DataMember]
        public String PaycodeAssociationTypeCode { get; set; }

        [DataMember]
        public String ExternalCode { get; set; }

        [DataMember]
        public String ProvinceStateCode { get; set; }
        #endregion

        #region construct/destruct
        public PaycodeAssociation()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(PaycodeAssociation data)
        {
            base.CopyTo(data);

            data.PaycodeAssociationId = PaycodeAssociationId;
            data.PaycodeCode = PaycodeCode;
            data.ExternalCode = ExternalCode;
            data.PaycodeAssociationTypeCode = PaycodeAssociationTypeCode;
            data.ProvinceStateCode = ProvinceStateCode;
        }

        public new void Clear()
        {
            base.Clear();

            PaycodeAssociationId = -1;
            PaycodeCode = null;
            ExternalCode = null;
            PaycodeAssociationTypeCode = "";
            ProvinceStateCode = "";
        }
        #endregion
    }
}