﻿using System;
using System.Runtime.Serialization;
using WorkLinks.BusinessLayer.BusinessObjects.CalcModel;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PaycodePayrollTransactionOffsetAssociationCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PaycodePayrollTransactionOffsetAssociation>
    {

        public PayrollTransactionEmployeePaycodeCollection GetOffsetTransactions(PayrollTransactionEmployeePaycodeCollection items, ref int id )
        {
            PayrollTransactionEmployeePaycodeCollection rtn = new PayrollTransactionEmployeePaycodeCollection();

            PayrollTransactionEmployeePaycodeCollection offset1 = new PayrollTransactionEmployeePaycodeCollection();
            foreach (PayrollTransactionEmployeePaycode item in items)
            {
                offset1.Add(GetOffsetTransactions(item, ref id));
            }

            rtn.Add(offset1);

            PayrollTransactionEmployeePaycodeCollection offset2 = new PayrollTransactionEmployeePaycodeCollection();
            foreach (PayrollTransactionEmployeePaycode item in offset1)//offset the offset
            {
                offset2.Add(GetOffsetTransactions(item, ref id));
            }
            rtn.Add(offset2);

            return rtn;
        }

        private PayrollTransactionEmployeePaycodeCollection GetOffsetTransactions(PayrollTransactionEmployeePaycode inputTransaction, ref int id)
        {
            PayrollTransactionEmployeePaycodeCollection rtn = new PayrollTransactionEmployeePaycodeCollection();
            foreach (PaycodePayrollTransactionOffsetAssociation item in GetOffsets(inputTransaction.PaycodeCode))
            {
                PayrollTransactionEmployeePaycode offsetTransaction = (PayrollTransactionEmployeePaycode)inputTransaction.Clone();
                offsetTransaction.PayrollTransactionId = id--;
                offsetTransaction.PaycodeCode = item.OffsetCodePaycodeCode;
                offsetTransaction.EmploymentInsuranceHour = 0;
                offsetTransaction.Units = item.UseBaseSalaryRateFlag ? 1 : inputTransaction.Units;
			    offsetTransaction.Rate = item.UseBaseSalaryRateFlag?(inputTransaction.BaseSalaryRate * inputTransaction.EmploymentInsuranceHour):( inputTransaction.Amount * item.OffsetMultiplier);
                offsetTransaction.PaycodeTypeCode = item.PaycodeTypeCode;
                offsetTransaction.PaycodeIncomePayTypeCode = item.PaycodeIncomePayTypeCode;

                rtn.Add(offsetTransaction);
            }

            return rtn;
        }


        private PaycodePayrollTransactionOffsetAssociationCollection GetOffsets(String paycode)
        {
            PaycodePayrollTransactionOffsetAssociationCollection rtn = new PaycodePayrollTransactionOffsetAssociationCollection();

            foreach (PaycodePayrollTransactionOffsetAssociation item in this)
            {
                if (item.PaycodeCode.ToLower().Equals(paycode.ToLower()))
                {
                    rtn.Add(item);
                }
            }

            return rtn;
        }
    }
}