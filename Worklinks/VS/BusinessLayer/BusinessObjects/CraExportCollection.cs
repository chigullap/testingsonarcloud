﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class CraExportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<CraExport>
    {
    }
}
