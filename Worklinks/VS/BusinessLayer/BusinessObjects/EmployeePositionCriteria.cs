﻿using System;
using System.Collections.Generic;
using System.Text;

using System.ComponentModel.DataAnnotations;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class EmployeePositionCriteria
    {
        #region properties
        public bool SecurityOverrideFlag { get; set; }
        public long EmployeeId { get; set; }
        public long? EmployeePositionId { get; set; }
        public bool GetCurrentPositionFlag { get; set; }
        public bool GetLatestRecordFlag { get; set; }
        public String PayrollProcessGroupCode { get; set; }
        public long? NextEmployeePositionId { get; set; }
        public DateTime? TransactionDate { get; set; }
        public bool IncludeWorkDays { get; set; }
        public bool IncludeOrganizationUnit { get; set; }
        #endregion

        public EmployeePositionCriteria()
        {
            Clear();
        }

        public void Clear()
        {
            SecurityOverrideFlag = false;
            EmployeeId = -1;
            EmployeePositionId = null;
            GetCurrentPositionFlag = false;
            GetLatestRecordFlag = false;
            PayrollProcessGroupCode = null;
            NextEmployeePositionId = null;
            TransactionDate = null;
            IncludeWorkDays = true;
            IncludeOrganizationUnit = true;
        }
    }

}
