﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    [KnownType(typeof(LabourUnionSummary))]
    public class LabourUnion : Person
    {
        #region properties
        [DataMember]
        public long LabourUnionId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String EnglishDescription { get; set; }

        [DataMember]
        public String FrenchDescription { get; set; }

        [DataMember]
        public DateTime? EffectiveDate { get; set; }
        #endregion

        #region construct/destruct
        public LabourUnion()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(LabourUnion data)
        {
            base.CopyTo(data);

            data.LabourUnionId = LabourUnionId;
            data.EnglishDescription = EnglishDescription;
            data.FrenchDescription = FrenchDescription;
            data.PersonId = PersonId;
            data.EffectiveDate = EffectiveDate;
            data.TitleCode = TitleCode;
        }
        public new void Clear()
        {
            base.Clear();

            LabourUnionId = -1;
            EnglishDescription = null;
            FrenchDescription = null;
            PersonId = -1;
            EffectiveDate = null;
            TitleCode = null;
        }
        #endregion
    }
}