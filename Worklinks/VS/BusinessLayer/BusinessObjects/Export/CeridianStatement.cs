﻿using System;
using System.Text;

namespace WorkLinks.BusinessLayer.BusinessObjects.Export
{
    public class CeridianStatement : CeridianPay
    {
        protected override string CeridianExportType
        {
            get
            {
                return "SSS";
            }
        }

        public PayrollMasterPaymentCollection EFTS { get; set; }

        #region A
        protected String A(PayrollMasterPayment eft)
        {
            StringBuilder output = new StringBuilder();
            output.Append('A');  //01-01 --Record Type
            output.Append(String.Empty.PadLeft(1));//02-02 --filler
            output.Append(FormatMoney(eft.Amount).PadLeft(13));//03-15 -- Deposit Amount
            output.Append(String.Empty.PadLeft(2));//16-17 --filler
            output.Append("Deposit".PadLeft(7));//18-24 --Type
            output.Append(String.Empty.PadLeft(2));//25-26 --filler
            output.Append(eft.CodeEmployeeBankCd.PadLeft(4, '0'));//27-30 --institution
            output.Append(String.Empty.PadLeft(2));//31-32 --filler
            output.Append(eft.TransitNumber.PadLeft(5));//33-37 --branch
            output.Append(eft.AccountNumber.PadLeft(12));//25-26 --account

            if (output.Length != 49) throw new Exception("error forming 'A' record");

            return output.ToString();
        }
        #endregion

        public override string ToString()
        {
            StringBuilder output = new StringBuilder();

            //C
            output.AppendLine(C());

            //I
            output.AppendLine(I());

            //F
            output.AppendLine(F());

            //K
            output.AppendLine(K());

            //E
            //decimal ytdTotalEarnings = 0;
            //foreach (DynamicsMasterPayDetail detail in Earnings)
            //{
            output.AppendLine(E());
            //    ytdTotalEarnings += detail.PaycodeYTDDollars;
            //}

            //D
            //decimal totalDeductions = 0;
            //decimal ytdTotalDeductions = 0;
            //foreach (DynamicsMasterPayDetail detail in Deductions)
            //{
            output.AppendLine(D());
            //    totalDeductions += detail.PaycodeCurrentDollars;
            //    ytdTotalDeductions += detail.PaycodeYTDDollars;
            //}

            //B
            //foreach (DynamicsMasterPayDetail detail in Benefits)
            //{
            output.AppendLine(B());
            //}

            //X
            //skipped 

            //M
            //skipped 

            //S
            output.AppendLine(S());

            //Y
            output.AppendLine(Y());

            //A
            foreach (PayrollMasterPayment eft in EFTS)
            {
                output.AppendLine(A(eft));
            }

            return output.ToString();
        }
    }
}