﻿using System;
using System.Text;

namespace WorkLinks.BusinessLayer.BusinessObjects.Export
{
    public class VendorCheque : CeridianCheque
    {
        #region properties
        public String AddressLine1 { get; set; }
        public String ProvinceStateCode { get; set; }
        public String PostalZipCode { get; set; }
        public String VendorName { get; set; }
        public String City { get; set; }
        public String OrderNumber { get; set; }
        #endregion

        #region I
        protected override String I()
        {
            StringBuilder output = new StringBuilder();
            output.Append('I');  //01-01 --Record Type
            output.Append(String.Empty.PadLeft(4)); //02-05 --Division/Department (left blank as sample was also blank)
            output.Append(String.Empty.PadLeft(1)); //06-06 --filler
            output.Append(Employee.EmployeeNumber.PadLeft(10)); //07-16 --Employee Number
            output.Append(VendorName.PadRight(30).Substring(0, 30)); //17-46 --Employee Name
            output.Append(AddressLine1.PadRight(34).Substring(0, 34)); //47-80   --Employee Address 1 /*pad with spaces*/
            output.Append(ProvinceStateCode.PadRight(30).Substring(0, 30)); //81-110   --Employee Address 3 /*pad with spaces*/
            output.Append(PostalZipCode.PadRight(7).Substring(0, 7)); //111-117   --Employee Postal Code /*pad with spaces*/
            output.Append(FormatDate(PeriodDate)); //118-127 --period end date
            output.Append(FormatDate(ChequeDate)); //128-137 --pay date
            output.Append(ChequeNumber.PadLeft(8).Substring(0, 8)); //138-145 --sequence (use cheque number)
            output.Append(String.Empty.PadLeft(1)); //146-146 --filler
            output.Append(FormatMoney(Amount).PadLeft(13)); //147-159 --pay date
            output.Append(" "); //160-160 -- space /*pad with spaces*/
            output.Append(" "); //161-161 -- Void Indicator  /*pad with spaces*/
            output.Append(String.Empty.PadLeft(1)); //161-161 --filler 

            if (output.Length != 162) throw new Exception("error forming 'I' record");

            return output.ToString();
        }
        #endregion

        #region F
        protected override String F()
        {
            StringBuilder output = new StringBuilder();
            output.Append('F');  //01-01 --Record Type
            output.Append(String.Empty.PadLeft(15)); //02-16 --Job Description (left blank as sample was also blank)
            output.Append(String.Empty.PadLeft(35)); //17-51 --filler
            output.Append(City.PadRight(30)); //52-81   --Employee Address 2 /*pad with spaces*/
            output.Append(String.Empty.PadLeft(81)); //82-162 --filler

            if (output.Length != 162) throw new Exception("error forming 'F' record");

            return output.ToString();
        }
        protected String M(String message)
        {
            StringBuilder output = new StringBuilder();
            output.Append('M');  //01-01 --Record Type
            output.Append(message.PadRight(60).Substring(0, 60)); //02-61 --MESSAGE 
            output.Append(String.Empty.PadLeft(101)); //62-162 --filler

            if (output.Length != 162) throw new Exception("error forming 'F' record");

            return output.ToString();
        }
        #endregion

        public override string ToString()
        {
            StringBuilder output = new StringBuilder();

            //header
            output.AppendLine(HAT());

            //C
            output.AppendLine(C());

            //I
            output.AppendLine(I());

            //F
            output.AppendLine(F());

            //K
            output.AppendLine(K());

            //E
            //decimal ytdTotalEarnings = 0;
            //foreach (DynamicsMasterPayDetail detail in Earnings)
            //{
            output.AppendLine(E());
            //    ytdTotalEarnings += detail.PaycodeYTDDollars;
            //}

            //D
            //decimal totalDeductions = 0;
            //decimal ytdTotalDeductions = 0;
            //foreach (DynamicsMasterPayDetail detail in Deductions)
            //{
            output.AppendLine(D());
            //    totalDeductions += detail.PaycodeCurrentDollars;
            //    ytdTotalDeductions += detail.PaycodeYTDDollars;
            //}

            //B
            //foreach (DynamicsMasterPayDetail detail in Benefits)
            //{
            output.AppendLine(B());
            //}

            //X
            //skipped 

            //M
            output.AppendLine(M(Employee.ChequeName));
            output.AppendLine(M(String.Format("ORDER# {0}", OrderNumber)));

            //S
            output.AppendLine(S());

            //Y
            output.AppendLine(Y());

            //A
            //skipped 

            return output.ToString();
        }
    }
}