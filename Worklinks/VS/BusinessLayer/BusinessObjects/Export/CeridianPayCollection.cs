﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects.Export
{
    public class CeridianPayCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<CeridianPay>
    {
        public override string ToString()
        {
            StringBuilder output = new StringBuilder();
            if (this.Count > 0)
                output.AppendLine(this[0].Header);
            foreach (CeridianPay cheque in this)
            {
                output.Append(cheque.ToString());
            }

            return output.ToString();
        }
    }
}
