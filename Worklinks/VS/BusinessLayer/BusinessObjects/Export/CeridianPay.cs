﻿using System;
using System.Text;

namespace WorkLinks.BusinessLayer.BusinessObjects.Export
{
    public class CeridianPay : PayrollMasterPayment
    {
        #region properties
        protected virtual String CeridianExportType
        {
            get
            {
                throw new Exception("CeridianExportType must be overriden");
            }
        }
        public Address CompanyAddress { get; set; }
        public String CompanyNumber { get; set; }
        public String CompanyName { get; set; }
        public short PeriodNumber { get; set; }
        public DateTime PeriodDate { get; set; }
        public int PageNumber { get; set; }
        public EmployeeSummary Employee { get; set; }
        //public DynamicsMasterPayDetailCollection Earnings {get;set;}
        //public DynamicsMasterPayDetailCollection Benefits {get;set;}
        //public DynamicsMasterPayDetailCollection Deductions { get; set; }
        #endregion

        protected String FormatDate(DateTime date)
        {
            return date.ToString("yyyy/MM/dd");
        }
        protected String FormatMoney(Decimal money)
        {
            return String.Format("{0:#,##0.00}", money);
        }

        #region calc properties
        protected String FormattedCompanyNumber { get { return CompanyNumber.PadRight(6, ' '); } }
        #endregion

        #region header
        public String Header
        {
            get
            {
                StringBuilder output = new StringBuilder();
                output.Append("FILEID"); //01-06 --fileid
                output.Append(' '); //07-07 --filler
                output.Append(FormattedCompanyNumber); //08-13 --company number
                output.Append(' '); //14-14 --filler
                output.Append(CeridianExportType.PadRight(8, ' '));
                return output.ToString();
            }
        }
        #endregion

        #region C
        protected String C()
        {
            StringBuilder output = new StringBuilder();
            output.Append('C');  //01-01 --Record Type
            output.Append(FormattedCompanyNumber); //02-07 --company number
            output.Append(CompanyName.PadRight(34).Substring(0, 34)); //08-41 --company name
            output.Append((CompanyAddress.AddressLine1 ?? String.Empty).PadRight(30).Substring(0, 30)); //42-71   --Company Address 1
            output.Append((CompanyAddress.City ?? String.Empty).PadRight(30).Substring(0, 30)); //72-101  --Company Address 2
            output.Append((CompanyAddress.ProvinceStateCode ?? String.Empty).PadRight(30).Substring(0, 30)); //102-131 --Company Address 3
            output.Append((CompanyAddress.PostalZipCode ?? String.Empty).PadRight(7).Substring(0, 7)); //132-138 --Company Postal Code
            output.Append("4".PadRight(3, '0')); //139-141 --No. of lines (hard coded for now)
            output.Append("1"); //142-142 --constant
            output.Append(PeriodNumber.ToString().PadLeft(2, '0')); //143-144 --pay number
            output.Append(FormatDate(PeriodDate)); //145-154 --period date (using period end date for now)
            output.Append('T');  //155-155 --hard code 'T'
            output.Append(PageNumber.ToString().PadLeft(5)); //156-160 --page number  
            output.Append(' ');  //161-161 --logo (L='print logo')
            output.Append(' ');  //162-162 --Record Type (I='add postage')

            if (output.Length != 162) throw new Exception("error forming 'C' record");

            return output.ToString();
        }
        #endregion

        #region I
        protected virtual String I()
        {
            StringBuilder output = new StringBuilder();
            output.Append('I');  //01-01 --Record Type
            output.Append(String.Empty.PadLeft(4)); //02-05 --Division/Department (left blank as sample was also blank)
            output.Append(String.Empty.PadLeft(1)); //06-06 --filler
            output.Append(Employee.EmployeeNumber.PadLeft(10)); //07-16 --Employee Number
            output.Append(Employee.ChequeName.PadRight(30).Substring(0, 30)); //17-46 --Employee Name
            output.Append(String.Empty.PadRight(34).Substring(0, 34)); //47-80   --Employee Address 1 /*pad with spaces*/
            output.Append(String.Empty.PadRight(30).Substring(0, 30)); //81-110   --Employee Address 3 /*pad with spaces*/
            output.Append(String.Empty.PadRight(7).Substring(0, 7)); //111-117   --Employee Postal Code /*pad with spaces*/
            output.Append(FormatDate(PeriodDate)); //118-127 --period end date
            output.Append(FormatDate(ChequeDate)); //128-137 --pay date
            output.Append(ChequeNumber.PadLeft(8).Substring(0, 8)); //138-145 --sequence (use cheque number)
            output.Append(String.Empty.PadLeft(1)); //146-146 --filler
            output.Append(FormatMoney(Amount).PadLeft(13)); //147-159 --pay date
            output.Append(" "); //160-160 -- space /*pad with spaces*/
            output.Append(" "); //161-161 -- Void Indicator  /*pad with spaces*/
            output.Append(String.Empty.PadLeft(1)); //161-161 --filler 

            if (output.Length != 162) throw new Exception("error forming 'I' record");

            return output.ToString();
        }
        #endregion

        #region F
        protected virtual String F()
        {
            StringBuilder output = new StringBuilder();
            output.Append('F');  //01-01 --Record Type
            output.Append(String.Empty.PadLeft(15)); //02-16 --Job Description (left blank as sample was also blank)
            output.Append(String.Empty.PadLeft(35)); //17-51 --filler
            output.Append(String.Empty.PadRight(30)); //52-81   --Employee Address 2 /*pad with spaces*/
            output.Append(String.Empty.PadLeft(81)); //82-162 --filler

            if (output.Length != 162) throw new Exception("error forming 'F' record");

            return output.ToString();
        }
        #endregion

        #region K
        protected String K()  //using for sort order only
        {
            StringBuilder output = new StringBuilder();
            output.Append('K');  //01-01 --Record Type
            output.Append(String.Empty.PadLeft(4)); //02-05 --control 3
            output.Append(String.Empty.PadLeft(1)); //06-06 --filler 
            output.Append(String.Empty.PadLeft(4)); //07-10 --control 4 
            output.Append(String.Empty.PadLeft(1)); //11-11 --filler 
            output.Append(String.Empty.PadLeft(4)); //12-15 --control 5
            output.Append(String.Empty.PadLeft(1)); //16-16 --filler 
            output.Append(String.Empty.PadLeft(4)); //17-20 --control 6 
            output.Append(String.Empty.PadLeft(1)); //21-21 --filler 
            output.Append(String.Empty.PadLeft(10)); //22-31 --Employee Number  /*pad with spaces*/
            output.Append(String.Empty.PadLeft(131)); //32-162

            if (output.Length != 162) throw new Exception("error forming 'K' record");

            return output.ToString();
        }
        #endregion

        #region E
        protected String E()
        {
            StringBuilder output = new StringBuilder();
            output.Append('E');  //01-01 --Record Type
            output.Append(String.Empty.PadRight(13)); //02-14 --Description
            output.Append("0".PadLeft(9, '0')); //15-23 --Hours
            //            output.Append(Math.Round(detail.PaycodeCurrentDollars / detail.PaycodeCurrentUnits, 2, System.MidpointRounding.AwayFromZero).ToString("0.00").PadLeft(8)); //24-31 --Rate ****BOB**** this is hacked, ok? (not sure what the decimal will do, sample is 4)
            output.Append(String.Empty.PadLeft(8)); //24-31 --Rate (left blank)
            output.Append("0".PadLeft(14, '0')); //32-45 --current amount
            output.Append("0".PadLeft(14, '0')); //46-59 --ytd amount
            output.Append(String.Empty.PadLeft(103)); //60-162

            if (output.Length != 162) throw new Exception("error forming 'E' record");

            return output.ToString();
        }
        #endregion

        #region D
        protected String D()
        {
            StringBuilder output = new StringBuilder();
            output.Append('D');  //01-01 --Record Type
            output.Append(String.Empty.PadRight(13)); //02-14 --Description
            output.Append("0".PadLeft(13, '0')); //15-27 --current amount
            output.Append("0".PadLeft(14, '0')); //28-41 --ytd amount
            output.Append(String.Empty.PadLeft(121)); //42-162

            if (output.Length != 162) throw new Exception("error forming 'D' record");

            return output.ToString();
        }
        #endregion

        #region B
        protected String B()
        {
            StringBuilder output = new StringBuilder();
            output.Append('B');  //01-01 --Record Type
            output.Append(String.Empty.PadRight(13)); //02-14 --Description
            output.Append("0".PadLeft(13, '0')); //15-27 --current amount
            output.Append("0".PadLeft(14, '0')); //28-41 --ytd amount
            output.Append(String.Empty.PadLeft(121)); //42-162

            if (output.Length != 162) throw new Exception("error forming 'B' record");

            return output.ToString();
        }
        #endregion

        #region X
        //skipped
        #endregion

        #region M
        //skipped
        #endregion

        #region S
        protected String S()
        {
            StringBuilder output = new StringBuilder();
            output.Append('S');  //01-01 --Record Type
            output.Append("0".PadLeft(14, '0')); //02-15 --current gross  
            output.Append("0".PadLeft(14, '0')); //16-29 --current deductions  
            output.Append(Amount.ToString("0.00").PadLeft(14)); //30-43 --current net pay
            output.Append(String.Empty.PadLeft(119)); //44-162

            if (output.Length != 162) throw new Exception("error forming 'S' record");

            return output.ToString();
        }
        #endregion

        #region Y
        protected String Y()
        {
            StringBuilder output = new StringBuilder();
            output.Append('Y');  //01-01 --Record Type
            output.Append("0".PadLeft(14, '0')); //02-15 --ytd gross 
            output.Append("0".PadLeft(14, '0')); //16-29 --ytd deductions 
            output.Append("0".PadLeft(14, '0')); //30-43 --ytd net pay
            output.Append(String.Empty.PadLeft(119)); //44-162

            if (output.Length != 162) throw new Exception("error forming 'S' record");

            return output.ToString();
        }
        #endregion

        #region A
        //skipeed
        #endregion
    }
}