﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects.Export
{
    public class CeridianCheque : CeridianPay
    {
        protected override string CeridianExportType
        {
            get
            {
                return "PSC";
            }
        }

        #region header
        protected String HAT()
        {
            StringBuilder output = new StringBuilder();
            output.Append('^');  //01-01 --Record Type
            output.Append(String.Empty.PadLeft(1)); //02-02 --filler
            output.Append(PayrollProcessId.ToString().PadLeft(8, '0'));//03-10 --filler
            output.Append(String.Empty.PadLeft(10)); //11-20 --filler
            output.Append(FormattedCompanyNumber); //21-26 --company number
            output.Append(String.Empty.PadLeft(136)); //27-162 --filler

            if (output.Length != 162) throw new Exception("error forming 'Header' record");

            return output.ToString();
        }
        #endregion



        

        public override string ToString()
        {
            StringBuilder output = new StringBuilder();

            //header
            output.AppendLine(HAT());

            //C
            output.AppendLine(C());

            //I
            output.AppendLine(I());

            //F
            output.AppendLine(F());

            //K
            output.AppendLine(K());

            //E
            //decimal ytdTotalEarnings = 0;
            //foreach (DynamicsMasterPayDetail detail in Earnings)
            //{
                output.AppendLine(E());
            //    ytdTotalEarnings += detail.PaycodeYTDDollars;
            //}

            //D
            //decimal totalDeductions = 0;
            //decimal ytdTotalDeductions = 0;
            //foreach (DynamicsMasterPayDetail detail in Deductions)
            //{
                output.AppendLine(D());
            //    totalDeductions += detail.PaycodeCurrentDollars;
            //    ytdTotalDeductions += detail.PaycodeYTDDollars;
            //}

            //B
            //foreach (DynamicsMasterPayDetail detail in Benefits)
            //{
                output.AppendLine(B());
            //}

            //X
            //skipped 

            //M
            //skipped 

            //S
            output.AppendLine(S());

            //Y
            output.AppendLine(Y());

            //A
            //skipped 

            return output.ToString();
        }


        #region constructor

        #endregion
    }
}
