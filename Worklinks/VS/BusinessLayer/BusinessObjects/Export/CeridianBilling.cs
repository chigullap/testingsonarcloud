﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkLinks.BusinessLayer.BusinessObjects.Export
{
    public class CeridianBilling : PayrollMasterCollection
    {
        public enum ProcessType
        {
            PROD = 1,
            TEST = 2,
            VOID = 3
        }

        public enum PayFrequency
        {
            Weekly = 1,
            BiWeekly = 2,
            SemiMonthly = 3,
            Monthly = 4
        }

        #region fields
        private const String _ceridianExportType = "BIL";
        private const short _howPaidDeposit = 1;
        private const short _howPaidCheque = 2;

        private String _companyNumber = null;
        private ProcessType _processType = ProcessType.TEST;
        private PayFrequency _payFrequency = PayFrequency.SemiMonthly;
        private short _periodRunNumber = -1;
        private String _businessTaxNumber = null;
        private DateTime _wcbStartDate = DateTime.MinValue;
        private long _onEFTAmount = 0;
        #endregion

        #region calc fields
        private int _numberOfCheques = 0;
        private int _numberOfDeposits = 0;
        private int _numberOfSecondaryDeposits = 0;
        private Decimal _netChequeAmount = 0;
        private Decimal _netDepositAmount = 0;
        private Decimal _netSecondaryDepositAmount = 0;
        private Decimal _totalGrossAmount = 0;
        Dictionary<String, Decimal> _employeeCPPAmount = null;
        Dictionary<String, Decimal> _federalTaxAmount = null;
        Dictionary<String, Decimal> _provincialTaxAmount = null;
        Dictionary<String, Decimal> _employeeEITaxAmount = null;
        Dictionary<String, Decimal> _provincialParentalInsurancePlanPremiumAmount = null;
        Dictionary<String, Decimal> _employerHealthTaxAssessable = null;
        Dictionary<String, int> _employeeCount = null;
        Dictionary<String, Decimal> _wcbBase = null;
        Dictionary<String, Decimal> _wcbAssessed = null;
        Dictionary<PayFrequency, String> _payFrequencyList = null;
        #endregion

        #region calc functions
        public String Nine11V99(Decimal value)
        {
            const int stringLength = 13;
            String val = Math.Round(Math.Abs(value) * 100, 0, System.MidpointRounding.AwayFromZero).ToString().PadLeft(stringLength, '0');
            if (value < 0)
            {
                //get parts
                char endNumber = val[stringLength - 1];
                char endChar = ' ';

                switch (endNumber)
                {
                    case '0':
                        endChar = ' ';
                        break;
                    case '1':
                        endChar = '!';
                        break;
                    case '2':
                        endChar = '"';
                        break;
                    case '3':
                        endChar = '#';
                        break;
                    case '4':
                        endChar = '$';
                        break;
                    case '5':
                        endChar = '%';
                        break;
                    case '6':
                        endChar = '&';
                        break;
                    case '7':
                        endChar = '\'';
                        break;
                    case '8':
                        endChar = '(';
                        break;
                    case '9':
                        endChar = ')';
                        break;
                }

                val = val.Substring(0, stringLength - 1) + endChar;
            }

            return val;
        }
        public String Nine7V99(Decimal value)
        {
            return value < 0 ? "-" : "" + Math.Round(value * 100, 0, System.MidpointRounding.AwayFromZero).ToString().PadLeft(value < 0 ? 8 : 9, '0');
        }
        public Decimal GetEmployeeCPPAmount(String province)
        {
            return _employeeCPPAmount.ContainsKey(province) ? _employeeCPPAmount[province] : 0;
        }
        public Decimal GetFederalTaxAmount(String province)
        {
            return _federalTaxAmount.ContainsKey(province) ? _federalTaxAmount[province] : 0;
        }
        public Decimal GetProvincialTaxAmount(String province)
        {
            return _provincialTaxAmount.ContainsKey(province) ? _provincialTaxAmount[province] : 0;
        }
        public Decimal GetEmployeeEITaxAmount(String province)
        {
            return _employeeEITaxAmount.ContainsKey(province) ? _employeeEITaxAmount[province] : 0;
        }
        public Decimal GetProvincialParentalInsurancePlanPremiumAmount(String province)
        {
            return _provincialParentalInsurancePlanPremiumAmount.ContainsKey(province) ? _provincialParentalInsurancePlanPremiumAmount[province] : 0;
        }
        public Decimal GetEmployerHealthTaxAssessable(String province)
        {
            return _employerHealthTaxAssessable.ContainsKey(province) ? _employerHealthTaxAssessable[province] : 0;
        }
        public int EmployeeCount(String province)
        {
            return _employeeCount.ContainsKey(province) ? _employeeCount[province] : 0;
        }
        public Decimal WcbBase(String wsibCode)
        {
            return _wcbBase.ContainsKey(wsibCode) ? _wcbBase[wsibCode] : 0;
        }
        public Decimal WcbAssessed(String wsibCode)
        {
            return _wcbAssessed.ContainsKey(wsibCode) ? _wcbAssessed[wsibCode] : 0;
        }
        #endregion

        #region properties
        public PayrollMasterPaymentCollection EFTCollection { get; set; }
        public PayrollMasterPaycodeCollection Garnishments { get; set; }
        public HealthTaxCollection ProvinceRemittances { get; set; }
        public WSIBSummaryCollection WSIBSummarys { get; set; }
        #endregion

        #region calc columns
        private long PayrollProcessId { get { return this[0].PayrollProcessId; } }
        private String FormattedCompanyNumber { get { return _companyNumber.PadRight(6, ' ').Substring(0, 6); } } //added substring so it wont blow up if this field is greater than 6 chars
        private String FormattedNetChequeAmount { get { return Nine11V99(_netChequeAmount); } }
        private String FormattedNetDepositAmount { get { return Nine11V99(_netDepositAmount); } }
        private String FormattedSecondaryDepositAmount { get { return Nine11V99(_netSecondaryDepositAmount); } }
        private DateTime PaymentDate { get { return this[0].ChequeDate; } }
        private String FormattedPaymentDate { get { return String.Format("{0:yy}{1:000}", PaymentDate, PaymentDate.DayOfYear); } }
        public DateTime StartDate { get { return this[0].StartDate; } }
        public DateTime PeriodEndDate { get { return this[0].CutoffDate; } }
        public String FormattedPeriodEndDate { get { return String.Format("{0:yy}{1:000}", PeriodEndDate, PeriodEndDate.DayOfYear); } }
        public int TotalEmployeeCount { get { return this.Count; } }
        #endregion

        public PayrollMasterPaymentCollection GetEFTData(String employeeNumber)
        {
            PayrollMasterPaymentCollection rtn = new PayrollMasterPaymentCollection();

            foreach (PayrollMasterPayment eft in EFTCollection)
            {
                if (eft.EmployeeNumber.Equals(employeeNumber))
                    rtn.Add(eft);
            }

            return rtn;
        }

        #region billing lines

        #region header
        private String Header
        {
            get
            {
                StringBuilder output = new StringBuilder();
                output.Append("FILEID"); //01-06 --fileid
                output.Append(' '); //07-07 --filler
                output.Append(FormattedCompanyNumber); //08-13 --company number
                output.Append(' '); //14-14 --filler
                output.Append(_ceridianExportType.PadRight(8, ' '));
                return output.ToString();
            }
        }
        #endregion

        #region block 1
        private String CHEQ
        {
            get
            {
                StringBuilder output = new StringBuilder();
                output.Append('1');  //01-01 --card type
                output.Append(String.Empty.PadLeft(5)); //02-06 --filler
                output.Append(FormattedCompanyNumber); //07-12 --company number
                output.Append(_numberOfCheques.ToString().PadLeft(6, '0'));//13-18 --number of cheques
                output.Append(String.Empty.PadRight(34));//19-52 --filler
                output.Append("CHEQ");//53-56 --record type identifier
                output.Append("A0A0UPH1");//57-64 --constant
                output.Append(String.Empty.PadRight(16));

                if (output.Length != 80) throw new Exception("error forming 'CHEQ' record");

                return output.ToString();
            }
        }
        private String DDEP
        {
            get
            {
                StringBuilder output = new StringBuilder();
                output.Append('1');  //01-01 --card type
                output.Append(String.Empty.PadLeft(5)); //02-06 --filler
                output.Append(FormattedCompanyNumber); //07-12 --company number
                output.Append(_numberOfDeposits.ToString().PadLeft(6, '0'));//13-18 --number of deposits
                output.Append(String.Empty.PadRight(34));//19-52 --filler
                output.Append("DDEP");//53-56 --record type identifier
                output.Append("A6A6VPH1");//57-64 --constant
                output.Append(String.Empty.PadRight(16));

                if (output.Length != 80) throw new Exception("error forming 'DDEP' record");

                return output.ToString();
            }
        }
        #endregion

        #region block 2
        private String CNET
        {
            get
            {
                StringBuilder output = new StringBuilder();
                output.Append('2');  //01-01 --card type
                output.Append(String.Empty.PadLeft(5)); //02-06 --filler
                output.Append(FormattedCompanyNumber); //07-12 --company number
                output.Append(FormattedNetChequeAmount);//13-25 --total cheque amount  ****BOB**** what is a secondary /**BOB TO CHECK THIS*/ deposit for this requirement (does it matter?)
                output.Append((_numberOfCheques).ToString().PadLeft(11, '0'));//26-36 --number of cheques
                output.Append(String.Empty.PadRight(2, '0'));//37-38 --filler
                output.Append(String.Empty.PadRight(1));//39-39 --filler
                output.Append(FormattedPaymentDate);//40-44 payment date (julian)
                output.Append(String.Empty.PadLeft(8));//45-52 --filler
                output.Append("NET ");//53-56 --record type identifier
                output.Append((char)0x37); output.Append((char)0xFB);//57-58 - WTF?
                output.Append('C');//59-59 - cheque or deposit indicator
                output.Append('1');//60-60 - constant
                output.Append(String.Empty.PadLeft(20));//61-80 --filler

                if (output.Length != 80) throw new Exception("error forming 'CNET' record");

                return output.ToString();
            }
        }
        private String DNET
        {
            get
            {
                StringBuilder output = new StringBuilder();
                output.Append('2');  //01-01 --card type
                output.Append(String.Empty.PadLeft(5)); //02-06 --filler
                output.Append(FormattedCompanyNumber); //07-12 --company number
                output.Append(FormattedNetDepositAmount);//13-25 --total deposit amount  
                output.Append((_numberOfDeposits).ToString().PadLeft(11, '0'));//26-36 --number of deposits
                output.Append(String.Empty.PadRight(2, '0'));//37-38 --filler
                output.Append(String.Empty.PadRight(1));//39-39 --filler
                output.Append(FormattedPaymentDate);//40-44 payment date (julian)
                output.Append(String.Empty.PadLeft(8));//45-52 --filler
                output.Append("NET ");//53-56 --record type identifier
                output.Append("7û");//57-58 - WTF?
                output.Append('D');//59-59 - cheque or deposit indicator
                output.Append('1');//60-60 - constant
                output.Append(String.Empty.PadLeft(20));//61-80 --filler

                if (output.Length != 80) throw new Exception("error forming 'DNET' record");

                return output.ToString();
            }
        }
        #endregion

        #region block 3
        private String D
        {
            get
            {
                StringBuilder output = new StringBuilder();
                output.Append('1');  //01-01 --card type
                output.Append(String.Empty.PadLeft(5)); //02-06 --filler
                output.Append(FormattedCompanyNumber); //07-12 --company number
                output.Append(_numberOfSecondaryDeposits.ToString().PadLeft(6, '0'));//13-18 --number of secondary deposits
                output.Append(String.Empty.PadRight(34));//19-52 --filler
                output.Append("D");//53-53 --record type identifier
                output.Append("998");//54-56 --HED Number
                output.Append("A6A6VPH1");//57-64 --constant
                output.Append(String.Empty.PadRight(16));

                if (output.Length != 80) throw new Exception("error forming 'D' record");

                return output.ToString();
            }
        }


        private String N
        {
            get
            {
                StringBuilder output = new StringBuilder();
                output.Append('2');  //01-01 --card type
                output.Append(String.Empty.PadLeft(5)); //02-06 --filler
                output.Append(FormattedCompanyNumber); //07-12 --company number
                output.Append(FormattedSecondaryDepositAmount);//13-25 --total secondary deposit amount  
                output.Append((_numberOfSecondaryDeposits).ToString().PadLeft(11, '0'));//26-36 --number secondary deposits
                output.Append(String.Empty.PadRight(2, '0'));//37-38 --filler
                output.Append(String.Empty.PadRight(1));//39-39 --filler
                output.Append(FormattedPaymentDate);//40-44 payment date (julian)
                output.Append(String.Empty.PadLeft(8));//45-52 --filler
                output.Append("N");//53-53 --record type identifier
                output.Append("998");//54-56 --HED Number
                output.Append('7');//57-57 - constant
                output.Append("998");//58-60 --HED Number
                output.Append(String.Empty.PadLeft(20));//61-80 --filler

                if (output.Length != 80) throw new Exception("error forming 'N' record");

                return output.ToString();
            }
        }

        #endregion

        #region block 4
        private String RCT
        {
            get
            {
                StringBuilder output = new StringBuilder();
                output.Append('1');  //01-01 --card type
                output.Append(String.Empty.PadLeft(5)); //02-06 --filler
                output.Append(FormattedCompanyNumber); //07-12 --company number
                output.Append(String.Empty.PadRight(26));//13-38 --filler
                output.Append(String.Empty.PadRight(1));//39-39 --filler
                output.Append(FormattedPaymentDate);//40-44 payment date (julian)
                output.Append(String.Empty.PadRight(8));//45-52 --filler
                output.Append("RCT ");//53-56 --record type identifier
                output.Append(String.Empty.PadRight(3));//57-59 --filler
                output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
                output.Append(String.Empty.PadRight(1));//61-61 --filler
                output.Append(String.Empty.PadRight(2));//62-63 --filler
                output.Append(_businessTaxNumber.PadRight(15));//64-78 --RCT number 
                output.Append(String.Empty.PadRight(2));//79-80 --filler

                if (output.Length != 80) throw new Exception("error forming 'RCT' record");

                return output.ToString();
            }
        }

        private String STAT2(String province)
        {
            StringBuilder output = new StringBuilder();
            output.Append('2');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(Nine11V99(GetFederalTaxAmount(province)));//13-25 --federal tax
            output.Append(Nine11V99(GetEmployeeCPPAmount(province)));//26-38 --cpp/qpp
            output.Append(String.Empty.PadLeft(1)); //39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append("STAT");//53-56 --record type identifier
            output.Append('2');//57-57 --constant
            output.Append(province.PadRight(2));//58-59 --province
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(20));//61-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'STAT2' record");

            return output.ToString();
        }

        //ADJ2 //skipped for now
        #endregion

        #region block 5
        private String STAT3(String province)
        {
            StringBuilder output = new StringBuilder();
            output.Append('2');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(Nine11V99((GetProvincialTaxAmount(province))));//13-25 --privincial tax  
            output.Append(Nine11V99(GetEmployeeEITaxAmount(province)));//26-38 --employee EI contribution
            output.Append(String.Empty.PadLeft(1)); //39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append("STAT");//53-56 --record type identifier
            output.Append('3');//57-57 --constant
            output.Append(province.PadRight(2));//58-59 --province
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(20));//61-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'STAT3' record");

            return output.ToString();
        }

        //ADJ3 skipped

        private String STAT4(String province)
        {
            StringBuilder output = new StringBuilder();
            output.Append('2');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(String.Empty.PadLeft(13, '0'));//13-25 --filler
            output.Append(Nine11V99(GetProvincialParentalInsurancePlanPremiumAmount(province)));//26-38 --Employee PPIP Contribution
            output.Append(String.Empty.PadLeft(1)); //39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append("STAT");//53-56 --record type identifier
            output.Append('4');//57-57 --constant
            output.Append(province.PadRight(2));//58-59 --province
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(20));//61-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'STAT4' record");

            return output.ToString();
        }
        //ADJ4 skipped

        #endregion

        #region block6
        private String HFQC(String province, String provinceIdentifier)
        {
            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(String.Empty.PadLeft(13, '0'));//13-25 --Gross Exemption (left blank for now)
            output.Append(String.Empty.PadLeft(13, '0'));//26-38 --QHF Rate sounds like it isn't needed
            output.Append(String.Empty.PadLeft(1)); //39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append(String.Format("R#{0}", provinceIdentifier));//53-56 --record type identifier
            output.Append(String.Empty.PadRight(3));//57-59 --filler
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(3));//61-63 --filler
            output.Append(ProvinceRemittances.GetProvinceRemittance(province).HealthRemittanceAccountNumber.PadLeft(14).Substring(0, 14));//64-77 --Province Tax Account Number
            output.Append(String.Empty.PadLeft(1)); //78-78 --filler
            output.Append("M"); //79-79 --Remit Freq (M=monthly)
            output.Append(String.Empty.PadLeft(1)); //80-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'HFQC' record");

            return output.ToString();
        }
        private String HF2QC(String province, String provinceIdentifier)
        {
            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(String.Empty.PadLeft(7));//13-19 --filler
            output.Append(String.Empty.PadLeft(6));//20-25 --filler
            output.Append(String.Empty.PadLeft(13));//26-38 --filler
            output.Append(String.Empty.PadLeft(1));//39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append(String.Format("R#{0}", provinceIdentifier));//53-56 --record type identifier
            output.Append("9");//57-57 --constant
            output.Append(String.Empty.PadRight(2));//58-59 --filler
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(3));//61-63 --filler
            output.Append(ProvinceRemittances.GetProvinceRemittance(province).HealthRemittanceAccountNumber.PadLeft(14).Substring(0, 14));//64-77 --Province Tax Account Number
            output.Append(String.Empty.PadLeft(1)); //78-78 --filler
            output.Append("R"); //79-79 --Remit Indicator 
            output.Append(String.Empty.PadLeft(1)); //80-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'HF2QC' record");

            return output.ToString();
        }
        private String STATQC(String province, String xxxRecordIndicator)
        {
            StringBuilder output = new StringBuilder();
            output.Append('2');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(Nine11V99(GetEmployerHealthTaxAssessable(province)));//13-25 --Gross Amount 
            output.Append(String.Empty.PadLeft(13));//26-38 --filler
            output.Append(String.Empty.PadLeft(1));//39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append("STAT");//53-56 --record type identifier
            output.Append(xxxRecordIndicator);//57-59 --constant
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(20));//61-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'STATQC' record");

            return output.ToString();
        }
        #endregion

        #region block7
        private String HFON(String province, String provinceIdentifier)
        {
            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(_onEFTAmount.ToString().PadLeft(13, '0'));//13-25 --Gross Exemption (left blank for now)/***HACK***/
            output.Append(String.Empty.PadLeft(13, '0'));//26-38 --QHF Rate sounds like it isn't needed
            output.Append(String.Empty.PadLeft(1)); //39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append(String.Format("R#{0}", provinceIdentifier));//53-56 --record type identifier
            output.Append(String.Empty.PadRight(3));//57-59 --filler
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(3));//61-63 --filler
            String healthRemittanceHealthAccountNumber = String.Empty;
            if (ProvinceRemittances.GetProvinceRemittance(province) != null)
                healthRemittanceHealthAccountNumber = ProvinceRemittances.GetProvinceRemittance(province).HealthRemittanceAccountNumber;
            output.Append(healthRemittanceHealthAccountNumber.PadLeft(15));//64-78 --Province Tax Account Number
            output.Append("M"); //79-79 --Remit Freq (M=monthly)
            output.Append(String.Empty.PadLeft(1)); //80-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'HFON' record");

            return output.ToString();
        }
        private String HF2ON(String province, String provinceIdentifier)
        {
            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(String.Empty.PadLeft(7));//13-19 --filler
            output.Append(String.Empty.PadLeft(6));//20-25 --filler
            output.Append(String.Empty.PadLeft(13));//26-38 --filler
            output.Append(String.Empty.PadLeft(1));//39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append(String.Format("R#{0}", provinceIdentifier));//53-56 --record type identifier
            output.Append("9");//57-57 --constant
            output.Append(String.Empty.PadRight(2));//58-59 --filler
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(3));//61-63 --filler
            String healthRemittanceHealthAccountNumber = String.Empty;
            if (ProvinceRemittances.GetProvinceRemittance(province) != null)
                healthRemittanceHealthAccountNumber = ProvinceRemittances.GetProvinceRemittance(province).HealthRemittanceAccountNumber;
            output.Append(healthRemittanceHealthAccountNumber.PadLeft(15));//64-78 --Province Tax Account Number
            output.Append("R"); //79-79 --Remit Indicator 
            output.Append(String.Empty.PadLeft(1)); //80-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'HF2ON' record");

            return output.ToString();
        }
        private String STATON(String province, String xxxRecordIndicator)
        {
            StringBuilder output = new StringBuilder();
            output.Append('2');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(Nine11V99(GetEmployerHealthTaxAssessable(province)));//13-25 --Gross Amount 
            output.Append(String.Empty.PadLeft(13));//26-38 --filler
            output.Append(String.Empty.PadLeft(1));//39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append("STAT");//53-56 --record type identifier
            output.Append(xxxRecordIndicator);//57-59 --constant
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(20));//61-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'STATON' record");

            return output.ToString();
        }
        #endregion

        #region block8
        private String HFMB(String province, String provinceIdentifier)
        {
            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(String.Empty.PadLeft(13, '0'));//13-25 --Gross Exemption (left blank for now)
            output.Append(String.Empty.PadLeft(13, '0'));//26-38 --QHF Rate sounds like it isn't needed
            output.Append(String.Empty.PadLeft(1)); //39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append(String.Format("R#{0}", provinceIdentifier));//53-56 --record type identifier
            output.Append(String.Empty.PadRight(3));//57-59 --filler
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(3));//61-63 --filler
            output.Append(ProvinceRemittances.GetProvinceRemittance(province).HealthRemittanceAccountNumber.PadLeft(15));//64-78 --Province Tax Account Number
            output.Append("M"); //79-79 --Remit Freq (M=monthly)
            output.Append(String.Empty.PadLeft(1)); //80-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'HFMB' record");

            return output.ToString();
        }
        private String HF2MB(String province, String provinceIdentifier)
        {
            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(String.Empty.PadLeft(7));//13-19 --filler
            output.Append(String.Empty.PadLeft(6));//20-25 --filler
            output.Append(String.Empty.PadLeft(13));//26-38 --filler
            output.Append(String.Empty.PadLeft(1));//39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append(String.Format("R#{0}", provinceIdentifier));//53-56 --record type identifier
            output.Append("9");//57-57 --constant
            output.Append(String.Empty.PadRight(2));//58-59 --filler
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(3));//61-63 --filler
            output.Append(ProvinceRemittances.GetProvinceRemittance(province).HealthRemittanceAccountNumber.PadLeft(15));//64-78 --Province Tax Account Number
            output.Append(" "); //79-79 --Remit Indicator 
            output.Append(String.Empty.PadLeft(1)); //80-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'HF2MB' record");

            return output.ToString();
        }
        private String STATMB(String province, String xxxRecordIndicator)
        {
            StringBuilder output = new StringBuilder();
            output.Append('2');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(Nine11V99(GetEmployerHealthTaxAssessable(province)));//13-25 --Gross Amount 
            output.Append(String.Empty.PadLeft(13));//26-38 --filler
            output.Append(String.Empty.PadLeft(1));//39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append("STAT");//53-56 --record type identifier
            output.Append(xxxRecordIndicator);//57-59 --constant
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(20));//61-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'STATMB' record");

            return output.ToString();
        }
        #endregion

        #region block9
        private String HFNL(String province, String provinceIdentifier)
        {
            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(String.Empty.PadLeft(13, '0'));//13-25 --Gross Exemption (left blank for now)
            output.Append(String.Empty.PadLeft(13, '0'));//26-38 --QHF Rate sounds like it isn't needed
            output.Append(String.Empty.PadLeft(1)); //39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append(String.Format("R#{0}", provinceIdentifier));//53-56 --record type identifier
            output.Append(String.Empty.PadRight(3));//57-59 --filler
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(3));//61-63 --filler
            output.Append(ProvinceRemittances.GetProvinceRemittance(province).HealthRemittanceAccountNumber.PadLeft(13));//64-76 --Province Tax Account Number
            output.Append(String.Empty.PadLeft(2)); //77-78 --filler
            output.Append("M"); //79-79 --Remit Freq (M=monthly)
            output.Append(String.Empty.PadLeft(1)); //80-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'HFNL' record");

            return output.ToString();
        }
        private String HF2NL(String province, String provinceIdentifier)
        {
            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(String.Empty.PadLeft(7));//13-19 --filler
            output.Append(String.Empty.PadLeft(6));//20-25 --filler
            output.Append(String.Empty.PadLeft(13));//26-38 --filler
            output.Append(String.Empty.PadLeft(1));//39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append(String.Format("R#{0}", provinceIdentifier));//53-56 --record type identifier
            output.Append("9");//57-57 --constant
            output.Append(String.Empty.PadRight(2));//58-59 --filler
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(3));//61-63 --filler
            output.Append(ProvinceRemittances.GetProvinceRemittance(province).HealthRemittanceAccountNumber.PadLeft(13));//64-76 --Province Tax Account Number
            output.Append(String.Empty.PadLeft(2)); //77-78 --filler
            output.Append(" "); //79-79 --Remit Indicator 
            output.Append(String.Empty.PadLeft(1)); //80-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'HF2NL' record");

            return output.ToString();
        }
        private String STATNL(String province, String xxxRecordIndicator)
        {
            StringBuilder output = new StringBuilder();
            output.Append('2');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(Nine11V99(GetEmployerHealthTaxAssessable(province)));//13-25 --Gross Amount 
            output.Append(String.Empty.PadLeft(13));//26-38 --filler
            output.Append(String.Empty.PadLeft(1));//39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append("STAT");//53-56 --record type identifier
            output.Append(xxxRecordIndicator);//57-59 --constant
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(20));//61-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'STATNL' record");

            return output.ToString();
        }
        #endregion

        #region block10
        private String HFNT(String province, String provinceIdentifier)
        {
            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(String.Empty.PadLeft(13, '0'));//13-25 --Gross Exemption (left blank for now)
            output.Append(String.Empty.PadLeft(13, '0'));//26-38 --QHF Rate sounds like it isn't needed
            output.Append(String.Empty.PadLeft(1)); //39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append(String.Format("R#{0}", provinceIdentifier));//53-56 --record type identifier
            output.Append(String.Empty.PadRight(3));//57-59 --filler
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(3));//61-63 --filler
            output.Append(ProvinceRemittances.GetProvinceRemittance(province).HealthRemittanceAccountNumber.PadLeft(9));//64-72 --Province Tax Account Number
            output.Append(String.Empty.PadLeft(6)); //73-78 --filler
            output.Append("M"); //79-79 --Remit Freq (M=monthly)
            output.Append(String.Empty.PadLeft(1)); //80-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'HFNT' record");

            return output.ToString();
        }
        private String HF2NT(String province, String provinceIdentifier)
        {
            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(String.Empty.PadLeft(7));//13-19 --filler
            output.Append(String.Empty.PadLeft(6));//20-25 --filler
            output.Append(String.Empty.PadLeft(13));//26-38 --filler
            output.Append(String.Empty.PadLeft(1));//39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append(String.Format("R#{0}", provinceIdentifier));//53-56 --record type identifier
            output.Append("9");//57-57 --constant
            output.Append(String.Empty.PadRight(2));//58-59 --filler
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(3));//61-63 --filler
            output.Append(ProvinceRemittances.GetProvinceRemittance(province).HealthRemittanceAccountNumber.PadLeft(9));//64-72 --Province Tax Account Number
            output.Append(String.Empty.PadLeft(6)); //73-78 --filler
            output.Append(" "); //79-79 --Remit Indicator 
            output.Append(String.Empty.PadLeft(1)); //80-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'HF2NT' record");

            return output.ToString();
        }
        private String STATNT(String province, String xxxRecordIndicator)
        {
            StringBuilder output = new StringBuilder();
            output.Append('2');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(Nine11V99(GetEmployerHealthTaxAssessable(province)));//13-25 --Gross Amount 
            output.Append(String.Empty.PadLeft(13));//26-38 --filler
            output.Append(String.Empty.PadLeft(1));//39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append("STAT");//53-56 --record type identifier
            output.Append(xxxRecordIndicator);//57-59 --constant
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(20));//61-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'STATNT' record");

            return output.ToString();
        }
        #endregion

        #region block11
        private String HFNU(String province, String provinceIdentifier)
        {
            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(String.Empty.PadLeft(13, '0'));//13-25 --Gross Exemption (left blank for now)
            output.Append(String.Empty.PadLeft(13, '0'));//26-38 --QHF Rate sounds like it isn't needed
            output.Append(String.Empty.PadLeft(1)); //39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append(String.Format("R#{0}", provinceIdentifier));//53-56 --record type identifier
            output.Append(String.Empty.PadRight(3));//57-59 --filler
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(3));//61-63 --filler
            output.Append(ProvinceRemittances.GetProvinceRemittance(province).HealthRemittanceAccountNumber.PadLeft(9));//64-72 --Province Tax Account Number
            output.Append(String.Empty.PadLeft(6)); //73-78 --filler
            output.Append("M"); //79-79 --Remit Freq (M=monthly)
            output.Append(String.Empty.PadLeft(1)); //80-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'HFNU' record");

            return output.ToString();
        }
        private String HF2NU(String province, String provinceIdentifier)
        {
            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(String.Empty.PadLeft(7));//13-19 --filler
            output.Append(String.Empty.PadLeft(6));//20-25 --filler
            output.Append(String.Empty.PadLeft(13));//26-38 --filler
            output.Append(String.Empty.PadLeft(1));//39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append(String.Format("R#{0}", provinceIdentifier));//53-56 --record type identifier
            output.Append("9");//57-57 --constant
            output.Append(String.Empty.PadRight(2));//58-59 --filler
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(3));//61-63 --filler
            output.Append(ProvinceRemittances.GetProvinceRemittance(province).HealthRemittanceAccountNumber.PadLeft(9));//64-72 --Province Tax Account Number
            output.Append(String.Empty.PadLeft(6)); //73-78 --filler
            output.Append(" "); //79-79 --Remit Indicator 
            output.Append(String.Empty.PadLeft(1)); //80-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'HF2NU' record");

            return output.ToString();
        }
        private String STATNU(String province, String xxxRecordIndicator)
        {
            StringBuilder output = new StringBuilder();
            output.Append('2');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(Nine11V99(GetEmployerHealthTaxAssessable(province)));//13-25 --Gross Amount 
            output.Append(String.Empty.PadLeft(13));//26-38 --filler
            output.Append(String.Empty.PadLeft(1));//39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append("STAT");//53-56 --record type identifier
            output.Append(xxxRecordIndicator);//57-59 --constant
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(20));//61-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'STATNU' record");

            return output.ToString();
        }
        #endregion

        #region block 12  
        private String WCB1NS()
        {

            const String province = "NS";
            const String constant1 = "WC2";
            const String constant2 = "W2";
            const String provincialWCBCode = "NW";

            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(constant1); //13-15 --constant
            output.Append(province); //16-17 - Province Code
            output.Append(String.Empty.PadLeft(34));//18-51 --filler

            output.Append("W");//52-52 --Remit Freq 
            output.Append("Y");//53-53 --Remit Indicator 
            output.Append(constant2);//54-55 --constant 2
            output.Append(province); //56-57 - Province Code
            output.Append(_wcbStartDate.ToString("yyyyMM"));//58-63 StartDate 
            output.Append(String.Empty.PadRight(9));//64-72 --filler
            output.Append(provincialWCBCode.PadRight(2));//73-74 --Provincial WCB Code 
            output.Append(String.Empty.PadRight(2));//75-76 --filler
            output.Append(PaymentDate.ToString("yy"));//77-78 Tax Year 
            output.Append(String.Empty.PadRight(2));//79-80 --filler


            if (output.Length != 80) throw new Exception("error forming 'WCB1NS' record");

            return output.ToString();
        }

        private String WCB2NS(WSIBSummary summary)
        {
            const String province = "NS";
            const String constant1 = "WC2";
            const String constant2 = "W2";

            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(constant1); //13-15 --constant
            output.Append(province); //16-17 - Province Code
            output.Append(String.Empty.PadLeft(1));//18-18 --filler
            output.Append(summary.WSIBCode.PadRight(8));//19-26 --Match Code  
            //output.Append("WCBCD");//27-31 --Match Code  (HARD CODE)
            //output.Append(String.Format("{0}", Math.Round((double)summary.WorkersCompensationRate, 2, System.MidpointRounding.AwayFromZero) * 100).PadLeft(4, '0'));//32-35 --wcb rate?
            output.Append(String.Empty.PadLeft(27));//27-53 --filler
            output.Append(constant2);//54-55 --constant 2
            output.Append(province); //56-57 - Province Code
            output.Append(String.Empty.PadLeft(6));//58-63 --filler
            output.Append(summary.WorkersCompensationAccountNumber.PadLeft(9).Substring(0, 9));//64-72 - WCB Business Number  
            output.Append(summary.WorkersCompensationAccountNumber.Substring(9, 2));//73-74 --Provincial WCB Code 
            output.Append(summary.WorkersCompensationAccountNumber.Substring(11, 4));//75-78 --WCB BN Sequence Number
            output.Append(String.Empty.PadRight(2));//79-80 --filler


            if (output.Length != 80) throw new Exception("error forming 'WCB2NS' record");

            return output.ToString();
        }

        private String WCB3NS(String wsibCode)
        {
            const String province = "NS";

            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(String.Empty.PadLeft(9)); //13-21 --filler
            output.Append(Nine7V99(WcbBase(wsibCode)));  //22-30 --Current WCB Assessable Amt 
            output.Append(String.Empty.PadLeft(9)); //31-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(Nine7V99(WcbAssessed(wsibCode))); //45-53 --Current WCB Premium Amt  
            output.Append("W"); //54-54 --constant
            output.Append(province); //55-56 --Province Code
            output.Append(wsibCode.PadLeft(8));//57-64 --Match Code (optional)
            output.Append(EmployeeCount(wsibCode).ToString().PadLeft(6, '0'));//65-70 --Employee Count (all employees for this province?)
            output.Append(String.Empty.PadRight(10));//71-80 --filler


            if (output.Length != 80) throw new Exception("error forming 'WCB3NS' record");

            return output.ToString();
        }

        #endregion

        #region block 13
        private String WCB1QC()
        {
            const String province = "QC";
            const String constant1 = "WC3";
            const String constant2 = "W3";
            const String provincialWCBCode = "QC";

            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(constant1); //13-15 --constant
            output.Append(province); //16-17 - Province Code
            output.Append(String.Empty.PadLeft(34));//18-51 --filler

            output.Append(" ");//52-52 --Remit Freq 
            output.Append("Y");//53-53 --Remit Indicator 
            output.Append(constant2);//54-55 --constant 2
            output.Append(province); //56-57 - Province Code
            output.Append(_wcbStartDate.ToString("yyyyMM"));//58-63 StartDate 
            output.Append(String.Empty.PadRight(9));//64-72 --filler
            output.Append(provincialWCBCode.PadRight(2));//73-74 --Provincial WCB Code 
            output.Append(String.Empty.PadRight(2));//75-76 --filler
            output.Append(PaymentDate.ToString("yy"));//77-78 Tax Year 
            output.Append(String.Empty.PadRight(2));//79-80 --filler


            if (output.Length != 80) throw new Exception("error forming 'WCB1QC' record");

            return output.ToString();
        }

        private String WCB2QC(WSIBSummary summary)
        {
            const String province = "QC";
            const String constant1 = "WC3";
            const String constant2 = "W3";

            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(constant1); //13-15 --constant
            output.Append(province); //16-17 - Province Code
            output.Append(String.Empty.PadLeft(1));//18-18 --filler
            output.Append(summary.WSIBCode.PadRight(8));//19-26 --Match Code  (HARD CODE)
            //output.Append("WCBCD");//27-31 --  (HARD CODE)
            //output.Append(String.Format("{0}", Math.Round((double)summary.WorkersCompensationRate, 2, System.MidpointRounding.AwayFromZero) * 100).PadLeft(4, '0'));//32-35 --wcb rate?
            output.Append(String.Empty.PadLeft(27));//27-53 --filler
            output.Append(constant2);//54-55 --constant 2
            output.Append(province); //56-57 - Province Code
            output.Append(String.Empty.PadLeft(6));//58-63 --filler
            output.Append(summary.WorkersCompensationAccountNumber.PadLeft(14));//64-77 - WCB Business Number  
            output.Append(String.Empty.PadRight(3));//78-80 --filler


            if (output.Length != 80) throw new Exception("error forming 'WCB2QC' record");

            return output.ToString();
        }

        private String WCB3QC(String wsibCode)
        {
            const String province = "QC";

            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(String.Empty.PadLeft(9)); //13-21 --filler
            output.Append(Nine7V99(WcbBase(wsibCode)));  //22-30 --Current WCB Assessable Amt 
            output.Append(String.Empty.PadLeft(9)); //31-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(Nine7V99(WcbAssessed(wsibCode))); //45-53 --Current WCB Premium Amt  
            output.Append("W"); //54-54 --constant
            output.Append(province); //55-56 --Province Code
            output.Append(wsibCode.PadLeft(8));//57-64 --Match Code (optional)
            output.Append(EmployeeCount(wsibCode).ToString().PadLeft(6, '0'));//65-70 --Employee Count (all employees for this province?)
            output.Append(String.Empty.PadRight(10));//71-80 --filler


            if (output.Length != 80) throw new Exception("error forming 'WCB3QC' record");

            return output.ToString();
        }

        #endregion

        #region block 14
        //skipped
        #endregion

        #region block 15
        private String CeridianProcess()
        {
            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(String.Empty.PadLeft(27)); //13-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadLeft(8)); //45-52 --filler
            output.Append(_processType.ToString().PadLeft(4)); //53-56 --Record Type ID
            output.Append(String.Empty.PadRight(24));//57-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'CeridianProcess' record");

            return output.ToString();
        }

        #endregion 

        #region block 16
        private String CARD()
        {
            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(String.Empty.PadLeft(40)); //13-52 --filler
            output.Append("CARD"); //53-56 --Record Type ID
            output.Append("E");//57-57 --constant
            output.Append(String.Empty.PadRight(23));//58-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'CeridianProcess' record");

            return output.ToString();
        }
        #endregion 

        #region block 17
        private String PCTR()
        {
            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(String.Empty.PadLeft(6)); //13-18 --filler
            output.Append(FormattedPeriodEndDate);//19-23 --Period End Date (julian)
            output.Append(String.Empty.PadRight(13));//24-36 --filler
            output.Append(_periodRunNumber.ToString().PadLeft(2, '0'));//37-38 --Period Run No
            output.Append(String.Empty.PadRight(1));//39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append("E");//45-45 --language code (engish)
            output.Append(String.Empty.PadRight(7));//46-52 --filler
            output.Append("PCTR"); //53-56 --Record Type ID
            output.Append(String.Empty.PadRight(3));//57-59 --filler
            output.Append("P");//60-60 --Cheque Option P = Pressure Sealed
            output.Append(String.Empty.PadRight(2));//61-62 --filler
            output.Append(((short)_payFrequency).ToString());//63-63 --Pay Frequency
            output.Append(_payFrequencyList[_payFrequency].PadRight(17));//64-80  -- Pay Frequency Description

            if (output.Length != 80) throw new Exception("error forming 'PCTR' record");

            return output.ToString();
        }
        #endregion

        #region block 18
        //skipped
        #endregion

        #region block 19
        //skipped
        #endregion

        #region block 20
        private String SEAL()
        {
            StringBuilder output = new StringBuilder();
            output.Append('1');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(String.Empty.PadLeft(40)); //13-52 --filler
            output.Append("SEAL");  //53-56 --Constant= ‘SEAL’
            output.Append(" ");  //57-57 --Seal Indictor
            output.Append(String.Empty.PadLeft(23)); //58-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'SEAL' record");

            return output.ToString();
        }
        #endregion

        #region block 21
        //skipped
        #endregion

        #region block 22
        private String GROS()
        {
            StringBuilder output = new StringBuilder();
            output.Append('2');  //01-01 --card type
            output.Append(String.Empty.PadLeft(5)); //02-06 --filler
            output.Append(FormattedCompanyNumber); //07-12 --company number
            output.Append(Nine11V99(_totalGrossAmount));//13-25 --Gross Earnings
            output.Append((TotalEmployeeCount * 100).ToString().PadLeft(13, '0'));//26-38 --Number of Employee Paid
            output.Append(String.Empty.PadRight(1));//39-39 --filler
            output.Append(FormattedPaymentDate);//40-44 payment date (julian)
            output.Append(String.Empty.PadRight(8));//45-52 --filler
            output.Append("GROS");//53-56 --record type identifier
            output.Append("2XX");//57-59 --constant
            output.Append(String.Empty.PadRight(1));//60-60 --EI Preference ' ' for now
            output.Append(String.Empty.PadRight(2));//61-62 --filler
            output.Append(((short)_payFrequency).ToString());//63-63 --Pay Frequency
            output.Append(_businessTaxNumber.PadRight(15));//64-78 --RCT number 
            output.Append(String.Empty.PadRight(2));//79-80 --filler

            if (output.Length != 80) throw new Exception("error forming 'RCT' record");

            return output.ToString();
        }
        #endregion

        #endregion

        public override string ToString()
        {
            PerformCalculations();
            StringBuilder output = new StringBuilder();

            //header
            output.AppendLine(Header);
            //block 1
            output.AppendLine(CHEQ);
            output.AppendLine(DDEP);
            //block 2
            output.AppendLine(CNET);
            output.AppendLine(DNET);
            //block 3 
            output.AppendLine(D);
            output.AppendLine(N);


            //block 4
            output.AppendLine(RCT);
            output.AppendLine(STAT2("AB"));
            output.AppendLine(STAT2("BC"));
            output.AppendLine(STAT2("MB"));
            output.AppendLine(STAT2("NB"));
            output.AppendLine(STAT2("NF"));
            output.AppendLine(STAT2("NL"));
            output.AppendLine(STAT2("NN"));
            output.AppendLine(STAT2("NS"));
            output.AppendLine(STAT2("NT"));
            output.AppendLine(STAT2("ON"));
            output.AppendLine(STAT2("PE"));
            output.AppendLine(STAT2("QC"));
            output.AppendLine(STAT2("SK"));
            output.AppendLine(STAT2("YT"));

            //block 5
            output.AppendLine(STAT3("AB"));
            output.AppendLine(STAT3("BC"));
            output.AppendLine(STAT3("MB"));
            output.AppendLine(STAT3("NB"));
            output.AppendLine(STAT3("NF"));
            output.AppendLine(STAT3("NL"));
            output.AppendLine(STAT3("NN"));
            output.AppendLine(STAT3("NS"));
            output.AppendLine(STAT3("NT"));
            output.AppendLine(STAT3("ON"));
            output.AppendLine(STAT3("PE"));
            output.AppendLine(STAT3("QC"));
            output.AppendLine(STAT3("SK"));
            output.AppendLine(STAT3("YT"));

            //            output.AppendLine(STAT4("AB"));
            //            output.AppendLine(STAT4("BC"));
            //            output.AppendLine(STAT4("MB"));
            //            output.AppendLine(STAT4("NB"));
            //            output.AppendLine(STAT4("NF"));
            //            output.AppendLine(STAT4("NL"));
            //            output.AppendLine(STAT4("NN"));
            //            output.AppendLine(STAT4("NS"));
            //            output.AppendLine(STAT4("NT"));
            //            output.AppendLine(STAT4("ON"));
            //            output.AppendLine(STAT4("PE"));
            output.AppendLine(STAT4("QC"));
            //            output.AppendLine(STAT4("SK"));
            //            output.AppendLine(STAT4("YT"));

            //block 6
            if (GetEmployerHealthTaxAssessable("QC") != 0)
            {
                output.AppendLine(HFQC("QC", "QC"));
                output.AppendLine(HF2QC("QC", "QC"));
                output.AppendLine(STATQC("QC", "QMD"));
            }

            //block 7
            if (GetEmployerHealthTaxAssessable("ON") != 0)
            {
                output.AppendLine(HFON("ON", "ON"));
                output.AppendLine(HF2ON("ON", "ON"));
                output.AppendLine(STATON("ON", "EHT"));
            }
            ////block 8
            //if (GetOntarioEmployerHealthQuebecHealthInsurancePremiumBase("MB") != 0)
            //{
            //    output.AppendLine(HFMB("MB", "MB"));
            //    output.AppendLine(HF2MB("MB", "MB"));
            //    output.AppendLine(STATMB("MB", "MHE"));
            //}

            //block 9
            if (GetEmployerHealthTaxAssessable("NL") != 0)
            {
                output.AppendLine(HFNL("NL", "NL"));
                output.AppendLine(HF2NL("NL", "NL"));
                output.AppendLine(STATNL("NL", "NFH"));
            }

            //block 10
            if (GetEmployerHealthTaxAssessable("NT") != 0)
            {
                output.AppendLine(HFNT("NT", "NT"));
                output.AppendLine(HF2NT("NT", "NT"));
                output.AppendLine(STATNT("NT", "NTP"));
            }

            //block 11
            if (GetEmployerHealthTaxAssessable("NU") != 0)
            {
                output.AppendLine(HFNU("NU", "NN"));
                output.AppendLine(HF2NU("NU", "NN"));
                output.AppendLine(STATNU("NU", "NNP"));
            }

            //block 12
            foreach (WSIBSummary summary in WSIBSummarys.GetWSIBSummaryByProvince("NS"))
            {
                if (WcbBase(summary.WSIBCode) != 0 || WcbAssessed(summary.WSIBCode) != 0)
                {
                    output.AppendLine(WCB1NS());
                    output.AppendLine(WCB2NS(summary));
                    output.AppendLine(WCB3NS(summary.WSIBCode));
                }
            }

            //block 13
            foreach (WSIBSummary summary in WSIBSummarys.GetWSIBSummaryByProvince("QC"))
            {
                if (WcbBase(summary.WSIBCode) != 0 || WcbAssessed(summary.WSIBCode) != 0)
                {
                    output.AppendLine(WCB1QC());
                    output.AppendLine(WCB2QC(summary));
                    output.AppendLine(WCB3QC(summary.WSIBCode));
                }
            }

            //block 14 skipped

            //block 15
            output.AppendLine(CeridianProcess());

            //block 16
            output.AppendLine(CARD());

            //block 17
            output.AppendLine(PCTR());

            //block 18 skipped
            //block 19 skipped
            //block 20 skipped 
            output.AppendLine(SEAL());
            //block 21 skipped //not in sample
            //block 22 
            output.AppendLine(GROS());


            return output.ToString();
        }

        private void PerformCalculations()
        {
            //reset calc fields
            _numberOfCheques = 0;
            _numberOfDeposits = 0;
            _numberOfSecondaryDeposits = 0;
            _netChequeAmount = 0;
            _netDepositAmount = 0;
            _netSecondaryDepositAmount = 0;
            _employeeCPPAmount = new Dictionary<String, Decimal>();
            _federalTaxAmount = new Dictionary<String, Decimal>();
            _provincialTaxAmount = new Dictionary<String, decimal>();
            _employeeEITaxAmount = new Dictionary<String, decimal>();
            _provincialParentalInsurancePlanPremiumAmount = new Dictionary<String, decimal>();
            _employerHealthTaxAssessable = new Dictionary<String, decimal>();
            _employeeCount = new Dictionary<String, int>();
            _wcbBase = new Dictionary<String, decimal>();
            _wcbAssessed = new Dictionary<String, decimal>();

            foreach (PayrollMaster item in this)
            {
                _totalGrossAmount += (item.GrossAmount);

                if (item.HowPaid.Equals(_howPaidCheque))
                {
                    _numberOfCheques++;
                    _netChequeAmount += item.ChequeAmount;
                }
                else
                {
                    if (item.ChequeAmount != 0)
                        _numberOfDeposits++;

                    PayrollMasterPaymentCollection deposits = GetEFTData(item.EmployeeNumber);

                    if (deposits.Count == 1)
                        _netDepositAmount += item.ChequeAmount;
                    else
                    {
                        //get max sequence number
                        short maxSequenceNumber = 0;

                        foreach (PayrollMasterPayment deposit in deposits)
                        {
                            if (Convert.ToInt16(deposit.CodeEmployeeBankingSequenceCd) > maxSequenceNumber)
                                maxSequenceNumber = Convert.ToInt16(deposit.CodeEmployeeBankingSequenceCd);
                        }

                        foreach (PayrollMasterPayment deposit in deposits)
                        {
                            if (Convert.ToInt16(deposit.CodeEmployeeBankingSequenceCd) == maxSequenceNumber)
                                _netDepositAmount += deposit.Amount;
                            else
                            {
                                if (deposit.Amount != 0)
                                    _numberOfSecondaryDeposits++;

                                _netSecondaryDepositAmount += deposit.Amount;
                            }
                        }
                    }
                }

                //break down CPP by province
                if (!_employeeCPPAmount.ContainsKey(item.CodeProvinceStateCd)) _employeeCPPAmount.Add(item.CodeProvinceStateCd, 0);
                _employeeCPPAmount[item.CodeProvinceStateCd] += item.CanadaPensionPlan;
                //break down income tax by province
                if (!_federalTaxAmount.ContainsKey(item.CodeProvinceStateCd)) _federalTaxAmount.Add(item.CodeProvinceStateCd, 0);
                _federalTaxAmount[item.CodeProvinceStateCd] += (item.FederalTax + item.QuebecTax);
                //break down provincial tax by province
                if (!_provincialTaxAmount.ContainsKey(item.CodeProvinceStateCd)) _provincialTaxAmount.Add(item.CodeProvinceStateCd, 0);
                _provincialTaxAmount[item.CodeProvinceStateCd] += item.QuebecTax;
                //break down employee employment insurance
                if (!_employeeEITaxAmount.ContainsKey(item.CodeProvinceStateCd)) _employeeEITaxAmount.Add(item.CodeProvinceStateCd, 0);
                _employeeEITaxAmount[item.CodeProvinceStateCd] += item.EmploymentInsurance;
                //break down provincial Parental Insurance Plan PremiumAmount
                if (!_provincialParentalInsurancePlanPremiumAmount.ContainsKey(item.CodeProvinceStateCd)) _provincialParentalInsurancePlanPremiumAmount.Add(item.CodeProvinceStateCd, 0);
                _provincialParentalInsurancePlanPremiumAmount[item.CodeProvinceStateCd] += item.QuebecParentalInsurancePlan;
                //break down provincial Parental Insurance Plan PremiumAmount
                if (!_employerHealthTaxAssessable.ContainsKey(item.CodeProvinceStateCd)) _employerHealthTaxAssessable.Add(item.CodeProvinceStateCd, 0);
                _employerHealthTaxAssessable[item.CodeProvinceStateCd] += item.EmployerHealthTaxEarningsAssessable;
                //break down employee count by province
                if (!_employeeCount.ContainsKey(item.CodeWsibCd)) _employeeCount.Add(item.CodeWsibCd, 0);
                _employeeCount[item.CodeWsibCd]++;  //need to check?
                //break down wcb assessable province
                if (!_wcbBase.ContainsKey(item.CodeWsibCd)) _wcbBase.Add(item.CodeWsibCd, 0);
                _wcbBase[item.CodeWsibCd] += item.WorkersCompensationEarningsAssessable;
                //break down wcb premium province
                if (!_wcbAssessed.ContainsKey(item.CodeWsibCd)) _wcbAssessed.Add(item.CodeWsibCd, 0);
                _wcbAssessed[item.CodeWsibCd] += item.EmployerWorkersCompensationPremium;
            }

            //add grievance to cheque count
            foreach (PayrollMasterPaycode garnishment in Garnishments)
            {
                _numberOfCheques++;
                _netChequeAmount += garnishment.Amount;
            }
        }

        private void InitializePayFrequency()
        {
            _payFrequencyList = new Dictionary<PayFrequency, string>();
            _payFrequencyList.Add(PayFrequency.Weekly, "Weekly");
            _payFrequencyList.Add(PayFrequency.BiWeekly, "Bi-Weekly");
            _payFrequencyList.Add(PayFrequency.SemiMonthly, "Semi-Monthly");
            _payFrequencyList.Add(PayFrequency.Monthly, "Monthly");
        }

        #region constructor
        public CeridianBilling(String companyNumber, ProcessType type, short periodRunNumber, DateTime wcbStartDate, String businessTaxNumber, long onEFTAmount)
        {
            _wcbStartDate = wcbStartDate;
            _companyNumber = companyNumber;
            _processType = type;
            _periodRunNumber = periodRunNumber;
            _businessTaxNumber = businessTaxNumber;
            _onEFTAmount = onEFTAmount;
            InitializePayFrequency();
        }
        #endregion
    }
}