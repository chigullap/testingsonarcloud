﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public class Company : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        public int CompanyId 
        {
            get { return (int)_Key; }
            set { _Key = value; }
        }
        public String Description { get; set; }

        #endregion

        #region construct/destruct
        public Company()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();
            CompanyId=0;
            Description = null;
        }

        #endregion
    }
}
