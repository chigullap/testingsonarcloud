﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;


namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PayrollProcessSummaryCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollProcessSummary>
    {
    }
}
