﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class CraGarnishmentCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<CraGarnishment>
    {
        public decimal SumGarnishmentAmount
        {
            get
            {
                Decimal rtn = 0;

                foreach (CraGarnishment item in this)
                {
                    rtn += item.GarnishmentAmount;
                }

                return rtn;
            }
        }
    }
}
