﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeProfile : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeProfileId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long? EmployeeId { get; set; }

        [DataMember]
        public String EmployeeNumber { get; set; }

        [DataMember]
        public String LastName { get; set; }

        [DataMember]
        public String FirstName { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public DateTime? HireDate { get; set; }

        [DataMember]
        public String Status { get; set; }

        [DataMember]
        public String PayrollProcessGroup { get; set; }

        [DataMember]
        public String Job { get; set; }

        [DataMember]
        public String PhoneNumber { get; set; }

        [DataMember]
        public String EmailAddress { get; set; }

        [DataMember]
        public String Address { get; set; }

        [DataMember]
        public DateTime? LastLoginDate { get; set; }

        [DataMember]
        public EmployeePhoto EmployeePhotoObject { get; set; }

        [DataMember]
        public byte[] EmployeePhoto
        {
            get
            {
                if (EmployeePhotoObject == null)
                    return null;
                else
                    return EmployeePhotoObject.Data;
            }
            set
            {
                if (EmployeePhotoObject == null)
                    EmployeePhotoObject = new EmployeePhoto() { Data = value };
                else
                    EmployeePhotoObject.Data = value;
            }
        }

        #region calcs
        public String BirthDateString
        {
            get
            {
                String birthDate = null;

                if (BirthDate != null)
                    birthDate = Convert.ToDateTime(BirthDate).ToString("MM/dd/yyyy");

                return birthDate;
            }
        }
        public String HireDateString
        {
            get
            {
                String hireDate = null;

                if (HireDate != null)
                    hireDate = Convert.ToDateTime(HireDate).ToString("MM/dd/yyyy");

                return hireDate;
            }
        }
        public String LastLoginString
        {
            get
            {
                String lastLogin = null;

                if (LastLoginDate != null)
                    lastLogin = Convert.ToDateTime(LastLoginDate).ToString("MM/dd/yyyy");

                return lastLogin;
            }
        }
        #endregion
        #endregion

        #region construct/destruct
        public EmployeeProfile()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeeProfile data)
        {
            base.CopyTo(data);

            data.EmployeeProfileId = EmployeeProfileId;
            data.EmployeeId = EmployeeId;
            data.EmployeeNumber = EmployeeNumber;
            data.LastName = LastName;
            data.FirstName = FirstName;
            data.BirthDate = BirthDate;
            data.HireDate = HireDate;
            data.Status = Status;
            data.PayrollProcessGroup = PayrollProcessGroup;
            data.Job = Job;
            data.LastLoginDate = LastLoginDate;
            data.PhoneNumber = PhoneNumber;
            data.EmailAddress = EmailAddress;
            data.Address = Address;
            data.EmployeePhotoObject = EmployeePhotoObject;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeeProfileId = -1;
            EmployeeId = null;
            EmployeeNumber = null;
            LastName = null;
            FirstName = null;
            BirthDate = null;
            HireDate = null;
            Status = null;
            PayrollProcessGroup = null;
            Job = null;
            LastLoginDate = null;
            PhoneNumber = null;
            EmailAddress = null;
            Address = null;
            EmployeePhotoObject = null;
        }
        #endregion
    }
}