﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class WSIBSummary: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region fields
        #endregion
        
        #region properties
        [DataMember]
        public long WSIBEffectiveId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String WSIBCode { get;set;}
        [DataMember]
        public String WorkersCompensationAccountNumber { get; set; }
        [DataMember]
        public String ProvinceStateCode { get; set; }
        [DataMember]
        public Decimal? WorkersCompensationRate { get; set; }

        #endregion

        #region construct/destruct
        public WSIBSummary()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(WSIBSummary data)
        {
            base.CopyTo(data);

            data.WSIBEffectiveId = WSIBEffectiveId;
            data.WSIBCode = WSIBCode;
            data.WorkersCompensationAccountNumber = WorkersCompensationAccountNumber;
            data.ProvinceStateCode = ProvinceStateCode;
            data.WorkersCompensationRate = WorkersCompensationRate;
        }

        public new void Clear()
        {
            WSIBEffectiveId = -1;
            WSIBCode = null;
            WorkersCompensationAccountNumber = null;
            ProvinceStateCode = null;
            WorkersCompensationRate = null;
        }

        #endregion




    }
}
