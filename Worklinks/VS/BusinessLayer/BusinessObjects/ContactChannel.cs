﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;
using System.ComponentModel.DataAnnotations;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ContactChannel : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {

        #region properties
        [DataMember]
        public long ContactChannelId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        [Required]
        public String ContactChannelTypeCode { get; set; }

        [DataMember]
        [Required]
        public String PrimaryContactValue { get; set; }

        [DataMember]
        public String SecondaryContactValue { get; set; }

        [DataMember]
        public bool OverrideConcurrencyCheckFlag { get; set; }

        #endregion

        #region construct/destruct
        public ContactChannel()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(ContactChannel data)
        {
            base.CopyTo(data);

            data.ContactChannelId = ContactChannelId;
            data.ContactChannelTypeCode = ContactChannelTypeCode;
            data.PrimaryContactValue = PrimaryContactValue;
            data.SecondaryContactValue = SecondaryContactValue;
            data.OverrideConcurrencyCheckFlag = OverrideConcurrencyCheckFlag;
        }
        public new void Clear()
        {
            base.Clear();
            ContactChannelId = -1;
            ContactChannelTypeCode = null;
            PrimaryContactValue = null;
            SecondaryContactValue = null;
            OverrideConcurrencyCheckFlag = false;
        }

        #endregion
    }
}