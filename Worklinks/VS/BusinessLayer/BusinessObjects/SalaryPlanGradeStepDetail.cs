﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SalaryPlanGradeStepDetail : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long SalaryPlanGradeStepDetailId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long SalaryPlanGradeStepId { get; set; }

        [DataMember]
        public long SalaryPlanGradeEffectiveId { get; set; }

        [DataMember]
        public decimal Amount { get; set; }
        #endregion

        #region construct/destruct
        public SalaryPlanGradeStepDetail()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(SalaryPlanGradeStepDetail data)
        {
            base.CopyTo(data);

            data.SalaryPlanGradeStepDetailId = SalaryPlanGradeStepDetailId;
            data.SalaryPlanGradeStepId = SalaryPlanGradeStepId;
            data.SalaryPlanGradeEffectiveId = SalaryPlanGradeEffectiveId;
            data.Amount = Amount;
        }
        public new void Clear()
        {
            base.Clear();

            SalaryPlanGradeStepDetailId = -1;
            SalaryPlanGradeStepId = -1;
            SalaryPlanGradeEffectiveId = -1;
            Amount = 0;
        }
        #endregion
    }
}