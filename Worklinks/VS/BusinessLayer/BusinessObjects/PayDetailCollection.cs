﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PayDetailCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayDetail>
    {
        public int UniqueEmployeeCount
        {
            get
            {
                return UniqueEmployeeIds.Count;
            }
        }

        public Decimal TotalEmployeeGrossAmount
        {
            get
            {
                Decimal total = 0;
                foreach (long employeeId in UniqueEmployeeIds)
                    total += GetPayDetailByEmployeeId(employeeId)[0].GrossAmount ?? 0;

                return total;
            }
        }


        public Decimal TotalCurrentAmount
        {
            get
            {
                Decimal total = 0;
                foreach (PayDetail item in this)
                    total += item.CurrentAmount ?? 0;

                return total;
            }
        }

        public List<long> UniqueEmployeeIds
        {
            get
            {
                List<long> uniqueEmployeeIds = new List<long>();
                foreach (PayDetail paydetail in this)
                {
                    if (!uniqueEmployeeIds.Contains(paydetail.EmployeeId))
                        uniqueEmployeeIds.Add(paydetail.EmployeeId);
                }

                return uniqueEmployeeIds;
            }
        }

        public List<String> UniqueExternalCodeFirst7Chars
        {
            get
            {
                List<String> rtn = new List<string>();
                foreach (PayDetail pay in this)
                {
                    if (!rtn.Contains(pay.ExternalCode.Substring(0, 7)))
                        rtn.Add(pay.ExternalCode.Substring(0, 7));
                }

                return rtn;
            }
        }

        public List<String> UniqueExternalCodeFirst3Chars
        {
            get
            {
                List<String> rtn = new List<string>();
                foreach (PayDetail pay in this)
                {
                    if (!rtn.Contains(pay.ExternalCode.Substring(0, 3)))
                        rtn.Add(pay.ExternalCode.Substring(0, 3));
                }

                return rtn;
            }
        }

        public List<String> UniqueEmployerNumbers
        {
            get
            {
                List<String> rtn = new List<string>();
                foreach (PayDetail pay in this)
                {
                    if (!rtn.Contains(pay.EmployerNumber))
                        rtn.Add(pay.EmployerNumber);
                }

                return rtn;
            }
        }


        public List<String> UniqueExternalCodes
        {
            get
            {
                List<String> rtn = new List<string>();
                foreach (PayDetail pay in this)
                {
                    if (!rtn.Contains(pay.ExternalCode))
                        rtn.Add(pay.ExternalCode);
                }

                return rtn;
            }
        }
        public List<String> UniquePayCodes
        {
            get
            {
                List<String> rtn = new List<string>();
                foreach (PayDetail pay in this)
                {
                    if (!rtn.Contains(pay.PaycodeCode))
                        rtn.Add(pay.PaycodeCode);
                }

                return rtn;
            }
        }


        public PayDetailCollection GetPayDetailForQuebec()
        {
            PayDetailCollection rtn = new PayDetailCollection();
            foreach (PayDetail payDetail in this)
            {
                bool isQuebec = (payDetail.PaycodeCode.Equals("TAXQC")
                                    || payDetail.PaycodeCode.Equals("TAXQCR2")
                                    || payDetail.PaycodeCode.Equals("QPP")
                                    || (payDetail.PaycodeCode.Equals("EMPR_PP") && payDetail.ProvinceStateCode.Equals("QC"))
                                    || (payDetail.PaycodeCode.Equals("EMPR_HT") && payDetail.ProvinceStateCode.Equals("QC"))
                                    || (payDetail.PaycodeCode.Equals("CPP") && payDetail.ProvinceStateCode.Equals("QC"))
                                    || payDetail.PaycodeCode.Equals("PPIP")
                                    || payDetail.PaycodeCode.Equals("EMPR_PIP")
                                    || (payDetail.PaycodeCode.Equals("EMPR_WCB") && payDetail.ProvinceStateCode.Equals("QC"))
                                );
                if (isQuebec)
                    rtn.Add(payDetail);
            }

            return rtn;
        }
        public PayDetailCollection GetPayDetailForFederal()
        {
            PayDetailCollection rtn = new PayDetailCollection();
            foreach (PayDetail payDetail in this)
            {
                bool isQuebec = (payDetail.PaycodeCode.Equals("TAXQC")
                                    || payDetail.PaycodeCode.Equals("TAXQCR2")
                                    || payDetail.PaycodeCode.Equals("QPP")
                                    || (payDetail.PaycodeCode.Equals("EMPR_PP") && payDetail.ProvinceStateCode.Equals("QC"))
                                    || (payDetail.PaycodeCode.Equals("EMPR_HT") && payDetail.ProvinceStateCode.Equals("QC"))
                                    || (payDetail.PaycodeCode.Equals("CPP") && payDetail.ProvinceStateCode.Equals("QC"))
                                    || payDetail.PaycodeCode.Equals("PPIP")
                                    || payDetail.PaycodeCode.Equals("EMPR_PIP")
                                    || (payDetail.PaycodeCode.Equals("EMPR_WCB") && payDetail.ProvinceStateCode.Equals("QC"))
                                );
                if (!isQuebec)
                    rtn.Add(payDetail);
            }

            return rtn;
        }

        public PayDetailCollection GetPayDetailByEmployerNumber(String employerNumber)
        {
            PayDetailCollection rtn = new PayDetailCollection();
            foreach (PayDetail payDetail in this)
            {
                if (payDetail.EmployerNumber.Equals(employerNumber))
                    rtn.Add(payDetail);
            }

            return rtn;
        }

        public PayDetailCollection GetPayDetailByExternalCode(String externalCode)
        {
            PayDetailCollection rtn = new PayDetailCollection();
            foreach (PayDetail payDetail in this)
            {
                if (payDetail.ExternalCode.Equals(externalCode))
                    rtn.Add(payDetail);
            }

            return rtn;
        }

        public PayDetailCollection GetPayDetailByPayCode(String paycode)
        {
            PayDetailCollection rtn = new PayDetailCollection();
            foreach (PayDetail payDetail in this)
            {
                if (payDetail.PaycodeCode.Equals(paycode))
                    rtn.Add(payDetail);
            }

            return rtn;
        }

        public PayDetailCollection GetPayDetailByEmployeeId(long employeeId)
        {
            PayDetailCollection rtn = new PayDetailCollection();
            foreach (PayDetail payDetail in this)
            {
                if (payDetail.EmployeeId.Equals(employeeId))
                    rtn.Add(payDetail);
            }

            return rtn;
        }


        public PayDetailCollection GetPayDetailByExternalCodeFirst3Chars(String externalCodeFirst3Chars)
        {
            PayDetailCollection rtn = new PayDetailCollection();
            foreach (PayDetail payDetail in this)
            {
                if (payDetail.ExternalCode.Substring(0,3).Equals(externalCodeFirst3Chars))
                    rtn.Add(payDetail);
            }

            return rtn;
        }

        public PayDetailCollection GetPayDetailByExternalCodeFirst7Chars(String externalCodeFirst7Chars)
        {
            PayDetailCollection rtn = new PayDetailCollection();
            foreach (PayDetail payDetail in this)
            {
                if (payDetail.ExternalCode.Substring(0, 7).Equals(externalCodeFirst7Chars))
                    rtn.Add(payDetail);
            }

            return rtn;
        }

    }

}
