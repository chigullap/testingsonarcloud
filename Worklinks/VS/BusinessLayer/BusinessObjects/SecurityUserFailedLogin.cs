﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SecurityUserFailedLogin
    {
        #region fields
        #endregion
        
        #region properties
        [DataMember]
        public long SecurityUserFailedLoginId { get; set; }
        [DataMember]
        public String UserName { get; set; }
        [DataMember]
        public DateTime LastLoginAttempt { get; set; }

        #endregion

        #region construct/destruct
        public SecurityUserFailedLogin()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        
        public void CopyTo(SecurityUserFailedLogin data)
        {
            data.SecurityUserFailedLoginId = SecurityUserFailedLoginId;
            data.UserName = UserName;
            data.LastLoginAttempt = LastLoginAttempt;
        }

        public void Clear()
        {
            SecurityUserFailedLoginId = -1;
            UserName = "";
            LastLoginAttempt = System.DateTime.Now;
        }

        #endregion
    }
}
