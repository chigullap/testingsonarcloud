﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayRegisterExport : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PayrollProcessExportQueueId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long ExportId { get; set; }
        [DataMember]
        public long? ExportFtpId { get; set; }
        [DataMember]
        public bool ActiveFlag { get; set; }
        [DataMember]
        public bool UnzipFlag { get; set; }
        #endregion

        #region construct/destruct
        public PayRegisterExport()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PayRegisterExport data)
        {
            base.CopyTo(data);

            data.PayrollProcessExportQueueId = PayrollProcessExportQueueId;
            data.ExportId = ExportId;
            data.ExportFtpId = ExportFtpId;
            data.ActiveFlag = ActiveFlag;
            data.UnzipFlag = UnzipFlag;
        }
        public new void Clear()
        {
            base.Clear();

            PayrollProcessExportQueueId = -1;
            ExportId = -1;
            ExportFtpId = null;
            ActiveFlag = true;
            UnzipFlag = false;
        }
        #endregion
    }
}