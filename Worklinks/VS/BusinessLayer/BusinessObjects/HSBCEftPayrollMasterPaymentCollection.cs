﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class HSBCEftPayrollMasterPaymentCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<HSBCEftPayrollMasterPayment>
    {
        public decimal SumPaymentAmount
        {
            get
            {
                Decimal rtn = 0;

                foreach (HSBCEftPayrollMasterPayment item in this)
                {
                    rtn += item.ChequeAmount;
                }

                return rtn;
            }
        }
    }
}