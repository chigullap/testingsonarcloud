﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Runtime.Serialization;


namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class OrganizationUnitAssociationCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<OrganizationUnitAssociation>
    {
    }
}
/*namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class OrganizationUnitAssociationCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<OrganizationUnitAssociation>
    {
        #region fields
        OrganizationUnitCollection _organizationUnits;
        #endregion 

        [DataMember]
        public OrganizationUnitCollection OrganizationUnits 
        {
            get
            {
                if (_organizationUnits == null)
                    _organizationUnits = new OrganizationUnitCollection();
                return _organizationUnits;
            }
            set 
            { 
                _organizationUnits = value; 
            }
        }
        
    }
}*/
