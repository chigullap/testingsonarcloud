﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeePositionLabourDistributionSummary : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeePositionLabourDistributionId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeePositionId { get; set; }

        [DataMember]
        public Decimal Percentage { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public String OrganizationUnit { get; set; }
        #endregion

        #region construct/destruct
        public EmployeePositionLabourDistributionSummary()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeePositionLabourDistributionSummary data)
        {
            base.CopyTo(data);

            data.EmployeePositionLabourDistributionId = EmployeePositionLabourDistributionId;
            data.EmployeePositionId = EmployeePositionId;
            data.Percentage = Percentage;
            data.Description = Description;
            data.OrganizationUnit = OrganizationUnit;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeePositionLabourDistributionId = -1;
            EmployeePositionId = -1;
            Percentage = 0;
            Description = "";
            OrganizationUnit = null;
        }
        #endregion
    }
}