﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Reflection;

//TODO - Michael Allain - Used fro testign a theory ....remove if necessary
namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public class ExceptionHelper
    {
        /// <summary>
        /// reflects the object to serialize and builds a full list of related types for serialization.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static StringWriter SerializeWithKnownTypes(object obj)
        {

            List<Type> knownTypeList = new List<Type>();
            // get the list of related types for this object's type and instance
            Type[] knownTypes = getKnownTypesForInstance(obj, knownTypeList);
            // create an instance of the Xml Serializer and pass in our list of related types
            XmlSerializer serializer = new XmlSerializer(obj.GetType(), knownTypes);
            // Let the serializer do its thing
            StringWriter writer = new StringWriter();
            serializer.Serialize(writer, obj);
            return writer;
        }
        private static Type[] getKnownTypesForInstance(object obj, List<Type> knownTypeList)
        {
            // get the list of known types based on the object's type definition 
            getKnownTypes(obj.GetType(), knownTypeList);



            // now we need to get the list of types from the instance, since the type definition will
            // reference the base type, and we need the actual derived type that's used in the instance
            PropertyInfo[] props = (obj.GetType()).GetProperties();
            foreach (PropertyInfo propertyInfo in props)
            {
                // get the value of the property from the instance
                object propValue = null;//ReflectionTools.GetPropertyValue(obj, propertyInfo.Name, false);
                if (propValue == null)
                    getKnownTypes(propertyInfo.PropertyType, knownTypeList);
                else
                    getKnownTypes(propValue.GetType(), knownTypeList);
            }
            return knownTypeList.ToArray();
        }

        private static Type[] getKnownTypes(Type type, List<Type> knownTypeList)
        {
            // Ignore .NET types and types already in the list
            if (type.FullName.StartsWith("System.") || knownTypeList.Contains(type))
                return knownTypeList.ToArray();

            //add this type to the known types list
            knownTypeList.Add(type);

            // get the list of types for this object's fields
            FieldInfo[] fields = type.GetFields();
            foreach (FieldInfo field in fields)
            {
                getKnownTypes(field.FieldType, knownTypeList);
            }
            // get the list of types for this object's properties
            PropertyInfo[] props = type.GetProperties();
            foreach (PropertyInfo prop in props)
            {
                getKnownTypes(prop.PropertyType, knownTypeList);
            }
            // if this type isn't a base type, recursively call this method for the base type 
            // collecting the type information from the base type
            if (type.BaseType != null)
                getKnownTypes(type.BaseType, knownTypeList);
            return knownTypeList.ToArray();
        }
    }
}
