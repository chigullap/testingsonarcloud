﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [DataContract]
    public class PayrollValidationFault
    {
        private String _message = "";

        [DataMember]
        public String Message
        {
            get { return _message; }
            set { _message = value; }
        }

        public PayrollValidationFault(string message)
        {
            Message = message;
        }
    }
}
