﻿using System;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class UserAdminSearchCriteria
    {
        #region properties
        public String LastName { get; set; }
        public String FirstName { get; set; }
        public String UserName { get; set; }
        public bool IsLockedOutFlag { get; set; }
        public bool WorklinksAdministrationFlag { get; set; }
        #endregion

        #region construct/destruct
        public UserAdminSearchCriteria()
        {
            Clear();
        }
        #endregion

        #region clear
        public void Clear()
        {
            LastName = null;
            FirstName = null;
            UserName = null;
            IsLockedOutFlag = false;
            WorklinksAdministrationFlag = false;
        }
        #endregion
    }
}