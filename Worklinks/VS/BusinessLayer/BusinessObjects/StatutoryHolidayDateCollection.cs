﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class StatutoryHolidayDateCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<StatutoryHolidayDate>
    {
    }
}