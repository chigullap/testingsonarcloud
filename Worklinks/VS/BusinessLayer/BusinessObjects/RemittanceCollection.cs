﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class RemittanceCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<Remittance>
    {
    }
}