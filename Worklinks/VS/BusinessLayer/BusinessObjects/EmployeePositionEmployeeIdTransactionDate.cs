﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeePositionEmployeeIdTransactionDate : EmployeePosition
    {
        [DataMember]
        public override string Key
        {
            get { return DummyKey; }
        }

        [DataMember]
        public string DummyKey { get; set; }

    }
}
