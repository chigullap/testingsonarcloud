﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeRoeAmount: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        
        #region properties
        [DataMember]
        public long EmployeeRoeAmountId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long EmployeePositionId { get; set; }
        [DataMember]
        public Decimal TotalInsurableHours { get; set; }
        [DataMember]
        public Decimal TotalInsurableEarningAmount { get; set; }
        [DataMember]
        public Boolean RoeFileProcessedFlag { get; set; }
        [DataMember]
        public long? RoeId { get; set; }
        [DataMember]
        public EmployeeRoeAmountDetailSummaryCollection AmountDetailSummaryCollection { get; set; }
        #endregion

        #region construct/destruct
        public EmployeeRoeAmount()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeeRoeAmount data)
        {
            base.CopyTo(data);

            data.EmployeeRoeAmountId = EmployeeRoeAmountId;
            data.EmployeePositionId = EmployeePositionId;
            data.TotalInsurableHours = TotalInsurableHours;
            data.TotalInsurableEarningAmount = TotalInsurableEarningAmount;
            data.RoeFileProcessedFlag = RoeFileProcessedFlag;
            data.RoeId = RoeId;

            if (AmountDetailSummaryCollection != null)
            {
                data.AmountDetailSummaryCollection = new EmployeeRoeAmountDetailSummaryCollection();
                AmountDetailSummaryCollection.CopyTo(data.AmountDetailSummaryCollection);
            }
            else
            {
                data.AmountDetailSummaryCollection = null;
            }
        }
        public new void Clear()
        {
            base.Clear();

            EmployeeRoeAmountId = -1;
            EmployeePositionId = -1;
            TotalInsurableHours = 0;
            TotalInsurableEarningAmount = 0;
            RoeFileProcessedFlag = false;
            AmountDetailSummaryCollection = null;
            RoeId = null;
        }

        #endregion

    }
}
