﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class RevenueQuebecR2 : YearEndR2
    {
        public Employee Employee { get; set; }
        public PersonAddress Address { get; set; }
        public PersonContactChannel Phone { get; set; }
        public BusinessNumber BusinessNumber { get; set; }
    }
}
