﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SalaryPlanGradeMinEffectiveDate : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public string EffectiveDate
        {
            get { return (string)_Key; }
            set { _Key = value; }
        }

        #endregion

        #region construct/destruct
        public SalaryPlanGradeMinEffectiveDate()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(SalaryPlanGradeMinEffectiveDate data)
        {
            data.EffectiveDate = EffectiveDate;
            
        }
        public new void Clear()
        {
            EffectiveDate = DateTime.Now.ToString();
        }
        #endregion
    }
}