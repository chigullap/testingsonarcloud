﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CodeContactChannelType: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        
        #region properties
        //just used as a comparison object
        [DataMember]
        public String ContactTypeChannelCode 
        {
            get { return (String)_Key; }
            set { _Key = value; }
        }

        #endregion

        #region construct/destruct
        public CodeContactChannelType()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(CodeContactChannelType data)
        {
            data.ContactTypeChannelCode = ContactTypeChannelCode;
        }
        public new void Clear()
        {
            ContactTypeChannelCode = "";
        }

        #endregion

    }
}
