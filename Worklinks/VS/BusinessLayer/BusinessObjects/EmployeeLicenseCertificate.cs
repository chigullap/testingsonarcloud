﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeLicenseCertificate : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeLicenseCertificateId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public String EmployeeLicenseCertificateCode { get; set; }

        [DataMember]
        public String EmployeeLicenseCertificateNumber { get; set; }

        [DataMember]
        public DateTime? DateIssued { get; set; }

        [DataMember]
        public DateTime? ExpiryDate { get; set; }

        [DataMember]
        public Decimal? CostEmployee { get; set; }

        [DataMember]
        public Decimal? CostEmployer { get; set; }

        [DataMember]
        public Boolean? JobRelated { get; set; }

        [DataMember]
        public Boolean? JobRequired { get; set; }

        [DataMember]
        public long? AttachmentId { get; set; }

        [DataMember]
        public AttachmentCollection AttachmentObjectCollection { get; set; }

        //public property
        public String Description
        {
            get
            {
                if (AttachmentObjectCollection != null && AttachmentObjectCollection.Count > 0)
                    return AttachmentObjectCollection[0].Description;
                else
                    return null;
            }
        }
        #endregion

        #region construct/destruct
        public EmployeeLicenseCertificate()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(EmployeeLicenseCertificate data)
        {
            base.CopyTo(data);

            data.EmployeeLicenseCertificateId = EmployeeLicenseCertificateId;
            data.EmployeeId = EmployeeId;
            data.EmployeeLicenseCertificateCode = EmployeeLicenseCertificateCode;
            data.EmployeeLicenseCertificateNumber = EmployeeLicenseCertificateNumber;
            data.DateIssued = DateIssued;
            data.ExpiryDate = ExpiryDate;
            data.CostEmployee = CostEmployee;
            data.CostEmployer = CostEmployer;
            data.JobRelated = JobRelated;
            data.JobRequired = JobRequired;
            data.AttachmentId = AttachmentId;

            //attachments
            if (AttachmentObjectCollection != null)
            {
                data.AttachmentObjectCollection = new AttachmentCollection();
                AttachmentObjectCollection.CopyTo(data.AttachmentObjectCollection);
            }
            else
                data.AttachmentObjectCollection = AttachmentObjectCollection;
        }

        public new void Clear()
        {
            base.Clear();

            EmployeeLicenseCertificateId = -1;
            EmployeeId = -1;
            EmployeeLicenseCertificateCode = null;
            EmployeeLicenseCertificateNumber = null;
            DateIssued = null;
            ExpiryDate = null;
            CostEmployee = null;
            CostEmployer = null;
            JobRelated = null;
            JobRequired = null;
            AttachmentId = -1;
            AttachmentObjectCollection = null;
        }
        #endregion
    }
}