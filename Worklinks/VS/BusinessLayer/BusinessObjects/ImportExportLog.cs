﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using  WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ImportExportLog : BusinessObject
    {
        #region fields
        #endregion

        #region properties

        [DataMember]
        public long ImportExportLogId 
        {
            get { return (long)_Key; }
            set { _Key = value; }        
        }
        [DataMember]
        public long? ImportExportId { get; set; }
        [DataMember]
        public DateTime? ImportExportDate { get; set; }
        [DataMember]
        public String FileProcessingDirectory {get;set;}
        [DataMember]
        public String ProcessingOutput {get;set;}
        [DataMember]
        public bool SuccessFlag { get; set; }
        [DataMember]
        public bool WarningFlag { get; set; }
        [DataMember]
        public Guid UniqueIdentifier { get; set; }
        [DataMember]
        public String AlternateExternalIdentifier1 { get; set; }
        [DataMember]
        public String AlternateExternalIdentifier2 { get; set; }
        [DataMember]
        public String AlternateExternalIdentifier3 { get; set; }
        [DataMember]
        public String AlternateExternalIdentifier4 { get; set; }
        [DataMember]
        public long? RelatedImportExportLogId { get; set; }
        [DataMember]
        public ImportExportLog RelatedImportExportLog { get; set; }
        [DataMember]
        public long? RemoteImportExportLogId { get; set; }

        public String DatabaseName { get; set; }

        #endregion

        #region construct/destruct
        public ImportExportLog()
        {
            Clear();
            UniqueIdentifier = Guid.NewGuid();
        }
        public ImportExportLog(Guid uniqueIdentifier)
        {
            Clear();
            UniqueIdentifier = uniqueIdentifier;
        }

        #endregion

        #region clear/copy/clone

        public new void Clear()
        {
            base.Clear();
            ImportExportLogId=-1;
            ImportExportId = null;
            ImportExportDate = DateTime.Now;
            FileProcessingDirectory =null;
            ProcessingOutput =String.Empty;
            SuccessFlag =false;
            WarningFlag =false;
            UniqueIdentifier = Guid.NewGuid();
            AlternateExternalIdentifier1 = null;
            AlternateExternalIdentifier2 = null;
            AlternateExternalIdentifier3 = null;
            AlternateExternalIdentifier4 = null;
            RelatedImportExportLogId = null;
            RemoteImportExportLogId = null;
            RelatedImportExportLog = null;
            DatabaseName = null;
        }

        public void CopyTo(ImportExportLog data)
        {
            base.CopyTo(data);
            data.ImportExportLogId = ImportExportLogId;
            data.ImportExportId = ImportExportId;
            data.ImportExportDate = ImportExportDate;
            data.FileProcessingDirectory = FileProcessingDirectory;
            data.ProcessingOutput = ProcessingOutput;
            data.SuccessFlag = SuccessFlag;
            data.WarningFlag = WarningFlag;
            data.UniqueIdentifier = UniqueIdentifier;
            data.AlternateExternalIdentifier1 = AlternateExternalIdentifier1;
            data.AlternateExternalIdentifier2 = AlternateExternalIdentifier2;
            data.AlternateExternalIdentifier3 = AlternateExternalIdentifier3;
            data.AlternateExternalIdentifier4 = AlternateExternalIdentifier4;
            data.RelatedImportExportLogId = RelatedImportExportLogId;
            data.RemoteImportExportLogId = RemoteImportExportLogId;
            data.DatabaseName = DatabaseName;
            if (RelatedImportExportLog == null)
                data.RelatedImportExportLog = null;
            else
            {
                data.RelatedImportExportLog = new ImportExportLog();
                RelatedImportExportLog.CopyTo(data.RelatedImportExportLog);
            }
        }

        #endregion

        public override object Clone()
        {
            throw new NotImplementedException();
        }
    }
}
