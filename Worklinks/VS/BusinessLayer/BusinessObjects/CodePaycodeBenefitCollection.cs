﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class CodePaycodeBenefitCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<CodePaycodeBenefit>
    {
    }
}