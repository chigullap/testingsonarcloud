﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Runtime.Serialization;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeePaycodeArrearsCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeePaycodeArrears>
    {
        public PayrollTransactionCollection PayrollTransactionCollection
        {
            get
            {
                PayrollTransactionCollection items = new PayrollTransactionCollection();

                foreach (PayrollTransaction trans in this)
                    items.Add(trans);
                return items;
            }

        }

    }

   

}
