﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmailCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<Email>
    {
    }
}