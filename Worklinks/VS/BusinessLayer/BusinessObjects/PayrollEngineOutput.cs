﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollEngineOutput : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PayrollEngineOutputId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long EmployeeId { get; set; }
        [DataMember]
        public Decimal EmployeeCPP { get; set; }
        [DataMember]
        public Decimal EmployeeQPP { get; set; }
        [DataMember]
        public Decimal EmployerCppQpp { get; set; }
        [DataMember]
        public Decimal EmployeeEI { get; set; }
        [DataMember]
        public Decimal EmployerEI { get; set; }
        [DataMember]
        public Decimal EmployeeQpip { get; set; }
        [DataMember]
        public Decimal EmployerQpip { get; set; }
        [DataMember]
        public Decimal FederalTax { get; set; }
        [DataMember]
        public Decimal ProvincialTax { get; set; }
        [DataMember]
        public Decimal QuebecProvincialTax { get; set; }
        [DataMember]
        public Decimal QuebecHeathContribution { get; set; }
        [DataMember]
        public Decimal LumpSumTax { get; set; }
        [DataMember]
        public Decimal CombinedTaxes { get; set; }
        [DataMember]
        public Decimal BonusTaxFederal { get; set; }
        [DataMember]
        public Decimal BonusTaxProvincial { get; set; }
        [DataMember]
        public Decimal BonusEI { get; set; }
        [DataMember]
        public Decimal BonusCPP { get; set; }
        [DataMember]
        public Decimal ElapsedTimeInMilliSeconds { get; set; }

        #endregion

        #region construct/destruct
        public PayrollEngineOutput()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PayrollEngineOutput data)
        {
            base.CopyTo(data);

            data.EmployeeId = EmployeeId;
            data.PayrollEngineOutputId = PayrollEngineOutputId;
            data.EmployeeCPP = EmployeeCPP;
            data.EmployeeQPP = EmployeeQPP;
            data.EmployerCppQpp = EmployerCppQpp;
            data.EmployeeEI = EmployeeEI;
            data.EmployerEI = EmployerEI;
            data.EmployeeQpip = EmployeeQpip;
            data.EmployerQpip = EmployerQpip;
            data.FederalTax = FederalTax;
            data.ProvincialTax = ProvincialTax;
            data.QuebecProvincialTax = QuebecProvincialTax;
            data.QuebecHeathContribution = QuebecHeathContribution;
            data.LumpSumTax = LumpSumTax;
            data.CombinedTaxes = CombinedTaxes;
            data.BonusTaxFederal = BonusTaxFederal;
            data.BonusTaxProvincial = BonusTaxProvincial;
            data.BonusEI = BonusEI;
            data.BonusCPP = BonusCPP;
            data.ElapsedTimeInMilliSeconds = ElapsedTimeInMilliSeconds;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeeId = -1;
            PayrollEngineOutputId = -1;
            EmployeeCPP = 0;
            EmployeeQPP = 0;
            EmployerCppQpp = 0;
            EmployeeEI = 0;
            EmployerEI = 0;
            EmployeeQpip = 0;
            EmployerQpip = 0;
            FederalTax = 0;
            ProvincialTax = 0;
            QuebecProvincialTax = 0;
            QuebecHeathContribution = 0;
            LumpSumTax = 0;
            CombinedTaxes = 0;
            BonusTaxFederal = 0;
            BonusTaxProvincial = 0;
            BonusEI = 0;
            BonusCPP = 0;
            ElapsedTimeInMilliSeconds = 0;
        }
        #endregion
    }
}
