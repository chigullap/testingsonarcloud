﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CodePaycode : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        public enum PaycodeType
        {
            Income = 1,
            Benefit = 2,
            Deduction = 3,
            Employer = 6
        }

        #region fields
        private PaycodeAttachedPaycodeProvisionCollection _attachedPaycodeProvisionCollection = null;
        private PaycodeYearEndReportMapCollection _yearEndReportMapCollection = null;
        private PaycodePayrollTransactionOffsetAssociationCollection _payrollTransactionOffsetAssociationCollection = null;
        #endregion

        #region properties
        [DataMember]
        [Required]
        public String PaycodeCode
        {
            get { return (String)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String PaycodeTypeCode { get; set; }

        [DataMember]
        public String EnglishDescription { get; set; }

        [DataMember]
        public String FrenchDescription { get; set; }

        [DataMember]
        public String PaycodeCodeImportExternalIdentifier { get; set; }

        [DataMember]
        public Decimal? SortOrder { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public bool SystemFlag { get; set; }

        [DataMember]
        public bool IncludeInPayslipFlag { get; set; }

        [DataMember]
        public bool IncludeInPayRegisterFlag { get; set; }

        [DataMember]
        public String GeneralLedgerMask { get; set; }

        [DataMember]
        public bool AllowGeneralLedgerOverrideFlag { get; set; }

        [DataMember]
        public bool AutoPopulateRateFlag { get; set; }

        [DataMember]
        public decimal AmountRateFactor { get; set; }

        [DataMember]
        public bool IncludeEmploymentInsuranceHoursFlag { get; set; }

        [DataMember]
        public String ReportGroupCodePaycodeTypeCd { get; set; }

        [DataMember]
        public bool ReportDisplayUnitFlag { get; set; }

        [DataMember]
        public bool GarnishmentFlag { get; set; }

        [DataMember]
        public bool IncludeInArrearsFlag { get; set; }

        [DataMember]
        public bool RecurringIncomeCodeFlag { get; set; }

        [DataMember]
        public bool VacationCalculationOverrideFlag { get; set; }

        [DataMember]
        public bool SubtractHourFromSalaryFlag { get; set; }

        [DataMember]
        public bool UseSalaryStandardHourFlag { get; set; }

        [DataMember]
        public String PaycodeActivationFrequencyCode { get; set; }

        [DataMember]
        public Decimal? PayPeriodMaximumAmount { get; set; }

        [DataMember]
        public Decimal? PayPeriodMinimumAmount { get; set; }

        [DataMember]
        public Decimal? YearlyMaximumAmount { get; set; }

        [DataMember]
        public Decimal? YearlyMinimumAmount { get; set; }

        [DataMember]
        public Decimal? RequiredMinimumIncomeAmount { get; set; }

        [DataMember]
        public bool TaxOverrideFlag { get; set; }

        [DataMember]
        public PaycodeAttachedPaycodeProvisionCollection AttachedPaycodes
        {
            get
            {
                if (_attachedPaycodeProvisionCollection == null)
                    _attachedPaycodeProvisionCollection = new PaycodeAttachedPaycodeProvisionCollection();

                return _attachedPaycodeProvisionCollection;
            }
            set
            {
                _attachedPaycodeProvisionCollection = value;
            }
        }

        [DataMember]
        public PaycodeYearEndReportMapCollection YearEndReportMappings
        {
            get
            {
                if (_yearEndReportMapCollection == null)
                    _yearEndReportMapCollection = new PaycodeYearEndReportMapCollection();

                return _yearEndReportMapCollection;
            }
            set
            {
                _yearEndReportMapCollection = value;
            }
        }

        [DataMember]
        public PaycodePayrollTransactionOffsetAssociationCollection OffsetAssociations
        {
            get
            {
                if (_payrollTransactionOffsetAssociationCollection == null)
                    _payrollTransactionOffsetAssociationCollection = new PaycodePayrollTransactionOffsetAssociationCollection();

                return _payrollTransactionOffsetAssociationCollection;
            }
            set
            {
                _payrollTransactionOffsetAssociationCollection = value;
            }
        }
        #endregion

        #region construct/destruct
        public CodePaycode()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(CodePaycode data)
        {
            base.CopyTo(data);
            data.PaycodeCode = PaycodeCode;
            data.PaycodeTypeCode = PaycodeTypeCode;
            data.EnglishDescription = EnglishDescription;
            data.FrenchDescription = FrenchDescription;
            data.PaycodeCodeImportExternalIdentifier = PaycodeCodeImportExternalIdentifier;
            data.SortOrder = SortOrder;
            data.StartDate = StartDate;
            data.EndDate = EndDate;
            data.ActiveFlag = ActiveFlag;
            data.SystemFlag = SystemFlag;
            data.IncludeInPayslipFlag = IncludeInPayslipFlag;
            data.IncludeInPayRegisterFlag = IncludeInPayRegisterFlag;
            data.GeneralLedgerMask = GeneralLedgerMask;
            data.AllowGeneralLedgerOverrideFlag = AllowGeneralLedgerOverrideFlag;
            data.AutoPopulateRateFlag = AutoPopulateRateFlag;
            data.AmountRateFactor = AmountRateFactor;
            data.IncludeEmploymentInsuranceHoursFlag = IncludeEmploymentInsuranceHoursFlag;
            data.ReportGroupCodePaycodeTypeCd = ReportGroupCodePaycodeTypeCd;
            data.ReportDisplayUnitFlag = ReportDisplayUnitFlag;
            data.GarnishmentFlag = GarnishmentFlag;
            data.IncludeInArrearsFlag = IncludeInArrearsFlag;
            data.RecurringIncomeCodeFlag = RecurringIncomeCodeFlag;
            data.VacationCalculationOverrideFlag = VacationCalculationOverrideFlag;
            data.SubtractHourFromSalaryFlag = SubtractHourFromSalaryFlag;
            data.UseSalaryStandardHourFlag = UseSalaryStandardHourFlag;
            data.PaycodeActivationFrequencyCode = PaycodeActivationFrequencyCode;
            data.PayPeriodMaximumAmount = PayPeriodMaximumAmount;
            data.PayPeriodMinimumAmount = PayPeriodMinimumAmount;
            data.YearlyMaximumAmount = YearlyMaximumAmount;
            data.YearlyMinimumAmount = YearlyMinimumAmount;
            data.RequiredMinimumIncomeAmount = RequiredMinimumIncomeAmount;
            data.AttachedPaycodes = AttachedPaycodes;
            data.YearEndReportMappings = YearEndReportMappings;
            data.OffsetAssociations = OffsetAssociations;
            data.TaxOverrideFlag = TaxOverrideFlag;
        }
        public new void Clear()
        {
            base.Clear();
            PaycodeCode = null;
            PaycodeTypeCode = null;
            EnglishDescription = null;
            FrenchDescription = null;
            PaycodeCodeImportExternalIdentifier = null;
            SortOrder = 1;
            StartDate = null;
            EndDate = null;
            ActiveFlag = true;
            SystemFlag = true;
            IncludeInPayslipFlag = true;
            IncludeInPayRegisterFlag = true;
            GeneralLedgerMask = null;
            AllowGeneralLedgerOverrideFlag = false;
            AutoPopulateRateFlag = false;
            AmountRateFactor = -1;
            IncludeEmploymentInsuranceHoursFlag = true;
            ReportGroupCodePaycodeTypeCd = null;
            ReportDisplayUnitFlag = false;
            GarnishmentFlag = false;
            IncludeInArrearsFlag = false;
            RecurringIncomeCodeFlag = false;
            VacationCalculationOverrideFlag = false;
            SubtractHourFromSalaryFlag = false;
            UseSalaryStandardHourFlag = false;
            PaycodeActivationFrequencyCode = "NORMAL";
            PayPeriodMaximumAmount = null;
            PayPeriodMinimumAmount = null;
            YearlyMaximumAmount = null;
            YearlyMinimumAmount = null;
            RequiredMinimumIncomeAmount = null;
            AttachedPaycodes = null;
            YearEndReportMappings = null;
            OffsetAssociations = null;
            TaxOverrideFlag = false;
        }
        #endregion
    }
}