﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ContactSummary: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        
        #region properties

        [DataMember]
        public long ContactId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String LastName { get; set; }
        [DataMember]
        public String FirstName { get; set; }
        [DataMember]
        public DateTime EffectiveDate { get; set; }
        [DataMember]
        public String ContactRelationshipCode { get; set; }


        #endregion

        #region construct/destruct
        public ContactSummary()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();
            ContactId = -1;
            LastName = null;
            FirstName = null;
            EffectiveDate = DateTime.Now;
            ContactRelationshipCode = null;
        }

        #endregion
    }
}
