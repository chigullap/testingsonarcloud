﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SalaryPlan : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long SalaryPlanId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public DateTime EffectiveDate { get; set; }

        [DataMember]
        public DateTime EndDate { get; set; }

        [DataMember]
        public String Name { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public String EnglishDescription { get; set; }

        [DataMember]
        public String FrenchDescription { get; set; }
        #endregion

        #region construct/destruct
        public SalaryPlan()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(SalaryPlan data)
        {
            base.CopyTo(data);

            data.SalaryPlanId = SalaryPlanId;
            data.EffectiveDate = EffectiveDate;
            data.EndDate = EndDate;
            data.Name = Name;
            data.ActiveFlag = ActiveFlag;
            data.EnglishDescription = EnglishDescription;
            data.FrenchDescription = FrenchDescription;
        }
        public new void Clear()
        {
            base.Clear();

            SalaryPlanId = -1;
            EffectiveDate = DateTime.Now;
            EndDate = DateTime.Now;
            Name = null;
            ActiveFlag = false;
            EnglishDescription = null;
            FrenchDescription = null;
        }
        #endregion
    }
}