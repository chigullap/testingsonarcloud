﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeePositionWorkdayCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeePositionWorkday>
    {
    }
}