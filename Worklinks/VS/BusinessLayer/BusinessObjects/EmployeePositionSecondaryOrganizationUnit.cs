﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeePositionSecondaryOrganizationUnit : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public virtual long OrganizationUnitLevelId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public virtual long EmployeePositionSecondaryOrganizationUnitId { get; set; }

        [DataMember]
        public long EmployeePositionSecondaryId { get; set; }

        [DataMember]
        public long? OrganizationUnitId { get; set; }

        [DataMember]
        public DateTime? OrganizationUnitStartDate { get; set; }

        [DataMember]
        public bool UsedOnSecondaryEmployeePositionFlag { get; set; }
        #endregion

        #region construct/destruct
        public EmployeePositionSecondaryOrganizationUnit()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            EmployeePositionSecondaryOrganizationUnit rtn = new EmployeePositionSecondaryOrganizationUnit();
            this.CopyTo(rtn);
            return rtn;
        }
        public void CopyTo(EmployeePositionSecondaryOrganizationUnit data)
        {
            base.CopyTo(data);

            data.EmployeePositionSecondaryOrganizationUnitId = EmployeePositionSecondaryOrganizationUnitId;
            data.EmployeePositionSecondaryId = EmployeePositionSecondaryId;
            data.OrganizationUnitLevelId = OrganizationUnitLevelId;
            data.OrganizationUnitId = OrganizationUnitId;
            data.OrganizationUnitStartDate = OrganizationUnitStartDate;
            data.UsedOnSecondaryEmployeePositionFlag = UsedOnSecondaryEmployeePositionFlag;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeePositionSecondaryOrganizationUnitId = -1;
            OrganizationUnitLevelId = -1;
            EmployeePositionSecondaryId = -1;
            OrganizationUnitId = null;
            OrganizationUnitStartDate = null;
            UsedOnSecondaryEmployeePositionFlag = true;
        }
        #endregion
    }
}