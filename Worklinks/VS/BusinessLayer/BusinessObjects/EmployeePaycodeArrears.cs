﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeePaycodeArrears: PayrollTransaction
    {
        #region fields
        private long _employeePaycodeArrearsId = -1;
        #endregion

        #region properties
        public override String Key
        {
            get
            {
                return EmployeePaycodeArrearsId.ToString();
            }
        }
        [DataMember]
        public long EmployeePaycodeArrearsId 
        {
            get { return _employeePaycodeArrearsId; }
            set
            {
                KeyChangedEventArgs args = new KeyChangedEventArgs();
                args.OldKey = _employeePaycodeArrearsId.ToString();
                _employeePaycodeArrearsId = value;
                args.NewKey = Key;
                if (!(args.OldKey ?? String.Empty).Equals(args.NewKey ?? String.Empty) && this.GetType().Equals(typeof(EmployeePaycodeArrears)))
                    OnKeyChanged(args);
            } 
        }
        [DataMember]
        public String CodeEmployeePaycodeArrearsStatusCd { get; set; }
        [DataMember]
        public long PayrollProcessId { get; set; }
        [DataMember]
        public new long? PayrollTransactionId { get; set; }
        [DataMember]
        public long OriginatingPayrollProcessId { get; set; }
        [DataMember]
        public long? ProcessingEmployeePositionId {get; set;}
        #endregion

        #region construct/destruct
        public EmployeePaycodeArrears()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            EmployeePaycodeArrears item = new EmployeePaycodeArrears();
            this.CopyTo(item);
            return item;
        }
        public void CopyTo(EmployeePaycodeArrears data)
        {
            data.EmployeePaycodeArrearsId = EmployeePaycodeArrearsId;
            data.CodeEmployeePaycodeArrearsStatusCd = CodeEmployeePaycodeArrearsStatusCd;
            data.PayrollProcessId = PayrollProcessId;
            data.PayrollTransactionId = PayrollTransactionId;
            data.OriginatingPayrollProcessId = OriginatingPayrollProcessId;
            data.ProcessingEmployeePositionId = ProcessingEmployeePositionId;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeePaycodeArrearsId = -1;
            CodeEmployeePaycodeArrearsStatusCd = null;
            PayrollProcessId = -1;
            PayrollTransactionId = -1;
            OriginatingPayrollProcessId = -1;
            ProcessingEmployeePositionId = null;
        }

        #endregion

    }
}
