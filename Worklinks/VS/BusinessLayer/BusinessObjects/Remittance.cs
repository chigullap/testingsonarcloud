﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class Remittance : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties

        [DataMember]
        public long RemittanceId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String RemittanceType { get; set; }
        [DataMember]
        public String Description { get; set; }
        [DataMember]
        public DateTime? EffectiveEntryDate { get; set; }
        [DataMember]
        public DateTime? RemittancePeriodStartDate { get; set; }
        [DataMember]
        public DateTime? RemittancePeriodEndDate { get; set; }
        [DataMember]
        public Decimal RemittanceAmount { get; set; }
        [DataMember]
        public String FileTransmissionDateString { get; set; }
        [DataMember]
        public long? RbcSequenceNumber { get; set; }
        [DataMember]
        public String DetailType { get; set; }
        [DataMember]
        public String RemitCode { get; set; }
        [DataMember]
        public String ExportId { get; set; }
        [DataMember]
        public String RemitDateString { get; set; }

        #endregion

        #region construct/destruct
        public Remittance()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(Remittance data)
        {
            base.CopyTo(data);

            data.RemittanceId = RemittanceId;
            data.RemittanceType = RemittanceType;
            data.Description = Description;
            data.EffectiveEntryDate = EffectiveEntryDate;
            data.RemittancePeriodStartDate = RemittancePeriodStartDate;
            data.RemittancePeriodEndDate = RemittancePeriodEndDate;
            data.RemittanceAmount = RemittanceAmount;
            data.FileTransmissionDateString = FileTransmissionDateString;
            data.RbcSequenceNumber = RbcSequenceNumber;
            data.DetailType = DetailType;
            data.RemitCode = RemitCode;
            data.ExportId = ExportId;
            data.RemitDateString = RemitDateString;
        }
        public new void Clear()
        {
            base.Clear();

            RemittanceId = -1;
            RemittanceType = null;
            Description = null;
            EffectiveEntryDate = null;
            RemittancePeriodStartDate = null;
            RemittancePeriodEndDate = null;
            RemittanceAmount = 0;
            FileTransmissionDateString = null;
            RbcSequenceNumber = null;
            DetailType = null;
            RemitCode = null;
            ExportId = null;
            RemitDateString = null;
        }
        #endregion
    }
}