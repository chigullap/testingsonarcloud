﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeePositionOrganizationUnit : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public virtual long OrganizationUnitLevelId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public virtual long EmployeePositionOrganizationUnitId { get; set; }

        [DataMember]
        public long EmployeePositionId { get; set; }

        [DataMember]
        public long? OrganizationUnitId { get; set; }

        [DataMember]
        public DateTime? OrganizationUnitStartDate { get; set; }

        [DataMember]
        public bool UsedOnSecondaryEmployeePositionFlag { get; set; }
        #endregion

        #region construct/destruct
        public EmployeePositionOrganizationUnit()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            EmployeePositionOrganizationUnit rtn = new EmployeePositionOrganizationUnit();
            this.CopyTo(rtn);
            return rtn;
        }
        public void CopyTo(EmployeePositionOrganizationUnit data)
        {
            base.CopyTo(data);

            data.EmployeePositionOrganizationUnitId = EmployeePositionOrganizationUnitId;
            data.EmployeePositionId = EmployeePositionId;
            data.OrganizationUnitLevelId = OrganizationUnitLevelId;
            data.OrganizationUnitId = OrganizationUnitId;
            data.OrganizationUnitStartDate = OrganizationUnitStartDate;
            data.UsedOnSecondaryEmployeePositionFlag = UsedOnSecondaryEmployeePositionFlag;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeePositionOrganizationUnitId = -1;
            OrganizationUnitLevelId = -1;
            EmployeePositionId = -1;
            OrganizationUnitId = null;
            OrganizationUnitStartDate = null;
            UsedOnSecondaryEmployeePositionFlag = true;
        }
        #endregion

        public bool Equals(EmployeePositionOrganizationUnit other)
        {
            return this.OrganizationUnitId.Equals(other.OrganizationUnitId);
        }
    }
}