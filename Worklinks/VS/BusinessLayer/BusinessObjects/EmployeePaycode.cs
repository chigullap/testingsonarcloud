﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeePaycode : CodePaycode
    {
        #region properties
        [DataMember]
        public virtual long EmployeePaycodeId { get; set; }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public Decimal? AmountRate { get; set; }

        [DataMember]
        public Decimal? AmountUnits { get; set; }

        [DataMember]
        public Decimal? AmountPercentage { get; set; }

        [DataMember]
        public Decimal? EmploymentInsuranceInsurableHoursPerUnit { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? CutoffDate { get; set; }

        [DataMember]
        public Decimal? PayPeriodMinimum { get; set; }

        [DataMember]
        public Decimal? PayPeriodMaximum { get; set; }

        [DataMember]
        public Decimal? MonthlyMaximum { get; set; }

        [DataMember]
        public Decimal? YearlyMaximum { get; set; }

        [DataMember]
        public Decimal? YearlyMinimumCalculationOffset { get; set; }

        [DataMember]
        public Decimal? ExcludeAmount { get; set; }

        [DataMember]
        public Decimal? ExcludePercentage { get; set; }

        [DataMember]
        public Decimal? GroupIncomeFactor { get; set; }

        [DataMember]
        public Decimal? RoundUpTo { get; set; }

        [DataMember]
        public Decimal? RatePerRoundUpTo { get; set; }

        [DataMember]
        public Decimal? ExemptAmount { get; set; }

        [DataMember]
        public String ExemptAmountCodeNetGrossCd { get; set; }

        [DataMember]
        public String ExemptAmountCodeBeforeAfterCd { get; set; }

        [DataMember]
        public Decimal? ExemptPercentage { get; set; }

        [DataMember]
        public String ExemptPercentageCodeNetGrossCd { get; set; }

        [DataMember]
        public String ExemptPercentageCodeBeforeAfterCd { get; set; }

        [DataMember]
        public long? GarnishmentVendorId { get; set; }

        [DataMember]
        public String GarnishmentOrderNumber { get; set; }

        [DataMember]
        public String OrganizationUnit { get; set; }

        [DataMember]
        public String OrganizationUnitDescription { get; set; }

        [DataMember]
        public bool EligibleByStatus { get; set; }

        [DataMember]
        public bool GlobalEmployeePaycodeFlag { get; set; }

        [DataMember]
        public bool OverrideConcurrencyCheck { get; set; }

        [DataMember]
        public EmployeeRemittanceStubCollection RemittanceStubCollection { get; set; }
        #endregion

        #region construct/destruct
        public EmployeePaycode()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeePaycode data)
        {
            base.CopyTo(data);

            data.EmployeePaycodeId = EmployeePaycodeId;
            data.EmployeeId = EmployeeId;
            data.PaycodeCode = PaycodeCode;
            data.AmountRate = AmountRate;
            data.AmountUnits = AmountUnits;
            data.AmountPercentage = AmountPercentage;
            data.EmploymentInsuranceInsurableHoursPerUnit = EmploymentInsuranceInsurableHoursPerUnit;
            data.StartDate = StartDate;
            data.CutoffDate = CutoffDate;
            data.PayPeriodMinimum = PayPeriodMinimum;
            data.PayPeriodMaximum = PayPeriodMaximum;
            data.MonthlyMaximum = MonthlyMaximum;
            data.YearlyMaximum = YearlyMaximum;
            data.YearlyMinimumCalculationOffset = YearlyMinimumCalculationOffset;
            data.ExcludeAmount = ExcludeAmount;
            data.ExcludePercentage = ExcludePercentage;
            data.GroupIncomeFactor = GroupIncomeFactor;
            data.RoundUpTo = RoundUpTo;
            data.RatePerRoundUpTo = RatePerRoundUpTo;
            data.ExemptAmount = ExemptAmount;
            data.ExemptAmountCodeNetGrossCd = ExemptAmountCodeNetGrossCd;
            data.ExemptAmountCodeBeforeAfterCd = ExemptAmountCodeBeforeAfterCd;
            data.ExemptPercentage = ExemptPercentage;
            data.ExemptPercentageCodeNetGrossCd = ExemptPercentageCodeNetGrossCd;
            data.ExemptPercentageCodeBeforeAfterCd = ExemptPercentageCodeBeforeAfterCd;
            data.GarnishmentVendorId = GarnishmentVendorId;
            data.GarnishmentOrderNumber = GarnishmentOrderNumber;
            data.OrganizationUnit = OrganizationUnit;
            data.OrganizationUnitDescription = OrganizationUnitDescription;
            data.GlobalEmployeePaycodeFlag = GlobalEmployeePaycodeFlag;
            data.OverrideConcurrencyCheck = OverrideConcurrencyCheck;

            if (RemittanceStubCollection != null)
            {
                data.RemittanceStubCollection = new EmployeeRemittanceStubCollection();
                RemittanceStubCollection.CopyTo(data.RemittanceStubCollection);
            }
            else
                data.RemittanceStubCollection = RemittanceStubCollection;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeePaycodeId = -1;
            EmployeeId = -1;
            PaycodeCode = null;
            AmountRate = null;
            AmountUnits = null;
            AmountPercentage = null;
            EmploymentInsuranceInsurableHoursPerUnit = null;
            StartDate = null;
            CutoffDate = null;
            PayPeriodMinimum = null;
            PayPeriodMaximum = null;
            MonthlyMaximum = null;
            YearlyMaximum = null;
            YearlyMinimumCalculationOffset = null;
            ExcludeAmount = null;
            ExcludePercentage = null;
            GroupIncomeFactor = null;
            RoundUpTo = null;
            RatePerRoundUpTo = null;
            ExemptAmount = null;
            ExemptAmountCodeNetGrossCd = null;
            ExemptAmountCodeBeforeAfterCd = null;
            ExemptPercentage = null;
            ExemptPercentageCodeNetGrossCd = null;
            ExemptPercentageCodeBeforeAfterCd = null;
            GarnishmentVendorId = null;
            GarnishmentOrderNumber = null;
            OrganizationUnit = null;
            OrganizationUnitDescription = null;
            GlobalEmployeePaycodeFlag = false;
            OverrideConcurrencyCheck = false;
            RemittanceStubCollection = null;
        }
        #endregion
    }
}