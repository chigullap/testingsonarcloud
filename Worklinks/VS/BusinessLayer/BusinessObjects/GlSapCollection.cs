﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class GlSapCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<GlSap>
    {
        public Decimal TotalDebitAmount
        {
            get
            {
                Decimal totalAmount = 0;
                foreach (GlSap rec in this)
                {
                    totalAmount += rec.DebitAmount;
                }
                return totalAmount;
            }
        }
        public Decimal TotalCreditAmount
        {
            get
            {
                Decimal totalAmount = 0;
                foreach (GlSap rec in this)
                {
                    totalAmount += rec.CreditAmount;
                }
                return totalAmount;
            }
        }
    }
}

