﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class YearEndR1Collection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<YearEndR1>
    {
    }
}