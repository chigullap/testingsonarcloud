﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class CraRemittanceSummaryCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<CraRemittanceSummary>
    {
        public decimal SumPaymentAmount
        {
            get
            {
                Decimal rtn = 0;

                foreach (CraRemittanceSummary item in this)
                {
                    rtn += item.PaymentAmount;
                }

                return rtn;
            }
        }
    }
}
