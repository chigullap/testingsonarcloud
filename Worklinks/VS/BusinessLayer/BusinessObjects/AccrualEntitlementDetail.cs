﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class AccrualEntitlementDetail : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long AccrualEntitlementDetailId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long AccrualEntitlementId { get; set; }

        [DataMember]
        public DateTime? EffectiveDate { get; set; }

        [DataMember]
        public long? PayrollProcessId { get; set; }

        [DataMember]
        public String EmploymentDateCode { get; set; }

        [DataMember]
        public String EntitlementRecurrenceCode { get; set; }
        #endregion

        #region construct/destruct
        public AccrualEntitlementDetail()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(AccrualEntitlementDetail data)
        {
            base.CopyTo(data);

            data.AccrualEntitlementDetailId = AccrualEntitlementDetailId;
            data.AccrualEntitlementId = AccrualEntitlementId;
            data.EffectiveDate = EffectiveDate;
            data.PayrollProcessId = PayrollProcessId;
            data.EmploymentDateCode = EmploymentDateCode;
            data.EntitlementRecurrenceCode = EntitlementRecurrenceCode;
        }
        public new void Clear()
        {
            base.Clear();

            AccrualEntitlementDetailId = -1;
            AccrualEntitlementId = -1;
            EffectiveDate = null;
            PayrollProcessId = null;
            EmploymentDateCode = null;
            EntitlementRecurrenceCode = null;
        }
        #endregion
    }
}