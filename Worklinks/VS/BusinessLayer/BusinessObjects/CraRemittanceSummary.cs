﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CraRemittanceSummary : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        public long DummyKey
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String BusinessTaxNumber { get; set; }
        [DataMember]
        public DateTime ChequeDate { get; set; }
        [DataMember]
        public Int64 EmployeeCount { get; set; }
        [DataMember]
        public decimal PaymentAmount { get; set; }
        [DataMember]
        public decimal TotalTaxableIncome { get; set; }
        [DataMember]
        public string CodeCraRemittancePeriodCd { get; set; }
        [DataMember]
        public DateTime? RemittancePeriodStartDate { get; set; }
        [DataMember]
        public DateTime? RemittancePeriodEndDate { get; set; }
        [DataMember]
        public DateTime? EffectiveEntryDate { get; set; }
        #endregion

        #region construct/destruct
        public CraRemittanceSummary()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(CraRemittanceSummary data)
        {
            base.CopyTo(data);

            data.DummyKey = DummyKey;
            data.BusinessTaxNumber = BusinessTaxNumber;
            data.ChequeDate = ChequeDate;
            data.EmployeeCount = EmployeeCount;
            data.PaymentAmount = PaymentAmount;
            data.TotalTaxableIncome = TotalTaxableIncome;
            data.CodeCraRemittancePeriodCd = CodeCraRemittancePeriodCd;
            data.RemittancePeriodStartDate = RemittancePeriodStartDate;
            data.RemittancePeriodEndDate = RemittancePeriodEndDate;
            data.EffectiveEntryDate = EffectiveEntryDate;
        }

        public new void Clear()
        {
            base.Clear();

            DummyKey = -1;
            BusinessTaxNumber = null;
            ChequeDate = DateTime.Now;
            EmployeeCount = 0;
            PaymentAmount = 0;
            TotalTaxableIncome = 0;
            CodeCraRemittancePeriodCd = null;
            RemittancePeriodStartDate = null;
            RemittancePeriodEndDate = null;
            EffectiveEntryDate = null;
        }

        #endregion
    }
}
