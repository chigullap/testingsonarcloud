﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeePayslip : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PayrollProcessId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public String EmployeeNumber { get; set; }

        [DataMember]
        public DateTime CutoffDate { get; set; }

        [DataMember]
        public DateTime ChequeDate { get; set; }

        [DataMember]
        public Decimal TotalGross { get; set; }

        [DataMember]
        public Decimal TotalDeductions { get; set; }

        [DataMember]
        public Decimal TotalTaxes { get; set; }

        [DataMember]
        public Decimal NetPay { get; set; }
        #endregion

        #region construct/destruct
        public EmployeePayslip()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeePayslip data)
        {
            base.CopyTo(data);

            data.PayrollProcessId = PayrollProcessId;
            data.EmployeeId = EmployeeId;
            data.EmployeeNumber = EmployeeNumber;
            data.CutoffDate = CutoffDate;
            data.ChequeDate = ChequeDate;
            data.TotalGross = TotalGross;
            data.TotalDeductions = TotalDeductions;
            data.TotalTaxes = TotalTaxes;
            data.NetPay = NetPay;
        }
        public new void Clear()
        {
            base.Clear();

            PayrollProcessId = -1;
            EmployeeId = -1;
            EmployeeNumber = null;
            CutoffDate = DateTime.Now;
            ChequeDate = DateTime.Now;
            TotalGross = 0;
            TotalDeductions = 0;
            TotalTaxes = 0;
            NetPay = 0;
        }
        #endregion
    }
}