﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployerContributionAmountCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployerContributionAmount>
    {
    }
}
