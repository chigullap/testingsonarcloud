﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SalaryPlanGrade : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long SalaryPlanGradeId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long SalaryPlanId { get; set; }

        [DataMember]
        public String Name { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public String EnglishDescription { get; set; }

        [DataMember]
        public String FrenchDescription { get; set; }
        #endregion

        #region construct/destruct
        public SalaryPlanGrade()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(SalaryPlanGrade data)
        {
            base.CopyTo(data);

            data.SalaryPlanGradeId = SalaryPlanGradeId;
            data.SalaryPlanId = SalaryPlanId;
            data.Name = Name;
            data.ActiveFlag = ActiveFlag;
            data.EnglishDescription = EnglishDescription;
            data.FrenchDescription = FrenchDescription;
        }
        public new void Clear()
        {
            base.Clear();

            SalaryPlanGradeId = -1;
            SalaryPlanId = -1;
            Name = null;
            ActiveFlag = false;
            EnglishDescription = null;
            FrenchDescription = null;
        }
        #endregion
    }
}