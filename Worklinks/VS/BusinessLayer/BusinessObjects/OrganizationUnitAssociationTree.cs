﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class OrganizationUnitAssociationTree : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long OrganizationUnitAssociationId {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long? ParentOrganizationUnitId { get; set; }
        [DataMember]
        public long OrganizationUnitId { get; set; }
        [DataMember]
        public String Description { get; set; }


        #endregion

        #region construct/destruct
        public OrganizationUnitAssociationTree()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PayDetail data)
        {
            throw new NotImplementedException();
        }

        public new void Clear()
        {
            base.Clear();

            OrganizationUnitAssociationId = -1;
            ParentOrganizationUnitId = null;
            OrganizationUnitId = -1;
            Description = "";
        }

        #endregion

    }
}
