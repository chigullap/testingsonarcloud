﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class CodePaycodeIncomeCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<CodePaycodeIncome>
    {
    }
}