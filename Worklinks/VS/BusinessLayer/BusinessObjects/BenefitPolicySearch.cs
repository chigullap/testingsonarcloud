﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class BenefitPolicySearch : BusinessObject
    {
        #region properties
        [DataMember]
        public long BenefitPolicyId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long BenefitPolicyDescriptionId { get; set; }

        [DataMember]
        public String BenefitPolicyDescriptionField { get; set; }
        #endregion

        #region construct/destruct
        public BenefitPolicySearch()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(BenefitPolicySearch data)
        {
            base.CopyTo(data);

            data.BenefitPolicyId = BenefitPolicyId;
            data.BenefitPolicyDescriptionId = BenefitPolicyDescriptionId;
            data.BenefitPolicyDescriptionField = BenefitPolicyDescriptionField;
        }
        public new void Clear()
        {
            base.Clear();

            BenefitPolicyId = -1;
            BenefitPolicyDescriptionId = -1;
            BenefitPolicyDescriptionField = "";
        }
        #endregion
    }
}
