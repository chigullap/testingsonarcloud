﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SecurityUser : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {


        #region properties
        [DataMember]
        public long SecurityUserId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String UserName { get; set; }

        [DataMember]
        public String Email { get; set; }

        [DataMember]
        public long? ContactChannelId { get; set; }

        [DataMember]
        public String Password { get; set; }

        [DataMember]
        public String PasswordSalt { get; set; }

        [DataMember]
        public long? PersonId { get; set; }

        [DataMember]
        public DateTime? LastFailedPasswordAttemptDatetime { get; set; }

        [DataMember]
        public int LastFailedPasswordAttemptCount { get; set; }

        [DataMember]
        public DateTime? LastLoginDate { get; set; }

        [DataMember]
        public bool WorklinksAdministrationFlag { get; set; }

        [DataMember]
        public bool IsLockedOutFlag { get; set; }

        [DataMember]
        public DateTime? LastLockoutDatetime { get; set; }

        [DataMember]
        public DateTime? LastPasswordChangedDatetime { get; set; }

        [DataMember]
        public String Comment { get; set; }

        [DataMember]
        public DateTime? LastActivityDatetime { get; set; }

        [DataMember]
        public DateTime? PasswordExpiryDatetime { get; set; }

        [DataMember]
        public DateTime? LoginExpiryDatetime { get; set; }

        [DataMember]
        public String LastName { get; set; }

        [DataMember]
        public String FirstName { get; set; }

        //regular property
        [DataMember]
        public bool IsEmployee { get; set; }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public String PasswordEntry { get; set; }

        [DataMember]
        public String ConfirmPasswordEntry { get; set; }

        [DataMember]
        public long? AttachmentId { get; set; }

        [DataMember]
        public Attachment AttachmentObject { get; set; }

        [DataMember]
        public byte[] Signature
        {
            get
            {
                if (AttachmentObject == null)
                    return null;
                else
                    return AttachmentObject.Data;
            }
            set
            {
                if (AttachmentObject == null)
                    AttachmentObject = new Attachment() { Data = value };
                else
                    AttachmentObject.Data = value;
            }
        }

        #endregion

        #region construct/destruct
        public SecurityUser()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(SecurityUser data)
        {
            base.CopyTo(data);
            data.SecurityUserId = SecurityUserId;
            data.UserName = UserName;
            data.Email = Email;
            data.ContactChannelId = ContactChannelId;
            data.Password = Password;
            data.PasswordSalt = PasswordSalt;
            data.PersonId = PersonId;
            data.LastFailedPasswordAttemptDatetime = LastFailedPasswordAttemptDatetime;
            data.LastFailedPasswordAttemptCount = LastFailedPasswordAttemptCount;
            data.LastLoginDate = LastLoginDate;
            data.WorklinksAdministrationFlag = WorklinksAdministrationFlag;
            data.IsLockedOutFlag = IsLockedOutFlag;
            data.LastLockoutDatetime = LastLockoutDatetime;
            data.LastPasswordChangedDatetime = LastPasswordChangedDatetime;
            data.Comment = Comment;
            data.LastActivityDatetime = LastActivityDatetime;
            data.PasswordExpiryDatetime = PasswordExpiryDatetime;
            data.LoginExpiryDatetime = LoginExpiryDatetime;
            data.LastName = LastName;
            data.FirstName = FirstName;

            data.AttachmentId = AttachmentId;
            //attachments
            if (AttachmentObject != null)
            {
                data.AttachmentObject = new Attachment();
                AttachmentObject.CopyTo(data.AttachmentObject);
            }
            else
                data.AttachmentObject = AttachmentObject;
        }

        public new void Clear()
        {
            base.Clear();
            SecurityUserId = -1;
            UserName = "";
            Email = "";
            ContactChannelId = null;
            Password = null;
            PasswordSalt = null;
            PersonId = null;
            LastFailedPasswordAttemptDatetime = null;
            LastFailedPasswordAttemptCount = 0;
            LastLoginDate = null;
            WorklinksAdministrationFlag = false;
            IsLockedOutFlag = false;
            LastLockoutDatetime = null;
            LastPasswordChangedDatetime = null;
            LoginExpiryDatetime = null;
            Comment = null;
            LastActivityDatetime = null;
            PasswordExpiryDatetime = null;
            LastName = "";
            FirstName = "";
            AttachmentId = null;
            AttachmentObject = null;
        }
        #endregion


    }
}