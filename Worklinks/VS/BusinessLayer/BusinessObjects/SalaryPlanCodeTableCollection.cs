﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class SalaryPlanCodeTableCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<SalaryPlanCodeTable>
    {
    }
}