﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class RemittanceDetailGarnishmentDeductions : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties

        [DataMember]
        public long DummyKey
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String GarnishmentOrderNumber { get; set; }
        [DataMember]
        public String EmployeeName { get; set; }
        [DataMember]
        public DateTime ChequeDate { get; set; }
        [DataMember]
        public String CodePaycodeCd { get; set; }
        [DataMember]
        public Decimal Amount { get; set; }
        [DataMember]
        public Decimal Period { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public DateTime CutoffDate { get; set; }
        [DataMember]
        public long PayrollProcessId { get; set; }

        #endregion

        #region construct/destruct
        public RemittanceDetailGarnishmentDeductions()
        {
            //this object is only used to show details of a remittance, it is not used to copy/store data, so no Clear() or CopyTo() is coded.
        }

        public override object Clone()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}