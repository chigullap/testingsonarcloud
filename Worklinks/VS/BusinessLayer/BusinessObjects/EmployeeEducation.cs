﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeEducation : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeEducationId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public Employee ParentEmployee { get; set; }

        [DataMember]
        public String EmployeeEducationDegreeCode { get; set; }

        [DataMember]
        public String EmployeeEducationMajorCode { get; set; }

        [DataMember]
        public String School { get; set; }

        [DataMember]
        public Decimal? YearEarned { get; set; }

        [DataMember]
        public String CountryCode { get; set; }

        [DataMember]
        public short? CreditHour { get; set; }

        [DataMember]
        public String EmployeeEducationCreditHourCode { get; set; }

        [DataMember]
        public long? AttachmentId { get; set; }

        [DataMember]
        public bool JobRelatedFlag { get; set; }

        [DataMember]
        public bool JobRequiredFlag { get; set; }

        [DataMember]
        public AttachmentCollection AttachmentObjectCollection { get; set; }

        //public property
        public String Description
        {
            get
            {
                if (AttachmentObjectCollection != null && AttachmentObjectCollection.Count > 0)
                    return AttachmentObjectCollection[0].Description;
                else
                    return null;
            }
        }
        #endregion

        #region construct/destruct
        public EmployeeEducation()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public new void Clear()
        {
            base.Clear();
            EmployeeEducationId = -1;
            EmployeeId = -1;
            ParentEmployee = null;
            EmployeeEducationDegreeCode = null;
            EmployeeEducationMajorCode = null;
            School = null;
            YearEarned = -1;
            CountryCode = null;
            CreditHour = -1;
            EmployeeEducationCreditHourCode = null;
            AttachmentId = -1;
            JobRelatedFlag = false;
            JobRequiredFlag = false;
            AttachmentObjectCollection = null;
        }

        public void CopyTo(EmployeeEducation data)
        {
            base.CopyTo(data);

            data.EmployeeEducationId = EmployeeEducationId;
            data.EmployeeId = EmployeeId;
            data.ParentEmployee = ParentEmployee;
            data.EmployeeEducationDegreeCode = EmployeeEducationDegreeCode;
            data.EmployeeEducationMajorCode = EmployeeEducationMajorCode;
            data.School = School;
            data.YearEarned = YearEarned;
            data.CountryCode = CountryCode;
            data.CreditHour = CreditHour;
            data.EmployeeEducationCreditHourCode = EmployeeEducationCreditHourCode;
            data.AttachmentId = AttachmentId;
            data.JobRelatedFlag = JobRelatedFlag;
            data.JobRequiredFlag = JobRequiredFlag;

            //attachments
            if (AttachmentObjectCollection != null)
            {
                data.AttachmentObjectCollection = new AttachmentCollection();
                AttachmentObjectCollection.CopyTo(data.AttachmentObjectCollection);
            }
            else
                data.AttachmentObjectCollection = AttachmentObjectCollection;
        }
        #endregion
    }
}