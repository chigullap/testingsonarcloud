﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [System.Xml.Serialization.XmlInclude(typeof(Xsd.WorkLinksPersonAddress))]
    [Serializable]
    [DataContract]
    public class Employee : Person
    {
        #region fields
        private EmployeeBankingCollection _bankingCollection = null;
        private EmployeePaycodeCollection _employeePaycodeCollection = null;
        private EmployeeEmploymentEquityCollection _employmentEquityCollection = null;
        private long _employeeId = -1;
        #endregion

        #region data properties
        [DataMember]
        public long EmployeeId
        {
            get { return _employeeId; }
            set
            {
                KeyChangedEventArgs args = new KeyChangedEventArgs();
                args.OldKey = _employeeId.ToString();
                _employeeId = value;
                args.NewKey = Key;
                if (!(args.OldKey ?? String.Empty).Equals(args.NewKey ?? String.Empty) && this.GetType().Equals(typeof(Employee)))
                    OnKeyChanged(args);
            }
        }

        [DataMember]
        [Required]
        public String EmployeeNumber { get; set; }

        [DataMember]
        public String MaritalStatusCode { get; set; }

        [DataMember]
        public DateTime? MaritalStatusEffectiveDate { get; set; }

        [DataMember]
        public String BilingualismCode { get; set; }

        [DataMember]
        public String HealthCareNumber { get; set; }

        [DataMember]
        public String HealthCareProvinceStateCode { get; set; }

        [DataMember]
        public String CitizenshipCode { get; set; }

        [DataMember]
        public String BadgeNumber { get; set; }

        [DataMember]
        virtual public String ImportExternalIdentifier { get; set; }

        [DataMember]
        public ContactCollection Contacts { get; set; }

        [DataMember]
        public ContactTypeCollection ContactTypes { get; set; }

        [DataMember]
        public virtual PersonAddress PrimaryAddress { get; set; }

        [DataMember]
        public StatutoryDeductionSummary StatutoryDeductionSummary { get; set; }

        [DataMember]
        public EmployeeEmploymentInformation EmploymentInformation { get; set; }

        [DataMember]
        public EmployeePosition CurrentEmployeePosition { get; set; }

        [DataMember]
        public EmployeeTerminationRoe CurrentEmployeeTerminationRoe { get; set; }

        [DataMember]
        public EmployeeRoeAmountSummary EmployeeRoeAmountSummary { get; set; }

        [DataMember]
        public EmployeePaycodeCollection Paycodes
        {
            get
            {
                if (_employeePaycodeCollection == null)
                    _employeePaycodeCollection = new EmployeePaycodeCollection();

                return _employeePaycodeCollection;
            }
            set
            {
                _employeePaycodeCollection = value;
            }
        }

        [DataMember]
        public EmployeePhoto EmployeePhotoObject { get; set; }

        [DataMember]
        public byte[] EmployeePhoto
        {
            get
            {
                if (EmployeePhotoObject == null)
                    return null;
                else
                    return EmployeePhotoObject.Data;
            }
            set
            {
                if (EmployeePhotoObject == null)
                    EmployeePhotoObject = new EmployeePhoto() { EmployeeId = this.EmployeeId, Data = value };
                else
                    EmployeePhotoObject.Data = value;
            }
        }

        [DataMember]
        public EmployeeBankingCollection BankingCollection
        {
            get
            {
                if (_bankingCollection == null)
                    _bankingCollection = new EmployeeBankingCollection();

                return _bankingCollection;
            }
            set
            {
                _bankingCollection = value;
            }
        }

        [DataMember]
        public EmployeeEmploymentEquityCollection EmploymentEquities
        {
            get
            {
                if (_employmentEquityCollection == null)
                    _employmentEquityCollection = new EmployeeEmploymentEquityCollection();

                return _employmentEquityCollection;
            }
            set
            {
                _employmentEquityCollection = value;
            }
        }
        #endregion

        //used in payroll validation when calculation button is pressed
        public bool MeetsPayrollRequirements()
        {
            return GetPayrollRequirementsValidationInformation() == null;
        }
        public String GetPayrollRequirementsValidationInformation()
        {
            String validationMessage = null;
            StringBuilder output = new StringBuilder();

            //check if all required fields are present for payroll in the Employee object
            if (!IsValid)
                output.Append(GetValidationInformation());

            if (PrimaryAddress == null)
                output.AppendLine("Employee:PrimaryAddress missing.");
            else if (!PrimaryAddress.IsValid)
                output.Append(PrimaryAddress.GetValidationInformation());

            if (StatutoryDeductionSummary == null)
                output.AppendLine("Employee:StatutoryDeductionSummary missing.");
            else if (!StatutoryDeductionSummary.IsValid)
                output.Append(StatutoryDeductionSummary.GetValidationInformation());

            if (EmploymentInformation == null)
                output.AppendLine("Employee:EmploymentInformation missing.");
            else if (!EmploymentInformation.IsValid)
                output.Append(EmploymentInformation.GetValidationInformation());

            if (CurrentEmployeePosition == null)
                output.AppendLine("Employee:CurrentEmployeePosition missing.");
            else if (!CurrentEmployeePosition.IsValid)
                output.Append(CurrentEmployeePosition.GetValidationInformation());

            if (Paycodes.Count > 0 && !Paycodes.IsValid)
                output.Append(Paycodes.GetValidationInformation());

            if (BankingCollection.Count > 0 && !BankingCollection.IsValid)
                output.Append(BankingCollection.GetValidationInformation());

            if (output.Length > 0)
            {
                //add employee number to message
                validationMessage = String.Format("Employee {0}: ", EmployeeNumber) + Environment.NewLine;
                validationMessage += output.ToString();
            }

            return validationMessage;
        }
      
        #region calc properties
        public override string Key
        {
            get
            {
                return EmployeeId.ToString();
            }
        }
        public IList<ContactType> DeletedContactTypes
        {
            get
            {
                IList<ContactType> rtn = null;

                if (ContactTypes != null)
                    rtn = ContactTypes.DeletedItems;

                return rtn;
            }
        }
        public EmployeeBanking PrimaryBank
        {
            get
            {
                if (BankingCollection.Count == 0)
                    return null;
                else
                    return _bankingCollection.SortedCollection[0]; //return lowest sequence code
            }
        }
        #endregion

        #region construct/destruct
        public Employee()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            Employee item = new Employee();
            this.CopyTo(item);
            return item;
        }
        public new void Clear()
        {
            base.Clear();
            EmployeeId = -1;
            EmployeeNumber = null;
            MaritalStatusCode = null;
            MaritalStatusEffectiveDate = null;
            BilingualismCode = null;
            HealthCareNumber = null;
            HealthCareProvinceStateCode = null;
            CitizenshipCode = null;
            BadgeNumber = null;
            ImportExternalIdentifier = null;
            Contacts = null;
            ContactTypes = null;
            PrimaryAddress = null;
            BankingCollection = null;
            StatutoryDeductionSummary = null;
            EmploymentInformation = null;
            CurrentEmployeePosition = null;
            CurrentEmployeeTerminationRoe = null;
            Paycodes = null;
            EmployeeRoeAmountSummary = null;
            EmployeePhotoObject = null;
            EmploymentEquities = null;
        }
        public virtual void CopyTo(Employee data)
        {
            base.CopyTo(data);
            data.EmployeeId = EmployeeId;
            data.EmployeeNumber = EmployeeNumber;
            data.MaritalStatusCode = MaritalStatusCode;
            data.MaritalStatusEffectiveDate = MaritalStatusEffectiveDate;
            data.BilingualismCode = BilingualismCode;
            data.HealthCareNumber = HealthCareNumber;
            data.HealthCareProvinceStateCode = HealthCareProvinceStateCode;
            data.CitizenshipCode = CitizenshipCode;
            data.BadgeNumber = BadgeNumber;
            data.ImportExternalIdentifier = ImportExternalIdentifier;
            data.Contacts = Contacts;
            data.ContactTypes = ContactTypes;
            data.PrimaryAddress = PrimaryAddress;
            data.StatutoryDeductionSummary = StatutoryDeductionSummary;
            data.EmploymentInformation = EmploymentInformation;
            data.CurrentEmployeePosition = CurrentEmployeePosition;
            data.CurrentEmployeeTerminationRoe = CurrentEmployeeTerminationRoe;
            data.EmployeeRoeAmountSummary = EmployeeRoeAmountSummary;
            data.EmployeePhotoObject = EmployeePhotoObject;
            data.EmploymentEquities = EmploymentEquities;
        }
        #endregion
    }
}