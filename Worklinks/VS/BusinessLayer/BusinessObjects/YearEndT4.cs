﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class YearEndT4 : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long YearEndT4Id
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long EmployeeId { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string AddressLine1 { get; set; }
        [DataMember]
        public string AddressLine2 { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string PostalZipCode { get; set; }
        [DataMember]
        public string ProvinceStateCode { get; set; }
        [DataMember]
        public string CountryCode { get; set; }
        [DataMember]
        public string SocialInsuranceNumberSegment1 { get; set; }
        [DataMember]
        public string SocialInsuranceNumberSegment2 { get; set; }
        [DataMember]
        public string SocialInsuranceNumberSegment3 { get; set; }
        [DataMember]
        public string EmployeeNumber { get; set; }
        [DataMember]
        public string EmployerNumber { get; set; }
        [DataMember]
        public string TaxableCodeProvinceStateCd { get; set; }
        [DataMember]
        public short? T4Sequence { get; set; }
        [DataMember]
        public decimal Year { get; set; }
        [DataMember]
        public decimal EmploymentIncome14 { get; set; }
        [DataMember]
        public decimal CanadaPensionPlanDeducted16 { get; set; }
        [DataMember]
        public decimal QuebecPensionPlanDeducted17 { get; set; }
        [DataMember]
        public decimal EmploymentInsuranceDeducted18 { get; set; }
        [DataMember]
        public decimal RegisteredPension20 { get; set; }
        [DataMember]
        public decimal IncomeTaxDeducted22 { get; set; }
        [DataMember]
        public decimal EmploymentInsuranceEarnings24 { get; set; }
        [DataMember]
        public decimal CanadaPensionPlanEarn26 { get; set; }
        [DataMember]
        public decimal UnionDues44 { get; set; }
        [DataMember]
        public decimal CharitableDonate46 { get; set; }
        [DataMember]
        public string OriginalRegistrationNumber50 { get; set; }
        [DataMember]
        public decimal PensionAdjustment52 { get; set; }
        [DataMember]
        public decimal EmployerCanadaPensionPlanAmount { get; set; }
        [DataMember]
        public decimal EmployerEmploymentInsuranceAmount { get; set; }
        [DataMember]
        public bool EmploymentInsuranceExemption28Flag { get; set; }
        [DataMember]
        public bool CanadaPensionPlanExempt28Flag { get; set; }
        [DataMember]
        public decimal EmploymentInsuranceEarnings24Report { get; set; }
        [DataMember]
        public decimal CanadaPensionPlanEarn26Report { get; set; }
        [DataMember]
        public decimal ProvincialParentalInsurancePlanPremiums { get; set; }
        [DataMember]
        public decimal ProvincialParentalInsurancePlanEarnings { get; set; }
        [DataMember]
        public bool ProvincialParentalInsurancePlanExemption28Flag { get; set; }
        [DataMember]
        public short? RecordStatus { get; set; }
        [DataMember]
        public byte? GenerateXml { get; set; }
        [DataMember]
        public byte? XmlCreated { get; set; }
        [DataMember]
        public decimal HousingBoardLodging30 { get; set; }
        [DataMember]
        public decimal SpecialWorksite31 { get; set; }
        [DataMember]
        public decimal DesignatedAreaTravel32 { get; set; }
        [DataMember]
        public decimal MedicalTravel33 { get; set; }
        [DataMember]
        public decimal PersonalAutoUse34 { get; set; }
        [DataMember]
        public decimal InterestFreeLoan36 { get; set; }
        [DataMember]
        public decimal StockOptionBenefit38 { get; set; }
        [DataMember]
        public decimal OtherTaxAll40 { get; set; }
        [DataMember]
        public decimal CommissionIncome42 { get; set; }
        [DataMember]
        public decimal DeferredSecurity53 { get; set; }
        [DataMember]
        public decimal EligibleRetiringAllow { get; set; }
        [DataMember]
        public decimal NonEligibleRetiringAllow { get; set; }
        [DataMember]
        public decimal IndianNonEligibleRetiring { get; set; }
        [DataMember]
        public decimal StatusIndian71 { get; set; }
        [DataMember]
        public decimal WorkersCompensationBoardRepaid77 { get; set; }
        [DataMember]
        public decimal PrivateHealth85 { get; set; }
        [DataMember]
        public decimal SupplementaryTopupAmountWhileOnLeave89 { get; set; }
        [DataMember]
        public decimal? EmployeeHomeRelocationLoanDeduction37 { get; set; }
        [DataMember]
        public decimal? SecurityOptionsDeduction110_1_d_39 { get; set; }
        [DataMember]
        public decimal? SecurityOptionsDeduction_110_1_41 { get; set; }
        [DataMember]
        public int? T4BoxNumber1 { get; set; }
        [DataMember]
        public decimal? T4BoxAmount1 { get; set; }
        [DataMember]
        public int? T4BoxNumber2 { get; set; }
        [DataMember]
        public decimal? T4BoxAmount2 { get; set; }
        [DataMember]
        public int? T4BoxNumber3 { get; set; }
        [DataMember]
        public decimal? T4BoxAmount3 { get; set; }
        [DataMember]
        public int? T4BoxNumber4 { get; set; }
        [DataMember]
        public decimal? T4BoxAmount4 { get; set; }
        [DataMember]
        public int? T4BoxNumber5 { get; set; }
        [DataMember]
        public decimal? T4BoxAmount5 { get; set; }
        [DataMember]
        public int? T4BoxNumber6 { get; set; }
        [DataMember]
        public decimal? T4BoxAmount6 { get; set; }
        [DataMember]
        public bool ActiveFlag { get; set; }
        [DataMember]
        public int Revision { get; set; }
        [DataMember]
        public long? PreviousRevisionYearEndT4Id { get; set; }
        [DataMember]
        public string RegistrationNumber50 { get; set; }
        #endregion

        #region construct/destruct
        public YearEndT4()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(YearEndT4 data)
        {
            base.CopyTo(data);

            data.YearEndT4Id = YearEndT4Id;
            data.Year = Year;
            data.EmployeeId = EmployeeId;
            data.LastName = LastName;
            data.FirstName = FirstName;
            data.AddressLine1 = AddressLine1;
            data.AddressLine2 = AddressLine2;
            data.City = City;
            data.PostalZipCode = PostalZipCode;
            data.ProvinceStateCode = ProvinceStateCode;
            data.CountryCode = CountryCode;
            data.SocialInsuranceNumberSegment1 = SocialInsuranceNumberSegment1;
            data.SocialInsuranceNumberSegment2 = SocialInsuranceNumberSegment2;
            data.SocialInsuranceNumberSegment3 = SocialInsuranceNumberSegment3;
            data.EmployeeNumber = EmployeeNumber;
            data.EmployerNumber = EmployerNumber;
            data.TaxableCodeProvinceStateCd = TaxableCodeProvinceStateCd;
            data.T4Sequence = T4Sequence;
            data.EmploymentIncome14 = EmploymentIncome14;
            data.CanadaPensionPlanDeducted16 = CanadaPensionPlanDeducted16;
            data.QuebecPensionPlanDeducted17 = QuebecPensionPlanDeducted17;
            data.EmploymentInsuranceDeducted18 = EmploymentInsuranceDeducted18;
            data.RegisteredPension20 = RegisteredPension20;
            data.IncomeTaxDeducted22 = IncomeTaxDeducted22;
            data.EmploymentInsuranceEarnings24 = EmploymentInsuranceEarnings24;
            data.CanadaPensionPlanEarn26 = CanadaPensionPlanEarn26;
            data.UnionDues44 = UnionDues44;
            data.CharitableDonate46 = CharitableDonate46;
            data.OriginalRegistrationNumber50 = OriginalRegistrationNumber50;
            data.PensionAdjustment52 = PensionAdjustment52;
            data.EmployerCanadaPensionPlanAmount = EmployerCanadaPensionPlanAmount;
            data.EmployerEmploymentInsuranceAmount = EmployerEmploymentInsuranceAmount;
            data.EmploymentInsuranceExemption28Flag = EmploymentInsuranceExemption28Flag;
            data.CanadaPensionPlanExempt28Flag = CanadaPensionPlanExempt28Flag;
            data.EmploymentInsuranceEarnings24Report = EmploymentInsuranceEarnings24Report;
            data.CanadaPensionPlanEarn26Report = CanadaPensionPlanEarn26Report;
            data.ProvincialParentalInsurancePlanPremiums = ProvincialParentalInsurancePlanPremiums;
            data.ProvincialParentalInsurancePlanEarnings = ProvincialParentalInsurancePlanEarnings;
            data.ProvincialParentalInsurancePlanExemption28Flag = ProvincialParentalInsurancePlanExemption28Flag;
            data.RecordStatus = RecordStatus;
            data.GenerateXml = GenerateXml;
            data.XmlCreated = XmlCreated;
            data.HousingBoardLodging30 = HousingBoardLodging30;
            data.SpecialWorksite31 = SpecialWorksite31;
            data.DesignatedAreaTravel32 = DesignatedAreaTravel32;
            data.MedicalTravel33 = MedicalTravel33;
            data.PersonalAutoUse34 = PersonalAutoUse34;
            data.InterestFreeLoan36 = InterestFreeLoan36;
            data.StockOptionBenefit38 = StockOptionBenefit38;
            data.OtherTaxAll40 = OtherTaxAll40;
            data.CommissionIncome42 = CommissionIncome42;
            data.DeferredSecurity53 = DeferredSecurity53;
            data.EligibleRetiringAllow = EligibleRetiringAllow;
            data.NonEligibleRetiringAllow = NonEligibleRetiringAllow;
            data.IndianNonEligibleRetiring = IndianNonEligibleRetiring;
            data.StatusIndian71 = StatusIndian71;
            data.WorkersCompensationBoardRepaid77 = WorkersCompensationBoardRepaid77;
            data.PrivateHealth85 = PrivateHealth85;
            data.SupplementaryTopupAmountWhileOnLeave89 = SupplementaryTopupAmountWhileOnLeave89;
            data.EmployeeHomeRelocationLoanDeduction37 = EmployeeHomeRelocationLoanDeduction37;
            data.SecurityOptionsDeduction110_1_d_39 = SecurityOptionsDeduction110_1_d_39;
            data.SecurityOptionsDeduction_110_1_41 = SecurityOptionsDeduction_110_1_41;
            data.T4BoxNumber1 = T4BoxNumber1;
            data.T4BoxAmount1 = T4BoxAmount1;
            data.T4BoxNumber2 = T4BoxNumber2;
            data.T4BoxAmount2 = T4BoxAmount2;
            data.T4BoxNumber3 = T4BoxNumber3;
            data.T4BoxAmount3 = T4BoxAmount3;
            data.T4BoxNumber4 = T4BoxNumber4;
            data.T4BoxAmount4 = T4BoxAmount4;
            data.T4BoxNumber5 = T4BoxNumber5;
            data.T4BoxAmount5 = T4BoxAmount5;
            data.T4BoxNumber6 = T4BoxNumber6;
            data.T4BoxAmount6 = T4BoxAmount6;
            data.ActiveFlag = ActiveFlag;
            data.Revision = Revision;
            data.PreviousRevisionYearEndT4Id = PreviousRevisionYearEndT4Id;
            data.RegistrationNumber50 = RegistrationNumber50;
        }
        public new void Clear()
        {
            base.Clear();

            YearEndT4Id = -1;
            Year = 0;
            EmployeeId = -1;
            LastName = null;
            FirstName = null;
            AddressLine1 = null;
            AddressLine2 = null;
            City = null;
            PostalZipCode = null;
            ProvinceStateCode = null;
            CountryCode = null;
            SocialInsuranceNumberSegment1 = null;
            SocialInsuranceNumberSegment2 = null;
            SocialInsuranceNumberSegment3 = null;
            EmployeeNumber = null;
            EmployerNumber = null;
            TaxableCodeProvinceStateCd = null;
            T4Sequence = 1;
            EmploymentIncome14 = 0;
            CanadaPensionPlanDeducted16 = 0;
            QuebecPensionPlanDeducted17 = 0;
            EmploymentInsuranceDeducted18 = 0;
            RegisteredPension20 = 0;
            IncomeTaxDeducted22 = 0;
            EmploymentInsuranceEarnings24 = 0;
            CanadaPensionPlanEarn26 = 0;
            UnionDues44 = 0;
            CharitableDonate46 = 0;
            OriginalRegistrationNumber50 = null;
            PensionAdjustment52 = 0;
            EmployerCanadaPensionPlanAmount = 0;
            EmployerEmploymentInsuranceAmount = 0;
            EmploymentInsuranceExemption28Flag = false;
            CanadaPensionPlanExempt28Flag = false;
            EmploymentInsuranceEarnings24Report = 0;
            CanadaPensionPlanEarn26Report = 0;
            ProvincialParentalInsurancePlanPremiums = 0;
            ProvincialParentalInsurancePlanEarnings = 0;
            ProvincialParentalInsurancePlanExemption28Flag = false;
            RecordStatus = null;
            GenerateXml = null;
            XmlCreated = null;
            HousingBoardLodging30 = 0;
            SpecialWorksite31 = 0;
            DesignatedAreaTravel32 = 0;
            MedicalTravel33 = 0;
            PersonalAutoUse34 = 0;
            InterestFreeLoan36 = 0;
            StockOptionBenefit38 = 0;
            OtherTaxAll40 = 0;
            CommissionIncome42 = 0;
            DeferredSecurity53 = 0;
            EligibleRetiringAllow = 0;
            NonEligibleRetiringAllow = 0;
            IndianNonEligibleRetiring = 0;
            StatusIndian71 = 0;
            WorkersCompensationBoardRepaid77 = 0;
            PrivateHealth85 = 0;
            SupplementaryTopupAmountWhileOnLeave89 = 0;
            EmployeeHomeRelocationLoanDeduction37 = null;
            SecurityOptionsDeduction110_1_d_39 = null;
            SecurityOptionsDeduction_110_1_41 = null;
            T4BoxNumber1 = null;
            T4BoxAmount1 = null;
            T4BoxNumber2 = null;
            T4BoxAmount2 = null;
            T4BoxNumber3 = null;
            T4BoxAmount3 = null;
            T4BoxNumber4 = null;
            T4BoxAmount4 = null;
            T4BoxNumber5 = null;
            T4BoxAmount5 = null;
            T4BoxNumber6 = null;
            T4BoxAmount6 = null;
            ActiveFlag = true;
            Revision = 0;
            PreviousRevisionYearEndT4Id = null;
            RegistrationNumber50 = null;
        }
        #endregion
    }
}