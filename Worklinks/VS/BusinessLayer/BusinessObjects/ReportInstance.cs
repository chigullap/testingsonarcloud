﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public class ReportInstance  : WLP.BusinessLayer.BusinessObjects.BusinessObject, ICloneable
    {
         #region fields
        private ReportInstanceParameterCollection _parameters = null;
        #endregion

        #region properties
        [DataMember]
        public long ReportInstanceId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long ReportId { get; set; }
        [DataMember]
        public byte[] Data { get; set; }

        [DataMember]
        public ReportInstanceParameterCollection Parameters 
        {
            get
            {
                if (_parameters == null)
                    _parameters = new ReportInstanceParameterCollection();
                return _parameters;
            }
            set
            {
                _parameters = value;
            }
        }

        #endregion


        #region construct/destruct
        public ReportInstance()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone

        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(ReportInstance data)
        {
            base.CopyTo(data);
            data.ReportInstanceId = ReportInstanceId;
            data.ReportId = ReportId;
            data.Data = Data;
            Parameters.CopyTo(data.Parameters);
        }
        public new void Clear()
        {
            base.Clear();
            ReportInstanceId = -1;
            ReportId = -1;
            Data = null;
            Parameters = null;
        }

        #endregion
    }
}
