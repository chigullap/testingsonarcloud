﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ImportExportSummary : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long ImportExportId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String LanguageCode { get; set; }

        [DataMember]
        public String Label { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public bool CanConvertXlsxToCsv { get; set; }

        [DataMember]
        public bool RemoveByteOrderMark { get; set; }
        #endregion

        #region construct/destruct
        public ImportExportSummary()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public new void Clear()
        {
            base.Clear();

            ImportExportId = -1;
            LanguageCode = null;
            Label = null;
            Description = null;
            ActiveFlag = true;
            RowVersion = new byte[8];
            CanConvertXlsxToCsv = false;
            RemoveByteOrderMark = false;
        }
        public void CopyTo(ImportExportSummary data)
        {
            base.CopyTo(data);

            data.ImportExportId = ImportExportId;
            data.LanguageCode = LanguageCode;
            data.Label = Label;
            data.Description = Description;
            data.ActiveFlag = ActiveFlag;
            data.CanConvertXlsxToCsv = CanConvertXlsxToCsv;
            data.RemoveByteOrderMark = RemoveByteOrderMark;
        }
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}