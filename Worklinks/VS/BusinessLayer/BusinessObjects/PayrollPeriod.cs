﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollPeriod: WLP.BusinessLayer.BusinessObjects.BusinessObject, ICloneable
    {
        
        #region properties
        [DataMember]
        public long PayrollPeriodId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public Decimal PeriodYear { get; set; }
        [DataMember]
        public Decimal Period { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public DateTime CutoffDate { get; set; }
        [DataMember]
        public Decimal PPCurrentPayrollYear { get; set; }
        [DataMember]
        public String PayrollProcessGroupCode { get; set; }
        [DataMember]
        public DateTime? ProcessedDate { get; set; }
        [DataMember]
        public string CodePayrollProcessGroupCountryCd { get; set; }
        [DataMember]
        public string CodeCountryCd { get; set; }

        public String StartDatePeriodYear 
        {
            get
            { 
                return StartDate + " - " + PeriodYear.ToString(); 
            }
        }

        public List<DateTime> Day1TransactionDates
        {
            get
            {
                List<DateTime> list = new List<DateTime>();
                DateTime currentDate=StartDate;
                while (currentDate < CutoffDate)
                {
                    list.Add(currentDate);
                    currentDate=currentDate.AddDays(7);
                }
                return list;
            }
        }

        #endregion

        #region construct/destruct
        public PayrollPeriod()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public new void Clear()
        {
            base.Clear();
            PayrollPeriodId = -1;
            ProcessedDate = null;
            PeriodYear = -1;
            Period = -1;
            StartDate = DateTime.MinValue;
            CutoffDate = DateTime.MinValue;
            PPCurrentPayrollYear = 0;
            PayrollProcessGroupCode = null;
            CodePayrollProcessGroupCountryCd = null;
            CodeCountryCd = null;
        }

        public void CopyTo(PayrollPeriod data)
        {
            base.CopyTo(data);
            data.PayrollPeriodId = PayrollPeriodId;
            data.ProcessedDate = ProcessedDate;
            data.PeriodYear = PeriodYear;
            data.Period = Period;
            data.StartDate = StartDate;
            data.CutoffDate = CutoffDate;
            data.PPCurrentPayrollYear = PPCurrentPayrollYear;
            data.PayrollProcessGroupCode = PayrollProcessGroupCode;
            data.CodePayrollProcessGroupCountryCd = CodePayrollProcessGroupCountryCd;
            data.CodeCountryCd = CodeCountryCd;
        }

        #endregion

    }
}
