﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]

    public class FormDescription : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties

        [DataMember]
        public long FormId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long FormDescriptionId { get; set; }
        [DataMember]
        public String FormDescriptionField { get; set; }

        #endregion

        #region construct/destruct
        public FormDescription()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(FormDescription data)
        {
            base.CopyTo(data);

            data.FormDescriptionId = FormDescriptionId;
            data.FormId = FormId;
            data.FormDescriptionField = FormDescriptionField;
        }
        public new void Clear()
        {
            base.Clear();

            FormDescriptionId = -1;
            FormId = -1;
            FormDescriptionField = "";
        }

        #endregion
    }
}
