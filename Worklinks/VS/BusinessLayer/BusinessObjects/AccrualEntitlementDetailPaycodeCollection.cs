﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class AccrualEntitlementDetailPaycodeCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<AccrualEntitlementDetailPaycode>
    {
    }
}