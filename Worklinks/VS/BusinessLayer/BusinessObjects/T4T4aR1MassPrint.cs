﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class T4T4aR1MassPrint : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long T4T4aR1MassPrintId //this will be a dummy value as we wont be using it
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String EmployeeNumber { get; set; }

        [DataMember]
        public String Year { get; set; }
        [DataMember]
        public String EmployerNumber { get; set; }

        [DataMember]
        public String TaxableCodeProvinceStateCd { get; set; }

        [DataMember]
        public long EmployeeId { get; set; }
        #endregion

        #region construct/destruct
        public T4T4aR1MassPrint()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(T4T4aR1MassPrint data)
        {
            base.CopyTo(data);

            data.T4T4aR1MassPrintId = T4T4aR1MassPrintId;
            data.EmployeeNumber = EmployeeNumber;
            data.Year = Year;
            data.EmployerNumber = EmployerNumber;
            data.TaxableCodeProvinceStateCd = TaxableCodeProvinceStateCd;
            data.EmployeeId = EmployeeId;
        }
        public new void Clear()
        {
            base.Clear();

            T4T4aR1MassPrintId = -1;
            EmployeeNumber = null;
            Year = null;
            EmployerNumber = null;
            TaxableCodeProvinceStateCd = null;
            EmployeeId = -1;
        }
        #endregion
    }
}