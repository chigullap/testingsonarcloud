﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class REALCustomExportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<REALCustomExport>
    {
    }
}