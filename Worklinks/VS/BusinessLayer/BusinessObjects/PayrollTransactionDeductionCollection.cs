﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class PayrollTransactionDeductionCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollTransactionDeduction>
    {
    }
}