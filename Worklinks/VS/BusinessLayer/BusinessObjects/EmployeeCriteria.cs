﻿using System;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class EmployeeCriteria
    {
        #region properties
        public bool SecurityOverrideFlag { get; set; }
        public long? EmployeeId { get; set; }
        public String LanguageCode { get; set; }
        public String EmployeeNumber { get; set; }
        public String SocialInsuranceNumber { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String OrganizationUnitName { get; set; }
        public String PayrollProcessGroupCode { get; set; }
        public long? PayrollProcessId { get; set; }
        public String ImportExternalIdentifierNotUsed { get; set; }
        public bool IncludeContacts { get; set; }
        public bool IncludeContactTypes { get; set; }
        public String IncludeContactTypeCode { get; set; }
        public bool IncludePrimaryAddress { get; set; }
        public bool IncludeStatutoryDeduction { get; set; }
        public bool IncludeBanking { get; set; }
        public bool IncludeEmploymentInformation { get; set; }
        public bool IncludePayCode { get; set; }
        public bool IncludeCurrentPosition { get; set; }
        public bool IsWizardSearch { get; set; }
        public bool UseLikeStatementInProcSelect { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public String SortColumn { get; set; }
        public String SortDirection { get; set; }
        public bool IncludeEmployeePhotoFlag { get; set; }
        public bool IncludeEmployeeEmploymentEquityFlag { get; set; }
        public String Description { get; set; }
        public bool TemplateFlag { get; set; }
        public CodeCollection EmployeePositionStatusCode { get; set; }
        public String EmployeePositionStatusCodeWhereClause { get; set; }
        #endregion

        #region construct/destruct
        public EmployeeCriteria()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public void Clear()
        {
            SecurityOverrideFlag = false;
            EmployeeId = null;
            EmployeeNumber = null;
            FirstName = null;
            LastName = null;
            SocialInsuranceNumber = null;
            OrganizationUnitName = null;
            PayrollProcessGroupCode = null;
            IncludeContacts = false;
            IncludeContactTypes = false;
            IncludeContactTypeCode = null;
            IncludePrimaryAddress = false;
            IncludeStatutoryDeduction = false;
            IncludeBanking = false;
            IncludeEmploymentInformation = false;
            IncludePayCode = false;
            IncludeCurrentPosition = false;
            LanguageCode = null;
            IsWizardSearch = false;
            UseLikeStatementInProcSelect = true;
            PayrollProcessId = null;
            ImportExternalIdentifierNotUsed = null;
            PageIndex = 0;
            PageSize = 9999999;
            SortColumn = null;
            SortDirection = null;
            IncludeEmployeePhotoFlag = false;
            IncludeEmployeeEmploymentEquityFlag = false;
            Description = null;
            TemplateFlag = false;
            EmployeePositionStatusCode = null;
            EmployeePositionStatusCodeWhereClause = null;
        }
        #endregion
    }
}