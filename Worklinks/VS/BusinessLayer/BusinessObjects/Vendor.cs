﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class Vendor : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long VendorId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String Number { get; set; }

        [DataMember]
        public String Name { get; set; }

        [DataMember]
        public String GarnishmentTypeCode { get; set; }

        [DataMember]
        public Address Address { get; set; }

        [DataMember]
        public String PhoneNumber { get; set; }

        [DataMember]
        public String PhoneNumberExtension { get; set; }

        [DataMember]
        public String EmailAddress { get; set; }

        public long AddressId
        {
            get { return Address.AddressId; }
            set { Address.AddressId = value; }
        }

        public String AddressLine1
        {
            get { return Address.AddressLine1; }
            set { Address.AddressLine1 = value; }
        }

        public String AddressLine2
        {
            get { return Address.AddressLine2; }
            set { Address.AddressLine2 = value; }
        }

        public String City
        {
            get { return Address.City; }
            set { Address.City = value; }
        }

        public String PostalZipCode
        {
            get { return Address.PostalZipCode; }
            set { Address.PostalZipCode = value; }
        }

        public String ProvinceStateCode
        {
            get { return Address.ProvinceStateCode; }
            set { Address.ProvinceStateCode = value; }
        }

        public String CountryCode
        {
            get { return Address.CountryCode; }
            set { Address.CountryCode = value; }
        }

        public String MailingLabel
        {
            get
            {
                System.Text.StringBuilder label = new System.Text.StringBuilder(String.Format("{0}\r\n", AddressLine1));

                if (AddressLine2 != null)
                    label.Append(String.Format("{0}\r\n", AddressLine2));

                //if (AddressLine3 != null)
                //    label.Append(String.Format("{0}\r\n", AddressLine3));

                label.Append(String.Format("{0} {1} {2}", City, ProvinceStateCode, PostalZipCode));

                return label.ToString();
            }
        }
        #endregion

        #region construct/destruct
        public Vendor()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(Vendor data)
        {
            base.CopyTo(data);

            data.VendorId = VendorId;
            data.Number = Number;
            data.Name = Name;
            data.GarnishmentTypeCode = GarnishmentTypeCode;
            Address.CopyTo(data.Address);
            data.PhoneNumber = PhoneNumber;
            data.PhoneNumberExtension = PhoneNumberExtension;
            data.EmailAddress = EmailAddress;
        }

        public new void Clear()
        {
            base.Clear();

            VendorId = -1;
            Number = null;
            Name = null;
            GarnishmentTypeCode = null;
            Address = new Address();
        }
        #endregion
    }
}