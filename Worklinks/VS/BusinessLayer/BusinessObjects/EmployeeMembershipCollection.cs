﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeeMembershipCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeeMembership>
    {
    }
}