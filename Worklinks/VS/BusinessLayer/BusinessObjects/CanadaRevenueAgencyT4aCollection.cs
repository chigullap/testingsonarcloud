﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class CanadaRevenueAgencyT4aCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<CanadaRevenueAgencyT4a>
    {
        public CanadaRevenueAgencyT4aCollection()
        {
        }
        public CanadaRevenueAgencyT4aCollection(YearEndT4aCollection yearEndT4aCollection)
        {
            foreach (YearEndT4a t4 in yearEndT4aCollection)
            {
                CanadaRevenueAgencyT4a crt4a = new CanadaRevenueAgencyT4a();
                t4.CopyTo(crt4a);
                this.Add(crt4a);
            }
        }
    }
}
