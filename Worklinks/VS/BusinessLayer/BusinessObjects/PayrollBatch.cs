﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollBatch : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PayrollBatchId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String BatchSource { get; set; }

        [DataMember]
        public String PayrollProcessRunTypeCode { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public String PayrollBatchTypeCode { get; set; }

        [DataMember]
        public String PayrollBatchStatusCode { get; set; }

        [DataMember]
        public String PayrollProcessGroupCode { get; set; }

        [DataMember]
        public long? PayrollProcessId { get; set; }

        [DataMember]
        public bool SystemGeneratedFlag { get; set; }

        [DataMember]
        public PayrollTransactionSummaryCollection Transactions { get; set; }
        #endregion

        #region construct/destruct
        public PayrollBatch()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            PayrollBatch item = new PayrollBatch();
            this.CopyTo(item);

            return item;
        }
        public void CopyTo(PayrollBatch data)
        {
            base.CopyTo(data);

            data.PayrollBatchId = PayrollBatchId;
            data.PayrollBatchTypeCode = PayrollBatchTypeCode;
            data.BatchSource = BatchSource;
            data.PayrollProcessRunTypeCode = PayrollProcessRunTypeCode;
            data.Description = Description;
            data.PayrollBatchStatusCode = PayrollBatchStatusCode;
            data.PayrollProcessGroupCode = PayrollProcessGroupCode;
            data.PayrollProcessId = PayrollProcessId;
            data.SystemGeneratedFlag = SystemGeneratedFlag;

            if (Transactions == null)
                data.Transactions = Transactions;
            else
            {
                data.Transactions = new PayrollTransactionSummaryCollection();
                Transactions.CopyTo(data.Transactions);
            }
        }
        public new void Clear()
        {
            base.Clear();

            PayrollBatchId = -1;
            PayrollBatchTypeCode = null;
            BatchSource = "";
            PayrollProcessRunTypeCode = null;
            Description = "";
            PayrollBatchStatusCode = "";
            PayrollProcessGroupCode = String.Empty;
            PayrollProcessId = null;
            Transactions = null;
            SystemGeneratedFlag = false;
        }
        #endregion
    }
}