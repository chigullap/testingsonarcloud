﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class OrganizationUnitAssociation : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {

        //these represent the code for the English and French descriptions in the "Description" object, which is a "OrganizationUnitLevelDescriptionCollection"
        private static string _englishCd = "EN";
        private static string _frenchCd = "FR";
        private OrganizationUnitDescriptionCollection _descriptions;

        #region properties

        public long OrganizationUnitId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long OrganizationUnitAssociationId { get; set; }

        [DataMember]
        public String Description { get; set; }
        [DataMember]
        public long ParentOrganizationUnitId { get; set; }
        [DataMember]
        public long LevelId { get; set; }

        [DataMember]
        public String ParentDescription { get; set; }
        [DataMember]
        public bool Assigned { get; set; }

        [DataMember]
        public String LanguageCode { get; set; }
        [DataMember]
        public OrganizationUnitDescriptionCollection Descriptions
        {
            get
            {
                if (_descriptions == null)
                    _descriptions = new OrganizationUnitDescriptionCollection();
                return _descriptions;
            }
            set { _descriptions = value; }
        }
        [DataMember]
        public string EnglishDescription
        {
            get
            {
                return Descriptions[_englishCd] == null ? null : Descriptions[_englishCd].UnitDescription;
            }
            set
            {
                if (EnglishDescription == null)
                {
                    Descriptions.Add(new OrganizationUnitDescription() { LanguageCode = _englishCd, OrganizationUnitId = OrganizationUnitId, CreateUser = UpdateUser, UpdateUser = UpdateUser, UnitDescription = value });
                }
                else
                {
                    Descriptions[_englishCd].UpdateUser = UpdateUser;
                    Descriptions[_englishCd].UnitDescription = value;
                }
            }
        }
        [DataMember]
        public string FrenchDescription
        {
            get
            {
                return Descriptions[_frenchCd] == null ? null : Descriptions[_frenchCd].UnitDescription;
            }
            set
            {
                if (FrenchDescription == null)
                {
                    Descriptions.Add(new OrganizationUnitDescription() { LanguageCode = _frenchCd, OrganizationUnitId = OrganizationUnitId, CreateUser = UpdateUser, UpdateUser = UpdateUser, UnitDescription = value });
                }
                else
                {
                    Descriptions[_frenchCd].UpdateUser = UpdateUser;
                    Descriptions[_frenchCd].UnitDescription = value;
                }
            }
        }
        #endregion

        #region construct/destruct
        public OrganizationUnitAssociation()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            OrganizationUnitAssociation organizationUnitAssociation = new OrganizationUnitAssociation();
            this.CopyTo(organizationUnitAssociation);
            return organizationUnitAssociation;
        }
        public void CopyTo(OrganizationUnitAssociation data)
        {
            base.CopyTo(data);
            data.OrganizationUnitAssociationId = OrganizationUnitAssociationId;
            data.OrganizationUnitId = OrganizationUnitId;
            data.LanguageCode = LanguageCode;
            data.Description = Description;
            if (Descriptions != null)
            {
                data.Descriptions = new OrganizationUnitDescriptionCollection();
                Descriptions.CopyTo(data.Descriptions);
            }
        }
        public new void Clear()
        {
            base.Clear();
            OrganizationUnitAssociationId = -1;
            OrganizationUnitId = -1;
            LanguageCode = null;
            Description = null;
            Descriptions = null;
        }
        #endregion
    }
}
