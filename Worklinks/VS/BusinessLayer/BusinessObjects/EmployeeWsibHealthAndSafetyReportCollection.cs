﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeeWsibHealthAndSafetyReportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeeWsibHealthAndSafetyReport>
    {
    }
}