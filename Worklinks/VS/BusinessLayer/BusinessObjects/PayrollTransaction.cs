﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollTransaction : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region fields
        private long _payrollTransactionId = -1;
        #endregion

        #region properties
        public override String Key { get { return PayrollTransactionId.ToString(); } }

        [DataMember]
        public long PayrollTransactionId
        {
            get
            {
                return _payrollTransactionId;
            }
            set
            {
                WLP.BusinessLayer.BusinessObjects.KeyChangedEventArgs args = new WLP.BusinessLayer.BusinessObjects.KeyChangedEventArgs();
                args.OldKey = _payrollTransactionId.ToString();
                _payrollTransactionId = value;
                args.NewKey = Key;

                if (!(args.OldKey ?? String.Empty).Equals(args.NewKey ?? String.Empty) && this.GetType().Equals(typeof(PayrollTransaction)))
                    OnKeyChanged(args);
            }
        }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public long PayrollBatchId { get; set; }

        [DataMember]
        public DateTime TransactionDate { get; set; }

        [DataMember]
        public String PaycodeCode { get; set; }

        [DataMember]
        public Decimal Units { get; set; }

        [DataMember]
        public Decimal? Rate { get; set; }

        [DataMember]
        public Decimal EmploymentInsuranceHour { get; set; }

        [DataMember]
        public Decimal? OverrideBonusDeductionBeforeTaxPercent { get; set; }

        [DataMember]
        public String EntryTypeCode { get; set; }

        [DataMember]
        public Decimal? FederalTaxAmount { get; set; }

        [DataMember]
        public Decimal? ProvincialTaxAmount { get; set; }

        [DataMember]
        public Decimal? BaseSalaryRate { get; set; }

        [DataMember]
        public String ImportExternalIdentifier { get; set; }

        [DataMember]
        public int? RetroactiveNumberOfPayPeriods { get; set; }

        [DataMember]
        public String OrganizationUnit { get; set; }

        [DataMember]
        public TimeSpan? TransactionStartTime { get; set; }

        [DataMember]
        public TimeSpan? TransactionEndTime { get; set; }

        [DataMember]
        public long? RelatedPayrollTransactionId { get; set; }

        [DataMember]
        public bool OriginalImportFlag { get; set; }

        //regular property set from GUI, not stored in DB
        [DataMember]
        public bool OverrideEiHours { get; set; }
        #endregion

        #region construct/destruct
        public PayrollTransaction()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            PayrollTransaction data = new PayrollTransaction();
            CopyTo(data);

            return data;
        }
        public void CopyTo(PayrollTransaction data)
        {
            base.CopyTo(data);

            data.PayrollTransactionId = PayrollTransactionId;
            data.EmployeeId = EmployeeId;
            data.PayrollBatchId = PayrollBatchId;
            data.TransactionDate = TransactionDate;
            data.PaycodeCode = PaycodeCode;
            data.Units = Units;
            data.Rate = Rate;
            data.EmploymentInsuranceHour = EmploymentInsuranceHour;
            data.OverrideBonusDeductionBeforeTaxPercent = OverrideBonusDeductionBeforeTaxPercent;
            data.EntryTypeCode = EntryTypeCode;
            data.FederalTaxAmount = FederalTaxAmount;
            data.ProvincialTaxAmount = ProvincialTaxAmount;
            data.BaseSalaryRate = BaseSalaryRate;
            data.ImportExternalIdentifier = ImportExternalIdentifier;
            data.RetroactiveNumberOfPayPeriods = RetroactiveNumberOfPayPeriods;
            data.OrganizationUnit = OrganizationUnit;
            data.TransactionStartTime = TransactionStartTime;
            data.TransactionEndTime = TransactionEndTime;
            data.RelatedPayrollTransactionId = RelatedPayrollTransactionId;
            data.OriginalImportFlag = OriginalImportFlag;
            data.OverrideEiHours = OverrideEiHours;
        }
        public new void Clear()
        {
            base.Clear();

            PayrollTransactionId = -1;
            PayrollBatchId = -1;
            EmployeeId = EmployeeId;
            TransactionDate = DateTime.MinValue;
            PaycodeCode = null;
            Units = 0;
            Rate = 0;
            EmploymentInsuranceHour = 0;
            OverrideBonusDeductionBeforeTaxPercent = 0;
            EntryTypeCode = "NORM";
            FederalTaxAmount = 0;
            ProvincialTaxAmount = 0;
            BaseSalaryRate = null;
            ImportExternalIdentifier = null;
            RetroactiveNumberOfPayPeriods = null;
            OrganizationUnit = null;
            TransactionStartTime = null;
            TransactionEndTime = null;
            RelatedPayrollTransactionId = null;
            OriginalImportFlag = false;
            OverrideEiHours = false;
        }
        #endregion

        #region main
        public static Decimal CalculateEmploymentInsuranceHourAndUnits(Decimal units, Decimal standardHours, Decimal salaryHoursDeduction, bool includeEmploymentInsuranceHoursFlag, bool useSalaryStandardHoursFlag)
        {
            Decimal rtn = -1;

            if (includeEmploymentInsuranceHoursFlag)
            {
                if (useSalaryStandardHoursFlag)
                    rtn = standardHours - salaryHoursDeduction;
                else
                    rtn = units;
            }
            else
                rtn = 0;

            return rtn;
        }
        public void SetCalculateEmploymentInsuranceHourAndUnits(Decimal units, Decimal standardHours, Decimal salaryHoursDeduction, bool includeEmploymentInsuranceHoursFlag, bool useSalaryStandardHoursFlag, bool overrideEiHours = false)
        {
            Units = units;
            if (!overrideEiHours)
                EmploymentInsuranceHour = CalculateEmploymentInsuranceHourAndUnits(units, standardHours, salaryHoursDeduction, includeEmploymentInsuranceHoursFlag, useSalaryStandardHoursFlag);
        }

        public bool FederalTaxAmountContainsMoreThanTwoDecimalPlaces
        {
            get
            {
                if (FederalTaxAmount != null && (FederalTaxAmount * 100 != (int)(FederalTaxAmount * 100)))
                    return true;

                return false;
            }
        }
        public bool ProvincialTaxAmountContainsMoreThanTwoDecimalPlaces
        {
            get
            {
                if (ProvincialTaxAmount != null && (ProvincialTaxAmount * 100 != (int)(ProvincialTaxAmount * 100)))
                    return true;

                return false;
            }
        }

        public Decimal Amount
        {
            get
            {
                Decimal units = Units == 0 ? 1 : Units;
                return Math.Round((Rate ?? 0) * Units, 2, MidpointRounding.AwayFromZero);
            }
        }
        #endregion
    }
}