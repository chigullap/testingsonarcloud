﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CraWcbRemittanceSummary : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        public long BusinessNumberId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String WorkersCompensationAccountNumber { get; set; }
        [DataMember]
        public DateTime ChequeDate { get; set; }
        [DataMember]
        public Int64 EmployeeCount { get; set; }
        [DataMember]
        public decimal CurrentPremium { get; set; }
        [DataMember]
        public decimal CurrentAssessableEarnings { get; set; }
        [DataMember]
        public string CodeCraRemittancePeriodCd { get; set; }
        [DataMember]
        public DateTime? RemittancePeriodStartDate { get; set; }
        [DataMember]
        public DateTime? RemittancePeriodEndDate { get; set; }
        [DataMember]
        public DateTime? EffectiveEntryDate { get; set; }
        #endregion

        #region construct/destruct
        public CraWcbRemittanceSummary()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(CraWcbRemittanceSummary data)
        {
            base.CopyTo(data);

            data.BusinessNumberId = BusinessNumberId;
            data.WorkersCompensationAccountNumber = WorkersCompensationAccountNumber;
            data.ChequeDate = ChequeDate;
            data.EmployeeCount = EmployeeCount;
            data.CurrentPremium = CurrentPremium;
            data.CurrentAssessableEarnings = CurrentAssessableEarnings;
            data.CodeCraRemittancePeriodCd = CodeCraRemittancePeriodCd;
            data.RemittancePeriodStartDate = RemittancePeriodStartDate;
            data.RemittancePeriodEndDate = RemittancePeriodEndDate;
            data.EffectiveEntryDate = EffectiveEntryDate;
        }

        public new void Clear()
        {
            base.Clear();

            BusinessNumberId = -1;
            WorkersCompensationAccountNumber = null;
            ChequeDate = DateTime.Now;
            EmployeeCount = 0;
            CurrentPremium = 0;
            CurrentAssessableEarnings = 0;
            CodeCraRemittancePeriodCd = null;
            RemittancePeriodStartDate = null;
            RemittancePeriodEndDate = null;
            EffectiveEntryDate = null;
        }

        #endregion
    }
}
