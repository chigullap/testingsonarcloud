﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SalaryPlanGradeRule : BusinessObject
    {
        [DataMember]
        public long SalaryPlanGradeRuleId
        {
            get => (long)_Key;
            set => _Key = value;
        }
        [DataMember]
        public bool AutomaticAdvanceFlag { get; set; }
        [DataMember]
        public int MinimumHour { get; set; }
        [DataMember]
        public string CodeSalaryPlanAdvanceBasedOnCd { get; set; }

        #region construct/destruct
        public SalaryPlanGradeRule()
        {
            Clear();
        }
        #endregion

        #region Overrides of BusinessObject

        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(SalaryPlanGradeRule data)
        {
            base.CopyTo(data);

            data.SalaryPlanGradeRuleId = SalaryPlanGradeRuleId;
            data.AutomaticAdvanceFlag = AutomaticAdvanceFlag;
            data.MinimumHour = MinimumHour;
            data.CodeSalaryPlanAdvanceBasedOnCd = CodeSalaryPlanAdvanceBasedOnCd;
        }
        public new void Clear()
        {
            base.Clear();

            SalaryPlanGradeRuleId = -1;
            AutomaticAdvanceFlag = true;
            MinimumHour = 0;
            CodeSalaryPlanAdvanceBasedOnCd = null;
        }

        #endregion
    }
}
