﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollMasterDetail : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PayrollMasterDetailId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }


        [DataMember]
        public long PayrollMasterId { get; set; }

        [DataMember]
        public String OrganizationUnit { get; set; }


        [DataMember]
        public Decimal TaxableIncome { get; set; }

        [DataMember]
        public Decimal OverrideTaxIncome { get; set; }

        [DataMember]
        public Decimal QuebecTaxableIncome { get; set; }

        [DataMember]
        public Decimal BonusTaxableIncome { get; set; }

        [DataMember]
        public Decimal QuebecBonusTaxableIncome { get; set; }

        [DataMember]
        public Decimal LumpsumTaxableIncome { get; set; }

        [DataMember]
        public Decimal QuebecLumpsumTaxableIncome { get; set; }

        [DataMember]
        public Decimal PensionTaxableIncome { get; set; }

        [DataMember]
        public Decimal QuebecPensionTaxableIncome { get; set; }

        [DataMember]
        public Decimal TaxableBenefit { get; set; }

        [DataMember]
        public Decimal QuebecTaxableBenefit { get; set; }

        [DataMember]
        public Decimal TaxableCashBenefit { get; set; }

        [DataMember]
        public Decimal QuebecTaxableCashBenefit { get; set; }

        [DataMember]
        public Decimal TaxableNonCashBenefit { get; set; }

        [DataMember]
        public Decimal QuebecTaxableNonCashBenefit { get; set; }

        [DataMember]
        public Decimal SeveranceTaxableIncome { get; set; }

        [DataMember]
        public Decimal QuebecSeveranceTaxableIncome { get; set; }

        [DataMember]
        public Decimal CanadaPensionPlanEarnings { get; set; }

        [DataMember]
        public Decimal QuebecPensionPlanEarnings { get; set; }

        [DataMember]
        public Decimal QuebecParentalInsurancePlanEarnings { get; set; }

        [DataMember]
        public Decimal EmploymentInsuranceEarnings { get; set; }

        [DataMember]
        public Decimal CommissionTaxableIncome { get; set; }

        [DataMember]
        public Decimal QuebecCommissionTaxableIncome { get; set; }

        [DataMember]
        public Decimal IncomeOnlyDeductionBeforeTax { get; set; }

        [DataMember]
        public Decimal BonusOnlyDeductionBeforeTax { get; set; }

        [DataMember]
        public Decimal BonusIncomeBeforeTaxDeduction { get; set; }

        [DataMember]
        public Decimal WorkersCompensationEarnings { get; set; }

        [DataMember]
        public Decimal EmployerHealthTaxEarnings { get; set; }

        [DataMember]
        public PayrollMaster PayrollMaster { get; set; }
        #endregion

        public Decimal BeforeIncomeTaxDeduction
        {
            get
            {
                return ((this.PayrollMaster.BonusTaxableIncome + this.PayrollMaster.TaxableIncome == 0) ? BonusIncomeBeforeTaxDeduction : Math.Round(BonusIncomeBeforeTaxDeduction * this.PayrollMaster.TaxableIncome / (this.PayrollMaster.TaxableIncome + this.PayrollMaster.BonusTaxableIncome), 2, MidpointRounding.AwayFromZero)) + IncomeOnlyDeductionBeforeTax;
            }
        }
        public Decimal BeforeBonusTaxDeduction
        {
            get
            {
                return ((this.PayrollMaster.BonusTaxableIncome + this.PayrollMaster.TaxableIncome == 0) ? 0 : Math.Round(BonusIncomeBeforeTaxDeduction * this.PayrollMaster.BonusTaxableIncome / (this.PayrollMaster.TaxableIncome + this.PayrollMaster.BonusTaxableIncome), 2, MidpointRounding.AwayFromZero)) + BonusOnlyDeductionBeforeTax;
            }
        }

        #region clear/copy/clone
        public PayrollMasterDetail()
        {
            this.Clear();
        }
        public PayrollMasterDetail(long payrollMasterDetailId, long payrollMasterId, String organizationUnit, PayrollMaster payrollMaster)
        {
            this.Clear();
            this.PayrollMasterDetailId = payrollMasterDetailId;
            this.PayrollMasterId = payrollMasterId;
            this.OrganizationUnit = organizationUnit;
            this.PayrollMaster = payrollMaster;
        }
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PayrollMasterDetail data)
        {
            base.CopyTo(data);

            data.PayrollMasterDetailId = PayrollMasterDetailId;
            data.PayrollMasterId = PayrollMasterId;
            data.OrganizationUnit = OrganizationUnit;
            data.TaxableIncome = TaxableIncome;
            data.OverrideTaxIncome = OverrideTaxIncome;
            data.QuebecTaxableIncome = QuebecTaxableIncome;
            data.OverrideTaxIncome = OverrideTaxIncome;
            data.BonusTaxableIncome = BonusTaxableIncome;
            data.QuebecBonusTaxableIncome = QuebecBonusTaxableIncome;
            data.LumpsumTaxableIncome = LumpsumTaxableIncome;
            data.QuebecLumpsumTaxableIncome = QuebecLumpsumTaxableIncome;
            data.PensionTaxableIncome = PensionTaxableIncome;
            data.QuebecPensionTaxableIncome = QuebecPensionTaxableIncome;
            data.TaxableBenefit = TaxableBenefit;
            data.QuebecTaxableBenefit = QuebecTaxableBenefit;
            data.TaxableCashBenefit = TaxableCashBenefit;
            data.QuebecTaxableCashBenefit = QuebecTaxableCashBenefit;
            data.TaxableNonCashBenefit = TaxableNonCashBenefit;
            data.QuebecTaxableNonCashBenefit = QuebecTaxableNonCashBenefit;
            data.SeveranceTaxableIncome = SeveranceTaxableIncome;
            data.QuebecSeveranceTaxableIncome = QuebecSeveranceTaxableIncome;
            data.CanadaPensionPlanEarnings = CanadaPensionPlanEarnings;
            data.QuebecPensionPlanEarnings = QuebecPensionPlanEarnings;
            data.QuebecParentalInsurancePlanEarnings = QuebecParentalInsurancePlanEarnings;
            data.EmploymentInsuranceEarnings = EmploymentInsuranceEarnings;
            data.CommissionTaxableIncome = CommissionTaxableIncome;
            data.QuebecCommissionTaxableIncome = QuebecCommissionTaxableIncome;
            data.IncomeOnlyDeductionBeforeTax = IncomeOnlyDeductionBeforeTax;
            data.BonusOnlyDeductionBeforeTax = BonusOnlyDeductionBeforeTax;
            data.BonusIncomeBeforeTaxDeduction = BonusIncomeBeforeTaxDeduction;
            data.WorkersCompensationEarnings = WorkersCompensationEarnings;
            data.EmployerHealthTaxEarnings = EmployerHealthTaxEarnings;

            if (PayrollMaster != null)
            {
                data.PayrollMaster = new PayrollMaster();
                this.PayrollMaster.CopyTo(data.PayrollMaster);
            }

        }
        public new void Clear()
        {
            base.Clear();

            PayrollMasterDetailId = -1;
            PayrollMasterId = -1;
            OrganizationUnit = null;
            TaxableIncome = 0;
            QuebecTaxableIncome = 0;
            BonusTaxableIncome = 0;
            QuebecBonusTaxableIncome = 0;
            LumpsumTaxableIncome = 0;
            QuebecLumpsumTaxableIncome = 0;
            PensionTaxableIncome = 0;
            QuebecPensionTaxableIncome = 0;
            TaxableBenefit = 0;
            QuebecTaxableBenefit = 0;
            TaxableCashBenefit = 0;
            QuebecTaxableCashBenefit = 0;
            TaxableNonCashBenefit = 0;
            QuebecTaxableNonCashBenefit = 0;
            SeveranceTaxableIncome = 0;
            QuebecSeveranceTaxableIncome = 0;
            CommissionTaxableIncome = 0;
            QuebecCommissionTaxableIncome = 0;
            IncomeOnlyDeductionBeforeTax = 0;
            BonusOnlyDeductionBeforeTax = 0;
            BonusIncomeBeforeTaxDeduction = 0;
            EmployerHealthTaxEarnings = 0;

            CanadaPensionPlanEarnings = 0;
            QuebecPensionPlanEarnings = 0;
            QuebecParentalInsurancePlanEarnings = 0;
            EmploymentInsuranceEarnings = 0;
            WorkersCompensationEarnings = 0;

            PayrollMaster = null;
        }
        #endregion


        #region main

        #endregion
    }
}