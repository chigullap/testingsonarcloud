﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.CalcModel
{
    [Serializable]
    [CollectionDataContract]
    public class PayrollTransactionPayrollMasterCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollTransactionPayrollMaster>
    {
    }
}