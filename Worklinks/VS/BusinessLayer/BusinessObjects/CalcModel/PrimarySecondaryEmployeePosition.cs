﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.CalcModel
{
    [Serializable]
    [DataContract]
    public class PrimarySecondaryEmployeePosition : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        public long PrimarySecondaryEmployeePositionId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        public long EmployeeId { get; set; }
        public long EmployeePositionId { get; set; }
        public long? EmployeePositionSecondaryId { get; set; }
        public long? SalaryPlanGradeStepId { get; set; }
        public decimal? GradeHour { get; set; }
        public decimal? PlanHour { get; set; }
        public int? MinimumHour { get; set; }
        public string SalaryPlanAdvanceBasedOnCode { get; set; }
        public int? ProbationHour { get; set; }
        public long? AdvanceSalaryPlanGradeStepId { get; set; }
        public decimal? AdvanceAmount { get; set; }
        #endregion

        #region construct/destruct
        public PrimarySecondaryEmployeePosition()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException("PrimarySecondaryEmployeePosition:Clone");
        }
        public void CopyTo(PrimarySecondaryEmployeePosition data)
        {
            base.CopyTo(data);

            data.PrimarySecondaryEmployeePositionId = PrimarySecondaryEmployeePositionId;
            data.EmployeeId = EmployeeId;
            data.EmployeePositionId = EmployeePositionId;
            data.EmployeePositionSecondaryId = EmployeePositionSecondaryId;
            data.SalaryPlanGradeStepId = SalaryPlanGradeStepId;
            data.GradeHour = GradeHour;
            data.PlanHour = PlanHour;
            data.MinimumHour = MinimumHour;
            data.SalaryPlanAdvanceBasedOnCode = SalaryPlanAdvanceBasedOnCode;
            data.ProbationHour = ProbationHour;
            data.AdvanceSalaryPlanGradeStepId = AdvanceSalaryPlanGradeStepId;
            data.AdvanceAmount = AdvanceAmount;
        }
        public new void Clear()
        {
            base.Clear();

            PrimarySecondaryEmployeePositionId = -1;
            EmployeeId = -1;
            EmployeePositionId = -1;
            EmployeePositionSecondaryId = null;
            SalaryPlanGradeStepId = null;
            GradeHour = null;
            PlanHour = null;
            MinimumHour = null;
            SalaryPlanAdvanceBasedOnCode = null;
            ProbationHour = null;
            AdvanceSalaryPlanGradeStepId = null;
            AdvanceAmount = null;
        }
        #endregion
    }
}