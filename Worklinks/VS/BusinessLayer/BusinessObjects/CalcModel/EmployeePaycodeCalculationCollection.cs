﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Runtime.Serialization;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;

namespace WorkLinks.BusinessLayer.BusinessObjects.CalcModel
{
    public class EmployeePaycodeCalculationCollection
    {
        public EmployeePaycodeCalculationCollection(bool newArrearsProcessingFlag)
        {
            _newArrearsProcessingFlag = newArrearsProcessingFlag;
        }

        private Dictionary<long, Dictionary<String, EmployeePaycodeCalculation>> Lookup = new Dictionary<long, Dictionary<String, EmployeePaycodeCalculation>>();
        private List<EmployeePaycodeCalculation> Items = new List<EmployeePaycodeCalculation>();
        private Dictionary<String, PaycodeAttachedPaycodeProvisionCollection> _provisions = null;
        private bool _newArrearsProcessingFlag;


        public void LoadProvisions(PaycodeAttachedPaycodeProvisionCollection provisions)
        {
            _provisions = new Dictionary<string, PaycodeAttachedPaycodeProvisionCollection>();

            foreach (PaycodeAttachedPaycodeProvision provision in provisions)
            {
                PaycodeAttachedPaycodeProvisionCollection currentCollection = null;
                if (!_provisions.TryGetValue(provision.PaycodeCode, out currentCollection))
                {
                    currentCollection = new PaycodeAttachedPaycodeProvisionCollection();
                    _provisions.Add(provision.PaycodeCode, currentCollection);
                }
                currentCollection.Add(provision);
            }
        }
        public void LoadTransactions(PayrollTransactionEmployeePaycodeCollection transactions, bool updateTotals, PayrollTransactionEmployeePaycodeCollection previousTransactions)
        {
            if (_provisions == null)
                throw new Exception("Load provisions before transactions");
            
            Dictionary<long, PayrollTransactionEmployeePaycodeCollection> transactionLookup = new Dictionary<long, PayrollTransactionEmployeePaycodeCollection>();

            foreach (PayrollTransactionEmployeePaycode transaction in transactions)
            {
                PayrollTransactionEmployeePaycodeCollection currentCollection = null;
                if (!transactionLookup.TryGetValue(transaction.EmployeeId, out currentCollection))
                {
                    currentCollection = new PayrollTransactionEmployeePaycodeCollection();
                    transactionLookup.Add(transaction.EmployeeId, currentCollection);
                }
                currentCollection.Add(transaction);
            }

            foreach (EmployeePaycodeCalculation item in Items)
            {
                PayrollTransactionEmployeePaycodeCollection currentCollection = null;
                if (transactionLookup.TryGetValue(item.EmployeeId, out currentCollection))
                {
                    PaycodeAttachedPaycodeProvisionCollection provisions = null;
                    _provisions.TryGetValue(item.PaycodeCode, out provisions);

                    item.LoadTransactions(currentCollection, provisions);
                }
            }

            if (updateTotals)
            {
                foreach (EmployeePaycodeCalculation item in Items)
                {
                    item.PaycodeRelatedTransactions = previousTransactions.GetTransactionByPaycodeAndEmployeeId(item.PaycodeCode, item.EmployeeId);
                    item.PreviousEmployeeTransactionsHasIncome = previousTransactions.HasIncome;
                }
            }

        }


        public void Add(
            bool newArrearsProcessingFlag,
            long employeeId,
            long employeePaycodeId,
            String paycode,
            String payrollProcessRunTypeCode,
            String paycodeActivationFrequencyCode,
            int payrollCountThisMonth,
            Decimal? requiredMinimumIncomeAmount,
            Decimal? yearlyMaximumAmount,
            Decimal? periodMaximumAmount,
            bool? beforeBonusTaxDeductionFlag,
            bool? beforeIncomeTaxDeductionFlag,
            String paycodeTypeCode,
            String paycodeIncomePayTypeCode,
            String paycodeRateBasedOnCode,
            bool recurringIncomeCodeFlag,
            String organizationUnit,
            Decimal? ytdIncomeAmount,
            Decimal? ytdPaycodeAmount,
            Decimal? periodToDateAmount,
//            String incomeOrganizationUnit,
//            Decimal? incomeAmount,
            Decimal? amountPercentage,
            Decimal? amountRate,
            Decimal? amountUnits,
            bool includeInArrearsFlag,
            String arrearsOffsetPaycodeCode
     //       String PaycodeIncomePayTypeCode
            )
        {
            //get employee in index
            EmployeePaycodeCalculation calc = GetAddEmployeePaycodeCalculation(employeeId, paycode);

            if (calc.PaycodeCode == null)
            {
                //load parent;
                calc.newArrearsProcessingFlag = newArrearsProcessingFlag;
                calc.EmployeeId = employeeId;
                calc.PaycodeCode = paycode;
                calc.PayrollProcessRunTypeCode = payrollProcessRunTypeCode;
                calc.PaycodeActivationFrequencyCode = paycodeActivationFrequencyCode;
                calc.PayrollCountThisMonth = payrollCountThisMonth;
                calc.RequiredMinimumIncomeAmount = requiredMinimumIncomeAmount ?? 0;
                calc.YearlyMaximumAmount = yearlyMaximumAmount ?? Decimal.MaxValue;
                calc.PeriodMaximumAmount = periodMaximumAmount ?? Decimal.MaxValue;
                calc.BeforeBonusTaxDeductionFlag = beforeBonusTaxDeductionFlag??false;
                calc.BeforeIncomeTaxDeductionFlag = beforeIncomeTaxDeductionFlag??false;
                calc.PaycodeTypeCode = paycodeTypeCode;
                calc.PaycodeIncomePayTypeCode = paycodeIncomePayTypeCode;
                calc.PaycodeRateBasedOnCode = paycodeRateBasedOnCode;
                calc.RecurringIncomeCodeFlag = recurringIncomeCodeFlag;
                calc.OrganizationUnit = organizationUnit;
                calc.YtdIncomeAmount = ytdIncomeAmount ?? 0;
                calc.YtdPaycodeAmount = ytdPaycodeAmount ?? 0;
                calc.PeriodToDateAmount = periodToDateAmount ?? 0;

                calc.AmountPercentage = amountPercentage??0;
                calc.AmountRate = amountRate ?? 0;
                calc.AmountUnits = amountUnits ?? 0;
                calc.ArrearsOffsetPaycodeCode = arrearsOffsetPaycodeCode;
                calc.IncludeInArrearsFlag = includeInArrearsFlag;
                calc.EmployeePaycodes = Lookup[employeeId];

                Items.Add(calc);
            }

        }

        public PayrollTransactionEmployeePaycodeCollection GetTransactions(ref int id, long payrollBatchId, DateTime cutoffDate, bool percentCalcOnly)
        {
            PayrollTransactionEmployeePaycodeCollection collection = new PayrollTransactionEmployeePaycodeCollection();

            foreach (EmployeePaycodeCalculation calc in Items)
            {
                collection.Add(calc.GetTransactions(ref id, payrollBatchId, cutoffDate, percentCalcOnly));
            }

             return collection;
        }

        private EmployeePaycodeCalculation GetAddEmployeePaycodeCalculation(long employeeId, String paycode)
        {
            //get employee
            Dictionary<String, EmployeePaycodeCalculation> employee = GetAddEmployeeFromIndex(employeeId);

            EmployeePaycodeCalculation rtn = null;

            if (!employee.TryGetValue(paycode,out rtn))
            {
                rtn = new EmployeePaycodeCalculation();
                employee.Add(paycode, rtn);
            }

            return rtn;
        }

        private Dictionary<String, EmployeePaycodeCalculation> GetAddEmployeeFromIndex(long employeeId)
        {
            Dictionary<String, EmployeePaycodeCalculation> employee = null;
            if (Lookup.TryGetValue(employeeId, out employee))
            {
                return employee;
            }
            Lookup.Add(employeeId, new Dictionary<string, EmployeePaycodeCalculation>());

            return GetAddEmployeeFromIndex(employeeId);
        }



    }



}
