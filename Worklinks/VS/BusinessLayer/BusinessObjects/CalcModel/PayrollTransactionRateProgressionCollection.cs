﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.CalcModel
{
    [Serializable]
    [CollectionDataContract]
    public class PayrollTransactionRateProgressionCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollTransactionRateProgression>
    {
        public PayrollTransactionCollection GetRateAdjustedTransactions(long payrollBatchId)
        {
            long id = -1;
            PayrollTransactionCollection collection = new PayrollTransactionCollection();

            foreach (PayrollTransactionRateProgression rate in this)
            {
                collection.Add(rate.GetRateAdjustedTransactions(payrollBatchId, ref id));
                //collection.Add(rate.GetBonusTransactions(payrollBatchId, ref id));
            }

            return collection;
        }


        public SalaryPlanGradeAccumulationCollection GetCurrentAccumulations()
        {
            long id = -1;
            Dictionary<String, SalaryPlanGradeAccumulation> collection = new Dictionary<String, SalaryPlanGradeAccumulation>();
            SalaryPlanGradeAccumulationCollection rtn = new SalaryPlanGradeAccumulationCollection();

            foreach (PayrollTransactionRateProgression item in this)
            {
                SalaryPlanGradeAccumulation currentItem = item.SalaryPlanGradeAccumulation;
                if (currentItem != null)
                {
                    SalaryPlanGradeAccumulation rtnItem = null;
                    if (collection.TryGetValue(currentItem.KeyMask, out rtnItem))
                    {
                        rtnItem.Hours += currentItem.Hours;
                        rtnItem.Amount += currentItem.Amount;
                    }
                    else
                    {
                        rtnItem = new SalaryPlanGradeAccumulation();
                        currentItem.CopyTo(rtnItem);
                        rtnItem.SalaryPlanGradeAccumulationId = id--;
                        collection.Add(currentItem.KeyMask, rtnItem);
                        rtn.Add(rtnItem);
                    }
                }

            }

            return rtn;
        }


    }
}