﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.CalcModel
{
    [Serializable]
    [CollectionDataContract]
    public class PrimarySecondaryEmployeePositionCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PrimarySecondaryEmployeePosition>
    {
    }
}