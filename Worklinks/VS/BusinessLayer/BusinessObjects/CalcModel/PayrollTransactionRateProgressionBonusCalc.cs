﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects.CalcModel
{
    public class PayrollTransactionRateProgressionBonusCalc
    {

        public static PayrollTransaction GetBonusPayrollTransaction(
            BonusRule rule, PayrollTransactionRateProgressionBonus transaction, long payrollBatchId, ref long id)
        {
            if (rule != null)
            {
                Decimal totalPlanHour = (transaction.PreviousTotalPlanHour ?? 0) + transaction.CurrentPeriodPlanHour??0;
                Decimal startingHour = totalPlanHour - transaction.Units;

                if (startingHour >= rule.MinimumHour && totalPlanHour < rule.MaximumHour) //fully within range of rule
                {
                    return PayrollTransactionRateProgressionBonusCalc.GetPayrollTransaction(id--, payrollBatchId, transaction.TransactionDate, transaction.EmployeeId, rule.Amount, transaction.AmountRateFactor ,transaction.Units, transaction.OutputPaycode, transaction.PayrollTransactionId);
                }
                else if (startingHour < rule.MinimumHour && totalPlanHour > rule.MinimumHour) //intersecting min rule
                {
                    return PayrollTransactionRateProgressionBonusCalc.GetPayrollTransaction(id--, payrollBatchId, transaction.TransactionDate, transaction.EmployeeId, rule.Amount, transaction.AmountRateFactor,(totalPlanHour - rule.MinimumHour), transaction.OutputPaycode, transaction.PayrollTransactionId);
                }
                else if (startingHour < rule.MaximumHour && totalPlanHour > rule.MaximumHour) //intersecting max rule
                {
                    return PayrollTransactionRateProgressionBonusCalc.GetPayrollTransaction(id--, payrollBatchId, transaction.TransactionDate, transaction.EmployeeId, rule.Amount, transaction.AmountRateFactor,(rule.MaximumHour - startingHour), transaction.OutputPaycode, transaction.PayrollTransactionId);
                }
            }

            return null;
        }

        private static PayrollTransaction GetPayrollTransaction(long id, long payrollBatchId, DateTime transactionDate,long employeeId, Decimal rate, Decimal amountRateFactor, Decimal hour, String paycode, long relatedPayrollTransactionId)
        {
            return new PayrollTransaction()
            {
                PayrollTransactionId = id,
                PayrollBatchId = payrollBatchId,
                TransactionDate=transactionDate,
                EmployeeId = employeeId,
                PaycodeCode = paycode,
                Units = hour,
                EmploymentInsuranceHour = hour,
                Rate = Math.Round(rate* amountRateFactor,4, MidpointRounding.AwayFromZero),
                RelatedPayrollTransactionId = relatedPayrollTransactionId,
            };
        }
    }
}
