﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects.CalcModel
{
    public class PayrollTransactionRateProgression : PayrollTransaction
    {

        #region fields
        public Decimal _probationHour;
        #endregion

        #region properties
        public long? SalaryPlanId { get; set; }
        public long? SalaryPlanGradeId { get; set; }
        public long? SalaryPlanGradeStepId { get; set; }
        public Decimal? CurrentStepAmount { get; set; }
        public Decimal? CurrentPeriodPlanHour { get; set; }
        public Decimal? CurrentPeriodGradeHour { get; set; }
        public Decimal? PreviousTotalPlanHour { get; set; }
        public Decimal? PreviousTotalGradeHour { get; set; }
        public int? AdvancementToMinimumHour { get; set; }
        public String SalaryPlanAdvanceBasedOnCode { get; set; }
        public Decimal? AdvanceToAmount { get; set; }
        public Decimal? ProbationHour { get { return _probationHour; } set { _probationHour = value ?? 0; } }
        public long? AdvanceSalaryPlanGradeStepId { get; set; }
        public Decimal AmountRateFactor { get; set; }
        public long PayrollProcessId { get; set; }
        public bool AutoPopulateRateFlag { get; set; }
        #endregion

        #region constructor/destructor
        public PayrollTransactionRateProgression()
        {

        }

        //public PayrollTransactionRateProgression(BonusRuleCollection rule)
        //{
        //    this.BonusRules = rule;
        //}
        #endregion

        #region calcs
        public Decimal TotalPlanHour { get { return (CurrentPeriodPlanHour ?? 0) + (PreviousTotalPlanHour ?? 0); } }
        public Decimal CurrentStartingPlanHour { get { return (TotalPlanHour-Units); } }

        public Decimal TotalGradeHour { get { return (CurrentPeriodGradeHour ?? 0) + (PreviousTotalGradeHour ?? 0); } }

        private bool IsPlanAdvancement { get { return SalaryPlanAdvanceBasedOnCode == "SP"; } }
        private bool IsGradeAdvancement { get { return SalaryPlanAdvanceBasedOnCode == "GR"; } }
        private Decimal HourThreshold { get { return (((Decimal)ProbationHour) > (AdvancementToMinimumHour ?? Decimal.MaxValue)) ? (Decimal)ProbationHour : AdvancementToMinimumHour ?? Decimal.MaxValue; } }
        public bool AdvancementFlag
        {
            get
            {
                if (SalaryPlanId == null || AdvancementToMinimumHour == null || SalaryPlanAdvanceBasedOnCode == null || AdvanceSalaryPlanGradeStepId == null)
                    return false;
                else if (ProbationHour > TotalPlanHour) //probation hour total not passed
                    return false;
                else if (IsPlanAdvancement && TotalPlanHour > HourThreshold) //advance based on salary plan
                    return true;
                else if (IsGradeAdvancement && TotalGradeHour > HourThreshold) //advance based on salary plan
                    return true;
                return false;
            }
        }



        public PayrollTransactionCollection GetRateAdjustedTransactions(long payrollBatchId, ref long id)
        {
            PayrollTransactionCollection rtn = new PayrollTransactionCollection();

            if (AdvancementFlag && AutoPopulateRateFlag)
            {
                //add reverse
                rtn.Add(GetReverseTransaction(payrollBatchId, ref id));

                //place back advanced hours
                Decimal hourDelta = IsGradeAdvancement ? (TotalGradeHour - HourThreshold) : (TotalPlanHour - HourThreshold);
                Decimal advancementHour = hourDelta > Units ? Units : hourDelta;
                PayrollTransaction advancementTransaction = (PayrollTransaction)this.Clone();
                advancementTransaction.PayrollTransactionId = id--;
                advancementTransaction.PayrollBatchId = payrollBatchId;
                advancementTransaction.Units = advancementHour;
                advancementTransaction.EmploymentInsuranceHour = advancementHour;
                advancementTransaction.Rate = this.AdvanceToAmount * this.AmountRateFactor;
                advancementTransaction.RelatedPayrollTransactionId = this.PayrollTransactionId;
                rtn.Add(advancementTransaction);

                //place back non-advancement hours if needed
                if (advancementHour != Units)
                {
                    PayrollTransaction nonAdvancementTransaction = (PayrollTransaction)this.Clone();
                    nonAdvancementTransaction.PayrollTransactionId = id--;
                    nonAdvancementTransaction.PayrollBatchId = payrollBatchId;
                    nonAdvancementTransaction.Units = this.Units - advancementHour;
                    nonAdvancementTransaction.EmploymentInsuranceHour = nonAdvancementTransaction.Units;
                    nonAdvancementTransaction.RelatedPayrollTransactionId = this.PayrollTransactionId;
                    rtn.Add(nonAdvancementTransaction);
                }


            }
            return rtn;
        }

        public SalaryPlanGradeAccumulation SalaryPlanGradeAccumulation
        {
            get
            {

                SalaryPlanGradeAccumulation rtn = null;
                if (SalaryPlanId != null)
                {
                    rtn = new SalaryPlanGradeAccumulation();
                    rtn.PayrollProcessId = PayrollProcessId;
                    rtn.EmployeeId = EmployeeId;
                    rtn.SalaryPlanGradeId = (long)SalaryPlanGradeId;
                    rtn.Hours = EmploymentInsuranceHour;
                    rtn.Amount = 0; //add later
                }

                return rtn;
            }
        }

        private PayrollTransaction GetReverseTransaction(long payrollBatchId, ref long id)
        {
            PayrollTransaction reverseTransaction = (PayrollTransaction)this.Clone();
            this.CopyTo(reverseTransaction);

            reverseTransaction.RelatedPayrollTransactionId = this.RelatedPayrollTransactionId;
            reverseTransaction.PayrollTransactionId = id--;
            reverseTransaction.Units = -reverseTransaction.Units;
            reverseTransaction.EmploymentInsuranceHour = -reverseTransaction.EmploymentInsuranceHour;
            reverseTransaction.PayrollBatchId = payrollBatchId;

            return reverseTransaction;
        }
        #endregion


    }
}

