﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.CalcModel
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeeRateCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeeRate>
    {

    }
}