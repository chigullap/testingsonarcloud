﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.CalcModel
{
    [Serializable]
    [CollectionDataContract]
    public class BonusRuleCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<BonusRule>
    {
        public BonusRule GetBonusRule(Decimal amount, long? salaryPlanId)
        {
            foreach (BonusRule rule in this)
            {
                if (rule.MinimumHour<=amount && amount < rule.MaximumHour && rule.SalaryPlanId==salaryPlanId)
                {
                    return rule;
                }
            }

            return null;
        }
    }
}