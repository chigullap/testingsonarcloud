﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects.CalcModel
{
    public class BonusRule : BusinessObject
    {
        public long MinimumHour
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        public long SalaryPlanId { get; set; }
        public int MaximumHour { get; set; }
        public Decimal Amount { get; set; }
//        public String OutputPaycode { get; set; }

        public override object Clone()
        {
            throw new NotImplementedException();
        }
    }
}
