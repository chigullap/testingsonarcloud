﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.CalcModel
{



    [Serializable]
    [CollectionDataContract]
    public class PayrollTransactionEmployeePaycodeCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollTransactionEmployeePaycode>
    {
        public class OrganizationUnitPercent
        {
            public String OrganizationUnit { get; set; }
            public Decimal Percent { get; set; }
            public Decimal Amount { get; set; }
        }

        private PayrollTransactionEmployeePaycode GetAddTranaction(ref int id, long payrollBatchId, long employeeId, String paycode, DateTime cutoffDate, Decimal rate, String organizationUnit)
        {
            PayrollTransactionEmployeePaycode transaction = new PayrollTransactionEmployeePaycode();
            transaction.PayrollTransactionId = id--;
            transaction.Units = 0;
            transaction.PayrollBatchId = payrollBatchId;
            transaction.EmployeeId = employeeId;
            transaction.TransactionDate = cutoffDate;
            transaction.PaycodeCode = paycode;
            transaction.EntryTypeCode = "NORM";
            transaction.Units = 1;
            transaction.Rate = rate;
            transaction.OrganizationUnit = organizationUnit;

            return transaction;
        }

        /// <summary>
        /// temporary function to handle tax override
        /// </summary>
        /// <param name="id"></param>
        /// <param name="payrollBatchId"></param>
        /// <param name="cutoffDate"></param>
        /// <returns></returns>
        public PayrollTransactionEmployeePaycodeCollection GetTaxTransactions(ref int id, long payrollBatchId, DateTime cutoffDate)
        {
            PayrollTransactionEmployeePaycodeCollection rtn = new PayrollTransactionEmployeePaycodeCollection();
            foreach (PayrollTransactionEmployeePaycode transaction in this)
            {

                if ((transaction.FederalTaxAmount ?? 0) != 0)
                {
                    rtn.Add(GetAddTranaction(ref id, payrollBatchId, transaction.EmployeeId, "TAX", cutoffDate, (Decimal)transaction.FederalTaxAmount, transaction.OrganizationUnit));
                }
                if ((transaction.ProvincialTaxAmount ?? 0) != 0)
                {
                    rtn.Add(GetAddTranaction(ref id, payrollBatchId, transaction.EmployeeId, "TAXQC", cutoffDate, (Decimal)transaction.ProvincialTaxAmount, transaction.OrganizationUnit));
                }
            }

            return rtn;
        }

        public OrganizationUnitPercent[] GetOrganizationUnitBreakdown(String organizationUnit)
        {
            Dictionary<String, OrganizationUnitPercent> breakdown = new Dictionary<string, OrganizationUnitPercent>();
            List<OrganizationUnitPercent> rtn = new List<OrganizationUnitPercent>();

            foreach (PayrollTransactionEmployeePaycode tran in this)
            {
                if (tran.PaycodeTypeCode == "1" && tran.IsProvisionPaycode && tran.Amount != 0)
                {
                    OrganizationUnitPercent current = null;
                    if (breakdown.TryGetValue(tran.OrganizationUnit ?? organizationUnit, out current))
                    {
                        current.Amount += tran.Amount;
                    }
                    else
                    {
                        current = new OrganizationUnitPercent() { OrganizationUnit = tran.OrganizationUnit ?? organizationUnit, Amount = tran.Amount };
                        breakdown.Add(current.OrganizationUnit, current);
                        rtn.Add(current);
                    }
                }
            }
            //get total
            Decimal totalProvisionIncomeAmount = this.TotalProvisionIncomeAmount;

            if (totalProvisionIncomeAmount != 0)
            {
                foreach (OrganizationUnitPercent item in rtn)
                {
                    item.Percent = item.Amount / totalProvisionIncomeAmount;
                }
            }

            return rtn.ToArray();
        }

        public Decimal TotalProvisionIncomeAmount
        {
            get
            {
                Decimal rtn = 0;
                foreach (PayrollTransactionEmployeePaycode item in this)
                {
                    if (item.PaycodeTypeCode == "1" && item.IsProvisionPaycode)
                    {
                        rtn += item.Amount;
                    }
                }

                return rtn;
            }
        }

        public Decimal TotalProvisionEmploymentInsuranceHour
        {
            get
            {
                Decimal rtn = 0;
                foreach (PayrollTransactionEmployeePaycode item in this)
                {
                    if (item.PaycodeTypeCode == "1" && item.IsProvisionPaycode)
                    {
                        rtn += item.EmploymentInsuranceHour;
                    }
                }

                return rtn;
            }
        }



        public Decimal TotalProvisionBonusIncomeAmount
        {
            get
            {
                Decimal rtn = 0;
                foreach (PayrollTransactionEmployeePaycode item in this)
                {
                    if (item.PaycodeTypeCode == "1" && item.PaycodeIncomePayTypeCode.ToLower().Equals("bonus") && item.IsProvisionPaycode)
                    {
                        rtn += item.Amount;
                    }
                }

                return rtn;
            }
        }

        public Decimal TotalAmount
        {
            get
            {
                Decimal rtn = 0;
                foreach (PayrollTransactionEmployeePaycode item in this)
                {
                    
                    rtn += item.Amount;
                }

                return rtn;
            }
        }

        public bool HasIncome
        {
            get
            {
                foreach (PayrollTransactionEmployeePaycode item in this)
                {
                    if (item.PaycodeTypeCode == "1" && item.Amount != 0)
                        if (this.GetTransactionByPaycode(item.PaycodeCode).TotalAmount!=0)//check total for same paycode (could be negated)
                            return true;
                }

                return false;
            }
        }


        public PayrollTransactionEmployeePaycodeCollection GetTransactionByPaycodeAndEmployeeId(String paycode, long employeeId)
        {
            PayrollTransactionEmployeePaycodeCollection rtn = new PayrollTransactionEmployeePaycodeCollection();
            foreach (PayrollTransactionEmployeePaycode item in this)
            {
                if (item.PaycodeCode == paycode && item.EmployeeId==employeeId)
                {
                    rtn.Add(item);
                }
            }

            return rtn;
        }

        public PayrollTransactionEmployeePaycodeCollection GetTransactionByPaycode(String paycode)
        {
            PayrollTransactionEmployeePaycodeCollection rtn = new PayrollTransactionEmployeePaycodeCollection();
            foreach (PayrollTransactionEmployeePaycode item in this)
            {
                if (item.PaycodeCode == paycode)
                {
                    rtn.Add(item);
                }
            }

            return rtn;
        }

        public PayrollTransactionCollection PayrollTransactions
        {
            get
            {
                PayrollTransactionCollection rtn = new PayrollTransactionCollection();
                foreach (PayrollTransaction item in this)
                {
                    rtn.Add(item);
                }
                return rtn;
            }
        }
    }
}