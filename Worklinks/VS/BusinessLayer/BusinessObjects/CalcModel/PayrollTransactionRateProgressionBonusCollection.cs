﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.CalcModel
{
    [Serializable]
    [CollectionDataContract]
    public class PayrollTransactionRateProgressionBonusCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<PayrollTransactionRateProgressionBonus>
    {

        public PayrollTransactionCollection GetBonusTransactions(long payrollBatchId)
        {
            long id = -1;
            PayrollTransactionCollection collection = new PayrollTransactionCollection();

            foreach (PayrollTransactionRateProgressionBonus rate in this)
            {
                collection.Add(rate.GetBonusTransactions(payrollBatchId, ref id));
            }

            return collection;
        }

        public SalaryPlanGradeAccumulationCollection GetCurrentAccumulations()
        {
            long id = -1;
            Dictionary<String, SalaryPlanGradeAccumulation> collection = new Dictionary<String, SalaryPlanGradeAccumulation>();
            SalaryPlanGradeAccumulationCollection rtn = new SalaryPlanGradeAccumulationCollection();

            foreach (PayrollTransactionRateProgressionBonus item in this)            {
                SalaryPlanGradeAccumulation currentItem = item.SalaryPlanGradeAccumulation;
                if (currentItem != null)
                {
                    SalaryPlanGradeAccumulation rtnItem = null;
                    if (collection.TryGetValue(currentItem.KeyMask, out rtnItem))
                    {
                        rtnItem.Hours += currentItem.Hours;
                        rtnItem.Amount += currentItem.Amount;
                    }
                    else
                    {
                        rtnItem = new SalaryPlanGradeAccumulation();
                        currentItem.CopyTo(rtnItem);
                        rtnItem.SalaryPlanGradeAccumulationId = id--;
                        collection.Add(currentItem.KeyMask, rtnItem);
                        rtn.Add(rtnItem);
                    }
                }

            }

            return rtn;
        }


    }
}