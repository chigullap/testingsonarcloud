﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.CalcModel
{
    public class PayrollTransactionEmployeePaycode : PayrollTransaction
    {
        #region properties

        public String PaycodeTypeCode { get; set; }
        public bool IsProvisionPaycode { get; set; }
        public String PaycodeIncomePayTypeCode { get; set;  }
        #endregion

        #region calcs
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            PayrollTransactionEmployeePaycode data = new PayrollTransactionEmployeePaycode();
            CopyTo(data);

            return data;
        }
        public void CopyTo(PayrollTransactionEmployeePaycode data)
        {
            base.CopyTo(data);

            data.PaycodeTypeCode = PaycodeTypeCode;
            data.IsProvisionPaycode = IsProvisionPaycode;
            data.PaycodeIncomePayTypeCode = PaycodeIncomePayTypeCode;
        }
        public new void Clear()
        {
            base.Clear();

            PaycodeTypeCode = null;
            IsProvisionPaycode = false;
            PaycodeIncomePayTypeCode = null;
        }
        #endregion
    }
}