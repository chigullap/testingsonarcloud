﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects.CalcModel
{
    public class PayrollTransactionRateProgressionBonus : PayrollTransaction
    {

        #region fields
        #endregion

        #region properties
        public long? SalaryPlanId { get; set; }
        public long? SalaryPlanGradeId { get; set; }
        public Decimal? CurrentPeriodPlanHour { get; set; }
        public Decimal? PreviousTotalPlanHour { get; set; }
        public long PayrollProcessId { get; set; }
        public String OutputPaycode { get; set; }
        public Decimal AmountRateFactor { get; set; }
        public BonusRuleCollection BonusRules { get; set; }
        #endregion

        #region constructor/destructor
        public PayrollTransactionRateProgressionBonus()
        {

        }

        public PayrollTransactionRateProgressionBonus(BonusRuleCollection rule)
        {
            this.BonusRules = rule;
        }
        #endregion

        #region calcs
        public Decimal TotalPlanHour { get { return (CurrentPeriodPlanHour ?? 0) + (PreviousTotalPlanHour ?? 0); } }
        public Decimal CurrentStartingPlanHour { get { return (TotalPlanHour-Units); } }


        public PayrollTransactionCollection GetBonusTransactions(long payrollBatchId, ref long id)
        {
            PayrollTransactionCollection rtn = new PayrollTransactionCollection();

            if (OutputPaycode !=null && BonusRules != null && BonusRules.Count>0)
            {
                BonusRule startBonus = BonusRules.GetBonusRule(this.CurrentStartingPlanHour, SalaryPlanId);
                BonusRule endBonus = BonusRules.GetBonusRule(this.TotalPlanHour, SalaryPlanId);

                if (startBonus != endBonus) //
                {
                    PayrollTransaction startTransaction = PayrollTransactionRateProgressionBonusCalc.GetBonusPayrollTransaction(startBonus, this, payrollBatchId, ref id);
                    if (startTransaction != null) rtn.Add(startTransaction);
                    PayrollTransaction endTransaction = PayrollTransactionRateProgressionBonusCalc.GetBonusPayrollTransaction(endBonus, this, payrollBatchId, ref id);
                    if (endTransaction != null) rtn.Add(endTransaction);
                }
                else if (startBonus==endBonus)
                {
                    PayrollTransaction transaction = PayrollTransactionRateProgressionBonusCalc.GetBonusPayrollTransaction(startBonus, this, payrollBatchId, ref id);
                    if (transaction != null) rtn.Add(transaction);
                }
            }

            return rtn;
        }


        public SalaryPlanGradeAccumulation SalaryPlanGradeAccumulation
        {
            get
            {

                SalaryPlanGradeAccumulation rtn = null;
                if (SalaryPlanId != null)
                {
                    rtn = new SalaryPlanGradeAccumulation();
                    rtn.PayrollProcessId = PayrollProcessId;
                    rtn.EmployeeId = EmployeeId;
                    rtn.SalaryPlanGradeId = (long)SalaryPlanGradeId;
                    rtn.Hours = EmploymentInsuranceHour;
                    rtn.Amount = 0; //add later
                }

                return rtn;
            }
        }

        #endregion


    }
}

