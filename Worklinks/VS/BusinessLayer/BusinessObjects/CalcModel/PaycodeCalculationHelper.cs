﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects.CalcModel
{
    public static class PaycodeCalculationHelper
    {
        public static Decimal GetYearMaximumAmount(Decimal amount, Decimal payrollToDateAmount,Decimal currentYTDAmount, Decimal? yearlyMaximumAmount)
        {
            if (yearlyMaximumAmount == null)
                return Decimal.MaxValue;
            else if ( (currentYTDAmount + payrollToDateAmount)  >= yearlyMaximumAmount)
                return 0;
            else if ((currentYTDAmount + amount + payrollToDateAmount) > yearlyMaximumAmount)
                return (Decimal)yearlyMaximumAmount - (currentYTDAmount+ payrollToDateAmount);
            else
                return Decimal.MaxValue;
        }

        public static Decimal GetPeriodMaximumAmount(Decimal amount, Decimal payrollToDateAmount, Decimal PeriodToDateAmount, Decimal? periodMaximumAmount)
        {
            if (periodMaximumAmount == null)
                return Decimal.MaxValue;
            else if ((payrollToDateAmount + PeriodToDateAmount) >= periodMaximumAmount)
                return 0;
            else if ((payrollToDateAmount + PeriodToDateAmount+amount>periodMaximumAmount))
                return (Decimal) periodMaximumAmount - (PeriodToDateAmount + payrollToDateAmount);
            else
                return (Decimal)periodMaximumAmount;
        }

        public static Decimal GetMaximum(Decimal amount, Decimal payrollToDateAmount, Decimal PeriodToDateAmount, Decimal currentYTDAmount, Decimal? periodMaximumAmount, Decimal? yearlyMaximumAmount)
        {
            Decimal yearMax = GetYearMaximumAmount(amount, payrollToDateAmount, currentYTDAmount, yearlyMaximumAmount);
            Decimal periodMax = GetPeriodMaximumAmount(amount, payrollToDateAmount, PeriodToDateAmount, periodMaximumAmount);

            //take least max
            if (yearMax < periodMax)
                return yearMax;
            else
                return periodMax;
        }
    }
}
