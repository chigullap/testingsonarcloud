﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.CalcModel
{
    [Serializable]
    [DataContract]
    public class PayrollTransactionPayrollMaster : PayrollTransaction
    {
        #region properties
        public bool IncomeTaxFlag { get; set; }
        public bool QuebecTaxFlag { get; set; }
        public bool TaxOverrideFlag { get; set; }
        public bool CashTaxableFlag { get; set; }
        public bool CanadaQuebecPensionPlanFlag { get; set; }
        public bool ProvincialParentalInsurancePlanFlag { get; set; }
        public bool EmploymentInsuranceFlag { get; set; }
        public bool BeforeIncomeTaxDeductionFlag { get; set; }
        public bool BeforeBonusTaxDeductionFlag { get; set; }
        public bool WorkersCompensationBoardFlag { get; set; }
        public bool ProvincialHealthTaxFlag { get; set; }
        public String PaycodeTypeCode { get; set; }
        public String PaycodeIncomePayTypeCode { get; set; }


        #endregion

        #region calcs
        public Decimal Amount { get { return Math.Round((Rate ?? 0) * Units, 2, MidpointRounding.AwayFromZero); } }
        public bool IsIncome { get { return PaycodeTypeCode == ((short)CodePaycode.PaycodeType.Income).ToString(); } }
        public bool IsBenefit { get { return PaycodeTypeCode == ((short)CodePaycode.PaycodeType.Benefit).ToString(); } }
        public bool IsDeduction { get { return PaycodeTypeCode == ((short)CodePaycode.PaycodeType.Deduction).ToString(); } }
        public Decimal TaxableIncome
        {
            get
            {
                return (IsIncome && IncomeTaxFlag && PaycodeIncomePayTypeCode == "REGPAY") ? Amount : 0;
            }
        }
        public Decimal OverrideTaxIncome
        {
            get
            {
                return TaxOverrideFlag ? Amount : 0;
            }
        }
        public Decimal QuebecTaxableIncome
        {
            get
            {
                return (IsIncome && QuebecTaxFlag && PaycodeIncomePayTypeCode == "REGPAY") ? Amount : 0;
            }
        }
        public Decimal BonusTaxableIncome
        {
            get
            {
                return (IsIncome && IncomeTaxFlag && (PaycodeIncomePayTypeCode == "BONUS" || PaycodeIncomePayTypeCode == "LUMPSUM")) ? Amount : 0;
            }
        }
        public Decimal QuebecBonusTaxableIncome
        {
            get
            {
                return (IsIncome && QuebecTaxFlag && (PaycodeIncomePayTypeCode == "BONUS" || PaycodeIncomePayTypeCode == "LUMPSUM")) ? Amount : 0;
            }
        }
        public Decimal LumpsumTaxableIncome { get; } //future....need this
        public Decimal QuebecLumpsumTaxableIncome { get; } //future....need this
        public Decimal PensionTaxableIncome
        {
            get
            {
                return (IsIncome && IncomeTaxFlag && PaycodeIncomePayTypeCode == "PENSION") ? Amount : 0;
            }
        }
        public Decimal QuebecPensionTaxableIncome
        {
            get
            {
                return (IsIncome && QuebecTaxFlag && PaycodeIncomePayTypeCode == "PENSION") ? Amount : 0;
            }
        }
        public Decimal TaxableBenefit
        {
            get
            {
                return (IsBenefit && IncomeTaxFlag) ? Amount : 0;
            }
        }
        public Decimal QuebecTaxableBenefit
        {
            get
            {
                return (IsBenefit && QuebecTaxFlag) ? Amount : 0;
            }
        }
        public Decimal TaxableCashBenefit
        {
            get
            {
                return (IsBenefit && IncomeTaxFlag && CashTaxableFlag) ? Amount : 0;
            }
        }
        public Decimal QuebecTaxableCashBenefit
        {
            get
            {
                return (IsBenefit && QuebecTaxFlag && CashTaxableFlag) ? Amount : 0;
            }
        }
        public Decimal TaxableNonCashBenefit
        {
            get
            {
                return (IsBenefit && IncomeTaxFlag && !CashTaxableFlag) ? Amount : 0;
            }
        }
        public Decimal QuebecTaxableNonCashBenefit
        {
            get
            {
                return (IsBenefit && QuebecTaxFlag && !CashTaxableFlag) ? Amount : 0;
            }
        }
        public Decimal SeveranceTaxableIncome
        {
            get
            {
                return (IsIncome && IncomeTaxFlag && PaycodeIncomePayTypeCode == "SEVRNCE") ? Amount : 0;
            }
        }
        public Decimal QuebecSeveranceTaxableIncome
        {
            get
            {
                return (IsIncome && QuebecTaxFlag && PaycodeIncomePayTypeCode == "SEVRNCE") ? Amount : 0;
            }
        }
        public Decimal CanadaQuebecPensionPlanEarnings
        {
            get
            {
                return CanadaQuebecPensionPlanFlag ? Amount : 0;
            }
        }
        public Decimal ProvincialParentalInsurancePlanEarnings
        {
            get
            {
                return ProvincialParentalInsurancePlanFlag ? Amount : 0;
            }
        }
        public Decimal EmploymentInsuranceEarnings
        {
            get
            {
                return EmploymentInsuranceFlag ? Amount : 0;
            }
        }
        public Decimal CommissionTaxableIncome
        {
            get
            {
                return (IsIncome && IncomeTaxFlag && PaycodeIncomePayTypeCode == "COMM") ? Amount : 0;
            }
        }
        public Decimal QuebecCommissionTaxableIncome
        {
            get
            {
                return (IsIncome && QuebecTaxFlag && PaycodeIncomePayTypeCode == "COMM") ? Amount : 0;
            }
        }
        public Decimal BonusIncomeBeforeTaxDeduction
        {
            get
            {
                return (IsDeduction && OverrideBonusDeductionBeforeTaxPercent == null && BeforeIncomeTaxDeductionFlag && BeforeBonusTaxDeductionFlag) ? Amount : 0;
            }
        }
        public Decimal IncomeOnlyDeductionBeforeTax
        {
            get
            {
                if (IsDeduction && OverrideBonusDeductionBeforeTaxPercent != null)
                    return Math.Round((100 - (Decimal)OverrideBonusDeductionBeforeTaxPercent) / 100 * Amount, 2, MidpointRounding.AwayFromZero);
                else if (IsDeduction && BeforeIncomeTaxDeductionFlag && !BeforeBonusTaxDeductionFlag)
                    return Amount;
                return 0;
            }
        }
        public Decimal BonusOnlyDeductionBeforeTax
        {
            get
            {
                if (OverrideBonusDeductionBeforeTaxPercent != null)
                    return Math.Round((Decimal)OverrideBonusDeductionBeforeTaxPercent / 100 * Amount, 2, MidpointRounding.AwayFromZero);
                else if (!BeforeIncomeTaxDeductionFlag && BeforeBonusTaxDeductionFlag)
                    return Amount;

                return 0;

            }
        }

        //public Decimal BeforeIncomeTaxDeduction
        //{
        //    get
        //    {
        //        return ((BonusTaxableIncome + TaxableIncome == 0) ? BeforeTaxDeduction : Math.Round(BeforeTaxDeduction * TaxableIncome / (TaxableIncome + BonusTaxableIncome), 2, MidpointRounding.AwayFromZero)) + IncomeOnlyDeductionBeforeTax;
        //    }
        //}

        //public Decimal BeforeBonusTaxDeduction
        //{
        //    get
        //    {
        //        return ((BonusTaxableIncome + TaxableIncome == 0) ? 0 : Math.Round(BeforeTaxDeduction * BonusTaxableIncome / (TaxableIncome + BonusTaxableIncome), 2, MidpointRounding.AwayFromZero)) + BonusOnlyDeductionBeforeTax;
        //    }
        //}
        public Decimal WorkersCompensationEarnings
        {
            get
            {
                return WorkersCompensationBoardFlag?Amount:0;
            }
        }
        public Decimal EmployerHealthTaxEarnings
        {
            get
            {
                return (ProvincialHealthTaxFlag) ? Amount : 0;
            }
        }

        #endregion
    }
}