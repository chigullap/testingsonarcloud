﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using static WorkLinks.BusinessLayer.BusinessObjects.CalcModel.PayrollTransactionEmployeePaycodeCollection;

namespace WorkLinks.BusinessLayer.BusinessObjects.CalcModel
{
    public class EmployeePaycodeCalculation : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region fields
        private Decimal _amountUnits;
        #endregion

        #region properties

        public long EmployeePaycodeCalculationId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        public long EmployeeId { get; set; }
        public String PaycodeCode { get; set; }
        public Decimal? AmountPercentage { get; set; }
        public Decimal? AmountRate { get; set; }
        public Decimal AmountUnits
        {
            get { return _amountUnits; }
            set { _amountUnits = (value == 0) ? 1 : value; }
        }
        public bool RecurringIncomeCodeFlag { get; set; }
        public String OrganizationUnit { get; set; }
        public Decimal YtdIncomeAmount { get; set; }
        public Decimal YtdPaycodeAmount { get; set; }
        public Decimal PeriodToDateAmount { get; set; }
        public String PaycodeTypeCode { get; set; }
        public String PaycodeIncomePayTypeCode { get; set; }
        public bool BeforeBonusTaxDeductionFlag { get; set; }
        public bool BeforeIncomeTaxDeductionFlag { get; set; }
        public String PaycodeRateBasedOnCode { get; set; }

        //restriction parameters that effect provision amounts
        public Decimal RequiredMinimumIncomeAmount { get; set; }
        //future public Decimal RequiredMaximumIncomeAmount 

        //restriction parameters that affect final calculated value
        //future public TotalMaximumAmount {get;set;}
        public Decimal YearlyMaximumAmount { get; set; }
        //future public Decimal YearlyOffsetAmount { get; set; }
        public Decimal PeriodMaximumAmount { get; set; }
        //future public Decimal PeriodOffsetAmount { get; set; }
        //future public Decimal MonthlyMaximumAmount { get; set; }
        //future public Decimal MonthlyOffsetAmount { get; set; }

        //restriction parameters that affect calc/skip calc
        public String PayrollProcessRunTypeCode { get; set; }
        public String PaycodeActivationFrequencyCode { get; set; }
        public int PayrollCountThisMonth { get; set; }
        public String ArrearsOffsetPaycodeCode { get; set; }
        public bool IncludeInArrearsFlag { get; set; }
        public bool newArrearsProcessingFlag { get; set; }
        private Decimal _relatedTransactionAmount = 0;
        private PayrollTransactionEmployeePaycodeCollection _transactions = null;
        private PayrollTransactionEmployeePaycodeCollection _paycodeRelatedTransactions = null;
        public Dictionary<string, EmployeePaycodeCalculation> EmployeePaycodes { get; set; }
        public bool PreviousEmployeeTransactionsHasIncome { get; set; }


        public PayrollTransactionEmployeePaycodeCollection PaycodeRelatedTransactions
        {
            set
            {
                _paycodeRelatedTransactions = value;
                _relatedTransactionAmount = _paycodeRelatedTransactions.TotalAmount;
            }
        }

        #endregion

        #region calcs
        public bool CurrentTransactionsHaveIncome
        {
            get
            {
                return _transactions != null && _transactions.HasIncome;
            }
        }
        public bool ValidPaycodeForPayroll
        {
            get
            {
                //               if (_transactions != null) //don't process if no income ...temporary HACK
                //                {

                if (PayrollProcessRunTypeCode.ToLower() == "regular")
                {
                    switch (PaycodeActivationFrequencyCode.ToLower())
                    {
                        case "first":
                            if (PayrollCountThisMonth == 1)
                                return true;
                            break;
                        case "firstsec":
                            if (PayrollCountThisMonth == 1 || PayrollCountThisMonth == 2)
                                return true;
                            break;
                        case "normal":
                        case "every":
                            return true;
                    }
                }
                else if (PaycodeActivationFrequencyCode.ToLower() == "every")
                {
                    return true;
                }
                //                }
                return false;
            }
        }
        private bool EmployeePaycodeHasIncome
        {
            get
            {
                foreach (EmployeePaycodeCalculation calc in this.EmployeePaycodes.Values)
                {
                    if ((calc.PaycodeTypeCode == "1" || calc.RecurringIncomeCodeFlag)  && calc.AmountUnits > 0)
                        return true;
                }

                return false;
            }
        }



        /// <summary>
        /// returns the fixed amount total for an employee paycode
        /// </summary>
        public Decimal StaticAmount
        {
            get
            {
                if (
                        (CurrentTransactionsHaveIncome) 
                        || this.ArrearsOffsetPaycodeCode != null 
                        || this.EmployeePaycodeHasIncome 
                        || (this.newArrearsProcessingFlag && this.IncludeInArrearsFlag)
                    )
                    return Math.Round(AmountUnits * (AmountRate ?? 0) * ((PaycodeRateBasedOnCode ?? String.Empty).ToLower().Equals("eihours") ? _transactions.TotalProvisionEmploymentInsuranceHour : 1), 2, MidpointRounding.AwayFromZero);
                else
                    return 0;
            }
        }
        //how much provision are we allowed to use?
        private Decimal ProvisionAmount
        {
            get
            {
                if (_transactions == null)
                    return 0;
                else
                {
                    Decimal totalProvisionIncomeAmount = _transactions.TotalProvisionIncomeAmount;

                    if (this.YtdIncomeAmount + totalProvisionIncomeAmount < RequiredMinimumIncomeAmount) //we havent reached our lower limit of income required to calculate
                        return 0;
                    else if (this.YtdIncomeAmount >= RequiredMinimumIncomeAmount) //provision amount already exceeds lower limit, use full provision amount
                        return totalProvisionIncomeAmount;
                    else //provision amount is crossing lower boundary
                        return totalProvisionIncomeAmount + YtdIncomeAmount - RequiredMinimumIncomeAmount;
                }

            }
        }
        public Decimal PercentAmount
        {
            get
            {
                if (CurrentTransactionsHaveIncome)
                    return Math.Round(((AmountPercentage ?? 0) / 100 * ProvisionAmount), 2, MidpointRounding.AwayFromZero);
                else
                    return 0;
            }
        }

        public Decimal? OverrideBonusDeductionBeforeTaxPercent
        {
            get
            {
                Decimal? rtn = null;

                if (_transactions != null)
                {
                    Decimal totalProvisionIncomeAmount = _transactions.TotalProvisionIncomeAmount;
                    Decimal totalProvisionBonusIncomeAmount = _transactions.TotalProvisionBonusIncomeAmount;

                    if (this.PaycodeTypeCode == "3" && BeforeBonusTaxDeductionFlag && BeforeIncomeTaxDeductionFlag && totalProvisionBonusIncomeAmount != 0 && totalProvisionIncomeAmount != 0) //rule, if both before bonus and before income deduction apply, distribution of amounts must match proprotions produced from attached paycodes
                    {
                        rtn = Math.Round(100 * totalProvisionBonusIncomeAmount / totalProvisionIncomeAmount, 4, MidpointRounding.AwayFromZero);
                    }
                }

                return rtn;
            }
        }

        #endregion

        #region main


        public PayrollTransactionEmployeePaycodeCollection GetTransactions(ref int id, long payrollBatchId, DateTime cutoffDate, bool percentCalcOnly)
        {

            Dictionary<String, PayrollTransactionEmployeePaycode> transactions = new Dictionary<string, PayrollTransactionEmployeePaycode>();
            PayrollTransactionEmployeePaycodeCollection rtn = new PayrollTransactionEmployeePaycodeCollection();

            if (!ValidPaycodeForPayroll)
                return rtn;

            //merge amounts
            if (GetCalculatedPaycodeAmount(percentCalcOnly) != 0)
            {
                Decimal runningAmount = 0;

                if (_transactions != null && _transactions.GetOrganizationUnitBreakdown(this.OrganizationUnit).Length > 0) //we have transactions that have to be applied against
                {


                    foreach (OrganizationUnitPercent item in _transactions.GetOrganizationUnitBreakdown(this.OrganizationUnit))
                    {
                        Decimal amount = Math.Round(GetCalculatedPaycodeAmount(percentCalcOnly) * item.Percent, 2, MidpointRounding.AwayFromZero);
                        runningAmount += amount;
                        rtn.Add(GetAddTranaction(transactions, ref id, payrollBatchId, cutoffDate, 0, amount, item.OrganizationUnit ?? this.OrganizationUnit, true));

                    }


                    if ((GetCalculatedPaycodeAmount(percentCalcOnly) - runningAmount) != 0)
                    {
                        rtn[0].Rate += GetCalculatedPaycodeAmount(percentCalcOnly) - runningAmount;
                    }
                }
                else
                {
                    rtn.Add(GetAddTranaction(transactions, ref id, payrollBatchId, cutoffDate, 0, GetCalculatedPaycodeAmount(percentCalcOnly), this.OrganizationUnit, true));
                }
            }

            //arrears offsets
            if (PayrollProcessRunTypeCode.ToLower() != "adjust" && this.ArrearsOffsetPaycodeCode != null && !CurrentTransactionsHaveIncome && !PreviousEmployeeTransactionsHasIncome)
            {
                PayrollTransactionEmployeePaycodeCollection arrearsOffsets = new PayrollTransactionEmployeePaycodeCollection();
                foreach (PayrollTransactionEmployeePaycode item in rtn)
                {
                    PayrollTransactionEmployeePaycode code = (PayrollTransactionEmployeePaycode)item.Clone();
                    code.PaycodeCode = this.ArrearsOffsetPaycodeCode;
                    code.PayrollTransactionId = id--;
                    arrearsOffsets.Add(code);
                }

                rtn.Add(arrearsOffsets);
            }

            return rtn;
        }

        private PayrollTransactionEmployeePaycode GetAddTranaction(Dictionary<String, PayrollTransactionEmployeePaycode> transactions,
            ref int id, long payrollBatchId, DateTime cutoffDate, Decimal units, Decimal rate, String incomeOrganizationUnit, bool isPercentageBased = false)
        {
            String organizationUnit = incomeOrganizationUnit ?? OrganizationUnit;
            String key = String.Format("{0}", organizationUnit);

            PayrollTransactionEmployeePaycode transaction = null;

            if (!transactions.TryGetValue(key, out transaction))
            {
                transaction = new PayrollTransactionEmployeePaycode();
                transaction.PayrollTransactionId = id--;
                transaction.Units = isPercentageBased ? 1 : 0;
                transactions.Add(key, transaction);
            }
            transaction.PayrollBatchId = payrollBatchId;
            transaction.EmployeeId = this.EmployeeId;
            transaction.TransactionDate = cutoffDate;
            transaction.PaycodeCode = this.PaycodeCode;
            transaction.EntryTypeCode = "NORM";
            transaction.Units += units;
            transaction.Rate += rate;
            transaction.PaycodeTypeCode = this.PaycodeTypeCode;
            transaction.PaycodeIncomePayTypeCode = this.PaycodeIncomePayTypeCode;
            transaction.OverrideBonusDeductionBeforeTaxPercent = this.OverrideBonusDeductionBeforeTaxPercent;
            transaction.OrganizationUnit = incomeOrganizationUnit ?? OrganizationUnit;

            return transaction;
        }
        public void LoadTransactions(PayrollTransactionEmployeePaycodeCollection transactions, PaycodeAttachedPaycodeProvisionCollection provisions)
        {
            //step 1 make a copy
            _transactions = new PayrollTransactionEmployeePaycodeCollection();
            transactions.CopyTo(_transactions);

            //step 2 set provision flags
            if (provisions != null)
                SetTransactionProvisionFlags(provisions);
        }
        private void SetTransactionProvisionFlags(PaycodeAttachedPaycodeProvisionCollection provisions)
        {
            foreach (PaycodeAttachedPaycodeProvision provision in provisions)
            {
                foreach (PayrollTransactionEmployeePaycode tranaction in _transactions.GetTransactionByPaycode(provision.ProvisionPaycodeCode))
                {
                    tranaction.IsProvisionPaycode = true;
                }

            }
        }

        /// <summary>
        /// calculates the upper maximum amount for this paycode (yearly, monthly, per period etc)
        /// </summary>
        /// <param name="unrestrictedPaycodeAmount"></param>
        /// <returns></returns>
        private Decimal GetMaximumAmountForPaycode(Decimal paycodeAmount)
        {
            return PaycodeCalculationHelper.GetMaximum(paycodeAmount, _relatedTransactionAmount, PeriodToDateAmount, YtdPaycodeAmount, PeriodMaximumAmount, YearlyMaximumAmount);
        }



        /// <summary>
        /// calculate amount with all restrictions calculated in
        /// </summary>
        private Decimal GetCalculatedPaycodeAmount(bool percentCalcOnly)
        {
            //get base amount
            Decimal paycodeAmount = (percentCalcOnly ? 0 : StaticAmount) + PercentAmount;

            //get max amount
            Decimal maxAmount = GetMaximumAmountForPaycode(paycodeAmount);

            if (maxAmount > paycodeAmount)
                return paycodeAmount;
            else
                return maxAmount;
        }

        #endregion

        #region construct/destruct
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeePaycodeCalculation data)
        {
            throw new NotImplementedException();
            base.CopyTo(data);

        }
        public new void Clear()
        {
            throw new NotImplementedException();
            base.Clear();
        }
        #endregion
    }
}