﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class CodePaycodeCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<CodePaycode>
    {
    }
}