﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CanadaRevenueAgencyT619 : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long CanadaRevenueAgencyT619Id
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String TransmitterNumber { get; set; }
        [DataMember]
        public String TransmitterName { get; set; }
        [DataMember]
        public String CanadaRevenueAgencyT619TransmitterTypeCode { get; set; }
        [DataMember]
        public String LanguageCode { get; set; }
        [DataMember]
        public Person Person { get; set; }
        [DataMember]
        public PersonAddress PersonAddress { get; set; }
        [DataMember]
        public PersonContactChannel Email { get; set; }
        [DataMember]
        public PersonContactChannel Phone { get; set; }
        [DataMember]
        public long TransmitterPersonId { get; set; }

        #endregion


        #region construct/destruct
        public CanadaRevenueAgencyT619()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(CanadaRevenueAgencyT619 data)
        {
            base.CopyTo(data);

            data.CanadaRevenueAgencyT619Id = CanadaRevenueAgencyT619Id;
            data.TransmitterNumber = TransmitterNumber;
            data.TransmitterName = TransmitterName;
            data.CanadaRevenueAgencyT619TransmitterTypeCode = CanadaRevenueAgencyT619TransmitterTypeCode;
            data.LanguageCode = LanguageCode;
            data.Person = Person;
            data.PersonAddress = PersonAddress;
            data.Email = Email;
            data.Phone = Phone;
            data.TransmitterPersonId = TransmitterPersonId;
        }

        public new void Clear()
        {
            base.Clear();

            CanadaRevenueAgencyT619Id = -1;
            TransmitterNumber=null;
            TransmitterName = null;
            CanadaRevenueAgencyT619TransmitterTypeCode = null;
            LanguageCode=null;
            Person=null;
            PersonAddress=null;
            Email=null;
            Phone = null;
            TransmitterPersonId = -1;
        }

        #endregion
    }
}
