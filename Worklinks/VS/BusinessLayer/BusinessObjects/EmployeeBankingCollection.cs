﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Linq;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeeBankingCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeeBanking>
    {
        public EmployeeBankingCollection SortedCollection
        {
            get
            {
                EmployeeBankingCollection sortedCollection = new EmployeeBankingCollection();
                IEnumerable<EmployeeBanking> query 
                    = from bank in this
                      orderby bank.CodeEmployeeBankingSequenceCode
                      select bank;

                foreach (EmployeeBanking bank in query)
                    sortedCollection.Add(bank);

                return sortedCollection;
            }

        }
    }

}
