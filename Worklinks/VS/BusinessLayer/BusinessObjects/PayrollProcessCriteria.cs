﻿using System;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class PayrollProcessCriteria
    {
        #region properties
        public bool GetLastOpenPayrollProcessFlag { get; set; }
        public long? PayrollProcessId { get; set; }
        public long? PayrollPeriodId { get; set; }
        public bool IncludePayrollTransactions { get; set; }
        public String PayrollProcessGroupCode { get; set; }
        public String PayrollProcessRunTypeCode { get; set; }
        public long? EmployeeId { get; set; }
        public String PayrollProcessStatusCode { get; set; }
        public bool SecurityOverrideFlag { get; set; }
        public int? Limit { get; set; }
        #endregion

        public PayrollProcessCriteria()
        {
            Clear();
        }

        public void Clear()
        {
            PayrollProcessId = null;
            PayrollPeriodId = null;
            GetLastOpenPayrollProcessFlag = false;
            IncludePayrollTransactions = false;
            PayrollProcessGroupCode = null;
            PayrollProcessRunTypeCode = null;
            EmployeeId = null;
            PayrollProcessStatusCode = "PT"; //processed
            SecurityOverrideFlag = true;
            Limit = null;
        }
    }
}