﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class LabourUnionCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<LabourUnion>
    {
    }
}