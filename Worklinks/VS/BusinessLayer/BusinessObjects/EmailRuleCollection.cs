﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmailRuleCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmailRule>
    {
    }
}