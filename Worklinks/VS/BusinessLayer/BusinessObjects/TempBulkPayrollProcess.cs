﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class TempBulkPayrollProcess : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PayrollPeriodId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String PayrollProcessGroupCode { get; set; }

        [DataMember]
        public int EmployeeCount { get; set; }

        [DataMember]
        public int TransactionCount { get; set; }

        [DataMember]
        public int StatutoryDeductionCount { get; set; }

        [DataMember]
        public String PayrollProcessStatusCode { get; set; }
        #endregion

        #region construct/destruct
        public TempBulkPayrollProcess()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(TempBulkPayrollProcess data)
        {
            base.CopyTo(data);

            data.PayrollPeriodId = PayrollPeriodId;
            data.PayrollProcessGroupCode = PayrollProcessGroupCode;
            data.EmployeeCount = EmployeeCount;
            data.TransactionCount = TransactionCount;
            data.StatutoryDeductionCount = StatutoryDeductionCount;
            data.PayrollProcessStatusCode = PayrollProcessStatusCode;
        }
        public new void Clear()
        {
            base.Clear();

            PayrollPeriodId = -1;
            PayrollProcessGroupCode = null;
            EmployeeCount = 0;
            TransactionCount = 0;
            StatutoryDeductionCount = 0;
            PayrollProcessStatusCode = null;
        }
        #endregion
    }
}