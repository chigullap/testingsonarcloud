﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class HealthTaxDetailCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<HealthTaxDetail>
    {
    }
}