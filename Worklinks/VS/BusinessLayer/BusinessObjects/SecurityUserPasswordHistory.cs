﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SecurityUserPasswordHistory: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {

        #region fields
        #endregion
        
        #region properties
        [DataMember]
        public long SecurityUserPasswordHistoryId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long SecurityUserId { get; set; }
        [DataMember]
        public String UserName { get; set; }
        [DataMember]
        public String Password { get; set; }
        [DataMember]
        public String PasswordSalt { get; set; }
        [DataMember]
        public long? PersonId { get; set; }
        [DataMember]
        public DateTime? LastPasswordChangedDatetime { get; set; }
        
        #endregion

        #region construct/destruct
        public SecurityUserPasswordHistory()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(SecurityUserPasswordHistory data)
        {
            base.CopyTo(data);

            data.SecurityUserPasswordHistoryId = SecurityUserPasswordHistoryId;
            data.SecurityUserId = SecurityUserId;
            data.UserName = UserName;
            data.Password = Password;
            data.PasswordSalt = PasswordSalt;
            data.PersonId = PersonId;
            data.LastPasswordChangedDatetime = LastPasswordChangedDatetime;
        }

        public new void Clear()
        {
            base.Clear();

            SecurityUserPasswordHistoryId = -1;
            SecurityUserId = -1;
            UserName = "";
            Password = null;
            PasswordSalt = null;
            PersonId = null;
            LastPasswordChangedDatetime = null;
        }

        #endregion
    }
}
