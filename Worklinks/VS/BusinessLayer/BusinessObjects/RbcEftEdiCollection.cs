﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class RbcEftEdiCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<RbcEftEdi>
    {
        public int CountOfRecordsInFile
        {
            get
            {
                int rec = 0;

                foreach (RbcEftEdi eft in this)
                {
                    if (eft.DirectDepositFlag || (!eft.DirectDepositFlag && eft.ManualChequeFlag == false))
                        rec++;
                }

                return rec;
            }
        }
    }
}
