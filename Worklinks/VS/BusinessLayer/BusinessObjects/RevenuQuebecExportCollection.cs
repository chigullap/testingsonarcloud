﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class RevenuQuebecExportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<RevenuQuebecExport>
    {
    }
}
