﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeeEmploymentEquityCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeeEmploymentEquity>
    {
    }
}