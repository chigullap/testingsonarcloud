﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class WizardCache : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        public class ItemType
        {
            public const String None = "None";
            public const String EmployeeBiographicalControl = "EmployeeBiographicalControl";
            public const String EmployeePositionViewControl = "EmployeePositionViewControl";
            public const String EmployeeBankingInformationControl = "EmployeeBankingInformationControl";
            public const String EmployeePaycodeModuleControl = "EmployeePaycodeModuleControl";
            public const String PersonPhoneControl = "PersonPhoneControl";
            public const String PersonEmailControl = "PersonEmailControl";
            public const String AddressDetailControl = "AddressDetailControl";
            public const String StatutoryDeductionViewControl = "StatutoryDeductionViewControl";
            public const String EmployeeEmploymentInformationControl = "EmployeeEmploymentInformationControl";
            public const String EmployeeEducationControl = "EmployeeEducationControl";
            public const String EmployeeSkillControl = "EmployeeSkillControl";
            public const String EmployeeCompanyPropertyControl = "EmployeeCompanyPropertyControl";
            public const String EmployeeLicenseCertificateControl = "EmployeeLicenseCertificateControl";
        }

        #region properties
        [DataMember]
        public long WizardCacheId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long WizardId { get; set; }

        [DataMember]
        public bool TemplateFlag { get; set; }

        [DataMember]
        public bool ActiveFlag { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public String ImportExternalIdentifier { get; set; }

        [DataMember]
        public String CountryCode { get; set; }

        [DataMember]
        public WizardCacheItemCollection Items { get; set; }

        public WizardCacheItemCollection ActiveItems
        {
            get
            {
                WizardCacheItemCollection activeItems = new WizardCacheItemCollection();

                foreach (WizardCacheItem item in Items)
                {
                    if (item.ActiveFlag)
                        activeItems.Add(item);
                }

                return activeItems;
            }
        }
        #endregion

        #region construct/destruct
        public WizardCache()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public void CopyTo(WizardCache data)
        {
            base.CopyTo(data);

            data.WizardCacheId = WizardCacheId;
            data.WizardId = WizardId;
            data.TemplateFlag = TemplateFlag;
            data.ActiveFlag = ActiveFlag;
            data.Description = Description;
            data.ImportExternalIdentifier = ImportExternalIdentifier;
            data.CountryCode = CountryCode;

            if (Items == null)
                data.Items = null;
            else
            {
                data.Items = new WizardCacheItemCollection();
                Items.CopyTo(data.Items);
            }
        }
        public new void Clear()
        {
            base.Clear();

            WizardCacheId = -1;
            WizardId = -1;
            TemplateFlag = false;
            ActiveFlag = true;
            Description = null;
            ImportExternalIdentifier = null;
            CountryCode = null;
            Items = null;
        }
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}