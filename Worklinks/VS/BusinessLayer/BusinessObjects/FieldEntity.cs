﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]

    public class FieldEntity : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties

        [DataMember]
        public long ControlFieldValueId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long ControlFieldId { get; set; }
        [DataMember]
        public String FieldIdentifier { get; set; }
        [DataMember]
        public String CodeLanguageCd { get; set; }
        [DataMember]
        public long SecurityRoleId { get; set; }    //"role_id" from "form_description" table, which is passed into the select procedure that populates this class
        [DataMember]
        public String AttributeIdentifier { get; set; }
        [DataMember]
        public String FieldValue { get; set; }

        public bool FieldValueAsBoolean
        {
            get
            {
                return Convert.ToBoolean(FieldValue);
            }
            set
            {
                FieldValue = Convert.ToString(value);
            }
        }
        
        #endregion

        #region construct/destruct
        public FieldEntity()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(FieldEntity data)
        {
            base.CopyTo(data);

            data.ControlFieldValueId = ControlFieldValueId;
            data.ControlFieldId = ControlFieldId;
            data.FieldIdentifier = FieldIdentifier;
            data.CodeLanguageCd = CodeLanguageCd;
            data.SecurityRoleId = SecurityRoleId;
            data.AttributeIdentifier = AttributeIdentifier;
            data.FieldValue = FieldValue;
        }
        public new void Clear()
        {
            base.Clear();

            ControlFieldValueId = -1;
            ControlFieldId = -1;
            FieldIdentifier = "";
            CodeLanguageCd = "";
            SecurityRoleId = -1;
            AttributeIdentifier = "";
            FieldValue = "";
        }

        #endregion
    }
}
