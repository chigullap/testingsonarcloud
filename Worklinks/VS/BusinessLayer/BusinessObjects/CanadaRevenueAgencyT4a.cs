﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public class CanadaRevenueAgencyT4a : YearEndT4a
    {
        public Employee Employee { get; set; }
        public PersonAddress Address { get; set; }
        public PersonContactChannel Phone { get; set; }
        public BusinessNumber BusinessNumber { get; set; }
    }
}
