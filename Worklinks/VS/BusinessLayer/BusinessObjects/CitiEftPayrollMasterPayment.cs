﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CitiEftPayrollMasterPayment : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PayrollMasterPaymentId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String CurrentDate1 { get; set; }

        [DataMember]
        public String ChequeAmount { get; set; }

        [DataMember]
        public String CurrentDate2 { get; set; }

        [DataMember]
        public String ChequeNumber { get; set; }

        [DataMember]
        public String CompanyShortName { get; set; }

        [DataMember]
        public String CompanyLongName { get; set; }

        [DataMember]
        public String BankAccountNumber { get; set; }

        [DataMember]
        public String EmployeeName { get; set; }

        [DataMember]
        public String BankTransInstCode { get; set; }
        #endregion

        #region construct/destruct
        public CitiEftPayrollMasterPayment()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(CitiEftPayrollMasterPayment data)
        {
            base.CopyTo(data);

            data.PayrollMasterPaymentId = PayrollMasterPaymentId;
            data.CurrentDate1 = CurrentDate1;
            data.ChequeAmount = ChequeAmount;
            data.CurrentDate2 = CurrentDate2;
            data.ChequeNumber = ChequeNumber;
            data.CompanyShortName = CompanyShortName;
            data.CompanyLongName = CompanyLongName;
            data.BankAccountNumber = BankAccountNumber;
            data.EmployeeName = EmployeeName;
            data.BankTransInstCode = BankTransInstCode;
        }
        public new void Clear()
        {
            base.Clear();

            PayrollMasterPaymentId = -1;
            CurrentDate1 = "";
            ChequeAmount = "";
            CurrentDate2 = "";
            ChequeNumber = "";
            CompanyShortName = "";
            CompanyLongName = "";
            BankAccountNumber = "";
            EmployeeName = "";
            BankTransInstCode = "";
        }
        #endregion
    }
}