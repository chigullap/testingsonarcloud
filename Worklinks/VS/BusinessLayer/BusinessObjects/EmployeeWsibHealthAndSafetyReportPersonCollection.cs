﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeeWsibHealthAndSafetyReportPersonCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<EmployeeWsibHealthAndSafetyReportPerson>
    {
    }
}