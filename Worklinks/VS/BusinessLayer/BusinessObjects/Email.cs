﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class Email : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmailId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String Name { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public String EmailTemplateTypeCode { get; set; }
        #endregion

        #region construct/destruct
        public Email()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            Email item = new Email();
            this.CopyTo(item);
            return item;
        }
        public virtual void CopyTo(Email data)
        {
            base.CopyTo(data);
            data.EmailId = EmailId;
            data.Name = Name;
            data.Description = Description;
            data.EmailTemplateTypeCode = EmailTemplateTypeCode;
        }
        public new void Clear()
        {
            base.Clear();
            EmailId = -1;
            Name = null;
            Description = null;
            EmailTemplateTypeCode = null;
        }
        #endregion
    }
}