﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    public class PayrollTransactionError : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties

        [DataMember]
        public long PayrollTransactionId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String EmployeeNumber { get; set; }

        [DataMember]
        public Decimal YearlyMaximum { get; set; }

        [DataMember]
        public String PaycodeCode { get; set; }

        [DataMember]
        public Decimal TotalAmount { get; set; }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PayrollTransactionError data)
        {
            base.CopyTo(data);

            data.PayrollTransactionId = PayrollTransactionId;
            data.EmployeeNumber = EmployeeNumber;
            data.YearlyMaximum = YearlyMaximum;
            data.PaycodeCode = PaycodeCode;
            data.TotalAmount = TotalAmount;
        }
        public new void Clear()
        {
            base.Clear();

            PayrollTransactionId = -1;
            EmployeeNumber = null;
            YearlyMaximum = 0;
            PaycodeCode = null;
            TotalAmount = 0;
        }
        #endregion
    }
}