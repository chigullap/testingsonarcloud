﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PaycodeAttachedPaycodeProvision : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PaycodeAttachedPaycodeProvisionId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String PaycodeCode { get; set; }

        [DataMember]
        public String ProvisionPaycodeCode { get; set; }
        #endregion

        #region construct/destruct
        public PaycodeAttachedPaycodeProvision()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public virtual void CopyTo(PaycodeAttachedPaycodeProvision data)
        {
            base.CopyTo(data);

            data.PaycodeAttachedPaycodeProvisionId = PaycodeAttachedPaycodeProvisionId;
            data.PaycodeCode = PaycodeCode;
            data.ProvisionPaycodeCode = ProvisionPaycodeCode;
        }
        public new void Clear()
        {
            base.Clear();

            PaycodeAttachedPaycodeProvisionId = -1;
            PaycodeCode = null;
            ProvisionPaycodeCode = null;
        }
        #endregion
    }
}