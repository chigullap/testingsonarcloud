﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class CodeTableIndex: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {

        #region properties
        [DataMember]
        public long CodeTableIndexId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String TableName { get; set; }
        [DataMember]
        public String TableDescription { get; set; }
        [DataMember]
        public String ParentTableName { get; set; }
        [DataMember]
        public bool VisibleFlag { get; set; }

        #endregion

        #region construct/destruct
        public CodeTableIndex()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(CodeTableIndex data)
        {
            base.CopyTo(data);

            data.CodeTableIndexId = CodeTableIndexId;
            data.TableName = TableName;
            data.TableDescription = TableDescription;
            data.ParentTableName = ParentTableName;
            data.VisibleFlag = VisibleFlag;
        }
        public new void Clear()
        {
            base.Clear();

            CodeTableIndexId = -1;
            TableName = "";
            TableDescription = "";
            VisibleFlag = true;
        }

        #endregion

    }
}
