﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class YearEndT4arcaCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<YearEndT4arca>
    {
    }
}