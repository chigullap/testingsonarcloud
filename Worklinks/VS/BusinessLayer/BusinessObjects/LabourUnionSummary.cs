﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class LabourUnionSummary : LabourUnion
    {
        #region properties
        //other colummns will be accessed in UnionContactInformation class, which this inherits from
        [DataMember]
        public String AddressLine1 { get; set; }

        [DataMember]
        public String PhoneNumber { get; set; }
        #endregion

        #region construct/destruct
        public LabourUnionSummary()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(LabourUnionSummary data)
        {
            base.CopyTo(data);

            data.AddressLine1 = AddressLine1;
            data.PhoneNumber = PhoneNumber;
        }
        public new void Clear()
        {
            base.Clear();

            AddressLine1 = "";
            PhoneNumber = "";
        }
        #endregion
    }
}