﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class RoeCreationSearchResults : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String EmployeeNumber { get; set; }

        [DataMember]
        public String LastName { get; set; }

        [DataMember]
        public String FirstName { get; set; }

        [DataMember]
        public String PayrollProcessGroupCode { get; set; }

        [DataMember]
        public String OrgUnitDescription { get; set; }

        [DataMember]
        public DateTime? TerminationDate { get; set; }

        [DataMember]
        public String EmployeePositionStatusCode { get; set; }

        [DataMember]
        public long EmployeePositionId { get; set; }
        #endregion

        #region construct/destruct
        public RoeCreationSearchResults()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(RoeCreationSearchResults data)
        {
            base.CopyTo(data);

            data.EmployeeId = EmployeeId;
            data.EmployeeNumber = EmployeeNumber;
            data.LastName = LastName;
            data.FirstName = FirstName;
            data.PayrollProcessGroupCode = PayrollProcessGroupCode;
            data.OrgUnitDescription = OrgUnitDescription;
            data.TerminationDate = TerminationDate;
            data.EmployeePositionStatusCode = EmployeePositionStatusCode;
            data.EmployeePositionId = EmployeePositionId;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeeId = -1;
            EmployeeNumber = null;
            LastName = null;
            FirstName = null;
            PayrollProcessGroupCode = null;
            OrgUnitDescription = null;
            TerminationDate = null;
            EmployeePositionStatusCode = null;
            EmployeePositionId = -1;
        }
        #endregion
    }
}