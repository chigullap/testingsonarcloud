﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class YearEndAdjustment : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public String ColumnName
        {
            get { return (String)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String TableName { get; set; }

        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public String ColumnValue { get; set; }

        [DataMember]
        public bool Visible { get; set; }

        [DataMember]
        public bool ModifyFlag { get; set; }

        [DataMember]
        public String DataType { get; set; }

        [DataMember]
        public Decimal Precision { get; set; }

        [DataMember]
        public String Scale { get; set; }

        [DataMember]
        public Decimal? Minimum { get; set; }

        [DataMember]
        public Decimal? Maximum { get; set; }

        [DataMember]
        public Decimal? AdjustmentValue { get; set; }

        public bool IsNumericFlag { get { return (DataType??"").ToLower().Equals("numeric"); } }

        public Decimal? NumericValue
        {
            get
            {
                if (ColumnValue != null && IsNumericFlag)
                {
                    if (ColumnValue.Trim().Equals(String.Empty))
                        return null;
                    else
                        //return Convert.ToDecimal(ColumnValue);
                        return Convert.ToDecimal(ColumnValue, new System.Globalization.CultureInfo("EN-US"));
                }
                else
                    return null;
            }
            set
            {
                if (IsNumericFlag)
                {
                    if (value != null)
                        ColumnValue = value.ToString();
                    else
                        ColumnValue = null;
                }
            }
        }

        public bool IsStringFlag { get { return DataType.ToLower().Equals("varchar"); } }

        public String StringValue
        {
            get
            {
                if (ColumnValue != null && IsStringFlag)
                    return ColumnValue;
                else
                    return null;
            }
            set
            {
                if (IsStringFlag)
                    ColumnValue = value;
            }
        }

        public bool EnableMinimumValidator
        {
            get
            {
                if (MinimumValue != null)
                    return true;
                else
                    return false;
            }
        }

        public bool EnableMaximumValidator
        {
            get
            {
                if (MaximumValue != null)
                    return true;
                else
                    return false;
            }
        }

        //regular properties
        public int FieldPrecision { get { return Convert.ToInt16(Precision); } }

        public int FieldScale
        {
            get
            {
                if (Scale != "")
                    return Convert.ToInt16(Scale);
                else
                    return 0;
            }
        }

        public String MinimumValue
        {
            get
            {
                if (Minimum != null && IsNumericFlag)
                    return Minimum.ToString();
                else
                    return null;
            }
        }

        public String MaximumValue
        {
            get
            {
                if (Maximum != null && IsNumericFlag)
                    return Maximum.ToString();
                else
                    return null;
            }
        }

        public bool AllowEdit { get { return !ModifyFlag; } }

        public virtual String TotalValue
        {
            get
            {
                if (IsNumericFlag)
                {
                    return getTotalValue().ToString();
                }
                else
                {
                    return null;
                }
            }
        }



        #endregion

        #region functions
        public void FixMinMax()
        {
            if (IsNumericFlag)
            {
                if (EnableMinimumValidator)
                {
                    if (Convert.ToDecimal(MinimumValue)> getTotalValue())
                    {
                        AdjustmentValue = 0;
                        ColumnValue = MinimumValue;
                    }
                }
                if (EnableMaximumValidator)
                {
                    if (getTotalValue() > Convert.ToDecimal(MaximumValue))
                    {
                        AdjustmentValue = 0;
                        ColumnValue = MaximumValue;
                    }
                }
            }
        }
        private Decimal getTotalValue()
        {
            Decimal columnValue = (ColumnValue ?? String.Empty).Equals(String.Empty) ? 0 : Convert.ToDecimal(ColumnValue, new System.Globalization.CultureInfo("EN-US"));
            return ((AdjustmentValue ?? 0) + columnValue);
        }
        #endregion

        #region construct/destruct
        public YearEndAdjustment()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(YearEndAdjustment data)
        {
            base.CopyTo(data);

            data.ColumnName = ColumnName;
            data.TableName = TableName;
            data.Id = Id;
            data.Description = Description;
            data.ColumnValue = ColumnValue;
            data.Visible = Visible;
            data.ModifyFlag = ModifyFlag;
            data.DataType = DataType;
            data.Precision = Precision;
            data.Scale = Scale;
            data.Minimum = Minimum;
            data.Maximum = Maximum;
            data.AdjustmentValue = AdjustmentValue;
        }


        public new void Clear()
        {
            base.Clear();

            ColumnName = null;
            TableName = null;
            Id = -1;
            Description = null;
            ColumnValue = "-1";
            Visible = true;
            ModifyFlag = true;
            DataType = null;
            Precision = 0;
            Scale = "0";
            Minimum = null;
            Maximum = null;
            AdjustmentValue = null;
        }
        #endregion
    }
}