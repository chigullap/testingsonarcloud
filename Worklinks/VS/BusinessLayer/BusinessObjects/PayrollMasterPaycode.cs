﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollMasterPaycode : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long PayrollMasterPaycodeId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public bool HasCurrentFlag { get; set; }

        [DataMember]
        public long PayrollMasterId { get; set; }

        [DataMember]
        public long PayrollProcessId { get; set; }

        [DataMember]
        public long PayrollPeriodId { get; set; }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public String CodePaycodeCd { get; set; }

        [DataMember]
        public Decimal Amount { get; set; }

        [DataMember]
        public Decimal UnitsInAmount { get; set; }

        [DataMember]
        public Decimal PeriodYear { get; set; }

        [DataMember]
        public Int16 ChequeMonth { get; set; }

        [DataMember]
        public Decimal MtdAmount { get; set; }

        [DataMember]
        public Decimal MtdUnitsInAmount { get; set; }

        [DataMember]
        public Decimal YtdAmount { get; set; }

        [DataMember]
        public Decimal YtdUnitsInAmount { get; set; }

        //for exports
        [DataMember]
        public String CodeCompanyCd { get; set; }

        [DataMember]
        public String EmployeeNumber { get; set; }

        [DataMember]
        public String ExternalCode { get; set; }
        #endregion

        #region construct/destruct
        public PayrollMasterPaycode()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PayrollMasterPaycode data)
        {
            base.CopyTo(data);

            data.PayrollMasterPaycodeId = PayrollMasterPaycodeId;
            data.HasCurrentFlag = HasCurrentFlag;
            data.PayrollMasterId = PayrollMasterId;
            data.PayrollProcessId = PayrollProcessId;
            data.PayrollPeriodId = PayrollPeriodId;
            data.EmployeeId = EmployeeId;
            data.CodePaycodeCd = CodePaycodeCd;
            data.Amount = Amount;
            data.UnitsInAmount = UnitsInAmount;
            data.PeriodYear = PeriodYear;
            data.ChequeMonth = ChequeMonth;
            data.MtdAmount = MtdAmount;
            data.MtdUnitsInAmount = MtdUnitsInAmount;
            data.YtdAmount = YtdAmount;
            data.YtdUnitsInAmount = YtdUnitsInAmount;

            //for exports
            data.CodeCompanyCd = CodeCompanyCd;
            data.EmployeeNumber = EmployeeNumber;
            data.ExternalCode = ExternalCode;
        }
        public new void Clear()
        {
            base.Clear();

            PayrollMasterPaycodeId = -1;
            HasCurrentFlag = false;
            PayrollMasterId = -1;
            PayrollProcessId = -1;
            PayrollPeriodId = -1;
            EmployeeId = -1;
            CodePaycodeCd = null;
            Amount = 0;
            UnitsInAmount = 0;
            PeriodYear = 0;
            ChequeMonth = 0;
            MtdAmount = 0;
            MtdUnitsInAmount = 0;
            YtdAmount = 0;
            YtdUnitsInAmount = 0;

            //for exports
            CodeCompanyCd = null;
            EmployeeNumber = null;
            ExternalCode = null;
        }
        #endregion
    }
}