﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class YearEndCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<YearEnd>
    {
    }
}