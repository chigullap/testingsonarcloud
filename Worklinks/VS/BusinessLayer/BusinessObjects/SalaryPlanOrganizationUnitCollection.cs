﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class SalaryPlanOrganizationUnitCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<SalaryPlanOrganizationUnit>
    {
    }
}