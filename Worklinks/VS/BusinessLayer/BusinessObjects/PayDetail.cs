﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayDetail : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {

        #region properties
        [DataMember]
        public long PayDetailId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long PayrollProcessId { get; set; }      
        [DataMember]
        public long EmployeeId { get; set; }      
        [DataMember]
        public String EmployeeNumber { get; set; }      
        [DataMember]
        public String ProvinceStateCode { get; set; }      
        [DataMember]
        public String PaycodeCode { get; set; }      
        [DataMember]
        public Decimal? CurrentAmount { get; set; }      
        [DataMember]
        public Decimal CurrentUnit { get; set; }      
        [DataMember]
        public Decimal? YearToDateAmount { get; set; }      
        [DataMember]
        public Decimal? YearToDateUnit { get; set; }
        [DataMember]
        public String ExternalCode { get; set; }
        [DataMember]
        public String EmployerNumber { get; set; }
        [DataMember]
        public Decimal? GrossAmount { get; set; }
        [DataMember]
        public DateTime ChequeDate { get; set; }

        #endregion

        #region construct/destruct
        public PayDetail()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PayDetail data)
        {
            base.CopyTo(data);

            data.PayDetailId = PayDetailId;
            data.PayrollProcessId = PayrollProcessId;
            data.EmployeeId = EmployeeId;
            data.EmployeeNumber = EmployeeNumber;
            data.ProvinceStateCode = ProvinceStateCode;
            data.PaycodeCode = PaycodeCode;
            data.CurrentAmount = CurrentAmount;
            data.CurrentUnit = CurrentUnit;
            data.YearToDateAmount = YearToDateAmount;
            data.YearToDateUnit = YearToDateUnit;
            data.ExternalCode = ExternalCode;
            data.EmployerNumber = EmployerNumber;
            data.GrossAmount = GrossAmount;
            data.ChequeDate = ChequeDate;
        }

        public new void Clear()
        {
            base.Clear();

            PayDetailId = -1;
            PayrollProcessId = -1;
            EmployeeId = -1;
            EmployeeNumber = null;
            ProvinceStateCode = null;
            PaycodeCode = null;
            CurrentAmount = null;
            CurrentUnit = 0;
            YearToDateAmount = null;
            YearToDateUnit = null;
            ExternalCode = null;
            EmployerNumber = null;
            GrossAmount = null;
            ChequeDate = DateTime.Now;
        }

        #endregion

    }
}
