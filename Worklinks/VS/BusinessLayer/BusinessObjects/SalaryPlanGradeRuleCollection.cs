﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class SalaryPlanGradeRuleCollection : DataItemCollection<SalaryPlanGradeRule>
    {
        public CodeCollection ToCodeCollection()
        {
            CodeCollection col = new CodeCollection();

            col.Load(this.OrderBy(p=> p.MinimumHour)
                .Select(p => new CodeObject
                {
                    Code = p.SalaryPlanGradeRuleId.ToString(),
                    Description = p.MinimumHour.ToString()
                }));

            return col;
        }
    }
}
