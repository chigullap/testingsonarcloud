﻿using System;
using System.Runtime.Serialization;
using WorkLinks.BusinessLayer.BusinessObjects.Import;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class PayrollTransactionImport : PayrollTransactionSummary, IEmployeeIdentifier, IPaycodeIdentifier
    {
        #region properties
        public String EmployeeImportExternalIdentifier
        {
            get { return ImportExternalIdentifier; }
            set { ImportExternalIdentifier = value; }
        }
        public String PaycodeCodeImportExternalIdentifier { get; set; }
        public String OrganizationUnitImportExternalIdentifier { get; set; }
        public Decimal AmountRateFactor { get; set; }
        public bool IncludeEmploymentInsuranceHoursFlag { get; set; }
        public bool UseSalaryStandardHoursFlag { get; set; }
        public bool AutoPopulateRateFlag { get; set; }
        public bool HasErrors { get; set; }
        public bool ErrorMessage { get; set; }
        public bool UnitsIsNull { get; set; }
        public String PayrollProcessGroupCode { get; set; }
        #endregion

        #region construct/destruct
        public PayrollTransactionImport(Xsd.PayrollTransaction item)
            : this()
        {
            EmployeeImportExternalIdentifier = item.EmployeeIdentifier;
            //data.PayrollBatchId = PayrollBatchId;
            TransactionDate = item.TransactionDate;
            PaycodeCodeImportExternalIdentifier = item.PayCode.CodeIdentifier ?? String.Empty;
            OrganizationUnitImportExternalIdentifier = item.OrganizationUnitImportExternalIdentifier;

            if (item.Units == null)
            {
                Units = 1;
                UnitsIsNull = true;
            }
            else
                Units = (Decimal)item.Units;

            FederalTaxAmount = item.FederalTaxAmount;
            ProvincialTaxAmount = item.ProvincialTaxAmount;

            TransactionStartTime = item.TransactionStartTime;
            TransactionEndTime = item.TransactionEndTime;

            Rate = item.Rate;
        }

        public PayrollTransactionImport(AdvantageTimeRecordDetail item, string paycodeCodeImportExternalIdentifier)
            : this()
        {
            EmployeeImportExternalIdentifier = item.EmployeeImportExternalIdentifier;
            TransactionDate = item.TransactionDate;
            PaycodeCodeImportExternalIdentifier = paycodeCodeImportExternalIdentifier;
            decimal? units = GetUnits(item, paycodeCodeImportExternalIdentifier);
            if (units == null)
            {
                Units = 1;
                UnitsIsNull = true;
            }
            else
                Units = (Decimal)units;

            Rate = null;
        }

        public PayrollTransactionImport()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(PayrollTransactionImport data)
        {
            base.CopyTo(data);

            data.EmployeeImportExternalIdentifier = EmployeeImportExternalIdentifier;
            data.PaycodeCodeImportExternalIdentifier = PaycodeCodeImportExternalIdentifier;
            data.OrganizationUnitImportExternalIdentifier = OrganizationUnitImportExternalIdentifier;
            data.AmountRateFactor = AmountRateFactor;
            data.IncludeEmploymentInsuranceHoursFlag = IncludeEmploymentInsuranceHoursFlag;
            data.UseSalaryStandardHoursFlag = UseSalaryStandardHoursFlag;
            data.AutoPopulateRateFlag = AutoPopulateRateFlag;
            data.HasErrors = HasErrors;
            data.UnitsIsNull = UnitsIsNull;
            data.PayrollProcessGroupCode = PayrollProcessGroupCode;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeeImportExternalIdentifier = null;
            PaycodeCodeImportExternalIdentifier = null;
            OrganizationUnitImportExternalIdentifier = null;
            AmountRateFactor = 1;
            IncludeEmploymentInsuranceHoursFlag = false;
            UseSalaryStandardHoursFlag = false;
            AutoPopulateRateFlag = false;
            HasErrors = false;
            UnitsIsNull = false;
            PayrollProcessGroupCode = null;
        }
        #endregion

        private decimal? GetUnits(AdvantageTimeRecordDetail item, string paycodeCodeImportExternalIdentifier)
        {
            decimal? units = null;


            for (int i = 0; i <= 50; i++)
            {
                string paycodeExternalName = "Timecode_" + i.ToString("00");

                if (paycodeCodeImportExternalIdentifier.Equals(paycodeExternalName))
                {
                    units = GetTimeCodeUnits(item, i);
                    break;
                }
            }

            return units;
        }


        private decimal? GetTimeCodeUnits(AdvantageTimeRecordDetail item, int i)
        {
            decimal? units = null;


            if (i == 0)
            {
                units = item.TimeCode00;
            }
            else if (i == 1)
            {
                units = item.TimeCode01;
            }
            else if (i == 2)
            {
                units = item.TimeCode02;
            }
            else if (i == 3)
            {
                units = item.TimeCode03;
            }
            else if (i == 4)
            {
                units = item.TimeCode04;
            }
            else if (i == 5)
            {
                units = item.TimeCode05;
            }
            else if (i == 6)
            {
                units = item.TimeCode06;
            }
            else if (i == 7)
            {
                units = item.TimeCode07;
            }
            else if (i == 8)
            {
                units = item.TimeCode08;
            }
            else if (i == 9)
            {
                units = item.TimeCode09;
            }
            else if (i == 10)
            {
                units = item.TimeCode10;
            }
            else if (i == 11)
            {
                units = item.TimeCode11;
            }
            else if (i == 12)
            {
                units = item.TimeCode12;
            }
            else if (i == 13)
            {
                units = item.TimeCode13;
            }
            else if (i == 14)
            {
                units = item.TimeCode14;
            }
            else if (i == 15)
            {
                units = item.TimeCode15;
            }
            else if (i == 16)
            {
                units = item.TimeCode16;
            }
            else if (i == 17)
            {
                units = item.TimeCode17;
            }
            else if (i == 18)
            {
                units = item.TimeCode18;
            }
            else if (i == 19)
            {
                units = item.TimeCode19;
            }
            else if (i == 20)
            {
                units = item.TimeCode20;
            }
            else if (i == 21)
            {
                units = item.TimeCode21;
            }
            else if (i == 22)
            {
                units = item.TimeCode22;
            }
            else if (i == 23)
            {
                units = item.TimeCode23;
            }
            else if (i == 24)
            {
                units = item.TimeCode24;
            }
            else if (i == 25)
            {
                units = item.TimeCode25;
            }
            else if (i == 26)
            {
                units = item.TimeCode26;
            }
            else if (i == 27)
            {
                units = item.TimeCode27;
            }
            else if (i == 28)
            {
                units = item.TimeCode28;
            }
            else if (i == 29)
            {
                units = item.TimeCode29;
            }
            else if (i == 30)
            {
                units = item.TimeCode30;
            }
            else if (i == 31)
            {
                units = item.TimeCode31;
            }
            else if (i == 32)
            {
                units = item.TimeCode32;
            }
            else if (i == 33)
            {
                units = item.TimeCode33;
            }
            else if (i == 34)
            {
                units = item.TimeCode34;
            }
            else if (i == 35)
            {
                units = item.TimeCode35;
            }
            else if (i == 36)
            {
                units = item.TimeCode36;
            }
            else if (i == 37)
            {
                units = item.TimeCode37;
            }
            else if (i == 38)
            {
                units = item.TimeCode38;
            }
            else if (i == 39)
            {
                units = item.TimeCode39;
            }
            else if (i == 40)
            {
                units = item.TimeCode40;
            }
            else if (i == 41)
            {
                units = item.TimeCode41;
            }
            else if (i == 42)
            {
                units = item.TimeCode42;
            }
            else if (i == 43)
            {
                units = item.TimeCode43;
            }
            else if (i == 44)
            {
                units = item.TimeCode44;
            }
            else if (i == 45)
            {
                units = item.TimeCode45;
            }
            else if (i == 46)
            {
                units = item.TimeCode46;
            }
            else if (i == 47)
            {
                units = item.TimeCode47;
            }
            else if (i == 48)
            {
                units = item.TimeCode48;
            }
            else if (i == 49)
            {
                units = item.TimeCode49;
            }
            else if (i == 50)
            {
                units = item.TimeCode50;
            }

            return units;
        }
    }
}