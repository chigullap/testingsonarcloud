﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;
using System.Xml.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using Newtonsoft.Json;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SecurityUserProfile : SecurityUserProfileBase
    {
        private Object _user = null;


    public String ProfileData
        {
            get
            {

                Newtonsoft.Json.Serialization.DefaultContractResolver contractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver
                {
                    NamingStrategy = new Newtonsoft.Json.Serialization.CamelCaseNamingStrategy()
                };

                return Newtonsoft.Json.JsonConvert.SerializeObject(ProfileCache, new JsonSerializerSettings() { ContractResolver = contractResolver, Formatting = Formatting.Indented });
            }
            set
            {
                if (value == null)
                    ProfileCache = new ProfileCache();
                else
                {
                    try
                    {
                        Newtonsoft.Json.Serialization.DefaultContractResolver contractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver
                        {
                            NamingStrategy = new Newtonsoft.Json.Serialization.CamelCaseNamingStrategy()
                        };

                        ProfileCache = Newtonsoft.Json.JsonConvert.DeserializeObject<ProfileCache>(value, new JsonSerializerSettings() { ContractResolver = contractResolver, Formatting = Formatting.Indented });
                    }
                    catch (Exception exc)
                    {
                        ProfileCache = new ProfileCache();
                    }
                }
            }

        }
    /*    public long SecurityUserId
        {
            get
            {
                if (UserIdentifier == null)
                    return -1;
                else
                    return Convert.ToInt64(UserIdentifier);
            }
        }*/
        public void Initialize()
        {
            ProfileCache = new ProfileCache();
        }

        public Object User
        {
            get
            {
                return _user;
            }
            set
            {
                _user = value;
            }
        }
    }
}
