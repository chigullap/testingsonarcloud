﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeMembership : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeMembershipId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public String EmployeeMembershipCode { get; set; }

        [DataMember]
        public String EmployeeAssociationCode { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? ExpiryDate { get; set; }

        [DataMember]
        public Decimal? EmployerCost { get; set; }

        [DataMember]
        public Decimal? EmployeeCost { get; set; }

        [DataMember]
        public bool JobRelatedFlag { get; set; }

        [DataMember]
        public bool JobRequiredFlag { get; set; }
        #endregion

        #region construct/destruct
        public EmployeeMembership()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeeMembership data)
        {
            base.CopyTo(data);
            data.EmployeeMembershipId = EmployeeMembershipId;
            data.EmployeeId = EmployeeId;
            data.EmployeeMembershipCode = EmployeeMembershipCode;
            data.EmployeeAssociationCode = EmployeeAssociationCode;
            data.StartDate = StartDate;
            data.ExpiryDate = ExpiryDate;
            data.EmployerCost = EmployerCost;
            data.EmployeeCost = EmployeeCost;
            data.JobRelatedFlag = JobRelatedFlag;
            data.JobRequiredFlag = JobRequiredFlag;
        }
        public new void Clear()
        {
            base.Clear();
            EmployeeMembershipId = -1;
            EmployeeId = -1;
            EmployeeMembershipCode = null;
            EmployeeAssociationCode = null;
            StartDate = DateTime.Now;
            ExpiryDate = DateTime.Now;
            EmployerCost = 0;
            EmployeeCost = 0;
            JobRelatedFlag = false;
            JobRequiredFlag = false;
        }
        #endregion
    }
}