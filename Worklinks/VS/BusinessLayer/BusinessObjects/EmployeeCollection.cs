﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class EmployeeCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<Employee>
    {
        public bool MeetsPayrollRequirements()
        {
            foreach (Employee emp in this)
            {
                if (!emp.MeetsPayrollRequirements())
                    return false;
            }
            return true;
        }

        public String GetPayrollRequirementsValidationInformation()
        {
            StringBuilder rtn = new StringBuilder();
            foreach (Employee emp in this)
            {
                if (!emp.MeetsPayrollRequirements())
                    rtn.AppendLine(emp.GetPayrollRequirementsValidationInformation());
            }

            return rtn.ToString();
        }

        public EmployeePositionCollection EmployeePositionCurrentPositionCollection
        {
            get
            {
                EmployeePositionCollection items = new EmployeePositionCollection();

                foreach (Employee emp in this)
                    items.Add(emp.CurrentEmployeePosition);
                return items;
            }
        }
    }
}
