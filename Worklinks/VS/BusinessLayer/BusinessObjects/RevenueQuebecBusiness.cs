﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class RevenueQuebecBusiness : BusinessObject
    {
        #region Properties

        [DataMember]
        public string BusinessId { get; set; }

        [DataMember]
        public string FileSequenceNumber { get; set; }

        #endregion


        #region construct/destruct

        public RevenueQuebecBusiness()
        {
            Clear();
        }

        #endregion


        #region clear/copy/clone

        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(RevenueQuebecBusiness data)
        {
            base.CopyTo(data);

            data.BusinessId = BusinessId;
            data.FileSequenceNumber = FileSequenceNumber;
        }

        public new void Clear()
        {
            base.Clear();

            BusinessId = "";
            FileSequenceNumber = null;
        }

        #endregion
    }
}
