﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeEmploymentInformation : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long EmployeeEmploymentInformationId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        [Required]
        public DateTime? HireDate { get; set; }

        [DataMember]
        public DateTime? TerminationDate { get; set; }

        [DataMember]
        public DateTime? RehireDate { get; set; }

        [DataMember]
        public DateTime? ProbationDate { get; set; }

        [DataMember]
        public DateTime? SeniorityDate { get; set; }

        [DataMember]
        public DateTime? LastReviewDate { get; set; }

        [DataMember]
        public DateTime? NextReviewDate { get; set; }

        [DataMember]
        public DateTime? IncreaseDate { get; set; }

        [DataMember]
        public DateTime? AnniversaryDate { get; set; }

        [DataMember]
        public Decimal? LunchDuration { get; set; }

        [DataMember]
        public Decimal? DaysPerWeek { get; set; }

        [DataMember]
        [Required]
        public String WSIBCode { get; set; }

        [DataMember]
        [Required]
        public String PayrollProcessGroupCode { get; set; }

        [DataMember]
        public bool ExcludeMassPayslipFileFlag { get; set; }

        [DataMember]
        public bool ExcludeMassPayslipPrintFlag { get; set; }

        [DataMember]
        public DateTime? PensionEffectiveDate { get; set; }

        [DataMember]
        public int? ProbationHour { get; set; }

        [DataMember]
        public string CodeProbationStatusCode { get; set; }

        [DataMember]
        public bool OverrideConcurrencyCheckFlag { get; set; }

        [DataMember]
        public EmployeeCustomFieldCollection EmployeeCustomFields { get; set; }

        public DateTime? LastHireDate { get { return RehireDate ?? HireDate; } }
        #endregion

        #region construct/destruct
        public EmployeeEmploymentInformation()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeeEmploymentInformation data)
        {
            base.CopyTo(data);

            data.EmployeeEmploymentInformationId = EmployeeEmploymentInformationId;
            data.EmployeeId = EmployeeId;
            data.HireDate = HireDate;
            data.TerminationDate = TerminationDate;
            data.RehireDate = RehireDate;
            data.ProbationDate = ProbationDate;
            data.SeniorityDate = SeniorityDate;
            data.LastReviewDate = LastReviewDate;
            data.NextReviewDate = NextReviewDate;
            data.IncreaseDate = IncreaseDate;
            data.AnniversaryDate = AnniversaryDate;
            data.LunchDuration = LunchDuration;
            data.DaysPerWeek = DaysPerWeek;
            data.PayrollProcessGroupCode = PayrollProcessGroupCode;
            data.WSIBCode = WSIBCode;
            data.ExcludeMassPayslipFileFlag = ExcludeMassPayslipFileFlag;
            data.ExcludeMassPayslipPrintFlag = ExcludeMassPayslipPrintFlag;
            data.PensionEffectiveDate = PensionEffectiveDate;
            data.ProbationHour = ProbationHour;
            data.CodeProbationStatusCode = CodeProbationStatusCode;
            data.OverrideConcurrencyCheckFlag = OverrideConcurrencyCheckFlag;

            if (EmployeeCustomFields != null)
            {
                EmployeeCustomFieldCollection newCollection = new EmployeeCustomFieldCollection();
                EmployeeCustomFields.CopyTo(newCollection);
                data.EmployeeCustomFields = newCollection;
            }
            else
                data.EmployeeCustomFields = null;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeeEmploymentInformationId = -1;
            EmployeeId = -1;
            HireDate = null;
            TerminationDate = null;
            RehireDate = null;
            ProbationDate = null;
            SeniorityDate = null;
            LastReviewDate = null;
            NextReviewDate = null;
            IncreaseDate = null;
            AnniversaryDate = null;
            LunchDuration = null;
            DaysPerWeek = null;
            PayrollProcessGroupCode = null;
            WSIBCode = null;
            ExcludeMassPayslipFileFlag = false;
            ExcludeMassPayslipPrintFlag = false;
            PensionEffectiveDate = null;
            ProbationHour = null;
            CodeProbationStatusCode = null;
            OverrideConcurrencyCheckFlag = false;
            EmployeeCustomFields = null;
        }
        #endregion
    }
}