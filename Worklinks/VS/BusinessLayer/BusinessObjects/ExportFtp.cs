﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class ExportFtp : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long ExportFtpId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public string ExportFtpTypeCode { get; set; }
        [DataMember]
        public string FtpName { get; set; }        
        [DataMember]
        public string UriName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Login { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public int Port { get; set; }
        [DataMember]
        public bool EnableSslFlag { get; set; }
        [DataMember]
        public bool BinaryModeFlag { get; set; }
        [DataMember]
        public bool PassiveModeFlag { get; set; }
        [DataMember]
        public bool HasChildrenFlag { get; set; }
        [DataMember]
        public String Directory { get; set; }
        [DataMember]
        public bool UnixPathFlag { get; set; }
        #endregion

        #region construct/destruct
        public ExportFtp()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(ExportFtp data)
        {
            base.CopyTo(data);

            data.ExportFtpId = ExportFtpId;
            data.ExportFtpTypeCode = ExportFtpTypeCode;
            data.FtpName = FtpName;
            data.UriName = UriName;
            data.Description = Description;
            data.Login = Login;
            data.Password = Password;
            data.Port = Port;
            data.EnableSslFlag = EnableSslFlag;
            data.BinaryModeFlag = BinaryModeFlag;
            data.PassiveModeFlag = PassiveModeFlag;
            data.HasChildrenFlag = HasChildrenFlag;
            data.Directory = Directory;
            data.UnixPathFlag = UnixPathFlag;
        }
        public new void Clear()
        {
            base.Clear();

            ExportFtpId = -1;
            ExportFtpTypeCode = null;
            FtpName = null;
            UriName = null;
            Description = null;
            Login = null;
            Password = null;
            Port = 1;
            EnableSslFlag = false;
            BinaryModeFlag = false;
            PassiveModeFlag = false;
            HasChildrenFlag = false;
            Directory = null;
            UnixPathFlag = true;
        }
        #endregion
    }
}