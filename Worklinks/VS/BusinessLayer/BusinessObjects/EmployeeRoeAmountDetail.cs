﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EmployeeRoeAmountDetail: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        
        #region properties
        [DataMember]
        public long EmployeeRoeAmountDetailId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public long EmployeeRoeAmountId { get; set; }
        [DataMember]
        public long PayrollPeriodId { get; set; }
        [DataMember]
        public Decimal InsurableEarningAmount { get; set; }
       
        #endregion

        #region construct/destruct
        public EmployeeRoeAmountDetail()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(EmployeeRoeAmountDetail data)
        {
            base.CopyTo(data);

            data.EmployeeRoeAmountDetailId = EmployeeRoeAmountDetailId;
            data.EmployeeRoeAmountId = EmployeeRoeAmountId;
            data.PayrollPeriodId = PayrollPeriodId;
            data.InsurableEarningAmount = InsurableEarningAmount;
        }
        public new void Clear()
        {
            base.Clear();

            EmployeeRoeAmountDetailId = -1;
            EmployeeRoeAmountId = -1;
            PayrollPeriodId = -1;
            InsurableEarningAmount = 0;
        }

        #endregion

    }
}
