﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class Job: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        
        #region properties
        [DataMember]
        public long JobId 
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }
        [DataMember]
        public String Description { get;set;}
        
        

        #endregion

        #region construct/destruct
        public Job()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void Copyto(Job data)
        {
            base.CopyTo(data);
            data.JobId = JobId;
            data.Description = Description;
        }

        public new void Clear()
        {
            base.Clear();
            JobId=-1;
            Description=null;
        }

        #endregion


    }
}
