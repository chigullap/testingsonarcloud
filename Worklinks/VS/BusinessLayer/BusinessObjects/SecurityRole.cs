﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class SecurityRole : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long SecurityRoleId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long SecurityApplicationId { get; set; }

        [DataMember]
        public int SystemFlag { get; set; }

        [DataMember]
        public String CodeSecurityRoleTypeCd { get; set; }

        [DataMember]
        public long? SystemRoleId { get; set; }

        [DataMember]
        public bool WorklinksAdministrationFlag { get; set; }
        #endregion

        #region construct/destruct
        public SecurityRole()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(SecurityRole data)
        {
            base.CopyTo(data);

            data.SecurityRoleId = SecurityRoleId;
            data.SecurityApplicationId = SecurityApplicationId;
            data.SystemFlag = SystemFlag;
            data.CodeSecurityRoleTypeCd = CodeSecurityRoleTypeCd;
            data.SystemRoleId = SystemRoleId;
            data.WorklinksAdministrationFlag = WorklinksAdministrationFlag;
        }

        public new void Clear()
        {
            base.Clear();

            SecurityRoleId = -1;
            SecurityApplicationId = -1;
            SystemFlag = 0;
            CodeSecurityRoleTypeCd = "";
            SystemRoleId = null;
            WorklinksAdministrationFlag = false;
        }
        #endregion
    }
}