﻿using System;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class YearEndReportsCriteria
    {
        #region properties
        public string ReportName { get; set; }
        public string ReportType { get; set; }
        public string YearEndT4Ids { get; set; }
        public string Year { get; set; }
        #endregion

        #region construct/destruct
        public YearEndReportsCriteria()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public void Clear()
        {
            ReportName = null;
            ReportType = null;
            YearEndT4Ids = null;
            Year = null;
        }
        #endregion
    }
}