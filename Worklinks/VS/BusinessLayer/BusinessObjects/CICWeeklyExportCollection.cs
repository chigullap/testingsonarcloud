﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [CollectionDataContract]
    public class CICWeeklyExportCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<CICWeeklyExport>
    {
    }
}
