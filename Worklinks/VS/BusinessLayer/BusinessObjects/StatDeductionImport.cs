﻿using FileHelpers;
using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    [DelimitedRecord(",")]
    public class StatDeductionImport
    {
        [DataMember]
        public String EmployeeIdentifier;

        [DataMember]
        public String EmployerNumber;

        [DataMember]
        public String CodeProvinceStateCd;

        [DataMember]
        [FieldConverter(ConverterKind.Boolean)]
        public bool PayEmploymentInsuranceFlag;

        [DataMember]
        [FieldConverter(ConverterKind.Boolean)]
        public bool PayCanadaPensionPlanFlag;

        [DataMember]
        [FieldConverter(ConverterKind.Boolean)]
        public bool ParentalInsurancePlanFlag;

        [DataMember]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public Decimal FederalTaxClaim;

        [DataMember]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public Decimal FederalAdditionalTax;

        [DataMember]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public Decimal FederalDesignatedAreaDeduction;

        [DataMember]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public Decimal FederalAuthorizedAnnualDeduction;

        [DataMember]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public Decimal FederalAuthorizedAnnualTaxCredit;

        [DataMember]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public Decimal FederalLabourTaxCredit;

        [DataMember]
        [FieldConverter(ConverterKind.Boolean)]
        public bool FederalPayTaxFlag;

        [DataMember]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public Decimal ProvincialTaxClaim;

        [DataMember]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public Decimal ProvincialAdditionalTax;

        [DataMember]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public Decimal ProvincialDesignatedAreaDeduction;

        [DataMember]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public Decimal ProvincialAuthorizedAnnualDeduction;

        [DataMember]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public Decimal ProvincialAuthorizedAnnualTaxCredit;

        [DataMember]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public Decimal ProvincialLabourTaxCredit;

        [DataMember]
        [FieldConverter(ConverterKind.Boolean)]
        public bool ProvincialPayTaxFlag;

        [DataMember]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public Decimal EstimatedAnnualIncome;

        [DataMember]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public Decimal EstimatedAnnualExpense;

        [DataMember]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public Decimal EstimatedNetIncome;

        [DataMember]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public Decimal CommissionPercentage;
    }
}