﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class AccrualPolicySearch : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long AccrualPolicyId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public long AccrualPolicyDescriptionId { get; set; }

        [DataMember]
        public String AccrualPolicyDescriptionField { get; set; }
        #endregion

        #region construct/destruct
        public AccrualPolicySearch()
        {
            Clear();
        }
        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(AccrualPolicySearch data)
        {
            base.CopyTo(data);

            data.AccrualPolicyId = AccrualPolicyId;
            data.AccrualPolicyDescriptionId = AccrualPolicyDescriptionId;
            data.AccrualPolicyDescriptionField = AccrualPolicyDescriptionField;
        }
        public new void Clear()
        {
            base.Clear();

            AccrualPolicyId = -1;
            AccrualPolicyDescriptionId = -1;
            AccrualPolicyDescriptionField = "";
        }
        #endregion
    }
}