﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Central
{
    [Serializable]
    [DataContract]
    public class RevenuQuebecRemittanceDatabaseMap : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long RevenuQuebecRemittanceDatabaseMapId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String LogicalIdPrefix { get; set; }
        [DataMember]
        public String DatabaseName { get; set; }

        #endregion

        #region construct/destruct
        public RevenuQuebecRemittanceDatabaseMap()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(RevenuQuebecRemittanceDatabaseMap data)
        {
            base.CopyTo(data);
            data.RevenuQuebecRemittanceDatabaseMapId = RevenuQuebecRemittanceDatabaseMapId;
            data.LogicalIdPrefix = LogicalIdPrefix;
            data.DatabaseName = DatabaseName;
        }

        public new void Clear()
        {
            base.Clear();
            RevenuQuebecRemittanceDatabaseMapId = -1;
            LogicalIdPrefix = null;
            DatabaseName = null;
        }

        #endregion


    }
}
