﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Central
{
    [Serializable]
    [CollectionDataContract]
    public class AutoCalculatePayrollDatabaseMapCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<AutoCalculatePayrollDatabaseMap>
    {
    }
}
