﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Central
{
    [Serializable]
    [CollectionDataContract]
    public class HrxmlDatabaseMapCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<HrxmlDatabaseMap>
    {
        public String GetDataBaseName(String logicalId, String referenceIdNumber)
        {
            HrxmlDatabaseMapCollection mapping = GetLogicalIdMapping(logicalId);

            //try to map on referenceIdNumber
            foreach (HrxmlDatabaseMap map in mapping)
            {
                if (map.ReferenceIdNumber!=null && map.ReferenceIdNumber.Equals(referenceIdNumber))
                    return map.DatabaseName;
            }

            //still here no mapping, get the null mapping
            foreach (HrxmlDatabaseMap map in mapping)
            {
                if (map.ReferenceIdNumber == null)
                    return map.DatabaseName;
            }

            return null;
        }

        public HrxmlDatabaseMapCollection GetLogicalIdMapping(String logicalId)
        {
            HrxmlDatabaseMapCollection rtn = new HrxmlDatabaseMapCollection();
            foreach (HrxmlDatabaseMap map in this)
            {
                if (logicalId.StartsWith(map.LogicalIdPrefix))
                {
                    rtn.Add(map);
                }
            }

            return rtn;
        }
    }

}
