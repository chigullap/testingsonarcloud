﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Central
{
    [Serializable]
    [CollectionDataContract]
    public class DirectDepositDatabaseMapCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<DirectDepositDatabaseMap>
    {
    }
}
