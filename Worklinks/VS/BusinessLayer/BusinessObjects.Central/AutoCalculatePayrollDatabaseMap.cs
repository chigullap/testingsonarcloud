﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Central
{
    [Serializable]
    [DataContract]
    public class AutoCalculatePayrollDatabaseMap : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long AutoCalculatePayrollDatabaseMapId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String LogicalIdPrefix { get; set; }
        [DataMember]
        public String DatabaseName { get; set; }

        #endregion

        #region construct/destruct
        public AutoCalculatePayrollDatabaseMap()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(AutoCalculatePayrollDatabaseMap data)
        {
            base.CopyTo(data);
            data.AutoCalculatePayrollDatabaseMapId = AutoCalculatePayrollDatabaseMapId;
            data.LogicalIdPrefix = LogicalIdPrefix;
            data.DatabaseName = DatabaseName;
        }

        public new void Clear()
        {
            base.Clear();
            AutoCalculatePayrollDatabaseMapId = -1;
            LogicalIdPrefix = null;
            DatabaseName = null;
        }

        #endregion


    }
}
