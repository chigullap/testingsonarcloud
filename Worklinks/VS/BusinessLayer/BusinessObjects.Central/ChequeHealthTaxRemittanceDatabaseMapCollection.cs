﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Central
{
    [Serializable]
    [CollectionDataContract]
    public class ChequeHealthTaxRemittanceDatabaseMapCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<ChequeHealthTaxRemittanceDatabaseMap>
    {
    }
}
