﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Central
{
    [Serializable]
    [DataContract]
    public class ChequeHealthTaxRemittanceDatabaseMap : WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
        #region properties
        [DataMember]
        public long ChequeHealthTaxRemittanceDatabaseMapId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String LogicalIdPrefix { get; set; }
        [DataMember]
        public String DatabaseName { get; set; }

        #endregion

        #region construct/destruct
        public ChequeHealthTaxRemittanceDatabaseMap()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(ChequeHealthTaxRemittanceDatabaseMap data)
        {
            base.CopyTo(data);
            data.ChequeHealthTaxRemittanceDatabaseMapId = ChequeHealthTaxRemittanceDatabaseMapId;
            data.LogicalIdPrefix = LogicalIdPrefix;
            data.DatabaseName = DatabaseName;
        }

        public new void Clear()
        {
            base.Clear();
            ChequeHealthTaxRemittanceDatabaseMapId = -1;
            LogicalIdPrefix = null;
            DatabaseName = null;
        }

        #endregion
    }
}
