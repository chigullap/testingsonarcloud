﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Central
{
    [Serializable]
    [CollectionDataContract]
    public class CraRemittanceDatabaseMapCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<CraRemittanceDatabaseMap>
    {
    }
}
