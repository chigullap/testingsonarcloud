﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Central
{
    [Serializable]
    [CollectionDataContract]
    public class ChequeWcbRemittanceDatabaseMapCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<ChequeWcbRemittanceDatabaseMap>
    {
    }
}
