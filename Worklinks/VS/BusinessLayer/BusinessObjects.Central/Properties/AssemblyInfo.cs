﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("WorkLinks.BusinessLayer.BusinessObjects.Central")]
[assembly: AssemblyDescription("WorkLinks central data objects.")]
#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif
[assembly: AssemblyCompany("WorkLinks")]
[assembly: AssemblyProduct("WorkLinks.BusinessLayer.BusinessObjects.Central")]
[assembly: AssemblyCopyright("Copyright © WorkLinks 2019. All Rights Reserved")]
[assembly: AssemblyTrademark("Human Capital Management Systems")]
[assembly: AssemblyCulture("")]


// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ab5cac31-3a4b-40c1-a9c2-c2e1169cc2d9")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion(WorkLinks.BusinessLayer.BusinessObjects.All.Version.VersionNumber)]
[assembly: AssemblyFileVersion(WorkLinks.BusinessLayer.BusinessObjects.All.Version.VersionNumber)]
