﻿using System;
using System.Runtime.Serialization;

namespace WorkLinks.BusinessLayer.BusinessObjects.Central
{
    [Serializable]
    [CollectionDataContract]
    public class RevenuQuebecRemittanceDatabaseMapCollection : WLP.BusinessLayer.BusinessObjects.DataItemCollection<RevenuQuebecRemittanceDatabaseMap>
    {
    }
}
