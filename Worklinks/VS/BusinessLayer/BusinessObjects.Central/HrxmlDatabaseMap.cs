﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using WLP.BusinessLayer.BusinessObjects;

namespace WorkLinks.BusinessLayer.BusinessObjects.Central
{
    [Serializable]
    [DataContract]
    public class HrxmlDatabaseMap: WLP.BusinessLayer.BusinessObjects.BusinessObject
    {
      
        #region properties
        [DataMember]
        public long HrxmlDatabaseMapId
        {
            get { return (long)_Key; }
            set { _Key = value; }
        }

        [DataMember]
        public String LogicalIdPrefix { get; set; }
        [DataMember]
        public String ReferenceIdNumber { get; set; }
        [DataMember]
        public String DatabaseName { get; set; }
        

        #endregion

        #region construct/destruct
        public HrxmlDatabaseMap()
        {
            Clear();
        }

        #endregion

        #region clear/copy/clone
        public override object Clone()
        {
            throw new NotImplementedException();
        }
        public void CopyTo(HrxmlDatabaseMap data)
        {
            base.CopyTo(data);
            data.HrxmlDatabaseMapId = HrxmlDatabaseMapId;
            data.LogicalIdPrefix = LogicalIdPrefix;
            data.ReferenceIdNumber = ReferenceIdNumber;
            data.DatabaseName = DatabaseName;
        }

        public new void Clear()
        {
            base.Clear();
            HrxmlDatabaseMapId = -1;
            LogicalIdPrefix = null;
            ReferenceIdNumber = null;
            DatabaseName = null;
        }

        #endregion


    }
}
