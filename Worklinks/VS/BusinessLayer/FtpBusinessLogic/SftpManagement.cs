﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using System.IO;
using System.Net;
using Renci.SshNet.Sftp;

namespace WorkLinks.BusinessLayer.BusinessLogic.FTP
{
    public class SftpManagement
    {
        #region SFTP

        public SortedDictionary<string, byte[]> DownloadSftpFile(string host, int port, string directory, bool unixPathFlag, string username, string password, string patternToFind, string patternToFindExtension = null)
        {
            string path = CreatePath(directory, "", unixPathFlag);

            using (var sftp = new SftpClient(host, port, username, password))
            {
                sftp.Connect();

                SortedDictionary<string, byte[]> outputFiles = new SortedDictionary<string, byte[]>();

                //get list of files in the directory
                IEnumerable<SftpFile> fileList = sftp.ListDirectory(path);

                try
                {
                    foreach (SftpFile file in fileList)
                    {
                        //check if file matches the pattern supplied.  If extension is supplied, filter on that as well.  If not supplied, it's ignored with the 'true' condition
                        if (file.Name.Contains(patternToFind) && (patternToFindExtension != null ? file.Name.EndsWith(patternToFindExtension) : true))
                        {
                            //open file for reading
                            using (MemoryStream ms = new MemoryStream())
                            {
                                //download the file
                                sftp.DownloadFile(CreatePath(path, file.Name, unixPathFlag), ms);
                                //store file(s)
                                outputFiles.Add(file.Name, ms.ToArray());
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    sftp.Disconnect();
                }

                return outputFiles;
            }
        }
        public void DeleteSftpFile(string host, int port, string directory, bool unixPathFlag, string username, string password, string fileNameToDelete)
        {
            string path = CreatePath(directory, fileNameToDelete, unixPathFlag);

            using (var sftp = new SftpClient(host, port, username, password))
            {
                sftp.Connect();

                try
                {
                    //check if file exists
                    if (sftp.Exists(path))
                    {
                        //delete the file
                        sftp.DeleteFile(path);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    sftp.Disconnect();
                }
            }
        }

        public void MoveSftpFile(string host, int port, string directory, bool unixPathFlag, string username, string password, string fileNameToDelete, string folderToMoveProcessedFiles)
        {
            string originalPath = CreatePath(directory, fileNameToDelete, unixPathFlag);
            string processedPath = CreatePath(folderToMoveProcessedFiles, fileNameToDelete, unixPathFlag);

            using (var sftp = new SftpClient(host, port, username, password))
            {
                sftp.Connect();

                try
                {
                    //get the file
                    SftpFile processedFile = sftp.Get(originalPath);

                    //move the file
                    processedFile.MoveTo(processedPath);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    sftp.Disconnect();
                }
            }
        }

        public void UploadSftpFile(string host, int port, string directory, bool unixPathFlag, string username, string password, byte[] file, string fileName)
        {
            string path = CreatePath(directory, fileName, unixPathFlag);

            //upload a file
            using (var sftp = new SftpClient(host, port, username, password))
            {
                sftp.Connect();

                using (MemoryStream uploadFile = new MemoryStream(file))
                {
                    sftp.UploadFile(uploadFile, path);
                }

                sftp.Disconnect();
            }
        }

        private string CreatePath(string directory, string fileName, bool unixPathFlag)
        {
            string path = "";

            if (directory != null && directory.Length > 0)
            {
                path = Path.Combine(directory, fileName);
                if (unixPathFlag)
                    path = path.Replace(@"\", @"/");                //path structure of /blah/blah/filename.txt for unix_path_flag=true
                else
                    path = path.Replace(@"/", @"\");                //path structure of \blah\blah\filename.txt for unix_path_flag=0
            }
            else
                path = fileName;

            return path;
        }

        #endregion

        #region FTP

        public void UploadFtpsFile(string fileName, byte[] fileContents, string uriName, string login, string password, bool enableSsl, bool useBinary, bool usePassive)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(uriName + @"/" + fileName);
            request.Credentials = new NetworkCredential(login, password);
            request.EnableSsl = true;
            request.UseBinary = true;
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.UsePassive = true;

            request.ContentLength = fileContents.Length;
            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(fileContents, 0, fileContents.Length);
            }
        }

        public void DownloadAllFtpsFilesAndArchive(string uriName, string login, string password, string destinationPath, string archiveFolderName)
        {
            //setup ftp info
            FtpWebRequest ftpRequest = CreateRequest(uriName, login, password, WebRequestMethods.Ftp.ListDirectory);

            //setup response
            FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse();
            StreamReader streamReader = new StreamReader(response.GetResponseStream());

            //store filenames
            List<string> directories = new List<string>();

            //get list of directory contents
            string line = streamReader.ReadLine();
            while (!string.IsNullOrEmpty(line))
            {
                directories.Add(line);
                line = streamReader.ReadLine();
            }
            streamReader.Close();

            //download the files in the directory and move to the archive folder
            using (WebClient ftpClient = new WebClient())
            {
                ftpClient.Credentials = new NetworkCredential(login, password);

                for (int i = 0; i <= directories.Count - 1; i++)
                {
                    if (directories[i].Contains("."))
                    {
                        string currentFilePath = uriName + "//" + directories[i].ToString();
                        string destinationFilePath = destinationPath + "\\" + directories[i].ToString();

                        //download file from FTP to server
                        ftpClient.DownloadFile(currentFilePath, destinationFilePath);
                                                
                        string archivePath = archiveFolderName + "//" + directories[i].ToString();

                        try
                        {
                            MoveFileToArchive(login, password, currentFilePath, archivePath);
                        }
                        catch (Exception ex)
                        {
                            //filename already exists in Archive folder.  Delete old file and move new one.
                            if (ex.Message.ToLower().Contains("file unavailable"))
                            {
                                //delete existing file
                                FtpWebRequest ftpMoveRequest = CreateRequest(uriName + "//" + archivePath, login, password, WebRequestMethods.Ftp.DeleteFile);
                                ftpMoveRequest.GetResponse().Close();

                                //replace with the current file
                                MoveFileToArchive(login, password, currentFilePath, archivePath);
                            }
                        }
                    }
                }
            }
        }

        private void MoveFileToArchive(string login, string password, string path, string archivePath)
        {
            //move the files on the FTP to the FTP Archive directory
            FtpWebRequest ftpMoveRequest = CreateRequest(path, login, password, WebRequestMethods.Ftp.Rename);
            ftpMoveRequest.RenameTo = archivePath;
            ftpMoveRequest.GetResponse().Close();
        }

        private FtpWebRequest CreateRequest(string uriName, string login, string password, string method)
        {
            FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(uriName);
            ftpRequest.Credentials = new NetworkCredential(login, password);
            ftpRequest.Method = method;
            ftpRequest.EnableSsl = true;

            return ftpRequest;
        }

        #endregion
    }
}
