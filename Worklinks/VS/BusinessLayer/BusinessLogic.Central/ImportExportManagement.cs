﻿using System;
using System.Collections.Generic;
using System.Text;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;
using WorkLinks.DataLayer.DataAccess;
using WorkLinks.DataLayer.DataAccess.Central;

namespace WorkLinks.BusinessLayer.BusinessLogic.Central
{
    public class ImportExportManagement
    {
        //add data access
        ImportExportAccess _importExportAccess = new ImportExportAccess();
        CodeManagement _codeManagement = new CodeManagement();

        public ImportExportAccess ImportExportAccess
        {
            get
            {
                if (_importExportAccess == null)
                    _importExportAccess = new ImportExportAccess();
                return _importExportAccess;
            }
        }



        #region hrxml
        public HrxmlDatabaseMapCollection GetHrxmlDatabaseMap(DatabaseUser user)
        {
            try
            {
                return ImportExportAccess.GetHrxmlDatabaseMap(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ImportExportManagement - GetHrxmlDatabaseMap", ex);
                throw;
            }
        }

        public String GetSystemCodeDescription(String databaseName, String code)
        {
            return _codeManagement.GetCodeTable(databaseName,"EN",CodeTableType.System,code)[0].Description;
        }

        #endregion


    }
}

