﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;
using WorkLinks.DataLayer.DataAccess.Central;

namespace WorkLinks.BusinessLayer.BusinessLogic.Central
{
    public class ChequeWcbRemittanceManagement
    {
        //add data access
        private ChequeWcbRemittanceAccess _chequeWcbRemittanceAccess = null;

        public ChequeWcbRemittanceAccess ChequeWcbRemittanceAccess
        {
            get
            {
                if (_chequeWcbRemittanceAccess == null)
                    _chequeWcbRemittanceAccess = new ChequeWcbRemittanceAccess();
                return _chequeWcbRemittanceAccess;
            }
        }

        #region methods
        public ChequeWcbRemittanceDatabaseMapCollection GetChequeWcbRemittanceDatabaseMap(DatabaseUser user)
        {
            try
            {
                return ChequeWcbRemittanceAccess.GetChequeWcbRemittanceDatabaseMap(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ChequeWcbRemittanceManagement - GetChequeWcbRemittanceDatabaseMap", ex);
                throw;
            }
        }

        #endregion

    }
}
