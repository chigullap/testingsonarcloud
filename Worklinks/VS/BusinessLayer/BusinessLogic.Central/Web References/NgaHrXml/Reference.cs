﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace WorkLinks.BusinessLayer.BusinessLogic.Central.NgaHrXml {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1586.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="nga_pex_ExternalInbound_webservices_provider_retrieveBackEndBod_Binder", Namespace="http://pyxwmdap01.pyx.erp/nga.pex.ExternalInbound.webservices.provider:retrieveBa" +
        "ckEndBod")]
    public partial class ngapexExternalInboundwebservicesproviderretrieveBackEndBod : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback retrieveBackendBODOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public ngapexExternalInboundwebservicesproviderretrieveBackEndBod() {
            this.Url = global::WorkLinks.BusinessLayer.BusinessLogic.Central.Properties.Settings.Default.BusinessLogic_Central_NgaHrXml_nga_pex_ExternalInbound_webservices_provider_retrieveBackEndBod;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event retrieveBackendBODCompletedEventHandler retrieveBackendBODCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("nga_pex_ExternalInbound_webservices_provider_retrieveBackEndBod_Binder_retrieveBa" +
            "ckendBOD", RequestNamespace="http://pyxwmdap01.pyx.erp/nga.pex.ExternalInbound.webservices.provider:retrieveBa" +
            "ckEndBod", ResponseNamespace="http://pyxwmdap01.pyx.erp/nga.pex.ExternalInbound.webservices.provider:retrieveBa" +
            "ckEndBod", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("retrieveBackendBODResponse", Namespace="http://www.ngahr.com/NGA_Pex_ExternalInbound/")]
        public retrieveBackendBODResponse retrieveBackendBOD([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.ngahr.com/NGA_Pex_ExternalInbound/")] retrieveBackendBODRequest retrieveBackendBODRequest) {
            object[] results = this.Invoke("retrieveBackendBOD", new object[] {
                        retrieveBackendBODRequest});
            return ((retrieveBackendBODResponse)(results[0]));
        }
        
        /// <remarks/>
        public void retrieveBackendBODAsync(retrieveBackendBODRequest retrieveBackendBODRequest) {
            this.retrieveBackendBODAsync(retrieveBackendBODRequest, null);
        }
        
        /// <remarks/>
        public void retrieveBackendBODAsync(retrieveBackendBODRequest retrieveBackendBODRequest, object userState) {
            if ((this.retrieveBackendBODOperationCompleted == null)) {
                this.retrieveBackendBODOperationCompleted = new System.Threading.SendOrPostCallback(this.OnretrieveBackendBODOperationCompleted);
            }
            this.InvokeAsync("retrieveBackendBOD", new object[] {
                        retrieveBackendBODRequest}, this.retrieveBackendBODOperationCompleted, userState);
        }
        
        private void OnretrieveBackendBODOperationCompleted(object arg) {
            if ((this.retrieveBackendBODCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.retrieveBackendBODCompleted(this, new retrieveBackendBODCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.ngahr.com/NGA_Pex_ExternalInbound/")]
    public partial class retrieveBackendBODRequest {
        
        private string xmlInField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string xmlIn {
            get {
                return this.xmlInField;
            }
            set {
                this.xmlInField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.ngahr.com/NGA_Pex_ExternalInbound/")]
    public partial class retrieveBackendBODResponse {
        
        private string xmlOutField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string xmlOut {
            get {
                return this.xmlOutField;
            }
            set {
                this.xmlOutField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1586.0")]
    public delegate void retrieveBackendBODCompletedEventHandler(object sender, retrieveBackendBODCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1586.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class retrieveBackendBODCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal retrieveBackendBODCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public retrieveBackendBODResponse Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((retrieveBackendBODResponse)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591