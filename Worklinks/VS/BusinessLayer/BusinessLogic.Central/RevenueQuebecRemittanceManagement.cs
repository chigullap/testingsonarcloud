﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;
using WorkLinks.DataLayer.DataAccess.Central;

namespace WorkLinks.BusinessLayer.BusinessLogic.Central
{
    public class RevenuQuebecRemittanceManagement
    {
        //add data access
        private RevenuQuebecRemittanceAccess _rqRemittanceAccess = null;

        public RevenuQuebecRemittanceAccess RqRemittanceAccess
        {
            get
            {
                if (_rqRemittanceAccess == null)
                    _rqRemittanceAccess = new RevenuQuebecRemittanceAccess();
                return _rqRemittanceAccess;
            }
        }

        #region methods
        public RevenuQuebecRemittanceDatabaseMapCollection GetRevenuQuebecRemittanceDatabaseMap(DatabaseUser user)
               
        {
            try
            {
                return RqRemittanceAccess.GetRevenuQuebecRemittanceDatabaseMap(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "RevenuQuebecRemittanceManagement - GetRevenuQuebecRemittanceDatabaseMap", ex);
                throw;
            }
        }

        #endregion

    }
}

