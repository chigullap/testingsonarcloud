﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;
using WorkLinks.DataLayer.DataAccess.Central;

namespace WorkLinks.BusinessLayer.BusinessLogic.Central
{
    public class CraRemittanceManagement
    {
        //add data access
        private CraRemittanceAccess _craRemittanceAccess = null;

        public CraRemittanceAccess CraRemittanceAccess
        {
            get
            {
                if (_craRemittanceAccess == null)
                    _craRemittanceAccess = new CraRemittanceAccess();
                return _craRemittanceAccess;
            }
        }

        #region methods
        public CraRemittanceDatabaseMapCollection GetCraRemittanceDatabaseMap(DatabaseUser user)
        {
            try
            {
                return CraRemittanceAccess.GetCraRemittanceDatabaseMap(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "CraRemittanceManagement - GetCraRemittanceDatabaseMap", ex);
                throw;
            }
        }

        #endregion

    }
}

