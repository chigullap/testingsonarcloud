﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Transactions;
using System.Xml;
using System.Xml.Serialization;
using NLog;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;
using WorkLinks.BusinessLayer.BusinessObjects.Xsd;
using WorkLinks.DataLayer.DataAccess.Central;

namespace WorkLinks.BusinessLayer.BusinessLogic.Central
{
    public class QueueManagement : IDisposable
    {
        public QueueManagement(DatabaseUser user, string importQueueName, string exportQueueName, string outputDirectoryPath, string importExportProcessingDirectory,
            bool disableNgaResponse, string ngaHrxmlConfirmationUrl, string ngaHrxmlConfirmationLogin, string ngaHrxmlConfirmationPassword,
            bool hrmlAddressEnabledFlag, bool hrmlBankingEnabledFlag, bool hrmlPayrollBatchEnabledFlag, bool hrxmlBiographicalEnabledFlag,
            bool hrxmlEmploymentInformationEnabledFlag, bool hrmlPaycodeEnabledFlag, bool hrmlPositionEnabledFlag, bool hrmlEmailEnabledFlag,
            bool hrmlRehireEnabledFlag, bool hrmlStatutoryDeductionEnabledFlag, bool hrmlPhoneEnabledFlag, bool hrmlTerminationEnabledFlag,
            bool hrxmlLeaveEnabledFlag, bool overrideQueue = false, ILogger logger = null)
        {
            _importQueueName = importQueueName;
            _exportQueueName = exportQueueName;
            _outputDirectoryPath = outputDirectoryPath;
            _importExportProcessingDirectory = importExportProcessingDirectory;
            _user = user;
            _disableNgaResponse = disableNgaResponse;

            _ngaHrxmlConfirmationUrl = ngaHrxmlConfirmationUrl;
            _ngaHrxmlConfirmationLogin = ngaHrxmlConfirmationLogin;
            _ngaHrxmlConfirmationPassword = ngaHrxmlConfirmationPassword;

            _hrmlAddressEnabledFlag = hrmlAddressEnabledFlag;
            _hrmlBankingEnabledFlag = hrmlBankingEnabledFlag;
            _hrmlPayrollBatchEnabledFlag = hrmlPayrollBatchEnabledFlag;
            _hrxmlBiographicalEnabledFlag = hrxmlBiographicalEnabledFlag;
            _hrxmlEmploymentInformationEnabledFlag = hrxmlEmploymentInformationEnabledFlag;
            _hrmlPaycodeEnabledFlag = hrmlPaycodeEnabledFlag;
            _hrmlPositionEnabledFlag = hrmlPositionEnabledFlag;
            _hrmlEmailEnabledFlag = hrmlEmailEnabledFlag;
            _hrmlRehireEnabledFlag = hrmlRehireEnabledFlag;
            _hrmlStatutoryDeductionEnabledFlag = hrmlStatutoryDeductionEnabledFlag;
            _hrmlPhoneEnabledFlag = hrmlPhoneEnabledFlag;
            _hrmlTerminationEnabledFlag = hrmlTerminationEnabledFlag;
            _hrxmlLeaveEnabledFlag = hrxmlLeaveEnabledFlag;
            _overrideQueue = overrideQueue;

            _logger = logger ?? LogManager.GetCurrentClassLogger(typeof(QueueManagement));

            //start queue listeners
            if (!overrideQueue)
            {
                _logger.Info($"Starting peek operation on import queue. User={System.Security.Principal.WindowsIdentity.GetCurrent().Name}");
                try
                {
                    ImportQueue.BeginPeek();
                }
                catch (Exception e)
                {
                    _logger.Error($"Error starting the peek operation on the import queue: {e}");
                    throw;
                }
                
                _logger.Info($"Starting peek operation on export queue. User={System.Security.Principal.WindowsIdentity.GetCurrent().Name}");
                try
                {
                    ExportQueue.BeginPeek();
                }
                catch (Exception e)
                {
                    _logger.Error($"Error starting the peek operation on the export queue: {e}");
                    throw;
                }
            }
        }

        public void Dispose()
        {
            _logger.Info("Dispose called.");
            ImportQueue.Close();
            ImportQueue.Dispose();
            ExportQueue.Close();
            ExportQueue.Dispose();
        }

        private void _importQueue_PeekCompleted(object sender, PeekCompletedEventArgs e)
        {
            MessageQueue queue = (MessageQueue) sender;

            using (MessageQueueTransaction msmqTransaction = new MessageQueueTransaction())
            {
                try
                {
                    msmqTransaction.Begin();
                    queue.Formatter = new XmlMessageFormatter(new[] {typeof(RemoteImportExportLog)});
                    _logger.Info("Attempting to receive message from the import queue...");
                    Message message = queue.Receive(msmqTransaction);

                    RemoteImportExportLog log = (RemoteImportExportLog) message.Body;

                    _logger.Info("Attempting to load the received message...");
                    LoadFile(_user, log, _outputDirectoryPath, _importExportProcessingDirectory);
                    _logger.Info("Message loaded successfully.");
                    msmqTransaction.Commit();
                }
                catch (Exception exc)
                {
                    _logger.Error($"Error during the import queue peek operation: {exc}");
                    ErrorManagement.HandleError(_user, "_importQueue_PeekCompleted", exc);
                    try
                    {
                        msmqTransaction.Abort();
                    }
                    catch
                    {
                    }

                    //wait a bit to not overrun the logs
                    System.Threading.Thread.Sleep(60 * 1000);
                }
                finally
                {
                    ImportQueue.BeginPeek();
                }
            }
        }

        private void _exportQueue_PeekCompleted(object sender, PeekCompletedEventArgs e)
        {
            MessageQueue queue = (MessageQueue) sender;

            using (MessageQueueTransaction msmqTransaction = new MessageQueueTransaction())
            {
                try
                {
                    msmqTransaction.Begin();

                    queue.Formatter = new XmlMessageFormatter(new[] {typeof(ImportExportLog)});

                    _logger.Info("Receiving message...");
                    Message message = queue.Receive(msmqTransaction);
                    _logger.Info("Message received.");
                    ImportExportLog log = (ImportExportLog) message.Body;

                    _logger.Info("Retreiving xls for response transform...");
                    ImportExportXmlSummary xsl = XmlManagement.GetImportExportXmlSummary(_user, "HrXmlResponse"); //**HARDCODE**//

                    //create working directory
                    string workingDirectory = XmlManagement.CreateDirectory($"{_user.DatabaseName}_response", _importExportProcessingDirectory);
                    string sourceFilePath = Path.Combine(workingDirectory, "hrxmlExport-0.xml");
                    string outputFilePath = Path.Combine(workingDirectory, "hrxmlExport-1.xml");
                    string ackFilePath = Path.Combine(workingDirectory, "hrxmlExportAck-2.xml");

                    //serialize source
                    _logger.Info($"Writing message to {sourceFilePath}...");
                    using (FileStream sourceStream = new FileStream(sourceFilePath, FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(log.GetType());
                        serializer.Serialize(sourceStream, log);
                    }

                    _logger.Info("Transforming message file...");
                    XmlManagement.TransformXml(xsl, sourceFilePath, outputFilePath);
                    _logger.Info("Message file transformed.");

                    if (!_disableNgaResponse)
                    {
                        _logger.Info("Sending hrxml acknowledgement...");
                        string response = SendHrXmlResponse(outputFilePath, _ngaHrxmlConfirmationUrl, _ngaHrxmlConfirmationLogin, _ngaHrxmlConfirmationPassword);
                        _logger.Info($"Writing acknowledgement file to {ackFilePath}");
                        File.WriteAllText(ackFilePath, response);
                    }

                    msmqTransaction.Commit();
                }
                catch (Exception exc)
                {
                    _logger.Error($"Error during the import queue peek operation: {exc}");
                    ErrorManagement.HandleError(_user, "_importQueue_PeekCompleted", exc);
                    try
                    {
                        msmqTransaction.Abort();
                    }
                    catch
                    {
                    }

                    //wait a bit to not overrun the logs
                    System.Threading.Thread.Sleep(60 * 1000);
                }
                finally
                {
                    ExportQueue.BeginPeek();
                }
            }
        }

        public string SendHrXmlResponse(string fileName, string url, string login, string password)
        {
            //load file
            string fileContents = File.ReadAllText(fileName);
            _logger.Info($"Loaded {fileName} to send as a response");
            //transmit response
            NgaHrXml.ngapexExternalInboundwebservicesproviderretrieveBackEndBod client = new NgaHrXml.ngapexExternalInboundwebservicesproviderretrieveBackEndBod();
            NgaHrXml.retrieveBackendBODRequest request = new NgaHrXml.retrieveBackendBODRequest {xmlIn = fileContents};

            client.Credentials = new System.Net.NetworkCredential(login, password);
            client.Url = url;

            _logger.Info($"Sending response to {url}");
            NgaHrXml.retrieveBackendBODResponse response = client.retrieveBackendBOD(request);

            return response.xmlOut;
        }

        public void LoadFile(DatabaseUser workLinksUser, RemoteImportExportLog log, string outputDirectory, string parentImportExportProcessingDirectory)
        {
            if (!Directory.Exists(outputDirectory))
            {
                Directory.CreateDirectory(outputDirectory);
            }

            if (!Directory.Exists(parentImportExportProcessingDirectory))
            {
                Directory.CreateDirectory(parentImportExportProcessingDirectory);
            }
            
            using (FileStream file = FileManagement.LockFile(log.FilePath))
            {
                try
                {
                    //using (TransactionScope scope = new TransactionScope())
                    {
                        log.Imports = new ImportExportLogCollection();

                        //if file is zip extract
                        if (Path.GetExtension(log.FilePath).ToLower().Equals("zip"))
                        {
                            throw new Exception("missing extraction logic for zip.  Please code this");
                        }

                        _logger.Info($"Creating HrXmlFile object from {log.FilePath}.");
                        HrXmlFile doc = GetHrXmlFile(file);
                        // Adding this assignment here as we were encountering exception where importexporttype was null
                        log.ImportExportType = doc.ImportFileType.ToString(); 

                        if (doc.ImportFileType.Equals(ImportFileType.Unknown))
                        {
                            _logger.Info("Import file type is 'Unknown'");
                            log.ImportExportType = doc.ImportFileType.ToString();
                            FileManagement.UnlockFileAndMove(file, outputDirectory);
                            throw new Exception("unknown file format");
                        }

                        if (doc.ImportFileType.Equals(ImportFileType.UnknownHrXml))
                        {
                            _logger.Info("Import file type is 'UnknownHrXml'");
                            log.SuccessFlag = true;
                            log.ImportExportType = doc.ImportFileType.ToString();
                            log.Identifier = doc.BODID.ToString();
                            log.ProcessingOutput = "Unknown hrxml file";
                            InsertRemoteImportExportLog(log);
                            FileManagement.UnlockFileAndMove(file, outputDirectory);
                        }
                        else
                        {
                            string importExportProcessingDirectory = XmlManagement.CreateDirectory($"{workLinksUser.DatabaseName}_request", parentImportExportProcessingDirectory);

                            //write imported file to temp directory
                            string tempSourceFileName = Path.Combine(importExportProcessingDirectory, doc.FileName);
                            _logger.Info($"Writing bytes to {tempSourceFileName}");
                            XmlManagement.WriteFile(tempSourceFileName, doc.GetBytes(), null);

                            ImportExportLog localLog = new ImportExportLog {FileProcessingDirectory = importExportProcessingDirectory};
                            _logger.Info("Converting import file into a WorkLinksEmployee object");
                            WorkLinksEmployee employee = (WorkLinksEmployee) XmlManagement.ConvertImportFile(workLinksUser, doc.XMLImportTag, tempSourceFileName, localLog);
                            doc.AlternateExternalIdentifier1 = employee.ImportExternalIdentifier;
                            localLog.AlternateExternalIdentifier1 = doc.AlternateExternalIdentifier1;

                            //get database import is going to
                            doc.DatabaseName = AssignHrXmlDatabase(workLinksUser, employee);
                            _logger.Info($"Assigning db user {workLinksUser.UserName} the database {workLinksUser.DatabaseName}");
                            DatabaseUser clientUser = workLinksUser.UseDatabase(doc.DatabaseName);

                            if (!Guid.TryParse(employee.BODID, out Guid guid))
                            {
                                log.SuccessFlag = false;
                                localLog.FileProcessingDirectory = importExportProcessingDirectory;
                                localLog.ProcessingOutput = $"BODID:{employee.BODID} is invalid.";
                                _logger.Error($"BODID:{employee.BODID} is invalid.");
                            }
                            else if (HasBODIDBeenUsed(clientUser, guid))
                            {
                                _logger.Error($"BODID:{employee.BODID} has already been processed.");
                                localLog.UniqueIdentifier = guid;
                                log.SuccessFlag = false;
                                localLog.FileProcessingDirectory = importExportProcessingDirectory;
                                localLog.ProcessingOutput = $"BODID:{employee.BODID} has already been processed.";
                            }
                            //we have an import
                            else
                            {
                                _logger.Info("Starting loading...");

                                /********temp*******/
                                employee.AddressEnabledFlag = _hrmlAddressEnabledFlag;
                                employee.BankingEnabledFlag = _hrmlBankingEnabledFlag;
                                employee.PayrollBatchEnabledFlag = _hrmlPayrollBatchEnabledFlag;
                                employee.BiographicalEnabledFlag = _hrxmlBiographicalEnabledFlag;
                                employee.EmploymentInformationEnabledFlag = _hrxmlEmploymentInformationEnabledFlag;
                                employee.PaycodeEnabledFlag = _hrmlPaycodeEnabledFlag;
                                employee.EmailEnabledFlag = _hrmlEmailEnabledFlag;
                                employee.StatutoryDeductionEnabledFlag = _hrmlStatutoryDeductionEnabledFlag;
                                employee.PhoneEnabledFlag = _hrmlPhoneEnabledFlag;
                                employee.PositionEnabledFlag = _hrmlPositionEnabledFlag;
                                employee.RehireEnabledFlag = _hrmlRehireEnabledFlag;
                                employee.LeaveEnabledFlag = _hrxmlLeaveEnabledFlag;
                                employee.TerminationEnabledFlag = _hrmlTerminationEnabledFlag;
                                /********end temp****/

                                try
                                {
                                    using (TransactionScope innerscope = new TransactionScope())
                                    {
                                        localLog.UniqueIdentifier = guid;
                                        doc.BODID = guid;

                                        _logger.Info($"BODID:{employee.BODID} - Checking if employee exists with identifier {employee.ImportExternalIdentifier} against db {clientUser.DatabaseName}");
                                        //does the employee exist?
                                        EmployeeSummaryCollection dbEmployees = EmployeeManagement.GetEmployeeSummary(clientUser, new EmployeeCriteria {EmployeeNumber = employee.ImportExternalIdentifier, SecurityOverrideFlag = true, UseLikeStatementInProcSelect = false});
                                        //employee exists, we can do updates
                                        if (dbEmployees.Count > 0)
                                        {
                                            //biographical
                                            Employee dbEmployee = EmployeeManagement.GetEmployee(clientUser, new EmployeeCriteria {EmployeeId = dbEmployees[0].EmployeeId, SecurityOverrideFlag = true})[0];
                                            employee.EmployeeId = dbEmployee.EmployeeId;
                                            employee.PersonId = dbEmployee.PersonId;
                                            _logger.Info($"BODID:{employee.BODID} - EmployeeId { employee.EmployeeId} found.");

                                            //add employee sin if doesnt exist
                                            if (employee.GovernmentIdentificationNumber1 == null)
                                                employee.GovernmentIdentificationNumber1 = dbEmployee.GovernmentIdentificationNumber1;

                                            UpdateEmployee(clientUser, EmployeeStatus.Exists, employee, localLog);
                                            //address
                                            UpdateAddress(clientUser, EmployeeStatus.Exists, employee, localLog);
                                            //phone/email
                                            UpdateContactChannel(clientUser, EmployeeStatus.Exists, employee, localLog);
                                            //employee position
                                            UpdateEmployeePosition(clientUser, EmployeeStatus.Exists, employee, localLog);
                                            //employee rehire
                                            UpdateRehire(clientUser, EmployeeStatus.Exists, employee, localLog);
                                            //employee terminate
                                            UpdateTerminate(clientUser, EmployeeStatus.Exists, employee, localLog);
                                            //employee leave
                                            UpdateLeave(clientUser, EmployeeStatus.Exists, employee, localLog);
                                            //update banking
                                            UpdateEmployeeBanking(clientUser, EmployeeStatus.Exists, employee, localLog);
                                            //employee paycode
                                            UpdateEmployeePaycode(clientUser, EmployeeStatus.Exists, employee, localLog);
                                            //payroll batch
                                            UpdatePayrollBatch(clientUser, EmployeeStatus.Exists, employee, localLog);
                                        }
                                        else
                                        {
                                            //is there a pending employee
                                            _logger.Info($"BODID:{employee.BODID} - Checking for pending employess...");
                                            EmployeeSummaryCollection pendingEmployees = EmployeeManagement.GetPendingEmployeeSummary(clientUser, new EmployeeCriteria {EmployeeNumber = employee.ImportExternalIdentifier, UseLikeStatementInProcSelect = false, SecurityOverrideFlag = true});
                                            if (pendingEmployees.Count > 0)
                                            {
                                                //biographical
                                                employee.EmployeeId = Math.Abs(pendingEmployees[0].EmployeeId);
                                                _logger.Info($"BODID:{employee.BODID} - Pending employeeId {employee.EmployeeId} found");
                                                UpdateEmployee(clientUser, EmployeeStatus.Pending, employee, localLog);
                                                //address
                                                UpdateAddress(clientUser, EmployeeStatus.Pending, employee, localLog);
                                                //phone/email
                                                UpdateContactChannel(clientUser, EmployeeStatus.Pending, employee, localLog);
                                                //employee position
                                                UpdateEmployeePosition(clientUser, EmployeeStatus.Pending, employee, localLog);
                                                //employee rehire
                                                UpdateRehire(clientUser, EmployeeStatus.Pending, employee, localLog);
                                                //employee terminate
                                                UpdateTerminate(clientUser, EmployeeStatus.Pending, employee, localLog);
                                                //employee leave
                                                UpdateLeave(clientUser, EmployeeStatus.Pending, employee, localLog);
                                                //update banking
                                                UpdateEmployeeBanking(clientUser, EmployeeStatus.Pending, employee, localLog);
                                                //employee paycode
                                                UpdateEmployeePaycode(clientUser, EmployeeStatus.Pending, employee, localLog);
                                                //payroll batch
                                                UpdatePayrollBatch(clientUser, EmployeeStatus.Pending, employee, localLog);
                                            }
                                            else //no employee
                                            {
                                                _logger.Info($"BODID:{employee.BODID} - No employee found");
                                                //biographical
                                                UpdateEmployee(clientUser, EmployeeStatus.Missing, employee, localLog);
                                                //address
                                                UpdateAddress(clientUser, EmployeeStatus.Missing, employee, localLog);
                                                //phone/email
                                                UpdateContactChannel(clientUser, EmployeeStatus.Missing, employee, localLog);
                                                //employee position
                                                UpdateEmployeePosition(clientUser, EmployeeStatus.Missing, employee, localLog);
                                                //employee rehire
                                                UpdateRehire(clientUser, EmployeeStatus.Missing, employee, localLog);
                                                //employee terminate
                                                UpdateTerminate(clientUser, EmployeeStatus.Missing, employee, localLog);
                                                //employee leave
                                                UpdateLeave(clientUser, EmployeeStatus.Missing, employee, localLog);
                                                //update banking
                                                UpdateEmployeeBanking(clientUser, EmployeeStatus.Missing, employee, localLog);
                                                //employee paycode
                                                UpdateEmployeePaycode(clientUser, EmployeeStatus.Missing, employee, localLog);
                                                //payroll batch
                                                UpdatePayrollBatch(clientUser, EmployeeStatus.Missing, employee, localLog);
                                            }

                                            _logger.Info($"BODID:{employee.BODID} - Validating EmployeeId {employee.EmployeeId}");
                                            ValidatePendingEmployee(clientUser, employee.EmployeeId, localLog);
                                        }

                                        localLog.SuccessFlag = true;
                                        log.SuccessFlag = true;
                                        innerscope.Complete();
                                    }
                                }
                                catch (Exception exc)
                                {
                                    _logger.Error($"Error loading file {log.FilePath}: {exc}");
                                    localLog.ProcessingOutput += exc.Message + "\n";
                                }
                            }

                            _logger.Info($"BODID:{employee.BODID} - Processing output:");
                            _logger.Info($"BODID:{employee.BODID} - {localLog.ProcessingOutput}");

                            log.ProcessingOutput = localLog.ProcessingOutput;
                            log.Imports.Add(localLog);
                            //output to central log
                            log.Identifier = doc.BODID.ToString();
                            InsertRemoteImportExportLog(log);

                            localLog.RemoteImportExportLogId = log.RemoteImportExportLogId;
                            XmlManagement.InsertImportExportLog(clientUser, localLog);

                            //create response
                            ImportExportLog response = new ImportExportLog
                            {
                                RelatedImportExportLogId = localLog.ImportExportLogId,
                                RelatedImportExportLog = localLog
                            };
                            //log to queue for processing
                            if (!_overrideQueue)
                                AddMessageToQueue(response);

                            // On the odd case the stream
                            if (File.Exists(file.Name))
                            {
                                FileManagement.UnlockFileAndMove(file, outputDirectory);
                            }
                        }

                        //                        scope.Complete();
                    }
                }
                catch (Exception exc)
                {
                    _logger.Error($"Error loading file {log.FilePath}: {exc}");
                    log.SuccessFlag = false;
                    log.ProcessingOutput = exc.Message;
                    InsertRemoteImportExportLog(log);
                    throw;
                }
            }
        }

        private void ValidatePendingEmployee(DatabaseUser user, long employeeId, ImportExportLog log)
        {
            //get employee
            WizardCache cache = WizardManagement.GetWizardCache(user, 1, employeeId, WizardCache.ItemType.None)[0];

            //check employee
            Employee employee = ((EmployeeCollection) cache.Items[WizardCache.ItemType.EmployeeBiographicalControl].Data).FirstOrDefault();
            if (employee == null || !employee.IsValid)
                SetLogWarning(log, (employee?.GetValidationInformation()) ?? "Employee Biographical record not found");

            //check position
            if (cache.Items[WizardCache.ItemType.EmployeePositionViewControl]?.Data == null)
                SetLogWarning(log, "Employee position information is missing");

            EmployeePosition employeePosition = ((EmployeePositionCollection) cache.Items[WizardCache.ItemType.EmployeePositionViewControl].Data)?.FirstOrDefault();
            if (employeePosition == null || !employeePosition.IsValid)
                SetLogWarning(log, employeePosition?.GetValidationInformation() ?? "Employee position information is missing");

            //check employee employment information
            if (cache.Items[WizardCache.ItemType.EmployeeEmploymentInformationControl]?.Data == null)
            {
                SetLogWarning(log, "Employee employment information is missing");
            }
            //else
            //    EmployeeEmploymentInformation employeeEmploymentInformation = ((EmployeeEmploymentInformationCollection)cache.Items[WizardCache.ItemType.EmployeeEmploymentInformationControl].Data)[0];
            //if (!employeeEmploymentInformation.IsValid)
            //    SetLogWarning(log, employeeEmploymentInformation.GetValidationInformation());

            if (cache.Items[WizardCache.ItemType.AddressDetailControl]?.Data == null)
                SetLogWarning(log, "Employee address is missing");

            PersonAddressCollection addressData = ((PersonAddressCollection) cache.Items[WizardCache.ItemType.AddressDetailControl].Data);
            PersonAddress address = addressData?.FirstOrDefault();
            if (address == null || !address.IsValid)
                SetLogWarning(log, (address?.GetValidationInformation())??"Employee address is missing");

            if (cache.Items[WizardCache.ItemType.StatutoryDeductionViewControl]?.Data == null)
                SetLogWarning(log, "Statutory deduction is missing");
            //StatutoryDeduction statutoryDeduction = ((StatutoryDeductionCollection)cache.Items[WizardCache.ItemType.StatutoryDeductionViewControl].Data)[0];
            //if (!statutoryDeduction.IsValid)
            //    SetLogWarning(log, statutoryDeduction.GetValidationInformation()); 
        }

        private void UpdateLeave(DatabaseUser user, EmployeeStatus status, WorkLinksEmployee employee, ImportExportLog log)
        {
            if (employee.LeaveEnabledFlag && !employee.LeaveChangeType.Equals(WorkLinksEmployee.ChangeType.None))
            {
                if (status.Equals(EmployeeStatus.Exists))
                {
                    EmployeePosition dbPosition = EmployeeManagement.GetEmployeePosition(user, new EmployeePositionCriteria {SecurityOverrideFlag = true, EmployeeId = employee.EmployeeId, GetLatestRecordFlag = true})[0];
                    //get current cutoff date
                    EmployeeEmploymentInformation info = EmployeeManagement.GetEmployeeEmploymentInformation(user, employee.EmployeeId, true)[0];
                    PayrollPeriodCollection periods = PayrollManagement.GetPayrollPeriod(user, new PayrollPeriodCriteria {PayrollProcessGroupCode = info.PayrollProcessGroupCode, GetMinOpenPeriodFlag = true});
                    if (employee.WorkReturnDate == null)
                    {
                        //get roe info
                        string roeDefaultContactLastName = CodeManagement.GetCodeSystem(user)["ROELNAME"].Description;
                        string roeDefaultContactFirstName = CodeManagement.GetCodeSystem(user)["ROEFNAME"].Description;
                        string roeDefaultContactTelephone = CodeManagement.GetCodeSystem(user)["ROEPHONE"].Description;
                        string roeDefaultContactTelephoneExt = CodeManagement.GetCodeSystem(user)["ROEPHEXT"].Description;

                        if (dbPosition.EmployeePositionStatusCode == "TER")
                            throw new Exception("Cannot put employee on leave.  Employee is terminated");
                        if (dbPosition.EffectiveDate > employee.LeaveTerminationDate)
                        {
                            throw new Exception("Leave date is prior to last position effective date");
                        }

                        //get employee position reason
                        string leaveReason = DecodeCode(user, CodeTableType.EmployeePositionReasonCode, employee.LeaveTerminationTypeCodeExternalIdentifier);
                        if (leaveReason == null)
                            throw new Exception(string.Format("Cannot find leave reason code {0} in worklinks", employee.LeaveTerminationTypeCodeExternalIdentifier));
                        CodeObject leaveReasonCode = CodeManagement.GetCodeTable(user.DatabaseName, "EN", CodeTableType.EmployeePositionReasonCode, leaveReason)[0];
                        CodeObject leaveActionCode = CodeManagement.GetCodeTable(user.DatabaseName, "EN", CodeTableType.EmployeePositionActionCode, leaveReasonCode.ParentCode)[0];

                        EmployeePosition leavePosition = dbPosition.GetBasePositionRecord();
                        leavePosition.EmployeePositionStatusCode = leaveActionCode.ParentCode;
                        leavePosition.EmployeePositionActionCode = leaveActionCode.Code;
                        leavePosition.EmployeePositionReasonCode = leaveReasonCode.Code;
                        leavePosition.EffectiveDate = employee.LeaveTerminationDate;

                        if (leavePosition.IsTermRelated)
                            leavePosition.LastDayPaid = ((DateTime) leavePosition.EffectiveDate).AddDays(-1);

                        EmployeeManagement.InsertEmployeePosition(user, leavePosition, dbPosition, periods[0].CutoffDate, info.PayrollProcessGroupCode, roeDefaultContactLastName, roeDefaultContactFirstName, roeDefaultContactTelephone, roeDefaultContactTelephoneExt, false);
                    }
                    else //returning from leave
                    {
                        EmployeePosition position = dbPosition.GetBasePositionRecord();
                        if (dbPosition.IsTermRelated) //rehire
                        {
                            position.EmployeePositionActionCode = "RH";
                        }
                        else //return from leave
                        {
                            if (dbPosition.EmployeePositionStatusCode == "AC")
                                throw new Exception("Cannot return from leave.  Employee is not on leave.");

                            position.EmployeePositionActionCode = "RH";
                        }

                        position.EmployeePositionStatusCode = "AC";
                        position.EffectiveDate = (DateTime) employee.WorkReturnDate;

                        if (dbPosition.EffectiveDate > position.EffectiveDate)
                        {
                            throw new Exception("Employee Position effective date is prior to last position effective date");
                        }

                        EmployeeManagement.InsertEmployeePosition(user, position, dbPosition, periods[0].CutoffDate, info.PayrollProcessGroupCode, null, null, null, null, false);
                    }
                }
                else
                {
                    throw new Exception("Leave invalid.  Employee does not exist in WorkLinks.");
                }
            }
        }

        private void UpdateTerminate(DatabaseUser user, EmployeeStatus status, WorkLinksEmployee employee, ImportExportLog log)
        {
            if (employee.TerminationEnabledFlag && !employee.TerminationChangeType.Equals(WorkLinksEmployee.ChangeType.None))
            {
                if (status.Equals(EmployeeStatus.Exists))
                {
                    //get roe info
                    EmployeePosition dbPosition = EmployeeManagement.GetEmployeePosition(user, new EmployeePositionCriteria {SecurityOverrideFlag = true, EmployeeId = employee.EmployeeId, GetLatestRecordFlag = true})[0];
                    string roeDefaultContactLastName = CodeManagement.GetCodeSystem(user)["ROELNAME"].Description;
                    string roeDefaultContactFirstName = CodeManagement.GetCodeSystem(user)["ROEFNAME"].Description;
                    string roeDefaultContactTelephone = CodeManagement.GetCodeSystem(user)["ROEPHONE"].Description;
                    string roeDefaultContactTelephoneExt = CodeManagement.GetCodeSystem(user)["ROEPHEXT"].Description;

                    //does this company ignore messages regarding terminating an already terminated employee?
                    bool enableTerminationCheck = Convert.ToBoolean(CodeManagement.GetCodeSystem(user)["HRX_ETCK"].Description);

                    if (dbPosition.EmployeePositionStatusCode == "TER")
                    {
                        if (enableTerminationCheck)
                            throw new Exception("Employee already terminated");
                    }
                    else
                    {
                        if (dbPosition.EffectiveDate > employee.LeaveTerminationDate)
                        {
                            throw new Exception("Termination date is prior to last position effective date");
                        }

                        //get current cutoff date
                        EmployeeEmploymentInformation info = EmployeeManagement.GetEmployeeEmploymentInformation(user, employee.EmployeeId, true)[0];
                        PayrollPeriodCollection periods = PayrollManagement.GetPayrollPeriod(user, new PayrollPeriodCriteria {PayrollProcessGroupCode = info.PayrollProcessGroupCode, GetMinOpenPeriodFlag = true});

                        EmployeePosition position = dbPosition.GetBasePositionRecord();
                        position.EmployeePositionStatusCode = "TER"; //hard code
                        position.EmployeePositionActionCode = "TERM"; //hard code
                        position.EmployeePositionReasonCode = DecodeCode(user, CodeTableType.EmployeePositionReasonCode, employee.LeaveTerminationTypeCodeExternalIdentifier);
                        position.EffectiveDate = employee.LeaveTerminationDate;
                        position.LastDayPaid = ((DateTime) position.EffectiveDate).AddDays(-1);

                        EmployeeManagement.InsertEmployeePosition(user, position, dbPosition, periods[0].CutoffDate, info.PayrollProcessGroupCode, roeDefaultContactLastName, roeDefaultContactFirstName, roeDefaultContactTelephone, roeDefaultContactTelephoneExt, false);
                    }
                }
                else
                {
                    throw new Exception("Terminate invalid.  Employee does not exist in WorkLinks.");
                }
            }
        }

        private void UpdateRehire(DatabaseUser user, EmployeeStatus status, WorkLinksEmployee employee, ImportExportLog log)
        {
            if (employee.RehireEnabledFlag && !employee.RehireChangeType.Equals(WorkLinksEmployee.ChangeType.None))
            {
                if (status.Equals(EmployeeStatus.Exists))
                {
                    //do nothing for now
                }
                else
                {
                    throw new Exception("Rehire invalid.  Employee does not exist in WorkLinks.");
                }
            }
        }

        private void UpdateEmployeePosition(DatabaseUser user, EmployeeStatus status, WorkLinksEmployee employee, ImportExportLog log)
        {
            if (employee.PositionEnabledFlag && !employee.PositionChangeType.Equals(WorkLinksEmployee.ChangeType.None))
            {
                WorkLinksEmployeePosition position = employee.WorkLinksEmployeePosition;

                if (status.Equals(EmployeeStatus.Exists))
                {
                    //get last employee position
                    EmployeePosition dbPosition = EmployeeManagement.GetEmployeePosition(user, new EmployeePositionCriteria {SecurityOverrideFlag = true, EmployeeId = employee.EmployeeId, GetLatestRecordFlag = true})[0];
                    if (dbPosition.EffectiveDate > position.EffectiveDate)
                        throw new Exception("Employee Position effective date is prior to last position effective date");

                    //map the org unit
                    if (position.OrganationUnitImportExternalIdentifier1 != null)
                        position.EmployeePositionOrganizationUnits = MapEmployeeOrganizationUnit(user, position.OrganationUnitImportExternalIdentifier1, position.EffectiveDate);
                    else
                        position.EmployeePositionOrganizationUnits = dbPosition.EmployeePositionOrganizationUnits;

                    //map the salary
                    if (position.PaymentMethodCode == null)
                    {
                        position.PaymentMethodCode = dbPosition.PaymentMethodCode;
                        position.CompensationAmount = dbPosition.CompensationAmount;
                    }

                    //use last workdays
                    position.Workdays = dbPosition.Workdays;

                    //use last standard hours if not supplied
                    if (position.StandardHours < 0)
                        position.StandardHours = dbPosition.StandardHours;

                    position.EmployeeId = employee.EmployeeId;
                    //get current cutoff date
                    EmployeeEmploymentInformation info = EmployeeManagement.GetEmployeeEmploymentInformation(user, employee.EmployeeId, true)[0];
                    PayrollPeriodCollection periods = PayrollManagement.GetPayrollPeriod(user, new PayrollPeriodCriteria {PayrollProcessGroupCode = info.PayrollProcessGroupCode, GetMinOpenPeriodFlag = true});

                    if (employee.RehireEnabledFlag && !employee.RehireChangeType.Equals(WorkLinksEmployee.ChangeType.None))
                    {
                        if (dbPosition.EmployeePositionStatusCode == "AC")
                            throw new Exception("Employee is active, cannot rehire");

                        position.EmployeePositionActionCode = "RH"; //hard code
                        position.EmployeePositionStatusCode = "AC"; //hard code
                        EmployeeManagement.InsertEmployeePosition(user, position, dbPosition, periods[0].CutoffDate, info.PayrollProcessGroupCode, null, null, null, null, false);
                    }
                    else
                    {
                        //what changes do we have?
                        position.EmployeePositionActionCode = "CHNG"; //hard code
                        position.EmployeePositionStatusCode = dbPosition.EmployeePositionStatusCode;
                        EmployeeManagement.InsertEmployeePosition(user, position, dbPosition, null, null, null, null, null, null, false);
                    }
                }
                else
                {
                    try
                    {
                        position.EmployeePositionOrganizationUnits = MapEmployeeOrganizationUnit(user, position.OrganationUnitImportExternalIdentifier1, position.EffectiveDate);
                    }
                    catch (Exception exc)
                    {
                        SetLogWarning(log, exc.Message);
                    }

                    //set the default workdays from the code system
                    char[] defaultWorkdays = CodeManagement.GetCodeTable(user.DatabaseName, "EN", CodeTableType.System, "DEWRKDAY")[0].Description.ToCharArray();

                    for (int i = 0; i < defaultWorkdays.Length; i++)
                    {
                        position.Workdays.Add(new EmployeePositionWorkday
                        {
                            EmployeePositionWorkdayId = -(i + 1),
                            EmployeePositionId = position.EmployeePositionId,
                            IsWorkdayFlag = defaultWorkdays[i].ToString() == "1" ? true : false,
                            DayOfWeek = Convert.ToString(i + 1)
                        });
                    }

                    WizardCache cache = WizardManagement.GetWizardCache(user, 1, employee.EmployeeId, WizardCache.ItemType.EmployeePositionViewControl)[0];
                    WizardCacheItem item = cache.Items[0];
                    position.EmployeePositionActionCode = "NEW"; //hard code;
                    position.EmployeePositionStatusCode = "AC"; //hard code

                    item.Data = employee.EmployeePositionCollection;
                    item.WizardCacheId = employee.EmployeeId;
                    // if (!position.IsValid) { SetLogWarning(log, position.GetValidationInformation()); }
                    WizardManagement.UpdateWizardCacheItem(user, cache.Items[0]);
                }
            }
        }

        private EmployeePositionOrganizationUnitCollection MapEmployeeOrganizationUnit(DatabaseUser user, string organationUnitImportExternalIdentifier1, DateTime? effectiveDate)
        {
            EmployeePositionOrganizationUnitCollection collection = new EmployeePositionOrganizationUnitCollection();

            string orgUnits = EmployeeManagement.SelectEmployeePositionOrganizationUnitLevelHrxmlMatch(user, organationUnitImportExternalIdentifier1);

            int i = 1;
            if (orgUnits != null && orgUnits.Length > 1)
            {
                foreach (string organizationUnitId in orgUnits.Substring(1).Split('/'))
                {
                    EmployeePositionOrganizationUnit orgUnit = new EmployeePositionOrganizationUnit
                    {
                        OrganizationUnitLevelId = i++,
                        OrganizationUnitId = Convert.ToInt64(organizationUnitId),
                        OrganizationUnitStartDate = effectiveDate
                    };
                    collection.Add(orgUnit);
                }
            }

            if (collection.Count == 0)
                throw new Exception($"Cost center:{organationUnitImportExternalIdentifier1} cannot be found in WorkLinks.");

            foreach (EmployeePositionOrganizationUnit item in collection)
            {
                item.OrganizationUnitStartDate = effectiveDate;
            }

            return collection;
        }

        private void UpdateEmployeeBanking(DatabaseUser user, EmployeeStatus status, WorkLinksEmployee employee, ImportExportLog log)
        {
            if (employee.BankingEnabledFlag && !employee.BankingChangeType.Equals(WorkLinksEmployee.ChangeType.None))
            {
                foreach (WorkLinksEmployeeBanking bank in employee.WorkLinksBankingCollection)
                {
                    bank.CodeEmployeeBankingRestrictionCode = DecodeCode(user, CodeTableType.EmployeeBankingRestrictionCodeType, bank.BankingRestrictionExternalIdentifier);
                    bank.CodeEmployeeBankingSequenceCode = DecodeCode(user, CodeTableType.EmployeeBankingSequenceNumberTypeCode, bank.SequenceImportExternalIdentifier);
                    bank.EmployeeBankingCode = DecodeCode(user, CodeTableType.EmployeeBankingTypeCode, bank.BankingCodeImportExternalIdentifier);
                }

                if (status.Equals(EmployeeStatus.Exists))
                {
                    if (employee.WorkLinksBankingCollection.IsValid)
                    {
                        //get current channels
                        EmployeeBankingCollection dbBanks = EmployeeManagement.GetEmployeeBanking(user, employee.EmployeeId, true);
                        foreach (EmployeeBanking bank in dbBanks)
                        {
                            EmployeeManagement.DeleteEmployeeBanking(user, bank);
                        }

                        foreach (WorkLinksEmployeeBanking bank in employee.WorkLinksBankingCollection)
                        {
                            if (!bank.DeleteFlag)
                            {
                                bank.EmployeeId = employee.EmployeeId;
                                EmployeeManagement.InsertEmployeeBanking(user, bank);
                            }
                        }
                    }
                    else
                    {
                        throw new Exception(employee.WorkLinksBankingCollection.GetValidationInformation());
                    }
                }
                else
                {
                    if (!employee.WorkLinksBankingCollection.IsValid)
                    {
                        SetLogWarning(log, employee.WorkLinksBankingCollection.GetValidationInformation());
                    }

                    WizardCache cache = WizardManagement.GetWizardCache(user, 1, employee.EmployeeId, WizardCache.ItemType.EmployeeBankingInformationControl)[0];
                    WizardCacheItem item = cache.Items[0];
                    EmployeeBankingCollection newBanks = new EmployeeBankingCollection();
                    foreach (WorkLinksEmployeeBanking bank in employee.WorkLinksBankingCollection)
                    {
                        EmployeeBanking newBank = new EmployeeBanking();
                        bank.CopyTo(newBank);
                        newBanks.Add(newBank);
                    }

                    item.Data = newBanks;
                    item.WizardCacheId = employee.EmployeeId;
                    WizardManagement.UpdateWizardCacheItem(user, cache.Items[0]);
                }
            }
        }

        private void UpdatePayrollBatch(DatabaseUser user, EmployeeStatus status, WorkLinksEmployee employee, ImportExportLog log)
        {
            if (employee.PayrollBatchEnabledFlag && !employee.PayrollBatchChangeType.Equals(WorkLinksEmployee.ChangeType.None))
            {
                if (status.Equals(EmployeeStatus.Exists))
                {
                    PayrollManagement.ProcessPayrollBatchImport(user, employee.PayrollBatch, log, true);
                    if (!log.SuccessFlag)
                        throw new Exception(log.ProcessingOutput);
                }
                else
                {
                    throw new Exception("Cannot process payroll records.  Employee cannot be found in WorkLinks or is in Pending Hire status.");
                }
            }
        }

        private void UpdateEmployeePaycode(DatabaseUser user, EmployeeStatus status, WorkLinksEmployee employee, ImportExportLog log)
        {
            if (employee.PaycodeEnabledFlag && !employee.PaycodeChangeType.Equals(WorkLinksEmployee.ChangeType.None))
            {
                if (status.Equals(EmployeeStatus.Exists))
                {
                    EmployeeManagement.ProcessEmployeePaycodeImport(user, employee.Paycodes, log, employee.ImportExternalIdentifier);
                    if (!log.SuccessFlag)
                        throw new Exception(log.ProcessingOutput);
                }
                else if (status.Equals(EmployeeStatus.Pending) || status.Equals(EmployeeStatus.Missing))
                {
                    foreach (EmployeePaycode paycode in employee.Paycodes)
                    {
                        paycode.PaycodeCode = DecodeCode(user, CodeTableType.PaycodeCode, paycode.PaycodeCodeImportExternalIdentifier);
                        paycode.PaycodeTypeCode = CodeManagement.GetPaycodeTypeCode(user, paycode.Key)[0].Key;
                    }

                    if (!employee.Paycodes.IsValid)
                    {
                        SetLogWarning(log, employee.Paycodes.GetValidationInformation());
                    }

                    WizardCache cache = WizardManagement.GetWizardCache(user, 1, employee.EmployeeId, WizardCache.ItemType.EmployeePaycodeModuleControl)[0];
                    WizardCacheItem item = cache.Items[0];
                    item.Data = employee.Paycodes;
                    item.WizardCacheId = employee.EmployeeId;
                    WizardManagement.UpdateWizardCacheItem(user, cache.Items[0]);
                }
            }
        }

        private void UpdateContactChannel(DatabaseUser user, EmployeeStatus status, WorkLinksEmployee employee, ImportExportLog log)
        {
            if (employee.PhoneEnabledFlag && !employee.PhoneChangeType.Equals(WorkLinksEmployee.ChangeType.None))
            {
                UpdateContactChannel(user, status, employee, employee.Phones, log, true);
            }

            if (employee.EmailEnabledFlag && !employee.EmailChangeType.Equals(WorkLinksEmployee.ChangeType.None))
            {
                UpdateContactChannel(user, status, employee, employee.Emails, log, false);
            }
        }

        private void UpdateContactChannel(DatabaseUser user, EmployeeStatus status, WorkLinksEmployee employee, WorkLinksContactChannelCollection channels, ImportExportLog log, bool phoneFlag)
        {
            //get the codes synced
            foreach (WorkLinksContactChannel channel in channels)
            {
                channel.ContactChannelTypeCode = DecodeCode(user, CodeTableType.ContactChannelTypeCode, channel.ImportExternalIdentifier);
                channel.PersonId = employee.PersonId;
            }

            if (status.Equals(EmployeeStatus.Exists))
            {
                if (channels.IsValid)
                {
                    //get current channels
                    PersonContactChannelCollection dbChannels = PersonManagement.GetPersonContactChannel(user, employee.PersonId, phoneFlag ? "Phone" : "Email");
                    foreach (WorkLinksContactChannel channel in channels)
                    {
                        if (dbChannels.GetPersonContactChannelByCode(channel.ContactChannelTypeCode) == null)
                        {
                            PersonManagement.InsertPersonContactChannel(user, channel);
                        }
                        else
                        {
                            channel.PersonContactChannelId = dbChannels.GetPersonContactChannelByCode(channel.ContactChannelTypeCode).PersonContactChannelId;
                            channel.ContactChannelId = dbChannels.GetPersonContactChannelByCode(channel.ContactChannelTypeCode).ContactChannelId;
                            channel.OverrideConcurrencyCheckFlag = true;
                            if (channel.DeleteFlag)
                                PersonManagement.DeletePersonContactChannel(user, channel);
                            else
                                PersonManagement.UpdatePersonContactChannel(user, channel, dbChannels.ToList());
                        }
                    }
                }
                else
                {
                    throw new Exception(channels.GetValidationInformation());
                }
            }
            else if (status.Equals(EmployeeStatus.Missing) || status.Equals(EmployeeStatus.Pending))
            {
                //replace contact channels

                if (!channels.IsValid)
                {
                    SetLogWarning(log, channels.GetValidationInformation());
                }

                WizardCache cache = WizardManagement.GetWizardCache(user, 1, employee.EmployeeId, phoneFlag ? WizardCache.ItemType.PersonPhoneControl : WizardCache.ItemType.PersonEmailControl)[0];
                WizardCacheItem item = cache.Items[0];
                //convert to common types
                PersonContactChannelCollection collection = new PersonContactChannelCollection();
                foreach (WorkLinksContactChannel channel in channels)
                {
                    if (!channel.DeleteFlag)
                    {
                        PersonContactChannel newChannel = new PersonContactChannel();
                        channel.CopyTo(newChannel);
                        collection.Add(newChannel);
                    }
                }

                item.Data = collection;
                item.WizardCacheId = cache.WizardCacheId;
                WizardManagement.UpdateWizardCacheItem(user, cache.Items[0]);
            }
        }

        private void UpdateAddress(DatabaseUser user, EmployeeStatus status, WorkLinksEmployee employee, ImportExportLog log)
        {
            if (employee.AddressEnabledFlag && !employee.AddressChangeType.Equals(WorkLinksEmployee.ChangeType.None))
            {
                WorkLinksPersonAddressCollection addresses = employee.WorkLinksAddressCollection;

                foreach (WorkLinksPersonAddress address in addresses)
                {
                    address.CountryCode = DecodeCode(user, CodeTableType.CountryCode, address.CountryCodeExternalIdentifier);
                    address.ProvinceStateCode = DecodeCode(user, CodeTableType.ProvinceStateCode, address.ProvinceStateCodeExternalIdentifier);
                    address.PersonAddressTypeCode = DecodeCode(user, CodeTableType.PersonAddressTypeCode, address.PersonAddressTypeCodeExternalIdentifier);
                }

                if (status.Equals(EmployeeStatus.Missing) || status.Equals(EmployeeStatus.Pending))
                {
                    //if (!address.IsValid) { SetLogWarning(log, address.GetValidationInformation()); }
                    WizardCache cache = WizardManagement.GetWizardCache(user, 1, employee.EmployeeId, WizardCache.ItemType.AddressDetailControl)[0];
                    WizardCacheItem item = cache.Items[0];
                    PersonAddressCollection collection = new PersonAddressCollection();
                    foreach (WorkLinksPersonAddress address in addresses)
                    {
                        PersonAddress saveAddress = new PersonAddress();
                        address.CopyTo(saveAddress);
                        collection.Add(saveAddress);
                    }

                    item.Data = collection;
                    item.WizardCacheId = employee.EmployeeId;
                    WizardManagement.UpdateWizardCacheItem(user, cache.Items[0]);
                }
                else if (status.Equals(EmployeeStatus.Exists))
                {
                    PersonAddressCollection databaseAddresses = PersonManagement.GetPersonAddress(user, employee.PersonId);
                    //delete missing
                    foreach (PersonAddress databaseAddress in databaseAddresses)
                    {
                        WorkLinksPersonAddress address = addresses.GetAddressByType(databaseAddress.PersonAddressTypeCode);
                        if (address == null)
                        {
                            PersonManagement.DeletePersonAddress(user, databaseAddress);
                        }
                    }

                    foreach (WorkLinksPersonAddress address in addresses)
                    {
                        PersonAddress databaseAddress = databaseAddresses.GetAddressByType(address.PersonAddressTypeCode);
                        if (databaseAddress != null)
                        {
                            address.PersonId = databaseAddress.PersonId;
                            address.PersonAddressId = databaseAddress.PersonAddressId;
                            address.AddressId = databaseAddress.AddressId;
                            address.OverrideConcurrencyCheckFlag = true;
                            PersonManagement.UpdatePersonAddress(user, address, false, new List<PersonAddress>());
                        }
                        else
                        {
                            address.PersonId = databaseAddresses[0].PersonId;
                            address.AddressId = -1;
                            address.OverrideConcurrencyCheckFlag = true;
                            PersonManagement.UpdatePersonAddress(user, address, true, new List<PersonAddress>());
                        }

                        if (!address.IsValid)
                        {
                            throw new Exception(address.GetValidationInformation());
                        }
                    }
                }
            }
        }

        private string DecodeCode(DatabaseUser user, CodeTableType type, string externalCode)
        {
            return CodeManagement.DecodeCode(user, type, externalCode);
        }

        private void UpdateEmployee(DatabaseUser user, EmployeeStatus status, WorkLinksEmployee employee, ImportExportLog log)
        {
            if (employee.BiographicalEnabledFlag)
            {
                //map GenderExternalIdentifier to our GenderCode
                try
                {
                    employee.GenderCode = DecodeCode(user, CodeTableType.GenderCode, employee.GenderExternalIdentifier);
                }
                catch(Exception ex)
                {
                    _logger.Error($"Error setting GenderCode: {ex}");
                    //ignore, too many unknown types being sent
                }

                if (status.Equals(EmployeeStatus.Missing))
                {
                    //check for employee add, else fail out
                    if (!employee.BiographicalChangeType.Equals(WorkLinksEmployee.ChangeType.Insert))
                    {
                        throw new Exception("XML file indicates an employee modification.  Employee cannot be found in WorkLinks.");
                    }

                    WizardCache cache = WizardManagement.GetWizardCache(user, 1, null, WizardCache.ItemType.EmployeeBiographicalControl)[0];
                    WizardManagement.UpdateWizardCache(user, cache);
                    WizardCacheItem employeeItem = cache.Items[0];
                    //if (!employee.IsValid) { SetLogWarning(log, employee.GetValidationInformation()); }
                    EmployeeCollection collection = new EmployeeCollection();
                    Employee newEmployee = new Employee();
                    employee.CopyTo(newEmployee);
                    newEmployee.PrimaryAddress = null;
                    collection.Add(newEmployee);
                    employeeItem.Data = collection;
                    employeeItem.WizardCacheId = cache.WizardCacheId;
                    WizardManagement.UpdateWizardCacheItem(user, employeeItem);
                    employee.EmployeeId = (long) employeeItem.WizardCacheId;
                }
                else if (status.Equals(EmployeeStatus.Pending) && !employee.BiographicalChangeType.Equals(WorkLinksEmployee.ChangeType.None))
                {
                    WizardCache cache = WizardManagement.GetWizardCache(user, 1, employee.EmployeeId, WizardCache.ItemType.EmployeeBiographicalControl)[0];
                    WizardCacheItem employeeItem = cache.Items[0];
                    //if (!employee.IsValid) { SetLogWarning(log, employee.GetValidationInformation()); }
                    employeeItem.Data = employee.EmployeeCollection;
                    WizardManagement.UpdateWizardCacheItem(user, cache.Items[0]);
                }
                else if (status.Equals(EmployeeStatus.Exists))
                {
                    //employee biographical
                    if (employee.BiographicalChangeType.Equals(WorkLinksEmployee.ChangeType.Modify))
                    {
                        employee.OverrideConcurrencyCheckFlag = true;
                        if (!employee.IsValid)
                        {
                            throw new Exception(employee.GetValidationInformation());
                        }

                        EmployeeManagement.UpdateEmployee(user, employee, null);
                    }
                    else if (employee.BiographicalChangeType.Equals(WorkLinksEmployee.ChangeType.Insert))
                    {
                        throw new Exception("Cannot add employee.  Employee already exists in database.");
                    }
                }
            }
        }

        private void SetLogWarning(ImportExportLog log, string message)
        {
            _logger.Warn(message);
            log.ProcessingOutput += message + "\n";
            log.WarningFlag = true;
        }

        private bool HasBODIDBeenUsed(DatabaseUser user, Guid bodid)
        {
            if (XmlManagement.GetImportExportLog(user, bodid).Count > 0) //more than one row means this BODID has been previously processed
                return true;

            return false;
        }

        private string AssignHrXmlDatabase(DatabaseUser user, WorkLinksEmployee employee)
        {
            //get hrxml database mapping
            HrxmlDatabaseMapCollection map = ImportExportAccess.GetHrxmlDatabaseMap(user);
            return map.GetDataBaseName(employee.ImportExternalDatabaseLogicalId, employee.ImportExternalDatabaseReferenceId);
        }

        private HrXmlFile GetHrXmlFile(FileStream file)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(file);

            //build document
            HrXmlFile hrXmlFile = new HrXmlFile {Document = doc};

            string rootElementName = doc.DocumentElement.Name;

            if (rootElementName == "nga:ProcessPayServEmp")
            {
                hrXmlFile.ImportFileType = ImportFileType.EmployeePaycode;
                hrXmlFile.XMLImportTag = "HRXML_employee";
                //set filename
                hrXmlFile.FileName = Path.GetFileNameWithoutExtension(file.Name) + "." + hrXmlFile.BODID + Path.GetExtension(file.Name);
            }
            else
            {
                hrXmlFile.ImportFileType = ImportFileType.UnknownHrXml;
            }

            return hrXmlFile;
        }

        private void AddMessageToQueue(ImportExportLog response)
        {
            const string queueName = @".\Private$\hrxml_out";
            const string queueLabel = "WorkLink HrXml outbound message queue";

            _logger.Info($"Queuing message to {queueName}");

            //recoverableMessage.Body = filePath;
            //recoverableMessage.Recoverable = true;

            MessageQueue queue = null;
            if (MessageQueue.Exists(queueName))
            {
                queue = new MessageQueue(queueName);
            }
            else
            {
                // Create the Queue
                MessageQueue.Create(queueName, true);
                queue = new MessageQueue(queueName);
            }

            Message msg = new Message();

            using (MessageQueueTransaction tran = new MessageQueueTransaction())
            {
                tran.Begin();
                msg.Formatter = new XmlMessageFormatter(new[] {typeof(ImportExportLog)});
                msg.Body = response;
                queue.Send(msg, queueLabel, tran);
                tran.Commit();
            }
        }

        public void GetQueueStatus()
        {
        }

        public void InsertRemoteImportExportLog(RemoteImportExportLog item)
        {
            ImportExportAccess.InsertRemoteImportExportLog(item);
        }

        private enum ImportFileType
        {
            PayrollBatch,
            EmployeePaycode,
            UnknownHrXml,
            Unknown
        }

        private enum EmployeeStatus
        {
            Missing,
            Exists,
            Pending
        }

        private class HrXmlFile
        {
            public ImportFileType ImportFileType { get; set; }
            public bool RequiresConfirmation { get; set; }
            public string FileName { get; set; }
            public string EnvironmentIdentifier { get; set; }
            public string ReleaseId { get; set; }
            public XmlDocument Document { get; set; }
            public DateTime? FileCreationDate { get; set; }
            public Guid BODID { get; set; }
            public string XMLImportTag { get; set; }
            public string DatabaseName { get; set; }
            public string AlternateExternalIdentifier1 { get; set; }
            public string AlternateExternalIdentifier2 { get; set; }
            public string AlternateExternalIdentifier3 { get; set; }
            public string AlternateExternalIdentifier4 { get; set; }

            public byte[] GetBytes()
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    Document.Save(stream);

                    return stream.ToArray();
                }
            }
        }

        #region fields

        private MessageQueue _importQueue;
        private MessageQueue _exportQueue;
        private readonly string _importQueueName;
        private readonly string _exportQueueName;
        private readonly string _outputDirectoryPath;
        private XmlManagement _xmlManagement;
        private EmployeeManagement _employeeManagement;
        private PersonManagement _personManagement;
        private WizardManagement _wizardManagement;
        private CodeManagement _codeManagement;
        private PayrollManagement _payrollManagement;
        private readonly string _importExportProcessingDirectory;
        private readonly DatabaseUser _user;
        private ImportExportAccess _importExportAccess;
        private readonly bool _disableNgaResponse;
        private string _ngaHrxmlConfirmationUrl { get; }
        private string _ngaHrxmlConfirmationLogin { get; }
        private string _ngaHrxmlConfirmationPassword { get; }
        private readonly ILogger _logger;

        private bool _hrmlAddressEnabledFlag { get; }
        private bool _hrmlBankingEnabledFlag { get; }
        private bool _hrmlPayrollBatchEnabledFlag { get; }
        private bool _hrxmlBiographicalEnabledFlag { get; }
        private bool _hrxmlEmploymentInformationEnabledFlag { get; }
        private bool _hrmlPaycodeEnabledFlag { get; }
        private bool _hrmlPositionEnabledFlag { get; }
        private bool _hrmlEmailEnabledFlag { get; }
        private bool _hrmlRehireEnabledFlag { get; }
        private bool _hrmlStatutoryDeductionEnabledFlag { get; }
        private bool _hrmlPhoneEnabledFlag { get; }
        private bool _hrmlTerminationEnabledFlag { get; }
        private bool _hrxmlLeaveEnabledFlag { get; }
        private bool _overrideQueue { get; }

        #endregion

        #region properties

        private ImportExportAccess ImportExportAccess => _importExportAccess ?? (_importExportAccess = new ImportExportAccess());

        private EmployeeManagement EmployeeManagement => _employeeManagement ?? (_employeeManagement = new EmployeeManagement());

        private WizardManagement WizardManagement => _wizardManagement ?? (_wizardManagement = new WizardManagement());

        private CodeManagement CodeManagement => _codeManagement ?? (_codeManagement = new CodeManagement());

        private PersonManagement PersonManagement => _personManagement ?? (_personManagement = new PersonManagement());

        private PayrollManagement PayrollManagement => _payrollManagement ?? (_payrollManagement = new PayrollManagement());

        public XmlManagement XmlManagement => _xmlManagement ?? (_xmlManagement = new XmlManagement());

        private MessageQueue ImportQueue
        {
            get
            {
                if (_importQueue == null)
                {
                    // Create an instance of MessageQueue. Set its formatter.
                    _importQueue = new MessageQueue(_importQueueName) {Formatter = new XmlMessageFormatter(new[] {typeof(string)})};
                    _importQueue.PeekCompleted += _importQueue_PeekCompleted;
                }

                return _importQueue;
            }
        }

        private MessageQueue ExportQueue
        {
            get
            {
                if (_exportQueue == null)
                {
                    // Create an instance of MessageQueue. Set its formatter.
                    _exportQueue = new MessageQueue(_exportQueueName) {Formatter = new XmlMessageFormatter(new[] {typeof(string)})};
                    _exportQueue.PeekCompleted += _exportQueue_PeekCompleted;
                }

                return _exportQueue;
            }
        }

        #endregion
    }
}