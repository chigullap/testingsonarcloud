﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;
using WorkLinks.DataLayer.DataAccess.Central;

namespace WorkLinks.BusinessLayer.BusinessLogic.Central
{
    public class ChequeHealthRemittanceManagement
    {
        //add data access
        private ChequeHealthTaxRemittanceAccess _chequeHealthTaxRemittanceAccess = null;

        public ChequeHealthTaxRemittanceAccess ChequeHealthTaxRemittanceAccess
        {
            get
            {
                if (_chequeHealthTaxRemittanceAccess == null)
                    _chequeHealthTaxRemittanceAccess = new ChequeHealthTaxRemittanceAccess();
                return _chequeHealthTaxRemittanceAccess;
            }
        }

        #region methods
        public ChequeHealthTaxRemittanceDatabaseMapCollection GetChequeHealthTaxRemittanceDatabaseMap(DatabaseUser user)
        {
            try
            {
                return ChequeHealthTaxRemittanceAccess.GetChequeHealthTaxRemittanceDatabaseMap(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "ChequeHealthTaxRemittanceManagement - GetChequeHealthRemittanceDatabaseMap", ex);
                throw;
            }
        }

        #endregion

    }
}
