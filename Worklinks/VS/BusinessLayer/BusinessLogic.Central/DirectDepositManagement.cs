﻿using System;
using WLP.BusinessLayer.BusinessObjects;
using WorkLinks.BusinessLayer.BusinessObjects.Central;
using WorkLinks.DataLayer.DataAccess.Central;

namespace WorkLinks.BusinessLayer.BusinessLogic.Central
{
    public class DirectDepositManagement
    {
        //add data access
        private DirectDepositAccess _directDepositAccess = null;

        public DirectDepositAccess DirectDepositAccess
        {
            get
            {
                if (_directDepositAccess == null)
                    _directDepositAccess = new DirectDepositAccess();
                return _directDepositAccess;
            }
        }

        #region methods
        public DirectDepositDatabaseMapCollection GetDirectDepositDatabaseMapCollection(DatabaseUser user)
        {
            try
            {
                return DirectDepositAccess.GetDirectDepositDatabaseMapCollection(user);
            }
            catch (Exception ex)
            {
                ErrorManagement.HandleError(user, "DirectDepositManagement - GetDirectDepositDatabaseMapCollection", ex);
                throw;
            }
        }

        #endregion

    }
}

